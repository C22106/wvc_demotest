package timeshare.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import timeshare.pages.TimeshareHomePage;
import timeshare.pages.TimeshareHomePage_Web;

public class TimeshareScripts extends TestBase {
	public static final Logger log = Logger.getLogger(TimeshareScripts.class);

	@Test(dataProvider = "testData", groups = { "header", "regression", "timeshare" })

	public void tc01_Timeshare_Header_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplication();

			homePage.validateLogo();
			homePage.validateHomeHeaderMenu();

			homePage.validateHomeHeaderSubMenu("Timeshare 101", "Understanding Timeshare", 1);

			homePage.validateHomeHeaderSubMenu("Ownership Basics", "Choose Your Product", 3);
			homePage.validateHomeHeaderSubMenu("Ownership Basics", "Book Your Best Vacation", 3);
			homePage.validateHomeHeaderSubMenu("Ownership Basics", "Understand How Your Product Works", 3);

			homePage.validateHomeHeaderSubMenu("Timeshare Exit Toolkit", "How to Exit a Timeshare", 6);
			homePage.validateHomeHeaderSubMenu("Timeshare Exit Toolkit", "Exit Safely", 6);
			homePage.validateHomeHeaderSubMenu("Timeshare Exit Toolkit", "Timeshare Scams", 6);
			homePage.validateHomeHeaderSubMenu("Timeshare Exit Toolkit", "Signs of Exit Fraud", 6);
			homePage.validateHomeHeaderSubMenu("Timeshare Exit Toolkit", "Resell Safely", 6);
			homePage.validateHomeHeaderSubMenu("Timeshare Exit Toolkit", "Frequently Asked Questions", 6);

			homePage.validateHomeHeaderSubMenu("Resources", "Learn About Ownership", 2);
			homePage.validateHomeHeaderSubMenu("Resources", "Stay Informed About Exit Scams", 2);

			homePage.validateHomeHeaderSubMenu("About Us", "About Us", 0);

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "banner", "regression", "timeshare" })

	public void tc02_Timeshare_Banner_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateBanners();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "exploringOwnerhsip", "regression", "timeshare" })

	public void tc03_Timeshare_Exploring_Ownership_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateCardComponent();
			homePage.validateContentSlice();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "maximizeOwnership", "regression", "timeshare" })

	public void tc04_Timeshare_Maximize_Your_Ownership(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateMaximizeYourOwnership();
			homePage.validateExchangeBanner();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "latestNews", "regression", "timeshare" })

	public void tc05_Timeshare_Latest_News(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateLatestNews();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "keysToGreatVacation", "regression", "timeshare" })

	public void tc06_Timeshare_Keys_To_A_Great_Vacation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateKeysToGreatVacation();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "footer", "wip", "timeshare" })

	public void tc07_Timeshare_Validate_Footer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateFooterMenu();
			homePage.validateHomeFooterSubMenu("Timeshare 101", "Understanding Timeshare", 1);

			homePage.validateHomeFooterSubMenu("Ownership Basics", "Choose Your Product", 3);
			homePage.validateHomeFooterSubMenu("Ownership Basics", "Book Your Best Vacation", 3);
			homePage.validateHomeFooterSubMenu("Ownership Basics", "Understand How Your Product Works", 3);

			homePage.validateHomeFooterSubMenu("Timeshare Toolkit", "How to Exit a Timeshare", 6);
			homePage.validateHomeFooterSubMenu("Timeshare Toolkit", "Exit Safely", 6);
			homePage.validateHomeFooterSubMenu("Timeshare Toolkit", "Timeshare Scams", 6);
			homePage.validateHomeFooterSubMenu("Timeshare Toolkit", "Signs of Exit Fraud", 6);
			homePage.validateHomeFooterSubMenu("Timeshare Toolkit", "Resell Safely", 6);
			homePage.validateHomeFooterSubMenu("Timeshare Toolkit", "Frequently Asked Questions", 6);

			homePage.validateHomeFooterSubMenu("Resources", "Learn About Ownership", 2);
			homePage.validateHomeFooterSubMenu("Resources", "Stay Informed About Exit Scams", 2);

			homePage.validateHomeFooterSubMenu("About Us", "About Us", 0);

			homePage.validateFacebookLink();
			homePage.validateTwitterLink();
			homePage.validateYoutubeLink();
			homePage.validateInstagramLink();

			homePage.validatefooterlogo();
			homePage.validateDoNotSellInfo();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "footer", "brokenlink", "timeshare" })

	public void tc08_Timeshare_Broken_Link_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TimeshareHomePage homePage = new TimeshareHomePage_Web(tcconfig);
			homePage.launchApplicationforBrokenLinkValidation();
			homePage.brokenLinkValidation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}