package timeshare.pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TimeshareHomePage_Android extends TimeshareHomePage_Web {

	protected By hamburgerMenu = By.xpath("//div[@class='navHamburger']/span[2]");
	protected By bannertitle = By.xpath("//div[@class='image-quote-banner']//div[contains(@class,'title-1')]");

	public TimeshareHomePage_Android(TestConfig tcconfig) {
		super(tcconfig);
		timeshareLogo = By.xpath("//a[img[contains(@class,'logo')] and parent::div[contains(@class,'small')]]");

		timeshare101 = By.xpath("//li[contains(@class,'mobileMenuItem')]/a[contains(.,'Timeshare 101')]");
		ownershipBasics = By.xpath("//li[contains(@class,'mobileMenuItem')]/a[contains(.,'Ownership Basics')]");
		timeshareToolkit = By.xpath("//li[contains(@class,'mobileMenuItem')]/a[contains(.,'Timeshare Exit Toolkit')]");
		resources = By.xpath("//li[contains(@class,'mobileMenuItem')]/a[contains(.,'Resources')]");
		aboutUS = By.xpath("//li[contains(@class,'mobileMenuItem')]/a[contains(.,'About Us')]");

		breadcrumbLink = By.xpath("//ul/li[last()]/a[@data-eventname='breadCrumb']");
		contentSliceComponent = By
				.xpath("//div[@class='cardComponent']//div[div/a[@data-eventname='Card']/img[contains(@src,'svg')]]");
		
		maximizeOwnershipImage = By.xpath(
				"(//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[@class='cardBanner']//img)[2]");

		maximizeOwnershipTitle = By.xpath(
				"(//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[2]");
		maximizeOwnershipBody = By.xpath(
				"(//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[2]");
		btnLearnMoreMaximizeOwnership = By.xpath(
				"(//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[2]");
		
		exchangeOwnershipImage = By
				.xpath("(//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//img)[2]");
		exchangeOwnershipTitle = By.xpath(
				"(//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//div[contains(@class,'title')])[3]");
		exchangeOwnershipBody = By.xpath(
				"(//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//div[contains(@class,'body')]/p)[2]");
		btnLearnMoreExchangeOwnership = By
				.xpath("(//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//a)[2]");
	}

	public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href");
		// String title = getElementAttribute(link, "data-eventlabel");
		String title = getObject(link).findElement(By.xpath("./span")).getText();
		clickElementBy(link);
		pageCheck();
		assertTrue(getCurrentURL().trim().equalsIgnoreCase(href),
				"Link Navigation not correct, expected : " + href + " | Actual : " + getCurrentURL().trim());

		/*assertTrue(getElementText(bannertitle).trim().equalsIgnoreCase(title),
				"Link Navigation not correct, expected Title: " + title + " | Actual Title: "
						+ getElementText(bannertitle).trim());*/

		tcConfig.updateTestReporter("TimeshareHomePage", "navigateValidateAndReturn", Status.PASS,
				"Title : " + title + " navigating to link : " + href + " successful");

		navigateBack();
		pageCheck();

	}

	public void validateHomeHeaderMenu() {

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(timeshare101), "Timeshare 101 menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Timeshare 101 menu item is present");
		navigateValidateAndReturn(timeshare101);

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(ownershipBasics), "Ownership basics menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Ownership basics menu item is present");
		navigateValidateAndReturn(ownershipBasics);

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(timeshareToolkit), "Timeshare toolkit menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Timeshare toolkit menu item is present");
		navigateValidateAndReturn(timeshareToolkit);

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(resources), "Resources menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Resources menu item is present");
		navigateValidateAndReturn(resources);

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(aboutUS), "About US menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"About US menu item is present");
		navigateValidateAndReturn(aboutUS);
	}

	private void clickHamburgerMenu() {
		clickElementBy(hamburgerMenu);
		assertTrue(verifyObjectDisplayed(timeshare101), "Hamburger Menu not clicked successfully");

	}

	public void validateHomeHeaderSubMenu(String headerMenuItem, String subMenu, int count) {

		clickHamburgerMenu();
		if (count == 0) {

			String href = getElementAttribute(
					By.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='" + headerMenuItem + "']]]/a"),
					"href");
			clickElementBy(
					By.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='" + headerMenuItem + "']]]/a"));

			pageCheck();
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), subMenu + " navigations not successful");

			assertTrue(
					getElementText(bannertitle).trim().equalsIgnoreCase(headerMenuItem)
							|| getElementText(bannertitle).trim().equalsIgnoreCase(subMenu),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();

		} else {

			clickElementBy(By
					.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='" + headerMenuItem + "']]]/button"));
			waitForSometime("1");

			assertTrue(
					getList(By.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='" + headerMenuItem
							+ "']]]//div[@aria-hidden='false']/a")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='" + headerMenuItem
									+ "']]]//div[@aria-hidden='false']/a")).size());

			assertTrue(
					verifyObjectDisplayed(By.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='"
							+ headerMenuItem + "']]]//div[@aria-hidden='false']/a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			String href = getElementAttribute(By.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='"
					+ headerMenuItem + "']]]//div[@aria-hidden='false']/a[contains(.,'" + subMenu + "')]"), "href");
			clickElementJSWithWait(By.xpath("//*[contains(@class,'mobileMenuItem')][a[span[text()='" + headerMenuItem
					+ "']]]//div[@aria-hidden='false']/a[contains(.,'" + subMenu + "')]"));

			pageCheck();

			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), subMenu + " navigations not successful");

			/*assertTrue(
					getElementText(bannertitle).trim().equalsIgnoreCase(headerMenuItem)
							|| getElementText(bannertitle).trim().equalsIgnoreCase(subMenu),
					subMenu + " navigations not successful");*/

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();
		}

	}

	public void validateBanners() {

		String prevBannerTitle = "";
		List<WebElement> slickList = getList(slickDots);

		for (int i = 0; i < slickList.size(); i++) {

			slickList = getList(slickDots);
			slickList.get(i).click();
			waitForSometime("3");
			assertTrue(getElementAttribute(By.xpath("(//ul[@class='slick-dots']/li)[" + (i + 1) + "]"), "class").trim()
					.equalsIgnoreCase("slick-active"), "Correct slick dot not higlighted");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateBanners", Status.PASS,
					"Correct banner is displayed");
			assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
			String bannerTitle = getElementText(bannerTitleObj);
			assertTrue(!bannerTitle.trim().equals(""), "Banner title absent");
			assertTrue(!bannerTitle.trim().equals(prevBannerTitle), "Banner title is not changing");
			prevBannerTitle = bannerTitle;
			assertTrue(verifyObjectDisplayed(learnMoreBtn), "Learn more button is absent");
			String linkdetails = getElementAttribute(learnMoreBtn, "href");
			clickElementBy(learnMoreBtn);
			pageCheck();
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(linkdetails),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateBanners", Status.PASS,
					"Banner Title : " + bannerTitle + " navigating to link : " + linkdetails + " successful");
			navigateBack();
			pageCheck();

		}

	}

	public void validateContentSlice() {

		List<WebElement> contentSliceList = getList(contentSliceComponent);

		for (int i = 0; i < contentSliceList.size(); i++) {
			contentSliceList = getList(contentSliceComponent);
			assertTrue(
					contentSliceList.get(i).findElement(By.xpath("./div/a/img")).getAttribute("src").contains(".svg"),
					"Exploring option image is absent");

			String subTitle = contentSliceList.get(i).findElement(By.xpath(".//div[@class='card-section']/a/div"))
					.getText().trim();

			assertTrue(subTitle.length() > 0, "Exploring option subtitle not present in Content slice");

			assertTrue(contentSliceList.get(i)
					.findElement(By.xpath(".//div[@class='card-section']/a/div[@class='body-1']/p")).getText()
					.length() > 0, "Exploring option text is absent");

			WebElement btnLearnMore = contentSliceList.get(i).findElement(By.xpath(".//a[2]"));

			assertTrue(verifyObjectDisplayed(btnLearnMore), "Exploring option Learn more button is absent");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateContentSlice", Status.PASS,
					"All content is validated successfully within the content slice, which has title : " + subTitle);

			String href = getElementAttribute(btnLearnMore, "href");

			clickElementBy(btnLearnMore);
			pageCheck();

			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
					"Navigation to learn more link not correct");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateContentSlice", Status.PASS,
					"Content Title : " + subTitle + " navigating to link : " + href + " successful");

			navigateBack();
			pageCheck();

		}
	}

	public void validateFacebookLink() {

		assertTrue(verifyObjectDisplayed(facebookIcon), "Facebook icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateFacebookLink", Status.PASS,
				"Facebook icon is present");

		clickElementBy(facebookIcon);
		// pageCheck();
		waitForSometime("3");

		assertTrue(getCurrentURL().trim().equalsIgnoreCase("https://m.facebook.com/timesharecom"),
				"Facebook link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateFacebookLink", Status.PASS,
				"Facebook link navigation successful");

		navigateToURL(url);
		pageCheck();

	}

	public void validateTwitterLink() {

		assertTrue(verifyObjectDisplayed(twitterIcon), "Twitter icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateTwitterLink", Status.PASS, "Twitter icon is present");

		clickElementBy(twitterIcon);
		// pageCheck();
		waitForSometime("3");

		assertTrue(getCurrentURL().trim().equalsIgnoreCase("https://mobile.twitter.com/timeshare_com"),
				"Twitter link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateTwitterLink", Status.PASS,
				"Twitter link navigation successful");
		navigateToURL(url);
		pageCheck();

	}

	public void validateYoutubeLink() {

		assertTrue(verifyObjectDisplayed(youtubeIcon), "Youtube icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateYoutubeLink", Status.PASS, "Youtube icon is present");

		clickElementBy(youtubeIcon);
		// pageCheck();
		waitForSometime("3");

		assertTrue(getCurrentURL().trim().equalsIgnoreCase("https://m.youtube.com/channel/UCwy9UWOWSLAypQmetVS95Cw"),
				"Youtube link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateYoutubeLink", Status.PASS,
				"Youtube link navigation successful");
		navigateToURL(url);
		pageCheck();

	}

	public void validateInstagramLink() {

		assertTrue(verifyObjectDisplayed(instaIcon), "Instagram icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateInstagramLink", Status.PASS,
				"Instagram icon is present");

		clickElementBy(instaIcon);
		// pageCheck();
		waitForSometime("3");

		assertTrue(getCurrentURL().trim().equalsIgnoreCase("https://www.instagram.com/timeshare_com/"),
				"Instagram link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateYoutubeLink", Status.PASS,
				"Instagram link navigation successful");
		navigateToURL(url);
		pageCheck();

	}

	public void validateDoNotSellInfo() {
		assertTrue(verifyObjectDisplayed(doNotsellInfo), "Do not sell info link is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
				"Do not sell info link is present");

		clickElementBy(doNotsellInfo);
		// pageCheck();
		waitForSometime("3");

		assertTrue(
				getCurrentURL().trim().equalsIgnoreCase(
						"https://www.wyndhamdestinations.com/us/en/privacy-notice/opt-out-request-form"),
				"Opt out form not displayed");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
				"Opt out form is displayed");

	}
	
	@Override
	public void validateMaximizeYourOwnership() {

		assertTrue(verifyObjectDisplayed(maximizeOwnershipHeader), "Maximize ownership header is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateMaximizeYourOwnership", Status.PASS,
				"Maximize ownership header is present");

		assertTrue(verifyObjectDisplayed(maximizeOwnershipImage), "Maximize ownership image is absent");
		assertTrue(verifyObjectDisplayed(maximizeOwnershipTitle), "Maximize ownership title is absent");
		assertTrue(getElementText(maximizeOwnershipTitle).trim().length() > 0, "Maximize ownership title is blank");
		String title = getElementText(maximizeOwnershipTitle).trim();

		assertTrue(verifyObjectDisplayed(maximizeOwnershipBody), "Maximize ownership body is absent");
		assertTrue(getElementText(maximizeOwnershipBody).trim().length() > 0, "Maximize ownership body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreMaximizeOwnership),
				"Maximize ownership Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreMaximizeOwnership, "href");
		clickElementBy(btnLearnMoreMaximizeOwnership);
		pageCheck();
		assertTrue(getElementAttribute(breadcrumbLink, "href").trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("TimeshareHomePage", "validateMaximizeYourOwnership", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}
	
	@Override
	public void validateExchangeBanner() {

		assertTrue(verifyObjectDisplayed(exchangeOwnershipImage), "Exchange ownership image is absent");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateExchangeBanner", Status.PASS,
				"Exchange ownership image is present");

		assertTrue(verifyObjectDisplayed(exchangeOwnershipTitle), "Maximize ownership title is absent");
		assertTrue(getElementText(exchangeOwnershipTitle).trim().length() > 0, "Maximize ownership title is blank");
		String title = getElementText(exchangeOwnershipTitle).trim();

		assertTrue(verifyObjectDisplayed(exchangeOwnershipBody), "Maximize ownership body is absent");
		assertTrue(getElementText(exchangeOwnershipBody).trim().length() > 0, "Maximize ownership body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreExchangeOwnership),
				"Maximize ownership Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreExchangeOwnership, "href");
		clickElementBy(btnLearnMoreExchangeOwnership);
		pageCheck();
		assertTrue(getElementAttribute(breadcrumbLink, "href").trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateExchangeBanner", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();

	}

}
