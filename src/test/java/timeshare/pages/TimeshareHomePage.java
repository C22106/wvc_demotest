package timeshare.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public abstract class TimeshareHomePage extends FunctionalComponents {

	protected By cookieAlert = By.xpath("//button[contains(@class,'setCookie')]");
	// protected By timeshareLogo =
	// By.xpath("//img[contains(@class,'show-for-large') and
	// contains(@class,'logo')]");
	protected By timeshareLogo = By
			.xpath("//a[img[contains(@class,'logo')] and parent::div[contains(@class,'large')]]");
	// protected By timeshare101 =
	// By.xpath("//li[@role='menuitem']/a[contains(@href,'timeshare-101')]");
	protected By timeshare101 = By.xpath("//div[@class='menuPrimaryItems']/a[contains(.,'Timeshare 101')]");
	protected By ownershipBasics = By.xpath("//div[@class='menuPrimaryItems']/a[contains(.,'Ownership Basics')]");
	protected By timeshareToolkit = By
			.xpath("//div[@class='menuPrimaryItems']/a[contains(.,'Timeshare Exit Toolkit')]");
	protected By resources = By.xpath("//div[@class='menuPrimaryItems']/a[contains(.,'Resources')]");
	protected By aboutUS = By.xpath("//div[@class='menuPrimaryItems']/a[contains(.,'About Us')]");
	protected By slickDots = By.xpath("//ul[@class='slick-dots']/li");
	protected By bannerImgSrc = By.xpath("//div[@aria-hidden='false']//div[@class='image-quote-banner']//img");
	protected By bannerTitleObj = By.xpath("//div[@aria-hidden='false']//div[contains(@class,'title-1')]");
	protected By learnMoreBtn = By.xpath("//div[@aria-hidden='false']//a[text()='Learn More']");
	protected By breadcrumbLink = By.xpath("//ul/li[last()]/a[@data-eventname='breadCrumb']");
	protected By exploringTimeshareHeader = By.xpath("//div[text()='Exploring Timeshare Ownership']");
	protected By cardComponent = By.xpath(
			"//div[contains(.,'Exploring Timeshare')]/div[@class='cardComponent']//div[@class='cell']/div/div[a]");

	protected By contentSliceComponent = By.xpath("//div[div[@class='ctaSlice']][div[@class='imageSlice']]");
	protected By maximizeOwnershipHeader = By
			.xpath("//div[contains(@class,'caption') and contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]");
	protected By maximizeOwnershipImage = By.xpath(
			"//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[@class='cardBanner']//img");

	protected By maximizeOwnershipTitle = By.xpath(
			"//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1']");
	protected By maximizeOwnershipBody = By.xpath(
			"//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')]");
	protected By btnLearnMoreMaximizeOwnership = By.xpath(
			"//div[contains(.,'HOW TO MAXIMIZE YOUR OWNERSHIP')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a");
	protected By exchangeOwnershipImage = By
			.xpath("//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//img");
	protected By exchangeOwnershipTitle = By.xpath(
			"//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//div[contains(@class,'title')]");
	protected By exchangeOwnershipBody = By.xpath(
			"//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//div[contains(@class,'body')]/p");
	protected By btnLearnMoreExchangeOwnership = By
			.xpath("//section[contains(@class,'banner  animation-wrapper ') and contains(.,'Exchange')]//a");

	protected By greatVacationHeader = By.xpath(
			"//div[contains(.,'Keys To A Great Timeshare Vacation') and contains(@class,'contentSlice')]//div[contains(@class,'caption')]");
	protected By keystoVacationComponent = By.xpath(
			"//div[contains(.,'Keys To A Great Timeshare Vacation')]/div[@class='cardComponent']//div[@class='cell']/div/div[a]");
	protected By latestNewsHeader = By.xpath("//div[@class='contentSlice' and contains(.,'Latest News')]");
	protected By latestNewsComponent = By.xpath(
			"//div[contains(.,'Latest News')]/div[@class='indexCardComponent']//div[@class='cell']/div/div[a and @data-ishidden='false']");
	protected By aboutUSFooter = By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='About Us']]");
	protected By resourcesFooter = By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='Resources']]");
	protected By timeshareToolkitFooter = By
			.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='Timeshare Toolkit']]");
	protected By ownershipBasicsFooter = By
			.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='Ownership Basics']]");
	protected By timeshare101Footer = By
			.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='Timeshare 101']]");

	protected By facebookIcon = By.xpath("//li/a[contains(@href,'www.facebook.com/timesharecom')]");
	protected By facebookPageObj = By.xpath("//a[contains(@href,'www.facebook.com/timesharecom')]");

	protected By twitterIcon = By.xpath("//li/a[contains(@href,'https://twitter.com/timeshare_com')]");
	protected By twitterPageObj = By.xpath("//span[text()='@Timeshare_com']");

	protected By instaIcon = By.xpath("//li/a[contains(@href,'https://www.instagram.com/timeshare_com/')]");
	protected By instaPageObj = By.xpath("//a[@page_id='profilePage' and text()='www.timeshare.com']");

	protected By youtubeIcon = By
			.xpath("//li/a[contains(@href,'https://www.youtube.com/channel/UCwy9UWOWSLAypQmetVS95Cw')]");
	protected By youtubePageObj = By.xpath("//div[@id='text-container' and contains(.,'Timeshare Info')]");
	protected By footerWyndhamLogo = By.xpath("//a[@data-eventlabel='FooterLogo' and img[@alt='Wyndham Logo']]");
	protected By privacyNotice = By.xpath("//a[@data-eventlabel='Privacy Notice']");
	protected By doNotsellInfo = By.xpath("//a[@data-eventlabel='Sitemap']");
	protected By optOutForm = By.xpath("//div[text()='Opt Out Request Form']");

	protected String url;

	public TimeshareHomePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	public void launchApplication() {

		url = tcConfig.getTestData().get("URL");
		driver.get(url);
		pageCheck();
		if (verifyObjectDisplayed(cookieAlert)) {
			clickElementBy(cookieAlert);
		}

		tcConfig.updateTestReporter("TimeshareHomePage", "launchApplication", Status.PASS,
				"Timeshare URL was launched successfully");
	}

	public void validateLogo() {
		assertTrue(verifyObjectDisplayed(timeshareLogo), "Header Timeshare logo is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateLogo", Status.PASS,
				"Header Timeshare logo is displayed");

		assertTrue(getElementAttribute(timeshareLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Header Timeshare logo url not correct");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateLogo", Status.PASS, "Header Wyndham logo is present");
	}

	public void validateHomeHeaderMenu() {

		assertTrue(verifyObjectDisplayed(timeshare101), "Timeshare 101 menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Timeshare 101 menu item is present");
		navigateValidateAndReturn(timeshare101);

		assertTrue(verifyObjectDisplayed(ownershipBasics), "Ownership basics menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Ownership basics menu item is present");
		navigateValidateAndReturn(ownershipBasics);

		assertTrue(verifyObjectDisplayed(timeshareToolkit), "Timeshare toolkit menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Timeshare toolkit menu item is present");
		navigateValidateAndReturn(timeshareToolkit);

		assertTrue(verifyObjectDisplayed(resources), "Resources menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Resources menu item is present");
		navigateValidateAndReturn(resources);

		assertTrue(verifyObjectDisplayed(aboutUS), "About US menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"About US menu item is present");
		navigateValidateAndReturn(aboutUS);
	}

	public void validateHomeHeaderSubMenu(String headerMenuItem, String subMenu, int count) {

		if (count == 0) {
			clickElementBy(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem + "']]]"));
			pageCheck();

			assertTrue(
					verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();

		} else {
			// scrollDownForElementJSBy(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='"
			// + headerMenuItem + "']]]"));
			/*
			 * hoverOnElementWithoutClick(
			 * By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" +
			 * headerMenuItem + "']]]"));
			 */

			hoverOnElementWithJS(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem + "']]]"));
			waitForSometime("1");

			assertTrue(
					getList(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem
							+ "']]]/ul[not(contains(@style,'none'))]/li"))
									.size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem
									+ "']]]/ul[not(contains(@style,'none'))]/li")).size());

			assertTrue(
					verifyObjectDisplayed(
							By.xpath("//ul[contains(@class,'float-center')]//a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			// clickElementBy(By.xpath("//ul[contains(@class,'float-center')]//a[contains(.,'"
			// + subMenu + "')]"));

			clickElementJSWithWait(By.xpath("//ul[contains(@class,'float-center')]//a[contains(.,'" + subMenu + "')]"));

			pageCheck();

			if (subMenu.equalsIgnoreCase("Frequently Asked Questions")) {
				subMenu = "FAQs";
			} else if (subMenu.equalsIgnoreCase("Signs of Exit Fraud")) {
				subMenu = "Signs of Timeshare Exit Fraud";
			} else if (subMenu.equalsIgnoreCase("Learn About Ownership")) {
				subMenu = "Learn More About Timeshare Ownership";
			} else if (subMenu.equalsIgnoreCase("Stay Informed About Exit Scams")) {
				subMenu = "Stay Informed About Timeshare Exit Scams";
			}

			assertTrue(verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();
		}

	}

	public void validateBanners() {

		String prevBannerTitle = "";
		List<WebElement> slickList = getList(slickDots);

		for (int i = 0; i < slickList.size(); i++) {

			slickList = getList(slickDots);
			slickList.get(i).click();
			waitForSometime("3");
			assertTrue(getElementAttribute(By.xpath("(//ul[@class='slick-dots']/li)[" + (i + 1) + "]"), "class").trim()
					.equalsIgnoreCase("slick-active"), "Correct slick dot not higlighted");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateBanners", Status.PASS,
					"Correct banner is displayed");
			assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
			String bannerTitle = getElementText(bannerTitleObj);
			assertTrue(!bannerTitle.trim().equals(""), "Banner title absent");
			assertTrue(!bannerTitle.trim().equals(prevBannerTitle), "Banner title is not changing");
			prevBannerTitle = bannerTitle;
			assertTrue(verifyObjectDisplayed(learnMoreBtn), "Learn more button is absent");
			String linkdetails = getElementAttribute(learnMoreBtn, "href");
			clickElementBy(learnMoreBtn);
			pageCheck();
			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(linkdetails),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateBanners", Status.PASS,
					"Banner Title : " + bannerTitle + " navigating to link : " + linkdetails + " successful");
			navigateBack();
			pageCheck();

		}

	}

	public void validateCardComponent() {

		assertTrue(verifyObjectDisplayed(exploringTimeshareHeader), "Timeshare toolkit menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateCardComponent", Status.PASS,
				"Exploring timeshare ownership header present");

		List<WebElement> cardList = getList(cardComponent);
		for (int i = 0; i < cardList.size(); i++) {

			cardList = getList(cardComponent);

			assertTrue(cardList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Exploring Timeshare cards link not present");
			String hrefCard = cardList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(cardList.get(i).findElement(By.xpath("./div/a")).isDisplayed(),
					"Exploring Timeshare pictures link not present");
			String hrefPicture = cardList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Exploring Timeshare card section link not present");
			String hrefCardSection = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Exploring Timeshare Cards");

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.isDisplayed(), "Exploring Timeshare card section title not present");
			String cardTitle = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateCardComponent", Status.PASS,
					"All links are in sync within the Exploring Timeshare Card, which is : " + hrefCard);

			WebElement eleLearnMore = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Learn More']"));
			clickElementBy(eleLearnMore);
			pageCheck();
			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(hrefCard),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateBanners", Status.PASS,
					"Banner Title : " + cardTitle + " navigating to link : " + hrefCard + " successful");
			navigateBack();
			pageCheck();

		}
	}

	public void validateContentSlice() {

		List<WebElement> contentSliceList = getList(contentSliceComponent);

		for (int i = 0; i < contentSliceList.size(); i++) {
			contentSliceList = getList(contentSliceComponent);
			assertTrue(
					contentSliceList.get(i)
							.findElement(
									By.xpath("./div[@class='imageSlice']//div[contains(@class,'show-for-large')]/img"))
							.getAttribute("src").contains(".svg"),
					"Exploring option image is absent");

			String subTitle = contentSliceList.get(i)
					.findElement(By.xpath("./div[@class='contentSlice']/div[contains(@class,'hide-for-small')]//h3"))
					.getText().trim();

			assertTrue(subTitle.length() > 0, "Exploring option subtitle not present in Content slice");

			assertTrue(contentSliceList.get(i)
					.findElement(By
							.xpath("./div[@class='contentSlice']/div[contains(@class,'hide-for-small')]//div[contains(@class,'body-1')]/p"))
					.getText().length() > 0, "Exploring option text is absent");

			WebElement btnLearnMore = contentSliceList.get(i).findElement(By.xpath("./div[@class='ctaSlice']//a"));

			assertTrue(verifyObjectDisplayed(btnLearnMore), "Exploring option Learn more button is absent");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateContentSlice", Status.PASS,
					"All content is validated successfully within the content slice, which has title : " + subTitle);

			String href = getElementAttribute(btnLearnMore, "href");

			clickElementBy(btnLearnMore);
			pageCheck();

			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
					"Navigation to learn more link not correct");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateContentSlice", Status.PASS,
					"Content Title : " + subTitle + " navigating to link : " + href + " successful");

			navigateBack();
			pageCheck();

		}
	}

	public void validateMaximizeYourOwnership() {

		assertTrue(verifyObjectDisplayed(maximizeOwnershipHeader), "Maximize ownership header is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateMaximizeYourOwnership", Status.PASS,
				"Maximize ownership header is present");

		assertTrue(verifyObjectDisplayed(maximizeOwnershipImage), "Maximize ownership image is absent");
		assertTrue(verifyObjectDisplayed(maximizeOwnershipTitle), "Maximize ownership title is absent");
		assertTrue(getElementText(maximizeOwnershipTitle).trim().length() > 0, "Maximize ownership title is blank");
		String title = getElementText(maximizeOwnershipTitle).trim();

		assertTrue(verifyObjectDisplayed(maximizeOwnershipBody), "Maximize ownership body is absent");
		assertTrue(getElementText(maximizeOwnershipBody).trim().length() > 0, "Maximize ownership body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreMaximizeOwnership),
				"Maximize ownership Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreMaximizeOwnership, "href");
		clickElementBy(btnLearnMoreMaximizeOwnership);
		pageCheck();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("TimeshareHomePage", "validateMaximizeYourOwnership", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	public void validateExchangeBanner() {

		assertTrue(verifyObjectDisplayed(exchangeOwnershipImage), "Exchange ownership image is absent");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateExchangeBanner", Status.PASS,
				"Exchange ownership image is present");

		assertTrue(verifyObjectDisplayed(exchangeOwnershipTitle), "Maximize ownership title is absent");
		assertTrue(getElementText(exchangeOwnershipTitle).trim().length() > 0, "Maximize ownership title is blank");
		String title = getElementText(exchangeOwnershipTitle).trim();

		assertTrue(verifyObjectDisplayed(exchangeOwnershipBody), "Maximize ownership body is absent");
		assertTrue(getElementText(exchangeOwnershipBody).trim().length() > 0, "Maximize ownership body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreExchangeOwnership),
				"Maximize ownership Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreExchangeOwnership, "href");
		clickElementBy(btnLearnMoreExchangeOwnership);
		pageCheck();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateExchangeBanner", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();

	}

	public void validateKeysToGreatVacation() {

		assertTrue(verifyObjectDisplayed(greatVacationHeader), "Keys To A Great Vacation header is absent");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateKeysToGreatVacation", Status.PASS,
				"Keys To A Great Vacation header is present");

		List<WebElement> keystoVacationList = getList(keystoVacationComponent);

		for (int i = 0; i < keystoVacationList.size(); i++) {

			keystoVacationList = getList(keystoVacationComponent);

			assertTrue(keystoVacationList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Exploring Timeshare cards link not present");
			String hrefCard = keystoVacationList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(keystoVacationList.get(i).findElement(By.xpath("./div/a")).isDisplayed(),
					"Exploring Timeshare pictures link not present");
			String hrefPicture = keystoVacationList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(keystoVacationList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Exploring Timeshare card section link not present");
			String hrefCardSection = keystoVacationList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Exploring Timeshare Cards");

			assertTrue(keystoVacationList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.isDisplayed(), "Exploring Timeshare card section title not present");
			String cardTitle = keystoVacationList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateKeysToGreatVacation", Status.PASS,
					"All links are in sync within the Keys to Great Vacation Card, which is : " + hrefCard);
			WebElement eleLearnMore = keystoVacationList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Read More']"));
			clickElementJSWithWait(eleLearnMore);

			pageCheck();
			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(hrefCard),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateKeysToGreatVacation", Status.PASS,
					"Keys to Great vacation Title : " + cardTitle + " navigating to link : " + hrefCard
							+ " successful");
			navigateBack();
			pageCheck();

		}

	}

	public void validateLatestNews() {

		assertTrue(verifyObjectDisplayed(latestNewsHeader), "Latest News header is absent");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateLatestNews", Status.PASS,
				"Latest News header is present");

		List<WebElement> newsList = getList(latestNewsComponent);

		for (int i = 0; i < newsList.size(); i++) {

			newsList = getList(latestNewsComponent);

			assertTrue(newsList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Latest News cards link not present");
			String hrefCard = newsList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(newsList.get(i).findElement(By.xpath("./div/a")).isDisplayed(),
					"Latest News pictures link not present");
			String hrefPicture = newsList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Latest News card section link not present");
			String hrefCardSection = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Latest News card");

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.isDisplayed(), "Latest News card section title not present");
			String cardTitle = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateLatestNews", Status.PASS,
					"All links are in sync within the Latest News Card, which is : " + hrefCard);

			WebElement eleReadMore = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Read More']"));
			clickElementBy(eleReadMore);

			pageCheck();
			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(hrefCard),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateLatestNews", Status.PASS,
					"Latest News Title : " + cardTitle + " navigating to link : " + hrefCard + " successful");
			navigateBack();
			pageCheck();

		}
	}

	public void validateFooterMenu() {

		assertTrue(verifyObjectDisplayed(timeshare101Footer), "Timeshare 101 menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Timeshare 101 menu item is present");
		navigateValidateAndReturn(timeshare101Footer);

		assertTrue(verifyObjectDisplayed(ownershipBasicsFooter), "Ownership basics menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Ownership basics menu item is present");
		navigateValidateAndReturn(ownershipBasicsFooter);

		assertTrue(verifyObjectDisplayed(timeshareToolkitFooter), "Timeshare toolkit menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Timeshare toolkit menu item is present");
		navigateValidateAndReturn(timeshareToolkitFooter);

		assertTrue(verifyObjectDisplayed(resourcesFooter), "Resources menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Resources menu item is present");
		navigateValidateAndReturn(resourcesFooter);

		assertTrue(verifyObjectDisplayed(aboutUSFooter), "About US menu item is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderMenu", Status.PASS,
				"About US menu item is present");
		navigateValidateAndReturn(aboutUSFooter);

	}

	public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href");
		String title = getElementAttribute(link, "data-eventlabel");
		clickElementBy(link);
		pageCheck();
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
				"Link Navigation not correct, expected : " + href + " | Actual : "
						+ getCurrentURL().trim());
		tcConfig.updateTestReporter("TimeshareHomePage", "navigateValidateAndReturn", Status.PASS,
				"Latest News Title : " + title + " navigating to link : " + href + " successful");

		navigateBack();
		pageCheck();

	}

	public void validateHomeFooterSubMenu(String headerMenuItem, String subMenu, int count) {

		if (count == 0) {
			navigateValidateAndReturn(
					By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='" + headerMenuItem + "']]"));

		} else {

			assertTrue(
					getList(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='" + headerMenuItem
							+ "']]/following-sibling::ul//a")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='"
									+ headerMenuItem + "']]/following-sibling::ul//a")).size());

			assertTrue(
					verifyObjectDisplayed(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='"
							+ headerMenuItem + "']]/following-sibling::ul//a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeFooterSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			navigateValidateAndReturn(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='"
					+ headerMenuItem + "']]/following-sibling::ul//a[contains(.,'" + subMenu + "')]"));
		}

	}

	public void validateFacebookLink() {

		assertTrue(verifyObjectDisplayed(facebookIcon), "Facebook icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateFacebookLink", Status.PASS,
				"Facebook icon is present");

		clickElementBy(facebookIcon);
		waitForSometime("5");
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		// pageCheck();
		newTabNavigations(tabs2);
		assertTrue(getCurrentURL().trim().contains("https://www.facebook.com/"),
				"Facebook link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateFacebookLink", Status.PASS,
				"Facebook link navigation successful");
		navigateToMainTab(tabs2);

	}

	public void validateTwitterLink() {

		assertTrue(verifyObjectDisplayed(twitterIcon), "Twitter icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateTwitterLink", Status.PASS, "Twitter icon is present");

		clickElementBy(twitterIcon);
		waitForSometime("5");
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		// pageCheck();
		newTabNavigations(tabs2);
		assertTrue(getCurrentURL().trim().contains("https://twitter.com/timeshare_com"),
				"Twitter link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateTwitterLink", Status.PASS,
				"Twitter link navigation successful");
		navigateToMainTab(tabs2);

	}

	public void validateYoutubeLink() {

		assertTrue(verifyObjectDisplayed(youtubeIcon), "Youtube icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateYoutubeLink", Status.PASS, "Youtube icon is present");

		clickElementBy(youtubeIcon);
		waitForSometime("5");
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		// pageCheck();
		newTabNavigations(tabs2);
		assertTrue(getCurrentURL().trim().contains("https://www.youtube.com/"),
				"Youtube link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateYoutubeLink", Status.PASS,
				"Youtube link navigation successful");
		navigateToMainTab(tabs2);

	}

	public void validateInstagramLink() {

		assertTrue(verifyObjectDisplayed(instaIcon), "Instagram icon is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateInstagramLink", Status.PASS,
				"Instagram icon is present");
		String href = getElementAttribute(instaIcon, "href");
		clickElementBy(instaIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		// pageCheck();
		newTabNavigations(tabs2);
		// assertTrue(verifyObjectDisplayed(instaPageObj), "Instagram link
		// navigation not successful");
		assertTrue(driver.getCurrentUrl().contains("https://www.instagram.com/"),
				"Instagram link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateYoutubeLink", Status.PASS,
				"Instagram link navigation successful");
		navigateToMainTab(tabs2);

	}

	public void validatefooterlogo() {

		assertTrue(verifyObjectDisplayed(footerWyndhamLogo), "Footer Wyndham logo is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validatefooterlogo", Status.PASS,
				"Footer Wyndham logo is present");

		assertTrue(getElementAttribute(footerWyndhamLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Footer url not correct");
		tcConfig.updateTestReporter("TimeshareHomePage", "validatefooterlogo", Status.PASS,
				"Footer Wyndham logo is present");

	}

	public void validatePrivacyNotice() {

		assertTrue(verifyObjectDisplayed(privacyNotice), "Privacy notice link is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validatePrivacyNotice", Status.PASS,
				"Privacy notice link is present");
		navigateValidateAndReturn(privacyNotice);

	}

	public void validateDoNotSellInfo() {
		assertTrue(verifyObjectDisplayed(doNotsellInfo), "Do not sell info link is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
				"Do not sell info link is present");
		clickElementBy(doNotsellInfo);
		pageCheck();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if (tabs.size() == 2) {
			newTabNavigations(tabs);
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateToMainTab(tabs);
		} else {
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateBack();
			pageCheck();
		}

	}

	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("html")) {
					System.out.println("URL contians html");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it.");
				 * report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components",
							"brokenLinkValidation", Status.FAIL, "[Response Code: " + respCode + "] [Response message: "
									+ respMessage + "] [URL:" + url + "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}

	public void launchApplicationforBrokenLinkValidation() {
		driver.get(tcConfig.getTestData().get("URL"));

	}

}
