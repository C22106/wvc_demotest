package panoramaco.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public abstract class PanoramacoHomePage extends FunctionalComponents {

	protected By cookieAlert = By.xpath("//button[contains(@class,'setCookie')]");
	protected By panoramacoLogo = By
			.xpath("(//a[img[contains(@class,'logo')] and parent::div[contains(@class,'small')]])[1]");
	protected By bookYourVacation = By.xpath("//li[@class='mainMenuItems']/a[contains(.,'Book Your Vacation')]");
	protected By careers = By.xpath("//li[@class='mainMenuItems']/a[contains(.,'Careers')]");
	protected By contactUs = By.xpath("//li[@class='mainMenuItems']/a[contains(.,'Contact Us')]");
	protected By aboutUS = By.xpath("//li[@class='mainMenuItems']/a[contains(.,'About Us')]");

	protected By bannerImgSrc = By.xpath("//div[@class='titleBanner']/div[contains(@class,'image')]/img");
	protected By bannerTitleObj = By
			.xpath("//div[@class='titleBanner']//div[contains(@class,'bannerText')]/div[@class='title-1']");
	protected By bannerHeader = By.xpath("//div[@class='contentSlice']//h2[contains(@class,'title-1')]");
	protected By bannerSubDescription = By.xpath(
			"//div[@class='contentSlice']//following-sibling::h2[contains(@class,'title-1')]/../../../..//div[contains(@class,'body-1')]");

	protected By videoBanner = By.xpath("(//div[@class='videoSectionSimple']//img)[1]");
	protected By videoPlayerButton = By.xpath("(//div[@class='videoSectionSimple']//img)[1]/../../a");
	protected By iframe = By.xpath("//iframe[@title='vimeo-player']");

	protected By videoPlayerHeader = By.xpath("//div[@id='player']//h1/a");
	protected By closeButton = By.xpath("//button[@class='close-button']");

	protected By learnMoreBtn = By.xpath("//div[@aria-hidden='false']//a[text()='Learn More']");
	protected By breadcrumbLink = By.xpath("//ul/li[last()]/a[@data-eventname='breadCrumb']");
	protected By exploringTimeshareHeader = By.xpath("//div[text()='Exploring Timeshare Ownership']");
	protected By cardComponent = By.xpath(
			"//div[contains(.,'Exploring Timeshare')]/div[@class='cardComponent']//div[@class='cell']/div/div[a]");

	protected By contentSliceComponent = By.xpath("//div[div[@class='ctaSlice']][div[@class='imageSlice']]");
	protected By customB2BHeader = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')]");
	protected By exploreNewShapeHeader = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')]");
	protected By customB2BImage = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')]//..//..//..//..//..//div[@class='cardBanner']//img");
	protected By exploreNewShapeImage = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')]//..//..//..//..//..//div[@class='cardBanner']//img");
	protected By customB2BBody = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')]/../div[contains(@class,'body-1')]");
	protected By exploreNewShapeBody = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')]/../div[contains(@class,'body-1')]");

	protected By btnLearnMoreCustomB2B = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')]/../a[contains(.,'Learn More')]");
	protected By btnLearnMoreExploreNewShape = By.xpath(
			"//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')]/../a[contains(.,'Learn More')]");

	protected By latestNewsHeader = By.xpath("//div[@class='contentSlice' and contains(.,'Latest News')]");
	protected By latestNewsComponent = By.xpath(
			"//div[contains(.,'Latest News')]/div[@class='indexCardComponent']//div[@class='cell']/div/div[a and @data-ishidden='false']");
	protected By WyndhamDestinationsFooter = By.xpath("(//a/sub[contains(text(),'Wyndham Destinations')])[1]/..| (//a[@data-eventlabel='Wyndham Destinations'])[1] | (//a[@data-eventlabel='Travel + Leisure Co.'])[1]");
	protected By termsOfUseFooter = By.xpath("(//a/sub[contains(text(),'Terms of Use')])[1]/..| (//a[@data-eventlabel='Terms of Use'])[1]");
	protected By privacyPolicyFooter = By.xpath("(//a/sub[contains(text(),'Privacy Policy')])[1]/..| (//a[@data-eventlabel='Privacy Policy'])[1]");
	protected By pressReleaseFooter = By.xpath("(//a/sub[contains(text(),'Press Releases')])[2]/..| (//a[@data-eventlabel='Press Releases'])[3]");
	protected By footerLogo = By.xpath("//img[contains(@src,'logo-footer')]/../../../../../div[contains(@class,'large')]");
	protected By doNotsellInfo = By.xpath("(//a[contains(text(),'Do Not Sell')])[1]");
	protected By optOutForm = By.xpath("//div[text()='Opt Out Request Form']");

	protected By brandLogoList = By.xpath(
			"//div[contains(@class,'large-2')]//div[@class='contentSlice']//div[contains(@class,'hide-for-medium-only')]//descendant::div[contains(@class,'body-1')]//p/b/a");

	protected By RCIImage = By.xpath("//img[@alt='RCI logo']");
	protected By AcrossImage = By.xpath("//img[@alt='7Across logo']");
	protected By loveHomeSwapLogo = By.xpath("//img[@alt='Love Home Swap logo']");
	protected By extraHolidaysImage = By.xpath("//img[@alt='Extra Holidays logo']");
	protected By registryImage = By.xpath("//img[@alt='The Registry Collection logo']");
	protected By allianceImage = By.xpath("//img[@alt='Alliance Reservations Network logo']");
	protected By allianceBrandDescription = By.xpath(
			"//a[img[@alt='Alliance Reservations Network logo']]/../div[@class='card-section']//div[@class='body-1']");
	protected By registryBrandDescription = By
			.xpath("//a[img[@alt='The Registry Collection logo']]/../div[@class='card-section']//div[@class='body-1']");
	protected By extraHolidaysBrandDescription = By
			.xpath("//a[img[@alt='Extra Holidays logo']]/../div[@class='card-section']//div[@class='body-1']");
	protected By loveHomeSwapBrandDescription = By
			.xpath("//a[img[@alt='Love Home Swap logo']]/../div[@class='card-section']//div[@class='body-1']");
	protected By AcrossBrandDescription = By
			.xpath("//a[img[@alt='7Across logo']]/../div[@class='card-section']//div[@class='body-1']");
	protected By rciBrandDescription = By
			.xpath("//a[img[@alt='RCI logo']]/../div[@class='card-section']//div[@class='body-1']");
	protected By allianceBrandCTA = By.xpath(
			"//a[img[@alt='Alliance Reservations Network logo']]/../div[@class='card-section']/a[div[contains(@class,'body-1-link')]]");
	protected By registryBrandCTA = By.xpath(
			"//a[img[@alt='The Registry Collection logo']]/../div[@class='card-section']/a[div[contains(@class,'body-1-link')]]");
	protected By extraHolidaysBrandCTA = By.xpath(
			"//a[img[@alt='Extra Holidays logo']]/../div[@class='card-section']/a[div[contains(@class,'body-1-link')]]");
	protected By loveHomeSwapBrandCTA = By.xpath(
			"//a[img[@alt='Love Home Swap logo']]/../div[@class='card-section']/a[div[contains(@class,'body-1-link')]]");
	protected By AcrossBrandCTA = By.xpath(
			"//a[img[@alt='7Across logo']]/../div[@class='card-section']/a[div[contains(@class,'body-1-link')]]");
	protected By rciBrandCTA = By
			.xpath("//a[img[@alt='RCI logo']]/../div[@class='card-section']/a[div[contains(@class,'body-1-link')]]");
	protected String url;

	public PanoramacoHomePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method Name:launchApplication Description:To launch the Application URL Date
	 * - September/2020 Author: Priya Das
	 */

	public void launchApplication() {

		url = tcConfig.getTestData().get("URL");
		driver.get(url);
		pageCheck();
		if (verifyObjectDisplayed(cookieAlert)) {
			clickElementBy(cookieAlert);
		}

		tcConfig.updateTestReporter("PanoramacoHomePage", "launchApplication", Status.PASS,
				"Panoramaco URL was launched successfully");
	}

	/*
	 * Method validateLogo Description:To validate Panoramaco logo in Homepage Date
	 * - September/2020 Author: Priya Das
	 */

	public void validateLogo() {
		assertTrue(verifyObjectDisplayed(panoramacoLogo), "Header Panoramaco logo is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateLogo", Status.PASS,
				"Header Panoramaco logo is displayed");
		String href=getElementAttribute(panoramacoLogo, "href");
		clickElementBy(panoramacoLogo);
		pageCheck();

		assertTrue(href.equalsIgnoreCase(driver.getCurrentUrl()),
				"Header Panoramaco logo url not correct");
		driver.navigate().back();
		pageCheck();
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateLogo", Status.PASS,
				"Header Panoramaco logo is present");
	}

	/*
	 * Method validateHomeHeaderMenu Description:To validate headers in the Home
	 * page Date - September/2020 Author: Priya Das
	 */

	public void validateHomeHeaderMenu() {

		assertTrue(verifyObjectDisplayed(bookYourVacation), "Book Your Vacation menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Book Your Vacation menu item is present");
		navigateValidateAndReturn(bookYourVacation);

		assertTrue(verifyObjectDisplayed(careers), "Careers menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Careers menu item is present");
		navigateValidateAndReturn(careers);

		assertTrue(verifyObjectDisplayed(contactUs), "Contact Us menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Contact Us menu item is present");
		navigateValidateAndReturn(contactUs);

		assertTrue(verifyObjectDisplayed(aboutUS), "About US menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"About US menu item is present");
		navigateValidateAndReturn(aboutUS);
	}

	/*
	 * Method validateBanners Description:To validate the Image Banner and the
	 * description of the homepage Date - September/2020 Author: Priya Das
	 */

	public void validateBanners() {

		assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
				"Correct banner is displayed");

		String bannerTitle = getElementText(bannerTitleObj);
		assertTrue(!bannerTitle.trim().equals(""), "Banner title absent");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS, "Banner Title is displayed");
		getElementInView(bannerHeader);
		String bannerHeading = getElementText(bannerHeader).trim();
		assertTrue(bannerHeading.length() > 0, "Banner Header is not present");
		String bannerSubHeading = getElementText(bannerSubDescription).trim();
		assertTrue(bannerSubHeading.length() > 0, "Banner Sub Description is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
				"Banner Header and Sub Description is displayed");

	}

	/*
	 * Method validateVideoPlayer Description:To validate the Video player present
	 * in the homepage Date - September/2020 Author: Priya Das
	 */

	public void validateVideoPlayer() {
		getElementInView(videoBanner);
		assertTrue(getElementAttribute(videoBanner, "src").contains(".jpg"), "Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateVideoPlayer", Status.PASS,
				"Correct banner is displayed");

		assertTrue(verifyObjectDisplayed(videoPlayerButton), "Video Player button is not Displayed");
		clickElementBy(videoPlayerButton);
		pageCheck();
		switchToFrame(iframe);
		String videoHeader = getElementText(videoPlayerHeader).trim();
		assertTrue(videoHeader.length() > 0, "Video Player header is not Displayed");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateVideoPlayer", Status.PASS,
				"Video PLayer header is displayed");
		driver.switchTo().defaultContent();
		clickElementBy(closeButton);

	}

	/*
	 * Method validateCustomB2BTravel Description:To validate Custom B2B card
	 * section in the homepage Date - September/2020 Author: Priya Das
	 */

	public void validateCustomB2BTravel() {

		getElementInView(customB2BHeader);
		assertTrue(verifyObjectDisplayed(customB2BHeader), "Custom B2B Travel Club Solutions header is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel", Status.PASS,
				"Custom B2B Travel Club Solutions header is present");

		assertTrue(verifyObjectDisplayed(customB2BImage), "Custom B2B Travel Club Solutions image is absent");
		assertTrue(getElementText(customB2BHeader).trim().length() > 0,
				"Custom B2B Travel Club Solutions header is blank");
		String title = getElementText(customB2BHeader).trim();

		assertTrue(verifyObjectDisplayed(customB2BBody), "Custom B2B Travel Club Solutions body is absent");
		assertTrue(getElementText(customB2BBody).trim().length() > 0, "Custom B2B Travel Club Solutions body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreCustomB2B),
				"Custom B2B Travel Club Solution Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreCustomB2B, "href");
		String mainWindow = driver.getWindowHandle();
		clickElementJSWithWait(btnLearnMoreCustomB2B);
		waitForSometime("5");
		ArrayList<String> tabList = new ArrayList<String>(driver.getWindowHandles());
		if (tabList.size() == 2) {
			/*
			 * newTabNavigations(tabList); pageCheck();
			 * assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href + "v6"),
			 * "Navigation to learn more link not correct");
			 * tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel",
			 * Status.PASS, "Content Title : " + title + " navigating to link : " + href +
			 * " successful"); navigateToMainTab(tabList);
			 */
			for (String windowHandle : driver.getWindowHandles()) {
				driver.switchTo().window(windowHandle);
				if (driver.getCurrentUrl().trim().equalsIgnoreCase(href + "v6")) {
					tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel", Status.PASS,
							"Content Title : " + title + " navigating to link : " + href + " successful");
					break;
				}
			}
			driver.close();
			driver.switchTo().window(mainWindow);
		} else {
			pageCheck();
			assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href + "/v6"),
					"Navigation to learn more link not correct");

			tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}

	/*
	 * Method validateCExploreNewShape Description:To validate New Explore card
	 * section in the homepage Date - September/2020 Author: Priya Das
	 */

	public void validateCExploreNewShape() {

		getElementInView(exploreNewShapeHeader);
		assertTrue(verifyObjectDisplayed(exploreNewShapeHeader),
				"Explore The New Shape of Travel With RCI header is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateCExploreNewShape", Status.PASS,
				"Explore The New Shape of Travel With RCI header is present");

		assertTrue(verifyObjectDisplayed(exploreNewShapeImage),
				"Explore The New Shape of Travel With RCI image is absent");
		assertTrue(getElementText(exploreNewShapeHeader).trim().length() > 0,
				"Explore The New Shape of Travel With RCI header is blank");
		String title = getElementText(exploreNewShapeHeader).trim();

		assertTrue(verifyObjectDisplayed(exploreNewShapeBody),
				"Explore The New Shape of Travel With RCI body is absent");
		assertTrue(getElementText(exploreNewShapeBody).trim().length() > 0,
				"Explore The New Shape of Travel With RCI body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreExploreNewShape),
				"Explore The New Shape of Travel With RCI Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreExploreNewShape, "href");
		String mainWindow = driver.getWindowHandle();
		clickElementJSWithWait(btnLearnMoreExploreNewShape);
		waitForSometime("5");
		ArrayList<String> tabList = new ArrayList<String>(driver.getWindowHandles());
		if (tabList.size() == 2) {
			/*
			 * newTabNavigations(tabList); pageCheck();
			 * assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
			 * "Navigation to learn more link not correct");
			 * tcConfig.updateTestReporter("PanoramacoHomePage", "validateCExploreNewShape",
			 * Status.PASS, "Content Title : " + title + " navigating to link : " + href +
			 * " successful"); navigateToMainTab(tabList);
			 */
			for (String windowHandle : driver.getWindowHandles()) {
				driver.switchTo().window(windowHandle);
				if (driver.getCurrentUrl().trim().equalsIgnoreCase(href)) {
					tcConfig.updateTestReporter("PanoramacoHomePage", "validateCExploreNewShape", Status.PASS,
							"Content Title : " + title + " navigating to link : " + href + " successful");
					break;
				}
			}
			driver.close();
			driver.switchTo().window(mainWindow);
		} else {
			pageCheck();
			assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
					"Navigation to learn more link not correct");

			tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}

	/*
	 * Method validateLatestNews Description:To validate latest News section in the
	 * homepage Date - September/2020 Author: Priya Das
	 */

	public void validateLatestNews() {

		assertTrue(verifyObjectDisplayed(latestNewsHeader), "Latest News header is absent");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateLatestNews", Status.PASS,
				"Latest News header is present");

		List<WebElement> newsList = getList(latestNewsComponent);

		for (int i = 0; i < newsList.size(); i++) {

			newsList = getList(latestNewsComponent);

			assertTrue(newsList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Latest News cards link not present");
			String hrefCard = newsList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Latest News card section link not present");
			String hrefCardSection = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Latest News card");

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.isDisplayed(), "Latest News card section title not present");
			String cardTitle = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.getText().trim();

			tcConfig.updateTestReporter("PanoramaHomePage", "validateLatestNews", Status.PASS,
					"All links are in sync within the Latest News Card, which is : " + hrefCard);

			WebElement readMoreEle = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Read More']"));
			clickElementBy(readMoreEle);
			pageCheck();
			waitUntilElementVisibleBy(driver, breadcrumbLink, "ExplicitLongWait");
			assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(hrefCard),
					"Navigation to Read More link not correct");
			tcConfig.updateTestReporter("PanoramaHomePage", "validateLatestNews", Status.PASS,
					"Latest News Title : " + cardTitle + " navigating to link : " + hrefCard + " successful");
			navigateBack();
			pageCheck();

		}
	}

	/*
	 * Method validateFooterMenu Description:To validate footer menu option in the
	 * homepage Date - September/2020 Author: Priya Das
	 */

	public void validateFooterMenu() {

		assertTrue(verifyObjectDisplayed(WyndhamDestinationsFooter), "Wyndham Destinations menu item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Wyndham Destinations menu item is present");
		navigateValidateAndReturn(WyndhamDestinationsFooter);

		assertTrue(verifyObjectDisplayed(termsOfUseFooter), "Terms of Use menu item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Terms of Use menu item is present");
		navigateValidateAndReturn(termsOfUseFooter);

		assertTrue(verifyObjectDisplayed(privacyPolicyFooter), "Privacy Policy menu item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Privacy Policy menu item is present");
		navigateValidateAndReturn(privacyPolicyFooter);

		assertTrue(verifyObjectDisplayed(pressReleaseFooter), "Press Release item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Press Release menu item is present");
		navigateValidateAndReturn(pressReleaseFooter);

	}

	/*
	 * Method navigateValidateAndReturn Description:This method will navigate to
	 * link , validate the page link and navigate back to previous page Date -
	 * September/2020 Author: Priya Das
	 */

	public void navigateValidateAndReturn(By link) {
		String title = "";
		String mainWindow = driver.getWindowHandle();
		String href = getElementAttribute(link, "href");
		clickElementJSWithWait(link);
		waitForSometime("5");
		ArrayList<String> tabList = new ArrayList<String>(driver.getWindowHandles());
		if (tabList.size() == 2) {
			/*
			 * newTabNavigations(tabList); pageCheck(); title = driver.getTitle();
			 * assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
			 * "Link Navigation not correct, expected : " + href + " | Actual : " +
			 * driver.getCurrentUrl().trim());
			 * tcConfig.updateTestReporter("PanoramacoHomePage",
			 * "navigateValidateAndReturn", Status.PASS, "Page Title : " + title +
			 * " navigating to link : " + href + " successful"); navigateToMainTab(tabList);
			 * waitForSometime("5"); pageCheck();
			 */
			for (String windowHandle : driver.getWindowHandles()) {
				driver.switchTo().window(windowHandle);
				if (driver.getCurrentUrl().trim().equalsIgnoreCase(href)) {
					tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
							"Page Title : " + title + " navigating to link : " + href + " successful");
					break;
				}
			}
			driver.close();
			driver.switchTo().window(mainWindow);

		} else {
			pageCheck();
			waitForSometime("5");
			title = driver.getTitle();
			assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + driver.getCurrentUrl().trim());
			tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page Title : " + title + " navigating to link : " + href + " successful");

			navigateBack();
			waitForSometime("5");
			pageCheck();
		}

	}

	/*
	 * Method validatefooterlogo Description: To validate the Panoramaco logo in the
	 * footer section Date - September/2020 Author: Priya Das
	 */

	public void validatefooterlogo() {

		getElementInView(footerLogo);
		assertTrue(verifyObjectDisplayed(footerLogo), "Footer Panorama logo is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validatefooterlogo", Status.PASS,
				"Footer Panoramaco logo is present");

	}

	/*
	 * Method validateDoNotSellInfo Description: To validate the Do not Seel Info
	 * link and Form in the footer section Date - September/2020 Author: Priya Das
	 */

	public void validateDoNotSellInfo() {
		getElementInView(doNotsellInfo);
		assertTrue(verifyObjectDisplayed(doNotsellInfo), "Do not sell info link is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
				"Do not sell info link is present");
		clickElementBy(doNotsellInfo);
		waitForSometime("5");
		ArrayList<String> tabList = new ArrayList<String>(driver.getWindowHandles());
		if (tabList.size() == 2) {
			newTabNavigations(tabList);
			pageCheck();
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateToMainTab(tabList);
		} else {
			pageCheck();
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateBack();
			pageCheck();
		}

	}

	/*
	 * Method validateBrandImages Description: To validate the Brand Images in the
	 * footer section Date - September/2020 Author: Priya Das
	 */

	public void validateBrandImages()

	{

		getElementInView(RCIImage);
		assertTrue(getElementAttribute(RCIImage, "src").contains(".png"), "RCI Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"RCI Image source present");
		assertTrue(getElementAttribute(AcrossImage, "src").contains(".png"), "Across Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Across Image source present");
		assertTrue(getElementAttribute(loveHomeSwapLogo, "src").contains(".png"),
				"Love Home Swap Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Love Home Swap Image source present");
		assertTrue(getElementAttribute(extraHolidaysImage, "src").contains(".png"),
				"Extra Holidays Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Extra Holidays Image source present");
		assertTrue(getElementAttribute(registryImage, "src").contains(".png"), "Registry Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Registry Image source present");
		assertTrue(getElementAttribute(allianceImage, "src").contains(".png"), "Alliance Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Alliance Image source present");

	}

	/*
	 * Method validateBrandDescriptionDescription Description: To validate the Brand
	 * Description in the footer section Date - September/2020 Author: Priya Das
	 */

	public void validateBrandDescriptionDescription() {
		assertTrue(getElementText(rciBrandDescription).trim().length() > 0, "RCI Brand Description is Blank");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandDescriptionDescription", Status.PASS,
				"RCI Brand Description is present");
		assertTrue(getElementText(AcrossBrandDescription).trim().length() > 0, "7Across Brand Description is Blank");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandDescriptionDescription", Status.PASS,
				"7 Across Brand Description present");
		assertTrue(getElementText(loveHomeSwapBrandDescription).trim().length() > 0,
				"Love Home Swap Brand Description is Blank");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandDescriptionDescription", Status.PASS,
				"Love Home Swap Brand Description present");
		assertTrue(getElementText(extraHolidaysBrandDescription).trim().length() > 0,
				"Extra Holidays Brand Description is Blank");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandDescriptionDescription", Status.PASS,
				"Extra Holidays Brand Description present");
		assertTrue(getElementText(registryBrandDescription).trim().length() > 0, "Registry Brand Description is Blank");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandDescriptionDescription", Status.PASS,
				"Registry Brand Description present");
		assertTrue(getElementText(allianceBrandDescription).trim().length() > 0, "Alliance Brand Description is Blank");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandDescriptionDescription", Status.PASS,
				"Alliance Brand Description present");

	}

	/*
	 * Method validateRCIBrandCTA Description: To validate the RCI Brand CTA and
	 * respective page link in the footer section Date - September/2020 Author:
	 * Priya Das
	 */

	public void validateRCIBrandCTA() {
		assertTrue(verifyObjectDisplayed(rciBrandCTA), "RCI Brand CTA is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateRCIBrandCTA", Status.PASS, "RCI Brand CTA is present");
		navigateValidateAndReturn(rciBrandCTA);
	}

	/*
	 * Method validate7AcrossBrandCTA Description: To validate the 7Across Brand CTA
	 * and respective page link in the footer section Date - September/2020 Author:
	 * Priya Das
	 */

	public void validate7AcrossBrandCTA() {
		assertTrue(verifyObjectDisplayed(AcrossBrandCTA), "7 Across Brand CTA is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validate7AcrossBrandCTA", Status.PASS,
				"7 Across Brand CTA is present");
		navigateValidateAndReturn(AcrossBrandCTA);
	}

	/*
	 * Method validateLoveHomeSwapBrandCTA Description: To validate the LoveHomeSwap
	 * Brand CTA and respective page link in the footer section Date -
	 * September/2020 Author: Priya Das
	 */

	public void validateLoveHomeSwapBrandCTA() {
		assertTrue(verifyObjectDisplayed(loveHomeSwapBrandCTA), "Love Home Swap Brand CTA is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateLoveHomeSwapBrandCTA", Status.PASS,
				"Love Home Swap Brand CTA is present");
		navigateValidateAndReturn(loveHomeSwapBrandCTA);
	}

	/*
	 * Method validateExtraHolidaysBrandCTA Description: To validate the Extra
	 * Holidays Brand CTA and respective page link in the footer section Date -
	 * September/2020 Author: Priya Das
	 */

	public void validateExtraHolidaysBrandCTA() {
		assertTrue(verifyObjectDisplayed(extraHolidaysBrandCTA), "Extra Holidays Brand CTA is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateExtraHolidaysBrandCTA", Status.PASS,
				"Extra Holidays Brand CTA is present");
		navigateValidateAndReturn(extraHolidaysBrandCTA);
	}

	/*
	 * Method validateResgistryBrandCTA Description: To validate the Registry Brand
	 * CTA and respective page link in the footer section Date - September/2020
	 * Author: Priya Das
	 */

	public void validateResgistryBrandCTA() {
		assertTrue(verifyObjectDisplayed(registryBrandCTA), "Registry Brand CTA is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateResgistryBrandCTA", Status.PASS,
				"Registry Brand CTA is present");
		navigateValidateAndReturn(registryBrandCTA);
	}

	/*
	 * Method validateAllianceBrandCTA Description: To validate the Alliance Brand
	 * CTA and respective page link in the footer section Date - September/2020
	 * Author: Priya Das
	 */

	public void validateAllianceBrandCTA() {
		assertTrue(verifyObjectDisplayed(allianceBrandCTA), "Alliance Brand CTA is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateAllianceBrandCTA", Status.PASS,
				"Alliance Brand CTA is present");
		navigateValidateAndReturn(allianceBrandCTA);
	}

	/*
	 * Method validateFooterBrandlogo Description: To validate the logo present in
	 * the footer and respective page link in the footer section Date -
	 * September/2020 Author: Priya Das
	 */

	public void validateFooterBrandlogo() {
		List<WebElement> brandList = getList(brandLogoList);
		getElementInView(brandLogoList);
		for (int i = 0; i < brandList.size(); i++) {

			assertTrue(verifyObjectDisplayed(
					By.xpath("(//div[@class='contentSlice']//div[contains(@class,'body-1')]//p/b/a[contains(.,'"
							+ testData.get("Brand " + (i + 1)) + "')])[1]")),
					"Brand Title is not present");

			String brandTitle = getElementText(
					By.xpath("(//div[@class='contentSlice']//div[contains(@class,'body-1')]//p/b/a[contains(.,'"
							+ testData.get("Brand " + (i + 1)) + "')])[1]"));
			tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterBrandlogo", Status.PASS,
					"Brand Logo is Present, which is : " + brandTitle);
			navigateValidateAndReturn(
					By.xpath("(//div[@class='contentSlice']//div[contains(@class,'body-1')]//p/b/a[contains(.,'"
							+ testData.get("Brand " + (i + 1)) + "')])[1]"));

		}
	}

	public void validateBrands() {
		// TODO Auto-generated method stub

	}

	public void launchApplicationforBrokenLinkValidation() {
		driver.get(tcConfig.getTestData().get("URL"));

	}

	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("html")) {
					System.out.println("URL contians html");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it."); report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}

}
