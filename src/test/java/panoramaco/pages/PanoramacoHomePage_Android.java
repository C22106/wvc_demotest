package panoramaco.pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PanoramacoHomePage_Android extends PanoramacoHomePage_Web {

	protected By hamburgerMenu = By.xpath("(//div[contains(@id,'nav-icon')]/span)[1]");

	public PanoramacoHomePage_Android(TestConfig tcconfig) {
		super(tcconfig);

		panoramacoLogo = By.xpath("(//a[img[contains(@class,'logo')] and parent::div[contains(@class,'small')]])[2]");
		bookYourVacation = By
				.xpath("//ul[contains(@class,'accordion-menu')]//span[contains(.,'Book Your Vacation')]/..");
		careers = By.xpath("//ul[contains(@class,'accordion-menu')]//span[contains(.,'Careers')]/..");
		contactUs = By.xpath("//ul[contains(@class,'accordion-menu')]//span[contains(.,'Contact Us')]/..");
		aboutUS = By.xpath("//ul[contains(@class,'accordion-menu')]//span[contains(.,'About Us')]/..");
		footerLogo = By.xpath("//img[contains(@src,'panorama-logo-footer')]/../..//div[contains(@class,'medium')]");
		doNotsellInfo = By.xpath("(//a/sub[contains(text(),'Do Not Sell')])[1]");
		WyndhamDestinationsFooter = By.xpath("(//a/sub[contains(text(),'Wyndham Destinations')])[2]/..");
		termsOfUseFooter = By.xpath("(//a/sub[contains(text(),'Terms of Use')])[2]/..");
		privacyPolicyFooter = By.xpath("(//a/sub[contains(text(),'Privacy Policy')])[2]/..");
		pressReleaseFooter = By.xpath("(//a/sub[contains(text(),'Press Releases')])[1]/..");
		bannerTitleObj = By.xpath(
				"//div[@class='image-quote-banner']//div[contains(@class,'quote-content')]//div[contains(@class,'title-1')]");
		customB2BHeader = By.xpath("(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')])[2]");
		exploreNewShapeHeader = By.xpath("(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')])[2]");
		customB2BImage = By.xpath(
				"(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')]//..//..//..//..//..//div[@class='cardBanner']//img)[2]");
		exploreNewShapeImage = By.xpath(
				"(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')]//..//..//..//..//..//div[@class='cardBanner']//img)[2]");
		customB2BBody = By.xpath(
		"(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')]/../div[contains(@class,'body-1')])[2]");
		exploreNewShapeBody = By.xpath(
				"(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')]/../div[contains(@class,'body-1')])[2]");
		btnLearnMoreCustomB2B = By.xpath(
				"(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Custom B2B Travel Club Solutions')]/../a[contains(.,'Learn More')])[2]");
		btnLearnMoreExploreNewShape = By.xpath(
				"(//section[contains(@class,'banner')]//div[@class='title-1' and contains(.,'Explore The New Shape of Travel With RCI')]/../a[contains(.,'Learn More')])[2]");
	}

	@Override
	public void validateLogo() {
		assertTrue(verifyObjectDisplayed(panoramacoLogo), "Header Panoramaco logo is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateLogo", Status.PASS,
				"Header Panoramaco logo is displayed");

		assertTrue(getElementAttribute(panoramacoLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Header Panoramaco logo url not correct");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateLogo", Status.PASS,
				"Header Panoramaco logo is present");
	}

	private void clickHamburgerMenu() {
		clickElementBy(hamburgerMenu);
		assertTrue(verifyObjectDisplayed(bookYourVacation), "Hamburger Menu not clicked successfully");

	}

	@Override
	public void validateHomeHeaderMenu() {

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(bookYourVacation), "Book Your Vacation menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Book Your Vacation menu item is present");
		navigateValidateAndReturn(bookYourVacation);

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(careers), "Careers menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Careers menu item is present");
		navigateValidateAndReturn(careers);

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(contactUs), "Contact Us menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Contact Us menu item is present");
		navigateValidateAndReturn(contactUs);

		clickHamburgerMenu();
		assertTrue(verifyObjectDisplayed(aboutUS), "About US menu item is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateHomeHeaderMenu", Status.PASS,
				"About US menu item is present");
		navigateValidateAndReturn(aboutUS);
	}

	/*
	 * Method validatefooterlogo Description: To validate the Panoramaco logo in the
	 * footer section Date - September/2020 Author: Priya Das
	 */
	@Override
	public void validatefooterlogo() {

		scrollDownForElementJSBy(footerLogo);
		assertTrue(verifyObjectDisplayed(footerLogo), "Footer Panorama logo is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validatefooterlogo", Status.PASS,
				"Footer Panoramaco logo is present");

	}

	@Override
	public void validateDoNotSellInfo() {
		scrollDownForElementJSBy(doNotsellInfo);
		assertTrue(verifyObjectDisplayed(doNotsellInfo), "Do not sell info link is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
				"Do not sell info link is present");
		clickElementBy(doNotsellInfo);
		waitForSometime("5");

		assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateDoNotSellInfo", Status.PASS,
				"Opt out form is displayed");
		navigateToURL(url);
		pageCheck();
	}

	/*
	 * Method validateFooterMenu Description:To validate footer menu option in the
	 * homepage Date - September/2020 Author: Priya Das
	 */

	@Override
	public void validateFooterMenu() {

		scrollDownForElementJSBy(WyndhamDestinationsFooter);
		assertTrue(verifyObjectDisplayed(WyndhamDestinationsFooter), "Wyndham Destinations menu item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Wyndham Destinations menu item is present");
		navigateValidateAndReturn(WyndhamDestinationsFooter);

		assertTrue(verifyObjectDisplayed(termsOfUseFooter), "Terms of Use menu item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Terms of Use menu item is present");
		navigateValidateAndReturn(termsOfUseFooter);

		assertTrue(verifyObjectDisplayed(privacyPolicyFooter), "Privacy Policy menu item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Privacy Policy menu item is present");
		navigateValidateAndReturn(privacyPolicyFooter);

		assertTrue(verifyObjectDisplayed(pressReleaseFooter), "Press Release item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Press Release menu item is present");
		navigateValidateAndReturn(pressReleaseFooter);

	}

	/*
	 * Method navigateValidateAndReturn Description:This method will navigate to
	 * link , validate the page link and navigate back to previous page Date -
	 * September/2020 Author: Priya Das
	 */

	@Override
	public void navigateValidateAndReturn(By link) {
		String title = "";
		String href = getElementAttribute(link, "href");
		clickElementJSWithWait(link);
		waitForSometime("5");
		title = driver.getTitle();
		assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
				"Link Navigation not correct, expected : " + href + " | Actual : " + driver.getCurrentUrl().trim());
		tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
				"Page Title : " + title + " navigating to link : " + href + " successful");

		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	/*
	 * Method validateLatestNews Description:To validate latest News section in the
	 * homepage Date - September/2020 Author: Priya Das
	 */

	@Override
	public void validateLatestNews() {

		assertTrue(verifyObjectDisplayed(latestNewsHeader), "Latest News header is absent");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateLatestNews", Status.PASS,
				"Latest News header is present");

		List<WebElement> newsList = getList(latestNewsComponent);

		for (int i = 0; i < newsList.size(); i++) {
			getElementInView(latestNewsComponent);
			newsList = getList(latestNewsComponent);

			assertTrue(newsList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Latest News cards link not present");
			String hrefCard = newsList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Latest News card section link not present");
			String hrefCardSection = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Latest News card");

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.isDisplayed(), "Latest News card section title not present");
			String cardTitle = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.getText().trim();

			tcConfig.updateTestReporter("PanoramaHomePage", "validateLatestNews", Status.PASS,
					"All links are in sync within the Latest News Card, which is : " + hrefCard);

			WebElement eleReadMore = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Read More']"));
			clickElementBy(eleReadMore);
			pageCheck();
			assertTrue(getElementAttribute(breadcrumbLink, "href").trim().equalsIgnoreCase(hrefCard),
					"Navigation to Read More link not correct");
			tcConfig.updateTestReporter("PanoramaHomePage", "validateLatestNews", Status.PASS,
					"Latest News Title : " + cardTitle + " navigating to link : " + hrefCard + " successful");
			navigateBack();
			pageCheck();

		}
	}

	/*
	 * Method validateCustomB2BTravel Description:To validate Custom B2B card
	 * section in the homepage Date - September/2020 Author: Priya Das
	 */

	@Override
	public void validateCustomB2BTravel() {

		getElementInView(customB2BHeader);
		assertTrue(verifyObjectDisplayed(customB2BHeader), "Custom B2B Travel Club Solutions header is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel", Status.PASS,
				"Custom B2B Travel Club Solutions header is present");

		assertTrue(verifyObjectDisplayed(customB2BImage), "Custom B2B Travel Club Solutions image is absent");
		assertTrue(getElementText(customB2BHeader).trim().length() > 0,
				"Custom B2B Travel Club Solutions header is blank");
		String title = getElementText(customB2BHeader).trim();

		assertTrue(verifyObjectDisplayed(customB2BBody), "Custom B2B Travel Club Solutions body is absent");
		assertTrue(getElementText(customB2BBody).trim().length() > 0, "Custom B2B Travel Club Solutions body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreCustomB2B),
				"Custom B2B Travel Club Solution Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreCustomB2B, "href");
		clickElementJSWithWait(btnLearnMoreCustomB2B);
		waitForSometime("5");
		pageCheck();
		assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href + "v6"),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	/*
	 * Method validateCExploreNewShape Description:To validate New Explore card
	 * section in the homepage Date - September/2020 Author: Priya Das
	 */

	@Override

	public void validateCExploreNewShape() {

		getElementInView(exploreNewShapeHeader);
		assertTrue(verifyObjectDisplayed(exploreNewShapeHeader),
				"Explore The New Shape of Travel With RCI header is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateCExploreNewShape", Status.PASS,
				"Explore The New Shape of Travel With RCI header is present");

		assertTrue(verifyObjectDisplayed(exploreNewShapeImage),
				"Explore The New Shape of Travel With RCI image is absent");
		assertTrue(getElementText(exploreNewShapeHeader).trim().length() > 0,
				"Explore The New Shape of Travel With RCI header is blank");
		String title = getElementText(exploreNewShapeHeader).trim();

		assertTrue(verifyObjectDisplayed(exploreNewShapeBody),
				"Explore The New Shape of Travel With RCI body is absent");
		assertTrue(getElementText(exploreNewShapeBody).trim().length() > 0,
				"Explore The New Shape of Travel With RCI body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreExploreNewShape),
				"Explore The New Shape of Travel With RCI Learn more button is absent");

		String href = getElementAttribute(btnLearnMoreExploreNewShape, "href");
		clickElementJSWithWait(btnLearnMoreExploreNewShape);
		waitForSometime("5");

		pageCheck();
		assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href), "Navigation to learn more link not correct");

		tcConfig.updateTestReporter("PanoramacoHomePage", "validateCustomB2BTravel", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	/*
	 * Method validateBanners Description:To validate the Image Banner and the
	 * description of the homepage Date - September/2020 Author: Priya Das
	 */

	@Override
	public void validateBanners() {

		assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
				"Correct banner is displayed");

		String bannerTitle = getElementText(bannerTitleObj);
		assertTrue(!bannerTitle.trim().equals(""), "Banner title absent");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS, "Banner Title is displayed");
		getElementInView(bannerHeader);
		String bannerHeading = getElementText(bannerHeader).trim();
		assertTrue(bannerHeading.length() > 0, "Banner Header is not present");
		String bannerSubHeading = getElementText(bannerSubDescription).trim();
		assertTrue(bannerSubHeading.length() > 0, "Banner Sub Description is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
				"Banner Header and Sub Description is displayed");

	}

	/*
	 * Method validateVideoPlayer Description:To validate the Video player present
	 * in the homepage Date - September/2020 Author: Priya Das
	 */

	@Override
	public void validateVideoPlayer() {
		getElementInView(videoBanner);
		assertTrue(getElementAttribute(videoBanner, "src").contains(".jpg"), "Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateVideoPlayer", Status.PASS,
				"Correct banner is displayed");

		assertTrue(verifyObjectDisplayed(videoPlayerButton), "Video Player button is not Displayed");
		clickElementBy(videoPlayerButton);
		pageCheck();
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateVideoPlayer", Status.PASS,
				"Video PLayer header is displayed");
		clickElementBy(closeButton);

	}

	/*
	 * Method validateBrandImages Description: To validate the Brand Images in the
	 * footer section Date - September/2020 Author: Priya Das
	 */

	public void validateBrands()

	{

		getElementInView(RCIImage);
		assertTrue(getElementAttribute(RCIImage, "src").contains(".png")
				&& getElementText(rciBrandDescription).trim().length() > 0, "RCI Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"RCI Image source and Description present");
		validateRCIBrandCTA();

		getElementInView(AcrossImage);
		assertTrue(
				getElementAttribute(AcrossImage, "src").contains(".png")
						&& getElementText(AcrossBrandDescription).trim().length() > 0,
				"Across Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Across Image source and Description present");
		validate7AcrossBrandCTA();

		getElementInView(loveHomeSwapLogo);
		assertTrue(
				getElementAttribute(loveHomeSwapLogo, "src").contains(".png")
						&& getElementText(loveHomeSwapBrandDescription).trim().length() > 0,
				"Love Home Swap Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Love Home Swap Image source and Description present");
		validateLoveHomeSwapBrandCTA();

		getElementInView(extraHolidaysImage);
		assertTrue(
				getElementAttribute(extraHolidaysImage, "src").contains(".png")
						&& getElementText(extraHolidaysBrandDescription).trim().length() > 0,
				"Extra Holidays Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Extra Holidays Image source and Description present");
		validateExtraHolidaysBrandCTA();

		getElementInView(registryImage);
		assertTrue(
				getElementAttribute(registryImage, "src").contains(".png")
						&& getElementText(registryBrandDescription).trim().length() > 0,
				"Registry Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Registry Image source and Description present");
		validateResgistryBrandCTA();

		getElementInView(allianceImage);
		assertTrue(
				getElementAttribute(allianceImage, "src").contains(".png")
						&& getElementText(allianceBrandDescription).trim().length() > 0,
				"Alliance Image source not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateBrandImages", Status.PASS,
				"Alliance Image source and Description present");
		validateAllianceBrandCTA();

	}

	/*
	 * Method validateFooterBrandlogo Description: To validate the logo present in
	 * the footer and respective page link in the footer section Date -
	 * September/2020 Author: Priya Das
	 */

	@Override
	public void validateFooterBrandlogo() {
		List<WebElement> brandList = getList(brandLogoList);
		getElementInView(brandLogoList);
		for (int i = 0; i < brandList.size(); i++) {

			assertTrue(verifyObjectDisplayed(
					By.xpath("(//div[@class='contentSlice']//div[contains(@class,'body-1')]//p/b/a[contains(.,'"
							+ testData.get("Brand " + (i + 1)) + "')])[1]")),
					"Brand Title is not present");

			String brandTitle = getElementText(
					By.xpath("(//div[@class='contentSlice']//div[contains(@class,'body-1')]//p/b/a[contains(.,'"
							+ testData.get("Brand " + (i + 1)) + "')])[1]"));
			tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterBrandlogo", Status.PASS,
					"Brand Logo is Present, which is : " + brandTitle);
			navigateValidateAndReturn(
					By.xpath("(//div[@class='contentSlice']//div[contains(@class,'body-1')]//p/b/a[contains(.,'"
							+ testData.get("Brand " + (i + 1)) + "')])[1]"));

		}
	}

}
