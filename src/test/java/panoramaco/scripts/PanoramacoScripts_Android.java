package panoramaco.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import panoramaco.pages.PanoramacoHomePage;
import panoramaco.pages.PanoramacoHomePage_Android;

public class PanoramacoScripts_Android extends TestBase {
	public static final Logger log = Logger.getLogger(PanoramacoScripts_Android.class);
	public String environment;

	/*
	 * TC Name: tc01_Panoramaco_Header_Validation Description: Verify the Header
	 * navigation in Panoramaco home page Date - September/2020 Author: Priya
	 * Das
	 */

	@Test(dataProvider = "testData", groups = { "header", "regression", "android", "panoramaco" })

	public void tc01_Panoramaco_Header_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			PanoramacoHomePage homePage = new PanoramacoHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateLogo();
			homePage.validateHomeHeaderMenu();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * TC Name: tc02_Panoramaco_Validate_Footer Description: Verify the Footer
	 * link navigation in Panoramaco home page Date - September/2020 Author:
	 * Priya Das
	 */

	@Test(dataProvider = "testData", groups = { "footer", "", "android", "panoramaco" })

	public void tc02_Panoramaco_Validate_Footer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			PanoramacoHomePage homePage = new PanoramacoHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validatefooterlogo();
			homePage.validateDoNotSellInfo();
			homePage.validateFooterMenu();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * TC Name: tc03_Panoramaco_Latest_News Description: Verify the latest news
	 * section in Panoramaco home page Date - September/2020 Author: Priya Das
	 */

	@Test(dataProvider = "testData", groups = { "latestNews", "", "android", "panoramaco" })

	public void tc03_Panoramaco_Latest_News(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			PanoramacoHomePage homePage = new PanoramacoHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateLatestNews();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * TC Name: tc04_Panoramaco_Custom_B2B_Travel_Club_Solution Description:
	 * Verify the custom B2B section in Panoramaco home page Date -
	 * September/2020 Author: Priya Das
	 */

	@Test(dataProvider = "testData", groups = { "learnmoreCTA", "regression", "android", "panoramaco" })

	public void tc04_Panoramaco_Custom_B2B_Travel_Club_Solution(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			PanoramacoHomePage homePage = new PanoramacoHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateCustomB2BTravel();
			homePage.validateCExploreNewShape();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			cleanUp();
		}

	}

	/*
	 * TC Name: tc05_Panoramaco_VideoBanner_Validation Description: Verify the
	 * VIDEO BANNER in Panoramaco home page Date - September/2020 Author: Priya
	 * Das
	 */

	@Test(dataProvider = "testData", groups = { "videobanner", "regression", "android", "panoramaco" })

	public void tc05_Panoramaco_VideoBanner_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			PanoramacoHomePage homePage = new PanoramacoHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateBanners();
			homePage.validateVideoPlayer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * TC Name: tc06_Panoramaco_Brand_Name_Validation Description: Verify the
	 * Brand Name, Description and Links in Panoramaco home page Date -
	 * September/2020 Author: Priya Das
	 */

	@Test(dataProvider = "testData", groups = { "brand", "regression", "android", "panoramaco" })

	public void tc06_Panoramaco_Brand_Name_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			PanoramacoHomePage homePage = new PanoramacoHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateBrands();
			homePage.validateFooterBrandlogo();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}