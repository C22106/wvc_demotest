package acs.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;



import acs.pages.ACSLoginPage;
import acs.pages.ApplicantSelectionPage;
import acs.pages.MemberNumberEntry;
import acs.pages.RetrieveCustPage;
import acs.pages.ScoreCustomerPage;
import acs.pages.SelectLocationSoftscore;
import acs.pages.SelectRolesPages;
import automation.core.TestBase;

public class ACSTestScripts extends TestBase {

	public static final Logger log = Logger.getLogger(ACSTestScripts.class);

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC01_WVR_Telesales_SaleSite_ApplyForLOC_ApplyBarclay: ACS Tele sales
	 * Search Site Apply for LOC Apply Barclays
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wvr", "batch1" })
	public void ACS_TC01_WVR_Telesales_SaleSite_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
		SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
		MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
		ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);
		try {
			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			selectRole.enitySelection();
			applicanSelection.clubCreditPrescreenRequest();
			selectRole.navigateToHome();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC02_WVR_NONTelesales_SaleSite_ApplyForLOC_ApplyBarclay: ACS Non Tele
	 * sales Search Site Apply for LOC Apply Barclays
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wvr", "batch1" })
	public void ACS_TC02_WVR_NONTelesales_SaleSite_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			selectRole.enitySelection();
			applicanSelection.clubCreditPrescreenRequest();
			selectRole.navigateToHome();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			//applicanSelection.fillUpWRCForm();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC03_WBW_Telesales_SaleSite_ApplyForLOC_ApplyBarclay: ACS Tele sales
	 * Search Site Apply for LOC Apply Barclays
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wbw", "batch1" })
	public void ACS_TC03_WBW_Telesales_SaleSite_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			selectRole.enitySelection();
			applicanSelection.clubCreditPrescreenRequest();
			selectRole.navigateToHome();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			//applicanSelection.fillUpWRCForm();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC04_WBW_NonTelesales_SaleSite_ApplyForLOC_ApplyBarclay: ACS Non Tele
	 * sales Search Site Apply for LOC Apply Barclays
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wbw", "batch1" })
	public void ACS_TC04_WBW_NonTelesales_SaleSite_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			selectRole.enitySelection();
			applicanSelection.clubCreditPrescreenRequest();
			selectRole.navigateToHome();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			//applicanSelection.fillUpWRCForm();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC05_WVR_Telesales_SearchVCC_ApplyForLOC_ApplyBarclay: ACS Tele sales
	 * Search VCC Apply for LOC Apply Barclays
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wvr", "batch2" })
	public void ACS_TC05_WVR_Telesales_SearchVCC_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			applicanSelection.searchVCC();
			applicanSelection.verifySearchResultVCC();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			applicanSelection.fillUpWRCForm();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC06_WVR_NonTelesales_SearchVCC_ApplyForLOC_ApplyBarclay: ACS Non
	 * Tele sales Search VCC Apply for LOC Apply Barclays
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wvr",  "batch2" })
	public void ACS_TC06_WVR_NonTelesales_SearchVCC_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			applicanSelection.searchVCC();
			applicanSelection.verifySearchResultVCC();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			applicanSelection.fillUpWRCForm();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC07_WBW_Telesales_SearchVCC_ApplyForLOC_ApplyBarclay: ACS Tele sales
	 * Search VCC Apply for LOC Apply Barclays
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wbw", "batch2" })
	public void ACS_TC07_WBW_Telesales_SearchVCC_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			Thread.sleep(30000);
			applicanSelection.searchVCC();
			applicanSelection.verifySearchResultVCC();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			applicanSelection.fillUpWRCForm();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Kamalesh Function/event:
	 * ACS_TC08_WBW_NonTelesales_SearchVCC_ApplyForLOC_ApplyBarclay: ACS Non
	 * Tele sales Search VCC Apply for LOC Apply Barclays
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wbw", "batch2" })
	public void ACS_TC08_WBW_NonTelesales_SearchVCC_ApplyForLOC_ApplyBarclay(Map<String, String> testData)
			throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole_VCCPrescreen();
			selectRole.vacationClubCreditPrescreen_Selection();
			applicanSelection.searchVCC();
			applicanSelection.verifySearchResultVCC();
			selectRole.SelectRole_LineofCredit();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomerApplyWVC();
			applicanSelection.fillUpWRCForm();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/** OLD Scripts Developed by UTSAV (Based on Previous Requirements) **/

	/*
	 * Name:Utsav Function/event: tc1_ACS_WBW_Hardscoring Description: ACS
	 * Application HardScoring
	 * 
	 */

	@Test(dataProvider = "testData")
	public void tc1_ACS_WBW_Hardscoring(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			RetrieveCustPage retrieveCust = new RetrieveCustPage(tcconfig);
			ScoreCustomerPage scoreCust = new ScoreCustomerPage(tcconfig);

			acsLogin.launchApplication();
			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			retrieveCust.retrieveCustomer();
			scoreCust.updateVerifyCustInfoForm();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}
	
	
	/*
	 * Name:Utsav Function/event: tc2_ACS_WVR_Hardscoring 
	 * Description: ACS
	 * Application HardScoring
	 * 
	 */

	@Test(dataProvider = "testData")
	public void tc2_ACS_WVR_Hardscoring(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			RetrieveCustPage retrieveCust = new RetrieveCustPage(tcconfig);
			ScoreCustomerPage scoreCust = new ScoreCustomerPage(tcconfig);

			acsLogin.launchApplication();
			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			retrieveCust.retrieveCustomer();
			scoreCust.updateVerifyCustInfoForm();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Utsav Function/event: tc2_VCCfor_any_Credit_Band Description: ACS
	 * Application VCC for any credit band.
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "acs", "vcc" , "oldBatch1"})
	public void tc2_VCCfor_any_Credit_Band(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomer();
			applicanSelection.verifyCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Utsav Function/event: tc3_ACS_Search_WBW_tourID Description: ACS
	 * Application tour id retrive customer
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "acs", "wbw", "oldBatch1"})
	public void tc3_ACS_Search_WBW_tourID(Map<String, String> testData) throws Exception {
		try {
			// cleanUp();
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			RetrieveCustPage retrieveCust = new RetrieveCustPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			retrieveCust.retrieveCustomer();
			acsLogin.ACSLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Utsav Function/event: tc4_ACS_Search_WVR_tourID Description: ACS
	 * Application search for a tour id
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "acs", "wvr", "oldBatch1" })
	public void tc4_ACS_Search_WVR_tourID(Map<String, String> testData) throws Exception {

		try {

			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			RetrieveCustPage retrieveCust = new RetrieveCustPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			retrieveCust.retrieveCustomer();
			acsLogin.ACSLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name:Utsav Function/event: tc5_SearchLOCApplication Description: ACS
	 * Application Loc Search form
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "acs", "loc" , "oldBatch2"})
	public void tc5_SearchLOCApplication(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			selectRole.searchLOC();
			memEntry.searchfilledupLOCSearchForm();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name:Utsav Function/event: tc6_SoftScoreWVRTourID Description: ACS
	 * Application Soft Score WVR TourID
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wvr" , "oldBatch2"})
	public void tc6_SoftScoreWVRTourID(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			RetrieveCustPage retrieveCust = new RetrieveCustPage(tcconfig);
			ScoreCustomerPage scoreCust = new ScoreCustomerPage(tcconfig);
			SelectLocationSoftscore locationSoftScore = new SelectLocationSoftscore(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			retrieveCust.retrieveCustomer();
			scoreCust.updateVerifyCustInfoForm();

			selectRole.SelectRoleForSoftScore();
			locationSoftScore.SelectLocationDetails();
			locationSoftScore.validateSoftscore();

			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Name:Utsav Function/event: tc7_SoftScoreWVRTourIDWithoutHardscore
	 * Description: ACS Application Softscore without hard score
	 * 
	 */
	@Test(dataProvider = "testData")
	public void tc7_SoftScoreWVRTourIDWithoutHardscore(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			SelectLocationSoftscore locationSoftScore = new SelectLocationSoftscore(tcconfig);
			/*RetrieveCustPage retrieveCust = new RetrieveCustPage(tcconfig);
			ScoreCustomerPage scoreCust = new ScoreCustomerPage(tcconfig);*/
			
			acsLogin.launchApplication();
			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			/*retrieveCust.retrieveCustomer();
			scoreCust.updateVerifyCustInfoForm();*/
			selectRole.SelectRoleForSoftScore();
			locationSoftScore.SelectLocationDetails();
			locationSoftScore.validateSoftscore();

			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	
	/* * Name:Utsav Function/event: tc7_SoftScoreWVRTourIDWithoutHardscore
	 * Description: ACS Application Softscore without hard score
	 * 
	 */
	@Test(dataProvider = "testData")
	public void tc8_SoftScoreWBWTourIDWithoutHardscore(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			SelectLocationSoftscore locationSoftScore = new SelectLocationSoftscore(tcconfig);
						
			acsLogin.launchApplication();
			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			selectRole.SelectRoleForSoftScore();
			locationSoftScore.SelectLocationDetails();
			locationSoftScore.validateSoftscore();

			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}
	/*
	 * Name:Utsav Function/event: tc8_VCCfor_any_Credit_Band_WVR Description:
	 * ACS Application WVR VCC for Any Credit Band
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "wvr", "vcc" , "oldBatch2"})
	public void tc8_VCCfor_any_Credit_Band_WVR(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectCustomer();
			applicanSelection.verifyCreditApproval();
			applicanSelection.verifyVCCCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name:Utsav Function/event: tc9_Barclays_Application_Submit Description:
	 * ACS Application Barclays Submit
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "acs", "loc", "barclays" , "oldBatch2"})
	public void tc9_Barclays_Application_Submit(Map<String, String> testData) throws Exception {
		try {
			setupTestData(testData);
			ACSLoginPage acsLogin = new ACSLoginPage(tcconfig);
			SelectRolesPages selectRole = new SelectRolesPages(tcconfig);
			MemberNumberEntry memEntry = new MemberNumberEntry(tcconfig);
			ApplicantSelectionPage applicanSelection = new ApplicantSelectionPage(tcconfig);

			acsLogin.launchApplication();

			acsLogin.loginToACSApp();
			selectRole.SelectRole();
			selectRole.applyLOC();
			memEntry.fillupMemberFormdetails();
			applicanSelection.selectBarclaysCustomer();
			// applicanSelection.selectCustomer();
			applicanSelection.verifyBarclaysCreditApproval();
			acsLogin.ACSLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

}
