package acs.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class ACSBasePage extends FunctionalComponents {

	public ACSBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}
	
	/*
	 * Function to wait until the specified element is visible
	 * 
	 * @param by The {@link WebDriver} locator used to identify the element
	 * 
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementVisible(WebDriver driver, WebElement locator, String timeOutInSeconds) {
		try {
			String strValue = "";
			strValue = tcConfig.getConfig().get(timeOutInSeconds);
			Long timeOut = Long.parseLong(strValue);
			(new WebDriverWait(driver, timeOut)).until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception e) {
			log.info("Loading Element: " + locator);
		}

	}
	
	/**
	 * Check If any extra tabs Present
	 * 
	 * @param applicationURl
	 */
	public void navigateToAppURL(String applicationURl) {
		driver.navigate().to(applicationURl);
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info("No Alert Present");
		}
	}

	
	/*
	 * Function to wait until the specified element is visible
	 * 
	 * @param by The {@link WebDriver} locator used to identify the element
	 * 
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementVisible(WebDriver driver, By by, String timeOutInSeconds) {
		try {
			String strValue = "";
			strValue = tcConfig.getConfig().get(timeOutInSeconds);
			Long timeOut = Long.parseLong(strValue);

			(new WebDriverWait(driver, timeOut)).until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			log.info("Loading Element: " + by);
		}

	}
	
	/*
	 * *************************Method: mouseoverAndClick ***
	 * *************Description: mouse hover And Click**
	 */
	public void mouseoverAndClick(By by) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", getObject(by));
			new Actions(driver).moveToElement(getObject(by)).click().perform();
		} catch (Exception e) {
			log.info("unable to hover and click " + "element due to " + e.getMessage());
		}

	}


}
