package acs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SelectLocationSoftscore extends ACSBasePage {

	public static final Logger log = Logger.getLogger(SelectRolesPages.class);

	public SelectLocationSoftscore(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By stateDropDwn1 = By.xpath("//select[@name='siteState']");
	protected By cityDrpdown = By.xpath("//select[@name='siteCity']");
	protected By locationDrpdown = By.xpath("//select[@name='locationName']");
	protected By tourSiteDropDown = By.xpath("//select[@name='tourSite']");
	@FindBy(id = "next-btn")
	protected WebElement NextBtn;
	@FindBy(xpath = "//button[@name='score-btn']")
	protected WebElement newApplicationButton;
	@FindBy(xpath = "//input[@id='firstname']")
	protected WebElement txtFirstname;
	@FindBy(xpath = "//input[@id='lastname']")
	protected WebElement txtlastname;
	@FindBy(xpath = "//input[@id='addressline1']")
	protected WebElement txtAddress1;
	@FindBy(xpath = "//input[@id='city']")
	protected WebElement txtCity;
	@FindBy(xpath = "//input[@id='zipcode']")
	protected WebElement txtPostalCode;
	@FindBy(xpath = "//input[@id='homenumber1']")
	protected WebElement txtHome1;
	@FindBy(xpath = "//input[@id='homenumber2']")
	protected WebElement txtHome2;
	@FindBy(xpath = "//input[@id='homenumber3']")
	protected WebElement txtHome3;
	@FindBy(xpath = "//input[@id='ssn1']")
	protected WebElement txtSSN1;
	@FindBy(xpath = "//input[@id='ssn2']")
	protected WebElement txtSSN2;
	@FindBy(xpath = "//input[@id='ssn3']")
	protected WebElement txtSSN3;
	@FindBy(xpath = "//input[@id='emailaddress']")
	protected WebElement txtEmail;
	protected By stateDropDwn = By.xpath("//select[@name='state']");
	protected By monthDropDwn = By.xpath("//select[@name='dob_month']");
	protected By dateDropDwn = By.xpath("//select[@name='dob_day']");
	protected By yearDropDwn = By.xpath("//select[@name='dob_year']");
	protected By softScoreStatus = By.xpath("//form[@id='formCustomerDetail']//h1");
	protected By btnAddTourId= By.xpath("//button[contains(text(),'Add CRS ID') or contains(text(),'Add EPNY ID')]");
	protected By txtTourID=By.xpath("//input[@name='tour_id']");
	protected By btnPreview= By.xpath("//button[contains(text(),'Preview')]");
	protected By btnSave= By.xpath("//button[@name='tour-save-btn']");
	protected By btnDetails=By.xpath("//button[@name='results-btn']");
	/*
	 * Method: SelectLocationDetails Description: Select Location Detail Date:
	 * 2020 Author: Utsav Changes By: NA
	 */
	public void SelectLocationDetails() throws Exception {

		waitUntilElementVisible(driver, NextBtn, "ExplicitWaitLongerTime");

		String Location = testData.get("Location");
		String State = testData.get("State2");
		String City = testData.get("City2");
		String Site = testData.get("Site2");
		new Select(driver.findElement(stateDropDwn1)).selectByVisibleText(State);
		new Select(driver.findElement(cityDrpdown)).selectByVisibleText(City);
		new Select(driver.findElement(locationDrpdown)).selectByVisibleText(Location);
		new Select(driver.findElement(tourSiteDropDown)).selectByVisibleText(Site);
		mouseoverAndClick(NextBtn);
		// NextBtn.click();
		waitUntilElementVisible(driver, newApplicationButton, "ExplicitWaitLongerTime");
		if (verifyElementDisplayed(newApplicationButton)) {
			tcConfig.updateTestReporter("selectLocationSoftscore", "SelectLocationDetails", Status.PASS,
					"Navigated to Customer Look up page");
		} else {
			tcConfig.updateTestReporter("selectLocationSoftscore", "SelectLocationDetails", Status.FAIL,
					"Unable to navigate to Customer Look up page");
		}

	}

	/*
	 * Method: validateSoftscore Description: Validate Soft Score Date: 2020
	 * Author: Utsav Changes By: NA
	 */
	public void validateSoftscore() throws Exception {
		newApplicationButton.click();

		waitUntilElementVisible(driver, NextBtn, "ExplicitWaitLongerTime");

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("selectLocationSoftscore", "validateSoftscore", Status.PASS,
				"New application button clicked");

		String ssn = testData.get("SSN");
		String dob = testData.get("DOB");
		String State = testData.get("State");
		String ssn1 = ssn.substring(0, 3);
		String ssn2 = ssn.substring(3, 5);
		String ssn3 = ssn.substring(5, 9);

		String dobmonth = "", dobDay = "";
		String arr[] = dob.split("/");
		// MonthFormation
		if (arr[0].length() == 1) {
			dobmonth = "0" + arr[0];
		} else {
			dobmonth = arr[0];
		}
		// DayFormation
		if (arr[1].length() == 1) {
			dobDay = "0" + arr[1];
		} else {
			dobDay = arr[1];
		}

		txtSSN1.sendKeys(ssn1);
		txtSSN2.sendKeys(ssn2);
		txtSSN3.sendKeys(ssn3);
		txtFirstname.clear();
		txtFirstname.sendKeys(testData.get("First"));
		txtlastname.clear();
		txtlastname.sendKeys(testData.get("Last"));

		new Select(driver.findElement(monthDropDwn)).selectByValue(dobmonth);
		new Select(driver.findElement(dateDropDwn)).selectByValue(dobDay);
		new Select(driver.findElement(yearDropDwn)).selectByValue(arr[2]);

		new Select(driver.findElement(stateDropDwn)).selectByValue(State);

		txtAddress1.sendKeys(testData.get("Street#") + " " + testData.get("StreetName"));
		txtCity.sendKeys(testData.get("City"));
		txtPostalCode.sendKeys(testData.get("Zip"));

		txtHome1.sendKeys(testData.get("HomePhoneNumber").substring(0, 3));
		txtHome2.sendKeys(testData.get("HomePhoneNumber").substring(3, 6));
		txtHome3.sendKeys(testData.get("HomePhoneNumber").substring(6, 10));
		txtEmail.sendKeys(testData.get("email"));

		NextBtn.click();

		waitUntilElementVisible(driver, softScoreStatus, "ExplicitWaitLongerTime");

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(softScoreStatus)) {
			if (driver.findElement(softScoreStatus).getText().equalsIgnoreCase("Q")
					|| driver.findElement(softScoreStatus).getText().equalsIgnoreCase("NQ") ||driver.findElement(softScoreStatus).getText().equalsIgnoreCase("PC1")) {
				tcConfig.updateTestReporter("selectLocationSoftscore", "validateSoftscore", Status.PASS,
						"WVR tour id Soft scored : Status :" + driver.findElement(softScoreStatus).getText());
			} else {
				tcConfig.updateTestReporter("selectLocationSoftscore", "validateSoftscore", Status.FAIL,
						"WVR tour id  has not been Soft scored");
			}
		}
		clickElementBy(btnAddTourId);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(txtTourID, testData.get("TourID"));
		clickElementBy(btnPreview);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(btnSave);	
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(btnDetails);	
		waitForSometime(tcConfig.getConfig().get("MedWait"));		
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"WVR tour id: "+testData.get("TourID")+" has been Soft scored");
	}

}
