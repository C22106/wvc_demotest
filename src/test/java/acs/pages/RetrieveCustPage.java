package acs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class RetrieveCustPage extends ACSBasePage {

	public static final Logger log = Logger.getLogger(RetrieveCustPage.class);

	public RetrieveCustPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(id = "getcustomer-btn")
	protected WebElement RetrieveCustBtn;
	protected By retrieveByDropdown = By.xpath("//select[@id='retrieveBy']");
	@FindBy(xpath = "//input[@name='id']")
	protected WebElement retieveByValueTxt;
	@FindBy(xpath = "//div[@id='credit_band']")
	protected WebElement txtCreditBand;

	/*
	 * Method: retrieveCustomer Description: Retrieve Customer Details Date:
	 * 2020 Author: Utsav Changes By: NA
	 */
	public void retrieveCustomer() throws Exception {
		String roleOption = testData.get("RetrieveBy");
		String TourID = testData.get("TourID");
		new Select(driver.findElement(retrieveByDropdown)).selectByVisibleText(roleOption);
		waitUntilElementVisible(driver, retieveByValueTxt, "ExplicitWaitLongerTime");
		retieveByValueTxt.sendKeys(TourID);
		RetrieveCustBtn.click();
		// waitUntilElementVisible(driver, txtCreditBand,
		// "ExplicitWaitLongerTime");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilElementVisible(driver, txtCreditBand, "ExplicitLowWait");
		if (txtCreditBand.getText().contains("Credit Band")) {
			tcConfig.updateTestReporter("retrieveCustPage", "retrieveCustomer", Status.PASS,
					"Navigated to Verify & Score Customer page");
		}

	}

}
