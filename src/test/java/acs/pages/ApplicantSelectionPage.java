package acs.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ApplicantSelectionPage extends ACSBasePage {

	public static final Logger log = Logger.getLogger(RetrieveCustPage.class);
	public boolean existingMemberFlag;

	public ApplicantSelectionPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(id = "VCCABtn")
	protected WebElement applyVCCBtn;
	@FindBy(id = "WRCCBtn")
	protected WebElement applyWRCBtn;
	@FindBy(xpath = "//form[@id = 'appStatus']")
	protected WebElement vccApplyResult;
	@FindBy(xpath = "//button[@id = 'close']")
	protected WebElement vccClose;
	@FindBy(id = "vccaEntry-telephone")
	protected WebElement vccaEntrytelephonetxt;
	@FindBy(id = "vccaEntry-dob")
	protected WebElement vccaEntrydobtxt;
	@FindBy(id = "vccaEntry-income")
	protected WebElement vccaEntryIncometxt;
	@FindBy(id = "continueBtn")
	protected WebElement continueBtn;
	@FindBy(id = "vccaEntry-ssn")
	protected WebElement ssnTxtfield;
	@FindBy(xpath = "//input[@id='vccaApproval-limit']")
	protected WebElement approvalLimit;
	@FindBy(id = "vccaEntry-email")
	protected WebElement txtEmail;
	@FindBy(id = "vccaReEntry-ssn")
	protected WebElement ssnTxtfieldreEntry;
	@FindBy(id = "vccaEntry-city")
	protected WebElement txtvccCity;
	@FindBy(id = "vccaEntry-zip")
	protected WebElement txtvccZip;
	@FindBy(xpath = "//button[@form='vccaApproval']")
	protected WebElement btnGetStatusUpdate;
	@FindBy(xpath = "//button[@id='cardDetails']")
	protected WebElement btnGetStatusUpdatewrv;
	@FindBy(id = "wrvEntry-email")
	protected WebElement txtWRVEmail;
	protected By addressLine1 = By.xpath("//input[contains(@name,'addressLine1')]");
	@FindBy(id = "wrvEntry-city")
	protected WebElement txtwrvCity;
	@FindBy(id = "wrvEntry-postal")
	protected WebElement txtwrvZip;
	@FindBy(id = "wrvEntry-dob")
	protected WebElement wrvEntrydobtxt;
	@FindBy(xpath = "//input[@id='wrvApproval-limit']")
	protected WebElement wrvApprovalLimit;
	@FindBy(xpath = "//div/h1[contains(text(),'Decline')]")
	protected WebElement declinedTxt;
	protected By errorResponse = By.xpath("//div[@class='msg error']");
	protected By tourNumber_VCCSearch = By.xpath("//input[@id='appSearch-tourNumber']");
	protected By entity_VCCSearch = By.xpath("//select[@id='appSearch-entity']");
	protected By site_VCCSearch = By.xpath("//select[@id='appSearch-site']");
	protected By listOfApplicant = By.xpath("//table[@id='applicants-selection']/tbody");
	protected By nonscoredApplicant = By.xpath("//input[@id='nonScoredApplicant']");
	protected By nonscoredApplicantRadioBtn = By.xpath("//input[@id='nonScoredApplicant-radio']");
	protected By btnApplyWRCC = By.xpath("//button[@id='WRCCBtn']");
	protected By closeBtn = By.xpath("//button[@id='closeBtn']");
	protected By state = By.xpath("//select[@id='wrvEntry-state']");
	protected By txtwrvMaiden = By.xpath("//input[@id='wrvEntry-maiden']");
	protected By txtwrvincome = By.xpath("//input[@id='wrvEntry-income']");
	protected By listACSid = By.xpath("//table[@id='application-search-results']/tbody/tr/td/input");
	protected By acsID = By.id("appSearch-appID");

	protected By vccRequest_FirstName = By.xpath("//input[@id='vccaEntry-firstName']");
	protected By vccRequest_LastName = By.xpath("//input[@id='vccaEntry-lastName']");
	protected By vccRequest_MiddleName = By.xpath("//input[@id='vccaEntry-initial']");
	protected By vccRequest_StAddress = By.xpath("//label[text()='Street Address']/../input");
	protected By vccRequest_StType = By.xpath("//label[text()='Unit Type & Number']/../input");
	protected By vccRequest_City = By.xpath("//input[@id='vccaEntry-city']");
	protected By vccRequest_Zip = By.xpath("//input[@id='vccaEntry-zip']");
	protected By vccRequest_Income = By.xpath("//input[@id='vccaEntry-income']");
	protected By vccRequest_StateDrpdwn = By.xpath("//select[@id='vccaEntry-state']");
	protected By pageHeader = By.xpath("//*[@id='page-body-content-1col']/h1");
	protected By homeIcon = By.xpath("//*[@id='bc-home']");
	protected By applyforVCCAccountBtn = By.xpath("//button[@id='applyForVCCBtn']");
	protected By ssn = By.xpath("//input[@id='vccaEntry-ssn']");
	protected By ressn = By.xpath("//input[@id='vccaReEntry-ssn']");
	protected By telephone = By.xpath("//input[@id='vccaReEntry-telephone']");

	/*
	 * Method: selectCustomer Description:Select Customer Date: 2020 Author:
	 * Utsav Changes By: NA
	 */
	public void selectCustomer() throws Exception {

		waitUntilElementVisible(driver, closeBtn, "ExplicitWaitLongerTime");

		if (testData.get("NewApplicant").equalsIgnoreCase("NO")) {
			List<WebElement> applicantList = driver.findElements(listOfApplicant);
			int rcnt = applicantList.size();

			for (int iterator = 1; iterator <= rcnt; iterator++) {
				String memberName = driver
						.findElement(
								By.xpath("(//table[@id='applicants-selection']/tbody[" + iterator + "]//td[2])[1]"))
						.getText();
				if (memberName.contains(testData.get("First"))) {
					driver.findElement(By.xpath("(//table[@id='applicants-selection']/tbody[" + iterator
							+ "]//td[1]/input[@name='applicants-radio'])[1]")).click();
				}
			}
		} else if (testData.get("NewApplicant").equalsIgnoreCase("YES")) {
			String strFirstName = testData.get("NewFirst");
			String strLastName = testData.get("NewLast");

			mouseoverAndClick(nonscoredApplicant);

			driver.findElement(nonscoredApplicant).sendKeys(strFirstName + " " + strLastName);
			driver.findElement(nonscoredApplicantRadioBtn).click();
		}

	}

	/*
	 * Method: selectBarclaysCustomer Description:Select barclays Customer Date:
	 * 2020 Author: Utsav Changes By: NA
	 */
	public void selectBarclaysCustomer() throws Exception {

		waitUntilElementVisible(driver, closeBtn, "ExplicitWaitLongerTime");

		if (testData.get("NewApplicant").equalsIgnoreCase("YES")) {

			mouseoverAndClick(nonscoredApplicant);
			driver.findElement(nonscoredApplicant).sendKeys(testData.get("NewFirst") + " " + testData.get("NewLast"));
			driver.findElement(nonscoredApplicantRadioBtn).click();
			driver.findElement(btnApplyWRCC).click();

			waitUntilElementVisible(driver, continueBtn, "ExplicitWaitLongerTime");

			By married = By.xpath("//select[@id='wrvEntry-married']");

			driver.findElement(addressLine1).clear();
			driver.findElement(addressLine1).sendKeys(testData.get("Street#") + testData.get("StreetName"));
			new Select(driver.findElement(state)).selectByValue(testData.get("State"));
			txtwrvCity.sendKeys(testData.get("City"));
			txtwrvZip.sendKeys(testData.get("Zip"));
			new Select(driver.findElement(married)).selectByValue("Yes");
			By txtTelephone = By.xpath("//input[@id='wrvEntry-telephone']");
			driver.findElement(txtTelephone).clear();
			driver.findElement(txtTelephone).sendKeys(testData.get("HomePhoneNumber"));
			txtWRVEmail.clear();
			txtWRVEmail.sendKeys(testData.get("email"));
			String dob = testData.get("DOB");
			String dobmonth = "", dobDay = "";
			String arr[] = dob.split("/");
			// MonthFormation
			if (arr[0].length() == 1) {
				dobmonth = "0" + arr[0];
			} else {
				dobmonth = arr[0];
			}
			// DayFormation
			if (arr[1].length() == 1) {
				dobDay = "0" + arr[1];
			} else {
				dobDay = arr[1];
			}

			String actDOB = dobmonth + dobDay + arr[2];
			wrvEntrydobtxt.clear();
			wrvEntrydobtxt.sendKeys(actDOB);
			By txtwrvSSN = By.xpath("//input[@id='wrvEntry-ssn']");
			driver.findElement(txtwrvSSN).clear();
			driver.findElement(txtwrvSSN).sendKeys(testData.get("SSN"));

			driver.findElement(txtwrvMaiden).clear();
			driver.findElement(txtwrvMaiden).sendKeys(testData.get("Mother Maiden Name"));
			driver.findElement(txtwrvincome).clear();
			driver.findElement(txtwrvincome).sendKeys(testData.get("Income"));
			new Select(driver.findElement(By.xpath("//select[@id='wrvEntry-occupation']")))
					.selectByValue(testData.get("Profession"));
			new Select(driver.findElement(By.xpath("//select[@id='wrvEntry-checking']")))
					.selectByValue(testData.get("Checking"));
			new Select(driver.findElement(By.xpath("//select[@id='wrvEntry-saving']")))
					.selectByValue(testData.get("Saving"));
			By txtwrvEmployer = By.xpath("//input[@id='wrvEntry-employer']");
			driver.findElement(txtwrvEmployer).clear();
			driver.findElement(txtwrvEmployer).sendKeys(testData.get("Employer"));
			By btnSubmit = By.xpath("//button[@id='continueBtn']");

			driver.findElement(btnSubmit).click();
		} else {
			By txtwrvEmployer = By.xpath("//input[@id='wrvEntry-employer']");
		}

		// }else if(existingMemberFlag==true) {
		// By btnResubmitWRV= By.xpath ("//button[@id='resubmitWRVBtn']");
		// waitUntilObjectVisible(driver, btnResubmitWRV, 120);
		//
		// if(verifyObjectDisplayed(btnResubmitWRV)) {
		// driver.findElement(btnResubmitWRV).click();
		// tcConfig.updateTestReporter(getClassName(),
		// "selectBarclaysCustomer", Status.PASS,
		// "re submit barclays form button is displayed");
		// }else {
		// tcConfig.updateTestReporter(getClassName(),
		// "selectBarclaysCustomer", Status.FAIL,
		// "re submit barclays form button is NOT displayed");
		// }
		//
		// By btnSubmit=By.xpath("//button[@id='continueBtn']");
		// waitUntilObjectVisible(driver, btnSubmit, 120);
		// if(verifyObjectDisplayed(btnSubmit)) {
		// driver.findElement(btnSubmit).click();
		// }
		// }
	}

	/*
	 * Method: verifyCreditApproval Description:Verify Credit Approval Date:
	 * 2020 Author: Utsav Changes By: NA
	 */
	public void verifyCreditApproval() throws Exception {
		String dob = testData.get("DOB");

		String dobmonth = "", dobDay = "";
		String arr[] = dob.split("/");
		// MonthFormation
		if (arr[0].length() == 1) {
			dobmonth = "0" + arr[0];
		} else {
			dobmonth = arr[0];
		}
		// DayFormation
		if (arr[1].length() == 1) {
			dobDay = "0" + arr[1];
		} else {
			dobDay = arr[1];
		}

		String actDOB = dobmonth + dobDay + arr[2];

		waitUntilElementVisible(driver, applyVCCBtn, "ExplicitWaitLongerTime");

		if (verifyElementDisplayed(applyVCCBtn)) {
			tcConfig.updateTestReporter(getClassName(), "verifyCreditApproval", Status.PASS,
					"Apply for Vacation Club Credit button is displayed");
		} else {
			tcConfig.updateTestReporter(getClassName(), "verifyCreditApproval", Status.FAIL,
					"Apply for Vacation Club Credit button is NOT displayed");
		}

		applyVCCBtn.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			waitUntilElementVisible(driver, continueBtn, "ExplicitWaitLongerTime");

			if (verifyElementDisplayed(continueBtn)) {
				if (testData.get("NewApplicant").equalsIgnoreCase("YES")) {
					driver.findElement(By.xpath("//input[contains(@name,'addressLine1')]"))
							.sendKeys(testData.get("Street#") + testData.get("StreetName"));
					txtvccCity.sendKeys(testData.get("City"));
					txtvccZip.sendKeys(testData.get("Zip"));
					new Select(driver.findElement(By.xpath("//select[@id='vccaEntry-state']")))
							.selectByValue(testData.get("State"));
				} else {
					driver.findElement(addressLine1).clear();
					driver.findElement(addressLine1).sendKeys(testData.get("Street#") + testData.get("StreetName"));
					txtvccCity.clear();
					txtvccCity.sendKeys(testData.get("City"));
					txtvccZip.clear();
					txtvccZip.sendKeys(testData.get("Zip"));
					new Select(driver.findElement(By.xpath("//select[@id='vccaEntry-state']")))
							.selectByValue(testData.get("State"));

				}
				ssnTxtfield.clear();
				ssnTxtfield.sendKeys(testData.get("SSN"));
				ssnTxtfieldreEntry.clear();
				ssnTxtfieldreEntry.sendKeys(testData.get("SSN"));
				txtEmail.clear();
				txtEmail.sendKeys(testData.get("email"));
				vccaEntrydobtxt.clear();
				vccaEntrydobtxt.sendKeys(actDOB);
				vccaEntrytelephonetxt.clear();
				vccaEntrytelephonetxt.sendKeys(testData.get("HomePhoneNumber"));
				vccaEntryIncometxt.clear();
				vccaEntryIncometxt.sendKeys(testData.get("Income"));

				continueBtn.click();
			}

		} catch (Exception e) {
			if (verifyElementDisplayed(declinedTxt)) {
				tcConfig.updateTestReporter(getClassName(), "verifyCreditApproval", Status.FAIL,
						"Credit card NOT approved");
			}
		}

	}

	/*
	 * Method: verifyVCCCreditApproval Description:Verify VCC Credit Approval
	 * Date: 2020 Author: Utsav Changes By: NA
	 */
	public void verifyVCCCreditApproval() {
		WebElement btnCancel = driver.findElement(By.xpath("//button[@id='CancelBtn']"));
		waitUntilElementVisible(driver, btnCancel, "ExplicitWaitLongerTime");

		String errCode = driver.findElement(errorResponse).getText();

		if ((errCode.equals("")) || (errCode == null)) {

			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.PASS,
					"Response is received successfully");

		} else {

			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.FAIL,
					"Error Response is received ");

		}

		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// waitUntilElementVisible(driver, approvalLimit, 120);
		// if (verifyElementDisplayed(approvalLimit)) {
		// tcConfig.updateTestReporter(getClassName(),
		// "verifyCreditApproval", Status.PASS,
		// "Credit card approved with credit limit : " +
		// approvalLimit.getAttribute("value"));
		// } else if(verifyElementDisplayed(btnGetStatusUpdate)) {
		// tcConfig.updateTestReporter(getClassName(),
		// "verifyCreditApproval", Status.PASS,
		// "Credit card approval Pending");
		// } else if (verifyElementDisplayed(declinedTxt)) {
		// tcConfig.updateTestReporter(getClassName(),
		// "verifyCreditApproval", Status.PASS,
		// "Credit Declined : Change Test data");
		// }
		// else {
		// tcConfig.updateTestReporter(getClassName(),
		// "verifyCreditApproval", Status.FAIL,
		// "Credit card NOT approved");
		// }
	}

	/*
	 * Method: verifyBarclaysCreditApproval Description:Verify Barclays Credit
	 * Approval Date: 2020 Author: Utsav Changes By: NA
	 */
	public void verifyBarclaysCreditApproval() {
		WebElement btnCancel = driver.findElement(By.xpath("//button[@id='CancelBtn']"));
		waitUntilElementVisible(driver, btnCancel, "ExplicitWaitLongerTime");

		String errCode = driver.findElement(errorResponse).getText();

		if ((errCode.equals("")) || (errCode == null)) {

			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.PASS,
					"Response is received successfully");

		} else {

			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.FAIL,
					"Error Response is received ");

		}

		if (verifyElementDisplayed(wrvApprovalLimit)) {
			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.PASS,
					"Credit card approved with credit limit : " + wrvApprovalLimit.getAttribute("value"));
		} else if (verifyElementDisplayed(btnGetStatusUpdatewrv)) {
			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.INFO,
					"Credit card approval Pending");
		} else if (verifyElementDisplayed(declinedTxt)) {
			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.INFO,
					"Credit Declined : Change Test data");
		} else {
			tcConfig.updateTestReporter(getClassName(), "verifyBarclaysCreditApproval", Status.INFO,
					"Credit card NOT approved");
		}
	}

	/*
	 * Method: clubCreditPrescreenRequest Description:Verify Club Credit Request
	 * Date: 2020 Author: Utsav Changes By: NA
	 */
	public void clubCreditPrescreenRequest() {
		waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");
		if (driver.findElement(pageHeader).getText()
				.equalsIgnoreCase("Process WVR Vacation Club Credit Prescreen Request")||driver.findElement(pageHeader).getText().equalsIgnoreCase("Process WBW Vacation Club Credit Prescreen Request") ) {
			tcConfig.updateTestReporter(getClassName(), "clubCreditPrescreenequest", Status.PASS,
					"Navigated to Process Vacation Club Credit Prescreen Request Page");

			driver.findElement(vccRequest_FirstName).clear();
			driver.findElement(vccRequest_FirstName).sendKeys(testData.get("NewFirst"));
			driver.findElement(vccRequest_MiddleName).clear();
			driver.findElement(vccRequest_LastName).clear();
			driver.findElement(vccRequest_LastName).sendKeys(testData.get("NewLast"));
			driver.findElement(vccRequest_StAddress).clear();
			driver.findElement(vccRequest_StAddress)
					.sendKeys(testData.get("Street#") + " " + testData.get("StreetName"));
			driver.findElement(vccRequest_City).clear();
			driver.findElement(vccRequest_City).sendKeys(testData.get("City"));
			driver.findElement(vccRequest_Zip).clear();
			driver.findElement(vccRequest_Zip).sendKeys(testData.get("Zip"));
			driver.findElement(vccRequest_Income).clear();
			driver.findElement(vccRequest_Income).sendKeys(testData.get("Income2"));
			//new Select(driver.findElement(vccRequest_StateDrpdwn)).selectByVisibleText(testData.get("State"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			continueBtn.click();
			waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");
			if (driver.findElement(pageHeader).getText()
					.equalsIgnoreCase("Vacation Club Credit Prescreen Submitted Response")) {
				tcConfig.updateTestReporter(getClassName(), "clubCreditPrescreenequest", Status.PASS,
						"Navigated to Vacation Club Credit Prescreen Submitted Response Page");
				driver.findElement(applyforVCCAccountBtn).click();
				waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");
				if (driver.findElement(pageHeader).getText()
						.equalsIgnoreCase("Vacation Club Credit Application Entry")) {
					tcConfig.updateTestReporter(getClassName(), "clubCreditPrescreenequest", Status.PASS,
							"Navigated to Vacation Club Credit Application Entry Page");
				} else {
					tcConfig.updateTestReporter(getClassName(), "clubCreditPrescreenequest", Status.FAIL,
							"Unable to navigate to Vacation Club Credit Application Entry page");
				}

				driver.findElement(ssn).sendKeys(testData.get("SSN"));
				driver.findElement(ressn).sendKeys(testData.get("SSN"));
				driver.findElement(ressn).sendKeys(testData.get("HomePhoneNumber"));
				/*
				 * String dob = testData.get("DOB"); String dobmonth = "",
				 * dobDay = ""; String arr[] = dob.split("/"); if
				 * (arr[0].length() == 1) { dobmonth = "0" + arr[0]; } else {
				 * dobmonth = arr[0]; } if (arr[1].length() == 1) { dobDay = "0"
				 * + arr[1]; } else { dobDay = arr[1]; }
				 * 
				 * String actDOB = dobmonth + dobDay + arr[2];
				 */
				ssnTxtfield.clear();
				ssnTxtfield.sendKeys(testData.get("SSN"));
				vccaEntrytelephonetxt.clear();
				vccaEntrytelephonetxt.sendKeys(testData.get("HomePhoneNumber"));
				vccaEntrydobtxt.clear();
				vccaEntrydobtxt.sendKeys(testData.get("DOB"));
				driver.findElement(ssn).sendKeys(testData.get("SSN"));
				driver.findElement(ressn).sendKeys(testData.get("SSN"));
				vccaEntryIncometxt.sendKeys(testData.get("Income2"));
				continueBtn.click();
				waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");
			} else {
				tcConfig.updateTestReporter(getClassName(), "clubCreditPrescreenequest", Status.FAIL,
						"Unable to navigate to Vacation Club Credit Prescreen Submitted Response");
			}

		} else if (driver.findElement(pageHeader).getText()
				.equalsIgnoreCase("Vacation Club Credit New Prescreen Offer")) {
			tcConfig.updateTestReporter(getClassName(), "clubCreditPrescreenequest", Status.PASS,
					"Navigated to Vacation Club Credit Page");

			driver.findElement(homeIcon).click();
			tcConfig.updateTestReporter(getClassName(), "clubCreditPrescreenequest", Status.PASS,
					"Navigated to Home Page");
		}

	}

	/*
	 * Method: selectCustomerApplyWVC Description:Select Customer Apply VCC
	 * Date: 2020 Author: Utsav Changes By: NA
	 */
	public void selectCustomerApplyWVC() throws Exception {

		String strFirstName = testData.get("NewFirst");
		String strLastName = testData.get("NewLast");
		List<WebElement> applicantList = driver.findElements(By.xpath("//table[@id='applicants-selection']/tbody"));
		int flagIND = 0;

		int rcnt = applicantList.size();

		if (rcnt > 0) {

			for (int iterator = 1; iterator < rcnt + 1; iterator++) {
				String memberName = driver
						.findElement(
								By.xpath("(//table[@id='applicants-selection']/tbody[" + iterator + "]//td[2])[1]"))
						.getText();
				if (memberName.contains(strFirstName)) {
					flagIND = 1;
					driver.findElement(By.xpath("(//table[@id='applicants-selection']/tbody[" + iterator
							+ "]//td[1]/input[@name='applicants-radio'])[1]")).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					break;

				}
			}

		}

		else if (rcnt == 0) {
			WebElement nonScoreApplicant = driver.findElement(By.xpath("//input[@id='nonScoredApplicant']"));
			mouseoverAndClick(nonScoreApplicant);
			driver.findElement(By.xpath("//input[@id='nonScoredApplicant']"))
					.sendKeys(strFirstName + " " + strLastName);
			driver.findElement(By.xpath("//input[@id='nonScoredApplicant-radio']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			applyWRCExisting();

			fillUpWRCForm();
		}

		if (flagIND == 0) {
			WebElement nonScoreApplicant = driver.findElement(By.xpath("//input[@id='nonScoredApplicant']"));
			mouseoverAndClick(nonScoreApplicant);
			driver.findElement(By.xpath("//input[@id='nonScoredApplicant']"))
					.sendKeys(strFirstName + " " + strLastName);
			driver.findElement(By.xpath("//input[@id='nonScoredApplicant-radio']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			applyWRCExisting();

			fillUpWRCForm();

		}

		if (flagIND > 0) {

			applyWRCExisting();

		}

	}

	/*
	 * Method: applyWRCExisting Description:Apply WRC Existing Date: 2020
	 * Author: Utsav Changes By: NA
	 */
	public void applyWRCExisting() throws Exception {

		if (verifyElementDisplayed(applyWRCBtn)) {
			tcConfig.updateTestReporter(getClassName(), "verifyCreditApproval", Status.PASS,
					"Apply for Wyndham Reward Card button is displayed");
		} else {
			tcConfig.updateTestReporter(getClassName(), "verifyCreditApproval", Status.FAIL,
					"Apply for Wyndham Reward Card button is NOT displayed");
		}

		applyWRCBtn.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: searchVCC Description:Search VCC Date: 2020 Author: Utsav Changes
	 * By: NA
	 */
	public void searchVCC() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(tourNumber_VCCSearch).sendKeys(testData.get("TourID"));
		new Select(driver.findElement(entity_VCCSearch)).selectByVisibleText(testData.get("Entity"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(site_VCCSearch)).selectByVisibleText(testData.get("Location"));
		continueBtn.click();

		tcConfig.updateTestReporter(getClassName(), "searchVCC", Status.PASS,
				"Navigated to VCC Prescreen Search Results Page");

	}

	/*
	 * Method: searcverifySearchResultVCC Description:Verify Search result VCC
	 * Date: 2020 Author: Utsav Changes By: NA
	 */
	public void verifySearchResultVCC() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(vccApplyResult)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(vccClose);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> acsIds = driver.findElements(listACSid);
			if (acsIds.size() > 0 || driver.findElement(acsID).isDisplayed()) {
				tcConfig.updateTestReporter(getClassName(), "verifySearchResultVCC", Status.PASS,
						"VCC Search Result is displayed");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(homeIcon).click();
				tcConfig.updateTestReporter(getClassName(), "verifySearchResultVCC", Status.PASS,
						"Navigated to Home Page");
			} else {
				tcConfig.updateTestReporter(getClassName(), "verifySearchResultVCC", Status.FAIL,
						"VCC Search Result is not displayed");
			}
		}else{
			List<WebElement> acsIds = driver.findElements(listACSid);
			if (acsIds.size() > 0 || driver.findElement(acsID).isDisplayed()) {
				tcConfig.updateTestReporter(getClassName(), "verifySearchResultVCC", Status.PASS,
						"VCC Search Result is displayed");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(homeIcon).click();
				tcConfig.updateTestReporter(getClassName(), "verifySearchResultVCC", Status.PASS,
						"Navigated to Home Page");
			} else {
				tcConfig.updateTestReporter(getClassName(), "verifySearchResultVCC", Status.FAIL,
						"VCC Search Result is not displayed");
			}
		}
		
	}

	/*
	 * Method: fillUpWRCForm Description:fill WRC form Date: 2020 Author: Utsav
	 * Changes By: NA
	 */
	public void fillUpWRCForm() throws Exception {

		waitUntilElementVisible(driver, By.xpath("//button[@id='continueBtn']"), "ExplicitWaitLongerTime");
		By txtTelephone = By.xpath("//input[@id='wrvEntry-telephone']");
		driver.findElement(txtTelephone).clear();
		driver.findElement(txtTelephone).sendKeys(testData.get("WRCPhoneNumber"));
		txtWRVEmail.clear();
		txtWRVEmail.sendKeys(testData.get("email"));
		By txtwrvSSN = By.xpath("//input[@id='wrvEntry-ssn']");
		driver.findElement(txtwrvSSN).clear();
		driver.findElement(txtwrvSSN).sendKeys(testData.get("WRC_SSN"));
		By txtwrvMaiden = By.xpath("//input[@id='wrvEntry-maiden']");
		By txtwrvincome = By.xpath("//input[@id='wrvEntry-income']");
		driver.findElement(txtwrvMaiden).clear();
		driver.findElement(txtwrvMaiden).sendKeys(testData.get("Mother Maiden Name"));
		driver.findElement(txtwrvincome).clear();
		driver.findElement(txtwrvincome).sendKeys(testData.get("Income"));
		By txtwrvEmployer = By.xpath("//select[@id='wrvEntry-employer']");
		new Select(driver.findElement(txtwrvEmployer)).selectByValue(testData.get("Employer"));
		new Select(driver.findElement(By.xpath("//select[@id='wrvEntry-occupation']")))
				.selectByValue(testData.get("Profession"));
		new Select(driver.findElement(By.xpath("//select[@id='wrvEntry-checking']")))
				.selectByValue(testData.get("Checking"));
		new Select(driver.findElement(By.xpath("//select[@id='wrvEntry-saving']")))
				.selectByValue(testData.get("Saving"));
		/*driver.findElement(By.id("wrvEntry-rewardsID")).clear();
		driver.findElement(By.id("wrvEntry-rewardsID")).sendKeys(testData.get("WyndhamRewardsMemberID"));*/
		By btnSubmit = By.xpath("//button[@id='continueBtn']");
		driver.findElement(btnSubmit).click();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
}
