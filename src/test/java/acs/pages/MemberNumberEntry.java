package acs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class MemberNumberEntry extends ACSBasePage {

	public static final Logger log = Logger.getLogger(RetrieveCustPage.class);

	public MemberNumberEntry(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//input[@name='tourNumber']")
	protected WebElement editTourNumber;
	@FindBy(id = "continueBtn")
	protected WebElement continueBtn;
	protected By entityDropDwn = By.xpath("//select[@id='selectEntity']");
	protected By locDropDwn = By.xpath("//select[@id='location']");
	protected By buttonLabel = By.xpath("(//div[@class='buttons'])[3]");
	@FindBy(id = "appSearch-firstName")
	protected WebElement appfirstName;
	@FindBy(id = "appSearch-lastName")
	protected WebElement appLastName;
	protected By typeDropDwn = By.xpath("//select[@id='appSearch-ppcOrWrv']");
	protected By applicantSelection = By.xpath("(//input[@name='applicants-radio'])[1]");
	protected By applyforWyndhamRewardsCC_Btn = By.xpath("//button[@id='WRCCBtn']");
	@FindBy(xpath = "//table[@id='application-status-history']")
	protected WebElement appHistoryTable;
	protected By pageHeader = By.xpath("//*[@id='page-body-content-1col']/h1");

	/*
	 * Method: fillupMemberFormdetails_ApplicantSelection Description: Fill up
	 * Member Form Details Date: 2020 Author: Utsav Changes By: NA
	 */
	public void fillupMemberFormdetails_ApplicantSelection() throws Exception {
		new Select(driver.findElement(entityDropDwn)).selectByVisibleText(testData.get("Entity"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		new Select(driver.findElement(locDropDwn)).selectByVisibleText(testData.get("Location"));
		editTourNumber.sendKeys(testData.get("TourID"));
		continueBtn.click();
		waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");

		if (driver.findElement(pageHeader).getText().equalsIgnoreCase("Applicant Selection")) {
			tcConfig.updateTestReporter("memberNumberEntry", "fillupMemberFormdetails", Status.PASS,
					"Navigated to Applicant Selection page");
		} else {
			tcConfig.updateTestReporter("memberNumberEntry", "fillupMemberFormdetails", Status.FAIL,
					"Unable to navigate to Applicant Selection page");
		}

		driver.findElement(applicantSelection).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("selectRolesPages", "enitySelection", Status.PASS,
				"Applicant Selected successfully");

		driver.findElement(applyforWyndhamRewardsCC_Btn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(pageHeader).getText()
				.equalsIgnoreCase("Wyndham Rewards Visa Credit Application Entry")) {
			tcConfig.updateTestReporter("memberNumberEntry", "fillupMemberFormdetails", Status.PASS,
					"Navigated to Wyndham Rewards Visa Credit Application Entry page");
		} else {
			tcConfig.updateTestReporter("memberNumberEntry", "fillupMemberFormdetails", Status.FAIL,
					"Unable to navigate to Wyndham Rewards Visa Credit Application Entry page");
		}
	}

	/*
	 * Method: fillupMemberFormdetails_ApplicantSelection Description: Fill up
	 * Member Form Details Date: 2020 Author: Utsav Changes By: NA
	 */
	public void fillupMemberFormdetails() throws Exception {
		new Select(driver.findElement(entityDropDwn)).selectByVisibleText(testData.get("Entity"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		new Select(driver.findElement(locDropDwn)).selectByVisibleText(testData.get("Location"));
		editTourNumber.sendKeys(testData.get("TourID"));
		continueBtn.click();
		waitUntilElementVisible(driver, buttonLabel, "ExplicitWaitLongerTime");

		if (verifyObjectDisplayed(buttonLabel)) {
			tcConfig.updateTestReporter("memberNumberEntry", "fillupMemberFormdetails", Status.PASS,
					"Navigated to Applicant Selection page");
		} else {
			tcConfig.updateTestReporter("memberNumberEntry", "fillupMemberFormdetails", Status.FAIL,
					"Unable to navigate to Applicant Selection page");
		}

	}

	/*
	 * Method: searchfilledupLOCSearchForm Description: Search filled up LOC
	 * Search form Date: 2020 Author: Utsav Changes By: NA
	 */
	public void searchfilledupLOCSearchForm() throws Exception {
		waitUntilElementVisible(driver, appfirstName, "ExplicitWaitLongerTime");
		appfirstName.sendKeys(testData.get("First"));
		appLastName.sendKeys(testData.get("Last"));
		new Select(driver.findElement(typeDropDwn)).selectByVisibleText("VCC");
		continueBtn.click();

		waitUntilElementVisible(driver, appHistoryTable, "ExplicitLowWait");

		if (verifyElementDisplayed(appHistoryTable)) {
			tcConfig.updateTestReporter("memberNumberEntry", "searchfilledupLOCSearchForm", Status.PASS,
					"Search result is displayed in Table");
		} else {
			tcConfig.updateTestReporter("memberNumberEntry", "searchfilledupLOCSearchForm", Status.FAIL,
					"Search result is NOT displayed in Table");
		}

	}

}
