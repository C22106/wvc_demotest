package acs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ScoreCustomerPage extends ACSBasePage {

	public static final Logger log = Logger.getLogger(RetrieveCustPage.class);

	public ScoreCustomerPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(name = "ssn1")
	protected WebElement txtSSN1;
	@FindBy(name = "ssn2")
	protected WebElement txtSSN2;
	@FindBy(name = "ssn3")
	protected WebElement txtSSN3;
	@FindBy(xpath = "//input[@name='firstname']")
	protected WebElement txtFirstName;
	@FindBy(xpath = "//input[@name='middlename']")
	protected WebElement txtmiddlename;
	@FindBy(xpath = "//input[@name='lastname']")
	protected WebElement txtlastname;
	@FindBy(xpath = "//input[@name='address_no']")
	protected WebElement txtaddressno;
	@FindBy(xpath = "//input[@name='email']")
	protected WebElement txtEmail;
	@FindBy(xpath = "//input[@name='address_name']")
	protected WebElement txtaddressname;
	@FindBy(xpath = "//input[@name='home_phone']")
	protected WebElement txthome_phone;
	@FindBy(xpath = "//input[@name='address_postal_code']")
	protected WebElement txtaddresspostalcode;
	@FindBy(xpath = "//input[@name='totalIncome']")
	protected WebElement txttotalIncome;
	@FindBy(xpath = "//input[@name='rent']")
	protected WebElement txtrent;
	@FindBy(id = "scoreBtn")
	protected WebElement getScoreBtn;
	protected By genderDropdown = By.xpath("//select[@name='gender']");
	protected By addresstypeDropdown = By.xpath("//select[@name='address_type']");
	protected By countryDropdown = By.xpath("//select[@name='address_country']");
	protected By serviceEntityDropdown = By.xpath("//select[@name='serviceEntity']");
	protected By dob_monthDropdown = By.xpath("//select[@name='dob_month']");
	protected By dob_dayDropdown = By.xpath("//select[@name='dob_day']");
	protected By dob_yearDropdown = By.xpath("//select[@name='dob_year']");
	protected By txtCreditBand = By.xpath("//div[@id='credit_band']");
	protected By editCustomerLink = By.xpath("//a[text()='Edit Customer']");

	/*
	 * Method: updateVerifyCustInfoForm Description: Verify and Fill Customer
	 * information Form Details Date: 2020 Author: Utsav Changes By: NA
	 */
	public void updateVerifyCustInfoForm() throws Exception {
		// waitUntilElementVisible(driver, txtSSN1, "ExplicitLowWait");
		
		String ssn = getRandomString(10);
		String dob = testData.get("DOB");
		String ssn1 = ssn.substring(0, 3);
		String ssn2 = ssn.substring(3, 5);
		String ssn3 = ssn.substring(5, 9);
		String creditBand[] = null;

		String dobmonth = "", dobDay = "";
		String arr[] = dob.split("/");
		// MonthFormation
		if (arr[0].length() == 1) {
			dobmonth = "0" + arr[0];
		} else {
			dobmonth = arr[0];
		}
		// DayFormation
		if (arr[1].length() == 1) {
			dobDay = "0" + arr[1];
		} else {
			dobDay = arr[1];
		}

		if (verifyObjectDisplayed(editCustomerLink)) {
			driver.findElement(editCustomerLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			mouseoverAndClick(getScoreBtn);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			creditBand = driver.findElement(txtCreditBand).getText().split("\\n");
		} else {
			txtSSN1.sendKeys(ssn1);
			txtSSN2.sendKeys(ssn2);
			txtSSN3.sendKeys(ssn3);
			txtFirstName.clear();
			txtFirstName.sendKeys(getRandomString(6, "Upper"));
			txtmiddlename.clear();
			txtmiddlename.sendKeys(getRandomString(2, "Upper"));
			txtlastname.clear();
			txtlastname.sendKeys(getRandomString(8, "Upper"));

			new Select(driver.findElement(dob_monthDropdown)).selectByValue(dobmonth);
			new Select(driver.findElement(dob_dayDropdown)).selectByValue(dobDay);
			new Select(driver.findElement(dob_yearDropdown)).selectByValue(arr[2]);

			txtaddressno.clear();
			txtaddressno.sendKeys(testData.get("Street#"));
			txtaddressname.clear();
			txtaddressname.sendKeys(testData.get("StreetName"));
			new Select(driver.findElement(addresstypeDropdown)).selectByVisibleText("STREET");
			// new
			// Select(driver.findElement(addresstypeDropdown)).selectByIndex(0);
			txtaddresspostalcode.clear();
			txtaddresspostalcode.sendKeys(testData.get("Zip"));

			new Select(driver.findElement(countryDropdown)).selectByVisibleText("UNITED STATES");

			txthome_phone.clear();
			txthome_phone.sendKeys(getRandomString(10));
			txtEmail.clear();
			txtEmail.sendKeys(testData.get("email"));
			txttotalIncome.sendKeys(getRandomString(5));
			txtrent.clear();
			txtrent.sendKeys(getRandomString(4));
			mouseoverAndClick(getScoreBtn);
			// .click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			creditBand = driver.findElement(txtCreditBand).getText().split("\\n");
		}
		
		if (creditBand[1].trim() != "no score on file") {
			tcConfig.updateTestReporter("scoreCustomerPage", "updateVerifyCustInfoForm", Status.PASS,
					"Tour ID Hard scored ; Credit Band : " + creditBand[1].trim());
		} else {
			tcConfig.updateTestReporter("scoreCustomerPage", "updateVerifyCustInfoForm", Status.FAIL,
					"No Score On File");
		}

	}

}
