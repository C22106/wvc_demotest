package acs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SelectRolesPages extends ACSBasePage {

	public static final Logger log = Logger.getLogger(SelectRolesPages.class);

	public SelectRolesPages(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By serviceEntityDropdown = By.xpath("//select[@name='serviceEntity']");
	@FindBy(id = "next-btn")
	protected WebElement NextBtn;
	@FindBy(id = "getcustomer-btn")
	protected WebElement RetrieveCustBtn;
	@FindBy(xpath = "//a[contains(text(),'Apply for Line of Credit')]")
	protected WebElement ApplyLOCLink;
	@FindBy(xpath = "//a[contains(text(),'Search LOC Applications')]")
	protected WebElement searchLOCLink;
	@FindBy(id = "continueBtn")
	protected WebElement continueBtn;
	protected By entityDrpdwn = By.xpath("//select[@id='selectEntity']");
	protected By locationDrpdwn = By.xpath("//select[@id='location']");
	protected By tourMemberPrescreenId_Drpdwn = By.xpath("//select[@id='selectItem']");
	protected By tourNumber_Txtbx = By.xpath("//input[@id='tMNumber-tourNumber']");
	protected By applicantSelection = By.xpath("(//table[@id='applicants-selection']//input)[1]");
	protected By nextBtn_ApplicantSelection = By.xpath("//button[@id='VCCPrescreenBtn']");
	protected By headerPrescreenMainMenu = By.xpath("//*[@id='page-body-content-1col']/h1");
	protected By prescreenDashboard_lnk = By.xpath("//ul[@id='loc-links']/li[2]/a");
	protected By LookuporSubmitPrescreenRequest_lnk = By.xpath("//ul[@id='loc-links']/li[1]/a");
	protected By pageHeader = By.xpath("//*[@id='page-body-content-1col']/h1");
	@FindBy(xpath = "//img[@title='Home']")
	protected WebElement homeIcon;
	protected By LocationDrpdown = By.xpath("//select[@name='locationName']");

	/*
	 * Method: SelectRole Description: Select Role Date: 2020 Author: Utsav
	 * Changes By: NA
	 */
	public void SelectRole() throws Exception {
		String roleOption = testData.get("ServiceEntity");
		waitUntilElementVisible(driver, NextBtn, "ExplicitWaitLongerTime");
		new Select(driver.findElement(serviceEntityDropdown)).selectByVisibleText(roleOption);

		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				roleOption + "has been selected from Service Entity dropdown");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// NextBtn.click();
		mouseoverAndClick(NextBtn);
		// driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (roleOption.contains("Line")) {
			waitUntilElementVisible(driver, ApplyLOCLink, "ExplicitWaitLongerTime");
			if (verifyElementDisplayed(ApplyLOCLink)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Navigated to main menu selection page");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Unable to navigate to main menu selection page");
			}
		} else {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisible(driver, RetrieveCustBtn, "ExplicitWaitLongerTime");

			if (verifyElementDisplayed(RetrieveCustBtn)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Navigated to Retrieve customer page");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Unable to navigate to Retrieve customer page");
			}
		}

	}

	/*
	 * Method: SelectRole_VCCPrescreen Description: Select Location VCC
	 * PreScreen Detail Date: 2020 Author: Utsav Changes By: NA
	 */
	public void SelectRole_VCCPrescreen() throws Exception {
		String roleOption = testData.get("ServiceEntity");
		waitUntilElementVisible(driver, NextBtn, "ExplicitWaitLongerTime");
		new Select(driver.findElement(serviceEntityDropdown)).selectByVisibleText(roleOption);

		tcConfig.updateTestReporter("selectRolesPages", "SelectRole_VCCPrescreen", Status.PASS,
				roleOption + " has been selected from Service Entity dropdown");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		mouseoverAndClick(NextBtn);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisible(driver, headerPrescreenMainMenu, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(headerPrescreenMainMenu)) {
			tcConfig.updateTestReporter("selectRolesPages", "SelectRole_VCCPrescreen", Status.PASS,
					"Navigated to Prescreen main menu selection page");
		} else {
			tcConfig.updateTestReporter("selectRolesPages", "SelectRole_VCCPrescreen", Status.FAIL,
					"Unable to navigate to Prescreen main menu selection page");
		}

	}

	/*
	 * Method: navigateToHome Description: Navigate to home Date: 2020 Author:
	 * Utsav Changes By: NA
	 */
	public void navigateToHome() {
		waitUntilElementVisible(driver, homeIcon, "ExplicitWaitLongerTime");
		homeIcon.click();
	}

	/*
	 * Method: SelectRole_LineofCredit Description: Select Line of credit Date:
	 * 2020 Author: Utsav Changes By: NA
	 */
	public void SelectRole_LineofCredit() {
		String roleOption = testData.get("ServiceEntity2");
		waitUntilElementVisible(driver, NextBtn, "ExplicitWaitLongerTime");
		new Select(driver.findElement(serviceEntityDropdown)).selectByVisibleText(roleOption);

		tcConfig.updateTestReporter("selectRolesPages", "SelectRole_LineofCredit", Status.PASS,
				roleOption + " has been selected from Service Entity dropdown");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		mouseoverAndClick(NextBtn);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisible(driver, headerPrescreenMainMenu, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(headerPrescreenMainMenu)) {
			tcConfig.updateTestReporter("selectRolesPages", "SelectRole_LineofCredit", Status.PASS,
					"Navigated to Prescreen main menu selection page");
		} else {
			tcConfig.updateTestReporter("selectRolesPages", "SelectRole_LineofCredit", Status.FAIL,
					"Unable to navigate to Prescreen main menu selection page");
		}

	}

	/*
	 * Method: vacationClubCreditPrescreen_Selection Description: Vacation Club
	 * Credit Prescreen Date: 2020 Author: Utsav Changes By: NA
	 */
	public void vacationClubCreditPrescreen_Selection() throws Exception {
		String selectionOption = testData.get("PrescreenSelection");

		if (selectionOption.equalsIgnoreCase("Prescreen Dashboard")) {
			driver.findElement(prescreenDashboard_lnk).click();
			tcConfig.updateTestReporter("selectRolesPages", "vacationClubCreditPrescreen_Selection", Status.PASS,
					"Clicked on Prescreen Dashboard");
			waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");

			if (driver.findElement(pageHeader).getText()
					.equalsIgnoreCase("Vacation Club Credit Prescreen Offer Search")) {
				tcConfig.updateTestReporter("selectRolesPages", "vacationClubCreditPrescreen_Selection", Status.PASS,
						"Navigated to Vacation Club Credit Prescreen Offer Search page");
			} else {
				tcConfig.updateTestReporter("selectRolesPages", "vacationClubCreditPrescreen_Selection", Status.FAIL,
						"Unable to navigate to Vacation Club Credit Prescreen Offer Search page");
			}
		}

		if (selectionOption.equalsIgnoreCase("Look up or Submit Prescreen Request")) {
			driver.findElement(LookuporSubmitPrescreenRequest_lnk).click();
			tcConfig.updateTestReporter("selectRolesPages", "vacationClubCreditPrescreen_Selection", Status.PASS,
					"Clicked on Look up or Submit Prescreen Request");
			waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");

			if (driver.findElement(pageHeader).getText()
					.equalsIgnoreCase("Prescreen Tour Number / Member Number /Prescreen Id Entry")) {
				tcConfig.updateTestReporter("selectRolesPages", "vacationClubCreditPrescreen_Selection", Status.PASS,
						"Navigated to entity selection page");
			} else {
				tcConfig.updateTestReporter("selectRolesPages", "vacationClubCreditPrescreen_Selection", Status.FAIL,
						"Unable to navigate entity selection page");
			}
		}
	}

	/*
	 * Method: enitySelection Description: Select Entity Date: 2020 Author:
	 * Utsav Changes By: NA
	 */
	public void enitySelection() throws Exception {

		new Select(driver.findElement(entityDrpdwn)).selectByVisibleText(testData.get("Entity"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(locationDrpdwn)).selectByVisibleText(testData.get("Location"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(tourMemberPrescreenId_Drpdwn))
				.selectByVisibleText(testData.get("TourMemberPrescreenId"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisible(driver, tourNumber_Txtbx, "ExplicitWaitLongerTime");

		driver.findElement(tourNumber_Txtbx).sendKeys(testData.get("TourID"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		continueBtn.click();
		waitUntilElementVisible(driver, pageHeader, "ExplicitWaitLongerTime");

		if (driver.findElement(pageHeader).getText().equalsIgnoreCase("Applicant Selection")) {
			tcConfig.updateTestReporter("selectRolesPages", "enitySelection", Status.PASS,
					"Navigated to Applicant Selection page");
		} else {
			tcConfig.updateTestReporter("selectRolesPages", "enitySelection", Status.FAIL,
					"Unable to navigate Applicant Selection page");
		}

		driver.findElement(applicantSelection).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("selectRolesPages", "enitySelection", Status.PASS,
				"Applicant Selected successfully");

		// String strFirstName= testData.get("First");
		// String strLastName= testData.get("Last");
		// driver.findElement(By.xpath("//input[@id='nonScoredApplicant']")).sendKeys(strFirstName+"
		// "+strLastName);

		driver.findElement(nextBtn_ApplicantSelection).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: applyLOC Description: Apply LOC Date: 2020 Author: Utsav Changes
	 * By: NA
	 */
	public void applyLOC() throws Exception {
		waitUntilElementVisible(driver, ApplyLOCLink, "ExplicitWaitLongerTime");
		ApplyLOCLink.click();
		waitUntilElementVisible(driver, continueBtn, "ExplicitWaitLongerTime");
		if (verifyElementDisplayed(continueBtn)) {
			tcConfig.updateTestReporter("selectRolesPages", "applyLOC", Status.PASS,
					"Successfully navigated to Member number entry page");
		}

	}

	/*
	 * Method: searchLOC Description: Search Loc Date: 2020 Author: Utsav
	 * Changes By: NA
	 */
	public void searchLOC() throws Exception {
		waitUntilElementVisible(driver, ApplyLOCLink, "ExplicitLowWait");
		searchLOCLink.click();

	}

	/*
	 * Method: SelectRoleForSoftScore Description: Select Role for Softscore:
	 * 2020 Author: Utsav Changes By: NA
	 */
	public void SelectRoleForSoftScore() throws Exception {
		String roleOption = testData.get("ServiceEntitySoftscore");
		homeIcon.click();
		waitUntilElementVisible(driver, serviceEntityDropdown, "ExplicitWaitLongerTime");
		new Select(driver.findElement(serviceEntityDropdown)).selectByVisibleText(roleOption);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		tcConfig.updateTestReporter("selectRolesPages", "SelectRoleForSoftScore", Status.PASS,
				roleOption + "has been selected from Service Entity dropdown");
		// NextBtn.click();
		mouseoverAndClick(NextBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		// waitUntilObjectVisible(driver, LocationDrpdown, 20);
		if (verifyObjectDisplayed(LocationDrpdown)) {
			tcConfig.updateTestReporter("selectRolesPages", "SelectRoleForSoftScore", Status.PASS,
					"Navigated to Select Location Page");
		} else {
			tcConfig.updateTestReporter("selectRolesPages", "SelectRoleForSoftScore", Status.FAIL,
					"Unable to navigate to Select Location Page");
		}
	}

}
