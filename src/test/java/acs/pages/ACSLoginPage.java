package acs.pages;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ACSLoginPage extends ACSBasePage {

	public static final Logger log = Logger.getLogger(ACSLoginPage.class);

	public ACSLoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@id='fldUsername']")
	protected WebElement textUsername;
	@FindBy(id = "login-btn")
	protected WebElement buttonLogIn;
	@FindBy(xpath = "//input[@id='fldPassword']")
	protected WebElement textPassword;
	@FindBy(xpath = "//a[text()='Logout']")
	protected WebElement LogoutBtn;
	@FindBy(xpath = "//div[@class='msg inform']")
	protected WebElement logoutMsg;
	@FindBy(xpath = "//img[@title='Home']")
	protected WebElement homeIcon;
	@FindBy(id = "next-btn")
	protected WebElement NextBtn;
	protected By buildVersion = By.xpath("//div[@id='page-hdr-content']");

	/*
	 * Method: setUserName Description:Set Username Date: 2020 Author: Utsav
	 * Changes By: NA
	 */
	protected void setUserName(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsername, "ExplicitWaitLongerTime");
		textUsername.sendKeys(strUserName);
		tcConfig.updateTestReporter("ACSLoginPage", "setUserName", Status.PASS, "Username is entered for ACS Login");
	}
	
	/*
	 * Method : launchApplication Parameters :None Description : launch
	 * application in browser Author : CTS Date : 2020 Change By : None
	 */

	public void launchApplication() {
		String url = testData.get("URL");
		driver.manage().deleteAllCookies();
		navigateToAppURL(url);

		try {
			driver.manage().window().maximize();
		} catch (Exception e) {
			log.info("Browser is Maximized");
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Application is Launched Successfully , URL : " + url);

	}
	

	/*
	 * Method: setPassword Description:Set Password Date:2020 Author: Utsav
	 * Changes By: NA
	 */
	public void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		textPassword.sendKeys(dString);
		tcConfig.updateTestReporter("ACSLoginPage", "setPassword", Status.PASS, "Entered password for ACS login");
	}

	/*
	 * Method: checkBuildVersion Description:Check Build Version Date: 2020
	 * Author: Utsav Changes By: NA
	 */
	public void checkBuildVersion() {
		waitUntilElementVisible(driver, buildVersion, "ExplicitMedWait");
		if (verifyObjectDisplayed(buildVersion)) {
			String strBuild = driver.findElement(buildVersion).getAttribute("innerText");

			tcConfig.updateTestReporter("ACSLoginPage", "launchApp", Status.PASS,
					"ACS URL launched-buildversion: " + strBuild);
		} else {
			tcConfig.updateTestReporter("ACSLoginPage", "launchApp", Status.FAIL, "ACS URL launch failed");
		}
	}

	/*
	 * Method: loginToACSApp Description:Login to ACS Date: 2020 Author: Utsav
	 * Changes By: NA
	 */
	public void loginToACSApp() throws Exception {
		checkBuildVersion();
		String userid = testData.get("UserID");
		String password = testData.get("Password");
		setUserName(userid);
		setPassword(password);
		waitUntilElementVisible(driver, buttonLogIn, "ExplicitWaitLongerTime");
		buttonLogIn.click();
		waitUntilElementVisible(driver, NextBtn, "ExplicitWaitLongerTime");
		if (verifyElementDisplayed(NextBtn)) {
			tcConfig.updateTestReporter("ACSLoginPage", "loginToACSApp", Status.PASS, "ACS Log in successfull");
		} else {
			mouseoverAndClick(buttonLogIn);
			waitUntilElementVisible(driver, NextBtn, "ExplicitWaitLongerTime");
			if (verifyElementDisplayed(NextBtn)) {
				tcConfig.updateTestReporter("ACSLoginPage", "loginToACSApp", Status.PASS, "ACS Log in successfull");
			} else {
				tcConfig.updateTestReporter("ACSLoginPage", "loginToACSApp", Status.FAIL, "ACS Log in failed");
			}
		}

	}

	/*
	 * Method: ACSLogout Description:Log Out Application Date: 2020 Author:
	 * Utsav Changes By: NA
	 */
	public void ACSLogout() throws Exception {

		try {
			waitUntilElementVisible(driver, LogoutBtn, "ExplicitWaitLongerTime");
			LogoutBtn.click();
			waitUntilElementVisible(driver, logoutMsg, "ExplicitWaitLongerTime");
			homeIcon.click();
			waitUntilElementVisible(driver, textUsername, "ExplicitWaitLongerTime");
			if (verifyElementDisplayed(textUsername)) {
				tcConfig.updateTestReporter("ACSLoginPage", "loginToACSApp", Status.PASS, "Log Off Successful");
			} else {
				tcConfig.updateTestReporter("ACSLoginPage", "loginToACSApp", Status.FAIL, "Log Off unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("ACSLoginPage", "loginToACSApp", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

}
