package salesforce.scripts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import salesforce.pages.AdditionalGuestsPage;
import salesforce.pages.ArrivalPage;
import salesforce.pages.BookATourPage;
import salesforce.pages.BusinessRules;
import salesforce.pages.CasesPage;
import salesforce.pages.HomePage;
import salesforce.pages.LeadsPage;
import salesforce.pages.OwnersPage;
import salesforce.pages.ProductPage;
import salesforce.pages.ReportPage;
import salesforce.pages.ReservationPage;
import salesforce.pages.SF_LoginPage;
import salesforce.pages.SalesRepAssignmentPage;
import salesforce.pages.SalesStorePage;
import salesforce.pages.TourRecordsPage;

public class SalesForceScripts extends TestBase {

	public static final Logger log = Logger.getLogger(SalesForceScripts.class);
	public static String resNum[];

	/*
	 * Function/event: tc_01_SalesForceLeadCreation Description: Salesforce Lead
	 * create and search with the created lead
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_001_NOV_SF_Disposition_WBWOwnerTour_Purchased(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_002_NOV_SF_WBWOwnerTour_SecSalesStore_Tour_Purchased(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_003_NOV_SF_Disposition_WVROwnerTour_Purchased(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// ownerPage.selectOwnerCreateTour();
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_004_NOV_SF_WVROwnerTour_SecSalesStore_Tour_Purchased(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_005_NOV_SF_Disposition_WBWOwnerTour_DNP(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// ownerPage.selectOwnerCreateTour();
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");

			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_006_NOV_SF_WBWOwnerTour_SecSalesStore_Tour_DNP(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_007_NOV_SF_Disposition_WVROwnerTour_DNP(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_008_NOV_SF_WVROwnerTour_SecSalesStore_Tour_DNP(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * for Bus Ops profile
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_009_NOV_SF_Disposition_WBWOwnerTour_Purchased_BusOPS(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_010_NOV_SF_WBWOwnerTour_SecSalesStore_Tour_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_011_NOV_SF_Disposition_WVROwnerTour_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// ownerPage.selectOwnerCreateTour();
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_012_NOV_SF_WVROwnerTour_SecSalesStore_Tour_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_013_NOV_SF_Disposition_WBWOwnerTour_DNP_BusOps(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// ownerPage.selectOwnerCreateTour();
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");

			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_014_NOV_SF_WBWOwnerTour_SecSalesStore_Tour_DNP_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();

			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_015_NOV_SF_Disposition_WVROwnerTour_DNP_BusOps(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");

			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_016_NOV_SF_WVROwnerTour_SecSalesStore_Tour_DNP_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_017_NOV_SF_Disposition_WBWLeadTour_Purchased(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_018_NOV_SF_Disposition_WBWLeadTour_SecSalesStore_Purchased(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_019_NOV_SF_Disposition_WVRLeadTour_Purchased(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_020_NOV_SF_Disposition_WVRLeadTour_SecSalesStore_Purchased(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_021_NOV_SF_Disposition_WBWLeadTour_DNP(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_022_NOV_SF_Disposition_WBWLeadTour_SecSalesStore__DNP(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_023_NOV_SF_Disposition_WVRLeadTour_DNP(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_024_NOV_SF_Disposition_WVRLeadTour_SecSalesStore_DNP(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_025_NOV_SF_Disposition_WBWLeadTour_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_026_NOV_SF_Disposition_WBWLeadTour_SecSalesStore_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_027_NOV_SF_Disposition_WVRLeadTour_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_028_NOV_SF_Disposition_WVRLeadTour_SecSalesStore_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_029_NOV_SF_Disposition_WBWLeadTour_DNP_BusOps(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_030_NOV_SF_Disposition_WBWLeadTour_SecSalesStore_Purchased_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_031_NOV_SF_Disposition_WVRLeadTour_DNP_BusOps(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_032_NOV_SF_Disposition_WVRLeadTour_SecSalesStore_DNP_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_033_NOV_SF_BookAppointmentNotPossible_WBW_Owner_NoSalesStore(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);

		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownerPage.CreateTourforOwner();
			// lead.createTourFromLead();
			lead.getlatestTour();
			trPage.validatebookAppointmentNoSales();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_034_NOV_SF_BookAppointmentNotPossible_WBW_Lead_NoSalesStore(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.validatebookAppointmentNoSales();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_035_NOV_SF_ReScoringErrorMsg_Lead(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);

		LeadsPage lead = new LeadsPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLead("ManualAddress");
			lead.validateSoftScoreOflead();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_036_NOV_SF_bookingWBWOwnerResTourAppointment(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour_Reservation();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_037_NOV_SF_bookingWVROwnerResTourAppointment(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour_Reservation();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_038_NOV_SF_bookingWBWOwnerResTourAppointment_SecSales(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			
			login.launchApp("strBrowserInUse");
            login.loginToSalesForceDiffUsers();
            homePage.navigateToMenu("1");
            ownPage.CreateTour_Reservation();
            leadspage.getlatestTour();
            trPage.bookAppointment();
            homePage.navigateToMenu("2");
            trPage.dispositionTour();
            homePage.navigateToMenu("3");
            trPage.searchForTour();
            trPage.validateTourStatus();
            login.salesForceLogout();
            // login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_039_NOV_SF_bookingWVROwnerResTourAppointment_SecSales(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			
			 login.launchApp("strBrowserInUse");
	            login.loginToSalesForceDiffUsers();
	            homePage.navigateToMenu("1");
	            ownPage.CreateTour_Reservation();
	            leadspage.getlatestTour();
	            trPage.bookAppointment();
	            homePage.navigateToMenu("2");
	            trPage.dispositionTour();
	            homePage.navigateToMenu("3");
	            trPage.searchForTour();
	            trPage.validateTourStatus();
	            login.salesForceLogout();
	          

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_040_NOV_SF_bookingWBWLeadResTourAppointment(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour_Reservation();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_041_NOV_SF_bookingWVRLeadResTourAppointment(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour_Reservation();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_042_NOV_SF_bookingWBWLeadResTourAppointment_SecSales(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour_Reservation();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_043_NOV_SF_bookingWVRLeadResTourAppointment_SecSales(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour_Reservation();
			homePage.navigateToMenu("3");
			trPage.searchForTour();
			trPage.validateTourStatus();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_044_NOV_SF_USafeHouseOwner_NoTour(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		/*
		 * LeadsPage leadspage=new LeadsPage(tcconfig); ReservationPage resPage=new
		 * ReservationPage(tcconfig);
		 */
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// trPage.searchForTour();
			ownPage.validate_NoTourErrorMessae_UsafeHouse_OwnerReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_045_NOV_SF_USafeHouseOwner_NoTour(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		/*
		 * LeadsPage leadspage=new LeadsPage(tcconfig); ReservationPage resPage=new
		 * ReservationPage(tcconfig);
		 */
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// trPage.searchForTour();
			ownPage.validate_NoTourErrorMessae_UsafeHouse_OwnerReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_046_NOV_SF_DuplicateLeadValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);

		LeadsPage lead = new LeadsPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.validateLead_Duplicate();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_047_NOV_SF_DNGOwnerTOurErrormsgValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		/*
		 * LeadsPage leadspage=new LeadsPage(tcconfig); ReservationPage resPage=new
		 * ReservationPage(tcconfig);
		 */
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// trPage.searchForTour();
			ownPage.validate_DNGReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_048_NOV_SF_ValidateLeadCreatefromGoogleAddress(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.createLead("GoogleAddress");
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * cover test case TC83 and TC84
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_049_NOV_SF_ValidateMsgAndTOurCreationOfDNGOwner(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);

		OwnersPage ownPage = new OwnersPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.CreateAndValidateTourforDNGOwner();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_050_NOV_SF_ValidateMsgAndTOurCreationOfNQLead(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.validateCommentMsgAndTourforNQLead();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_051_NOV_SF_ValidateMsgNQLeadReservation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.validateMsgForNQReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * TC_95
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_052_NOV_SF_validateLeadResTourAppointmentAndLessthanForteendaysErrormsg(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.dispositionTour_Reservation();
			ownPage.validate_NoTourErrorMessae_Reservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_053_NOV_SF_validateOwnerResBookTourUnlink(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		/*
		 * LeadsPage leadspage=new LeadsPage(tcconfig); ReservationPage resPage=new
		 * ReservationPage(tcconfig);
		 */
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.tour_Create_Reservation();
			ownPage.unLink();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_054_NOV_SF_validateNonOwnerResBookTourUnlink(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		/*
		 * LeadsPage leadspage=new LeadsPage(tcconfig); ReservationPage resPage=new
		 * ReservationPage(tcconfig);
		 */
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.tour_Create_Reservation();
			// ownPage.CreateTour_Reservation();
			// ownPage.getlatestTour();
			// ownPage.CreateTour_Reservation();
			// ownPage.createAndValidateUnlinkTourForOwnerAndLeadReservation();
			ownPage.unLink();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_055_NOV_SF_validateCheckdOutReservationNoTour(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// login.launchApp("strBrowserInUse");
			leadspage.validateCheckdOutReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_056_NOV_SF_validateCancelledReservationNoTour(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			// login.launchApp("strBrowserInUse");
			leadspage.validateNoShowCancelledReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_057_NOV_SF_validateTourBookForReservedReservation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			// login.launchApp("strBrowserInUse");
			homePage.navigateToMenu("1");
			ownPage.CreateTour_Reservation();
			leadspage.getlatestTour();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * TC011
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_058_NOV_SF_validateTourBookForNonOwnerNonUSAReservation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.bookTourforNonUSAReservation();
			leadspage.getlatestTour();
			// ownPage.CreateTour_Reservation();
			// ownPage.createAndValidateUnlinkTourForOwnerAndLeadReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_059_NOV_SF_validateEmailAddressPhoneNumberChangeOwnerReservation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			res.nativeSearchReservation();
			res.editNonOwnerReservation();
			login.salesForceLogout();
		

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * covered the TC_0142 and TC_0146
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_060_NOV_SF_validatePrintAndMailLetterbuttonforQbandLead(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.validateprintandMailLetterforQLead();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_061_NOV_SF_validatePrintAndMailLetterbuttonforNQbandLead(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.validateMailLetterforNQLead();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * TC_0145 and TC_0148
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_062_NOV_SF_validatePrintAndMailLetterbuttonforPCUDbandLead(Map<String, String> testData)
			throws Exception {
		System.out.println("Ritam");
		setupTestData(testData);
	 	SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		
		OwnersPage ownPage = new OwnersPage(tcconfig);
		
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
	
	
		
	
		try {
			
			System.out.println("Ritam");

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.validatemailandprintdisabledforPCband();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * TC_0147
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_063_NOV_SF_validateMailLetterbuttonforNQbandLead(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.validateMailLetterForQLead();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_064_NOV_SF_validatePrintbuttonInvisibleforOwners(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.validatePrintButtonNotvisibleForOwner();
			// ownPage.createAndValidateUnlinkTourForOwnerAndLeadReservation();

			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_065_NOV_SF_validateEmailAddressPhoneNumberChangeOwnerReservation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			resPage.nativeSearchReservation();
			resPage.editNonOwnerReservation();
			login.salesForceLogout();
			
			

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_066_NOV_SF_validateEmailAddressPhoneNumberChangeNonOwnerReservation_SalceforceAdmin(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.validatePhone_Email_change_non_owner_Reservation();
			login.salesForceLogout();
		
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_067_NOV_SF_validateBatchPrintNotpossibleForPCUDLeads(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);

		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.createLeadAndTour("ManualAddress");
			leadspage.getlatestTour();
			trPage.bookAppointment();
			homePage.navigateToMenu("2");
			trPage.validatePrintDisableforPCUDScore();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_068_NOV_SF_validateSameaddresswithPrimaryGuest(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.validatesameAddressAsGuest();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_069_NOV_SF_validateSameaddresswithPrimaryGuest_InhouseMarketingAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchReservationTour(testData.get("ReservationNumber"), "Reservation");
			ownPage.validatesameAddressAsGuest_inhouseMarketer();
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_070_NOV_SF_User_CancelTour_MobileUI_InHouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			leadPage.validateCreateAddGuest();
			bookTourPage.createTourMobile();
			trPage.selectCriteria();
			trPage.enterDigitalSignature();
			trPage.enterDetailsAndBookTour();
			bookTourPage.paymentContactDetails();
			leadPage.validateClickTour();
			login.salesForceLogoutMarketer();
		

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_071_NOV_SF_ChangeRequest_CaseSubmission_MarketingAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchReservationTour(testData.get("ReservationNumber"), "Reservation");
			res.caseSubmissionValidation();
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_072_NOV_SF_ChangeRequest_CaseSubmissionWVO_CorporateSuperUser(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			// homePage.navigateToUrl("1");
			homePage.navigateToMenu("1");
			// report.goToReservation();
			res.searchReservation();
			res.createChangerequest();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_073_NOV_SF_validateMailLetterbuttonEnabledForQlead(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			leadspage.searchForLead();
			leadspage.validateMailLetterForQLead();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_074_NOV_SF_validateCaseOnHold(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			// homePage.navigateToMenu("1");
			// homePage.openArrivals();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.createChangerequest();
			homePage.navigateToMenu("2");
			casePage.navigateToMyOpenCase();
			casePage.searchCreatedCase();
			casePage.updateStatus_Case_ON_Hold();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_075_NOV_SF_validatePrintButtonForMultipleTourRecord(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			// login.launchApp("strBrowserInUse");
			homePage.navigateToMenu("1");
			leadspage.searchForLead();
			leadspage.createTourFromLead();
			leadspage.getlatestTour();
			trPage.bookAppointment();
			leadspage.searchForLead();
			leadspage.createTourFromLead();
			leadspage.getlatestTour1();
			trPage.bookAppointment_Modified();
			homePage.navigateToMenu("2");
			trPage.validateMultiple();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_076_NOV_SF_validateWorkingCasesNotAppearinMycases(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		CasesPage cases = new CasesPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.navigateToMenu("2");
			cases.searchCase();
			cases.validateWorkingCasesNotAppearinMycases();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_0175_Verify_QOH_Updates_After_UpdatingSalesStore
	 * Description: To verify that Variable Priced Items “Wyndham Rewards Points
	 * " and "American Express" decreases by 1 when their voucher number is same
	 * when logged in as Corporate Super User. Designed By: Sabyasachi Nag
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_077_SF2JE_Verify_QOH_Updates_After_DistributeAndReturn_CorporateSuperUser(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ProductPage productPage = new ProductPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			int intQuantity = trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.validateQOHDectreaseAfterDistribute(intQuantity);
			trPage.validateQOHIncreasedAfterReturn(intQuantity);
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * 0176
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_078_SF2JE_Verify_QOH_Updates_After_DistributeAndReturn_BusOps(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ProductPage productPage = new ProductPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			int intQuantity = trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.validateQOHDectreaseAfterDistribute(intQuantity);
			trPage.validateQOHIncreasedAfterReturn(intQuantity);
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * designed by=Ajib parida
	 * TS=TC_JR_079_NOV_SF_BookAppointmentNotPossible_OwnerRes_NoSalesStore
	 * Description:-
	 * TC_JR_079_NOV_SF_BookAppointmentNotPossible_OwnerRes_NoSalesStore Test
	 * cases:-TC_018 Owner Reservation Tour cannot be booked_Without Sales Store
	 */

	@Test(dataProvider = "testData")
	public void TC_JR_079_NOV_SF_BookAppointmentNotPossible_OwnerRes_NoSalesStore(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);

		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.validatebookAppointmentNoSales();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * designed by=Ajib parida
	 * TS=TC_JR_080_NOV_SF_BookAppointmentNotPossible_LeadRes_NoSalesStore
	 * Description:-
	 * TC_JR_080_NOV_SF_BookAppointmentNotPossible_LeadRes_NoSalesStore Test
	 * cases:-TC_018 Lead Reservation Tour cannot be booked_Without Sales Store
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_080_NOV_SF_BookAppointmentNotPossible_LeadRes_NoSalesStore(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage leadspage = new LeadsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadspage.CreateTour_Reservation();
			leadspage.getlatestTour();
			trPage.validatebookAppointmentNoSales();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	// Sprint 14 Journey Enhancement scripts

	/*
	 * Function/event: TC_031_UpdateBookedTour_WithChildSalestore Description:
	 * Verify corporate super user is able to update a booked tour with new child
	 * salestore which is accessible. Designed By: Soumya Tripathy Pre-Requisites:
	 * 1- Check whether the lead present in excel sheet is showing in journey. If
	 * not update a new lead in excel sheet 2- Check for the primary and secondary
	 * child stores for the user and update it in the excel sheet in "SalesStore"
	 * and "NewSalesStore" field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_155_SF_JE_UpdateBookedTour_WithChildSalestore(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.getAppointmentSlot();
			trPage.verifyTourUpdated();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_032_Verify_MarketingFields_Prepopulate_or_LeftBlank_UpdatingSalesStore
	 * Description: Verify user is able to see the prepopulated marketing fields
	 * while updating the salesstore. Designed By: Soumya Tripathy Pre-Requisites:
	 * 1- Check whether the lead present in excel sheet is showing in journey. If
	 * not update a new lead in excel sheet 2- Check for the primary and secondary
	 * child stores for the user and update it in the excel sheet in "SalesStore"
	 * and "NewSalesStore" field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_156_SF_JE_Verify_MarketingFields_Prepopulate_or_LeftBlank_UpdatingSalesStore(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			lstStrMktgFields = trPage.bookTourAppointment();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.verifyPrepopulatedMktgFields(lstStrMktgFields);
			trPage.verifyBlankFieldAndUpdate();
			trPage.getAppointmentSlot();
			trPage.verifyTourUpdated();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_033_Track_History_AfterUpdating_SalesStore Description:
	 * Verify user is able to see the updated history in summary page Designed By:
	 * Soumya Tripathy Pre-Requisites: 1- Check whether the lead present in excel
	 * sheet is showing in journey. If not update a new lead in excel sheet 2- Check
	 * for the primary and secondary child stores for the user and update it in the
	 * excel sheet in "SalesStore" and "NewSalesStore" field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_157_SF_JE_Track_History_AfterUpdating_SalesStore(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			lstStrMktgFields = trPage.bookTourAppointment();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.getAppointmentSlot();
			trPage.verifyTourUpdated();
			trPage.navigateToSummaryTab();
			trPage.verifySalesStoreUpdatedHistory();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_034_Verify_Error_WhenIncentive_NotPresent_ForUpdatedSalesStore
	 * Description: Verify user will get an error if the incentive selected for
	 * parent salestore is not present in update salesstore Designed By: Soumya
	 * Tripathy Pre-Requisites: 1- Check whether the lead present in excel sheet is
	 * showing in journey. If not update a new lead in excel sheet 2- Check for the
	 * primary and secondary child stores for the user and update it in the excel
	 * sheet in "SalesStore" and "NewSalesStore" field 3- Check the
	 * Product/Incentive given in data sheet is present for saelstore while booking
	 * the tour and not there for salestore which user want to update to
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_158_SF_JE_Verify_Error_WhenIncentive_NotPresent_ForUpdatedSalesStore(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.verifyNotAbleToBookAppointment();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_035_Verify_QOH_Updates_After_UpdatingSalesStore
	 * Description: Verify if the Premium exists in the new Sales Store with QOH > 0
	 * return the distributed premium to the old sales store and deduct the QOH from
	 * the new sales store Designed By: Soumya Tripathy Pre-Requisites: 1- Check
	 * whether the lead present in excel sheet is showing in journey. If not update
	 * a new lead in excel sheet 2- Check for the primary and secondary child stores
	 * for the user and update it in the excel sheet in "SalesStore" and
	 * "NewSalesStore" field 3- Check the Product/Incentive given in data sheet is
	 * present for both salestores while booking the tour and which user want to
	 * update
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_159_SF2JE_Verify_QOH_Updates_After_UpdatingSalesStore(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ProductPage productPage = new ProductPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrQOH = new ArrayList<String>();
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("SalesStore"));
			salesStorePage.navigateToProduct();
			lstStrQOH = productPage.verifyProductAndGetQOH();
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			int intQuantity = trPage.addPromisedIncentive();
			System.out.println("The quantity is : " + intQuantity);
			trPage.distributePromisedIncentive();
			// homePage.globalSearch(testData.get("Product"));
			homePage.globalSearch(testData.get("SalesStore"));
			salesStorePage.navigateToProduct();
			productPage.verifyQOHDeductedForPrimarySalesStore(intQuantity, lstStrQOH.get(0));
			homePage.globalSearchTour(LeadsPage.createdTourID);
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.getAppointmentSlot();
			trPage.verifyTourUpdated();
			trPage.verifyIncentivesAdded();
			// homePage.globalSearch(testData.get("Product"));
			homePage.globalSearch(testData.get("SalesStore"));
			salesStorePage.navigateToProduct();
			productPage.verifyQOHUpdate(intQuantity, lstStrQOH);
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_036_Verify_Error_IfQOHequals0_ForNewSalesStorePremium
	 * Description: Verify if the Premium exists in the new Sales Store with QOH = 0
	 * then error will be thrown to user Designed By: Soumya Tripathy
	 * Pre-Requisites: 1- Check whether the lead present in excel sheet is showing
	 * in journey. If not update a new lead in excel sheet 2- Check for the primary
	 * and secondary child stores for the user and update it in the excel sheet in
	 * "SalesStore" and "NewSalesStore" field 3- Check the Product/Incentive given
	 * in data sheet is present for both salestores while booking the tour and which
	 * user want to update
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_160_SF2JE_Verify_Error_IfQOHequals0_ForNewSalesStorePremium(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ProductPage productPage = new ProductPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);
		List<String> lstStrQOH = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("SalesStore"));
			salesStorePage.navigateToProduct();
			lstStrQOH = productPage.verifyProductAndGetQOH();
			productPage.verifyZeroQOH(lstStrQOH);
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookAppointment();
			//trPage.bookTourAppointment();  /* Removed by Ritam  */
			trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.verifyNotAbleToBookAppointment();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_037_Verify_Error_IfDistributedIncenctive_NotPresentFor_SecondarySalesStore
	 * Description: Verify if the Premium exists in the new Sales Store is
	 * distributed and the same premium is not present in secondary salesstore then
	 * error will be thrown to user Designed By: Soumya Tripathy Pre-Requisites: 1-
	 * Check whether the lead present in excel sheet is showing in journey. If not
	 * update a new lead in excel sheet 2- Check for the primary and secondary child
	 * stores for the user and update it in the excel sheet in "SalesStore" and
	 * "NewSalesStore" field 3- Check the Product/Incentive given in data sheet is
	 * present for only primary salestores while booking the tour
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_161_SF2JE_Verify_Error_IfDistributedIncenctive_NotPresentFor_SecondarySalesStore(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ProductPage productPage = new ProductPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);
		List<String> lstStrQOH = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.verifyNotAbleToBookAppointment();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	// Sprint 15 Journey Enhancement
	/*
	 * Function/event: TC_038_Verify_OnPoint_InRotation_SalesRep Description: Verify
	 * SalesRep displayed in the order of rotation logic and in sales rep rotation
	 * screen an icon indicator is displayed who is on deck or next up to take a
	 * tour Designed By: Soumya Tripathy Pre-Requisites: 1- Check whether the lead
	 * present in excel sheet is showing in journey. If not update a new lead in
	 * excel sheet
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_162_SF2JE_Verify_OnPoint_InRotation_SalesRep(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			homePage.navigateToMenu("2");
			trPage.assignSaleRep();
			trPage.verifySalesRepRotation();
			homePage.navigateToMenu("3");
			trPage.verifyOnTourIconSalesRotation();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	// Sprint 14- Titan
	/*
	 * Function/event: TC_39_Verify_User_AbleTo_CreateOnlyOneChildTour Description:
	 * Verify user is able to create only one child tour in discovery tab for a
	 * dispositioned tour Designed By: Soumya Tripathy Pre-Requisites: Check whether
	 * the lead present in excel sheet is showing in journey. If not update a new
	 * lead in excel sheet
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_163_SF2JE_Verify_User_AbleTo_CreateOnlyOneChildTour(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.clickEditTour();
			trPage.tourDispositionTourPage("Yes");
			trPage.navigateToDiscoveryTab();
			trPage.addChildTour();
			trPage.openAndValidateChildTour();
			trPage.verifyAddingSecondChildTour();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_040_Verify_RecentNotes_SalesRepAssignment Description:
	 * Verify user is able to create and view recent notes in sales rep assignment
	 * screen Designed By: Soumya Tripathy Pre-Requisites: Check whether the lead
	 * present in excel sheet is showing in journey. If not update a new lead in
	 * excel sheet
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_164_SF2JE_Verify_RecentNotes_SalesRepAssignment(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.navigateToSummaryTab();
			trPage.createNewNote();
			homePage.navigateToMenu("2");
			trPage.verifyRecentNotesForAllTab();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_165_Rank_Change_WBW_Tour Description: verify user is
	 * able to change the rank of Sales Rep on Rotation Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_165_Rank_Change_WBW_Tour(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			trPage.moveSalesRepFromNotRotationToRotation();
			trPage.verifyRotationOfSalesRep();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_166_Verify_MysteryGift_Incentive_QOH Description:
	 * Verify that on distributing mystery gifts, the QOH of the mystery item is
	 * decreasing when logged in as Corporate Super User Designed By: Soumya
	 * Tripathy Pre-Requisites: Check whether the lead present in excel sheet is
	 * showing in journey. If not update a new lead in excel sheet
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_166_Verify_MysteryGift_Incentive_QOH(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);
		ProductPage productPage = new ProductPage(tcconfig);
		List<String> lstStrQOH = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.distributeAdditionalItems();
			// trPage.addMultiplePromisedIncentiveAndDistibute();
			// homePage.globalSearch(testData.get("SalesStore"));
			trPage.verifyQOHForMysteryGift();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_167_Verify_ClubMembershipAccount_GlobalSearch
	 * Description: verify that user is able to search club membership account when
	 * enter membership # in global search box Designed By: Soumya Tripathy
	 * Pre-Requisites: Update any club membership account number in excel before
	 * running
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_167_Verify_ClubMembershipAccount_GlobalSearch(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownerPage = new OwnersPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch_MembershipNo(testData.get("MmbershipNumber"));
			ownerPage.verifyClubMembershipAccount();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_168_Verify_ClubMembershipAccountWBW Description: verify
	 * that WorldMark brand is displayed for the club membership account Designed
	 * By: Soumya Tripathy Pre-Requisites: Update any club membership account number
	 * having brand as Worldmark in excel before running
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_168_Verify_ClubMembershipAccountWBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownersPage = new OwnersPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownersPage.searchOwnerByMemberNumber();
			ownersPage.verifyBrand();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_169_Verify_ClubMembershipAccountBrand_WBR Description:
	 * verify that Club Wyndham brand is displayed for the club membership account
	 * Designed By: Soumya Tripathy Pre-Requisites: Update any club membership
	 * account number having brand as Club Wyndham in excel before running
	 */

	@Test(dataProvider = "testData")
	public void TC_JR_169_Verify_ClubMembershipAccountBrand_WVR(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownersPage = new OwnersPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownersPage.searchOwnerByMemberNumber();
			ownersPage.verifyBrand();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_170_Verify_ClubMembershipAccountBrand_Shell
	 * Description: verify that Shell brand is displayed for the club membership
	 * account Designed By: Soumya Tripathy Pre-Requisites: Update any club
	 * membership account number having brand as Club Wyndham in excel before
	 * running
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_170_Verify_ClubMembershipAccountBrand_Shell(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownersPage = new OwnersPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownersPage.searchOwnerByMemberNumber();
			ownersPage.verifyBrand();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_171_Verify_WAAMIndicator_ArrivalList_InHouseMarketer
	 * Description: verify that Shell brand is displayed for the club membership
	 * account Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_171_Verify_WAAMIndicator_ServiceEntity_ArrivalList_InHouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownersPage = new OwnersPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			arrivalPage.verifyWAMIndicatorAndServiceEntity();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_172_Verify_InHouseAgent_CantAssignArrival_ToOthers
	 * Description: verify that In-House Marketing Agent cannot assign Arrival to
	 * any other agent except themselves Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_172_Verify_InHouseAgent_CantAssignArrival_ToOthers(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownersPage = new OwnersPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.verifyAssignMarketingAgent();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_173_Verify_CancelTour_AfterBooking Description: verify
	 * cancel tour after booking Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_173_Verify_CancelTour_AfterBooking(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookAppointment();
			trPage.verifyCancelTour();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_174_Verify_ExtendedStay_Reservation_InhouseMarketer
	 * Description: verify extended stay reservation logic for inhouse marketer
	 * Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_174_Verify_ExtendedStay_Reservation_InhouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		ReservationPage reservationPage = new ReservationPage(tcconfig);
		resNum = testData.get("ReservationNumber").split(",");
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchReservationTour(resNum[2], "Reservation");
			// homePage.searchReservation(resNum[2]);
			String[] date = arrivalpage.getCheckOutDate();
			homePage.searchReservationTour(resNum[0], "Reservation");
			// homePage.searchReservation(resNum[0]);
			arrivalpage.verifyExtendedStayPropForR1(date[0]);
			homePage.searchReservationTour(resNum[1], "Reservation");
			// homePage.searchReservation(resNum[1]);
			arrivalpage.verifyExtendedStayPropForR2(date[1]);
			arrivalpage.validateRelatedExtendedReservationsFields();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_175_Verify_Add_Transportation_ForBookedTour
	 * Description: verify adding transportation for a booked tour Designed By:
	 * Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_175_Verify_Add_Transportation_ForBookedTour(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		ReservationPage reservationPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			//trPage.clickBookAppointment();
			//trPage.updateSalesStore();
			trPage.bookAppointment();
			//trPage.bookTourAppointment();
			trPage.verifyTourUpdated();
			trPage.clickTransportationTab();
			trPage.createAndVerifyTransportation();
			login.salesForceLogout();
			
		

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_176_Verify_Score_Date_IsDisplayed Description: verify
	 * scoring date after softscoring Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_176_Verify_Score_Date_IsDisplayed(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.createLead("ManualAddress");
			leadPage.searchForLead();
			String time = leadPage.verifyTimeStamp();
			leadPage.clickIncentivebuttonandsoftscore();
			leadPage.verifyScoreBand();
			leadPage.verifyTimeStampAfterScoring(time);
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_177_Verify_AU_Value_UDNG_ClubPass_Owner Description:
	 * verify U-DNG value for a reservation Designed By: Soumya Tripathy
	 * Prerequisites: Take any DNG Owner reservation with following details: 1-Guest
	 * type - Owner 2-Stay Channel-Wyndham Club Pass 3-Stay type -Owner Wyndham Club
	 * Pass
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_177_Verify_AU_Value_UDNG_ClubPass_Owner(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.verifyAUValue(testData.get("A/U"));
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_178_Verify_Accountable_Arrival_InHouseMarketing
	 * Description: verify Inhouse Marketing Agent; is able to see that arrival is
	 * Arrivals as Accountable. when a reservation is created with below criteria
	 * Designed By: Soumya Tripathy Prerequisites: Take any reservation with
	 * following details: 1-Reservation Type = Owner or Trial Ownership Discovery or
	 * Club Pass 2-Guest Type or Record Type of Reservation = Owner Length of stay
	 * on the reservation is greater than or equal to one night. 3-Reservation
	 * Status (from Front Desk) is not = 'No Show' or 'Cancelled' 4-Arrival is not
	 * an Extended Stay
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_178_Verify_Accountable_Arrival_InHouseMarketing(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.verifyAccountableArrivals();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_230_Verify_Error_IfQOHequals0_Incentive Description:
	 * Verify if the Premium exists in the Sales Store with QOH = 0 then error will
	 * be thrown to user Designed By: Soumya Tripathy Pre-Requisites: 1- Check
	 * whether the lead present in excel sheet is showing in journey. If not update
	 * a new lead in excel sheet 2- Check for the primary child stores for the user
	 * and update it in the excel sheet in "SalesStore" field 3- Check the
	 * Product/Incentive given in data sheet is present for while booking the tour
	 * and which user want to update
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_230_Verify_Error_IfQOHequals0_Incentive(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_179_Verify_Accountable_Arrival_CorporateSuperUser
	 * Description: verify Corporate Super User; is able to see that arrival is
	 * Arrivals as Accountable Designed By: Soumya Tripathy Pre-Requisites:
	 * Reservation is created with below criteria 1- Reservation Type = Owner or
	 * Trial Ownership Discovery or Club Pass 2- Guest Type or Record Type of
	 * Reservation = Owner 3- Length of stay on the reservation is greater than or
	 * equal to one night 4- Reservation Status (from Front Desk) is not = 'No Show'
	 * or 'Cancelled' 5- Arrival is not an Extended Stay
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_179_Verify_Accountable_Arrival_CorporateSuperUser(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.verifyAccountableReservation();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_180_Verify_Multiple_Reservation_Owner Description:
	 * verify that user is able to create multiple reservations and reservation Type
	 * of first reservation will be same as received from external application which
	 * is Owner and for the rest of the reservations system should automatically
	 * change the reservation status as "Guest of Owner" Designed By: Soumya
	 * Tripathy Pre-Requisites: Two Reservations are created with below criteria 1-
	 * Same Resort Name 2- Same Primary Guest Name 3- Reservation Type = Owner 4-
	 * Check-In and Check-Out dates are either same on such reservations or Overlap
	 * 5- Stay type-'$75k+ Balance Owed 6- Stay Channel -Wyndham
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_180_Verify_Multiple_Reservation_Owner_GuestType_Exchange(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.verifyMultipleReservation();
			resPage.validateRelatedMultipleReservationFields();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_181_Verify_Tour_Created_IfStatus_ReservedOrInHouse_InhouseMarketingAgent
	 * Description: verify that a Tour is only created if a reservation status is
	 * Reserved, In-House ("Checked Out" and the Actual Check out date time is equal
	 * to today) Designed By: Soumya Tripathy Pre-Requisites: A reservation with
	 * status = Reserved should be created
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_181_Verify_Tour_Created_IfStatus_ReservedOrInHouse_InhouseMarketingAgent(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchReservationTour(testData.get("ReservationNumber"), "Reservation");
			resPage.verifyTourBooking("Reserved");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_182_Verify_Tour_NotCreated_IfStatus_CheckedOut_InhouseMarketingAgent
	 * Description: verify that a Tour cannot be created if reservation status is
	 * Checked-out and check out date and the Actual Check out date time is 24 hours
	 * prior to today Designed By: Soumya Tripathy Pre-Requisites: A reservation
	 * with status = Checked Out and the Actual Check out date time is 24 hours
	 * prior to today should be present
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_182_Verify_Tour_NotCreated_IfStatus_CheckedOut_InhouseMarketingAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchReservationTour(testData.get("ReservationNumber"), "Reservation");
			resPage.verifyTourBooking("Checked-Out");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_183_Verify_Tour_NotCreated_IfStatus_CancelledOrNoShow_InhouseMarketingAgent
	 * Description: verify that a Tour cannot be created if reservation status is
	 * Checked-out and check out date and the Actual Check out date time is 24 hours
	 * prior to today Designed By: Soumya Tripathy Pre-Requisites: A reservation
	 * with status = Checked Out and the Actual Check out date time is 24 hours
	 * prior to today should be present
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_183_Verify_Tour_NotCreated_IfStatus_CancelledOrNoShow_InhouseMarketingAgent(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchReservationTour(testData.get("ReservationNumber"), "Reservation");
			resPage.verifyTourBooking("Cancelled");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_184_Create_Tour_Slot_Wave_Allocation Description:
	 * Verify that user is able to Search Sales Store and Allocate Tour Slots
	 * Designed By: Soumya Tripathy Pre-Requisites: Before running the script update
	 * the date for which you want to allocate the tour
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_184_Create_Tour_Slot_Wave_Allocation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			// login.loginToSalesForce();
			homePage.navigateToMenu("1");
			homePage.globalSearchOperatinghours(testData.get("SalesStore"));
			salesStorePage.navigateToTourSlotAllocation();
			salesStorePage.tourSlotAllocation();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_185_Highlight_NQ_Arrivals_in_Red_if_age_less_than_25
	 * Description: : To verify that system is highlighting the entire row for age
	 * less than 25 in RED on the Arrival List Report for Non-Owner reservation
	 * Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_185_Highlight_NQ_Arrivals_in_Red_if_age_less_than_25(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			arrivalPage.selectArrivalAndVerifyRedHighlight("Age");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_186_Highlight_NQ_Arrivals_in_Red_if_Income_less_than_59999_InhouseMarketer
	 * Description: : To verify that system is highlighting the entire row for
	 * household income less than 59999 in RED on the Arrival List Report for
	 * Non-Owner reservation Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_186_Highlight_NQ_Arrivals_in_Red_if_Income_less_than_59999_InhouseMarketer(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			arrivalPage.selectArrivalAndVerifyRedHighlight("Income");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_187_Highlight_NQ_Arrivals_in_Red_if_SpecialOccasion_Added_InhouseMarketer
	 * Description: : To verify that system is highlighting the entire row for
	 * special occasion added in RED on the Arrival List Report for Non-Owner
	 * reservation Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_187_Highlight_NQ_Arrivals_in_Red_if_SpecialOccasion_Added_InhouseMarketer(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			arrivalPage.selectArrivalAndVerifyRedHighlight("Special Occasion");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_188_ChaseList_NoShow_Tour_Inhouse_CorporateUser
	 * Description: : verify an arrival is not displayed on the Chase List if the
	 * arrival is Inhouse status and there are multiple tours having one No show
	 * tour Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_188_ChaseList_NoShow_Tour_Inhouse_CorporateUser(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			/*
			 * homePage.navigateToMenu("1");
			 * homePage.globalSearch(testData.get("ReservationNumber"));
			 * resPage.bookTourFromReservation(); trPage.clickBookAppointment();
			 * trPage.bookTourAppointment(); trPage.clickEditTour();
			 * trPage.tourDispositionTourPage("No");
			 */
			homePage.navigateToMenu("2");
			reportPage.searchReservationRecord("Visible");
			/*
			 * homePage.globalSearch(testData.get("ReservationNumber"));
			 * resPage.bookTourFromReservation(); trPage.clickBookAppointment();
			 * trPage.bookTourAppointment(); reportPage.searchReservationRecord(
			 * "Not Visible");
			 */
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_0114 Case update_ WVO In-house marketing admin _Working
	 * Description: : To verify WVO In-house marketing admin is able to change the
	 * status to Working and save successfully Designed By: KGUPTA
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_202_UpdateCase_Working_Inhouse_MarketingAdmin(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.createChangerequest();
			homePage.navigateToMenu("2");
			casePage.navigateToMyOpenCase();
			casePage.searchCreatedCase();
			casePage.updateStatusandSave();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_0115 Case update_ WVO In-house marketing
	 * agent_Submitted_Custom UI Description: : To verify WVO In-house marketing
	 * agent is able to change the status to submitted and save successfully
	 * Designed By: KGUPTA
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_204_UpdateCase_Submitted_Inhouse_MarketingAdmin_MobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.selectFirstArrival();
			arrivalPage.createChangeRequest();
			casePage.validateCaseStatus_MobileUI();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_189_ArrivalList_Pagination_InHouseMarketer Description:
	 * : verify on the Arrival List pagination is enabled and Prev & back buttons
	 * are working properly Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_189_ArrivalList_Pagination_InHouseMarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.verifyPagination("ARRIVAL");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_190_ChaseList_Pagination_InHouseMarketer Description: :
	 * verify on the Arrival List pagination is enabled and Prev & back buttons are
	 * working properly Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_190_ChaseList_Pagination_InHouseMarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.verifyPagination("CHASE");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	// Partial (Soumya)

	/*
	 * Function/event: TC_JR_191_Verify_Discovery_ReservationType Description: :
	 * verify that the every reservation from Focus with Guest Type of Trial
	 * Ownership shows in Journey Reservation Type as Discovery Designed By: Soumya
	 * Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_191_Verify_Discovery_ReservationType(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.validatetravelchannel();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_192_Verify_ClubPass_ReservationType Description: :
	 * verify that the every reservation from Focus with Guest Type of 'Owner' shows
	 * in Journey Reservation Type 'Owner' or 'Club Pass' Designed By: Soumya
	 * Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_192_Verify_ClubPass_ReservationType(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.validatetravelchannel();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_193_Verify_SalesforceAdmin_CanAssign_ArrivalTo_OtherAgent Description:
	 * : verify that Salesforce Admin can assign Arrival to any other agent Designed
	 * By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_193_Verify_SalesforceAdmin_CanAssign_ArrivalTo_OtherAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			reportPage.searchRequiredList();
			reportPage.verifyAssigningArrival();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_194_Verify_Discovery_ReservationType_InHouseMarketer
	 * Description: : verify that the every reservation from Focus with Guest Type
	 * of Trial Ownership shows in Journey Reservation Type as Discovery when
	 * checked in Community Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_194_Verify_Discovery_ReservationType_InHouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchReservationTour(testData.get("ReservationNumber"), "Reservation");
			arrivalPage.validateTravelChannel();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_195_Verify_AddressBox_AdditionalGuest_InHouseMarketer
	 * Description: : verify In-house Marketing Agent is able to assign the a
	 * "Same Address" on the additional guest screen by clicking on same as Primary
	 * Guest address checkbox. Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_195_Verify_AddressBox_AdditionalGuest_InHouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.selectFirstArrival();
			String add = arrivalPage.getAddressDetailsOfRes();
			arrivalPage.clickAddAditionalGuest();
			arrivalPage.verifySameAsPrimaryAddressChkBox(add);
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_196_Verify_AddressBox_AdditionalGuest_BusinessOps
	 * Description: : verify Business Ops is able to assign the a "Same Address" on
	 * the additional guest screen by clicking on same as Primary Guest address
	 * checkbox. Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_196_Verify_AddressBox_AdditionalGuest_BusinessOps(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			resPage.selectReservation();
			String address = resPage.getGuestAddress();
			resPage.clickAddAdditionalGuest();
			resPage.verifyChkBoxSameAsPrimaryGuest(address);
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_197_Verify_MyCasesList_NotPresent_InHouseMarketer
	 * Description: : Verify that search for "My Cases" list will give no result
	 * Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_197_Verify_MyCasesList_NotPresent_InHouseMarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.selectFirstArrival();
			arrivalPage.createChangeRequest();
			arrivalPage.verifyCaseCreation();
			arrivalPage.verifyMyCasesOrMyResortOpenCasesNotPresent();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_198_Verify_MyResortOpenCasesList_NotPresent_InHouseMarketer
	 * Description: : Verify that search for "My Resort_Open_Cases" list will give
	 * no result Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_198_Verify_MyResortOpenCasesList_NotPresent_InHouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.selectFirstArrival();
			arrivalPage.createChangeRequest();
			arrivalPage.verifyCaseCreation();
			arrivalPage.verifyMyCasesOrMyResortOpenCasesNotPresent();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_199_Verify_New_Gift_Signature_Premium_Multiple_Value
	 * Description: Verify for variable priced premiums where the user enter
	 * multiple Values in the value field then on the Tour confirmation page and
	 * email sum the values and show the qty as 1 for a lead Designed By: Soumya
	 * Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_199_Verify_New_Gift_Signature_Premium_Multiple_Value(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addMultipleValuesIncentiveAndDistribute();
			trPage.clickTourDetails();
			trPage.clickGuestSignature();
			trPage.verifyQuantityInSignaturePage();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_201_Verify_Corporate_User_Cant_CloseCase_Without_Disposition
	 * Description: Verify that Corporate Super User cannot close a case without a
	 * case disposition . Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_201_Verify_Corporate_User_Cant_CloseCase_Without_Disposition(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.createChangerequest();
			homePage.navigateToMenu("2");
			casePage.navigateToMyOpenCase();
			casePage.searchCreatedCase();
			casePage.verifyCaseClosedError();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_202_Verify_Marketing_Admin_Cant_CloseCase_Without_Disposition
	 * Description: Verify that marketing admin cannot close a case without a case
	 * disposition . Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_202_Verify_Marketing_Admin_Cant_CloseCase_Without_Disposition(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.createChangerequest();
			homePage.navigateToMenu("2");
			casePage.navigateToMyOpenCase();
			casePage.searchCreatedCase();
			casePage.verifyCaseClosedError();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_203_Verify_UnAccountableOrAccountable_Changes_Report_SalesforceAdmin
	 * Description: Verify that marketing admin cannot close a case without a case
	 * disposition . Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_203_Verify_UnAccountableOrAccountable_Changes_Report_SalesforceAdmin(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		ReportPage reportsPage = new ReportPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			reportsPage.searchRequiredList();
			reportsPage.verifyAccountableOrUnAccReservation();
			homePage.globalSearch(testData.get("ReservationNumber"));
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}
	/* @Jyoti's script Nov-stiching */

	/**
	 *
	 * profile-- corporate user
	 * 
	 * @param testData
	 * @throws Exception
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_205_NOV_SF_Modify_NonOwner_Additional_Guest(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			res.nativeSearchReservation();
			res.editNonOwnerReservation();
			login.salesForceLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_205_NOV_SF_Modify_Owner_Additional_Guest(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			res.nativeSearchReservation();
			res.editOwnerReservation();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: helpIconValidation Description: Verify the Journey Help popup
	 */

	@Test(dataProvider = "testData")
	public void TC_JR_206_NOV_SF_All_Cases_Today_WVO_SystemAdmin_NativeUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			res.createChangerequest();
			homePage.navigateToMenu("2");
			report.searchAndNavigateToReport();
			report.verifyMyChangeReq();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_207_NOV_SF_Error_message_populate_Softscore_AdditionalGuest
	 * Description: User is able to create tour from Lead Reservation associated
	 * with additional guest without soft scoring even if the error message is
	 * displayed
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_207_NOV_SF_Error_message_populate_Softscore_AdditionalGuest(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			resPage.nativeSearchReservation();
			guestPage.clickAdditionalGuest();

			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Function/event: helpIconValidation Description: To verify WVO In-house
	 * marketing agent is able to change the status to closed and save successfully
	 * popup
	 */

	@Test(dataProvider = "testData")
	public void TC_JR_208_NOV_SF_Caseupdate_WVOCorporateSuperUser_Closed_nativeUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		CasesPage casePgae = new CasesPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			res.createChangerequest();
			homePage.navigateToMenu("2");
			casePgae.navigateToMyOpenCase();
			casePgae.searchCreatedCase();
			casePgae.updateStatusandSave();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "tc_JR_022_NOV_SF_verify_extended_stay_Reservation_InhousemarketingAgent"
	 * Description: To verify that user is able to create Extended Stay reservations
	 * and the Extended Check box should be checked in Journey for 2nd and 3rd
	 * reservations in the set of Extended stay reservations
	 */
	@Test(dataProvider = "testData")
	public void tc_JR_022_NOV_SF_verify_extended_stay_Reservation_InhousemarketingAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		AdditionalGuestsPage addGuest = new AdditionalGuestsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			res.validateGlobalSearchReservation();
			res.verifyRelatedReservation();
			login.testUserIHMarketerLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC_JR_023_NOV_SF_CancelTour_afterBooking_appointment_CorporateSuperUser"
	 * Description: To verify that user is able to create Extended Stay reservations
	 * and the Extended Check box should be checked in Journey for 2nd and 3rd
	 * reservations in the set of Extended stay reservations
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_023_NOV_SF_CancelTour_afterBooking_appointment_CorporateSuperUser(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		AdditionalGuestsPage addGuest = new AdditionalGuestsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.validateCancelledTour();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC_JR_209_NOV_SF_SalesforceAdmin_assign_arrival_to_anyother_agent"
	 * Description: To verify that user is able to create Extended Stay reservations
	 * and the Extended Check box should be checked in Journey for 2nd and 3rd
	 * reservations in the set of Extended stay reservations
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_209_NOV_SF_SalesforceAdmin_assign_arrival_to_anyother_agent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		AdditionalGuestsPage addGuest = new AdditionalGuestsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			report.validateArrivalList();
			report.validateClickReservation();
			res.validateMarketingAgentBlank();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC_JR_210_NOV_SF_User_update_secondary_childsalestore_Corporatesuperuser"
	 * Description: To verify user is able to update the sales store to any
	 * secondary child sales store it has access to and book tour
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_210_NOV_SF_User_update_secondary_childsalestore_Corporatesuperuser(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		AdditionalGuestsPage addGuest = new AdditionalGuestsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC_JR_211_NOV_SF_Verify_User_limit_Childtour_parentTour_CorporateSuperUser"
	 * Description: To verify user is able to update the sales store to any
	 * secondary child sales store it has access to and book tour
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_211_NOV_SF_Verify_User_limit_Childtour_parentTour_CorporateSuperUser(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		AdditionalGuestsPage addGuest = new AdditionalGuestsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			res.nativeSearchReservation();
			res.validateInhouseReservation();
			lead.getlatestTour();
			trPage.bookAppointment();
			bookTourPage.verifyTourDisposition();
			bookTourPage.validateDiscoveryTour();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC_JR_212_NOV_SF_NewGiftSignature_Template_AX_Premiummultiple_Lead"
	 * Description: Verify for variable priced premiums where the user enter
	 * multiple Values in the value field then on the Tour confirmation page and
	 * email sum the values and show the qty as 1 for a lead.(For example-he agent
	 * selects a American Express premium and adds the value as 50 and 100 in the
	 * value.
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_212_NOV_SF_NewGiftSignature_Template_AX_Premiummultiple_Lead(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		AdditionalGuestsPage addGuest = new AdditionalGuestsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.distributeAdditonalItems();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: "TC_JR_213_NOV_SF_AdditionalGuest_Creation_Owner"
	 * Description: To Verify User is able to create tour from additional guest of
	 * Owner Reservation when logged in as Corporate Super User
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_213_NOV_SF_AdditionalGuest_Creation_Owner(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		AdditionalGuestsPage addnGuestPage = new AdditionalGuestsPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.clickAddAdditionalGuest();
			resPage.addAddtnlGuest();
			addnGuestPage.createLead();
			addnGuestPage.softScoreAddoitionalGuest();
			addnGuestPage.bookTourForAdditionalGuest();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: "TC_JR_214_NOV_SF_Edit_NonOwner_Reservation" Description: To
	 * Verify User is able to create tour from additional guest of Owner Reservation
	 * when logged in as Corporate Super User
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_214_NOV_SF_Edit_NonOwner_Reservation(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		AdditionalGuestsPage addGuest = new AdditionalGuestsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			res.nativeSearchReservation();
			// res.validateEmailPhoneUpdate();
			res.edit_NonOwner_Reservation_Details();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: "TC_JR_215_NOV_SF_Edit_Owner_Reservation" Description: To
	 * Verify User is able to create tour from additional guest of Owner Reservation
	 * when logged in as Corporate Super User
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_215_NOV_SF_Edit_Owner_Reservation(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			res.nativeSearchReservation();
			res.edit_Owner_Reservation_Details();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: "TC_JR_216_NOV_SF_All_Cases_Today_WVO_InhouseMarketingAgent"
	 * Description: To Verify User is able to create tour from additional guest of
	 * Owner Reservation when logged in as Corporate Super User
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_216_NOV_SF_All_Cases_Today_WVO_InhouseMarketingAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");

			login.loginToSalesForceDiffUsers();
			res.validateGlobalSearchReservation();
			res.createChangeRequestInhouseMarketer();
			res.getlatestCase_Latest();
			homePage.navigateToReportMobUi();
			reportPage.searchRequiredListMobUi();
			res.getReportsMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC_JR_217_NOV_SF_All_Cases_Today_Corporatesuperuser_NativeUI" Description:
	 * To Verify User is able to create tour from additional guest of Owner
	 * Reservation when logged in as Corporate Super User
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_217_NOV_SF_All_Cases_Today_Corporatesuperuser_NativeUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		// SalesRepAssignmentPage slsRepPage = new
		// SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			report.searchAndNavigateToReport();
			report.validateMyChangeReq();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: "TC_JR_218_NOV_SF_All_Cases_Today_WVO_SystemAdmin_NativeUI"
	 * Description: To verify WVO In-house marketing agent is able to change the
	 * status to closed and save successfully
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_218_NOV_SF_All_Cases_Today_WVO_SystemAdmin_NativeUI(Map<String, String> testData)
	 
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			report.validateArrivalList();
			report.validateClickReservation();
			res.verifyCreateChangeRequest();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC_JR_219_NOV_SF_CreateChangerequest_assign_Inhousemarketingagent_mobileUI"
	 * Description: To verify WVO In-house marketing agent is able to change the
	 * status to closed and save successfully
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_219_NOV_SF_CreateChangerequest_assign_Inhousemarketingagent_mobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.selectFirstArrival();
			arrivalPage.createChangeRequestWithUserOrQueue();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: "TC_JR_220_NOV_SF_verify_BookAppointment" Description: To
	 * verify that the system should not allow to select Mystery Gift unless Mystery
	 * Gift AMEX or Mystery Gift WRP have been distributed first.
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_220_NOV_SF_verify_BookAppointment(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.returnAddPromisedItem();
			trPage.verifyMysteryNotPresent();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: "TC_JR_221_NOV_SF_Minivac_reservations_flowingJourney_WBW"
	 * Description: To verify that the Mini-vac reservation booked in CRS for WBW is
	 * flowing to Journey in real time.
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_221_NOV_SF_Minivac_reservations_flowingJourney_WBW(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			trPage.searchCRSTour();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_222_Verify_GuestSignature_Validation_AfterBooking
	 * Description: verify user is able to click Guest signature Button on the top.
	 * After Clicking , the user should be able to sign the Document confirming the
	 * Promised Items Designed By: Soumya Tripathy Pre-Requisites: 1- Check whether
	 * the lead present in excel sheet is showing in journey. If not update a new
	 * lead in excel sheet 2- Check the Product/Incentive given in data sheet is
	 * present for only primary salestores while booking the tour
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_222_Verify_GuestSignature_Validation_AfterBooking(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookAppointment();
			//trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.clickTourDetails();
			trPage.clickGuestSignature();
			trPage.verifyGuestSignAndSubmit();
			trPage.verifyTmSignaturePdf();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_223_Verify_AdditionalGuest_Creation_Owner Description:
	 * Verify User is able to create tour from additional guest of Non-Owner
	 * Reservation when logged in as Inhouse marketing Agent Designed By: Soumya
	 * Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_223_Verify_AdditionalGuest_Creation_Owner(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		AdditionalGuestsPage addnGuestPage = new AdditionalGuestsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.clickAddAdditionalGuest();
			resPage.addAddtnlGuest();
			addnGuestPage.createLead();
			addnGuestPage.softScoreAddoitionalGuest();
			addnGuestPage.bookTourForAdditionalGuest();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_JR_223_Verify_AdditionalGuest_Creation_NonOwner(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		AdditionalGuestsPage addnGuestPage = new AdditionalGuestsPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			homePage.globalSearch(testData.get("ReservationNumber"));
			resPage.clickAddAdditionalGuest();
			resPage.addAddtnlGuest();
			addnGuestPage.createLead();
			addnGuestPage.softScoreAddoitionalGuest();
			addnGuestPage.bookTourForAdditionalGuest();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_224_Verify_Add_Distribute_Premium Description: Verify
	 * user is able to distribute Beneath the Streets Premium to a booked tour
	 * Designed By: Soumya Tripathy Pre-Requisites: 1- Check whether the lead
	 * present in excel sheet is showing in journey. If not update a new lead in
	 * excel sheet 2- Check the Product/Incentive given in data sheet is present for
	 * only primary salestores while booking the tour
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_224_Verify_Add_Distribute_Premium(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.distributePromisedIncentive();
			trPage.distributeAdditionalItems();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_225_Verify_Assign_QueueToCase_InHouseMarketer
	 * Description: : Verify that search for "My Resort_Open_Cases" list will give
	 * no result Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_225_Verify_Assign_QueueToCase_InHouseMarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			homePage.openArrivals();
			arrivalPage.selectFirstArrival();
			arrivalPage.createChangeRequestWithUserOrQueue();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_226_Verify_HelpIcon_CaseCreation_TypeHowTo_InHouseMarketer Description:
	 * :Verify the WVO in-house marketing agent is able to create cases and validate
	 * the same in Aged Cases by Account Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_226_Verify_HelpIcon_CaseCreation_TypeHowTo_InHouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			casePage.createNewCaseHelpIconMobUi();
			homePage.navigateToReportMobUi();
			reportPage.searchRequiredListMobUi();
			reportPage.verifyCreatedCase();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_227_Verify_HelpIcon_CaseCreation_CorporateSuperUser
	 * Description: : Verify the Corporate super Corporate super user is able to
	 * crate cases and validated the same cases in My cases report Designed By:
	 * Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_227_Verify_HelpIcon_CaseCreation_CorporateSuperUser(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			casePage.createNewCaseHelpIcon();
			homePage.navigateToMenu("1");
			casePage.navigateToRecentlyViewedCase();
			casePage.selectCreatedCaseFromHelpIcon();
			casePage.changeCaseOwner();
			homePage.navigateToMenu("1");
			casePage.navigateToMyCases();
			casePage.verifyCaseInList();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_228_Verify_HelpIcon_CaseCreation_TypeProblem_InHouseMarketer
	 * Description: :Verify the WVO in-house marketing agent is able to create cases
	 * and validate the same in Aged Cases by Account Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_228_Verify_HelpIcon_CaseCreation_TypeProblem_InHouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			casePage.createNewCaseHelpIconMobUi();
			homePage.navigateToReportMobUi();
			reportPage.searchRequiredListMobUi();
			reportPage.verifyCreatedCase();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_229_Verify_HelpIcon_Error_MandatoryFields_CaseCreation_InHouseMarketer
	 * Description: :Verify the WVO in-house marketing agent is able to create cases
	 * and validate the same in Aged Cases by Account Designed By: Soumya Tripathy
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_229_Verify_HelpIcon_Error_MandatoryFields_CaseCreation_InHouseMarketer(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		CasesPage casePage = new CasesPage(tcconfig);
		ReportPage reportPage = new ReportPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			casePage.verifyMandatoryFieldError();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	// Sprint 22 Journey Enhancement scripts

	/*
	 * Function/event: TC_JR_231_Verify_RebookTour_Same_Salestore_WithIncentive
	 * Description: 1-verify user is able to re-book date and time of a no-show tour
	 * for Corporate super user(TC_01_JE_2096) 2-verify user is able to re-book date
	 * and time of a no-show tour if all the combination exists in book appointment
	 * screen for Corporate super user(TC_03_JE_2096) 3-verify user is able to
	 * re-book a no-show tour when one marketing field does not exist in book
	 * appointment screen for Corporate super user(TC_04_JE_2096) 4-verify that the
	 * promised items are copied from no show tour to new tour for same sales
	 * store(TC_05_JE_2096) Designed By: Soumya Tripathy Pre-Requisites: 1- Check
	 * whether the lead present in excel sheet is showing in journey. If not update
	 * a new lead in excel sheet 2- Check for the primary and secondary child stores
	 * for the user and update it in the excel sheet in "SalesStore" and
	 * "NewSalesStore" field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_231_Verify_RebookTour_Same_Salestore_WithIncentive(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			lstStrMktgFields = trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.clickTourDetails();
			trPage.clickEditTour();
			trPage.tourDispositionTourPage("No");
			trPage.clickBookAppointment();
			trPage.reBookTour();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.verifyPrepopulatedMktgFields(lstStrMktgFields);
			// trPage.verifyBlankFieldAndUpdate();
			trPage.getAppointmentSlot();
			trPage.verifyTourUpdated();
			trPage.verifyIncentivesAdded();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_232_Verify_RebookTour_Change_Salestore_WithIncentive
	 * Description: 1-verify user is able to re-book date and time of a no-show tour
	 * for different sales store for Corporate super user(TC_02_JE_2096) 2-verify
	 * that the promised items are copied from no show tour to new tour for
	 * different sales store(TC_06_JE_2096) Designed By: Soumya Tripathy
	 * Pre-Requisites: 1- Check whether the lead present in excel sheet is showing
	 * in journey. If not update a new lead in excel sheet 2- Check for the primary
	 * and secondary child stores for the user and update it in the excel sheet in
	 * "SalesStore" and "NewSalesStore" field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_232_Verify_RebookTour_Change_Salestore_WithIncentive(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			/*
			 * homePage.navigateToMenu("1"); leadPage.createLeadAndTour("ManualAddress");
			 */
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.clickTourDetails();
			trPage.clickEditTour();
			trPage.tourDispositionTourPage("No");
			trPage.clickBookAppointment();
			trPage.reBookTour();
			trPage.clickBookAppointment();
			trPage.updateSalesStore();
			trPage.getAppointmentSlot();
			trPage.verifyTourUpdated();
			trPage.verifyIncentivesAdded();
			login.salesForceLogout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_233_Verify_RebookTour_Same_Salestore_WithIncentive_InhouseMarketer
	 * Description: 1-verify user is able to re-book date and time of a no-show tour
	 * for different sales store for Corporate super user(TC_02_JE_2096) 2-verify
	 * that the promised items are copied from no show tour to new tour for
	 * different sales store(TC_06_JE_2096) Designed By: Soumya Tripathy
	 * Pre-Requisites: 1- Check whether the lead present in excel sheet is showing
	 * in journey. If not update a new lead in excel sheet 2- Check for the primary
	 * and secondary child stores for the user and update it in the excel sheet in
	 * "SalesStore" and "NewSalesStore" field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_233_Verify_RebookTour_Same_Salestore_WithIncentive_InhouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			lstStrMktgFields = trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.clickTourDetails();
			trPage.clickEditTour();
			trPage.tourDispositionTourPage("No");
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			// homePage.searchTour(LeadsPage.createdTourID); /*******Ritam
			// **********/
			trPage.reBookTour();
			trPage.verifyPrepopulatedMktgFieldsMarketer(lstStrMktgFields);
			trPage.verifyBlankFieldAndUpdateMobUI();
			trPage.getAppointmentSlotMobUi();
			trPage.clickNext();
			trPage.verifyIncentiveAvailableMobUi();
			// trPage.clickNext();
			trPage.selectCriteria();
			trPage.enterDigitalSignature();
			trPage.enterDetailsAndBookTour();
			bookTourPage.paymentContactDetails();
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * TC_JR_234_Verify_RebookTour_Same_Salestore_WithIncentive_InhouseMarketer
	 * Description: Verify When a User pulls up the Tour that is in No-Show status
	 * then he/she should have the ability to re-book the tour with a different
	 * salestore(TC_002_JE_1957) Designed By: Soumya Tripathy Pre-Requisites: 1-
	 * Verify When a User pulls up the Tour that is in No-Show status then he/she
	 * should have the ability to re-book the tour with a different salestore 2-
	 * Check for the primary and secondary child stores for the user and update it
	 * in the excel sheet in "SalesStore" and "NewSalesStore" field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_234_Verify_RebookTour_Different_Salestore_WithIncentive_InhouseMarketer(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookTourAppointment();
			trPage.addPromisedIncentive();
			trPage.clickTourDetails();
			trPage.clickEditTour();
			trPage.tourDispositionTourPage("No");
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			// homePage.searchTour(LeadsPage.createdTourID); /**********Ritam
			// *********/
			// homePage.searchTour("57485970");
			trPage.reBookTour();
			trPage.verifyBlankFieldAndUpdateMobUI();
			trPage.getAppointmentSlotMobUi();
			trPage.clickNext();
			trPage.verifyIncentiveAvailableMobUi();
			trPage.selectCriteria();
			trPage.enterDigitalSignature();
			trPage.enterDetailsAndBookTour();
			bookTourPage.paymentContactDetails();
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_JR_235_Search_Owner Description: Search owner having no
	 * disposition status or no purchase(TC_002_JE_1957) Designed By:
	 * Pre-Requisites: 1- Verify When a User pulls up the Tour that is in No-Show
	 * status then he/she should have the ability to re-book the tour with a
	 * different salestore 2- Check for the primary and secondary child stores for
	 * the user and update it in the excel sheet in "SalesStore" and "NewSalesStore"
	 * field
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_235_Testing(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			ownPage.searchAllOwner();
			ownPage.fetchAllOwners();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC10_JE5260_Verify that the User should be getting an error
	 * Message when the Payment Record is unable to match to a Clover Transaction
	 * and the payment record should remain as an Offline Refund _ Corporate User
	 * Description: Verify that the User should be getting an error Message when the
	 * Payment Record is unable to match to a Clover Transaction and the payment
	 * record should remain as an Offline Refund Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: 1.User Should have an Existing Tour with an offline payment
	 * 2. User should be having an Payment taken with Clover device for the same
	 * tour 3. User should be having Credentials for Corporate User
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_236_Verify_errorMsg_OfflineRefund(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			trPage.searchForTourId();
			trPage.navigateToPayments();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC06_JE5470_When an offline payment is taken clover id should
	 * be recorded on the payment field_In House marketer Description: When an
	 * offline payment is taken clover id should be recorded on the payment field
	 * Designed By: Jyoti_Chowdhury Pre-Requisites:1. User should have a clover
	 * device 2. user should have credentials to login as IHM
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_237_Verify_offlinepayment_inhousemarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			leadPage.validateCreateAddGuest();
			bookTourPage.createTourMobile();
			bookTourPage.addIncentivesandPayments();
			bookTourPage.tourConfirmation();
			bookTourPage.addCloverDevice();
			bookTourPage.ValidateSignature();
			bookTourPage.verifyTwoButtonsPresent();
			bookTourPage.paymentContactDetails();
			bookTourPage.validateAddTourNotes();
			bookTourPage.clickOnCreatedTour(LeadsPage.lead1);
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC01_JE5261_refund_connectivity_two buttons_inhouse marketer
	 * & TC03_JE5261_refund_success message popup_inhouse marketer Description:
	 * Verify that the User should be able to see the Two Buttons on Popup Message
	 * after Clicking Refund Button Designed By: Jyoti_Chowdhury Pre-Requisites:1.
	 * User should be having Credentials for In House marketer. 2. A Payment record
	 * should already be added to the tour( either by cash or by Credit card)
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_238_Verify_refund_OfflineRefund_inhousemarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchTour_Mobile();
			bookTourPage.clickPaymentId();
			bookTourPage.selectLineItems();
			bookTourPage.clickOfRefundButton();
			bookTourPage.clickOfflineRefundButton();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC02_JE5261_refund_connectivity_retry button function_
	 * marketer Description: Verify that the User should be able to reattempt the
	 * connection from journey on Clicking Retry Button Designed By: Jyoti_Chowdhury
	 * Pre-Requisites:1. User should be having Credentials for In House marketer. 2.
	 * A Payment record should already be added to the tour
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_239_Verify_refund_retrybutton_inhousemarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			homePage.searchTour_Mobile();
			bookTourPage.clickPaymentId();
			bookTourPage.selectLineItems();
			bookTourPage.clickOfRefundButton();
			bookTourPage.clickRetryButton();
			bookTourPage.clickOfRefundButton();
			bookTourPage.clickOfflineRefundButton();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_JE-6321_Validate country qualification Rule(negative) –
	 * owner (User can only book a tour for a Owner only if country is included in
	 * the country qualification rule including US)_Corp User TC004_JE-6321_Validate
	 * country qualification Rule(negative) – Lead (User can only book a tour for a
	 * Lead only if country is included in the country qualification rule including
	 * US)_Corp User Description: To verify User is not able to book a tour for a
	 * particluar country, if active country qualification rule is not present for
	 * that country Designed By: Jyoti_Chowdhury Pre-Requisites: 1) Country
	 * Qualifiaction should be present 2)Country qualification rule is set up (for
	 * testing setup for US) for owners and is inactive
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_240_Validate_countryqualificationrule_leadOWner_Standard(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			/*
			 * login.loginToSalesForceAdmin(); homePage.navigateToBusinessRuleNew();;
			 * businessPage.deleteRule_CountryRule(); login.salesForceLogout();
			 */
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			leadPage.searchForLead();
			leadPage.clickTourRecord();
			leadPage.bookTourForHardStop();
			homePage.navigateToMenu("3");
			ownPage.searchForOwner();
			ownPage.clickTourRecord();
			ownPage.bookTourForHardStop();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC33_JE-5597_New Credit Band rule_error message on
	 * conflict_CRM Admin Description: Verify that To verify user is able to create
	 * New credit Band Rule for a specific Credit Band Score, by Salestore, by
	 * category owner, creating a hard stop on the Tour for a Secondary Lead, if the
	 * Original Lead is a specific Credit Score Band Designed By: Jyoti_Chowdhury
	 * Pre-Requisites:1. User should be having Credentials for CRM Admin. 2. New
	 * Credit Band rule_created
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_259_Verify_CreditBandrule_errorMsg_CRMAdmin(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToBusinessRuleNew();
			// homePage.navigateToBusinessRuleNew();;
			businessPage.clickNew();
			businessPage.createNewBusinessRule();
			businessPage.validateErrorMessage();
			businessPage.deleteRule_CreditRule();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC29_JE-5597_New Credit Band rule_created
	 * successfully_Soft_CRM Admin Description: Verify that To verify user is able
	 * to create New credit Band Rule for a specific Credit Band Score, by
	 * Salestore, by category Non owner. Designed By: Jyoti_Chowdhury
	 * Pre-Requisites:1. User should be having Credentials for CRM Admin. 2. New
	 * Credit Band rule_created
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_258_Verify_CreditBandrule_softstop_CRMAdmin(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToBusinessRuleNew();
			;
			businessPage.deleteRule_CreditRule();
			businessPage.clickNew();
			businessPage.createNewBusinessRule();
			businessPage.createCreditBandSoftStop();
			businessPage.validateBusinessRule();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC001_JE-6321_New Credit Band rule_Country
	 * Qualifications_Salesforce Admin Description: To verify new rule-country
	 * qualification is added to the Business rule page and validate UI Designed By:
	 * Jyoti_Chowdhury Pre-Requisites: Country Qualifiaction should be present
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_242_Validate_Country_Qualifications_newRule_SalesforceAdmin(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToBusinessRuleNew();
			// homePage.navigateToBusinessRuleNew();;
			businessPage.clickNew();
			businessPage.selectCountryQualificationRule();
			businessPage.createCountryRule_AddCountry();
			businessPage.validateSucessMsg();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * 
	 * Function/event: TC001_JE-5807_Validate_incentive eligible_popup_Salesforce
	 * admin TC002_JE-5807_Create_incentive eligble rule_salesforce admin
	 * TC003_JE-5807_Validate_incentive eligible_on lead page_Disabled JE-6416
	 * -Sprint 29 added to esisting test case Description: To verify user is able to
	 * Validate incentive eligible UI and create the rule successfully Designed By:
	 * Jyoti_Chowdhury Pre-Requisites:1. User should be having Credentials for
	 * Salesforce admin 2. New Credit Band rule_created
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_243_Verify_incentive_eligible_fields_salesforceAdmin(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			homePage.navigateToBusinessRuleNew();
			;
			businessPage.clickNew();
			businessPage.selectIncentiveEligibleRule();
			businessPage.validateIncentiveFields();
			businessPage.createIncentiveRule();
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			leadPage.searchLead_Native();
			leadPage.scoreAndDisable();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * 
	 * Function/event: TC004_JE-5807_Validate incentive eligible rule on Reservation
	 * page Description: To verify user is able to Validate incentive eligible UI
	 * and create the rule successfully Designed By: Jyoti_Chowdhury
	 * Pre-Requisites:1. User should be having Credentials for Salesforce admin 2.
	 * New Credit Band rule_created
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_245_Verify_incentive_eligible_LeadReservation_CropUser(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			resPage.searchReservation_Native();
			resPage.scoreIncentivebtn_Native();
			resPage.validateACSScoreNative();
			resPage.validateIncentiveButtonDisabled_Native();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC005_JE-6320_Validate incentive eligible rule for
	 * international lead (standard only) Description: To verify that after an
	 * international lead is scored incentive eligible button is disabled in screen.
	 * Designed By: Jyoti_Chowdhury Pre-Requisites:1. User should be having
	 * Credentials for Salesforce admin 2. New Credit Band rule_created
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_244_Verify_incentive_eligible_international_lead(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchLead_Native();
			leadPage.scoreAndDisable();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC001_JE-5699_Validate credit band rule in Reservation page
	 * Standard_Hard stop Description: To verify user is able to create New credit
	 * Band Rule for a specific Credit Band Score and generate Hard Stop message
	 * while booking a tour. Designed By: Jyoti_Chowdhury Pre-Requisites: 1)PC
	 * credit score rule should be created. 2) A reservation linked to Lead with
	 * score PC should be avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_246_Validate_creditBandRule_Reservation_Standard_Hardstop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			homePage.navigateToBusinessRuleNew();
			;
			businessPage.clickNew();
			businessPage.createNewBusinessRule();
			businessPage.createBusinessRule();
			businessPage.validateBusinessRule();
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			resPage.searchReservation();
			resPage.bookTourForHardStop();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC003_JE-5799_Validate credit band rule in Reservation
	 * page_mobile_hard stop Description: To verify user is able to create New
	 * credit Band Rule for a specific Credit Band Score and generate Hard Stop
	 * message while booking a tour. Designed By: Jyoti_Chowdhury Pre-Requisites:
	 * 1)PC credit score rule should be created. 2) A reservation linked to Lead
	 * with score PC should be avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_247_Validate_creditBandRule_Reservation_MobileUI_Hardstop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			resPage.searchReservationTour();
			resPage.bookTourHardStop_Mobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC001_JE-5698_Validate credit band rule in lead page
	 * Standard_Hard stop Description: To verify user is able to create New credit
	 * Band Rule for a specific Credit Band Score and generate Soft Stop message
	 * while booking a tour. Designed By: Jyoti_Chowdhury Pre-Requisites: 1)PC
	 * credit score rule should be created. 2) A reservation linked to Lead with
	 * score PC should be avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_248_Validate_creditBandRule_Lead_Standard_Hardstop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchLead_Native();
			leadPage.createTourFromLead();
			leadPage.bookTourForHardStop();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC003_JE-5798_Validate credit band rule in lead
	 * page_mobile_hard stop Description: To verify user is able to create New
	 * credit Band Rule for a specific Credit Band Score and generate Soft Stop
	 * message while booking a tour. Designed By: Jyoti_Chowdhury Pre-Requisites:
	 * 1)PC credit score rule should be created. 2) A reservation linked to Lead
	 * with score PC should be avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_249_Validate_creditBandRule_Lead_MobileUI_Hardstop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");

			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchLeadMobile();
			leadPage.serachLeadBookTourMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_JE-5699_Validate credit band rule in Reservation page
	 * Standard _soft stop JE-6701 -Sprint 29 added to existing test case
	 * Description: To verify user is able to create New credit Band Rule for a
	 * specific Credit Band Score and generate Hard Stop message while booking a
	 * tour. Designed By: Jyoti_Chowdhury Pre-Requisites: 1)PC credit score rule
	 * should be created. 2) A reservation linked to Lead with score PC should be
	 * avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_250_Validate_creditBandRule_Reservation_Standard_Softstop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			homePage.navigateToBusinessRuleNew();
			;
			businessPage.deleteRule_CreditRule();
			businessPage.clickNew();
			businessPage.createNewBusinessRule();
			businessPage.createBusinessRule();
			businessPage.validateBusinessRule();
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			resPage.searchReservation();
			resPage.bookTour_Native();
			leadPage.getlatestTour_Latest();
			resPage.bookTourSoftStop();
			trPage.verifyCreditBandPresent();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC004_JE-5799_Validate credit band rule in Reservation
	 * page_mobile_soft stop Description: To verify user is able to create New
	 * credit Band Rule for a specific Credit Band Score and generate Soft Stop
	 * message while booking a tour. Designed By: Jyoti_Chowdhury Pre-Requisites:
	 * 1)PC credit score rule should be created. 2) A reservation linked to Lead
	 * with score PC should be avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_251_Validate_creditBandRule_Reservation_MobileUI_Softstop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			resPage.validateGlobalSearchReservation();
			resPage.bookTourSoftStop_Mobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_JE-5698_Validate credit band rule in lead page Standard
	 * _soft stop Description: To verify user is able to create New credit Band Rule
	 * for a specific Credit Band Score and generate Soft Stop message while booking
	 * a tour. Designed By: Jyoti_Chowdhury Pre-Requisites:1)PC credit score rule
	 * should be created. 2) A lead linked to Lead with score PC should be avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_252_Validate_creditBandRule_Lead_Standard_SoftStop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchLead_Native();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			leadPage.bookTourSoftStop();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:TC004_JE-5798_Validate credit band rule in lead
	 * page_mobile_soft stop Description: To verify user is able to create New
	 * credit Band Rule for a specific Credit Band Score and generate Soft Stop
	 * message while booking a tour. Designed By: Jyoti_Chowdhury Pre-Requisites:
	 * 1)PC credit score rule should be created. 2) A lead linked to Lead with score
	 * PC should be avialble
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_253_Validate_creditBandRule_Lead_MobileUI_SoftStop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");

			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchLeadMobile();
			leadPage.serachLeadBookTourMobile();
			leadPage.bookTourSoftStop_Mobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC001_JE5003_related reservations
	 * displayed_for_multi_stay_Reservations_Related Member_Owner_standard UI
	 * "Objective:- To verify if user is able to see the all the related
	 * reservations for a Multi stay reservation for the Related Member of an owner
	 * in Journey Designed By: Jyoti_Chowdhury Pre-Requisites: 1. A reservation
	 * having multiple stay reservation should already be created from Focus and
	 * present in Journey 2.Exact First Name , 3.Exact Last Name , 4. Same Resort 5.
	 * All the reservation should be having overlapping Scheduled Check in and Check
	 * out date ( 1st res -->Check In Date =21st Nov ,Check Out date = 23rd Nov ,2nd
	 * res --> check in date =22nd Nov , check out date = 23rd Nov ) 6. Member
	 * Number that belong to the same Owner Account.
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_254_Validate_related_reservations_multistay_standard(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");

			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			resPage.searchReservation();
			resPage.validateRelatedReservations();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_JE5003_all reservations displayed_related
	 * reservation_Multistay Reservations_Related member_Owner_Mobile UI
	 * Description: To verify if user is able to see the all the related
	 * reservations for a Multi stay reservation for the Related Member of an owner
	 * in Journey Designed By: Jyoti_Chowdhury Pre-Requisites: 1. A reservation
	 * having multiple stay reservation should already be created from Focus and
	 * present in Journey 2.Exact First Name , 3.Exact Last Name , 4. Same Resort 5.
	 * All the reservation should be having overlapping Scheduled Check in and Check
	 * out date ( 1st res -->Check In Date =21st Nov ,Check Out date = 23rd Nov ,2nd
	 * res --> check in date =22nd Nov , check out date = 23rd Nov ) 6. Member
	 * Number that belong to the same Owner Account.
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_255_Validate_related_reservations_multistay_MobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			resPage.validateGlobalSearchReservation();
			resPage.validateRelatedReservationMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC004_JE5003_all reservations displayed_Extended stay
	 * reservation_related Member for the same Owner_Mobile UI Description: To
	 * verify if User should be able to see the all the related reservations for a
	 * extended stay reservation for a Particular in Journey Designed By:
	 * Jyoti_Chowdhury Pre-Requisites: 1.A reservation having multiple stay
	 * reservation should already be created from Focus and present in Journey
	 * 2.Exact First Name , 3.Exact Last Name , 4. Same Resort 5. All the
	 * reservation should be having overlapping Scheduled Check in and Check out
	 * date ( 1st res -->Check In Date =21st Nov ,Check Out date = 23rd Nov ,2nd res
	 * --> check in date =22nd Nov , check out date = 23rd Nov ) 6. Member Number
	 * that belong to the same Owner Account.
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_256_Validate_related_reservations_ExtendedStay_MobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			resPage.validateGlobalSearchReservation();
			resPage.validateExtendedStayMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC001_JE-6329_Verify that international Lead is scored B1 on
	 * click on incentive eligible on lead page and reservation page_Tour Receptor
	 * TC003_JE-6329_Verify that international Owner is scored B1 on click on
	 * incentive eligible on Owner page and reservation page_Tour Receptor
	 * Description: To verify that international Lead is scored B1 on click on
	 * incentive eligible on lead page and reservation page JE-6677 -Sprint 29
	 * Designed By: Jyoti_Chowdhury Pre-Requisites: 1) Make sure no incentive rule
	 * is active for null credit band(if active, incentive button would be disabled
	 * and we cannot score) 2)Non –Lead reservation for the selected lead
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_260_Validate_internationalLeadandOwner_scored_TourReceptor(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchLead_Native();
			leadPage.scoreAndDisable();
			leadPage.clickOnRelatedTab();
			resPage.validateACSScoreNative();
			homePage.navigateToMenu("2");
			ownPage.searchForOwner();
			ownPage.validateIncentiveButtonDisable_Native();
			ownPage.validateQualificationStatus_OwnerNative();
			ownPage.clickOwnerReservation_Native();
			resPage.validateACSScoreNativeOwner();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC004_JE-6329_Verify that international Owner is scored B1 on
	 * click on incentive eligible on Owner page and reservation page_In house
	 * marketer TC002_JE-6329_Verify that international Lead is scored B1 on click
	 * on incentive eligible on lead page and reservation page_In house marketer
	 * Description: To verify that international Lead is scored B1 on click on
	 * incentive eligible on lead page and reservation page Designed By:
	 * Jyoti_Chowdhury Pre-Requisites: 1) Make sure no incentive rule is active for
	 * null credit band(if active, incentive button would be disabled and we cannot
	 * score) 2)Non –Lead reservation for the selected lead
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_261_Validate_internationalLeadandOwner_scored_MobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchLeadMobile();
			leadPage.searchLeadInMobile();
			leadPage.validateScoreAndQualification_Mobile();
			leadPage.clickReservation_Mobile();
			resPage.validateACSInMobile();
			homePage.navigateToSearchLeadMobile();
			ownPage.navigateToOwnerPageMobile();
			leadPage.validateScoreAndQualification_Mobile();
			leadPage.clickReservation_Mobile();
			resPage.validateACSInMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:TC001_JE-5789_Verify incentive rule is applicable for owners
	 * and reservation_in-house marketer Description: Verify incentive rule is
	 * applicable for owners and reservation Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: 1. Incentive rule is available for owner 2. Disable soft
	 * scoring setting is unchecked 3. US owner record and reservation for that
	 * owner
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_262_Verify_incentiveRule_OwnersandReservation_MobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchLeadMobile();
			ownPage.navigateToOwnerPageMobile();
			ownPage.scoreAndDisableOwner_Mobile();
			ownPage.validateQualificationStatus_Mobile();
			leadPage.clickReservation_Mobile();
			resPage.verifyIncentiveEligibleDisabled_Mobile();
			homePage.navigateToSearchLeadMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:TC002_JE-5789_Verify credit band rule is applicable for owners
	 * and reservation_hardstop_inhouse marketer Description: Verify credit band
	 * rule is applicable for owners and hard stop message is shown and not able to
	 * book tour Designed By: Jyoti_Chowdhury Pre-Requisites: 1. Credit band rule is
	 * available for owner for PC band for hard stop 2. Disable soft scoring setting
	 * is unchecked 3. US owner record and related reservation 4. US added to
	 * country qualification rule to allow to book rule
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_264_Verify_creditBandRule_OwnersandReservation_hardstop_MobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			homePage.navigateToBusinessRuleNew();
			;
			businessPage.deleteRule_CreditRule();
			businessPage.clickNew();
			businessPage.createNewBusinessRule();
			businessPage.createCreditBandRuleHardStop_Owner();
			businessPage.validateBusinessRule();
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchLeadMobile();
			ownPage.serachOwnerBookTourMobile();
			ownPage.validateQualificationStatus_Mobile();
			leadPage.clickReservation_Mobile();
			resPage.bookTourHardStop_Mobile();
			homePage.navigateToSearchLeadMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC003_JE-5789_Verify credit band rule is applicable for
	 * owners and reservation_softstop_inhouse marketer Description: Verify that
	 * credit band rule is applicable for owners and reservation and soft stop
	 * message is thrown and user able to book tour Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: 1. Credit band rule is available for owner for PC band for
	 * soft stop 2. Disable soft scoring setting is unchecked 3. US owner record and
	 * related reservation 4. US added to country qualification rule to allow to
	 * book rules
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_266_Verify_creditBandRule_OwnersandReservation_softstop_MobileUI(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			homePage.navigateToBusinessRuleNew();
			;
			businessPage.deleteRule_CreditRule();
			businessPage.clickNew();
			businessPage.createNewBusinessRule();
			businessPage.createCreditBandRuleHardStop_Owner();
			businessPage.validateBusinessRule();
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchLeadMobile();
			ownPage.serachOwnerBookTourMobile();
			ownPage.validateQualificationStatus_Mobile();
			leadPage.bookTourSoftStop_Mobile();
			homePage.navigateToSearchLeadMobile();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC001_JE-5874_Verify incentive rule is applicable for
	 * owners_Business ops Description: Verify incentive rule is applicable for
	 * owners and reservation Designed By: Jyoti_Chowdhury Pre-Requisites: 1.
	 * Incentive rule is available for owner 2. Disable soft scoring setting is
	 * unchecked 3. US owner record and reservation for that owner
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_263_Verify_incentiveRule_OwnersandReservation_Businessop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.searchForOwner();
			ownPage.validateIncentiveButtonDisable_Native();
			ownPage.validateQualificationStatus_OwnerNative();
			ownPage.clickOwnerReservation_Native();
			resPage.validateIncentiveButtonDisabled_Native();
			resPage.validateACSScoreNative();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_JE-5874_Verify credit band rule is applicable for
	 * owners and reservation_hardstop_Business Ops Description: Verify incentive
	 * rule is applicable for owners and reservation Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: 1. Credit band rule is available for owner for PC band for
	 * hard stop 2. Disable soft scoring setting is unchecked 3. US owner record and
	 * related reservation 4. US added to country qualification rule to allow to
	 * book rule
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_265_Verify_creditBandRule_OwnersandReservation_hardstop_Businessop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");

			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			ownPage.searchForOwner();
			ownPage.validateQualificationStatus();
			ownPage.clickTourRecord();
			ownPage.bookTourForHardStop();
			ownPage.clickOwnerReservation_Native();
			resPage.bookTourForHardStop();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC003_JE-5874_Verify credit band rule is applicable for
	 * owners and reservation_softstop_Business Ops Description: Verify incentive
	 * rule is applicable for owners and reservation Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: 1. Credit band rule is available for owner for PC band for
	 * soft stop 2. Disable soft scoring setting is unchecked 3. US owner record and
	 * related reservation 4. US added to country qualification rule to allow to
	 * book rules
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_267_Verify_creditBandRule_OwnersandReservation_softstop_Businessop(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			homePage.navigateToMenu("1");
			businessPage.deleteRule_CreditRule();
			businessPage.clickNew();
			businessPage.createNewBusinessRule();
			businessPage.createCreditBandRuleHardStop_Owner();
			businessPage.validateBusinessRule();
			login.salesForceLogout();
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			ownPage.searchForOwner();
			ownPage.validateQualificationStatus();
			ownPage.clickTourRecord();
			ownPage.getlatestTour();
			ownPage.validateSoftStop_Native();
			ownPage.clickOwnerReservation_Native();
			resPage.bookTour_Native();
			leadPage.getlatestTour_Latest();
			resPage.bookTourSoftStop();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC001_JE-6495_Duplicate error message for Country and
	 * Incentive Rule_Salesforce Admin JE-6495 Description: Verify incentive rule is
	 * applicable for owners and reservation Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: 1. Credit band rule is available for owner for PC band for
	 * soft stop 2. Disable soft scoring setting is unchecked 3. US owner record and
	 * related reservation 4. US added to country qualification rule to allow to
	 * book rules
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_268_Verify_DuplicateRules_Standard(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		BusinessRules businessPage = new BusinessRules(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceAdmin();
			// homePage.navigateToBusinessRules();
			homePage.navigateToBusinessRuleNew();
			;
			businessPage.clickNew();
			businessPage.selectIncentiveEligibleRule();
			businessPage.validateDuplicateIncentive();
			businessPage.validateDuplicateErrorMsg();
			businessPage.clickNew();
			businessPage.selectCountryQualificationRule();
			businessPage.createCountryRule_AddCountry();
			businessPage.validateDuplicateErrorMsg();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void tc_004_All_booked_tour_for_Primary_Parent_SalesStore_MobileUI_Marketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			leadPage.VerifyBookTourSalesstortTodayDate();
			leadPage.validateTourConfirmDropdown();
//			leadPage.selectSiteToursTomorrowAndVerifyTourRecord();
			leadPage.selectSiteToursThisWeekAndVerifyTourRecord();
			leadPage.selectMyToursTomorrowAndVerifyTourRecord();
			leadPage.selectMyTourThisWeekAndVerifyTourRecord();
			leadPage.validateCreateAddGuest();
			bookTourPage.createTourMobile();
			trPage.selectCriteria();
			trPage.enterDigitalSignature();
			trPage.enterDetailsAndBookTour();
//			bookTourPage.paymentContactDetails();
			leadPage.validateBookTourWithSameLead();
			leadPage.selectMyTourTodayAndVerifyTourRecord();
			leadPage.verifyIncreaseTourRecords();
			leadPage.selectMyTourTodayAndVerifyTourRecord();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void tc_028_Highlight_NQ_Arrivals_Red_MobileUI_InhouseMarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToArrivalListMobUI();
			arrivalPage.selectArrivalAndVerifyRedHighlight("Income");
			login.salesForceLogoutMarketer();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/************** SFMC Automation *******************************/

	@Test(dataProvider = "testData")
	public void TC01_Emails_Triggered_from_Marketing_cloud_Send_Email_checkbox_checked(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		SalesStorePage salesStrPage = new SalesStorePage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			salesStrPage.searchSalesStore();
			salesStrPage.selectSSCheckMC();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.verifySFMC();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC05_Tour confirmation Email triggered to customer_CW
	 * brand_Corporate Super User SFMC Description: Verify that when a CW Tour is
	 * booked in journey, a tour confirmation email is triggered to the Customer
	 * Designed By: Jyoti_Chowdhury Pre-Requisites: 1)Send Email from MC should be
	 * checked in the parent sales store to get emails from MC. 2) A reservation
	 * should already be present in Journe
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_05_SFMC_ResBookTour_CW_CropUser(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			resPage.searchReservation();
			resPage.bookTourFromReservation();
			// leadPage.bookTour_Native();
			//leadPage.getlatestTour_Latest();
			trPage.bookAppointment();
			trPage.clickGuestConfirmationSFMC();
			login.salesForceLogout();
			

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC06_Tour confirmation Email triggered to customer_WBW
	 * brand_Corporate Super User SFMC Description: Verify that when a WBW Tour is
	 * booked in journey, a tour confirmation email is triggered to the Customer
	 * Designed By: Jyoti_Chowdhury Pre-Requisites: 1)Send Email from MC should be
	 * checked in the parent sales store to get emails from MC
	 */
	@Test(dataProvider = "testData")
	public void TC_JR_06_SFMC_LeadBookTour_WBW_CropUser(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OwnersPage ownPage = new OwnersPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		ReservationPage resPage = new ReservationPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			leadPage.searchForLead();
			leadPage.createTourFromLead();
			leadPage.getlatestTour();
			trPage.bookAppointment();
			trPage.clickGuestConfirmationSFMC();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC12_SFMC_Verify_Email_Triggering_Process_for_MGVC_Tour_Update(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.updateAppointmentDate();
			trPage.clickGuestConfirmationSFMC();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC13_SFMC_Verify_Email_Triggering_Process_for_CW_Tour_Update(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.changebookingTime();
			trPage.clickGuestConfirmationforCWTour();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC14_SFMC_Verify_Email_Triggering_Process_for_WBW_Tour_Update(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.changeSalesStore();
			trPage.clickGuestConfirmationSFMC();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC01_Emails_Triggered_from_Marketing_cloud_Send_Email_checkbox_checked"
	 * Description: To verify
	 * Emails_Triggered_from_Marketing_cloud_Send_Email_checkbox_checked
	 * 
	 */

	@Test(dataProvider = "testData")
	public void TC10_Just_Breakfast_Checkin_Instructions_field_editable_Corporate_Super_User(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);

		SalesStorePage salesStrPage = new SalesStorePage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			salesStrPage.searchChildSalesStore();
			salesStrPage.editChildSSAddInst();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * "TC17_Tour_confirmation_Email_triggered_to_customer_CorporateMarketingreservations_minivac_CorporateSuperUser"
	 * Description: To Verify that tour confirmation email is triggered for a tour
	 * booked for Minivac reservation Pre-Requisite: CReate a Reservation & pass
	 * Reservation Number as Input in Data sheet Pre-Requisite:Send Email from SFMC
	 * needs to be checked for Parent Sales Store & it is done through code
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC17_Tour_confirmation_Email_triggered_to_customer_CorporateMarketingreservations_minivac_CorporateSuperUser(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		SalesStorePage salesStrPage = new SalesStorePage(tcconfig);
		ReservationPage reservationPage = new ReservationPage(tcconfig);
		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("2");
			salesStrPage.searchSalesStore();
			salesStrPage.selectSSCheckMC();
			homePage.navigateToMenu("3");
			reservationPage.seacrhReservationBookTour();
			// lead.getlatestTour_Latest();
			lead.getlatestTour();
			trPage.bookAppointmentMini();
			trPage.clickGuestConfirmationSFMC();
			// trPage.verifySFMC();
			// add email click function
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/********************
	 * SFMC Automation
	 **************************************/

	@Test(dataProvider = "testData")
	public void TC_001_Journey_verify_unique_paymentIdentifier_for_payment(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();
			// trPage.clickonPaymentsTab();
			trPage.clickonPaymentsTab();
			trPage.clickonPaymentIdLink();

			// trPage.clickPaymentTab();
			trPage.validatePresenceofPaymentsDetailsPage();

			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_002_user_select_payment_method_cash_credit(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 * trPage.clickOnCreatePayment(); trPage.addIncentive();
			 * trPage.processPayment();
			 */

			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_003_Reason type selection is required by user to process
	 * payment Automation Case:TC_003_Reason_type_selection Pre-Requisites: user
	 * should have a booked tour
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_003_Reason_type_selection(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.validateReasonDrpdwn();
			trPage.validateDefaultReason();
			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 * 
			 * trPage.clickOnCreatePayment(); trPage.addIncentive();
			 * trPage.verifyReasonTypes();
			 */

			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_004_Verify_Transaction_Error_Message_for_Unbooked_Tours(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.clickonIncentiveTab();
			trPage.validateTransactionErrorMessage();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_005_resend a text receipt for all past payments Automation
	 * Case:TC_005_resend_text_receipt_for_past_payments Pre-Requisites: user should
	 * have a booked tour
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_005_resend_text_receipt_for_past_payments(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */
			trPage.clickPaymentTab();
			trPage.verifyClickResendEmail();

			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_006_Verify_the_Payment_Receipt_Generation_functionality(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.clickonPrintReceiptButton();
			trPage.verifyPDFgeneration();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC_007_Verify that elements of the receipt Automation
	 * Case:TC_007_Verify_that_elements_of_the_receipt Pre-Requisites: user should
	 * have a booked tour
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_007_Verify_that_elements_of_the_receipt(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */
			trPage.clickPaymentTab();

			trPage.clickPrintReceipt();

			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_008_Verify_Discount_Sharing_Process_Between_Incentives(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.validateReasonDrpdwn();
			trPage.validateDefaultReason();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.verifyDiscountDistribution();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_009_Verify_the_Availability_of_Refundable_Tour_Deposit_CheckBox(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.globalSearchaSalesStore();
			salesStorePage.verifyRefundableTourDepositCheckbox();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_010_Verify_that_Refundable_Tour_Deposit_CheckBox_is_Editable(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.globalSearchaSalesStore();
			salesStorePage.verifyRefundableTourDepositCheckbox();
			salesStorePage.verifyRefndbleTourDepositisEdtble();
			salesStorePage.verifyRefndbleTourDepositisEdtble();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_011_Verify_The_Presence_of_Refundable_Tour_Deposit_Details(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.validateRefundableTourDepositSection();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_012_Verify_The_Absence_of_Refundable_Tour_Deposit(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ReservationPage res = new ReservationPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		ReportPage report = new ReportPage(tcconfig);
		ArrivalPage arrivalpage = new ArrivalPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		AdditionalGuestsPage guestPage = new AdditionalGuestsPage(tcconfig);
		SalesRepAssignmentPage slsRepPage = new SalesRepAssignmentPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.validateAbsenceofRefndbleTourDpstSection();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_013_Verify_the_Calculation_of_Marketer_Cost(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.captureInitialMarketerCost();
			trPage.enterIncentiveAmountCollected();
			trPage.verifyUpdatedMrktrCost();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_014_Verify_the_Calculation_of_Discount_Applied(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.verifyDiscountAppliedAmt();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_015_Verify_the_Availability_of_Payment_Integration_CheckBox_for_Corp_Super_User(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.globalSearchaSalesStore();
			salesStorePage.verifyPaymentIntegrationCheckbox();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC016_New Field present in the Incentive Page as Incentive
	 * Status with dropdown Values_Corporate Super User Autiomation Case:
	 * TC016_New_Field_Incentive_Page_as_IncentiveStatus_with_dropdown_CorpSuperUser
	 * Description: : Verify new Field is present in the Incentive Page as Incentive
	 * Status with dropdown Values as- 1. Promised Item 2. Pre-Gift 3.Distributed
	 * Incentive Test Data- Booked Tour should be available Designed By: Prattusha
	 * Dutta
	 */
	@Test(dataProvider = "testData")
	public void TC_016_New_Field_Incentive_Page_as_IncentiveStatus_with_dropdown_CorpSuperUser(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMobUI();
			homePage.navigateToSearchReservationOrTour();
			// homePage.searchReservationTour(testData.get("ReservationNumber"),
			// "Reservation");
			homePage.searchReservationTour(testData.get("TourNo"), "Tour");
			homePage.verifyAddIncentiveScreen();
			homePage.clickToAddPromisedItem();
			homePage.verifyIncentiveStatus();

			login.salesForceLogoutMarketer();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC017_New Field present in the Incentive Page as Incentive
	 * Status with dropdown Values_InHouse Marketer Autiomation Case:
	 * TC017_New_Field_Incentive_Page_as_IncentiveStatus_with_dropdown_InHouseMarketer
	 * Description: : Verify new Field is present in the Incentive Page as Incentive
	 * Status with dropdown Values as- 1. Promised Item 2. Pre-Gift 3.Distributed
	 * Incentive Pre-Requisite- Booked Tour should be available Test Data: A booked
	 * Tour Designed By: Prattusha Dutta
	 */
	@Test(dataProvider = "testData")
	public void TC_017_New_Field_Incentive_Page_as_IncentiveStatus_with_dropdown_InHouseMarketer(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			// homePage.searchReservationTour(testData.get("ReservationNumber"),
			// "Reservation");
			homePage.searchReservationTour(testData.get("TourNo"), "Tour");
			homePage.verifyAddIncentiveScreen();
			homePage.clickToAddPromisedItem();
			homePage.verifyIncentiveStatus();
			// arrivalPage.validateTravelChannel();
			login.salesForceLogoutMarketer();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:TC018_Marketer Cost field to be hidden on the line item level
	 * and only appear in the bottom blue tool bar in Mobile UI Automation Case:
	 * TC018_Marketer_Cost_field_hidden_appear_in_bottom_blue_toolbar_in_Mobile_UI
	 * Pre-Requisites: user should have a booked tour, and payment record Test data:
	 * A booked Tour with Incentive Added- USe TC_17 Tour
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_018_Marketer_Cost_field_hidden_appear_in_bottom_blue_toolbar_in_Mobile_UI(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			// homePage.searchReservationTour(testData.get("ReservationNumber"),"Reservation");
			homePage.searchReservationTour(testData.get("TourNo"), "Tour");
			homePage.verifyAddIncentiveScreen();

			homePage.verifyMarketerCost();
			// arrivalPage.validateTravelChannel();
			login.salesForceLogoutMarketer();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_019_Verify_Payment_Refund_functionality(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.clickonRefundButton();
			trPage.verifyRefundDetails();
			trPage.enterCloverDeviceNameinRefundPopUp();
			trPage.clickonRefndBtninRefundDtlsPopUp();
			trPage.clickonOfflineRefundButton();
			trPage.acceptRefundMessage();
			trPage.clickonPaymentContactDtlsCancelButton();
			// trPage.validateTransactionStatus();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_020_Verify_Payment_Refund_Receipt_Delivery_functionality(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.clickonRefundButton();
			trPage.verifyRefundDetails();
			trPage.enterCloverDeviceNameinRefundPopUp();
			trPage.clickonRefndBtninRefundDtlsPopUp();
			trPage.clickonOfflineRefundButton();
			trPage.acceptRefundMessage();
			trPage.clickonPaymentContactDtlsCancelButton();
			// trPage.validateTransactionStatus();
			// homePage.globalSearchTransactionId();
			trPage.clickonResendEmail_TextButton();
			trPage.validateRefundEmailfunationality();
			trPage.clickonResendEmail_TextButton();
			trPage.validateRefundTextfunationality();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_021_Verify_Refund_functionality_for_Payments_taken_through_Integration(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.clickonRefundButton();
			trPage.verifyRefundDetails();
			trPage.enterCloverDeviceNameinRefundPopUp();
			trPage.clickonRefndBtninRefundDtlsPopUp();
			trPage.clickonOfflineRefundButton();
			trPage.acceptRefundMessage();
			trPage.clickonPaymentContactDtlsCancelButton();
			// trPage.validateTransactionStatus();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_023_Verify_the_absence_of_Void_Button(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.globalSearchExistingTour();
			trPage.clickonPaymentsTab();
			trPage.clickonPaymentIdLink();
			trPage.validatetheAbsenceofVoidButton();
			// trPage.validatePresenceofPaymentsDetailsPage();

			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_025_Verify_Payment_Refund_through_Cash_functionality(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.clickonRefundButton();
			trPage.verifyRefundDetails();
			trPage.enterCloverDeviceNameinRefundPopUp();
			trPage.clickonRefndBtninRefundDtlsPopUp();
			trPage.clickonOfflineRefundButton();
			trPage.acceptRefundMessage();
			trPage.clickonPaymentContactDtlsCancelButton();
			// trPage.validateTransactionStatus();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:TC026_Cash refund ability is Restricted to User Roles
	 * Automation Case:TC026_Cash_refund_ability_Restricted_to_IH_Marketer
	 * Pre-Requisites: user should have a booked tour, and payment record Test data:
	 * A booked Tour with Payment Processed
	 * 
	 */

	@Test(dataProvider = "testData")
	public void TC_026_Cash_refund_ability_Restricted_to_IH_Marketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		ArrivalPage arrivalPage = new ArrivalPage(tcconfig);
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToSearchReservationOrTour();
			// homePage.searchReservationTour(testData.get("ReservationNumber"),"Reservation");
			homePage.searchReservationTour(testData.get("TourNo"), "Tour");

			homePage.clickOnPaymentRecordMob();
			homePage.verifyRefundBtn();

			login.salesForceLogoutMarketer();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_028_Verify_that_Payment_Details_can_be_searched_using_Guest_Name(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchGuestName();
			lead.clickonRelatedTab();
			lead.clickonTourId();
			trPage.clickonPaymentsTab();
			trPage.clickonPaymentIdLink();
			trPage.validatePresenceofPaymentsDetailsPage();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_029_Verify_that_Payment_Details_can_be_searched_using_Tour_Id(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();
			trPage.clickonPaymentsTab();
			trPage.clickonPaymentIdLink();
			trPage.validatePresenceofPaymentsDetailsPage();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_030_Verify_that_Payment_Details_can_be_searched_using_Transaction_Id(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.getTransactionId();
			homePage.globalSearchTransactionId();
			trPage.validatePresenceofPaymentsDetailsPage();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_031_Verify_Payment_Refund_Receipt_Print_functionality(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.clickonRefundButton();
			trPage.verifyRefundDetails();
			trPage.enterCloverDeviceNameinRefundPopUp();
			trPage.clickonRefndBtninRefundDtlsPopUp();
			trPage.clickonOfflineRefundButton();
			trPage.acceptRefundMessage();
			trPage.clickonPaymentContactDtlsCancelButton();
			// trPage.validateTransactionStatus();
			trPage.clickonPrintReceiptButton();
			trPage.verifyPDFgeneration();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_032_Verify_Payment_Refund_Log(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			trPage.clickonRefundButton();
			trPage.verifyRefundDetails();
			trPage.enterCloverDeviceNameinRefundPopUp();
			trPage.clickonRefndBtninRefundDtlsPopUp();
			trPage.clickonOfflineRefundButton();
			trPage.acceptRefundMessage();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTransactionId();
			trPage.validateTransactionStatus();
			trPage.validateRefundLog();
			trPage.clickonPrintReceiptButton();
			trPage.verifyPDFgeneration();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_033_Verify_the_Availability_of_Payment_Integration_CheckBox_for_SalesForce_Admin(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);

		try {

			login.launchApp("CHROME");
			login.loginToSalesForceDiffUsers();
			homePage.globalSearchaSalesStore();
			salesStorePage.verifyPaymentIntegrationCheckbox();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_034_Verify_the_Availability_of_Payment_Integration_CheckBox_for_Corp_Super_User(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.globalSearchaSalesStore();
			salesStorePage.verifyPaymentIntegrationCheckbox();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_035_Verify_that_Manual_Payment_is_not_allowed_for_CMA_User(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		SalesStorePage salesStorePage = new SalesStorePage(tcconfig);
		BookATourPage bookTourPage = new BookATourPage(tcconfig);
		LeadsPage leadPage = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			leadPage.validateCreateAddGuest();
			bookTourPage.createTourMobile();
			bookTourPage.addIncentivesandPayments();
			bookTourPage.tourConfirmation();
			bookTourPage.addCloverDevice();
			bookTourPage.ValidateSignature();
			bookTourPage.verifyTwoButtonsPresent();
			bookTourPage.paymentContactDetails();
			bookTourPage.validateAddTourNotes();
			bookTourPage.clickOnCreatedTour(LeadsPage.lead1);
			login.salesForceLogoutMarketer();

			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	@Test(dataProvider = "testData")
	public void TC_037_Verify_the_Notification_Message_where_Incentive_Amt_is_greater_than_Retail_Amt(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);

		try {

			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			login.salesForceLogout();
			login.clearallStaticVariable();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC038_User should be able to see historical record update
	 * activities for manual payment records Automation Case:
	 * TC038_User_should_see_historical_record_update_activities_manual_payment_records
	 * Pre-Requisites: user should have a booked tour, and payment record
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_038_User_should_see_historical_record_update_activities_manual_payment_records(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 * trPage.clickonPaymentsTab();
			 */
			trPage.clickToAddManualRecord();
			trPage.navigateToPaymentRecord();
			trPage.verifyIntRecordEditManualSales();
			trPage.verifyLastModified(); /******** Ritam **********************/
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC039_Integrated payment records are editable for all fields
	 * only for salesforce admin profile Automation Case:
	 * TC039_Integrated_payment_records_editable_all_fields_salesforce_admin_profile
	 * Pre-Requisites: user should have a booked tour, and payment record
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_039_Integrated_payment_records_editable_all_fields_salesforce_admin_profile(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */

			trPage.clickPaymentTab();
			trPage.verifyallFieldsEditSalesAdmin();
			trPage.verifyIntRecordEditCorpUser();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC040_Integrated payment records are non editable for all
	 * fields for corporate super user Automation Case:
	 * TC040_Integrated_payment_records_non_editable_all_fields_corporate_super_user
	 * Pre-Requisites: user should have a booked tour, and paymnent record
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_040_Integrated_payment_records_non_editable_all_fields_corporate_super_user(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */
			trPage.clickPaymentTab();
			trPage.verifyIntRecordNonEditCorpUser();
			trPage.verifyIntRecordEditCorpUser();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC041_Integrated payment records are editable for Phone,
	 * email, carrier fields on the payment record is editable for corporate user
	 * Automation Case:
	 * TC041_Integrated_payment_records_editable_Phone_Email_Carrier_fields_corporate_user
	 * Pre-Requisites: user should have a booked tour, and paymnet record
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_041_Integrated_payment_records_editable_Phone_Email_Carrier_fields_corporate_user(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */
			trPage.clickPaymentTab();

			trPage.verifyIntRecordEditCorpUser();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC042_Manual payment records are editable for all fields for
	 * all profiles Automation
	 * Case:TC042_Manual_payment_records_editable_all_fields_all_profiles
	 * Pre-Requisites: user should have a booked tour, and paymnet record
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_042_Manual_payment_records_editable_all_fields_all_profiles(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			// when Created Tour ID is used
			/*
			 * homePage.navigateToMenu("2"); lead.searchTour_global();
			 */
			trPage.clickToAddManualRecord();
			trPage.navigateToPaymentRecord();
			trPage.verifyallFieldsEditSalesAdmin();
			trPage.verifyIntRecordEditCorpUser();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC043_Verify the Transfer button_Salesforce Admin Automation
	 * Case:TC_043_Verify_the_Transfer_button_Salesforce_Admin Pre-Requisites: user
	 * should have a booked tour, and paymnet record And another Tour Number where
	 * it is willing to transfer paymnet
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_043_Verify_the_Transfer_button_Salesforce_Admin(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			/*
			 * //when Created Tour ID is used homePage.navigateToMenu("2");
			 * lead.searchTour_global();
			 */
			trPage.clickPaymentTab();
			trPage.verifyTransferBtn();
			trPage.clickTransferBtnPopup();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:TC044_User can clcik on Cancel/Exit to exit the Transfer
	 * Payment Process Automation
	 * Case:TC_044_User_clcik_on_Cancel_Exit_to_exit_Transfer_Payment_Process
	 * Pre-Requisites: user should have a booked tour, and integrated payment record
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_044_User_clcik_on_Cancel_Exit_to_exit_Transfer_Payment_Process(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			/*
			 * //when Created Tour ID is used homePage.navigateToMenu("2");
			 * lead.searchTour_global();
			 */
			trPage.clickPaymentTab();
			trPage.verifyTransferBtn();
			trPage.clickCancelBtnPopup();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC045_Source and Target Tour/Payment/Premium Records will all
	 * have a "Flag" that will be visible within the Standard and Mobile UI to allow
	 * for Users to quickly see that a Transfer has occurred Automation Case:
	 * TC045_Source_Target_Tour_Payment_PremiumRecords_Flag_that__visible_Standard_Mobile_UI
	 * Pre-Requisites: user should have a booked tour, and paymnet record
	 * 
	 */
	@Test(dataProvider = "testData")
	public void TC_045_Source_Target_Tour_Payment_PremiumRecords_Flag_that__visible_Standard_Mobile_UI(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		List<String> lstStrMktgFields = new ArrayList<String>();
		try {
			login.launchApp("strBrowserInUse");
			login.loginToSalesForceDiffUsers();
			homePage.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();

			trPage.clickonIncentiveTab();
			trPage.clickonCreatePaymentButton();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddIncentive();
			trPage.clickonAddIncentiveButton();
			trPage.searchandAddSecondIncentive();
			trPage.enterIncentiveAmountCollected();
			trPage.enterCloverDeviceName();
			trPage.selectPaymentMethod();
			trPage.clickonProcessPaymentButton();
			trPage.validateWarningMessage();
			trPage.clickonPaymentProceedButton();
			trPage.clickonPaymentContactDtlsCancelButton();
			homePage.globalSearchTourId();

			/*
			 * //when Created Tour ID is used homePage.navigateToMenu("2");
			 * lead.searchTour_global();
			 */
			trPage.clickPaymentTab();
			trPage.verifyTransferBtn();
			trPage.clickTransferBtnPopup();
			trPage.clickCancelBtnPopup();
			login.salesForceLogout();
			login.clearallStaticVariable();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

}
