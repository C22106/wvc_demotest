package salesforce.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class HelpPopup extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(HelpPopup.class);

	public HelpPopup(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String HelpSubject;

	@FindBy(xpath = "//button[@class='slds-button']")
	WebElement menuIcon;
	@FindBy(xpath = "//input[@class='slds-input input']")
	WebElement txtSearchBy;
	private By helpIcon = By.xpath("//a[contains(@class,'global-actions')]");
	private By JourneyHelp = By.xpath("//span[text()='Journey Help']");
	private By typeDD = By.xpath("//span[text()='Type']/../..//a[@class='select']");
	private By topicDD = By.xpath("//span[text()='Topic']/../..//a[@class='select']");
	private By subjectDD = By.xpath("//span[text()='Subject']/../..//input[@type='text']");
	private By topicDetailDD = By.xpath("//span[text()='Topic Detail']/../..//a[@class='select']");
	private By subDetails = By.xpath("//button[@title='Left align text']");
	private By subDetailtxt = By.xpath("//div[contains(@class,'ql-editor')]");
	private By saveBtn = By.xpath("//div[@class='bottomBarRight slds-col--bump-left']//span[text()='Save']");
	private By confMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	private By errorList = By.xpath("//ul[@class='errorsList']");
	private By journeyHelpMarketing = By.xpath("//button[@id='createCase']");
	private By typeDDMarketing = By.xpath("//input[@name='Type']");
	private By topicDDMarketing = By.xpath("//input[@name='Topic__c']");
	private By subjectDDMarketing = By.xpath("//input[@name='Subject']");
	private By topicDetailDDMarketing = By.xpath("//input[@name='Topic_Detail__c']");
	private By subDetailsMarketing = By.xpath("//button[@title='Left align text']");
	private By subDetailtxtMarketing = By
			.xpath("//div[@aria-label='Compose text' or @class='slds-rich-text-editor__textarea slds-grid']");
	private By saveBtnMarketing = By.xpath("//button[text()='Save']");

	public void validateHelpPopup() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, helpIcon, 120);

		tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Help Icon is Present");
		driver.findElement(helpIcon).click();
		driver.findElement(JourneyHelp).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(typeDD)) {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Type dropdown is Present");
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Type dropdown not is Present");
		}

		if (verifyObjectDisplayed(topicDD)) {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Topic dropdown is Present");
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Topic dropdown not is Present");
		}

		if (verifyObjectDisplayed(subjectDD)) {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Subject field is Present");
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Subject field not is Present");
		}

		if (verifyObjectDisplayed(topicDetailDD)) {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS,
					"Topic Detail dropdown is Present");
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS,
					"Topic Detail dropdown not is Present");
		}

	}

	public void fillUpHelpForm() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, helpIcon, 120);

		tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Help Icon is Present");
		driver.findElement(helpIcon).click();
		driver.findElement(JourneyHelp).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(typeDD).isEnabled()) {
			driver.findElement(typeDD).click();
			driver.findElement(By.xpath("//div[@class='select-options']//a[text()='" + testData.get("HelpType") + "']"))
					.click();
		}

		if (driver.findElement(topicDD).isEnabled()) {
			driver.findElement(topicDD).click();
			driver.findElement(
					By.xpath("//div[@class='select-options']//a[text()='" + testData.get("HelpTopic") + "']")).click();
		}

		if (driver.findElement(subjectDD).isEnabled()) {
			String random = getRandomString(3);
			HelpSubject = testData.get("HelpSubject") + random;
			driver.findElement(subjectDD).sendKeys(HelpSubject);
		}
		if (driver.findElement(topicDetailDD).isEnabled()) {
			driver.findElement(topicDetailDD).click();
			driver.findElement(
					By.xpath("//div[@class='select-options']//a[text()='" + testData.get("TopicDetail") + "']"))
					.click();
		}
		clickElementBy(subDetails);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(subDetailtxt, testData.get("SubDetails"));
		driver.findElement(saveBtn).click();
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, confMsg, 120);
		if (verifyObjectDisplayed(confMsg)) {
			String msgText = driver.findElement(confMsg).getText();
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, msgText);

		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case not created");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void navigateToHelpForm() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, helpIcon, 120);

		tcConfig.updateTestReporter("helpIcon", "navigateToHelpForm", Status.PASS, "Help Icon is Present");
		driver.findElement(helpIcon).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(journeyHelpMarketing).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	private By Helpmarketer = By.xpath("//button[@title='Help']");
	private By journeyHelpmarketer = By.xpath("//button[contains(text(),'Journey help')]");

	public void navigateToHelpFormmarketer() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, Helpmarketer, 120);

		tcConfig.updateTestReporter("helpIcon", "navigateToHelpForm", Status.PASS, "Help Icon is Present");
		driver.findElement(Helpmarketer).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, journeyHelpmarketer, 120);
		driver.findElement(journeyHelpmarketer).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void fillUpHelpFormSave() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (driver.findElement(typeDDMarketing).isEnabled()) {
			driver.findElement(typeDDMarketing).click();
			driver.findElement(By.xpath("//div//*[@title='" + testData.get("HelpType") + "']")).click();
		}

		if (driver.findElement(topicDDMarketing).isEnabled()) {
			driver.findElement(topicDDMarketing).click();
			driver.findElement(By.xpath("//div//*[@title='" + testData.get("HelpTopic") + "']")).click();
		}

		if (driver.findElement(subjectDDMarketing).isEnabled()) {
			String random = getRandomString(3);
			HelpSubject = testData.get("HelpSubject") + random;
			driver.findElement(subjectDDMarketing).sendKeys(HelpSubject);
			System.out.println("Here");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (driver.findElement(topicDetailDDMarketing).isEnabled()) {
			System.out.println("Here 2");
			driver.findElement(topicDetailDDMarketing).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> Topic = driver
					.findElements(By.xpath("//div//*[@title='" + testData.get("TopicDetail") + "']"));
//			driver.findElement(By.xpath("//div//*[@title='"+testData.get("TopicDetail")+"']")).click();
			Topic.get(Topic.size() - 1).click();
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(subDetailsMarketing);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(subDetailtxtMarketing, testData.get("SubDetails"));
		driver.findElement(saveBtnMarketing).click();
		waitUntilElementVisibleBy(driver, confMsg, 120);
		if (verifyObjectDisplayed(confMsg)) {

			String msgText = driver.findElement(confMsg).getText();
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, msgText);
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case not created");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void errorValidationHelp() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, helpIcon, 120);

		tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Help Icon is Present");
		driver.findElement(helpIcon).click();
		driver.findElement(JourneyHelp).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(typeDD).isEnabled()) {
			driver.findElement(typeDD).click();
			driver.findElement(By.xpath("//div[@class='select-options']//a[text()='" + testData.get("HelpType") + "']"))
					.click();
		}

		if (driver.findElement(topicDD).isEnabled()) {
			driver.findElement(topicDD).click();
			driver.findElement(
					By.xpath("//div[@class='select-options']//a[text()='" + testData.get("HelpTopic") + "']")).click();
		}

		if (driver.findElement(subjectDD).isEnabled()) {
			String random = getRandomString(3);
			HelpSubject = testData.get("HelpSubject") + random;
			driver.findElement(subjectDD).sendKeys(HelpSubject);
		}
		/*
		 * if(driver.findElement(topicDetailDD).isEnabled()) {
		 * driver.findElement(topicDetailDD).click();
		 * driver.findElement(By.xpath("//div[@class='select-options']//a[text()='"+
		 * testData.get("TopicDetail")+"']")).click(); }
		 */
		clickElementBy(subDetails);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(subDetailtxt, testData.get("SubDetails"));
		driver.findElement(saveBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, errorList, 120);
		if (verifyObjectDisplayed(errorList)) {

			String msgText = driver.findElement(errorList).getText();
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, msgText);
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case created");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

}
