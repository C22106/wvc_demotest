package salesforce.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OwnersPage extends SalesforceBasePage {

	String WAAMIndicatorOwner;
	String WAAMINdicatorReservation;

	public static final Logger log = Logger.getLogger(HomePage.class);

	public OwnersPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	public static String createdTourID;

	public static String Owner;
	protected By txtQualification = By.xpath(
			"//span[contains(.,'Qualification Status')]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By imgHome = By.xpath("//img[contains(@src,'Home')]");
	protected By lblOwner = By.xpath("//tbody//td[contains(@data-label,'Guest Name')]//a");
	protected By txtCustomerInfo = By.xpath("//h2[contains(.,'Customer Information')]");
	protected By searchLead = By.xpath("//input[@placeholder='Search Name, Member Number, Email, City, Zip...']");
	protected By lst_Lead = By.xpath("//tbody//td[contains(@data-label,'Guest Name')]//a");
	protected By btnBookTourMobile = By.xpath("//button[contains(.,'BOOK TOUR')]");
	protected By hardStop_Mobile = By
			.xpath("//div[@class='slds-modal__content slds-p-around_medium slds-text-align_left']");
	protected By btnCancel_Mobile = By.xpath("//button[contains(.,'CANCEL')]");
	protected By btnConfirmTour = By.xpath("//button[contains(.,'CREATE TOUR')]");
	protected By softScoreMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtQuaNative = By
			.xpath("//span[contains(.,'Qualification Status')]/../..//div[2]//lightning-formatted-text");
	protected By txtOwner = By.xpath("//div[@class='entityNameTitle slds-line-height--reset' and contains(.,'Owner')]");
	protected By txtReservationOwner_Native = By.xpath("//span[contains(@title,'Reservations (Guest Name (Owner))')]");
	protected By tbl_Reserv = By.xpath("//tbody//tr//th//a[@data-aura-class='forceOutputLookup']");
	protected By txtOwnerReserv = By.xpath("//h1[@title='Reservations (Guest Name (Owner))']");
	protected By bntCancel = By
			.xpath("//button[contains(@class,'slds-button slds-button--neutral cuf-publisherCancelButton uiButton')]");
	protected By btnSave = By.xpath(
			"//button[contains(@class,'slds-button slds-button_brand cuf-publisherShareButton ')]//span[contains(.,'Save')]");
	protected By txtCreateTourRecord = By.xpath("//h2[contains(.,'Create New Tour Record')]");
	protected By btnIncentiveEligible = By.xpath("//button[contains(.,'Incentive Eligible')]");
	protected By errorMsg = By.xpath("//ul[@class='errorsList']");
	protected By globalSearchEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'default')]");
	protected By selectLeadItem = By.xpath("(//span[@class='uiImage']/img[contains(@class,'icon')])[1]");
	protected By qs = By.xpath(
			"//div[contains(.,'Score Band') and @class='slds-form-element__label']/../..//span[@class='content-text uiOutputText']");
	protected By btnincentive = By.xpath("//button[@id='tourCheckBTN' and contains(.,'Incentive Eligible')]");
	protected By softscore = By
			.xpath("//button[contains(.,'Request Soft Score') and contains(@class,'slds-button slds-button_brand')]");
	protected By btnCreateTourRecord = By.xpath("(//button[text()='Create New Tour Record'])[2]");
	private By ownerSearchBoxEdit = By.xpath("//input[contains(@name,'search-input')]");
	private By createNewTourLink = By.xpath("//button[contains(text(),'Create New Tour Record')]");

	private By popUpSaveBtn1 = By.xpath("//div[contains(@class,'modal')]/button/span[text()='Save']");
	// private By popUpSaveBtn =
	// By.xpath("//div[contains(@class,'modal')]/button/span[text()='Save'] |
	// //div[contains(@class,'modal')]//button[text()='Save']");
	private By popUpSaveBtn = By.xpath("//button[text()='Save']");
	private By tourlanuageDrpDwn = By.xpath("//span[text()='Tour Language']/../..//a");
	private By tourLangValue = By.xpath("//div/a[text()='English']");
	private By createNewTour = By.xpath("//div[@class='slds-truncate'and contains(.,'Create New Tour Record')]");
	private By commentsEdit = By.xpath("//span[text()='Comments']/../..//input");
	private By qualificationStatus = By
			.xpath("//span[contains(text(),'Qualification')]/parent::span/following-sibling::div//a");
	private By qualificationSave = By.xpath("//div[@class='modal-footer slds-modal__footer']//span[text()='Save']");
	private By clubMembershipLink = By.xpath("(//span[contains(text(),'Club Membership Account')])[1]");
	private By editMembership = By.xpath("//button[@title='Edit Club Membership']");
	private By editMembershipDD = By.xpath("//span[text()='Club Membership']/../..//div[@class='uiMenu']");
	private By searchOwner = By.xpath("//input[@placeholder='Search Owners and more...']");
	private By selOwner = By.xpath("//a[contains(@class,'forceOutputLookup')]");
	private By dNG = By.xpath("//img[@class=' checked']");
	private By reservSelect = By.xpath("//a[contains(@class,'forceOutputLookup')]");
	private By verifyAU = By.xpath("//span[contains(text(),'U-DNG')]");
	private By globalsrch = By.xpath("//input[contains(@title,'Search')]");
	private By forteendaysErrormessageNotour = By.xpath("//span[contains(text(),'Error!!..')]");
	private By forteendaysErrormessage = By.xpath("//input[contains(@class, 'slds-input i')]");
	private By dngtourmsg = By.xpath("//span[contains(text(),'This Guest is not eligible to Tour.')]");
	private By deleteBtn = By.xpath("//a[@title='Delete']");
	private By deleteTourBtn = By.xpath("//span[text()='Delete']");
	private By printButton = By.xpath("//button[text()='Print']");
	private By AddAdditinalguest = By.xpath("//button[text()='Add Additional Guest']");
	private By streetAddressPrimary = By.xpath(
			"//span[text()='Street Address']/parent::div/following-sibling::div/span/slot/slot/lightning-formatted-text");
	private By guestAddressField = By.xpath("//label[text()='Street']/following-sibling::div/input");
	private By streetAddPrimaryInhouse = By
			.xpath("//span[text()='Street Address']/parent::div/following-sibling::div/span/span");
	private By btnCancel = By.xpath("//div[@class='slds-modal__container']//button");
	private By cases1 = By.xpath(
			"//img[@alt='Cases']/parent::span/parent::div/parent::div/parent::header/parent::div/following-sibling::div/div/div/div/ul/li[1]/div[2]/h3/div/a");
	private By statusEdit = By
			.xpath("(//span[text()='Status'])[2]/parent::div/following-sibling::div/button/lightning-primitive-icon");
	private By statusDropDown = By
			.xpath("(//span[text()='Status'])[3]/parent::span/following-sibling::div/div/div[1]/div/a");
	private By onhldsEdit = By.xpath(
			"(//span[text()='On Hold Notes'])[1]/parent::div/following-sibling::div/button/lightning-primitive-icon");
	private By onholdsText = By.xpath("(//span[text()='On Hold Notes'])[2]/parent::div/div/div[2]/div[1]");
	private By saveCase = By.xpath("//span[text()='Save']");
	private By onHoldValue = By.xpath("//a[text()='On Hold']");
	private By OnhldStatus = By.xpath("(//span[text()='Status'])[2]/parent::div/following-sibling::div/span/span");
	private By viewProfile = By.xpath("//img[@title='User']");

	@FindBy(xpath = "//h1[@class = 'profile-card-name']//a[@class = 'profile-link-label']")
	WebElement marketingagentname;
	// private By createNewTourLink = By.xpath("//div[@title='Create New Tour
	// Record']");

	private By marketingagent = By.xpath("//span[text()='Marketing Agent']/../..//span[@class = 'pillText']");

	@FindBy(xpath = "//div[contains(@class,'modal-container')]//span[text() = 'Qualification Status']/../..//span[@class = 'uiOutputText']")
	WebElement qualificationsts;

	public static List<String> lstOwners = new ArrayList<String>();

	// private By noOfRowsInTourRecord=By.xpath("(//table)[2]/tbody/tr/th");

	public void validateOwnerDNG() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		// waitUntilElementVisibleBy(driver, searchOwner, 120);

		fieldDataEnter(searchOwner, testData.get("LeadName"));
		driver.findElement(searchOwner).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		List<WebElement> Name = driver.findElements(selOwner);

		Name.get(Name.size() - 2).click();

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,700)", "");

		if (driver.findElement(dNG).isDisplayed()) {
			tcConfig.updateTestReporter("ReservationPage", "validateDate", Status.PASS, "DNG is checked");
		} else {
			tcConfig.updateTestReporter("ReservationPage", "validateDate", Status.FAIL, "DNG is unchecked");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> reserv = driver.findElements(reservSelect);

		reserv.get(reserv.size() - 2).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> verify = driver.findElements(verifyAU);

		String AU = verify.get(verify.size() - 2).getText();
		if (AU.contains("" + testData.get("A/U") + "")) {
			tcConfig.updateTestReporter("OwnersPage", "validateOwnerDNG", Status.PASS, "A/U is U-DNG:" + AU);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateOwnerDNG", Status.FAIL, "Failed while validating A/U");
		}

	}

	public void selectOwnerCreateTour() {

		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.PASS,
					"User is navigated to Owners Page");

			fieldDataEnter(ownerSearchBoxEdit, testData.get("OwnerName"));
			driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement seacrhResOwnerLink = driver
					.findElement(By.xpath("//th/span/a[contains(text(),'" + testData.get("OwnerName") + "')]"));
			seacrhResOwnerLink.click();
			waitUntilElementVisibleBy(driver, createNewTourLink, 120);
			clickElementBy(createNewTourLink);

			waitUntilElementVisibleBy(driver, tourlanuageDrpDwn, 120);
			clickElementBy(tourlanuageDrpDwn);
			clickElementBy(tourLangValue);
			waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);
			clickElementBy(popUpSaveBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	/*
	 * public void CreateTourforOwner() {
	 * 
	 * waitUntilElementVisibleBy(driver, globalsrch, 120); if
	 * (verifyObjectDisplayed(globalsrch)) { //
	 * tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour",
	 * Status.PASS, // "User is navigated to Owners Page");
	 * 
	 * fieldDataEnter(globalsrch, testData.get("OwnerName")); //
	 * driver.findElement(globalsrch).sendKeys(Keys.ENTER);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * if(driver.findElement(By.xpath(
	 * "(//div//div[contains(text(),'Owner')  and @dir='ltr'])[1]"
	 * )).getText().contains("Owner")) { WebElement seacrhResOwnerLink = driver
	 * .findElement(By.xpath("(//a//div//span[@title='" + testData.get("OwnerName")
	 * + "'])[1]")); seacrhResOwnerLink.click(); waitUntilElementVisibleBy(driver,
	 * createNewOwnerLink, 120); clickElementBy(createNewOwnerLink);
	 * 
	 * waitUntilElementVisibleBy(driver, tourlanuageDrpDwn, 120);
	 * clickElementBy(tourlanuageDrpDwn); clickElementBy(tourLangValue);
	 * waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);
	 * clickElementBy(popUpSaveBtn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); }else{
	 * tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour",
	 * Status.FAIL, "No Search Result found"); }
	 * 
	 * } else { tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour",
	 * Status.FAIL, "User is unable to navigate to Owners Page"); }
	 * 
	 * }
	 */
	private By tourlanguage = By.xpath("//span[contains(text(),'Language')]/parent::span/following-sibling::div//a");

	public void CreateTourforOwner() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			fieldDataEnter(globalsrch, testData.get("OwnerName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div[text() = 'Owner' and contains(@class,'uiOutputRichText')]"))
					.getText().contains("Owner")) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement seacrhResOwnerLink = driver.findElement(
						By.xpath("//div[text() = 'Owner' and contains(@class,'uiOutputRichText')]/../..//span[@title='"
								+ testData.get("OwnerName") + "']"));
				clickElementJS(seacrhResOwnerLink);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, createNewTourLink, 120);
				if (verifyElementDisplayed(driver.findElement(createNewTourLink))) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					clickElementJSWithWait(createNewTourLink);
					waitUntilElementVisibleBy(driver, marketingagent, 120);
					if (verifyObjectDisplayed(marketingagent)) {
						String maragntname = driver.findElement(marketingagent).getText().trim();
						System.out.println(maragntname);
						tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.PASS,
								"Marketing agent Field present");

					} else {
						tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
								"Marketing agent Field not present");
					}
					if (verifyElementDisplayed(qualificationsts)) {
						System.out.println(qualificationsts.getText());
						tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.PASS,
								"Qualification Status is: " + qualificationsts.getText());
					} else {
						tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
								"Qualification status field not present");
					}
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisibleBy(driver, tourlanuageDrpDwn, 120);
					clickElementBy(tourlanguage);
					WebElement tourlanguageSelect = driver
							.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tourlanguageSelect.click();
					waitUntilElementVisibleBy(driver, qualificationSave, 120);
					clickElementJSWithWait(qualificationSave);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
							"unable to select owner from list! Please check data");
				}
			} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	private By bookTourBtn = By.xpath("//div//button[text()='Book Tour']");
	private By languageDrpdwn = By
			.xpath("//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select']");
	// private By
	// languageDrpdwn=By.xpath("//label[text()='Langauge']/..//select[contains(@class,'slds-select
	// select')]");
	private By latestTour = By.xpath("//span[text()='Tour Record']//div");
	private By latestTour_new = By.xpath("//span[text()='Tour Record']//a//div");

	public void CreateTourforReservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				String Message = driver.findElement(By.xpath("//input[@disabled]")).getAttribute("value");
				System.out.println(Message);

				String[] msg = Message.split("Owner");
				System.out.println(msg[0]);
				if (msg[0].contains("courtesy")) {

					tcConfig.updateTestReporter("LeadPage", "Create Tour", Status.PASS,
							"The Tour Message is :" + msg[0]);
				}

				/*
				 * clickElementBy(tourlanuageDrpDwn); clickElementBy(tourLangValue);
				 * clickElementBy(popUpSaveBtn);
				 */
				Select s = new Select(driver.findElement(languageDrpdwn));
				s.selectByVisibleText("English");

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// clickElementBy(popUpSaveBtn);
				driver.findElement(By.xpath("//footer[contains(@class,'slds-modal')]/button[text()='Save']")).click();

				waitUntilElementVisibleBy(driver, latestTour, 120);
				if (verifyObjectDisplayed(latestTour)) {
					createdTourID = driver.findElement(latestTour).getText();
					System.out.println("created TourID: " + createdTourID);
					tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS,
							"created TourID: " + createdTourID);
					driver.findElement(latestTour).click();
				} else {
					tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	public void printVerificationNegative() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (isElementPresent(By.xpath("//button[text()='Print')]"))) {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.FAIL,
					"Print button is present on Owners Page");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS,
					"Print button is not present on Owners Page");
		}

	}

	public void searchOwner() {

		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS, "User is navigated to Owners Page");
		}
		fieldDataEnter(ownerSearchBoxEdit, testData.get("OwnerName"));
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement seacrhResOwnerLink = driver
				.findElement(By.xpath("//th/span/a[contains(text(),'" + testData.get("OwnerName") + "')]"));
		seacrhResOwnerLink.click();
	}

	// Display All Owners
	private By OwnersDropDown = By.xpath(
			"//span[@class='slds-truncate' and text()='Owners']//parent::a//following-sibling::one-app-nav-bar-item-dropdown/div/one-app-nav-bar-menu-button/a");
	private By AllOwnersOption = By.xpath("//span[@class='slds-truncate']//child::span[text()='All Owners']");
	private By AllOwnersHeader = By
			.xpath("//span[@class='triggerLinkText selectedListView uiOutputText' and text()='All Owners']");

	public void searchAllOwner() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS, "User is navigated to Owners Page");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(OwnersDropDown).click();
		waitUntilElementVisibleBy(driver, AllOwnersOption, 120);
		clickElementJS(AllOwnersOption);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleBy(driver, AllOwnersHeader, 120);
		if (verifyObjectDisplayed(AllOwnersHeader)) {
			tcConfig.updateTestReporter("OwnersPage", "searchAllOwner", Status.PASS,
					"User is navigated to All Owners section");
		}

	}

	// Select the owner eligible for creating tour
	private By allOwners = By.xpath("//table//tbody//th//a");
	private By TourRecordsCount = By.xpath(
			"//span[@class='slds-card__header-title slds-truncate slds-m-right--xx-small' and text()='Tour Records']//following-sibling::span");
	private By status = By.xpath("//table[@role='grid']/tbody/tr[1]/td[5]/descendant::span[@class='slds-truncate']");
	private By DispositionStatus = By
			.xpath("//table[@role='grid']/tbody/tr[1]/td[4]/descendant::span[@class='slds-truncate']");

	public String fetchAllOwners() {
		int i;
		List<WebElement> ownerList = driver.findElements(allOwners);

		for (i = 0; i < ownerList.size(); i++) {
			try {

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(ownerList.get(i));
			} catch (StaleElementReferenceException e) {

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				e.toString();
				System.out.println(e.getMessage());

				ownerList = driver.findElements(By.xpath("//table//tbody//th//a"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				System.out.println("Exception caught trying once more");
				clickElementJS(ownerList.get(i));
				System.out.println(ownerList.get(i).getAttribute("title"));
			}

			// scrolling down for more owners displayed in the page
			if (i >= 22) {
				scrollDownByPixel(600);
			}

			// Checking for eligible owner
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> TourCount = driver.findElements(TourRecordsCount);
			// System.out.println(TourCount.size());
			WebElement TourRecord = TourCount.get(TourCount.size() - 1);
			waitUntilElementVisibleWb(driver, TourRecord, 120);

			if (verifyElementDisplayed(TourRecord)) {

				tcConfig.updateTestReporter("OwnerPage", "fetchAllOwners", Status.PASS,
						"User is navigated to Owner details section");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				String TourNumber = TourRecord.getAttribute("title");
				TourNumber = TourNumber.replaceAll("[^0-9]", " ").trim();
				System.out.println("Tour Count:" + TourNumber);

				if (TourNumber.equals("0")) {
					System.out.println("No tours booked found for this owner");
					break;
				} else {

					clickElementJS(TourRecord);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (verifyElementDisplayed(driver.findElement(status))) {
						tcConfig.updateTestReporter("Tour Records Details", "fetchAllOwners", Status.PASS,
								"User is navigated to Tour Records details section");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

					if (driver.findElement(status).getText().equalsIgnoreCase("Dispositioned")
							& driver.findElement(DispositionStatus).getText().equalsIgnoreCase("Did Not Purchase")) {
						System.out.println("Disposition Status is Did Not Purchase");
						break;
					} else if (driver.findElement(status).getText().equalsIgnoreCase("Draft")) {
						System.out.println("Status is Draft");
						break;
					} else {
						System.out.println("Moving back to Owners list");
						searchAllOwner();
						System.out.println("Checking for next the Owner");
					}

				}

			}
		}
		String OwnerName = ownerList.get(i).getAttribute("title");
		System.out.println(OwnerName);
		return OwnerName;

	}

	public void globalsearchOwner() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS, "User is navigated to Owners Page");
		}
		fieldDataEnter(globalsrch, testData.get("OwnerName"));
		// driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(By.xpath("//div//div[contains(text(),'Owner')  and @dir='ltr']")).getText()
				.contains("Owner")) {
			WebElement seacrhResOwnerLink = driver
					.findElement(By.xpath("//a//div//span[@title='" + testData.get("OwnerName") + "']"));
			seacrhResOwnerLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL, "No Search Result found");
		}

	}

	public void searchOwnerByMemberNumber() {

		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS, "User is navigated to Owners Page");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(ownerSearchBoxEdit, testData.get("MmbershipNumber"));
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement seacrhResOwnerLink = driver
				.findElement(By.xpath("//th/span/a[contains(text(),'" + testData.get("OwnerName") + "')]"));
		seacrhResOwnerLink.click();
	}

	public void globalSarchOwnerByMemberNumber() {

		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS, "User is navigated to Owners Page");
		}
		fieldDataEnter(ownerSearchBoxEdit, testData.get("MmbershipNumber"));
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement seacrhResOwnerLink = driver
				.findElement(By.xpath("//th/span/a[contains(text(),'" + testData.get("OwnerName") + "')]"));
		seacrhResOwnerLink.click();
	}

	private By MembershipField = By
			.xpath("(//div[contains(@class,'label')]//span[contains(text(),'Club Membership')]//..)[1]");

	public void verifyBrandsMembershipDD() throws AWTException {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		Actions actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN);

		if (verifyObjectDisplayed(MembershipField)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			actions.doubleClick(driver.findElement(MembershipField)).perform();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS,
					"editMembership is present on Owners Page");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "searchOwner", Status.PASS,
					"editMembership is not present on Owners Page");
		}

		waitUntilElementVisibleBy(driver, editMembershipDD, 120);
		clickElementBy(editMembershipDD);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String Membership1 = driver.findElement(By.xpath("//a[@title='Club Wyndham']")).getText().trim();
		Assert.assertEquals(Membership1, "Club Wyndham");

		String Membership2 = driver.findElement(By.xpath("//a[@title='WorldMark']")).getText().trim();
		Assert.assertEquals(Membership2, "WorldMark");
		String Membership3 = driver.findElement(By.xpath("//a[@title='Shell']")).getText().trim();
		Assert.assertEquals(Membership3, "Shell");
		String Membership4 = driver.findElement(By.xpath("//a[@title='Multiple Clubs']")).getText().trim();
		Assert.assertEquals(Membership4, "Multiple Clubs");

	}

	public void validateOwnerDetails() {
		waitUntilElementVisibleBy(driver, clubMembershipLink, 120);
		if (verifyObjectDisplayed(clubMembershipLink)) {
			tcConfig.updateTestReporter("OwnersPage", "validateOwnerDetails", Status.PASS,
					"Club membership link is displayed properly");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateOwnerDetails", Status.FAIL,
					"Club membership link " + "is NOT displayed");
		}
	}

	public void createTourFromOwner() {
		waitUntilElementVisibleBy(driver, createNewTour, 120);
		clickElementBy(createNewTour);
		/*
		 * waitUntilElementVisibleBy(driver, qualificationStatus, 120); if
		 * (driver.findElement(commentsEdit).getAttribute("value").contains(
		 * "not eligible to Tour")) { tcConfig.updateTestReporter("OwnersPage",
		 * "createTourFromLead", Status.PASS,
		 * "Error Message is displayed during creation of Tours"); } else {
		 * tcConfig.updateTestReporter("OwnersPage", "createTourFromLead", Status.FAIL,
		 * "Error Message is NOT displayed during creation of Tours"); }
		 * clickElementBy(qualificationStatus);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(By.xpath("//a[@title='" + testData.get("QualificationScore") +
		 * "']"));
		 */
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(qualificationSave);
		waitForSometime(tcConfig.getConfig().get("LongWait"));

	}

	private By GlobalSrch = By.xpath("//input[@title ='Search Owners and more']");
	private By activecheck = By.xpath("//span[text()='Active']/../..//span/img");
	private By selectOwner = By.xpath("");

	public void validateOwner() {
		String accNo = testData.get("MemberAccNo");
		waitUntilElementVisibleBy(driver, GlobalSrch, 120);
		driver.findElement(GlobalSrch).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(GlobalSrch).sendKeys(accNo);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(GlobalSrch).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		// clickElementBy(By.xpath("(//a[@title='"+accNo+"'])"));
		List<WebElement> Acct = driver.findElements(By.xpath("(//a[@title='" + accNo + "'])"));
		Acct.get(Acct.size() - 1).click();

		waitUntilElementVisibleBy(driver, activecheck, 120);
		if (driver.findElement(activecheck).isSelected() == true) {
			System.out.println("Selected");
			tcConfig.updateTestReporter("OwnersPage", "validateOwner", Status.PASS, "Member is Active");
		} else {
			System.out.println("Not Selected");
			tcConfig.updateTestReporter("OwnersPage", "validateOwner", Status.PASS, "Member is Inactive");
		}

	}

	private By memberSrch = By.xpath("//input[@placeholder='Search Salesforce']");

	private By memberNumber = By.xpath("//div/div/table/tbody/tr[1]/td[6]/span");

	public void validatememberNumber() {

		String accNo = testData.get("MemberAccNo");
		waitUntilElementVisibleBy(driver, memberSrch, 120);
		driver.findElement(memberSrch).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(memberSrch).sendKeys(accNo);
		driver.findElement(memberSrch).sendKeys(Keys.ENTER);

		if (accNo.equalsIgnoreCase(driver.findElement(memberNumber).getText())) {
			tcConfig.updateTestReporter("OwnersPage", "memberNumber", Status.PASS, "Member is Present");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "memberNumber", Status.PASS, "Member is Not Present");
		}

	}

	private By club_membership = By.xpath(
			"//span[@title='Club Membership']/..//div/span | //span[text()='Club Membership']/../../div[2]//lightning-formatted-text");

	public void verifyBrand() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String membership = testData.get("Brand");
		waitUntilElementVisibleBy(driver, club_membership, 120);
		String screen_member = driver.findElement(club_membership).getText();
		if (screen_member.equalsIgnoreCase(membership)) {
			tcConfig.updateTestReporter("OwnersPage", "verifyBrand", Status.PASS,
					"Club membership is validated and Club Membership is:" + screen_member);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "verifyBrand", Status.FAIL,
					"Failed while validating club membership");
		}
	}

	private By WAAMIndicatorOwnerPage = By
			.xpath("//span[text()='WAAM Indicator']//..//..//span[contains(@class,'test-id__field-value')]");

	public void WAAMIndicatorOwner() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		if (verifyObjectDisplayed(WAAMIndicatorOwnerPage)) {
			WAAMIndicatorOwner = driver.findElement(WAAMIndicatorOwnerPage).getText().trim();
			tcConfig.updateTestReporter("OwnersPage", "WAAMIndicatorOwner", Status.PASS,
					"WAAM Indicator is:" + WAAMIndicatorOwner);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateOwnerDetails", Status.FAIL,
					"WAAM Indicator not displayed");
		}
	}

	private By selres_owner = By.xpath(
			"(//span[text()='Reservations (Guest Name (Owner))'])[1]/ancestor::article[@data-aura-class='forceRelatedListCardDesktop']//a[@data-refid='recordId']");

	public void selresowner() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> numofres = driver.findElements(selres_owner);
		if (numofres.size() > 0) {
			numofres.get(0).click();
			tcConfig.updateTestReporter("OwnersPage", "selresowner", Status.PASS, "Resrvation selected successfully");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "selresowner", Status.FAIL, "Unable to click on reservation");
		}
	}

	private By WAAMIndicatorResPage = By
			.xpath("(//span[text()='WAAM Indicator'])[2]//..//..//span[@data-aura-class='uiOutputTextArea']");

	public void WAAMIndicatorRes() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(WAAMIndicatorResPage)) {
			WAAMINdicatorReservation = driver.findElement(WAAMIndicatorResPage).getText().trim();
			tcConfig.updateTestReporter("OwnersPage", "WAAMIndicatorOwner", Status.PASS,
					"WAAM Indicator is:" + WAAMINdicatorReservation);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateOwnerDetails", Status.FAIL,
					"WAAM Indicator not displayed");
		}
	}

	public void validateWAAMIndicator() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (WAAMINdicatorReservation.equals(WAAMIndicatorOwner)) {
			tcConfig.updateTestReporter("OwnersPage", "WAAMIndicatorOwner", Status.PASS,
					"WAAM Indicator Validated and WAAM Indicator is:" + WAAMINdicatorReservation);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateOwnerDetails", Status.FAIL,
					"WAAM Indicator validation failed");
		}
	}

	private By marketingagent1 = By
			.xpath("//label[text()='Marketing Agent']/..//div/span/a[@title='WVO Corporate Super User']");
	private By qulifications = By
			.xpath("//label[contains(text(),'ACS Qualification')]/..//div/input[@name='acsScore']");

	public void CreateTour_Reservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				WebElement Message = driver.findElement(By.xpath("//label[text()='Comments']/../div/input"));
				if (verifyElementDisplayed(Message)) {
					String Messagee = driver.findElement(By.xpath("//label[text()='Comments']/../div/input"))
							.getAttribute("value");
					System.out.println(Messagee);
					tcConfig.updateTestReporter("OwnersPage", "messagevarify", Status.PASS,
							"Message name: " + Messagee);
				} else {
					tcConfig.updateTestReporter("OwnersPage", "messagevarify", Status.FAIL, "messages: ");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement marketagent = driver.findElement(marketingagent1);
				if (verifyElementDisplayed(marketagent)) {
					String maragntname = marketagent.getText().trim();
					System.out.println(maragntname);
					if (maragntname.contains("WVO Corporate ")) {
						tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.PASS,
								"Marketing agent name is: " + maragntname);
					} else {
						tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
								"Failed while validating marketing agent name!");
					}
				} else {
					tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
							"Marketing agent Field not present");
				}
				/*
				 * WebElement qualification=driver.findElement(qulifications); if
				 * (verifyElementDisplayed(qualification)) { if
				 * (qualification.getText().contains("Q")) {
				 * System.out.println(qualification.getText());
				 * tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour",
				 * Status.PASS, "Qualification Status is: "+ qualificationsts.getText()); }else{
				 * tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour",
				 * Status.FAIL, "Failed while validating Status"); } }else{
				 * tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour",
				 * Status.FAIL, "Qualification status field not present"); }
				 */
				/*
				 * waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);
				 * clickElementBy(popUpSaveBtn);
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 */

				/*
				 * if(Message.equalsIgnoreCase(testData.get( "verifyDuplicateLead"))) {
				 * tcConfig.updateTestReporter("OwnerPage",
				 * "Verify COurtesy tour for DNG Owner with warning message",
				 * Status.PASS,"created TourID : " + createdTourID); } else {
				 * tcConfig.updateTestReporter("OwnerPage",
				 * "Verify COurtesy tour for DNG Owner with warning message",
				 * Status.FAIL,"Not Created"); }
				 */

				waitUntilElementVisibleBy(driver, languageDrpdwn, 120);

				Select s = new Select(driver.findElement(languageDrpdwn));
				s.selectByVisibleText(testData.get("tourlanguage"));

				waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				WebElement tour = driver.findElement(popUpSaveBtn);
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", tour);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				// clickElementBy(popUpSaveBtn);
				// driver.findElement(By.xpath("//footer[contains(@class,'slds-modal')]/button[text()='Save']")).click();

				/*
				 * waitUntilElementVisibleBy(driver, latestTour, 120); if
				 * (verifyObjectDisplayed(latestTour)) { createdTourID =
				 * driver.findElement(latestTour).getText(); System.out.println(
				 * "created TourID: " + createdTourID); tcConfig.updateTestReporter("LeadPage",
				 * "getlatestTour", Status.PASS,"created TourID: " + createdTourID);
				 * driver.findElement(latestTour).click(); } else {
				 * tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL,
				 * "Tour not created"); } waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 */} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	public void tour_Create_Reservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				WebElement tour = driver.findElement(popUpSaveBtn);
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", tour);

			} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	/**
	 * c24125
	 **/
	public void validate_NoTourErrorMessae_UsafeHouse_OwnerReservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String Message = driver.findElement(By.xpath("(//input[@disabled])[1]")).getAttribute("value");
				System.out.println(Message);
				waitUntilElementVisibleBy(driver, forteendaysErrormessage, 120);
				if (driver.findElement(forteendaysErrormessage).isDisplayed()) {
					tcConfig.updateTestReporter("OwnersPage", "validate_NoTourErrorMessae_UsafeHouse_OwnerReservation",
							Status.PASS, "Comments Updated : Error messsage Found::" + Message);

				}

			} else {
				tcConfig.updateTestReporter("OwnersPage", "validate_NoTourErrorMessae_UsafeHouse_OwnerReservation",
						Status.FAIL, "Comments not updated : No Error message found");
			}

			waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

			WebElement tour = driver.findElement(popUpSaveBtn);
			JavascriptExecutor executor1 = (JavascriptExecutor) driver;
			executor1.executeScript("arguments[0].click();", tour);
		}

	}

	public void validate_NoTourErrorMessae_Reservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				String Message = driver.findElement(By.xpath("//label[text()='Comments']/../div/input"))
						.getAttribute("value");
				System.out.println(Message);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(popUpSaveBtn)) {
					clickElementJSWithWait(popUpSaveBtn);
				}

				waitUntilElementVisibleBy(driver, forteendaysErrormessageNotour, 120);
				if (driver.findElement(forteendaysErrormessageNotour).isDisplayed()) {
					tcConfig.updateTestReporter("OwnersPage", "Owneris not able to create tour in last 14 days",
							Status.PASS, "Comments Updated : Error messsage Found::"
									+ driver.findElement(forteendaysErrormessageNotour).getText());

				}

			} else {
				tcConfig.updateTestReporter("OwnersPage", "Owner is not able to create tour in last 14 days",
						Status.FAIL, "Comments not updated : No Error message found");
			}

			waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

			WebElement tour = driver.findElement(popUpSaveBtn);
			JavascriptExecutor executor1 = (JavascriptExecutor) driver;
			executor1.executeScript("arguments[0].click();", tour);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			// clickElementBy(popUpSaveBtn);
			// driver.findElement(By.xpath("//footer[contains(@class,'slds-modal')]/button[text()='Save']")).click();

		}

	}

	/**
	 * c24125 validate comments message and tour for DNG owner Testcase covered-
	 * TC_83 and TC_84
	 */

	public void CreateAndValidateTourforDNGOwner() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("OwnerName"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("(//div//div[contains(text(),'Owner')  and @dir='ltr'])[1]")).getText()
					.contains("Owner")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("(//a//div//span[@title='" + testData.get("OwnerName") + "'])[1]"));
				seacrhResOwnerLink.click();
				waitUntilElementVisibleBy(driver, createNewTourLink, 120);
				clickElementJSWithWait(createNewTourLink);

				waitUntilElementVisibleBy(driver, dngtourmsg, 120);
				String expected = driver.findElement(dngtourmsg).getText();
				System.out.println(expected);
				if (expected.equalsIgnoreCase("This Guest is not eligible to Tour.")) {
					tcConfig.updateTestReporter("OwnersPage", "validate comments message for dng owner", Status.PASS,
							"MSg validated:" + expected);
				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate comments message for dng owner", Status.FAIL,
							"Message not validate");
				}

				waitUntilElementVisibleBy(driver, tourlanguage, 120);
				clickElementJSWithWait(tourlanguage);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement tourlanguageSelect = driver
						.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
				tourlanguageSelect.click();
				waitUntilElementVisibleBy(driver, popUpSaveBtn1, 120);
				// clickElementBy(popUpSaveBtn);
				if (verifyElementDisplayed(driver.findElement(popUpSaveBtn1))) {

					clickElementJSWithWait(popUpSaveBtn1);
				} else {
					clickElementJSWithWait(popUpSaveBtn1);
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				/*
				 * waitUntilElementVisibleBy(driver, latestTour, 120); if
				 * (verifyElementDisplayed(driver.findElement(latestTour))) { createdTourID =
				 * driver.findElement(latestTour).getText();
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * driver.findElement(latestTour).click(); System.out.println(
				 * "created TourID: " + createdTourID); tcConfig.updateTestReporter("LeadPage",
				 * "getlatestTour", Status.PASS,"created TourID: " + createdTourID);
				 * 
				 * } else { tcConfig.updateTestReporter("LeadPage", "getlatestTour",
				 * Status.FAIL, "Tour not created"); }
				 */

			} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		}

		else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	public void createAndValidateUnlinkTourForOwnerAndLeadReservation() {
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				String Message = driver.findElement(By.xpath("//input[@disabled]")).getAttribute("value");
				System.out.println(Message);
				if (Message.equalsIgnoreCase(testData.get("verifyDuplicateLead"))) {
					tcConfig.updateTestReporter("OwnerPage", "Verify COurtesy tour for DNG Owner with warning message",
							Status.PASS, "created TourID : " + createdTourID);
				} else {
					tcConfig.updateTestReporter("OwnerPage", "Verify COurtesy tour for DNG Owner with warning message",
							Status.FAIL, "Not Created");
				}

				/*
				 * waitUntilElementVisibleBy(driver, languageDrpdwn, 120);
				 * 
				 * clickElementBy(tourlanuageDrpDwn); clickElementBy(tourLangValue);
				 * clickElementBy(popUpSaveBtn); Select s = new
				 * Select(driver.findElement(languageDrpdwn)); s.selectByVisibleText("English");
				 */

				waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				WebElement tour = driver.findElement(popUpSaveBtn);
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", tour);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				// clickElementBy(popUpSaveBtn);
				// driver.findElement(By.xpath("//footer[contains(@class,'slds-modal')]/button[text()='Save']")).click();

				waitUntilElementVisibleBy(driver, latestTour, 120);
				if (verifyObjectDisplayed(latestTour)) {
					createdTourID = driver.findElement(latestTour).getText();
					System.out.println("created TourID: " + createdTourID);
					tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS,
							"created TourID: " + createdTourID);
					// driver.findElement(latestTour).click();
					WebElement elem = driver.findElement(By.xpath("//span[text()='Tour Record']//div"));
					JavascriptExecutor executor3 = (JavascriptExecutor) driver;
					executor3.executeScript("arguments[0].click();", elem);
				} else {
					tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
				}

				// unLink();

			} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	protected By deleteSuccessMsg = By.xpath("//span[contains(@class,'toastMessage slds-text-heading')]");

	public void unLink() {
		waitUntilElementVisibleBy(driver, latestTour, 120);
		if (verifyObjectDisplayed(latestTour)) {
			createdTourID = driver.findElement(latestTour).getText();
			System.out.println("created TourID: " + createdTourID);
		}

		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// driver.navigate().back();
		WebElement ele = driver.findElement(By.xpath("(//span[@class='view-all-label'])[1]"));
		waitUntilElementVisible(driver, ele, 120);
		// waitForSometime(tcConfig.getConfig().get("LongWait"));
		do {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,250)");

		} while (!driver.findElement(By.xpath("(//span[@class='view-all-label'])[1]")).isDisplayed());

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		// waitUntilElementVisible(driver, allview, 120);
		WebElement allview = driver.findElement(By.xpath("(//span[@class='view-all-label'])[1]"));
		clickElementJSWithWait(allview);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		WebElement ele1 = driver.findElement(By.xpath("//a[contains(@title,'" + createdTourID
				+ "')]//parent::span/parent::th/following-sibling::td[3]/span/span"));
		waitUntilElementVisible(driver, ele1, 120);

		/*
		 * do{ JavascriptExecutor js = (JavascriptExecutor) driver;
		 * js.executeScript("window.scrollBy(0,250)");
		 * 
		 * 
		 * }while(!ele1.isDisplayed());
		 */

		/*
		 * driver.navigate().refresh(); scrollDownForElementJSWb(ele1); Actions ac=new
		 * Actions(driver); ac.sendKeys(Keys.PAGE_DOWN);
		 */
		System.out.println(ele1.getText());
		if (ele1.getText().equalsIgnoreCase("Booked") || ele1.getText().equalsIgnoreCase("Draft")) {
			System.out.println(ele1.getText());
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// waitUntilElementVisible(driver, ele2, 120);
			WebElement ele2 = driver.findElement(By.xpath("//a[contains(@title,'" + createdTourID
					+ "')]//parent::span/parent::th/following-sibling::td[7]/span/div/a"));
			waitUntilElementVisible(driver, ele2, 120);
			clickElementJSWithWait(ele2);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, deleteBtn, 120);
			clickElementJSWithWait(deleteBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, deleteTourBtn, 120);
			clickElementJSWithWait(deleteTourBtn);

			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, deleteSuccessMsg, 120);
			if (verifyObjectDisplayed(deleteSuccessMsg)) {
				String deleteSucessmsg = driver.findElement(deleteSuccessMsg).getText();
				System.out.println(deleteSucessmsg);
				tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.PASS,
						"Tour unlinked successfully");

			} else {
				tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.PASS,
						"Tour unlinked successfully");

			}

			/*
			 * String part1="((//table)[1]/tbody/tr/th)["; String part2="]";
			 * 
			 * List<WebElement>
			 * le=driver.findElements(By.xpath("(//table)[1]/tbody/tr/th")); int
			 * size=le.size()-1; System.out.println(size); for(int i=size-1;i>=1;i--) {
			 * String fullxpath=part1+i+part2; WebElement
			 * elem2=driver.findElement(By.xpath(fullxpath));
			 * System.out.println(elem2.getText());
			 * if(!elem2.getText().equalsIgnoreCase(createdTourID)) {
			 * tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation",
			 * Status.PASS, "Tour unlinked successfully"); break; } else{ continue; } }
			 */

			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			/// if
			// (!driver.findElement(By.xpath("//a[contains(@title,'"+createdTourID+"')]")).isDisplayed())

		} else {

			tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.FAIL,
					"Unable to find the record");
		}

	}

	public void validate_DNGReservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				/*
				 * waitUntilElementVisibleBy(driver, languageDrpdwn, 120);
				 * 
				 * clickElementBy(tourlanuageDrpDwn); clickElementBy(tourLangValue);
				 * clickElementBy(popUpSaveBtn); Select s = new
				 * Select(driver.findElement(languageDrpdwn)); s.selectByVisibleText("English");
				 */

				waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				WebElement tour = driver.findElement(popUpSaveBtn);
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", tour);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				// clickElementBy(popUpSaveBtn);
				// driver.findElement(By.xpath("//footer[contains(@class,'slds-modal')]/button[text()='Save']")).click();

				// Modify By Ajib PArida

				waitUntilElementVisibleBy(driver, latestTour, 120);
				if (verifyObjectDisplayed(latestTour)) {
					createdTourID = driver.findElement(latestTour).getText();
					System.out.println("created TourID: " + createdTourID);
					tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS,
							"created TourID: " + createdTourID);

				} else {
					tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	public void getlatestTour() {

		waitUntilElementVisibleBy(driver, latestTour, 120);
		if (verifyObjectDisplayed(latestTour)) {
			createdTourID = driver.findElement(latestTour).getText();
			driver.findElement(latestTour).click();
			System.out.println("created TourID: " + createdTourID);
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS, "created TourID: " + createdTourID);

		} else {
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
		}

	}

	private By fieldClubMemAcc = By.xpath(
			"//span[text()='Account Name']/../../div[2]/span/slot/slot/force-lookup/div/force-hoverable-link/div/a");

	public void verifyClubMembershipAccount() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldClubMemAcc)) {
			if (driver.findElement(fieldClubMemAcc).getText().equals(testData.get("MmbershipNumber"))) {
				tcConfig.updateTestReporter("OwnersPage", "verifyClubMembershipAccount", Status.PASS,
						"Membership account number is visible after global search");
			} else {
				tcConfig.updateTestReporter("OwnersPage", "verifyClubMembershipAccount", Status.FAIL,
						"Membership account number is not visible after global search");
			}
		} else {
			tcConfig.updateTestReporter("OwnersPage", "verifyClubMembershipAccount", Status.FAIL,
					"Membership account number field is not visible");
		}
	}

	public void validatePrintButtonNotvisibleForOwner() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("OwnerName"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("(//div//div[contains(text(),'Owner')  and @dir='ltr'])[1]")).getText()
					.contains("Owner")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("(//a//div//span[@title='" + testData.get("OwnerName") + "'])[1]"));
				seacrhResOwnerLink.click();
				waitUntilElementVisibleBy(driver, createNewTourLink, 120);

				if (!(driver.findElement(printButton)).isEnabled()) {
					System.out.println(driver.findElement(printButton).isDisplayed());
					tcConfig.updateTestReporter("OwnersPage", "validate Print  Button not displayed for Owners ",
							Status.PASS, "Unsuccessfull");
				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate Print  Button not displayed for Owners ",
							Status.FAIL, "successfull");
				}

			} else {
				tcConfig.updateTestReporter("OwnersPage", "validate Print  Button not displayed for Owners ",
						Status.FAIL, "Unsuccessfull");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "validate Print  Button not displayed for Owners ", Status.FAIL,
					"Unsuccessfull");
		}
	}

	/**
	 * c24125, TC_0279
	 * 
	 * @throws AWTException
	 */
	public void validatesameAddressAsGuest() throws AWTException {
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);

				Robot r = new Robot();

				r.mouseWheel(5);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleBy(driver, streetAddressPrimary, 120);

				String primaryAddress = getTextFromBodyBy(streetAddressPrimary).trim();
				// System.out.println(primaryAddress);
				Robot r1 = new Robot();

				r1.mouseWheel(-5);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, AddAdditinalguest, 120);
				// clickElementBy(AddAdditinalguest);
				clickElementJS(AddAdditinalguest);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, guestAddressField, 120);

				WebElement ele = driver
						.findElement(By.xpath("//label[text()='Same as Primary Guest Address']/parent::div/div/input"));

				waitUntilElementVisibleWb(driver, ele, 120);
				Actions act = new Actions(driver);
				act.moveToElement(ele).click().build().perform();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String guestAdd = driver.findElement(guestAddressField).getAttribute("value").trim();
				System.out.println(guestAdd);
				if (primaryAddress.equalsIgnoreCase(guestAdd)) {
					tcConfig.updateTestReporter("OwnersPage",
							"validate after clicking on same as primary check box the address populate same with primary ",
							Status.PASS, "successfull");
				} else {
					tcConfig.updateTestReporter("OwnersPage",
							"validate after clicking on same as primary check box the address populate same with primary ",
							Status.FAIL, "unsuccessfull");
				}

			} else {
				tcConfig.updateTestReporter("OwnersPage",
						"validate after clicking on same as primary check box the address populate same with primary ",
						Status.FAIL, "unsuccessfull");
			}
		} else {
			tcConfig.updateTestReporter("OwnersPage",
					"validate after clicking on same as primary check box the address populate same with primary ",
					Status.FAIL, "unsuccessfull");
		}
	}

	/**
	 * c24125, TC_0277 in-house marketer
	 * 
	 * @throws AWTException
	 */
	public void validatesameAddressAsGuest_inhouseMarketer() throws AWTException {

		waitUntilElementVisibleBy(driver, AddAdditinalguest, 120);
		Robot r = new Robot();
		r.mouseWheel(5);
		waitUntilElementVisibleBy(driver, streetAddPrimaryInhouse, 120);
		String primaryAddress = getTextFromBodyBy(streetAddPrimaryInhouse).trim();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Robot r1 = new Robot();
		r1.mouseWheel(-5);
		// clickElementBy(AddAdditinalguest);
		clickElementJS(AddAdditinalguest);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, guestAddressField, 120);

		WebElement ele = driver
				.findElement(By.xpath("//label[text()='Same as Primary Guest Address']/parent::div/div/input"));

		waitUntilElementVisibleWb(driver, ele, 120);
		Actions act = new Actions(driver);
		act.moveToElement(ele).click().build().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String guestAdd = driver.findElement(guestAddressField).getAttribute("value").trim();
		System.out.println(guestAdd);
		if (primaryAddress.equalsIgnoreCase(guestAdd)) {
			tcConfig.updateTestReporter("OwnersPage",
					"validate after clicking on same as primary check box the address populate same with primary ",
					Status.PASS, "successfull");
			clickElementBy(btnCancel);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("OwnersPage",
					"validate after clicking on same as primary check box the address populate same with primary ",
					Status.FAIL, "unsuccessfull");
		}

	}

	public void validateCaseOnhold() throws AWTException {
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);

				Robot r = new Robot();

				r.mouseWheel(9);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, cases1, 120);
				clickElementJSWithWait(cases1);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Robot r1 = new Robot();

				r1.mouseWheel(3);
				waitUntilElementVisibleBy(driver, statusEdit, 120);
				clickElementJSWithWait(statusEdit);
				waitUntilElementVisibleBy(driver, statusDropDown, 120);
				clickElementJSWithWait(statusDropDown);

				/*
				 * Select s = new Select(driver.findElement(statusDropDown));
				 * s.selectByVisibleText("On Hold");
				 */
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, onHoldValue, 120);
				clickElementJSWithWait(onHoldValue);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// waitUntilElementVisibleBy(driver, onhldsEdit, 120);
				// clickElementBy(onhldsEdit);
				waitUntilElementVisibleBy(driver, onholdsText, 120);
				fieldDataEnter(onholdsText, "OK");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, saveCase, 120);
				clickElementBy(saveCase);
				Robot r2 = new Robot();

				r2.mouseWheel(-2);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, OnhldStatus, 120);
				String s1 = getTextFromBodyBy(OnhldStatus).trim();
				String actual = "On Hold".trim();
				if (s1.equalsIgnoreCase(actual)) {
					tcConfig.updateTestReporter("OwnersPage", "validate the case is on hold ", Status.PASS,
							"successfull");
				} else {

					tcConfig.updateTestReporter("OwnersPage", "validate the case is on hold ", Status.FAIL,
							"Unsuccessfull");

				}

			} else {
				tcConfig.updateTestReporter("OwnersPage", "validate the case is on hold ", Status.FAIL,
						"Unsuccessfull");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "validate the case is on hold ", Status.FAIL, "Unsuccessfull");
		}
	}

	/*
	 * Method-To search for a Owner record Designed By-Jyoti
	 */

	public void searchForOwner() {
		waitUntilElementVisibleBy(driver, globalSearchEdit, 120);
		if (Owner == null) {
			fieldDataEnter(globalSearchEdit, testData.get("OwnerName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(selectLeadItem);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("OwnersPage", "searchForOwner", Status.PASS,
					"Owner details is displayed after searching for Owner : " + testData.get("OwnerName"));
		} else {
			fieldDataEnter(globalSearchEdit, Owner);
			clickElementBy(selectLeadItem);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("OwnersPage", "searchForOwner", Status.FAIL,
					"Unable to find the Owner details after searching for Owner : " + Owner);
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method-Create Tour in Owner PAge Designed By-Jyoti
	 */

	public void clickTourRecord() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnCreateTourRecord, 120);
		clickElementJSWithWait(btnCreateTourRecord);
		waitUntilElementVisibleBy(driver, txtCreateTourRecord, 120);
		clickElementJSWithWait(btnSave);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-VerifyHardStop message in OwnerPage Designed By-Jyoti
	 */

	public void bookTourForHardStop() {
		waitUntilElementVisibleBy(driver, errorMsg, 120);
		String str = driver.findElement(errorMsg).getText();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("OwnersPage", "bookTourForHardStop", Status.PASS,
				"User can see Error Messgae displayed as :- " + str);
		clickElementJS(bntCancel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-validate Incentive is scored Designed By-JYoti
	 */
	public void validateIncentiveButtonDisable_Native() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnincentive, 120);
		clickElementJS(btnincentive);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, softscore, 120);
		clickElementJS(softscore);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (driver.findElement(btnincentive).getAttribute("disabled").contains("true")) {
			tcConfig.updateTestReporter("OwnersPage", "validateIncentiveButtonDisable_Native", Status.PASS,
					"Incentive Eligible button is disabled");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateIncentiveButtonDisable_Native", Status.FAIL,
					"Incentive Eligible button is not disabled");
		}

	}

	/*
	 * Method-validate Qualification score in Owner page Native UI Designed By-JYoti
	 */
	public void validateQualificationStatus_OwnerNative() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,300)", "");
		String strScore = driver.findElement(txtQuaNative).getText();
		if (strScore.equalsIgnoreCase(testData.get("QualificationScore"))) {
			tcConfig.updateTestReporter("ReservationPage", "validateQualificationStatus_OwnerNative", Status.PASS,
					"Qualification Score is: " + strScore);
		} else {
			tcConfig.updateTestReporter("ReservationPage", "validateQualificationStatus_OwnerNative", Status.FAIL,
					"Failed while getting Qualification Score");
		}
	}

	/*
	 * Method-click Owner Reservation in Native UI Designed By-JYoti
	 */
	public void clickOwnerReservation_Native() {
		waitUntilElementVisibleBy(driver, txtReservationOwner_Native, 120);
		clickElementJSWithWait(txtReservationOwner_Native);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtOwnerReserv, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> lstbutton = driver.findElements(tbl_Reserv);
		lstbutton.get(lstbutton.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Serach Owner in MobileUI Designed By-JYoti
	 */

	public void navigateToOwnerPageMobile() {
		waitUntilElementVisibleBy(driver, searchLead, 120);
		driver.findElement(searchLead).clear();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(searchLead, testData.get("OwnerName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(searchLead).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(lblOwner);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtCustomerInfo, 120);
		if (verifyObjectDisplayed(txtCustomerInfo)) {
			tcConfig.updateTestReporter("OwnersPage", "navigateToOwnerPageMobile", Status.PASS,
					"Owner details is displayed after searching for Owner : ");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "navigateToOwnerPageMobile", Status.FAIL,
					"Failed : while creating page");
		}

	}

	/*
	 * Method-Validate score and disable incentive button Designed By-JYoti
	 */
	public void scoreAndDisableOwner_Mobile() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtCustomerInfo, 120);
		if (driver.findElement(btnIncentiveEligible).isEnabled()) {
			clickElementJS(btnIncentiveEligible);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilObjectVisible(driver, softscore, 120);
			clickElementJS(softscore);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("OwnersPage", "scoreAndDisableOwner_Mobile", Status.PASS,
					"The Incentive button is disabled and scored ");
		}

		else {
			tcConfig.updateTestReporter("OwnersPage", "scoreAndDisableOwner_Mobile", Status.FAIL,
					"The button is not disabled ");
		}
	}

	/*
	 * Method-validate qualification score is Mobile UI Designed By-JYoti
	 */
	public void validateQualificationStatus_Mobile() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, txtCustomerInfo, 120);
		String strstatus = driver.findElement(qs).getText();
		if (testData.get("QualificationScore").equalsIgnoreCase(strstatus)) {
			tcConfig.updateTestReporter("OwnersPage", "validateQualificationStatus", Status.PASS,
					"The score created is :- " + strstatus);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateQualificationStatus", Status.FAIL,
					"Failed : score mismatch");
		}
	}

	/*
	 * Method-Serach Owner and Print Error message Designed By-JYoti
	 */
	public void serachOwnerBookTourMobile() {

		waitUntilElementVisibleBy(driver, searchLead, 120);
		driver.findElement(searchLead).clear();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(searchLead, testData.get("OwnerName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(searchLead).sendKeys(Keys.ENTER);
		List<WebElement> lstelements = driver.findElements(lst_Lead);
		System.out.println("Number of elements:" + lstelements.size());
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(lblOwner);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnBookTourMobile, 120);
		clickElementJSWithWait(btnBookTourMobile);
		waitUntilElementVisibleBy(driver, hardStop_Mobile, 120);
		String str = driver.findElement(hardStop_Mobile).getText();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("OwnersPage", "serachOwnerBookTourMobile", Status.PASS,
				"User can see Error Messgae displayed as :- " + str);
		clickElementJSWithWait(btnCancel_Mobile);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	/*
	 * Method-validate Soft stopMessage error message Designed By-JYoti
	 */
	public void validateSoftStop_Native() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, softScoreMsg, 120);
		String str = driver.findElement(softScoreMsg).getText();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("ReservationPage", "bookTourSoftStop", Status.PASS,
				"User can see Error Messgae displayed as :- " + str);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().back();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method- Validate Qualification score in Mobile UI Designed By-JYoti
	 */
	public void validateQualificationStatus() {
		waitUntilElementVisibleBy(driver, btnCreateTourRecord, 120);
		String strstatus = driver.findElement(txtQualification).getText();
		if (testData.get("QualificationScore").equalsIgnoreCase(strstatus)) {
			tcConfig.updateTestReporter("OwnersPage", "validateQualificationStatus", Status.PASS,
					"The score displayed is :- " + strstatus);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateQualificationStatus", Status.FAIL,
					"Failed : while validating score");
		}
	}

	/*
	 * Method-validate Incentive button is disabled Designed By-JYoti
	 */
	public void validateIncentiveButtonDisableMobile() {
		waitUntilElementVisibleBy(driver, btnIncentiveEligible, 120);
		if (!driver.findElement(btnIncentiveEligible).isEnabled()) {
			tcConfig.updateTestReporter("OwnersPage", "validateIncentiveButtonDisableMobile", Status.PASS,
					"The button is disabled ");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validateIncentiveButtonDisableMobile", Status.FAIL,
					"Failed : Incentive button is not disabled");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(imgHome);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

}
