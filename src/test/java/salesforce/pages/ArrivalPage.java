package salesforce.pages;
import java.awt.AWTException;
import java.awt.Robot;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import salesforce.scripts.SalesForceScripts;


public class ArrivalPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);
	public String ResName = "";
	public static String txtCaseNumber="";

	public ArrivalPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	private By senstext = By.xpath("//input[@name='text-input']");
	private By editfields = By.xpath("//lightning-icon[@class='slds-icon-utility-new-direct-message slds-icon_container']");
	private By savebtn = By.xpath("//button[@title='SAVE']");
	private By owner = By.xpath("//a[contains(.,'"+testData.get("OwnerName")+"')]");
	public void addsensitivityandsave(){
		waitUntilElementVisibleBy(driver, editfields, 120);
		driver.findElement(editfields).click();
		waitUntilElementVisibleBy(driver, senstext, 120);
		driver.findElement(senstext).clear();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(senstext).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(senstext).sendKeys("test");
		
		waitUntilElementVisibleBy(driver, savebtn, 120);
		driver.findElement(savebtn).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String clratt = driver.findElement(owner).getAttribute("style");
		System.out.println(clratt);
		String clrattfinal = (clratt.split(":")[1]).trim();
		System.out.println(clrattfinal);
		if(clrattfinal.equalsIgnoreCase("red;")){
			tcConfig.updateTestReporter("ArrivalPage", "addsensitivityandsave", Status.PASS, "Validation of colour passed");
		}
		else{
			tcConfig.updateTestReporter("ArrivalPage", "addsensitivityandsave", Status.FAIL, "Validation of colour failed");
		}
	}
	
	private By homebtn=By.xpath("//a//span[text()='Home']");
	private By arrList=By.xpath("//a//b[starts-with(text(),'Arrival List')]");
	private By ReportArrival= By.xpath("//div//h1[@title='Arrival List']");
	private By Filterbtn=By.xpath("//div//button[contains(@class,'toggleFilter')]");
	private By Checkin=By.xpath("//div//button//p[contains(text(),'Check In Date')]");
	private By customize=By.xpath("//button//span[text()='Custom']");
	private By Applybtn= By.xpath("//button[contains(text(),'Apply')]");
	private By listitemCus=By.xpath("//a//span[text()='Custom' and @class='picklistLabel']");
	
	
	public void verifySensitivity() throws AWTException{
				
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement Homebtn=driver.findElement(homebtn);
		waitUntilElementVisibleBy(driver, homebtn, 120);
		executor.executeScript("arguments[0].click();",Homebtn );
//		clickElementBy(homebtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Robot rob = new Robot();
		rob.mouseWheel(10);
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement ArrList=driver.findElement(arrList);
//		clickElementBy(arrList);
		executor.executeScript("arguments[0].click();",ArrList );
		waitForSometime(tcConfig.getConfig().get("LongWait"));
//		waitUntilElementVisibleBy(driver, ReportArrival, 120);
//		SwitchtoWindow();
		
		Set<String> handle=driver.getWindowHandles();
		System.out.println(handle);
		ArrayList tabs = new ArrayList (driver.getWindowHandles());
		System.out.println(tabs.size());
		//Use the list of window handles to switch between windows
		driver.switchTo().window((String) tabs.get(1));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().frame(0);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ReportArrival, 120);
//		clickElementBy(Filterbtn);
		WebElement fltrBtn=driver.findElement(Filterbtn);
		executor.executeScript("arguments[0].click();",fltrBtn );
		waitForSometime(tcConfig.getConfig().get("LowWait"));
//		clickElementBy(Checkin);
		WebElement Chkin=driver.findElement(Checkin);
		executor.executeScript("arguments[0].click();",Chkin );
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, customize, 120);
		clickElementBy(customize);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(listitemCus);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> date= driver.findElements(By.xpath("//input[@placeholder='Pick a date']"));
		date.get(0).clear();
		date.get(0).sendKeys(testData.get("CheckInDate"));
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(Applybtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
	
		
	}
	
	
	
	
	private By gotores = By.xpath("(//div[@class='wave-table-cell-text' and contains(.,'GOTO RESERVATION')])[1]");
	private By editmarket = By.xpath("//button[@title='Edit Marketing Agent Assigned']");
	private By deleteagent = By.xpath("(//span[text()='Marketing Agent Assigned'])[2]//..//..//span[@class='deleteIcon']");
	private By agent = By.xpath("(//span[text()='Marketing Agent Assigned'])[2]//..//..//input[@type='text']");
	private By savebtnmar = By.xpath("//span[text()='Save']");
	private By agentname = By.xpath("(//span[text()='Marketing Agent Assigned'])[1]//..//..//a");
	public void changeagentandvalidate(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(2));
	    waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, editmarket, 120);
		driver.findElement(editmarket).click();
		waitUntilElementVisibleBy(driver, deleteagent, 120);
		driver.findElement(deleteagent).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, agent, 120);
		/*driver.findElement(marketingagent).sendKeys(Keys.BACK_SPACE);
		waitForSometime(tcConfig.getConfig().get("LowWait"));*/
		driver.findElement(agent).sendKeys(testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(agent).sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(agent).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(savebtnmar).click();
		waitUntilElementVisibleBy(driver, agentname, 120);
		String AgnetName = driver.findElement(agentname).getText();
		System.out.println(AgnetName);
		if(AgnetName.equalsIgnoreCase(testData.get("AgentName"))){
			tcConfig.updateTestReporter("ArrivalPage", "selectgotoresandvalidate",
					Status.PASS, "Agent name validated and Agent name is:"+AgnetName);
		}
		else{
			tcConfig.updateTestReporter("ArrivalPage", "selectgotoresandvalidate",
					Status.PASS, "Failed while validating Agent name");
		}
		
	    driver.switchTo().window(tabs.get(0));
	}
	
	private By searchGuest = By.xpath("//input[@placeholder='Search Guest, RM #']");
	private By guest = By.xpath("//td[@data-label='Guest Name']//div/a[contains(.,'"+testData.get("LeadName")+"')]");
	private By btnEdit = By.xpath("(//li//a[@class='forceActionLink'])[1]");
	private By addressDetails =By.xpath("(//span[contains(.,'Address Details')])[2]");
	private By streetAddress = By.xpath("(//span[contains(.,'Street Address')])[3]");
	private By city = By.xpath("(//span[contains(.,'City')])[3]");
	private By zip =By.xpath("(//span[contains(.,'Zip/Postal Code')])[3]");
	private By btnCancel = By.xpath("//button[@title='Cancel']");
	private By txtEmail = By.xpath("//input[@type='email']");
	private By reservationNumber =By.xpath("(//input[@class=' input'])[1]");
	private By imgArrival= By.xpath("(//img[@class='wn-left_custom_icon'])[3]");
	private By drpDate = By.xpath("(//select[@class='slds-select'])[1]");
	private By gstname= By.xpath("//table[contains(@class,'slds-table')]//tbody//tr[@id='arrivalRows']//td[4]//div//a");
	private By scroll=By.xpath("//div//h3//span[contains(text(),'Check In & Check Out Details')]");
	private By  verText=By.xpath("//input[contains(@class,'input' )]");
	private By delEmail =By.xpath("//input[@type='email']");		
	private By btnSave = By.xpath("//button[@title='Save']");
	
	public void validateDeleteEmail() throws AWTException{
	
		waitForSometime(tcConfig.getConfig().get("MedWait"));	
		waitUntilElementVisibleBy(driver, drpDate, 120);		
		driver.findElement(drpDate).click();
		Select date = new Select(driver.findElement(drpDate));
		date.selectByValue("Yesterday");	
		waitUntilElementVisibleBy(driver, searchGuest, 120);
		driver.findElement(searchGuest).click();
		fieldDataEnter(searchGuest, testData.get("LeadName"));
	    waitForSometime(tcConfig.getConfig().get("LongWait"));
		driver.findElement(gstname).click();	
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(btnEdit).click();				
		waitForSometime(tcConfig.getConfig().get("MedWait"));				
		Set<String> handel=driver.getWindowHandles();	
		waitForSometime(tcConfig.getConfig().get("MedWait"));	
		System.out.println(handel);		
		Robot rbup= new Robot();
		rbup.mouseWheel(3);
		waitUntilElementVisibleBy(driver, delEmail, 120);
		driver.findElement(delEmail).clear();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Robot rb= new Robot();
		rb.mouseWheel(7);
		
//		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", scroll);

		List<WebElement> ltext=driver.findElements(verText);
		
		String  StAdd=ltext.get(7).getAttribute("value");
		String  City=ltext.get(8).getAttribute("value");
		String Zip=ltext.get(9).getAttribute("value");
		
		System.out.println( "Street Address  "+StAdd  +"\n" +"City "+ City +"\n"+  "Zip "+ Zip );
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		driver.findElement(btnSave).click();
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		

	    driver.findElement(imgArrival).click();
	}
			
	public void validateOwnerArrival() throws AWTException{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		
		waitUntilElementVisibleBy(driver, drpDate, 120);
		
		driver.findElement(drpDate).click();
		Select date = new Select(driver.findElement(drpDate));
		date.selectByValue("Yesterday");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement norecord=driver.findElement(By.xpath("//div[@class='noResultPart']"));
		if(norecord.isDisplayed()){
			
			String NoRecord=norecord.getText();
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, NoRecord);
			
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
		}
		else{
		
				
		
		waitUntilElementVisibleBy(driver, searchGuest, 120);
		driver.findElement(searchGuest).click();
		fieldDataEnter(searchGuest, testData.get("LeadName"));

		
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		driver.findElement(gstname).click();
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.findElement(btnEdit).click();
		
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		
		Set<String> handel=driver.getWindowHandles();
		
		System.out.println(handel);
		
		Robot rb= new Robot();
		rb.mouseWheel(7);
		
//		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", scroll);
		
		List<WebElement> ltext=driver.findElements(verText);
		
		String  StAdd=ltext.get(7).getAttribute("value");
		String  City=ltext.get(8).getAttribute("value");
		String Zip=ltext.get(9).getAttribute("value");
		
		System.out.println( "Street Address  "+StAdd  +"\n" +"City "+ City +"\n"+  "Zip "+ Zip ); 
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(btnCancel).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	    driver.findElement(imgArrival).click();
	    
	    
	    
	    tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, "The user is validated");
	    driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		}
	}
	
	private By drpDownSalesStore = By.xpath("//input[@name='territoryType']");
	private By drpDwnArrivalList = By.xpath("(//select[@class='slds-select'])[2]");
	private By drpDwnWeek = By.xpath("(//select[@class='slds-select'])[1]");
	private By btnExpand = By.xpath("//div[@title='expand'][contains(@id,'arrivalList')]");
	private By fieldWAMIndicator = By.xpath("//div[contains(text(),'WAAM Indicator')]");
	private By fieldArrivalServiceEntity = By.xpath("//div[contains(text(),'Arrival Service Entity')]");
	
	public void verifyWAMIndicatorAndServiceEntity()
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
		
		WebElement option = new Select(driver.findElement(drpDwnArrivalList)).getFirstSelectedOption();
		if(!(option.getText().equals("ARRIVAL LIST")))
		{
			new Select(driver.findElement(drpDwnArrivalList)).selectByIndex(0);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		
		List<WebElement> e2 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
		if(e2.size()>0)
		{
			new Select(driver.findElement(drpDwnWeek)).selectByIndex(3);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			while(driver.findElements(btnExpand).size()==0 && driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]")).size()==0)
			{
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			List<WebElement> e1 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
			if(e1.size()>=0)
			{
				driver.findElement(drpDownSalesStore).click();;
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//span[text()='WorldMark Branson']")).click();
				//clickElementJSWithWait(driver.findElements(By.xpath("//lightning-base-combobox-item")).get(1));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		List<WebElement> e = driver.findElements(btnExpand);
		if(e.size()>0)
		{
			e.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyWAMIndicator", Status.FAIL, "Arrival List records are not present");
		}
		
		if(verifyObjectDisplayed(fieldWAMIndicator))
		{
			System.out.println(driver.findElement(fieldWAMIndicator).getText());
			tcConfig.updateTestReporter("ArrivalPage", "verifyWAMIndicator", Status.PASS, "WAM Indicator and Service Entity field is visible");
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyWAMIndicator", Status.FAIL, "WAM Indicator and Service Entity field is not visible");
		}
		if(verifyObjectDisplayed(fieldArrivalServiceEntity))
		{
			System.out.println(driver.findElement(fieldArrivalServiceEntity).getText());
			tcConfig.updateTestReporter("ArrivalPage", "verifyWAMIndicator", Status.PASS, "WAM Indicator and Service Entity field is visible");
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyWAMIndicator", Status.FAIL, "WAM Indicator and Service Entity field is not visible");
		}
		
	}
	
	private By linkResFirstName = By.xpath("//tr[@id='arrivalRows']/td[5]//a");
	private By chkBoxArrivalList = By.xpath("//tr[@id='arrivalRows']/td[2]/div//lightning-input");
	private By btnEditRes = By.xpath("//a[@title='Edit']");
	private By headerEditWindow = By.xpath("//h2[contains(@id,'title')]");
	private By fieldMarkngAssigned = By.xpath("//h3[contains(@class,'shade primaryPaletteBorder')]/..//span[contains(text(),'Marketing Agent')]/../../div[2]/span");
	private By lnkAssignedMrktngAgent = By.xpath("//span[text()='Marketing Agent Assigned']/../../div[2]//a");
	private By btnCancelRes = By.xpath("//button[@title='Cancel']");
	private By btnSaveAssignAgent = By.xpath("//button[text()='Save']");
	private By listArrival = By.xpath("//img[contains(@src,'Reservation_List')]");
	private By drpDayType = By.xpath("(//select[@class='slds-select'])[1]");
	private By btnAssignToMe = By.xpath("//button[contains(text(),'Assign To Me')]");
	
	public void verifyAssignMarketingAgent()
	{
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
		List<WebElement> lstMarketingAgentStatus = driver.findElements(By.xpath("//tr[@id='arrivalRows']/td[3]/div"));
		List<WebElement> lstResFName = driver.findElements(linkResFirstName);
		List<WebElement> lnkExpand = driver.findElements(btnExpand);
		List<WebElement> chkBoxArrival = driver.findElements(chkBoxArrivalList);
		String name = "";
		for(int i =0; i< lstResFName.size(); i++)
		{
			int size = lstMarketingAgentStatus.get(i).findElements(By.xpath(".//*")).size();
			if(size==0)
			{
				name = lstResFName.get(i).getText();
				clickElementWb(lnkExpand.get(i));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementWb(chkBoxArrival.get(i));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementWb(lstResFName.get(i));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				break;
			}
		}
		
		
		waitUntilElementVisibleBy(driver, btnEditRes, 120);
		clickElementJSWithWait(btnEditRes);
		waitUntilElementVisibleBy(driver, headerEditWindow, 120);
		
		if(verifyObjectDisplayed(fieldMarkngAssigned))
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAssignMarketingAgent", Status.PASS, "Marketing Agent Assigned field is non editable");
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAssignMarketingAgent", Status.FAIL, "Marketing Agent Assigned field is editable");
		}
		clickElementJSWithWait(btnCancelRes);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().back();
		
		waitUntilElementVisibleBy(driver, btnAssignToMe, 120);
		clickElementJSWithWait(btnAssignToMe);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnSaveAssignAgent);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, btnAssignToMe, 120);
		
		
		List<WebElement> e2 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
		if(e2.size()>0)
		{
			new Select(driver.findElement(drpDwnWeek)).selectByIndex(3);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(drpDownSalesStore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(driver.findElements(By.xpath("//lightning-base-combobox-item")).get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		
		WebElement e = driver.findElement(By.xpath("//a[text()='"+name+"']/../../../td[3]//lightning-icon"));
		if(verifyElementDisplayed(e))
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAssignMarketingAgent", Status.PASS, "Agent assigned symbol is visible");
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAssignMarketingAgent", Status.FAIL, "Agent assigned symbol is not visible");
		}
		
		WebElement linkFName = driver.findElement(By.xpath("//a[text()='"+name+"']"));
		clickElementJSWithWait(linkFName);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lnkAssignedMrktngAgent, 120);
		if(driver.findElement(lnkAssignedMrktngAgent).getText().trim().equals(testData.get("User")))
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAssignMarketingAgent", Status.PASS, "In House marketing name is reflecting in Marketing agent assigned field");
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAssignMarketingAgent", Status.FAIL, "In House marketing name is not reflecting in Marketing agent assigned field");
		}
		
	}
	
	private By fieldCheckOutDate = By.xpath("//span[text()='Check Out Date']/../../div[2]/span/span");
	private By fieldCheckInDate = By.xpath("//span[text()='Check In Date']/../../div[2]/span/span");
	private By chkBoxExtendedStay = By.xpath("//span[text()='Extended Stay']/../../div[2]//img");
	private By fieldRelatedRes = By.xpath("//span[contains(text(),'Related Reservations')]/../../div[2]/span/span");
	private By fieldExtendedCheckOutDate = By.xpath("//span[contains(text(),'Extended Stay Check')]/../../div[2]/span/span");
	private By lnkRelatedReservation = By.xpath("//span[@title='Related Reservations']/..");
	private By headerRelatedReservation = By.xpath("//h1[@title='Related Reservations']");
	private By listTableContents = By.xpath("//table[contains(@class,'uiVirtualDataTable')]//tbody/tr/th");
	private By txtBoxSearchUser = By.xpath("//input[@class='slds-input']");
	private By fieldAuValue = By.xpath("//a[text()='"+testData.get("FirstName")+"']/../../../td[@data-label='A']");
	private By listSalestoreValues = By.xpath("//span[@title='"+testData.get("SalesStore")+"']/..");
	private By fieldAUValue = By.xpath("//span[@title='A/U']/../div//span");
	
	public String[] getCheckOutDate()
	{
		waitUntilElementVisibleBy(driver, btnAssignToMe, 120);
		scrollDownForElementJSBy(fieldCheckOutDate);
		scrollUpByPixel(150);
		String checkOutDate = driver.findElement(fieldCheckOutDate).getText();
		String checkInDate = driver.findElement(fieldCheckInDate).getText();
		driver.navigate().back();
		String[] dates = {checkOutDate,checkInDate};
		return dates;
	}
	
	public void verifyExtendedStayPropForR1(String checkOutDate)
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, chkBoxExtendedStay, 120);
		if(driver.findElement(chkBoxExtendedStay).getAttribute("class").contains("checked"))
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.PASS, "Extended stay checkbox is checked for the reservation");
			if(driver.findElement(fieldRelatedRes).getText().equals(SalesForceScripts.resNum[1]))
			{
				tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.PASS, "Reservation- "+SalesForceScripts.resNum[1]+"is visible in the related reservation section of Reservation- "+SalesForceScripts.resNum[0]);
				if(driver.findElement(fieldExtendedCheckOutDate).getText().equals(checkOutDate))
				{
					tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.PASS, "Extended checkout date for"+ SalesForceScripts.resNum[0] + "is showing the scheduled checkout date of"+ SalesForceScripts.resNum[2]+"- "+ checkOutDate);
					driver.navigate().back();
				}
				else
				{
					tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.FAIL, "Extended checkout date for"+ SalesForceScripts.resNum[0] + "is not showing the scheduled checkout date of"+ SalesForceScripts.resNum[2]+"- "+ checkOutDate);
					driver.navigate().back();
				}
			}
			else
			{
				tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.FAIL, SalesForceScripts.resNum[1]+" is not visible in the related reservation section of "+SalesForceScripts.resNum[0]);
				driver.navigate().back();
			}
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.FAIL, "Extended stay checkbox is not checked for the reservation");
			driver.navigate().back();
		}	
	}
	
	public void verifyExtendedStayPropForR2(String checkInDate)
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, chkBoxExtendedStay, 120);
		if(driver.findElement(chkBoxExtendedStay).getAttribute("class").contains("checked"))
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.PASS, "Extended stay checkbox is checked for the reservation");
			if(driver.findElement(fieldRelatedRes).getText().contains(SalesForceScripts.resNum[0]) && driver.findElement(fieldRelatedRes).getText().contains(SalesForceScripts.resNum[2]))
			{
				tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.PASS, "Reservations- "+SalesForceScripts.resNum[1]+"and"+SalesForceScripts.resNum[2]+"is visible in the related reservation section of Reservation- "+SalesForceScripts.resNum[1]);
				if(driver.findElement(fieldExtendedCheckOutDate).getText().equals(checkInDate))
				{
					tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.PASS, "Extended checkout date for"+ SalesForceScripts.resNum[1] + "is showing the scheduled checkin date of"+ SalesForceScripts.resNum[2]+"- "+ checkInDate);
					
				}
				else
				{
					tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.FAIL, "Extended checkout date for"+ SalesForceScripts.resNum[1] + "is not showing the scheduled checkin date of"+ SalesForceScripts.resNum[2]+"- "+ checkInDate);
					
				}
			}
			else
			{
				tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.FAIL, SalesForceScripts.resNum[1]+"and"+ SalesForceScripts.resNum[2]+" is not visible in the related reservation section of "+SalesForceScripts.resNum[1]);
				
			}
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyExtendedStayProp", Status.FAIL, "Extended stay checkbox is not checked for the reservation");
			
		}
	}
	
	public void validateRelatedExtendedReservationsFields()
	{
		waitUntilElementVisibleBy(driver, lnkRelatedReservation, 120);
		clickElementBy(lnkRelatedReservation);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, headerRelatedReservation, 120);
		String[] reserNum = testData.get("ReservationNumber").split(",");
		String[] guestName = testData.get("OwnerName").split(",");
		String location = testData.get("BookingLocation");
		int size = driver.findElements(listTableContents).size();
		for(int i=0; i<size; i++)
		{
			if(i==1)
			{
				i=2;
			}
				WebElement e = driver.findElement(By.xpath("//a[text()='"+reserNum[i]+"']/../../../../td[2]/span/span"));
				WebElement e1 = driver.findElement(By.xpath("//a[text()='"+reserNum[i]+"']/../../../../td[3]/span/span"));
				
			if(i==2)
			{
				i=1;
			}
			if(e.getText().trim().equals(guestName[i]) && e1.getText().trim().equals(location))
			{
				tcConfig.updateTestReporter("ArrivalPage", "validateRelatedReservationsFields", Status.PASS, "Related reservation fields: GuestName:"+ guestName[i] + " and ResortName:"+ location+ " is visible");
			}
			else
			{
				tcConfig.updateTestReporter("ArrivalPage", "validateRelatedReservationsFields", Status.FAIL, "Related reservation fields are not visible");
			}
		}
		
		driver.navigate().back();
		
		
	}
	
	
	public void verifyAccountableArrivals()
	{
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		List<WebElement> e = driver.findElements(lnkAccountableArrivals);
		clickElementJSWithWait(e.get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		waitUntilElementVisibleBy(driver,fieldAUValue , 120);
		
		if(driver.findElement(fieldAUValue).getText().equals("ACCOUNTABLE"))
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAccountableArrivals", Status.PASS, "Accountable arrival is visible");
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "verifyAccountableArrivals", Status.FAIL, "Accountable arrival is not visible");
		}
		
		
	}
	
	private By lnkAccountableArrivals = By.xpath("//td[text()='A']/../../../td[@data-label='Guest First Name']//a");
	private By listArrivalName = By.xpath("//tr[not(contains(@class,'NQCSS'))]//td[text()='Exchange' or text()='Rental' or text()='Corp-Guest']/../td[text()='A']/../td[@data-label='Guest First Name']//a");
	private By listArrivalLastName = By.xpath("//td[text()='Exchange' or text()='Rental']/../td[text()='A']/../td[@data-label='Guest Last Name']//a");
	private By btnNext = By.xpath("//button[text()='Next']");
	private By fieldReservationNumber = By.xpath("//span[text()='Res Number']/../../div[2]/span/span");
	private By btnEditAge = By.xpath("//button[@title='Edit Age']");
	private By lnkAge = By.xpath("//span[text()='Age']/../../div//a");
	private By lstdrpDownSelections = By.xpath("//a[@role='menuitemradio']");
	private By txtBoxSearchGuest = By.xpath("//input[contains(@placeholder,'Search Guest')]");
	private By lnkHouseHoldIncome = By.xpath("//span[text()='Household Income']/../../div//a");
	private By lnkAnniversaryMonth = By.xpath("//span[text()='Anniversary(Month)']/../../div//a");
	private By lnkAnniversaryDay = By.xpath("//span[text()='Anniversary(Day)']/../../div//a");
	private By lnkDOBMonth = By.xpath("//span[text()='Date of Birth(Month)']/../../div//a");
	private By lnkDOBDay = By.xpath("//span[text()='Date of Birth(Day)']/../../div//a");
	
	public void selectArrivalAndVerifyRedHighlight(String NQChoice)
	{
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
		
		WebElement option = new Select(driver.findElement(drpDwnArrivalList)).getFirstSelectedOption();
		if(!(option.getText().equals("ARRIVAL LIST")))
		{
			new Select(driver.findElement(drpDwnArrivalList)).selectByIndex(0);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		
		new Select(driver.findElement(drpDwnWeek)).selectByIndex(3);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		List<WebElement> e1 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
		if(e1.size()>0)
		{
			clickElementBy(drpDownSalesStore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(driver.findElements(By.xpath("//lightning-base-combobox-item")).get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		
		//boolean flag = true;
		boolean flag1 = false;
		while(flag1==false)
		{
			flag1 = getArrivalDetailsAndVerifyRedHighlight(NQChoice);
		}
		
	}
	
	
	public boolean searchArrival()
	{
		boolean flag = true;
		while(driver.findElements(listArrivalName).size()==0)
		{
			waitUntilElementVisibleBy(driver, btnNext, 120);
			scrollDownForElementJSBy(btnNext);
			if(driver.findElement(btnNext).isEnabled())
			{
				clickElementBy(btnNext);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, txtBoxSearchUser, 120);
			}
			else
			{
				flag = false;
				break;
			}
			
		}
		return flag;
	}
	
	public boolean checkReservationNumber()
	{
		int count =0;
		boolean flag = false;
		for(int i =0 ; i<driver.findElements(listArrivalName).size(); i++ )
		{
			driver.findElements(listArrivalName).get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, chkBoxExtendedStay, 120);
			if(!(driver.findElement(fieldReservationNumber).getText().startsWith("1")))
			{
				flag = true;
				break;
			}
			else
			{
				count++;
			}
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		if(count==driver.findElements(listArrivalName).size())
		{
			flag = false;
		}
		return flag;
	}
	
	private By cssRow = By.xpath("//tr[@class='NQCSS']");
	
	public boolean getArrivalDetailsAndVerifyRedHighlight(String field)
	{
		boolean flag = false;
		if(driver.findElements(listArrivalName).size()>0)
		{
			for(int i =0 ; i<driver.findElements(listArrivalName).size(); i++ )
			{
				String Fname = driver.findElements(listArrivalName).get(i).getText();
				String Lname = driver.findElements(listArrivalLastName).get(i).getText();
				ResName = Fname+" "+Lname;
				clickElementJSWithWait(driver.findElements(listArrivalName).get(i));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, chkBoxExtendedStay, 120);
				if(!(driver.findElement(fieldReservationNumber).getText().startsWith("1")))
				{
					tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.PASS, "Required arrival detail is present in the list");
					clickElementBy(btnEdit);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					switch(field){
					case "Age" :
						scrollDownForElementJSBy(lnkAge);
						scrollUpByPixel(150);
						clickElementBy(lnkAge);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementWb(driver.findElements(lstdrpDownSelections).get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						break;
					case "Income" :
						scrollDownForElementJSBy(lnkAge);
						scrollUpByPixel(150);
						clickElementBy(lnkHouseHoldIncome);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementWb(driver.findElements(lstdrpDownSelections).get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						break;
					case "Special Occasion" :
						scrollDownForElementJSBy(lnkAnniversaryMonth);
						scrollUpByPixel(150);
						clickElementBy(lnkAnniversaryMonth);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementWb(driver.findElements(lstdrpDownSelections).get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(lnkAnniversaryDay);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementWb(driver.findElements(lstdrpDownSelections).get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(lnkDOBMonth);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementWb(driver.findElements(lstdrpDownSelections).get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(lnkDOBDay);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementWb(driver.findElements(lstdrpDownSelections).get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(lnkHouseHoldIncome);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementWb(driver.findElements(lstdrpDownSelections).get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						break;
						}
					clickElementBy(savebtnmar);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
					WebElement option = new Select(driver.findElement(drpDwnArrivalList)).getFirstSelectedOption();
					if(!(option.getText().equals("ARRIVAL LIST")))
					{
						new Select(driver.findElement(drpDwnArrivalList)).selectByIndex(0);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					
					new Select(driver.findElement(drpDwnWeek)).selectByIndex(3);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					List<WebElement> e1 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
					if(e1.size()>0)
					{
						clickElementBy(drpDownSalesStore);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementWb(driver.findElements(By.xpath("//lightning-base-combobox-item")).get(1));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					
					waitUntilElementVisibleBy(driver, txtBoxSearchGuest, 120);
					fieldDataEnter(txtBoxSearchGuest, ResName);
					driver.findElement(txtBoxSearchGuest).sendKeys(Keys.ENTER);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					 if(driver.findElement(cssRow).getAttribute("className").equals("NQCSS"))
						{
							tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.PASS, "Record is highlighted in Red");
						}
					 else{
							tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.FAIL, "Record is not highlighted in Red");
						}
					flag = true;
					break;
					
				}
				else
				{
					tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.PASS, "No proper data is found in this page");
				}
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			if(flag==false)
			{
				if(driver.findElement(btnNext).isEnabled())
				{
					tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.PASS, "No data is found in this page");
					clickElementBy(btnNext);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				else
				{
					flag=true;
					tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.PASS, "All records are searched and no data found");
				}
			}
		}
		else
		{
			//flag=false;
			if(driver.findElement(btnNext).isEnabled())
			{
				tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.PASS, "No data is found in this page");
				clickElementBy(btnNext);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			else
			{
				flag=true;
				tcConfig.updateTestReporter("ArrivalPage", "getArrivalDetailsAndVerifyRedHighlight", Status.PASS, "All records are searched and no data found");
			}
			
		}
		return flag;
	}
		
	
	
		private By imgSearchIcon = By.xpath("//img[contains(@src,'Search')]");
		//private By txtBoxSearchGuest = By.xpath("//input[@name='enter-search']");
		private By lnkLeadName = By.xpath("//a[@data-type='Lead']"); 
		private By lnkResFirstNameForAdditionalGst = By.xpath("//button[contains(@class,'cTMCMA')]/../../../td[@data-label='Guest First Name']//a");
		private By channelType = By.xpath("//span[contains(@data-apiname,'TMTravelChannelReport')]/td");
		
			
			
			/**
			 * 
			 * Added by Ritam
			 */
			
			
			public void selectFirstArrival()
			{
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
				if(driver.findElements(lnkResFirstNameForAdditionalGst).size()>0)
				{
					tcConfig.updateTestReporter("ArrivalPage", "selectFirstArrival", Status.PASS, "Required arrival data is present");
					for (int i = 0; i < driver.findElements(lnkResFirstNameForAdditionalGst).size(); i++) {
						if (driver.findElements(channelType).get(i).getText().contains("Owner")) {
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							clickElementWb(driver.findElements(lnkResFirstNameForAdditionalGst).get(i));
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							break;
						}
					}
					
				}
				else
				{
					tcConfig.updateTestReporter("ArrivalPage", "selectFirstArrival", Status.FAIL, "Requires arrival data is not present");
				}
				
			}
			
			
			
			private By lnkCreateChangeRequest = By.xpath("//a[@title='Create Change Request']");
			private By lnkChangeRequestType = By.xpath("//span[text()='Change Request Type']/../..//a");
			
			private By txtBoxRequestReason = By.xpath("//span[text()='Change Request Reason']/../..//input");
			private By lstDrpDwnvalues = By.xpath("//a[@role='menuitemradio']");
			private By lnkCaseCreatedMsg = By.xpath("//span[contains(@class,'toastMessage')]/a/div");
			private By btnSaveChangeRequest = By.xpath("//div[contains(@class,'modal-footer')]/button[2]");
			
			public void createChangeRequest()
			{
				waitUntilElementVisibleBy(driver, lnkCreateChangeRequest, 120);
				clickElementBy(lnkCreateChangeRequest);
				waitUntilElementVisibleBy(driver, lnkChangeRequestType, 120);
				clickElementJSWithWait(lnkChangeRequestType);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> e = driver.findElements(lstDrpDwnvalues);
				if(e.size()>0)
				{
					tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.PASS, "Dropdown values are present");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementWb(e.get(2));
					waitUntilElementVisibleBy(driver, txtBoxRequestReason, 120);
					fieldDataEnter(txtBoxRequestReason, generateRandomString(5,Format.CHARACTER_STRING.getFormat()));
					waitUntilElementVisibleBy(driver, btnSaveChangeRequest, 120);
					clickElementBy(btnSaveChangeRequest);
					waitUntilElementVisibleBy(driver, lnkCaseCreatedMsg, 120);
					if(verifyObjectDisplayed(lnkCaseCreatedMsg))
					{
						//waitForSometime(tcConfig.getConfig().get("LowWait"));
						txtCaseNumber = driver.findElement(lnkCaseCreatedMsg).getAttribute("title");
						tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.PASS, "Case creation is successfull");
					}
					else
					{
						tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.FAIL, "Case creation Failed");
					}
				}
				else
				{
					tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.FAIL, "Change request values are not present");
				}
				
			} 
			
			

			private By listArrivalDetails = By.xpath("//tr[@id='arrivalRows']");
			private By fieldPagination = By.xpath("//span[@class='slds-p-horizontal_small']");
			private By btnPrev = By.xpath("//button[text()='Prev']");
			private By btnNextPage = By.xpath("//button[text()='Next']");
			
			public void verifyPagination(String options)
			{
				waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
				
				List<WebElement> e2 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
				
				if(e2.size()==0)
				{
					if(driver.findElements(btnExpand).size()<Integer.parseInt(testData.get("RecordSize")))
					{
						new Select(driver.findElement(drpDwnWeek)).selectByIndex(3);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
					if(driver.findElements(fieldPagination).size()>0)
					{
						tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.PASS, "Pagination is present");
						int size = driver.findElements(listArrivalDetails).size();
						if(size==Integer.parseInt(testData.get("RecordSize")))
						{
							tcConfig.updateTestReporterWithoutScreenshot("ArrivalPage", "verifyPagination", Status.PASS, testData.get("RecordSize")+" count of data is present in the page");
							if(!driver.findElement(btnPrev).isEnabled() && driver.findElement(btnNextPage).isEnabled())
							{
								tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.PASS, "User is in first page");
								clickElementBy(btnNextPage);
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								if(driver.findElement(btnPrev).isEnabled())
								{
									int size1 = driver.findElements(listArrivalDetails).size();
									if(size1==Integer.parseInt(testData.get("RecordSize")))
									{
										tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.PASS, testData.get("RecordSize")+" count of data is present in the second page");
										clickElementBy(btnPrev);
										waitForSometime(tcConfig.getConfig().get("MedWait"));
										if(!driver.findElement(btnPrev).isEnabled())
										{
											tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.PASS, "Pagination is working properly");
										}
										else
										{
											tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.FAIL, "Pagination is not working");
										}
									}
									else
									{
										tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.PASS, testData.get("RecordSize")+" count of data is not present in the second page");
									}
								}
								else
								{
									tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.FAIL, "User is not able to navigate to second page");
								}
							}
							else
							{
								tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.FAIL, "Previous button or Next button is not displayed properly");
							}
						}
						else
						{
							tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.FAIL, testData.get("RecordSize")+" count of data is not present in the page");
						}
					}
					else
					{
						tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.FAIL,"Required amount of data is not present for pagination");
					}
					
					
				}
			}
			
			private By fieldTravelChannel = By.xpath("//span[text()='Travel Channel']/../../div[2]/span");
			private By btnAdditionalGuest = By.xpath("//button[text()='Add Additional Guest']");
			private By lnkOwner = By.xpath("//span[contains(text(),'Guest Name')]/../../div[2]//a");
			
			
			/**
			 * 
			 * Updated by Ritam
			 */
			
			public void validateTravelChannel()
			{
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, fieldTravelChannel, 120);
				String travelchannelValue=driver.findElement(fieldTravelChannel).getText();
				if(travelchannelValue.equals(testData.get("TravelChannel")))
				{
					tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.PASS, "Travel channel is showing as "+ travelchannelValue);
				}
				else
				{
					tcConfig.updateTestReporter("ArrivalPage", "verifyPagination", Status.FAIL, "Travel channel is not showing as "+ testData.get("TravelChannel"));
				}
			}
			private By fieldOwnerName = By.xpath("//span[text()='Name']/../..");
			private By lnkOwnerAdd = By.xpath("//span[text()='Mailing Address']/../../div[2]//a");
			
			public String getAddressDetailsOfRes()
			{
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, btnAssignToMe, 120);
				scrollDownForElementJSBy(lnkOwner);
				scrollUpByPixel(100);
				clickElementJSWithWait(lnkOwner);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, fieldOwnerName, 120);
				scrollDownForElementJSBy(lnkOwnerAdd);
				scrollUpByPixel(100);
				String adress = driver.findElement(lnkOwnerAdd).getAttribute("title");
				driver.navigate().back();
				
				return adress;
			}
			
			public void clickAddAditionalGuest()
			{
				waitUntilElementVisibleBy(driver, btnAdditionalGuest, 120);
				clickElementJSWithWait(btnAdditionalGuest);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, chkBoxSameAsPrimary, 120);
			}
			private By chkBoxSameAsPrimary = By.xpath("//label[text()='Same as Primary Guest Address']/..//input");
			private By txtBoxAddressGuest = By.xpath("//span[text()='Address Details']/../..//input");
			private By btnCancelGuest = By.xpath("//button[text()='Cancel']");
			
			public void verifySameAsPrimaryAddressChkBox(String address)
			{
				int count = 0;
				waitUntilElementVisibleBy(driver, chkBoxSameAsPrimary, 120);
				clickElementBy(chkBoxSameAsPrimary);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				for(int i =0; i<driver.findElements(txtBoxAddressGuest).size(); i++)
				{
					if(address.toLowerCase().contains(driver.findElements(txtBoxAddressGuest).get(i).getText().toLowerCase()))
					{
						count++;
					}
				}
				
				if(count==driver.findElements(txtBoxAddressGuest).size())
				{
					tcConfig.updateTestReporter("AdditionalGuest", "verifySameAsPrimaryAddressChkBox", Status.PASS, "Address of owner is reflecting after clicking on the checkbox");
				}
				else
				{
					tcConfig.updateTestReporter("AdditionalGuest", "verifySameAsPrimaryAddressChkBox", Status.FAIL, "Address of owner is not reflecting correctly after clicking on the checkbox");
				}
				
				clickElementBy(btnCancelGuest);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			private By caseSection = By.xpath("//span[@title='Cases']");
			
			public void verifyCaseCreation()
			{
				waitUntilElementVisibleBy(driver, caseSection, 120);
				scrollDownForElementJSBy(caseSection);
				//scrollUpByPixel(50);
				waitUntilElementVisibleBy(driver, By.xpath("//a[text()='"+txtCaseNumber+"']"), 120);
				if(verifyElementDisplayed(driver.findElement(By.xpath("//a[text()='"+txtCaseNumber+"']"))))
				{
					System.out.println(driver.findElement(By.xpath("//a[text()='"+txtCaseNumber+"']")));
					tcConfig.updateTestReporter("ChangeRequest", "verifyCaseCreation", Status.PASS, "Newly created case "+txtCaseNumber+" is present in Cases section");
				}
				else
				{
					tcConfig.updateTestReporter("ChangeRequest", "verifyCaseCreation", Status.FAIL, "Newly created case "+txtCaseNumber+" is not present in Cases section");
				}
			}
			
			private By headerCases = By.xpath("//h1[@title='Cases']");
			
			public void verifyMyCasesOrMyResortOpenCasesNotPresent()
			{
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, caseSection, 120);
				scrollDownForElementJSBy(caseSection);
				clickElementBy(caseSection);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, headerCases, 120);
				if(verifyElementDisplayed(driver.findElement(headerCases)))
				{
					System.out.println(driver.findElement(headerCases).getText());
					tcConfig.updateTestReporter("ChangeRequest", "verifyMyCasesNotPresent", Status.PASS, "My Cases link is not present");
				}
				else
				{
					tcConfig.updateTestReporter("ChangeRequest", "verifyMyCasesNotPresent", Status.FAIL, "Cases header is not present");
				}
				
				driver.navigate().back();
				scrollUpByPixel(300);
			}
			private By listArrivals = By.xpath("//img[contains(@src,'Reservation_List')]");
			public void verifyCreateChangeReq(){
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, listArrivals, 120);
				List<WebElement> button =driver.findElements(imgArrival);
				 int count=button.size();
				 if(count>=0)
				 {
					 button.get(0).click();	
					 waitForSometime(tcConfig.getConfig().get("MedWait"));
				 }
				 else
				 {
					 tcConfig.updateTestReporter("ReservationPage", "nativeSearchReservation", Status.FAIL, "Reservation not found");
				 }
				 
			}
			
			
			private By lnkDelAction = By.xpath("//a[@class='deleteAction']");
			private By lnkCaseOwner = By.xpath("//a[contains(@class,'entityMenuTrigger')]");
			private By lnkChangeOwner = By.xpath("//a[@title='"+testData.get("ChangeOwner")+"']");
			private By lnkQueueName = By.xpath("//div[@title='"+testData.get("QueueUser")+"']/../..");
			
			public void createChangeRequestWithUserOrQueue()
			{
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, lnkCreateChangeRequest, 120);
				clickElementBy(lnkCreateChangeRequest);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, lnkChangeRequestType, 120);
				clickElementJSWithWait(lnkChangeRequestType);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> e = driver.findElements(lstDrpDwnvalues);
				if(e.size()>0)
				{
					tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.PASS, "Dropdown values are present");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementWb(e.get(2));
					waitUntilElementVisibleBy(driver, txtBoxRequestReason, 120);
			fieldDataEnter(txtBoxRequestReason, generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
					waitUntilElementVisibleBy(driver, lnkDelAction, 120);
					clickElementBy(lnkDelAction);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, lnkCaseOwner, 120);
					clickElementBy(lnkCaseOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, lnkChangeOwner, 120);
					clickElementJS(lnkChangeOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> e2 = driver.findElements(By.xpath("//div[contains(@class,'inputWrapper')]//input"));
					if(e2.size()>0)
					{
						e2.get(e2.size()-1).sendKeys(testData.get("QueueUser"));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						waitUntilElementVisibleBy(driver, lnkQueueName, 120);
						clickElementBy(lnkQueueName);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
					waitUntilElementVisibleBy(driver, btnSaveChangeRequest, 120);
					clickElementBy(btnSaveChangeRequest);
					waitUntilElementVisibleBy(driver, lnkCaseCreatedMsg, 120);
					if(verifyObjectDisplayed(lnkCaseCreatedMsg))
					{
						txtCaseNumber = driver.findElement(lnkCaseCreatedMsg).getAttribute("title");
						tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.PASS, "Case creation is successfull");
					}
					else
					{
						tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.FAIL, "Case creation Failed");
					}
				}
				else
				{
					tcConfig.updateTestReporter("ChangeRequest", "createChangeRequest", Status.FAIL, "Change request values are not present");
				}
			}

}

