package salesforce.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.List;

import org.apache.log4j.Logger;
//import org.jboss.netty.util.internal.SystemPropertyUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BusinessRules extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(BusinessRules.class);

	public BusinessRules(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	JavascriptExecutor je = (JavascriptExecutor) driver;
	protected By btnNew = By.xpath("//a[@title='New']");
	protected By txtCreditBand = By.xpath("//h2[contains(.,'New Business Rule: Credit Band Rule')]");
	protected By creditBand = By.xpath("(//button[@title='Move selection to Chosen']//lightning-primitive-icon)[1]");
	protected By tblChosenValue = By.xpath("//span[contains(.,'Chosen')]/..//span[@class='slds-media__body']//span");
	protected By tblChosenLst = By.xpath("(//button[@title='Move selection to Chosen']//lightning-primitive-icon)");
	protected By drpType = By.xpath("//label[text()='Type']//..//div//input");
	protected By drpLeadType = By.xpath("//label[text()='Lead Type']//..//div//input");
	protected By message = By.xpath("//span[contains(.,'Message')]");
	protected By btnsave = By.xpath("//button[@title='Save']");
	protected By chkActive = By.xpath("(//span[contains(.,'Active')])[2]");
	protected By errorMsg = By.xpath("//ul[contains(@class,'errorsList')]//li");
	protected By drpTypeValue = By.xpath("//lightning-base-combobox-item[@data-value='Soft Stop']");
	protected By drpTypeValuehrd = By.xpath("//lightning-base-combobox-item[@data-value='Hard Stop']");
	protected By newValue = By.xpath("//lightning-base-combobox-item[@data-value='Lead']");
	protected By btnCancel = By.xpath("//button[@title='Cancel']");
	protected By txtBusinessRule = By.xpath("//h2[contains(.,'New Business Rule')]");
	protected By btnNext = By.xpath("//button[@data-aura-class='uiButton']//span[contains(.,'Next')]");
	protected By countryQualification = By
			.xpath("//span[contains(.,'Country Qualification')]/../..//span[@class='slds-radio--faux']");
	protected By incentivePopup = By
			.xpath("//span[contains(.,'Incentive Eligible Rule')]/../..//span[@class='slds-radio--faux']");
	protected By sucessMsg = By.xpath("//div[@class='slds-align-middle slds-hyphenate']//span");
	protected By primaryLeadCredit = By.xpath("(//div[@class='slds-dueling-list__options slds-is-disabled'])[1]");
	protected By type = By.xpath("(//span[contains(.,'Type')]/../..//lightning-formatted-text[contains(.,'"
			+ testData.get("Type") + "')])[1]");
	protected By detailsTab = By.xpath("//a[@id='detailTab__item']");
	protected By creditBands = By.xpath("//div[contains(text(),'Credit Bands')]");
	protected By profiles = By.xpath("//div[contains(text(),'Profiles')]");
	protected By category = By.xpath("//div[contains(text(),'Category')]");
	protected By noOfDays = By.xpath("//span[contains(.,'Number of Days')]");
	// protected By chkActive =By.xpath("(//span[contains(.,'Active')])[2]");
	protected By txtIncentiveForm = By.xpath("//h2[contains(text(),'Incentive Eligible Rule')]");
	// protected By btnCancel = By.xpath("(//span[contains(.,'Cancel')])[2]");
	protected By chkBoxActive = By.xpath("(//span[contains(.,'Active')]/../..//input)[2]");
	protected By valueCreditBand = By.xpath("//span[contains(.,'Credit Bands')]/../..//lightning-formatted-text");
	protected By txtCountryQua = By.xpath("//h2[contains(.,'New Business Rule: Country Qualification')]");
	protected By country = By
			.xpath("//div[@class='slds-form-element__label slds-form-element__legend' and contains(.,'Country')]");
	protected By txtCategory = By
			.xpath("//div[@class='slds-form-element__label slds-form-element__legend' and contains(.,'Category')]");
	protected By messgae = By.xpath("//span[contains(.,'Message')]/..//span[@class='required ']");
	protected By txtCountryRule = By
			.xpath("(//div[@class='entityNameTitle slds-line-height--reset']/../..//lightning-formatted-text)[2]");
	protected By tabDetails = By.xpath("//a[@id='detailTab__item' and contains(.,'Details')]");
	protected By btnEdit = By.xpath("//button[@class='slds-button slds-button_neutral' and @name='Edit']");
	protected By txtCreditBandchkd = By
			.xpath("//img[@class='slds-truncate checked']/../../../..//span[@title='Credit Band Rule']");
	protected By btnEditRule = By.xpath(
			"//img[@class='slds-truncate checked']/../../../..//span[@title='Credit Band Rule']/../../..//lightning-icon[@class='slds-icon-utility-down slds-icon_container']//lightning-primitive-icon");
	protected By selectList = By.xpath(
			"(//div[contains(@class,'branding-actions actionMenu popupTargetContainer uiPopupTarget uiMenuList forceActionsDropDownMenuList uiMenuList')]//div[@class='branding-actions actionMenu'])");
	protected By delRule = By.xpath("//ul[@class='scrollable']//a[@title='Delete']");
	protected By txtDelRule = By.xpath("//h2[@class='title slds-text-heading--medium slds-hyphenate']");
	protected By btnDelete = By.xpath("//button[@title='Delete']");
	protected By delMsg = By.xpath("//div[@class='slds-align-middle slds-hyphenate']//span");
	protected By txtCountryRuleChkd = By
			.xpath("//img[@class='slds-truncate checked']/../../../..//span[@title='Country Qualification']");
	protected By txtMessage = By.xpath("//span[contains(.,'Message')]");
	protected By selectOwner = By.xpath(
			"//div[@class='slds-dueling-list__column slds-dueling-list__column_responsive']//div[@class='slds-dueling-list__options']//span[@class='slds-media__body']//span[@title='Owner']");

	protected By btnEditCounrtey = By.xpath(
			"//img[@class='slds-truncate checked']/../../../..//span[@title='Country Qualification']/../../..//lightning-icon[@class='slds-icon-utility-down slds-icon_container']//lightning-primitive-icon");
	protected By secSelect = By.xpath(
			"//div[@class='slds-dueling-list__column slds-dueling-list__column_responsive']//div[@class='slds-dueling-list__options']//span[@class='slds-media__body']//span[@title='Non-Owner']");
	public static String CountryRule;

	/*
	 * Method-Click New button Designed By-JYoti
	 */
	public void clickNew() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnNew, 120);
		clickElementBy(btnNew);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Create New Business Rule Designed By-JYoti
	 */
	public void createNewBusinessRule() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtBusinessRule, 120);
		clickElementJSWithWait(btnNext);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Select Incentive Rule Designed By-JYoti
	 */
	public void selectIncentiveEligibleRule() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtBusinessRule, 120);
		clickElementJSWithWait(incentivePopup);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnNext);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Select country Rule Designed By-JYoti
	 */
	public void selectCountryQualificationRule() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtBusinessRule, 120);
		clickElementJSWithWait(countryQualification);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnNext);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Validate Duplicate Error Messgae Designed By-JYoti
	 */
	public void validateErrorMessage() throws AWTException {

		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		WebElement creditBands = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("CreditBands") + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSWb(creditBands);
		creditBands.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category") + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSWb(category);
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button1 = driver.findElements(tblChosenLst);
		scrollDownForElementJSWb(button1.get(0));
		button1.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, drpType, 120);
		scrollDownForElementJSBy(drpType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpTypeValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * WebElement type =
		 * driver.findElement(By.xpath("//a[@title='"+testData.get("Type")+"']"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); type.click();
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpLeadType, 120);
		scrollDownForElementJSBy(drpLeadType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpLeadType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(newValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * WebElement leadType =
		 * driver.findElement(By.xpath("//a[@title='"+testData.get("LeadType")+"']"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); leadType.click();
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * do{ Robot rob = new Robot(); rob.mouseWheel(5);
		 * }while(!driver.findElement(chkActive).isDisplayed());
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnsave);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(errorMsg)) {
			String msg = driver.findElement(errorMsg).getText();
			tcConfig.updateTestReporter("BusinessRules", "validateErrorMessage", Status.PASS,
					"The error message displayed is :- " + msg);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "validateErrorMessage", Status.FAIL,
					"Failed : error messgae not displayed ");
		}
		clickElementBy(btnCancel);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	/*
	 * Method-Create Credit Band Rule for Hard Stop Designed By-JYoti
	 */
	public void createCreditBandRuleHardStop() throws AWTException {

		WebElement element = driver.findElement(txtCreditBand);
		je.executeScript("arguments[0].scrollIntoView(true);", element);
		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		WebElement creditBands = driver.findElement(By
				.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("CreditBands").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		creditBands.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// clickElementBy(creditBand);
		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement elements = driver.findElement(txtCreditBand);
		je.executeScript("arguments[0].scrollIntoView(true);", elements);
		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button1 = driver.findElements(tblChosenLst);
		button1.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, drpType, 120);
		clickElementJSWithWait(drpType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpTypeValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement type = driver.findElement(By.xpath("//a[@title='" + testData.get("Type").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		type.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpLeadType, 120);
		clickElementJSWithWait(drpLeadType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(newValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement leadType = driver.findElement(By.xpath("//a[@title='" + testData.get("LeadType").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		leadType.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		do {
			Robot rob = new Robot();
			rob.mouseWheel(5);
		} while (!driver.findElement(chkActive).isDisplayed());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, chkActive, 120);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJSWithWait(btnsave);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String msg = driver.findElement(sucessMsg).getText();

		if (verifyObjectDisplayed(tabDetails)) {

			tcConfig.updateTestReporter("BusinessRules", "createCreditBandRuleHardStop", Status.PASS,
					"The Business rule created is :- " + msg);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "createCreditBandRuleHardStop", Status.FAIL,
					"Failed : Business rule not created ");
		}

	}

	
	
	
	/**
	 * 
	 * Added by Ritam
	 * @throws AWTException
	 */
	
	public void createCreditBandSoftStop() throws AWTException {

        waitUntilElementVisibleBy(driver, txtCreditBand, 120);
        WebElement creditBands = driver.findElement(
                     By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("CreditBands") + "']"));
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        scrollDownForElementJSWb(creditBands);
        creditBands.click();
        waitForSometime(tcConfig.getConfig().get("LowWait"));

        List<WebElement> button = driver.findElements(tblChosenLst);
        button.get(1).click();
        waitForSometime(tcConfig.getConfig().get("LowWait"));

        waitUntilElementVisibleBy(driver, txtCreditBand, 120);
        WebElement category = driver.findElement(
                     By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category") + "']"));
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        scrollDownForElementJSWb(category);
        category.click();
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        List<WebElement> button1 = driver.findElements(tblChosenLst);
        scrollDownForElementJSWb(button1.get(0));
        button1.get(0).click();
        waitForSometime(tcConfig.getConfig().get("LowWait"));

        waitUntilElementVisibleBy(driver, drpType, 120);
        scrollDownForElementJSBy(drpType);
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementJSWithWait(drpType);
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementJSWithWait(drpTypeValue);
        /*
        * waitForSometime(tcConfig.getConfig().get("LowWait")); WebElement type =
        * driver.findElement(By.xpath("//a[@title='"+testData.get("Type")+"']"));
        * waitForSometime(tcConfig.getConfig().get("LowWait")); type.click();
        */
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        waitUntilElementVisibleBy(driver, drpLeadType, 120);
        scrollDownForElementJSBy(drpLeadType);
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementJSWithWait(drpLeadType);
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementJSWithWait(newValue);
        /*
        * waitForSometime(tcConfig.getConfig().get("LowWait")); WebElement leadType =
        * driver.findElement(By.xpath("//a[@title='"+testData.get("LeadType")+"']"));
        * waitForSometime(tcConfig.getConfig().get("LowWait")); leadType.click();
        */
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        /*
        * do{ Robot rob = new Robot(); rob.mouseWheel(5);
        * }while(!driver.findElement(chkActive).isDisplayed());
        */
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementJSWithWait(btnsave);
        /*waitForSometime(tcConfig.getConfig().get("LowWait"));
        waitForSometime(tcConfig.getConfig().get("LowWait"));*/
        /*
        * waitUntilElementVisibleBy(driver, sucessMsg, 120); String msg
        * =driver.findElement(sucessMsg).getText();
        */
        waitUntilElementVisibleBy(driver, tabDetails, 120);
        if (verifyObjectDisplayed(tabDetails)) {

               tcConfig.updateTestReporter("BusinessRules", "createCreditBandSoftStop", Status.PASS,
                            "The Business rule created is :- ");
        } else {
               tcConfig.updateTestReporter("BusinessRules", "createCreditBandSoftStop", Status.FAIL,
                            "Failed : Business rule not created ");
        }
 }



	/*
	 * Method-Validate Business Rule Designed By-JYoti
	 */
	public void validateBusinessRule() {

		waitUntilElementVisibleBy(driver, btnEdit, 120);
		String strType = driver.findElement(type).getText();
		if (testData.get("Type").equals(strType)) {
			tcConfig.updateTestReporter("BusinessRules", "validateBusinessRule", Status.PASS,
					"The type created is :- " + strType);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "validateBusinessRule", Status.FAIL,
					"Failed : while creating page");
		}
	}

	/*
	 * Method-Validate Incentive Fields Designed By-JYoti
	 */
	public void validateIncentiveFields() throws AWTException {

		waitUntilElementVisibleBy(driver, txtIncentiveForm, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		do {
			Robot rob = new Robot();
			rob.mouseWheel(3);
		} while (!driver.findElement(chkActive).isDisplayed());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String strcredit = driver.findElement(creditBands).getText();
		String strprofile = driver.findElement(profiles).getText();
		String strcategy = driver.findElement(category).getText();
		String strdays = driver.findElement(noOfDays).getText();
		String strmessgae = driver.findElement(txtMessage).getText();
		tcConfig.updateTestReporter("BusinessRules", "validateIncentiveFields", Status.PASS,
				"The incentive field present are :- " + strcredit + " and" + strprofile + " and" + strcategy + " and"
						+ strdays + " and" + strmessgae);
	}

	/*
	 * Method-Create Business Rule Designed By-JYoti
	 */
	public void createBusinessRule() throws AWTException {
		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		WebElement creditBands = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("CreditBands") + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSWb(creditBands);
		creditBands.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> button = driver.findElements(tblChosenLst);
		scrollDownForElementJSWb(button.get(1));
		button.get(1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category") + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSWb(category);
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button1 = driver.findElements(tblChosenLst);
		scrollDownForElementJSWb(button1.get(0));
		button1.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, drpType, 120);
		scrollDownForElementJSBy(drpType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpTypeValuehrd);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * WebElement type =
		 * driver.findElement(By.xpath("//a[@title='"+testData.get("Type")+"']"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); type.click();
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpLeadType, 120);
		scrollDownForElementJSBy(drpLeadType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpLeadType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(newValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * WebElement leadType =
		 * driver.findElement(By.xpath("//a[@title='"+testData.get("LeadType")+"']"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); leadType.click();
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * do{ Robot rob = new Robot(); rob.mouseWheel(5);
		 * }while(!driver.findElement(chkActive).isDisplayed());
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnsave);
		waitUntilElementVisibleBy(driver, sucessMsg, 120);
		String strmsg = driver.findElement(sucessMsg).getText();
		if (verifyObjectDisplayed(tabDetails)) {

			tcConfig.updateTestReporter("BusinessRules", "createBusinessRule", Status.PASS,
					"The Business rule created is :- " + strmsg);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "createBusinessRule", Status.FAIL,
					"Failed : Business rule not created ");
		}
	}

	/*
	 * Method-Create Country Rule Designed By-JYoti
	 */
	public void createCountryRule() {
		waitUntilElementVisibleBy(driver, txtCountryQua, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtCategory, 120);
		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button1 = driver.findElements(tblChosenLst);
		button1.get(2).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnsave);
		waitUntilElementVisibleBy(driver, sucessMsg, 120);
		String strmsg = driver.findElement(sucessMsg).getText();
		if (verifyObjectDisplayed(tabDetails)) {
			tcConfig.updateTestReporter("BusinessRules", "createCountryRule", Status.PASS,
					"The Country Qualification rule created is :- " + strmsg);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "createCountryRule", Status.FAIL,
					"Failed : Country Qualification rule not created ");
		}
	}

	/*
	 * Method-Create Inentive Rule Designed By-JYoti
	 */
	public void createIncentiveRule() throws AWTException {

		WebElement element = driver.findElement(creditBands);
		je.executeScript("arguments[0].scrollIntoView(true);", element);
		WebElement creditBands = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("CreditBands") + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		creditBands.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(2).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		WebElement band = driver.findElement(category);
		je.executeScript("arguments[0].scrollIntoView(true);", band);
		waitUntilElementVisibleBy(driver, category, 120);

		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category") + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> button1 = driver.findElements(tblChosenLst);
		button1.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> elements = driver.findElements(selectOwner);
		System.out.println("Number of elements:" + elements.size());
		elements.get(elements.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> lst = driver.findElements(tblChosenLst);
		lst.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * do{ Robot rob = new Robot(); rob.mouseWheel(2);
		 * }while(!driver.findElement(chkActive).isDisplayed());
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnsave);
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, sucessMsg, 120);
		String strmsg = driver.findElement(sucessMsg).getText();
		if (verifyObjectDisplayed(tabDetails)) {

			tcConfig.updateTestReporter("BusinessRules", "createIncentiveRule", Status.PASS,
					"The Business rule created is :- " + strmsg);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "createIncentiveRule", Status.FAIL,
					"Failed : Business rule not created ");
		}
		waitUntilElementVisibleBy(driver, detailsTab, 120);
		String strvalues = driver.findElement(valueCreditBand).getText();
		if (testData.get("CreditBands").equalsIgnoreCase(strvalues)) {
			tcConfig.updateTestReporter("BusinessRules", "createIncentiveRule", Status.PASS,
					"The Band present in screen is :- " + strvalues);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "createIncentiveRule", Status.FAIL,
					"Failed : while creating page");
		}

	}

	/*
	 * Method-Create Credit Band Rule for Owner/Lead Designed By-JYoti
	 */
	public void createCreditBandRuleHardStop_Owner() throws AWTException {

		WebElement element = driver.findElement(txtCreditBand);
		je.executeScript("arguments[0].scrollIntoView(true);", element);
		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		WebElement creditBands = driver.findElement(By
				.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("CreditBands").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		creditBands.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement band = driver.findElement(txtCategory);
		je.executeScript("arguments[0].scrollIntoView(true);", band);
		waitUntilElementVisibleBy(driver, txtCategory, 120);
		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button1 = driver.findElements(tblChosenLst);
		button1.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpType, 120);
		clickElementJSWithWait(drpType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(drpTypeValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement type = driver.findElement(By.xpath("//a[@title='" + testData.get("Type").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		type.click();
		waitUntilElementVisibleBy(driver, drpLeadType, 120);
		clickElementJSWithWait(drpLeadType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(newValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement leadType = driver.findElement(By.xpath("//a[@title='" + testData.get("LeadType").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		leadType.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		do {
			Robot rob = new Robot();
			rob.mouseWheel(5);
		} while (!driver.findElement(chkActive).isDisplayed());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, chkActive, 120);
		clickElementJSWithWait(btnsave);
		waitUntilElementVisibleBy(driver, sucessMsg, 120);
		String strmsg = driver.findElement(sucessMsg).getText();
		if (verifyObjectDisplayed(sucessMsg)) {

			tcConfig.updateTestReporter("BusinessRules", "createCreditBandRuleHardStop_Owner", Status.PASS,
					"The Business rule created is :- " + strmsg);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "createCreditBandRuleHardStop_Owner", Status.FAIL,
					"Failed : Business rule not created ");
		}
	}

	/*
	 * Method-Delete Duplicate Credit Rule Designed By-JYoti
	 */
	public void deleteRule_CreditRule() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtCreditBandchkd, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> elements = driver.findElements(btnEditRule);
		System.out.println("Number of elements:" + elements.size());
		elements.get(elements.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(selectList);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(delRule);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtDelRule, 120);
		clickElementJSWithWait(btnDelete);
		waitUntilElementVisibleBy(driver, delMsg, 120);
		String str = driver.findElement(delMsg).getText();
		if (verifyObjectDisplayed(delMsg)) {
			tcConfig.updateTestReporter("BusinessRules", "deleteRule_CreditRule", Status.PASS,
					"The Business rule created is :- " + str);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "deleteRule_CreditRule", Status.FAIL,
					"Failed : Business rule not deleted ");
		}
	}

	/*
	 * Method-Delete Country Rule Designed By-JYoti
	 */
	public void deleteRule_CountryRule() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtCountryRuleChkd, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> elements = driver.findElements(btnEditCounrtey);
		System.out.println("Number of elements:" + elements.size());
		elements.get(elements.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(selectList);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(delRule);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtDelRule, 120);
		clickElementJSWithWait(btnDelete);
		waitUntilElementVisibleBy(driver, delMsg, 120);
		String strmsg = driver.findElement(delMsg).getText();
		if (verifyObjectDisplayed(delMsg)) {
			tcConfig.updateTestReporter("BusinessRules", "deleteRule_CountryRule", Status.PASS,
					"The Business rule deleted is :- " + strmsg);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "deleteRule_CountryRule", Status.FAIL,
					"Failed : Business rule not deleted ");
		}
	}

	/*
	 * Method-Navigate Back to Business Rule page Designed By-JYoti
	 */
	public void navigateBack() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().back();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnCancel, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnCancel);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	/*
	 * Method-Create Country Rule , Add another Country Designed By-JYoti
	 */
	public void createCountryRule_AddCountry() {
		waitUntilElementVisibleBy(driver, txtCountryQua, 120);
		WebElement creditBands = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Country").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		creditBands.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> lstbutton = driver.findElements(tblChosenLst);
		lstbutton.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtCategory, 120);
		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category") + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSWb(category);
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> lstbutton1 = driver.findElements(tblChosenLst);
		scrollDownForElementJSWb(lstbutton1.get(2));
		lstbutton1.get(2).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> lstelements = driver.findElements(secSelect);
		System.out.println("Number of elements:" + lstelements.size());
		lstelements.get(lstelements.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> lst = driver.findElements(tblChosenLst);
		scrollDownForElementJSWb(lst.get(2));
		lst.get(2).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnsave);

	}

	/*
	 * Method-Validate sucess Message Designed By-JYoti
	 */
	public void validateSucessMsg() {
		waitUntilElementVisibleBy(driver, sucessMsg, 120);
		String str = driver.findElement(sucessMsg).getText();
		if (verifyObjectDisplayed(tabDetails)) {
			tcConfig.updateTestReporter("BusinessRules", "validateSucessMsg", Status.PASS,
					"The Country Qualification rule created is :- " + str);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "validateSucessMsg", Status.FAIL,
					"Failed : Country Qualification rule not created ");
		}
	}

	/*
	 * Method-Validate Duplicate Error Messgae Designed By-JYoti
	 */
	public void validateDuplicateErrorMsg() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(errorMsg)) {
			String str = driver.findElement(errorMsg).getText();
			tcConfig.updateTestReporter("BusinessRules", "validateDuplicateErrorMsg", Status.PASS,
					"The error message displayed is :- " + str);
		} else {
			tcConfig.updateTestReporter("BusinessRules", "validateDuplicateErrorMsg", Status.FAIL,
					"Failed : error messgae not displayed ");
		}
		clickElementBy(btnCancel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Validate Duplicate Incentive Rule Designed By-JYoti
	 */
	public void validateDuplicateIncentive() throws AWTException {
		WebElement element = driver.findElement(creditBands);
		je.executeScript("arguments[0].scrollIntoView(true);", element);
		WebElement creditBands = driver.findElement(By
				.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("CreditBands").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		creditBands.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(2).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		WebElement band = driver.findElement(category);
		je.executeScript("arguments[0].scrollIntoView(true);", band);
		waitUntilElementVisibleBy(driver, category, 120);

		WebElement category = driver.findElement(
				By.xpath("//span[@class='slds-media__body']//span[@title='" + testData.get("Category").trim() + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		category.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> button1 = driver.findElements(tblChosenLst);
		button1.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		do {
			Robot rob = new Robot();
			rob.mouseWheel(2);
		} while (!driver.findElement(chkActive).isDisplayed());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnsave);
	}

}
