package salesforce.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;;


public class ReservationPage extends SalesforceBasePage {
//	LeadsPage leadPage = new LeadsPage(tcConfig);
	public static final Logger log = Logger.getLogger(ReservationPage.class);

	public ReservationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	
	public static String guestId="";
	public static String createdResNumber;
	public static String createdCase;
	public static String Checkindate;
	static final String SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String strStatus;
	JavascriptExecutor je = (JavascriptExecutor) driver;
	protected By btnIncentiveEligible = By.xpath("//button[contains(.,'Incentive Eligible')]");
    protected By qualificationScore  = By.xpath("//span[contains(.,'Qualification Status')]/../..//lightning-formatted-text[contains(.,'')]");
    protected By btnincentive = By.xpath("(//button[contains(.,'Incentive Eligible') and @id='tourCheckBTN'])[2]");
    protected By txtACSMobile = By.xpath("//span[contains(@class,'slds-form-element__label slds-truncate') and contains(@title,'ACS')]/../..//span[@class='uiOutputTextArea']");
    protected By softScoreMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By BtnbookTour = By.xpath("(//button[contains(text(),'Book Tour')])[2]");
	protected By latestTour_new = By.xpath("//span[text()='Tour Record']//a//div[@class='slds-truncate']");
	protected By clickReservMobile = By.xpath("(//a[@class='slds-show slds-truncate'])[1]");
	protected By imgMultiRes = By.xpath("//span[contains(.,'Multiple Res.')]/../..//img");
    protected By imgExtendedStay = By.xpath("//span[contains(.,'Extended Stay')]/../..//img"); 		
    protected By relatedResrvMobile = By.xpath("//span[contains(.,'Related Reservation')]/../..//span[@class='uiOutputTextArea']");
    protected By tabGuestDetails = By.xpath("//span[contains(.,'Guest Details')]");
    protected By btnYes= By.xpath("//button[contains(.,'YES')]");
	protected By relatedResrv = By.xpath("(//span[contains(.,'Related Reservation')])[1]");
    protected By relatedResrvations = By.xpath("//span[@title='Related Reservations']");
    protected By tabAddressDetails = By.xpath("//span[contains(.,'Address Details')]");
    protected By relatedResrvFields = By.xpath("//span[contains(.,'Related Reservation')]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
    protected By lstRelatedResrv = By.xpath("//tbody//th//span[@data-aura-class='forceOutputFormulaHtml']//a");
    protected By chkMultiRes = By.xpath("//input[@name='TMMultipleReservations__c']/../..//label//span[@class='slds-checkbox_faux']");
	protected By btnConfirmTour = By.xpath("//button[contains(.,'CREATE TOUR')]");
	protected By hardStop_Mobile = By.xpath("//div[@class='slds-modal__content slds-p-around_medium slds-text-align_left']");
    protected By btnCancel_Mobile = By.xpath("//button[contains(.,'CANCEL')]");
	protected  By btnBookTour_Moblie = By.xpath("//button[contains(.,'BOOK TOUR')]");
	protected By bntCancel = By.xpath("(//button[contains(.,'Cancel')])[2]");
	protected By errorMsg = By.xpath("//span[@class='toastMessage forceActionsText']");
	protected By txtReservation_Native = By.xpath("//div[@class='entityNameTitle slds-line-height--reset']");
	protected By btnIncentiveReserv = By.xpath("//button[contains(.,'Incentive Eligible')]");
	protected By softscore = By.xpath("//button[contains(.,' Request Soft Score ')]");
	protected By txtACS = By.xpath("//p[@title='ACS']/../..//lightning-formatted-text");
	protected By txtACSOwner = By.xpath("(//p[@title='ACS']/../..//lightning-formatted-text)[2]");
	private By newBtn= By.xpath("//div[@title='New']");
	private By nextBtn= By.xpath("//span[text()='Next']");
	private By agentEdit=By.xpath("//input[contains(@title,'People')]");
	private By ResNumber=By.xpath("//label/span[text()='Res Number']/../../input");
	private By txtEmail=By.xpath("//input[@type='email']");
	private By txtFirstName=By.xpath("//span[contains(text(),'First Name')]/../../input");
	private By txtLastName=By.xpath("//span[contains(text(),'Last Name')]/../../input");
	private By txtTravelChannel=By.xpath("//span[text()='Travel Channel']/../../div//a[@role='button']");
	private By strReservationStatus=By.xpath("//span[text()='Reservation Status']/../..//a[@role='button']");
	private By saveBtn= By.xpath("//button[contains(@class,'forceActionButton')]/span[text()='Save']");
	private By chkBoxSameAsPrimaryAddress = By.xpath("//label[text()='Same as Primary Guest Address']/..//input");
	private By btnAddAdditionalGuest = By.xpath("//button[text()='Add Additional Guest']");
	private By additionalGuestID=By.xpath("//span[contains(.,'was created.')]/a/div");
	private By ownerSearchBoxEdit=By.xpath("//input[contains(@name,'search-input')]");
	private By bookTourBtn=By.xpath("//div//button[text()='Book Tour']");
	private By globalSearchTxtBox=By.xpath("//input[@title='Search Salesforce']");
	
	private By popUpSaveBtn=By.xpath("//button[text()='Save']");
	private By tourlanuageDrpDwn=By.xpath("//span[text()='Tour Language']/../..//a");
	private By tourLangValue=By.xpath("//li/a[text()='English']");
	private By commentsEdit=By.xpath("//Label[text()='Comments']/../..//input[@type='text']");
	private By addAdditionalGuestBtn=By.xpath("//button[text()='Add Additional Guest']");
	private By firstName=By.xpath("//label[text()='First Name']/..//input");
	private By lastName=By.xpath("//label[text()='Last Name']/..//input");
	private By Street=By.xpath("//label[text()='Street']/..//input");
	private By City=By.xpath("//label[text()='City']/..//input");
	private By State=By.xpath("//label[text()='State']/..//input");
	private By Country=By.xpath("//label[text()='Country']/..//input");
	private By Postal=By.xpath("//label[text()='Postal Code']/..//input");
	
	
	//private By pidTxt=By.xpath("//span[text()='PID']/../../input");
	private By addGuestsaveBtn=By.xpath("//button[text()='Save']");
	private By txtPhoneNum=By.xpath("//label[text()='Phone']/..//input");
	private By txtEmailAddress=By.xpath("//label[text()='Email Name']/..//input");
	private By additionalGuestLbl=By.xpath("//span[text()='Additional Guests']/../span[contains(text(),'(0)')]");
	private By createLeadBtn=By.xpath("//button[text()='Create Lead']");
	private By txtLeadFirstName=By.xpath("//label[text()='First Name']/..//input");
	private By txtLeadLastName=By.xpath("//label[text()='Last Name']/..//input");
	private By txtMobile=By.xpath("//label[text()='Mobile #']/..//input");
	private By txtLeadEmail=By.xpath("//label[text()='Email']/..//input");
	private By txtZipCode=By.xpath("//label[text()='Zip']/..//input");
	private By createCustomerBtn=By.xpath("//button[text()='Create Customer']");
	
	private By leadMaritalBtn=By.xpath("(//h2[text()='Marital Status']/..//span)[1]");
	private By householdBtn=By.xpath("(//h2[text()='Household Income']/..//span)[1]");
	private By ageCustBtn=By.xpath("(//h2[text()='Age of Customer']/..//span)[1]");
	
	private By newCreatedTour=By.xpath("(//span[text()='Tour Records']/../../../../../..//a[@data-refid='recordId'])[1]");
	private By expandTourAction=By.xpath("//div[contains(@class,'forceVirtualAction')]//a");
	private By deleteBtn=By.xpath("(//a[@title='Delete'])[1]");
	private By deleteTourBtn=By.xpath("//span[text()='Delete']");
	private By qualificationStatus=By.xpath("//span[contains(text(),'Qualification')]/parent::span/following-sibling::div//a");
	private By deleteIcon=By.xpath("//span[@class='deleteIcon']");
	
	private By guestFirstNameEdit=By.xpath("//span[text()='Primary Guest First Name']/../..//input");
	private By guestLastNameEdit=By.xpath("//span[text()='Primary Guest Last Name']/../..//input");
	private By guestEditSaveBtn=By.xpath("//button[@title='Save']");
	private By newGuestName=By.xpath("//h1//div//div//a[@data-refid='recordId']");
	
	private By createChangeReq=By.xpath("//div[@title='Create Change Request'] | //button[contains(@name,'CreateChangeRequest')]");
	private By changeReqType=By.xpath("//span[text()='Change Request Type']/../..//a");
	private By changeReqTypeMarketer=By.xpath("//a[@class='select']");
	private By changeReqReason=By.xpath("//span[text()='Change Request Reason']/../..//input");
	private By changeReqSave=By.xpath("//div[@class='modal-footer slds-modal__footer']//span[text()='Save']");
	private By caseCreated=By.xpath("//span[text()='Case']//div");
	//private By caseCreated=By.xpath("(//span[text()='Cases'])[2]");
	private By userDD=By.xpath("//img[@title='Users']");
	private By user=By.xpath("//span[@title='Queues']");
	private By searchMaketer=By.xpath("//input[@title='Search Queues']");
	private By homeIcon=By.xpath("//li[@data-menu-item-id='0']");
	private By editButton=By.xpath("//button[contains(@title,'Edit Guest Name (Lead)')]");
	private By travelingchanel=By.xpath("//span[text()='Travel Channel']/../../div[2]/span/slot/slot/lightning-formatted-text");
	private By arrivalDetails=By.xpath("//span[text()='Arrival Details']");
	private By tempPage=By.xpath("(//span[text()='Temp'])[1]");
	private By guistFirstName=By.xpath("//input[contains(@name,'PrimaryGuestFirstName')]");
	private By guistLastName=By.xpath("//input[contains(@name,'PrimaryGuestLastName')]");
	private By saveButton=By.xpath("//button[text()='Save']");
	private By Updatename=By.xpath("//span[text()='Guest Name (Lead)']/ancestor::div[contains(@class,'parent')]//a");
	private By guistDetailTab=By.xpath("//span[text()='Guest Details']");
	private By marktingAgentsearchBox=By.xpath("//input[@title='Search People']");
	private By mrktUserWVOlink=By.xpath("//div[@title='WVO Corporate Super User']");
	private By Ownerupdatename=By.xpath("//span[text()='Guest Name (Owner)']/../../div/span/slot/slot/force-lookup/div/force-hoverable-link/div/a");
	protected By txtReservationSearch = By.xpath("//input[contains(@placeholder,'Search Reservations and more...')]");
    protected By imgResrv = By.xpath("//span[@class='uiImage']//img[@class='icon ']");
	protected By reservationNum = By.xpath("(//a[@class='slds-show slds-truncate'])[1]");
	public static String createdTourID;
	protected By inputSearchTourRes = By.xpath("//input[contains(@placeholder,'Number')]");
	protected By drpDwnTourOrRes = By.xpath("//select[@name='Dropdown']");
	protected By searchReservationNum = By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']");
	
	
	public void edit_NonOwner_Reservation_Details(){
		String PreOwnerguestName = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(verifyObjectDisplayed(travelingchanel)){
			waitUntilElementVisibleBy(driver, travelingchanel, 120);
			WebElement travelChanel=driver.findElement(travelingchanel);
			String chanel=travelChanel.getText();
			System.out.println(chanel);
			tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.PASS, 
					"User is able get travel channel before editing: "+chanel);
		}else{
			tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.FAIL, 
					"User is not able get travel channel ");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(guistDetailTab);
		scrollUpByPixel(200);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, guistDetailTab, 120);
		if(verifyObjectDisplayed(guistDetailTab)){
			 PreOwnerguestName=driver.findElement(Updatename).getText();
			tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.PASS, 
					"User is able get Guest Name before editing: "+PreOwnerguestName);
		}
		else{
			tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.FAIL, 
					"User is not able get Guest Details TAB");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(800);
		waitUntilElementVisibleBy(driver, editButton, 120);
		if(verifyObjectDisplayed(editButton)){
			waitUntilElementVisibleBy(driver, editButton, 120);
			clickElementJSWithWait(editButton);
			tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.PASS, 
					"User is able to click Edit Button");
		}else{
			tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.FAIL, 
					"User is not able to click Edit Button");
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, arrivalDetails, 120);
		if(verifyObjectDisplayed(arrivalDetails)){
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, tempPage, 120);
			scrollDownForElementJSBy(tempPage);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(guistFirstName).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String First=randomAlphaNumeric(5);
			driver.findElement(guistFirstName).sendKeys(First);
			//fieldDataEnter(guistFirstName, First);
			waitUntilElementVisibleBy(driver, guistLastName, 120);
			driver.findElement(guistLastName).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Last=randomAlphaNumeric(5);
			fieldDataEnter(guistLastName, Last);
			driver.findElement(guistLastName).sendKeys(Keys.TAB);
			waitUntilElementVisibleBy(driver, saveButton, 120);
			clickElementJSWithWait(saveButton);

			tcConfig.updateTestReporter("ReservationPage", "changeGuistName", Status.PASS, 
					"User is  able to change Guest Details and save");
		}else{
			tcConfig.updateTestReporter("ReservationPage", "changeGuistName", Status.FAIL, 
					"User is not able to change Guist name and save");
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(guistDetailTab);
		scrollUpByPixel(200);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if(verifyObjectDisplayed(guistDetailTab)){
			String PostguestOwnerUpdateName=driver.findElement(Updatename).getText();
			if (PostguestOwnerUpdateName.equals(PreOwnerguestName)) {
				tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.FAIL, 
						"Failed to update Guest Name");
			}else{
				
				tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.PASS, 
						"User Name after editing: "+PostguestOwnerUpdateName);
			}
		}
		else{
			tcConfig.updateTestReporter("ReservationPage", "edit_NonOwner_Reservation_Details", Status.FAIL, 
					"Guest Details Tab not diaplayed");
		}	
	}
	
	public void edit_Owner_Reservation_Details(){
		String PreOwnerguestName = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(verifyObjectDisplayed(travelingchanel)){
			waitUntilElementVisibleBy(driver, travelingchanel, 120);
			WebElement travelChanel=driver.findElement(travelingchanel);
			String chanel=travelChanel.getText();
			System.out.println(chanel);
			tcConfig.updateTestReporter("ReservationPage", "TravelChannelvisible", Status.PASS, 
					"User is able get travel channel before editing"+chanel);
		}else{
			tcConfig.updateTestReporter("ReservationPage", "TravelChannelvisible", Status.FAIL, 
					"User is not able get travel channel");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(guistDetailTab);
		scrollUpByPixel(200);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, guistDetailTab, 120);
		if(verifyObjectDisplayed(guistDetailTab)){
			 PreOwnerguestName=driver.findElement(Updatename).getText();
			tcConfig.updateTestReporter("ReservationPage", "updateOwnerGuistName", Status.PASS, 
					"User is able get OwnerGuist Name before editing: "+PreOwnerguestName);
		}
		else{
			tcConfig.updateTestReporter("ReservationPage", "updateOwnerGuistName", Status.FAIL, 
					"User is able get OwnerGuist Name before editing");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(800);
		waitUntilElementVisibleBy(driver, editButton, 120);
		if(verifyObjectDisplayed(editButton)){
			clickElementJSWithWait(editButton);
			tcConfig.updateTestReporter("ReservationPage", "clickEditButton", Status.PASS, 
					"User is able to click click Button");
		}else{
			tcConfig.updateTestReporter("ReservationPage", "clickEditButton", Status.FAIL, 
					"User is not able to click click Button");
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, arrivalDetails, 120);
		if(verifyObjectDisplayed(arrivalDetails)){
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, tempPage, 120);
			scrollDownForElementJSBy(tempPage);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(guistFirstName).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String First=randomAlphaNumeric(5);
			driver.findElement(guistFirstName).sendKeys(First);
			//fieldDataEnter(guistFirstName, First);
			waitUntilElementVisibleBy(driver, guistLastName, 120);
			driver.findElement(guistLastName).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Last=randomAlphaNumeric(5);
			fieldDataEnter(guistLastName, Last);
			driver.findElement(guistLastName).sendKeys(Keys.TAB);
			waitUntilElementVisibleBy(driver, saveButton, 120);
			clickElementJSWithWait(saveButton);
			
			tcConfig.updateTestReporter("ReservationPage", "changeGuistName", Status.PASS, 
					"User is able to enter new OwnerGuest Name and save");
		}else{
			tcConfig.updateTestReporter("ReservationPage", "changeGuistName", Status.FAIL, 
					"User is not able to change OwnerGuist name and save");
		}
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(guistDetailTab);
		scrollUpByPixel(200);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if(verifyObjectDisplayed(guistDetailTab)){
			String PostguestOwnerUpdateName=driver.findElement(Updatename).getText();
			if (PostguestOwnerUpdateName.equals(PreOwnerguestName)) {
				tcConfig.updateTestReporter("ReservationPage", "updateOwnerGuistName", Status.FAIL, 
						"Failed to update Guest Name");
			}else{
				
				tcConfig.updateTestReporter("ReservationPage", "updateOwnerGuistName", Status.PASS, 
						"User Name after editing: "+PostguestOwnerUpdateName);
			}
			
		}
		else{
			tcConfig.updateTestReporter("ReservationPage", "updateOwnerGuistName", Status.FAIL, 
					"Guest Details Tab not diaplayed");
		}	
	}
	public void CreateReservation(){
		waitUntilElementVisibleBy(driver, newBtn, 120);
		
		if(verifyObjectDisplayed(newBtn)) {
			tcConfig.updateTestReporter("ReservationPage", "CreateReservation", Status.PASS, 
					"User is navigated to Reservation creation page");
			clickElementBy(newBtn);
			String strLeadType=testData.get("ReservationType");
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			WebElement leadTypeRadioBtn = null;
			if (strLeadType.equalsIgnoreCase("Pre-Lead")){
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				leadTypeRadioBtn=driver.findElement(By.xpath("(//input[contains(@class,'uiInputRadio')])[3]"));
			}else if(strLeadType.equalsIgnoreCase("Non-Owner")){
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				leadTypeRadioBtn=driver.findElement(By.xpath("(//input[contains(@class,'uiInputRadio')])[2]"));
			}else if(strLeadType.equalsIgnoreCase("Owner")){
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				leadTypeRadioBtn=driver.findElement(By.xpath("(//input[contains(@class,'uiInputRadio')])[1]"));
			}
			waitUntilElementVisible(driver, leadTypeRadioBtn, 120);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
//			leadTypeRadioBtn.click();
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", leadTypeRadioBtn);
			clickElementBy(nextBtn);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			fieldDataEnter(agentEdit, testData.get("AgentName"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(By.xpath("//div[@title='"+testData.get("AgentName")+"']")).click();
			createdResNumber="AUTO_RES"+getRandomString(6);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			fieldDataEnter(ResNumber,createdResNumber );
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			fieldDataEnter(txtEmail,testData.get("Email") );
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			fieldDataEnter(txtFirstName,testData.get("FirstName"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			fieldDataEnter(txtLastName,testData.get("LastName"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(txtTravelChannel);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement travelChannelOpt=driver.findElement(By.xpath("//li/a[text()='"+testData.get("TravelChannel")+"']"));
			travelChannelOpt.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(strReservationStatus);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement resStatusOption=driver.findElement(By.xpath("//li/a[text()='"+testData.get("ReservationStatus")+"']"));
			resStatusOption.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(saveBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			driver.findElement(By.xpath("//input[@placeholder='Search Reservations and more...']")).sendKeys(createdResNumber);
			driver.findElement(By.xpath("//input[@placeholder='Search Reservations and more...']")).sendKeys(Keys.ENTER);
			
				tcConfig.updateTestReporter("ReservationPage", "CreateReservation", Status.PASS, "The reservation is created");
			
		}else{
			tcConfig.updateTestReporter("ReservationPage", "CreateReservation", Status.FAIL, "User is unable to navigate to reservation creation page");
		}
	
	}
	
	
	
	public String createAditionalGuest()	{
		waitUntilObjectVisible(driver, btnAddAdditionalGuest, 120);
		List<WebElement> ele = driver.findElements(btnAddAdditionalGuest);
		clickElementWb(ele.get(ele.size()-1));
		/*String strGuestType=testData.get("GuestType");
		WebElement GuestTypeRadioBtn = null;
		waitUntilObjectVisible(driver, ownerRadioBtn, 120);
		if (strGuestType.equalsIgnoreCase("Owner")){
			GuestTypeRadioBtn=driver.findElement(ownerRadioBtn);
		}else if(strGuestType.equalsIgnoreCase("Lead")){
			GuestTypeRadioBtn=driver.findElement(By.xpath("(//label[@class='slds-radio']//span[contains(@class,'slds-radio')])[2]"));
		}else if(strGuestType.equalsIgnoreCase("Pre-Lead")){
			GuestTypeRadioBtn=driver.findElement(By.xpath("(//label[@class='slds-radio']//span[contains(@class,'slds-radio')])[3]"));
		}
		waitUntilElementVisible(driver, GuestTypeRadioBtn, 120);
		GuestTypeRadioBtn.click();*/
		waitUntilObjectVisible(driver, firstName, 120);
		fieldDataEnter(firstName, testData.get("FirstName"));
		fieldDataEnter(lastName, testData.get("LastName"));
		String PID = getRandomString(6);
		clickElementBy(chkBoxSameAsPrimaryAddress);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//fieldDataEnter(pidTxt, PID);
		clickElementBy(popUpSaveBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> guest = driver.findElements(additionalGuestID);
		if(guest.get(0).isDisplayed()){
			//WebElement addGuestCreated = driver.findElement(additionalGuestID);
			 guestId = guest.get(0).getText();
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createOwnerAditionalGuest", Status.PASS, "Additional guest has been created successfully "
					+ "with Guest ID: " + guestId + " and PID: " + PID);				
		}else{
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createOwnerAditionalGuest", Status.FAIL, "Additional guest NOT created successfully ");	
		}
		
		return guestId;
}


	
	public void unlinkTourFromReservation() throws AWTException{
		String tourNo=null;
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().back();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		do{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,250)");
			
			
		}while(!driver.findElement(By.xpath("(//span[@class='view-all-label'])[1]")).isDisplayed());
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("(//span[@class='view-all-label'])[1]")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		
		
		
		/*if(verifyObjectDisplayed(newCreatedTour)){
			tourNo=driver.findElement(newCreatedTour).getAttribute("text");
			tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.PASS, "New Tour "
					+ " has been created successfully : "+ tourNo);
			System.out.println(driver.findElement(newCreatedTour).getAttribute("text"));
		}else{
			tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.FAIL, "Failed to create New Tour");
		}*/
		
		
		String totalitem=driver.findElement(By.xpath("//div//span[@class='countSortedByFilteredBy']")).getText();
		String[] num= totalitem.split(" ");
		System.out.println(num[0]);		
		driver.findElement(By.xpath("//tr[10]//span//span[text()='Cancelled' or text()='Draft' or text()='Booked']")).click();
		String expand="//tr["+num[0]+"]"+"//div[contains(@class,'forceVirtualAction')]//a";
//		List<WebElement> expadIconList= driver.findElements(expandTourAction);
			
		for(int i=11;i<=Integer.parseInt(num[0]);i++){
			
			Actions action = new Actions(driver);
			action.sendKeys(Keys.ARROW_DOWN).build().perform();
			
			
		}
		
		List<WebElement> expadIconList= driver.findElements(By.xpath(expand));
		System.out.println(expadIconList.size());
	

		do{
			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("window.scrollBy(0,750)");
			
			
		}while(!expadIconList.get(expadIconList.size()-1).isDisplayed());
		
		
		expadIconList.get(expadIconList.size()-1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(deleteBtn);
		waitUntilObjectVisible(driver, deleteTourBtn, 120);
		clickElementBy(deleteTourBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(newCreatedTour)){
			if(driver.findElement(newCreatedTour).getAttribute("text").equalsIgnoreCase(tourNo)){
				tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.FAIL, "Unable to unlink tour");
			}else{
				tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.PASS, "Tour unlinked successfully");
			}
		}else{
			tcConfig.updateTestReporter("ReservationPage", "unlinkTourFromReservation", Status.PASS, "Tour unlinked successfully");
		}
	}
	
	private By checkInDates_updt=By.xpath("(//span[text()='Check In Date']/../../div[2]//span)[3]");
	private By checkInAndCheckOutTab = By.xpath("//span[text()='Check In & Check Out Details']/..");
	
	public void ValidateResevationZeroNightStay_Corp() throws ParseException{
		
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		/*By resNum = By.xpath("//span[@title='"+ testData.get("ReservationNumber") +"']/../..");
		//By ResNum=By.xpath("//a[@title='"+ testData.get("ReservationNumber") +"']");
		waitUntilElementVisibleBy(driver, resNum, 120);
		List<WebElement> ReserveNum=driver.findElements(resNum);
		ReserveNum.get(ReserveNum.size()-1).click();
//		waitUntilElementVisibleBy(driver, ResNum, 120);
*/			
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(!(driver.findElements(checkInDates_updt).size()>0 && driver.findElement(checkInDates_updt).isDisplayed()))
		{
			clickElementBy(checkInAndCheckOutTab);
		}
		//List<WebElement> Dates=driver.findElements(By.xpath("//span[@data-aura-class='uiOutputDate']"));
		
		//Collections.reverse(Dates);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		 //Checkindate=Dates.get(1).getText();
		Checkindate = driver.findElement(By.xpath("(//span[text()='Check In Date']/../../div[2]//span)[3]//slot//slot//lightning-formatted-text")).getText();
		String Checkoutdate = driver.findElement(By.xpath("(//span[text()='Check Out Date']/../../div[2]//span)[3]//slot//slot//lightning-formatted-text")).getText();
		System.out.println(Checkoutdate);
		//String Checkoutdate=Dates.get(0).getText();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		SimpleDateFormat sdf= new SimpleDateFormat("mm/dd/yyyy");
		Date Datein=sdf.parse(Checkindate);
		Date Dateout=sdf.parse(Checkoutdate);
		if(Datein.compareTo(Dateout)==0){
			
			tcConfig.updateTestReporter("ReservationPage", "ValidateResevationZeroNightStay",
					Status.PASS, "The Check in Date and Check out date is same  i.e.  "+ Datein);
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			//ReserveNum.get(ReserveNum.size()-1).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			if(testData.get("User").contains("Business") )
			{
				strStatus=driver.findElement(status1).getText();
			}
			else if(testData.get("User").contains("Corporate") )
			{
				strStatus=driver.findElement(status1).getText();
			}
			else if(testData.get("User").contains("Marketing Admin") )
			{
				strStatus=driver.findElement(status1).getText();
			}
			else if(testData.get("User").contains("Tour Reception") )
			{
				strStatus=driver.findElement(status1).getText();
			}
			else
			{
				strStatus=driver.findElement(status1).getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			
			
			
			if(strStatus.contains("U")){
				
				tcConfig.updateTestReporter("ReservationPage", "ValidateResevationZeroNightStay",
						Status.PASS, "The A/U is  "+ strStatus);
				
			}else{
				
				tcConfig.updateTestReporter("ReservationPage", "ValidateResevationZeroNightStay",
						Status.FAIL, "The A/U is  "+ strStatus);
				
			}
			
		}
		
		
		
		
	}
	
	
	public void searchReservation(){
		
		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		
			tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "User is navigated to Reservations Page");
		
		fieldDataEnter(ownerSearchBoxEdit, testData.get("ReservationNumber"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));		
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LongWait"));		
		WebElement seacrhResOwnerLink=driver.findElement(By.xpath("//th//span//a[contains(@title,'"+testData.get("ReservationNumber")+"')]"));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		seacrhResOwnerLink.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "Reservations is accessed");
		
	}
	
	private By globalSearch=By.xpath("//input[contains(@placeholder,'Search ')]");
	public void seacrhReservationBookTour(){
		
		waitUntilElementVisibleBy(driver, globalSearch, 120);
		if (verifyObjectDisplayed(globalSearch)){
		tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "User is navigated to Reservations Page");
	
	fieldDataEnter(globalSearch, testData.get("ReservationNumber"));
	waitForSometime(tcConfig.getConfig().get("MedWait"));		
	driver.findElement(globalSearch).sendKeys(Keys.ENTER);
	waitForSometime(tcConfig.getConfig().get("MedWait"));		
	List<WebElement> seacrhResOwnerLink=driver.findElements(By.xpath("(//th//span//a[contains(@title,'"+testData.get("ReservationNumber")+"')])"));
	waitForSometime(tcConfig.getConfig().get("MedWait"));
	seacrhResOwnerLink.get(seacrhResOwnerLink.size()-1).click();
	waitForSometime(tcConfig.getConfig().get("MedWait"));
	tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "Reservations is accessed");
	
		
		/*waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)){
			tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "User is navigated to Reservations Page");
		
		fieldDataEnter(ownerSearchBoxEdit, testData.get("ReservationNumber"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));		
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LongWait"));		
		WebElement seacrhResOwnerLink=driver.findElement(By.xpath("//th//span//a[contains(@title,'"+testData.get("ReservationNumber")+"')]"));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		seacrhResOwnerLink.click();*/
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, bookTourBtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("AdditionalGuest").equalsIgnoreCase("Y")){
			addAdditionalGuest();
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
//		clickElementBy(bookTourBtn);
		WebElement book = driver.findElement(bookTourBtn);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", book);
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		
		waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);
		if(testData.get("SoftScoredValidation").equalsIgnoreCase("Y")){
			try{
				if(driver.findElement(commentsEdit).getAttribute("value").contains("This guest was not soft scored")){
					tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.PASS, "Error Message is displayed during creation of Tours");
				}else{
					tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.FAIL, "Error Message is NOT displayed during creation of Tours");
				}
			}catch (Exception e){
				tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.FAIL, "Unable to read the error message text may be the locator is invalid");
			}
		}else if(testData.get("CourtseyTourValidation").equalsIgnoreCase("Y")){
			try{
				if(driver.findElement(commentsEdit).getAttribute("value").contains("courtesy tour only")){
					tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.PASS, "Error Message related to Courtsey tour is displayed during creation of Tours");
				}else{
					tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.FAIL, "Error Message related to Courtsey tour is NOT displayed during creation of Tours");
				}
			}catch (Exception e){
				tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.FAIL, "Unable to read the error message text may be the locator is invalid");
			}	
		}
		
//		if(driver.findElement(qualificationStatus).isEnabled()){
//			clickElementBy(qualificationStatus);
//			waitForSometime(tcConfig.getConfig().get("LowWait"));
//			clickElementBy(By.xpath("//a[@title='"+testData.get("QualificationScore")+"']"));
//			waitForSometime(tcConfig.getConfig().get("LowWait"));
//		}
		
		
		//clickElementBy(tourlanuageDrpDwn);
		//clickElementBy(tourLangValue);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
//		clickElementBy(popUpSaveBtn);
		WebElement popUpSave= driver.findElement(popUpSaveBtn);
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", popUpSave);
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "Save button clicked");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		}else{
			
			tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.FAIL, "User is unable to navigate to Reservations Page");
		}
		}
		
	public void search_additonal(){
		
		
		waitUntilElementVisibleBy(driver, globalSearch, 120);
		if (verifyObjectDisplayed(globalSearch)){
		tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "User is navigated to Reservations Page");
	
	fieldDataEnter(globalSearch, testData.get("ReservationNumber"));
	waitForSometime(tcConfig.getConfig().get("MedWait"));		
	driver.findElement(globalSearch).sendKeys(Keys.ENTER);
	waitForSometime(tcConfig.getConfig().get("MedWait"));		
	List<WebElement> seacrhResOwnerLink=driver.findElements(By.xpath("(//th//span//a[contains(@title,'"+testData.get("ReservationNumber")+"')])"));
	waitForSometime(tcConfig.getConfig().get("MedWait"));
	seacrhResOwnerLink.get(seacrhResOwnerLink.size()-1).click();
	waitForSometime(tcConfig.getConfig().get("MedWait"));
	tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "Reservations is accessed");
	
		
		/*waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)){
			tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "User is navigated to Reservations Page");
		
		fieldDataEnter(ownerSearchBoxEdit, testData.get("ReservationNumber"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));		
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LongWait"));		
		WebElement seacrhResOwnerLink=driver.findElement(By.xpath("//th//span//a[contains(@title,'"+testData.get("ReservationNumber")+"')]"));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		seacrhResOwnerLink.click();*/
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, bookTourBtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
			addAdditionalGuest();
		
		}
	}
	
	public void addAdditionalGuest(){
		clickElementBy(addAdditionalGuestBtn);		
		waitUntilObjectVisible(driver, firstName, 120);
		fieldDataEnter(firstName, testData.get("FirstName"));
		fieldDataEnter(lastName, testData.get("LastName"));
		String strPhoneNum=getRandomString(10);
		fieldDataEnter(txtPhoneNum, strPhoneNum);
		fieldDataEnter(txtEmailAddress, testData.get("Email"));
		fieldDataEnter(Street, testData.get("StreetName"));
		fieldDataEnter(City, testData.get("City"));
		fieldDataEnter(State, testData.get("State"));
		fieldDataEnter(Country, testData.get("Country"));
		fieldDataEnter(Postal, testData.get("ZIP"));
		clickElementBy(addGuestsaveBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		driver.navigate().refresh();		
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (verifyObjectDisplayed(additionalGuestLbl)){
			tcConfig.updateTestReporter("ReservationPage", "addAdditionalGuest", Status.FAIL, "Additional guest NOT added ");
		}else{
			tcConfig.updateTestReporter("ReservationPage", "addAdditionalGuest", Status.PASS, "Additional guest added ");
		}
	}
	
	public void searchForReservation(){
		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)){
			tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "User is navigated to Reservations Page");
		}
		fieldDataEnter(ownerSearchBoxEdit, testData.get("ReservationNumber"));
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));		
		WebElement seacrhResOwnerLink=driver.findElement(By.xpath("//th/span/a[contains(text(),'"+testData.get("ReservationNumber")+"')]"));
		waitUntilElementVisible(driver, seacrhResOwnerLink, 120);
		seacrhResOwnerLink.click();
		//waitForSometime(tcConfig.getConfig().get("LongWait"));
		tcConfig.updateTestReporter("ReservationPage", "Search Reservation", Status.PASS, "Reservation found");
		
	}
		
	private By EditsenBtn=By.xpath("//button[@title='Edit Sensitivity']");
	private By Sentext=By.xpath("//input[@name='TMSensitivity__c']");
	private By SenSave=By.xpath("//button[@title='Save']");
	public void AddSensitivity() throws AWTException{
		
		
		Robot rob= new Robot();
		rob.mouseWheel(4);
		waitUntilElementVisibleBy(driver, EditsenBtn, 120);
		if (verifyObjectDisplayed(EditsenBtn)){
			tcConfig.updateTestReporter("ReservationPage", "Add Sensitivity", Status.PASS, "Add Sensitivity field is found");
		}
		
		clickElementBy(EditsenBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement txtarea=driver.findElement(Sentext);
		txtarea.clear();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		txtarea.sendKeys(testData.get("SensitivityText"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(SenSave);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		
	}
		
	public void createLeadFromReservation(){
		waitUntilObjectVisible(driver, createLeadBtn, 120);
		clickElementBy(createLeadBtn);
		waitUntilObjectVisible(driver, txtLeadFirstName, 120);
		fieldDataEnter(txtLeadFirstName, testData.get("FirstName"));
		fieldDataEnter(txtLeadLastName, testData.get("LastName"));
		fieldDataEnter(txtMobile, testData.get("MobileNo"));
		fieldDataEnter(txtLeadEmail, testData.get("Email"));
		//fieldDataEnter(txtLeadFirstName, testData.get("FirstName"));
		clickElementBy(txtZipCode);
		driver.findElement(txtZipCode).sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(createCustomerBtn);
		waitUntilObjectVisible(driver, leadMaritalBtn, 120);
		clickElementBy(leadMaritalBtn);
		clickElementBy(householdBtn);
		clickElementBy(ageCustBtn);
		driver.findElement(By.xpath("//button[text()='SAVE']")).click();		
		waitUntilObjectVisible(driver, By.xpath("//a[text()='"+testData.get("LeadName")+"']"), 120);
		if(verifyObjectDisplayed(By.xpath("//a[text()='"+testData.get("LeadName")+"']"))){
			tcConfig.updateTestReporter("ReservationPage", "createLeadFromReservation", Status.PASS, "Lead created from Lead Reservation");
		}else{
			tcConfig.updateTestReporter("ReservationPage", "createLeadFromReservation", Status.FAIL, "Unable to create new Lead");
		}
		
	}
	
	public void createChangerequest() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, createChangeReq, 120);
		if(verifyObjectDisplayed(createChangeReq)) {
			tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.PASS, "createChangeReq button located");
			clickElementJSWithWait(createChangeReq);
		}else {

			tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.FAIL, "createChangeReq not present located");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(changeReqType);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement element=driver.findElement(By.xpath("//a[@title='"+testData.get("ChangeReqType")+"']"));
		clickElementJSWithWait(element);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, changeReqReason, 120);
		fieldDataEnter(changeReqReason, generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
		driver.findElement(changeReqReason).sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, changeReqSave, 120);
		clickElementJSWithWait(changeReqSave);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, caseCreated, 120);
		createdCase = driver.findElement(caseCreated).getText().trim();
		System.out.println(createdCase);
		tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.PASS, "change request created: " +createdCase);
	}
	
	public void createChangeRequestMarketer() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, createChangeReq, 120);
		if(verifyObjectDisplayed(createChangeReq)) {
			tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.PASS, "createChangeReq button located");
			clickElementBy(createChangeReq);
		}else {

			tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.FAIL, "createChangeReq not present located");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(changeReqTypeMarketer);
		driver.findElement(By.xpath("//a[@title='"+testData.get("ChangeReqType")+"']")).click();
		clickElementBy(changeReqReason);
		fieldDataEnter(changeReqReason, testData.get("ChangeReqReasion"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		if(testData.get("ChangeOwner").equalsIgnoreCase("Y")) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(deleteIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(userDD);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(user);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(searchMaketer);
			fieldDataEnter(searchMaketer, testData.get("QueueUser"));
			driver.findElement(searchMaketer).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(By.xpath("//a[@title='"+testData.get("QueueUser")+"']"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.PASS, "createChangeReq button located");
		}
		
		
		clickElementBy(changeReqSave);
		waitUntilElementVisibleBy(driver, caseCreated, 120);
		createdCase = driver.findElement(caseCreated).getText().trim();
//		
		tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.PASS, "change request created: " +createdCase);
		driver.findElement(caseCreated).click();
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(driver.findElement(By.xpath("//span[contains(text(),'"+createdCase+"')]")).isDisplayed()){
			
		String status=driver.findElement(By.xpath("//span[contains(@title,'Status')]//following-sibling::div/div/span")).getText();
		tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.PASS, "The Case is verified with the status :   " +status);
		
		}
		
		clickElementBy(homeIcon);
	}
	
	
	
	
	
	
	
	public void editReservation(){
		
		/*WebElement ele=driver.findElement(By.xpath("//button[contains(@title,'Edit Primary Guest First Name')]"));
		waitUntilElementVisible(driver, ele, 120);*/
		
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		do{
			//waitUntilElementVisibleBy(driver, caseCreated, 120);
			//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",ele);
			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("window.scrollBy(0,2500)");
			
			
		}while(!driver.findElement(By.xpath("//h3//button//span[contains(text(),'Temp')]")).isDisplayed());
		//while(!driver.findElement(By.xpath("//h3//button//span[contains(text(),'System Information')]")).isDisplayed());
		
		
		
		
		
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		//waitUntilElementVisibleBy(driver, guestFirstNameBtn, 120);
		//mouseoverAndClick(guestFirstNameBtn);
		//clickElementJS(guestFirstNameBtn);
		WebElement guestFirstNameBtn=driver.findElement(By.xpath("//button[contains(@title,'Edit Primary Guest First Name')]"));
		System.out.println(guestFirstNameBtn.isDisplayed());
		//guestFirstNameBtn.click();
		mouseoverAndClick(guestFirstNameBtn);
		//clickElementJS(guestFirstNameBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		driver.findElement(guestFirstNameEdit).clear();
		fieldDataEnter(guestFirstNameEdit, testData.get("FirstName"));
		
		driver.findElement(guestLastNameEdit).clear();
		fieldDataEnter(guestLastNameEdit, testData.get("LastName"));
		
		clickElementBy(guestEditSaveBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (driver.findElement(newGuestName).getText().
				equalsIgnoreCase(testData.get("FirstName")+" "+testData.get("LastName"))){
			tcConfig.updateTestReporter("ReservationPage", "editReservation",
					Status.PASS, "Guest details updated in reservation page");
		}else{
			tcConfig.updateTestReporter("ReservationPage", "editReservation",
					Status.FAIL, "Failed to update Reservation details");
		}
		
	}
	
	public void Search_Reservation(){
		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)){
			tcConfig.updateTestReporter("ReservationPage", "seacrhReservationBookTour", Status.PASS, "User is navigated to Reservations Page");
		
		fieldDataEnter(ownerSearchBoxEdit, testData.get("ReservationNumber"));
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));		
		WebElement seacrhResOwnerLink=driver.findElement(By.xpath("//th/span/a[contains(text(),'"+testData.get("ReservationNumber")+"')]"));
		seacrhResOwnerLink.click();
		}
	}
	
	private By updateEmailaddress=By.xpath("//input[@type='email']");
//	private By updateContactNum = By.xpath("//label[contains(.,'Phone Number')]//..//input");
	private By updateContactNum = By.xpath("//input[@name='TMPhoneNumber__c' or @type='tel']");
	private By editemailbtn=By.xpath("//button[@title='Edit Email']");
	private By savebtn = By.xpath("//button[@title='Save']");
	private By checkemail = By.xpath("(//div[@role='listitem']//div//span//a[contains(.,'@wyn.com')])[2]");
	private By checkmob = By.xpath("(//div[@role='listitem']//div//span//span[@class='uiOutputPhone'])[2]");
	private By clicklead = By.xpath("(//a[contains(.,'"+testData.get("LeadName")+"')])[2]");
//	private By clickName =By.xpath("//nav[@class='entityNameTitle']/..//a");
	private By clickName =By.xpath("//div//a[@id='window']");
//	private By ownerMobile=By.xpath("//button[@title='Edit Mobile']/..//span/span");
	private By ownerMobile=By.xpath("//span//span[@data-aura-class='uiOutputPhone' and text()]"); //webelement list
//	private By ownerEmail = By.xpath("//button[@title='Edit Email']/..//span/span");
	private By ownerEmail = By.xpath("//span//a[@data-aura-class='emailuiFormattedEmail']");
	
			
	
	public void update_ownerdetails() throws AWTException{
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		
		do{
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,250)");
			
			
		}while(!driver.findElement(By.xpath("//button//span[contains(text(),'Special')]")).isDisplayed());
		waitUntilElementVisibleBy(driver, editemailbtn, 120);
		driver.findElement(editemailbtn).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(updateEmailaddress).clear();
		driver.findElement(updateEmailaddress).click();
		String rndm = getRandomString(2);
		String cntctrndmnum = getRandomString(10);
		System.out.println("here is:"+cntctrndmnum);
		String updatedemail = "test"+rndm+"@wyn.com";
		driver.findElement(updateEmailaddress).sendKeys(updatedemail);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(updateContactNum).click();
		driver.findElement(updateContactNum).clear();
		driver.findElement(updateContactNum).sendKeys(cntctrndmnum);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(savebtn).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Actions action =new Actions(driver);
		action.sendKeys(Keys.PAGE_UP);
		
		waitUntilElementVisibleBy(driver, clicklead, 120);
		driver.findElement(clicklead).click();
		waitUntilElementVisibleBy(driver, checkemail, 120);
		String emailchk = driver.findElement(checkemail).getText();
		String mobcheck = driver.findElement(checkmob).getText();
		System.out.println(mobcheck);
		String update = (mobcheck.split("(")[1]).trim();
		System.out.println(update);
		String update_1 = (update.split(")")[0]).trim();
		String update_2 = (update.split(")")[1]).trim();
		System.out.println(update_2);
		String update1 = (mobcheck.split(" ")[1]).trim();
		String update2 = (update_2.split("-")[0]).trim();
		String update3 = (update_2.split("-")[1]).trim();
		String mobcheck_final = (update_1+update2+update3).trim();
		System.out.println(mobcheck_final);
		if (emailchk.equals(updatedemail)) {
			tcConfig.updateTestReporter("ReservationPage", "update_ownerdetails",
					Status.PASS, "Email updated to:"+updatedemail);
		}
		else{
			tcConfig.updateTestReporter("ReservationPage", "editReservation",
					Status.FAIL, "Failed while updating email");
		}
		if (mobcheck_final.equals(cntctrndmnum)) {
			tcConfig.updateTestReporter("ReservationPage", "update_ownerdetails",
					Status.PASS, "Mobile number updtaed to:"+cntctrndmnum);
		}
		else{
			tcConfig.updateTestReporter("ReservationPage", "editReservation",
					Status.FAIL, "Failed while updating mobile number");
		}
	}
	//nav[@class='entityNameTitle']/..//a
	public void ownerDetailsNotUpdated(){
		
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		
		waitUntilElementVisibleBy(driver, editemailbtn, 120);
		driver.findElement(editemailbtn).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(updateEmailaddress).clear();
		driver.findElement(updateEmailaddress).click();
		String rndm = getRandomString(2);
		String cntctrndmnum = getRandomString(10);
		System.out.println("here is:"+cntctrndmnum);
		String updatedemail = "test"+rndm+"@wyn.com";
		driver.findElement(updateEmailaddress).sendKeys(updatedemail);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(updateContactNum).click();
		driver.findElement(updateContactNum).clear();
		driver.findElement(updateContactNum).sendKeys(cntctrndmnum);
		
		String parent = driver.getWindowHandle();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(savebtn).click();
//		waitForSometime(tcConfig.getConfig().get("LongWait"));
//		List <WebElement> Name= driver.findElements(clickName);
//		Name.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(parent);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, clickName, 120);
		clickElementBy(clickName);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		if(updatedemail.equals(driver.findElement(ownerEmail).getText())){
			tcConfig.updateTestReporter("resPage", "updatedemail", Status.FAIL,
					"Failed while getting Owner email details");
		} else {
			tcConfig.updateTestReporter("resPage", "validatescore", Status.PASS,
					"Owner details email: "+updatedemail);
		}
			List<WebElement> contact=driver.findElements(ownerMobile);
			
		if(cntctrndmnum.equals(contact.get(contact.size()-1).getText())){
			tcConfig.updateTestReporter("resPage", "updatedemail", Status.FAIL,
					"Failed while getting Owner phone no details");
		} else {
			tcConfig.updateTestReporter("resPage", "cntctrndmnum", Status.PASS,
					"Owner details phone no: "+cntctrndmnum);
		}
		
		
		/*if(verifyElementDisplayed(driver.findElement(clickName))){
			clickElementBy(clickName);
			waitUntilObjectVisible(driver, clickName, 120);
			tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS,
					"Owner details : "+clickName);
		}else{
			tcConfig.updateTestReporter("LeadsPage", "clickName", Status.FAIL,
					"Failed while getting Owner details");
		} */
		
	}
	
	
	//private By travelchnl = By.xpath("//span[text()='Travel Channel']/../../div[2]/span/span | //span[text()='Travel Channel']/../..//lightning-formatted-text");
	private By travelchnl = By.xpath("//span[text()='Travel Channel']/../..//lightning-formatted-text");
	
	
	
	/**
	 * 
	 * Added by Ritam
	 */
	
	public void validatetravelchannel(){
		waitUntilElementVisibleBy(driver, travelchnl, 120);
		String travelchannel = driver.findElement(travelchnl).getText();
		if(travelchannel.equalsIgnoreCase(testData.get("TravelChannel"))){
			tcConfig.updateTestReporter("ReservationPage", "validatetravelchannel",
					Status.PASS, "Travel channel is:"+travelchannel);
		}
		else{
			tcConfig.updateTestReporter("ReservationPage", "validatetravelchannel",
					Status.FAIL, "Failed while validating Travel Channel");
		}
	}
	
	private By ressel = By.xpath("//a[text()='Reservations']/ancestor::div[@class='slds-grid slds-grid--vertical slds-m-bottom_small']//a[text()='"+testData.get("ReservationNumber")+"']");
	//private By conform =By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only'])[28]");	
	private By checkOutDate =By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only'])[24]");
	private By checkInDate = By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only'])[22]");
	private By los =By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only'])[32]");
	private By streetAddress = By.xpath("(//span[contains(.,'Street Address')]/following::span)[1]");
	private By state = By.xpath("(//span[contains(.,'State/Province')]/following::span)[1]");
	private By city =By.xpath("(//span[contains(.,'City')]/following::span)[1]");
	private By country = By.xpath("(//span[contains(.,'Country')]/following::span)[1]");
	private By zip = By.xpath("(//span[contains(.,'Zip/Postal Code')]/following::span)[1]");
	private By addressDetails = By.xpath("//a[contains(text(),'"+testData.get("LeadName")+"')][1]//following::span[contains(text(),'Address Details')][1]");
	
	
		public void validateDefaultCoporateAddress(){
		
			waitUntilElementVisibleBy(driver, ressel, 120);
			driver.findElement(ressel).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,700)", "");
			
			waitUntilElementVisibleBy(driver, addressDetails, 120);
			String addressverify = driver.findElement(addressDetails).getText();
			String txtstrtAddress = driver.findElement(streetAddress).getText();
			String txtstate = driver.findElement(state).getText();
			String txtcity = driver.findElement(city).getText();
			String txtcountry = driver.findElement(country).getText();
			String txtzip = driver.findElement(zip).getText();
			
			if (txtstrtAddress.equalsIgnoreCase(""+testData.get("StreetName")+"") && 
					txtcity.equalsIgnoreCase(""+testData.get("City")+"") && 
				txtzip.equalsIgnoreCase(""+testData.get("ZIP")+"") && 
				txtstate.equalsIgnoreCase(""+testData.get("State")+"")  && 
				txtcountry.equalsIgnoreCase(""+testData.get("Country")+"")){
				tcConfig.updateTestReporter("ReservationPage", "validateDefaultCoporateAddress",
						Status.PASS, "Address is default corporate address "+addressverify);
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateDefaultCoporateAddress",
						Status.FAIL, "Failed while validating address detail ");
			
		}
		}
		
		
		private By status= By.xpath("//span[@title='A/U']/../div//span | //p[@title='A/U']/..//lightning-formatted-text");
		private By status1=By.xpath("//div[@class='slds-form-element__control']//span[contains(text(),'U-ZERO NIGHT STAY')]");
		
		public void ValidateResevationZeroNightStay() throws ParseException{
			
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			By ResNum=By.xpath("//a[@title='"+ testData.get("ReservationNumber") +"']");
			
			List<WebElement> ReserveNum=driver.findElements(ResNum);
			ReserveNum.get(ReserveNum.size()-1);
//			waitUntilElementVisibleBy(driver, ResNum, 120);
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			List<WebElement> Dates=driver.findElements(By.xpath("//span[@data-aura-class='uiOutputDate']"));
			
			Collections.reverse(Dates);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			 Checkindate=Dates.get(1).getText();
			String Checkoutdate=Dates.get(0).getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			SimpleDateFormat sdf= new SimpleDateFormat("mm/dd/yyyy");
			Date Datein=sdf.parse(Checkindate);
			Date Dateout=sdf.parse(Checkoutdate);
			if(Datein.compareTo(Dateout)==0){
				
				tcConfig.updateTestReporter("ReservationPage", "ValidateResevationZeroNightStay",
						Status.PASS, "The Check in Date and Check out date is same  i.e.  "+ Datein);
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ReserveNum.get(ReserveNum.size()-1).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				if(testData.get("User").contains("Business") )
				{
					strStatus=driver.findElement(status1).getText();
				}
				else if(testData.get("User").contains("Corporate") )
				{
					strStatus=driver.findElement(status1).getText();
				}
				else if(testData.get("User").contains("Marketing Admin") )
				{
					strStatus=driver.findElement(status1).getText();
				}
				else if(testData.get("User").contains("Tour Reception") )
				{
					strStatus=driver.findElement(status1).getText();
				}
				else
				{
					strStatus=driver.findElement(status).getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				
				
				
				if(strStatus.contains("U-ZERO NIGHT STAY")){
					
					tcConfig.updateTestReporter("ReservationPage", "ValidateResevationZeroNightStay",
							Status.PASS, "The A/U is  "+ strStatus);
					
				}else{
					
					tcConfig.updateTestReporter("ReservationPage", "ValidateResevationZeroNightStay",
							Status.FAIL, "The A/U is  "+ strStatus);
					
				}
				
			}
			
			
			
			
		}
		
		private By Home=By.xpath("//span[contains(text(),'Home')]");
		private By ArrivalLink=By.xpath("//a//b [starts-with(text(),'Arrival List')]");
		private By ReportArrival= By.xpath("//div//h1[@title='Arrival List']");
		private By Filterbtn=By.xpath("//div//button[contains(@class,'toggleFilter')]");
		private By Checkin=By.xpath("//div//button//p[contains(text(),'Check In Date')]");
		private By customize=By.xpath("//a[contains(text(),'Customize')]");
		private By Applybtn= By.xpath("//button[contains(text(),'Apply')]");
				
				
		public void validateArrivalListZeroNight() throws AWTException{
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, Home, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Home);
			
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			Robot rob = new Robot();
			rob.mouseWheel(10);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, ArrivalLink, 120);
			clickElementBy(ArrivalLink);
			
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, ReportArrival, 120);
			clickElementBy(Filterbtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Checkin);
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, customize, 120);
			clickElementBy(customize);
			
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> date= driver.findElements(By.xpath("//input[@placeholder='Pick a date']"));
			date.get(0).sendKeys(Checkindate);
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(Applybtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			
			
		}
		
		
		
		
		
		public void validateDate(){
		
			waitUntilElementVisibleBy(driver, ressel, 120);
			driver.findElement(ressel).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,1100)", "");
			
			
			
			String checkIn = driver.findElement(checkInDate).getText();
			String checkOut =driver.findElement(checkOutDate).getText();
			if (checkIn.equals(checkOut)) {
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.PASS, "Check in date is same as check out date");
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.FAIL, "Check in date is not same as check out date");
			}
			
			waitUntilElementVisibleBy(driver, los, 120);
			String auverify = driver.findElement(los).getText();
			if (auverify.equalsIgnoreCase(""+testData.get("Los")+"")){
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.PASS, "LOS is zero:"+auverify);
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.FAIL, "Failed while validating LOS");
			}
		}
		private By checkOutDateInHouseMarketer =By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only'])[25]");
		private By checkInDateInHouseMarketer = By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only'])[23]");
		private By losInHouseMarketer =By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only'])[24]");
		
				
		
		public void validateDateForInHouseMarketer(){
			
			
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,1100)", "");
			
			
			
			String checkIn = driver.findElement(checkInDateInHouseMarketer).getText();
			String checkOut =driver.findElement(checkOutDateInHouseMarketer).getText();
			if (checkIn.equals(checkOut)) {
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.PASS, "Check in date is same as check out date");
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.FAIL, "Check in date is not same as check out date");
			}
			
			waitUntilElementVisibleBy(driver, losInHouseMarketer, 120);
			String auverify = driver.findElement(losInHouseMarketer).getText();
			if (auverify.equalsIgnoreCase(""+testData.get("Los")+"")){
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.PASS, "LOS is zero:"+auverify);
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateDate",
						Status.FAIL, "Failed while validating LOS");
			}
		}
		
		private By click_booktour =By.xpath("//span//button[text()='Book Tour']");
		private By booktourtext = By.xpath("//div[contains(@id,'modal-content-id')]");
		private By closebooktext = By.xpath("//button[text()='Cancel']");
		
		public void booktour_message() {
			waitUntilElementVisibleBy(driver, click_booktour, 120);
			System.out.println("Checking Click button");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			 WebElement bookbtn=driver.findElement(click_booktour);
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", bookbtn);
			
			
			tcConfig.updateTestReporter("ReservationPage", "booknotenabledckeckout",
					Status.PASS, "successfully clicked on Book Tour Button");
			waitUntilElementVisibleBy(driver, booktourtext, 120);
			String Book_Text = (driver.findElement(booktourtext).getText()).trim();
			String Book_Text_excel = testData.get("Book_Text").trim();
			if (Book_Text.equals(Book_Text_excel)) {
				tcConfig.updateTestReporter("ReservationPage", "booknotenabledckeckout",
						Status.PASS, "Required text is displyed with text:"+Book_Text);
			}
			else {
				tcConfig.updateTestReporter("ReservationPage", "booknotenabledckeckout",
						Status.FAIL, "Required text is not displyed");	
				}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement clsbtn=driver.findElement(closebooktext);
			JavascriptExecutor executor1 = (JavascriptExecutor)driver;
			executor1.executeScript("arguments[0].click();", clsbtn);
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		
		
		
		/**
		 * 
		 * Added by Ritam
		 * @param auValue
		 */
		
		
		public void verifyAUValue(String auValue)
		{
			waitUntilElementVisibleBy(driver, status, 120);
			if(driver.findElement(status).getText().equals(auValue))
			{
				tcConfig.updateTestReporter("ReservationPage", "verifyAUValue",
						Status.PASS, "Value showing as: "+ driver.findElement(status).getText());
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "verifyAUValue",
						Status.FAIL, "Value is not showing as expected and value reflected as: "+ driver.findElement(status).getText());
			}
		}
		
		
		
		private By fieldTravelChannel = By.xpath("//span[text()='Travel Channel']/../../div[2]/span/slot/slot/lightning-formatted-text");
		private By fieldGuestType = By.xpath("//span[text()='Guest Type']/../../div[2]//lightning-formatted-text");
		private By fieldReservationStatus = By.xpath("//span[text()='Reservation Status']/../../div[2]//lightning-formatted-text");
		private By chkBoxExtendedStay = By.xpath("//input[@name='TMExtendedStay__c']");
		private By chkBoxMultipleRes = By.xpath("//input[@name='TMMultipleReservations__c']/../..//label");
		private By lnkReservationRelated = By.xpath("//div[@class='listItemBody withActions']/h3[@class='primaryField']/span/a");
		private By lnkRelatedReservation = By.xpath("//img[@title='Related Reservations']/../../../../div[2]/h2/a");
		private By btnBookTour_MobUI = By.xpath("//button[text()='BOOK TOUR']");
		private By drpDownBrand_MobUI = By.xpath("//span[text()='Brand']/../../div//select");
		private By popupError = By.xpath("//div[@class='slds-modal__container']//div");
		private By btnCancel = By.xpath("//div[@class='slds-modal__container']//button");
		
		
	
		
		/**
		 * 
		 * 
		 * Added By Ritam
		 */
		
		public void verifyAccountableReservation()
		{  
			waitUntilElementVisibleBy(driver, fieldTravelChannel, 120);
			
			if(verifyObjectDisplayed(fieldTravelChannel)){
				String guistVAlue=driver.findElement(fieldGuestType).getText();
				if(guistVAlue.equals("Owner"))
				{
					System.out.println(guistVAlue);
					if(verifyObjectDisplayed(fieldReservationStatus))
					{
						if(!(driver.findElement(chkBoxExtendedStay)).isSelected())
						{
							waitUntilElementVisibleBy(driver, status, 120);
							String statusValue=driver.findElement(status).getText();
							System.out.println(statusValue);
							if(statusValue.equalsIgnoreCase((testData.get("A/U"))))
							{
								tcConfig.updateTestReporter("ReservationPage", "verifyAccountableReservation",
										Status.PASS, "Reservation contains all the values required for accountable");
							}
							else
							{
								tcConfig.updateTestReporter("ReservationPage", "verifyAccountableReservation",
										Status.FAIL, "A/U value is not correct");
							}
						}
						else
						{
							tcConfig.updateTestReporter("ReservationPage", "verifyAccountableReservation",
									Status.FAIL, "Extended stay checkbox is checked");
						}
					}
					else
					{
						tcConfig.updateTestReporter("ReservationPage", "verifyAccountableReservation",
								Status.FAIL, "Reservation status is not dispalyed");
					}
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyAccountableReservation",
						 Status.FAIL, "Guest type is not owner");
				}
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "verifyAccountableReservation",
						Status.FAIL, "Travel channel field is not displayed");
			}
		}

		
		
		public void verifyMultipleReservation()
		{
			waitUntilElementVisibleBy(driver, status, 120);
			if(driver.findElement(fieldTravelChannel).getText().equals(((testData.get("TravelChannel").split(","))[0])))
			{
				tcConfig.updateTestReporterWithoutScreenshot("ReservationPage", "verifyMultipleReservation",
						Status.PASS, "Reservation type for first reservation is: "+ testData.get("TravelChannel")+"[0]");
				
				if(driver.findElement(chkBoxMultipleRes).isSelected())
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyMultipleReservation",
							Status.PASS, "Multiple reservation checkbox is checked");
					
					int length = testData.get("ReservationNumber").split(",").length;
					
					for(int i =0 ; i<length; i++)
					{
					waitUntilElementVisibleBy(driver, lnkReservationRelated, 120);
					List<WebElement> resId = driver.findElements(lnkReservationRelated);
					clickElementWb(resId.get(i));
					waitUntilElementVisibleBy(driver, status, 120);
					
					if(driver.findElement(fieldTravelChannel).getText().equals(((testData.get("TravelChannel").split(","))[1])))
					{
						tcConfig.updateTestReporterWithoutScreenshot("ReservationPage", "verifyMultipleReservation",
								Status.PASS, "Reservation type for first reservation is: "+ testData.get("TravelChannel")+"[1]");
						
						if(driver.findElement(chkBoxMultipleRes).isSelected())
						{
							tcConfig.updateTestReporter("ReservationPage", "verifyMultipleReservation",
									Status.PASS, "Multiple reservation checkbox is checked");
						}
						else
						{
							tcConfig.updateTestReporter("ReservationPage", "verifyMultipleReservation",
									Status.FAIL, "Multiple reservation check box is not checked");
						}
						
					}
					else
					{
						tcConfig.updateTestReporter("ReservationPage", "verifyMultipleReservation",
								Status.FAIL, "Reservation type for first reservation is not coming as: "+ testData.get("TravelChannel")+"[1]");
					}
					
				}
					
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyMultipleReservation",
							Status.FAIL, "Multiple reservation check box is not checked");
				}
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "verifyMultipleReservation",
						Status.FAIL, "Reservation type for first reservation is not coming as: "+ testData.get("TravelChannel")+"[0]");
			}
		}
		
		private By lnkReservation = By.xpath("//span[@title='Reservation Number']/../../../../../../tbody/tr/th//a");
		
		public void validateRelatedMultipleReservationFields()
		{
			waitUntilElementVisibleBy(driver, lnkRelatedReservation, 120);
			clickElementBy(lnkRelatedReservation);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement res = driver.findElement(By.xpath("//span[@title='Reservation Number']"));
			WebElement guest = driver.findElement(By.xpath("//span[@title='Guest Name']"));
			if(verifyObjectDisplayed(By.xpath("//a[text()='"+testData.get("ReservationNumber")+"["+0+"]"+"]")) && verifyElementDisplayed(res))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.PASS, "Reservation number field is visible in related reservation page");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.FAIL, "Reservation number field is not visible in related reservation page");
			}
			
			if(verifyObjectDisplayed(By.xpath("//span[text()='"+testData.get("OwnerName")+"']")) && verifyElementDisplayed(guest))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.PASS, "Guest Name field is visible in related reservation page");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.FAIL, "Guest Name field is not visible in related reservation page");
			}
			
			if(verifyObjectDisplayed(By.xpath("//span[@title='Resort']")))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.PASS, "Resort field is visible in related reservation page");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.FAIL, "Resort field is not visible in related reservation page");
			}
			
			if(verifyObjectDisplayed(By.xpath("//span[@title='Check-In Date']")))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.PASS, "Check-In date field is visible in related reservation page");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.FAIL, "Check-In date field is not visible in related reservation page");
			}
			
			if(verifyObjectDisplayed(By.xpath("//span[@title='Check-Out Date']")))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.PASS, "Check-Out date field is visible in related reservation page");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.FAIL, "Check-Out date field is not visible in related reservation page");
			}
			
			if(verifyObjectDisplayed(By.xpath("//span[@title='Member Number']")))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.PASS, "Member Number field is visible in related reservation page");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "validateRelatedMultipleReservationFields",
						Status.FAIL, "Member Number field is not visible in related reservation page");
			}
		}
		
		
		public void verifyTourBooking(String status)
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnBookTour_MobUI, 120);
			clickElementJSWithWait(btnBookTour_MobUI);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			if(status.equalsIgnoreCase("Reserved"))
			{
				waitUntilElementVisibleBy(driver, drpDownBrand_MobUI, 120);
				if(verifyObjectDisplayed(drpDownBrand_MobUI))
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyTourBooking",
							Status.PASS, "Able to book tour for reserved and inhouse reservation");
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyTourBooking",
							Status.FAIL, "Not able to book tour for reserved and inhouse reservation");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else if(status.equalsIgnoreCase("Checked-Out") || status.equalsIgnoreCase("Cancelled"))
			{
				if(verifyObjectDisplayed(popupError))
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyTourBooking",
							Status.PASS, "Not able to book tour for Checked out or cancelled reservation");
					clickElementBy(btnCancel);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.navigate().back();
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyTourBooking",
							Status.FAIL, "Able to book tour for Checked out or cancelled reservation");
				}
			}
		}
		
		private By fieldSuccessMsg = By.xpath("//span[contains(@class,'toastMessage')]//a");
		
		
		
		/**
		 * 
		 * Added by Ritam
		 */
		public void bookTourFromReservation()
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, bookTourBtn, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (testData.get("AdditionalGuest").equalsIgnoreCase("Y")){
				addAdditionalGuest();
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			clickElementBy(bookTourBtn);
			WebElement book = driver.findElement(bookTourBtn);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", book);
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			WebElement popUpSave= driver.findElement(popUpSaveBtn);
			JavascriptExecutor executor1 = (JavascriptExecutor)driver;
			executor1.executeScript("arguments[0].click();", popUpSave);
			
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, fieldSuccessMsg, 120);
			if(verifyObjectDisplayed(fieldSuccessMsg))
			{
				tcConfig.updateTestReporter("ReservationPage", "bookTourFromReservation",
						Status.PASS, "Tour is created successfully");
				clickElementBy(fieldSuccessMsg);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "bookTourFromReservation",
						Status.FAIL, "Tour is not created");
			}
				
		}

		public void caseSubmissionValidation()
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, createChangeReq, 120);
			if(verifyObjectDisplayed(createChangeReq)) {
				tcConfig.updateTestReporter("createChangerequest", "caseSubmissionValidation", Status.PASS, "createChangeReq button located");
				clickElementBy(createChangeReq);
			}else {

				tcConfig.updateTestReporter("createChangerequest", "caseSubmissionValidation", Status.FAIL, "createChangeReq not present");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(changeReqTypeMarketer);
			driver.findElement(By.xpath("//a[@title='"+testData.get("ChangeReqType")+"']")).click();
			clickElementBy(changeReqReason);
			fieldDataEnter(changeReqReason, testData.get("ChangeReqReasion"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			if(testData.get("ChangeOwner").equalsIgnoreCase("Y")) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(deleteIcon);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(userDD);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(user);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(searchMaketer);
				fieldDataEnter(searchMaketer, testData.get("QueueUser"));
				driver.findElement(searchMaketer).sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(By.xpath("//a[@title='"+testData.get("QueueUser")+"']"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("createChangerequest", "caseSubmissionValidation", Status.PASS, "createChangeReq button located");
			}
			
			
			clickElementBy(changeReqSave);
			waitUntilElementVisibleBy(driver, caseCreated, 120);
			driver.findElement(caseCreated).click();
			createdCase = driver.findElement(caseCreated).getText().trim();
			tcConfig.updateTestReporter("createChangerequest", "caseSubmissionValidation", Status.PASS, "Change Request Created: " +createdCase);
			
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(driver.findElement(By.xpath("//span[contains(text(),'"+createdCase+"')]")).isDisplayed()){
				
			String status=driver.findElement(By.xpath("//span[@title ='Status']/following-sibling::div//span")).getText();
			tcConfig.updateTestReporter("createChangerequest", "caseSubmissionValidation", Status.PASS, "The Case is verified with the status :   " +status);
			
			}
			else{
				tcConfig.updateTestReporter("createChangerequest", "caseSubmissionValidation", Status.FAIL,"Created Change Request is not visible" );
				
			}
			
			clickElementBy(homeIcon);
		}
		
		private By txtBoxSearchRes = By.xpath("//input[@name='TMReservation__c-search-input']");
		private By lnkReservations = By.xpath("//div[contains(@class,'forceInlineEditGrid')]//tbody//tr/th//a");
		private By btnAddGuest = By.xpath("//button[text()='Add Additional Guest']");
		private By lnkGuestName = By.xpath("//span[contains(text(),'Guest Name')]/../../div[2]//a");
		private By lnkGuestAddr = By.xpath("//span[contains(text(),'Address')]/../../div[2]//a");
		private By chkBoxSameAsPrimaryGuest = By.xpath("//label[text()='Same as Primary Guest Address']/..//input");
		
		public void selectReservation()
		{
			waitUntilElementVisibleBy(driver, txtBoxSearchRes, 120);
			clickElementWb(driver.findElements(lnkReservations).get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		
		public void clickAddAdditionalGuest()
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleWb(driver, driver.findElements(btnAddGuest).get(driver.findElements(btnAddGuest).size()-1), 120);
			List<WebElement> e = driver.findElements(btnAddGuest);
			scrollDownForElementJSWb(e.get(e.size()-1));
			scrollUpByPixel(300);
			Actions a = new Actions(driver);
			a.moveToElement(e.get(0)).click().build().perform();
			//clickElementJS(e.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		
		public String getGuestAddress()
		{
			waitUntilElementVisibleWb(driver, driver.findElements(lnkGuestName).get(driver.findElements(lnkGuestName).size()-1), 120);
			List<WebElement> e = driver.findElements(lnkGuestName);
			scrollDownForElementJSWb(e.get(e.size()-1));
			scrollUpByPixel(280);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(e.get(e.size()-1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkGuestAddr, 120);
			scrollDownForElementJSBy(lnkGuestAddr);
			scrollUpByPixel(100);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String addr = driver.findElement(lnkGuestAddr).getAttribute("title");
			if(!addr.equals(""))
			{
				tcConfig.updateTestReporter("GuestPage", "getGuestAddress", Status.PASS, "Address is present for the guest");
			}
			else
			{
				tcConfig.updateTestReporter("GuestPage", "getGuestAddress", Status.FAIL, "Address is not present for the guest");
			}
			
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			return addr;
		}
		
		private By txtBoxGuestAddress = By.xpath("//span[text()='Address Details']/../..//input");
		
		public void verifyChkBoxSameAsPrimaryGuest(String address)
		{
			int count = 0;
			waitUntilElementVisibleBy(driver, chkBoxSameAsPrimaryGuest, 120);
			clickElementJSWithWait(chkBoxSameAsPrimaryGuest);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			for(int i =0; i<driver.findElements(txtBoxGuestAddress).size(); i++)
			{
				if(address.toLowerCase().contains(driver.findElements(txtBoxGuestAddress).get(i).getText().toLowerCase()))
				{
					count++;
				}
			}
			
			if(count==driver.findElements(txtBoxGuestAddress).size())
			{
				tcConfig.updateTestReporter("ReservationPage", "verifyChkBoxSameAsPrimaryGuest", Status.PASS, "Primary guest address is showing after clicking the checkbox ");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "verifyChkBoxSameAsPrimaryGuest", Status.FAIL, "Primary guest address is not showing after clicking the checkbox ");
			}
			clickElementJSWithWait(closebooktext);
			 waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		
/*<Jyoti's code>*/
		
		private By txtSearchReservation = By.xpath("//input[contains(@title,'Search Reservations and more')]");
		private By btnBookTourNative = By.xpath("//button[contains(text(),'Book Tour')]");
		private By clickReservation = By.xpath("//a[contains(@title,'"+testData.get("ReservationNumber")+"')]");
		private By lnkReservationss = By.xpath("//a[contains(text(),'Reservations')]");
		
		public void nativeSearchReservation(){
			
			try {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisibleBy(driver, txtSearchReservation, 120);
					
				fieldDataEnter(txtSearchReservation, testData.get("ReservationNumber").trim());
				tcConfig.updateTestReporter("ReservationPage", "nativeSearchReservation", Status.PASS, "Reservation Page is displayed");						
				
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(txtSearchReservation).sendKeys(Keys.ENTER);
					waitUntilElementVisibleBy(driver, lnkReservationss, 120);	
					tcConfig.updateTestReporter("ReservationPage", "nativeSearchReservation", Status.PASS, "In-House Reservation Details is displayed");
					//driver.findElement(clickReservation).click();
					List<WebElement> button =driver.findElements(clickReservation);
					button.get(button.size()-1).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));					 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					tcConfig.updateTestReporter("ReservationPage", "nativeSearchReservation", Status.FAIL, "Reservation Details is not found");
				}		
			}

		public By lblPrimaryGuestFirstName = By.xpath("(//div[@class='slds-form-element slds-hint-parent']//span[contains(text(),'Primary Guest First Name')]/following::input)[1]");
		public By lblPrimaryGuestLastName  = By.xpath("(//div[@class='slds-form-element slds-hint-parent']//span[contains(text(),'Primary Guest Last Name')]/following::input)[1]");
		public By btnSave = By.xpath("//span[contains(text(),'Save')]");
		public By btnEdit = By.xpath("//div[@title='Edit']");
		public By lnkAdditonalGuest = By.xpath("//span[@title='Additional Guests']");
		public By tblAdditionalGuest = By.xpath("(//tr//th//span//a)");
		public By lblAG = By.xpath("//span[@title='Additional Guests Name']");	
		public By btnCreateLead = By.xpath("//button[contains(text(),'Create Lead')]");
		public By txtFirstNameAG = By.xpath("//input[@name='TMFirstName__c']");
		public By txtLastNameAG = By.xpath("//input[@name='TMLastName__c']");
		public By txtEmailAG = By.xpath("//input[@name='TMEmail__c']");
		 //public By txtEmailAG = By.xpath("//input[@type='email']");
		public By txtEditAg = By.xpath("//h2[contains(text(),'Edit Additional Guest')]");
		public By btnSaveAG = By.xpath("//button[@title='Save']");
		public By oldFirstName = By.xpath("(//span[text()='First Name'])[2]/../../div//span//lightning-formatted-text");
		public By oldLastName = By.xpath("(//span[text()='Last Name'])[2]/../../div//span//lightning-formatted-text");
		public By oldEmail = By.xpath("(//span[text()='Email']/parent::div/following-sibling::div/span//slot//a)[2]");
		public By newFN = By.xpath("(//span[text()='First Name'])[2]/../../div//span//lightning-formatted-text");
		public By newLN = By.xpath("(//span[text()='Last Name'])[2]/../../div//span//lightning-formatted-text");
		public By newEmail = By.xpath("(//span[text()='Email']/parent::div/following-sibling::div/span//slot//a)[2]");
		public By editFN = By.xpath("(//button[@title='Edit First Name']//span)[1]");
		public By txtPhoneLnk = By.xpath("//input[@name='TMPhone__c']");
		public By popup =By.xpath("//button[@title='OK']");
		public By email = By.xpath("//input[@name='TMEmail__c']");
		//protected By updatedPhone = By.xpath("//span[contains(.,'Phone')]/../..//div//a");
		protected By updatedPhone = By.xpath("(//span[contains(.,'Phone')]/../..//div/span/slot/slot/lightning-formatted-phone)[2]");
		
		public void editNonOwnerReservation(){
			try {
			waitUntilElementVisibleBy(driver, btnBookTourNative, 120);
			clickElementJSWithWait(lnkAdditonalGuest);
			waitUntilElementVisibleBy(driver, lblAG, 120);
			List<WebElement> button =driver.findElements(tblAdditionalGuest);
			System.out.println("Number of elements:" +button.size());
			button.get(button.size()-1).click();
			waitUntilElementVisibleBy(driver, oldFirstName, 120); 
			String First = getTextFromBodyBy(oldFirstName);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Last = getTextFromBodyBy(oldLastName);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Email = getTextFromBodyBy(oldEmail);
			waitUntilElementVisibleBy(driver, editFN, 120); 
			String phn = getTextFromBodyBy(updatedPhone);
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name present :- "+First+ "and Last name present :- " +Last+ "and Email present :-" +Email+ "and Phone number present :-" +phn);
			clickElementJSWithWait(editFN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtFirstNameAG, 120);
			driver.findElement(txtFirstNameAG).click();
			driver.findElement(txtFirstNameAG).clear();
			String FirstNm=randomAlphaNumeric(3);
			fieldDataEnter(txtFirstNameAG, FirstNm);
			waitUntilElementVisibleBy(driver, txtLastNameAG, 120);
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtLastNameAG).click();
			driver.findElement(txtLastNameAG).clear();
			String LastNm=randomAlphaNumeric(4); 
			fieldDataEnter(txtLastNameAG, LastNm); 
			waitUntilElementVisibleBy(driver, txtEmailAG, 120);
			driver.findElement(txtEmailAG).clear();
			driver.findElement(txtEmailAG).sendKeys(getRandomStringEmail());
			driver.findElement(txtEmailAG).sendKeys(Keys.TAB);
			String phone = getRandomString(10); 
			driver.findElement(txtPhoneLnk).clear();
			driver.findElement(txtPhoneLnk).sendKeys(phone);
			driver.findElement(txtPhoneLnk).sendKeys(Keys.TAB);
			
			//fieldDataEnter(txtEmailAG,getRandomStringEmail());
			//driver.findElement(txtEmailAG).sendKeys(Keys.ENTER);
			
			
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnSaveAG, 120);
			clickElementJSWithWait(btnSaveAG);
			
			waitForSometime(tcConfig.getConfig().get("MedWait")); 
			String FN =getTextFromBodyBy(newFN);
			String LN=getTextFromBodyBy(newLN); 
			String EmailUpdated=getTextFromBodyBy(newEmail);
			String phnUpdated = getTextFromBodyBy(updatedPhone);
			if (!First.equals(FN) && (!Last.equals(LN)) && (!Email.equals(EmailUpdated)) && (!phone.equals(phnUpdated))) {
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name updated to:"+FN+ "and Last name updtaed to:- " +LN+ "and Email updtaed to:-" +EmailUpdated+ "and Phone number updated to :-" +phnUpdated);
			}
			else{
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating Fields");
			}
			/*else if (!Last.equals(LN)) {
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name updated to:"+LN);
			}
			else{
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating First Name");
			}
			else if (!Email.equals(EmailUpdated)) {
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name updated to:"+EmailUpdated);
			}
			else{
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating  Email");
			}*/

			} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating  ");
			} 
			}
		
		public void editOwnerReservation(){
			try {
			waitUntilElementVisibleBy(driver, btnBookTourNative, 120);
			clickElementJSWithWait(lnkAdditonalGuest);
			waitUntilElementVisibleBy(driver, lblAG, 120);
			List<WebElement> button =driver.findElements(tblAdditionalGuest);
			System.out.println("Number of elements:" +button.size());
			button.get(button.size()-1).click();
			waitUntilElementVisibleBy(driver, oldFirstName, 120); 
			String First = getTextFromBodyBy(oldFirstName);
			waitUntilElementVisibleBy(driver, oldLastName, 120);
			String Last = getTextFromBodyBy(oldLastName);
			waitUntilElementVisibleBy(driver, updatedPhone, 120);
			String Email = getTextFromBodyBy(oldEmail);
			waitUntilElementVisibleBy(driver, updatedPhone, 120); 
			String phn = getTextFromBodyBy(updatedPhone);
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name present :- "+First+ "and Last name present :- " +Last+ "and Email present :-" +Email+ "and Phone number present :-" +phn);
			clickElementJSWithWait(editFN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtFirstNameAG, 120);
			driver.findElement(txtFirstNameAG).click();
			driver.findElement(txtFirstNameAG).clear();
			String FirstNm=randomAlphaNumeric(3);
			fieldDataEnter(txtFirstNameAG, FirstNm);
			waitUntilElementVisibleBy(driver, txtLastNameAG, 120);
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtLastNameAG).click();
			driver.findElement(txtLastNameAG).clear();
			String LastNm=randomAlphaNumeric(4); 
			fieldDataEnter(txtLastNameAG, LastNm); 
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtPhoneLnk, 120);
			driver.findElement(txtPhoneLnk).click();
			driver.findElement(txtPhoneLnk).clear();
			String phone = getRandomString(10); 
			fieldDataEnter(txtPhoneLnk, phone);
			driver.findElement(txtPhoneLnk).sendKeys(Keys.TAB);
			driver.findElement(txtPhoneLnk).sendKeys(Keys.TAB);
			driver.findElement(txtPhoneLnk).sendKeys(Keys.TAB);
			waitUntilElementVisibleBy(driver, txtEmailAG, 120);
			driver.findElement(txtEmailAG).clear();
			driver.findElement(txtEmailAG).sendKeys(getRandomStringEmail());;
			//fieldDataEnter(txtEmailAG,getRandomStringEmail());
			//driver.findElement(txtEmailAG).sendKeys(Keys.ENTER);
			
			driver.findElement(txtEmailAG).sendKeys(Keys.TAB);
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnSaveAG, 120);
			clickElementJSWithWait(btnSaveAG);
			
			waitForSometime(tcConfig.getConfig().get("MedWait")); 
			String FN =getTextFromBodyBy(newFN);
			String LN=getTextFromBodyBy(newLN); 
			String EmailUpdated=getTextFromBodyBy(newEmail);
			String phnUpdated = getTextFromBodyBy(updatedPhone);
			if (!First.equals(FN) && (!Last.equals(LN)) && (!Email.equals(EmailUpdated)) && (!phone.equals(phnUpdated))) {
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name updated to:"+FN+ "and Last name updtaed to:- " +LN+ "and Email updtaed to:-" +EmailUpdated+ "and Phone number updated to :-" +phnUpdated);
			}
			else{
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating First Name");
			}
			/*else if (!Last.equals(LN)) {
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name updated to:"+LN);
			}
			else{
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating First Name");
			}
			else if (!Email.equals(EmailUpdated)) {
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.PASS, "First Name updated to:"+EmailUpdated);
			}
			else{
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating  Email");
			}*/

			} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("ReservationPage", "editNonOwnerReservation",
			Status.FAIL, "Failed while updating  ");
			} 
			}

		public static String randomAlphaNumeric(int count) {
			StringBuilder builder = new StringBuilder();
			while (count-- != 0) {
			int character = (int)(Math.random()*SOURCE.length());
			builder.append(SOURCE.charAt(character));
			}
			return builder.toString();
			}
		
		private By click_AllGuest = By.xpath("//img[contains(@src,'All_Guests')]");
		private By click_dropdown = By.xpath("//select[@name='Dropdown']");
		private By drpReservation = By.xpath("//input[@name='inline-search-input']");
		private By txtReservation = By.xpath("//input[@name='inline-search-input']");
		private By tblReservation = By.xpath("//table[contains(@class,'slds-table wyn_tabContainer')]");
		
		public void validateGlobalSearchReservation(){
			/*try{
					*/
					waitUntilElementVisibleBy(driver, click_AllGuest, 120);	
					driver.findElement(click_AllGuest).click();
					tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.PASS, "All Guest value is selected");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					driver.findElement(click_dropdown).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					Select dropdown = new Select(driver.findElement(click_dropdown));
					dropdown.selectByValue("Reservation");
					tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.PASS, "Reservation value is selected");
					waitUntilElementVisibleBy(driver, txtReservation, 120);
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(inputSearchTourRes, testData.get("ReservationNumber"));
				WebElement wb=driver.findElement(inputSearchTourRes);
				wb.sendKeys(Keys.ENTER);
				wb.sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']"), 240);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if(driver.findElements(By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']")).size()==0)
				{
					waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
					new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					fieldDataEnter(inputSearchTourRes, testData.get("ReservationNumber"));
					WebElement web=driver.findElement(inputSearchTourRes);
					web.sendKeys(Keys.ENTER);
					web.sendKeys(Keys.ENTER);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']"), 120);
				}
				if(verifyObjectDisplayed(By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']")))
				{
					tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.PASS, "Reservation details are displayed");
					clickElementBy(By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']"));
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.FAIL, "Reservation details are not displayed");
				}
					
			
			}
		public By chkExtendedStay = By.xpath("(//span[contains(text(),'Extended Stay')]/following::span//img)[1]");
		public By tblRelatedReservation = By.xpath("//table[@data-aura-class='uiVirtualDataTable']//span[@class='slds-truncate']//a");
		public By tblColumns = By.xpath("//table[@data-aura-class='uiVirtualDataTable']//span[@class='slds-truncate']");
		public By lnkRelatedResv = By.xpath("//span[@title='Related Reservations']");
		public By txtRelatedResv = By.xpath("//h1[@title='Related Reservations']");
		public By btnEditRelated = By.xpath("//div[@title='Edit']");
		public By vlaues = By.xpath("//table[@data-aura-class='uiVirtualDataTable']//tbody//span[@class='slds-grid slds-grid--align-spread']");
		public By relatedValues = By.xpath("//span[@class='slds-truncate uiOutputTextArea']");
		public By resrvValues = By.xpath("//tbody//span[@class='uiOutputTextArea']");
		public By realtedResrv = By.xpath("//td//span[@data-aura-class='uiOutputTextArea']");
		private By click_ReservationLink = By.xpath("//div//a[contains(text(),'"+testData.get("ReservationNumber")+"')]");
		//private By btnNew = By.xpath("//div//a[contains(text(),'"+testData.get("ReservationNumber")+"')]");
		private By txtTitle = By.xpath("//span[text()='Title']/../..//textarea");
		private By txtDescription = By.xpath("//span[text()='Description']/../..//textarea");
		private By btnNewSave = By.xpath("//button//span[contains(text(),'Save')]");
		private By btnNew =By.xpath("(//div[@title='New'])[1]");
		private By btnBookTour = By.xpath("//button[contains(text(),'BOOK TOUR')]");
		//private By btnCancel = By.xpath("//button//span[contains(text(),'Cancel')]");
		
		private By lnknotes = By.xpath("//span[contains(text(),'Test Demo1')]/../../th");
		
				
		public void verifyRelatedReservation()
		{
			try {
				waitUntilElementVisibleBy(driver, click_ReservationLink, 120);					
				driver.findElement(click_ReservationLink).click();		   
								
				waitUntilElementVisibleBy(driver, btnBookTour, 120);
				tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.PASS, "Reservation Details page is displayed");
				if(driver.findElement(chkExtendedStay).getAttribute("className").contains(" checked"))
				{					
					tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.PASS, "Extended Stay is checked in Reservation");
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.FAIL, "Failed while displaying Reservation");
				}
				List<WebElement> items = driver.findElements(resrvValues);
				System.out.println("Number of fields present in Related Reservation are :" +items.size());
				
				clickElementJSWithWait(lnkRelatedResv);
				waitUntilElementVisibleBy(driver, txtRelatedResv, 120);
				if(verifyElementDisplayed(driver.findElement(txtRelatedResv)))
				{
					List<WebElement> list = driver.findElements(tblColumns);
					System.out.println("Number of fields present are :" +list.size());
					for (int i=0; i<=list.size()-1;i++)
					{					
				    	/*tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.PASS, "List of Related Reservation Fields present are :"+list.get(i).getText());*/
						tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.PASS, "List of Related Reservation Fields present are displyed");
					}						
				}
				else{
					tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.FAIL, "Failed while displaying Related Reservation");
				}
				List<WebElement> values = driver.findElements(realtedResrv);
				System.out.println("Number of fields present in Related Reservation are :" +values.size());
				if(values.size()>=items.size())
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.PASS, "Related Reservation Fields are reflecting");
				}
				else{
					tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.FAIL, "Failed while comparing fields");
				}
									
				List<WebElement> buttons = driver.findElements(tblRelatedReservation);	
				System.out.println("Number of fields present are :" +buttons.size());
				buttons.get(0).click();		
				
				waitUntilElementVisibleBy(driver, btnEditRelated, 120);
				if(driver.findElement(chkExtendedStay).getAttribute("className").contains(" checked"))
				{
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.PASS, "Extended Stay is checked in Related Reservation record page");
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyRelatedReservation", Status.FAIL, "Failed while displaying Related Reservation record page");
				}
				driver.navigate().back();
				waitUntilElementVisibleBy(driver, txtRelatedResv, 120);
				driver.navigate().back();
				waitUntilElementVisibleBy(driver, btnBookTour, 120);
				driver.navigate().back();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				tcConfig.updateTestReporter("ReservationPage", "validateAUStatus", Status.FAIL,
						"Failed due to : exception caught " + e.getMessage());
			}
		}
		
		public By lblMarketingAgent = By.xpath("(//span[contains(text(),'Marketing Agent Assigned')]/following::div//span)[1]");
		public By btnEditReserv = By.xpath("//a[@title='Edit']");
		public By btnEditMarketingAgent = By.xpath("//span[contains(text(),'Edit Marketing Agent Assigned')]");
		public By value = By.xpath("//input[@placeholder='Search People...']");
		public By btnSaveMarketingAgent = By.xpath("//button[contains(text(),'Save')]");
		public By validate = By.xpath("//span[contains(text(),'Marketing Agent Assigned')]/../../div[2]/span/slot//force-lookup/div/force-hoverable-link/div/a");
		public By agentNmae = By.xpath("//input[@placeholder='Salesforce Admin']");
		public By closeButton = By.xpath("(//button[@title='Clear Selection'])[1]");
		//span[contains(text(),'Marketing Agent Assigned')]/../../div[2]/span/slot//force-lookup/div/force-hoverable-link/div/a
		public void validateMarketingAgentBlank(){
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.navigate().refresh();
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			//waitUntilElementVisibleBy(driver, btnEditReserv, 120);
			if(verifyObjectDisplayed(lblMarketingAgent)){
			if(driver.findElement(lblMarketingAgent).getText().equalsIgnoreCase(""))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateMarketingAgentBlank", Status.PASS, "Marketing Agent is blank");								
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateMarketingAgentBlank", Status.FAIL, "Failed while displaying  Reservation record page");
			}
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(btnEditMarketingAgent);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(value).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(value).sendKeys("Salesforce admin");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(value).sendKeys(Keys.ARROW_DOWN);
			driver.findElement(value).sendKeys(Keys.ENTER);
			//clickElementJS(agentNmae);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(btnSaveMarketingAgent);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String dd=driver.findElement(validate).getText().trim();
			System.out.println(dd);
			if(driver.findElement(validate).getText().equalsIgnoreCase("Salesforce admin")){
				tcConfig.updateTestReporter("ReservationPage", "validateMarketingAgent", Status.PASS, "MarketingAgent update Validated sucessfully");
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateMarketingAgent", Status.FAIL, "Failed while validating");
			}
		}
		
		public By lblMarketingStatus = By.xpath("//span[contains(text(),'Reservation Status')]/following::div//lightning-formatted-text[contains(text(),'In-House')]");
		//public By tourlanguage = By.xpath("//label[contains(text(),'Langauge')]/following::div//select[contains(@class,'slds-select select uiInput uiInputSelect uiInput')]");
		public By tourlanguage = By.xpath("//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select']");
		
		public By btnBookTourResrv = By.xpath("//button[contains(text(),'Book Tour')]");
		public By btnSaveReserv = By.xpath("//button[contains(text(),'Save')]");
		
		public void validateInhouseReservation(){
			/*waitUntilElementVisibleBy(driver, click_ReservationLink, 120);					
			driver.findElement(click_ReservationLink).click();	*/	   
							
			waitUntilElementVisibleBy(driver, btnBookTourResrv, 120);
			tcConfig.updateTestReporter("ReservationPage", "validateInhouseReservation", Status.PASS, "Reservation Details page is displayed");
			if(driver.findElement(lblMarketingStatus).getText().contains("In-House"))
			{		
				System.out.println(driver.findElement(lblMarketingStatus).getText());
				tcConfig.updateTestReporter("ReservationPage", "validateInhouseReservation", Status.PASS, "In-house reservation is displyed");
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "validateInhouseReservation", Status.FAIL, "Failed while displaying Reservation");
			}
			waitUntilObjectVisible(driver, btnBookTourResrv, 120);
			clickElementJSWithWait(btnBookTourResrv);
			 waitForSometime(tcConfig.getConfig().get("MedWait"));
			 /*clickElementJSWithWait(tourlanguage);
				WebElement tourlanguageSelect = driver.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
				tourlanguageSelect.click();*/
			 Select select = new Select(driver.findElement(tourlanguage));			 
			 select.selectByVisibleText("English");
			 waitForSometime(tcConfig.getConfig().get("MedWait"));
			 clickElementJSWithWait(btnSaveReserv);			
		}
		
		public By btnAddAdditionalGuestNew = By.xpath("//button[contains(text(),'Add Additional Guest')]");
		public By txtPhoneNumberold = By.xpath("((//span[contains(text(),'Phone Number')])[2]/following::div//a)[1]");
		public By txtEmailold =By.xpath("((//span[contains(text(),'Email')])[1]/following::div//a)[1]");
		public By editPhoneNumber = By.xpath("//button[@title='Edit Phone Number']");
		public By phnNumber = By.xpath("//input[@name='TMPhoneNumber__c']");
		//public By email = By.xpath("//input[@name='TMEmail__c']");
		public By btnSavemodify = By.xpath("//button[contains(text(),'Save')]");
		public By txtPhoneNumberUpdated = By.xpath("((//span[contains(text(),'Phone Number')])[2]/following::div//a)[1]");
		public By txtEmailUpdated =By.xpath("((//span[contains(text(),'Email')])[1]/following::div//a)[1]");
		public By txtMobileClick = By.xpath("//label[contains(text(),'Mobile Number')]/..//div//input");
		
		public void validateEmailPhoneUpdate(){
			waitUntilElementVisibleBy(driver, btnAddAdditionalGuestNew, 120);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,300)");
			String phoneOld = getTextFromBodyBy(txtPhoneNumberold);
			String emailOld = getTextFromBodyBy(txtEmailold);			
			waitUntilElementVisibleBy(driver, txtEmailold, 120);
			clickElementJSWithWait(editPhoneNumber);	
			waitUntilElementVisibleBy(driver, phnNumber, 120);
			 driver.findElement(phnNumber).click();
			 driver.findElement(phnNumber).clear();
			 String Phone=getRandomString(10);
			 fieldDataEnter(phnNumber, Phone);
			 waitUntilElementVisibleBy(driver, email, 120);
			 driver.findElement(email).click();
			 driver.findElement(email).clear();				 
			fieldDataEnter(email,getRandomStringEmail());
			driver.findElement(email).sendKeys(Keys.ENTER);
			waitUntilElementVisibleBy(driver, txtMobileClick, 120);
			 driver.findElement(txtMobileClick).click();
			 waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(btnSavemodify);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String phoneUpdated = getTextFromBodyBy(txtPhoneNumberUpdated);
			String emailUpdated =getTextFromBodyBy(txtEmailUpdated);	
			if(!phoneOld.equals(phoneUpdated)){
				tcConfig.updateTestReporter("ReservationPage", "validateEmailPhoneUpdate",
						Status.PASS, "Phone Number updated to:"+phoneUpdated);
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateEmailPhoneUpdate",
						Status.FAIL, "Failed while updating Phone Number");
			}
			
			if (!emailOld.equals(emailUpdated)) {
				tcConfig.updateTestReporter("ReservationPage", "validateEmailPhoneUpdate",
						Status.PASS, "First Name updated to:"+emailUpdated);
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateEmailPhoneUpdate",
						Status.FAIL, "Failed while updating Email");
			}
			
		}
		public By drpValue = By.xpath("(//div[contains(@class,'select-options popupTargetContainer')]//div//ul//li//a)[2]");
		public By imgReports = By.xpath("//img[contains(@src,'Report')]");
		public By txtRecents = By.xpath("//span[@title='Recent']");
		public By lnkMyChangeReq = By.xpath("//button[@title='My Change Requests']/../..");
		public By txtTotalRecords = By.xpath("//div[@title='Total Records']");
		public By caseNumber = By.xpath("(//tr[contains(@class,'dataRow')]//td//a[starts-with(text(),'0000') and starts-with(@href,'/wvo/s/detail/5')])");
		public By date = By.xpath("//td[@class='nowrapRight']");
		public By imgHome = By.xpath("//img[contains(@src,'Home')]");
		
		
		
		
		public void createChangeRequestInhouseMarketer(){
			try {
				
				
				waitUntilElementVisibleBy(driver, click_ReservationLink, 120);
				
				if(verifyObjectDisplayed(click_ReservationLink)){
				waitUntilElementVisibleBy(driver, click_ReservationLink, 120);
				clickElementJSWithWait(click_ReservationLink);
				tcConfig.updateTestReporter("ReservationPage", "click Reservation Link", Status.PASS, "Reservation Link click");			
				
				}else
				{
					tcConfig.updateTestReporter("ReservationPage", "verifyAdditionalGuest", Status.FAIL, "Reservation Link click");
				}
				//waitForSometime(tcConfig.getConfig().get("MedWait"));
				//tcConfig.updateTestReporter("ReservationPage", "verifyAdditionalGuest", Status.PASS, "Reservation Details page is displayed");
				//waitUntilElementVisibleBy(driver, btnBookTour, 120);
				
				/*if(verifyObjectDisplayed(click_ReservationLink)) {
					waitUntilElementVisibleBy(driver, click_ReservationLink, 120);					
					driver.findElement(click_ReservationLink).click();		   
									
					waitUntilElementVisibleBy(driver, btnBookTour, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}else {

					tcConfig.updateTestReporter("createChangeRequestMarketer", "create Change Req", Status.FAIL, "createChangeReq not present located");
				}*/
				
				waitUntilElementVisibleBy(driver, createChangeReq, 120);
				if(verifyObjectDisplayed(createChangeReq)) {
					clickElementBy(createChangeReq);
					tcConfig.updateTestReporter("createChangeRequestMarketer", "create Change Req", Status.PASS, "createChangeReq button located");
					
				}else {

					tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.FAIL, "createChangeReq not present located");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(changeReqTypeMarketer);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("//a[@title='U-EMPLOYEE']")).click();
				//clickElementJSWithWait(drpValue);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(changeReqReason);
				String ReqReason =randomAlphaNumeric(3);
				fieldDataEnter(changeReqReason, ReqReason);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(changeReqReason).sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(changeReqSave);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				createdCase = driver.findElement(caseCreated).getText().trim();	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				tcConfig.updateTestReporter("createChangerequest", "create Change Req", Status.FAIL, "createChangeReq not present located");
			}
			
		}
		//public By txtCases= By.xpath("//span[@title='Cases']");
		public void getlatestCase_Latest() throws AWTException {		
			//waitUntilElementVisibleBy(driver, caseCreated, 120);
					if (verifyObjectDisplayed(caseCreated)) {
						/*createdCase = driver.findElement(caseCreated).getText();
						System.out.println("created NoteID: " + createdCase);
						tcConfig.updateTestReporter("ReservationPage", "getlatestCase_Latest", Status.PASS,"created NoteID: " + createdCase);*/
						driver.findElement(caseCreated).click();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("ReservationPage", "getlatestCase_Latest", Status.PASS, "Note created sucessfully");					
					} else {
						tcConfig.updateTestReporter("ReservationPage", "getlatestCase_Latest", Status.FAIL, "Note not created");
					}
					driver.navigate().back();
					//waitForSometime(tcConfig.getConfig().get("MedWait"));				
		}
		public By caseCreatedNew = By.xpath("//span[contains(text(),'U-CONTRACTED GROUP')]/../../..//th//span//a");
		public By lnkAllReports =  By.xpath("//a[contains(text(),'Public Reports')]");
		public By txtSerachReports = By.xpath("//input[contains(@class,'search-text-field slds-input input uiInput')]");
		
		private By lnkFilters = By.xpath("//button[contains(@class,'toggleFilter')]");
		private By drpDwnRange = By.xpath("//label[text()='Range']/..//button");
		private By lnkCustom = By.xpath("//span[text()='Custom']/../..");
		private By txtBoxStartTime = By.xpath("//label[text()='Start Date']/..//input");
		private By txtBoxEndTime = By.xpath("//label[text()='End Date']/..//input");
		private By btnApplyMobUi = By.xpath("//button[text()='Apply']");
		protected By caseList=By.xpath("//table[@class='data-grid-table data-grid-full-table']//tr//td/div/div/a");
		protected By homeTab=By.xpath("//ul[contains(@class,'slds-grid slds-grid_vertical slds-grid_align-center slds-grid_vertical-align-center ')]/li[2]/a/img");
		/*
		 * Added Ajib Parida
		 */
		public void getReportsMobile()
		{
			driver.switchTo().frame(0);
			waitUntilElementVisibleBy(driver, lnkFilters, 120);
			clickElementJSWithWait(lnkFilters);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			driver.findElement(By.xpath("//p[text()='Opened Date']")).click();  
			
			waitUntilElementVisibleBy(driver, drpDwnRange, 120);
			//new Select(driver.findElement(drpDwnRange)).selectByIndex(0);
			clickElementJSWithWait(drpDwnRange);
			//driver.findElement(drpDwnRange).click();
			waitUntilElementVisibleBy(driver, lnkCustom, 120);
			clickElementBy(lnkCustom);
			//driver.findElement(lnkCustom).click();
			waitUntilElementVisibleBy(driver, txtBoxStartTime, 120);
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Calendar calobj = Calendar.getInstance();
			String todaysDate = df.format(calobj.getTime());
			fieldDataEnter(txtBoxStartTime, todaysDate);
			waitUntilElementVisibleBy(driver, txtBoxEndTime, 120);
			fieldDataEnter(txtBoxEndTime, todaysDate);
			//waitUntilElementVisibleBy(driver, btnDone, 120);
			//clickElementBy(btnDone);
			waitUntilElementVisibleBy(driver, btnApplyMobUi, 120);
			clickElementJSWithWait(btnApplyMobUi);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			List<WebElement> allCasesList=driver.findElements(caseList);
			System.out.println(allCasesList.size());
	
			/*for(int i=0;i<=allCasesList.size()-1;i++)
			{
				System.out.println(i);
				
			}*/
			
			if(verifyObjectDisplayed(caseList))
			{
				tcConfig.updateTestReporter("ReservationPage", "validateToday/current Date cases", Status.PASS, "we should able to see all cases which are created by today");
			}else{
				tcConfig.updateTestReporter("ReservationPage", "validateToday/current Date cases", Status.FAIL, "we should not able to see all cases which are created by today");
			}
			
			driver.switchTo().defaultContent();
			ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs2.get(0));
		    
		    waitForSometime(tcConfig.getConfig().get("MedWait"));
		    clickElementJS(homeTab);
		    
			//table[@class='data-grid-table data-grid-full-table']//tr

			/*WebElement txtCases =driver.findElement(By.xpath("//span[@title='Cases']"));	
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);",txtCases);	
			waitForSometime(tcConfig.getConfig().get("LowWait"));*/
			/*clickElementJS(txtCases);
			waitForSometime(tcConfig.getConfig().get("MedWait"));			
			List<WebElement> button =driver.findElements(caseCreatedNew);		
			System.out.println(button.get(button.size()-1).getText());
			String caseNew = button.get(button.size()-1).getText();	//table[@class='data-grid-table data-grid-full-table']//tr	
			if(caseNew.trim() != null){
				System.out.println("Pass");
				
			}else{
				System.out.println("Fail");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, imgReports, 120);
			clickElementJSWithWait(imgReports);
			driver.navigate().refresh();
			waitUntilElementVisibleBy(driver, txtRecents, 120);*/
			/*clickElementBy(lnkAllReports);
			waitUntilElementVisibleBy(driver, txtSerachReports, 120);*/
			/*if(verifyElementDisplayed(driver.findElement(txtSerachReports)))			
			{		
				fieldDataEnter(txtSerachReports, testData.get("Menu2"));
				
				tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.PASS, "Search for My Chnage Request in search box");
			
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
			
				driver.findElement(txtSerachReports).sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.PASS, "My Chnage Request is displayed in screen");
			}
			else{
				tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.FAIL, "Failed while loading My Chnage Request");
			}*/
			/*clickElementBy(lnkMyChangeReq);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement Frame=driver.findElement(By.xpath(""));
			driver.switchTo().frame(Frame);	//iframe[@class='isView reportsReportBuilder']
			waitUntilElementVisibleBy(driver, txtTotalRecords, 120);
			List<WebElement> elements = driver.findElements(caseNumber);
			System.out.println("Number of elements:" +elements.size());		
			String pattern = "MM/d/YYYY";
			SimpleDateFormat simpleDateFormat =
			        new SimpleDateFormat(pattern);
			Date date1 = new Date();
			String curdate = simpleDateFormat.format(date1);
			System.out.println(curdate);		
			List<WebElement> tblDate = driver.findElements(date);
			System.out.println("Number of elements:" +tblDate.size());	
			System.out.println(tblDate.get(tblDate.size()-1).getText());
			String Date = tblDate.get(tblDate.size()-1).getText();		
			if(Date.contains(curdate)){
				if(caseNew.toString() != null)
				tcConfig.updateTestReporter("createChangeRequestInhouseMarketer", "create Change Req", Status.PASS, "Current date is displyed for Case created");
				else{}
			}
			else{
				tcConfig.updateTestReporter("createChangeRequestInhouseMarketer", "create Change Req", Status.FAIL, "Failed while displying current date");
			}
			driver.switchTo().parentFrame();
			driver.switchTo().defaultContent();
			clickElementBy(imgHome);
			waitForSometime(tcConfig.getConfig().get("MedWait"));*/
		}

		public By drpChangeReqType = By.xpath("//div[@data-aura-class='uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short']//li//a[@title='U-DNG']");
				
		/**
		 * 
		 * Added by Ritam
		 */
		
		public void verifyCreateChangeRequest() {
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            driver.navigate().refresh();
            waitUntilElementVisibleBy(driver, createChangeReq, 120);
            if (verifyObjectDisplayed(createChangeReq)) {
                   tcConfig.updateTestReporter("ReservationPage", "verifyCreateChangeRequest", Status.PASS,
                                "createChangeReq button located");
                   clickElementJS(createChangeReq);
            } else {

                   tcConfig.updateTestReporter("ReservationPage", "verifyCreateChangeRequest", Status.FAIL,
                                "createChangeReq not present located");
            }
            waitForSometime(tcConfig.getConfig().get("MedWait"));
            clickElementJS(changeReqType);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            clickElementJS(drpChangeReqType);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            clickElementBy(changeReqReason);
            fieldDataEnter(changeReqReason, testData.get("ChangeReqReasion"));
            clickElementBy(changeReqSave);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            waitUntilElementVisibleBy(driver, caseCreated, 120);
            createdCase = driver.findElement(caseCreated).getText().trim();
            tcConfig.updateTestReporter("ReservationPage", "verifyCreateChangeRequest", Status.PASS,
                         "change request created: " + createdCase);
            driver.findElement(caseCreated).click();
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            tcConfig.updateTestReporter("ReservationPage", "verifyCreateChangeRequest", Status.PASS,
                         "Note created sucessfully");

     }

		
		
private By lnkCreatedAddGuest = By.xpath("//span[contains(@class,'toastMessage')]//a");
		
		
		
		
		/**
		 * 
		 * 
		 * Added by Ritam
		 */
		
	
		
		public void addAddtnlGuest()
		{
			waitUntilObjectVisible(driver, firstName, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions a = new Actions(driver);
			
			
			WebElement eleFirstName = driver.findElement(firstName);
			a.sendKeys(eleFirstName, generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
			//fieldDataEnter(firstName, generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement eleLastName = driver.findElement(lastName);
			a.sendKeys(eleLastName, generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
			//fieldDataEnter(lastName, generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			a.build().perform();
			String strPhoneNum=getRandomString(10);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(txtPhoneNum);
			driver.findElement(txtPhoneNum).sendKeys(strPhoneNum);
			//fieldDataEnter(txtPhoneNum, strPhoneNum);
			/*a.sendKeys(driver.findElement(txtPhoneNum), strPhoneNum);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			*/
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(chkBoxSameAsPrimaryGuest);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String text = driver.findElements(txtBoxGuestAddress).get(0).getAttribute("value");
			if(text.length()>0)
			{
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(txtPhoneNum);
				driver.findElement(txtPhoneNum).sendKeys(strPhoneNum);
				tcConfig.updateTestReporter("ReservationPage", "verifyChkBoxSameAsPrimaryGuest", Status.PASS, "Address details are reflecting after clicking checkbox");
			}
			else
			{
				fieldDataEnter(Street, testData.get("StreetName"));
				fieldDataEnter(City, testData.get("City"));
				fieldDataEnter(State, testData.get("State"));
				fieldDataEnter(Country, testData.get("Country"));
				fieldDataEnter(Postal, testData.get("ZIP"));
			}
			clickElementJS(addGuestsaveBtn);
			waitUntilObjectVisible(driver, lnkCreatedAddGuest, 120);
			if (verifyObjectDisplayed(lnkCreatedAddGuest)){
				clickElementJS(lnkCreatedAddGuest);
				tcConfig.updateTestReporter("ReservationPage", "addAdditionalGuest", Status.PASS, "Additional guest added ");
			}else{
				tcConfig.updateTestReporter("ReservationPage", "addAdditionalGuest", Status.FAIL, "Additional guest NOT added ");
			}
		}
		
		/*
		 * Method-Search Reservation in nativeUI
		 * Designed By-JYoti
		 * */
		public void searchReservation_Native(){
			waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);   		
			tcConfig.updateTestReporter("ReservationPage", "searchReservation", Status.PASS, "User is navigated to Reservations Page");		
		fieldDataEnter(txtReservationSearch, testData.get("ReservationNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));						
		WebElement seacrhResOwnerLink=driver.findElement(imgResrv);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		seacrhResOwnerLink.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("ReservationPage", "searchReservation", Status.PASS, "Reservations is displayed");
		}
		
		/*
		 * Method-validate Score Incentive in Native UI
		 * Designed By-JYoti
		 * */
		public void scoreIncentivebtn_Native(){
						
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilObjectVisible(driver, btnIncentiveReserv, 120);											
				clickElementJSWithWait(btnIncentiveReserv);
				tcConfig.updateTestReporter("ReservationPage", "scoreandDisableIncentivebtn_Native", Status.PASS,"Incentive Eligible button is clicked");
				waitForSometime(tcConfig.getConfig().get("MedWait"));							
				waitUntilObjectVisible(driver, softscore, 120);
				clickElementJSWithWait(softscore);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				
			
		}
		
		/*
		 * Method-validate ACS status in native UI
		 * Designed By-JYoti
		 * */
		public void validateACSScoreNative (){
			waitUntilObjectVisible(driver, txtACS, 120);
			String strScore = driver.findElement(txtACS).getText();
			if (strScore.equalsIgnoreCase(testData.get("QualificationScore"))){
				tcConfig.updateTestReporter("ReservationPage", "validateACSScore", Status.PASS,
						"Qualification Score is: "+strScore);
			} else {
				tcConfig.updateTestReporter("ReservationPage", "validateACSScore", Status.FAIL,
						"Failed while getting Qualification Score");
			}
		}
		
		public void validateACSScoreNativeOwner (){
			waitUntilObjectVisible(driver, txtACSOwner, 120);
			String strScore = driver.findElement(txtACSOwner).getText();
			if (strScore.equalsIgnoreCase(testData.get("QualificationScore"))){
				tcConfig.updateTestReporter("ReservationPage", "validateACSScore", Status.PASS,
						"Qualification Score is: "+strScore);
			} else {
				tcConfig.updateTestReporter("ReservationPage", "validateACSScore", Status.FAIL,
						"Failed while getting Qualification Score");
			}
		}
		
		/*
		 * Method-validate Incentive button is disabled in Native UI
		 * Designed By-JYoti
		 * */
		public void validateIncentiveButtonDisabled_Native(){
			waitUntilObjectVisible(driver, txtReservation_Native, 120);
			if(driver.findElement(btnIncentiveReserv).getAttribute("disabled").contains("true")){
				tcConfig.updateTestReporter("ReservationPage", "validateIncentiveButtonDisabled_Native", Status.PASS,"Incentive Eligible button is disabled");
			}
			else{
				tcConfig.updateTestReporter("ReservationPage", "validateIncentiveButtonDisabled_Native", Status.FAIL,"Incentive Eligible button is not disabled");
			}
		}
		
		/*
		 * Method-validate Book Tour for Hard Stop Messgae in Native UI
		 * Designed By-JYoti
		 * */
		public void bookTourForHardStop(){
			
				 	waitUntilObjectVisible(driver, btnBookTourResrv, 120);
					clickElementJSWithWait(btnBookTourResrv);
					 waitForSometime(tcConfig.getConfig().get("LowWait"));					 
					 clickElementJSWithWait(btnSaveReserv);
				 	waitUntilElementVisibleBy(driver, errorMsg, 120);
				 	String strmsg = driver.findElement(errorMsg).getText();
										
						waitForSometime(tcConfig.getConfig().get("LowWait"));						
						tcConfig.updateTestReporter("ReservationPage", "bookTourForHardStop", Status.PASS,"User can see Error Messgae displayed as :- " +strmsg);
						clickElementJS(bntCancel);
						waitForSometime(tcConfig.getConfig().get("LowWait"));									
		}
		
		public void searchReservationTour()
		{
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
				new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(inputSearchTourRes, testData.get("ReservationNumber"));
			WebElement wb=driver.findElement(inputSearchTourRes);
			wb.sendKeys(Keys.ENTER);
			wb.sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']"), 240);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if(driver.findElements(By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']")).size()==0)
			{
				waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
				new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(inputSearchTourRes, testData.get("ReservationNumber"));
				WebElement web=driver.findElement(inputSearchTourRes);
				web.sendKeys(Keys.ENTER);
				web.sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']"), 120);
			}
			if(verifyObjectDisplayed(By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']")))
			{
				tcConfig.updateTestReporter("ReservationPage", "searchReservation", Status.PASS, "Searched Result is displayed");
				clickElementBy(By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']"));
			}
			else
			{
				tcConfig.updateTestReporter("ReservationPage", "searchReservation", Status.FAIL, "Searched Result is not displayed");
			}
		}
		
		/*
		 * Method-validate Book Tour for Hard Stop Messgae in Mobile UI
		 * Designed By-JYoti
		 * */

		public void bookTourHardStop_Mobile() throws Exception{
			       	waitForSometime(tcConfig.getConfig().get("MedWait"));
				 	waitUntilObjectVisible(driver, btnBookTour_Moblie, 120);
					clickElementJSWithWait(btnBookTour_Moblie);
					 waitForSometime(tcConfig.getConfig().get("LowWait"));					 					 
				 	waitUntilElementVisibleBy(driver, hardStop_Mobile, 120);
				 	String strmsg = driver.findElement(hardStop_Mobile).getText();										
						waitForSometime(tcConfig.getConfig().get("LowWait"));						
						tcConfig.updateTestReporter("ReservationPage", "bookTourHardStop_Mobile", Status.PASS,"User can see Error Message displayed as :- " +strmsg);
						clickElementJS(btnCancel_Mobile);
						waitForSometime(tcConfig.getConfig().get("LowWait"));																
		}
		
		/*
		 * Method-validate Book Tour for Soft Stop Messgae in Mobile UI
		 * Designed By-JYoti
		 * */
		public void bookTourSoftStop_Mobile() throws Exception{
			 
						waitForSometime(tcConfig.getConfig().get("MedWait"));	   		
					 	waitUntilObjectVisible(driver, btnBookTour_Moblie, 120);
						clickElementJSWithWait(btnBookTour_Moblie);
						 waitForSometime(tcConfig.getConfig().get("LowWait"));					 					 
					 	waitUntilElementVisibleBy(driver, hardStop_Mobile, 120);
					 	String strmsg = driver.findElement(hardStop_Mobile).getText();										
							waitForSometime(tcConfig.getConfig().get("LowWait"));						
							tcConfig.updateTestReporter("ReservationPage", "bookTourSoftStop_Mobile", Status.PASS,"User can see Message displayed as :- " +strmsg);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if(verifyObjectDisplayed(btnCancel_Mobile)){
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							clickElementJS(btnCancel_Mobile);
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							tcConfig.updateTestReporter("ReservationPage", "bookTourSoftStop_Mobile", Status.PASS,"User should be able to create Tour");
							}
							else{
								tcConfig.updateTestReporter("ReservationPage", "bookTourSoftStop_Mobile", Status.FAIL,"User should not be able to create Tour");
							}   						   				
			}
		
		/*
		 * Method-validate related reservation checkbox in native UI
		 * Designed By-JYoti
		 * */
		public void validateRelatedReservations(){        	

				waitUntilElementVisibleBy(driver, relatedResrv, 120);	
				if(driver.findElement(chkMultiRes).getAttribute("className").contains("slds-checkbox_faux"))
				{
					tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservations",
							Status.PASS, "Multiple reservation checkbox is checked");					
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservations",
							Status.FAIL, "Multiple reservation check box is not checked");
				}
				String strAll_Reservation_number = driver.findElement(relatedResrvFields).getText();	
				System.out.println(strAll_Reservation_number);					
				if (strAll_Reservation_number.contains(";"))
				{
				    System.out.println("Reservation numbers have ;");
				    String[] strsplit_Reservations = strAll_Reservation_number.split(";");
				    for(int intk =0;intk<strsplit_Reservations.length;intk++)
				    {
				        System.out.println("Reservation number is ==>"+strsplit_Reservations[intk]);
				        tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservations", Status.PASS,"The related reservation numbers present are :" +strsplit_Reservations[intk]);			        
				    }
				}
				else{
					tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservations", Status.FAIL,"User should not be able to view Related Reservations");
				}		
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement agent = driver.findElement(tabAddressDetails);									 								 										 
				je.executeScript("arguments[0].scrollIntoView(true);",agent);				
				waitUntilElementVisibleBy(driver, relatedResrvations, 120);
				clickElementJS(relatedResrvations);
				waitUntilElementVisibleBy(driver, txtRelatedResv, 120);
				List<WebElement> elements = driver.findElements(lstRelatedResrv);
			    System.out.println("Number of elements:" +elements.size());
				tcConfig.updateTestReporter("HomePage", "validateRelatedReservations", Status.PASS, "Number of Related Reservations displayed:-" +elements.size());			  			
		}


				
		/*
		 * Method-validate related reservation checkbox in Mobile UI
		 * Designed By-JYoti
		 * */	
		public void validateRelatedReservationMobile(){

				/*waitUntilElementVisibleBy(driver, clickReservMobile, 120);
				clickElementJS(clickReservMobile);*/
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, imgMultiRes, 120);	
				if(driver.findElement(imgMultiRes).getAttribute("className").contains(" checked"))
				{
					tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile",
							Status.PASS, "Multiple reservation checkbox is checked");					
				}
				else
				{
					tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile",
							Status.FAIL, "Multiple reservation check box is not checked");
				}
				String strAll_Reservation_number = driver.findElement(relatedResrvMobile).getText();	
				System.out.println(strAll_Reservation_number);					
				if (strAll_Reservation_number.contains(";"))
				{
				    System.out.println("Related Reservation numbers have ;");
				    String[] strsplit_Reservations = strAll_Reservation_number.split(";");
				    for(int intk =0;intk<strsplit_Reservations.length;intk++)
				    {
				        System.out.println("Reservation number is ==>"+strsplit_Reservations[intk]);
				        tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile", Status.PASS,"The related reservation numbers present are :" +strsplit_Reservations[intk]);			        
				    }
				}
				else{
					tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile", Status.FAIL,"User should not be able to view Related Reservations");
				}	
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement agent = driver.findElement(lnkRelatedResv);									 								 										 
				je.executeScript("arguments[0].scrollIntoView(true);",agent);				
				waitUntilElementVisibleBy(driver, tabGuestDetails, 120);
				clickElementJS(lnkRelatedResv);
				waitUntilElementVisibleBy(driver, txtRelatedResv, 120);
				List<WebElement> elements = driver.findElements(lstRelatedResrv);
				System.out.println("Number of elements:" +elements.size());
				tcConfig.updateTestReporter("HomePage", "validateRelatedReservationMobile", Status.PASS, "Number of Related Reservations displayed:-" +elements.size());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("LowWait"));			   
		}

		/*
		 * Method-validate extended stay checkbox in Mobile UI
		 * Designed By-JYoti
		 * */
		public void validateExtendedStayMobile(){
		   
		    		waitUntilElementVisibleBy(driver, clickReservMobile, 120);
		    		clickElementJS(clickReservMobile);
		    		waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, imgExtendedStay, 120);	
					if(driver.findElement(imgExtendedStay).getAttribute("className").contains(" checked"))
					{
						tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile",
								Status.PASS, "Extended reservation checkbox is checked");					
					}
					else
					{
						tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile",
								Status.FAIL, "Extended reservation check box is not checked");
					}
					String strAll_Reservation_number = driver.findElement(relatedResrvMobile).getText();	
					System.out.println(strAll_Reservation_number);					
					if (strAll_Reservation_number.contains(";"))
					{
					    System.out.println("Related Reservation numbers have ;");
					    String[] strsplit_Reservations = strAll_Reservation_number.split(";");
					    for(int intk =0;intk<strsplit_Reservations.length;intk++)
					    {
					        System.out.println("Reservation number is ==>"+strsplit_Reservations[intk]);
					        tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile", Status.PASS,"The related reservation numbers present are :" +strsplit_Reservations[intk]);			        
					    }
					}
					else{
						tcConfig.updateTestReporter("ReservationPage", "validateRelatedReservationMobile", Status.FAIL,"User should not be able to view Related Reservations");
					}	
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement agent = driver.findElement(lnkRelatedResv);									 								 										 
					je.executeScript("arguments[0].scrollIntoView(true);",agent);				
					waitUntilElementVisibleBy(driver, tabGuestDetails, 120);
					clickElementJS(lnkRelatedResv);
					waitUntilElementVisibleBy(driver, txtRelatedResv, 120);
					List<WebElement> elements = driver.findElements(lstRelatedResrv);
					System.out.println("Number of elements:" +elements.size());
					tcConfig.updateTestReporter("HomePage", "validateRelatedReservationMobile", Status.PASS, "Number of Related Reservations displayed:-" +elements.size());
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("LowWait"));  				   
		}
		
		/*
		 * Method-validate ACS status in MobileUI
		 * Designed By-JYoti
		 * */
		public void validateACSInMobile(){
			waitUntilObjectVisible(driver, txtACSMobile, 120);
			String strScore = driver.findElement(txtACSMobile).getText();
			if (strScore.equalsIgnoreCase(testData.get("QualificationScore"))){
				tcConfig.updateTestReporter("ReservationPage", "validateACSInMobile", Status.PASS,
						"Qualification Score is: "+strScore);
			} else {
				tcConfig.updateTestReporter("ReservationPage", "validateACSInMobile", Status.FAIL,
						"Failed while getting Qualification Score");
			}
		}
		
		/*
		 * Method-Validate incentive eligible disabled in Native UI
		 * Designed By-JYoti
		 * */
		public void verifyIncentiveEligibleDisabled_Mobile(){
		  
					waitForSometime(tcConfig.getConfig().get("LowWait"));																			
					if(driver.findElement(btnIncentiveEligible).getAttribute("disabled").contains("true")){
						tcConfig.updateTestReporter("ReservationPage", "verifyIncentiveEligibleDisabled_Mobile", Status.PASS,"Incentive Eligible button is disabled");
					}
					else{
						tcConfig.updateTestReporter("ReservationPage", "verifyIncentiveEligibleDisabled_Mobile", Status.FAIL,"Incentive Eligible button is not disabled");
					}
				
		    }
		/*
		 * Method-validate Book Tour native UI
		 * Designed By-JYoti
		 * */
		public void bookTour_Native(){
			waitUntilObjectVisible(driver, btnBookTourResrv, 120);
			clickElementJSWithWait(btnBookTourResrv);
			 waitForSometime(tcConfig.getConfig().get("LowWait"));					 
			 clickElementJSWithWait(btnSaveReserv);
			 waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		
		/*
		 * Method-validate Book Tour for Soft Stop Messgae in Native UI
		 * Designed By-JYoti
		 * */
		public void bookTourSoftStop() throws Exception{
						 
				 	waitUntilObjectVisible(driver, softScoreMsg, 120);
				 	String strmsg = driver.findElement(softScoreMsg).getText();			
					 waitForSometime(tcConfig.getConfig().get("LowWait"));					 										
						tcConfig.updateTestReporter("ReservationPage", "bookTourSoftStop", Status.PASS,"User can see  Messgae displayed as :- " +strmsg);													
		}

}
		
		
	

	
	

