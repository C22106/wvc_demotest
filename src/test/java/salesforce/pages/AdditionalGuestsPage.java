package salesforce.pages;

import java.awt.AWTException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;


public class AdditionalGuestsPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(AdditionalGuestsPage.class);
	public static String guestId = "";

	public AdditionalGuestsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	static final String SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	// private By additionalGuestslink= By.xpath("//span[@class='label slds-truncate
	// slds-text-link' and contains(text(),'Additional Guests')]");
	private By newBtn = By.xpath("//div[@title='New']");
	private By ownerRadioBtn = By.xpath("(//label[@class='slds-radio']//span[contains(@class,'slds-radio')])[1]");
//	private By leadRadioBtn= By.xpath("//div[@class='changeRecordTypeRightColumn slds-form-element__control']/div[3]");
//	private By preleadRadioBtn= By.xpath("//div[@class='changeRecordTypeRightColumn slds-form-element__control']/div[5]");
	private By nextBtnPop = By.xpath("//span[text()='Next']");
	private By firstName = By.xpath("//span[text()='First Name']/../../input");
	private By lastName = By.xpath("//span[text()='Last Name']/../../input");
	private By pidTxt = By.xpath("//span[text()='PID']/../../input");
	private By saveBtn = By.xpath("//button[@title='Save']/span[text()='Save']");
	private By additionalGuestID = By.xpath("//div//span[contains(text(),'AG-')]");
//	private By additionalGuestID=By.xpath("//lightning-formatted-text[starts-with(text(),'AG')]");
	private By ownerSearchBoxEdit = By.xpath("//input[contains(@name,'search-input')]");
	private By bookTourBtn = By.xpath("(//button[text()='Book Tour'])[1]");
	private By popUpSaveBtn = By.xpath("//button[text()='Save']");
	private By commentsEdit = By.xpath("//Label[text()='Comments']/../..//input[@type='text']");
	private By createdTourtxt = By.xpath("//span[text()='Tour Record']/a/div");
	private By giftEligbleBtn = By.xpath("//button[text()='Book Tour']/../../..//button[text()='Incentive Eligible']");
	// private By crossBtn = By.xpath("//div[@role='combobox']//span/button");
	private By reqSoftScoreBtn = By.xpath("//button[text()='Request Soft Score']");
//	private By creditBandCode =	By.xpath("//span[text()='"+testData.get("QualificationScore")+"']");
//	private By check  = By.xpath("//th[text()='Qualification Score Name']//..//..//..//a[@data-refid='recordId']");
	// private By checkQualificationScore = By.xpath("//span[text()='Qualification
	// Status']//..//..//span[text()='"+testData.get("QualificationScore")+"']");

	public void createAditionalGuest() {
		waitUntilObjectVisible(driver, newBtn, 120);
		clickElementBy(newBtn);
		String strGuestType = testData.get("GuestType");
		WebElement GuestTypeRadioBtn = null;
		waitUntilObjectVisible(driver, ownerRadioBtn, 120);
		if (strGuestType.equalsIgnoreCase("Owner")) {
			GuestTypeRadioBtn = driver.findElement(ownerRadioBtn);
		} else if (strGuestType.equalsIgnoreCase("Lead")) {
			GuestTypeRadioBtn = driver
					.findElement(By.xpath("(//label[@class='slds-radio']//span[contains(@class,'slds-radio')])[2]"));
		} else if (strGuestType.equalsIgnoreCase("Pre-Lead")) {
			GuestTypeRadioBtn = driver
					.findElement(By.xpath("(//label[@class='slds-radio']//span[contains(@class,'slds-radio')])[3]"));
		}
		waitUntilElementVisible(driver, GuestTypeRadioBtn, 120);
		GuestTypeRadioBtn.click();
		clickElementBy(nextBtnPop);
		waitUntilObjectVisible(driver, firstName, 120);
		fieldDataEnter(firstName, testData.get("FirstName"));
		fieldDataEnter(lastName, testData.get("LastName"));
		String tel = getRandomString(10);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//input[@type='tel']")).clear();
		driver.findElement(By.xpath("//input[@type='tel']")).sendKeys(tel);
		// fieldDataEnter(pidTxt, PID);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//input[@type='email']")).clear();
		driver.findElement(By.xpath("//input[@type='email']")).sendKeys("xyz@wyn.com");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(saveBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> AGid = driver.findElements(additionalGuestID);
		waitUntilObjectVisible(driver, AGid.get(0), 120);
		if (verifyObjectDisplayed(additionalGuestID)) {
//				WebElement addGuestCreated = driver.findElement(additionalGuestID);
			guestId = AGid.get(0).getText();
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createOwnerAditionalGuest", Status.PASS,
					"Additional guest has been created successfully " + "with Guest ID: " + guestId);
		} else {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createOwnerAditionalGuest", Status.FAIL,
					"Additional guest NOT created successfully ");
		}
	}

	public void backButtonClick() {
		driver.navigate().back();
	}

	public void searchAdditionalGuests(String... id) {
		String guestId = "";
		if (!(id == null)) {
			guestId = id[0];
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("TourRecordsPage", "searchForTour", Status.PASS,
					"User is navigated to Reservations Page");
			fieldDataEnter(ownerSearchBoxEdit, guestId);
		}
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement seacrhResOwnerLink = driver.findElement(By.xpath("//th/span/a[contains(text(),'" + guestId + "')]"));
		seacrhResOwnerLink.click();
	}

	public void searchAdditionalGuests() {
		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("TourRecordsPage", "searchForTour", Status.PASS,
					"User is navigated to Reservations Page");
			fieldDataEnter(ownerSearchBoxEdit, guestId);
		}
		driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement seacrhResOwnerLink = driver.findElement(By.xpath("//th/span/a[contains(text(),'" + guestId + "')]"));
		seacrhResOwnerLink.click();
	}

	WebElement relatedTab;
	WebElement SearchLocation;

	public void checkQualificationScore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, giftEligbleBtn, 120);
		clickElementJSWithWait(giftEligbleBtn);

		waitUntilObjectVisible(driver, reqSoftScoreBtn, 120);

		clickElementJSWithWait(reqSoftScoreBtn);

	}

	public void bookTourfromGuestPage() {
		waitUntilElementVisibleBy(driver, bookTourBtn, 120);

		clickElementBy(bookTourBtn);
		waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);
		if (testData.get("CourtseyTourValidation").equalsIgnoreCase("Y")) {
			try {
				if (driver.findElement(commentsEdit).getAttribute("value").contains("booked as a Courtesy Tour")
						|| driver.findElement(commentsEdit).getAttribute("value")
								.contains("Please review the lead record.")) {
					tcConfig.updateTestReporter("AdditionalGuestsPage", "bookTourfromGuestPage", Status.PASS,
							"Error Message related to Courtsey tour is displayed during creation of Tours");
				} else {
					tcConfig.updateTestReporter("AdditionalGuestsPage", "bookTourfromGuestPage", Status.FAIL,
							"Error Message related to Courtsey tour is NOT displayed during creation of Tours");
				}
			} catch (Exception e) {
				tcConfig.updateTestReporter("AdditionalGuestsPage", "bookTourfromGuestPage", Status.FAIL,
						"Unable to read the error message text may be the locator is invalid");
			}
		}
		clickElementBy(popUpSaveBtn);
		waitUntilElementVisibleBy(driver, createdTourtxt, 120);
		if (verifyObjectDisplayed(createdTourtxt)) {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "bookTourfromGuestPage", Status.PASS,
					"Tour records has been created : " + driver.findElement(createdTourtxt).getText());
		} else {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "bookTourfromGuestPage", Status.PASS,
					"Unable to create" + " new tour");
		}
	}

	/* <Jyoti's code> */

	public By lnkAdditioanlGuest = By.xpath("(//span[contains(text(),'Additional Guests')])[1]");
	public By txtAdditionalGuestName = By.xpath("//span[contains(text(),'Additional Guests Name')]");
	public By tblAG = By.xpath("//table[@data-aura-class='uiVirtualDataTable']");
	public By btnIncneitveElligible = By.xpath("(//button[contains(text(),'Incentive Eligible')])[2]");
	public By header = By.xpath("//h2[contains(text(),'Check Qualification Score')]");
	public By text = By
			.xpath("//p[contains(text(),'This lead has an international address and does not require soft scoring.')]");
	public By tblAdditionalGuest = By.xpath("(//tr//th//span//a)");
	public By btnClose = By.xpath("//button[contains(text(),'Close')]");

	public void clickAdditionalGuest() {
		try {
			waitUntilElementVisibleBy(driver, lnkAdditioanlGuest, 120);
			clickElementJSWithWait(lnkAdditioanlGuest);

			waitUntilElementVisibleBy(driver, txtAdditionalGuestName, 120);
			List<WebElement> button = driver.findElements(tblAdditionalGuest);
			System.out.println("Number of elements:" + button.size());
			button.get(button.size() - 1).click();
			waitUntilElementVisibleBy(driver, btnIncneitveElligible, 120);
			clickElementJSWithWait(btnIncneitveElligible);
			waitUntilElementVisibleBy(driver, header, 120);
			if (driver.findElement(text).getText()
					.equals("This lead has an international address and does not require soft scoring.")) {
				tcConfig.updateTestReporter("ReservationPage", "clickAdditionalGuest", Status.PASS,
						"This lead has an international address and does not require soft scoring: is displayed");
			} else {
				tcConfig.updateTestReporter("ReservationPage", "clickAdditionalGuest", Status.FAIL,
						"Additonal Guest Details is not found");
			}
			clickElementJSWithWait(btnClose);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("AdditionalGuestsPage", "clickAdditionalGuest", Status.PASS,
					"Unable to create" + " new tour");
		}

	}

	public By btnAddAdditionalGuest = By.xpath("//button[contains(text(),'Add Additional Guest')]");
	public By txtAdditonalGuest = By.xpath("//h2[contains(text(),'Add Additional Guest')]");
	public By txtFirstName = By
			.xpath("//label[contains(text(),'First Name')]/..//div//input[contains(@class,'slds-input input')]");
	public By txtLastName = By
			.xpath("//label[contains(text(),'Last Name')]/..//div//input[contains(@class,'slds-input input')]");
	public By txtStreet = By.xpath("//label[contains(text(),'Street')]/..//div//input");
	public By txtCity = By.xpath("//label[contains(text(),'City')]/..//div//input");
	public By txtState = By.xpath("//label[contains(text(),'State')]/..//div//input");
	public By txtCountry = By.xpath("//label[contains(text(),'Country')]/..//div//input");
	public By txtPostalCode = By.xpath("//label[contains(text(),'Postal Code')]/..//div//input");
	public By btnSave = By.xpath("//button[contains(text(),'Save')]");
	public By txtSearchAddress = By.xpath("(//input[contains(@class,'slds-input slds-combobox')])[2]");
	public By suggestion = By.xpath("//div[@class='optionText']");

	//
	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * SOURCE.length());
			builder.append(SOURCE.charAt(character));
		}
		return builder.toString();
	}

	public void createAdditionalGuestNew() {

		try {
			waitUntilElementVisibleBy(driver, btnAddAdditionalGuest, 120);
			clickElementJSWithWait(btnAddAdditionalGuest);
			// waitUntilElementVisibleBy(driver, txtAdditonalGuest, 120);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String First = randomAlphaNumeric(3);
//			waitUntilElementVisibleBy(driver, txtFirstName, 120);
			fieldDataEnter(txtFirstName, First);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Last = randomAlphaNumeric(4);
//			waitUntilElementVisibleBy(driver, txtLastName, 120);
			fieldDataEnter(txtLastName, Last);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtSearchAddress, 120);
			WebElement ele = driver
					.findElement(By.xpath("//label[text()='Same as Primary Guest Address']/parent::div/div/input"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions act = new Actions(driver);
			act.moveToElement(ele).click().build().perform();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(btnSave);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createAdditionalGuestNew", Status.FAIL,
					"Failed while updating address");
		}
	}

	public static String createdTourID;
	private By latestNote_new = By.xpath("//span[contains(text(),'Additional Guest')]//a//div");

	public void getlatestAdditonalGuest() throws AWTException {

		// waitUntilElementVisibleBy(driver, latestNote_new, 120);
		if (verifyObjectDisplayed(latestNote_new)) {
			createdTourID = driver.findElement(latestNote_new).getText();
			System.out.println("created NoteID: " + createdTourID);
			tcConfig.updateTestReporter("ReservationPage", "getlatestTour", Status.PASS,
					"created Additional Guest: " + createdTourID);
			driver.findElement(latestNote_new).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("ReservationPage", "getlatestTour", Status.FAIL,
					"Additional Guest not created");
		}
	}

	private By btnCreateLead = By.xpath("//button[text()='Create Lead']");
	private By headerCreateLead = By.xpath("//h2[text()='Create New Lead']");
	private By btnCreateCustomer = By.xpath("//button[text()='Create Customer']");
	private By divSuccessMsg = By.xpath("//span[contains(@class,'toastMessage')]");
	private By btnIncentiveEligible = By
			.xpath("//div[contains(@class,'cTMAdditionalGuestButtons')]//button[text()='Incentive Eligible']");
	private By btnRequestSoftScore = By.xpath("//button[contains(text(),'Request Soft Score')]");
	private By fieldScoreBand = By.xpath("//p[@title='Score Band']/..//lightning-formatted-text");

	public void createLead() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnCreateLead, 120);
		clickElementJSWithWait(btnCreateLead);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, headerCreateLead, 120);
		scrollDownForElementJSBy(btnCreateCustomer);
		clickElementJS(btnCreateCustomer);
		waitUntilElementVisibleBy(driver, divSuccessMsg, 120);
		if (verifyObjectDisplayed(divSuccessMsg)) {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createLead", Status.PASS,
					"Lead is created successfully");
		} else {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createLead", Status.FAIL, "Lead creation failed");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(btnIncentiveEligible)) {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createLead", Status.PASS,
					"Incentive Eligible button is visible after creating lead");
		} else {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "createLead", Status.FAIL,
					"Incentive Eligible button is not visible after creating lead");
		}
	}

	
	
	/**
	 * 
	 * Added by Ritam
	 */
	
	public void softScoreAddoitionalGuest()
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnIncentiveEligible, 120);
		clickElementJS(btnIncentiveEligible);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnRequestSoftScore, 120);
		clickElementJS(btnRequestSoftScore);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, fieldScoreBand, 240);
		if(driver.findElement(fieldScoreBand).getText().length()>0)
		{
			tcConfig.updateTestReporter("AdditionalGuestsPage", "softScoreAddoitionalGuest", Status.PASS, "Additional Guest is softscored successfully");
		}
		else
		{
			tcConfig.updateTestReporter("AdditionalGuestsPage", "softScoreAddoitionalGuest", Status.FAIL, "Softscoring of additional guest failed");
		}
		
		if(verifyObjectDisplayed(btnBookTour))
		{
			tcConfig.updateTestReporter("AdditionalGuestsPage", "softScoreAddoitionalGuest", Status.PASS, "Book tour button is visible after sofscoring");
		}
		else
		{
			tcConfig.updateTestReporter("AdditionalGuestsPage", "softScoreAddoitionalGuest", Status.FAIL, "Book tour button is not visible after sofscoring");
		}
	}
	
	
	

	private By btnBookTour = By
			.xpath("//div[contains(@class,'cTMAdditionalGuestButtons')]//button[text()='Book Tour']");
	private By headerCreateTourRecord = By.xpath("//h2[text()='Create New Tour Record']");
	// private By btnSave = By.xpath("//button[text()='Save']");
	private By lnkCreatedTourId = By.xpath("//span[contains(@class,'toastMessage')]//a");

	public void bookTourForAdditionalGuest() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnBookTour, 120);
		clickElementJS(btnBookTour);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, headerCreateTourRecord, 120);
		clickElementJS(btnSave);
		waitUntilElementVisibleBy(driver, lnkCreatedTourId, 120);
		if (verifyObjectDisplayed(lnkCreatedTourId)) {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "bookTourForAdditionalGuest", Status.PASS,
					"Tour is successfully created" + ":" + driver.findElement(lnkCreatedTourId).getText());
		} else {
			tcConfig.updateTestReporter("AdditionalGuestsPage", "bookTourForAdditionalGuest", Status.FAIL,
					"Tour creation failed");
		}

	}
}
