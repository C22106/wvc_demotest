package salesforce.pages;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ProductPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public ProductPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}
	
	List<String> strQOHValue = new ArrayList<String>();
	private By headerProductPage = By.xpath("//span[text()='"+testData.get("Product")+"')]/..");
	private By lnkViewAll = By.xpath("//div[contains(@class,'region-sidebar-right')]//span[@class='view-all-label']");

	public List<String> verifyProductAndGetQOH()
	{
		try{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			scrollDownForElementJSBy(lnkViewAll);
			scrollUpByPixel(200);
			Actions a = new Actions(driver);
			a.moveToElement(driver.findElement(lnkViewAll)).click().build().perform();
			//clickElementJSWithWait(lnkViewAll);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String primarySalesStore = (testData.get("SalesStore").split("-"))[0].trim();
			String secondarySalesStore = (testData.get("NewSalesStore").split("-"))[0].trim();
			WebElement lstPrimarySalesStoreQOH = driver.findElement(By.xpath("//a[@title='"+primarySalesStore+"']/../../..//th/span/a"));
			WebElement lstSecondarySalesStoreQOH = driver.findElement(By.xpath("//a[@title='"+secondarySalesStore+"']/../../..//th/span/a"));
			if(verifyElementDisplayed(lstPrimarySalesStoreQOH))
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyProductAndGetQOH", Status.PASS, testData.get("Product")+" is present for " + primarySalesStore);
//				lstPrimarySalesStoreQOH.click();
				clickElementJS(lstPrimarySalesStoreQOH);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement  primaryQOH = driver.findElement(By.xpath("//span[contains(text(),'Quantity on Hand')]/../..//div[2]//span//lightning-formatted-number"));
				strQOHValue.add(primaryQOH.getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if(verifyElementDisplayed(lstSecondarySalesStoreQOH))
				{
					tcConfig.updateTestReporter("SalesStorePage", "verifyProductAndGetQOH", Status.PASS, testData.get("Product")+" is present for " + secondarySalesStore);
//					lstSecondarySalesStoreQOH.click();
					clickElementJS(lstSecondarySalesStoreQOH);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					WebElement  secondryQOH = driver.findElement(By.xpath("(//span[contains(text(),'Quantity on Hand')]/../..//div[2]//span//lightning-formatted-number)[2]"));
					strQOHValue.add(secondryQOH.getText());
					System.out.println("secondry before :"+secondryQOH.getText());
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
				else
				{
					tcConfig.updateTestReporter("SalesStorePage", "verifyProductAndGetQOH", Status.FAIL, testData.get("Product")+" is not present for the " + secondarySalesStore);
				}
			}
			else
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyProductAndGetQOH", Status.FAIL, testData.get("Product")+" is not present for the " + primarySalesStore);
			}
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("SalesStorePage", "verifyProductAndGetQOH", Status.FAIL, "Getting error: " + e);
			throw e;
		}
		System.out.println("QOH :"+ strQOHValue);
		return strQOHValue;
		
	}
		
	public String verifyQOHDeductedForPrimarySalesStore(int quantity, String qoh)
	{
		String qohValue = "";
		try{
			scrollDownForElementJSBy(lnkViewAll);
			scrollUpByPixel(200);
			clickElementBy(lnkViewAll);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String primarySalesStore = (testData.get("SalesStore").split("-"))[0].trim();
			WebElement lstPrimarySalesStoreQOH = driver.findElement(By.xpath("//a[@title='"+primarySalesStore+"']/../../..//th/span/a"));
			if(verifyElementDisplayed(lstPrimarySalesStoreQOH))
			{
				clickElementJS(lstPrimarySalesStoreQOH);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement  primaryQOH = driver.findElement(By.xpath("//span[contains(text(),'Quantity on Hand')]/../..//div[2]//span//lightning-formatted-number"));
				qohValue = primaryQOH.getText();
				Number n = NumberFormat.getNumberInstance(java.util.Locale.US).parse(qoh);
				System.out.println("The n is :"+n.intValue());
				Number n1 = NumberFormat.getNumberInstance(java.util.Locale.US).parse(qohValue);
				System.out.println("The N1 is :"+ n1.intValue());
				int deductedQohValue = n.intValue() - n1.intValue();
				
				if(n.intValue()==quantity)
				{
					tcConfig.updateTestReporter("SalesStorePage", "verifyQOHDeductedForPrimarySalesStore", Status.PASS, "Quantity on hand for primary salestore decreased by "+deductedQohValue);
				}
				else
				{
					tcConfig.updateTestReporter("SalesStorePage", "verifyQOHDeductedForPrimarySalesStore", Status.FAIL, "Quantity on hand for primary salestore is not decreased by "+quantity);
				}
			}
			else
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyQOHDeductedForPrimarySalesStore", Status.FAIL, testData.get("Product")+" is not present for the " + primarySalesStore);
			}
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("SalesStorePage", "verifyQOHDeductedForPrimarySalesStore", Status.FAIL, "Getting error: " + e);
		}
		return qohValue;
	}
	
	public void verifyQOHUpdate(int quantity, List<String> qoh)
	{
		try{
			scrollDownForElementJSBy(lnkViewAll);
			scrollUpByPixel(200);
			clickElementBy(lnkViewAll);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String primarySalesStore = (testData.get("SalesStore").split("-"))[0].trim();
			String secondarySalesStore = (testData.get("NewSalesStore").split("-"))[0].trim();
			WebElement lstPrimarySalesStoreQOH = driver.findElement(By.xpath("//a[@title='"+primarySalesStore+"']/../../../td[5]/span/span"));
			WebElement lstSecondarySalesStoreQOH = driver.findElement(By.xpath("//a[@title='"+secondarySalesStore+"']/../../../td[5]/span/span"));
			String newPrimaryQoh = lstPrimarySalesStoreQOH.getText();
			String newSecondaryQoh = lstSecondarySalesStoreQOH.getText();
			System.out.println("Secondry n2 :"+newSecondaryQoh +"and quantity"+quantity);
			Number n = NumberFormat.getNumberInstance(java.util.Locale.US).parse(qoh.get(0));
			Number n1 = NumberFormat.getNumberInstance(java.util.Locale.US).parse(newPrimaryQoh);
			Number n2 = NumberFormat.getNumberInstance(java.util.Locale.US).parse(newSecondaryQoh);
			Number n3 = NumberFormat.getNumberInstance(java.util.Locale.US).parse(qoh.get(1));
			if(n.intValue() == n1.intValue())
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyQOHUpdate", Status.PASS, "QOH for primary salesstore is restored to original value");
			}
			else
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyQOHUpdate", Status.FAIL, "QOH for primary salesstore is not restored to original value");
			}
			if(n2.intValue() ==n3.intValue()-1)
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyQOHUpdate", Status.PASS, "Quantity on hand for secondary salestore decreased by 1.");
			}
			else
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyQOHUpdate", Status.FAIL, "Quantity on hand for primary salestore is not decreased by 1.");
			}	
			
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("SalesStorePage", "verifyQOHDeductedForPrimarySalesStore", Status.FAIL, "Getting error: " + e);
		}
	}
	
	public void verifyZeroQOH(List<String> qohValue)
	{
		int count = 0;
		try{
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			for(String s : qohValue)
			{
				if(s.equals("0"))
				{
					tcConfig.updateTestReporter("SalesStorePage", "verifyZeroQOH", Status.PASS, "QOH for new salesstore is 0");
					break;
				}
				else
				{
					count++;
				}
			}
			if(count==qohValue.size())
			{
				tcConfig.updateTestReporter("SalesStorePage", "verifyZeroQOH", Status.FAIL, "QOH is not zero for new salesstore");
			}
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("SalesStorePage", "verifyZeroQOH", Status.FAIL, "Getting error: " + e);
			throw e;
		}
	}
	
	private By lnkParentSalesStore = By.xpath("//span[text()='Parent Territory']/../../div[2]//a");
	private By lnkRelated = By.xpath("//div[contains(@class,'windowViewMode-normal')]//a[@title='Related']");
	private By lnkViewAllSalesStore = By.xpath("//a[contains(@href,'Sales_Store')]//span[@class='view-all-label']");
	
	public List<String> getMysteryGiftQoh()
	{
		waitUntilElementVisibleBy(driver, lnkParentSalesStore, 120);
		clickElementBy(lnkParentSalesStore);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver,lnkRelated, 120);
		clickElementBy(lnkRelated);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, lnkViewAllSalesStore, 120);
		clickElementBy(lnkViewAllSalesStore);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String[] productNames = testData.get("Product").split("/");
		List<String> qohValues = new ArrayList<String>();
		for(int i =0; i<productNames.length; i++)
		{
			qohValues.add(driver.findElement(By.xpath("//a[@title='"+productNames[i]+"']/../../../td[3]/span/span")).getText());
		}
		return qohValues;
	}
	
	public void verifyQOHForMysteryGift(List<String> qoh) throws ParseException
	{
		waitUntilElementVisibleBy(driver, lnkParentSalesStore, 120);
		clickElementBy(lnkParentSalesStore);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver,lnkRelated, 120);
		clickElementBy(lnkRelated);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, lnkViewAll, 120);
		clickElementBy(lnkViewAll);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<String> qohValues = new ArrayList<String>();
		String[] productNames = testData.get("Product").split("/");
		for(int i =0; i<productNames.length; i++)
		{
			qohValues.add(driver.findElement(By.xpath("//a[@title='"+productNames[i]+"']/../../../td[3]/span/span")).getText());
		}
		
		int count = 0;
		
		for(int i =0; i<qoh.size(); i++)
		{
			if(NumberFormat.getNumberInstance(java.util.Locale.US).parse(qoh.get(0)).intValue() - 1 == 
					NumberFormat.getNumberInstance(java.util.Locale.US).parse(qohValues.get(0)).intValue())
			{
				count++;
			}
		}
		
		if(count == qoh.size())
		{
			tcConfig.updateTestReporter("SalesStorePage", "verifyQOHForMysteryGift", Status.PASS, "Qoh values of all product are decreased after distributing");
		}
		else
		{
			tcConfig.updateTestReporter("SalesStorePage", "verifyQOHForMysteryGift", Status.FAIL, "Qoh values of all product are not decreasing after distributing");
		}
		
	}
}
