package salesforce.pages;

import java.awt.AWTException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CasesPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(CasesPage.class);

	public CasesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String txtSubject;
	public static String strCaseNumber;
	@FindBy(xpath = "//button[@class='slds-button']")
	WebElement menuIcon;
	@FindBy(xpath = "//input[@class='slds-input input']")
	WebElement txtSearchBy;
	private By caseSearch = By.xpath("//input[@name='Case-search-input']");
	private By caseNum = By.xpath("//a[@data-refid='recordId']");
	private By caseOwner = By.xpath("//div[@class='ownerName']");
	private By status = By
			.xpath("(//span[@class='test-id__field-label' and contains(text(),'Status')]/../..//span)[2]");
	private By AssignedDep = By.xpath("//span[@title='Assigned Department']//following-sibling::div/div/span");
	private By AssignedDepartment = By.xpath("(//span[@title='Assigned Department']/..//span)[2]");
	private By listView = By.xpath("//a[@title='Select List View']");
	private By listViewSearch = By.xpath("(//input[@role='combobox'])[2]");
	private By listViewSearch2 = By.xpath("(//input[@role='combobox'])[4]");
	private By listViewSearch3 = By.xpath("(//input[@role='combobox'])[3]");
	// private By caseStatus = By.xpath("(//span[@class='test-id__field-label'
	// and contains(text(),'Status')]/../..//span)[2]");
	private By caseStatus = By.xpath("(//span[text()='Status']/../..//span/span)[2]");
	private By editStatus = By.xpath("//button[@title='Edit Status']");
	private By editdrop = By.xpath("//a[contains(@title,'more actions') and starts-with(@title,'Show')]");
	private By editStatusDD = By.xpath("//span[text()='Status']/../..//a[@class='select']");
	private By caseDescriptionDD = By.xpath("//span[text()='Case Disposition']/../..//a[@class='select']");
	private By caseSaveBtn = By.xpath("//button[@title='Save']");
	private By caseStatusMarketer = By.xpath("//span[@title='Status']/..//div/span");
	private By crmCaseOwner = By.xpath("//span[@title='CRM Team Case Queue']");
	private By caseid = By.xpath("//a[starts-with(@title,'000')]");
	// private By
	// statusFieldValue=By.xpath("//button[@title='caseSaveBtn']/../span/span");
	private By statusFieldValue = By.xpath(
			"(//span[text()='Case Working Information'])[1]/../../..//span[text() = 'Status']/../..//lightning-formatted-text");

	private By caseWorkingTxt = By.xpath("//span[text()='Case Working Information']");

	private By lnkStatus = By.xpath("//span[text()='Status']/../..//a");
	private By statusname = By.xpath("//label[text()='Status']/../div/lightning-base-combobox/div/div/input");

	public void validateCaseAssignment() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, caseSearch, 120);
		if (verifyObjectDisplayed(caseSearch)) {
			String CaseSub = HelpPopup.HelpSubject;
			fieldDataEnter(caseSearch, CaseSub);
			driver.findElement(caseSearch).sendKeys(Keys.ENTER);
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Case located");
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case not located");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> CaseNum = driver.findElements(caseNum);
		if (CaseNum.size() > 0) {
			CaseNum.get(CaseNum.size() - 1).click();
		}
//		clickElementBy(caseNum);
		waitUntilElementVisibleBy(driver, caseOwner, 120);
		if (verifyObjectDisplayed(caseOwner)) {
			String CaseOwner = driver.findElement(caseOwner).getText().trim();
			Assert.assertEquals(CaseOwner, testData.get("CaseOwner"));
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Case owner is: " + CaseOwner);
		} else {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case owner is not correct ");
		}
		waitUntilElementVisibleBy(driver, status, 120);
		if (verifyObjectDisplayed(status)) {
			String Casestatus = driver.findElement(status).getText().trim();
			Assert.assertEquals(Casestatus, testData.get("CaseStatus1"));
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Case owner is: " + Casestatus);
		} else {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case status is not correct ");
		}
		waitUntilElementVisibleBy(driver, AssignedDep, 120);
		if (verifyObjectDisplayed(AssignedDep)) {
			String AssignedDepartment = driver.findElement(AssignedDep).getText().trim();
			Assert.assertEquals(AssignedDepartment, testData.get("AssignedDepartment"));
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS,
					"Case owner is: " + AssignedDepartment);
		} else {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL,
					" AssignedDepartment is not correct ");
		}

	}

	public void validateCaseAssignmentToCRMQ() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, caseSearch, 120);
		if (verifyObjectDisplayed(caseSearch)) {
			String CaseSub = HelpPopup.HelpSubject;
			fieldDataEnter(caseSearch, CaseSub);
			driver.findElement(caseSearch).sendKeys(Keys.ENTER);
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Case located");
		} else {

			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case not located");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(crmCaseOwner)) {
			String CaseOwner = driver.findElement(crmCaseOwner).getText().trim();
			Assert.assertEquals(CaseOwner, testData.get("CaseOwner"));
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Case owner is: " + CaseOwner);
		} else {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case owner is not correct ");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(caseid);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, caseStatus, 120);
		if (verifyObjectDisplayed(caseStatus)) {
			String Casestatus = driver.findElement(caseStatus).getText().trim();
			Assert.assertEquals(Casestatus, testData.get("CaseStatus1"));
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS, "Case status is: " + Casestatus);
		} else {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL, "Case status is not correct ");
		}

		waitUntilElementVisibleBy(driver, AssignedDepartment, 120);
		if (verifyObjectDisplayed(AssignedDepartment)) {
			String AssignedDep = driver.findElement(AssignedDepartment).getText().trim();
			Assert.assertEquals(AssignedDep, testData.get("AssignedDepartment"));
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.PASS,
					"Assigned department is: " + AssignedDep);
		} else {
			tcConfig.updateTestReporter("helpIcon", "validateHelpIcon", Status.FAIL,
					" AssignedDepartment is not correct ");
		}

	}

	public void caseListView(String iterator) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilElementVisibleBy(driver, listView, 120);
		clickElementBy(listView);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		/*
		 * if (iterator.equalsIgnoreCase("1")){ clickElementBy(listViewSearch);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clearElementBy(listViewSearch);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * fieldDataEnter(listViewSearch, testData.get("CaseType" + iterator)); }else if
		 * (iterator.equalsIgnoreCase("2")){ clickElementBy(listViewSearch2);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clearElementBy(listViewSearch2);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * fieldDataEnter(listViewSearch2, testData.get("CaseType" + iterator)); }
		 */

		if (verifyObjectDisplayed(listViewSearch)) {
			clickElementBy(listViewSearch);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearElementBy(listViewSearch);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(listViewSearch, testData.get("CaseType" + iterator));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(By.xpath("//mark[text()='" + testData.get("CaseType" + iterator) + "']"));

		} else if (verifyObjectDisplayed(listViewSearch2)) {
			clickElementBy(listViewSearch2);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearElementBy(listViewSearch2);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(listViewSearch2, testData.get("CaseType" + iterator));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(By.xpath("//mark[text()='" + testData.get("CaseType" + iterator) + "']"));

		} else if (verifyObjectDisplayed(listViewSearch3)) {
			clickElementBy(listViewSearch3);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearElementBy(listViewSearch3);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(listViewSearch3, testData.get("CaseType" + iterator));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(By.xpath("//mark[text()='" + testData.get("CaseType" + iterator) + "']"));
		} else {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement option = driver
					.findElement(By.xpath("//span[contains(text(),'" + testData.get("CaseType" + iterator) + "')]"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			option.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, caseSearch, 120);
		if (verifyObjectDisplayed(caseSearch)) {
			String createdCase = ReservationPage.createdCase;
			// String createdCase = "00001402";
			fieldDataEnter(caseSearch, createdCase);
			driver.findElement(caseSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.findElement(By.xpath("//table//a[text()='" + createdCase +
			// "']")).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("caseListView", "case page", Status.PASS, "Case located");
		} else {

			tcConfig.updateTestReporter("caseListView", "case page", Status.FAIL, "Case not located");
		}
	}

	private By editbtn = By.xpath("//a[@title='Edit']");
	private By changetype = By.xpath("//span[@title='Change Request Type']/following-sibling::div//span");
	private By stat = By.xpath("//span[@title='Status']/following-sibling::div//span");
	private By reserveNum = By.xpath("//span[@title='Reservation']/following-sibling::div//a");
	private By AU = By.xpath("//span[@title='Current A/U Value']/following-sibling::div//span");
	private By disposition = By.xpath("//span[@title='Case Disposition']/following-sibling::div//span");

	public void validatecase() {
		String createdCase = ReservationPage.createdCase;
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//table//a[text()='" + createdCase + "']")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String requesttypr = driver.findElement(changetype).getText();
		String status = driver.findElement(stat).getText();
		String ResNumber = driver.findElement(reserveNum).getText();
		String currentAU = driver.findElement(AU).getText();
		String disp = driver.findElement(disposition).getText();

		tcConfig.updateTestReporter("changeCaseStatus", "case page", Status.PASS,
				" Below Details Are visible on page :-\n" + " Request Type   :  " + requesttypr + "\n"
						+ " Status    :   " + status + "\n" + " Reservation Number   :  " + ResNumber + "\n"
						+ " Current A/U    :  " + currentAU + "\n" + " Disposition   :  " + disp + "\n");

		System.out.println(" Below Details Are visible on page :-\n" + " Request Type   :  " + requesttypr + "\n"
				+ " Status    :   " + status + "\n" + " Reservation Number   :  " + ResNumber + "\n"
				+ " Current A/U    :  " + currentAU + "\n" + " Disposition   :  " + disp + "\n");

	}

	public void changeCaseStatus(String iterator) {
		String createdCase = ReservationPage.createdCase;
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//table//a[text()='" + createdCase + "']")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
//		waitUntilElementVisibleBy(driver, editStatus, 120);
		waitUntilElementVisibleBy(driver, editdrop, 120);
//		if (verifyObjectDisplayed(editStatus))
		if (verifyObjectDisplayed(editdrop)) {
//			clickElementBy(editStatus);
			clickElementBy(editdrop);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
//			clickElementBy(editbtn);
			List<WebElement> edit = driver.findElements(editbtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", edit.get(edit.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("javascript:window.scrollBy(250,350)");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(editStatusDD);
			driver.findElement(By
					.xpath("//div[@class='select-options']//a[@title='" + testData.get("CaseStatus" + iterator) + "']"))
					.click();
			tcConfig.updateTestReporter("changeCaseStatus", "case page", Status.PASS, "case status selected ");
		} else {
			tcConfig.updateTestReporter("changeCaseStatus", "case page", Status.FAIL, "Unable to select case status ");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (driver.findElement(caseDescriptionDD).isEnabled()) {
			clickElementBy(caseDescriptionDD);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(
					By.xpath("//div[@class='select-options']//a[@title='" + testData.get("CaseDescription") + "']"))
					.click();
			clickElementBy(caseSaveBtn);
			tcConfig.updateTestReporter("changeCaseStatus", "case page", Status.PASS, "case status Changed ");
		} else {
			tcConfig.updateTestReporter("changeCaseStatus", "case page", Status.FAIL, "Unable to change case status ");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void updateCaseStatusToSubmit(String iterator) {
		String createdCase = ReservationPage.createdCase;
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//table//a[text()='" + createdCase + "']")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, editdrop, 120);
		if (verifyObjectDisplayed(editdrop)) {
			clickElementBy(editdrop);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> edit = driver.findElements(editbtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", edit.get(edit.size() - 1));

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("javascript:window.scrollBy(250,350)");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(editStatusDD);
			driver.findElement(By
					.xpath("//div[@class='select-options']//a[@title='" + testData.get("CaseStatus" + iterator) + "']"))
					.click();
			tcConfig.updateTestReporter("changeCaseStatus", "case page", Status.PASS, "case status selected ");
		} else {
			tcConfig.updateTestReporter("changeCaseStatus", "case page", Status.FAIL, "Unable to select case status ");
		}

		clickElementBy(caseSaveBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void validateCaseStatus(String iterator) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(
				By.xpath("//table/tbody/tr/td/span/span[text()='" + testData.get("CaseStatus" + iterator) + "']"))) {
			tcConfig.updateTestReporter("caseListView", "case page", Status.PASS,
					"Case is: " + testData.get("CaseStatus" + iterator));
		} else {
			tcConfig.updateTestReporter("caseListView", "case page", Status.FAIL, "Case status is not correct ");
		}

	}

	public void validateClosedCaseNegative(String iterator) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(
				By.xpath("//table/tbody/tr/td/span/span[text()='" + testData.get("CaseStatus" + iterator) + "']"))) {
			tcConfig.updateTestReporter("caseListView", "case page", Status.PASS,
					"Closed Case is displaying: " + testData.get("CaseStatus" + iterator));
		} else {
			tcConfig.updateTestReporter("caseListView", "case page", Status.FAIL, "Closed case is not displaying");
		}

	}

	public void validateCaseStatusMarketer() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(caseStatusMarketer)) {
			String status = driver.findElement(caseStatusMarketer).getText();
			Assert.assertEquals("New", status);
		} else {
			tcConfig.updateTestReporter("caseListView", "case page", Status.FAIL, "Case status is not correct ");
		}

	}

	private By lnkCasesListView = By.xpath("//a[@title='Select List View']/../span[2]");
	private By lnkMyOpenCases = By.xpath("(//span[text()='My Open Cases']/..)[1]");
	private By txtBoxSearchCases = By.xpath("//input[contains(@placeholder,'Search this list')]");

	public void navigateToMyOpenCase() {
		waitUntilElementVisibleBy(driver, lnkCasesListView, 120);
		if (!(driver.findElement(lnkCasesListView).getText().equals("My Open Cases"))) {
			clickElementJSWithWait(lnkCasesListView);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(lnkMyOpenCases);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	// Added Ajib Parida
	private By openCaselink = By.xpath("(//span[text()='My Open Cases'])[1]");

	public void navigateMyOpenCase() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, openCaselink, 120);
		if (verifyObjectDisplayed(openCaselink)) {
			clickElementJSWithWait(openCaselink);
			tcConfig.updateTestReporter("clickOpencaselink", "case page", Status.FAIL, "my open cases link is click");

		} else {
			tcConfig.updateTestReporter("clickOpencaselink", "case page", Status.FAIL,
					"my open cases link is not click");

		}

		waitUntilElementVisibleBy(driver, lnkCasesListView, 120);
		if (!(driver.findElement(lnkCasesListView).getText().equals("My Open Cases"))) {
			clickElementJSWithWait(lnkCasesListView);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(lnkMyOpenCases);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	public void searchCreatedCase() {
		waitUntilElementVisibleBy(driver, txtBoxSearchCases, 120);
		fieldDataEnter(txtBoxSearchCases, ReservationPage.createdCase);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(txtBoxSearchCases).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement e = driver.findElement(By.xpath("//a[@title='" + ReservationPage.createdCase + "']"));
		clickElementJSWithWait(e);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	// Added By Ajib pAridA
	public void updateStatusandSave() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(caseWorkingTxt);
		scrollUpByPixel(150);
		waitUntilElementVisibleBy(driver, editStatus, 120);
		clickElementJSWithWait(editStatus);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(lnkStatus);
		scrollUpByPixel(250);
		// clickElementJSWithWait(lnkStatus);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, statusname, 120);

		clickElementJSWithWait(statusname);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement casestatus = driver.findElement(By.xpath("//span[@title='" + testData.get("CaseStatus1") + "']"));
		clickElementJSWithWait(casestatus);
		System.out.println(casestatus.getText());
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, caseSaveBtn, 120);
		clickElementJSWithWait(caseSaveBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollDownForElementJSBy(caseWorkingTxt);
		scrollUpByPixel(200);
		WebElement updateStatusvalue = driver.findElement(statusFieldValue);

		if (verifyElementDisplayed(updateStatusvalue)) {

			tcConfig.updateTestReporter("CasePage", "updateStatusandSave", Status.PASS,
					"Successfully changed the status to:" + updateStatusvalue.getText());
		} else {
			tcConfig.updateTestReporter("CasePage", "updateStatusandSave", Status.FAIL,
					"Not able to changed the status");
		}

	}

	public void updateStatus_Case_ON_Hold() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(caseWorkingTxt);
		scrollUpByPixel(150);
		waitUntilElementVisibleBy(driver, editStatus, 120);
		clickElementJSWithWait(editStatus);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(lnkStatus);
		scrollUpByPixel(250);
		// clickElementJSWithWait(lnkStatus);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, statusname, 120);

		clickElementJSWithWait(statusname);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement casestatus = driver.findElement(By.xpath("//span[@title='" + testData.get("CaseStatus1") + "']"));
		clickElementJSWithWait(casestatus);
		System.out.println(casestatus.getText());
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, caseSaveBtn, 120);
		clickElementJSWithWait(caseSaveBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollDownForElementJSBy(caseWorkingTxt);
		scrollUpByPixel(200);
		WebElement updateStatusvalue = driver.findElement(statusFieldValue);
		updateStatusvalue.getText();
		System.out.println(updateStatusvalue);

		if (verifyElementDisplayed(updateStatusvalue)) {

			tcConfig.updateTestReporter("CasePage", "updateStatus Case on Hold", Status.PASS,
					"Successfully changed the status to Hold On:" + updateStatusvalue.getText());
		} else {
			tcConfig.updateTestReporter("CasePage", "updateStatusandSave", Status.FAIL,
					"Not able to changed the status to Hold On:");
		}

	}

	// Added By Ajib Parida
	public void validateWorkingCasesNotAppearinMycases() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(caseWorkingTxt);
		scrollUpByPixel(150);
		waitUntilElementVisibleBy(driver, editStatus, 120);
		clickElementJSWithWait(editStatus);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(lnkStatus);
		scrollUpByPixel(250);
		// clickElementJSWithWait(lnkStatus);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, statusname, 120);

		clickElementJSWithWait(statusname);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement casestatus = driver.findElement(By.xpath("//span[@title='" + testData.get("CaseStatus1") + "']"));
		clickElementJSWithWait(casestatus);
		System.out.println(casestatus.getText());
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, caseSaveBtn, 120);
		clickElementJSWithWait(caseSaveBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		scrollDownForElementJSBy(caseWorkingTxt);
		scrollUpByPixel(200);
		WebElement updateStatusvalue = driver.findElement(statusFieldValue);

		if (verifyElementDisplayed(updateStatusvalue)) {

			tcConfig.updateTestReporter("CasePage", "updateStatusCase not working", Status.PASS,
					"Successfully changed the status to:" + updateStatusvalue.getText());
		} else {
			tcConfig.updateTestReporter("CasePage", "updateStatusCase not working", Status.FAIL,
					"Not able to changed the status to:" + updateStatusvalue.getText());
		}

	}

	private By caseLink_MobileUI = By.xpath("//span[text()='Cases']");
	protected By homelink = By.xpath("(//a/img[@class='wn-left_custom_icon'])[1]");

	/// Added By Ajib
	public void validateCaseStatus_MobileUI() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(caseLink_MobileUI);
		scrollUpByPixel(150);
		clickElementJS(caseLink_MobileUI);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("//a[@title='" + ArrivalPage.txtCaseNumber + "']")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (driver.findElement(statusFieldValue).getText().equalsIgnoreCase("New")) {
			System.out.println(driver.findElement(statusFieldValue).getText());
			tcConfig.updateTestReporter("CasePage", "navigateToCasePage_MobileUI", Status.PASS, "Status is New");
		} else {
			tcConfig.updateTestReporter("CasePage", "navigateToCasePage_MobileUI", Status.FAIL, "Status is not New");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(homelink)) {
			waitUntilElementVisibleBy(driver, homelink, 120);
			WebElement Homelink = driver.findElement(homelink);
			clickElementJSWithWait(Homelink);
			tcConfig.updateTestReporter("CasePage", "clickhomeLink", Status.PASS, "clickhome Link");
		} else {
			tcConfig.updateTestReporter("CasePage", "clickhomeLink", Status.FAIL, "clickhome Link");
		}

	}

	private By txtErrorMsg = By.xpath("//ul[@class='errorsList']/li");
	private By headerEditCase = By.xpath("//h2[contains(@class,'inlineTitle')]");
	// private By moreOptionsdrpDwn =
	// By.xpath("//div[contains(@class,'windowViewMode-normal')]//li[@class='oneActionsDropDown']//a");
	private By moreOptionsdrpDwn = By.xpath("//span[text() = 'Show more actions']/..");
	private By lnkEditCase = By.xpath("//a[@name='Edit']");
	private By lnkStatusCase = By.xpath("//span[text()='Status']/../..//div[@class='uiPopupTrigger']//a");
	private By btnSaveCase = By.xpath("//button[@title='Save']");

	public void verifyCaseClosedError() {
		waitUntilElementVisibleBy(driver, moreOptionsdrpDwn, 120);
		clickElementBy(moreOptionsdrpDwn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(driver.findElements(lnkEditCase).get(driver.findElements(lnkEditCase).size() - 1));
		waitUntilElementVisibleBy(driver, headerEditCase, 120);
		scrollDownForElementJSBy(lnkStatusCase);
		scrollUpByPixel(50);
		clickElementBy(lnkStatusCase);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> e = driver.findElements(By.xpath("//a[@title='" + testData.get("CaseStatus1") + "']"));
		try {
			clickElementWb(e.get(e.size() - 1));
		} catch (Exception ex) {
			tcConfig.updateTestReporter("CasePage", "verifyCaseClosedError", Status.FAIL, "Getting Error: " + ex);
			throw ex;
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(btnSaveCase);
		waitUntilElementVisibleBy(driver, txtErrorMsg, 120);
		if (verifyObjectDisplayed(txtErrorMsg) && driver.findElement(txtErrorMsg).getText().contains("Disposition")) {
			tcConfig.updateTestReporter("CasePage", "verifyCaseClosedError", Status.PASS,
					"Not able to close case without selecting the disposition value");
		} else {
			tcConfig.updateTestReporter("CasePage", "verifyCaseClosedError", Status.FAIL,
					"Not getting error while closing the case without selecting disposition");
		}
	}

	public void searchCase() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, txtBoxSearchCases, 120);
		if (verifyObjectDisplayed(txtBoxSearchCases)) {
			tcConfig.updateTestReporter("CasePage", "searchForcases", Status.PASS,
					"User is navigated to cases search Page");
			if (testData.get("TourNo") == null || testData.get("TourNo").isEmpty()
					|| testData.get("TourNo").contentEquals("")) {
				fieldDataEnter(txtBoxSearchCases, ReservationPage.createdCase);
			} else {
				fieldDataEnter(txtBoxSearchCases, testData.get("TourNo"));
			}
			driver.findElement(txtBoxSearchCases).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement seacrhResOwnerLink = driver.findElement(By.xpath("//th/span/a[contains(text(),'"
					+ testData.get("TourNo") + "') or contains(text(),'" + ReservationPage.createdCase + "')]"));
			seacrhResOwnerLink.click();
		}

	}

	private By chngOwner = By.xpath("//span[contains(text(),'Change Owner')]/parent::button/lightning-primitive-icon");
	private By searchOwn = By.xpath("//input[@title='Search People']");
	private By ownValue = By
			.xpath("//ul[@class='lookup__list  visible']/li[1]/a/div[2]/div[@title='WVO Corporate Super User']");
	private By chngOwnerButton = By.xpath("//button[text()='Change Owner']");
	private By chngedState = By
			.xpath("(//span[text()='Case Owner'])[1]/parent::div/following-sibling::div/span/div/div/div/a");
	private By MyOpencases = By.xpath("//a[@title='Select List View']/../span[2]");
	private By MyOpencases1 = By.xpath("(//span[text()='My Open Cases'])[1]");
	private By caseDropDown = By.xpath("//a[@title='Select List View']/lightning-icon/lightning-primitive-icon");
	private By statusEdit = By
			.xpath("(//span[text()='Status'])[2]/parent::div/following-sibling::div/button/lightning-primitive-icon");
	private By statusDropDown = By
			.xpath("(//span[text()='Status'])[3]/parent::span/following-sibling::div/div/div[1]/div/a");
	private By MyCases = By.xpath("//span[text()='My Cases']");
	private By Working = By.xpath("//a[text()='Working']");
	private By saveCase = By.xpath("//span[text()='Save']");
	private By noItemsDisplay = By.xpath("//p[text()='No items to display.']");

	public void changeCashOwner() throws AWTException {
		waitUntilElementVisibleBy(driver, chngOwner, 120);
		clickElementJSWithWait(chngOwner);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, searchOwn, 120);
		clickElementJSWithWait(searchOwn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(searchOwn, "WVO Corporate Super User");
		waitUntilElementVisibleBy(driver, ownValue, 120);
		clickElementJSWithWait(ownValue);
		waitUntilElementVisibleBy(driver, chngOwnerButton, 120);
		clickElementJSWithWait(chngOwnerButton);

		/*
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * waitUntilElementVisibleBy(driver, chngedState, 120);
		 * if(driver.findElement(chngedState).getText().
		 * equals("WVO Corporate Super User"))
		 * 
		 * { tcConfig.updateTestReporter("TourRecordsPage", "change Owner of case",
		 * Status.PASS, "owner change"); } else
		 * if(driver.findElement(chngedState).getText().equals("Tamara Wertheim")){
		 * 
		 * tcConfig.updateTestReporter("TourRecordsPage", "change Owner of case",
		 * Status.PASS, "owner changed"); }else{
		 * tcConfig.updateTestReporter("TourRecordsPage", "change Owner of case",
		 * Status.FAIL, "owner not changed"); } waitUntilElementVisibleBy(driver,
		 * MyOpencases1, 120); clickElementJSWithWait(MyOpencases1);
		 * waitUntilElementVisibleBy(driver, MyOpencases, 120);
		 * clickElementJSWithWait(MyOpencases);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * waitUntilElementVisibleBy(driver, caseDropDown, 120);
		 * clickElementJSWithWait(caseDropDown); waitUntilElementVisibleBy(driver,
		 * MyCases, 120); clickElementJSWithWait(MyCases);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, txtBoxSearchCases, 120); if
		 * (verifyObjectDisplayed(txtBoxSearchCases)){
		 * tcConfig.updateTestReporter("CasePage", "searchForcases", Status.PASS,
		 * "User is navigated to cases search Page"); if
		 * (testData.get("TourNo")==null||testData.get("TourNo").isEmpty()||testData.get
		 * ("TourNo").contentEquals("")){
		 * fieldDataEnter(txtBoxSearchCases,ReservationPage.createdCase); }else{
		 * fieldDataEnter(txtBoxSearchCases, testData.get("TourNo")); }
		 * driver.findElement(txtBoxSearchCases).sendKeys(Keys.ENTER);
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); WebElement
		 * seacrhResOwnerLink=driver.findElement(By.xpath(
		 * "//th/span/a[contains(text(),'"+testData.get("TourNo")
		 * +"') or contains(text(),'"+ReservationPage.createdCase+"')]"));
		 * seacrhResOwnerLink.click();
		 * driver.findElement(txtBoxSearchCases).sendKeys(Keys.ENTER);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * 
		 * waitUntilElementVisibleWb(driver, seacrhResOwnerLink, 120);
		 * if(driver.findElement(By.xpath("//th/span/a[contains(text(),'"+testData.get(
		 * "TourNo")+"')]")).isDisplayed()) { tcConfig.updateTestReporter("CasePage",
		 * "searchForcases", Status.PASS,
		 * "case is displayed before changing to status"); } else{
		 * tcConfig.updateTestReporter("CasePage", "searchForcases", Status.FAIL,
		 * "case is not  displayed before changing to status"); }
		 * 
		 * seacrhResOwnerLink.click();
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * 
		 * Robot r1=new Robot(); r1.mouseWheel(3);
		 * 
		 * waitUntilElementVisibleBy(driver, statusEdit, 120);
		 * clickElementJSWithWait(statusEdit);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, statusDropDown, 120);
		 * clickElementJSWithWait(statusDropDown); waitUntilElementVisibleBy(driver,
		 * Working, 120); clickElementJSWithWait(Working);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, saveCase, 120); clickElementBy(saveCase);
		 * Robot r2=new Robot();
		 * 
		 * r2.mouseWheel(-2); waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * 
		 * waitUntilElementVisibleBy(driver, MyOpencases, 120);
		 * clickElementJSWithWait(MyOpencases);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * waitUntilElementVisibleBy(driver, caseDropDown, 120);
		 * clickElementJSWithWait(caseDropDown); waitUntilElementVisibleBy(driver,
		 * MyCases, 120); clickElementJSWithWait(MyCases);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, txtBoxSearchCases, 120); if
		 * (verifyObjectDisplayed(txtBoxSearchCases)){
		 * tcConfig.updateTestReporter("CasePage", "searchForcases", Status.PASS,
		 * "User is navigated to cases search Page"); if
		 * (testData.get("TourNo")==null||testData.get("TourNo").isEmpty()||testData.get
		 * ("TourNo").contentEquals("")){
		 * fieldDataEnter(txtBoxSearchCases,ReservationPage.createdCase); }else{
		 * fieldDataEnter(txtBoxSearchCases, testData.get("TourNo")); }
		 * driver.findElement(txtBoxSearchCases).sendKeys(Keys.ENTER);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * if(driver.findElement(noItemsDisplay).getText().equals("No items to display."
		 * )) { tcConfig.updateTestReporter("CasePage", "searchForcases", Status.PASS,
		 * "Working cases not displayed in My Cases"+"::"+driver.findElement(
		 * noItemsDisplay).getText()); } else{ tcConfig.updateTestReporter("CasePage",
		 * "searchForcases", Status.FAIL, "Working cases  displayed in My Cases"); }
		 * 
		 * } else{ tcConfig.updateTestReporter("CasePage", "searchForcases",
		 * Status.FAIL, "Working cases  displayed in My Cases"); }
		 */
	}

	// private By lnkPlusIconMobUi = By.xpath("//div[@class='help_icon_section']");
	// private By
	// lnkPlusIconMobUi=By.xpath("//div[@class='help_icon_section']/button/span[text()='Help']");
	private By lnkPlusIconMobUi = By.xpath("//button[@title='Help']");
	// private By lnkPlusIconMobUi = By.xpath("//span[text()='Help']");
	private By lnkJourneyHelpMobUi = By.xpath("//button[text()='Journey help']");
	private By lnkTypeMobUi = By.xpath("//div[@class='helpTextHide']//input");
	private By lnkTopicMobUi = By.xpath("//label[text()='Topic']/..//input");
	private By txtBoxSubjectMobUi = By.xpath("//input[@name='Subject']");
	private By lnkTopicDetailMobUi = By.xpath("//label[text()='Topic Detail']/..//input");
	private By drpDownOptionsMobUi = By.xpath("//span[@class='slds-media__body']/..");
	private By txtBoxDetails = By.xpath("//div[contains(@class,'slds-rich-text-area')]");
	private By btnSaveCaseMobUi = By.xpath("//button[text()='Save']");
	private By lnkSaveCase = By.xpath("//div[contains(@class,'footerAction')]//span[text()='Save']/..");

	public void verifyMandatoryFieldError() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lnkPlusIconMobUi, 120);
		clickElementBy(lnkPlusIconMobUi);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, lnkJourneyHelpMobUi, 120);
		clickElementBy(lnkJourneyHelpMobUi);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(lnkTypeMobUi);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> option = driver
				.findElements(By.xpath("//span[@title='" + testData.get("CaseType1") + "']/../.."));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (option.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(option.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkTopicMobUi, 120);
			clickElementBy(lnkTopicMobUi);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//label[text()='Topic']/../div//div[2]//span[2]"), 120);
			clickElementWb(driver.findElements(By.xpath("//label[text()='Topic']/../div//div[2]//span[2]")).get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtBoxSubjectMobUi, 120);
			txtSubject = generateRandomString(5, Format.CHARACTER_STRING.getFormat());
			fieldDataEnter(txtBoxSubjectMobUi, txtSubject);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkTopicDetailMobUi, 120);
			clickElementBy(lnkTopicDetailMobUi);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//input[contains(@name,'Topic_Detail')]/../../div[2]//span[2]"),
					120);
			clickElementWb(driver
					.findElements(By.xpath("//input[contains(@name,'Topic_Detail')]/../../div[2]//span[2]")).get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnSaveCaseMobUi, 120);
			clickElementJSWithWait(btnSaveCaseMobUi);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtErrorMsgCase, 120);
			if (verifyObjectDisplayed(txtErrorMsgCase)) {
				tcConfig.updateTestReporter("CasePage", "VerifyCaseCreationError", Status.PASS,
						"Getting error: " + driver.findElement(txtErrorMsgCase).getText());
			} else {
				tcConfig.updateTestReporter("CasePage", "VerifyCaseCreationError", Status.FAIL, "Not Getting error ");
			}

			clickElementJSWithWait(lnkCloseHelpIcon);
		}
	}

	private By txtErrorMsgCase = By.xpath("//div[contains(@class,'forceToastMessage')]");
	private By lnkCloseHelpIcon = By.xpath("//button[@title='Close']");
	private By txtSuccessMsg = By.xpath("//span[contains(@class,'toastMessage')]");

	public void createNewCaseHelpIconMobUi() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, lnkPlusIconMobUi, 120);
		clickElementJSWithWait(lnkPlusIconMobUi);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lnkJourneyHelpMobUi, 120);
		clickElementBy(lnkJourneyHelpMobUi);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(lnkTypeMobUi);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> option = driver
				.findElements(By.xpath("//span[@title='" + testData.get("CaseType1") + "']/../.."));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (option.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementWb(option.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lnkTopicMobUi, 120);
			clickElementBy(lnkTopicMobUi);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//label[text()='Topic']/../div//div[2]//span[2]"), 120);
			clickElementWb(driver.findElements(By.xpath("//label[text()='Topic']/../div//div[2]//span[2]")).get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtBoxSubjectMobUi, 120);
			txtSubject = generateRandomString(5, Format.CHARACTER_STRING.getFormat());
			System.out.println(txtSubject);
			fieldDataEnter(txtBoxSubjectMobUi, txtSubject);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkTopicDetailMobUi, 120);
			clickElementBy(lnkTopicDetailMobUi);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//input[contains(@name,'Topic_Detail')]/../../div[2]//span[2]"),
					120);
			clickElementWb(driver
					.findElements(By.xpath("//input[contains(@name,'Topic_Detail')]/../../div[2]//span[2]")).get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtBoxDetails, 120);
			fieldDataEnter(txtBoxDetails, generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnSaveCaseMobUi, 120);
			clickElementJSWithWait(btnSaveCaseMobUi);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
			if (verifyObjectDisplayed(txtSuccessMsg)) {
				tcConfig.updateTestReporter("HomePage", "createNewCaseHelpIconMobUi", Status.PASS,
						"Case is created successfully");
			} else {
				tcConfig.updateTestReporter("HomePage", "createNewCaseHelpIconMobUi", Status.FAIL,
						"Case creation failed");
			}
		} else {
			tcConfig.updateTestReporter("caseListView", "createNewCaseHelpIconMobUi", Status.FAIL,
					"Type options are not present");
		}
	}

	private By lnkPlusIcon = By.xpath("//a[contains(@class,'globalCreateTrigger')]");
	private By lnkJourneyHelp = By.xpath("//a[@title='Journey Help']");
	private By lnkType = By.xpath("//span[text()='Type']/../..//a");
	private By drpDwnoption = By.xpath("//a[@role='menuitemradio']");
	private By lnkTopic = By.xpath("//span[text()='Topic']/../..//a");
	private By lnkTopicDetail = By.xpath("//span[text()='Topic Detail']/../..//a");
	private By txtBoxSubject = By.xpath("//span[text()='Subject']/../..//input");
	private By txtAreaDetails = By.xpath("//div[contains(@class,'textarea')]/div[1]");

	public void createNewCaseHelpIcon() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lnkPlusIcon, 120);
		clickElementBy(lnkPlusIcon);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, lnkJourneyHelp, 120);
		clickElementBy(lnkJourneyHelp);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(lnkType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> option = driver.findElements(drpDwnoption);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (option.size() > 0) {
			clickElementWb(option.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("caseListView", "createNewCase", Status.FAIL, "Type options are not present");
		}

		if (driver.findElement(lnkTopic).isEnabled()) {
			clickElementBy(lnkTopic);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> topic = driver.findElements(drpDwnoption);
			if (topic.size() > 0) {
				clickElementWb(topic.get(5));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("caseListView", "createNewCase", Status.FAIL,
						"Topic options are not present");
			}
		}

		scrollDownForElementJSBy(lnkTopicDetail);
		scrollUpByPixel(50);

		if (driver.findElement(lnkTopicDetail).isEnabled()) {
			clickElementBy(lnkTopicDetail);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> topicDetail = driver.findElements(drpDwnoption);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (topicDetail.size() > 0) {
				clickElementWb(topicDetail.get(26));
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("caseListView", "createNewCase", Status.FAIL,
						"Topic detail options are not present");
			}
		}
		scrollDownForElementJSWb(driver.findElements(txtBoxSubject).get(driver.findElements(txtBoxSubject).size() - 1));
		scrollUpByPixel(50);
		List<WebElement> txtSubject = driver.findElements(txtBoxSubject);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		txtSubject.get(txtSubject.size() - 1).sendKeys(generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> txtDetails = driver.findElements(txtAreaDetails);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		txtDetails.get(txtDetails.size() - 1).click();
		txtDetails.get(txtDetails.size() - 1).sendKeys(generateRandomString(5, Format.CHARACTER_STRING.getFormat()));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		// clickElementBy(btnSaveCase);
		clickElementJSWithWait(driver.findElements(btnSaveCase).get(driver.findElements(btnSaveCase).size() - 1));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
		if (verifyObjectDisplayed(txtSuccessMsg)) {
			tcConfig.updateTestReporter("HomePage", "createNewCaseHelp", Status.PASS, "Case is created successfully");
		} else {
			tcConfig.updateTestReporter("HomePage", "createNewCaseHelp", Status.FAIL, "Case creation failed");
		}

		if (verifyObjectDisplayed(btnCaseCloseWindow)) {
			clickElementJSWithWait(btnCaseCloseWindow);
		}
	}

	private By btnCaseCloseWindow = By.xpath("//button[@title='Close']");
	private By lnkRecentlyViewed = By.xpath("//span[text()='Recently Viewed']/..");

	public void navigateToRecentlyViewedCase() {
		waitUntilElementVisibleBy(driver, lnkCasesListView, 120);
		if (!(driver.findElement(lnkCasesListView).getText().equals("Recently Viewed"))) {
			clickElementBy(lnkCasesListView);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(lnkRecentlyViewed);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	private By lnkCases = By.xpath("//span[@title='Case Number']/../../../../../../tbody//th//a");

	public void selectCreatedCaseFromHelpIcon() {
		waitUntilElementVisibleBy(driver, txtBoxSearchCases, 120);
		clickElementJSWithWait(driver.findElements(lnkCases).get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, moreOptionsdrpDwn, 120);
	}

	private By btnEditChangeOwner = By.xpath("//button[@title='Change Owner']");
	private By btnChangeOwner = By.xpath("//button[text()='Change Owner' or @value='change owner']");
	private By divSuccessMsg = By.xpath("//span[contains(@class,'toastMessage')]");
	private By txtCaseNumber = By.xpath("//span[text()='Case Number']/../../div[2]//lightning-formatted-text");
	private By txtBoxSearchAgent = By.xpath("//input[@title='Search People']");

	public void changeCaseOwner() {
		waitUntilElementVisibleWb(driver,
				driver.findElements(btnEditChangeOwner).get(driver.findElements(btnEditChangeOwner).size() - 1), 120);
		List<WebElement> edit = driver.findElements(btnEditChangeOwner);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		edit.get(edit.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtBoxSearchAgent, 120);
		fieldDataEnter(txtBoxSearchAgent, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> e = driver.findElements(By.xpath("//div[@title='" + testData.get("AgentName") + "']/../.."));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (e.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("caseListView", "createNewCase", Status.PASS, "Agent is present for selection");
			e.get(e.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> btnChangeOwners = driver.findElements(btnChangeOwner);
			clickElementJS(btnChangeOwners.get(btnChangeOwners.size() - 1));
			waitUntilElementVisibleBy(driver, divSuccessMsg, 120);
			if (verifyObjectDisplayed(divSuccessMsg)) {
				tcConfig.updateTestReporter("HomePage", "changeCaseOwner", Status.PASS,
						"Case owner is changed successfully");
			} else {
				tcConfig.updateTestReporter("HomePage", "changeCaseOwner", Status.FAIL, "Case owner change failed");
			}

		} else {
			tcConfig.updateTestReporter("caseListView", "createNewCase", Status.FAIL,
					"Agent is not present for selection");
		}

		strCaseNumber = driver.findElement(txtCaseNumber).getText();
	}

	private By lnkMyCases = By.xpath("//span[text()='My Cases']/..");

	public void navigateToMyCases() {
		waitUntilElementVisibleBy(driver, lnkCasesListView, 120);
		if (!(driver.findElement(lnkCasesListView).getText().equals("My Cases"))) {
			clickElementBy(lnkCasesListView);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(lnkMyCases);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	public void verifyCaseInList() {
		waitUntilElementVisibleBy(driver, txtBoxSearchCases, 120);
		Actions a = new Actions(driver);
		WebElement ele = driver.findElement(txtBoxSearchCases);
		a.sendKeys(ele, strCaseNumber).build().perform();
		// fieldDataEnter(txtBoxSearchCases, strCaseNumber);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(txtBoxSearchCases).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement e = driver.findElement(By.xpath("//a[@title='" + strCaseNumber + "']"));
		if (verifyElementDisplayed(e)) {
			tcConfig.updateTestReporter("ListViewCase", "verifyCaseInList", Status.PASS,
					"Created case from help icon is coming in My cases");
		} else {
			tcConfig.updateTestReporter("ListViewCase", "verifyCaseInList", Status.FAIL,
					"Created case from help icon is not coming in My cases");
		}
	}

	/*
	 * public void validateCaseStatus(String iterator) {
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); JavascriptExecutor js =
	 * (JavascriptExecutor) driver;
	 * js.executeScript("javascript:window.scrollBy(250,350)");
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
	 * (verifyObjectDisplayed(caseStatus)) { String Casestatus =
	 * driver.findElement(caseStatus).getText().trim();
	 * Assert.assertEquals(Casestatus, testData.get("CaseStatus"+iterator));
	 * tcConfig.updateTestReporter("caseListView", "case page", Status.PASS,
	 * "Case is: " + Casestatus); } else {
	 * tcConfig.updateTestReporter("caseListView", "case page", Status.FAIL,
	 * "Case status is not correct "); } }
	 */
}
