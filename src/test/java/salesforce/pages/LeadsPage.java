package salesforce.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
//import org.jboss.netty.util.internal.SystemPropertyUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class LeadsPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(LeadsPage.class);

	public LeadsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String createdPID;
	// protected String createdTourID;
	public static String createdTourID;
	public static HashMap<String, String> TourIdmap = new HashMap<String, String>();

	protected By reqScoreMobile = By.xpath("//button[contains(.,'Request Soft Score')]");
	protected By scoreBandMobile = By
			.xpath("//div[@class='slds-form-element cTMCMAFields']//div[contains(.,'Score Band')]/../..//span");
	protected By tabReservationMobile = By
			.xpath("//a[@class='slds-tabs_default__link' and contains(.,'Reservations ')]");
	protected By hypReservationMobile = By
			.xpath("//a[@class='slds-show slds-truncate' and contains(@href,'tmreservation')]");

	protected By btnBookTourResrv = By.xpath("//button[contains(text(),'Book Tour')]");
	protected By btnSaveReserv = By
			.xpath("//button[contains(@class,'slds-button slds-button_brand cuf')]//span[contains(.,'Save')]");

	protected By hypLead = By.xpath("//td[contains(@data-label,'Guest Name')]//a");
	protected By tabRelated = By.xpath("(//a[@id='relatedListsTab__item' and contains(.,'Related')])[1]");
	protected By tabReservation = By.xpath("//span[@title='Reservations']");
	protected By lnkViewAll = By
			.xpath("//a[contains(@href,'Lead_Arrivals__r')]//span[@class='view-all-label' and contains(.,'View All')]");
	protected By txtReservation = By.xpath("//h1[@title='Reservations']");
	protected By reserv = By.xpath("//th//a[@data-aura-class='forceOutputLookup']");
	protected By tabOwner = By.xpath("//a[@title='Owners']");

	protected By txtCustInfo_Mobile = By.xpath("//h2[contains(.,'Customer Information')]");
	protected By tbl_Reserv = By.xpath("//tbody//tr//th//a[@data-aura-class='forceOutputLookup']");
	protected By txtOwnerReserv = By.xpath("//h1[@class='slds-page-header__title listViewTitle slds-truncate']");

	// SiteFactory factory = new SiteFactory();

	public static String createdTourID1;
	public static String Lead;
	static final String SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	protected By btnSave = By
			.xpath("//button[contains(@class,'slds-button slds-button_brand cuf-publisherShareButton ')]");
	protected By txtCreateTourRecord = By.xpath("//h2[contains(.,'Create New Tour Record')]");
	protected By lst_Lead = By.xpath("//tbody//tr[@class='slds-line-height_reset']");
	protected By searchLead = By.xpath("//input[@placeholder='Search Name, Member Number, Email, City, Zip...']");
	protected By btnBookTourMobile = By.xpath("//button[contains(.,'BOOK TOUR')]");
	protected By hardStop_Mobile = By
			.xpath("//div[@class='slds-modal__content slds-p-around_medium slds-text-align_left']");
	protected By btnCancel_Mobile = By.xpath("//button[contains(.,'CANCEL')]");
	protected By btnConfirmTour = By.xpath("//button[contains(.,'CREATE TOUR')]");
	protected By softScoreMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By errorMsg = By.xpath("//ul[contains(@class,'errorsList')]");
	protected By bntCancel = By
			.xpath("//button[contains(@class,'slds-button slds-button--neutral cuf-publisherCancelButton uiButton')]");
	protected By btnCreateTourRecord = By.xpath("//button[text()='Create New Tour Record'] | div[@title='Create New Tour Record']");
	protected By qualificationScore = By
			.xpath("//span[contains(.,'Qualification Status')]/../..//lightning-formatted-text[contains(.,'')]");
	protected By btnIncentiveEligible = By.xpath("//button[contains(.,'Incentive Eligible')]");
	protected By txtLeadSearch = By.xpath("//input[contains(@placeholder,'Search Leads and more...')]");
	protected By imgResrv = By.xpath("//span[@class='uiImage']//img[@class='icon ']");
	private By homeTab = By.xpath("//span[contains(.,'Home')]");
	private By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	private By newLeads = By.xpath("//div[@title='New']");
	private By salutationField = By.xpath("//div[contains(@class,'salutation')]//a");
	//private By firstName = By.xpath("//input[contains(@class,'firstName')]");
	
	private By firstName = By.xpath("//input[contains(@name,'firstName')]");
	
	
	//private By lastName = By.xpath("//input[contains(@class,'lastName')]");
	
	private By lastName = By.xpath("//input[contains(@name,'lastName')]");
	private By mobile = By.xpath("//input[@type='tel']");// used as webelement
															// list - first
															// entry
	private By email = By.xpath("//input[@inputmode='email']");
	private By temporaryLeadBox = By.xpath("//input[@class=' input'])[4]");
	private By leadName = By
			.xpath("//span[contains(.,'Temporary Lead Identifier')]/parent::label/following-sibling::input");
	private By pidEnter = By.xpath("//span[text()='PID']/parent::label/following-sibling::input");
	private By streetEnter = By.xpath("//textarea[contains(@class,'street ')]");
	private By cityEnter = By.xpath("//input[contains(@class,'city')]");
	private By zipEnter = By.xpath("//input[contains(@class,'postalCode')]");
	// private By stateEnter = By.xpath("//a[(@class='select')]"); // to be used
	// as web element list-last entry
	private By stateEnter = By.xpath(
			"//span[@data-aura-class='uiPicklistLabel']//span[text()='State/Province']/../..//a[@class='select']");
	private By countryEnter = By.xpath("//a[(@class='select')]"); // to be used
																	// as web
																	// element
																	// list- 2nd
																	// last
																	// entry
	private By saveButton = By.xpath("//button[@title='Save'] "); // or
																	// //span[text()='Save']
	private By createNewTour = By.xpath("//button[contains(text(),'Create New Tour Record')]");
	private By acsButton = By.id("tourCheckBTN");
	private By tourRecords = By.xpath("//span[text()='Tour Records']");
	private By tourlanguage = By.xpath("//span[contains(text(),'Language')]/parent::span/following-sibling::div//a");
	private By qualificationStatus = By
			.xpath("//span[contains(text(),'Qualification')]/parent::span/following-sibling::div//a");
	private By qSelect = By.xpath("//a[@title='Q']");
	private By qualificationSave = By.xpath("//div[@class='modal-footer slds-modal__footer']//span[text()='Save'] | //button[text()='Save']");
	private By tourIdText = By.xpath("//a[@data-refid='recordId']");
	private By txtDuplicateMsg = By.xpath("//div[contains(text(),'duplicate')]/a[contains(text(),'Duplicates')]");
	private By searchAddressLink = By.xpath("//span[text()='Search Address']");
	private By enterAddressEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'uiInput--default')]");
	private By addressOption = By.xpath("(//div[@class='option'])[1]");
	private By globalSearchEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'default')]");
	private By selectLeadItem = By.xpath("(//span[@class='uiImage']/img[contains(@class,'icon')])[1]");
	private By commentsEdit = By.xpath("//span[text()='Comments']/../..//input");
	private By giftEligbleBtn = By.xpath("//button[text()='Incentive Eligible']");
	private By closePopUpBtn = By.xpath("//button[text()='Close']");
	private By txtErrorMsg = By
			.xpath("//p[contains(text(),'This lead has an international address and does not require soft scoring')]");
	private By txtErrorMsgCondition = By
			.xpath("//p[contains(text(),'Please select a Location for the Lead to check Qualification Score')]");
	private By reqSoftScoreBtn = By.xpath("//button[text()='Request Soft Score']");
	private By txtQualifiedMsg = By
			.xpath("//p[contains(text(),'This guest received an active score within the last 30 days.')]");
	private By latestTour = By.xpath("//span[text()='Tour Record']//div");
	private By latestTour_new = By.xpath("//span[text()='Tour Record']//a//div");
	/**
	 * added for score generaion scenario for stitching
	 */
	private By incentiveEligible = By.xpath("(//button[contains(text(),'Incentive Eligible')])[1]");
	private By requestSoftScore = By.xpath("//button[contains(text(),'Request Soft Score')]");
	// private By qscore=By.xpath("(//span[contains(text(),'Qualification
	// Status')])[1]/parent::div/following-sibling::div/span/span |
	// //span[contains(text(),'Qualification
	// Status')]/../..//lightning-formatted-text");
	private By qscore = By.xpath("//span[contains(text(),'Qualification Status')]/../..//lightning-formatted-text");
	private String qscore1 = "(//span[contains(text(),'Qualification Status')])[";
	private String qscore2 = "]/parent::div/following-sibling::div/span/span";
	private By reScoremessage = By.xpath("//p[contains(text(),'active score')]");
	private By closeButton = By.xpath("//button[contains(text(),'Close')]");
	private By globalsrch = By.xpath("//input[contains(@title,'Search')]");
	private By popUpSaveBtn = By.xpath("//button[text()='Save']");
	private By popUpSaveBtn1 = By.xpath("//div[contains(@class,'modal')]/footer/button[text()='Save']");
	private By tourlanuageDrpDwn = By.xpath("//span[text()='Tour Language']/../..//a");
	private By tourLangValue = By.xpath("//li/a[text()='English']");
	private By nqscore = By.xpath(
			"//span[contains(text(),'Qualification Status')]/../../div[2]/span/slot/slot/lightning-formatted-text");
	private By nqLeadcommentmsg = By.xpath("//span[contains(text(),'This Guest is not eligible to Tour.')]");
	private By bookTourBtn = By.xpath("//div//button[text()='Book Tour']");

	private By reservationStatus = By.xpath(
			"(//div[contains(@id,'sectionContent')]//span[text()='Reservation Status'])/parent::div/following-sibling::div/span/slot[1]/slot/lightning-formatted-text");
	// private By reservationStatus=By.xpath("(//span[text()='Reservation
	// Status'])[3]/parent::div/following-sibling::div/span/span");
	private By errorCheckOut = By.xpath("//p[contains(text(),'Tour cannot be')]");
	// ((//span[text()='NQ']))[2]
	// private By

	/**
	 * Added by c24125
	 */
	private By countryName = By.xpath(
			"//span[text()='Country']//parent::div/following-sibling::div/span/slot/slot/lightning-formatted-text");

	// private By phoneNum= By.xpath("//span[text()='Phone
	// Number']/parent::div/following-sibling::div/span/slot/slot/lightning-formatted-phone/a");
	private By phoneNum = By.xpath(
			"//span[text()='Phone Number']/parent::div/following-sibling::div/span/slot/slot/lightning-formatted-phone");
	private By emailLead = By.xpath(
			"//span[text()='Email']/parent::div/following-sibling::div/span/slot/slot/emailui-formatted-email-wrapper/force-aura-action-wrapper/div");
	private By phoneNumEdit = By
			.xpath("(//span[text()='Phone Number'])[1]/parent::div/following-sibling::div/button/span[1]");
	private By phoneNumEdittxtbox = By.xpath("//input[@name='TMPhoneNumber__c']");
	private By emailedittxtbox = By.xpath("//input[@name='TMEmail__c']");
	private By saveLead = By.xpath("//button[text()='Save']");
	// private By guestName= By.xpath("(//a[text()='NOSHOW TEST'])[2]");
	private By guestName = By.xpath(
			"//span[text()='Guest Name (Lead)']/parent::div/following-sibling::div/span/slot/slot/force-lookup/div/force-hoverable-link/div/a");

	private By mobileLead = By.xpath(
			"//span[text()='Phone Number']/parent::div/following-sibling::div/span/slot/slot/lightning-formatted-phone");
	private By emailafter = By.xpath(
			"//span[text()='Email']/parent::div/following-sibling::div/span/slot/slot/emailui-formatted-email-wrapper/force-aura-action-wrapper/div");
	// private By printButton=By.xpath("(//button[contains(text(),'Incentive
	// Eligible')])[1]/parent::div/parent::span/parent::div/preceding-sibling::div[1]/span/button");
	private By printButton = By.xpath("//button[text()='Print']");
	private By printBtn = By.xpath("(//button[text()='Print'])[1]");
	private By mailLetterButton = By.xpath("//button[text()='Mail Letter']");
	private By middleName = By.xpath("//input[contains(@class,'middleName')]");
	private By leadBanner = By.xpath("//header[@role='banner']/div[2]/div/div[1]/div[2]/h1/div[text()='Lead']");
	private By tmcletter = By.xpath("//embed[@type='application/pdf']");
	private By guestLead = By.xpath("//span[text()='Guest Details']");
	public static String lead1;

	@FindBy(xpath = "//a[contains(text(),'Related')]")
	WebElement relatedTab;

	@FindBy(xpath = "//th[contains(@title,'Tour Record Number')]/../../../tbody/tr[1]//a")
	WebElement tourIdLink;

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * SOURCE.length());
			builder.append(SOURCE.charAt(character));
		}
		return builder.toString();
	}

	public void createLead(String addressEnter) {
		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);

			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver.findElement(By.xpath("//a[@title='Mr.']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			// clickElementBy(salutationSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String First = randomAlphaNumeric(5);
			String Last = randomAlphaNumeric(5);
			fieldDataEnter(firstName, First);
			fieldDataEnter(lastName, Last);
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

			// fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));

			Lead = First + " " + Last;
			fieldDataEnter(leadName, Lead);
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				// clickElementBy(countryEnter);
				/*
				 * List <WebElement>country=driver.findElements(countryEnter);
				 * country.get(country.size()-2).click();
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * WebElement countrySelect = driver
				 * .findElement(By.xpath("//a[text()='" +
				 * testData.get("Country").trim() + "']"));
				 * countrySelect.click();
				 */

				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));

				// clickElementBy(stateEnter);
				List<WebElement> State = driver.findElements(countryEnter);
				State.get(State.size() - 1).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State").trim() + "')]"));
				stateSelect.click();

				// clickElementBy(stateSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {

				clickElementBy(searchAddressLink);
				String cityname = testData.get("City");
				// String cityNum = cityname.split(" ")[0];

				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit,
						cityname);/*
									 * + " " + testData.get("City") + " " +
									 * testData.get("State") + " " +
									 * testData.get("ZIP"));
									 */
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(addressOption)) {
					clickElementJSWithWait(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead With Google Address", Status.PASS,
							"Address identified by Google Address help");
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead With Google Address", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(streetEnter, testData.get("StreetName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(zipEnter, testData.get("ZIP"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(zipEnter).sendKeys(Keys.TAB);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(saveButton);

			WebElement SAVE = driver.findElement(saveButton);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", SAVE);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}
			} else {
				waitUntilElementVisibleBy(driver, createNewTour, 120);
				WebElement lblLeadConfirm = driver.findElement(
						By.xpath("//div[contains(@class,'page-header')]//lightning-formatted-name[contains(text(),'"
								+ Lead + "')]"));

				if (verifyElementDisplayed(lblLeadConfirm)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"New Lead Created by Name : " + Lead);
					// + "//with PID : "+createdPID);
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL, "Lead Creation Failed");
				}
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"User is unable to navigate to Leads creation page");
		}

	}

	public void createLead_Duplicate(String addressEnter) {
		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);

			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			// clickElementBy(salutationSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(firstName, testData.get("FirstName"));
			fieldDataEnter(lastName, testData.get("LastName"));
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

			// fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			fieldDataEnter(leadName, testData.get("LeadName"));
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				/*
				 * clickElementBy(countryEnter);
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * WebElement countrySelect = driver
				 * .findElement(By.xpath("//a[text()='" +
				 * testData.get("Country").trim() + "']"));
				 * countrySelect.click();
				 */
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));

				clickElementBy(stateEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver.findElement(By.xpath("//a[@title='" + testData.get("State") + "']"));
				stateSelect.click();

				// clickElementBy(stateSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// clickElementBy(saveButton);
			WebElement SAVE = driver.findElement(saveButton);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", SAVE);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}
			} else {
				waitUntilElementVisibleBy(driver, createNewTour, 120);
				WebElement lblLeadConfirm = driver
						.findElement(By.xpath("//div[contains(@class,'page-header')]/span[contains(text(),'"
								+ testData.get("LeadName") + "')]"));

				if (verifyElementDisplayed(lblLeadConfirm)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"New Lead Created by Name : " + testData.get("LeadName"));
					// + "//with PID : "+createdPID);
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL, "Lead Creation Failed");
				}
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"User is unable to navigate to Leads creation page");
		}

	}

	public void clickIncentivebuttonandsoftscoreNQ() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, btnincentive, 120);
		clickElementBy(btnincentive);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, softscore, 120);
		clickElementBy(softscore);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("//button[@title='Edit Qualification Status']")).click();
		clickElementBy(qualificationStatus);
		WebElement qualificationStatusSelect = driver
				.findElement(By.xpath("//a[@title='" + testData.get("QualificationScore") + "']"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		qualificationStatusSelect.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("(//span[@class=' label bBody' and text()='Save'])[2]")).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String scoretxt = driver.findElement(score).getText();
		if (verifyElementDisplayed(driver.findElement(score))) {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS,
					"Soft score successful and printed as " + scoretxt);
		} else {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Failed while Soft Scoring");
		}
	}

	public void searchForLead() {
		waitUntilElementVisibleBy(driver, globalSearchEdit, 120);
		if (Lead == null) {
			fieldDataEnter(globalSearchEdit, testData.get("LeadName"));
		} else {
			fieldDataEnter(globalSearchEdit, Lead);
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(selectLeadItem);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (Lead == null) {
			WebElement lblLeadConfirm = driver.findElement(
					By.xpath("//div[contains(@class,'page-header')]//lightning-formatted-name[contains(text(),'"
							+ testData.get("LeadName") + "')]"));
			if (verifyElementDisplayed(lblLeadConfirm)) {
				tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.PASS,
						"Lead details is displayed after searching for lead : " + testData.get("LeadName"));
			} else {
				tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.FAIL,
						"Unable to find the Lead details after searching for lead : " + Lead);
			}
		} else {
			WebElement lblLeadConfirm = driver.findElement(
					By.xpath("//div[contains(@class,'page-header')]//lightning-formatted-name[contains(text(),'" + Lead
							+ "')]"));
			if (verifyElementDisplayed(lblLeadConfirm)) {
				tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.PASS,
						"Lead details is displayed after searching for lead : " + Lead);
			} else {
				tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.FAIL,
						"Unable to find the Lead details after searching for lead : " + Lead);
			}
		}
	}

	public void createTourFromLead() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, createNewTour, 120);
		if (verifyObjectDisplayed(createNewTour)) {
			clickElementJSWithWait(createNewTour);
			tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.PASS,
					"able to click create new tour button");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.FAIL,
					"able to click create new tour button");
		}

		/*
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, tourlanuageDrpDwn, 120);
		 * clickElementJSWithWait(tourlanguage); WebElement tourlanguageSelect =
		 * driver.findElement(By.xpath("//a[@title='" +
		 * testData.get("tourlanguage") + "']"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementJS(tourlanguageSelect);
		 * tcConfig.updateTestReporter("LeadsPage", "createTourFromLead",
		 * Status.PASS, "able to click tour language");
		 */

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, qualificationSave, 120);
		if (verifyObjectDisplayed(qualificationSave)) {
			clickElementJSWithWait(qualificationSave);
			tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.PASS, "able to click save button");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.FAIL, "able to click save button");
		}

		/*
		 * waitUntilElementVisibleBy(driver, tourRecords, 120);
		 * 
		 * if(verifyObjectDisplayed(tourRecords)) { clickElementBy(tourRecords);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * List<WebElement> listtourRecord= driver.findElements(tourIdText);
		 * tcConfig.updateTestReporter("LeadsPage", "createLead",
		 * Status.PASS, "Tour Created with No. "
		 * +listtourRecord.get(0).getText());
		 * 
		 * }else { tcConfig.updateTestReporter("LeadsPage", "createLead",
		 * Status.FAIL, "Tour Creation Failed"); }
		 */
	}

	public void checkGiftEligibility() {
		waitUntilObjectVisible(driver, giftEligbleBtn, 120);
		clickElementBy(giftEligbleBtn);
		waitUntilObjectVisible(driver, closePopUpBtn, 120);
		if (verifyObjectDisplayed(txtErrorMsg)) {
			clickElementBy(closePopUpBtn);
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.PASS,
					"Error message is displayed properly");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.FAIL,
					"Error message NOT displayed properly");
			clickElementBy(closePopUpBtn);
		}
	}

	public void checkGiftEligibilityLeads() {
		waitUntilObjectVisible(driver, giftEligbleBtn, 120);
		clickElementBy(giftEligbleBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (verifyObjectDisplayed(txtQualifiedMsg)) {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.PASS,
					"User is unable to softscore the Lead as it " + "is already softscored");
		} else {

			waitUntilObjectVisible(driver, reqSoftScoreBtn, 120);
			if (verifyObjectDisplayed(txtErrorMsgCondition)) {
				tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibilityLeads", Status.PASS,
						"User is able to softscore new Lead");
			} else {
				tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibilityLeads", Status.FAIL,
						"User is unable to softscore");
			}
			clickElementBy(reqSoftScoreBtn);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			clickElementBy(giftEligbleBtn);
			waitUntilObjectVisible(driver, closePopUpBtn, 120);
			if (verifyObjectDisplayed(txtQualifiedMsg)) {
				tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.PASS,
						"User is unable to softscore the Lead as it " + "is already softscored");
			} else {
				tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.FAIL,
						"User is able to softscore the Lead even if it " + "is already softscored");
			}
		}
		clickElementBy(closePopUpBtn);

	}

	public void createLeadAndTour(String addressEnter) {

		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementJSWithWait(newLeads);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisible(driver, driver.findElement(firstName), 120);
			clickElementJSWithWait(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver.findElement(By.xpath("//a[@title='Mr.']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String First = randomAlphaNumeric(5);
			String Last = randomAlphaNumeric(5);
			fieldDataEnter(firstName, First);
			fieldDataEnter(lastName, Last);
			fieldDataEnter(email, testData.get("Email"));
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));
			Lead = First + " " + Last;
			fieldDataEnter(leadName, Lead);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> addList = driver.findElements(stateEnter);
				addList.get(addList.size() - 1).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
				stateSelect.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementJSWithWait(searchAddressLink);
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLeadAndTour", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLeadAndTour", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			clickElementJSWithWait(saveButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLeadAndTour", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}
			}

			if (testData.get("QScoreGenerate").equalsIgnoreCase("Y")) {
				validateSoftScoreOflead();

			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilObjectVisible(driver, createNewTour, 120);
			clickElementJSWithWait(createNewTour);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilObjectVisible(driver, tourlanguage, 120);
			clickElementJSWithWait(tourlanguage);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement tourlanguageSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
			tourlanguageSelect.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilObjectVisible(driver, qualificationSave, 120);
			clickElementJSWithWait(qualificationSave);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
		}
	}

	public void getlatestTour() {

		waitUntilObjectVisible(driver, latestTour, 120);
		if (verifyObjectDisplayed(latestTour)) {
			createdTourID = driver.findElement(latestTour).getText();
			TourIdmap.put(testData.get("AutomationID"), driver.findElement(latestTour).getText());
			clickElementJS(latestTour);
			System.out.println("created TourID: " + createdTourID);
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS,
					"created TourID: " + createdTourID);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
		}

	}

	public void getlatestTour_Latest() throws AWTException {

		// waitUntilElementVisibleBy(driver, latestTour_new, 120);
		if (verifyObjectDisplayed(latestTour_new)) {
			createdTourID = driver.findElement(latestTour_new).getText();
			driver.findElement(latestTour_new).click();
			System.out.println("created TourID: " + createdTourID);
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS,
					"created TourID: " + createdTourID);

		} else {
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
		}

	}

	public void searchTour_global() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.findElement(By.xpath("//a//span[text()='Home']")).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("//input[contains(@title,'Search Tour')]")).sendKeys(createdTourID);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("//a//div//span[contains(@title,'" + createdTourID + "')]")).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	private By btnincentive = By.xpath("//button[@id='tourCheckBTN' and contains(.,'Incentive Eligible')]");
	private By softscore = By.xpath("//button[text()=' Request Soft Score ']");

	private By score = By.xpath("//span[text()='Qualification Status']/../..//lightning-formatted-text[text()='"
			+ testData.get("QualificationScore") + "']");

	public void clickIncentivebuttonandsoftscore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, btnincentive, 120);
		clickElementJS(btnincentive);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, softscore, 120);
		clickElementJS(softscore);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(score);
		scrollUpByPixel(200);
		waitUntilElementVisibleBy(driver, score, 120);
		if (verifyElementDisplayed(driver.findElement(score))) {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Soft score successful");
		} else {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Failed while Soft Scoring");
		}
	}

	public void validatescore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, score, 120);
		String Score = driver.findElement(score).getText();
		if (Score.equalsIgnoreCase(testData.get("QualificationScore"))) {
			tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS,
					"Qualification Score is: " + Score);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"Failed while getting Qualification Score");
		}

	}

	private By crossBtn = By.xpath("//div[@role='combobox']//span/button");
	private By check = By.xpath("//a[contains(@class,'textUnderline outputLookupLink')]"); // used
																							// as
																							// Webelement
																							// list
																							// -
																							// last
																							// entry
	String Creditcode = testData.get("QualificationScore");
	private By creditBandCode = By.xpath("//span[(text()='" + Creditcode.trim() + "')]");
	private By creditBand = By.xpath("//div//span[text()='Credit Band Code']//..//following-sibling::div");

	@FindBy(xpath = "//div[@class='slds-form-element']//input[@class='slds-input slds-combobox__input']")
	WebElement SearchLocation;

	public void checkQualificationScore() throws AWTException {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, giftEligbleBtn, 240);
		clickElementBy(giftEligbleBtn);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, crossBtn, 120);
		clickElementBy(crossBtn);
		SearchLocation.sendKeys(testData.get("AppointmentLoc"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Robot rob;

		rob = new Robot();
		rob.mouseWheel(15);
		List<WebElement> resultList = driver.findElements(By.xpath("//li[@class='slds-listbox__item']"));

		resultList.get(0).click();

		waitUntilObjectVisible(driver, reqSoftScoreBtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(reqSoftScoreBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		relatedTab.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		Robot rob1;

		rob1 = new Robot();
		rob1.mouseWheel(10);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> Check = driver.findElements(check);

		waitUntilObjectVisible(driver, Check.get(Check.size() - 1), 120);
		// clickElementBy(check);
		// clickElementJS(check);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Check.get(Check.size() - 1).click();
		System.out.println("The code is here");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilObjectVisible(driver, creditBandCode, 120);
		if (verifyElementDisplayed(driver.findElement(creditBand))) {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.PASS,
					"Credit Band Code is: " + (driver.findElement(creditBand).getText()));
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
					"Failed while getting Credit Band Code");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	// String Creditcode=testData.get("QualificationScore");
	// private By creditBandCode =
	// By.xpath("//span[(text()='"+Creditcode.trim()+"')]");
	// private By checkQualificationScore =
	// By.xpath("//span[text()='Qualification
	// Status']//..//..//span[text()='"+testData.get("QualificationScore")+"']");
	private By btnMailLetter = By.id("refresh");
	private By btnPrintLetter = By.id("leadPrint");

	public void scorValidation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, giftEligbleBtn, 120);
		clickElementBy(giftEligbleBtn);
		waitUntilObjectVisible(driver, reqSoftScoreBtn, 120);

		clickElementBy(reqSoftScoreBtn);
		waitUntilObjectVisible(driver, creditBandCode, 120);

		/*
		 * if(driver.findElement(checkQualificationScore).getText().
		 * equalsIgnoreCase(testData.get("QualificationScore"))||
		 * driver.findElement(checkQualificationScore).getText().
		 * equalsIgnoreCase(testData.get("TourNo"))){
		 * tcConfig.updateTestReporter("LeadsPage", "validatescore",
		 * Status.PASS, "Check Qualification is: "+checkQualificationScore);
		 * }else{ tcConfig.updateTestReporter("LeadsPage",
		 * "checkQualificationScore", Status.FAIL,
		 * "Failed while getting Qualification"); }
		 */

		if (verifyElementDisplayed(driver.findElement(creditBandCode))) {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.PASS,
					"Check Qualification is: " + testData.get("QualificationScore"));
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
					"Failed while getting Qualification");
		}
	}

	public void printButtonNotEnabled() {

		if (!driver.findElement(btnPrintLetter).isEnabled()) {
			tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS, "Print button is disabled ");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
					"Print button is enabled");
		}
	}

	public void mailLetterButtonNotEnabled() {

		if (!driver.findElement(btnMailLetter).isEnabled()) {
			tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS,
					"Mail Letter button is disabled ");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
					"Mail Letter button is enabled");
		}
	}

	public void mailLetterButtonClick() {
		waitUntilObjectVisible(driver, btnMailLetter, 120);
		clickElementBy(btnMailLetter);
	}

	public void validateSoftScoreOflead() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, incentiveEligible, 120);
		clickElementJSWithWait(incentiveEligible);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, requestSoftScore, 120);
		clickElementJSWithWait(requestSoftScore);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(qscore);
		waitUntilElementVisibleBy(driver, qscore, 120);
		String score = driver.findElement(qscore).getText();
		if (driver.findElement(qscore).isDisplayed()) {
			tcConfig.updateTestReporter("LeadsPage", "validateSoftScoreOflead", Status.PASS,
					"User is able to to Generate Qualification Score::" + score);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateSoftScoreOflead", Status.FAIL,
					"User is Not able to to Generate Qualification Score");
		}

	}

	public void CreateTourforLead() {
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("OwnerName"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("(//div//div[contains(text(),'Lead')  and @dir='ltr'])[1]")).getText()
					.contains("Lead")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("(//a//div//span[@title='" + testData.get("LeadName") + "'])[1]"));
				seacrhResOwnerLink.click();
				waitUntilElementVisibleBy(driver, createNewTour, 120);
				clickElementBy(createNewTour);
				waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				clickElementBy(tourlanuageDrpDwn);
				clickElementBy(tourLangValue);
				clickElementBy(popUpSaveBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("LeadsPage", "selectLeadCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectLeadCreateTour", Status.FAIL,
					"User is unable to navigate to Lead Page");
		}

	}

	/**
	 * done by c24125
	 */
	public void createLeadAndGenerateScore(String addressEnter) {
		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.findElement(By.xpath("//span[text()='Next']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);
			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// WebElement salutationSelect =
			// driver.findElement(By.xpath("//a[@title='" +
			// testData.get("Salutation").trim() + "']"));
			WebElement salutationSelect = driver.findElement(By.xpath("//a[@title='Mr.']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String First = randomAlphaNumeric(5);
			String Last = randomAlphaNumeric(5);
			fieldDataEnter(firstName, First);
			fieldDataEnter(lastName, Last);
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

			// fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			Lead = First + " " + Last;
			fieldDataEnter(leadName, Lead);
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> addList = driver.findElements(stateEnter);
				addList.get(addList.size() - 1).click();
				// clickElementBy(stateEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
				stateSelect.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			clickElementBy(saveButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * if(testData.get("QScoreGenerate").equalsIgnoreCase("Y")) {
			 * log.info(
			 * "Trying to validate score generation for new lead-------------");
			 * validateSoftScoreOflead();
			 */

			// }
		}
	}

	public void validateLead_Duplicate() {
		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementJSWithWait(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);

			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			// clickElementBy(salutationSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(firstName, testData.get("FirstName"));
			fieldDataEnter(lastName, testData.get("LastName"));

			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

			// fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			// fieldDataEnter(streetEnter, testData.get("StreetName"));
			;
			driver.findElement(mobile).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtDuplicateMsg, 120);
			if (verifyObjectDisplayed(txtDuplicateMsg)) {
				System.out.println(driver.findElement(txtDuplicateMsg).getText());
				tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
						"Duplicate message is displayed if same data is used during Lead Creation");
			} else {
				tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
						"Duplicate message is NOT displayed if same data is used during Lead Creation");
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"User is unable to navigate to Leads creation page");
		}
	}
	
	

	/**
	 * TC_96
	 */
	public void validateCommentMsgAndTourforNQLead() {

		searchForLead();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, nqscore, 120);

		String score = driver.findElement(nqscore).getText();
		System.out.println(score);
		if (driver.findElement(nqscore).isDisplayed()) {
			tcConfig.updateTestReporter("LeadsPage", "validateCommentMsgAndTourforNQLead", Status.PASS,
					"The band is ::" + score);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateCommentMsgAndTourforNQLead", Status.FAIL,
					"The band is not NQ");
		}
		if (verifyObjectDisplayed(createNewTour)) {
			waitUntilObjectVisible(driver, createNewTour, 120);
			clickElementJSWithWait(createNewTour);
			tcConfig.updateTestReporter("LeadsPage", "validateCommentMsgAndTourforNQLead", Status.PASS,
					"able to click create tour button");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateCommentMsgAndTourforNQLead", Status.FAIL,
					" not able to click create tour button");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, tourlanguage, 120);
		clickElementJSWithWait(tourlanguage);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement tourlanguageSelect = driver
				.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
		tourlanguageSelect.click();
		waitUntilObjectVisible(driver, qualificationSave, 120);
		if (verifyObjectDisplayed(qualificationSave)) {
			clickElementJSWithWait(qualificationSave);
			waitUntilObjectVisible(driver, errorMessage, 30);
			if (verifyObjectDisplayed(errorMessage)) {
				String Messageerror = driver.findElement(errorMessage).getText();
				tcConfig.updateTestReporter("LeadPage", "validateCommentMsgAndTourforNQLead", Status.PASS, "Error Message present as: "+Messageerror);
			}else{
				tcConfig.updateTestReporter("LeadPage", "validateCommentMsgAndTourforNQLead", Status.FAIL, "Error message not present even if tour dispositoned within 14 days");
			}
			
		} else {
			tcConfig.updateTestReporter("LeadPage", "validateCommentMsgAndTourforNQLead", Status.FAIL, "Failed to click save button");

		}
		waitUntilElementVisibleBy(driver, cancelButton, 120);
		if (verifyObjectDisplayed(cancelButton)) {
			clickElementJSWithWait(cancelButton);
			tcConfig.updateTestReporter("", "validateCommentMsgAndTourforNQLead", Status.PASS, "Successfully clicked on Cancel Button");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateCommentMsgAndTourforNQLead", Status.FAIL,
					"Failed to click cancel button");
		}

	}

	private By nqscoreBand = By.xpath("//label[text()='ACS Qualification Score']/following-sibling::div/input");
	private By commentNQ = By.xpath("//label[text()='Comments']/../div/input");
	private By cancelButton = By.xpath("//footer[@class='slds-modal__footer']/button[text()='Cancel'] | //div[contains(@class,'slds-modal__footer')]//span[text()='Cancel']");
	protected By errorMessage = By.xpath("//span[contains(@class,'toastMessage')] | //div[@class = 'genericNotification']/following-sibling::ul/li");

	public void validateMsgForNQReservation() {
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, nqscoreBand, 120);
				if (verifyObjectDisplayed(nqscoreBand)) {
					String nqScoreBand = driver.findElement(nqscoreBand).getAttribute("value");
					System.out.println(nqScoreBand);
					tcConfig.updateTestReporter("LeadsPage", "validateMsgForNQReservation", Status.PASS,
							"Score Band is visible");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "validateMsgForNQReservation", Status.FAIL,
							" Score not present");
				}
				waitUntilObjectVisible(driver, qualificationSave, 120);
				if (verifyObjectDisplayed(qualificationSave)) {
					clickElementJSWithWait(qualificationSave);
					waitUntilObjectVisible(driver, errorMessage, 30);
					if (verifyObjectDisplayed(errorMessage)) {
						String Messageerror = driver.findElement(errorMessage).getText();
						tcConfig.updateTestReporter("LeadPage", "validateMsgForNQReservation", Status.PASS, "Error Message present as: "+Messageerror);
					}else{
						tcConfig.updateTestReporter("LeadPage", "validateMsgForNQReservation", Status.FAIL, "Error message not present even if tour dispositoned within 14 days");
					}
					
				} else {
					tcConfig.updateTestReporter("LeadPage", "validateMsgForNQReservation", Status.FAIL, "Failed to click save button");

				}
				
				

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, cancelButton, 120);
				if (verifyObjectDisplayed(cancelButton)) {
					clickElementJSWithWait(cancelButton);
					tcConfig.updateTestReporter("LeadsPage", "validateMsgForNQReservation", Status.PASS, "Successfully clicked on Cancel Button");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "validateMsgForNQReservation", Status.FAIL,
							"Failed to click cancel button");
				}

			}
		}

	}

	public void CreateTour_Reservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyElementDisplayed(driver.findElement(globalsrch))) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				String Message = driver.findElement(By.xpath("//input[@disabled]")).getAttribute("value");
				System.out.println(Message);
				/*
				 * if(Message.equalsIgnoreCase(testData.get(
				 * "verifyDuplicateLead"))) {
				 * tcConfig.updateTestReporter("OwnerPage",
				 * "Verify COurtesy tour for DNG Owner with warning message",
				 * Status.PASS,"created TourID : " + createdTourID); } else {
				 * tcConfig.updateTestReporter("OwnerPage",
				 * "Verify COurtesy tour for DNG Owner with warning message",
				 * Status.FAIL,"Not Created"); }
				 */

				/*
				 * waitUntilElementVisibleBy(driver, languageDrpdwn, 120);
				 * 
				 * clickElementBy(tourlanuageDrpDwn);
				 * clickElementBy(tourLangValue); clickElementBy(popUpSaveBtn);
				 * Select s = new Select(driver.findElement(languageDrpdwn));
				 * s.selectByVisibleText("English");
				 */

				waitUntilElementVisibleBy(driver, popUpSaveBtn1, 120);

				WebElement tour = driver.findElement(popUpSaveBtn1);
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", tour);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				// clickElementBy(popUpSaveBtn);
				// driver.findElement(By.xpath("//footer[contains(@class,'slds-modal')]/button[text()='Save']")).click();

				/*
				 * waitUntilElementVisibleBy(driver, latestTour, 120); if
				 * (verifyObjectDisplayed(latestTour)) { createdTourID =
				 * driver.findElement(latestTour).getText(); System.out.println(
				 * "created TourID: " + createdTourID);
				 * tcConfig.updateTestReporter("LeadPage", "getlatestTour",
				 * Status.PASS,"created TourID: " + createdTourID);
				 * driver.findElement(latestTour).click(); } else {
				 * tcConfig.updateTestReporter("LeadPage", "getlatestTour",
				 * Status.FAIL, "Tour not created"); }
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 */} else {
				tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerCreateTour", Status.FAIL,
					"User is unable to navigate to Owners Page");
		}

	}

	public void validateCheckdOutReservation() {
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);

				waitUntilElementVisibleBy(driver, reservationStatus, 120);
				String s = getTextFromBodyBy(reservationStatus);
				if (s.equalsIgnoreCase("Checked-Out")) {
					tcConfig.updateTestReporter("OwnersPage", "validate the reservation is checkd out", Status.PASS,
							"Status matched");

				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate the reservation is checkd out", Status.PASS,
							"Status does not matched");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);
				waitUntilElementVisibleBy(driver, errorCheckOut, 120);
				String s1 = getTextFromBodyBy(errorCheckOut);
				if (s1.equalsIgnoreCase("Tour cannot be created as Guest reservation is already Checked-Out.")) {
					tcConfig.updateTestReporter("OwnersPage", "validate the checkedout reservation cannot be book tour",
							Status.PASS, "Error message validated");
				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate the checkedout reservation cannot be book tour",
							Status.FAIL, "Error message can not be validated");

				}

			} else {
				tcConfig.updateTestReporter("OwnersPage", "validate the checkedout reservation cannot be book tour",
						Status.FAIL, "Error message can not be validated");
			}
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validate the checkedout reservation cannot be book tour",
					Status.FAIL, "Error message can not be validated");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, closeButton, 120);
		// clickElementBy(closeButton);
		clickElementJSWithWait(closeButton);

	}

	public void validateCancelledReservation() {
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);

				waitUntilElementVisibleBy(driver, reservationStatus, 120);
				String s = getTextFromBodyBy(reservationStatus);
				if (s.equalsIgnoreCase("Cancelled")) {
					tcConfig.updateTestReporter("OwnersPage", "validate the reservation is Cancelled", Status.PASS,
							"Status matched");

				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate the reservation is Cancelled", Status.FAIL,
							"Status does not matched");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);
				waitUntilElementVisibleBy(driver, errorCheckOut, 120);
				String s1 = getTextFromBodyBy(errorCheckOut);
				if (s1.equalsIgnoreCase("Tour cannot be created as Guest reservation is already Cancelled/No Show.")) {
					tcConfig.updateTestReporter("OwnersPage", "validate the Cancelled reservation cannot be book tour",
							Status.PASS, "Error message validated");
				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate the Cancelled reservation cannot be book tour",
							Status.FAIL, "Error message can not be validated");

				}

			} else {
				tcConfig.updateTestReporter("OwnersPage", "validate the Cancelled reservation cannot be book tour",
						Status.FAIL, "Error message can not be validated");
			}
		} else {
			tcConfig.updateTestReporter("OwnersPage", "validate the checkedout reservation cannot be book tour",
					Status.FAIL, "Error message can not be validated");
		}

	}

	public void bookTourforNonUSAReservation() {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");

			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();

				waitUntilElementVisibleBy(driver, countryName, 120);
				String countryname = driver.findElement(countryName).getText();
				System.out.println(countryname);
				if (!countryname.equalsIgnoreCase("United States")) {
					tcConfig.updateTestReporter("LeadsPage", "validate country is Non USA", Status.PASS,
							"country is other than USA:" + countryname);
				} else {
					tcConfig.updateTestReporter("LeadsPage", "validate country is USA", Status.FAIL,
							"country is USA");
				}
				// clickElementBy(createNewOwnerLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);
				waitUntilElementVisibleBy(driver, popUpSaveBtn, 120);

				WebElement tour = driver.findElement(popUpSaveBtn);
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", tour);
				//
			} else {
				tcConfig.updateTestReporter("LeadsPage", "validate tour not happen for non USA", Status.FAIL,
						"No Search Result found");
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "validate tour not happen for non USA", Status.FAIL,
					"No Search Result found");
		}

	}

	public void validatePhone_Email_change_non_owner_Reservation() throws AWTException {

		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver
						.findElement(By.xpath("//a//div//span[@title='" + testData.get("ReservationNumber") + "']"));
				seacrhResOwnerLink.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				scrollDownForElementJSBy(guestLead);
				scrollUpByPixel(200);
				if (verifyObjectDisplayed(guestLead)) {
					tcConfig.updateTestReporter("LeadsPage","validatePhone_Email_change_non_owner_Reservation", Status.PASS,"Found Guest Details");
					waitUntilElementVisibleBy(driver, phoneNum, 120);
					String beforeUpdatephn = getTextFromBodyBy(phoneNum);
					waitUntilElementVisibleBy(driver, emailLead, 120);
					String beforeUpdateemail = getTextFromBodyBy(emailLead);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementJSWithWait(phoneNumEdit);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, phoneNumEdittxtbox, 120);
					driver.findElement(phoneNumEdittxtbox).click();
					driver.findElement(phoneNumEdittxtbox).clear();
					String phone = getRandomString(10);
					fieldDataEnter(phoneNumEdittxtbox, phone);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, emailedittxtbox, 120);
					driver.findElement(emailedittxtbox).clear();
					driver.findElement(emailedittxtbox).sendKeys(getRandomStringEmail());
					driver.findElement(emailedittxtbox).sendKeys(Keys.TAB);
					clickElementJSWithWait(saveLead);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					waitUntilElementVisibleBy(driver, mobileLead, 120);
					String afterUpdatephn = getTextFromBodyBy(mobileLead);
					String afterUpdateemail = getTextFromBodyBy(emailafter);

					if (beforeUpdateemail.contains(afterUpdateemail) && (beforeUpdatephn.contains(afterUpdatephn))) {
						tcConfig.updateTestReporter("LeadsPage",
								"validatePhone_Email_change_non_owner_Reservation", Status.FAIL,
								"Email or Phone Number Update not successfull");
					} else {
						tcConfig.updateTestReporter("LeadsPage",
								"validate Email and phone number Update of non owner Reservation", Status.PASS,
								"Email and Phone Number is Updated successfull");
					}
				} else {
					tcConfig.updateTestReporter("LeadsPage",
							"validate Email and phone number Update of non owner Reservation", Status.FAIL,
							"Update Not successfull");
				}
			} else {
				tcConfig.updateTestReporter("LeadsPage",
						"validate Email and phone number Update of non owner Reservation", Status.FAIL,
						"Update Not successfull");
			}
		}

	}

	public void validateprintandMailLetterforQLead() {
		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementJSWithWait(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.findElement(By.xpath("//span[text()='Next']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);
			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// WebElement salutationSelect =
			// driver.findElement(By.xpath("//a[@title='" +
			// testData.get("Salutation").trim() + "']"));
			WebElement salutationSelect = driver.findElement(By.xpath("//a[@title='Mr.']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String First = randomAlphaNumeric(5);
			String Last = randomAlphaNumeric(5);
			fieldDataEnter(firstName, First);
			fieldDataEnter(lastName, Last);
			if (testData.get("MiddleNamePresent").equalsIgnoreCase("Y")) {
				fieldDataEnter(middleName, testData.get("MiddleName"));
			}
			fieldDataEnter(lastName, Last);
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

			// fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			Lead = First + " " + Last;
			fieldDataEnter(leadName, Lead);

			fieldDataEnter(streetEnter, testData.get("StreetName"));
			fieldDataEnter(cityEnter, testData.get("City"));
			fieldDataEnter(zipEnter, testData.get("ZIP"));

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> addList = driver.findElements(stateEnter);
			addList.get(addList.size() - 1).click();
			// clickElementBy(stateEnter);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement stateSelect = driver.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
			stateSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJSWithWait(saveButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			log.info("Trying to validate score generation for new lead-------------");
			waitUntilElementVisibleBy(driver, incentiveEligible, 120);
			clickElementJSWithWait(incentiveEligible);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, requestSoftScore, 120);
			clickElementJSWithWait(requestSoftScore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, qscore, 120);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String score = driver.findElement(qscore).getText();
			System.out.println(score);
			if (driver.findElement(qscore).isDisplayed()) {
				tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS,
						"User is able to to Generate Qualification Score::" + score);
			} else {
				tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
						"User is Not able to to Generate Qualification Score");
			}

			waitUntilElementVisibleBy(driver, printButton, 120);

			// && driver.findElement(mailLetterButton).isEnabled()

			if (!driver.findElement(printButton).isEnabled()) {
				// System.out.println(driver.findElement(printButton).isEnabled());
				tcConfig.updateTestReporter("LeadsPage", "validate Print  Button enabled for Q and NQ band lead ",
						Status.PASS, "successfull");

			} else {

				tcConfig.updateTestReporter("LeadsPage",
						"validate Print and MailLetter Button enabled for Q band lead ", Status.FAIL,
						"Unsuccessfull");
			}

		} else {

			tcConfig.updateTestReporter("LeadsPage", "validate Print and MailLetter Button enabled for Q band lead ",
					Status.FAIL, "Unsuccessfull");

		}
	}

	public void validatemailandprintdisabledforPCband() {
		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.findElement(By.xpath("//span[text()='Next']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);
			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// WebElement salutationSelect =
			// driver.findElement(By.xpath("//a[@title='" +
			// testData.get("Salutation").trim() + "']"));
			WebElement salutationSelect = driver.findElement(By.xpath("//a[@title='Mr.']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String First = randomAlphaNumeric(5);
			String Last = randomAlphaNumeric(5);
			fieldDataEnter(firstName, First);
			fieldDataEnter(lastName, Last);
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

			// fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			Lead = First + " " + Last;
			fieldDataEnter(leadName, Lead);
			fieldDataEnter(streetEnter, testData.get("StreetName"));
			fieldDataEnter(cityEnter, testData.get("City"));
			fieldDataEnter(zipEnter, testData.get("ZIP"));

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> addList = driver.findElements(stateEnter);
			addList.get(addList.size() - 1).click();
			// clickElementBy(stateEnter);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement stateSelect = driver.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
			stateSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(saveButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info("Trying to validate score generation for new lead-------------");
			waitUntilElementVisibleBy(driver, incentiveEligible, 120);
			if (verifyObjectDisplayed(incentiveEligible)) {

				clickElementJSWithWait(incentiveEligible);
				tcConfig.updateTestReporter("LeadsPage", "validateincentiveEligible", Status.PASS,
						"User is able to to click incentiveEligible");
			} else {
				tcConfig.updateTestReporter("LeadsPage", "validateincentiveEligible", Status.FAIL,
						"User is not able to to click incentiveEligible");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, requestSoftScore, 120);
			clickElementJSWithWait(requestSoftScore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, qscore, 120);
			String score = driver.findElement(qscore).getText();
			System.out.println(score);
			if (driver.findElement(qscore).isDisplayed()) {
				tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS,
						"User is able to to Generate Qualification Score::" + score);
			} else {
				tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
						"User is Not able to to Generate Qualification Score");
			}

			waitUntilElementVisibleBy(driver, printButton, 120);

			if (!driver.findElement(printButton).isEnabled()) {
				// System.out.println(driver.findElement(printButton).isEnabled());
				tcConfig.updateTestReporter("LeadsPage", "validate Print  Button enabled forPC band lead ",
						Status.PASS, "Unsuccessfull");

			} else {

				tcConfig.updateTestReporter("LeadsPage",
						"validate Print and MailLetter Button enabled for PC band lead ", Status.FAIL,
						"successfull");
			}

			waitUntilElementVisibleBy(driver, mailLetterButton, 120);
			if (!driver.findElement(mailLetterButton).isEnabled()) {
				// System.out.println(driver.findElement(printButton).isEnabled());
				tcConfig.updateTestReporter("LeadsPage", "validate mailLetter Button enabled forPC band lead ",
						Status.PASS, "Unsuccessfull");
			} else {

				tcConfig.updateTestReporter("LeadsPage",
						"validate Print and MailLetter Button enabled for PC band lead ", Status.FAIL,
						"successfull");
			}
		}

	}

	// Journey NotStarted TestCases
	private By leadTimeStamp = By
			.xpath("//span[@class='uiOutputDateTime'] | //p[@title='Last Modified By']/..//lightning-formatted-text");
	private By fieldScoreBand = By.xpath("//span[text()='Qualification Status']/../..//lightning-formatted-text");

	public String verifyTimeStamp() {
		waitUntilElementVisibleBy(driver, leadTimeStamp, 120);
		if (driver.findElement(leadTimeStamp).getText().length() > 0) {
			tcConfig.updateTestReporter("LeadsPage", "verifyTimeStamp", Status.PASS,
					"Time stamp is visible after creating the lead");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "verifyTimeStamp", Status.FAIL,
					"Time stamp is not visible after creating the lead");
		}
		return driver.findElement(leadTimeStamp).getText();
	}

	public void verifyScoreBand() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, fieldScoreBand, 120);
		if (driver.findElement(fieldScoreBand).getText().length() > 0) {
			tcConfig.updateTestReporter("LeadsPage", "verifyScoreBand", Status.PASS,
					"Score band is visible after sofscoring");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "verifyScoreBand", Status.FAIL,
					"Score band is not visible after sofscoring");
		}
	}

	public void verifyTimeStampAfterScoring(String time) {
		scrollUpByPixel(600);
		waitUntilElementVisibleBy(driver, leadTimeStamp, 120);
		if (!driver.findElement(leadTimeStamp).getText().equals(time)) {
			tcConfig.updateTestReporter("LeadsPage", "verifyTimeStamp", Status.PASS,
					"Updated Time stamp is visible after softscoring");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "verifyTimeStamp", Status.FAIL,
					"Updated Time stamp is not visible after creating softscoring");
		}
	}

	public void validateNoShowCancelledReservation() {
		/*
		 * String data[]=testData.get("ReservationNumber").split(",");
		 * 
		 * String cancel=data[0]; String noshow=data[1];
		 * System.out.println(noshow+" "+cancel);
		 */
		String cancel = testData.get("ReservationNumber");
		waitUntilElementVisibleBy(driver, globalsrch, 120);
		if (verifyObjectDisplayed(globalsrch)) {
			// tcConfig.updateTestReporter("OwnersPage",
			// "selectOwnerCreateTour", Status.PASS,
			// "User is navigated to Owners Page");
			fieldDataEnter(globalsrch, cancel);
			// fieldDataEnter(globalsrch, testData.get("ReservationNumber"));
			// driver.findElement(globalsrch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(By.xpath("//div//div[contains(text(),'Reservation')  and @dir='ltr']")).getText()
					.contains("Reservation")) {
				WebElement seacrhResOwnerLink = driver.findElement(By.xpath("//a//div//span[@title='" + cancel + "']"));
				seacrhResOwnerLink.click();
				// waitUntilElementVisibleBy(driver, createNewOwnerLink, 120);
				// clickElementBy(createNewOwnerLink);

				waitUntilElementVisibleBy(driver, reservationStatus, 120);
				String s = getTextFromBodyBy(reservationStatus);
				if (s.equalsIgnoreCase("Cancelled")) {
					tcConfig.updateTestReporter("OwnersPage", "validate the reservation is Cancelled", Status.PASS,
							"Status matched");

				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate the reservation is Cancelled", Status.FAIL,
							"Status does not matched");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bookTourBtn, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement book = driver.findElement(bookTourBtn);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", book);
				waitUntilElementVisibleBy(driver, errorCheckOut, 120);
				String s1 = getTextFromBodyBy(errorCheckOut);
				if (s1.equalsIgnoreCase("Tour cannot be created as Guest reservation is already Cancelled/No Show.")) {
					tcConfig.updateTestReporter("OwnersPage", "validate the Cancelled reservation cannot be book tour",
							Status.PASS, "Error message validated");

				} else {
					tcConfig.updateTestReporter("OwnersPage", "validate the Cancelled reservation cannot be book tour",
							Status.FAIL, "Error message can not be validated");

				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, closeButton, 120);
				// clickElementBy(closeButton);
				clickElementJSWithWait(closeButton);

			} else {
				tcConfig.updateTestReporter("OwnersPage", "validate the Cancelled reservation cannot be book tour",
						Status.FAIL, "Error message can not be validated");
			}
		}

	}

	public void validateMailLetterforNQLead() {
		waitUntilElementVisibleBy(driver, newLeads, 120);
		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "validateMailLetterforNQLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);
			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver.findElement(By.xpath("//a[@title='Mr.']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String First = randomAlphaNumeric(5);
			String Last = randomAlphaNumeric(5);
			fieldDataEnter(firstName, First);
			fieldDataEnter(lastName, Last);
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			Lead = First + " " + Last;
			fieldDataEnter(leadName, Lead);
			fieldDataEnter(streetEnter, testData.get("StreetName"));
			fieldDataEnter(cityEnter, testData.get("City"));
			fieldDataEnter(zipEnter, testData.get("ZIP"));

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> addList = driver.findElements(stateEnter);
			addList.get(addList.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement stateSelect = driver.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
			stateSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(saveButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, incentiveEligible, 120);
			if (verifyObjectDisplayed(incentiveEligible)) {
				clickElementJSWithWait(incentiveEligible);
				tcConfig.updateTestReporter("LeadsPage", "validateMailLetterforNQLead", Status.PASS,
						"User is able to to click incentiveEligible");
			} else {
				tcConfig.updateTestReporter("LeadsPage", "validateMailLetterforNQLead", Status.FAIL,
						"User is not able to to click incentiveEligible");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, requestSoftScore, 120);
			clickElementJSWithWait(requestSoftScore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, qscore, 120);
			String score = driver.findElement(qscore).getText();
			System.out.println(score);
			if (driver.findElement(qscore).isDisplayed()) {
				tcConfig.updateTestReporter("LeadsPage", "validateMailLetterforNQLead", Status.PASS,
						"User is able to to Generate Qualification Score::" + score);
			} else {
				tcConfig.updateTestReporter("LeadsPage", "validateMailLetterforNQLead", Status.FAIL,
						"User is Not able to to Generate Qualification Score");
			}

			waitUntilElementVisibleBy(driver, printButton, 120);

			if (!driver.findElement(printButton).isEnabled()) {
				tcConfig.updateTestReporter("LeadsPage", "validateMailLetterforNQLead",
						Status.PASS, "Print button is disabled for PC or NQ Score Lead");

			} else {

				tcConfig.updateTestReporter("LeadsPage","validateMailLetterforNQLead", Status.FAIL,"Print button is not disabled for PC or NQ Score Lead");
			}

			waitUntilElementVisibleBy(driver, mailLetterButton, 120);
			if (!driver.findElement(mailLetterButton).isEnabled()) {
				tcConfig.updateTestReporter("LeadsPage", "validateMailLetterforNQLead",
						Status.PASS, "Mail Button is disabled for PC or NQ Score Lead");
			} else {

				tcConfig.updateTestReporter("LeadsPage","validateMailLetterforNQLead", Status.FAIL,"Mail Button is not disabled for PC or NQ Score Lead");
			}
		}
	}

	public By lnkAdditonalGuest = By.xpath("//span[@title='Additional Guests']");
	public By tblAdditionalGuest = By.xpath("(//tr//th//span//a)");
	public By lblAG = By.xpath("//span[@title='Additional Guests Name']");
	public By btnCreateLead = By.xpath("//button[contains(text(),'Create Lead')]");
	public By txtFirstNameAG = By.xpath("//input[@name='TMFirstName__c']");
	public By txtLastNameAG = By.xpath("//input[@name='TMLastName__c']");
	public By txtEmailAG = By.xpath("//input[@name='TMEmail__c']");
	// public By txtEmailAG = By.xpath("//input[@type='email']");
	public By txtEditAg = By.xpath("//h2[contains(text(),'Edit Additional Guest')]");
	public By btnSaveAG = By.xpath("//button[@title='Save']");
	public By oldFirstName = By.xpath("(//span[text()='First Name'])[2]/../../div//span//lightning-formatted-text");
	public By oldLastName = By.xpath("(//span[text()='Last Name'])[2]/../../div//span//lightning-formatted-text");
	public By oldEmail = By.xpath("(//span[text()='Email']/parent::div/following-sibling::div/span//slot//a)[2]");
	public By newFN = By.xpath("(//span[text()='First Name'])[2]/../../div//span//lightning-formatted-text");
	public By newLN = By.xpath("(//span[text()='Last Name'])[2]/../../div//span//lightning-formatted-text");
	public By newEmail = By.xpath("(//span[text()='Email']/parent::div/following-sibling::div/span//slot//a)[2]");
	public By editFN = By.xpath("(//button[@title='Edit First Name']//span)[1]");
	public By txtPhoneLnk = By.xpath("//input[@name='TMPhone__c']");
	public By popup = By.xpath("//button[@title='OK']");
	public By Email = By.xpath("//input[@name='TMEmail__c']");
	public By saleforceoldEmail = By.xpath("//span[text()='Email']/parent::div/following-sibling::div/span//slot//a");
	// protected By updatedPhone =
	// By.xpath("//span[contains(.,'Phone')]/../..//div//a");
	protected By updatedPhone = By
			.xpath("(//span[contains(.,'Phone')]/../..//div/span/slot/slot/lightning-formatted-phone)[2]");
	public By saleforceoldPhone = By
			.xpath("//span[contains(.,'Phone')]/../..//div/span/slot/slot/lightning-formatted-phone");
	private By btnBookTourNative = By.xpath("//button[contains(text(),'Book Tour')]");

	public void validatePhone_Email_change_owner_Reservation() throws AWTException {
		try {
			waitUntilElementVisibleBy(driver, btnBookTourNative, 120);
			clickElementJSWithWait(lnkAdditonalGuest);
			waitUntilElementVisibleBy(driver, lblAG, 120);
			List<WebElement> button = driver.findElements(tblAdditionalGuest);
			System.out.println("Number of elements:" + button.size());
			button.get(button.size() - 1).click();
			waitUntilElementVisibleBy(driver, oldFirstName, 120);
			String First = getTextFromBodyBy(oldFirstName);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Last = getTextFromBodyBy(oldLastName);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Email = getTextFromBodyBy(oldEmail);
			waitUntilElementVisibleBy(driver, editFN, 120);
			String phn = getTextFromBodyBy(updatedPhone);
			tcConfig.updateTestReporter("ReservationPage", "validatePhone_Email_change_owner_Reservation",
					Status.PASS, "First Name present :- " + First + "and Last name present :- " + Last
							+ "and Email present :-" + Email + "and Phone number present :-" + phn);
			clickElementJSWithWait(editFN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtFirstNameAG, 120);
			driver.findElement(txtFirstNameAG).click();
			driver.findElement(txtFirstNameAG).clear();
			String FirstNm = randomAlphaNumeric(3);
			fieldDataEnter(txtFirstNameAG, FirstNm);
			waitUntilElementVisibleBy(driver, txtLastNameAG, 120);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtLastNameAG).click();
			driver.findElement(txtLastNameAG).clear();
			String LastNm = randomAlphaNumeric(4);
			fieldDataEnter(txtLastNameAG, LastNm);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtPhoneLnk, 120);
			driver.findElement(txtPhoneLnk).click();
			driver.findElement(txtPhoneLnk).clear();
			String phone = getRandomString(10);
			fieldDataEnter(txtPhoneLnk, phone);
			driver.findElement(txtPhoneLnk).sendKeys(Keys.TAB);
			driver.findElement(txtPhoneLnk).sendKeys(Keys.TAB);
			driver.findElement(txtPhoneLnk).sendKeys(Keys.TAB);
			waitUntilElementVisibleBy(driver, txtEmailAG, 120);
			driver.findElement(txtEmailAG).clear();
			driver.findElement(txtEmailAG).sendKeys(getRandomStringEmail());
			// fieldDataEnter(txtEmailAG,getRandomStringEmail());
			// driver.findElement(txtEmailAG).sendKeys(Keys.ENTER);

			driver.findElement(txtEmailAG).sendKeys(Keys.TAB);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnSaveAG, 120);
			clickElementJSWithWait(btnSaveAG);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String FN = getTextFromBodyBy(newFN);
			String LN = getTextFromBodyBy(newLN);
			String EmailUpdated = getTextFromBodyBy(newEmail);
			String phnUpdated = getTextFromBodyBy(updatedPhone);
			if (!First.equals(FN) && (!Last.equals(LN)) && (!Email.equals(EmailUpdated))
					&& (!phone.equals(phnUpdated))) {
				tcConfig.updateTestReporter("ReservationPage", "validatePhone_Email_change_owner_Reservation",
						Status.PASS,
						"First Name updated to:" + FN + "and Last name updtaed to:- " + LN + "and Email updtaed to:-"
								+ EmailUpdated + "and Phone number updated to :-" + phnUpdated);
			} else {
				tcConfig.updateTestReporter("ReservationPage", "validatePhone_Email_change_owner_Reservation",
						Status.FAIL, "Failed while updating First Name");
			}
			/*
			 * else if (!Last.equals(LN)) {
			 * tcConfig.updateTestReporter("ReservationPage",
			 * "editNonOwnerReservation", Status.PASS,
			 * "First Name updated to:"+LN); } else{
			 * tcConfig.updateTestReporter("ReservationPage",
			 * "editNonOwnerReservation", Status.FAIL,
			 * "Failed while updating First Name"); } else if
			 * (!Email.equals(EmailUpdated)) {
			 * tcConfig.updateTestReporter("ReservationPage",
			 * "editNonOwnerReservation", Status.PASS,
			 * "First Name updated to:"+EmailUpdated); } else{
			 * tcConfig.updateTestReporter("ReservationPage",
			 * "editNonOwnerReservation", Status.FAIL,
			 * "Failed while updating  Email"); }
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("ReservationPage", "validatePhone_Email_change_owner_Reservation",
					Status.FAIL, "Failed while updating  ");
		}

	}

	private By imgAddGuest = By.xpath("//img[contains(@src,'Add_Guest')]");
	private By txtFirstName = By
			.xpath("//div[contains(text(),'First Name')]/following::div[2]//input[@name='text-input']");
	private By txtLastName = By
			.xpath("//div[contains(text(),'Last Name')]/following::div[2]//input[@name='text-input']");
	private By txtAddress = By
			.xpath("//label[contains(text(),'Address')]/following::div[1]//textarea[@class='slds-textarea']");
	private By btnCreateGuest = By.xpath("//button[contains(text(),'CREATE GUEST')]");
	private By drpState = By
			.xpath("(//select[@data-aura-class='uiInput uiInputSelect uiInput--default uiInput--select'])[2]");
	private By txtCity = By.xpath("//div[contains(text(),'City')]/following::div[2]//input[@name='text-input']");
	private By txtZip = By.xpath("//div[contains(text(),'Zip')]/following::div[2]//input[@name='text-input']");
	private By btnNext = By.xpath("//button[contains(text(),'Next')]");
	private By btnIncentiveElligible = By.xpath("//button[contains(text(),'Incentive Eligible')]");
	private By btnReqSoftScore = By.xpath("//button[contains(text(),'Request Soft Score')]");
	private By imgIcon = By.xpath("//span[contains(text(),'You are eligible for promotions in your area')]");
	private By btnBookTour = By.xpath("//button[contains(text(),'BOOK TOUR')]");
	protected By fieldMobile = By.xpath("//label[text()='Mobile #']/..//input");

	public void validateCreateAddGuest() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, imgAddGuest, 120);
		driver.findElement(imgAddGuest).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		String firstName = getRandomStringText(4);
		driver.findElement(txtFirstName).sendKeys(firstName);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String lastName = getRandomStringText(4);
		driver.findElement(txtLastName).sendKeys(lastName);
		lead1 = firstName + " " + lastName;
		fieldDataEnter(fieldMobile, testData.get("MobileNo"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String address = getRandomStringText(5);
		driver.findElement(txtAddress).sendKeys(address);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		Select selectState = new Select(driver.findElement(drpState));
		selectState.selectByVisibleText(testData.get("State"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		String city = getRandomStringText(5);
		driver.findElement(txtCity).sendKeys(city);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		String zip = getRandomString(5);
		driver.findElement(txtZip).sendKeys(zip);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJSWithWait(btnCreateGuest);

		// clickElementBy(btnCreateGuest);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnNext, 120);
		clickElementJSWithWait(btnNext);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// clickElementBy(btnNext);
		/*
		 * waitUntilElementVisibleBy(driver, btnIncentiveElligible, 120);
		 * clickElementJSWithWait(btnIncentiveElligible);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * clickElementJSWithWait(btnReqSoftScore);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 */
		waitUntilElementVisibleBy(driver, btnBookTour, 120);
		clickElementJSWithWait(btnBookTour);

	}

	// protected By CancelButton=By.xpath("//button[@title='cancel tour']");
	protected By associatedPayments = By.xpath("//h2[contains(.,'Associated Payments')]");
	protected By paymentId = By.xpath("//table[@class='wn-premium_table slds-table slds-table_cell-buffer']//td//a");
	protected By transactStatus = By.xpath("//table[contains(@class,'wn-premium_table slds-table')]//td[2]");

	public void clickOnCreatedTour(String Lead) {
		try {
			Select selectTours = new Select(driver.findElement(drpTourHome));
			selectTours.selectByVisibleText("Site Tours Today");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// js.executeScript("window.scrollBy(0,500)");
			WebElement element = driver.findElement(By.xpath("//a[contains(text(),'" + LeadsPage.lead1 + "')]"));
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			System.out.println(element.getText());
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			System.out.println(lead1);
			driver.findElement(By.xpath("//a[contains(text(),'" + LeadsPage.lead1 + "')]")).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilObjectVisible(driver, tabDetails, 120);
			clickElementBy(tabTours);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lnkTourID, 120);
			clickElementJSWithWait(lnkTourID);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, CancelButton, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement text = driver.findElement(associatedPayments);
			js.executeScript("arguments[0].scrollIntoView(true);", text);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(paymentId)
					&& driver.findElement(transactStatus).getAttribute("outerText").contains("Offline Payment")) {
				String paymentID = driver.findElement(paymentId).getText();
				String tansactID = driver.findElement(transactStatus).getText();
				tcConfig.updateTestReporter("BookATourPage", "clickOnCreatedTour", Status.PASS,
						"Tour Page displayed sucessfully and Payment is :- " + paymentID
								+ " and transaction status is :- " + tansactID);
			} else {
				tcConfig.updateTestReporter("BookATourPage", "clickOnCreatedTour", Status.FAIL,
						"Failed while loading Tour page");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails", Status.FAIL, "Getting error:" + e);
			throw e;
		}

	}

	private By drpTourHome = By.xpath("//select[@class='slds-select']");
	private By tblTours = By.xpath("//td[@data-label='name']//a[@class='table_col_name']");
	private By tabDetails = By.xpath("//a[contains(text(),'Details')]");
	private By tabTours = By.xpath("//a[contains(text(),'Tours (1)')]");
	private By lnkTourID = By.xpath("//span[contains(text(),'Tour Number')]/following::div[1]/a");
	private By btncancelTour = By.xpath("//button[@title='cancel tour']");
	public By fromTxtBx = By.xpath("(//label[contains(text(),'Date')]/following::div[1]//input[@name='input1'])[1]");
	public By btnSaveNext = By.xpath("(//button[contains(text(),'Save & Exit')])[1]");
	public By btnYes = By.xpath("//button[contains(text(),'YES')]");
	public By btnAvialbleSlot = By.xpath(
			"(//section[@class='main-content time-btn-active1']//div[@class='available-time-slot']//button[@class='time-btn-active'])");
	public By btnReschAvailableSlot = By
			.xpath("((//div[@class='available-time-slot'])[1]//span[@class='button-content-label'])[1]");
	public By lblAvialableSlot = By.xpath("(//div[contains(text(),'Available Time')])[1]");
	public By CancelButton = By.xpath("//button[@title='cancel tour']");
	public By YesCancel = By.xpath("//button[text()='YES']");
	public By notour = By.xpath("//div[contains(text(),'No tour')]");
	private By cancelSyccessMas = By.xpath("//span[text()='Tour Cancelled Successfully']");
	protected By homeLink = (By.xpath("(//img[@class='wn-left_custom_icon'])[1]"));
	protected By imageSearch=By.xpath("//a/img[contains(@src,'Search')]");
	protected By textSearch=By.xpath("//input[@type='search']");
	protected By buttonViewDetails=By.xpath("//button[text()='VIEW DETAILS']");

	public void validateClickTour() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, imageSearch, 120);
		clickElementJSWithWait(imageSearch);
		waitUntilElementVisibleBy(driver, textSearch, 120);
		fieldDataEnter(textSearch, LeadsPage.lead1);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		tcConfig.updateTestReporter("LeadsPage", "validateClickTour", Status.PASS, "Searched with Guest: "+LeadsPage.lead1);
		waitUntilElementVisibleBy(driver, buttonViewDetails, 120);
		clickElementJSWithWait(buttonViewDetails);
		waitUntilObjectVisible(driver, tabTours, 120);
		clickElementJSWithWait(tabTours);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lnkTourID, 120);
		clickElementJSWithWait(lnkTourID);
		tcConfig.updateTestReporter("LeadsPage", "validateClickTour", Status.PASS,"Tour Page displayed sucessfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, CancelButton, 120);
		clickElementJSWithWait(CancelButton);
		waitUntilElementVisibleBy(driver, YesCancel, 120);
		clickElementJSWithWait(YesCancel);
		waitUntilElementVisibleBy(driver, cancelSyccessMas, 120);
		if (verifyObjectDisplayed(cancelSyccessMas)) {
			String sucessmsg = driver.findElement(cancelSyccessMas).getText();
			System.out.println(sucessmsg);
			tcConfig.updateTestReporter("LeadsPage", "validateClickTour", Status.PASS,
					"Cancel Message validated successfully");

		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateClickTour", Status.FAIL,
					"Failed to validate Cancel Message");

		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, homeLink, 120);
		if (verifyObjectDisplayed(homeLink)) {
			clickElementJSWithWait(homeLink);
			tcConfig.updateTestReporter("LeadsPage", "validateClickTour", Status.PASS, "Successfully Navigated to Home Page");

		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateClickTour", Status.FAIL, "Failed to navigate to Home Page");
		}
	}

	private By salesStore = By.xpath("//table[contains(@class,'tours-table slds-table slds-table')]//tr//td[3]");
	private By tourrecords = By.xpath("//table[contains(@class,'tours-table slds-table slds-table')]//tr//td[1]");

	public void VerifyBookTourSalesstortTodayDate() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> allparentsallesStore = driver.findElements(salesStore);
		List<WebElement> alltourRecord = driver.findElements(tourrecords);
		if (allparentsallesStore.size() > 0) {
			for (int salestore = 0; salestore <= allparentsallesStore.size() - 1; salestore++) {
				String salesStore = allparentsallesStore.get(salestore).getText();
				tcConfig.updateTestReporter("LeadsPage", "VerifyBookTourSalesstorCurrnrtDate", Status.PASS,
						"All salesstore should be display::" + salesStore);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "VerifyBookTourSalesstorCurrnrtDate", Status.FAIL,
					"salesstore should not be display::");
		}
		if (alltourRecord.size() > 0) {
			for (int tourRecord = 0; tourRecord <= allparentsallesStore.size() - 1; tourRecord++) {
				String tourRecords = alltourRecord.get(tourRecord).getText();
				tcConfig.updateTestReporter("LeadsPage", "VerifyBookTourSalesstorCurrnrtDate", Status.PASS,
						"All tour records should be display::" + tourRecords);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "VerifyBookTourSalesstorCurrnrtDate", Status.FAIL,
					"Tour records failed::");
		}
		if (alltourRecord.size() == allparentsallesStore.size()) {
			tcConfig.updateTestReporter("LeadsPage", "VerifyBookTourSalesstorCurrnrtDate", Status.PASS,
					"All salesstore and tour records should be display in system by today date::" + alltourRecord.size()
							+ "Sales store are present" + allparentsallesStore.size());
		} else {
			tcConfig.updateTestReporter("LeadsPage", "VerifyBookTourSalesstorCurrnrtDate", Status.FAIL,
					"System should not display tours record and sales Store");
		}
	}

	private By tomorrowNorecordfound = By.xpath("//td[text()='No Records Found.']");

	public void validateTourConfirmDropdown() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, drpTourHome, 120);
		if (verifyObjectDisplayed(drpTourHome)) {
			tcConfig.updateTestReporter("LeadsPage", "validateTourConfirmDropdown", Status.PASS,
					"User should be see Tour confirm drop down::" + salesStore);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Select selectTours = new Select(driver.findElement(drpTourHome));
			selectTours.selectByIndex(0);
			/*
			 * JavascriptExecutor js = (JavascriptExecutor) driver;
			 * js.executeScript("window.scrollBy(0,500)");
			 */
			List<WebElement> allparentsallesStore = driver.findElements(salesStore);
			List<WebElement> alltourRecord = driver.findElements(tourrecords);
			if (allparentsallesStore.size() > 0) {
				for (int salestore = 0; salestore <= allparentsallesStore.size() - 1; salestore++) {
					String salesStore = allparentsallesStore.get(salestore).getText();
					tcConfig.updateTestReporter("LeadsPage", "validateTourConfirmDropdown", Status.PASS,
							"All Site Tours today salesStore are Present::" + salesStore);
				}
			} else {
				tcConfig.updateTestReporter("LeadsPage", "validateTourConfirmDropdown", Status.FAIL,
						"salesstore should not be display::");
			}
			if (alltourRecord.size() > 0) {
				for (int tourRecord = 0; tourRecord <= allparentsallesStore.size() - 1; tourRecord++) {
					String tourRecords = alltourRecord.get(tourRecord).getText();
					tcConfig.updateTestReporter("LeadsPage", "validateTourConfirmDropdown", Status.PASS,
							"All Site Tours today Records are Populating::" + tourRecords);
				}
			} else {
				tcConfig.updateTestReporter("LeadsPage", "validateTourConfirmDropdown", Status.FAIL,
						"Tour records failed::");
			}
			if (alltourRecord.size() == allparentsallesStore.size()) {
				tcConfig.updateTestReporter("LeadsPage", "validateTourConfirmDropdown", Status.PASS,
						"All Site tours today records are POPULATING::" + alltourRecord.size()
								+ "Sales store are present" + allparentsallesStore.size());
			} else {
				tcConfig.updateTestReporter("LeadsPage", "validateTourConfirmDropdown", Status.FAIL,
						"System should not display tours record and sales Store");
			}

		}

	}

	public void selectSiteToursTomorrowAndVerifyTourRecord() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Select selectTours = new Select(driver.findElement(drpTourHome));
		selectTours.selectByIndex(1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(tomorrowNorecordfound)) {
			String record = driver.findElement(tomorrowNorecordfound).getText();
			tcConfig.updateTestReporter("LeadsPage", "selectSiteToursTomorrowAndVerifyTourRecord", Status.PASS,
					"select tomorrow record not found::" + record);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectSiteToursTomorrowAndVerifyTourRecord", Status.FAIL,
					"record failed");
		}

	}

	public void selectSiteToursThisWeekAndVerifyTourRecord() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Select selectTours = new Select(driver.findElement(drpTourHome));
		selectTours.selectByIndex(2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> allparentsallesStore = driver.findElements(salesStore);
		List<WebElement> alltourRecord = driver.findElements(tourrecords);
		if (allparentsallesStore.size() > 0) {
			for (int salestore = 0; salestore <= allparentsallesStore.size() - 1; salestore++) {
				String salesStore = allparentsallesStore.get(salestore).getText();
				tcConfig.updateTestReporter("LeadsPage", "selectSiteToursThisWeekAndVerifyTourRecord", Status.PASS,
						"All Site ToursThis Week salesStore are Present::" + salesStore);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectSiteToursThisWeekAndVerifyTourRecord", Status.FAIL,
					"salesstore should not be display::");
		}
		if (alltourRecord.size() > 0) {
			for (int tourRecord = 0; tourRecord <= allparentsallesStore.size() - 1; tourRecord++) {
				String tourRecords = alltourRecord.get(tourRecord).getText();
				tcConfig.updateTestReporter("LeadsPage", "selectSiteToursThisWeekAndVerifyTourRecord", Status.PASS,
						"All  Site ToursThis Week Tour record are Populating:: " + tourRecords);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectSiteToursThisWeekAndVerifyTourRecord", Status.FAIL,
					"Tour records failed::");
		}
		if (alltourRecord.size() == allparentsallesStore.size()) {
			tcConfig.updateTestReporter("LeadsPage", "selectSiteToursThisWeekAndVerifyTourRecord", Status.PASS,
					"All  Site ToursThis Week records are POPULATING::" + alltourRecord.size()
							+ "Sales store are present" + allparentsallesStore.size());
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectSiteToursThisWeekAndVerifyTourRecord", Status.FAIL,
					"System should not display tours record and sales Store");
		}
	}

	public void selectMyTourTodayAndVerifyTourRecord() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Select selectTours = new Select(driver.findElement(drpTourHome));
		selectTours.selectByIndex(3);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> allparentsallesStore = driver.findElements(salesStore);
		List<WebElement> alltourRecord = driver.findElements(tourrecords);
		if (allparentsallesStore.size() > 0) {
			for (int salestore = 0; salestore <= allparentsallesStore.size() - 1; salestore++) {
				String salesStore = allparentsallesStore.get(salestore).getText();
				tcConfig.updateTestReporter("LeadsPage", "selectMyTourTodayAndVerifyTourRecord", Status.PASS,
						"All My Tour Today salesStore are Present::" + salesStore);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourTodayAndVerifyTourRecord", Status.FAIL,
					"salesstore should not be display::");
		}
		if (alltourRecord.size() > 0) {
			for (int tourRecord = 0; tourRecord <= allparentsallesStore.size() - 1; tourRecord++) {
				String tourRecords = alltourRecord.get(tourRecord).getText();
				tcConfig.updateTestReporter("LeadsPage", "selectMyTourTodayAndVerifyTourRecord", Status.PASS,
						"All My Tour Today Tour record are Populating::" + tourRecords);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourTodayAndVerifyTourRecord", Status.FAIL,
					"Tour records failed::");
		}
		if (alltourRecord.size() == allparentsallesStore.size()) {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourTodayAndVerifyTourRecord", Status.PASS,
					"All  My Tour Today tour records are POPULATING::" + alltourRecord.size()
							+ "Sales store are present" + allparentsallesStore.size());
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourTodayAndVerifyTourRecord", Status.FAIL,
					"System should not display tours record and sales Store");
		}
	}

	public void selectMyToursTomorrowAndVerifyTourRecord() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Select selectTours = new Select(driver.findElement(drpTourHome));
		selectTours.selectByIndex(4);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(tomorrowNorecordfound)) {
			String record = driver.findElement(tomorrowNorecordfound).getText();
			tcConfig.updateTestReporter("LeadsPage", "selectMyToursTomorrowAndVerifyTourRecord", Status.PASS,
					"select tomorrow record not found::" + record);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectMyToursTomorrowAndVerifyTourRecord", Status.FAIL,
					"record failed");
		}

	}

	public void selectMyTourThisWeekAndVerifyTourRecord() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Select selectTours = new Select(driver.findElement(drpTourHome));
		selectTours.selectByIndex(5);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> allparentsallesStore = driver.findElements(salesStore);
		List<WebElement> alltourRecord = driver.findElements(tourrecords);
		if (allparentsallesStore.size() > 0) {
			for (int salestore = 0; salestore <= allparentsallesStore.size() - 1; salestore++) {
				String salesStore = allparentsallesStore.get(salestore).getText();
				tcConfig.updateTestReporter("LeadsPage", "selectMyTourThisWeekAndVerifyTourRecord", Status.PASS,
						"All My Tour This week salesStore are Present::" + salesStore);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourThisWeekAndVerifyTourRecord", Status.FAIL,
					"salesstore should not be display::");
		}
		if (alltourRecord.size() > 0) {
			for (int tourRecord = 0; tourRecord <= allparentsallesStore.size() - 1; tourRecord++) {
				String tourRecords = alltourRecord.get(tourRecord).getText();
				tcConfig.updateTestReporter("LeadsPage", "selectMyTourThisWeekAndVerifyTourRecord", Status.PASS,
						"All My Tour This week Tour record are Populating:: " + tourRecords);
			}
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourThisWeekAndVerifyTourRecord", Status.FAIL,
					"Tour records failed::");
		}
		if (alltourRecord.size() == allparentsallesStore.size()) {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourThisWeekAndVerifyTourRecord", Status.PASS,
					"All  My Tour This week tour records are POPULATING:: " + alltourRecord.size()
							+ "sales Store are present" + allparentsallesStore.size());
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectMyTourThisWeekAndVerifyTourRecord", Status.FAIL,
					"System should not display tours record and sales Store");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void validateBookTourWithSameLead() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, drpTourHome, 120);
		Select selectTours = new Select(driver.findElement(drpTourHome));
		selectTours.selectByVisibleText("Site Tours Today");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		System.out.println(lead1);
		driver.findElement(By.xpath("//a[contains(text(),'" + LeadsPage.lead1 + "')]")).click();
		/* clickElementJSWithWait(lnkName); */
		// System.out.println("Element is: "+lnkName);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilObjectVisible(driver, tabTours, 120);
		clickElementJSWithWait(tabTours);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// getCurrentDate =
		// driver.findElement(By.xpath("//span[contains(text(),'Start
		// Time')]/following::div[2]//span[@data-type='STRING']")).getText();
		waitUntilElementVisibleBy(driver, lnkTourID, 120);
		if (verifyObjectDisplayed(lnkTourID)) {
			clickElementJSWithWait(lnkTourID);
			// waitUntilObjectVisible(driver, btncancelTour, 120);
			tcConfig.updateTestReporter("LeadsPage", "validateAppointmentChange", Status.PASS,
					"Tour Page displayed sucessfully And Book tour with search same Lead");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateAppointmentChange", Status.PASS,
					" Book tour with search same Lead Unsuccessfully");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, homeLink, 120);
		if (verifyObjectDisplayed(homeLink)) {
			clickElementJSWithWait(homeLink);
			tcConfig.updateTestReporter("LeadsPage", "validate home link tab", Status.PASS, "able to click");

		} else {
			tcConfig.updateTestReporter("LeadsPage", "validate home link tab", Status.FAIL, " not able to click");
		}

	}

	public void verifyIncreaseTourRecords() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> afterCreateTourallsallesStore = driver.findElements(salesStore);
		List<WebElement> afterCreateTouralltourRecord = driver.findElements(tourrecords);

		if (afterCreateTouralltourRecord.size() == afterCreateTourallsallesStore.size()) {
			tcConfig.updateTestReporter("LeadsPage", "verifyIncreaseTourRecords", Status.PASS,
					"After Tour Created Tour Record should be Increase :" + afterCreateTouralltourRecord.size());
		} else {
			tcConfig.updateTestReporter("LeadsPage", "verifyIncreaseTourRecords", Status.FAIL,
					"Tour Record got failed");
		}
	}

	public void validateMailLetterForQLead() {
		waitUntilElementVisibleBy(driver, qscore, 120);
		String score = driver.findElement(qscore).getText();;
		if (verifyObjectDisplayed(qscore)) {
			tcConfig.updateTestReporter("LeadsPage", "validateMailLetterForQLead", Status.PASS, "User Band is::" + score);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateMailLetterForQLead", Status.FAIL, "User is not  Q band Lead");
		}

		waitUntilElementVisibleBy(driver, printButton, 120);

		String parentWindow = driver.getWindowHandle();
		clickElementJSWithWait(printButton);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Set<String> s1 = driver.getWindowHandles();
		System.out.println(s1.size());
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String childwindow = i1.next();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (!parentWindow.equals(childwindow)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(childwindow);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, tmcletter, 120);
				if (driver.findElement(tmcletter).isDisplayed()) {
					tcConfig.updateTestReporter("LeadsPage", "validateMailLetterForQLead",
							Status.PASS, "Print Button is Enabled and successfully Clicked");
					driver.close();
					break;
				} else {
					tcConfig.updateTestReporter("LeadsPage", "validateMailLetterForQLead",
							Status.FAIL, "Failed to click Print Button");
				}
			}
		}

		driver.switchTo().window(parentWindow);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, mailLetterButton, 120);
		if ((driver.findElement(mailLetterButton)).isEnabled()) {
			System.out.println(driver.findElement(mailLetterButton).isEnabled());
			tcConfig.updateTestReporter("LeadsPage", "validateMailLetterForQLead",
					Status.PASS, "Mail letter Button is enable");
		} else {

			tcConfig.updateTestReporter("LeadsPage", "validateMailLetterForQLead",
					Status.FAIL, "Mail letter button is not enabled");

		}
	}

	public void getlatestTour1() {

		waitUntilElementVisibleBy(driver, latestTour, 120);
		if (verifyObjectDisplayed(latestTour)) {
			createdTourID1 = driver.findElement(latestTour).getText();
			driver.findElement(latestTour).click();
			System.out.println("created TourID: " + createdTourID1);
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS,
					"created TourID: " + createdTourID1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
		}

	}

	public void clickonRelatedTab() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		relatedTab.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonTourId() throws AWTException {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Robot rob = new Robot();
		rob.mouseWheel(3);
		clickElementJSWithWait(tourIdLink);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Search Lead in Lead Page Designed By-JYoti
	 */
	public void searchLead_Native() {
		waitUntilElementVisibleBy(driver, txtLeadSearch, 120);
		tcConfig.updateTestReporter("LeadsPage", "searchLead_Native", Status.PASS,
				"User is navigated to Leads Page");
		fieldDataEnter(txtLeadSearch, testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement wbseacrhResOwnerLink = driver.findElement(imgResrv);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		wbseacrhResOwnerLink.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("LeadsPage", "searchLead_Native", Status.PASS, "Leads is displayed");
	}

	/* <Sprint Methods> */
	/*
	 * Method-validate Score and disable the record Designed By-JYoti
	 */

	public void scoreAndDisable() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, btnCreateTourRecord, 120);

		clickElementJS(btnincentive);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, softscore, 120);
		clickElementJS(softscore);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilObjectVisible(driver, qualificationScore, 120);
		String strScore = driver.findElement(qualificationScore).getText();
		if (strScore.equalsIgnoreCase(testData.get("QualificationScore"))) {
			tcConfig.updateTestReporter("LeadsPage", "scoreAndDisable", Status.PASS,
					"Qualification Score is: " + strScore);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "scoreAndDisable", Status.FAIL,
					"Failed : Qualification doesnot match with Rule score");
		}
		waitUntilElementVisibleBy(driver, btnCreateTourRecord, 120);
		if (driver.findElement(btnIncentiveEligible).getAttribute("disabled").contains("true")) {
			tcConfig.updateTestReporter("LeadPage", "scoreAndDisable", Status.PASS,
					"Incentive Eligible button is disabled");
		} else {
			tcConfig.updateTestReporter("LeadPage", "scoreAndDisable", Status.FAIL,
					"Incentive Eligible button is not disabled");
		}
	}

	/*
	 * Method-Verify Hard Stop message in LeadPage Desgined By-Jyoti
	 */
	public void bookTourForHardStop() {

		waitUntilElementVisibleBy(driver, errorMsg, 120);
		String str = driver.findElement(errorMsg).getText();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("LeadPage", "bookTourForHardStop", Status.PASS,
				"User can see Error Messgae displayed as :- " + str);
		clickElementJS(bntCancel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Validate Soft stop message Designed By-JYoti
	 */
	public void bookTourSoftStop() throws Exception {

		waitUntilObjectVisible(driver, softScoreMsg, 120);
		String str = driver.findElement(softScoreMsg).getText();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("ReservationPage", "bookTourSoftStop", Status.PASS,
				"User can see Error Messgae displayed as :- " + str);
	}

	/*
	 * Method-Book Tour and validate Soft stop message Designed By-JYoti
	 */
	public void bookTourSoftStop_Mobile() throws Exception {
		waitUntilObjectVisible(driver, btnBookTourMobile, 120);
		clickElementJSWithWait(btnBookTourMobile);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, hardStop_Mobile, 120);
		String str = driver.findElement(hardStop_Mobile).getText();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("ReservationPage", "bookTourSoftStop_Mobile", Status.PASS,
				"User can see Message displayed as :- " + str);
		if (verifyObjectDisplayed(btnCancel_Mobile)) {
			clickElementJS(btnCancel_Mobile);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("LeadPage", "bookTourSoftStop_Mobile", Status.PASS,
					"User should be able to create Tour");
		} else {
			tcConfig.updateTestReporter("LeadPage", "bookTourSoftStop_Mobile", Status.FAIL,
					"User should not be able to create Tour");
		}
	}

	/*
	 * Method-Search Lead and print error Message Designed By-JYoti
	 */
	public void serachLeadBookTourMobile() {

		waitUntilElementVisibleBy(driver, searchLead, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(searchLead, testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(searchLead).sendKeys(Keys.ENTER);
		List<WebElement> lstelements = driver.findElements(lst_Lead);
		System.out.println("Number of elements:" + lstelements.size());
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnBookTourMobile);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, hardStop_Mobile, 120);
		String str = driver.findElement(hardStop_Mobile).getText();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		tcConfig.updateTestReporter("LeadPage", "serachLeadMobile", Status.PASS,
				"User can see Error Messgae displayed as :- " + str);
		clickElementJSWithWait(btnCancel_Mobile);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method-Book Tour in Lead Page Designed By-JYoti
	 */

	public void clickTourRecord() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnIncentiveEligible, 120);
		clickElementJSWithWait(btnCreateTourRecord);
		waitUntilElementVisibleBy(driver, txtCreateTourRecord, 120);
		clickElementJSWithWait(btnSave);

	}

	/*
	 * Method-validate Incentive button is disabled Designed By-JYoti
	 */
	public void incentiveEligibleDisabled() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnCreateTourRecord, 120);
		if (driver.findElement(btnIncentiveEligible).getAttribute("disabled").contains("true")) {
			tcConfig.updateTestReporter("LeadPage", "incentiveEligibleDisabled", Status.PASS,
					"Incentive Eligible button is disabled");
		} else {
			tcConfig.updateTestReporter("LeadPage", "incentiveEligibleDisabled", Status.FAIL,
					"Incentive Eligible button is not disabled");
		}
	}

	/*
	 * Method-Score Inecntive and validate the score in mobile UI Designed
	 * By-JYoti
	 */
	public void validateScoreAndQualification_Mobile() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, txtCustInfo_Mobile, 120);
		if (driver.findElement(btnIncentiveEligible).isEnabled()) {
			clickElementJS(btnIncentiveEligible);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, reqScoreMobile, 120);
			clickElementJS(reqScoreMobile);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			System.out.println("Incentive button is scored and procced");
		} else if (!driver.findElement(btnIncentiveEligible).isEnabled()) {
			tcConfig.updateTestReporter("LeadsPage", "validateScoreAndQualification_Mobile", Status.PASS,
					"Incnetive is scored");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateScoreAndQualification_Mobile", Status.FAIL,
					"Incnetive button is disabled");
		}
		waitUntilObjectVisible(driver, scoreBandMobile, 120);
		String strScore = driver.findElement(scoreBandMobile).getText();
		if (strScore.equalsIgnoreCase(testData.get("QualificationScore"))) {
			tcConfig.updateTestReporter("LeadsPage", "validateScoreAndQualification_Mobile", Status.PASS,
					"Qualification Score is: " + strScore);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "validateScoreAndQualification_Mobile", Status.FAIL,
					"Failed while getting Qualification Score");
		}
	}

	/*
	 * Method-Click on Reservation to navigate to Details page in mobile UI
	 * Designed By-JYoti
	 */
	/**
	 * 
	 */
	public void clickReservation_Mobile() {
		waitUntilObjectVisible(driver, tabReservationMobile, 120);
		clickElementJSWithWait(tabReservationMobile);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, hypReservationMobile, 120);
		List<WebElement> lstreserv = driver.findElements(hypReservationMobile);
		lstreserv.get(lstreserv.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method-Click Incentive score Designed By-JYoti
	 */
	public void clickIncentiveScore() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, btnincentive, 120);
		clickElementJS(btnincentive);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, softscore, 120);
		clickElementJS(softscore);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilObjectVisible(driver, qualificationScore, 120);
		String strScore = driver.findElement(qualificationScore).getText();
		if (strScore.equalsIgnoreCase(testData.get("QualificationScore"))) {
			tcConfig.updateTestReporter("LeadsPage", "clickIncentiveScore", Status.PASS,
					"Qualification Score is: " + strScore);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "clickIncentiveScore", Status.FAIL,
					"Failed while getting Qualification Score");
		}
	}

	/*
	 * Method-Serach Lead in Mobile UI Designed By-JYoti
	 */
	public void searchLeadInMobile() {
		waitUntilElementVisibleBy(driver, searchLead, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(searchLead, testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(searchLead).sendKeys(Keys.ENTER);
		List<WebElement> lstelements = driver.findElements(lst_Lead);
		System.out.println("Number of elements:" + lstelements.size());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(hypLead)) {
			clickElementJSWithWait(hypLead);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("LeadPage", "searchLeadInMobile", Status.PASS,
					"User navigated to Lead page");
		} else {
			tcConfig.updateTestReporter("LeadPage", "searchLeadInMobile", Status.FAIL,
					"User should not be  navigated to Lead page");
		}
	}

	/*
	 * Method-Click Owner Tab Designed By-JYoti
	 */
	public void clickOwnerTab() {
		clickElementJSWithWait(tabOwner);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method-Click Related Tab Designed By-JYoti
	 */
	public void clickOnRelatedTab() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, tabRelated, 120);
		clickElementJSWithWait(tabRelated);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,300)", "");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(tabReservation);
		waitUntilElementVisibleBy(driver, tabReservation, 120);
		clickElementJSWithWait(lnkViewAll);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtReservation, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> lstbutton = driver.findElements(tbl_Reserv);
		lstbutton.get(lstbutton.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

}
