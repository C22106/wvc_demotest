package salesforce.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class HomePage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public HomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By homeTab = By.xpath("//span[contains(.,'Home')]");
	private By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	// private By menuIcon=By.xpath("//button[@class='slds-button']/div");
	protected By menuIcon = By.xpath("//button[contains(@class,'slds-button slds-show')]");
	@FindBy(xpath = "//button[@class='slds-button slds-show']")
	WebElement menuIcons;
	protected By noItems = By.xpath("//a[@title='No Items']//span");
	protected By joruneyDesktop = By.xpath("//p[contains(.,'Journey Desktop')]");
	protected By img_SearchInMobile = By.xpath("//img[contains(@src,'Search.svg')]");

	/******************** Ritam ****************************/

	// @FindBy(xpath="//input[contains(@placeholder,'Search apps or items')]")
	// WebElement txtSearchBy;

	/*************** Ritam *************************/
	// private By globalSearch = By.xpath("//input[@title='Search Vouchers and
	// more']");
	private By globalSearch = By.xpath("//input[contains(@title,'Search')]");

	protected By globalSearchSalesStore = By.xpath("//input[contains (@class,'uiInputTextForAutocomplete')]");

	/*************** Payment Integration **************/
	protected By promisedIncenIcon = By
			.xpath("//h2[text()='Promised Incentives']/span/lightning-icon/lightning-primitive-icon");
	// inetegrated version
	protected By addIncentiveBtn = By.xpath("//button[contains(@class, 'addPaymentButton')]");
	protected By searchIncenTxt = By.xpath("//input[contains(@placeholder, 'Search Product')]");
	protected By plusIcon = By.xpath("//label[contains(@class, 'checkbox_faux')]/span");
	protected By addBtn = By.xpath("//button[text()='Add']");
	protected By retailCostLbl = By.xpath("//div[@title='Price']");
	protected By txtSuccessMsg = By
			.xpath("//div[text()='Success']/../span[contains(text(),'Promise List Add Successfully')]");

	String srtIncentive;

	protected By incentiveStatLbl = By.xpath("//tr[contains(@class, 'heading')]/th[2]/div");
	protected By incetiveSDrpDwn = By.xpath("//div[contains(@class, 'select')]/select[@name='Incentive Status']");
	protected By statusOptions = By.xpath("//div[contains(@class, 'select')]/select[@name='Incentive Status']/option");
	protected By statusPromised = By
			.xpath("//div[contains(@class, 'select')]/select[@name='Incentive Status']/option[1]");

	protected By marketerLbl = By.xpath("//div[text()='Total Marketer Cost']");
	protected By marketerValue = By.xpath("//div[text()='Total Marketer Cost']/../div[2]/span");
	protected By blueArrow = By
			.xpath("//div[contains(@class, 'summary_arrow')]/lightning-icon/lightning-primitive-icon");

	protected By paymentId = By.xpath("//table[contains(@class, 'wn-premium')]/tbody/tr[1]/td/a");
	protected By paymentIDLbl = By.xpath("//div[contains(@class, 'page-header')]/span");
	protected By refundBtn = By.xpath("//button[@title='Refund']");
	protected By item1Chcek = By
			.xpath("(//label[contains(@class, 'slds-checkbox__label')]/span[@class='slds-checkbox_faux'])[2]");
	protected By refundPopBtn = By.xpath("(//button[text()='Refund'])[2]");
	protected By qntyInput1 = By.xpath("(//div[contains(@title, 'Quantity')]/input)[1]");
	protected By qnty2 = By.xpath("(//div[contains(@title, 'Quantity')]/input)[2]");
	protected By cloverInput = By.xpath("//input[@placeholder='Search for Clover']");

	/******************* Payment Integration *********************/

	public void homePageValidation() {
		waitUntilElementVisibleBy(driver, homeTab, 120);

		tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Homepage Navigated");

		if (verifyObjectDisplayed(leadsTab)) {
			clickElementBy(leadsTab);
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Leads Tab clicked");
		} else {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Leads Tab not present");
		}

	}

	public void globalSearch() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearch, 120);
		if (verifyObjectDisplayed(globalSearch)) {
			fieldDataEnter(globalSearch, testData.get("MmbershipNumber"));
			driver.findElement(globalSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}

	}

	public void verifyMemberGlobalSearch() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement mem = driver.findElement(By.xpath("(//a[@title='" + testData.get("MmbershipNumber") + "'])[2]"));
		String member = mem.getText().trim();
		if (member.equalsIgnoreCase(testData.get("MmbershipNumber"))) {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}

	}

	public void globalSearchTour(String data) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {

			waitUntilElementVisibleBy(driver, globalSearch, 120);
			if (verifyObjectDisplayed(globalSearch)) {
				fieldDataEnter(globalSearch, data);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				sendKeyboardKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// List<WebElement> e =
				// driver.findElements(By.xpath("//span[contains(@title,'"+data+"')]/../../../a"));
				// List<WebElement> e =
				// driver.findElements(By.xpath("//span[contains(@title,'"+data+"')]"));
				WebElement element = driver.findElement(By.xpath("//a[@title='" + data + "']"));
				// clickElementWb(element);
				if (verifyElementDisplayed(element)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementWb(element);

					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS,
							"Search Result displayed and clicked");

				}

				else {
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
							"Search Result is not displayed and not click");

				}

			} else {
				tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
						"Global search box is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Getting error:" + e);
			throw e;
		}

	}

	private By btnViewAll = By.xpath("//button[contains(text(),'View All')]");

	
	
	
	public void navigateToMenu(String Iterator){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, homeTab, 120);
		checkLoadingSpinner();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, menuIcon, 120);
		clickElementJSWithWait(menuIcon);
		waitUntilElementVisibleBy(driver, btnViewAll, 120);
		clickElementBy(btnViewAll);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisible(driver, txtSearchBy, 120);
		txtSearchBy.sendKeys(testData.get("Menu"+Iterator));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//mark[text()='"+testData.get("Menu"+Iterator)+"']")).click();
		tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to " + testData.get("Menu"+Iterator));
		
		
	}

	public void navigateToCommunity() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
//			waitUntilElementVisibleBy(driver, homeTab, 120);
		checkLoadingSpinner();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(menuIcon)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		driver.findElement(menuIcon).click();
		waitUntilElementVisible(driver, txtSearchBy, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//a[contains(text(),'Journey Community')]")).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		SwitchtoWindow();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void navigateToUrl(String Iterator) throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("URL" + Iterator));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	private By arivallist = By.xpath("(//img[@class='wn-left_custom_icon'])[3]");
	private By clickadd = By.xpath("(//div[@title='expand'])[2]");

	public void arrivallistselect() {
		waitUntilElementVisibleBy(driver, arivallist, 120);
		driver.findElement(arivallist).click();
		waitUntilElementVisibleBy(driver, clickadd, 120);
		driver.findElement(clickadd).click();
	}

	private By navigatejourneycomm = By.xpath("//a[contains(.,'Journey Community')]");
	private By Homemarketer = By.xpath("//li//a//img[contains(@src,'Home')]");

	public void navigatatojourneycommunity() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, homeTab, 120);
		checkLoadingSpinner();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(menuIcon)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		driver.findElement(menuIcon).click();
		waitUntilElementVisibleBy(driver, navigatejourneycomm, 120);
		driver.findElement(navigatejourneycomm).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		/*
		 * ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		 * driver.switchTo().window(tabs.get(1));
		 */

	}

	private By arlst = By.xpath("//b[text()='Arrival List']");

	public void selectarrivallist() {
		waitUntilElementVisibleBy(driver, arlst, 120);
		driver.findElement(arlst).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}

	private By ressel = By.xpath(
			"//a[text()='Reservations']/ancestor::div[@class='slds-grid slds-grid--vertical slds-m-bottom_small']//a[text()='"
					+ testData.get("ReservationNumber") + "']");
	private By travelchnl = By.xpath("//span[@title='Travel Channel']");
	private By Bookbtn = By.xpath("//button[text()='Book Tour']");

	public void SelRes() {
		waitUntilElementVisibleBy(driver, ressel, 120);
		driver.findElement(ressel).click();
		waitUntilElementVisibleBy(driver, Bookbtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(Bookbtn))) {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Clicked on Available Reservation");
		} else {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Reservation is not found");
		}
	}

	private By ownersel = By.xpath(
			"//a[text()='Owners']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-card']//a[text()='"
					+ testData.get("LeadName") + "']");
	private By ownerimg = By.xpath("//img[@title='Owner']");

	public void SelOwner() {
		waitUntilElementVisibleBy(driver, ownersel, 120);
		driver.findElement(ownersel).click();
		waitUntilElementVisibleBy(driver, ownerimg, 120);
		if (verifyElementDisplayed(driver.findElement(ownerimg))) {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Clicked on Available Owner");
		} else {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Owner is not found");
		}
	}

	private By txtTourRecords = By.xpath("//input[@class='slds-input input']");

	public void searchTourRecords() {
		waitUntilElementVisibleBy(driver, txtTourRecords, 120);
		driver.findElement(txtTourRecords).click();
	}

	private By searchBar = By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Search.svg']");
	private By memberNumber = By.xpath("//input[@class='slds-input']");
	private By btnbookTour = By.xpath("//button[text()='BOOK TOUR']");
	private By btnCreateTour = By.xpath("//button[text()='CREATE TOUR']");
	private By arrivalList = By
			.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Reservation_List.svg']");
	private By txtSearchGuest = By.xpath("//input[@id='text-input-id-1']");
	private By lnkGuestName = By.xpath("//a[text()='" + testData.get("LeadName") + "']");
	private By arrivalIcon = By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Reservation_List.svg'");
	private By imgArrival = By.xpath("//img[contains(@src,'Reservation_List')]");

	public void searchReservaion() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.findElement(arrivalList).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(txtSearchGuest).sendKeys(testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(lnkGuestName).click();

	}

	public void ownerArrival() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyObjectDisplayed(imgArrival)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(imgArrival).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, "Search Result displayed");
		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.FAIL, "Search Result not displayed");
		}
		waitForSometime(tcConfig.getConfig().get("LongWait"));
	}

	public void globalSearchOwner() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, searchBar, 120);
		if (verifyObjectDisplayed(searchBar)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(memberNumber, testData.get("LeadName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(memberNumber).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.FAIL, "Search Result not displayed");
		}
	}

	public void toursToConfirm() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// WebElement searchBar=
		// driver.findElement(By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Search.svg']"));
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		String name = testData.get("LeadName");
		waitUntilElementVisibleBy(driver, searchBar, 120);

		driver.findElement(searchBar).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(memberNumber).sendKeys(name);
		driver.findElement(memberNumber).sendKeys(Keys.ENTER);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnbookTour, 120);

		List<WebElement> button = driver.findElements(btnbookTour);
		int count = button.size();
		if (count > 0) {
			button.get(0).click();

		} else {
			tcConfig.updateTestReporter("HomePage", "toursToConfirm", Status.FAIL, "Book Tour button not displayed");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnCreateTour, 120);
		clickElementBy(btnCreateTour);
	}

	/*
	 * public void mouseoverAndClick(WebElement element){ try{ ((JavascriptExecutor)
	 * driver).executeScript("arguments[0].scrollIntoView(true);", element); new
	 * Actions(driver).moveToElement(element).click().perform(); }catch(Exception
	 * e){ tcConfig.updateTestReporter("", "mouseoverAndClick",Status.FAIL,
	 * "unable to hover and click " + "element due to "+ e.getMessage()); }
	 * 
	 * }
	 */

	private By ReservationSearch = By.xpath("//input[@placeholder='Search Reservations and more...']");

	public void globalSearchRes() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ReservationSearch, 120);
		if (verifyObjectDisplayed(ReservationSearch)) {
			fieldDataEnter(ReservationSearch, testData.get("ReservationNumber"));
			driver.findElement(ReservationSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
	}

	public void globalSearch_Owner() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearch, 120);
		if (verifyObjectDisplayed(globalSearch)) {
			fieldDataEnter(globalSearch, testData.get("LeadName"));
			driver.findElement(globalSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
	}

	public By FirstName = By.xpath("//span[text()='Primary Guest First Name']//..//..//span[@class='uiOutputText']");
	public By LastName = By.xpath("//span[text()='Primary Guest Last Name']//..//..//span[@class='uiOutputText']");

	public void validateFN_LN() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, FirstName, 120);
		String Fname = driver.findElement(FirstName).getText();
		if (Fname.equalsIgnoreCase(testData.get("FirstName"))) {
			tcConfig.updateTestReporter("HomePage", "validateFN_LN", Status.PASS, "First Name Validated");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Error while validating first name");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, LastName, 120);
		String Lname = driver.findElement(LastName).getText();
		if (Lname.equalsIgnoreCase(testData.get("LastName"))) {
			tcConfig.updateTestReporter("HomePage", "validateFN_LN", Status.PASS, "Last Name Validated");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Error while validating last name");
		}
	}

	public void globalSearch(String data) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {

			waitUntilElementVisibleBy(driver, globalSearch, 120);
			if (verifyObjectDisplayed(globalSearch)) {
				fieldDataEnter(globalSearch, data);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// List<WebElement> e =
				// driver.findElements(By.xpath("//span[contains(@title,'"+data+"')]/../../../a"));
				// List<WebElement> e =
				// driver.findElements(By.xpath("//span[contains(@title,'"+data+"')]"));
			//	WebElement element = driver.findElement(By.xpath("//a//div//span[@title='" + data + "']"));
				WebElement element=  driver.findElement(By.xpath("//a//div//span[@title='"+data+"']"));

				if (verifyElementDisplayed(element)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementWb(element);

					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS,
							"Search Result displayed and clicked");

				} else {
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
							"Search Result is not displayed and not click");

				}

			} else {
				tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
						"Global search box is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Getting error:" + e);
			throw e;
		}

	}

	// Added Ajib Parida
	public void globalSearch_MembershipNo(String data) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {

			waitUntilElementVisibleBy(driver, globalSearch, 120);
			if (verifyObjectDisplayed(globalSearch)) {
				fieldDataEnter(globalSearch, data);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				WebElement element = driver.findElement(By.xpath("//div/mark[text()='" + data + "']"));
				// clickElementWb(element);
				if (verifyElementDisplayed(element)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJSWithWait(element);

					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS,
							"Search Result displayed and clicked");

				} else {
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
							"Search Result is not displayed and not click");

				}

			} else {
				tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
						"Global search box is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Getting error:" + e);
			throw e;
		}

	}

	public void globalSearchOperatinghours(String data) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {

			waitUntilElementVisibleBy(driver, globalSearch, 120);
			if (verifyObjectDisplayed(globalSearch)) {
				fieldDataEnter(globalSearch, data);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// List<WebElement> e =
				// driver.findElements(By.xpath("//span[contains(@title,'"+data+"')]/../../../a"));
				WebElement operatinghours = driver
						.findElement(By.xpath("//span[contains(@title,'" + data + " - Operating')]"));
				/*
				 * if(e.size()>=0) { waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * clickElementWb(e.get(1));
				 * 
				 * tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS,
				 * "Search Result displayed and clicked"); } else {
				 * tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
				 * "Search Result is not displayed"); }
				 */
				if (verifyElementDisplayed(operatinghours)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementWb(operatinghours);

					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS,
							"Search Result displayed and clicked");

				} else {
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
							"Search Result is not displayed");

				}
			} else {
				tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
						"Global search box is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Getting error:" + e);
			throw e;
		}

	}

	private By journeyTabHomePage = By.xpath("//div[contains(@class,'wn-banner_home cTMCMAHomePage')]");
	private By lnkArrivalList = By.xpath("//img[contains(@src,'Reservation_List')]/..");
	private By drpDownSalesStore = By.xpath("//input[@name='territoryType']");
	private By lnkSearchGuest = By.xpath("//img[contains(@src,'All_Guests')]");
	private By drpDwnTourOrRes = By.xpath("//select[@name='Dropdown']");
	private By inputSearchTourRes = By.xpath("//input[contains(@placeholder,'Number')]");

	public void navigateToArrivalListMobUI() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, journeyTabHomePage, 120);
		clickElementJSWithWait(lnkArrivalList);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/********************** Ritam ***********************************/

	/*
	 * public void navigateToSearchReservationOrTour() {
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * //waitUntilElementVisibleBy(driver, journeyTabHomePage, 120);
	 * clickElementBy(lnkSearchGuest);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitUntilElementVisibleBy(driver, drpDwnTourOrRes, 120); }
	 */

	/******************** Ritam **************************/

	private By lnkTourDetails = By.xpath("//button[contains(@title,'navigate to tour')]");

	public void searchTour(String tourNo) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
		new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(0);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(inputSearchTourRes, tourNo);
		WebElement el = driver.findElement(inputSearchTourRes);
		el.sendKeys(Keys.ENTER);
		el.sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, By.xpath("//span[text()='" + tourNo + "']"), 240);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (driver.findElements(By.xpath("//span[text()='" + tourNo + "']")).size() == 0) {
			waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
			new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(inputSearchTourRes, tourNo);
			driver.findElement(inputSearchTourRes).sendKeys(Keys.ENTER);
			el.sendKeys(Keys.ENTER);
			el.sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//span[text()='" + tourNo + "']"), 120);
		}
		if (verifyObjectDisplayed(By.xpath("//span[text()='" + tourNo + "']"))) {
			tcConfig.updateTestReporter("HomePage", "searchReservation", Status.PASS, "Searched Result is displayed");
			clickElementBy(lnkTourDetails);
		} else {
			tcConfig.updateTestReporter("HomePage", "searchReservation", Status.FAIL,
					"Searched Result is not displayed");
		}
	}

	/******************** Ritam ************************************/
	/*
	 * public void searchReservationTour(String ReservationTourNum, String Choice) {
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
	 * if(Choice.contains("Res")) { new
	 * Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1); }
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * fieldDataEnter(inputSearchTourRes, ReservationTourNum); WebElement
	 * wb=driver.findElement(inputSearchTourRes); wb.sendKeys(Keys.ENTER);
	 * wb.sendKeys(Keys.ENTER);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver,
	 * By.xpath("//a[text()='"+ReservationTourNum+"']"), 240);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * if(driver.findElements(By.xpath("//a[text()='"+ReservationTourNum+"']")).size
	 * ()==0) { waitUntilElementVisibleBy(driver, inputSearchTourRes, 120); new
	 * Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * fieldDataEnter(inputSearchTourRes, ReservationTourNum); WebElement
	 * web=driver.findElement(inputSearchTourRes); web.sendKeys(Keys.ENTER);
	 * web.sendKeys(Keys.ENTER);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver,
	 * By.xpath("//a[text()='"+ReservationTourNum+"']"), 120); }
	 * if(verifyObjectDisplayed(By.xpath("//a[text()='"+ReservationTourNum+"']"))) {
	 * tcConfig.updateTestReporter("HomePage", "searchReservation", Status.PASS,
	 * "Searched Result is displayed");
	 * clickElementBy(By.xpath("//a[text()='"+ReservationTourNum+"']")); } else {
	 * tcConfig.updateTestReporter("HomePage", "searchReservation", Status.FAIL,
	 * "Searched Result is not displayed"); } }
	 */

	/*********************** Ritam *************************************/

	private By drpDwnArrivalList = By.xpath("(//select[@class='slds-select'])[2]");
	private By drpDwnWeek = By.xpath("(//select[@class='slds-select'])[1]");
	private By btnExpand = By.xpath("//div[@title='expand'][contains(@id,'arrivalList')]");

	public void openArrivals() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);

		WebElement option = new Select(driver.findElement(drpDwnArrivalList)).getFirstSelectedOption();
		if (!(option.getText().equals("ARRIVAL LIST"))) {
			new Select(driver.findElement(drpDwnArrivalList)).selectByIndex(0);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

		List<WebElement> e1 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
		if (e1.size() > 0) {
			new Select(driver.findElement(drpDwnWeek)).selectByIndex(3);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			while (driver.findElements(btnExpand).size() == 0
					&& driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]")).size() == 0) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			List<WebElement> e3 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
			if (e3.size() > 0) {
				clickElementBy(drpDownSalesStore);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementWb(driver.findElements(By.xpath("//lightning-base-combobox-item")).get(1));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			while (driver.findElements(btnExpand).size() == 0
					&& driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]")).size() == 0) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			List<WebElement> e2 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
			if (e2.size() == 0) {
				tcConfig.updateTestReporter("ArrivalPage", "openArrivals", Status.PASS, "Records are visible");
			} else {
				tcConfig.updateTestReporter("ArrivalPage", "openArrivals", Status.FAIL, "Records are not available");
			}
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	private By lnkReportMobUi = By.xpath("//img[contains(@src,'Report')]/..");
	private By errorMsg = By.xpath("//p[text()='If Reports and Dashboards are not present please refresh.']");
	private By txtBoxSearchReports = By.xpath("//input[contains(@placeholder,'Search recent reports')]");

	public void navigateToReportMobUi() {
		waitUntilElementVisibleBy(driver, lnkReportMobUi, 120);
		driver.findElement(lnkReportMobUi).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabsList = new ArrayList<>(driver.getWindowHandles());
		String nextTab = tabsList.get(1);
		System.out.println("Total handles =" + tabsList.size());
		driver.switchTo().window(nextTab);

	}

	public void globalSearchaSalesStore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearchSalesStore, 120);
		if (verifyObjectDisplayed(globalSearchSalesStore)) {
			fieldDataEnter(globalSearchSalesStore, testData.get("SalesStore"));
			// driver.findElement(globalSearchSalesStore ).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//span[@title='" + testData.get("SalesStore") + "']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchaSalesStore", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchaSalesStore", Status.FAIL,
					"Search Result not displayed");
		}

	}

	public void globalSearchGuestName() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearchSalesStore, 120);
		if (verifyObjectDisplayed(globalSearchSalesStore)) {
			// fieldDataEnter(globalSearchSalesStore , LeadsPage.Lead);
			fieldDataEnter(globalSearchSalesStore, "TSHPG XSFHA");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.findElement(By.xpath("//span[@title='"+LeadsPage.Lead+"']")).click();
			driver.findElement(By.xpath("//span[@title='TSHPG XSFHA']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchGuestName", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchGuestName", Status.FAIL,
					"Search Result not displayed");
		}

	}

	public void globalSearchTourId() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearchSalesStore, 120);
		if (verifyObjectDisplayed(globalSearchSalesStore)) {
			String TourId = LeadsPage.TourIdmap.get(testData.get("AutomationID"));
			System.out.println("Tour ID in Hash Map:" + TourId);

			fieldDataEnter(globalSearchSalesStore, TourId);
			// fieldDataEnter(globalSearchSalesStore , LeadsPage.createdTourID);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.findElement(By.xpath("//span[@title='"+LeadsPage.createdTourID+"']")).click();
			driver.findElement(By.xpath("//span[@title='" + TourId + "']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchTourId", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchTourId", Status.FAIL, "Search Result not displayed");
		}

	}

	private Object LeadsPage() {
		// TODO Auto-generated method stub
		return null;
	}

	public void globalSearchExistingTour() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearchSalesStore, 120);
		if (verifyObjectDisplayed(globalSearchSalesStore)) {
			fieldDataEnter(globalSearchSalesStore, testData.get("TourNo"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//span[@title='" + testData.get("TourNo") + "']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchTourId", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchTourId", Status.FAIL, "Search Result not displayed");
		}

	}

	public void globalSearchTransactionId() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearchSalesStore, 120);
		if (verifyObjectDisplayed(globalSearchSalesStore)) {
			// fieldDataEnter(globalSearchSalesStore , TourRecordsPage.strTransactionId);
			fieldDataEnter(globalSearchSalesStore, "P-000561621");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.findElement(By.xpath("//span[@title='"+TourRecordsPage.strTransactionId+"']")).click();
			driver.findElement(By.xpath("//span[@title='P-000561621']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchTourId", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchTourId", Status.FAIL, "Search Result not displayed");
		}

	}

	@FindBy(xpath = "//input[contains(@placeholder,'Search apps or items')]")
	WebElement txtSearchBy;

	public void navigateToMobUI() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
//	            waitUntilElementVisibleBy(driver, homeTab, 120);
		checkLoadingSpinner();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(menuIcon)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		driver.findElement(menuIcon).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(btnViewAll);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisible(driver, txtSearchBy, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//p[contains(text(),'Journey Tablet')]")).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		SwitchtoWindow();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void navigateToSearchReservationOrTour() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilElementVisibleBy(driver, journeyTabHomePage, 120);
		clickElementBy(lnkSearchGuest);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpDwnTourOrRes, 120);
	}

	/********* Ritam ***************************/

	/*
	 * protected By tourDetBtn = By.xpath("//button[text()='TOUR DETAILS']");
	 * 
	 * 
	 * public void searchReservationTour(String ReservationTourNum, String Choice) {
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
	 * if(Choice.contains("Res")) { new
	 * Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1); }
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * fieldDataEnter(inputSearchTourRes, ReservationTourNum); WebElement
	 * wb=driver.findElement(inputSearchTourRes); wb.sendKeys(Keys.ENTER);
	 * //wb.sendKeys(Keys.ENTER);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 */

	/****** Ritam *******/

	/*
	 * waitUntilElementVisibleBy(driver,
	 * By.xpath("//a[text()='"+ReservationTourNum+"']"), 240);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * if(driver.findElements(By.xpath("//a[text()='"+ReservationTourNum+"']")).size
	 * ()==0) { waitUntilElementVisibleBy(driver, inputSearchTourRes, 120); new
	 * Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * fieldDataEnter(inputSearchTourRes, ReservationTourNum); WebElement
	 * web=driver.findElement(inputSearchTourRes); web.sendKeys(Keys.ENTER);
	 * web.sendKeys(Keys.ENTER);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver,
	 * By.xpath("//a[text()='"+ReservationTourNum+"']"), 120); }
	 * if(verifyObjectDisplayed(By.xpath("//a[text()='"+ReservationTourNum+"']"))) {
	 * tcConfig.updateTestReporter("HomePage", "searchReservation", Status.PASS,
	 * "Searched Result is displayed");
	 * clickElementBy(By.xpath("//a[text()='"+ReservationTourNum+"']")); } else {
	 * tcConfig.updateTestReporter("HomePage", "searchReservation", Status.FAIL,
	 * "Searched Result is not displayed"); }
	 */
	/******* Ritam ****/
	/*
	 * if(verifyObjectDisplayed(By.xpath("//span[text()='"+ReservationTourNum+"']"))
	 * ) { tcConfig.updateTestReporter("HomePage", "searchReservationTour",
	 * Status.PASS, "Searched Result is displayed");
	 * clickElementBy(By.xpath("//span[text()='"+ReservationTourNum+"']"));
	 * clickElementJSWithWait(driver.findElement(tourDetBtn));
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * } else { tcConfig.updateTestReporter("HomePage", "searchReservationTour",
	 * Status.FAIL, "Searched Result is not displayed"); } }
	 */

	/************************** Ritam ***************************/
	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifyAddIncentiveScreen
	 * @Description : This function is used to Verify payment record is editable for
	 *              Phone, email, carrier fields on the payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void verifyAddIncentiveScreen() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			WebElement ele = driver.findElement(promisedIncenIcon);
			clickElementJSWithWait(ele);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(addIncentiveBtn))) {
				tcConfig.updateTestReporter("AddIncentivePage", "verifyAddIncentiveScreen", Status.PASS,
						"Navigated to Add Incentive Screen");
			} else {
				tcConfig.updateTestReporter("AddIncentivePage", "verifyAddIncentiveScreen", Status.FAIL,
						"Failed to navigate to Add Incentive Screen");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("AddIncentivePage", "verifyAddIncentiveScreen", Status.FAIL,
					"Failed in Method");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clcikToAddPromisedItem
	 * @Description : This function is used to Verify payment record is editable for
	 *              Phone, email, carrier fields on the payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickToAddPromisedItem() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		try {
			
			WebElement ele =driver.findElement(addIncentiveBtn);
			clickElementJSWithWait(ele);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			fieldDataEnter(searchIncenTxt, testData.get("Product"));
			WebElement wb = driver.findElement(searchIncenTxt);
			wb.sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJSWithWait(plusIcon);
			System.out.println("Successfuly Selected the Product");
			// waitUntilElementVisibleBy(driver, addBtn, 120);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			srtIncentive = driver.findElement(retailCostLbl).getText();
			System.out.println("retail cost in product= " + srtIncentive);
			clickElementJSWithWait(addBtn);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			// waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
			// div[@title='Baldknobber -Adult']
			String prodLocator = "//div[@title='" + testData.get("Product") + "']";
			By productLbl = By.xpath(prodLocator);
			if (verifyObjectDisplayed(productLbl)) {
				tcConfig.updateTestReporter("AddIncentivePage", "clcikToAddPromisedItem", Status.PASS,
						"Successfully added the  incentive");
			} else {
				tcConfig.updateTestReporter("AddIncentivePage", "clcikToAddPromisedItem", Status.FAIL,
						"FAiled to add added the  incentive");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("AddIncentivePage", "clcikToAddPromisedItem", Status.FAIL,
					"Failed in Method to add added the  incentive");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifyIncentiveStatus
	 * @Description : This function is used to Verify payment record is editable for
	 *              Phone, email, carrier fields on the payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void verifyIncentiveStatus() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(incentiveStatLbl)) {
			tcConfig.updateTestReporter("AddIncentivePage", "verifyIncentiveStatus", Status.PASS,
					"New Field Incentive Status is displayed Successfully");
		} else {
			tcConfig.updateTestReporter("AddIncentivePage", "verifyIncentiveStatus", Status.FAIL,
					"FAiled to dispaly New Field Incentive Status");
		}
		
		WebElement ele = driver.findElement(incetiveSDrpDwn);
		clickElementJSWithWait(ele);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> wbelList = driver.findElements(statusOptions);
		String strPromised = wbelList.get(0).getText();
		String strPregift = wbelList.get(1).getText();
		String strDistributed = wbelList.get(2).getText();
		System.out.println(strPromised);
		if (strPromised.equalsIgnoreCase("Promised Incentive") && strPregift.equalsIgnoreCase("Pre-Gift Incentive")
				&& strDistributed.equalsIgnoreCase("Distributed Incentive")) {
			tcConfig.updateTestReporter("AddIncentivePage", "verifyIncentiveStatus", Status.PASS,
					"Successfully verify Incentive Status");
		} else {
			tcConfig.updateTestReporter("AddIncentivePage", "verifyIncentiveStatus", Status.PASS,
					"Cannot Verify Incentive Status");
		}
		
		WebElement statusPromisedEle = driver.findElement(statusPromised);
		clickElementJSWithWait(statusPromisedEle);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("AddIncentivePage", "verifyIncentiveStatus", Status.PASS,
				"Successfully Selected One Incentive Value");
	}

	protected By tourDetBtn = By.xpath("//button[text()='TOUR DETAILS']");

	public void searchReservationTour(String ReservationTourNum, String Choice) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
		if (Choice.contains("Res")) {
			new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(inputSearchTourRes, ReservationTourNum);
		WebElement wb = driver.findElement(inputSearchTourRes);
		wb.sendKeys(Keys.ENTER);
		// wb.sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		/*
		 * waitUntilElementVisibleBy(driver,
		 * By.xpath("//a[text()='"+ReservationTourNum+"']"), 240);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * if(driver.findElements(By.xpath("//a[text()='"+ReservationTourNum+"']")).size
		 * ()==0) { waitUntilElementVisibleBy(driver, inputSearchTourRes, 120); new
		 * Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * fieldDataEnter(inputSearchTourRes, ReservationTourNum); WebElement
		 * web=driver.findElement(inputSearchTourRes); web.sendKeys(Keys.ENTER);
		 * web.sendKeys(Keys.ENTER);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * waitUntilElementVisibleBy(driver,
		 * By.xpath("//a[text()='"+ReservationTourNum+"']"), 120); }
		 * if(verifyObjectDisplayed(By.xpath("//a[text()='"+ReservationTourNum+"']"))) {
		 * tcConfig.updateTestReporter("HomePage", "searchReservation", Status.PASS,
		 * "Searched Result is displayed");
		 * clickElementBy(By.xpath("//a[text()='"+ReservationTourNum+"']")); } else {
		 * tcConfig.updateTestReporter("HomePage", "searchReservation", Status.FAIL,
		 * "Searched Result is not displayed"); }
		 */
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, By.xpath("//a[text()='" + ReservationTourNum + "']"), 120);
		if (verifyObjectDisplayed(By.xpath("//a[text()='" + ReservationTourNum + "']"))) {
			tcConfig.updateTestReporter("HomePage", "searchReservationTour", Status.PASS,
					"Searched Result is displayed");
			clickElementBy(By.xpath("//a[text()='" + ReservationTourNum + "']"));
			// clickElementJSWithWait(driver.findElement(tourDetBtn));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("HomePage", "searchReservationTour", Status.FAIL,
					"Searched Result is not displayed");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clcikToAddPromisedItem
	 * @Description : This function is used to Verify payment record is editable for
	 *              Phone, email, carrier fields on the payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void verifyMarketerCost() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// clickElementWb(driver.findElement(blueArrow));
		WebElement blueArrowEle = driver.findElement(blueArrow);
		clickElementJSWithWait(blueArrowEle);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (verifyElementDisplayed(driver.findElement(marketerLbl))
				&& verifyElementDisplayed(driver.findElement(marketerValue))) {

			tcConfig.updateTestReporter("AddIncentivePage", "verifyMarketerCost", Status.PASS,
					"Successfully Displayed Marketeer Cost");
		} else {
			tcConfig.updateTestReporter("AddIncentivePage", "verifyMarketerCost", Status.FAIL,
					"Failed to Displayed Marketeer Cost");
		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickOnPaymentRecordMob
	 * @Description : This function is used to Click on the paticular payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickOnPaymentRecordMob() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			pageDown(); /************* Ritam *******/
			WebElement paymentIdEle = driver.findElement(paymentId);
			clickElementJSWithWait(paymentIdEle);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "clickPaymentTab", Status.PASS,
					"Clicked on Payment ID successfully");

			if (verifyElementDisplayed(driver.findElement(paymentIDLbl))) {

				tcConfig.updateTestReporter("TourRecordsPage", "clickOnPaymentRecordMob", Status.PASS,
						"Successfully navigated to Payment Details screen");
			}

			else {
				tcConfig.updateTestReporter("TourRecordsPage", "clickOnPaymentRecordMob", Status.FAIL,
						"Failed to navigated to Payment Details screen");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickOnPaymentRecordMob", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifyRefundBtnMob
	 * @Description : This function is used to click on the refund button and refund
	 *              cash manually
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	// span/span[text()='FlexQA1']
	public void verifyRefundBtn() {
		String qunatityInput = testData.get("Quantity");
		String device = testData.get("CloverDeviceName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement refundBtnEle = driver.findElement(refundBtn);
		clickElementJSWithWait(refundBtnEle);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement item1ChcekEle = driver.findElement(item1Chcek);
		clickElementJSWithWait(item1ChcekEle);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(qntyInput1, "1");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement cloverInputEle = driver.findElement(cloverInput);
		clickElementJSWithWait(cloverInputEle);
		fieldDataEnter(cloverInput, device);
		driver.findElement(By.xpath("//span[contains(text(),'" + device + "')]")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// click the selected
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement refundPopBtnEle = driver.findElement(refundPopBtn);
		clickElementJSWithWait(refundPopBtnEle);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void pageDown() {
		driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
	}

	/*
	 * Method-Search Tour record in mobile UI Designed By-JYoti
	 */
	public void searchTour_Mobile() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
		new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(0);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String strtourNo = testData.get("TourNo").trim();
		fieldDataEnter(inputSearchTourRes, strtourNo);
		WebElement el = driver.findElement(inputSearchTourRes);
		el.sendKeys(Keys.ENTER);
		el.sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, By.xpath("//span[text()='" + testData.get("TourNo") + "']"), 240);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (driver.findElements(By.xpath("//span[text()='" + testData.get("TourNo") + "']")).size() == 0) {
			waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
			new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(inputSearchTourRes, strtourNo);
			driver.findElement(inputSearchTourRes).sendKeys(Keys.ENTER);
			el.sendKeys(Keys.ENTER);
			el.sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//span[text()='" + testData.get("TourNo") + "']"), 120);
		}
		if (verifyObjectDisplayed(By.xpath("//span[text()='" + testData.get("TourNo") + "']"))) {
			tcConfig.updateTestReporter("HomePage", "searchReservation", Status.PASS, "Searched Result is displayed");
			clickElementBy(lnkTourDetails);
		} else {
			tcConfig.updateTestReporter("HomePage", "searchReservation", Status.FAIL,
					"Searched Result is not displayed");
		}
	}

	/*
	 * Method-Naviagte to Menu Icon Designed By-JYoti
	 */
	public void navigateToBusinessRules(String Iterator) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, homeTab, 120);
		if (verifyObjectDisplayed(homeTab)) {
			checkLoadingSpinner();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyElementDisplayed(menuIcons)) {
				tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
						"User is navigated to Homepage");
			} else {
				tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
						"User is NOT navigated to Homepage");
			}
			menuIcons.click();
			waitUntilElementVisibleBy(driver, btnViewAll, 120);
			clickElementBy(btnViewAll);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisible(driver, txtSearchBy, 120);
			txtSearchBy.sendKeys(testData.get("Menu" + Iterator));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(By.xpath("(//mark[text()='" + testData.get("Menu" + Iterator) + "'])[1]")).click();
		} else {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(noItems)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(menuIcons)) {
					tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
							"User is navigated to Journey Desktop");
				} else {
					tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
							"User is NOT navigated to Journey Desktop");
				}
				menuIcons.click();
				waitUntilElementVisibleBy(driver, joruneyDesktop, 120);
				clickElementBy(joruneyDesktop);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, homeTab, 120);
				menuIcons.click();
				waitUntilElementVisibleBy(driver, btnViewAll, 120);
				clickElementBy(btnViewAll);
				waitUntilElementVisible(driver, txtSearchBy, 120);
				txtSearchBy.sendKeys(testData.get("Menu" + Iterator));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("(//mark[text()='" + testData.get("Menu" + Iterator) + "'])[1]")).click();
			}
		}

	}

	public void navigateToBusinessRuleNew() {
		if (verifyObjectDisplayed(
				By.xpath("//div[@class = 'slds-context-bar']//div[@role = 'list']//a[@title= 'Business Rules']"))) {
			clickElementJSWithWait(
					By.xpath("//div[@class = 'slds-context-bar']//div[@role = 'list']//a[@title= 'Business Rules']"));
			tcConfig.updateTestReporter("HomePage", "navigateToBusinessRuleNew", Status.PASS,
					"Clicked Business Rule Tab");
		} else {
			clickElementJSWithWait(By.xpath("//span[text()='More']"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(By.xpath("//a[@role = 'menuitemcheckbox']//span[text() = 'Business Rules']"))) {
				clickElementJSWithWait(By.xpath("//a[@role = 'menuitemcheckbox']//span[text() = 'Business Rules']"));
				tcConfig.updateTestReporter("HomePage", "navigateToBusinessRuleNew", Status.PASS,
						"Clicked Business Rule Tab");
			} else {
				tcConfig.updateTestReporter("HomePage", "navigateToBusinessRuleNew", Status.FAIL,
						"Business Rule Tab not found");
			}

		}
	}

	/*
	 * Method-NAvigate to search a Lead/Owner in MobileUI Designed By-JYoti
	 */

	public void navigateToSearchLeadMobile() {

		waitUntilElementVisibleBy(driver, img_SearchInMobile, 120);
		clickElementJSWithWait(img_SearchInMobile);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

}
