package salesforce.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SalesRepAssignmentPage extends SalesforceBasePage {
//	LeadsPage leadPage = new LeadsPage(tcConfig);
	public static final Logger log = Logger.getLogger(ReservationPage.class);

	public SalesRepAssignmentPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String newTourId;
	public static List<String> newTourIdSalesRep;
	public static String newTourId1;
	public static String bookingnumber;
	public static List<String> getTourID = new ArrayList<String>();
	public static String getTourID1;
	public static String getTourID2;

	public By chkGuestName = By.xpath("//td[@class='slds-text-align_right']//span[@class='slds-checkbox_faux']");
	public By btnCheckIn = By.xpath("//button[contains(text(),'Check-In')]");
	private By quickSearchEdit = By.xpath("//input[@placeholder='Quick Search']");
	private By guestSelectionChkBx = By.xpath("//input[contains(@id,'tour-table_checkbox')]/../label");
	private By checkInBtn = By.xpath("//button[contains(text(),'Check-In')]");
	private By checkedInLink = By.xpath("//a[contains(text(),'Checked-In')]");
	private By selectGuestCheckedIn = By.xpath("//div[@class='slds-form-element__control']/input[@type='checkbox']");
	private By assignRepsBtn = By.xpath("//button[text()='Assign Reps']");
	private By onTourLink = By.xpath("//a[contains(text(),'On Tour')]");
	private By completeTourBtn = By.xpath("(//button[text()='Complete Tour'])[1]");
	private By qaRepEdit = By.xpath("//span[text()='QA Rep']/../..//input");
	private By selectDidTourdrpDwn = By.xpath("//select[@name='didTheyTour']");
	private By saveDispositionBtn = By.xpath("//button[text()='Save']");
	private By dispositionDrpdwn = By.xpath("//select[@name='dispositionStatus']");
	public By saleStorDD = By.xpath("//select[@name='selectItem']");
	public By txtMarketingAgent = By.xpath(
			"(//span[contains(text(),'Marketing Agent')]/following::div//span[contains(text(),'Test User Tour Reception')])[1]");
	public By txtMarketingAgentuat = By.xpath(
			"//span[contains(text(),'Marketing Agent')]/following::div//span[contains(text(),'tourreception2')]");
	public By txtSalesAgent1 = By.xpath(
			"(//span[contains(text(),'Sales Agent 1')]/following::div//span[@class='slds-pill slds-pill_link fullWidth'])[2]");
	public By txtSalesManager = By.xpath(
			"(//span[contains(text(),'Sales Manager')]/following::div//input[@class='slds-input slds-combobox__input'])[1]");
	public By txtQARep = By.xpath("(//span[contains(text(),'QA Rep')]/following::div[@class='slds-form-element'])[1]");
	public By dispositionLink = By.xpath("//a[contains(text(),'Dispositioned')]");

	public void verifySalesRepAssignment() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		newTourId = LeadsPage.createdTourID;
		fieldDataEnter(quickSearchEdit, newTourId);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + newTourId + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in Sales Rep Assignment Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Tour has NOT been displayed in Sales Rep Assignment Page");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(guestSelectionChkBx);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(checkInBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> popup = driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
		if (popup.size() > 0) {
			popup.get(0).click();
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(checkedInLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(selectGuestCheckedIn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(assignRepsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(onTourLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(completeTourBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (driver.findElement(txtMarketingAgentuat).getText().contains("tourreception2")
				&& driver.findElement(txtSalesAgent1).getText().contains("")
				&& driver.findElement(txtSalesManager).getText().contains("")) {
			tcConfig.updateTestReporter("SalesRepAssignmentPage", "verifySalesRepAssignment", Status.PASS,
					"MarketingAgent,SalesAgent1 and Sales Manager is auto populated in screen");
		} else {
			tcConfig.updateTestReporter("ReservationPage", "validateAUStatus", Status.FAIL,
					" Failed while validating MarketingAgent,Sales Manager and SalesAgent1 ");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		new Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText("Yes");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText("Did Not Purchase");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.findElement(qaRepEdit).sendKeys("Test User Tour Reception");
		fieldDataEnter(qaRepEdit, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(saveDispositionBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(dispositionLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public By relatedTour = By.xpath("//span[@title='Related Tours']");

	public By btnBatchPrint = By.xpath("//button[contains(text(),'Batch Print')]");
	public By tourRecords = By.xpath("//div[@title='Tour Record Number']//a");

	public void verifyBatchPrintPresent() {
		waitUntilObjectVisible(driver, btnBatchPrint, 120);
		if (verifyObjectDisplayed(btnBatchPrint)) {
			tcConfig.updateTestReporter("SalesRepAssignmentPage", "verifyBatchPrintPresent", Status.PASS,
					"Batch Print button is present in screen");
		} else {
			tcConfig.updateTestReporter("SalesRepAssignmentPage", "verifyBatchPrintPresent", Status.FAIL,
					"Failed while validating Batch Print button");
		}
		newTourIdSalesRep = TourRecordsPage.getTourID;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// int getTour_Size = getTourID.size();
		checkbox_element(getTourID.get(getTourID.size() - 1)).click();
		checkbox_element(getTourID.get(getTourID.size() - 2)).click();

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> button = driver.findElements(tourRecords);
		System.out.println(button.size());

	}

	public WebElement checkbox_element(String tourPackageNumber) {
		return driver.findElement(By.xpath("//a[text()='" + tourPackageNumber + "']/../../..//label"));
	}

}