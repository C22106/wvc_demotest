package salesforce.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SalesStorePage extends SalesforceBasePage {
	int Quantityonhandbef;
	int Quantityonhandaft;

	public static final Logger log = Logger.getLogger(SalesStorePage.class);

	public SalesStorePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public By listview = By.xpath("//a[@title='Select List View']");
	public By listoptn = By.xpath("//span[contains(text(),'" + testData.get("ListOptn") + "')]");
	private By lnkParentSalesStore = By.xpath("//span[text()='Parent Territory']/../../div[2]//a");
	private By lnkRelated = By.xpath("//div[contains(@class,'windowViewMode-normal')]//a[@title='Related']");
	private By tblSalesStorePremium = By.xpath("//span[@title='Sales Store Premiums']");
	private By lnkPremium = By.xpath("//td[contains(@role,'gridcell')]//a[@title='" + testData.get("Product") + "']");
	private By lnkViewAll = By.xpath("//div[contains(@class,'oneContent active')]//span[text()='Sales Store Premiums']/../../..//span[contains(@class,'view-all')]");
	
	/******************* Payment Integration **************************/

	@FindBy(xpath = "//span[contains(text(),'Is Integration Available')]/..//following-sibling::div//span[contains(@class,'uiOutputCheckbox')]")
	WebElement paymentIntegrationAvailabilityChkbx;

	@FindBy(xpath = "//span[contains(text(),'Refundable Tour Deposit Allowed')]/..//following-sibling::div//span[contains(@class,'uiOutputCheckbox')]//img")
	WebElement refndbleTourDepositChkbx;

	@FindBy(xpath = "//span[contains(text(),'Edit Refundable Tour Deposit Allowed')]")
	WebElement editRefndbleTourDepositBtn;

	@FindBy(xpath = "//label//span[text()='Refundable Tour Deposit Allowed']/../../input[contains(@type,'checkbox')]")
	WebElement editRefndbleTourDepositChkbx;

	@FindBy(xpath = "//span[contains(text(),'Save')]")
	WebElement saveButton;

	public By searchParent = By.xpath("//input[contains(@name, 'search-input')]");
	public By checkBoxMC = By.xpath(
			"(//div/span[text()='Send Emails From MC']/../following-sibling::div/span/span/img[contains(@class, ' checked')])[1]");
	public By editMCButton = By.xpath("//div/span[text()='Send Emails From MC']/../following-sibling::div/button)[1]");
	public By editMCCheckBox = By.xpath("//div/label/span[text()='Send Emails From MC']/../following-sibling::input");
	public By MClabel = By.xpath("//div/span[text()='Send Emails From MC']");
	public By saveButtonforSFMC = By.xpath("(//button/span[text()='Save'])[1]");
//public By searchParent = By.xpath("//input[contains(@name, 'search-input')]");
	public By editButton = By.xpath("//a/div[@title='Edit']");
	public By journeyLbl = By.xpath("//label/span[text()='Just Breakfast Check-In Instructions']");
	public By journeyTxt = By
			.xpath("//label/span[text()='Just Breakfast Check-In Instructions']/../following-sibling::textarea");
	public By savePop = By.xpath("(//button/span[text()='Save'])[2]");
	public By journeySaved = By
			.xpath("//div/span[text()='Just Breakfast Check-In Instructions']/../following-sibling::div/span/span");

	/*********************** Payment Integration *****************************/

	public void selectlistview() {
		waitUntilElementVisibleBy(driver, listview, 120);
		driver.findElement(listview).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(listoptn))) {
			tcConfig.updateTestReporter("SalesStorePage", "selectlistview", Status.PASS,
					"Clicked on List View DropDown");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"Failed while clicking on List View DropDown");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, listoptn, 120);
		driver.findElement(listoptn).click();
		tcConfig.updateTestReporter("SalesStorePage", "selectlistview", Status.PASS,
				"Clicked on List Option successfully");
	}

	public By salestore = By.xpath("//a[@class='slds-truncate outputLookupLink slds-truncate forceOutputLookup']");

	public void selectSalesStore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, salestore, 120);
		List<WebElement> salestoreslist = driver.findElements(salestore);
		System.out.println(salestoreslist.size());
		if (salestoreslist.size() > 0) {
			salestoreslist.get(0).click();
			tcConfig.updateTestReporter("SalesStorePage", "selectSalesStore", Status.PASS,
					"Successfully clicked Sales Store");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"Failed while clicking on Sales Store");
		}
	}

	public By qunonhndbef = By.xpath("(//span[@title='Quantity on Hand'])//..//span[@class='uiOutputNumber']");

	public void quantityonhandbefore() {
		waitUntilElementVisibleBy(driver, qunonhndbef, 120);
		Quantityonhandbef = Integer.parseInt(driver.findElement(qunonhndbef).getText());
	}

	public By transtab = By.xpath("(//span[contains(text(),'Transactions')])");
	public By newtab = By.xpath("(//div[@title='New'])[2]");

	public void clicktransaction() {
		waitUntilElementVisibleBy(driver, transtab, 120);
		driver.findElement(transtab).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(newtab))) {
			tcConfig.updateTestReporter("SalesStorePage", "clicktransaction", Status.PASS, "Transaction tab clicked");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"Failed while clicking on Transaction Tab");

		}
	}

	public By Inventorysel = By.xpath("(//span[contains(text(),'Consume Inventory')])[2]");
	public By nexttab = By.xpath("//span[contains(text(),'Next')]");
	public By Quantityfield = By.xpath(
			"//label[@class='label inputLabel uiLabel-left form-element__label uiLabel']//span[contains(.,'Quantity')]//..//..//input");
	public By Transactionfield = By.xpath(
			"//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[contains(.,'Transaction Type')]//..//..//a");
	public By GlCodefield = By.xpath(
			"//label[@class='label inputLabel uiLabel-left form-element__label uiLabel']//span[contains(.,'GL Code')]//..//..//input");
	public By Subtypefield = By.xpath(
			"//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[contains(.,'Sub Type')]//..//..//a");
	public By Savebtn = By.xpath("(//span[text()='Save'])[2]");

	public void performconsumerinventory() {
		driver.findElement(newtab).click();
		waitUntilElementVisibleBy(driver, Inventorysel, 120);
		driver.findElement(Inventorysel).click();
		tcConfig.updateTestReporter("SalesStorePage", "performconsumerinventory", Status.PASS,
				"Inventory selected successfully");
		driver.findElement(nexttab).click();
		waitUntilElementVisibleBy(driver, Quantityfield, 120);
		clickElementBy(Quantityfield);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(Quantityfield).sendKeys(testData.get("Quantity"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(Transactionfield);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement transactiotypesel = driver
				.findElement(By.xpath("//a[contains(.,'" + testData.get("Transaction Type") + "')]"));
		transactiotypesel.click();
		waitUntilElementVisibleBy(driver, GlCodefield, 120);
		clickElementBy(GlCodefield);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(GlCodefield).sendKeys(testData.get("GLCODE"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(Subtypefield);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement subtypesel = driver.findElement(By.xpath("//a[contains(.,'" + testData.get("Sub Type") + "')]"));
		subtypesel.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalesStorePage", "performconsumerinventory", Status.PASS,
				"Filled all required fields");
		clickElementBy(Savebtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().refresh();

	}

	public By qunonhndaft = By.xpath("(//span[@title='Quantity on Hand'])//..//span[@class='uiOutputNumber']");

	public void quantityonhandafter() {
		waitUntilElementVisibleBy(driver, qunonhndbef, 120);
		Quantityonhandaft = Integer.parseInt(driver.findElement(qunonhndbef).getText());

		if (Quantityonhandaft < Quantityonhandbef) {
			tcConfig.updateTestReporter("SalesStorePage", "quantityonhandafter", Status.PASS,
					"Inventory created and QOH reduced");
		} else {
			tcConfig.updateTestReporter("SalesStorePage", "quantityonhandafter", Status.FAIL,
					"Either inventory not created or QOH failed to update");
		}
	}

	public By clktransfertab = By.xpath("//span[text()='Transfers']");
	public By selNewTrans = By.xpath(
			"(//span[@title='Initiated Transfers'])/ancestor::article[@class='slds-card slds-card_boundary forceRelatedListCardDesktop']//div[@title='New']");

	public void click_Transfer() {
		waitUntilElementVisibleBy(driver, clktransfertab, 120);
		clickElementBy(clktransfertab);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(selNewTrans))) {
			tcConfig.updateTestReporter("SalesStorePage", "click_Transfer", Status.PASS, "Clicked on Transfer tab");
		} else {
			tcConfig.updateTestReporter("SalesStorePage", "click_Transfer", Status.FAIL,
					"Failed while clicking on Transfer tab");
		}

	}

	public By StorePremium = By.xpath("(//input[@title='Search Sales Store Premiums'])[2]");
	public By quanreq = By
			.xpath("//span[text()='Quantity Requested']/ancestor::div[@class='slds-form-element__control']//input");
	public By srcpeople = By
			.xpath("//span[text()='Sending Sales Store Contact']//..//..//input[@title='Search People']");
	public By status = By.xpath("//span[text()='Status']//..//..//a");
	public By contactrecsite = By
			.xpath("//span[text()='Receiving Site Contact']//..//..//input[@title='Search People']");
	public By Approver = By.xpath("//span[text()='Approver']//..//..//input[@title='Search People']");
	public By SAvebtntransfer = By.xpath("(//span[text()='Save'])[2]");

	public void selectnewInitiatedtransfer() {
		waitUntilElementVisibleBy(driver, selNewTrans, 120);
		clickElementBy(selNewTrans);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(StorePremium);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(StorePremium).sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(StorePremium).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(quanreq);
		driver.findElement(quanreq).sendKeys(testData.get("QuantityTransfer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(srcpeople);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(srcpeople).sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(srcpeople).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * clickElementBy(status); waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * WebElement substatus = driver.findElement(By.xpath("//a[contains(.,'" +
		 * testData.get("Sub Status") + "')]")); substatus.click();
		 */
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(contactrecsite);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(contactrecsite).sendKeys(Keys.ARROW_DOWN);
		// driver.findElement(contactrecsite).sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(contactrecsite).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(Approver);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(Approver).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(Approver).sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(Approver).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("SalesStorePage", "click_Transfer", Status.PASS,
				"All data for transfer was enterd successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(SAvebtntransfer).click();
		tcConfig.updateTestReporter("SalesStorePage", "click_Transfer", Status.PASS, "Transfer was successful");
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}


	
	/**
	 * 
	 * 
	 * Added by Ritam
	 */
	
	public void navigateToProduct()
	{
		try{
			waitUntilElementVisibleBy(driver, lnkParentSalesStore, 120);
			if(verifyObjectDisplayed(lnkParentSalesStore))
			{
				clickElementJSWithWait(lnkParentSalesStore);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver,lnkRelated, 120);
				if(verifyObjectDisplayed(lnkRelated))
				{
					clickElementBy(lnkRelated);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<WebElement> e = driver.findElements(By.xpath("//span[@title='Sales Store Premiums']"));
					scrollDownForElementJSWb(e.get(e.size()-1));
					scrollDownByPixel(100);
					waitUntilElementVisibleBy(driver, lnkViewAll, 120);
					if(verifyObjectDisplayed(lnkViewAll))
					{
						clickElementJS(lnkViewAll);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						waitUntilElementVisibleBy(driver, lnkPremium, 120);
						if(verifyObjectDisplayed(lnkPremium))
						{
							scrollDownForElementJSBy(lnkPremium);
							scrollUpByPixel(100);
							clickElementBy(lnkPremium);
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							tcConfig.updateTestReporter("SalesStorePage", "navigateToProduct", Status.PASS, "Opened premium:" + testData.get("Product"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
						else
						{
							tcConfig.updateTestReporter("SalesStorePage", "navigateToProduct", Status.FAIL, "Premium link is not visible");
						}
					}
					else
					{
						tcConfig.updateTestReporter("SalesStorePage", "navigateToProduct", Status.FAIL, "View All link is not visible");
					}
					
				}
				else
				{
					tcConfig.updateTestReporter("SalesStorePage", "navigateToProduct", Status.FAIL, "Related tab is not visible");
				}
			}
			else
			{
				tcConfig.updateTestReporter("SalesStorePage", "navigateToProduct", Status.FAIL, "Parent salesstore link is not visible");
			}
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("SalesStorePage", "navigateToProduct", Status.FAIL, "Getting error:" + e);
		}
	}

	private By btnEdit = By.xpath("//a[@title='Edit']");
	private By lnkOperatingHours = By
			.xpath("//span[text()='Information']/../../../div/div//a[contains(@class,'textUnderline')]");
	// private By lnkTourWaveAlcn = By.xpath("//span[text()='Tour Wave Slot
	// Allocation']");
	// private By lnkTourWaveAlcn = By.xpath("//a[@title='Tour Wave Slot
	// Allocation']");
	private By lnkTourWaveAlcn = By
			.xpath("//a[@title='Tour Wave Slot Allocation']//span[contains(text(),'Tour Wave')]");
	private By btnSelfServiceAllocation = By.xpath("//button[contains(text(),'Self Service Allocation')]");
	private By dropDwnWaves = By.xpath("//label[text()='Waves']/..//button");
	private By lnkDrpDwnValues = By.xpath("//li[@class='slds-dropdown__item']/a");
	private By txtBoxDates = By.xpath("//input[@class='slds-input input']");
	private By drpDwnDayOfWeek = By.xpath("//label[text()='Day of Week']/..//button");
	private By txtBoxTotalSlots = By.xpath("//input[@name='number']");
	private By drpDwnAllotmentType = By.xpath("//label[text()='Allotment Type']/..//button");
	private By btnGenTourSlots = By.xpath("//button[contains(text(),'Generate Tour Slots')]");
	private By divSuccessMsg = By.xpath("//div[contains(@class,'slds-modal__content')]/p");
	private By btnOk = By.xpath("//div[contains(@class,'slds-modal__content')]/..//button");
	private By listDayOfWeek = By.xpath("//label[text()='Day of Week']/../div/div[1]/div//a");
	private By listAllotmentType = By.xpath("//label[text()='Allotment Type']/../div/div[1]/div//a");


	
	
	/**
	 * 
	 * Added by Ritam
	 */
	
	public void navigateToTourSlotAllocation()
	{
		try{
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lnkTourWaveAlcn, 120);
			if(verifyElementDisplayed(driver.findElement(lnkTourWaveAlcn)))
			{
//			
			driver.findElement(lnkTourWaveAlcn).click();
			
			tcConfig.updateTestReporter("Salestore", "navigateToTourSlotAllocation", Status.PASS, "Navigate Sucess to Tour Slot Allocation" );
			
			}
			else{
				tcConfig.updateTestReporter("Salestore", "navigateToTourSlotAllocation", Status.FAIL, "Link to navigate to tour slot allocation failed");
				
			}
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("Salestore", "navigateToTourSlotAllocation", Status.FAIL, "Failed while navigating to Tour slot allocation page");
		}
	}

	public void tourSlotAllocation() {
		try {
			waitUntilElementVisibleBy(driver, btnSelfServiceAllocation, 120);
			clickElementBy(dropDwnWaves);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(driver.findElements(lnkDrpDwnValues).get(4));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String startDate = getFutureDate(Integer.parseInt(testData.get("AvailableDays")) + 15, "MMM dd, yyyy");
			Date d = new SimpleDateFormat("MMM dd, yyyy").parse(startDate);
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			c.add(Calendar.DATE, 7);
			Date d1 = c.getTime();
			SimpleDateFormat sd = new SimpleDateFormat("MMM dd, yyyy");
			String endDate = sd.format(d1);
			// String endDate =
			// getFutureDate(Integer.parseInt(testData.get("EndDateinDays"))+15,"MMM dd,
			// yyyy");
			driver.findElements(txtBoxDates).get(0).sendKeys(startDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElements(txtBoxDates).get(1).sendKeys(endDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(drpDwnDayOfWeek);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			int size = driver.findElements(listDayOfWeek).size();
			for (int i = 0; i < size; i++) {
				clickElementWb(driver.findElements(listDayOfWeek).get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
			String totalSlots = generateRandomString(1, Format.NUMBER_STRING.getFormat());
			fieldDataEnter(txtBoxTotalSlots, totalSlots);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(drpDwnAllotmentType);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(driver.findElements(listAllotmentType).get(2));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(lnkTourWaveAlcn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (driver.findElement(btnGenTourSlots).isEnabled()) {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.PASS,
						"Generate tour slots button is getting enabled after filling all the details");
			} else {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.FAIL,
						"Generate tour slots button is not getting enabled after filling all the details");
			}

			clickElementJSWithWait(btnGenTourSlots);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWb(driver, driver.findElements(divSuccessMsg).get(0), 120);
			if (verifyElementDisplayed(driver.findElements(divSuccessMsg).get(0))) {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.PASS,
						"Tour slots are allocated successfully");
				clickElementWb(driver.findElements(btnOk).get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.FAIL,
						"Failed to allocate tour slots");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.FAIL, "Getting error" + e);
		}
	}

	/**
	 * This method is to validate if the Payment Integration Check Box is available
	 * or not in the Sales Store Details Screen
	 * 
	 * @author C40122
	 * @throws Exception
	 */

	public void verifyPaymentIntegrationCheckbox() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(paymentIntegrationAvailabilityChkbx.isDisplayed());
		tcConfig.updateTestReporter("SalesStoreDetailsPage", "verifyPaymentIntegrationCheckbox", Status.PASS,
				"Presence of Payment Integration Checkbox Validated Successfully");

	}

	/**
	 * This method is to validate if the Refundable Tour Deposit Check Box is
	 * available or not in the Sales Store Details Screen
	 * 
	 * @author C40122
	 * @throws Exception
	 */

	public void verifyRefundableTourDepositCheckbox() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(refndbleTourDepositChkbx.isDisplayed());
		tcConfig.updateTestReporter("SalesStoreDetailsPage", "verifyRefundableTourDepositCheckbox", Status.PASS,
				"Presence of Refundable Tour Deposit Checkbox Validated Successfully");

	}

	/**
	 * This method is to click on the Edit Refundable Tour Deposit Allowed Button
	 * 
	 * @author C40122
	 * @throws Exception
	 */

	public void clickonEditRefndbleDpstBtn() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(editRefndbleTourDepositBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void verifyRefndbleTourDepositisEdtble() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String statusBeforeEdit = refndbleTourDepositChkbx.getAttribute("alt");

		clickonEditRefndbleDpstBtn();
		clickElementJSWithWait(editRefndbleTourDepositChkbx);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(saveButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String statusAfterEdit = refndbleTourDepositChkbx.getAttribute("alt");

		Assert.assertNotEquals(statusBeforeEdit, statusAfterEdit);
		tcConfig.updateTestReporter("SalesStoreDetailsPage", "verifyRefndbleTourDepositisEdtble", Status.PASS,
				"Refundable Tour Deposit Checkbox is editable");

	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : searchSalesStore()
	 * @Description : This function is enter Parent Sales Store in search box &
	 *              search
	 * @Test Data : ParentSalesS
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void searchSalesStore() {
		String strParentSalesStore = testData.get("ParentSalesS");
		String parentStore = "//a[@title='" + strParentSalesStore + "']";
		Actions action = new Actions(driver);

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementLocated(driver, searchParent, 120);
		fieldDataEnter(searchParent, strParentSalesStore);
		action.sendKeys(Keys.ENTER).build().perform();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement wbel = driver.findElement(By.xpath(parentStore));
		if (verifyElementDisplayed(wbel)) {
			tcConfig.updateTestReporter("SalesStorePage", "searchSalesStore", Status.PASS,
					"Successfully searched Parent Sales Store");
		} else {
			tcConfig.updateTestReporter("SalesStorePage", "searchSalesStore", Status.FAIL,
					"Failed searched Parent Sales Store");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : selectSSCheckMC()
	 * @Description : This function is select the particular Parent Sales Store &
	 *              validate Send Email from MC is checked
	 * @Test Data : ParentSalesS
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void selectSSCheckMC() {
		try {
			String strParentSalesStore = testData.get("ParentSalesS");
			String parentStore = "//a[@title='" + strParentSalesStore + "']";
			WebElement parentSalesS = driver.findElement(By.xpath(parentStore));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, By.xpath(parentStore), 120);
			parentSalesS.click();
			tcConfig.updateTestReporter("SalesStorePage", "selectSSCheckMC", Status.PASS,
					"Successfully clicked Sales Store");
			// navigate to sales store details page
			waitUntilElementVisibleBy(driver, MClabel, 120);
			if (verifyElementDisplayed(driver.findElement(checkBoxMC))) {
				tcConfig.updateTestReporter("SalesStorePage", "selectSSCheckMC", Status.PASS,
						"Successfully verified checked Send Emails from MC");
			} else {
				driver.findElement(editMCButton).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(editMCCheckBox).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(saveButtonforSFMC);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("SalesStorePage", "selectSSCheckMC", Status.PASS,
						"Successfully checked Send Emails from MC");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("SalesStorePage", "selectSSCheckMC", Status.FAIL,
					"Failed while clicking on Sales Store");
		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : searchChildSalesStore()
	 * @Description : This function is enter Child Sales Store in search box &
	 *              search
	 * @Test Data : SalesStore
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void searchChildSalesStore() {
		String strSalesStore = testData.get("SalesStore");
		String salesStore = "//a[@title='" + strSalesStore + "']";
		Actions action = new Actions(driver);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementLocated(driver, searchParent, 120);
		fieldDataEnter(searchParent, strSalesStore);
		action.sendKeys(Keys.ENTER).build().perform();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement wbel = driver.findElement(By.xpath(salesStore));
		if (verifyElementDisplayed(wbel)) {
			tcConfig.updateTestReporter("SalesStorePage", "searchSalesStore", Status.PASS,
					"Successfully searched Child Sales Store");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			wbel.click();
			// wait till Edit button is located
			waitUntilElementLocated(driver, editButton, 120);
			tcConfig.updateTestReporter("SalesStorePage", "searchSalesStore", Status.PASS,
					"Successfully navigated to Child Sales Store details page");
		} else {
			tcConfig.updateTestReporter("SalesStorePage", "searchSalesStore", Status.FAIL,
					"Failed searched Child Sales Store");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : editChildSSAddInst()
	 * @Description : This function is enter Child Sales Store in search box &
	 *              search
	 * @Test Data : SalesStore
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void editChildSSAddInst() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String instTxt = "Yes needed";
		WebElement ele = driver.findElement(editButton);
		clickElementJS(ele);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalesStorePage", "editChildSSAddInst", Status.PASS,
				"Successfully clicked on Edit button");
		pageDown();
		waitUntilElementLocated(driver, journeyLbl, 120);
		driver.findElement(journeyTxt).clear();
		fieldDataEnter(journeyTxt, instTxt);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(savePop).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalesStorePage", "editChildSSAddInst", Status.PASS,
				"Successfully Added Text in Breakfast Checin Instructions");
		pageDown();
		// waitUntilElementLocated(driver, journeySaved, 120);
		String text = driver.findElement(journeySaved).getText();
		if (text.equalsIgnoreCase(instTxt)) {

			tcConfig.updateTestReporter("SalesStorePage", "editChildSSAddInst", Status.PASS,
					"Successfully edited the Journey Breakfast Instructions field");
		} else {
			tcConfig.updateTestReporter("SalesStorePage", "editChildSSAddInst", Status.FAIL,
					"Failed to edit the Journey Breakfast Instructions field");
		}

	}

}