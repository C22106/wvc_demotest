package salesforce.pages;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalesforceBasePage extends FunctionalComponents {

	public SalesforceBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	public void waitUntilElementVisibleIE(WebDriver driver, WebElement locator, long timeOutInSeconds) {
		for (long i = 0; i < timeOutInSeconds / 2; i++) {

			if (locator.isDisplayed()) {

				waitForSometime("2");
				break;
			} else {

				System.out.println(locator + " Element not visiable");
			}

		}

	}

	public void waitUntilElementVisibleIE(WebDriver driver, By by, long timeOutInSeconds) {
		boolean flag = false;

		for (long i = 0; i < timeOutInSeconds / 2; i++) {
			try {

				if (driver.findElement(by).isDisplayed()) {
					flag = true;
					waitForSometime("2");
					break;
				} else {

					System.out.println(by + " Element not visiable");
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				System.out.println(by + " Element not found");
				waitForSometime("2");
			}

		}
		if (!flag) {

			System.out.println("I am catched here!!!");
			driver.navigate().refresh();

			try {
				driver.switchTo().alert();
				waitForSometime("10");
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	public enum Format {
		ALPHA_NUMERIC_STRING("ABCDEFGHI0123456789@#$%!"), NUMERIC_STRING("0123456789"),
		CHARACTER_STRING("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"), NUMBER_STRING("123456789");

		private String format;

		Format(String fmat) {
			this.format = fmat;
		}

		public String getFormat() {
			return format;
		}
	}

	public void waitUntilElementVisibleWb(WebDriver driver, WebElement locator, long timeOutInSeconds) {
		for (long i = 0; i < timeOutInSeconds / 2; i++) {

			if (locator.isDisplayed()) {

				waitForSometime("2");
				break;
			} else {

				System.out.println(locator + " Element not visiable");
			}

		}

	}

	// @FindBy(xpath="//div[@class='loading-wrap']") WebElement loadingSpinner;
	By loadingSpinner = By.xpath("//div[@class='loader__tick']");

	public void checkLoadingSpinner() {
		waitUntilElementInVisible(driver, loadingSpinner, 60);
	}

	public void clickElementJSWithWait(WebElement wbel) {
		// wbel.click();

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("var elem=arguments[0];setTimeout(function(){elem.click();},100)", wbel);

	}

	public void clickElementJSWithWait(By by) {
		// driver.findElement(by).click();

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("var elem=arguments[0];setTimeout(function(){elem.click();},100)",
				driver.findElement(by));

	}

	public String getTextFromBodyBy(By by) {
		String text = "";
		try {
			text = driver.findElement(by).getText();
			log.info("The Text from the element is:" + text);

		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return text;
	}

	public String getRandomStringEmail() {

		String str = "abcdefghijklmnopqrstuvwxyz";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 4) { // length of the random string.
			int index = (int) (rnd.nextFloat() * str.length());
			salt.append(str.charAt(index));
		}
		String saltStr = salt.toString();

		String email = saltStr + "@wyn.com";

		return email;
	}

	public String getRandomStringText(int text) {
		String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < text) { // length of the random string.
			int index = (int) (rnd.nextFloat() * str.length());
			salt.append(str.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
		// System.out.println(saltStr);
		// testData.put("", "");
	}

	public boolean isElementPresent(By locatorKey) {
		try {
			driver.findElement(locatorKey);
			return true;
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}

	public void waitUntilElementVisibleBy(WebDriver driver, By by, long timeOutInSeconds) {
		boolean flag = false;

		for (long i = 0; i < timeOutInSeconds / 2; i++) {
			try {

				if (driver.findElement(by).isDisplayed()) {
					flag = true;
					waitForSometime("2");
					break;
				} else {

					System.out.println(by + " Element not visible");
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				System.out.println(by + " Element not found");
				waitForSometime("2");
			}

		}
		if (!flag) {

			System.out.println("I am catched here!!!");
			driver.navigate().refresh();

			try {
				driver.switchTo().alert();
				waitForSometime("10");
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	public void clickElementJS(WebElement wbel) {

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", wbel);

		// wbel.click();
	}

	public void pageDown() {
		driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
	}

	public void clearallStaticVariable() {
		if (LeadsPage.createdTourID != null) {
			LeadsPage.createdTourID = null;
		}
	}
}
