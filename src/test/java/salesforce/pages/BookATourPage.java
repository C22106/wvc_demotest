package salesforce.pages;

import java.awt.AWTException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BookATourPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(BookATourPage.class);

	public BookATourPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String createdPID;
	public static String createdTourID;
	static final String SOURCE="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	JavascriptExecutor js = (JavascriptExecutor) driver;

	protected By paymentRecord = By.xpath("(//table[@class='wn-premium_table slds-table slds-table_cell-buffer']//tbody//td[contains(.,'Deposit')]/..//a)[1]");
    protected By btnRefund = By.xpath("//button[@title='Refund']");
    protected By btnOfflineRefund = By.xpath("//button[@title='Offline Refund']"); 
    protected By lstLineItems = By.xpath("(//span[@class='slds-checkbox_faux'])[2]");
    protected By qtyToRefund = By.xpath("(//input[@class='slds-input qtySelect'])[1]");
    protected By refundBtn = By.xpath("//button[contains(.,'Refund') and contains(@class,'slds-button slds-button_neutral ')]");
    protected By txtOrderID = By.xpath("//span[contains(.,'Order ID')]/../..//span[@class='uiOutputText']");
    protected By imgHome = By.xpath("//img[contains(@src,'Home')]");
    protected By btnclose = By.xpath("//button[@title='Close']//lightning-primitive-icon");
    protected By btnAddIncentives = By.xpath("//button[contains(.,'Add Incentive')]");
    protected By txtSearchProduct = By.xpath("//input[@placeholder='Search Product']");
    protected By chkCheckbox = By.xpath("//label[@class='slds-checkbox_faux']");
    protected By addBtn=By.xpath("//button[text()='Add']");
    protected By productRecord = By.xpath("//tr[@class='slds-hint-parent']//th//div");
    protected By incentiveAmountAdd = By.xpath("//input[@placeholder='Enter Amount']");
    protected By chkPaymentMethod = By.xpath("(//select[@name='PaymentMethod'])[1]");
    protected By chkRefundableOption = By.xpath("//input[contains(@class,'refundableCheckbox uiInput uiInputCheckbox uiInput')]");
    protected By txtRefundableDeposit = By.xpath("//td[contains(.,'Refundable Tour Deposit')]");
    protected By warningMsg = By.xpath("//div[@class='slds-modal__content slds-p-around_medium']");
    protected By btnProcced = By.xpath("//button[@title='Proceed']");
//    protected By btnConfirm = By.xpath("//div[@class='confirm-button-container']");

    protected By txtClover = By.xpath("//input[@placeholder='Search for Clover']");
    protected By connectClover = By.xpath("//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']");
    //protected By txtEmail = By.xpath("//span[contains(text(),'Email')]/following::input[@type='text']");
    
    //protected By txtMobile = By.xpath("(//span[contains(text(),'Mobile#')]/following::input[@type='text'])[1]");
    
    protected By waitMsg = By.xpath("//div[@class='slds-clearfix']//p[contains(text(),'')]");
    protected By btnOfflinePayment = By.xpath("//button[@title='Offline Payment']");
    protected By btnRetry = By.xpath("//button[@title='Retry']");
    protected By btnCompleted = By.xpath("//button[@title='Completed']");
    //protected By drpCarrier = By.xpath("//select[@class='slds-select']");
    
    protected By associatedPayments = By.xpath("//h2[contains(.,'Associated Payments')]");
    protected By paymentId = By.xpath("//table[@class='wn-premium_table slds-table slds-table_cell-buffer']//td//a");
	 protected By transactStatus = By.xpath("//table[contains(@class,'wn-premium_table slds-table')]//td[2]");
    
    
protected By CancelButton=By.xpath("//button[@title='cancel tour']");
public static String lead1;
	private By drpBookingLocation = By.xpath("((//section[@class='main-content'])[1]//span[text()='Booking Location']//..//..)[1]//select");
	private By drpBrand = By.xpath("((//section[@class='main-content'])[1]//span[text()='Brand']//..//..)[1]//select");
	private By drpLanguage= By.xpath("((//section[@class='main-content'])[1]//span[text()='Language']//..//..)[1]//select");
	private By drpMarketingStatergy = By.xpath("((//section[@class='main-content'])[1]//span[text()='Marketing Strategy']//..//..)[1]//select");
	private By drpMarketingSourceType  =By.xpath("((//section[@class='main-content'])[1]//span[text()='Marketing Source Type']//..//..)[1]//select");
	private By drpTourType =By.xpath("((//section[@class='main-content'])[1]//label[text()='Tour Type']//..//..)[1]//select"); 
	private By txtNoOfPeople =By.xpath("((//section[@class='main-content'])[1]//label[text()='# of people']//..//..)[1]//input");
	
	private By availableTime = 
			By.xpath("((//div[text()='Available Time'])[1]/..//button[@class = 'time-btn']//span[not(contains(@class,'disable'))])[1]");
	//private By availableTime = 
			//By.xpath("(//div[@class='time-button-container']/button[@class='time-btn']//span[text()='8:30 AM'])[1]");
	
	private By btnTourLocation = By.xpath("((//section[@class='main-content'])[1])//button[@class='confirm-btn-black']");
	//private By btnTourLocation = By.xpath("(//button[@class='confirm-btn-black'])[1]");
	private By btnNextIncentive= By.xpath("//button[text()='Next']");
	private By btnNextPayments = By.xpath("//button[text()='Next']");

	
	
	public void createTour() {
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpBookingLocation, 120);
		Select drpBookingLoc = new Select(driver.findElement(drpBookingLocation));
		drpBookingLoc.selectByVisibleText(testData.get("BookingLocation"));
		
		Select band = new Select(driver.findElement(drpBrand));
		band.selectByVisibleText(testData.get("Brand"));
		
		Select tourType = new Select(driver.findElement(drpTourType));
		tourType.selectByVisibleText(testData.get("TourType"));
		
		fieldDataEnter(txtNoOfPeople, testData.get("NoOfPeople"));
		
		Select language = new Select(driver.findElement(drpLanguage));
		language.selectByVisibleText(testData.get("Language"));
		
		Select marketingStrategy = new Select(driver.findElement(drpMarketingStatergy));
		marketingStrategy.selectByVisibleText(testData.get("MarketingStatergy"));
		
		waitUntilElementVisibleBy(driver, drpMarketingSourceType, 120);
		Select marketingSourceType = new Select(driver.findElement(drpMarketingSourceType));
		marketingSourceType.selectByVisibleText(testData.get("MarketingSourceType"));
		
		WebElement availableTimeSlot = driver.findElement(availableTime);
		availableTimeSlot.click();
		
		WebElement btnTourLoc = driver.findElement(btnTourLocation);
		btnTourLoc.click();
		
		waitUntilElementVisibleBy(driver, btnNextIncentive, 120);
		WebElement btnIncentive = driver.findElement(btnNextIncentive);
		btnIncentive.click();
		
		waitUntilElementVisibleBy(driver, btnNextIncentive, 120);
		WebElement btnPayments = driver.findElement(btnNextPayments);
		btnPayments.click();
		
	}
	
	private By btnConfirm = By.xpath("//div[@class='confirm-button-container']");
	
    public void tourConfirmation(){
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnConfirm, 120);
		
		List<WebElement> tourConfirm = driver.findElements(btnConfirm);
		
		for (int i=0; i<tourConfirm.size(); i++){
		
			tourConfirm.get(i).click();
		}
		
	}
    
    private By canvas = By.xpath("//canvas[contains(@id,'signature-pad')]");
    private By submit = By.xpath("//button[text()='CONFIRM & BOOK']");
    private By txtMarketingSupervisor= By.xpath("(//input[@role='combobox'])[2]");
    private By txtMarketingManager =By.xpath("(//input[@role='combobox'])[3]");
	private By btnCnfirm  = By.xpath("//button[text()='Confirm']");
	
	
	
	public void SignatureValidate(){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		waitUntilObjectVisible(driver,canvas,120);
        Actions builder = new Actions(driver);
        WebElement canvasElement = driver.findElement(canvas);
        Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
                      .moveByOffset(50, 20).release().build();
        drawAction.perform();
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementBy(submit);
        tcConfig.updateTestReporter("TourRecordsPage", "SignatureValidate", Status.PASS, 
				"Signed successfully and clicked on submit button");
        
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementBy(submit);
        
        waitForSometime(tcConfig.getConfig().get("LowWait"));       
        fieldDataEnter(txtMarketingSupervisor, testData.get("MarketingSupervisor"));
        fieldDataEnter(txtMarketingManager, testData.get("MarketingManager"));
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementBy(btnCnfirm);
        waitForSometime(tcConfig.getConfig().get("LowWait"));
      //  driver.quit();
        
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
        
	}
	
	protected By drpCarrier = By.xpath("//select[@class='slds-select']");
	protected By txtSendEmail = By.xpath("//span[@class='slds-form-element__label' and contains(.,'Send Email')]");
	protected By chkSendEmail = By.xpath("(//span[@class='slds-checkbox_faux'])[1]");
	protected By chkSendText = By.xpath("(//span[@class='slds-checkbox_faux'])[2]");
	protected By txtSendText = By.xpath("//span[@class='slds-form-element__label' and contains(.,'Send Text')]");
	protected By btnSend = By.xpath("//button[contains(.,'Send')]");
	protected By confirmBtn= By.xpath("//button[contains(.,'Confirm')]");
	protected By paymentcontactPage=By.xpath("//h3[text()='Payment Contact Details']");
	protected By cancelBtn=By.xpath("//div[@class='slds-align_absolute-center']/button[text()='Cancel']");
	
	public void paymentContactDetails(){
	try {
		 waitForSometime(tcConfig.getConfig().get("MedWait"));
		    waitUntilElementVisibleBy(driver, paymentcontactPage, 120);
		    if(verifyObjectDisplayed(paymentcontactPage))
		    {
			waitUntilElementVisibleBy(driver, drpCarrier, 120);
			 Select s = new Select(driver.findElement(drpCarrier));
			 s.selectByVisibleText("Verizon");
			 waitForSometime(tcConfig.getConfig().get("LowWait"));
			 waitUntilElementVisibleBy(driver, txtSendEmail, 120);
			 WebElement chk=driver.findElement(chkSendEmail); 
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Actions actChk=new Actions(driver);
				actChk.moveToElement(chk).click().build().perform(); 
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				 waitUntilElementVisibleBy(driver, txtSendText, 120);
				 WebElement text=driver.findElement(chkSendText); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Actions actChkText=new Actions(driver);
					actChkText.moveToElement(text).click().build().perform(); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					 waitUntilElementVisibleBy(driver, btnSend, 120);
					if(verifyObjectDisplayed(btnSend)){						
					clickElementJSWithWait(btnSend);
					tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails", Status.PASS, "Payment contact details are saved sucessfully");
					}
					else{
						tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails", Status.FAIL, "Failed wihle adding Payment contact details" );
					}
					
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					 waitUntilElementVisibleBy(driver, confirmBtn, 120);
					 if(verifyObjectDisplayed(confirmBtn)){
						clickElementJSWithWait(confirmBtn);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails", Status.PASS, "Payment contact confirm sucessfully");
						
					 }else{
						 tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails", Status.FAIL, "Payment contact confirm not sucessfully");
							
					 }
		    }else{
		    	tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails", Status.FAIL, "Payment contact confirm not display sucessfully");
				
		    }
		} catch (Exception e) {
			tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails", Status.FAIL, "Getting error:" + e);
			throw e;
		}					
	}

	private By drpMarketingSubSource = By.xpath("(//span[contains(text(),'Marketing Sub Source')])[1]/following::div[1]//select");
    private By drpMarketingCampaign = By.xpath("(//span[contains(text(),'Marketing Campaign')])[1]/following::div[1]//select");
    public By date = By.xpath("(//input[@name='input1'])[1]");
    private By btnNext = By.xpath("//section[@class='main-content time-btn-active1']//button[contains(text(),'Next')]");
    public void createTourMobile()
    {
           waitForSometime(tcConfig.getConfig().get("LowWait"));
           waitUntilElementVisibleBy(driver, drpBookingLocation, 120);
           String pattern = "MMM dd, YYYY";
           SimpleDateFormat simpleDateFormat =
                   new SimpleDateFormat(pattern);
           Date date1 = new Date();
           String curdate = simpleDateFormat.format(date1);
           System.out.println(curdate);
           
           driver.findElement(date).clear();
           driver.findElement(date).sendKeys(curdate);
           
          // String noOfPeople = getRandomString(1);
     driver.findElement(txtNoOfPeople).sendKeys("1");
     waitForSometime(tcConfig.getConfig().get("LowWait"));
           //fieldDataEnter(txtNoOfPeople, testData.get("NoOfPeople"));
           
           /*Select language = new Select(driver.findElement(drpLanguage));
           language.selectByVisibleText(testData.get("Language"));*/
           
           Select marketingStrategy = new Select(driver.findElement(drpMarketingStatergy));
         //  marketingStrategy.selectByVisibleText(testData.get("MarketingStatergy"));
           marketingStrategy.selectByIndex(1);
           
           waitUntilElementVisibleBy(driver, drpMarketingSourceType, 120);
           Select marketingSourceType = new Select(driver.findElement(drpMarketingSourceType));
          // marketingSourceType.selectByVisibleText(testData.get("MarketingSourceType"));
           marketingSourceType.selectByIndex(1);
           
          /* Select marketingSubSource = new Select(driver.findElement(drpMarketingSubSource));
          // marketingSubSource.selectByVisibleText(testData.get("MarketingSupervisor"));
           marketingSubSource.selectByIndex(1);*/
           
           Select marketingCampaign = new Select(driver.findElement(drpMarketingCampaign));
          // marketingCampaign.selectByVisibleText(testData.get("MarketingManager"));
           marketingCampaign.selectByIndex(1);
           
           WebElement availableTimeSlot = driver.findElement(availableTime);
           availableTimeSlot.click();
           
          WebElement btnTourLoc = driver.findElement(btnTourLocation);
           btnTourLoc.click();
           
           waitUntilElementLocated(driver, btnNext, 120);
           if(driver.findElement(btnNext).isEnabled()){
                  clickElementBy(btnNext);
                  waitForSometime(tcConfig.getConfig().get("MedWait"));
                  
           }
           
           /*waitUntilElementVisibleBy(driver, btnNextIncentive, 120);
           WebElement btnIncentive = driver.findElement(btnNextIncentive);
           btnIncentive.click();*/
           waitForSometime(tcConfig.getConfig().get("LowWait"));
           waitUntilElementVisibleBy(driver, btnNextIncentive, 120);
           clickElementJSWithWait(btnNextIncentive);
           waitForSometime(tcConfig.getConfig().get("MedWait"));       
        }
    
   
    private By txtEmail = By.xpath("//span[contains(text(),'Email')]/following::input[@type='text']");
    private By txtMobile = By.xpath("(//span[contains(text(),'Mobile#')]/following::input[@type='text'])[1]");


    public void ValidateSignature()
    {
    JavascriptExecutor jse = (JavascriptExecutor)driver;
    jse.executeScript("window.scrollBy(0,500)", "");
    waitUntilObjectVisible(driver,canvas,120);
        Actions builder = new Actions(driver);
        WebElement canvasElement = driver.findElement(canvas);
        Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
                      .moveByOffset(50, 20).release().build();
        drawAction.perform();
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        
        String s= getRandomString(10);
        driver.findElement(txtMobile).sendKeys(s);
        
        fieldDataEnter(txtEmail, testData.get("Email"));
        waitForSometime(tcConfig.getConfig().get("MedWait"));
        
        clickElementJSWithWait(submit);
        tcConfig.updateTestReporter("TourRecordsPage", "SignatureValidate", Status.PASS, 
    "Signed successfully and clicked on submit button");
        waitForSometime(tcConfig.getConfig().get("MedWait"));
        
      
    }
    private By txtTitle = By.xpath("//span[contains(text(),'Title')]/following::input[@type='text']");
    private By txtDescription = By.xpath("//span[contains(text(),'Description')]/following::textarea[@role='textbox']");
    private By btnAddNote = By.xpath("//button[contains(text(),'ADD')]");
    private By txtNoteMsg = By.xpath("//span[contains(text(),'NOTE ADDED SUCCESSFULLY')]");
    private By btnConfirmNote = By.xpath("//button[contains(text(),'Confirm')]");
    private By drpTourHome = By.xpath("//select[@class='slds-select']");
    private By btnCreateGuest = By.xpath("//button[contains(text(),'CREATE GUEST')]");
    private By tblTours = By.xpath("//td[@data-label='name']//a[@class='table_col_name']");
    private By tabDetails = By.xpath("//a[contains(text(),'Details')]");
    private By tabTours = By.xpath("//a[contains(text(),'Tours (1)')]");
    private By lnkTourID = By.xpath("//span[contains(text(),'Tour Number')]/following::div[1]/a");
    private By btncancelTour = By.xpath("//button[@title='cancel tour']");
    public void validateAddTourNotes() throws AWTException
    {
    waitUntilObjectVisible(driver,txtTitle,120);
    driver.findElement(txtTitle).sendKeys(randomAlphaNumeric(3));
    //fieldDataEnter(txtTitle, testData.get("Title"));
    waitForSometime(tcConfig.getConfig().get("MedWait"));
    //fieldDataEnter(txtDescription, testData.get("Description"));
    driver.findElement(txtDescription).sendKeys(randomAlphaNumeric(3));
    waitForSometime(tcConfig.getConfig().get("MedWait"));
    clickElementJSWithWait(btnAddNote);
    waitForSometime(tcConfig.getConfig().get("LowWait"));
    if(verifyElementDisplayed(driver.findElement(txtNoteMsg)))
    {
      clickElementJSWithWait(btnConfirmNote);                
      waitForSometime(tcConfig.getConfig().get("LowWait"));
    }
    else{
      tcConfig.updateTestReporter("TourRecordsPage", "validateTourBook", Status.FAIL, "Failed while clicking notes");
    }
    waitForSometime(tcConfig.getConfig().get("MedWait"));
    waitUntilObjectVisible(driver,btnCreateGuest,120);
    waitForSometime(tcConfig.getConfig().get("MedWait"));

    }
    
    public static String randomAlphaNumeric(int count) {
    	StringBuilder builder = new StringBuilder();
    	while (count-- != 0) {
    	int character = (int)(Math.random()*SOURCE.length());
    	builder.append(SOURCE.charAt(character));
    	}
    	return builder.toString();
    	} 

    private By btnDispositionSatus = By.xpath("//span[contains(text(),'Disposition Details')]");
	private By drpEditDidTheyTour = By.xpath("//button[@title='Edit Did They Tour?']");
	private By didTheyTourDrpDwn=By.xpath("(//span[text()='Did They Tour?'])[2]/../../..//a[@class='select']");
	private By drpValue = By.xpath("//a[@title='No']");
	private By drpDispositionReason = By.xpath("(//span[contains(text(),'Disposition Reason')])[3]/following::div[2]//a[contains(text(),'--None--')]");
	//private By drpDispositionStatus = By.xpath("//li//a[@title='No Show']");
	private By drpDispositionStatus = By.xpath("(//a[text()='No Show'])[1]");
	private By btnSaveDispositon = By.xpath("//span[contains(text(),'Save')]");
	private By lnkDisposition = By.xpath("(//span[contains(text(),'Dispositioned')]/../..)[2]");
	public void verifyTourDisposition(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));		
		JavascriptExecutor Scrool = (JavascriptExecutor) driver;
		Scrool.executeScript("window.scrollBy(0,1400)", "");	
		waitUntilElementVisibleBy(driver, btnDispositionSatus, 120);
		if(verifyElementDisplayed(driver.findElement(btnDispositionSatus)))
		{
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(drpEditDidTheyTour);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(didTheyTourDrpDwn);			
			waitForSometime(tcConfig.getConfig().get("LowWait"));			
			driver.findElement(drpValue).click();			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(drpDispositionReason);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(drpDispositionStatus);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnSaveDispositon);	
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		else
		{
			tcConfig.updateTestReporter("BookATourPage", "validateTourDisposition", Status.FAIL, "Failed while dispositon tour");
		}
		JavascriptExecutor je = (JavascriptExecutor) driver;
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -800)", "");	
		String color = driver.findElement(lnkDisposition).getCssValue("background-color");
		System.out.println(color);
		String hexcol = Color.fromString(color).asHex();
		System.out.println(hexcol);
		
		if(!hexcol.equals("")){
			tcConfig.updateTestReporter("BookATourPage", "validateTourDisposition", Status.PASS, "Tour is dispositon successfully");
		}
		else
		{
			tcConfig.updateTestReporter("BookATourPage", "validateTourDisposition", Status.FAIL, "Failed while dispositon tour");
		}
	}
	public By lnkDiscoveryTour = By.xpath("(//span[contains(text(),'Discovery Tour')])[1]");
	public By btnAdd = By.xpath("//button[contains(text(),'Add')]");
	public By lnkChildTour = By.xpath("//span[@title='Child Tour Records']/following-sibling::span");
	public By txtErrorMsg = By.xpath("//span[text()='Discovery Tour has already been created for this Tour Record.']");
	
	public void validateDiscoveryTour(){
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lnkDiscoveryTour, 120);
		clickElementJS(lnkDiscoveryTour);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnAdd, 120);
		clickElementJS(btnAdd);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lnkChildTour, 60);
		String childLinkTour=driver.findElement(lnkChildTour).getText();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if(childLinkTour.equalsIgnoreCase("(1)")){
			tcConfig.updateTestReporter("BookATourPage", "validateDiscoveryTour", Status.PASS, "Child Tour is created successfully");
		}
		else
		{
			tcConfig.updateTestReporter("BookATourPage", "validateTourDisposition", Status.FAIL, "Failed while creating Child Tour");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnAdd);
		
		waitUntilElementInVisible(driver, txtErrorMsg, 120);
		if(verifyObjectDisplayed(txtErrorMsg))
		{
		String errormsgDiscovery=driver.findElement(txtErrorMsg).getText();
		
		if(errormsgDiscovery.equalsIgnoreCase("Discovery Tour has already been created for this Tour Record.")){
			tcConfig.updateTestReporter("BookATourPage", "validateDiscoveryTour", Status.PASS, "Error message generates ");
		}else
		{
			tcConfig.updateTestReporter("BookATourPage", "validateDiscoveryTour", Status.FAIL, "Failed : error message is not visible");
		}
		}
	}
	
	/********************Jyoti*************************************/
	
	
	/*
     * Method-Add incentives and Payment
     * Designed By-JYoti
     * */
     public void addIncentivesandPayments(){
            
                         waitUntilElementVisibleBy(driver, btnAddIncentives, 120);
                         clickElementBy(btnAddIncentives);
                         waitForSometime(tcConfig.getConfig().get("LowWait"));
                         String strproductName = testData.get("Product");
                         fieldDataEnter(txtSearchProduct,strproductName);
                         waitForSometime(tcConfig.getConfig().get("MedWait"));
                         waitUntilElementVisibleBy(driver, chkCheckbox, 120);
                         waitForSometime(tcConfig.getConfig().get("LowWait"));
                         clickElementBy(chkCheckbox);
                         waitForSometime(tcConfig.getConfig().get("LowWait"));
                         waitUntilElementVisibleBy(driver, addBtn, 120);
                         clickElementBy(addBtn);
                         waitForSometime(tcConfig.getConfig().get("LowWait"));
                         waitUntilElementVisibleBy(driver, incentiveAmountAdd, 120);
                         String stramountAdd = getRandomString(4);
                         driver.findElement(incentiveAmountAdd).sendKeys(stramountAdd);
                         
                          Select s = new Select(driver.findElement(chkPaymentMethod));
                         s.selectByVisibleText("Credit Card");
                         waitForSometime(tcConfig.getConfig().get("LowWait"));
                             waitUntilElementVisibleBy(driver, txtRefundableDeposit, 120);
                             waitForSometime(tcConfig.getConfig().get("LowWait"));
                                WebElement chk=driver.findElement(chkRefundableOption); 
                                waitForSometime(tcConfig.getConfig().get("LowWait"));
                                Actions actChk=new Actions(driver);
                                actChk.moveToElement(chk).click().build().perform(); 
                                waitForSometime(tcConfig.getConfig().get("LowWait"));
                         
                                WebElement btnIncentive = driver.findElement(btnNextIncentive);
                             btnIncentive.click();
                             waitForSometime(tcConfig.getConfig().get("LowWait"));
                             waitUntilElementVisibleBy(driver, btnNextIncentive, 120);
                             clickElementJSWithWait(btnNextIncentive);
                             //clickElementBy(btnNextIncentive);
                             waitForSometime(tcConfig.getConfig().get("LowWait"));
                             waitUntilElementVisibleBy(driver, warningMsg, 120);
                             if(verifyObjectDisplayed(btnProcced)){                        
                             clickElementBy(btnProcced);
                             waitForSometime(tcConfig.getConfig().get("LowWait"));
                             tcConfig.updateTestReporter("BookATourPage", "addIncentivesandPayments", Status.PASS, "Incentives added sucessfully");
                             }
                             else{
                                 tcConfig.updateTestReporter("BookATourPage", "addIncentivesandPayments", Status.FAIL, "Failed while adding incentives");
                             }
                   
             
     }


/*
     * Method-Add Clover Device
     * Designed By-JYoti
     * */
     public void addCloverDevice(){
            waitUntilElementVisibleBy(driver, txtClover, 120);
            String strcloverDevice = testData.get("CloverDevice");
            fieldDataEnter(txtClover,strcloverDevice);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            clickElementBy(connectClover);
            waitForSometime(tcConfig.getConfig().get("LowWait"));        
     }


/*
     * Method-Verify Offline Payment and Retry button is displyed
     * Designed By-JYoti
     * */
     
     public void verifyTwoButtonsPresent(){
     try {
                   waitForSometime(tcConfig.getConfig().get("MedWait"));
                   waitUntilElementVisibleBy(driver, btnRetry, 120);
                   waitUntilElementVisibleBy(driver, btnRetry, 120);
                   waitForSometime(tcConfig.getConfig().get("LowWait"));
                   if(verifyObjectDisplayed(btnRetry) && verifyObjectDisplayed(btnOfflinePayment)){
                         tcConfig.updateTestReporter("BookATourPage", "verifyTwoButtonsPresent", Status.PASS, "User can view two buttons present in screen");
                   }
                   else{
                         tcConfig.updateTestReporter("BookATourPage", "verifyTwoButtonsPresent", Status.FAIL, "Getting error:" );
                   }
                   waitForSometime(tcConfig.getConfig().get("LowWait"));
                   clickElementJSWithWait(btnOfflinePayment);
                   waitForSometime(tcConfig.getConfig().get("LowWait"));
                   waitUntilElementVisibleBy(driver, btnCompleted, 120);
                   if(verifyObjectDisplayed(btnCompleted)){
                   clickElementJSWithWait(btnCompleted);
                   waitForSometime(tcConfig.getConfig().get("MedWait"));
                   tcConfig.updateTestReporter("BookATourPage", "verifyTwoButtonsPresent", Status.PASS, "User is able to navigate to next page");
                   }
                   else{
                         tcConfig.updateTestReporter("BookATourPage", "verifyTwoButtonsPresent", Status.FAIL, "Failed while navigating to next page");
                   }
            } catch (Exception e) {
                   // TODO Auto-generated catch block
                   tcConfig.updateTestReporter("BookATourPage", "verifyTwoButtonsPresent", Status.FAIL, "Getting error:" + e);
                   throw e;
            }
            
     }


/*
     * Method-Click on Newly created Tour record in mobile UI
     * Designed By-JYoti
     * */
   public void clickOnCreatedTour(String Lead){
            
                         Select selectTours = new Select(driver.findElement(drpTourHome));
                         selectTours.selectByVisibleText("Site Tours Today");
                         waitForSometime(tcConfig.getConfig().get("MedWait"));
                         JavascriptExecutor js = (JavascriptExecutor) driver;                                            
                         WebElement element = driver.findElement(By.xpath("//a[contains(text(),'"+LeadsPage.lead1+"')]"));                    
                         js.executeScript("arguments[0].scrollIntoView(true);",element);                                        
                         System.out.println(element.getText());
                         waitForSometime(tcConfig.getConfig().get("MedWait"));
                         System.out.println(lead1);
                          driver.findElement(By.xpath("//a[contains(text(),'"+LeadsPage.lead1+"')]")).click();                          
                         waitForSometime(tcConfig.getConfig().get("MedWait"));
                         waitUntilObjectVisible(driver, tabDetails, 120);                                        
                         clickElementBy(tabTours);  
                         waitForSometime(tcConfig.getConfig().get("MedWait"));                       
                         waitUntilElementVisibleBy(driver, lnkTourID, 120);
                         clickElementJSWithWait(lnkTourID);
                         waitForSometime(tcConfig.getConfig().get("LowWait"));  
                         waitUntilElementVisibleBy(driver, CancelButton, 120);
                         waitForSometime(tcConfig.getConfig().get("LowWait"));  
                         WebElement text = driver.findElement(associatedPayments);                   
                         js.executeScript("arguments[0].scrollIntoView(true);",text);  
                         waitForSometime(tcConfig.getConfig().get("LowWait"));  
                         if(verifyObjectDisplayed(paymentId) && driver.findElement(transactStatus).getAttribute("outerText").contains("Offline Payment")){
                                String paymentID = driver.findElement(paymentId).getText();
                                String tansactID = driver.findElement(transactStatus).getText();
                                tcConfig.updateTestReporter("BookATourPage", "clickOnCreatedTour", Status.PASS, "Tour Page displayed sucessfully and Payment is :- " +paymentID+ " and transaction status is :- " +tansactID);              
                         }
                         else{
                                tcConfig.updateTestReporter("BookATourPage", "clickOnCreatedTour", Status.FAIL, "Failed while loading Tour page");
                         }                                                                           

     }
   
   /*
    * Method-  Click PAyment ID
    * Designed By-JYoti
    * */
   public void clickPaymentId(){
  	
  			waitForSometime(tcConfig.getConfig().get("LowWait"));	
  			waitUntilElementVisibleBy(driver, CancelButton, 120);
  			waitForSometime(tcConfig.getConfig().get("LowWait"));	
  			WebElement text = driver.findElement(associatedPayments);			 
  			js.executeScript("arguments[0].scrollIntoView(true);",text);	
  			waitForSometime(tcConfig.getConfig().get("LowWait"));
  			clickElementJSWithWait(paymentRecord);
  			waitUntilElementVisibleBy(driver, btnRefund, 120);
  			if(verifyObjectDisplayed(btnRefund)){
  			clickElementJSWithWait(btnRefund);
  			waitForSometime(tcConfig.getConfig().get("LowWait"));
  			tcConfig.updateTestReporter("BookATourPage", "clickPaymentId", Status.PASS, "The refund button is displyed");
  			}
  		
  					
   }
   

   
   /*
    * Method-Select inecntive 
    * Designed By-JYoti
    * */
   
   public void selectLineItems(){
   
  		waitForSometime(tcConfig.getConfig().get("LowWait"));
  		waitUntilElementVisibleBy(driver, lstLineItems, 120);
  		List<WebElement> chkBox = driver.findElements(lstLineItems); 		
  		for (int inti=0; inti<chkBox.size(); inti++){		
  			chkBox.get(inti).click();
  			break;
  		}
  		waitForSometime(tcConfig.getConfig().get("LowWait"));
  		waitUntilElementVisibleBy(driver, qtyToRefund, 120);
  		clickElementBy(qtyToRefund);
  		waitForSometime(tcConfig.getConfig().get("LowWait"));

  		clearElementBy(qtyToRefund);		
  		waitForSometime(tcConfig.getConfig().get("LowWait"));
  		driver.findElement(qtyToRefund).sendKeys(testData.get("QuantityToRefund"));
  		
  		waitForSometime(tcConfig.getConfig().get("LowWait"));
  		addCloverDevice();
  		waitForSometime(tcConfig.getConfig().get("LowWait"));
  		clickElementJSWithWait(refundBtn);
  		waitForSometime(tcConfig.getConfig().get("LowWait"));
  		waitUntilElementVisibleBy(driver, waitMsg, 120);
  		if(verifyObjectDisplayed(waitMsg))
  		{
  			String strtext = driver.findElement(waitMsg).getText();
  			tcConfig.updateTestReporter("BookATourPage", "selectLineItems", Status.PASS, "The error message displayed is :- " +strtext);					
  		}
  	
   }
   
   /*
    * Method-Click Refund Button
    * Designed By-JYoti
    * */
   public void clickOfRefundButton(){

  		waitForSometime(tcConfig.getConfig().get("MedWait"));
  		 waitUntilElementVisibleBy(driver, btnRetry, 120);
  		 waitUntilElementVisibleBy(driver, btnRetry, 120);
  		 waitForSometime(tcConfig.getConfig().get("LowWait"));
  		 if(verifyObjectDisplayed(btnRetry) && verifyObjectDisplayed(btnOfflineRefund)){
  			 tcConfig.updateTestReporter("BookATourPage", "onclickOfRefund", Status.PASS, "User can view two buttons present in screen");
  		 }
  		 else{
  			 tcConfig.updateTestReporter("BookATourPage", "onclickOfRefund", Status.FAIL, "Failed : buttons not displyed in screen");
  		 }
  	
   }
   
   
   
   /*
    * Method-Clcik Offline Refund button to proceed
    * Designed By-JYoti
    * */
   public void clickOfflineRefundButton(){

  		waitUntilElementVisibleBy(driver, btnOfflineRefund, 120);
  		 clickElementJSWithWait(btnOfflineRefund);
  		 waitForSometime(tcConfig.getConfig().get("LowWait"));
  		 waitUntilElementVisibleBy(driver, waitMsg, 120);
  		 if(verifyObjectDisplayed(waitMsg))
  			{
  				String strtext = driver.findElement(waitMsg).getText();
  				tcConfig.updateTestReporter("BookATourPage", "clickOfflineRefundButton", Status.PASS, "The error message displayed is :- " +strtext);
  				 waitUntilElementVisibleBy(driver, btnCompleted, 120);
  				 if(verifyObjectDisplayed(btnCompleted)){
  				 clickElementJSWithWait(btnCompleted);
  				 waitForSometime(tcConfig.getConfig().get("MedWait"));
  				 tcConfig.updateTestReporter("BookATourPage", "clickOfflineRefundButton", Status.PASS, "User is navigated to Payment Contact Details form");
  				 }
  				 else{
  					 tcConfig.updateTestReporter("BookATourPage", "clickOfflineRefundButton", Status.FAIL, "Failed while navigating to next page");
  				 }
  			}	
  		 else{ tcConfig.updateTestReporter("BookATourPage", "clickOfflineRefundButton", Status.FAIL, "Failed : message not displyed");}
  		 paymentContactDetails_modified();
  		 waitForSometime(tcConfig.getConfig().get("LowWait"));
  		 waitUntilElementVisibleBy(driver, txtOrderID, 120);
  		 if(driver.findElement(txtOrderID).getAttribute("className").contains("uiOutputText"))
  			{
  				String strval = driver.findElement(txtOrderID).getText();
  				tcConfig.updateTestReporter("BookATourPage", "clickOfflineRefundButton", Status.PASS, "The Order Id displayed is :- " +strval);
  			}
  		 else{
  			   tcConfig.updateTestReporter("BookATourPage", "clickOfflineRefundButton", Status.FAIL, "Failed while displaying Order Id");
  			 }
  		 clickElementBy(imgHome);
  		 waitForSometime(tcConfig.getConfig().get("LowWait"));
  	
  }
   
   /*
    * Method-Validate PAyemnt contact details
    * Designed By-JYoti
    * */
   public void paymentContactDetails_modified(){

   		waitUntilElementVisibleBy(driver, drpCarrier, 120);
   		 Select s = new Select(driver.findElement(drpCarrier));
   		 s.selectByIndex(3);
   		 waitForSometime(tcConfig.getConfig().get("LowWait"));
   		 waitUntilElementVisibleBy(driver, txtSendEmail, 120);
   		 WebElement chk=driver.findElement(chkSendEmail); 
   			waitForSometime(tcConfig.getConfig().get("LowWait"));
   			Actions actChk=new Actions(driver);
   			actChk.moveToElement(chk).click().build().perform(); 
   			waitForSometime(tcConfig.getConfig().get("LowWait"));
   			 waitUntilElementVisibleBy(driver, txtSendText, 120);
   			 WebElement text=driver.findElement(chkSendText); 
   				waitForSometime(tcConfig.getConfig().get("LowWait"));
   				Actions actChkText=new Actions(driver);
   				actChkText.moveToElement(text).click().build().perform(); 
   				waitForSometime(tcConfig.getConfig().get("LowWait"));
   				if(verifyObjectDisplayed(btnSend)){						
   				clickElementJSWithWait(btnSend);
   				waitForSometime(tcConfig.getConfig().get("LowWait"));
   				tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails_modified", Status.PASS, "Payment contact details are saved sucessfully");
   				}
   				else{
   					tcConfig.updateTestReporter("BookATourPage", "paymentContactDetails_modified", Status.FAIL, "Failed wihle adding Payment contact details" );
   				}
   	
   					
   }
   
   public void clickRetryButton(){
	   
		waitUntilElementVisibleBy(driver, btnRetry, 120);
		 clickElementJSWithWait(btnRetry);
		 waitForSometime(tcConfig.getConfig().get("LowWait"));
		 waitUntilElementVisibleBy(driver, waitMsg, 120);
		 if(verifyObjectDisplayed(waitMsg))
			{
				String strtext = driver.findElement(waitMsg).getText();
				tcConfig.updateTestReporter("BookATourPage", "clickRetryButton", Status.PASS, "The error message displayed is :- " +strtext);						 
			}
		 clickOfRefundButton();	
		 waitForSometime(tcConfig.getConfig().get("LowWait"));
		
	
}
	
}


           

