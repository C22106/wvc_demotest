package salesforce.pages;

import java.awt.AWTException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ReportPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(ReportPage.class);
	CasesPage casePage = new CasesPage(tcConfig);

	public ReportPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//button[@class='slds-button']")
	private WebElement menuIcon;
	@FindBy(xpath = "//input[@class='slds-input input']")
	private WebElement txtSearchBy;
	private By goToRes = By.xpath("(//a[text()='GOTO RESERVATION']/../..)[1]");
	private By searchReport = By.xpath("//input[contains(@placeholder,'Search recent reports')]");
	private By searchpublicReports = By.xpath("//input[contains(@placeholder,'Search public reports')]");
	private By searchAllReport = By.xpath("//input[contains(@placeholder,'Search all reports')]");
	private By myChangeRequests = By.xpath("//a[@title='My Change Requests']");
	private By myChangeRequestsMarketer = By.xpath("//h1[@title='My Change Requests']");
	private By filter = By.xpath("(//button[contains(@class,'reportAction')])[3]");
	private By filterRep = By.xpath("//button[contains(@class,'toggleFilter ')]");
	private By filterMarketer = By.xpath("//button[@title='Filters']");
	private By dateFilter = By.xpath("//span[text()='Opened Date']/..//span[@class='filterPredicate']");
	private By customizeDate = By.xpath("//a[text()='Customize']");
	private By selecDtate = By.xpath("//a[@class='filterCardEditor']//span[text()='Opened Date']");
	private By startDate = By.xpath("(//input[@placeholder='Pick a date'])[1]/../button");
	private By startDateMaketer = By.xpath("//select[@class='duration select']");
	private By today = By.xpath("//option[@value='TODAY']");
	private By doneBtn = By.xpath("//span[text()='Done']");
	private By endDate = By.xpath("(//input[@placeholder='Pick a date'])[2]/../button");
	private By todayBtn = By.xpath("//a[text()='Today']");
	private By applyBtn = By.xpath("//button[text()='Apply']");
	private By caselist = By.xpath("(//div[contains(@class,'report')]//a)[1]");
	private By arrivalList = By.xpath("//select[@name='Report Name']");
	private By datelList = By.xpath("//div[contains(@class,'thisWeek')]//select[@class='slds-select']");
	private By dateLastWeek = By.xpath("//select[@class='slds-select' ]/option[@value='LastWeek']");
	private By guest = By.xpath("(//td[contains(@data-label,'Name')]//a)[1]");
	private By reportMarketer = By.xpath("//li//a//img[contains(@src,'Report')]");
	private By allReportMarketer = By.xpath("//a[@title='All Reports']");
	private By noResult = By.xpath("//span[text()='No results found']");

	public void goToReservation() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		// waitUntilElementVisibleBy(driver, goToRes, 120);
		WebElement ele = driver.findElement(By.xpath("//iframe[@title='Report Viewer']"));
		driver.switchTo().frame(ele);
		if (verifyObjectDisplayed(goToRes)) {
			tcConfig.updateTestReporter("submitCaseFromRes", "goToRes", Status.PASS, "reservation located");
			clickElementBy(goToRes);
		} else {

			tcConfig.updateTestReporter("submitCaseFromRes", "goToRes", Status.FAIL, "reservation not present located");
		}

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}

	private By resrvationlist = By.xpath("//li//a//img[contains(@src,'Reservation')]");

	public void goToReservationMarketer() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, resrvationlist, 120);
		clickElementBy(resrvationlist);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, datelList, 120);
		if (verifyObjectDisplayed(datelList)) {
//			Select DDselect = new Select(driver.findElement(datelList));
//			DDselect.deselectByValue("LastWeek");
			clickElementBy(datelList);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			clickElementBy(dateLastWeek);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			checkLoadingSpinner();
			waitUntilElementVisibleBy(driver, guest, 120);
			clickElementBy(guest);
			tcConfig.updateTestReporter("submitCaseFromRes", "goToRes", Status.PASS, "reservation page is displayed");
		} else {

			tcConfig.updateTestReporter("submitCaseFromRes", "goToRes", Status.FAIL, "reservation not present located");
		}

	}

	public void searchAndNavigateToReport() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilElementVisibleBy(driver, goToRes, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElements(By.xpath("//a[@title='Public Reports']")).get(0).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		clickElementJSWithWait(searchpublicReports);
		fieldDataEnter(searchpublicReports, testData.get("ReportName"));
		// driver.findElement(searchReport).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("searchAndNavigateToReport", "ReportPage", Status.PASS, "Report located");
		WebElement myreport = driver.findElement(By.xpath("//span[text()='" + testData.get("ReportName") + "']"));
		waitUntilObjectClickable(driver, myreport, 120);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", myreport);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void navigateToReportMarketer() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(reportMarketer);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, allReportMarketer, 120);
		clickElementBy(allReportMarketer);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(searchAllReport);
		fieldDataEnter(searchAllReport, testData.get("ReportName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(searchAllReport).sendKeys(Keys.ENTER);
//		driver.findElement(searchAllReport).sendKeys(Keys.BACK_SPACE);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("searchAndNavigateToReport", "ReportPage", Status.PASS, "Report located");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(By.xpath("//button[@title='" + testData.get("ReportName") + "']"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public By filtercase = By.xpath("//button[@title='Filters']");
	public By Opencasedate = By.xpath("//div//span[text()='Opened Date']//parent::div//following-sibling::div//span");
	public By Donebtn = By.xpath("//button//span[text()='Done']");
	public By Applybtn = By.xpath("//button[@title='Click to apply changes']");

	public void navigateToCaseReportMarketer() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		clickElementBy(filtercase);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, Opencasedate, 120);
		clickElementBy(Opencasedate);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Select sel = new Select(driver.findElement(By.xpath("(//h2[text()='Range']//parent::div//div//select)[2]")));
		sel.selectByValue("TODAY");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(Donebtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(Applybtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("searchAndNavigateToReport", "ReportPage", Status.PASS, "Report located");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
//		clickElementJS(By.xpath("//button[@title='"+testData.get("ReportName")+"']"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void navigateToReportMarketerNegative() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(reportMarketer);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, allReportMarketer, 120);
		clickElementBy(allReportMarketer);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(searchAllReport);
		fieldDataEnter(searchAllReport, testData.get("ReportName"));
		driver.findElement(searchAllReport).sendKeys(Keys.BACK_SPACE);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(noResult)) {
			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.PASS, "My Cases is present");

		} else {

			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.PASS, "My Cases is Present");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void verifyMyChangeReq() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement ele = driver.findElement(By.xpath("//iframe[@title='Report Viewer']"));
		driver.switchTo().frame(ele);
		clickElementJS(filterRep);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(dateFilter);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(customizeDate);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(startDate);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(todayBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(endDate);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(todayBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(applyBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String Casenumber = ReservationPage.createdCase;

		if (verifyElementDisplayed(driver.findElement(By.xpath("//a[text()='" + Casenumber + "']")))) {
			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.PASS, "ChangeReq is present");
		} else {

			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.FAIL, "ChangeReq is not listed");
		}
		driver.switchTo().defaultContent();

	}

	public void verifyMyChangeReqMarketer() {

		String HelpSub = HelpPopup.HelpSubject;

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(driver.findElement(By.xpath("//a[text()='" + HelpSub + "']")))) {

			driver.findElement(By.xpath("//a[text()='" + HelpSub + "']")).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.PASS, "ChangeReq is present");

		} else {

			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.FAIL, "ChangeReq is not listed");
		}

		/*
		 * //driver.switchTo().frame(driver.findElement(By.
		 * xpath("//iframe[@title='Report Viewer']"))); clickElementBy(filterMarketer);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(selecDtate);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(startDateMaketer);
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); clickElementBy(today);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(doneBtn); String Casenumber = ReservationPage.createdCase;
		 * 
		 * if(verifyElementDisplayed(driver.findElement(By.xpath("//a[text()='"+
		 * Casenumber+"']")))) { tcConfig.updateTestReporter("verifyMyChangeReq",
		 * "Change Req", Status.PASS, "ChangeReq is present");
		 * driver.findElement(By.xpath("//a[text()='"+Casenumber+"']")).click();
		 * 
		 * }else {
		 * 
		 * tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.FAIL,
		 * "ChangeReq is not listed"); } driver.switchTo().defaultContent();
		 */

	}

	public void verifyCaseStatusMaketer() {
		String Casenumber = ReservationPage.createdCase;

		WebElement ele = driver.findElement(By.xpath("//iframe[@title='Report Viewer']"));
		driver.switchTo().frame(ele);

		if (verifyElementDisplayed(driver.findElement(By.xpath("//a[text()='" + Casenumber + "']")))) {
			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.PASS, "ChangeReq is present");
		} else {

			tcConfig.updateTestReporter("verifyMyChangeReq", "Change Req", Status.FAIL, "ChangeReq is not listed");
		}
		driver.switchTo().defaultContent();

	}

	private By txtBoxSearchReport = By.xpath("//div[contains(@class,'search-container')]//input");
	private By lnkAllreports = By.xpath("//a[@title='All Reports']");
	private By btnFilter = By.xpath("//button[contains(@class,'toggleFilter')]");
	private By btnFilterDate = By.xpath("//p[text()='Check In Date']/..");
	private By txtBoxCheckInDate = By.xpath("//label[text()='Start Date']/../div/input");
	private By btnApply = By.xpath("//button[text()='Apply']");
	private By trChaseListFirstRow = By.xpath("//table[contains(@class,'data-grid-full-table')]//tr[2]");
	private By reservationTab = By.xpath("//span[text()='Res Number']");

	// private By reservationTab =
	// By.xpath("//a[text()='"+testData.get("ReservationNumber")+"']");
	public void searchReservationRecord(String choice) {
		waitUntilElementVisibleBy(driver, txtBoxSearchReport, 120);
		/*
		 * clickElementBy(lnkAllreports);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 */
		fieldDataEnter(txtBoxSearchReport, testData.get("ReportName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement reportNmae = driver.findElement(By.xpath("//a[@title='" + testData.get("ReportName") + "']"));
		if (verifyElementDisplayed(reportNmae)) {
			clickElementJS(reportNmae);
			tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.PASS,
					"Chase list report is visible");

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement ele = driver.findElement(By.xpath("//iframe[@title='Report Viewer']"));
			driver.switchTo().frame(ele);
			waitUntilElementVisibleBy(driver, btnFilter, 240);
			if (verifyObjectDisplayed(btnFilter)) {
				clickElementJS(btnFilter);
				tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.PASS,
						"Filter Button is visible and able toclick");

			} else {
				tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.FAIL,
						"Filter Button is not visible");

			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnFilterDate, 120);
			clickElementJS(btnFilterDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtBoxCheckInDate, 120);
			fieldDataEnter(txtBoxCheckInDate, testData.get("CheckInDate"));
			waitUntilElementVisibleBy(driver, btnApply, 120);
			clickElementJS(btnApply);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnFilter, 240);
			clickElementJS(btnFilter);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, trChaseListFirstRow, 120);
			/* if(verifyObjectDisplayed(trChaseListFirstRow)){ */
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("document.getElementById('gvLocationHorizontalRail').scrollLeft += 250", "");

			/*
			 * WebElement scroll =
			 * driver.findElement(By.xpath("//div[@id='gvLocationHorizontalRail']"));
			 * JavascriptExecutor js = (JavascriptExecutor)driver;
			 * js.executeScript("window.scrollBy(250,0)", scroll);
			 */

			/*
			 * WebElement myElement = (new WebDriverWait(driver, 30))
			 * .until(ExpectedConditions.elementToBeClickable(By.cssSelector(
			 * ".ngscroll-scrollbar"))); myElement.click(); Actions move = new
			 * Actions(driver); move.moveToElement(myElement).clickAndHold();
			 * move.moveByOffset(125,0); move.release(); move.perform();
			 */

			/*
			 * JavascriptExecutor js = (JavascriptExecutor) remoteDriver;
			 * 
			 * WebElement
			 * 
			 * a=remoteDriver.findElement(By.xpath(".//td[@id='titleView!1Subtitle' and
			 * 
			 * contains(text(),'Time run: ')]"));
			 * 
			 * js.executeScript("arguments[0].scrollIntoView();",a );
			 */

			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			WebElement element = driver.findElement(reservationTab);
			js1.executeScript("arguments[0].scrollIntoView();", element);
			System.out.println("hello");
			switch (choice) {
			case "Visible":
				if (driver.findElements(By.xpath("//a[text()='" + testData.get("ReservationNumber") + "']"))
						.size() > 0) {
					tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.PASS,
							"Reservation record is visible in chase list");
				} else {
					tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.FAIL,
							"Reservation record is not visible in chase list");
				}
				break;

			/*
			 * case "Not Visible":
			 * if(driver.findElements(By.xpath("//a[text()='"+testData.get(
			 * "ReservationNumber")+"']")).size()>0) {
			 * tcConfig.updateTestReporter("ReportPage", "searchReservationRecord",
			 * Status.FAIL, "Reservation record is visible in chase list"); } else {
			 * tcConfig.updateTestReporter("ReportPage", "searchReservationRecord",
			 * Status.PASS, "Reservation record is not visible in chase list"); } break;
			 */
			}
		} else {
			tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.FAIL,
					"Chase list report is not visible");
		}

	}

	/*
	 * private By txtBoxSearchReport =
	 * By.xpath("//div[contains(@class,'search-container')]//input"); private By
	 * lnkAllreports = By.xpath("//a[@title='All Reports']"); private By btnFilter =
	 * By.xpath("//button[contains(@class,'toggleFilter')]"); private By
	 * btnFilterDate = By.xpath("//p[text()='Check In Date']/.."); private By
	 * txtBoxCheckInDate = By.xpath("//label[text()='Start Date']/../div/input");
	 * private By btnApply = By.xpath("//button[text()='Apply']"); private By
	 * trChaseListFirstRow =
	 * By.xpath("//table[contains(@class,'data-grid-full-table')]//tr[2]");
	 */
	private By iframeReports = By.xpath("//iframe[contains(@class,'reportsReportBuilder')]");

	public void searchRequiredList() throws ParseException {
		waitUntilElementVisibleBy(driver, txtBoxSearchReport, 120);
		clickElementBy(lnkAllreports);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(txtBoxSearchReport, testData.get("ReportName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> e = driver.findElements(By.xpath("//a[@title='" + testData.get("ReportName") + "']"));
		if (e.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.PASS,
					"Chase list report is visible");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (testData.get("ReportName").equals("Arrival List")) {
				clickElementJSWithWait(e.get(1));
			} else {
				clickElementJSWithWait(e.get(0));
			}
			// e.get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, iframeReports, 800);
			WebElement ele = driver.findElement(iframeReports);
			driver.switchTo().frame(ele);
			waitUntilElementVisibleBy(driver, btnFilter, 120);
		} else {
			tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.FAIL,
					"Chase list report is not visible");
		}
	}

	private By listMarketingAgent = By.xpath("//span[text()='Marketing Agent']/../../../../../../tr//td[2]//span");
	private By listResFirstName = By.xpath("//span[text()='Marketing Agent']/../../../../../../tr//td[4]//span");
	private By listGoToReservation = By.xpath("//a[text()='GOTO RESERVATION']");
	private By lnkEditMarketingAgent = By.xpath("//button[@title='Edit Marketing Agent Assigned']");
	private By txtBoxSearchAgent = By.xpath(
			"//span[text()='Marketing Agent Assigned']/../..//input | //label[text()='Marketing Agent Assigned']/..//input");
	private By btnSave = By.xpath("//button[@title='Save']");

	public void verifyAssigningArrival() {
		int count = 0;
		String fname = "";
		waitUntilElementVisibleBy(driver, btnFilter, 120);

		int size = driver.findElements(listMarketingAgent).size();
		if (size > 0) {
			for (int i = 0; i < size; i++) {
				if (driver.findElements(listMarketingAgent).get(i).getText().equals("")) {
					fname = driver.findElements(listResFirstName).get(i).getText();
					WebElement e1 = driver.findElement(By.xpath("(//div[@class='widgets'])[2]"));

					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("arguments[0].scrollLeft = arguments[0].offsetWidth", e1);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(driver.findElements(listGoToReservation).get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs.get(1));
					driver.switchTo().defaultContent();
					waitUntilElementVisibleBy(driver, lnkEditMarketingAgent, 120);
					clickElementBy(lnkEditMarketingAgent);
					waitUntilElementVisibleBy(driver, txtBoxSearchAgent, 120);
					fieldDataEnter(txtBoxSearchAgent, testData.get("AgentName"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> e = driver
							.findElements(By.xpath("//lightning-base-combobox-formatted-text[@title='"
									+ testData.get("AgentName") + "']/../../.."));
					if (e.size() > 0) {
						tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.PASS,
								"Required agent" + testData.get("AgentName") + " is present");
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementJSWithWait(e.get(e.size() - 1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(btnSave);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						waitUntilElementVisibleWb(driver,
								driver.findElement(
										By.xpath("//span[text()='Marketing Agent Assigned']/../..//span[text()='"
												+ testData.get("AgentName") + "']")),
								120);
						if (verifyElementDisplayed(driver
								.findElement(By.xpath("//span[text()='Marketing Agent Assigned']/../..//span[text()='"
										+ testData.get("AgentName") + "']")))) {
							tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.PASS,
									"Agent Name is updated");
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.close();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().window(tabs.get(0));
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.navigate().refresh();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							waitUntilElementVisibleBy(driver, iframeReports, 240);
							WebElement eleIframe = driver.findElement(iframeReports);
							driver.switchTo().frame(eleIframe);
							waitUntilElementVisibleBy(driver, btnFilter, 120);
							WebElement ele = driver
									.findElement(By.xpath("//span[text()='" + fname + "']/../../../..//td[2]//span"));
							if (ele.getText().equals(testData.get("AgentName"))) {
								tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.PASS,
										"Updated marketing agent is visible in arrival list");
							} else {
								tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.FAIL,
										"Updated marketing agent is not visible in arrival list");
							}

						} else {
							tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.FAIL,
									"Agent Name is not updated");
						}
					} else {
						tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.FAIL,
								"Required agent is not present");
					}
					break;
				} else {
					count++;
				}

			}

			if (count == size) {
				tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.FAIL,
						"No reservation found with marketing agent not assigned");
			}
		} else {
			tcConfig.updateTestReporter("ReservationPage", "verifyAssigningArrival", Status.FAIL,
					"No reservation is found in arrival list");
		}

		driver.switchTo().defaultContent();
	}

	public void verifyAccountableOrUnAccReservation() {

	}

	private By hyplnkAllReports = By.xpath("//a[text()='All Reports']");
	private By txtSearch = By.xpath("//label[contains(text(),'Search all reports...')]/../..//input");// input[contains(@class,'search-text-field')]
	private By txtSearchCss = By.cssSelector(".search-text-field");
	private By tblReportsAll = By.xpath("//div[@class='listViewContainer']//div//table[@role='grid']");// div[@class='listViewContainer']//div//table[@role='grid']
	private By hyplnkArrivalList = By
			.xpath("//div[@class='slds-scrollable_y']//table//tr/th//a[@title='Arrival List']");
	private By reportCreated = By.xpath("//a[@title='" + testData.get("ReportCreatedBy")
			+ "']/../../../../../../../..//span[contains(text(),'" + testData.get("Menu2") + "')]");
//private By reportClick = By.xpath("//a[@title='Veera Gandi']//ancestor::td//parent::tr//th//a");
//a//span[contains(text(),'Arrival List')]//label[contains(text(),'Search all reports...')]/../..//input
	private By imgReport = By.xpath("(//span[contains(text(),'Report: Reservation')])[2]");
	private By btnExport = By.xpath("//button[contains(text(),'Export')]");
	public By txtTotalRecords = By.xpath("*//div[@title='Total Records']");

	public void validateArrivalList() {
		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, hyplnkAllReports, 120);
			driver.findElement(hyplnkAllReports).click();

			waitUntilElementVisibleBy(driver, txtSearchCss, 120);
			if (verifyElementDisplayed(driver.findElement(txtSearchCss))) {
				fieldDataEnter(txtSearchCss, testData.get("Menu2"));

				tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.PASS,
						"Search for Arrival List in search box");

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.findElement(txtSearchCss).sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.PASS,
						"Arrival List is displayed in screen");
			} else {
				tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.FAIL,
						"Failed while loading Arrival List");
			}

			System.out.println("-----------------------------------------------");

			List<WebElement> elements = driver.findElements(By.xpath("//span[contains(text(),'Arrival List')]"));
			System.out.println("Number of elements:" + elements.size());

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", elements.get(5));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement Frame = driver.findElement(By.xpath("//iframe[@title='Report Viewer']"));
			driver.switchTo().frame(Frame);
			waitUntilElementVisibleBy(driver, txtTotalRecords, 120);
			tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.PASS,
					"Report is displayed in screen");

		} catch (Exception e) {

			tcConfig.updateTestReporter("ReportPage", "validateArrivalList", Status.FAIL,
					"Failed due to : exception caught " + e.getMessage());

		}
	}

	private By imgIconNew = By.xpath("//div//p[contains(text(),'1/1')]");
//private By resvNumber = By.xpath("//a[contains(text(),'"+testData.get("ReservationNumber")+"')]");
	private By resvNumber = By
			.xpath("//td[@class='data-grid-table-cell lightning-table-cell-wrap data-grid-table-cell-odd']/div/div//a");
//private By resvNumber = By.xpath("//a[@data-id='a2e1I000001gF5gQAE']");
	private By frame = By.xpath("//iframe[@title='Report Viewer']");
	private By lnkAllTime = By.xpath("//a[contains(text(),'All Time')]");
	private By lnkChaseList = By.xpath("//a[@title='Chase List']");
	private By imgSearch = By
			.xpath("//button[contains(@class,'slds-button slds-button_icon-border action-bar-action-searchTable')]");// button[contains(@class,'reportAction
																														// report-action-searchTable')]
	private By txtResev = By.xpath("//input[@class='search-input']");
	private By gvReserv = By.xpath("(//a[contains(text(),'" + testData.get("ReservationNumber") + "')])[1]");// a[contains(text(),'"+testData.get("ReservationNumber")+"')]
	private By imgVerify = By.xpath("//div[@class='num-of-results']//p");
//private By filters=By.xpath("//button//span[text()='Filters']//parent::button");
	private By filters = By.xpath("(//span[text()='Filters']/..)[2]");
	private By checkindate = By.xpath("//button//p[text()='Check In Date']");
	private By Range = By.xpath("//label[text()='Range']//following-sibling::div//button");
	private By alltime = By.xpath("//a//span[text()='All Time']");
//private By btnApply = By.xpath("//button[contains(text(),'Apply')]");

	public void validateClickReservation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(resvNumber)) {
			List<WebElement> reservationNumber = driver.findElements(resvNumber);
			if (reservationNumber.size() > 0) {
				clickElementJS(reservationNumber.get(1));

				tcConfig.updateTestReporter("ReportPage", "verifyReservationNotPresent", Status.PASS,
						"Reservation is found");
			} else {
				tcConfig.updateTestReporter("ReportPage", "verifyReservationNotPresent", Status.FAIL,
						"Reservation is not found");
			}
		}
	}

	public WebElement tableCalenderValue(String curdate) {
		return driver.findElement(By.xpath(
				"//div[@class='wave-table-cell-text' and @data-tooltip='" + curdate + "']/../../../td[2]/div/div"));
	}

	public String totalTableRecordCount() {
		return driver.findElement(By.xpath("//div[@class='metricsElement metricsValue']")).getText().trim();
	}

	public By range = By.xpath("//label[contains(text(),'Range')]/..//div//button");
	public By allTime = By
			.xpath("(//div[@class='slds-dropdown slds-dropdown_left']//li//a//span[contains(text(),'All Time')])");
	public By value = By.xpath("//div[@class='metricsElement metricsValue']");

	public void validateMyChangeReq() throws AWTException {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement ele = driver.findElement(By.xpath("//iframe[@title='Report Viewer']"));
		driver.switchTo().frame(ele);
		clickElementJS(filterRep);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(dateFilter);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(range);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(allTime);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJS(applyBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJS(filterRep);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String value = driver.findElement(By.xpath("//div[@class='metricsElement metricsValue']")).getText();
		// driver.findElement(By.xpath("//div[@class='metricsElement
		// metricsValue']")).click();
		String pattern = "MM/d/YYYY";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date date1 = new Date();
		String curdate = simpleDateFormat.format(date1);
		System.out.println(curdate);
		/*
		 * List<WebElement>
		 * dates=driver.findElements(By.xpath("//td//div//div[contains(text(),'"+curdate
		 * +"')]")); System.out.println(dates);
		 * 
		 * 
		 * do{ Robot rob=new Robot(); rob.mouseWheel(100);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * }while(!dates.get(dates.size()-1).isDisplayed());
		 * 
		 * 
		 * 
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * 
		 * // waitForSometime(tcConfig.getConfig().get("MedWait")); String Date =
		 * tableCalenderValue(curdate).getText().trim();
		 * //System.out.println(tableCalenderValue(totalTableRecordCount()).getText().
		 * trim()); System.out.println(Date);
		 */
		String pattern1 = "MM/d/YYYY";
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern);
		Date date2 = new Date();
		String curdate1 = simpleDateFormat.format(date2);
		System.out.println(curdate1);

		if (curdate.equalsIgnoreCase(curdate1)) {
			tcConfig.updateTestReporter("createChangeRequestInhouseMarketer", "create Change Req", Status.PASS,
					"Current date is displyed");
		} else {
			tcConfig.updateTestReporter("createChangeRequestInhouseMarketer", "create Change Req", Status.FAIL,
					"Failed while displying current date");
		}

		driver.switchTo().defaultContent();
	}

	private By lnkFilters = By.xpath("//button[contains(@class,'toggleFilter')]");

	public void searchRequiredListMobUi() throws ParseException {
		waitUntilElementVisibleBy(driver, txtBoxSearchReport, 120);
		clickElementBy(lnkAllreports);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions a = new Actions(driver);
		WebElement ele = driver.findElement(txtBoxSearchReport);
		a.sendKeys(ele, testData.get("ReportName")).build().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> e = driver.findElements(By.xpath("//a[@title='" + testData.get("ReportName") + "']"));
		if (e.size() >= 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.PASS,
					"Chase list report is visible");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (testData.get("ReportName").equals("Arrival List")) {
				clickElementJSWithWait(e.get(1));
			} else {
				clickElementJSWithWait(e.get(0));
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("ReportPage", "searchReservationRecord", Status.FAIL,
					"Chase list report is not visible");
		}
	}

	private By lnkOpenedDate = By.xpath("//p[text()='Escalated On']/..");
	private By drpDwnRange = By.xpath("//label[text()='Range']/..//button");
	private By txtBoxStartTime = By.xpath("//label[text()='Start Date']/..//input");
	private By txtBoxEndTime = By.xpath("//label[text()='End Date']/..//input");
	private By btnDone = By.xpath("//span[text()='Done']/..");
	private By btnApplyMobUi = By.xpath("//button[text()='Apply']");
	private By lnkCasesInReports = By.xpath("//td[@class='firstCell']/a");
	private By iframeReport = By.xpath("//iframe[contains(@class,'reportsReportBuilder')]");
	private By lnkCustom = By.xpath("//span[text()='Custom']/../..");

	public void verifyCreatedCase() {
		driver.switchTo().frame(0);
		waitUntilElementVisibleBy(driver, lnkFilters, 120);
		clickElementJSWithWait(lnkFilters);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//span[text()='Opened Date']")).click();
		waitUntilElementVisibleBy(driver, drpDwnRange, 120);
		clickElementJSWithWait(drpDwnRange);
		waitUntilElementVisibleBy(driver, lnkCustom, 120);
		clickElementBy(lnkCustom);
		waitUntilElementVisibleBy(driver, txtBoxStartTime, 120);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar calobj = Calendar.getInstance();
		String todaysDate = df.format(calobj.getTime());
		fieldDataEnter(txtBoxStartTime, todaysDate);
		waitUntilElementVisibleBy(driver, txtBoxEndTime, 120);
		fieldDataEnter(txtBoxEndTime, todaysDate);
		waitUntilElementVisibleBy(driver, btnApplyMobUi, 120);
		clickElementJSWithWait(btnApplyMobUi);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, By.xpath("//div[@data-tooltip='" + CasesPage.txtSubject + "']"), 120);
		WebElement caseValue = driver.findElement(By.xpath("//div[@data-tooltip='" + CasesPage.txtSubject + "']"));
		String CaseValue = caseValue.getText();
		System.out.println(CaseValue);

		if (CaseValue.equals(CaseValue)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			System.out.println(CaseValue);
			tcConfig.updateTestReporter("ReportPage", "verifyCreatedCase", Status.PASS,
					"Case created from help icon is visible in reports");
			driver.close();
			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(0));
			driver.switchTo().defaultContent();
		} else {
			tcConfig.updateTestReporter("ReportPage", "verifyCreatedCase", Status.FAIL,
					"Case created from help icon is not visible in reports");
		}

	}

	/*
	 * Function/event: Varify case Created using Help Icon Description: :Verify the
	 * How to case has been created using help icon in the mobile UI for journey.
	 * Designed By: Ajib Parida Date- 21-2-2020
	 */

	private By lastrow = By.xpath("//tr[@class='data-grid-table-row']"); // to be used in List -mutiple elements

	public void verifyCaseCreated() {
		driver.switchTo().frame(0);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> lstrec = driver.findElements(lastrow);
//		waitUntilElementVisibleBy(driver, lastrow, 120);
		System.out.println(lstrec.size());
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("(//th[@data-row-index='0'])[1]")).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("(//th[@data-row-index='0'])[5]//div/div/div/div/label/a")).click();

		/*
		 * if(verifyElementDisplayed(lstrec.get(lstrec.size()-1))){
		 * 
		 * // scrollDownForElementJSWb(lstrec.get(lstrec.size()-1));
		 * 
		 * System.out.println(driver.findElement(By.xpath(
		 * "(//tr[@class='data-grid-table-row']//th/div/div/div/div/label/a)["+(lstrec.
		 * size()-4)+"]")).getText());
		 * 
		 * }
		 */
	}

}
