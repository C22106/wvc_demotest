package salesforce.pages;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class SF_LoginPage extends SalesforceBasePage {

	public static final Logger log = Logger.getLogger(SF_LoginPage.class);
	ReadConfigFile config = new ReadConfigFile();

	public SF_LoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By username = By.xpath("//input[@id='username']");
	private By password = By.xpath("//input[@id='password']");
	private By loginButton = By.id("Login");
	private By logOut = By.xpath("//a[text()='Log Out']");
	private By viewProfile = By.xpath("//img[@title='User']");
	private By homeIcon = By.xpath("//li[@data-menu-item-id='0']");
	private By profileName = By.xpath("//span[@class=' profileName']");
	private By logOutMarketer = By.xpath("//a[@title='Logout']");
	private By imageLogOut = By.xpath("//span[@class='uiImage']//img[@alt='User']");
	private By imarkter = By.xpath(
			"//img[@src='/wvo/resource/1561942897000/Wyn_Custom/custom/img/wynd_dst_icon_sq_blk_rgb_150ppi.png']");
	protected By customerLookUpTeam = By.xpath("//div[@class = 'cCustomLookupTeam']");
	protected By TeamName = By.xpath("//input[@placeholder= 'Enter Team Name']");
	protected By saleschannnelName = By.xpath("//input[@placeholder= 'Enter Sales Channel Name']");
	protected By nextButton = By.xpath("//button[@title = 'Next']");

	private void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, 10);
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("SF_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	private void setPassword(String strPassword) throws Exception {

		// look for th prob;
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("SF_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	private By testUserIHMarketer = By.xpath("//span[contains(text(),'Test user Inhousemarketer')]");
	private By testUserLogOut = By.xpath("//a[@title='Logout']");
	private By btnNewLogin = By.xpath("//input[@name='Login']");

	public void testUserIHMarketerLogout() throws Exception {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, testUserIHMarketer, 120);
			clickElementBy(testUserIHMarketer);
			// driver.findElement(testUserIHMarketer).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(testUserLogOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnNewLogin, 120);
			if (verifyObjectDisplayed(btnNewLogin)) {
				tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.PASS,
						"Log out Successful");
			} else {
				tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.FAIL,
						"Log Off unsuccessful");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}

	}

	public void launchApp(String strBrowser) {

		// this.driver = checkAndInitBrowser(strBrowser);
		String url = config.get("SF_URL").trim();
		driver.navigate().to(url);
		driver.manage().window().maximize();

	}

	public void loginToSalesForce() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("SF_UserID");
		String password = tcConfig.getConfig().get("SF_Password");

		setUserName(userid);
		setPassword(password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");

	}

	public void loginToSalesForceAdmin() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("SF_UserID_Admin");
		String password = tcConfig.getConfig().get("SF_Password_Admin");

		setUserName(userid);
		setPassword(password);
		try {
			driver.findElement(loginButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinner();
			try {
				waitUntilElementVisibleBy(driver, customerLookUpTeam, 60);
				if (verifyObjectDisplayed(customerLookUpTeam)) {
					clickElementJSWithWait(TeamName);
					driver.findElement(TeamName).sendKeys(testData.get("TeamName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("TeamName") + "')]/..")).click();
					clickElementJSWithWait(saleschannnelName);
					driver.findElement(saleschannnelName).sendKeys(testData.get("SalesChannelName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("SalesChannelName") + "')]/.."))
							.click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(nextButton);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			} catch (Exception e) {
				log.info(e);
			}
		} catch (Exception e) {
			log.info(e);
		}
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (!verifyObjectDisplayed(journeydesktop) && !verifyObjectDisplayed(imarkter))

		{
			waitUntilElementVisibleWb(driver, menuIcon, 120);
			clickElementJSWithWait(menuIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkViewAll, 120);
			clickElementJS(lnkViewAll);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> addList = driver.findElements(txtSearchBy);
			addList.get(addList.size() - 1).click();
			addList.get(addList.size() - 1).sendKeys("Journey Desktop");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//mark[text()='Journey Desktop']")).click();
		} else if (verifyObjectDisplayed(journeydesktop)) {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage No need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else if (verifyObjectDisplayed(imarkter)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage No need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Failed to Login");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	/*
	 * public void loginToSalesForceDiffUsers() throws Exception {
	 * 
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); String userid =
	 * testData.get("SF_UserID"); String password = testData.get("SF_Password");
	 * 
	 * 
	 * setUserName(userid); setPassword(password);
	 * 
	 * 
	 * //waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * driver.findElement(loginButton).click(); checkLoadingSpinner();
	 * tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS,
	 * "Log in button clicked");
	 * 
	 * }
	 */

	private By lnkViewAll = By.xpath("//div[@class='slds-size_medium']//button");
	// @FindBy(xpath="//button[@class='slds-button']/div")
	// private WebElement menuIcon;

	@FindBy(xpath = "//div[contains(@role,'navigation')]//div")
	private WebElement menuIcon;

	private By homeTab = By.xpath("//span[contains(.,'Home')]");
	private By txtSearchBy = By.xpath("//input[@class='slds-input']");
	@FindBy(xpath = "//span[@title='Vouchers']")
	private WebElement voucher;

	private By journeydesktop = By.xpath("//span[@title = 'Journey Desktop']");

	/*
	 * public void loginToSalesForceDiffUsers() throws Exception {
	 * 
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); String userid =
	 * testData.get("SF_UserID"); String password = testData.get("SF_Password");
	 * 
	 * 
	 * setUserName(userid); setPassword(password);
	 * 
	 * 
	 * //waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * driver.findElement(loginButton).click(); checkLoadingSpinner();
	 * tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS,
	 * "Log in button clicked");
	 * //waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * //waitUntilElementVisibleBy(driver, homeTab, 120); //checkLoadingSpinner();
	 * //waitForSometime(tcConfig.getConfig().get("LowWait")); //WebElement
	 * ele=driver.findElement(By.xpath("//span[@title='Vouchers']"));
	 * if(verifyElementDisplayed(journeydesktop)) {
	 * tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
	 * "User is navigated to Homepage No need to switch explicitly");
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else{
	 * 
	 * waitUntilElementVisibleWb(driver, menuIcon, 120); menuIcon.click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitUntilElementVisibleBy(driver, lnkViewAll, 120);
	 * clickElementJS(lnkViewAll);
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); List<WebElement>
	 * addList= driver.findElements(txtSearchBy);
	 * addList.get(addList.size()-1).click();
	 * addList.get(addList.size()-1).sendKeys("Journey Desktop");
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(By.xpath("//mark[text()='Journey Desktop']")).click();
	 * 
	 * } }
	 */

	public void loginToSalesForceDiffUsers() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String userid = testData.get("SF_UserID");
		String password = testData.get("SF_Password");

		setUserName(userid);
		setPassword(password);
		try {
			driver.findElement(loginButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinner();
			try {
				waitUntilElementVisibleBy(driver, customerLookUpTeam, 60);
				if (verifyObjectDisplayed(customerLookUpTeam)) {
					clickElementJSWithWait(TeamName);
					driver.findElement(TeamName).sendKeys(testData.get("TeamName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("TeamName") + "')]/..")).click();
					clickElementJSWithWait(saleschannnelName);
					driver.findElement(saleschannnelName).sendKeys(testData.get("SalesChannelName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("SalesChannelName") + "')]/.."))
							.click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(nextButton);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			} catch (Exception e) {
				log.info(e);
			}
		} catch (Exception e) {
			log.info(e);
		}
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (!verifyObjectDisplayed(journeydesktop) && !verifyObjectDisplayed(imarkter))

		{
			waitUntilElementVisibleWb(driver, menuIcon, 120);
			clickElementJSWithWait(menuIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkViewAll, 120);
			clickElementJS(lnkViewAll);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> addList = driver.findElements(txtSearchBy);
			addList.get(addList.size() - 1).click();
			addList.get(addList.size() - 1).sendKeys("Journey Desktop");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//mark[text()='Journey Desktop']")).click();
		} else if (verifyObjectDisplayed(journeydesktop)) {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage No need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else if (verifyObjectDisplayed(imarkter)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage No need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Failed to Login");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	public void salesForceLogoutMarketer() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, homeIcon, 120);
			clickElementJSWithWait(homeIcon);
			waitUntilElementVisibleBy(driver, profileName, 120);
			clickElementJSWithWait(profileName);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, logOutMarketer, 120);
			clickElementJSWithWait(logOutMarketer);

			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.PASS, "Log Off successful");

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	public void salesForceLogout() throws Exception {

		try {
			/*
			 * waitForSometime(tcConfig.getConfig().get("LongWait")); List<WebElement> img =
			 * driver.findElements(imageLogOut);
			 * 
			 * 
			 * waitForSometime(tcConfig.getConfig().get("lowWait"));
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); img.get(0).click();
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * //waitUntilObjectVisible(driver, viewProfile, 120);
			 * //driver.findElement(viewProfile).click();
			 */
//			mouseoverAndClick(viewProfile);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			clickElementBy(viewProfile);

			WebElement viewpro = driver.findElement(viewProfile);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", viewpro);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, logOut, 120);
			driver.findElement(logOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, username, 120);
			if (verifyObjectDisplayed(username)) {
				tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.PASS,
						"Log outl Successful");
			} else {
				tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
						"Log Off unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	private By logout_inhouse = By.xpath("//li[@class='logOut uiMenuItem']");

	@FindBy(xpath = "//img[@title='In-House Admin 2']")
	WebElement profileImg;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrw;

	public void inHouseAdmin2Logout() throws Exception {

		try {

			mouseoverAndClick(profileImg);

			waitUntilElementVisibleBy(driver, logout_inhouse, 120);
			driver.findElement(logout_inhouse).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	@FindBy(xpath = "//img[@title='In-House Marketer 3']")
	WebElement profileImgMarAgent;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrwMarAgent;
	@FindBy(xpath = "//div//div//li//a[@title='Logout']")
	WebElement logOutMarketingAgent3;

	public void inHouseMarketerAgent3Logout() throws Exception {

	}

}