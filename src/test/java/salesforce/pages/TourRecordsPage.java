package salesforce.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

//import com.gargoylesoftware.htmlunit.javascript.host.Set;
import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TourRecordsPage extends SalesforceBasePage {

	int QOHbeforeDist;
	int QOAafterDist;

	public static final Logger log = Logger.getLogger(HomePage.class);

	public TourRecordsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	final String CHARACTER_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	final String INTEGER_STRING = "0123456789";
	public static String newTourId;
	public static String newTourId2;
	public static String TourId;
	public static List<String> lstSalesRepOnRotation = new ArrayList<String>();
	public static List<String> getTourID = new ArrayList<String>();
	public static String strSalesRepName;
	public static String strNotesTitle;
	public static String strNotesName;
	public static List<String> lstQohValues = new ArrayList<String>();
	public int totalAmount = 0;
	public double marketerCostAmt = 0.00;
	public double dblTotalAmt = 0.00;

	static ArrayList<String> listofIncentiveReasons = new ArrayList<String>();
	public static String strTransactionId;

	protected By txtTourNum = By.xpath("//span[@title='Tour Record Number']");
	protected By salesStoreDropDown = By.xpath(
			"//div[contains(@class,'cTMSalesRepAssignment')]//span[contains(text(),'Sales Store')]/../..//select");
	protected By txtCreditBand = By.xpath("//span[@class='test-id__field-label' and contains(.,'Credit Band')]");
	public By createdTourLink = By.xpath("(//table[contains(@class,'uiVirtualDataTable')]/tbody/tr/th//a)[1]");
	public By newBtn = By.xpath("//div[text()='New']");
	public By latestTour = By.xpath("//span[text()='Tour Record']//div");
	protected By lnkPayments = By.xpath("//span[contains(.,'Payments')]");
	protected By lnkPaymentRecords = By.xpath("//span[@title='Payment Records']");
	protected By lstPaymentId = By.xpath(
			"//table[@data-aura-class='uiVirtualDataTable']//tbody//td//span[@title='Offline Payment']/../../..//th//a");
	protected By detailsTab = By.xpath("//a[@id='detailTab__item']");
	protected By btnManualMatch = By.xpath("//button[contains(.,'Manual Match')]");
	protected By txtCloverId = By.xpath("//input[@name='cloverId']");
	protected By btnEnter = By.xpath("//button[@title='Enter']");
	protected By errorMsg = By.xpath("//span[@class='toastMessage forceActionsText']");
	protected By btnCancel = By.xpath("//button[@title='Cancel']");
	public By nextBtn = By.xpath("//button/span[text()='Next']");
	public By txtSalesStore = By.xpath("//input[contains(@title,'Sales Stores')]");
	public By itemStoreOption = By.xpath("(//li[contains(@class,'default uiAutocompleteOption')]/a)[2]");
	public By txtAppointmentLoc = By.xpath("//input[contains(@title,'Locations')]");
	public By txtLeads = By.xpath("//input[contains(@title,'Leads')]");
	public By txtOwners = By.xpath("//input[contains(@title,'Owners')]");
	public By tourwavetable = By.xpath("//div[@class = 'dataTable tourwaveTable']");
	public By txtMarkettingAgent = By.xpath("//span[text()='Marketing Agent']/../../../../div//input");
	public By lblTourID = By.xpath("//span[text()='Tour Record Number']/../div//span");
	public By saveBtn = By.xpath("//button[@title='Save']");
	// public By bookOptionSelect=By.xpath("//span[text()='Book Appointment']");
	public By bookOptionSelect = By.xpath("//span[text()='Book Appointment']");
	public By Incentivestab = By.xpath("//span[text()='Incentives']");
	public By addPromissedItem = By
			.xpath("//div[@class='slds-col slds-size_1-of-3']/button[contains(.,'Add Promised Item')]");
	public By addPromissedPage = By.xpath("//header[@class='slds-modal__header']/h2[contains(.,'Add Promised Item')]");
	public By addPromissedsearch = By.xpath("//input[@placeholder='Search Product']");
	public By addPromPlus = By.xpath("//label[@class='slds-checkbox_faux']");
	public By addBtn = By.xpath("//button[text()='Add']");
	public By addProdPop = By.xpath("//h2[text()='Add product Value']");
	// public By addProdPopvalue=By.xpath("//div[@class='slds-truncate
	// voucherNumber']");
	public By addProdPopvalue = By.xpath("//div[@class='slds-form-element__control slds-grow']/input");
	// public By
	// addProdPopAdd=By.xpath("(//footer[@class='slds-modal__footer']/button)[2]");
	public By addProdPopAdd = By
			.xpath("//footer[@class='slds-modal__footer']/button[@class='slds-button slds-button_brand']");
	public By checkAll = By.xpath("(//table/thead/tr/th/div/input[@type='checkbox'])[1]");
	public By distributeBtn = By.xpath("//div[@class='slds-col']//button");
	// public By
	// voucherInput=By.xpath("(//div[@class='slds-form-element__control
	// slds-grow']/input)[2]");
	public By voucherInputSecondText = By.xpath("(//div[@class='slds-form-element__control slds-grow']/input)[2]");
	public By voucherInput = By.xpath("(//div[@class='slds-form-element__control slds-grow']/input)[1]");
	// public By voucherInput=By.xpath("//input[@placeholder='Voucher/Serial
	// Number']");
	public By succesMsg = By.xpath("(//div[@class='slds-notify__content']/h2)[2]");

	// public By getAppointmentsBtn=By.xpath("//button[text()='Get
	// Appointments']");
	public By drpDwnMarketingStrtgy = By.xpath("(//select[@name='marketingStrategy'])[1]");
	public By drpDwnMarketingSrcType = By.xpath("(//select[@name='marketingSourceType'])[1]");
	public By lblAvailableSlot = By.xpath("(//span[text()='Available Slots'])[1]");
	protected By lblSecondAvailableSlot = By.xpath("(//span[text()='Available Slots'])[2]");

	public By yesBtn = By.xpath("//button[text()='YES']");
	public By txtAppointmentConfirm = By
			.xpath("//h2[text()='Appointment Confirmed']/../../div//span/div[contains(.,'Success')]");
	public By okBtn = By.xpath("//button[text()='OK']");
	private By dispositionedlnk = By.xpath("//a[contains(text(),'Dispositioned')]");
	public By qScoreDropdwn = By.xpath("//span[text()='Qualification Status']/../..//a[@class='select']");
	@FindBy(xpath = "//button[text()='Get Appointments']")
	WebElement getAppointmentsBtn;

	public By removeSalesStoreIcon = By
			.xpath("//div[contains(@class,'cTMBookSalesStoreSelect')]/..//button[contains(@title,'Remove')]");
	public By removeAppointmentLocIcon = By
			.xpath("//div[contains(@class,'BookLocationSelect')]/..//button[contains(@title,'Remove')]");

	// public By
	// reSalesStoretxt=By.xpath("//div[contains(@class,'SalesStoreSelect')]//input");
	public By reSalesStoretxt = By.xpath("//div[contains(@class,'cTMBookSalesStoreSelect')]//input");
	public By reBookLocation = By.xpath("//div[contains(@class,'BookLocationSelect')]//input");
	// public By reBookLocation=By.xpath("//span[contains(text(),'Appointment
	// Booking Location')]/../../div//a");
	private By ownerSearchBoxEdit = By.xpath("//input[contains(@name,'search-input')]");
	private By salesStoreDrpDwn = By.xpath("//select[@class='slds-select']");
	private By drpMarketingStatergy = By.xpath("//select[@name='controllerFld']");
	private By guestSelectionChkBx = By.xpath("//input[contains(@id,'tour-table_checkbox')]/../label");
	private By checkInBtn = By.xpath("//button[contains(text(),'Check-In')]");
	private By batchPrintBtn = By.xpath("//button[contains(text(),'Batch Print')]");
	private By btnPrint = By.xpath("//div[contains(@class,'oneContent')]/div[2]//button[text()='Print']");
	private By checkedInLink = By.xpath("//a[contains(text(),'Checked-In')]");
	private By selectGuestCheckedIn = By.xpath("//div[@class='slds-form-element__control']/input[@type='checkbox']");
	private By assignRepsBtn = By.xpath("//button[text()='Assign Reps']");
	private By onTourLink = By.xpath("//a[contains(text(),'On Tour')]");
	private By completeTourBtn = By.xpath("(//button[text()='Complete Tour'])[1]");
	private By qaRepEdit = By.xpath("//span[text()='QA Rep']/../..//input");
	private By selectDidTourdrpDwn = By.xpath("//select[@name='didTheyTour']");
	private By saveDispositionBtn = By.xpath("//button[text()='Save']");
	private By dispositionDrpdwn = By.xpath("//select[@name='dispositionStatus']");
	private By quickSearchEdit = By.xpath("//input[@placeholder='Quick Search']");
	public By saleStorDD = By.xpath("//select[@name='selectItem']");
	public By drpmarketingsubsource = By.xpath("(//select[@name='marketingSubSource'])[1]");
	public By drpMarketingcamp = By.xpath("//label[contains(text(),'Campaign')]//following-sibling::div//select");
	public By drpBrand = By.xpath("//label[contains(text(),'Brand')]//following-sibling::div//select");

	// Journey Enhancement

	public By linkAppointmentLoc = By.xpath("//span[contains(text(),'Appointment Booking Location')]/../../div//a");
	public By txtAllotmentType = By.xpath("//span[text()='Allotment Type']/../../div//select");
	private By lnkNewSalesStore = By.xpath(
			"//span[text()='Sales Store']/../../div[2]//a[contains(text(),'" + testData.get("SalesStore") + "')]");
	private By lnkSalesStore = By.xpath("//div[contains(@class,'cTMBookSalesStoreSelect')]//a");
	public By txtBoxSalesStore = By.xpath("//div[contains(@class,'cTMBookSalesStoreSelect')]//input");
	public By drpDwnMarketingCampaign = By.xpath("//label[contains(text(),'Marketing Campaign')]/../div//select");
	public By linkSummary = By.xpath("//a[@title='Summary']");
	public By tblWorkOrder = By.xpath("//span[text()='Work Order History']");
	public By lstHistoryDetails = By.xpath(
			"//span[@title='Work Order History']/../../../../../../div[2]//div[@class='listViewContent']//tbody/tr[1]/td/span");
	public By lnkIncentiveName = By.xpath("//span[@title='" + testData.get("Product") + "']");
	public By lblError = By.xpath("//span[contains(@class,'toastMessage')]");
	public By chkBoxPromisedItem = By.xpath("//tr[contains(@class,'cTMPromisedAssignmentItem')]//input");
	private By btnDistributeIncentive = By.xpath("(//button[contains(text(),'Distribute')])[1]");
	private By lstSalesRepName = By
			.xpath("//div[@class='dataTable tourwaveTable']//div[2]//tbody/tr/td[1]//span[@class='member_name']");
	private By imgOndeckIcon = By
			.xpath("//div[contains(@class,'dataTable tourwaveTable')]//tbody/tr/td[1]/div/span[2]/img");
	private By drpDownDidTheyTour = By.xpath("//span[contains(text(),'Did They Tour')]/../../div//a");
	private By lnkYesTour = By.xpath("//a[@title='Yes']");
	private By drpDownDispositionStatus = By.xpath("//span[text()='Disposition Status']/../../div//a");
	private By lnkMarketingAgent = By.xpath("//span[text()='Marketing Agent']/../../div//div[2]//ul/li[1]/a/span[2]");
	private By txtBoxSalesAgent = By.xpath("//span[text()='Sales Agent 1']/../../div//input");
	// private By txtSuccessMsg =
	// By.xpath("//div[@class='forceVisualMessageQueue']//div[@data-key='success']");
	private By txtSuccessMsg = By
			.xpath("//div[text()='Success']/../span[contains(text(),'Promise List Add Successfully')]");
	private By dispositionEditSuccessMsg = By
			.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	private By lnkDiscoveryTour = By.xpath("//a[@title='Discovery Tour']");
	private By btnAddChildTour = By.xpath("//button[text()='Add']");
	private By countChildTour = By.xpath("//span[@title='Child Tour Records']/../span[2]");
	private By lnkChildTour = By.xpath("//th[@title='Tour Record Number']/../../../tbody/tr[1]/th//a");
	private By lnkSalesStoreChildTour = By.xpath("//span[text()='Sales Store']/../../div[2]//a");
	private By divErrorMsg = By.xpath("//div[contains(@class,'toastContent')]/div");
	private By lnkNotes = By.xpath("//td[@data-label='Recent Notes']//a");
	private By headerNotesPage = By.xpath("//h1[@title='Notes']");
	private By lnkNotesName = By.xpath("//div[contains(@class,'actionBarPlugin')]//table//tbody//th//a");
	private By divHoverNotes = By.xpath("//table[contains(@class,'cTMNotesHover')]//tbody/tr/td/span");
	private By lnkNewNote = By.xpath("//span[text()='Note']//div");
	private By lnkNew = By.xpath("//span[@title='Notes']/../../../../..//a[@title='New']");
	private By textAreaTitle = By.xpath("//span[text()='Title']/../../textarea");
	private By textAreaDesc = By.xpath("//span[text()='Description']/../../textarea");
	private By lnkDispositioned = By.xpath("//a[contains(text(),'Dispositioned')]");
	private By btnSaveOrder = By.xpath("//button[@id='bt-save']");
	// private By quantityValue =
	// By.xpath("(//div[@title='Quantity'])[2]//input");
	private By quantityValue = By.xpath("//div[@class='slds-form-element__control']/input");
	protected By quantityOnHand = By.xpath("//th[@scope = 'row'][2]//div[@title = 'Name']");
	public By btnEdit = By.xpath("//a[@title='Edit']");
	// public By btnEdit = By.xpath("//div[text()='Edit']");
	// Journey Pending TestCases
	private By btnAddSalesRep = By.xpath("//tr[contains(@class,'cTMPowerLineRepeatItem')]/td[2]/span");
	private By salesRepName = By.xpath("//tr[contains(@class,'cTMPowerLineRepeatItem')]/td[1]");
	private By salesRepOnRotation = By.xpath("//td[@class='remove-td']/../td[2]");
	private By salesRepRank = By.xpath("//td[@class='remove-td']/../td[3]");
	private By btnDown = By.xpath("//span[@class='down']");
	private By btnDistributeAddItems = By.xpath("//button[contains(text(),'Distribute Additional Items')]");
	private By inputProductSearchBox = By.xpath("//div[contains(@class,'cTMPremiumProductLookup')]//input");
	private By drpDwnReason = By.xpath("//div[contains(@class,'cTMProductPicklistWithoutGift')]//select");
	private By inputVoucherValue = By.xpath("//div[contains(@class,'addVoucherNumberPanel')]//input");
	private By lnkCancelTour = By.xpath("//li[contains(@class,'slds-is-complete')]/../li[6]/a");
	private By btnMarkAsComplete = By.xpath(
			"//li[contains(@class,'slds-is-complete')]/../../../../..//button[contains(@class,'mark-complete')]");
	private By btnYes = By.xpath("//button[text()='Yes']");
	private By fieldCancel = By.xpath("//span[text()='Cancelled']/../../div[@class='slds-path__link']");
	private By lnkTransportation = By.xpath("//a[@title='Transportation']");
	private By lnkNewTransportation = By.xpath("//div[@class='slds-no-flex']//a[@title='New']");
	private By txtBoxNoOfAdults = By.xpath("//label[text()='Number Of Adults']/../..//input");
	private By textBoxDates = By.xpath("//label[text()='Date']/../input");
	protected By dropoffTime = By.xpath("//legend[text() = 'Drop-Off Time']/..//label[text()='Date']/..//input");
	protected By pickUpTime = By.xpath("//legend[text() = 'Pick-Up Time']/..//label[text()='Date']/..//input");
	private By textAreaTransportationVendor = By.xpath("//label[text()='Transportation Vendor']/../..//textArea");
	private By textAreaPickLocation = By.xpath("//label[text()='Pick-Up Location']/../..//textArea");
	private By lnkTransportationId = By.xpath("//th[@title='Transportation ID']/../../../tbody/tr/th/div/a");
	public By lstVoucherInput = By.xpath("(//div[@class='slds-form-element__control slds-grow']/input)[2]");
	public By printButton = By.xpath("//div[contains(@class,'cTMBatchPrint')]/button[text()='Print']");
	private By tmcletter = By.xpath("//embed[@type='application/pdf']");

	private By guestConfirmationButton = By.xpath("//button[contains (text(),'Guest Confirmation (SFMC)')]");

	@FindBy(xpath = "//span[contains(text(),'From')]//following::div[contains(@class,'form-element')]//a[contains(@class,'datePicker')]")
	WebElement apointmentFromDatePicker;

	@FindBy(xpath = "//td[contains(@class,'slds-is-today slds-is-selected uiDayInMonthCell')]/following-sibling::td")
	WebElement updatedApointmentFromDate;

	@FindBy(xpath = "//span[text()='To']//following::div[contains(@class,'form-element')]//a[contains(@class,'datePicker')]")
	WebElement apointmentToDatePicker;

	@FindBy(xpath = "//td[contains(@class,'slds-is-selected uiDayInMonthCell')]/following-sibling::td")
	WebElement updatedApointmentToDate;

	@FindBy(xpath = "//*[contains(@class,'slds-icon slds-icon-text-default slds-icon_small')]")
	WebElement removeExistingSalesStoreIcon;

	protected By btnSFMC = By.xpath("//button[contains(.,'Guest Confirmation')]");
	protected By txtMsg = By.xpath("//h2[@class='slds-text-heading_medium']");
	protected By btnEmailNotif = By.xpath("//button[contains(.,'YES')]");
	protected By sucessMsg = By
			.xpath("//div[contains(text(),'The guest confirmation email has been successfully sent!')]");
	protected By sucessMsgforCW = By
			.xpath("//div[contains(text(),'The confirmation email has been successfully sent!')]");
	protected By btnOk = By.xpath("//button[contains(.,'OK')]");

	public By sfmcButton = By.xpath("//button[text()='Guest Confirmation (SFMC)']");

	/************** Payment Integration ******************************/

	public By totRetailValue = By.xpath("//lightning-formatted-number[@class='retailCostID']");
	public By refTourDepChkBx = By.xpath("(//tr/td/input[@type='checkbox'])[1]");
	// public By selectPayment = By.xpath("(//label/span[text()='Payment
	// Method'])[1]/../following-sibling::div/div/select[@name='PaymentMethod']");
	// public By selectPayment =
	// By.xpath("(//div/select[@name='PaymentMethod'])[3]");

	protected By selectPayment = By.xpath(
			"//span[contains(text(),'Payment Method')]/../following::div//select[contains(@name,'PaymentMethod')]");

	public By selectCarrier = By.xpath("(//div/select[@name='PaymentMethod'])[3]");
	public By retailCostLbl = By.xpath("//div[@title='Price']");

	public By createPayBtn = By.xpath("(//div/button[contains(text(),'Create Payment')])[1]");
	// public By addIncentiveBtn = By.xpath("//button[contains(text(),'Add
	// Incentive')]");
	// public By addIncentiveBtn = By.xpath("(//button[contains(text(),'Add
	// Incentive')])[2]");
	protected By addIncentiveBtn = By.xpath("//button[contains(., 'Add Incentive')]");

	protected By processPaymentBtn = By.xpath("//button[contains(@title,'Submit for Payment')]");

	protected By incentiveAmountCollected = By.xpath("//input[contains(@placeholder,'Enter Amount')]");

	@FindBy(xpath = "//td[contains (.,'Refundable Tour Deposit')]")
	WebElement lblrefundableTourDeposite;

	@FindBy(xpath = "//p[contains(text(),'This tour’s total gift cost exceeds the company target of 0. Click Cancel to adjust, or Proceed.')]")
	WebElement processPaymentWarningMessage;

	@FindBy(xpath = "//button[contains(@title,'Proceed')]")
	WebElement paymentProceedButton;

	@FindBy(xpath = "//button[contains(text(),'Cancel')]")
	WebElement paymentCntctDtlsCancelButton;

	@FindBy(xpath = "//button[contains(@title, 'Print Receipt')]")
	WebElement printReceiptButton;

	@FindBy(xpath = "//button[contains(@title, 'Refund')]")
	WebElement refundButton;

	@FindBy(xpath = "//button[contains(text(), 'Resend Email/Text')]")
	WebElement resendEmailorTextButton;

	@FindBy(xpath = "//button[contains(text(),'Alt Card')]/following-sibling::button[contains(text(), 'Refund')]")
	WebElement refundButtoninDetailsPopUp;

	/*
	 * @FindBy (xpath = "//button[contains(@title,'Offline Refund')]") WebElement
	 * offlineRefundButton;
	 */

	protected By offlineRefundButton = By.xpath("//button[contains(@title,'Offline Refund')]");

	@FindBy(xpath = "//p[contains(text(),'Before clicking on the')]")
	WebElement refundSuccessMessage;

	@FindBy(xpath = "//button[contains(@title,'Completed')]")
	WebElement refundCompleteButton;

	/*
	 * @FindBy (xpath =
	 * "//p[contains(text(),'Transaction Status')]/following-sibling::p/slot")
	 * WebElement transactionStatus;
	 */

	@FindBy(xpath = "//span[contains(text(),'Transaction Status')]/../following-sibling::div//slot//slot")
	WebElement transactionStatus;

	@FindBy(xpath = "//th[contains(@title,'Payment ID')]/../../../tbody//a")
	WebElement paymentIdLink;

	@FindBy(xpath = "//div[contains(text(),'Payment Record')]")
	WebElement paymentsDetailsPageHdr;

	@FindBy(xpath = "//span[contains(text(),'An Appointment must be booked before you can add premiums to this Tour.')]")
	WebElement transactionErrorMsg;

	@FindBy(xpath = "//div[contains(text(),'Payment Record')]/..//slot//slot")
	WebElement transactionId;

	protected By paymentContactEmailtxt = By.xpath("//input[contains(@placeholder,'Enter Email')]");

	protected By sendEmailChkBox = By.xpath("//span[contains(text(),'Send Email')]/../span");

	protected By paymentContactPhoneNo = By.xpath("//input[contains(@placeholder,'Enter Phone Number')]");

	protected By sendTextChkBox = By.xpath("//span[contains(text(),'Send Text')]/../span");

	@FindBy(xpath = "//button[contains(text(),'Send')]")
	WebElement paymentCntctDtlsSendButton;

	@FindBy(xpath = "//b[contains(text(),'Receipt Successfully Sent!')]")
	WebElement refundReceiptProcessMessage;

	@FindBy(xpath = "//span[contains(text(),'close')]")
	WebElement contactDetailsPopUpCloseBtn;

	protected By txtCloverDeviceName = By.xpath("//input[contains(@placeholder,'Search for Clover')]");

	protected By incentivesTab = By.xpath("(//a/span[text()='Incentives'])[5]");

	@FindBy(xpath = "//span[contains(text(),'Refunded By')]/../following-sibling::div//slot//slot")
	WebElement refundedByUserId;

	@FindBy(xpath = "//span[contains(text(),'Refund Date & Time')]/../following-sibling::div//slot//slot//slot//..")
	WebElement refundDate;

	@FindBy(xpath = "//span[contains(text(),'Payment Method')]/../following-sibling::div//slot//slot")
	WebElement refundMethod;

	@FindBy(xpath = "//div[contains(text(),'Name')]/../../../..")
	WebElement incentiveDetailsTable;

	@FindBy(xpath = "//span[@title='Item']/../../../../..")
	WebElement refundDetailsTable;

	@FindBy(xpath = "//label[contains(text(),'Total Marketer Cost')]/..//*[contains(text(),'$')]")
	WebElement marketerCost;

	protected By paymentLink = By.xpath("//span[text()='Payments']");
	protected By paymentID = By.xpath("//table[contains(@class, 'forceRecordLayout')]/tbody/tr/th/div/a");
	public By paymentIDLbl = By.xpath("//div[contains(@class, 'media__body')]/h1/slot/slot");

	protected By newManualBtn = By.xpath("(//a/div[text()='New'])[2]");
	protected By newPayLabel = By.xpath("//h2[text()='New Payment Record: Deposit']");
	protected By transactionDrpDwn = By
			.xpath("//span[text()='Transaction Status']/../following-sibling::div/div/div/div/a");
	protected By paymentDrpDwn = By.xpath("//span[text()='Payment Method']/../following-sibling::div/div/div/div/a");
	protected By optionDep = By.xpath("//li/a[text()='Deposit']");
	protected By optionCash = By.xpath("//li/a[text()='Cash']");
	protected By savePop = By.xpath("//button[@title='Save']/span");
	protected By manPaymentIDLbl = By
			.xpath("//div[contains(@class, 'media__body')]/h1/slot/slot/lightning-formatted-text");
	protected By editIcon = By.xpath("//button[contains(@title, 'Edit Order ID')]");
	protected By orderInput = By.xpath("//input[contains(@name, 'TMTransactionID')]");
	protected By editSaveBtn = By.xpath("//button[@title='Save']");
	protected By errorPopMsg = By.xpath("//h2[contains(., 'We hit a snag.')]");
	protected By errorCancel = By.xpath("//button[contains(@title, 'Cancel')]");
	protected By editEmailIcon = By.xpath("//button[contains(@title, 'Edit Email')]");
	protected By inputEmail = By.xpath("//input[contains(@name, 'TMEmail')]");
	protected By inputPhone = By.xpath("//input[contains(@name, 'TMPhone')]");
	protected By emailTxt = By.xpath("//a[contains(@class, 'emailui')]");

	protected By carrierDrop = By.xpath("(//input[contains(@class, 'slds-combobox__input')])[6]");
	protected By editBtn = By.xpath("//button[contains(@name, 'Edit')]");
	protected By totalGuestCost = By.xpath("//div/label/span[text()='Total Guest Cost']/../following-sibling::input");
	protected By orderID = By.xpath("//div/label/span[text()='Order ID']/../following-sibling::input");
	protected By refundableAmt = By.xpath("//div/label/span[text()='Refundable Amount']/../following-sibling::input");
	protected By nonRefund = By.xpath("//div/label/span[text()='Non Refundable Amount']/../following-sibling::input");

	protected By closeBtn = By.xpath("//button[contains(@title, 'close')]/lightning-primitive-icon");

	protected By popTransferBtb = By.xpath("//footer/button[text()='Transfer']");
	protected By transferBtn = By.xpath("//button[text()='Transfer']");
	protected By searchTxt = By.xpath("//input[contains(@class, 'slds-lookup__search')]");
	protected By transferPopLbl = By.xpath("//h2[contains(., 'Transfer Payment To Another Tour')]");

	protected By printBtn = By.xpath("//button[contains(., 'Print Receipt')]");
	By incentiveReasonDropDown = By.xpath("//select[@name='Reason']");
	protected By resendBtn = By.xpath("//button[contains(., 'Resend Email/Text')]");
	protected By paymentScrnLbl = By.xpath("//h3[contains(@class, 'paymentFont')]");
	protected By phoneNumTxt = By.xpath("//div/input[contains(@placeholder, 'Enter Phone')]");
	protected By sndTxtChk = By.xpath("//span[contains(., 'Send Text')]/input[@name='recPhone']");
	protected By successMsg = By.xpath("//div[contains(@class, 'success')]/b[text()='Receipt Successfully Sent!']");
	protected By sendBtn = By.xpath("//button[text()='Send']");
	protected By carrierDrp = By.xpath("(//select[@class='slds-select'])[6]");
	protected By lastModTxt = By.xpath("//div/div/span[text()='Last Modified By']/../following-sibling::div/span");

	/******************* Payment Integration ********/

	public void verifyTourRecord() {
		waitUntilElementVisibleBy(driver, createdTourLink, 120);
		String strTourNumber = driver.findElement(createdTourLink).getAttribute("title");
		// clickElementBy(createdTourLink);
		if (verifyObjectDisplayed(createdTourLink)) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.PASS,
					"New tour created ; Tour ID : " + strTourNumber);
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.FAIL,
					"Unable to create new Tour Record");
		}
	}

	public void openCreatedTourToVerify() {
		boolean Flag = false;
		clickElementBy(createdTourLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> errorMsgList = driver
				.findElements(By.xpath("//span[contains(text(),'not eligible to Tour')]/../../../span"));
		if (errorMsgList.size() == 0) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.FAIL,
					"No error message is displayed");
		}
		for (int iterator = 0; iterator < errorMsgList.size(); iterator++) {
			if (errorMsgList.get(iterator).getAttribute("style").contains("rgb(255, 0, 0);")) {
				Flag = true;
				break;
			} else {
				Flag = false;
			}
		}
		if (Flag == true) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.PASS,
					"Error message" + " is displayed in RED color");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.FAIL,
					"Error message is NOT" + " displayed in RED color");
		}
	}

	public void selectTourType() {
		waitUntilElementVisibleBy(driver, createdTourLink, 120);
		if (verifyObjectDisplayed(newBtn)) {
			tcConfig.updateTestReporter("TourRecordsPage", "selectTourType", Status.PASS,
					"User is navigated to Tour Records Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "selectTourType", Status.FAIL,
					"User is Not navigated to Tour Records Page");
		}
		clickElementBy(newBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement ownerTourRecordRadioBtn = driver.findElement(By.xpath(
				"//div[@class='changeRecordTypeOptionRightColumn']/span[text()='" + testData.get("TourType") + "']"));
		ownerTourRecordRadioBtn.click();
		clickElementBy(nextBtn);
	}

	public void createNewTour() {

		waitUntilElementVisibleBy(driver, txtSalesStore, 120);
		if (testData.get("NoSalesStore").equalsIgnoreCase("N")) {
			fieldDataEnter(txtSalesStore, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(By.xpath("(//div[contains(@title,'" + testData.get("SalesStore") + "')])[1]"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		fieldDataEnter(txtAppointmentLoc, testData.get("AppointmentLoc"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement appLocOption = driver
				.findElement(By.xpath("(//div[contains(@title,'" + testData.get("AppointmentLoc") + "')])[1]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		appLocOption.click();
		clickElementBy(qScoreDropdwn);
		WebElement qScoreValue = driver
				.findElement(By.xpath("//a[@title='" + testData.get("QualificationScore") + "']"));
		qScoreValue.click();
		if (testData.get("TourType").contains("Lead")) {
			fieldDataEnter(txtLeads, testData.get("LeadName"));
		} else if (testData.get("TourType").contains("Owner")) {
			fieldDataEnter(txtOwners, testData.get("LeadName"));
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement txtLeadsOption = driver.findElement(By.xpath("//div[@title='" + testData.get("LeadName") + "']"));
		txtLeadsOption.click();
		fieldDataEnter(txtMarkettingAgent, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement marketingAgentOption = driver
				.findElement(By.xpath("//div[@title='" + testData.get("AgentName") + "']"));
		marketingAgentOption.click();
		clickElementBy(saveBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lblTourID, 120);
		System.out.println("Tour ID :" + driver.findElement(lblTourID).getText().trim());
		if (verifyObjectDisplayed(lblTourID)) {
			newTourId = driver.findElement(lblTourID).getText().trim();
			tcConfig.updateTestReporter("TourRecordsPage", "createNewTour", Status.PASS,
					"Tour ID : " + driver.findElement(lblTourID).getText().trim() + " has been created");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "createNewTour", Status.FAIL, "No tour has been created");
		}

	}
	/*
	 * public void bookAppointment(){ waitUntilObjectVisible(driver,
	 * bookOptionSelect, 120); clickElementBy(bookOptionSelect);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * if(testData.get("NoSalesStore").equalsIgnoreCase("Y")){
	 * clickElementBy(removeSalesStoreIcon);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * clickElementBy(removeSalesStoreIcon);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); try{
	 * if(getAppointmentsBtn.isEnabled()==false ||
	 * getAppointmentsBtn.getAttribute("disabled")=="true"){
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.PASS, "User is unable to book appointment if Sales store is blank");
	 * }else{ tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL, "User is able to book appointment if Sales store is blank"); }
	 * }catch(NoSuchElementException e){ System.out.println("catched");
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL, "User is able to book appointment if Sales store is blank"); }
	 * }else{ clickElementBy(removeSalesStoreIcon);
	 * fieldDataEnter(reSalesStoretxt,testData.get("SalesStore"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(By.xpath("//span[contains(text(),'"+testData.get(
	 * "SalesStore")+"')][1]")).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * clickElementBy(removeAppointmentLocIcon);
	 * fieldDataEnter(reBookLocation,testData.get("AppointmentLoc"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(By.xpath("//span[contains(text(),'"+testData.get(
	 * "AppointmentLoc")+"')]")).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * 
	 * new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * mouseoverAndClick(getAppointmentsBtn); //clickElementBy(getAppointmentsBtn);
	 * waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
	 * clickElementBy(lblAvailableSlot); waitUntilElementVisibleBy(driver, yesBtn,
	 * 120); clickElementBy(yesBtn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver, txtAppointmentConfirm,120);
	 * if(verifyObjectDisplayed(txtAppointmentConfirm)){
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.PASS, "New appointment has been created"); }else{
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL, "User is unable to book an appointment"); }
	 * waitUntilElementVisibleBy(driver, okBtn,120); clickElementBy(okBtn);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); }
	 * 
	 * }
	 */

	public By Additemsvalue = By.xpath("(//input[contains(@id,'input')])[3]");
	public By Additems = By.xpath("//footer[@class='slds-modal__footer']//button[contains(.,'Add')]");
	public By QOHVAlueDist = By.xpath("(//div[@title='Name'])[2]");

	public void addPromiseItemAndDistribute() throws AWTException {
		waitUntilObjectVisible(driver, Incentivestab, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(Incentivestab);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(addPromissedItem)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "Add Promise", Status.PASS, "User is able to add Promise");
			clickElementBy(addPromissedItem);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, addPromissedPage, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(addPromissedsearch);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(addPromissedsearch, testData.get("Product"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		QOHbeforeDist = Integer.parseInt(driver.findElement(QOHVAlueDist).getText());
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(addPromPlus).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(addBtn).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(Additemsvalue)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(Additemsvalue).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(Additemsvalue).sendKeys("1000");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(Additems).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			System.out.println("Add Items Values is not present");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(checkAll).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Robot rob = new Robot();
		rob.mouseWheel(5);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		// WebElement distribute = driver.findElement(distributeBtn);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", distribute);

		List<WebElement> distribute = driver.findElements(distributeBtn);
		distribute.get(distribute.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.findElement(voucherInput).click();
		// driver.findElement(voucherInput).clear();
		fieldDataEnter(voucherInput, testData.get("voucherNumber"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(addProdPopAdd).click();
		waitUntilObjectVisible(driver, succesMsg, 120);

		if (verifyObjectDisplayed(succesMsg)) {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
					"New appointment has been created");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
					"User is unable to book an appointment");
		}

	}

	/**
	 * modified by c24125
	 */
	public void bookAppointment() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, bookOptionSelect, 120);
		clickElementJSWithWait(bookOptionSelect);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("NoSalesStore").equalsIgnoreCase("Y")) {
			if (verifyObjectDisplayed(removeSalesStoreIcon)) {
				clickElementJSWithWait(removeSalesStoreIcon);
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
						"User is able to remove Sales store is blank");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.FAIL,
						"User is not able to remove Sales store is blank");

			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				if (getAppointmentsBtn.isEnabled() == false || getAppointmentsBtn.getAttribute("disabled") == "true") {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
							"User is unable to book appointment if Sales store is blank");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
							"User is able to book appointment if Sales store is blank");
				}
			} catch (NoSuchElementException e) {
				System.out.println("catched");
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is able to book appointment if Sales store is blank");
			}
		} else {
			if (verifyObjectDisplayed(removeSalesStoreIcon)) {
				clickElementJSWithWait(removeSalesStoreIcon);
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
						"User is able to remove Sales store is blank");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
						"Removing sales store not required");

			}

			try {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, reSalesStoretxt, 120);
				fieldDataEnter(reSalesStoretxt, testData.get("SalesStore"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]"))
						.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				e.getMessage();
			}

			try {
				clickElementJSWithWait(removeAppointmentLocIcon);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(reBookLocation, testData.get("AppointmentLoc"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"))
						.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				e.getMessage();
			}
			try {
				waitUntilElementVisibleBy(driver, drpBrand, 120);
				new Select(driver.findElement(drpBrand)).selectByVisibleText(testData.get("Brand"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {
				e.getMessage();
			}
			new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
				new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			waitUntilElementVisible(driver, getAppointmentsBtn, 120);
			mouseoverAndClick(getAppointmentsBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
			clickElementJS(lblAvailableSlot);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, yesBtn, 120);
			clickElementJS(yesBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, okBtn, 120);
			clickElementJS(okBtn);
			tcConfig.updateTestReporter("TourRecordsPage", "BookAppotment", Status.PASS,
					"BookAppoitment is succesfully");
		}

	}

	// Modify by Ajib Parida
	public void bookAppointment_Modified() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// waitUntilObjectVisible(driver, bookOptionSelect, 120);
		WebElement ele = driver.findElements(bookOptionSelect).get(1);
		waitUntilElementVisibleWb(driver, ele, 120);
		clickElementWb(ele);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		//
		//
		new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
			new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// new
			// Select(driver.findElement(drpMarketingcamp)).selectByIndex(1);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

		waitUntilElementVisible(driver, getAppointmentsBtn, 120);
		clickElementJSWithWait(getAppointmentsBtn);
		waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
		clickElementJSWithWait(lblAvailableSlot);
		waitUntilElementVisibleBy(driver, yesBtn, 120);
		clickElementJSWithWait(yesBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtAppointmentConfirm, 120);
		if (verifyObjectDisplayed(txtAppointmentConfirm)) {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
					"New appointment has been created");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
					"User is unable to book an appointment");
		}
		waitUntilElementVisibleBy(driver, okBtn, 120);
		clickElementJSWithWait(okBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// }

	}

	public By SaleStoretextBoxdropdown = By.xpath("//select[@class='slds-select']");

	public void getSaleStoretextBoxdropdown() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, SaleStoretextBoxdropdown, 120);
		if (verifyObjectDisplayed(SaleStoretextBoxdropdown)) {
			new Select(driver.findElement(SaleStoretextBoxdropdown)).selectByVisibleText(testData.get("SalesStore"));
			tcConfig.updateTestReporter("TourRecordsPage", "getSaleStoretextBoxdropdown", Status.PASS,
					"salas Store update successfully");

		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "getSaleStoretextBoxdropdown", Status.FAIL,
					"Getting error: ");
		}
	}

	public void searchForTour() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, 120);
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("TourRecordsPage", "searchForTour", Status.PASS,
					"User is navigated to Reservations Page");
			if (testData.get("TourNo") == null || testData.get("TourNo").isEmpty()
					|| testData.get("TourNo").contentEquals("")) {
				fieldDataEnter(ownerSearchBoxEdit, newTourId);
			} else {
				fieldDataEnter(ownerSearchBoxEdit, testData.get("TourNo"));
			}
			driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			WebElement seacrhResOwnerLink = driver.findElement(By.xpath("//th/span/a[contains(text(),'"
					+ testData.get("TourNo") + "') or contains(text(),'" + newTourId + "')]"));
			seacrhResOwnerLink.click();
		}
	}

	// public void getlatestTour(){
	//
	// waitUntilElementVisibleBy(driver, latestTour, 120);
	// newTourId = driver.findElement(latestTour).getText();
	// System.out.println(newTourId);
	// driver.findElement(latestTour).click();
	// }

	public void assignSaleRep() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, quickSearchEdit, 120);
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitUntilElementVisibleBy(driver, quickSearchEdit, 120);
		fieldDataEnter(quickSearchEdit, LeadsPage.createdTourID);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(
				By.xpath("//div[@title='Tour Record Number']/a[text()='" + LeadsPage.createdTourID + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in Sales Rep Assignment Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Tour has NOT been displayed in Sales Rep Assignment Page");
		}

		clickElementBy(guestSelectionChkBx);
		clickElementBy(checkInBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> popup = driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
		if (popup.size() > 0) {
			popup.get(0).click();
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(checkedInLink);

		getSalesRepRotationOrder();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(selectGuestCheckedIn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(assignRepsBtn);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * clickElementBy(onTourLink);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * if(verifyObjectDisplayed(completeTourBtn)){
		 * tcConfig.updateTestReporter("TourRecordsPage", "Assign Rep", Status.PASS,
		 * "Tour has been Assigned"); } clickElementBy(completeTourBtn);
		 * waitUntilElementVisibleBy(driver, saveDispositionBtn, 120);
		 * fieldDataEnter(qaRepEdit, testData.get("AgentName"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * driver.findElement(By.xpath("//span[text()='"+
		 * testData.get("AgentName")+"']")).click(); new
		 * Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText( "Yes");
		 * new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(
		 * testData.get("DispositionStatus")); clickElementBy(saveDispositionBtn);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 */
	}

	public void getSalesRepRotationOrder() {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, salesStoreDrpDwn, 120);
			new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lstSalesRepName, 120);
			List<WebElement> ele = driver.findElements(lstSalesRepName);
			for (int i = 0; i < ele.size(); i++) {
				lstSalesRepOnRotation
						.add((ele.get(i).getText().substring(0, ele.get(i).getText().length() - 3)).trim());
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "getSalesRepRotationOrder", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void completeTourAndDisposition() {
		waitUntilElementVisibleBy(driver, onTourLink, 120);
		clickElementBy(onTourLink);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(completeTourBtn)) {
			tcConfig.updateTestReporter("TourRecordsPage", "Assign Rep", Status.PASS, "Tour has been Assigned");
		}
		clickElementBy(completeTourBtn);
		waitUntilElementVisibleBy(driver, saveDispositionBtn, 120);
		fieldDataEnter(qaRepEdit, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//span[text()='" + testData.get("AgentName") + "']")).click();
		new Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText("Yes");
		new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(testData.get("DispositionStatus"));
		clickElementBy(saveDispositionBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public By saleRep = By.xpath("//table[@id='sortable1']/tbody/tr[2]/td[1]");
	public By addRight = By.xpath("//div[@class='add']");
	public By moveUp = By.xpath("//span[@class='up']");
	public String saleRepname;

	public void salesRepRotation() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, 120);
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		saleRepname = driver.findElement(saleRep).getText();
		driver.findElement(saleRep).click();
		driver.findElement(addRight).click();
		if (verifyObjectDisplayed(
				By.xpath("//table[@id='sortable2']/tbody/tr/td[contains(text(),'" + saleRepname + "')]"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "salesRepRotation", Status.PASS,
					"sale rep has been toggeled");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "salesRepRotation", Status.FAIL,
					"sale rep has been not been toggeled");
		}
		driver.findElement(By.xpath("//table[@id='sortable2']/tbody/tr/td[contains(text(),'" + saleRepname + "')]"))
				.click();
		driver.findElement(moveUp).click();
		tcConfig.updateTestReporter("TourRecordsPage", "salesRepRotation", Status.PASS, "sale rep has been toggeled");
	}

	/*
	 * public void dispositionTour(){ waitUntilElementVisibleBy(driver,
	 * salesStoreDrpDwn, 120); new
	 * Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData
	 * .get("SalesStore")); waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * newTourId = LeadsPage.createdTourID; fieldDataEnter(quickSearchEdit,
	 * newTourId); driver.switchTo().activeElement().sendKeys(Keys.ENTER);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); if
	 * (verifyObjectDisplayed(By.xpath(
	 * "//div[@title='Tour Record Number']/a[text()='"+newTourId+"']"))){
	 * tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour",
	 * Status.PASS, "Tour has been displayed in Sales Rep Assignment Page"); }else{
	 * tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour",
	 * Status.FAIL, "Tour has NOT been displayed in Sales Rep Assignment Page"); }
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * clickElementBy(guestSelectionChkBx);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * clickElementBy(checkInBtn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); List<WebElement>
	 * popup=driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
	 * if(popup.size()>0){ popup.get(0).click(); }
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * clickElementBy(checkedInLink);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * clickElementBy(selectGuestCheckedIn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * clickElementBy(assignRepsBtn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * clickElementBy(onTourLink);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * clickElementBy(completeTourBtn); waitUntilElementVisibleBy(driver,
	 * saveDispositionBtn, 120); fieldDataEnter(qaRepEdit,
	 * testData.get("AgentName"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(By.xpath("//span[text()='"+
	 * testData.get("AgentName")+"']")).click(); new
	 * Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText("Yes" );
	 * new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(
	 * testData.get("DispositionStatus")); clickElementBy(saveDispositionBtn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); }
	 */
	public void dispositionTour() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String firsttourmem = null;

		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, 120);
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		newTourId = LeadsPage.createdTourID;
		fieldDataEnter(quickSearchEdit, newTourId);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + newTourId + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in Sales Rep Assignment Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Tour has NOT been displayed in Sales Rep Assignment Page");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(guestSelectionChkBx);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(checkInBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> popup = driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
		if (popup.size() > 0) {
			popup.get(0).click();
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(checkedInLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Package Number']/a[text()='" + newTourId + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour successfully checked in and shown in check in tab");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Failed while checking in tour");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, tourwavetable, 120);
		if (verifyObjectDisplayed(tourwavetable)) {
			List<WebElement> tourmember = driver.findElements(By.xpath("//span[@class = 'member_name']"));
			if (tourmember.size() > 0) {
				firsttourmem = ((tourmember.get(0).getText().split("\\("))[0]).trim();
				tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
						"First tour member is: " + firsttourmem);

			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
						"No tour member is shown in the table");
			}

		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL, "Tour Wave table not found");
		}
		clickElementBy(selectGuestCheckedIn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(assignRepsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, tourwavetable, 120);
		if (verifyObjectDisplayed(tourwavetable)) {
			List<WebElement> tourmember = driver.findElements(By.xpath("//span[@class = 'member_name']"));
			String assignttourmem = ((tourmember.get(tourmember.size() - 1).getText().split("\\("))[0]).trim();
			if (assignttourmem.equals(firsttourmem)) {
				tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
						"Tour is getting assigned to Sales Rep with 1st sales Rep in the que");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
						"Tour is not getting assigned to Sales Rep with 1st sales Rep in the que");
			}
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL, "Tour Wave table not found");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(onTourLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + newTourId + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Successfully on ON tour link and tour present");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Failed while checking tour on- ON Tour link");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(completeTourBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, qaRepEdit, 120);
		fieldDataEnter(qaRepEdit, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//span[text()='" + testData.get("AgentName") + "']")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText("Yes");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(testData.get("DispositionStatus"));

		clickElementJSWithWait(saveDispositionBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, dispositionedlnk, 120);
		if (verifyObjectDisplayed(dispositionedlnk)) {
			clickElementBy(dispositionedlnk);
			if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + newTourId + "']"))) {
				tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
						"Tour successfully dispositioned");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
						"Failed while dispositioning the tour");
			}
		}
	}

	private By Salestore = By.xpath("//div[contains(@class,'active lafPageHost')]//select[@name='selectItem']");
	private By fltrbtn = By.xpath("//button[contains(text(),'Filter')]");
	private By print = By.xpath("(//button[contains(text(),'Print')])[2]");
	private By scoreband = By.xpath("//td//div[@class='slds-truncate' and text()='Q' or text()='NQ']");

	public void printSearchRecord() throws AWTException {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, batchPrintBtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(batchPrintBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		Select sel = new Select(driver.findElement(Salestore));
		sel.selectByVisibleText(testData.get("SalesStore"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> Date = driver.findElements(
				By.xpath("//div[contains(@class,'dateTime-inputDate form')]//input[@class='form-control input']"));
		Date.get(Date.size() - 1).clear();
		Date.get(Date.size() - 1).sendKeys(testData.get("TourDate"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fltrbtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> CheckEntries = driver.findElements(By.xpath("//span[@class='slds-checkbox--faux']"));
		CheckEntries.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (isAlertPresent()) {

			String AlrtMsg = driver.switchTo().alert().getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().alert().accept();
			System.out.println(AlrtMsg);
			tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
					"The Entries are not eligible to print : Alert Displayed -  " + AlrtMsg);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			Robot rob = new Robot();
			rob.mouseWheel(10);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> score = driver.findElements(scoreband);
			for (int i = 0; i <= score.size() - 1; i++) {
				String band = score.get(i).getText();
				tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
						"The Scoreband for entry " + i + " is " + band);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

			clickElementBy(print);

			if (isAlertPresent()) {

				String AlrtMsg = driver.switchTo().alert().getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.switchTo().alert().accept();
				System.out.println(AlrtMsg);
				tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
						"The Entries are not eligible to print : Alert Displayed -  " + AlrtMsg);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				String parent = driver.getWindowHandle();
				SwitchtoWindow();
				System.out.println(driver.getWindowHandles().size());

				tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS, "The Entries are printed");

				driver.switchTo().window(parent);

			}
		}

		/*
		 * if(driver.findElement(By.xpath(
		 * "//a[@class='slds-tabs_default__link color-book']"
		 * )).getText().contains("0")){
		 * 
		 * tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
		 * "The selected Sales store has Zero entries.");
		 * 
		 * } else{
		 * 
		 * 
		 * List<WebElement> checkbox= driver.findElements(guestSelectionChkBx);
		 * 
		 * for (int i=0; i<checkbox.size(); i++){
		 * 
		 * checkbox.get(i).click(); } }
		 * if(verifyElementDisplayed(driver.findElement(batchPrintBtn))){
		 * tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
		 * "Batch Print button is present in the sreen");
		 * 
		 * clickElementBy(batchPrintBtn); }else{
		 * tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.FAIL,
		 * "Batch Print button is not present in the sreen"); }
		 */

	}

	private By marketstrategy = By.xpath("//select[@class='slds-select' and @name='controllerFld']");

	public void marketstrategyValidation() throws AWTException {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, batchPrintBtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(batchPrintBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		Select sel = new Select(driver.findElement(Salestore));
		sel.selectByVisibleText(testData.get("SalesStore"));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		Select strategy = new Select(driver.findElement(marketstrategy));
		strategy.selectByVisibleText(testData.get("MarketingStrategy"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> Date = driver.findElements(
				By.xpath("//div[contains(@class,'dateTime-inputDate form')]//input[@class='form-control input']"));
		Date.get(Date.size() - 1).clear();
		Date.get(Date.size() - 1).sendKeys(testData.get("TourDate"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fltrbtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> CheckEntries = driver.findElements(By.xpath("//span[@class='slds-checkbox--faux']"));
		CheckEntries.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (isAlertPresent()) {

			String AlrtMsg = driver.switchTo().alert().getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().alert().accept();
			System.out.println(AlrtMsg);
			tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
					"The Entries are not eligible to print : Alert Displayed -  " + AlrtMsg);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			Robot rob = new Robot();
			rob.mouseWheel(10);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> score = driver.findElements(scoreband);
			for (int i = 0; i <= score.size() - 1; i++) {
				String band = score.get(i).getText();
				tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
						"The Scoreband for entry " + i + " is " + band);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

			clickElementBy(print);

			if (isAlertPresent()) {

				String AlrtMsg = driver.switchTo().alert().getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.switchTo().alert().accept();
				System.out.println(AlrtMsg);
				tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS,
						"The Entries are not eligible to print : Alert Displayed -  " + AlrtMsg);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				String parent = driver.getWindowHandle();
				SwitchtoWindow();
				System.out.println(driver.getWindowHandles().size());

				tcConfig.updateTestReporter("TourRecordsPage", "batchPrintBtn", Status.PASS, "The Entries are printed");

				driver.switchTo().window(parent);

			}
		}
	}

	public void validateMarketingStatergy() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpMarketingStatergy, 120);
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("MarketingStrategy"));
		clickElementBy(btnPrint);
		Alert alert = driver.switchTo().alert();
		alert.accept();

	}

	public void validateTourStatus() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> disp = driver.findElements(By.xpath(("//span[text()='" + testData.get("TourStatus") + "']")));
		if (disp.size() > 0) {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatus", Status.PASS,
					"Tour status has been changed To : " + testData.get("TourStatus"));
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatus", Status.FAIL,
					"tour status not changed to : " + testData.get("TourStatus"));
		}

	}

	public By adddistrubuted = By.xpath("//button[contains(.,'Distribute Additional Items')]");
	public By adddistproduct = By.xpath("(//span[contains(.,'" + testData.get("Product") + "')])[2]");
	public By adddistAdd = By.xpath("//button[@class='slds-button slds-button_brand' and contains(.,'Add')]");
	public By adddistvoucher = By.xpath("//input[@placeholder='Voucher/Serial Number']");

	public void AddDistrubutedItem() {
		waitUntilElementVisibleBy(driver, adddistrubuted, 120);
		clickElementBy(adddistrubuted);
		clickElementBy(adddistrubuted);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(adddistvoucher).sendKeys("1234");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(adddistAdd);
		waitUntilObjectVisible(driver, succesMsg, 120);
		if (verifyObjectDisplayed(succesMsg)) {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
					"New appointment has been created");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
					"User is unable to book an appointment");
		}

	}

	// @UTSAV
	public By incentiveLink = By.xpath("//span[text()='Incentives']");
	public By paymentsLink = By.xpath("//span[text()='Payments']");
	public By addPromisedItemBtn = By.xpath("//button[contains(text(),'Add Promised Item')]");
	public By itemLists = By.xpath("//div[@title='Name']");
	public By cancelBtn = By.xpath("//button[contains(text(),'Cancel')]");

	public void checkIncentives() {

		waitUntilObjectVisible(driver, incentiveLink, 120);
		clickElementBy(incentiveLink);

		waitUntilObjectVisible(driver, addPromisedItemBtn, 120);
		clickElementBy(addPromisedItemBtn);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> promisedItemList = driver.findElements(itemLists);

		for (int i = 1; i <= promisedItemList.size(); i++) {
			if (promisedItemList.get(i).getText().equalsIgnoreCase("Wyndham Rewards Card")) {
				tcConfig.updateTestReporter("TourRecordsPage", "checkIncentives", Status.FAIL,
						"User is able to see Wyndham Rewards Point Item even the quantity on hand is Zero");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "checkIncentives", Status.PASS,
						"Wyndham Rewards Point not visible when quantity on hand is Zero");
			}
		}

		clickElementBy(cancelBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public By tourdetailstab = By.xpath("//span[contains(text(),'Tour Details')]");
	public By guestbutton = By.xpath("//button[contains(text(),'Guest Signature')]");

	public void clickTourDetails() {
		waitUntilObjectVisible(driver, tourdetailstab, 120);
		clickElementBy(tourdetailstab);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(guestbutton)) {
			tcConfig.updateTestReporter("TourRecordsPage", "clickTourDetails", Status.PASS,
					"Clicked on Tour Details Tab");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "clickTourDetails", Status.FAIL,
					"Failed while clicking on Tour Details Tab");
		}
	}

	public void clickonguestSign() {
		waitUntilObjectVisible(driver, tourdetailstab, 120);
		clickElementBy(guestbutton);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		tcConfig.updateTestReporter("TourRecordsPage", "clickonguestSign", Status.PASS,
				"Clicked on Guest Signature Tab");

	}

	public By canvas = By.xpath("//canvas[contains(@id,'intial')]");
	public By submit = By.xpath("//button[@id='submitBtn']");

	public void SignatureValidate() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		waitUntilObjectVisible(driver, canvas, 120);
		Actions builder = new Actions(driver);
		WebElement canvasElement = driver.findElement(canvas);
		Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
				.moveByOffset(50, 20).release().build();
		drawAction.perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(submit);
		tcConfig.updateTestReporter("TourRecordsPage", "SignatureValidate", Status.PASS,
				"Signed successfully and clicked on submit button");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));

	}

	public By QOHVAlue = By.xpath("(//div[@title='Name'])[2]");
	public By cancelbtn = By.xpath("//button[contains(text(),'Cancel')]");

	public By searchTourNo = By.xpath("//input[@class='slds-input']");
	public By clickTourNo = By.xpath(
			"//span[text()='Tour Record Number']/ancestor::div[@data-aura-class='forceInlineEditGrid']//a[@title='"
					+ testData.get("TourNo") + "']");
	// public By clickTourNo =By.xpath("(//a[@class='slds-truncate
	// outputLookupLink slds-truncate forceOutputLookup'])[1]");
	// public By clickTourNo
	// =By.xpath("//a[text()='"+testData.get("TourNo")+"']/parent::span");

	public By btnAddPrmItem = By.xpath("(//button[contains(text(),'Add Promised Item')])[1]");
	public By searchProduct = By.xpath("(//input[@class='slds-input slds-combobox__input'])[2]");
	public By checkBox = By.xpath("//label[@class='slds-checkbox_faux']");
	public By btnAdd = By.xpath("//button[@class='slds-button slds-button_brand']");
	public By txtValue = By.xpath("//div[@class='slds-form-element__control slds-grow']/input");
	public By btnAddValue = By.xpath("//footer[@class='slds-modal__footer']//button[contains(.,'Add')]");
	public By chkValue = By.xpath("//tr[contains(@class,'parent')]//input[@type='checkbox']");
	public By btnDistribute = By.xpath(
			"//button[@class='slds-button slds-button_neutral uiButton--default uiButton--brand uiButton forceActionButton']");
	public By txtVoucher = By.xpath("(//div[@class='slds-truncate voucherNumber']//../input)[1]");
	public By btnAddDistribute = By.xpath("//button[@class='slds-button slds-button_brand']");
	public By btnDistributeAdditional = By.xpath("//button[contains(.,'Distribute Additional Items')]");
	public By txtSearch = By.xpath("//div[contains(@class,'ProductLook')]//input[@role='combobox']"); // ul/li[@class='slds-listbox__item']/span
	public By drpReason = By.xpath("//div[@class='slds-select_container']/select");
	public By drpQuantity = By.xpath("//label[contains(text(),'Quantity')]//following::input");
	public By txtserialNo = By.xpath("//input[@placeholder='Voucher/Serial Number']");
	public By txtValueNo = By.xpath("//input[@placeholder='Value']");
	public By addbtn = By.xpath("//button[@class='slds-button slds-button_brand']");

	// public By chkDistribute =
	// By.xpath("(//tr[contains(@class,'parent')]//input[@type='checkbox'])[2]");
	// //tr[contains(@class,'parent')][1])//input[@type='checkbox']

	public By chkDistribute = By.xpath("(//div[text()='Distributed Items']//..//..//..//..//input)[2]");
	// public By chkDistribute=
	// By.xpath("//tr[td[div[span[contains(text(),'Activator')]]]]/td[1]/div/input");
	public By btnReturn = By.xpath("//button[contains(.,'Return')]");
	public By returnReason = By.xpath("//div[contains(text(),'American Express® Reward Card')]/following::select");
	public By btnRturn = By.xpath("//div[@class='slds-modal__container']//button[contains(.,'Return')]");

	public void validateDistributeTour() {

		waitUntilObjectVisible(driver, searchTourNo, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(searchTourNo).sendKeys(testData.get("TourNo"));
		driver.findElement(searchTourNo).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, clickTourNo, 120);
		driver.findElement(clickTourNo).click();
		waitUntilObjectVisible(driver, Incentivestab, 120);
		if (verifyObjectDisplayed(Incentivestab)) {
			tcConfig.updateTestReporter("TourRecordsPage", "clickTourDetails", Status.PASS,
					"Clicked on Tour number successfully");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "clickTourDetails", Status.FAIL,
					"Failed while clicking on Tour number");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, Incentivestab, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(Incentivestab);
		/*
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 */
		waitUntilObjectVisible(driver, btnAddPrmItem, 120);
		clickElementBy(btnAddPrmItem);
		waitUntilObjectVisible(driver, searchProduct, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(searchProduct).sendKeys(testData.get("SearchProduct"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(checkBox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, btnAdd, 120);
		clickElementBy(btnAdd);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(txtValue).clear();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(txtValue).sendKeys(testData.get("ProductValue"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(btnAddValue);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, chkValue, 120);
		driver.findElement(chkValue).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,300)", "");
		clickElementBy(btnDistribute);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(txtVoucher).sendKeys(testData.get("VoucherNumber"));
		// driver.findElement(txtVoucher).sendKeys(Keys.ENTER);
		clickElementBy(btnAddDistribute);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, btnDistributeAdditional, 120);
		clickElementBy(btnDistributeAdditional);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(txtSearch).sendKeys(testData.get("SearchProduct"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy((By.xpath("//li//span[contains(text(),'" + testData.get("SearchProduct") + "')]")));

		new Select(driver.findElement(drpReason)).selectByIndex(1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// fieldDataEnter(drpQuantity, testData.get("Quantity"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(drpQuantity).clear();
		driver.findElement(drpQuantity).sendKeys("1");
		// new Select(driver.findElement(drpQuantity)).selectByIndex(1);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(txtserialNo, testData.get("VoucherNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(txtValueNo, testData.get("ProductValue"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(addbtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, chkDistribute, 120);
		driver.findElement(chkDistribute).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, btnReturn, 120);
		tcConfig.updateTestReporter("TourRecordsPage", "validateDistributeTour", Status.PASS,
				"Return button is visible");
		clickElementBy(btnReturn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		new Select(driver.findElement(returnReason)).selectByIndex(1);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, btnRturn, 120);
		clickElementBy(btnRturn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("TourRecordsPage", "validateDistributeTour", Status.PASS,
				"Successfully returned product");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void addPromiseItemAndcheckQOH() {
		/*
		 * int QOHValueScreen; int QOAValue;
		 */
		waitUntilObjectVisible(driver, Incentivestab, 120);
		clickElementBy(Incentivestab);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(addPromissedItem)) {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
					"User is unable to book appointment if Sales store is blank");
			clickElementBy(addPromissedItem);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		}

		waitUntilObjectVisible(driver, addPromissedPage, 120);
		clickElementBy(addPromissedsearch);
		fieldDataEnter(addPromissedsearch, testData.get("Product"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		int QOHValueScreen = Integer.parseInt(driver.findElement(QOHVAlue).getText());
		int QOAValue = Integer.parseInt(testData.get("QOH"));
		if (QOHValueScreen == QOAValue) {
			tcConfig.updateTestReporter("TourRecordsPage", "addPromiseItemAndcheckQOH", Status.PASS,
					"QOH validated. Same value is displayed");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "checkIncentives", Status.PASS, "QOA validation failed");
		}

		waitUntilObjectVisible(driver, cancelbtn, 120);
		clickElementBy(cancelbtn);

	}

	public By clickDisstrubutItems = By.xpath("(//button[contains(.,'Distribute Additional Items')])");
	public By product = By.xpath("//input[@class=' slds-input slds-combobox__input']");
	public By productSublist = By.xpath("//span[contains(@class,'optionTitle slds-listbox__option-text')]");
	public By Reason = By
			.xpath("//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select']");
	public By Reasonoptn = By.xpath("//option[@class='optionClass']");
	public By CancelbtnAdditional = By
			.xpath("//footer[@class='slds-modal__footer']//button[contains(text(),'Cancel')]");

	public void AddDistubuteItems_Vaidate() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, clickDisstrubutItems, 120);
		clickElementBy(clickDisstrubutItems);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(product).click();
		driver.findElement(product).sendKeys(testData.get("ProductAdditional"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(productSublist).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(Reason).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> optns = driver.findElements(Reasonoptn);
		int resonoptns = optns.size();
		for (int i = 0; i < resonoptns; i++) {
			String optnvalue = driver.findElement(Reasonoptn).getText();
			if (optnvalue.contains("Mystery")) {
				tcConfig.updateTestReporter("TourRecordsPage", "AddDistubuteItems_Vaidate", Status.FAIL,
						"Mystery Reason is shown");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "AddDistubuteItems_Vaidate", Status.PASS,
						"Mystery Reason is not shown");
			}

		}
		driver.findElement(CancelbtnAdditional).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public By salesstore = By.xpath("//a[text()='" + testData.get("SalesStore") + "'][1]");
	public By ParentTerritory = By.xpath("//span[text()='Parent Territory']//..//..//a");
	public By relatedtab = By.xpath("(//span[text()='Related'])[3]");
	public By viewallSalsStore = By.xpath(
			"//span[@title='Sales Store Premiums']/ancestor::article[@data-aura-class='forceRelatedListCardDesktop']//span[text()='View All']");
	public By QOHafter = By.xpath(
			"//a[text()='" + testData.get("Product") + "']//..//..//..//span[@class='slds-truncate uiOutputNumber']");
	public By productdist = By
			.xpath("//td[@class ='slds-cell-edit cellContainer']//a[@data-aura-class='forceOutputLookup']");

	public void QOAafterdistvalidate() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, salesstore, 120);
		driver.findElement(salesstore).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,100)", "");
		waitUntilObjectVisible(driver, ParentTerritory, 120);
		driver.findElement(ParentTerritory).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, relatedtab, 120);
		driver.findElement(relatedtab).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,2000)", "");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		jse.executeScript("window.scrollBy(0,2000)", "");
		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",
		// viewallSalsStore);
		waitUntilObjectVisible(driver, viewallSalsStore, 120);
		driver.findElement(viewallSalsStore).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.findElement(relatedtab).click();
		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
		jse2.executeScript("scroll(0,750);");
		List<WebElement> salestoreslist = driver.findElements(productdist);
		int listcount = salestoreslist.size();
		if (listcount > 0) {
			for (int i = 1; i <= listcount; i++) {
				String productname = driver.findElement(By.xpath(
						"(//td[@class ='slds-cell-edit cellContainer']//a[@data-aura-class='forceOutputLookup'])[" + i
								+ "]"))
						.getText();
				if (productname.equals(testData.get("Product"))) {
					QOAafterDist = Integer.parseInt(driver.findElement(QOHafter).getText());
					break;

				}

			}

		}

		if (QOHbeforeDist > QOAafterDist) {
			tcConfig.updateTestReporter("TourRecordsPage", "AddDistubuteItems_Vaidate", Status.PASS, "QOH Decreased");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "AddDistubuteItems_Vaidate", Status.PASS,
					"Error while validating QOH");

		}

	}

	private By drpDwnBrand = By.xpath("//label[contains(text(),'Brand')]/..//select");

	
	
	/**
	 * 
	 * Added by Ritam
	 * @return
	 */
	
	
	public List<String> bookTourAppointment() {
		List<String> appointmentFields = new ArrayList<String>();
		try {
			waitUntilObjectVisible(driver, bookOptionSelect, 120);
			if (verifyObjectDisplayed(bookOptionSelect)) {
				clickElementJSWithWait(bookOptionSelect);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> e = driver.findElements(linkAppointmentLoc);
				List<WebElement> e1 = driver.findElements(txtAllotmentType);
				List<WebElement> e2 = driver.findElements(drpDwnBrand);
				
				
				if (verifyObjectDisplayed(removeSalesStoreIcon)) {
					clickElementJSWithWait(removeSalesStoreIcon);
					tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
							"Removed Sales store");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
							"Removing sales store not required");

				}

				try {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, reSalesStoretxt, 120);
					fieldDataEnter(reSalesStoretxt, testData.get("SalesStore"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]"))
							.click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} catch (Exception e11) {
					e11.getMessage();
				}

				try {
					clickElementJSWithWait(removeAppointmentLocIcon);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					fieldDataEnter(reBookLocation, testData.get("AppointmentLoc"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"))
							.click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} catch (Exception e12) {
					e12.getMessage();
				}
				String brand = new Select(e2.get(e2.size() - 1)).getFirstSelectedOption().getText();
				if (brand.contains("None")) {
					new Select(e2.get(e2.size() - 1)).selectByIndex(1);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
				appointmentFields.add(e.get(e.size() - 1).getAttribute("title").trim());
				appointmentFields.add(new Select(e1.get(e1.size() - 1)).getFirstSelectedOption().getText().trim());
				new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				appointmentFields.add(new Select(driver.findElement(drpDwnMarketingStrtgy)).getFirstSelectedOption()
						.getText().trim());
				new Select(driver.findElement(drpDwnMarketingSrcType))
						.selectByVisibleText(testData.get("MarketingSourceType"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				appointmentFields.add(new Select(driver.findElement(drpDwnMarketingSrcType)).getFirstSelectedOption()
						.getText().trim());
				if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
					new Select(driver.findElement(drpmarketingsubsource))
							.selectByVisibleText(testData.get("MarketingSubSource"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					appointmentFields.add(
							new Select(driver.findElement(drpmarketingsubsource)).getFirstSelectedOption().getText());
				}
				if (verifyElementDisplayed(getAppointmentsBtn)) {
					mouseoverAndClick(getAppointmentsBtn);
					waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
					clickElementJS(lblAvailableSlot);
					waitUntilElementVisibleBy(driver, yesBtn, 120);
					clickElementJS(yesBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisibleBy(driver, txtAppointmentConfirm, 120);
					if (verifyObjectDisplayed(txtAppointmentConfirm)) {
						tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
								"New appointment has been created");
					} else {
						tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
								"User is unable to book an appointment");
					}
					waitUntilElementVisibleBy(driver, okBtn, 120);
					clickElementBy(okBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (driver.findElements(lnkNewSalesStore).size() == 0) {
						driver.navigate().refresh();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					/*waitUntilElementVisibleBy(driver, lnkNewSalesStore, 120);
					// List<WebElement> linkSalestore =
					// driver.findElements(By.xpath("//span[text()='Sales
					// Store']/../../div[2]//a[contains(text(),'"+testData.get("SalesStore")+"')]"));
					if (verifyObjectDisplayed(lnkNewSalesStore)) {
						tcConfig.updateTestReporter("TourRecordsPage", "verifyTourUpdated", LogStatus.PASS,
								"Salesstore is updated successfully for booked tour");
					} else {
						tcConfig.updateTestReporter("TourRecordsPage", "verifyTourUpdated", LogStatus.FAIL,
								"Salesstore is not updated successfully for booked tour");
					}*/
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
							"Get Appointment button is not visible");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"Book appointment tab is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL, "Getting error-" + e);
			throw e;
		}
		return appointmentFields;
	}
	
	
	

	public void clickBookAppointment() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleWb(driver,
					driver.findElements(bookOptionSelect).get(driver.findElements(bookOptionSelect).size() - 1), 120);
			// waitUntilElementVisibleWb(driver,
			// driver.findElement(bookOptionSelect), 120);
			if (verifyElementDisplayed(
					driver.findElements(bookOptionSelect).get(driver.findElements(bookOptionSelect).size() - 1))) {
				System.out.println("click button=");
				System.out.println(
						(driver.findElements(bookOptionSelect).get(driver.findElements(bookOptionSelect).size() - 1)));
				clickElementWb(
						driver.findElements(bookOptionSelect).get(driver.findElements(bookOptionSelect).size() - 1));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("TourRecordsPage", "clickBookAppointment", Status.PASS,
						"Book Appointment tab is clicked");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "clickBookAppointment", Status.FAIL,
						"Book Appointment tab is not present");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "clickBookAppointment", Status.FAIL, "Getting error-" + e);
		}
	}

	public void clickBookAppointment1() {
		try {
			waitUntilElementVisible(driver, driver.findElement(bookOptionSelect), 120);

			driver.findElement(bookOptionSelect).click();
			tcConfig.updateTestReporter("TourRecordsPage", "clickBookAppointment", Status.PASS,
					"Book Appointment tab is clicked");

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "clickBookAppointment", Status.FAIL, "Getting error-" + e);

		}
	}

	public void updateSalesStore() {
		try {
			waitUntilElementVisibleBy(driver, lnkSalesStore, 120);
			if (verifyObjectDisplayed(removeSalesStoreIcon)) {
				clickElementBy(removeSalesStoreIcon);
				tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.PASS,
						"SalesStore field is editable");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(txtBoxSalesStore)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// e.get(e.size()-1).sendKeys(testData.get("NewSalesStore"));
					fieldDataEnter(txtBoxSalesStore, testData.get("NewSalesStore"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(
							By.xpath("//span[contains(text(),'" + testData.get("NewSalesStore") + "')]/../../.."));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.PASS,
							"SalesStore textbox is visible and updated with new salesstore");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.FAIL,
							"SalesStore textbox is not visible");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.FAIL,
						"Salestore delete link is not present");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.FAIL, "Getting error-" + e);
			throw e;
		}
	}

	public void getAppointmentSlot() {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, getAppointmentsBtn, 120);
			if (verifyElementDisplayed(getAppointmentsBtn)) {
				mouseoverAndClick(getAppointmentsBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
				clickElementBy(lblAvailableSlot);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, yesBtn, 120);
				clickElementBy(yesBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, txtAppointmentConfirm, 120);
				if (verifyObjectDisplayed(txtAppointmentConfirm)) {
					tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.PASS,
							"New appointment has been created");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.FAIL,
							"User is unable to book an appointment");
				}
				waitUntilElementVisibleBy(driver, okBtn, 120);
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.FAIL,
						"Get Appointment button is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "updateTour", Status.FAIL, "Getting error :" + e);
			throw e;
		}
	}

	public void verifyTourUpdated() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleWb(driver,
					driver.findElements(tourdetailstab).get(driver.findElements(tourdetailstab).size() - 1), 120);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> linkSalestore = driver
					.findElements(By.xpath("//span[text()='Sales Store']/../../div[2]//a[contains(text(),'"
							+ testData.get("NewSalesStore") + "')]"));
			if (linkSalestore.size() == 0) {
				driver.navigate().refresh();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				linkSalestore = driver
						.findElements(By.xpath("//span[text()='Sales Store']/../../div[2]//a[contains(text(),'"
								+ testData.get("NewSalesStore") + "')]"));
				System.out.println(linkSalestore);
			}

			if (verifyElementDisplayed(linkSalestore.get(linkSalestore.size() - 1))) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyTourUpdated", Status.PASS,
						"Salesstore is updated successfully for booked tour");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyTourUpdated", Status.FAIL,
						"Salesstore is not updated successfully for booked tour");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourUpdated", Status.FAIL, "Getting error-" + e);
			throw e;
		}
	}

	public void verifyPrepopulatedMktgFields(List<String> mktgFields) {
		try {
			waitUntilElementVisibleBy(driver, lnkSalesStore, 120);
			List<String> lstStrMktgFields = new ArrayList<String>();
			List<WebElement> e = driver.findElements(linkAppointmentLoc);
			List<WebElement> e1 = driver.findElements(txtAllotmentType);
			lstStrMktgFields.add(e.get(e.size() - 1).getAttribute("title").trim());
			lstStrMktgFields.add(new Select(e1.get(e1.size() - 1)).getFirstSelectedOption().getText().trim());
			lstStrMktgFields.add(
					new Select(driver.findElement(drpDwnMarketingStrtgy)).getFirstSelectedOption().getText().trim());
			lstStrMktgFields.add(
					new Select(driver.findElement(drpDwnMarketingSrcType)).getFirstSelectedOption().getText().trim());
			String marketingSubSource = new Select(driver.findElement(drpmarketingsubsource)).getFirstSelectedOption()
					.getText().trim();
			if (!marketingSubSource.contains("None")) {
				lstStrMktgFields.add(marketingSubSource);
			}
			if (lstStrMktgFields.equals(mktgFields)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyPrepopulatedMktgFields", Status.PASS,
						"Marketing fields are prepopulating after updating the salesstore");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyPrepopulatedMktgFields", Status.FAIL,
						"Some marketing fields are not prepopulating after updating the salesstore");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyPrepopulatedMktgFields", Status.FAIL,
					"Getting error-" + e);
			throw e;
		}
	}

	public void verifyPrepopulatedMktgFieldsMarketer(List<String> mktgFields) {
		try {
			waitUntilElementVisibleBy(driver, btnNext, 120);
			List<String> lstStrMktgFields = new ArrayList<String>();
			WebElement bookingLocation = driver.findElement(By.xpath("//h2[text()='" + testData.get("NewSalesStore")
					+ "']/../../../div[2]//span[contains(text(),'Booking Location')]/../..//select"));
			WebElement tourType = driver.findElement(By.xpath("//h2[text()='" + testData.get("NewSalesStore")
					+ "']/../../../div[2]//label[contains(text(),'Tour Type')]/../..//select"));
			lstStrMktgFields.add(new Select(bookingLocation).getFirstSelectedOption().getText());
			lstStrMktgFields.add(new Select(tourType).getFirstSelectedOption().getText().trim());
			WebElement marktngStrategy = driver.findElement(By.xpath("//h2[text()='" + testData.get("NewSalesStore")
					+ "']/../../../div[2]//span[contains(text(),'Marketing Strategy')]/../..//select"));
			WebElement marketingSrctype = driver.findElement(By.xpath("//h2[text()='" + testData.get("NewSalesStore")
					+ "']/../../../div[2]//span[contains(text(),'Marketing Source Type')]/../..//select"));
			WebElement marketingSubSources = driver.findElement(By.xpath("//h2[text()='" + testData.get("NewSalesStore")
					+ "']/../../../div[2]//span[contains(text(),'Marketing Sub Source')]/../..//select"));
			lstStrMktgFields.add(new Select(marktngStrategy).getFirstSelectedOption().getText().trim());
			lstStrMktgFields.add(new Select(marketingSrctype).getFirstSelectedOption().getText().trim());
			String marketingSubSource = new Select(marketingSubSources).getFirstSelectedOption().getText().trim();
			if (!marketingSubSource.contains("None")) {
				lstStrMktgFields.add(marketingSubSource);
			}
			if (lstStrMktgFields.equals(mktgFields)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyPrepopulatedMktgFields", Status.PASS,
						"Marketing fields are prepopulating after updating the salesstore");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyPrepopulatedMktgFields", Status.FAIL,
						"Some marketing fields are not prepopulating after updating the salesstore");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyPrepopulatedMktgFields", Status.FAIL,
					"Getting error-" + e);
			throw e;
		}
	}

	private By CancleBookTourYesButton = By.xpath("//footer[@class='slds-modal__footer']/button[text()='YES']");

	public void cancle_Tour_InhouseMArkter() {
		driver.navigate().refresh();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().back();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("(//img[@class='wn-left_custom_icon'])[1]")).click();

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, CancleBookTourYesButton, 120);
		clickElementJSWithWait(CancleBookTourYesButton);

	}

	private By drpDwnmarktngCmpn = By.xpath("//h2[text()='" + testData.get("NewSalesStore")
			+ "']/../../../div[2]//span[contains(text(),'Marketing Campaign')]/../..//select");
	private By txtBoxNoOfPeople = By.xpath("//h2[text()='" + testData.get("NewSalesStore")
			+ "']/../../../div[2]//label[contains(text(),'of people')]/../..//input");

	public void verifyBlankFieldAndUpdateMobUI() {
		try {
			waitUntilElementVisibleBy(driver, drpDwnmarktngCmpn, 120);
			scrollDownForElementJSBy(drpDwnmarktngCmpn);
			// new
			// Select(driver.findElement(drpDwnmarktngCmpn)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(txtBoxNoOfPeople, "1");
			tcConfig.updateTestReporter("TourRecordsPage", "verifyBlankFieldAndUpdate", Status.PASS,
					"Marketing campaign field is updated");
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyBlankFieldAndUpdate", Status.FAIL,
					"Getting error-" + e);
			// throw e;
		}
	}

	public void verifyBlankFieldAndUpdate() {
		try {
			waitUntilElementVisibleBy(driver, drpDwnMarketingCampaign, 120);
			List<WebElement> e = driver.findElements(drpDwnMarketingCampaign);
			if (e.size() > 0) {
				scrollDownForElementJSWb(e.get(e.size() - 1));
				new Select(e.get(e.size() - 1)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("TourRecordsPage", "verifyBlankFieldAndUpdate", Status.PASS,
						"Marketing campaign field is updated");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyBlankFieldAndUpdate", Status.FAIL,
						"Marketing campaign field is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyBlankFieldAndUpdate", Status.FAIL,
					"Getting error-" + e);
			// throw e;
		}
	}

	public void navigateToSummaryTab() {
		try {
			waitUntilElementVisibleBy(driver, linkSummary, 120);
			if (verifyObjectDisplayed(linkSummary)) {
				clickElementBy(linkSummary);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("TourRecordsPage", "navigateToSummaryTab", Status.PASS,
						"Clicked on summary tab");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "navigateToSummaryTab", Status.FAIL,
						"Summary tab is not present");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "navigateToSummaryTab", Status.FAIL, "Getting error-" + e);
			throw e;
		}
	}

	public void verifySalesStoreUpdatedHistory() {
		try {
			waitUntilElementVisibleBy(driver, tblWorkOrder, 120);
			scrollDownForElementJSBy(tblWorkOrder);
			if (verifyElementDisplayed(driver.findElements(lstHistoryDetails).get(0))) {
				if (driver.findElements(lstHistoryDetails).get(0).getText().equalsIgnoreCase("Sales Store")) {
					if (driver.findElements(lstHistoryDetails).get(driver.findElements(lstHistoryDetails).size() - 1)
							.getText().equalsIgnoreCase(testData.get("NewSalesStore"))) {
						tcConfig.updateTestReporter("TourRecordsPage", "verifySalesStoreUpdatedHistory", Status.PASS,
								"Work order history is updated in the summary tab");
					} else {
						tcConfig.updateTestReporter("TourRecordsPage", "verifySalesStoreUpdatedHistory", Status.FAIL,
								"SalesStore" + testData.get("NewSalesStore") + " is not visible in work order history");
					}
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "verifySalesStoreUpdatedHistory", Status.FAIL,
							"Salesstore field is not visible");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "navigateToSummaryTab", Status.FAIL, "Getting error-" + e);
			throw e;
		}
	}

	public int addPromisedIncentive() {
		int intQuantityPurchased = 0;
		int intQuantityonHand = 0;
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, incentiveLink, 120);
			if (verifyObjectDisplayed(incentiveLink)) {
				clickElementJSWithWait(incentiveLink);
				waitUntilElementVisibleBy(driver, addPromisedItemBtn, 120);
				if (verifyObjectDisplayed(addPromisedItemBtn)) {
					clickElementJSWithWait(addPromisedItemBtn);
					waitUntilObjectVisible(driver, addPromissedPage, 120);
					if (verifyObjectDisplayed(addPromissedsearch)) {
						fieldDataEnter(addPromissedsearch, testData.get("Product"));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						waitUntilElementVisibleBy(driver, addPromPlus, 120);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						intQuantityonHand = Integer.parseInt(driver.findElement(quantityOnHand).getText().trim());
						// intQuantityPurchased =
						// Integer.parseInt(driver.findElement(quantityOnHand).getAttribute("value"));
						clickElementJSWithWait(addPromPlus);
						waitUntilElementVisibleBy(driver, addBtn, 120);
						clickElementJSWithWait(addBtn);

						waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
						if (verifyObjectDisplayed(txtSuccessMsg)) {
							tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.PASS,
									"Successfully added the promised incentive");
						} else {
							tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL,
									"Promised incentive is not added");
						}

					} else {
						tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL,
								"Promised search text box is not visible");
					}
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL,
							"Add promised button is not visible");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL,
						"Incentive link is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL, "Getting error:" + e);
		}
		return intQuantityonHand;
	}

	public void verifyNotAbleToBookAppointment() {
		try {
			waitUntilElementVisibleWb(driver, getAppointmentsBtn, 120);
			mouseoverAndClick(getAppointmentsBtn);
			waitUntilElementVisibleBy(driver, lblError, 120);
			if (verifyObjectDisplayed(lblError)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyNotAbleToBookAppointment", Status.PASS,
						"Error message is displayed while booking appointment: "
								+ driver.findElement(lblError).getText());
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyNotAbleToBookAppointment", Status.FAIL,
						"Error message is not displayed while booking appointment");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyNotAbleToBookAppointment", Status.FAIL,
					"Getting error:" + e);
		}
	}

	public void distributePromisedIncentive() {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, checkAll, 120);
			if (verifyObjectDisplayed(checkAll)) {
				clickElementJSWithWait(checkAll);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// List<WebElement> e =
				// driver.findElements(btnDistributeIncentive);
				// clickElementWb(e.get(0));
				driver.findElement(btnDistributeIncentive).click();
				waitUntilElementVisibleBy(driver, voucherInput, 120);
				fieldDataEnter(voucherInput, getRandomString(2));

				waitUntilElementVisibleBy(driver, addProdPopAdd, 120);
				clickElementJSWithWait(addProdPopAdd);

				tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.PASS,
						"Check  button is present");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.FAIL,
						"Check  button is not present");

			}
			waitUntilObjectVisible(driver, succesMsg, 120);
			String generatedMsg = driver.findElement(succesMsg).getText();
			System.out.println(generatedMsg);
			/*
			 * if(verifyObjectDisplayed(succesMsg)){ System.out.println(generatedMsg);
			 * tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive",
			 * Status.PASS, "Promised incentive is distributed"); }else{
			 * tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive",
			 * Status.FAIL, "Promised incecntive is not distributed"); }
			 */

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void verifySalesRepRotation() {
		int count = 0;
		List<String> newLstSalesRep = new ArrayList<String>();
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lstSalesRepName, 120);
			List<WebElement> ele = driver.findElements(lstSalesRepName);
			String x = ele.get(ele.size() - 1).getCssValue("font-weight");
			if (Integer.parseInt(x) >= 700) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.PASS,
						"Assigned reps is in bold");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.FAIL,
						"Assigned reps is not in bold");
			}
			for (int i = 0; i < ele.size(); i++) {
				newLstSalesRep.add((ele.get(i).getText().substring(0, ele.get(i).getText().length() - 3)).trim());
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (newLstSalesRep.size() == lstSalesRepOnRotation.size()) {
				if (lstSalesRepOnRotation.get(0).equals(newLstSalesRep.get(newLstSalesRep.size() - 1))) {
					List<WebElement> e = driver.findElements(By.xpath("//span[contains(text(),'"
							+ lstSalesRepOnRotation.get(0) + "')]/..//img[contains(@src,'onTourIcon')]"));
					if (e.size() > 0) {
						tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.PASS,
								"Selected sales rep is moving to bottom after assignment and on tour icon is displayed");
						strSalesRepName = lstSalesRepOnRotation.get(0);
						lstSalesRepOnRotation.remove(0);
						for (int i = 0; i < lstSalesRepOnRotation.size(); i++) {
							if (lstSalesRepOnRotation.get(i).equals(newLstSalesRep.get(i))) {
								count++;
							}
						}
						if (count == lstSalesRepOnRotation.size()) {
							tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.PASS,
									"Sales rep is still sorted after assignment");
						} else {
							tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.FAIL,
									"Sales rep is getting desorted after assignment");
						}
					} else {
						tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.FAIL,
								"On tour icon is not coming in assigned sales rep");
					}
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.FAIL,
							"Selected sales rep is not moving to bottom after assignment");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifySalesRepRotation", Status.FAIL,
						"SalesRep rotation count is not matching");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "getSalesRepRotationOrder", Status.FAIL,
					"Getting error: " + e);
			throw e;
		}
	}

	public void verifyOnTourIconSalesRotation() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnSaveOrder, 120);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> icon = driver
					.findElements(By.xpath("//td[text()='" + strSalesRepName + "']//img[contains(@src,'onTourIcon')]"));
			if (icon.size() > 0) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyOnTourIconSalesRotation", Status.PASS,
						"On tour icon is available for the assigned rep");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyOnTourIconSalesRotation", Status.FAIL,
						"On tour icon is not available for the assigned rep");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyOnTourIconSalesRotation", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void clickEditTour() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// driver.findElement(btnEdit).click();
		List<WebElement> e = driver.findElements(btnEdit);
		/*
		 * if(e.size()>0) {
		 */
		clickElementJS(e.get(e.size() - 1));

		// clickElementJSWithWait(btnEdit);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	private By drpDwnDispositionReason = By.xpath("//span[contains(text(),'Disposition Reason')]/../../div//a");
	private By fieldDispositionReason = By
			.xpath("//span[contains(text(),'Disposition Reason')]/../../div//a/../../../..");
	private By fieldDispositionStatus = By
			.xpath("//span[contains(text(),'Disposition Status')]/../../div//a/../../../..");
	private By txtBoxQARep = By.xpath("//span[text()='QA Rep']/../../div//input");

	public void tourDispositionTourPage(String DidTheyTour) {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollDownForElementJSBy(drpDownDidTheyTour);
			scrollUpByPixel(100);
			waitUntilElementVisibleBy(driver, drpDownDidTheyTour, 120);
			if (verifyObjectDisplayed(drpDownDidTheyTour)) {
				clickElementBy(drpDownDidTheyTour);
				waitUntilElementVisibleBy(driver,
						By.xpath("//a[@title='" + DidTheyTour + "'][@role='menuitemradio']/.."), 120);
				// waitUntilElementVisibleBy(driver, lnkYesTour, 120);
				// clickElementBy(lnkYesTour);
				clickElementBy(By.xpath("//a[@title='" + DidTheyTour + "']"));
				waitUntilElementVisibleBy(driver, drpDownDispositionStatus, 120);
				if (!driver.findElement(fieldDispositionReason).getAttribute("class").contains("disabled")) {
					clickElementBy(drpDwnDispositionReason);
					List<WebElement> e = driver.findElements(
							By.xpath("//a[@title='" + testData.get("DispositionStatus") + "'][@role='menuitemradio']"));
					if (e.size() > 0) {
						clickElementWb(e.get(e.size() - 1));
						waitUntilElementVisibleBy(driver, saveBtn, 120);
						clickElementBy(saveBtn);

					}
				} else if (!driver.findElement(fieldDispositionStatus).getAttribute("class").contains("disabled")) {
					clickElementBy(drpDownDispositionStatus);
					List<WebElement> e = driver
							.findElements(By.xpath("//a[@title='" + testData.get("DispositionStatus") + "']"));
					if (e.size() > 0) {
						clickElementWb(e.get(e.size() - 1));
						waitUntilElementVisibleBy(driver, lnkMarketingAgent, 120);
						String marketingAgent = driver.findElement(lnkMarketingAgent).getText().trim();
						if (verifyObjectDisplayed(txtBoxSalesAgent)) {
							fieldDataEnter(txtBoxSalesAgent, marketingAgent);
							List<WebElement> e1 = driver
									.findElements(By.xpath("//div[@title='" + marketingAgent + "']/../.."));
							if (e1.size() > 0) {
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								e1.get(e1.size() - 1).click();

								fieldDataEnter(txtBoxQARep, marketingAgent);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								List<WebElement> e2 = driver
										.findElements(By.xpath("//div[@title='" + marketingAgent + "']/../.."));
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								e2.get(e2.size() - 1).click();
								waitUntilElementVisibleBy(driver, saveBtn, 120);
								clickElementBy(saveBtn);

							} else {
								tcConfig.updateTestReporter("TourRecordsPage", "tourDispositionTourPage", Status.FAIL,
										"Sales Agent is not available");
							}
						} else {
							tcConfig.updateTestReporter("TourRecordsPage", "tourDispositionTourPage", Status.FAIL,
									"Sales Agent textbox is not available");
						}
					} else {
						tcConfig.updateTestReporter("TourRecordsPage", "tourDispositionTourPage", Status.FAIL,
								"Disposition status value is not visible");
					}
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "tourDispositionTourPage", Status.FAIL,
							"Disposition status dropdown is not visible");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "tourDispositionTourPage", Status.FAIL,
						"Did they tour dropdown is not visible");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "tourDispositionTourPage", Status.FAIL,
					"Getting error:" + e);
			throw e;
		}
	}

	public void navigateToDiscoveryTab() {
		try {
			waitUntilElementVisibleBy(driver, lnkDiscoveryTour, 120);
			if (verifyObjectDisplayed(lnkDiscoveryTour)) {
				clickElementJSWithWait(lnkDiscoveryTour);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("TourRecordsPage", "navigateToDiscoveryTab", Status.PASS,
						"Navigated to discovery tab");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "tourDispositionTourPage", Status.FAIL,
					"Getting error:" + e);
			throw e;
		}
	}

	public void addChildTour() {
		try {
			waitUntilElementVisibleBy(driver, btnAddChildTour, 120);
			if (verifyObjectDisplayed(countChildTour)) {
				String count = driver.findElement(countChildTour).getText();
				if (count.contains("0")) {
					tcConfig.updateTestReporter("TourRecordsPage", "addChildTour", Status.PASS,
							"There are no child tours present");
				}
				clickElementJSWithWait(btnAddChildTour);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, lnkChildTour, 120);
				if (verifyObjectDisplayed(lnkChildTour)) {
					tcConfig.updateTestReporter("TourRecordsPage", "addChildTour", Status.PASS,
							"Child tour is successfully added");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "addChildTour", Status.FAIL,
							"Failed to add child tour");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "addChildTour", Status.FAIL,
						"Already child tour is present");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "addChildTour", Status.FAIL, "Getting error:" + e);
			throw e;
		}

	}

	public void openAndValidateChildTour() {
		try {
			scrollDownForElementJSBy(lnkChildTour);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkChildTour, 120);
			clickElementBy(lnkChildTour);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> e = driver.findElements(lnkSalesStoreChildTour);
			if (e.size() > 0) {
				if (e.get(e.size() - 1).getText().equals(testData.get("SalesStore"))) {
					tcConfig.updateTestReporter("TourRecordsPage", "openAndValidateChildTour", Status.PASS,
							"Child tour details verified");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "openAndValidateChildTour", Status.FAIL,
							"Child tour details not verified");
				}
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "openAndValidateChildTour", Status.FAIL,
						"Child Salesstore is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "openAndValidateChildTour", Status.FAIL,
					"Getting error:" + e);
			throw e;
		}
	}

	public void verifyAddingSecondChildTour() {
		try {
			waitUntilElementVisibleBy(driver, btnAddChildTour, 120);
			clickElementBy(btnAddChildTour);
			waitUntilElementVisibleBy(driver, divErrorMsg, 120);
			if (verifyObjectDisplayed(divErrorMsg)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyAddingSecondChildTour", Status.PASS,
						"Not able to create a second child tour");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyAddingSecondChildTour", Status.FAIL,
						"Able to create a second child tour");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyAddingSecondChildTour", Status.FAIL,
					"Getting error:" + e);
			throw e;
		}
	}

	public void createNewNote() {
		try {
			waitUntilElementVisibleBy(driver, lnkNew, 120);
			scrollDownForElementJSBy(lnkNew);
			scrollUpByPixel(50);
			if (verifyObjectDisplayed(lnkNew)) {
				clickElementJS(lnkNew);
				waitUntilElementVisibleBy(driver, textAreaTitle, 120);
				if (verifyObjectDisplayed(textAreaTitle)) {
					strNotesTitle = generateRandomString(4, CHARACTER_STRING);
					fieldDataEnter(textAreaTitle, strNotesTitle);
					waitUntilElementVisibleBy(driver, saveBtn, 120);
					clickElementBy(saveBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, lnkNewNote, 120);
					strNotesName = driver.findElement(lnkNewNote).getText();
					tcConfig.updateTestReporter("TourRecordsPage", "createNewNote", Status.PASS,
							"New note created for the tour");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "createNewNote", Status.FAIL,
							"New note creation failed");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "createNewNote", Status.FAIL,
						"New link is not visible for note creation");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyAddingSecondChildTour", Status.FAIL,
					"Getting error:" + e);
			throw e;
		}
	}

	public void verifyRecentNotesForAllTab() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, quickSearchEdit, 120);
			new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
			waitUntilElementVisibleBy(driver, quickSearchEdit, 120);
			fieldDataEnter(quickSearchEdit, LeadsPage.createdTourID);
			driver.switchTo().activeElement().sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(
					By.xpath("//div[@title='Tour Record Number']/a[text()='" + LeadsPage.createdTourID + "']"))) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.PASS,
						"Tour has been displayed in Sales Rep Assignment Page");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.FAIL,
						"Tour has NOT been displayed in Sales Rep Assignment Page");
			}
			verifyRecentNotes();
			waitUntilElementVisibleBy(driver, guestSelectionChkBx, 120);
			clickElementBy(guestSelectionChkBx);
			clickElementBy(checkInBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> popup = driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
			if (popup.size() > 0) {
				popup.get(0).click();
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(checkedInLink);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(selectGuestCheckedIn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(assignRepsBtn);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, onTourLink, 120);
			clickElementBy(onTourLink);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(completeTourBtn)) {
				tcConfig.updateTestReporter("TourRecordsPage", "Assign Rep", Status.PASS, "Tour has been Assigned");
			}
			verifyRecentNotes();
			waitUntilElementVisibleBy(driver, completeTourBtn, 120);
			clickElementBy(completeTourBtn);
			waitUntilElementVisibleBy(driver, saveDispositionBtn, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollDownForElementJSBy(selectDidTourdrpDwn);
			scrollUpByPixel(50);
			new Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText("Yes");
			new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(testData.get("DispositionStatus"));
			clickElementBy(saveDispositionBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lnkDispositioned, 120);
			clickElementBy(lnkDispositioned);
			verifyRecentNotes();
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.FAIL, "Getting error:" + e);
			throw e;
		}
	}

	public void verifyRecentNotes() {
		try {

			if (verifyObjectDisplayed(lnkNotes)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.PASS,
						"Notes link is present in the booked tab");
				Actions a = new Actions(driver);
				a.moveToElement(driver.findElement(lnkNotes)).perform();
				List<WebElement> e = driver.findElements(divHoverNotes);
				if (e.size() > 0) {
					if (e.get(0).getText().equals(strNotesTitle)) {
						tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.PASS,
								"Notes title is visible after hovering on the recent note link");
						String window = driver.getWindowHandle();
						clickElementBy(lnkNotes);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
						driver.switchTo().window(tabs.get(tabs.size() - 1));
						waitUntilElementVisibleBy(driver, headerNotesPage, 120);
						if (driver.findElement(lnkNotesName).getText().equals(strNotesName)) {
							tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.PASS,
									"User is getting navigated to Notes page after clicking the Recent notes link");
						} else {
							tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.FAIL,
									"User is not getting navigated to Notes page after clicking the Recent notes link");
						}
						driver.close();
						driver.switchTo().window(window);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else {
						tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.FAIL,
								"Notes title is not same");
					}
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.FAIL,
							"Recent note hovering is not working");
				}
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.FAIL,
						"Recent Notes not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyRecentNotes", Status.FAIL, "Getting error:" + e);
			throw e;
		}
	}

	
	
	/**
     * done by c24125(Sabyasachi) validating tour booking not possible without
     * sales store
     */
    public void validatebookAppointmentNoSales() {
        waitForSometime(tcConfig.getConfig().get("MedWait"));
        waitUntilObjectVisible(driver, bookOptionSelect, 120);
        clickElementJS(bookOptionSelect);
        waitForSometime(tcConfig.getConfig().get("MedWait"));

 

        if (testData.get("NoSalesStore").equalsIgnoreCase("N")) {
            waitForSometime(tcConfig.getConfig().get("MedWait"));
            if (verifyObjectDisplayed(removeSalesStoreIcon)){
                clickElementBy(removeSalesStoreIcon);
            } else {
                tcConfig.updateTestReporter("TourRecordsPage", "SalesStoreBlank", Status.PASS,
                        " Sales store is blank");
            }
                
        } else {
            tcConfig.updateTestReporter("TourRecordsPage", "SalesStoreBlank", Status.FAIL,
                    "Sales store is not blank");

 

        }
        
        try {
            if (getAppointmentsBtn.isEnabled() == false || getAppointmentsBtn.getAttribute("disabled") == "true") {
                tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
                        "User is unable to book appointment if Sales store is blank");
            } else {
                tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
                        "User is able to book appointment if Sales store is blank");
            }
        } catch (NoSuchElementException e) {
            System.out.println("catched");
            tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
                    "User is able to book appointment if Sales store is blank");

 

        }
    }
	
	
	

	public void dispositionTour_Reservation() {
		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, 120);
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		newTourId = LeadsPage.createdTourID;
		fieldDataEnter(quickSearchEdit, newTourId);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + newTourId + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in Sales Rep Assignment Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Tour has NOT been displayed in Sales Rep Assignment Page");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(guestSelectionChkBx);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(checkInBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> popup = driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
		if (popup.size() > 0) {
			popup.get(0).click();
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(checkedInLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(selectGuestCheckedIn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(assignRepsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(onTourLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(completeTourBtn);
		waitUntilElementVisibleBy(driver, saveDispositionBtn, 120);
		fieldDataEnter(qaRepEdit, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//span[text()='" + testData.get("AgentName") + "']")).click();
		new Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText("Yes");
		new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(testData.get("DispositionStatus"));
		clickElementBy(saveDispositionBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void verifyIncentivesAdded() {
		waitUntilElementVisibleWb(driver,
				driver.findElements(incentiveLink).get(driver.findElements(incentiveLink).size() - 1), 120);
		List<WebElement> e1 = driver.findElements(incentiveLink);
		clickElementWb(e1.get(e1.size() - 1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> e2 = driver.findElements(addPromisedItemBtn);
		waitUntilElementVisibleWb(driver, e2.get(e2.size() - 1), 120);
		List<WebElement> e = driver.findElements(By.xpath("//span[@title='" + testData.get("Product") + "']"));
		if (e.size() >= 0) {
			tcConfig.updateTestReporter("TourPage", "verifyIncentivesAdded", Status.PASS,
					"Incentive added is not visible for updated salestore");
		} else {
			tcConfig.updateTestReporter("TourPage", "verifyIncentivesAdded", Status.FAIL,
					"Incentive added is visible for updated salestore");
		}
	}

	public void moveSalesRepFromNotRotationToRotation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, 120);
		List<WebElement> lstStrSalesRepName = driver.findElements(salesRepName);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> lstbtnAddSalesRep = driver.findElements(btnAddSalesRep);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String salesRepName = lstStrSalesRepName.get(0).getText().trim();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementWb(lstbtnAddSalesRep.get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> lstStrOnRotateSalesRep = driver.findElements(salesRepOnRotation);
		if (lstStrOnRotateSalesRep.get(lstStrOnRotateSalesRep.size() - 1).getText().trim().equals(salesRepName)) {
			tcConfig.updateTestReporter("TourRecordsPage", "moveSalesRepFromNotRotationToRotation", Status.PASS,
					"Sales rep is successfully added in On Rotation list");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "moveSalesRepFromNotRotationToRotation", Status.FAIL,
					"Sales rep is not added in On Rotation list");
		}
	}

	public void verifyRotationOfSalesRep() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, 120);
		List<WebElement> lstStrSalesRepName = driver.findElements(salesRepOnRotation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> lstStrSalesRepRank = driver.findElements(salesRepRank);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// String strSalesRepName = lstStrSalesRepName.get(0).getText();
		String strSalesRepRank = lstStrSalesRepRank.get(0).getText();
		String strSecSalesRepName = lstStrSalesRepName.get(1).getText();
		Actions a = new Actions(driver);
		a.doubleClick(lstStrSalesRepName.get(0)).build().perform();
		// clickElementJS(lstStrSalesRepName.get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnDown, 120);
		a.moveToElement(driver.findElement(btnDown)).click().perform();
		// clickElementJS(btnDown);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> lstStrSalesRepNames = driver.findElements(salesRepOnRotation);
		if (lstStrSalesRepNames.get(0).getText().equals(strSecSalesRepName)) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyRotationOfSalesRep", Status.PASS,
					"Sales Rep Name is swapped");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyRotationOfSalesRep", Status.FAIL,
					"Not able to swap SalesRep");
		}

		waitUntilElementVisibleBy(driver, btnSaveOrder, 120);

		clickElementJSWithWait(btnSaveOrder);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> lstFirstStrSalesRepName = driver.findElements(salesRepOnRotation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> lstFirstStrSalesRepRank = driver.findElements(salesRepRank);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (lstFirstStrSalesRepName.get(0).getText().equals(strSecSalesRepName)
				&& lstFirstStrSalesRepRank.get(0).getText().equals(strSalesRepRank)) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyRotationOfSalesRep", Status.PASS,
					"Sales Rep rank is changing according to the position");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyRotationOfSalesRep", Status.FAIL,
					"Sales Rep rank is not changing according to the position");
		}

	}

	public void addMultiplePromisedIncentiveAndDistibute() {
		waitUntilElementVisibleBy(driver, incentiveLink, 120);
		clickElementBy(incentiveLink);
		waitUntilElementVisibleBy(driver, addPromisedItemBtn, 120);
		clickElementBy(addPromisedItemBtn);
		waitUntilObjectVisible(driver, addPromissedPage, 120);
		String[] productNames = testData.get("Product").split("/");
		for (int i = 0; i < 2; i++) {
			fieldDataEnter(addPromissedsearch, productNames[i]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, addPromPlus, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(addPromPlus);
		}

		if (verifyObjectDisplayed(addBtn)) {
			waitUntilElementVisibleBy(driver, addBtn, 120);
			clickElementJSWithWait(addBtn);
			// driver.findElement(addBtn).click();
			// waitForSometime(tcConfig.getConfig().get("Wait"));
		}
		if (verifyObjectDisplayed(Additemsvalue)) {
			waitUntilElementVisibleBy(driver, Additemsvalue, 120);
			clickElementBy(Additemsvalue);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Additems).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
		if (verifyObjectDisplayed(txtSuccessMsg)) {
			tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.PASS,
					"Successfully added the promised incentive");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL,
					"Promised incentive is not added");
		}

		waitUntilElementVisibleBy(driver, checkAll, 120);

		clickElementBy(checkAll);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> e = driver.findElements(btnDistributeIncentive);
		scrollDownForElementJSWb(e.get(0));
		scrollUpByPixel(20);
		clickElementWb(e.get(0));
		waitUntilElementVisibleBy(driver, voucherInput, 120);
		fieldDataEnter(voucherInput, getRandomString(2));
		fieldDataEnter(lstVoucherInput, getRandomString(2));
		waitUntilElementVisibleBy(driver, addProdPopAdd, 120);
		clickElementJSWithWait(addProdPopAdd);
		waitUntilObjectVisible(driver, succesMsg, 120);

		if (verifyObjectDisplayed(succesMsg)) {
			tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.PASS,
					"Promised incentive is distributed");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.FAIL,
					"Promised incecntive is not distributed");
		}

		for (int i = 2; i < 4; i++) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnDistributeAddItems, 120);
			clickElementBy(btnDistributeAddItems);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, inputProductSearchBox, 120);
			fieldDataEnter(inputProductSearchBox, productNames[i]);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> e1 = driver.findElements(By.xpath("//span[@title='" + productNames[i] + "']"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (e1.size() > 0) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				e1.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			new Select(driver.findElement(drpDwnReason)).selectByIndex(2);

			List<WebElement> e3 = driver.findElements(inputVoucherValue);
			e3.get(0).sendKeys(generateRandomString(1, CHARACTER_STRING));
			e3.get(1).sendKeys("1000");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Additems);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

	public void verifyCancelTour() {
		waitUntilElementVisibleBy(driver, lnkCancelTour, 120);
		clickElementJSWithWait(lnkCancelTour);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnMarkAsComplete, 120);
		clickElementJSWithWait(btnMarkAsComplete);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnYes);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldCancel)) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyCancelTour", Status.PASS,
					"Tour is cancelled successfully");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.FAIL,
					"Not able to cancel the tour");
		}
	}

	public void clickTransportationTab() {
		waitUntilElementVisibleBy(driver, lnkTransportation, 120);
		clickElementJSWithWait(lnkTransportation);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, lnkNewTransportation, 120);
	}

	public void createAndVerifyTransportation() {
		waitUntilElementVisibleBy(driver, lnkNewTransportation, 120);
		clickElementJSWithWait(lnkNewTransportation);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtBoxNoOfAdults, 120);
		fieldDataEnter(txtBoxNoOfAdults, generateRandomString(1, INTEGER_STRING));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, pickUpTime, 120);
		driver.findElement(pickUpTime).sendKeys(getFutureDate(1, "MM/dd/yyyy"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, dropoffTime, 120);
		driver.findElement(dropoffTime).sendKeys(getFutureDate(2, "MM/dd/yyyy"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, textAreaPickLocation, 120);
		fieldDataEnter(textAreaPickLocation, generateRandomString(5, CHARACTER_STRING));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, textAreaTransportationVendor, 120);
		fieldDataEnter(textAreaTransportationVendor, generateRandomString(5, CHARACTER_STRING));
		driver.findElement(textAreaPickLocation).sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, saveBtn, 120);
		clickElementJSWithWait(saveBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(lnkTransportationId)) {
			tcConfig.updateTestReporter("TourRecordsPage", "clickTransportationTab", Status.PASS,
					"Transportation Id created is: " + driver.findElement(lnkTransportationId).getText());
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "clickTransportationTab", Status.FAIL,
					"Transportation Id is not visible");
		}
	}

	public void verifyZeroQOHErrorCMA() {
		waitUntilElementVisibleBy(driver, incentiveLink, 120);
		clickElementBy(incentiveLink);
		waitUntilElementVisibleBy(driver, addPromisedItemBtn, 120);
		clickElementBy(addPromisedItemBtn);
		waitUntilObjectVisible(driver, addPromissedPage, 120);
		fieldDataEnter(addPromissedsearch, testData.get("Product"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		/*
		 * if(driver.findElements(addPromPlus).size()==0) {
		 * 
		 * tcConfig.updateTestReporter("TourRecordsPage", "verifyZeroQOHErrorCMA",
		 * Status.PASS, "Incentive is not visible for QOH = 0"); } else {
		 * tcConfig.updateTestReporter("TourRecordsPage", "verifyZeroQOHErrorCMA",
		 * Status.FAIL, "Incentive is visible for QOH = 0"); }
		 */
	}

	public void validatePrintDisableforPCUDScore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, batchPrintBtn, 120);
		new Select(driver.findElement(salesStoreDropDown)).selectByVisibleText(testData.get("SalesStore"));

		clickElementBy(batchPrintBtn);

		newTourId = LeadsPage.createdTourID;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement elem = driver.findElements(printButton).get(0);
		System.out.println(elem.isDisplayed());
		waitUntilElementVisibleWb(driver, elem, 120);

		WebElement tourScore = driver.findElement(By.xpath(
				"//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='" + newTourId + "']/../../td[9]/div"));
		WebElement selectTourcheckbox = driver
				.findElement(By.xpath("//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='" + newTourId
						+ "']/../../td[1]//input"));
		waitUntilElementVisibleWb(driver, tourScore, 120);
		String score = tourScore.getText().trim();
		log.info(score);
		waitUntilElementVisibleWb(driver, selectTourcheckbox, 120);
		if (selectTourcheckbox.isEnabled()) {
			tcConfig.updateTestReporter("TourRecordsPage", "validatePrintDisableforPCUDScore", Status.FAIL,
					"Checkbox agains tour created is Enabled for PC and UD");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validatePrintDisableforPCUDScore", Status.PASS,
					"Checkbox agains tour created is not Enabled for PC and UD");
		}

	}

	/**
	 * @author c24125
	 */
	public void validateMultiple() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, batchPrintBtn, 120);

		if (verifyObjectDisplayed(batchPrintBtn)) {
			clickElementJSWithWait(batchPrintBtn);
			tcConfig.updateTestReporter("TourRecordsPage", "validate Batch Print Button", Status.PASS, "successful");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validate Batch Print Button", Status.FAIL, "unsuccessful");
		}
		newTourId = LeadsPage.createdTourID;
		newTourId2 = LeadsPage.createdTourID1;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement elem = driver.findElements(printButton).get(0);
		System.out.println(elem.isDisplayed());
		waitUntilElementVisibleWb(driver, elem, 120);
		WebElement band1 = driver.findElement(By.xpath(
				"//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='" + newTourId + "']/../../td[9]/div"));
		WebElement band2 = driver
				.findElement(By.xpath("//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='" + newTourId2
						+ "']/../../td[9]/div"));
		WebElement chbox1 = driver
				.findElement(By.xpath("//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='" + newTourId
						+ "']/../../td[1]//input"));
		WebElement chbox2 = driver
				.findElement(By.xpath("//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='" + newTourId2
						+ "']/../../td[1]//input"));

		/*
		 * WebElement band1 = driver.findElement(By.xpath(
		 * "//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='57291233']/../../td[9]/div"
		 * )); WebElement band2 = driver.findElement(By.xpath(
		 * "//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='57291234']/../../td[9]/div"
		 * )); WebElement chbox1 = driver.findElement(By.xpath(
		 * "//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='57291233']/../../td[1]//input"
		 * )); WebElement chbox2 = driver.findElement(By.xpath(
		 * "//table[contains(@class,'cTMBatchPrint')]//tr//td[5]//a[text()='57291234']/../../td[1]//input"
		 * ));
		 */
		/*
		 * WebElement ele=driver.findElement(By.xpath("(//a[text()='"+newTourId+
		 * "'])[2]/parent::td/following-sibling::td[4]/div")); WebElement
		 * ele1=driver.findElement(By.xpath("(//a[text()='"+newTourId+
		 * "'])[2]/parent::td/preceding-sibling::td[4]/div/div/label/input"));
		 */
		waitUntilElementVisibleWb(driver, band1, 120);
		String score1 = band1.getText().trim();
		String score2 = band2.getText().trim();
		log.info("scores are" + score1 + " " + score2);
		// waitUntilElementVisible(driver, ele1, 120);
		waitUntilElementVisibleWb(driver, chbox1, 120);
		if (chbox1.isEnabled() && chbox2.isEnabled()) {

			clickElementJS(chbox1);
			clickElementJS(chbox2);
			/*
			 * Actions act=new Actions(driver); act.moveToElement(chbox1).click()
			 * .moveToElement(chbox2).click().build().perform();
			 */
			/*
			 * clickElementWb(chbox1); clickElementWb(chbox2);
			 */
			tcConfig.updateTestReporter("TourRecordsPage", "validate Multiple tour record checkBox Activated",
					Status.PASS, "successful");
		}

		waitUntilElementVisibleWb(driver, elem, 120);

		String parentWindow = driver.getWindowHandle();
		System.out.println("Parent window:" + parentWindow);
		clickElementJSWithWait(elem);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Set<String> s1 = driver.getWindowHandles();
		System.out.println(s1.size());
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			String childwindow = i1.next();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			System.out.println(childwindow);
			if (!parentWindow.equals(childwindow)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(childwindow);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, tmcletter, 120);
				if (driver.findElement(tmcletter).isDisplayed()) {
					System.out.println("inside child window now");
					tcConfig.updateTestReporter("LeadsPage",
							"validate Print  Button enabled for Multiple Tour record of Q  band lead ", Status.PASS,
							"successfull");
					break;
				} else {
					tcConfig.updateTestReporter("LeadsPage",
							"validate Print  Button enabled for Multiple Tour record of Q  band lead ", Status.FAIL,
							"Unsuccessfull");
				}
			} else {
				System.out.println("This is not proper way of switching");
			}
		}

		driver.switchTo().window(parentWindow);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	private By txtBoxIncentiveQuantity = By.xpath("//tr[@class='slds-hint-parent']/td[5]//input");
	private By txtBoxItemsValue = By.xpath("//td[@data-label='Voucher Number']//input");
	private By txtBoxVoucherInput = By.xpath("//td[@data-label='Voucher Number']//input");
	private By fieldQOH = By.xpath("//tr[@class='slds-hint-parent']/th[2]/div");

	public void addMultipleValuesIncentiveAndDistribute() {

		waitUntilElementVisibleBy(driver, incentiveLink, 120);
		clickElementBy(incentiveLink);
		waitUntilElementVisibleBy(driver, addPromisedItemBtn, 120);
		clickElementBy(addPromisedItemBtn);
		waitUntilObjectVisible(driver, addPromissedPage, 120);
		String productName = testData.get("Product");
		fieldDataEnter(addPromissedsearch, productName);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, addPromPlus, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		lstQohValues.add(driver.findElement(fieldQOH).getText());
		// JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.findElement(txtBoxIncentiveQuantity).click();
		driver.findElement(txtBoxIncentiveQuantity).clear();
		driver.findElement(txtBoxIncentiveQuantity).sendKeys("2");
		// js.executeScript("return arguments[0].value",
		// driver.findElement(txtBoxIncentiveQuantity));
		// fieldDataEnter(txtBoxIncentiveQuantity, testData.get("Quantity"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(addPromPlus);

		waitUntilElementVisibleBy(driver, addBtn, 120);
		clickElementBy(addBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(checkAll)) {

			waitUntilElementVisibleBy(driver, checkAll, 120);

			clickElementBy(checkAll);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> e = driver.findElements(btnDistributeIncentive);
			scrollDownForElementJSWb(e.get(0));
			scrollUpByPixel(80);
			clickElementWb(e.get(0));
			waitUntilElementVisibleBy(driver, txtBoxVoucherInput, 120);
			driver.findElements(txtBoxVoucherInput).get(0).sendKeys(getRandomString(2));
			driver.findElements(txtBoxVoucherInput).get(1).sendKeys(getRandomString(2));
			for (int i = 1; i < driver.findElements(txtBoxVoucherInput).size(); i++) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (i % 2 == 0) {
					driver.findElements(txtBoxVoucherInput).get(i).sendKeys(getRandomString(2));
					driver.findElements(txtBoxVoucherInput).get(i).sendKeys(getRandomString(2));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			}
			waitUntilElementVisibleBy(driver, addProdPopAdd, 120);
			clickElementJSWithWait(addProdPopAdd);
			waitUntilObjectVisible(driver, succesMsg, 120);
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "checkAll", Status.FAIL, "checkBox are not click");

		}
		if (verifyObjectDisplayed(succesMsg)) {
			tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.PASS,
					"Promised incentive is distributed");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "distributePromisedIncentive", Status.FAIL,
					"Promised incecntive is not distributed");
		}

	}

	private By btnGuestSignature = By.xpath("//button[contains(text(),'Guest Signature')]");
	// private By txtGiftTitle = By.xpath("//h3[@class='text-center
	// gift-title']");
	// private By txtGiftTitle=By.xpath("//div/h3[contains(text(),'JIM STAFFORD
	// THEATRE – ADULT')]");
	private By txtGiftTitle = By.xpath("//div/h3[contains(text(),'JIM STAFFORD THEATRE – ADULT')]");
	private By fieldQuantity = By.xpath("//span[@class='quantity']");

	public void clickGuestSignature() {
		waitUntilElementVisibleBy(driver, btnGuestSignature, 120);
		String parentElement = driver.getWindowHandle();

		if (verifyObjectDisplayed(btnGuestSignature)) {
			tcConfig.updateTestReporter("TourRecordPage", "clickGuestSignature", Status.PASS,
					"Guest Signature button is present");
		} else {
			tcConfig.updateTestReporter("TourRecordPage", "clickGuestSignature", Status.FAIL, "Failed");
		}
		clickElementJSWithWait(btnGuestSignature);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windowHandles.get(1));
		/*
		 * Set<String> allwindow=driver.getWindowHandles();
		 * allwindow.removeAll(allwindow);
		 * 
		 * for(String childwindow:allwindow) { System.out.println(childwindow);
		 * driver.switchTo().window(childwindow); }
		 */

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void verifyQuantityInSignaturePage() {
		String giftName = "JIM STAFFORD THEATRE – ADULT";
		waitUntilElementVisibleBy(driver, txtGiftTitle, 120);
		// if(driver.findElement(txtGiftTitle).getText().contains(Integer.toString(totalAmount)))
		if (driver.findElement(txtGiftTitle).getText().equalsIgnoreCase(giftName)) {
			System.out.println(driver.findElement(txtGiftTitle).getText());
			if (driver.findElement(fieldQuantity).getText().equalsIgnoreCase("2")) {
				System.out.println(driver.findElement(fieldQuantity).getText());
				tcConfig.updateTestReporter("TourRecordPage", "verifyQuantityInSignaturePage", Status.PASS,
						"Amount in signature page is reflecting as " + Integer.toString(totalAmount)
								+ " and quantity is 2");
			} else {
				tcConfig.updateTestReporter("TourRecordPage", "verifyQuantityInSignaturePage", Status.FAIL,
						"Quantity is not showing as 1");
			}
		} else {
			tcConfig.updateTestReporter("TourRecordPage", "verifyQuantityInSignaturePage", Status.FAIL,
					"Amount in signature page is reflecting " + Integer.toString(totalAmount));
		}

		driver.close();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		ArrayList<String> handles = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(handles.get(0));
	}

	private By cancePrms = By.xpath("//footer[@class='slds-modal__footer']/button[contains(.,'Cancel')]");

	public void validateQOHDectreaseAfterDistribute(int beforequantity) {
		int Quantity = 0;
		// int decreasebyone=beforequantity-1;
		waitUntilElementVisibleBy(driver, addPromisedItemBtn, 120);
		if (verifyObjectDisplayed(addPromisedItemBtn)) {
			clickElementBy(addPromisedItemBtn);
			waitUntilObjectVisible(driver, addPromissedPage, 120);
			if (verifyObjectDisplayed(addPromissedsearch)) {
				fieldDataEnter(addPromissedsearch, testData.get("Product"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, addPromPlus, 120);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Quantity = Integer.parseInt(driver.findElement(quantityOnHand).getText().trim());
				System.out.println(Quantity);
				if (Quantity < beforequantity) {
					System.out.println(Quantity);
					tcConfig.updateTestReporter("TourRecordPage", "verifyquantity Decreased after After distribute",
							Status.PASS, "Amount in QOH is reflecting " + Integer.toString(Quantity));
				}

				else {
					tcConfig.updateTestReporter("TourRecordPage", "verifyquantity Decreased after After distribute",
							Status.FAIL, "Amount in QOH is reflecting " + Integer.toString(Quantity));
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, cancePrms, 120);
				// driver.findElement(cancePrms).click();
				clickElementJS(cancePrms);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("TourRecordPage", "verifyquantity Decreased after After distribute",
						Status.FAIL, "Amount in QOH is reflecting " + Integer.toString(Quantity));
			}

		} else {
			tcConfig.updateTestReporter("TourRecordPage", "verifyquantity Decreased after After distribute",
					Status.FAIL, "Amount in QOH is reflecting " + Integer.toString(Quantity));
		}
	}

	private By btnReturn1 = By.xpath("(//button[contains(.,'Return')])[1]");
	private By btnReturn2 = By.xpath("(//button[contains(.,'Return')])[2]");
	// (//button[contains(.,'Return')])[2]
	// private By drpTop = By.xpath("(//select[contains(@class,'slds-select
	// select uiInput')])[1]");
	private By chBox1 = By.xpath("//tr[contains(@class,'cTMConsumedAssignmentItem')]/td[1]/div/input");

	public void validateQOHIncreasedAfterReturn(int QOH) {
		String expected = Integer.toString(QOH);
		String actual = "";
		System.out.println(expected);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(chBox1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, chBox1, 120);
		clickElementJS(chBox1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnReturn1, 120);
		clickElementJSWithWait(btnReturn1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpTop, 120);

		new Select(driver.findElement(drpTop)).selectByIndex(2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, btnReturn2, 120);
		clickElementJSWithWait(btnReturn2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(addPromisedItemBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, addPromisedItemBtn, 120);
		if (verifyObjectDisplayed(addPromisedItemBtn)) {
			clickElementJSWithWait(addPromisedItemBtn);
			waitUntilObjectVisible(driver, addPromissedPage, 120);
			if (verifyObjectDisplayed(addPromissedsearch)) {
				fieldDataEnter(addPromissedsearch, testData.get("Product"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, addPromPlus, 120);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				actual = driver.findElement(quantityOnHand).getText().trim();
				if (expected.equals(actual)) {

					tcConfig.updateTestReporter("TourRecordPage", "validateQOHIncreasedAfterReturn", Status.PASS,
							"Amount in QOH is reflecting same as before after return");
				}

				else {
					tcConfig.updateTestReporter("TourRecordPage", "validateQOHIncreasedAfterReturn", Status.FAIL,
							"Amount in QOH is not increased after returing");
				}

				waitUntilElementVisibleBy(driver, cancePrms, 120);
				clickElementJSWithWait(cancePrms);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("TourRecordPage", "validateQOHIncreasedAfterReturn", Status.FAIL,
						"Area to serach Promised Item not displayed");
			}

		} else {
			tcConfig.updateTestReporter("TourRecordPage", "validateQOHIncreasedAfterReturn", Status.FAIL,
					"Promised item button not displayed");
		}
	}

	public By btnCancelled = By.xpath("//span[contains(text(),'Cancelled')]");
	public By lnkTourDetails = By.xpath("//span[contains(text(),'Tour Details')]");
	public By btnMarkStatusAsCompleted = By
			.xpath("//button[@class='slds-button slds-button_brand slds-path__mark-complete']");
	public By cancelMsg = By.xpath("//div[contains(text(),'Are you sure, you want to cancel the tour package?')]");
	public By btnYess = By.xpath("//button[contains(text(),'Yes')]");
	public By tourRecordNo = By.xpath("(//span[@title='Tour Record Number']/following::div//span)[1]");
	public By searchTour = By.xpath("//input[@title='Search Salesforce']");
	public By tblReservation = By.xpath("(//span[@title='Tour Record Number'])[2]");
	public By tourStatus = By.xpath("//span[contains(text(),'Cancelled')]");

	public void validateCancelledTour() {
		waitUntilElementVisibleBy(driver, lnkTourDetails, 120);
		clickElementJSWithWait(btnCancelled);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnMarkStatusAsCompleted);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (driver.findElement(cancelMsg).getText().equals("Are you sure, you want to cancel the tour package?")) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyCancelledTour", Status.PASS,
					"Tour cancellation message is displayed");
			clickElementJSWithWait(btnYess);
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyCancelledTour", Status.FAIL,
					"Failed while validating Tour cancellation");
		}
		waitUntilElementVisibleBy(driver, tourRecordNo, 120);
		String tour = driver.findElement(tourRecordNo).getText();
		waitUntilElementVisibleBy(driver, searchTour, 120);
		driver.findElement(searchTour).sendKeys(tour);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(searchTour).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, tblReservation, 120);
		tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.PASS,
				"Reservation details are displayed");
		if (driver.findElement(tourStatus).getText().equals("Cancelled")) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyCancelledTour", Status.PASS,
					"Tour status is displyed as: cancelled");
		}

	}

	public By chkDistributedItems = By.xpath("(//th/div/input[@type='checkbox'])[2]");
	public By btnDistributeAdditonalItems = By.xpath("//button[contains(text(),'Distribute Additional Items')]");
	public By drpReasons = By.xpath("//label[contains(text(),'Reason')]/following::div//select");

	public void distributeAdditonalItems() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(btnDistributeAdditonalItems);
		waitUntilElementVisibleBy(driver, btnDistributeAdditonalItems, 120);
		if (verifyObjectDisplayed(btnDistributeAdditonalItems)) {
			List<WebElement> button = driver.findElements(chkDistributedItems);
			button.get(button.size() - 1).click();
			waitUntilElementVisibleBy(driver, btnDistributeAdditonalItems, 120);
			clickElementJSWithWait(btnDistributeAdditonalItems);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, drpReason, 120);
			new Select(driver.findElement(drpReason)).selectByIndex(1);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			// fieldDataEnter(drpQuantity, testData.get("Quantity"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, drpQuantity, 120);
			driver.findElement(drpQuantity).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(drpQuantity).sendKeys("1");
			// new Select(driver.findElement(drpQuantity)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "ClearpromiseQuantity", Status.PASS,
					"user should able to remove and add the Quantity");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "ClearpromiseQuantity", Status.FAIL,
					"user should able to remove and add the Quantity");
		}
	}

	public By drpTop = By.xpath("(//select[contains(@class,'slds-select select uiInput')])[1]");
	public By drpDown = By.xpath("(//select[contains(@class,'slds-select select uiInput')])[2]");
	public By txtVoucherInput = By.xpath("//label[contains(text(),'Voucher/Serial Number')]/following::div//input");
	/*
	 * public By btnAddPrmItem = By.xpath(
	 * "(//button[contains(text(),'Add Promised Item')])[1]"); public By
	 * searchProduct =By.xpath(
	 * "(//input[@class='slds-input slds-combobox__input'])[2]"); public By checkBox
	 * =By.xpath("//label[@class='slds-checkbox_faux']"); public By btnAdd
	 * =By.xpath("//button[@class='slds-button slds-button_brand']"); public By
	 * txtValue = By.xpath(
	 * "//div[@class='slds-form-element__control slds-grow']/input"); public By
	 * btnAddValue = By.xpath(
	 * "//footer[@class='slds-modal__footer']//button[contains(.,'Add')]"); public
	 * By chkValue
	 * =By.xpath("//tr[contains(@class,'parent')]//input[@type='checkbox']"); public
	 * By btnDistribute = By.xpath(
	 * "//button[@class='slds-button slds-button_neutral uiButton--default uiButton--brand uiButton forceActionButton']"
	 * ); public By txtVoucher =By.xpath(
	 * "(//div[@class='slds-truncate voucherNumber']//../input)[1]"); public By
	 * btnAddDistribute = By.xpath(
	 * "//button[@class='slds-button slds-button_brand']"); public By
	 * btnDistributeAdditional = By.xpath(
	 * "//button[contains(.,'Distribute Additional Items')]"); public By txtSearch =
	 * By.xpath("//div[contains(@class,'ProductLook')]//input[@role='combobox']" );
	 * //ul/li[@class='slds-listbox__item']/span public By drpReason
	 * =By.xpath("//div[@class='slds-select_container']/select"); public By
	 * drpQuantity
	 * =By.xpath("//label[contains(text(),'Quantity')]//following::input"); public
	 * By txtserialNo =By.xpath( "//input[@placeholder='Voucher/Serial Number']");
	 * public By txtValueNo = By.xpath("//input[@placeholder='Value']"); public By
	 * addbtn =By.xpath( "//button[@class='slds-button slds-button_brand']");
	 */
	public By btnAddDistributeItem = By
			.xpath("//footer[@class='slds-modal__footer']/..//button[contains(text(),'Add')]");
	// public By chkDistribute =
	// By.xpath("(//tr[contains(@class,'parent')]//input[@type='checkbox'])[2]");
	// //tr[contains(@class,'parent')][1])//input[@type='checkbox']

	/*
	 * public By chkDistribute = By.xpath(
	 * "(//div[text()='Distributed Items']//..//..//..//..//input)[2]");
	 */
	public By chkDistributedItem = By.xpath("//tr[@class='slds-line-height_reset ']//input");

	// public By chkDistribute=
	// By.xpath("//tr[td[div[span[contains(text(),'Activator')]]]]/td[1]/div/input");
	/*
	 * public By btnReturn =By.xpath("//button[contains(.,'Return')]"); public By
	 * returnReason = By.xpath(
	 * "//div[contains(text(),'American Express® Reward Card')]/following::select"
	 * ); public By btnRturn = By.xpath(
	 * "//div[@class='slds-modal__container']//button[contains(.,'Return')]");
	 */
	public void returnAddPromisedItem() {
		int intQuantity = 0;

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(btnDistributeAdditonalItems);
		waitUntilElementVisibleBy(driver, btnDistributeAdditonalItems, 120);
		if (verifyObjectDisplayed(btnDistributeAdditonalItems)) {

			driver.findElement(chkDistributedItems).click();
			waitUntilElementVisibleBy(driver, btnDistributeAdditonalItems, 120);
			clickElementJSWithWait(btnDistributeAdditonalItems);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtSearch).sendKeys(testData.get("SearchProduct"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy((By.xpath("//li//span[contains(text(),'" + testData.get("SearchProduct") + "')]")));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement element = driver.findElement(drpReason);
			Select s = new Select(element);
			List<WebElement> elementcount = s.getOptions();

			System.out.println(elementcount.size());
			for (int i = 0; i < elementcount.size(); i++) {
				String value = elementcount.get(i).getText();
				System.out.println(value);
				if (value.equals("Mystery Gift")) {
					elementcount.get(i).click();
					System.out.println("Pass");
					break;
				} else {
					System.out.println("Fail");
				}

			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtVoucherInput, 120);
			fieldDataEnter(txtVoucherInput, getRandomString(3));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtVoucherInput).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(btnAddDistributeItem);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(chkDistributedItem)) {
				waitUntilObjectVisible(driver, chkDistributedItem, 120);
				clickElementJSWithWait(chkDistributedItem);
				tcConfig.updateTestReporter("TourRecordsPage", "chkDistributedItem", Status.PASS,
						"chhechBox able to click");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "chkDistributedItem", Status.PASS,
						"chhechBox is not able to click");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, btnReturn, 120);
			tcConfig.updateTestReporter("TourRecordsPage", "validateDistributeTour", Status.PASS,
					"Return button is visible");
			clickElementJSWithWait(btnReturn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			new Select(driver.findElement(drpTop)).selectByIndex(2);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			new Select(driver.findElement(drpDown)).selectByIndex(2);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, btnRturn, 120);
			clickElementJSWithWait(btnRturn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "validateDistributeTour", Status.PASS,
					"Successfully returned product");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateDistributeTour", Status.FAIL,
					"Successfully returned product");
			System.out.println("Fail");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(600);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(addPromisedItemBtn)) {
			clickElementJSWithWait(addPromisedItemBtn);
			waitUntilObjectVisible(driver, addPromissedPage, 120);
			if (verifyObjectDisplayed(addPromissedsearch)) {
				fieldDataEnter(addPromissedsearch, testData.get("voucherNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, addPromPlus, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				intQuantity = Integer.parseInt(driver.findElement(quantityValue).getAttribute("value"));
				System.out.println(intQuantity);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				clickElementJSWithWait(addPromPlus);
				waitUntilElementVisibleBy(driver, addBtn, 120);
				clickElementJSWithWait(addBtn);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
				if (verifyObjectDisplayed(txtSuccessMsg)) {
					tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.PASS,
							"Successfully added the promised incentive");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL,
							"Promised incentive is not added");
				}
				/*
				 * } else{ System.out.println("Add Items Values is not present" ); }
				 */
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "addPromisedIncentive", Status.FAIL,
						"Promised search text box is not visible");
			}
		}
	}

	public void verifyMysteryNotPresent() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(btnDistributeAdditonalItems);
		waitUntilElementVisibleBy(driver, btnDistributeAdditonalItems, 120);
		if (verifyObjectDisplayed(btnDistributeAdditonalItems)) {
			driver.findElement(chkDistributedItems).click();
			waitUntilElementVisibleBy(driver, btnDistributeAdditonalItems, 120);
			clickElementJSWithWait(btnDistributeAdditonalItems);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtSearch).sendKeys(testData.get("SearchProduct"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy((By.xpath("//li//span[contains(text(),'" + testData.get("SearchProduct") + "')]")));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			WebElement element = driver.findElement(drpReason);
			Select s = new Select(element);
			List<WebElement> elementcount = s.getOptions();

			System.out.println(elementcount.size());
			for (int i = 0; i < elementcount.size(); i++) {
				String value = elementcount.get(i).getText();
				System.out.println(value);
				if (!value.equals("Mystery Gift")) {
					// elementcount.get(i).click();
					System.out.println("Pass");

					tcConfig.updateTestReporter("TourRecordsPage", "verifyProduct", Status.PASS,
							"user should not able to verify Mystery Gift");

				} else {
					System.out.println("Fail");
					tcConfig.updateTestReporter("TourRecordsPage", "verifyProduct", Status.FAIL,
							"user should able to verify Mystery Gift");
				}

			}

			waitForSometime(tcConfig.getConfig().get("LongWait"));

			driver.findElement(By.xpath("//footer[contains(@class,'slds-modal__footer')]/button[text()='Cancel']"))
					.click();
		}
	}

	public By txtSearchTour = By.xpath("//input[@title='Search Tour Records and more']");
	public By clickTourID = By.xpath("//a[contains(@title,'" + testData.get("TourNo") + "')]");
	public By tourDetails = By.xpath("(//span[contains(text(),'Tour Details')])");
	public By crsNumber = By
			.xpath("(//span[contains(text(),'CRS Account Number')])[1]/../..//span[contains(@class,'test-id')]//span");

	public void searchCRSTour() {
		try {

			waitUntilElementVisibleBy(driver, txtSearchTour, 120);

			if (verifyElementDisplayed(driver.findElement(txtSearchTour))) {
				fieldDataEnter(txtSearchTour, testData.get("TourNo").trim());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(txtSearchTour).sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, tblReservation, 120);
				tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.PASS,
						"Tour details are displayed");
				List<WebElement> button = driver.findElements(clickTourID);
				button.get(button.size() - 1).click();
				waitUntilElementVisibleBy(driver, tourDetails, 120);
				((JavascriptExecutor) driver).executeScript("window.scrollBy(0,600)", "");
				if (verifyObjectDisplayed(crsNumber)) {
					tcConfig.updateTestReporter("TourRecordsPage", "verifyCancelledTour", Status.PASS,
							"Tour status is displyed as: cancelled");
				} else {
					tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.FAIL,
							"Tour details are not displayed");
				}
			} else {
				tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.FAIL,
						"Tour details are not displayed");
			}
		} catch (Exception e) {

			tcConfig.updateTestReporter("ReservationPage", "validateGlobalSearchReservation", Status.FAIL,
					"Failed due to : exception caught " + e.getMessage());
		}
	}

	public void verifyQOHForMysteryGift() throws ParseException {
		waitUntilElementVisibleBy(driver, addPromisedItemBtn, 120);
		scrollDownForElementJSBy(addPromisedItemBtn);
		scrollUpByPixel(200);
		clickElementBy(addPromisedItemBtn);
		waitUntilObjectVisible(driver, addPromissedPage, 120);
		String[] productNames = testData.get("Product").split("/");
		List<String> qohValues = new ArrayList<String>();
		for (int i = 0; i < productNames.length; i++) {
			fieldDataEnter(addPromissedsearch, productNames[i]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, addPromPlus, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			qohValues.add(driver.findElement(fieldQOH).getText());
		}

		clickElementBy(By.xpath("//button[contains(text(),'Cancel')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		int count = 0;

		for (int i = 0; i < lstQohValues.size(); i++) {
			if (NumberFormat.getNumberInstance(java.util.Locale.US).parse(lstQohValues.get(0)).intValue()
					- 1 == NumberFormat.getNumberInstance(java.util.Locale.US).parse(qohValues.get(0)).intValue()) {
				count++;
			}
		}

		if (count == lstQohValues.size()) {
			tcConfig.updateTestReporter("SalesStorePage", "verifyQOHForMysteryGift", Status.PASS,
					"Qoh values of all product are decreased after distributing");
		} else {
			tcConfig.updateTestReporter("SalesStorePage", "verifyQOHForMysteryGift", Status.FAIL,
					"Qoh values of all product are not decreasing after distributing");
		}

	}

	public void distributeAdditionalItems() {
		waitUntilElementVisibleBy(driver, btnDistributeAddItems, 120);
		clickElementJSWithWait(btnDistributeAddItems);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, inputProductSearchBox, 120);
		fieldDataEnter(inputProductSearchBox, testData.get("ProductAdditional"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> e1 = driver
				.findElements(By.xpath("//span[@title='" + testData.get("ProductAdditional") + "']/.."));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (e1.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWb(driver, e1.get(0), 120);
			clickElementJSWithWait(e1.get(0));
			// e1.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		new Select(driver.findElement(drpDwnReason)).selectByIndex(2);

		List<WebElement> e3 = driver.findElements(inputVoucherValue);
		e3.get(0).sendKeys(generateRandomString(1, CHARACTER_STRING));
		e3.get(1).sendKeys("1000");

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(Additems);
		waitUntilObjectVisible(driver, succesMsg, 120);

		if (verifyObjectDisplayed(succesMsg)) {
			tcConfig.updateTestReporter("TourRecordsPage", "distributeAdditionalItems", Status.PASS,
					"Promised incentive is distributed");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "distributeAdditionalItems", Status.FAIL,
					"Promised incecntive is not distributed");
		}
	}

	private By incentiveGuestSig = By.xpath("//h3[text()='" + testData.get("Product") + "']");
	private By txtBoxSignature = By.xpath("//strong[contains(text(),'Signature')]/../canvas");
	private By btnSubmit = By.xpath("//button[@id='submitBtn']");
	private By pdfPage = By.xpath("//embed[contains(@type,'pdf')]");

	public void verifyGuestSignAndSubmit() {
		waitUntilElementVisibleBy(driver, incentiveGuestSig, 120);
		Actions actionBuilder = new Actions(driver);
		Action drawOnCanvas = actionBuilder.moveToElement(driver.findElement(txtBoxSignature), 50, 50)
				.clickAndHold(driver.findElement(txtBoxSignature)).moveByOffset(100, 60)
				.release(driver.findElement(txtBoxSignature)).build();
		drawOnCanvas.perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(btnSubmit);
		if (driver.findElement(btnSubmit).isEnabled()) {
			tcConfig.updateTestReporter("TourPage", "verifyTmSignaturePdf", Status.PASS, "Submit button is enabled");
			clickElementBy(btnSubmit);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("TourPage", "verifyTmSignaturePdf", Status.FAIL,
					"Submit button is not enabled");
		}
	}

	public void verifyTmSignaturePdf() {
		waitUntilElementVisibleBy(driver, pdfPage, 240);
		if (verifyObjectDisplayed(pdfPage)) {
			tcConfig.updateTestReporter("TourPage", "verifyTmSignaturePdf", Status.PASS,
					"PDF is coming after submitting the signature");
		} else {
			tcConfig.updateTestReporter("TourPage", "verifyTmSignaturePdf", Status.FAIL,
					"PDF is not coming after submitting the signature");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windows.get(0));
	}

	private By btnReBookTour = By.xpath("//button[contains(text(),'Re-Book')]");
	private By fieldSalestore = By.xpath("//span[text()='Sales Store']/../../div[2]/span");
	private By btnNext = By.xpath("//button[contains(text(),'Next')]");

	public void reBookTour() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnReBookTour, 120);
		if (verifyObjectDisplayed(btnReBookTour)) {
			tcConfig.updateTestReporter("TourPage", "reBookTour", Status.PASS, "Rebook button is visible");
		} else {
			tcConfig.updateTestReporter("TourPage", "reBookTour", Status.FAIL, "Rebook button is not visible");
		}
		clickElementBy(btnReBookTour);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> e = driver.findElements(fieldSalestore);
		if (e.size() > 0) {
			int size = e.get(e.size() - 1).findElements(By.xpath(".//*")).size();
			if (size == 0) {
				tcConfig.updateTestReporter("TourPage", "reBookTour", Status.PASS,
						"Tour is coming to tour details after clicking Rebook Tour button");
			} else {
				tcConfig.updateTestReporter("TourPage", "reBookTour", Status.FAIL,
						"Tour is not coming to tour details after clicking Rebook Tour button");
			}
		} else if (driver.findElements(btnNext).size() > 0) {
			tcConfig.updateTestReporter("TourPage", "reBookTour", Status.PASS,
					"Tour is coming to tour details after clicking Rebook Tour button in Mobile UI");
		} else {
			tcConfig.updateTestReporter("TourPage", "reBookTour", Status.FAIL,
					"Tour is not coming to tour details after clicking Rebook Tour button in Mobile UI or Native");
		}
	}

	public void selectSalesstoreAnd() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnNext, 120);
	}

	private By btntimeSlotMobUi = By.xpath("//h2[text()='" + testData.get("NewSalesStore")
			+ "']/../../../div[3]//div[@class='available-time-slot']/div[2]/button");
	private By btnNextSalesStore = By
			.xpath("//h2[text()='" + testData.get("NewSalesStore") + "']/../../../div[1]//button");
	// private By btnNext = By.xpath("//button[contains(text(),'Next')]");

	public void getAppointmentSlotMobUi() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btntimeSlotMobUi, 120);
		clickElementJSWithWait(btntimeSlotMobUi);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	private By btnProceed = By.xpath("//button[@title='Proceed']");

	public void clickNext() {
		waitUntilElementVisibleBy(driver, btnNextSalesStore, 120);
		scrollUpByPixel(200);
		if (driver.findElement(btnNextSalesStore).isEnabled()) {
			clickElementBy(btnNextSalesStore);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElements(btnProceed).size() > 0) {
				clickElementBy(btnProceed);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("TourPage", "clickNext", Status.FAIL, "Next button is not enabled");
		}
	}

	private By btnAddIncentive = By.xpath("//button[contains(text(),'Add Incentive')]");
	private By fieldAddedIncentive = By
			.xpath("//div[@title='" + testData.get("Product") + "'] | //td[text()='" + testData.get("Product") + "']");

	public void verifyIncentiveAvailableMobUi() {
		waitUntilElementVisibleBy(driver, btnAddIncentive, 120);
		if (verifyObjectDisplayed(fieldAddedIncentive) || verifyElementDisplayed(driver.findElement(By.xpath("")))) {
			tcConfig.updateTestReporter("TourPage", "verifyIncentiveAvailableMobUi", Status.PASS,
					"Selected incentive is visible");
		} else {
			tcConfig.updateTestReporter("TourPage", "verifyIncentiveAvailableMobUi", Status.FAIL,
					"Selected incentive is not visible");
		}

		if (driver.findElements(btnNext).size() > 0) {
			clickElementBy(btnNext);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (driver.findElements(btnProceed).size() > 0) {
				clickElementBy(btnProceed);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			if (driver.findElements(btnNext).size() > 0) {
				clickElementBy(btnNext);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}
	}

	private By btnConfirmCriteria = By.xpath("//button[@class='confirm-btn']");
	private By divConfirmCriteria = By.xpath("//div[contains(@class,'cTMCMAEligibilityCriteriaItem')]");

	public void selectCriteria() {
		waitUntilElementVisibleBy(driver, divConfirmCriteria, 120);
		for (WebElement e : driver.findElements(btnConfirmCriteria)) {
			clickElementWb(e);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	private By canvasSignature = By.xpath("//canvas[@class='signature-pad']");
	private By btnConfirmAndBook = By.xpath("//button[text()='CONFIRM & BOOK']");

	public void enterDigitalSignature() {
		waitUntilElementVisibleBy(driver, canvasSignature, 120);
		Actions actionBuilder = new Actions(driver);
		Action drawOnCanvas = actionBuilder.moveToElement(driver.findElement(canvasSignature), 50, 50)
				.clickAndHold(driver.findElement(canvasSignature)).moveByOffset(100, 60)
				.release(driver.findElement(canvasSignature)).build();
		drawOnCanvas.perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	private By txtBoxMobile = By.xpath("//span[contains(text(),'Mobile')]/../..//input");
	private By txtBoxEmail = By.xpath("//span[contains(text(),'Email')]/../..//input");
	private By txtBoxTitle = By.xpath("//span[contains(text(),'Title')]/../..//input");
	private By btnConfirm = By.xpath("//button[text()='Confirm']");

	public void enterDetailsAndBookTour() {
		waitUntilElementVisibleBy(driver, btnConfirmAndBook, 120);
		if (driver.findElement(txtBoxMobile).getText().length() == 0) {
			fieldDataEnter(txtBoxMobile, testData.get("MobileNo"));
		}
		String parent = driver.getWindowHandle();
		if (driver.findElement(txtBoxEmail).getText().length() == 0) {
			fieldDataEnter(txtBoxEmail, testData.get("Email"));
			Actions act = new Actions(driver);
			act.sendKeys(Keys.TAB);
		}
		if (driver.findElement(btnConfirmAndBook).isEnabled()) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String par = driver.getWindowHandle();
			if (par.equals(parent)) {
				log.info("same handle");
			}
			List<WebElement> ele = driver.findElements(btnConfirmAndBook);
			ele.get(ele.size() - 1).click();
			// clickElementJSWithWait(btnConfirmAndBook);
			/*
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * waitUntilElementVisibleBy(driver, txtBoxTitle, 120);
			 * fieldDataEnter(txtBoxTitle, "test"); waitUntilElementVisibleBy(driver,
			 * btnConfirm, 120); clickElementJSWithWait(btnConfirm);
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			tcConfig.updateTestReporter("TourPage", "enterDetailsAndBookTour", Status.PASS,
					"Confirm and book tour button is enabled");
		} else {
			tcConfig.updateTestReporter("TourPage", "enterDetailsAndBookTour", Status.FAIL,
					"Confirm and book tour button is not enabled");
		}
		// driver.navigate().back();
		/*
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * driver.findElement(By.xpath(
		 * "(//img[@class='wn-left_custom_icon'])[1]")).click();
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 */
		waitForSometime(tcConfig.getConfig().get("LongWait"));

	}

	/**************************
	 * Added for SFMC Automation
	 ****************************/

	public void updateAppointmentDate() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(bookOptionSelect);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(apointmentFromDatePicker);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(updatedApointmentFromDate);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJSWithWait(apointmentToDatePicker);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(updatedApointmentToDate);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("TourRecordsPage", "updateAppointmentDate", Status.PASS,
				"Appointment date updated successfully");
		waitUntilElementVisible(driver, getAppointmentsBtn, 120);
		mouseoverAndClick(getAppointmentsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
		clickElementJS(lblAvailableSlot);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, yesBtn, 120);
		clickElementJS(yesBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, okBtn, 120);
		clickElementJS(okBtn);
		tcConfig.updateTestReporter("TourRecordsPage", "updateAppointmentDate", Status.PASS,
				"BookAppoitment is succesfully");

	}

	public void changeSalesStore() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(bookOptionSelect);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(removeSalesStoreIcon)) {
			clickElementJSWithWait(removeSalesStoreIcon);
			tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
					"User is able to remove Sales store is blank");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.FAIL,
					"User is not able to remove Sales store is blank");

		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, reSalesStoretxt, 120);
			fieldDataEnter(reSalesStoretxt, testData.get("NewSalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("NewSalesStore") + "')][1]")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			e.getMessage();
		}
		waitUntilElementVisible(driver, getAppointmentsBtn, 120);
		mouseoverAndClick(getAppointmentsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
		clickElementJS(lblAvailableSlot);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, yesBtn, 120);
		clickElementJS(yesBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, okBtn, 120);
		clickElementJS(okBtn);
		tcConfig.updateTestReporter("TourRecordsPage", "BookAppotment", Status.PASS,
				"Appoitment Update is succesfully");

	}

	public void changebookingTime() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(bookOptionSelect);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(getAppointmentsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lblSecondAvailableSlot, 120);
		clickElementJS(lblSecondAvailableSlot);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, yesBtn, 120);
		clickElementJS(yesBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, okBtn, 120);
		clickElementJS(okBtn);
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("TourRecordsPage", "BookAppotment", Status.PASS,
				"Appoitment Update is succesfully");

	}

	public void clickGuestConfirmationSFMC() {
		waitUntilObjectVisible(driver, btnSFMC, 120);
		clickElementJSWithWait(btnSFMC);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, txtMsg, 120);
		clickElementJSWithWait(btnEmailNotif);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(sucessMsg)) {
			waitUntilObjectVisible(driver, btnOk, 120);
			clickElementJSWithWait(btnOk);
			tcConfig.updateTestReporter("TourRecordsPage", "clickGuestConfirmationSFMC", Status.PASS,
					"Guest Confirmation button is clicked and email ntotification sent sucessfully");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "clickGuestConfirmationSFMC", Status.FAIL,
					"Guest Confirmation button not found");
		}
	}

	public void clickGuestConfirmationforCWTour() {
		waitUntilObjectVisible(driver, btnSFMC, 120);
		clickElementJSWithWait(btnSFMC);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, txtMsg, 120);
		clickElementJSWithWait(btnEmailNotif);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(sucessMsgforCW)) {
			waitUntilObjectVisible(driver, btnOk, 120);
			clickElementJSWithWait(btnOk);
			tcConfig.updateTestReporter("TourRecordsPage", "clickGuestConfirmationSFMC", Status.PASS,
					"Guest Confirmation button is clicked and email ntotification sent sucessfully");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "clickGuestConfirmationSFMC", Status.FAIL,
					"Guest Confirmation button not found");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickOnCreatePayment
	 * @Description : This function is used to click on Create Paymnet button
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickonIncentiveTab() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(incentiveLink);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void validateTransactionErrorMessage() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		Assert.assertTrue(transactionErrorMsg.isDisplayed());
		tcConfig.updateTestReporter("IncentiveDetailsPage", "validateTransactionErrorMessage", Status.PASS,
				"Transaction Error Message Validated Successfully");

	}

	public void clickonPaymentsTab() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(paymentsLink);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonPaymentIdLink() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		paymentIdLink.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/**
	 * This method is to validate if the refundable Tour Deposit Section is present
	 * or not in the Incentive details screen
	 * 
	 * @author C40122
	 * @throws Exception
	 */

	public void validatePresenceofPaymentsDetailsPage() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(paymentsDetailsPageHdr.isDisplayed());
		tcConfig.updateTestReporter("PaymentDetailsPage", "validatePresenceofPaymentsDetailsPage", Status.PASS,
				"Payment Details Page is loaded Successfully");

	}

	public void clickonCreatePaymentButton() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(createPayBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonAddIncentiveButton() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(addIncentiveBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void searchandAddIncentive() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(addPromissedsearch)) {
			fieldDataEnter(addPromissedsearch, testData.get("Product"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, addPromPlus, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(addPromPlus);
			waitUntilElementVisibleBy(driver, addBtn, 120);
			clickElementJSWithWait(addBtn);

			waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
			if (verifyObjectDisplayed(txtSuccessMsg)) {
				tcConfig.updateTestReporter("TourRecordsPage", "addIncentive", Status.PASS,
						"Successfully added the  incentive");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "addIncentive", Status.FAIL, "Failed to add Incentive");
			}

		}

	}

	public void searchandAddSecondIncentive() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(addPromissedsearch)) {
			fieldDataEnter(addPromissedsearch, testData.get("SecondProduct"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, addPromPlus, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(addPromPlus);
			waitUntilElementVisibleBy(driver, addBtn, 120);
			clickElementJSWithWait(addBtn);

			waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
			if (verifyObjectDisplayed(txtSuccessMsg)) {
				tcConfig.updateTestReporter("TourRecordsPage", "addIncentive", Status.PASS,
						"Successfully added the  incentive");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "addIncentive", Status.FAIL, "Failed to add Incentive");
			}

		}

	}

	public void captureInitialMarketerCost() throws Exception {

		String strMarketerCostValue = marketerCost.getText();
		marketerCostAmt = Double.parseDouble(strMarketerCostValue.replace("$", "").trim());
		System.out.println(marketerCostAmt);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void enterIncentiveAmountCollected() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		double dblTotalIncentiveAmt = totalRetailAmount()
				+ Double.parseDouble(testData.get("AdditionalIncentiveAmount"));
		System.out.println(dblTotalIncentiveAmt);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(incentiveAmountCollected, (Double.toString(dblTotalIncentiveAmt).split("\\.")[0].trim()));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void verifyDiscountDistribution() throws Exception {

		double dblTotalDiscount = 0.00;

		List<WebElement> Rows = incentiveDetailsTable.findElements(By.tagName("tr"));

		for (int i = 1; i < Rows.size(); i++) {

			String strDiscountPerProduct = driver
					.findElement(By.xpath("//div[contains(text(),'Name')]/../../../../tbody/tr[" + i + "]/td[5]"))
					.getText();

			double dblDiscountPerProduct = Double.parseDouble(strDiscountPerProduct.replace("$", "").trim());

			Assert.assertNotEquals(dblDiscountPerProduct, "0.00");
			tcConfig.updateTestReporter("IncentiveDiscountDetails", "verifyDiscountDistribution", Status.PASS,
					"Discount Amount is not zero for any Product");

			System.out.println("Amt :" + dblDiscountPerProduct);

			dblTotalDiscount = dblTotalDiscount + dblDiscountPerProduct;

		}

		System.out.println("Total Amt :" + dblTotalDiscount);

		Assert.assertEquals(dblTotalDiscount, Double.parseDouble(testData.get("AdditionalIncentiveAmount")));
		tcConfig.updateTestReporter("IncentiveDiscountDetails", "verifyDiscountDistribution", Status.PASS,
				"Distribution of Total Discount across the IncentivesTota validated successfully");

	}

	public void verifyUpdatedMrktrCost() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		String strUpdtdMarketerCost = marketerCost.getText();
		double dblUpdtdMarketerCostAmt = Double.parseDouble(strUpdtdMarketerCost.replace("$", "").trim());
		System.out.println(dblUpdtdMarketerCostAmt);
		double dbIncentiveAmt = dblTotalAmt + Double.parseDouble(testData.get("AdditionalIncentiveAmount"));

		double expectedUpdatedMarketerCost = (marketerCostAmt - dbIncentiveAmt);
		System.out.println(expectedUpdatedMarketerCost);
		Assert.assertEquals(dblUpdtdMarketerCostAmt, expectedUpdatedMarketerCost);
		tcConfig.updateTestReporter("IncentiveDiscountDetails", "verifyUpdatedMrktrCost", Status.PASS,
				"Marketer Cost Amount validated successfully");

	}

	public double totalRetailAmount() throws Exception {

		// double dblTotalAmt = 0.00;
		if (verifyElementDisplayed(incentiveDetailsTable)) {

			tcConfig.updateTestReporter("IncentiveDetailsPage", "totalDiscountAmout", Status.PASS,
					"Incentive Details Table Displayed Successfully");

		}

		List<WebElement> Rows = incentiveDetailsTable.findElements(By.tagName("tr"));

		for (int i = 1; i < Rows.size(); i++) {

			String strRetailValue = driver
					.findElement(By.xpath("//div[contains(text(),'Name')]/../../../../tbody/tr[" + i + "]/td[4]"))
					.getText();

			String strAmt = strRetailValue.replace("$", "").trim();

			System.out.println("Amt :" + strAmt);
			dblTotalAmt = dblTotalAmt + Double.parseDouble(strAmt);

		}

		System.out.println("Total Amt :" + dblTotalAmt);
		return dblTotalAmt;

	}

	public void verifyDiscountAppliedAmt() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> Rows = incentiveDetailsTable.findElements(By.tagName("tr"));

		for (int i = 1; i < Rows.size(); i++) {
			String strProductName = driver
					.findElement(By.xpath("//div[contains(text(),'Name')]/../../../../tbody/tr[" + i + "]/th"))
					.getText();
			String strRetailValue = driver
					.findElement(By.xpath("//div[contains(text(),'Name')]/../../../../tbody/tr[" + i + "]/td[4]"))
					.getText();
			String strDiscountApplied = driver
					.findElement(By.xpath("//div[contains(text(),'Name')]/../../../../tbody/tr[" + i + "]/td[5]"))
					.getText();
			String strIncentiveAmount = driver
					.findElement(By.xpath("//div[contains(text(),'Name')]/../../../../tbody/tr[" + i + "]/td[6]"))
					.getText();
			double dblIncentiveAmt = Double.parseDouble(strIncentiveAmount.replace("$", "").trim());
			double dblRetailsValue = Double.parseDouble(strRetailValue.replace("$", "").trim());

			double dblExpectedDiscount = dblIncentiveAmt - dblRetailsValue;

			Assert.assertEquals(strDiscountApplied.replace("$", "").trim(), String.format("%.2f", dblExpectedDiscount));
			tcConfig.updateTestReporter("IncentiveDiscountDetails", "verifyDiscountDistribution", Status.PASS,
					"Discount Applied amount is displaying correctly for the Product:" + strProductName);

		}

	}

	public void validateReasonDrpdwn() {

		Select select = new Select(driver.findElement(incentiveReasonDropDown));
		List<WebElement> allOptions = select.getOptions();

		for (int i = 1; i < allOptions.size(); i++)

		{
			System.out.println(allOptions.get(i).getText());
			listofIncentiveReasons.add(allOptions.get(i).getText());
		}

		ArrayList<String> expectedResonValues = new ArrayList<String>() {
			{
				add("Enticement");
				add("Activator");
				add("Promotion");
			}

		};

		Collections.sort(listofIncentiveReasons);
		Collections.sort(expectedResonValues);

		Assert.assertTrue(listofIncentiveReasons.equals(expectedResonValues));
		tcConfig.updateTestReporter("IncentiveDetailsPage", "validateReasonDrpdwn", Status.PASS,
				"Incentive Reason Values are correct");

	}

	/**
	 * This method is to validate if the default Incentive Reason value is correct
	 * or not
	 * 
	 * @author C40122
	 * @throws Exception
	 */

	public void validateDefaultReason() throws Exception {
		Select select = new Select(driver.findElement(incentiveReasonDropDown));
		WebElement option = select.getFirstSelectedOption();
		Assert.assertEquals(option.getText(), "Enticement");
		tcConfig.updateTestReporter("IncentiveDetailsPage", "validateDefaultReason", Status.PASS,
				"Default Incentive Reason value is validated");

	}

	/**
	 * This method is to validate if the refundable Tour Deposit Section is present
	 * or not in the Incentive details screen
	 * 
	 * @author C40122
	 * @throws Exception
	 */

	public void validateRefundableTourDepositSection() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(lblrefundableTourDeposite.isDisplayed());
		tcConfig.updateTestReporter("IncentiveDetailsPage", "validateRefundableTourDepositSection", Status.PASS,
				"Presence of Refundable Tour Deposit Section Validated Successfully");

	}

	/**
	 * This method is to validate if the refundable Tour Deposit Section is absent
	 * in the Incentive details screen
	 * 
	 * @author C40122
	 * @throws Exception
	 */

	public void validateAbsenceofRefndbleTourDpstSection() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(driver.findElements(By.xpath("//td[contains (.,'Refundable Tour Deposit')]")).size() < 1);
		tcConfig.updateTestReporter("IncentiveDetailsPage", "validateRefundableTourDepositSection", Status.PASS,
				"Absence of Refundable Tour Deposit Section Validated Successfully");

	}

	public void validatetheAbsenceofVoidButton() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(driver.findElements(By.xpath("//button[contains (text(),'Void')]")).size() < 1);
		tcConfig.updateTestReporter("PaymentDetailsPage", "validatetheAbsenceofVoidButton", Status.PASS,
				"Absence of Void Button Validated Successfully");

	}

	public void enterCloverDeviceName() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Robot rob = new Robot();
		rob.mouseWheel(5);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// driver.findElement(txtCloverDeviceName).click();
		clickElementJSWithWait(txtCloverDeviceName);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// driver.findElement(By.xpath("//span[contains(text(),'"+testData.get("CloverDeviceName")+"')]")).click();
		clickElementJSWithWait(By.xpath("//span[contains(text(),'" + testData.get("CloverDeviceName") + "')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void selectPaymentMethod() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(selectPayment)).selectByValue(testData.get("PaymentMethod"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonProcessPaymentButton() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(processPaymentBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void validateWarningMessage() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(processPaymentWarningMessage.isDisplayed());
		tcConfig.updateTestReporter("IncentiveDetailsPage", "validateWarningMessage", Status.PASS,
				"Payment Warning Message Validated Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void clickonPaymentProceedButton() {

		paymentProceedButton.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonPaymentContactDtlsCancelButton() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// paymentCntctDtlsCancelButton.click();
		clickElementJSWithWait(paymentCntctDtlsCancelButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().alert().accept();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonPrintReceiptButton() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(printReceiptButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// driver.switchTo().alert().accept();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void verifyPDFgeneration() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabs.size());
		driver.switchTo().window(tabs.get(1));
		waitUntilElementVisibleBy(driver, pdfPage, 240);

		if (verifyObjectDisplayed(pdfPage)) {
			tcConfig.updateTestReporter("TourPage", "verifyTmSignaturePdf", Status.PASS,
					"PDF is coming after submitting the signature");
		} else {
			tcConfig.updateTestReporter("TourPage", "verifyTmSignaturePdf", Status.FAIL,
					"PDF is not coming after submitting the signature");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(tabs.get(0));

	}

	public void clickonRefundButton() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(refundButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonResendEmail_TextButton() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(resendEmailorTextButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void clickonRefndBtninRefundDtlsPopUp() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(refundButtoninDetailsPopUp);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void clickonOfflineRefundButton() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, offlineRefundButton, 180);
		clickElementJSWithWait(offlineRefundButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void acceptRefundMessage() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(refundSuccessMessage.isDisplayed());
		System.out.println(refundSuccessMessage.getText());
		tcConfig.updateTestReporter("RefundDetailsPage", "acceptRefundMessage", Status.PASS,
				"Refund Message Validated Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(refundCompleteButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void validateTransactionStatus() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertEquals(transactionStatus.getText(), testData.get("TransactionStatus"));
		tcConfig.updateTestReporter("PaymentDetailsPage", "validateTransactionStatus", Status.PASS,
				"Transaction Status Validated Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void validateRefundEmailfunationality() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(paymentContactEmailtxt);
		fieldDataEnter(paymentContactEmailtxt, testData.get("Email"));
		clickElementJSWithWait(sendEmailChkBox);

		clickElementJSWithWait(paymentCntctDtlsSendButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(refundReceiptProcessMessage.isDisplayed());
		tcConfig.updateTestReporter("ContactDetailsPopUp", "validateRefundEmailfunationality", Status.PASS,
				"Payment Refund Receipt Successfully Send through Emaily");

		clickElementJSWithWait(contactDetailsPopUpCloseBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void validateRefundTextfunationality() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(paymentContactPhoneNo);

		String mobileNo = testData.get("MobileNo");
		System.out.println(mobileNo);
		// fieldDataEnter(paymentContactPhoneNo, mobileNo);
		driver.findElement(paymentContactPhoneNo).clear();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(paymentContactPhoneNo).sendKeys(mobileNo);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(sendTextChkBox);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(By.xpath("//select[contains (@class,'slds-select')]")))
				.selectByValue(testData.get("MobileCarrierName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJSWithWait(paymentCntctDtlsSendButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(refundReceiptProcessMessage.isDisplayed());
		tcConfig.updateTestReporter("ContactDetailsPopUp", "validateRefundEmailfunationality", Status.PASS,
				"Payment Refund Receipt Successfully Send through Emaily");

		clickElementJSWithWait(contactDetailsPopUpCloseBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void verifyRefundDetails() throws Exception {

		double dblTotalRefund = 0.00;
		List<WebElement> Rows = refundDetailsTable.findElements(By.tagName("tr"));

		for (int i = 1; i < Rows.size(); i++) {

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJSWithWait(
					By.xpath("//span[@title='Item']/../../../../../tbody/tr[" + i + "]/td[3]/div/input"));
			driver.findElement(By.xpath("//span[@title='Item']/../../../../../tbody/tr[" + i + "]/td[3]/div/input"))
					.clear();
			driver.findElement(By.xpath("//span[@title='Item']/../../../../../tbody/tr[" + i + "]/td[3]/div/input"))
					.sendKeys("1");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(
					By.xpath("//span[@title='Item']/../../../../../tbody/tr[" + i + "]/td[1]//div//span//label"))
					.click();

			String strRefundAmt = driver
					.findElement(By.xpath("//span[@title='Item']/../../../../../tbody/tr[" + i + "]/td[4]")).getText();
			System.out.println(strRefundAmt);

			double dblRedundPerProduct = Double.parseDouble(strRefundAmt.replace("$", "").trim());

			Assert.assertNotEquals(dblRedundPerProduct, "0.00");
			tcConfig.updateTestReporter("RefundDetails", "verifyRefundDetails", Status.PASS,
					"Refund Amount is not zero for any Product");

			System.out.println("Amt :" + dblRedundPerProduct);

			dblTotalRefund = dblTotalRefund + dblRedundPerProduct;

		}

		System.out.println("Total Amt :" + dblTotalRefund);

		double expectedTotalRefund = dblTotalAmt + Double.parseDouble(testData.get("AdditionalIncentiveAmount"));

		// double expectedTotalRefund = 71.00;
		Assert.assertEquals(dblTotalRefund, expectedTotalRefund);
		tcConfig.updateTestReporter("RefundDetails", "verifyRefundDetails", Status.PASS,
				"Summation of Refund Amount validated successfully");

	}

	public void enterCloverDeviceNameinRefundPopUp() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(txtCloverDeviceName);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(By.xpath("//span[contains(text(),'" + testData.get("CloverDeviceName") + "')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void getTransactionId() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		strTransactionId = transactionId.getText();

	}

	public void validateRefundLog() {

		validateRefundedByUserDetals();
		// validateRefundedDate();
		validateRefundMethod();

	}

	public void validateRefundedByUserDetals() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		System.out.println(refundedByUserId.getText());
		Assert.assertEquals(refundedByUserId.getText(), "Salesforce Admin");
		tcConfig.updateTestReporter("PaymentRefundDetailsPage", "validateRefundedByUserDetals", Status.PASS,
				"Refunded By user Id Validated Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void validateRefundMethod() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		System.out.println(refundMethod.getText());
		Assert.assertEquals(refundMethod.getText(), testData.get("PaymentMethod"));
		tcConfig.updateTestReporter("PaymentRefundDetailsPage", "validateRefundMethod", Status.PASS,
				"Refund Method Validated Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void validateRefundedDate() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();

		Date today = cal.getTime();
		String strExpectedDate = sdf.format(today);
		String strActualDate = refundDate.getText().split("\\s+")[0].trim();

		Assert.assertEquals(strExpectedDate, strActualDate);
		tcConfig.updateTestReporter("PaymentRefundDetailsPage", "validateRefundedDate", Status.PASS,
				"Refund Date Validated Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void clickOnCreatePayment() {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(incentiveLink);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, createPayBtn, 120);
			tcConfig.updateTestReporter("TourRecordsPage", "clickOnCreatePayment", Status.PASS,
					"Clicked on Incentive Tab successfully");

			clickElementJSWithWait(createPayBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJSWithWait(addIncentiveBtn);

			waitUntilElementVisible(driver, driver.findElement(addIncentiveBtn), 120);
			waitUntilObjectVisible(driver, addIncentiveBtn, 120);
			if (verifyElementDisplayed(driver.findElement(addIncentiveBtn))) {
				tcConfig.updateTestReporter("TourRecordsPage", "clickOnCreatePayment", Status.PASS,
						"Clicked on Create Payment SUccessfully");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "clickOnCreatePayment", Status.FAIL,
						"Failed to Clicked on Create Payment Successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickOnCreatePayment", Status.FAIL,
					"Failed to Clicked on Create Payment Successfully" + e.getMessage());

		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : addIncentive
	 * @Description : This function is used to click on add ncentive button and
	 *              serach an incetive
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void addIncentive() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// Integer intQuantity;
		Integer intIncentive;
		try {
			clickElementJSWithWait(addIncentiveBtn);
			if (verifyObjectDisplayed(addPromissedsearch)) {
				fieldDataEnter(addPromissedsearch, testData.get("Product"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, addPromPlus, 120);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// need to validate this line

				// intQuantity =
				// Integer.parseInt(driver.findElement(quantityValue).getAttribute("value"));
				clickElementJSWithWait(addPromPlus);
				waitUntilElementVisibleBy(driver, addBtn, 120);
				intIncentive = Integer.parseInt(driver.findElement(retailCostLbl).getAttribute("value"));
				clickElementJSWithWait(addBtn);

				waitUntilElementVisibleBy(driver, txtSuccessMsg, 120);
				if (verifyObjectDisplayed(txtSuccessMsg)) {
					tcConfig.updateTestReporter("TourRecordsPage", "addIncentive", Status.PASS,
							"Successfully added the  incentive");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "addIncentive", Status.FAIL,
							"Failed to add Incentive");
				}
				// Add Product was done. now

				if (verifyObjectDisplayed(Additemsvalue)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(Additemsvalue).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(Additemsvalue).sendKeys(intIncentive.toString());
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					// driver.findElement(Additems).click();
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					System.out.println("Add Items Values is not present");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(refTourDepChkBx).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				Robot rob = new Robot();
				rob.mouseWheel(5);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// clickElementBy(selectPayment);
				new Select(driver.findElement(selectPayment)).selectByValue("Cash");

			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickPaymentTab
	 * @Description : This function is used to navigate to Payment Tab
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickPaymentTab() {

		try {
			waitUntilObjectVisible(driver, paymentLink, 120);

			// waitUntilElementInVisible(driver, incentivesTab, 120);
			clickElementJSWithWait(paymentLink);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(paymentID);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "clickPaymentTab", Status.PASS,
					"Clicked on Payment ID successfully");

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(paymentIDLbl))) {

				tcConfig.updateTestReporter("TourRecordsPage", "clickPaymentTab", Status.PASS,
						"Successfully verified presence of Unique Paymnet Identifier");
			}

			else {
				tcConfig.updateTestReporter("TourRecordsPage", "clickPaymentTab", Status.FAIL,
						"Failed to verify presence of Unique Paymnet Identifier");
			}

		} catch (Exception e) {
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickPaymentTab", Status.FAIL,
					"Failed In Method" + e.getMessage());

		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickResendEmail
	 * @Description : This function is used to navigate to Payment Tab
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void verifyClickResendEmail() {
		String strPhoneNum = testData.get("Phone");
		try {
			waitUntilObjectVisible(driver, resendBtn, 120);

			// waitUntilElementInVisible(driver, incentivesTab, 120);
			clickElementJSWithWait(resendBtn);
			tcConfig.updateTestReporter("TourRecordsPage", "verifyClickResendEmail", Status.PASS,
					"Clicked on Resend EMail/Text successfully");

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(paymentScrnLbl))) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyClickResendEmail", Status.PASS,
						"Navigated to Payment Contact Details Screen");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				fieldDataEnter(phoneNumTxt, strPhoneNum);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(sndTxtChk);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(carrierDrp);
				waitForSometime(tcConfig.getConfig().get("MedwWait"));
				new Select(driver.findElement(carrierDrp)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedwWait"));
				tcConfig.updateTestReporter("TourRecordsPage", "verifyClickResendEmail", Status.PASS,
						"Phone Number details added Successfully");
				WebElement ele = driver.findElement(sendBtn);
				clickElementJS(ele);
				waitForSometime(tcConfig.getConfig().get("LongwWait"));
				tcConfig.updateTestReporter("TourRecordsPage", "verifyClickResendEmail", Status.PASS,
						"Navigated to Payment Contact Details Screen");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyClickResendEmail", Status.FAIL,
						"Failed to Navigate to Payment Contact Details Screen");
			}

		} catch (Exception e) {
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickPaymentTab", Status.FAIL,
					"Failed In Method to Resend Text Message" + e.getMessage());

		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickPrintReceipt
	 * @Description : This function is used to click on the Print Receipt and send
	 *              the receipt for printing
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickPrintReceipt() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			waitUntilObjectVisible(driver, printBtn, 120);

			clickElementJSWithWait(printBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "clickPrintReceipt", Status.PASS,
					"Clicked on Print Receipt successfully");

		} catch (Exception e) {
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickPrintReceipt", Status.FAIL,
					"Failed In Method" + e.getMessage());

		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifyTransferBtn
	 * @Description : This function is used to verify Transfer Payment Button
	 *              functionality
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void verifyTransferBtn() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String strTourNo = testData.get("TourNoPayment");
		try {
			waitUntilObjectVisible(driver, printBtn, 120);
			clickElementJSWithWait(transferBtn);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTransferBtn", Status.PASS,
					"Clicked on Tranfer Button successfully");
			if (verifyElementDisplayed(driver.findElement(transferPopLbl))) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyTransferBtn", Status.PASS,
						"Navigated to Transfer Payment Popup successfully");
				// Search for the Tour where the payment will be transferred

//                    driver.findElement(By.xpath("//a//span[text()='Home']")).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(searchTxt).sendKeys(strTourNo);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String selectTour = "//span[contains(@class, 'slds-media__body')]/span[contains(., '" + strTourNo
						+ "')]";
				WebElement ele = driver.findElement(By.xpath(selectTour));
				clickElementJSWithWait(ele);
				// driver.findElement(By.xpath(selectTour)).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				tcConfig.updateTestReporter("TourRecordsPage", "verifyTransferBtn", Status.PASS,
						"Clicked on Tour To Transfer Payment successfully");

			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyTransferBtn", Status.PASS,
						"Click on Transfer Payment button Unsuccessful");
			}
		} catch (Exception e) {
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTransferBtn", Status.FAIL,
					"Failed In Method" + e.getMessage());

		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickTransferBtnPopup
	 * @Description : This function is used to click on Transfer button in the Popup
	 *              Window
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickTransferBtnPopup() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			WebElement ele = driver.findElement(popTransferBtb);
			clickElementJSWithWait(ele);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTransferBtn", Status.PASS,
					"Successfully click on Transfer button in Popup");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickTransferBtnPopup", Status.FAIL,
					"Failed In Method to click on Transfer button in Popup" + e.getMessage());
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickCancelBtnPopup
	 * @Description : This function is used to click on cancel /Exit popup
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickCancelBtnPopup() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			WebElement ele = driver.findElement(closeBtn);
			clickElementJSWithWait(ele);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "clickCancelBtnPopup", Status.PASS,
					"Successfully click on Close button in Popup");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickTransferBtnPopup", Status.FAIL,
					"Failed In Method to click on Cloose button in Popup" + e.getMessage());
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifyallFieldsEditSalesAdmin
	 * @Description : This function is used to Verify payment record is editable for
	 *              Phone, email, carrier fields on the payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	// 5E8JYX67A2VFT
	public void verifyallFieldsEditSalesAdmin() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String orderIDStr = "5E8JYX67A2VFT";
		String guestCostStr = "40";
		String refund = "10";
		String nonRefunable = "30";
		try {
			WebElement ele = driver.findElement(editBtn);
			clickElementJSWithWait(ele);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(orderID, orderIDStr);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(totalGuestCost, guestCostStr);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(refundableAmt, refund);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(nonRefund, nonRefunable);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement editSaveBtnEle = driver.findElement(editSaveBtn);
			clickElementJSWithWait(editSaveBtnEle);
			// Thread.sleep(1000);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "verifyallFieldsEditSalesAdmin", Status.PASS,
					"Successfully Can edit All Fields");

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "verifyallFieldsEditSalesAdmin", Status.FAIL,
					"Failed in Method: Can't edit field Phone, EMail, Carrier");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifyIntRecordEditCorpUser
	 * @Description : This function is used to Verify payment record is editable for
	 *              Phone, email, carrier fields on the payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void verifyIntRecordEditCorpUser() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String email = "testmail@wyn.uat.com";
		String number = "1987654321";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			pageDown(); /************ Ritam ********/
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement editEmailIconEle = driver.findElement(editEmailIcon);
			clickElementJSWithWait(editEmailIconEle);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * Robot rob = new Robot(); rob.mouseWheel(4);
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 */

			fieldDataEnter(inputEmail, email);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(inputPhone, number);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// need code to choose the dropdown---chek this line to change Carrier
			// clickElementJSWithWait(driver.findElement(carrierDrop));
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement editSaveBtnEle = driver.findElement(editSaveBtn);
			clickElementJSWithWait(editSaveBtnEle);
			// Thread.sleep(1000);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement wbel = driver.findElement(emailTxt);
			String emailValue = wbel.getText();
			System.out.println("Email after edit: " + emailValue);
			if (emailValue.equalsIgnoreCase(email)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordEditCorpUser", Status.PASS,
						"Successfully Can edit field Phone, EMail, Carrier");

			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordEditCorpUser", Status.FAIL,
						"Can't edit field Phone, EMail, Carrier");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordEditCorpUser", Status.FAIL,
					"Failed in Method: Can't edit field Phone, EMail, Carrier");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifyIntRecordNonEditCorpUser
	 * @Description : This function is used to Verify payment record is editable for
	 *              Phone, email, carrier fields on the payment record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void verifyIntRecordNonEditCorpUser() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		try {
			WebElement editIconEle = driver.findElement(editIcon);
			clickElementJSWithWait(editIconEle);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(orderInput, "test");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement editSaveBtnEle = driver.findElement(editSaveBtn);
			clickElementJSWithWait(editSaveBtnEle);
			Thread.sleep(1000);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(errorPopMsg))) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordNonEditCorpUser", Status.PASS,
						"Can't edit field");
				WebElement errorCancelEle = driver.findElement(errorCancel);
				clickElementJSWithWait(errorCancelEle);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordNonEditCorpUser", Status.FAIL,
						"Can edit any field");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordNonEditCorpUser", Status.FAIL,
					"Failed in Method: Can edit any field");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : clickToAddManualRecord
	 * @Description : This function is used to navigate to Payment Tab & add manual
	 *              record
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void clickToAddManualRecord() {
		String orderIDStr = "TestOrder";
		String guestCostStr = "20";
		String refund = "5";
		String nonRefunable = "15";
		try {
			waitUntilObjectVisible(driver, paymentLink, 120);

			// waitUntilElementInVisible(driver, incentivesTab, 120);
			clickElementJSWithWait(paymentLink);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(newManualBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(nextBtn);
			// check if the new paymnet page is opening to add details
			// if(verifyElementDisplayed(driver.findElement(newPayLabel))){

			tcConfig.updateTestReporter("TourRecordsPage", "clickToAddManualRecord", Status.PASS,
					"Successfully navigated to New payment Details page for Manual record addition");
			/*
			 * } else{ tcConfig.updateTestReporter("TourRecordsPage",
			 * "clickToAddManualRecord", Status.FAIL,
			 * "Failed to navigate to New payment Details page for Manual record addition");
			 * }
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// add details in the form
			fieldDataEnter(orderID, orderIDStr);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement transactionDrpDwnEle = driver.findElement(transactionDrpDwn);
			clickElementJSWithWait(transactionDrpDwnEle);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement optionDepEle = driver.findElement(optionDep);
			clickElementJSWithWait(optionDepEle);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(totalGuestCost, guestCostStr);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(refundableAmt, refund);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(nonRefund, nonRefunable);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Robot rob = new Robot();
			rob.mouseWheel(8);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement paymentDrpDwnEle = driver.findElement(paymentDrpDwn);
			clickElementJSWithWait(paymentDrpDwnEle);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement optionCashEle = driver.findElement(optionCash);
			clickElementJSWithWait(optionCashEle);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement savePopEle = driver.findElement(savePop);
			clickElementJSWithWait(savePopEle);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(paymentID))) {

				tcConfig.updateTestReporter("TourRecordsPage", "clickToAddManualRecord", Status.PASS,
						"Successfully Created a Manual Payment");
			}

			else {
				tcConfig.updateTestReporter("TourRecordsPage", "clickToAddManualRecord", Status.FAIL,
						"Failed Created a Manual Payment");
			}

		} catch (Exception e) {
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "clickToAddManualRecord", Status.FAIL,
					"Failed In Method" + e.getMessage());

		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : navigateToPaymentRecord
	 * @Description : This function is used to navigate to naviagte to Manual Record
	 *              created
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void navigateToPaymentRecord() {

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(paymentID);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "navigateToPaymentRecord", Status.PASS,
					"Clicked on Payment ID successfully");

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(paymentIDLbl))) {

				tcConfig.updateTestReporter("TourRecordsPage", "navigateToPaymentRecord", Status.PASS,
						"Successfully navigated to Payment Record Details");
			}

			else {
				tcConfig.updateTestReporter("TourRecordsPage", "navigateToPaymentRecord", Status.FAIL,
						"Failed to navigated to Payment Record Details");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void verifyIntRecordEditManualSales() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String email = "testmail@wyn.uat.com";

		try {
			pageDown();

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement editEmailIconEle = driver.findElement(editEmailIcon);
			clickElementJSWithWait(editEmailIconEle);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			fieldDataEnter(inputEmail, email);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			WebElement editSaveBtnEle = driver.findElement(editSaveBtn);
			clickElementJSWithWait(editSaveBtnEle);
			// Thread.sleep(1000);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WebElement wbel = driver.findElement(emailTxt);
			String emailValue = wbel.getText();
			System.out.println("Email after edit: " + emailValue);
			if (emailValue.equalsIgnoreCase(email)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordEditCorpUser", Status.PASS,
						"Successfully modified record");

			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordEditCorpUser", Status.FAIL,
						"Can't edit field Phone, EMail, Carrier");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "verifyIntRecordEditCorpUser", Status.FAIL,
					"Failed in Method: Can't edit field Phone, EMail, Carrier");
		}
	}

	public void verifyLastModified() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			WebElement wbel = driver.findElement(lastModTxt);
			if (verifyElementDisplayed(wbel)) {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyLastModified", Status.PASS,
						"Successfully displaying last modfied data");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "verifyLastModified", Status.FAIL,
						"Failed in displaying last modfied data");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("TourRecordsPage", "verifyLastModified", Status.FAIL, "Failed in Method");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : verifySFMC()
	 * @Description : This function is used to verify presence of SFMC button in
	 *              Tour Details
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void verifySFMC() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (verifyElementDisplayed(driver.findElement(sfmcButton))) {
			tcConfig.updateTestReporter("TourPage", "verifySFMC", Status.PASS,
					"Verified successfully Button name displayed as Guest Confirmation (SFMC)");
		} else {
			tcConfig.updateTestReporter("TourPage", "verifySFMC", Status.FAIL,
					"Failed to verify Button name displayed as Guest Confirmation (SFMC)");
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Journey SFMC Regression.
	 * @Function Name : bookAppointmentMini
	 * @Description : This function is used to book appointment especially for
	 *              inivac Tour
	 * @Author : Prattusha DUtta
	 ***********************************************************************
	 */

	public void bookAppointmentMini() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilObjectVisible(driver, bookOptionSelect, 120);
		waitUntilElementVisibleBy(driver, bookOptionSelect, 120);
		clickElementJSWithWait(bookOptionSelect);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("NoSalesStore").equalsIgnoreCase("Y")) {
			if (verifyObjectDisplayed(removeSalesStoreIcon)) {
				clickElementJSWithWait(removeSalesStoreIcon);
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
						"User is able to remove Sales store is blank");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.FAIL,
						"User is not able to remove Sales store is blank");

			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * clickElementBy(removeSalesStoreIcon);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */
			try {
				if (getAppointmentsBtn.isEnabled() == false || getAppointmentsBtn.getAttribute("disabled") == "true") {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointmentMini", Status.PASS,
							"User is unable to book appointment if Sales store is blank");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointmentMinit", Status.FAIL,
							"User is able to book appointment if Sales store is blank");
				}
			} catch (NoSuchElementException e) {
				System.out.println("catched");
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is able to book appointment if Sales store is blank");
			}
		} else {

			// if(testData.get("SecSalesStore").equalsIgnoreCase("Y")){
			if (verifyObjectDisplayed(removeSalesStoreIcon)) {
				clickElementJSWithWait(removeSalesStoreIcon);
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.PASS,
						"User is able to remove Sales store is blank");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "removeSalesStore", Status.FAIL,
						"User is not able to remove Sales store is blank");

			}

			try {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, reSalesStoretxt, 120);
				fieldDataEnter(reSalesStoretxt, testData.get("SalesStore"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]"))
						.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				e.getMessage();
			}
			// Change Location
			try {
				clickElementJSWithWait(removeAppointmentLocIcon);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(reBookLocation, testData.get("AppointmentLoc"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"))
						.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} catch (Exception e) {
				e.getMessage();
			}

			// }

			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			// testData.get("Brand").equalsIgnoreCase("Y"))
			try {
				waitUntilElementVisibleBy(driver, drpBrand, 120);
				// System.out.println(testData.get("Brand"));
				new Select(driver.findElement(drpBrand)).selectByVisibleText(testData.get("Brand"));
				// new Select(driver.findElement(drpBrand)).selectByIndex(2);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {
				e.getMessage();
			}

			//
			new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			//
			//
			new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
				new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// new
				// Select(driver.findElement(drpMarketingcamp)).selectByIndex(1);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			waitUntilElementVisible(driver, getAppointmentsBtn, 120);
			mouseoverAndClick(getAppointmentsBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
			clickElementJS(lblAvailableSlot);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, yesBtn, 120);
			clickElementJS(yesBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, okBtn, 120);
			clickElementJS(okBtn);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointmentMinit", Status.PASS,
					"BookAppoitment is succesfully");
		}

	}

	/*
	 * Method-search for Tour ID Designed By-
	 */

	protected By globalSearchEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'default')]");
	protected By selectTourId = By.xpath("(//span[@class='uiImage']/img[contains(@class,'icon')])[1]");

	public void searchForTourId() {

		waitUntilElementVisibleBy(driver, globalSearchEdit, 120);
		if (TourId == null) {
			fieldDataEnter(globalSearchEdit, testData.get("TourNo"));
		} else {
			fieldDataEnter(globalSearchEdit, TourId);
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(selectTourId);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (TourId == null) {
			WebElement lblTourConfirm = driver.findElement(By.xpath(
					"//span[@title='Tour Record Number']/../..//div[@class='slds-form-element__control']//span[contains(text(),'"
							+ testData.get("TourNo") + "')]"));

			if (verifyElementDisplayed(lblTourConfirm)) {
				tcConfig.updateTestReporter("TourRecordsPage", "searchForTourId", Status.PASS,
						"Tour details is displayed after searching for Tour ID : " + testData.get("TourNo"));
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "searchForTourId", Status.FAIL,
						"Unable to find the Tour details after searching for Tour ID : " + TourId);
			}
		} else {
			WebElement lblTourConfirm = driver.findElement(By.xpath(
					"//span[@title='Tour Record Number']/../..//div[@class='slds-form-element__control']//span[contains(text(),'"
							+ testData.get("TourNo") + "')]"));
			if (verifyElementDisplayed(lblTourConfirm)) {
				tcConfig.updateTestReporter("TourRecordsPage", "searchForTourId", Status.PASS,
						"Tour details is displayed after searching for Tour ID : " + TourId);
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "searchForTourId", Status.FAIL,
						"Unable to find the Tour details after searching for Tour ID : " + TourId);
			}
		}

	}

	/*
	 * Method-Navigate to Payemnt page Designed By-JYoti
	 */
	public void navigateToPayments() {

		waitUntilElementVisibleBy(driver, lnkPayments, 120);
		clickElementBy(lnkPayments);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, lnkPaymentRecords, 120);
		clickElementBy(lnkPaymentRecords);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> tblPayments = driver.findElements(lstPaymentId);
		System.out.println(tblPayments.size());
		tblPayments.get(0).click();

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, detailsTab, 120);
		if (verifyObjectDisplayed(btnManualMatch)) {
			clickElementJS(btnManualMatch);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtCloverId, 120);
			String cloverId = getRandomString(10);
			driver.findElement(txtCloverId).sendKeys(cloverId);
			// clickElementJS(btnEnter);
			clickElementJSWithWait(btnEnter);
			waitUntilElementVisibleBy(driver, errorMsg, 120);
			if (verifyObjectDisplayed(errorMsg)) {
				String text = driver.findElement(errorMsg).getText();
				tcConfig.updateTestReporter("TourRecordsPage", "navigateToPayments", Status.PASS,
						"User should get the following message : " + text);
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "navigateToPayments", Status.FAIL,
						"Failed while getting message");
			}
			clickElementJSWithWait(btnCancel);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "navigateToPayments", Status.FAIL,
					"Failed : Button not found");
		}
	}

	/*
	 * Method-Verify Credit Band is present Designed By-JYoti
	 */
	public void verifyCreditBandPresent() {
		waitUntilElementVisibleBy(driver, txtTourNum, 120);
		((JavascriptExecutor) driver).executeScript("scroll(0,300);");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtCreditBand, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(txtCreditBand)) {
			String str = driver.findElement(txtCreditBand).getText();
			tcConfig.updateTestReporter("TourRecordsPage", "verifyCreditBandPresent", Status.PASS,
					"Band added is:- " + str);
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyCreditBandPresent", Status.FAIL,
					"Failed while getting message");
		}
	}

}
