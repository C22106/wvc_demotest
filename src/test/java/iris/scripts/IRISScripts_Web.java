package iris.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import iris.pages.IRISAccounts;
import iris.pages.IRISBookReservation;
import iris.pages.IRISContractPage;
import iris.pages.IRISLoginPage;
import iris.pages.IRISSearchAvailability;
import iris.pages.IRISSelectBookReservation;
import iris.pages.IRISSelectMember;
import iris.pages.WMTCPage;

public class IRISScripts_Web extends TestBase {
	public static final Logger log = Logger.getLogger(IRISScripts_Web.class);

	/*
	 * Method: validateMemberDetails Description: Validate user is displayed
	 * with proper member details in Account profile screen Date: Sep/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "AccountProfilePage", "iris", "batch1" })

	public void validateMemberDetails(Map<String, String> testData) throws Exception {
		String memberType = testData.get("MemberType");
		setupTestData(testData);
		IRISLoginPage loginpage = new IRISLoginPage(tcconfig);
		IRISSelectMember selectMemberPage = new IRISSelectMember(tcconfig);
		IRISAccounts accountPage = new IRISAccounts(tcconfig);
		IRISContractPage contractPage = new IRISContractPage(tcconfig);
		try {

			loginpage.launchAndLogInApplication();
			selectMemberPage.selectMember();
			accountPage.navigationToSearchOwnerPage();
			accountPage.searchAccount();
			accountPage.validateGrandfatheredHKPresence();
			accountPage.validateTierAndTSGranfathered();
			accountPage.selectActiveContract();
			contractPage.validateTravelShareDetails();
			loginpage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: reservationCreationAndValidationInWMTC_NARegion Description: User
	 * should be able to make a reservation booking in IRIS and validate the
	 * same in WMTC Date: Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "AccountProfilePage", "iris", "batch1" })

	public void reservationCreationAndValidationInWMTC_NARegion(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		IRISLoginPage loginpage = new IRISLoginPage(tcconfig);
		IRISSelectMember selectMemberPage = new IRISSelectMember(tcconfig);
		IRISAccounts accountPage = new IRISAccounts(tcconfig);
		IRISSelectBookReservation selectBookReservationPage = new IRISSelectBookReservation(tcconfig);
		IRISSearchAvailability searchAvailabilityPage = new IRISSearchAvailability(tcconfig);
		IRISBookReservation bookReservation = new IRISBookReservation(tcconfig);
		WMTCPage wmtcPage = new WMTCPage(tcconfig);
		try {

			loginpage.launchAndLogInApplication();
			selectMemberPage.selectMember();
			accountPage.navigationToSearchOwnerPage();
			accountPage.searchAccount();
			// selectBookReservationPage.selectBookReservation();
			/*
			 * searchAvailabilityPage.searchAvailability();
			 * bookReservation.bookReservation_NARegion();
			 */
			loginpage.logoutApplication();

			wmtcPage.launchAndLogInApplication();
			// wmtcPage.validateReservationInWMTC();
			wmtcPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

}