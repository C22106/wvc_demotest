package iris.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class IRISSelectMember extends IRISBasePage {

	public static final Logger log = Logger.getLogger(IRISLoginPage.class);

	public IRISSelectMember(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By members = By.xpath("//a[contains(.,'Members')]");

	
	
	/*
	 * Method: selectMember Description: Select Member Date: Sep/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	public void selectMember() throws Exception {
		waitUntilObjectClickable(driver, members, "ExplicitLongWait");
		clickElementJSWithWait(members);
		
		switchToDefaultContent();
		tcConfig.updateTestReporter("IRISSelectMember", "selectMember", Status.PASS, "Clicked on Members Option");
	}
}
