package iris.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class IRISSelectBookReservation extends IRISBasePage {

	public static final Logger log = Logger.getLogger(IRISLoginPage.class);

	public IRISSelectBookReservation(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//a[contains(text(),'Book Reservation')]")
	WebElement bookReservation;
	@FindBy(xpath = "//INPUT[@name='txtT1Region_Nm']")
	WebElement regionnm;
	@FindBy(xpath = "//table[@class='RecSrcScalarToolbar']//a[contains(@name,'AccountProfilesaveAll')]")
	WebElement saveBtn;
	protected By locateFrame = By.tagName("frame");

	/*
	 * Method: selectBookReservation Description: Select Book Reservation link
	 * Date: Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void selectBookReservation() throws Exception {
		int framecount = 0;

		switchToDefaultContent();
		framecount = getList(locateFrame).size();
		if (framecount > 1) {
			switchToFrame("Display");
			switchToFrame("Results");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "searchAccount", Status.FAIL, "Frame Not Found");
		}

		mouseoverAndClick(saveBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		acceptAlert();

		switchToDefaultContent();
		framecount = getList(locateFrame).size();
		if (framecount > 1) {
			switchToFrame("Display");
			switchToFrame("MenuList");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "searchAccount", Status.FAIL, "Frame Not Found");
		}
		
		clickElementJSWithWait(bookReservation);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		switchToDefaultContent();
		framecount = getList(locateFrame).size();
		if (framecount > 1) {
			switchToFrame("Display");
			switchToFrame("Results");
		} else {
			tcConfig.updateTestReporter("IRISSelectBookReservation", "selectBookReservation", Status.FAIL,
					"Frame Not Found");
		}
		Assert.assertTrue(verifyObjectDisplayed(regionnm), "User unable to navigate to Book Reservation Page");
		tcConfig.updateTestReporter("IRISSelectBookReservation", "selectBookReservation", Status.PASS,
				"User navigated to Book Reservation Page Successfully");

	}
}
