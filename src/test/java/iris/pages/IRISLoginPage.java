package iris.pages;

import java.io.UnsupportedEncodingException;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class IRISLoginPage extends IRISBasePage {

	public static final Logger log = Logger.getLogger(IRISLoginPage.class);

	public IRISLoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//INPUT[@name='login']")
	WebElement txtUserName;
	@FindBy(xpath = "//INPUT[@name='password']")
	WebElement txtPassword;
	@FindBy(xpath = "//SELECT[@name='OrgName']")
	WebElement region;
	@FindBy(xpath = "//INPUT[@name='vslogin']")
	WebElement loginButton;
	@FindBy(xpath = "//a[contains(.,'Logout')]")
	WebElement logoutButton;
	@FindBy(xpath = "//select[@name='txtT1Log_Code_ID']")
	WebElement logcode;
	@FindBy(xpath = "//textarea[@name='txtT1Note_DESC']")
	WebElement notes;
	@FindBy(xpath = "//a[@name='T1QRY_CreateAccountAccessLog2saveAll']")
	WebElement save;
	public By logoutReason = By.xpath("//a[contains(text(),'Just Looking')]");
	public By logoIris = By.xpath("//img[contains(@src, 'logo')]");
	public By members = By.xpath("//a[contains(.,'Members')]");
	public By locateFrame = By.tagName("frame");

	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void launchAndLogInApplication() throws UnsupportedEncodingException{
		getBrowserName(browserName);
		navigateToURL(testData.get("URL"));

		txtUserName.sendKeys(testData.get("IRIS_Username"));
		byte[] decodedString = DatatypeConverter.parseBase64Binary(testData.get("IRIS_Password"));
		String dString = new String(decodedString, "UTF-8");
		txtPassword.sendKeys(dString);

		new Select(region).selectByVisibleText(testData.get("Organization"));
		loginButton.click();

		switchToFrame("Startup");
		waitUntilElementVisibleBy(driver, logoIris, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(logoIris), "User unable to login to IRIS");
		tcConfig.updateTestReporter("IRISLoginPage", "launchApplication", Status.PASS, "User logged in to IRIS Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: logoutApplication Description: logout the application Date:
	 * Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void logoutApplication() throws Exception {
		try {
			switchToDefaultContent();
			switchToFrame("Startup");
			clickElementJSWithWait(logoutButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			switchToDefaultContent();
			int framecount = getList(locateFrame).size();
			if (framecount > 1) {
				switchToFrame("Display");
				switchToFrame("Results");
			} 
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(logoutReason);
			waitUntilElementVisibleBy(driver, txtUserName, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(txtUserName), "User unable to log out of IRIS");
			tcConfig.updateTestReporter("IRISLoginPage", "logoutApplication", Status.PASS, "User logged out Successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {

			tcConfig.updateTestReporter("IRISLoginPage", "logoutApplication", Status.FAIL, "Log Out Failed " + e.getMessage());

		}

	}

}
