package iris.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class IRISContractPage extends IRISBasePage {

	public static final Logger log = Logger.getLogger(IRISLoginPage.class);
	public IRISContractPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By TravelShareEligibleCredits = By
			.xpath("//td[contains(text(),'TravelShare Eligible Credits')]/following-sibling::td");
	protected By TravelShareGrandfatheredEligibleCredits = By
			.xpath("//td[contains(text(),'TravelShare Grandfathered Eligible Credits')]/following-sibling::td");
	protected By TravelShareGrandfathered = By
			.xpath("//td[contains(text(),'TravelShare Grandfathered?')]/following-sibling::td");

	/*
	 * Method: selectMember Description: Select Member Date: Sep/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	public void validateTravelShareDetails() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (getElementText(TravelShareGrandfathered).trim().equalsIgnoreCase(IRISAccounts.TSGrandfatheredValue)) {
			tcConfig.updateTestReporter("IRISContractPage", "validateTravelShareDetails", Status.PASS,
					"TravelShare Grandfathered flag value is correctly displayed on View Contract Information");
		} else {
			tcConfig.updateTestReporter("IRISContractPage", "validateTravelShareDetails", Status.FAIL,
					"TravelShare Grandfathered flag value is incorrectly displayed on View Contract Information");
		}

		if (Integer.parseInt(getElementText(TravelShareEligibleCredits).trim()) == (IRISAccounts.creditValue)) {
			tcConfig.updateTestReporter("IRISContractPage", "validateTravelShareDetails", Status.PASS,
					"TravelShare Eligible Credits value is correctly displayed on View Contract Information");
		} else {
			tcConfig.updateTestReporter("IRISContractPage", "validateTravelShareDetails", Status.FAIL,
					"TravelShare Eligible Credits value is incorrectly displayed on View Contract Information");
		}

		if (verifyObjectDisplayed(TravelShareGrandfatheredEligibleCredits)) {
			tcConfig.updateTestReporter("IRISContractPage", "validateTravelShareDetails", Status.PASS,
					"TravelShare Grandfathered Eligible Credits value is correctly displayed on View Contract Information");
		} else {
			tcConfig.updateTestReporter("IRISContractPage", "validateTravelShareDetails", Status.FAIL,
					"TravelShare Grandfathered Eligible Credits value is incorrectly displayed on View Contract Information");
		}

	}
}
