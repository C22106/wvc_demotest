package iris.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class IRISAccounts extends IRISBasePage {

	public static final Logger log = Logger.getLogger(IRISLoginPage.class);

	public IRISAccounts(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//a[@name='T1QRY_AccountSearchquery']")
	WebElement locate;
	protected By locateFrame = By.tagName("frame");
	protected By bookReservation = By.xpath("//a[contains(.,'Book Reservation')]");
	protected By labelGrandfatheredHK = By.xpath("//td[contains(text(),'Grandfathered HK')]");
	protected By tierName = By.xpath("//td[contains(text(),'Tier Name')]/following-sibling::td");
	protected By WMTSAnnualCredits = By.xpath("//td[contains(text(),'WMTS Annual Credits')]/following-sibling::td");
	protected By TSGrandfathered = By.xpath("//td[contains(text(),'TS Grandfathered')]/following-sibling::td");
	protected By contractTab = By.xpath("//a[contains(@name,'hrefTabT10QRY_AcctContract') and @class='menuBarText']");
	protected By activeContract = By
			.xpath("//table[contains(@name,'AcctContract')]//td[contains(text(),'Active')]/preceding-sibling::td/a");
	protected By contractPageTitle = By.xpath("//vstitle[contains(text(),'View Contract Information')]");
	protected By accounts = By.xpath("//a[contains(.,'Accounts')]");
	protected By accountEdit = By.xpath("//INPUT[@name='txtT1Account_Num']");
	protected By TravelShareGrandfathered = By
			.xpath("//td[contains(text(),'TravelShare Grandfathered?')]/following-sibling::td");
	protected static String calculatedTierName;
	protected static String TSGrandfatheredValue;
	protected static int creditValue;

	/*
	 * Method: navigationToSearchOwnerPage Description: Navigation To Search
	 * Owner Page Date: Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void navigationToSearchOwnerPage() {

		int framecountbefore = getList(locateFrame).size();
		if (framecountbefore > 1) {
			switchToFrame("Display");
			switchToFrame("MenuList");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "navigationToSearchOwnerPage", Status.FAIL,
					"Frame Not Found");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(accounts);

		switchToDefaultContent();
		int framecountafter = getList(locateFrame).size();
		if (framecountafter > 1) {
			switchToFrame("Display");
			switchToFrame("Results");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "navigationToSearchOwnerPage", Status.FAIL,
					"Frame Not Found");
		}
		Assert.assertTrue(verifyObjectDisplayed(accountEdit), "User unable to navigate to Search Owner Page");
		tcConfig.updateTestReporter("IRISLoginPage", "navigationToSearchOwnerPage", Status.PASS,
				"User navigated to Search Owner Page Successfully");
	}

	/*
	 * Method: searchAccount Description: Search Account Number Date: Sep/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */
	public void searchAccount() throws Exception {
		String accountnumber = testData.get("AccountEdit");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		senkKeysJS(accountEdit, accountnumber);

		clickElementJSWithWait(locate);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(By.xpath("(//a[contains(.,'" + accountnumber + "')])[1]"));

		acceptAlert();
		tcConfig.updateTestReporter("IRISAccounts", "searchAccount", Status.PASS, "Account Number Selected");

		switchToDefaultContent();
		int framecount = getList(locateFrame).size();
		if (framecount > 1) {
			switchToFrame("Display");
			switchToFrame("MenuList");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "searchAccount", Status.FAIL, "Frame Not Found");
		}
		Assert.assertTrue(verifyObjectDisplayed(bookReservation), "User unable to navigate to Account Profile Page");
		tcConfig.updateTestReporter("IRISAccounts", "searchAccount", Status.PASS,
				"User navigated to Account Profile Page Successfully");
	}

	/*
	 * Method: validateGrandfatheredHK Description: validate Grandfathered HK
	 * label Date: Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void validateGrandfatheredHKPresence() {
		switchToDefaultContent();
		int framecount = getList(locateFrame).size();

		if (framecount > 1) {
			switchToFrame("Display");
			switchToFrame("Results");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "searchAccount", Status.FAIL, "Frame Not Found");
		}

		if (verifyObjectDisplayed(labelGrandfatheredHK)) {
			tcConfig.updateTestReporter("IRISAccounts", "validateGrandfatheredHKPresence", Status.PASS,
					"Grandfathered HK label present on Account Profile Page ");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "validateGrandfatheredHKPresence", Status.FAIL,
					"Grandfathered HK label not present on Account Profile Page ");
		}

	}

	/*
	 * Method: validateTierName Description: validate Tier Name Date: Sep/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */
	public void validateTierAndTSGranfathered() {
		String member = testData.get("MemberType").trim();
		Assert.assertTrue(verifyObjectDisplayed(WMTSAnnualCredits), "Annaul Credit not dispalyed");
		creditValue = Integer.parseInt(getElementText(WMTSAnnualCredits).trim());
		tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.PASS,
				"WMTS Annual Credits label is displayed and the value is " + getElementText(WMTSAnnualCredits).trim());

		if (member.equalsIgnoreCase("Existing")) {
			tierAndGranfatheredValidation_ExistingMember();
		} else if (member.equalsIgnoreCase("New")) {
			tierAndGranfatheredValidation_NewMember();
		}
	}

	/*
	 * Method: tierAndGranfatheredValidation_ExistingMember Description:
	 * validate Tier Name and TS Grandfathered value for Existing Member Date:
	 * Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void tierAndGranfatheredValidation_ExistingMember() {

		if (getElementText(tierName).trim().equalsIgnoreCase(getTierName_OldCreditValue(creditValue))) {
			tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.PASS,
					"Existing member have the same tier name as before. Tier Name: " + getElementText(tierName).trim());

			calculatedTierName = getElementText(tierName).trim();
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.FAIL,
					"Existing member do not have the same tier name as before");
		}

		if (getTierName_OldCreditValue(creditValue).equalsIgnoreCase(getTierName_NewCreditValue(creditValue))) {
			if (getElementText(TSGrandfathered).trim().equalsIgnoreCase("No")) {
				tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.PASS,
						"Existing member have the TS Grandfathered flag value as No");
				TSGrandfatheredValue = "No";
			} else {
				tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.FAIL,
						"TS Grandfathered flag value is incorrectly for existing member");
			}

		} else {
			if (getElementText(TSGrandfathered).trim().equalsIgnoreCase("Yes")) {
				tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.PASS,
						"Existing member have the TS Grandfathered flag value as Yes");
				TSGrandfatheredValue = "Yes";
			} else {
				tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.FAIL,
						"TS Grandfathered flag value is incorrectly for existing member");
			}
		}

	}

	/*
	 * Method: tierAndGranfatheredValidation_NewMember Description: validate
	 * Tier Name and TS Grandfathered value for New Member Date: Sep/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */
	public void tierAndGranfatheredValidation_NewMember() {
		if (getElementText(tierName).trim().equalsIgnoreCase(getTierName_NewCreditValue(creditValue))) {
			tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.PASS,
					"Tier name is correctly displayed for New member" + getElementText(tierName).trim());

			calculatedTierName = getElementText(tierName).trim();

			if (getElementText(TSGrandfathered).trim().equalsIgnoreCase("No")) {
				tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.PASS,
						"New memeber have the TS Grandfathered flag value as No");
				TSGrandfatheredValue = "No";
			} else {
				tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.FAIL,
						"TS Grandfathered flag value is incorrectly for New member");
			}

		} else {
			tcConfig.updateTestReporter("IRISAccounts", "validateTierName", Status.FAIL,
					"Tier name is incorrectly displayed for New member");
		}

	}

	/*
	 * Method: getTierName_OldCreditValue Description: Calculation of Tier Name
	 * based on the old Credit value Date: Sep/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */
	public String getTierName_OldCreditValue(int value) {
		if (value >= 4000 && value <= 19999) {
			return "TS Elite Standard";
		} else if (value >= 20000 && value <= 34999) {
			return "TS Elite";
		} else if (value >= 35000 && value <= 62999) {
			return "TS Elite Diamond";
		} else if (value >= 63000) {
			return "TS Elite Platinum";
		}
		return "Incorrect Value";
	}

	/*
	 * Method: getTierName_NewCreditValue Description: Calculation of Tier Name
	 * based on the new Credit value Date: Sep/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */
	public String getTierName_NewCreditValue(int value) {
		if (value >= 4000 && value <= 29999) {
			return "TS Standard";
		} else if (value >= 30000 && value <= 44999) {
			return "TS Elite";
		} else if (value >= 45000 && value <= 64999) {
			return "TS Elite Diamond";
		} else if (value >= 65000) {
			return "TS Elite Platinum";
		}
		return "Incorrect Value";
	}

	/*
	 * Method: selectActiveContract Description: Select Active Contract Date:
	 * Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void selectActiveContract() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(contractTab);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(activeContract);

		switchToDefaultContent();
		int framecount = getList(locateFrame).size();
		if (framecount > 1) {
			switchToFrame("Display");
			switchToFrame("Results");
		} else {
			tcConfig.updateTestReporter("IRISAccounts", "selectActiveContract", Status.FAIL, "Frame Not Found");
		}
		Assert.assertTrue(verifyObjectDisplayed(TravelShareGrandfathered),
				"User unable to navigate to View Contract Information Page");
		tcConfig.updateTestReporter("IRISAccounts", "selectActiveContract", Status.PASS,
				"User navigated to View Contract Information Page Successfully");
	}
}
