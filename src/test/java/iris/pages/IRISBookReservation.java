package iris.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class IRISBookReservation extends IRISBasePage {

	public static final Logger log = Logger.getLogger(IRISBookReservation.class);

	public IRISBookReservation(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//INPUT[@name='txtT1CheckOut_DT']")
	WebElement checkoutdate;
	@FindBy(xpath = "//INPUT[@name='txtT1Checkin_DT']")
	WebElement checkindate;
	@FindBy(xpath = "//a[@name='btnT8pSearch_Availability']")
	WebElement cntnu;
	@FindBy(xpath = "//a[contains(.,'Auto Pay')]")
	WebElement autoPay;
	@FindBy(xpath = "//a[contains(.,'Generate Charges')]")
	WebElement generateCharge;
	@FindBy(xpath = "//a[contains(.,'New Credit Card Payment')]")
	WebElement newCreditpay;
	@FindBy(xpath = "//SELECT[@name='txtT1CC_Type_ID']")
	WebElement selectID;
	@FindBy(xpath = "//input[@name='txtT1CC_NUM']")
	WebElement ccnumber;
	@FindBy(xpath = "//input[@name='txtT1CC_Expiration_DT']")
	WebElement ccexp;
	@FindBy(xpath = "//input[@name='txtT1CC_Name']")
	WebElement ccname;
	@FindBy(xpath = "//input[@name='txtT1CC_Addr_1']")
	WebElement ccaddress;
	@FindBy(xpath = "//input[@name='txtT1CC_City']")
	WebElement cccity;
	@FindBy(xpath = "//SELECT[@name='txtT1CC_State_ID']")
	WebElement stateID;
	@FindBy(xpath = "//input[@name='txtT1CC_Postal_Code']")
	WebElement cczipcode;
	@FindBy(xpath = "//SELECT[@name='txtT1CC_Country_ID']")
	WebElement countryID;
	@FindBy(xpath = "//input[@name='txtT1CC_Phone']")
	WebElement ccphone;
	@FindBy(xpath = "//a[contains(.,'OK')]")
	WebElement ok;
	@FindBy(xpath = "//a[contains(.,'Confirm reservation')]")
	WebElement confirmReservation;
	@FindBy(xpath = "//td[@class='AttrCaption'][text()='Confirmation #']/..//td[@class='AttrField']")
	WebElement confirmation;
	@FindBy(xpath = "(//input[@vsdf='Credit_AMT'])[1]")
	WebElement creditAmt;
	@FindBy(xpath = "//select[@name='txtT1Deliver_Method_CODE']")
	WebElement deliverMethodDrpDwn;
	@FindBy(xpath = "//select[@name='txtT1Use_Code']")
	WebElement useCodeDrpDwn;
	@FindBy(xpath = "//a[contains(.,'Primary Owner')]")
	WebElement primaryOwner;
	@FindBy(xpath = "//input[@name='txtT4Email_Address']")
	WebElement email;
	@FindBy(xpath = "//a[text()='Max Credits']/../../../tr[2]/td[4]")
	WebElement maxCreditValue_FirstRow;
	@FindBy(xpath = "//a[@name='T1QRY_ReservationsaveAll']")
	WebElement saveReservation;
	protected By comment = By.xpath("//textarea[contains(@name,'Comments')]");
	protected By locateFrame = By.tagName("frame");
	protected static String confirmationNumber;

	/*
	 * Method: bookReservation_NARegion Description: Book Reservation in NA
	 * Region Date: Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void bookReservation_NARegion() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		switchToDefaultContent();
		int framecountbefore = getList(locateFrame).size();
		if (framecountbefore > 1) {
			switchToFrame("Display");
			switchToFrame("Results");
		} else {
			tcConfig.updateTestReporter("IRISBookReservation", "bookReservation_NARegion", Status.FAIL,
					"Frame Not Found");
		}

		senkKeysJS(checkoutdate, testData.get("CheckDate"));
		clickElementJSWithWait(cntnu);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJSWithWait(autoPay);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJSWithWait(generateCharge);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		acceptAlert();
		senkKeysJS(comment, "test");

		if (testData.get("FullPointBook").equalsIgnoreCase("No")) {
			String winHandleBefore = driver.getWindowHandle();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			newCreditpay.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			try {
				selectByText(selectID, testData.get("CCType"));
				senkKeysJS(ccnumber, testData.get("CCNumber"));
				senkKeysJS(ccexp, testData.get("CCExp"));
				senkKeysJS(ccname, testData.get("CCName"));
				senkKeysJS(ccaddress, testData.get("CCAddress"));
				senkKeysJS(cccity, testData.get("CCCity"));
				selectByText(stateID, testData.get("State_ID"));
				senkKeysJS(cczipcode, testData.get("CCZipCode"));
				selectByText(countryID, testData.get("Country_ID"));
				senkKeysJS(ccphone, testData.get("CCPhone"));
				tcConfig.updateTestReporter("IRISBookReservation", "bookReservation_NARegion", Status.PASS,
						"Payment details entered successfully");

			} catch (Exception e) {
				tcConfig.updateTestReporter("IRISBookReservation", "bookReservation_NARegion", Status.FAIL,
						"Error in entering payment details");
			}

			clickElementJSWithWait(ok);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			driver.switchTo().window(winHandleBefore);

			switchToDefaultContent();
			int framecountafter = getList(locateFrame).size();
			if (framecountafter > 1) {
				switchToFrame("Display");
				switchToFrame("Results");
			} else {
				tcConfig.updateTestReporter("IRISBookReservation", "bookReservation_NARegion", Status.FAIL,
						"Frame Not Found");
			}

			String winHandleAfter = driver.getWindowHandle();
			clickElementJSWithWait(confirmReservation);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}

			selectByText(useCodeDrpDwn, testData.get("UseCode"));
			selectByText(deliverMethodDrpDwn, testData.get("DeliveryMethod"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(primaryOwner);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(email);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			senkKeysJS(email, testData.get("Email"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			email.sendKeys(Keys.TAB);

			clickElementJSWithWait(ok);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().window(winHandleAfter);

			int framecountconfrmpage = getList(locateFrame).size();
			if (framecountconfrmpage > 1) {
				switchToFrame("Display");
				switchToFrame("Results");
			} else {
				tcConfig.updateTestReporter("IRISBookReservation", "bookReservation_NARegion", Status.FAIL,
						"Frame Not Found");
			}

			confirmationNumber = confirmation.getText();
			tcConfig.updateTestReporter("IRISBookReservation", "bookReservation_NARegion", Status.PASS,
					"Booking Successfully Created. Confirmation Number : " + confirmationNumber);
		}

	}
}
