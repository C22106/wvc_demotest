package iris.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class IRISSearchAvailability extends IRISBasePage {

	public static final Logger log = Logger.getLogger(IRISSearchAvailability.class);

	public IRISSearchAvailability(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//INPUT[@name='txtT1Region_Nm']")
	WebElement regionnm;
	@FindBy(xpath = "//INPUT[@name='txtT1Date_Id']")
	WebElement arrdate;
	@FindBy(xpath = "//INPUT[@name='txtT1Num_Nights_Stay']")
	WebElement nights;
	@FindBy(xpath = "//a[@name='T1QRY_PVT_RR_ALLOC_USAGE_OUTR_BRquery']")
	WebElement locate;
	@FindBy(xpath = "//a[contains(.,'OK')]")
	WebElement ok;
	protected By optn = By.xpath("(//a[contains(.,'Reserve')])[2]");
	protected By locateFrame = By.tagName("frame");

	/*
	 * Method: searchAvailability Description: Search Availability Date:
	 * Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void searchAvailability() throws Exception {
		String region = testData.get("Region");
		String arrivalDate = testData.get("ArrDate");
		String night = testData.get("Nights");

		senkKeysJS(regionnm, region);
		senkKeysJS(arrdate, arrivalDate);
		senkKeysJS(nights, night);

		clickElementJSWithWait(locate);
		String winHandleBefore = driver.getWindowHandle();

		waitUntilObjectClickable(driver, optn, "ExplicitLongWait");
		clickElementJSWithWait(optn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		if (verifyObjectDisplayed(ok)) {
			log.info("The page title is " + driver.getTitle());
		}
		waitUntilObjectClickable(driver, ok, 60);
		clickElementJSWithWait(ok);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.switchTo().window(winHandleBefore);
		switchToDefaultContent();
		tcConfig.updateTestReporter("IRISSearchAvailability", "searchAvailability", Status.PASS,
				"Availability Returned Successfully");

	}

}