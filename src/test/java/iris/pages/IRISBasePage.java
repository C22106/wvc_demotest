package iris.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class IRISBasePage extends FunctionalComponents {

	public IRISBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	/*
	 * Function to wait until the specified element is visible
	 * 
	 * @param by The {@link WebDriver} locator used to identify the element
	 * 
	 * @param timeOutInSeconds The wait timeout in seconds
	 */
	public void waitUntilElementVisible(WebDriver driver, WebElement locator, String timeOutInSeconds) {
		try {
			String strValue = "";
			strValue = tcConfig.getConfig().get(timeOutInSeconds);
			Long timeOut = Long.parseLong(strValue);
			(new WebDriverWait(driver, timeOut)).until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception e) {
			log.info("Loading Element: " + locator);
		}

	}

	/**
	 * Check If any extra tabs Present
	 * 
	 * @param applicationURl
	 */
	public void navigateToAppURL(String applicationURl) {
		driver.navigate().to(applicationURl);
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info("No Alert Present");
		}
	}

	/*
	 * *************************Method: switchToFrame ***
	 * *************Description: Switch to Frame by index **
	 */

	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
		} catch (Exception e) {
			log.error("Could Not switch to frame");
		}
	}

	/*
	 * *************************Method: switchToFrame ***
	 * *************Description: Switch to Frame by Frame Name **
	 */

	public void switchToFrame(String frameName) {
		try {
			driver.switchTo().frame(frameName);
		} catch (Exception e) {
			log.error("Could Not switch to frame");
		}
	}

	/*
	 * *************************Method: switchToDefaultContent ***
	 * *************Description: Switch to Default Content **
	 */

	public void switchToDefaultContent() {
		try {
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			log.error("Could Not switch to Default Content");
		}
	}

	public void senkKeysJS(By by, String value) {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[1].value = arguments[0];", value, getObject(by));

		} catch (Exception e) {
			tcConfig.updateTestReporter("Functional Component", "senkKeysJS", Status.FAIL,
					"Could Not Click the object");
		}
	}

	/*
	 * *************************Method: senkKeysJS *** *************Description:
	 * Send Keys with Java Script**
	 */
	public void senkKeysJS(WebElement wbel, String value) {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[1].value = arguments[0];", value, wbel);

		} catch (Exception e) {
			tcConfig.updateTestReporter("Functional Component", "senkKeysJS", Status.FAIL,
					"Could Not Click the object");
		}
	}

	/*
	 * *************************Method: selectByText ***
	 * *************Description: Drop Down Select By Text**
	 */
	public void selectByText(By by, String strData) {
		try {
			new Select(getObject(by)).selectByVisibleText(strData);
		} catch (Exception e) {
			tcConfig.updateTestReporter("Drp Down Select", "selectByText", Status.FAIL,
					"The Drop Down Text is not present");
		}
	}

	public void selectByText(WebElement we, String strData) {
		try {
			new Select(we).selectByVisibleText(strData);
		} catch (Exception e) {
			tcConfig.updateTestReporter("Drp Down Select", "selectByText", Status.FAIL,
					"The Drop Down Text is not present");
		}
	}

	/*
	 * *************************Method: getBrowserName ***
	 * *************Description: Get Browser name**
	 */
	public void getBrowserName(String strBrowser) {
		try {
			tcConfig.updateTestReporterWithoutScreenshot("Functional components", "BROWSER NAME", Status.PASS,
					strBrowser);
		} catch (Exception e) {
			tcConfig.updateTestReporter("Get Browser", "getBrowserName", Status.FAIL, "Could Not get the browser");
		}
	}

	/*
	 * *************************Method: waitUntilElementVisibleBy ***
	 * *************Description: Wait Until Element Visible**
	 */

	public void waitUntilElementVisibleBy(WebDriver driver, WebElement wb, String timeOutInSeconds) {
		boolean flag = false;
		String strValue = tcConfig.getConfig().get(timeOutInSeconds);

		Long timeOut = Long.parseLong(strValue);
		for (long i = 0; i < timeOut / 10; i++) {
			try {

				if (wb.isDisplayed()) {
					flag = true;
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					break;
				}

			} catch (Exception e) {
				log.info("Could Not Get the Element ");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

		}
		if (!flag) {

			try {
				driver.switchTo().alert();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {
				log.info("Element Found");
			}
		}

	}
	
	public void waitUntilObjectClickable(WebDriver driver, By by, String timeOutInSeconds) {
		String strValue = tcConfig.getConfig().get(timeOutInSeconds);
		Long timeOut = Long.parseLong(strValue);
		(new WebDriverWait(driver, timeOut)).until(ExpectedConditions.elementToBeClickable(by));
	}

}
