package iris.pages;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class WMTCPage extends IRISBasePage {

	public static final Logger log = Logger.getLogger(WMTCPage.class);

	public WMTCPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(id = "username")
	WebElement txtUserName;
	@FindBy(id = "password")
	WebElement txtPassword;
	@FindBy(xpath = "//input[@name='submit']")
	WebElement loginbutton;
	@FindBy(xpath = "//a[contains(.,'Logout')]")
	WebElement logoutbutton;
	public By headerAccountPage = By.xpath("//h1[contains(text(),'Account Page')]");
	public By headerViewModifyCancelReservationsPage = By.xpath("//h1[contains(text(),'Reservations')]");
	public By headerReservationDetailsPage = By.xpath("//h1[contains(text(),'Reservation Details')]");
	public By viewModifyCancelReservationsLink = By.xpath("//a[contains(.,'Reservations')]");
	public By searchTxtBx = By.xpath("//div[@id='example_filter']/input");
	public By reservationSelectedRow = By.xpath("//tr[@class='row_selected odd']/td/a");
	public By accountNumber = By.xpath("//td/strong[contains(text(),'Account')]/../following-sibling::td");
	public By logoutLink = By.xpath("//a[text()='Log Out']");
	protected By numberOfNights = By.xpath("//td/strong[contains(.,'Number of Night')]/../following-sibling::td");
	public By splashClose = By.id("splashClose");

	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void launchAndLogInApplication() throws Exception{
		getBrowserName(browserName);
		navigateToURL(testData.get("WMTCUrl"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(splashClose)) {
			clickElementJSWithWait(splashClose);
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		senkKeysJS(txtUserName, testData.get("WMTCUserName"));
		byte[] decodedString = DatatypeConverter.parseBase64Binary(testData.get("WMTC_Password"));
		String dString = new String(decodedString, "UTF-8");
		senkKeysJS(txtPassword, dString);
		loginbutton.click();

		waitUntilElementVisibleBy(driver, headerAccountPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerAccountPage), "User unable to login to WMTC Application");
		tcConfig.updateTestReporter("WMTCPage", "launchApplication", Status.PASS,
				"User logged in to WMTC Successfully");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: validateReservationInWMTC Description: validate Reservation is
	 * flowing from IRIS to WMTC Date: Sep/2020 Author: Kamalesh Gupta Changes
	 * By: NA
	 */
	public void validateReservationInWMTC() {
		clickElementBy(viewModifyCancelReservationsLink);
		waitUntilElementVisibleBy(driver, headerViewModifyCancelReservationsPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerViewModifyCancelReservationsPage),
				"User unable to navigate to View Modify Cancel Reservations Page");
		tcConfig.updateTestReporter("WMTCPage", "validateReservationInWMTC", Status.PASS,
				"User navigated to View Modify or Cancel Reservations Page Successfully");

		Assert.assertTrue(
				verifyObjectDisplayed(By.xpath("//a[contains(.,'" + IRISBookReservation.confirmationNumber + "')]")),
				"Reservations created in IRIS not flowing to WMTC");
		tcConfig.updateTestReporter("WMTCPage", "validateReservationInWMTC", Status.PASS,
				"Reservations created in IRIS flowing to WMTC Successfully");

		clickElementBy(By.xpath("//a[contains(.,'" + IRISBookReservation.confirmationNumber + "')]"));
		waitUntilElementVisibleBy(driver, headerReservationDetailsPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerReservationDetailsPage),
				"User unable to navigate to Reservations Details Page");
		tcConfig.updateTestReporter("WMTCPage", "validateReservationInWMTC", Status.PASS,
				"User navigated to Reservations Details Page Successfully");

		Assert.assertTrue(getElementText(accountNumber).trim().contains(testData.get("WMTCUserName")),
				"Account Number not matching on Reservations Details Page");
		tcConfig.updateTestReporter("WMTCPage", "validateReservationInWMTC", Status.PASS,
				"Account number validated on Reservations Details Page Successfully");

		Assert.assertTrue(getElementText(numberOfNights).trim().contains(testData.get("Nights")),
				"Number of Nights not matching on Reservations Details Page");
		tcConfig.updateTestReporter("WMTCPage", "validateReservationInWMTC", Status.PASS,
				"Number of Nights validated on Reservations Details Page Successfully");

	}

	/*
	 * Method: logoutApplication Description: logout the application Date:
	 * Sep/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void logoutApplication() throws Exception {
		clickElementBy(logoutLink);
		waitUntilElementVisibleBy(driver, txtUserName, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(txtUserName), "User unable to logout");
		tcConfig.updateTestReporter("WMTCPage", "logoutApplication", Status.PASS, "User log out Successfully");
	}

}
