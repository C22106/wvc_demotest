package eWorksheet.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class InformationPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(InformationPage.class);

	public InformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By tabCount = By.xpath("//div[@class='prospect-btn-row']/button");
	protected By primaryTab = By.xpath("//div[@class='prospect-btn-row']/button[@class='btn prospect enabled primary']");

	protected By firstName = By.name("firstName");
	protected By lastName = By.name("lastName");
	protected By dateOfBirth = By.name("dateOfBirth");
	protected By bankTypeSelect = By.name("bank");
	protected By mothersMdnNameInput = By.name("mothersMaidenName");
	protected By customerMdnNameInput = By.name("customersMaidenName");
	protected By ssnNumber = By.name("ssn");
	protected By citizenShipSelect = By.name("citizenship");
	protected By occupationSelect = By.name("occupation");
	protected By employerName = By.name("SourceofIncome");
	protected By EmployerName=By.xpath("//div[@class='demo-information']//label[text()='Source of Income']//../select");
	protected By annualIncomeInput = By.name("annualIncome");
	protected By individualIncomInput = By.name("individualIncome");
	protected By homePhone = By.name("homePhone");
	protected By workPhone = By.name("workPhone");
	protected By primaryEmail = By.name("primaryEmail");
	protected By addressLine1 = By.name("addressLine1");
	protected By countrySelect = By.name("country");
	protected By cityInput = By.name("city");
	protected By stateSelect = By.name("state");
	protected By postalCode = By.name("postalCode");
	protected By residenceTypeSelect = By.name("residenceType");
	protected By yearsAtAddress = By.name("yearsAtAddress");
	protected By monthsAtAddress = By.name("monthsAtAddress");
	protected By PopMsg = By.xpath("//div[@id='completed-but-modal']//p");
	protected By ErrorPopMsg = By.xpath("//div[@id='completed-but-modal']//p");

	protected By addCus = By.xpath("//button[@class='btn add']");
	protected By firstNameSecondCus = By.xpath("//div[@data-primary='false']//input[@name='firstName']");
	protected By lastNameSecondCus = By.xpath("//div[@data-primary='false']//input[@name='lastName']");
	protected By dateOfBirthSecondCus = By.xpath("//div[@data-primary='false']//input[@name='dateOfBirth']");
	protected By bankTypeSelectSecondCus = By.xpath("//div[@data-primary='false']//select[@name='bank']");
	protected By mothersMdnNameSecondCus = By.xpath("//div[@data-primary='false']//input[@name='mothersMaidenName']");
	protected By customerMdnNameSecondCus = By.xpath("//div[@data-primary='false']//input[@name='customersMaidenName']");
	protected By ssnNumberSecondCus = By.xpath("//div[@data-primary='false']//input[@name='ssn']");
	protected By citizenShipSelectSecondCus = By.xpath("//div[@data-primary='false']//select[@name='citizenship']");
	protected By occupationSelectSecondCus = By.xpath("//div[@data-primary='false']//select[@name='occupation']");
	protected By employerSecondCus = By.xpath("//div[@data-primary='false']//input[@name='employer']");
	protected By annualIncomeSecondCus = By.xpath("//div[@data-primary='false']//input[@name='annualIncome']");
	protected By individualIncomeSecondCus = By.xpath("//div[@data-primary='false']//input[@name='individualIncome']");
	protected By homePhoneSecondCus = By.xpath("//div[@data-primary='false']//input[@name='homePhone']");
	protected By workPhoneSecondCus = By.xpath("//div[@data-primary='false']//input[@name='workPhone']");
	protected By emailSecondCus = By.xpath("//div[@data-primary='false']//input[@name='primaryEmail']");
	protected By addressLine1SecondCus = By.xpath("//div[@data-primary='false']//input[@name='addressLine1']");
	protected By countrySelectSecondCus = By.xpath("//div[@data-primary='false']//select[@name='country']");
	protected By citySecondCus = By.xpath("//div[@data-primary='false']//input[@name='city']");
	protected By stateSelectSecondCus = By.xpath("//div[@data-primary='false']//select[@name='state']");
	protected By postalCodeSecondCus = By.xpath("//div[@data-primary='false']//input[@name='postalCode']");
	protected By residenceTypeSelectSecondCus = By.xpath("//div[@data-primary='false']//select[@name='residenceType']");
	protected By yearsAtAddressSecondCus = By.xpath("//div[@data-primary='false']//input[@name='yearsAtAddress']");
	protected By monthsAtAddressSecondCus = By.xpath("//div[@data-primary='false']//input[@name='monthsAtAddress']");

	protected By firstNameThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='firstName'])[2]");
	protected By lastNameThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='lastName'])[2]");
	protected By dateOfBirthThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='dateOfBirth'])[2]");
	protected By bankTypeSelectThirdCus = By.xpath("(//div[@data-primary='false']//select[@name='bank'])[2]");
	protected By mothersMdnNameThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='mothersMaidenName'])[2]");
	protected By customerMdnNameThirdCus = By
			.xpath("(//div[@data-primary='false']//input[@name='customersMaidenName'])[2]");
	protected By ssnNumberThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='ssn'])[2]");
	protected By citizenShipSelectThirdCus = By.xpath("(//div[@data-primary='false']//select[@name='citizenship'])[2]");
	protected By occupationSelectThirdCus = By.xpath("(//div[@data-primary='false']//select[@name='occupation'])[2]");
	protected By employerThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='employer'])[2]");
	protected By annualIncomeThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='annualIncome'])[2]");
	protected By individualIncomeThirdCus = By
			.xpath("(//div[@data-primary='false']//input[@name='individualIncome'])[2]");
	protected By homePhoneThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='homePhone'])[2]");
	protected By workPhoneThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='workPhone'])[2]");
	protected By emailThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='primaryEmail'])[2]");
	protected By addressLine1ThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='addressLine1'])[2]");
	protected By countrySelectThirdCus = By.xpath("(//div[@data-primary='false']//select[@name='country'])[2]");
	protected By cityThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='city'])[2]");
	protected By stateSelectThirdCus = By.xpath("(//div[@data-primary='false']//select[@name='state'])[2]");
	protected By postalCodeThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='postalCode'])[2]");
	protected By residenceTypeSelectThirdCus = By
			.xpath("(//div[@data-primary='false']//select[@name='residenceType'])[2]");
	protected By yearsAtAddressThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='yearsAtAddress'])[2]");
	protected By monthsAtAddressThirdCus = By.xpath("(//div[@data-primary='false']//input[@name='monthsAtAddress'])[2]");

	protected By continueBtn = By.xpath("//button[@class='btn btn-continue-completed' and text()='CONTINUE']  ");
//	or //button[@class='btn btn--inv next_btn float-right' and text()='Next']
	protected By NxtBtn = By.xpath("//button[@class='btn btn-continue-completed' and text()='NEXT']  ");
	protected By nextBtnPop = By.xpath("//button[@class='btn btn-continue-completed']");
	protected By cancellBtnPop = By.xpath("//button[@class='btn btn-cancel-completed-but']");

	protected By withholdBtn = By.xpath("//button[@class='btn btn--inv withhold float-right']");
	protected By withHoldCnfrm = By.xpath("//button[@class='btn btn-continue-withhold']");
	protected By nextBtn = By.xpath("//button[@class='btn btn--inv next_btn float-right']");
	protected By PlusIscon = By.xpath("//button[@class='btn btn-cancel-completed-add']");
	protected By withholdContinue = By
			.xpath("//button[@class='btn btn-continue-completed-continue' and text()='CONTINUE']");
	protected By withholdNext = By.xpath("//button[@class='btn btn-continue-completed-continue']");
	protected By financingPage = By.xpath("//h1[text()='Apply for Financing Options: ']");
	protected By popupmsg = By.xpath("//div[@class='modal']//div[@class='modal__inner']//div/p");
	protected By emptyStatusInfo = By.xpath("//button[@class='btn prospect enabled primary']");
	protected By completeStatusInfo = By.xpath("//button[@class='btn prospect primary disabled completed']");

	public void formFillUpFirstCustomer() {

		waitUntilElementVisibleBy(driver, primaryTab, 120);
		if (verifyObjectDisplayed(primaryTab)) {

			tcConfig.updateTestReporter("InformationPage", "fillupFormForPrimaryCustomer", Status.PASS,
					"Primary Customer Information Page Displayed");

			/*String firstCusfirstName = testData.get("FirstCusFirstName");
			String firstCuslastName = testData.get("FirstCusLastName");*/
			String firstCusDOB = testData.get("FirstCusDOB");
			/*String bankType = testData.get("BankType");*/
			String mothersMdnName = testData.get("MothersMdnName");
			/*String customerMdnName = testData.get("CustMdnName");*/
			String firstCusSSNumber = testData.get("FirstCusSSN");
			String citizenship = testData.get("Citizenship");
			String occupation = testData.get("Occupation");
			String employer = testData.get("Employer");
			String AnnualIncome = testData.get("AnnualIncome");
			String individualIn = testData.get("IndividualIncome");
			String firstCushomePhone = testData.get("FirstCusHomePhone");
			//String firstCusEmail = testData.get("FirstCusEmail");
			/*String firstCusAddress = testData.get("FirstCusAddress");
			String country = testData.get("Country");
			String state = testData.get("FirstCusStateCode");
			String city = testData.get("FirstCusCity");
			String firstCusPostalCode = testData.get("FirstCusPostalCode");*/
			String residencyType = testData.get("ResidenceType");
			String YearsAtAddress = testData.get("YearsAtRes");
			String MonthAtAddress = testData.get("MonthsAtRes");

			/*fieldDataEnter(firstName, firstCusfirstName);
			fieldDataEnter(lastName, firstCuslastName);*/
			driver.findElement(dateOfBirth).sendKeys(firstCusDOB);
			/*selectByText(bankTypeSelect, bankType);*/
			fieldDataEnter(mothersMdnNameInput, mothersMdnName);
			/*fieldDataEnter(customerMdnNameInput, customerMdnName);*/
			fieldDataEnter(ssnNumber, firstCusSSNumber);
			selectByText(citizenShipSelect, citizenship);
			selectByText(occupationSelect, occupation);
			selectByText(EmployerName, employer);
			fieldDataEnter(annualIncomeInput, AnnualIncome);
			fieldDataEnter(individualIncomInput, individualIn);
			pageDown();
			fieldDataEnter(homePhone, firstCushomePhone);
			/*fieldDataEnter(workPhone, firstCushomePhone);*/
			//fieldDataEnter(primaryEmail, firstCusEmail);
			/*fieldDataEnter(addressLine1, firstCusAddress);
			selectByText(countrySelect, country);
			selectByText(stateSelect, state);
			fieldDataEnter(cityInput, city);
			fieldDataEnter(postalCode, firstCusPostalCode);*/
			selectByText(residenceTypeSelect, residencyType);
			fieldDataEnter(yearsAtAddress, YearsAtAddress);
			fieldDataEnter(monthsAtAddress, MonthAtAddress);

			tcConfig.updateTestReporter("InformationPage", "fillupFormForPrimaryCustomer", Status.PASS,
					"All Information for the Customer are provided");

		} else {
			tcConfig.updateTestReporter("InformationPagep", "fillupFormForPrimaryCustomer", Status.FAIL,
					"Getting some error while proding Primary Customer information");

		}
	}

	public void formFillUpSecondCustomer() {

		if (verifyObjectDisplayed(firstNameSecondCus)) {
			tcConfig.updateTestReporter("InformationPage", "fillupFormFoSecondCusCustomer", Status.PASS,
					"SecondCus Customer Information Page Displayed");
			String SecondCusFirstName = testData.get("SecondCusFirstName");
			String SecondCusLastName = testData.get("SecondCusLastName");
			String SecondCusDob = testData.get("SecondCusDOB");
			String BankType = testData.get("BankType");
			String MothersMdnName = testData.get("MothersMdnName");
			String CustomerMdnName = testData.get("CustMdnName");
			String SecondCusSSN = testData.get("SecondCusSSN");
			String Citizenship = testData.get("Citizenship");
			String Occupation = testData.get("Occupation");
			String Employer = testData.get("Employer");
			String AnnualIncome = testData.get("AnnualIncome");
			String IndividualIn = testData.get("IndividualIncome");

			String SecondCusHomePhone = testData.get("SecondCusHomePhone");
			String SecondCusEmail = testData.get("SecondCusEmail");
			String SecondCusAddress = testData.get("SecondCusAddress");
			String Country = testData.get("Country");
			String SecondCusState = testData.get("SecondCusStateCode");
			String SecondCusCity = testData.get("SecondCusCity");
			String SecondCusPostalCode = testData.get("SecondCusPostalCode");
			String ResidencyType = testData.get("ResidenceType");
			String YearsAtAddress = testData.get("YearsAtRes");
			String MonthAtAddress = testData.get("MonthsAtRes");

			fieldDataEnter(firstNameSecondCus, SecondCusFirstName);
			fieldDataEnter(lastNameSecondCus, SecondCusLastName);
			driver.findElement(dateOfBirthSecondCus).sendKeys(SecondCusDob);
			selectByText(bankTypeSelectSecondCus, BankType);
			fieldDataEnter(mothersMdnNameSecondCus, MothersMdnName);
			fieldDataEnter(customerMdnNameSecondCus, CustomerMdnName);
			fieldDataEnter(ssnNumberSecondCus, SecondCusSSN);
			selectByText(citizenShipSelectSecondCus, Citizenship);
			selectByText(occupationSelectSecondCus, Occupation);
			selectByText(EmployerName, Employer);
			fieldDataEnter(annualIncomeSecondCus, AnnualIncome);
			fieldDataEnter(individualIncomeSecondCus, IndividualIn);
			pageDown();
			fieldDataEnter(homePhoneSecondCus, SecondCusHomePhone);
			fieldDataEnter(workPhoneSecondCus, SecondCusHomePhone);
			fieldDataEnter(emailSecondCus, SecondCusEmail);
			fieldDataEnter(addressLine1SecondCus, SecondCusAddress);
			selectByText(countrySelectSecondCus, Country);
			selectByText(stateSelectSecondCus, SecondCusState);
			fieldDataEnter(citySecondCus, SecondCusCity);
			fieldDataEnter(postalCodeSecondCus, SecondCusPostalCode);
			selectByText(residenceTypeSelectSecondCus, ResidencyType);
			fieldDataEnter(yearsAtAddressSecondCus, YearsAtAddress);
			fieldDataEnter(monthsAtAddressSecondCus, MonthAtAddress);

			tcConfig.updateTestReporter("InformationFillUp", "dataFillUp", Status.PASS,
					"All Information for secondary Customer are provided");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("InformationFillUp", "dataFillUp", Status.FAIL,
					"getting issue while providing information for  secondary Customer");
		}

	}

	public void formFillUpThirdCustomer() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(firstNameThirdCus)) {
			tcConfig.updateTestReporter("InformationPage", "fillupFormFoSecondaryCustomer", Status.PASS,
					"Secondary Customer Information Page Displayed");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String ThirdCusFirstName = testData.get("ThirdCusFirstName");
			String ThirdCusLastName = testData.get("ThirdCusLastName");
			String ThirdCusDob = testData.get("ThirdCusDOB");
			String BankType = testData.get("BankType");
			String MothersMdnName = testData.get("MothersMdnName");
			String CustomerMdnName = testData.get("CustMdnName");
			String ThirdCusSSN = testData.get("ThirdCusSSN");
			String Citizenship = testData.get("Citizenship");
			String Occupation = testData.get("Occupation");
			String Employer = testData.get("Employer");
			String AnnualIncome = testData.get("AnnualIncome");
			String IndividualIn = testData.get("IndividualIncome");

			String ThirdCusHomePhone = testData.get("ThirdCusHomePhone");
			String ThirdCusEmail = testData.get("ThirdCusEmail");
			String ThirdCusAddress = testData.get("ThirdCusAddress");
			String Country = testData.get("Country");
			String ThirdCusState = testData.get("ThirdCusStateCode");
			String ThirdCusCity = testData.get("ThirdCusCity");
			String ThirdCusPostalCode = testData.get("ThirdCusPostalCode");
			String ResidencyType = testData.get("ResidenceType");
			String YearsAtAddress = testData.get("YearsAtRes");
			String MonthAtAddress = testData.get("MonthsAtRes");

			fieldDataEnter(firstNameThirdCus, ThirdCusFirstName);
			fieldDataEnter(lastNameThirdCus, ThirdCusLastName);
			driver.findElement(dateOfBirthThirdCus).sendKeys(ThirdCusDob);
			selectByText(bankTypeSelectThirdCus, BankType);
			fieldDataEnter(mothersMdnNameThirdCus, MothersMdnName);
			fieldDataEnter(customerMdnNameThirdCus, CustomerMdnName);
			fieldDataEnter(ssnNumberThirdCus, ThirdCusSSN);
			selectByText(citizenShipSelectThirdCus, Citizenship);
			selectByText(occupationSelectThirdCus, Occupation);
			selectByText(EmployerName, Employer);
			fieldDataEnter(annualIncomeThirdCus, AnnualIncome);
			fieldDataEnter(individualIncomeThirdCus, IndividualIn);
			pageDown();
			fieldDataEnter(homePhoneThirdCus, ThirdCusHomePhone);
			fieldDataEnter(workPhoneThirdCus, ThirdCusHomePhone);
			fieldDataEnter(emailThirdCus, ThirdCusEmail);
			fieldDataEnter(addressLine1ThirdCus, ThirdCusAddress);
			selectByText(countrySelectThirdCus, Country);
			selectByText(stateSelectThirdCus, ThirdCusState);
			fieldDataEnter(cityThirdCus, ThirdCusCity);
			fieldDataEnter(postalCodeThirdCus, ThirdCusPostalCode);
			selectByText(residenceTypeSelectThirdCus, ResidencyType);
			fieldDataEnter(yearsAtAddressThirdCus, YearsAtAddress);
			fieldDataEnter(monthsAtAddressThirdCus, MonthAtAddress);

			tcConfig.updateTestReporter("InformationFillUp", "dataFillUp", Status.PASS,
					"All Information for secondary Customer are provided");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("InformationFillUp", "dataFillUp", Status.FAIL,
					"getting issue while providing information for  secondary Customer");
		}

	}

	public void navigateToCardSelectionPgae() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("InformationPage", "navigateToCardSelectionPgae", Status.PASS,
				"user navigated to card selection page");
		hoverOnElement(driver.findElement(nextBtn));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();
//		if (verifyObjectDisplayed(continueBtn) )
			if (verifyObjectDisplayed(NxtBtn) ){
//			clickElementBy(continueBtn);
				clickElementBy(NxtBtn);
				
			
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinnerEWS();
		} else {
			clickElementBy(nextBtnPop);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinnerEWS();
		}

	}

	public void withholdFisrtCustomer() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		pageUp();

		String firstCustName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");
		WebElement FirstCusTab = driver
				.findElement(By.xpath("//button[@type='button' and contains(text(),'" + firstCustName + "')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(FirstCusTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		checkLoadingSpinnerEWS();
		clickElementBy(withholdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(withHoldCnfrm)) {
			tcConfig.updateTestReporter("InformationPage", "withholdSecondCustomer", Status.PASS,
					"Withhold confirmation popup Page Displayed");
			clickElementBy(withHoldCnfrm);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("InformationPage", "withholdSecondCustomer", Status.FAIL,
					"Withhold confirmation popup Page is not Displayed");
		}

		checkLoadingSpinnerEWS();
		if (verifyObjectDisplayed(withholdContinue)) {
			clickElementBy(withholdContinue);
			checkLoadingSpinnerEWS();
		} else {
			clickElementBy(withholdNext);
			checkLoadingSpinnerEWS();
		}


	}

	public void switchToFirstCustomer() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		String FirstCustName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");
		WebElement CustTab = driver
				.findElement(By.xpath("//button[@type='button' and contains(text(),'" + FirstCustName + "')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(CustTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		checkLoadingSpinnerEWS();
	}

	public void switchToSecondCustomer() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		pageUp();

		String SecondCustName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");
		WebElement SecCustTab = driver
				.findElement(By.xpath("//button[@type='button' and contains(text(),'" + SecondCustName + "')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(SecCustTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		checkLoadingSpinnerEWS();

	}

	public void switchToThirdCustomer() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		pageUp();
		System.out.println("code here1");
		String ThirdCustName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");
		System.out.println(ThirdCustName);
		WebElement thirdCustTab = driver
				.findElement(By.xpath("//button[@type='button' and contains(text(),'" + ThirdCustName + "')]"));
		System.out.println("code here2");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(thirdCustTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		checkLoadingSpinnerEWS();

	}
	public void switchToFourthCustomer() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		pageUp();

		String FourthCustName = testData.get("FourthCusFirstName") + " " + testData.get("FourthCusLastName");
		WebElement fourthCustTab = driver
				.findElement(By.xpath("//button[@type='button' and contains(text(),'" + FourthCustName + "')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(fourthCustTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		checkLoadingSpinnerEWS();

	}

	public void addcustomer() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		pageUp();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(addCus);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void withholdSecondCustomer() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String PrimaryCustName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");
		String SecondaryCustName = testData.get("SecondaryFirstName") + " " + testData.get("SecondCusLastName");
		WebElement PrimaryCustTab = driver
				.findElement(By.xpath("//button[@type='button' and contains(text(),'" + PrimaryCustName + "')]"));
		WebElement SecCustTab = driver
				.findElement(By.xpath("//button[@type='button' and contains(text(),'" + SecondaryCustName + "')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(PrimaryCustTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		mouseoverAndClick(SecCustTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(withholdBtn);

		if (verifyObjectDisplayed(withHoldCnfrm)) {
			tcConfig.updateTestReporter("InformationPage", "withholdSecondCustomer", Status.PASS,
					"Withhold confirmation popup Page Displayed");
			clickElementBy(withHoldCnfrm);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("InformationPage", "withholdSecondCustomer", Status.FAIL,
					"Withhold confirmation popup Page is not Displayed");
		}

		clickElementBy(withholdContinue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// WebElement withholdConfirmation =
		// driver.findElement(By.xpath("//div[@class='buttonContainer']//button[text()='"+
		// PrimaryCustName +"']"));
		// if (verifyElementDisplayed(withholdConfirmation)) {
		// tcConfig.updateTestReporter("InformationPage",
		// "withholdSecondCustomer", Status.PASS,
		// "Withhold confirmation popup Page Displayed");
		// clickElementWb(withholdConfirmation);
		// } else{
		// tcConfig.updateTestReporter("InformationPage",
		// "withholdSecondCustomer", Status.FAIL,
		// "Withhold confirmation popup Page is not Displayed");
		// }

	}

	public void withholdCustomer() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		clickElementBy(withholdBtn);

		if (verifyObjectDisplayed(withHoldCnfrm)) {
			tcConfig.updateTestReporter("InformationPage", "withholdSecondCustomer", Status.PASS,
					"Withhold confirmation popup Page Displayed");
			clickElementBy(withHoldCnfrm);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("InformationPage", "withholdSecondCustomer", Status.FAIL,
					"Withhold confirmation popup Page is not Displayed");
		}

		clickElementBy(withholdContinue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		

	}

	public void verifyWitholdPopUp() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(withholdBtn);

		if (verifyObjectDisplayed(withHoldCnfrm)) {
			tcConfig.updateTestReporter("InformationPage", "verifyWitholdPopUp", Status.PASS,
					"Withhold confirmation popup Page Displayed");
			clickElementBy(withHoldCnfrm);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("InformationPage", "verifyWitholdPopUp", Status.FAIL,
					"Withhold confirmation popup Page is not Displayed");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(withholdContinue)) {
			String popMsg = driver.findElement(By.xpath("//div[@class='modal']//div[@class='modal__inner']//div/p"))
					.getText();
			tcConfig.updateTestReporter("InformationPage", "verifyWitholdPopUp", Status.PASS,
					"SUBMISSION SUCCESSFULLY COMPLETED popup Page Displayed with message: " + popMsg);
			clickElementBy(withholdContinue);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("InformationPage", "verifyWitholdPopUp", Status.FAIL,
					"Withhold confirmation popup Page is not Displayed");
		}

		if (verifyObjectDisplayed(financingPage)) {
			tcConfig.updateTestReporter("InformationPage", "verifyWitholdPopUp", Status.PASS,
					"user navigated to Apply for Financing Options: ");
		} else {
			tcConfig.updateTestReporter("InformationPage", "verifyWitholdPopUp", Status.FAIL,
					"user navigated to Apply for Financing Options:  is not Displayed");
		}

	}

	public void VerifynforSubmissionPopup() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String SecondaryCustName = testData.get("SecondaryFirstName") + " " + testData.get("SecondCusLastName");
		if (verifyObjectDisplayed(PopMsg)) {

			String Message = driver.findElement(PopMsg).getText();
			tcConfig.updateTestReporter("InformationPage", "VerifynfoSubmissionPopup", Status.PASS,
					" popup Message Displayed " + Message);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//div[@class='buttonContainer']//button[text()='" + SecondaryCustName + "']"))
					.click();
		} else {
			tcConfig.updateTestReporter("InformationPage", "VerifynfoSubmissionPopup", Status.FAIL,
					"Popup Message is not Displayed");
		}

	}

	public void VerifynStatusBox(String status) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (status.equalsIgnoreCase("Before")) {

			if (verifyObjectDisplayed(emptyStatusInfo)) {

				tcConfig.updateTestReporter("InformationPage", "VerifynStatusBox", Status.PASS,
						" Status box is empty ");

			} else {
				tcConfig.updateTestReporter("InformationPage", "VerifynStatusBox", Status.FAIL,
						"Status box is not empty");
			}
		} else if (status.equalsIgnoreCase("After")) {

			if (verifyObjectDisplayed(completeStatusInfo)) {

				tcConfig.updateTestReporter("InformationPage", "VerifynStatusBox", Status.PASS,
						" Status box is checked ");

			} else {
				tcConfig.updateTestReporter("InformationPage", "VerifynStatusBox", Status.FAIL,
						"Status box is not checked");
			}

		}

	}

	public void varigyErrorMessagePopUp() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(popupmsg)) {

			String Message = driver.findElement(popupmsg).getText();
			tcConfig.updateTestReporter("InformationPage", "varigyErrorMessagePopUp", Status.PASS,
					" popup Message Displayed " + Message);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(cancellBtnPop);
		} else {
			tcConfig.updateTestReporter("InformationPage", "varigyErrorMessagePopUp", Status.FAIL,
					"Popup Message is not Displayed");
		}

	}

	// ********************************************************************
	public void informationFillUp() {

		waitUntilElementVisibleBy(driver, primaryTab, 20);
		if (verifyObjectDisplayed(primaryTab)) {

			tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.PASS,
					"Customer Information Page Displayed");
			formFillUpFirstCustomer();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			checkLoadingSpinnerEWS();
			List<WebElement> tabCountList = driver.findElements(tabCount);

			int intTabCount = tabCountList.size();

			if (intTabCount > 2) {
				for (int i = 1; i < intTabCount - 1; i++) {

					clickElementWb(tabCountList.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.PASS,
							"Withholding this Customer");
					clickElementBy(withholdBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					if (verifyObjectDisplayed(withHoldCnfrm)) {
						clickElementBy(withHoldCnfrm);
					} else {
						tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.FAIL,
								"Withhold Error");
					}

				}
				clickElementBy(withholdContinue);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				hoverOnElement(driver.findElement(nextBtn));
				checkLoadingSpinnerEWS();
				if (verifyObjectDisplayed(continueBtn)) {
					clickElementBy(continueBtn);
				} else {
					clickElementBy(nextBtnPop);
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

		} else {
			tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.FAIL,
					"Customer Information Page not displayed");
		}

	}

	public void withHoldoneCustomer() {

		waitUntilElementVisibleBy(driver, primaryTab, 20);
		if (verifyObjectDisplayed(primaryTab)) {

			tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.PASS,
					"Customer Information Page Displayed");
			formFillUpFirstCustomer();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			checkLoadingSpinnerEWS();
			List<WebElement> tabCountList = driver.findElements(tabCount);

			int intTabCount = tabCountList.size();

			if (intTabCount > 2) {
				for (int i = 1; i < intTabCount - 1; i++) {

					clickElementWb(tabCountList.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.PASS,
							"Withholding this Customer");
					clickElementBy(withholdBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					if (verifyObjectDisplayed(withHoldCnfrm)) {
						clickElementBy(withHoldCnfrm);
					} else {
						tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.FAIL,
								"Withhold Error");
					}

				}
				clickElementBy(withholdContinue);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {

				clickElementBy(nextBtn);
				checkLoadingSpinnerEWS();
				clickElementBy(PlusIscon);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				formFillUpSecondCustomer();
				clickElementBy(nextBtn);

			}

		} else {
			tcConfig.updateTestReporter("InformationFillUp", "informationFillUp", Status.FAIL,
					"Customer Information Page not displayed");
		}

	}

}
