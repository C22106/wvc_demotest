package eWorksheet.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class TourBenefitPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(TourBenefitPage.class);

	public TourBenefitPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public String packageVersion = "";
	public static String strStudioTxt;
	public static String strPoint = "";
	protected By OptionLink = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::ul/li[@role='menuitem']/a[contains(text(),'Options ')]");
	protected By proceedButton = By.xpath("//button[contains(text(),'Yes, proceed to options')]");
	protected By benefitTab = By.xpath("//span[contains(text(),'Benefits')]");
	protected By onTourLabel = By.xpath("//h3[text()='On Tour Benefits']");
	protected By availableTourBenefitLabel = By.xpath("//h5[text()='Available On Tour Benefits']");
	protected By availableOnTourList = By.xpath(
			"//h5[text()='Available On Tour Benefits']//following::div[contains(@class,'package-points')]//strong");
	protected By bonusPointChkBox = By.xpath("//label[@for='benefits-checkbox4']");
	protected By matchRadioBtn = By.xpath("//strong[text()='Match']/../div/label[@for='benefits-radio1']");
	protected By updateBtn = By.xpath("//button[text()='Update' and @ type='button']");
	protected By CWPointsTxt = By.xpath("(//td[text()='CW Points']//following-sibling::td)[1]");
	protected By bonusPointsTxt = By.xpath("(//td[text()='Bonus Points']//following-sibling::td)[1]");
	protected By PICPlusChkBox = By.xpath("//strong[text()='PIC Plus']/../..//label[@for='benefits-checkbox2']");
	protected By PICExpressChkBox = By.xpath("//strong[text()='PIC Express']/../..//label[@for='benefits-checkbox5']");
	protected By RCIAnnualRadioBtn = By.xpath("//strong[text()='Annual']/../div/label[@for='first-annual']");
	protected By studioRadioBtn = By
			.xpath("//strong[text()='Studio']/../div/label[@for='picPlus-first-selection-70000']");
	protected By picPlusRoomTxt = By.xpath(
			"(//td[text()='Pic Plus " + testData.get("RoomType").replace(" ", "") + "']//following-sibling::td)[1]");
	protected By picExpressRoomTxt = By.xpath(
			"(//td[text()='Pic Express " + testData.get("RoomType").replace(" ", "") + "']//following-sibling::td)[1]");

	protected By deselectPicPlusTxt = By.xpath("(//td[text()='Pic Plus ']//following-sibling::td)[1]");
	protected By OTBSummaryTable = By.xpath("(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr");
	protected By cancelExitBtn = By.xpath("//button[text()='Cancel/Exit']");
	protected By applyBtn = By.xpath("//button[text()='Apply']");
	protected By benefitCheck = By
			.xpath("(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr[4]/td[text()='PIC Plus "
					+ testData.get("RoomType") + "']");
	protected By benefirRadioBtn = By.xpath("//strong[contains(text(),'" + testData.get("RoomType")
			+ "')]/../div/label[contains(@for,'first-studio')]");
	protected By benefirRadioBtnPICExpress = By
			.xpath("//strong[text()='Studio']/../div/label[contains(@for,'picExpress-first')]");
	protected By cwaPackagePoints = By
			.xpath("//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong");

	public void onTourBenefitDetails() {
		String strPoint = testData.get("TourPoints");
		String strAvlOnTourList[] = testData.get("AvailableOnTourList").split(";");
		int i;
		int strPrePackageVer = Integer.parseInt(CWAPitchBenefitDetailsPage.packageVersion);
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(onTourLabel))) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"On Tour Benefit Label is displayed successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"On Tour Benefit Label is not displayed successfully");
			}

			if (verifyElementDisplayed(driver.findElement(availableTourBenefitLabel))) {
				List<WebElement> allList = driver.findElements(availableOnTourList);
				for (i = 1; i <= allList.size(); i++) {
					String strName = driver.findElement(By
							.xpath("(//h5[text()='Available On Tour Benefits']//following::div[contains(@class,'package-points')]//strong)["
									+ i + "]"))
							.getText();
					if (strName.equals(strAvlOnTourList[i - 1])) {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
								"OnTour Benefit" + strName + " " + "is displayed successfully");
					} else {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
								"OnTour Benefit" + strName + " " + "is not displayed successfully");
					}
				}
			}
			String strCWPointsTxt = driver.findElement(CWPointsTxt).getText().trim();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(bonusPointChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(matchRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(updateBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strBonusPointsTxt = driver.findElement(bonusPointsTxt).getText().trim();
			if (strCWPointsTxt.equals(strBonusPointsTxt)) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"Bonus Points is Updated successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"Bonus Points is not Updated successfully");
			}
			/*
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(PICPlusChkBox).click();
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(RCIAnnualRadioBtn).click();
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(studioRadioBtn).click();
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(updateBtn).click();
			 * waitForSometime(tcConfig.getConfig().get("LongWait")); String
			 * strStudioTxt =
			 * driver.findElement(picPlusRoomTxt).getText().trim();
			 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
			 * (strStudioTxt.equals("70,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Plus" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 * else if (strStudioTxt.equals("150,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Plus" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 * else if (strStudioTxt.equals("154,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Plus" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 * else if (strStudioTxt.equals("254,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Plus" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					packageVersion = driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@ng-reflect-klass='card package-card ']/div[@class='card-header']/span)["
									+ j + "]"))
							.getText();
					driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::button[@id='button-basic'])["
									+ j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
							"Tour Point is displayed successfully");
					break;
				}

			}
			int strPostPackageVer = Integer.parseInt(packageVersion);
			int diff = strPostPackageVer - strPrePackageVer;
			if (diff == 1) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.PASS,
						"Tour Package version is updated successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.FAIL,
						"Tour Package version is not updated successfully");
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void deselectOnTourBenefitDetails() {
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(PICPlusChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strStudioTxt = driver.findElement(deselectPicPlusTxt).getText().trim();
			if (strStudioTxt.equals("-")) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"PIC Plus benefit is deselected successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"PIC Plus benefit is not deselected successfully");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(bonusPointChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strBonusPointsTxt = driver.findElement(bonusPointsTxt).getText().trim();
			if (strBonusPointsTxt.equals("-")) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"Bonus Points is deselected successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"Bonus Points is not deselected successfully");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void OTBSummary() {

		try {
			waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			driver.findElement(benefitTab).click();

			List<WebElement> allList = driver.findElements(OTBSummaryTable);
			for (int i = 2; i <= allList.size(); i++) {
				String stronTourBenefitName = null;
				String strPoints = null;
				stronTourBenefitName = driver
						.findElement(By.xpath(
								"(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr[" + i + "]/td[1]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strPoints = driver
						.findElement(By.xpath(
								"(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr[" + i + "]/td[2]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (stronTourBenefitName.isEmpty() == false && strPoints.isEmpty() == false) {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "OTBSummary", Status.PASS,
							"Benefits and Bonus Points is displayed successfully for " + stronTourBenefitName);
				} else {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "OTBSummary", Status.FAIL,
							"Benefits and Bonus Points is not displayed successfully for " + stronTourBenefitName);
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void deleteBenefit() {
		String strDeleteBenefit = testData.get("DeleteBenefit");
		try {
			List<WebElement> allList = driver.findElements(OTBSummaryTable);
			for (int i = 3; i <= allList.size(); i++) {
				String stronTourBenefitName = null;
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				stronTourBenefitName = driver
						.findElement(By.xpath(
								"(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr[" + i + "]/td[1]"))
						.getText();
				if (stronTourBenefitName.equals(strDeleteBenefit)) {
					driver.findElement(By.xpath(
							"(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr[" + i + "]/td[4]/img"))
							.click();
					break;
				}

			}
		} catch (Exception e) {
			e.getMessage();

		}
	}

	public void deleteBenefitCheck() {
		String strDeleteBenefit = testData.get("DeleteBenefit");
		String stronTourBenefitName = null;
		try {
			List<WebElement> allList = driver.findElements(OTBSummaryTable);
			for (int i = 3; i <= allList.size(); i++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				stronTourBenefitName = driver
						.findElement(By.xpath(
								"(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr[" + i + "]/td[1]"))
						.getText();
				if (stronTourBenefitName.equals(strDeleteBenefit)) {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "deleteBenefitCheck", Status.FAIL,
							"Benefits " + stronTourBenefitName + "is not deleted successfully");
				}

			}
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "deleteBenefitCheck", Status.PASS,
					"Benefits " + testData.get("DeleteBenefit") + " is deleted successfully");

		} catch (Exception e) {
			e.getMessage();

		}
	}

	public void exit_functionality() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(cancelExitBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void applyBtn_functionality() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void selectOnTourBenefitDetails_BonusPoints() {
		String strAvlOnTourList[] = testData.get("AvailableOnTourList").split(";");
		int i;
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			//waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(onTourLabel))) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"On Tour Benefit Label is displayed successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"On Tour Benefit Label is not displayed successfully");
			}

			if (verifyElementDisplayed(driver.findElement(availableTourBenefitLabel))) {
				List<WebElement> allList = driver.findElements(availableOnTourList);
				for (i = 1; i <= allList.size(); i++) {
					String strName = driver.findElement(By
							.xpath("(//h5[text()='Available On Tour Benefits']//following::div[contains(@class,'package-points')]//strong)["+i+"]"))
							.getText();
					if (strName.equals(strAvlOnTourList[i - 1])) {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
								"OnTour Benefit" + strName + " " + "is displayed successfully");
					} else {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
								"OnTour Benefit" + strName + " " + "is not displayed successfully");
					}
				}
			}
			String strCWPointsTxt = driver.findElement(CWPointsTxt).getText().trim();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(bonusPointChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(matchRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(updateBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strBonusPointsTxt = driver.findElement(bonusPointsTxt).getText().trim();
			if (strCWPointsTxt.equals(strBonusPointsTxt)) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"Bonus Points is Updated successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"Bonus Points is not Updated successfully");
			}
			applyBtn_functionality();
		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
					"Bonus Points is not Updated successfully" + e.getMessage());
		}
	}

	
	protected By btnAddPIC = By.xpath("//button[text()='Add PIC']");
	protected By strResortName = By.xpath("//input[@placeholder='Filter by Resort name']");
	protected By strCountry = By.xpath("//select[contains(@class,'sales-line')]");
	protected By selectCheckBox = By.xpath("//label[contains(@for,'select-checkbox')]");
	protected By linkInventoryDetails = By.xpath("//button[text()=' Inventory Details ']");
	protected By standardChkBox = By.xpath("//strong[text()='Standard']/../div/label[@for='first-standard']");
	protected By lockOffCheckBox = By.xpath("//strong[text()='Lock-Off']/../div/label[@for='first-lock-off']");
	protected By btnTotalPointsUsage = By.xpath("//button[text()=' Points Usage Summary ']");
	protected By btnPresentationTile= By.xpath("//button[text()='Go to Presentation Tile']");
	protected By lableResortID = By.xpath("//table[contains(@class,'resort-search')]//tbody/tr/td");

	
	
	

	protected By addAnother = By.xpath("//button[contains(text(),'Add Another')]");

	public void addPICBenefitValidation() {
		strPoint = testData.get("TourPoints");
		try {
			for (int i = 1; i < 3; i++) {
				waitUntilElementVisibleBy(driver, cwaPackagePoints, "ExplicitWaitLongestTime");
				List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
				for (int j = 1; j <= cwaPitchPoints.size(); j++) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String cwaPackagePoint = driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
									+ j + "]"))
							.getText();
					if (strPoint.equals(cwaPackagePoint)) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.findElement(By
								.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::button[@id='button-basic'])["
										+ j + "]"))
								.click();
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
								"Tour Point is displayed successfully");
						break;
					}

				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
				driver.findElement(OptionLink).click();
				waitUntilElementVisibleBy(driver, proceedButton, "ExplicitWaitLongestTime");
				driver.findElement(proceedButton).click();
				waitUntilElementVisibleBy(driver, benefitTab, 240);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(benefitTab).click();
				waitUntilElementVisibleBy(driver, PICPlusChkBox, "ExplicitWaitLongestTime");
				driver.findElement(PICPlusChkBox).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (i == 2) {
					driver.findElement(PICPlusChkBox).click();
				}
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"PIC Plus Benefit checkbox is Displayed successfully");
				if (i == 1) {
					waitUntilElementVisibleBy(driver, btnAddPIC, "ExplicitWaitLongestTime");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(btnAddPIC);
				} else {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisibleBy(driver, addAnother, "ExplicitWaitLongestTime");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(addAnother);
				}

				waitUntilElementVisibleBy(driver, strResortName, "ExplicitWaitLongestTime");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strResortTestData[] = testData.get("strResortName").split(";");
				driver.findElement(strResortName).sendKeys(strResortTestData[i - 1]);
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"Resort name is selected as " + strResortTestData[i - 1]);
				waitUntilElementVisibleBy(driver, strCountry, "ExplicitWaitLongestTime");
				new Select(driver.findElement(strCountry)).selectByValue(testData.get("strCountry"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(selectCheckBox);
				waitUntilElementVisibleBy(driver, linkInventoryDetails, "ExplicitWaitLongestTime");
				clickElementBy(linkInventoryDetails);
				waitUntilElementVisibleBy(driver, RCIAnnualRadioBtn, "ExplicitWaitLongestTime");
				driver.findElement(RCIAnnualRadioBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(standardChkBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(benefirRadioBtn).click();
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"PIC Plus" + testData.get("RoomType") + " points is Updated successfully");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(btnTotalPointsUsage);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, btnPresentationTile, "ExplicitWaitLongestTime");
				clickElementBy(btnPresentationTile);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	protected By addAnotherBtn = By.xpath("//button[contains(@class,'disabled') and contains(text(),'Add Another')]");

	public void validateAddPICButtonFunctionality() {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
			driver.findElement(OptionLink).click();
			waitUntilElementVisibleBy(driver, proceedButton, "ExplicitWaitLongestTime");
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, "ExplicitWaitLongestTime");
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, PICPlusChkBox, "ExplicitWaitLongestTime");
			driver.findElement(PICPlusChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(PICPlusChkBox).click();
			if (verifyElementDisplayed(driver.findElement(addAnotherBtn))) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality",
						Status.PASS, "Add Another button is disabled");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality",
						Status.FAIL, "Add Another button is not disabled");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality", Status.FAIL,
					"Failed in Method" + e.getLocalizedMessage());
		}
	}

	protected By picBenefitTable = By.xpath("//h4[text()='PIC Plus']/..//table/tbody/tr");
	protected By deleteBenefitPopUp = By.xpath("//p[text()='Deleting PIC Plus Resort & Inventory']");
	protected By yesBtn = By.xpath("//button[text()='Yes']");

	public void validateDeletePICBenefit() {
		String strResortName[] = testData.get("strResortName").split(";");
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> picRowCount = driver.findElements(picBenefitTable);
			int preRowCount = picRowCount.size();
			driver.findElement(By.xpath("//h4[text()='PIC Plus']/..//table/tbody/tr/td/strong[text()='"
					+ strResortName[1] + "']/../../td[6]//img[contains(@src,'icon-close')]")).click();
			waitUntilElementVisibleBy(driver, deleteBenefitPopUp, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(deleteBenefitPopUp))) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality",
						Status.PASS, "Delete Benefit Confirmation pop up is displayed");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(yesBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> picRowCount1 = driver.findElements(picBenefitTable);
				int postRowCount = picRowCount1.size();
				if (!(preRowCount == postRowCount)) {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality",
							Status.PASS,
							"PIC benefit for the resort" + strResortName[1] + " has been deleted successfully");
				} else {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality",
							Status.FAIL, "PIC benefit for the resort has not been deleted");
				}
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality",
						Status.FAIL, "Delete Benefit Confirmation pop up is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "validateAddPICButtonFunctionality", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	public void ValidateOTBSummary() {
		String stronTourBenefitName = null;
		String strPoints = null;
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			stronTourBenefitName = driver.findElement(benefitCheck).getText();
			if (stronTourBenefitName.equals("Pic Plus " + testData.get("RoomType") + "")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strPoints = driver
						.findElement(
								By.xpath("(//h4[text()='OTB Summary']//following-sibling::table/tbody)[1]/tr[4]/td[2]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (stronTourBenefitName.isEmpty() == false && strPoints.isEmpty() == false) {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "OTBSummary", Status.PASS,
							"Benefits and Bonus Points is displayed successfully for " + stronTourBenefitName);
				} else {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "OTBSummary", Status.FAIL,
							"Benefits and Bonus Points is not displayed successfully for " + stronTourBenefitName);
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "OTBSummary", Status.FAIL,
					"Benefits and Bonus Points is not displayed successfully for " + stronTourBenefitName);
		}
	}

	public void selectOnTourBenefitDetails_PICExpress() {
		String strAvlOnTourList[] = testData.get("AvailableOnTourList").split(";");
		int i;
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(onTourLabel))) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"On Tour Benefit Label is displayed successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"On Tour Benefit Label is not displayed successfully");
			}

			if (verifyElementDisplayed(driver.findElement(availableTourBenefitLabel))) {
				List<WebElement> allList = driver.findElements(availableOnTourList);
				for (i = 1; i <= allList.size(); i++) {
					String strName = driver.findElement(By
							.xpath("(//h5[text()='Available On Tour Benefits']//following::div[@class='package-points-card full-card' or @class='package-points-card']/strong)["
									+ i + "]"))
							.getText();
					if (strName.equals(strAvlOnTourList[i - 1])) {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
								"OnTour Benefit" + strName + " " + "is displayed successfully");
					} else {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
								"OnTour Benefit" + strName + " " + "is not displayed successfully");
					}
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(PICExpressChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * driver.findElement(RCIAnnualRadioBtn).click();
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(benefirRadioBtnPICExpress).click();
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(updateBtn).click();
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * strStudioTxt =
			 * driver.findElement(picExpressRoomTxt).getText().trim(); if
			 * (strStudioTxt.equals("70,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Express" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 * else if (strStudioTxt.equals("105,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Express" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 * else if (strStudioTxt.equals("154,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Express" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 * else if (strStudioTxt.equals("254,000 Points")) {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "PIC Express" +
			 * testData.get("RoomType") + " points is Updated successfully"); }
			 * applyBtn_functionality();
			 */

			clickElementBy(btnAddPIC);
			waitUntilElementVisibleBy(driver, strResortName, "ExplicitWaitLongestTime");
			driver.findElement(strResortName).sendKeys(testData.get("strResortName"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(strCountry)).selectByValue(testData.get("strCountry"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(selectCheckBox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(linkInventoryDetails);
			waitUntilElementVisibleBy(driver, RCIAnnualRadioBtn, "ExplicitWaitLongestTime");
			driver.findElement(RCIAnnualRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(standardChkBox);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(benefirRadioBtn).click();
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
					"PIC Express" + testData.get("RoomType") + " points is Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(btnTotalPointsUsage);
			waitUntilElementVisibleBy(driver, btnPresentationTile, "ExplicitWaitLongestTime");
			clickElementBy(btnPresentationTile);
		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
					"Bonus Points is not Updated successfully" + e.getMessage());
		}
	}

}
