package eWorksheet.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import eWorksheet.pages.AddPackageInventoryPage;
import eWorksheet.pages.CWAPackageWSAPage;
import eWorksheet.pages.CustomerDetailsPage;
import eWorksheet.pages.LeadsPage;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CustomerDetailsPage_Ipad extends CustomerDetailsPage {

	public static final Logger log = Logger.getLogger(CustomerDetailsPage_Ipad.class);

	public CustomerDetailsPage_Ipad(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By enterVacationCost = By.xpath("//input[@class='form-control' and @inputmode='numeric']");
	protected By vacaCostFromUI = By
			.xpath("//input[@class='form-control' and @inputmode='numeric']/../span[@class='highlight']");
	protected By setButton = By.xpath(
			"//button[contains(text(),'Set') and @type='button' and @class='float-right btn btn-black btn-grey']");
	protected By setButtonEnabled = By.xpath("(//button[contains(text(),'Set') and @type='button'])[2]");
	protected By successMsgOfVacationCost = By.xpath("//h2[contains(text(),'Lifetime Vacation Cost Successfully Set')]");
	protected By successMsgOKButton = By.xpath(
			"//h2[contains(text(),'Lifetime Vacation Cost Successfully Set')]//following::div[@class='swal2-actions']/button[contains(text(),'OK')]");
	protected By gotoWorksheetButton = By.xpath(
			"//button[not(contains(@class,'btn w-100 btn-black-outline disabled')) and contains(text(),'Go to Worksheet')]");
	protected By vacationCostInWorksheet = By.xpath("//span[contains(text(),'Vacation Cost')]/../strong");
	protected By moreIcon = By
			.xpath("//button[@class='btn btn-link dropdown-toggle' and @id='button-basic']/img[@alt='icon-more']");
	protected By exitButton = By.xpath(
			"//button[@class='btn btn-link dropdown-toggle' and @id='button-basic']/img[@alt='icon-more']//following::li[@role='menuitem']/a/img[@alt='icon-logout-grey']");
	protected By exitSureMessage = By.xpath("//button[contains(text(),'Yes, I’m sure')]");
	protected By vacationCostEditBtn = By
			.xpath("//button[contains(text(),'Edit') and @class='float-right btn btn-black']");
	protected By customerName = By.xpath("//div[@class='ews-tour-details-card ']/div[@class='ews-tour-header']/h3");
	protected By custNameinCreditSummary = By.xpath(
			"//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td//div[@class='custom-media-badge']//following::h5");
	protected By customerAddress = By.xpath(
			"//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td//div[@class='custom-media-badge']//following::address/p");
	protected By MSCreditBand = By.xpath(
			"(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[2]/img[contains(@src,'soft.png')])[1]");
	protected By MarketingScoreVCC = By.xpath(
			"//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[3]/strong");
	protected By SalesScoreCreditBand = By.xpath(
			"(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[4]/img[contains(@src,'hard.png')])[1]");
	protected By hardScIntCust = By.xpath(
			"//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[4]/img[contains(@src,'b1-credit-hard')]");
	protected By SalesScoreVCC = By.xpath(
			"(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[5])[1]");
	protected By RewardsVIsa = By.xpath(
			"(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[6])[1]");
	protected By firstCredit = By.xpath(
			"(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[7]/p/span)[1]");
	protected By WorkSheetMileStoneWidget = By.xpath(
			"(//h3[contains(text(),'Worksheet Milestones')]//following::table[@class='table custom-table'])[1]/tbody");
	protected By MilestoneState = By.xpath(
			"(//h3[contains(text(),'Worksheet Milestones')]//following::table[@class='table custom-table'])[1]/tbody/tr[1]/td/div/a/strong");
	protected By MileStoneStatus = By.xpath(
			"(//h3[contains(text(),'Worksheet Milestones')]//following::table[@class='table custom-table'])[1]/tbody/tr[1]/td[2]/div/a/span");
	protected By pointsPackagePage = By.xpath("//a[contains(text(),'Points Packages')]");
	protected By dpObtainedDate = By.xpath(
			"//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td[7]/p");
	protected By customerDetCount = By.xpath(
			"//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr");
	protected By viewMoreLink = By.xpath(
			"//ul[@role='navigation']/li[@class='pagination-next ng-star-inserted']/a[contains(text(),' Next ')]");
	protected By ViewCRCustomerName = By
			.xpath("((//table[contains(@class,'view-credit-table')])[2]/tbody/tr//descendant::strong)[1]");
	protected By ViewCRSoftScore = By.xpath("//img[contains(@src,'soft.png')]");
	protected By ViewCRHardScore = By.xpath("//img[contains(@src,'hard')]");
	protected By ViewCRBarclayVisa = By.xpath("(//datatable-body-cell[contains(@class,'visa')])[1]//descendant::span");
	protected By OptionLink = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::ul/li[@role='menuitem']/a[contains(text(),'Options ')]");
	protected By proceedButton = By.xpath("//button[contains(text(),'Yes, proceed to options')]");
	protected By creditTab = By.xpath("(//span[contains(text(),'Credit')])[1]");
	protected By startWorkSheetBtn = By.xpath(
			"//button[not(contains(@class,'btn w-100 btn-black-outline disabled')) and contains(text(),'Start Worksheet')]");
	protected By blankMileStoneStatus = By.xpath(
			"(//h3[contains(text(),'Worksheet Milestones')]//following::table[@class='table custom-table'])[1]/tbody/tr[1]/td[2]");
	// protected By creditBand = By.xpath("//img[@alt='credit-score' and
	// contains(@src,'credit-hard.png')]");
	protected By defaultCreditBand = By.xpath("//img[@alt='credit-score' and contains(@src,'credit-soft.png')]");
	protected By arrowBtn = By.xpath("(//img[@alt='icon-arrow-angle-right-white'])[1]");
	protected By tourStepsCnt = By.xpath("//a[text()='Tour Progress']//following::li[@class='nav-item']/a");
	protected By vccPreapproval = By
			.xpath("(//datatable-body-cell[contains(@class,'bg-grey-base sort')])[1]//descendant::span");
	protected By vccApproval = By
			.xpath("(//datatable-body-cell[contains(@class,'bg-grey-base sort')])[2]//descendant::span");
	protected By VCCustomerNameTxt = By.xpath(
			"(//h3[text()='View Credit Information']//following::div[@ng-reflect-klass='package-points-card'])[1]/strong");
	protected By vccMS = By.xpath("((//table[contains(@class,'view-credit-table')])[2]/tbody/tr//descendant::strong)[2]");
	protected By vccSS = By.xpath("((//table[contains(@class,'view-credit-table')])[2]/tbody/tr//descendant::strong)[3]");
	protected By WSCompletedImgClick = By.xpath(
			"(//h3[contains(text(),'Worksheet milestones')]//following::table[@class='table custom-table'])[1]/tbody/tr[1]/td[2]/img");

	public void verifyVacationCalculatorEntry() {
		boolean isSetButtonEnabled;
		String strVacationCost = testData.get("VacationCost");
		String strVacationCostfromUI = "";

		try {
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, enterVacationCost, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(enterVacationCost))) {
				strVacationCostfromUI = driver.findElement(enterVacationCost).getText();
				if (strVacationCostfromUI.equals("")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					isSetButtonEnabled = driver.findElement(setButton).isDisplayed();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (isSetButtonEnabled == true) {
						tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry",
								Status.PASS, "Set button is disabled");
					} else {
						tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry",
								Status.FAIL, "Set button is not disabled");
					}

					int strMxLenofVacaCost = Integer
							.parseInt(driver.findElement(enterVacationCost).getAttribute("maxlength"));
					String inputPattern = driver.findElement(enterVacationCost).getAttribute("pattern");
					if (strMxLenofVacaCost == 6 && inputPattern.equals("[0-9]*")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (strVacationCost.length() <= 6) {
							driver.findElement(enterVacationCost).sendKeys(strVacationCost);
							// waitForSometime(tcConfig.getConfig().get("MedWait"));
							waitUntilElementVisibleBy(driver, setButtonEnabled, "ExplicitWaitLongestTime");
							driver.findElement(setButtonEnabled).click();
							waitUntilElementVisibleBy(driver, successMsgOfVacationCost, "ExplicitWaitLongestTime");
							if (verifyElementDisplayed(driver.findElement(successMsgOfVacationCost))) {
								driver.findElement(successMsgOKButton).click();
							}
						} else {
							tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry",
									Status.FAIL, "Vacation Cost is Invalid");
						}

					}
				}
			}
			startWorksheet_functionality();
			AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcConfig);
			addPckg.selectCWAPackagesfromInventory();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, vacationCostInWorksheet, "ExplicitWaitLongestTime");
			String strVacationCostInWorksheet = driver.findElement(vacationCostInWorksheet).getText().substring(1)
					.replace(",", "");
			if (strVacationCost.equals(strVacationCostInWorksheet)) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry", Status.PASS,
						"Vacation Cost is updated successfully in Worksheet");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry", Status.FAIL,
						"Vacation Cost is not updated successfully in Worksheet");
			}
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, moreIcon, "ExplicitWaitLongestTime");
			driver.findElement(moreIcon).click();
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, exitButton, "ExplicitWaitLongestTime");
			driver.findElement(exitButton).click();
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, exitSureMessage, "ExplicitWaitLongestTime");
			driver.findElement(exitSureMessage).click();

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void verifyVacationCostEditButton() {
		boolean isEditBtnEnable = false;

		try {
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, vacationCostEditBtn, 240);
			if (verifyElementDisplayed(driver.findElement(vacationCostEditBtn))) {
				// waitForSometime(tcConfig.getConfig().get("LongWait"));
				isEditBtnEnable = driver.findElement(vacationCostEditBtn).isEnabled();
				if (isEditBtnEnable == true) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCostEditButton", Status.PASS,
							"Edit button is enabled after setting the vacation cost successfully");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCostEditButton", Status.FAIL,
							"Edit button is not enabled after setting the vacation cost successfully");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void customerNameValidation() {
		String strCustomerName = testData.get("CustomerName");
		String strCustAdd[] = testData.get("CustomerAddress").split(";");
		String strCustName[] = strCustomerName.split(";");
		String strCust1FirstName[] = strCustName[0].split(" ");
		String strCust2FirstName[] = strCustName[1].split(" ");
		String verifyCustName = "";
		String verifyCustNameFromUI = "";
		String strCustAddUI = "";

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (strCustName[0].equals(strCustName[1])) {
				verifyCustName = strCust1FirstName[0] + " " + "&" + " " + strCust2FirstName[0] + " "
						+ strCust1FirstName[1];
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, customerName, "ExplicitWaitLongestTime");
				verifyCustNameFromUI = driver.findElement(customerName).getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyCustName.equalsIgnoreCase(verifyCustNameFromUI)) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.PASS,
							"Customer name is displayed successfully in Customer Information section");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.FAIL,
							"Customer name is not displayed successfully in Customer Information section");
				}

			} else {

				verifyCustName = strCustName[0] + "/" + strCustName[1];
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, customerName, "ExplicitWaitLongestTime");
				verifyCustNameFromUI = driver.findElement(customerName).getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyCustName.equalsIgnoreCase(verifyCustNameFromUI)) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.PASS,
							"Customer name is displayed successfully in Customer Information section");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.FAIL,
							"Customer name is not displayed successfully in Customer Information section");
				}

			}

			// Customer Address Validation
			List<WebElement> allList = driver.findElements(customerAddress);
			for (int i = 1; i <= allList.size(); i++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strCustAddUI = driver.findElement(By
						.xpath("(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td//div[@class='custom-media-badge']//following::address/p)["
								+ i + "]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (strCustAdd[i - 1].equalsIgnoreCase(strCustAddUI)) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.PASS,
							"Customer address " + i + " is displayed and validated successfully");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.FAIL,
							"Customer address " + i + " is not displayed successfully");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void validateCustomerName() {
		try {
			waitUntilElementVisibleBy(driver, customerName, "ExplicitWaitLongestTime");
			String verifyCustNameFromUI = driver.findElement(customerName).getText();
			if (LeadsPage.strLeadFullName.equalsIgnoreCase(verifyCustNameFromUI)) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.PASS,
						"Customer name is displayed successfully in Customer Information section");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.FAIL,
						"Customer name is not displayed successfully in Customer Information section");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "customerNameValidation", Status.FAIL,
					"Customer name is not displayed successfully in Customer Information section " + e.getMessage());
		}
	}

	public void customerBadgeValidation() {
		String strCustBadgeName[];
		String strCustomerName = "";
		String nameInitials = "";
		try {
			List<WebElement> allList = driver.findElements(custNameinCreditSummary);
			for (int i = 1; i <= allList.size(); i++) {
				strCustomerName = driver.findElement(By
						.xpath("(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td//div[@class='custom-media-badge']//following::h5)["
								+ i + "]"))
						.getText();
				strCustBadgeName = strCustomerName.split(" ");
				for (String word : strCustBadgeName) {
					nameInitials += Character.toUpperCase(word.charAt(0));
				}
				String custBadgeNameUI = driver.findElement(By
						.xpath("(//h3[contains(text(),'Credit Summary')]//following::table[@class='table custom-table'][1]/tbody/tr/td//div[@class='custom-media-badge'])["
								+ i + "]"))
						.getText();
				if (nameInitials.equals(custBadgeNameUI)) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerBadgeValidation", Status.PASS,
							"Customer name initial is displayed successfully in Customer Information section");
					nameInitials = "";
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "customerBadgeValidation", Status.FAIL,
							"Customer name initial not is displayed successfully in Customer Information section");

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void customerDetailsValidation_MarketingScore() {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(MSCreditBand))) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation", Status.PASS,
						"Marketing Score for Soft score is displayed successfully");

			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation", Status.FAIL,
						"Marketing Score for Soft score is not displayed successfully");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * String strMarketingScoreVCCUI =
			 * driver.findElement(MarketingScoreVCC).getText(); if
			 * (strMarketingScoreVCCUI.equals(strMarketingScoreVCC)) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation", Status.PASS,
			 * "Marketing Score for VCC is displayed successfully"); } else {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation", Status.FAIL,
			 * "Marketing Score for VCC is not displayed successfully"); }
			 */

		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation", Status.FAIL,
					"Marketing Score for VCC is not displayed successfully");
		}
	}

	protected By ownerShipTxt = By.xpath("//td[text()='" + testData.get("Ownership") + "']");

	public void validateOwnership(String strOwnership) {
		try {
			if (strOwnership.equals("Non-Owner")) {
				// waitUntilElementVisibleBy(driver, ownerShipTxt, "ExplicitWaitLongestTime");
				if (verifyObjectDisplayed(ownerShipTxt)) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateOwnership", Status.FAIL,
							"Customer Non-owner ship is not displayed successfully");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateOwnership", Status.PASS,
							"Customer Non-owner ship is displayed successfully");
				}
			} else if (strOwnership.equals("Owner")) {
				// waitUntilElementVisibleBy(driver, ownerShipTxt, "ExplicitWaitLongestTime");
				if (verifyObjectDisplayed(ownerShipTxt)) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateOwnership", Status.FAIL,
							"Customer owner ship is not displayed successfully");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateOwnership", Status.PASS,
							"Customer owner ship is displayed successfully");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateOwnership", Status.FAIL,
					"Customer owner ship is not displayed successfully" + e.getMessage());
		}
	}

	public void customerDetailsValidation_SalesCentreScore() throws ParseException {

		String strSalesScoreVCC = testData.get("VCCSalesScore");
		String strRewardsVISA = testData.get("RewardsVISA");
		String strFirstCredit = testData.get("FirstCredit");
		String strTourDate = testData.get("TourDate");

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(SalesScoreCreditBand))) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation_SalesCentreScore",
						Status.PASS, "Sales centre Score for Hard score is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation_SalesCentreScore",
						Status.FAIL, "Sales centre Score for Hard score is not displayed successfully");
			}
			/*
			 * waitForSometime(tcConfig.getConfig().get("MedWait")); String
			 * strSalesScoreVCCUI = driver.findElement(SalesScoreVCC).getText();
			 * if (strSalesScoreVCCUI.equals(strSalesScoreVCC)) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.PASS,
			 * "Sales Score for VCC is displayed successfully"); } else {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.FAIL,
			 * "Sales Score for VCC is not displayed successfully"); }
			 */
			/*
			 * String strRewardsVISAUI =
			 * driver.findElement(RewardsVIsa).getText(); if
			 * (strRewardsVISAUI.equals(strRewardsVISA)) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.PASS,
			 * "Rewards VISA is displayed successfully"); } else {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.FAIL,
			 * "Rewards VISA is not displayed successfully"); } String
			 * strFirstCreditUI = driver.findElement(firstCredit).getText(); if
			 * (strFirstCreditUI.equalsIgnoreCase(strFirstCredit)) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.PASS,
			 * "First Credit obtained name is displayed successfully"); } else {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.FAIL,
			 * "First Credit obtained name is not displayed successfully"); }
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * String strDpObtainedDate[] =
			 * driver.findElement(dpObtainedDate).getText().split(" ");
			 * waitForSometime(tcConfig.getConfig().get("MedWait")); String
			 * doObtainedDt[] = strDpObtainedDate[1].split("\n"); String
			 * dpObtainedDt = doObtainedDt[1].substring(0, 10); SimpleDateFormat
			 * parser = new SimpleDateFormat("yyyy-MM-dd"); Date date =
			 * parser.parse(dpObtainedDt); SimpleDateFormat formatter = new
			 * SimpleDateFormat("d/MM/yyyy"); String formattedDate =
			 * formatter.format(date); if (strTourDate.equals(formattedDate)) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.PASS,
			 * "First Credit obtained date is displayed successfully"); } else {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "customerDetailsValidation_SalesCentreScore", Status.FAIL,
			 * "First Credit obtained date is not displayed successfully"); }
			 */

		} catch (NoSuchElementException e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation_SalesCentreScore",
					Status.FAIL, "Object not found" + e.getMessage());
		}
	}

	public void validatePagination() {
		boolean isEnable;
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> custDetailsList = driver.findElements(customerDetCount);
			int cusDetCount = custDetailsList.size();
			if (cusDetCount <= 2) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				isEnable = verifyElementDisplayed(driver.findElement(viewMoreLink));
				if (isEnable == false) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validatePagination", Status.PASS,
							"View More link is not displayed as the Customer Details are less than three");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validatePagination", Status.FAIL,
							"View More link is displayed as the Customer Details are less than three");
				}
			} else {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(viewMoreLink).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> custDetailsListInNext = driver.findElements(customerDetCount);
				int custDetailsListInNextPage = custDetailsListInNext.size();
				if (custDetailsListInNextPage >= 1) {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validatePagination", Status.PASS,
							"User is able to navigate to Next page if more than three customer details is available");
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validatePagination", Status.FAIL,
							"User is not able to navigate to Next page if more than three customer details is available");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validatePagination", Status.FAIL,
					"User is not able to navigate to Next page if more than three customer details is available");

		}
	}

	public void validateWorkSheetMilestoneWidget() {
		String strMileStoneState = "";
		String strMileStoneStatus = "";
		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(WorkSheetMileStoneWidget))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strMileStoneState = driver.findElement(MilestoneState).getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strMileStoneStatus = driver.findElement(MileStoneStatus).getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (strMileStoneState.equals("Inventory") && strMileStoneStatus.equals("In Progress")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget",
							Status.PASS, "Worksheet Milestone widget is displayed successfully");
					driver.findElement(MileStoneStatus).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyElementDisplayed(driver.findElement(pointsPackagePage))) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget",
								Status.PASS, "Inventory is initiated successfully");
					}
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget",
							Status.FAIL, "Worksheet Milestone widget is not displayed successfully");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget", Status.FAIL,
					"Worksheet Milestone widget is not displayed successfully");
		}
	}

	public void clickWorkSheetMilestoneWidget() {
		String strMileStoneState = "";

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(WorkSheetMileStoneWidget))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strMileStoneState = driver.findElement(MilestoneState).getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (strMileStoneState.equals("Inventory")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget",
							Status.PASS, "Worksheet Milestone widget is displayed successfully");
					driver.findElement(WSCompletedImgClick).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyElementDisplayed(driver.findElement(pointsPackagePage))) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget",
								Status.PASS, "Inventory is initiated successfully");
					}
				} else {
					tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget",
							Status.FAIL, "Worksheet Milestone widget is not displayed successfully");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("CustomerDetailsPage", "validateWorkSheetMilestoneWidget",
					Status.FAIL, "Worksheet Milestone widget is not displayed successfully" + e.getMessage());
		}
	}

	public void SheetMilestoneWidgetInventoryProcessStatus() {
		String strMileStoneState = "";
		String strMileStoneStatus = "";

		waitUntilElementVisibleBy(driver, WorkSheetMileStoneWidget, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(WorkSheetMileStoneWidget))) {
			strMileStoneState = driver.findElement(MilestoneState).getText();
			strMileStoneStatus = driver.findElement(blankMileStoneStatus).getText();
			if (strMileStoneState.equals("Inventory") && strMileStoneStatus.equals("")) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget", Status.PASS,
						"Worksheet Milestone widget is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateWorkSheetMilestoneWidget", Status.FAIL,
						"Worksheet Milestone widget is not displayed successfully");
			}
		}

	}

	public void validateVacationCostWidget() {
		try {
			if (verifyElementDisplayed(driver.findElement(enterVacationCost))) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateVacationCostWidget", Status.PASS,
						"Vacation cose widget is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateVacationCostWidget", Status.FAIL,
						"Vacation cose widget is not displayed successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void validateCustomerCreditInformation() {
		String customerName = testData.get("CustomerName");
		String visaValue = testData.get("RewardsVISA");
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strCustomerName = driver.findElement(ViewCRCustomerName).getText();
			if (customerName.equals(strCustomerName)) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.PASS,
						"Customer name is displayed successfully in Customer Information section");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.FAIL,
						"Customer name is not displayed successfully in Customer Information section");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(ViewCRHardScore))) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.PASS,
						"Hard score is considered for financial Calculations and Soft score is displayed successfully");

			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.FAIL,
						"Hard score is considered for financial Calculations and Soft score is not displayed successfully");
			}

			/*
			 * String strVisaValue =
			 * driver.findElement(ViewCRBarclayVisa).getText(); if
			 * (visaValue.equals(strVisaValue)) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "validateCustomerCreditInformation", Status.PASS,
			 * "Barclays VISA is displayed successfully"); } else {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "validateCustomerCreditInformation", Status.FAIL,
			 * "Barclays VISA is not displayed successfully"); }
			 */

			/*
			 * String strvccPreapproval =
			 * driver.findElement(vccPreapproval).getText(); String
			 * strvccApproval = driver.findElement(vccApproval).getText();
			 * if(strvccPreapproval.equals("-") && strvccApproval.equals("-")) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "validateCustomerCreditInformation", Status.PASS,
			 * "VCC Applied but not Approved Customer"); }else
			 * if(!strvccPreapproval.isEmpty() && !strvccApproval.isEmpty()){
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "validateCustomerCreditInformation", Status.PASS,
			 * "VCC Preapproved and Approved is displayed successfully");
			 * 
			 * }else{ tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "validateCustomerCreditInformation", Status.FAIL,
			 * "VCC Preapproved and Approved is not displayed successfully"); }
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * if(verifyElementDisplayed(driver.findElement(vccSS))) {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "validateCustomerCreditInformation", Status.PASS,
			 * "VCC for Sales score is displayed successfully"); }else {
			 * tcConfig.updateTestReporter("CustomerDetailsPage",
			 * "validateCustomerCreditInformation", Status.FAIL,
			 * "VCC for Sales score is not displayed successfully"); }
			 */

		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.FAIL,
					"VCC for Marketing score and Sales score is not displayed successfully");
		}
	}

	public void startWorksheet_functionality() {
		waitUntilElementVisibleBy(driver, startWorkSheetBtn, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(startWorkSheetBtn)) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "startWorksheet_functionality", Status.PASS,
					"Start Worksheet button is displayed successfully");
			driver.findElement(startWorkSheetBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}

	}

	public void gotoWorksheet_functionality() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(startWorkSheetBtn)) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "startWorksheet_functionality", Status.PASS,
						"Start Worksheet button is displayed successfully");
				driver.findElement(startWorkSheetBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void gotoWorksheet() {
		try {
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, gotoWorksheetButton, "ExplicitWaitLongestTime");
			if (verifyObjectDisplayed(gotoWorksheetButton)) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "startWorksheet_functionality", Status.PASS,
						"Go to Worksheet button is displayed successfully");
				driver.findElement(gotoWorksheetButton).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void selectCWAPackage() {
		boolean isSetButtonEnabled;
		String strVacationCost = testData.get("VacationCost");
		String strVacationCostfromUI = "";
		try {
			if (verifyElementDisplayed(driver.findElement(enterVacationCost))) {
				strVacationCostfromUI = driver.findElement(enterVacationCost).getText();
				if (strVacationCostfromUI.equals("")) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					isSetButtonEnabled = driver.findElement(setButton).isDisplayed();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (isSetButtonEnabled == true) {
						tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry",
								Status.PASS, "Set button is disabled");
					} else {
						tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry",
								Status.FAIL, "Set button is not disabled");
					}

					int strMxLenofVacaCost = Integer
							.parseInt(driver.findElement(enterVacationCost).getAttribute("maxlength"));
					String inputPattern = driver.findElement(enterVacationCost).getAttribute("pattern");
					if (strMxLenofVacaCost == 6 && inputPattern.equals("[0-9]*")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (strVacationCost.length() <= 6) {
							driver.findElement(enterVacationCost).sendKeys(strVacationCost);
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(setButtonEnabled).click();
							waitUntilElementVisibleBy(driver, successMsgOfVacationCost, "ExplicitWaitLongestTime");
							if (verifyElementDisplayed(driver.findElement(successMsgOfVacationCost))) {
								driver.findElement(successMsgOKButton).click();
							}
						} else {
							tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry",
									Status.FAIL, "Vacation Cost is Invalid");
						}

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void validateTourCost() {
		String strVacationCost = testData.get("VacationCost");
		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			String strVacationCostInWorksheet = driver.findElement(vacationCostInWorksheet).getText().substring(1)
					.replace(",", "");
			if (strVacationCost.equals(strVacationCostInWorksheet)) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry", Status.PASS,
						"Vacation Cost is updated successfully in Worksheet");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "verifyVacationCalculatorEntry", Status.FAIL,
						"Vacation Cost is not updated successfully in Worksheet");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	/*
	 * public void validateCustomerCreditBand() { boolean creditflag = false;
	 * boolean defaultCreditFlag = false; try {
	 * 
	 * creditflag = verifyElementDisplayed(driver.findElement(creditBand)); if
	 * (creditflag == true) { tcConfig.updateTestReporter("CustomerDetailsPage",
	 * "validateCustomerCreditBand", Status.PASS,
	 * "Credit Band for Customer is displayed successfully"); } else {
	 * tcConfig.updateTestReporter("CustomerDetailsPage",
	 * "validateCustomerCreditBand", Status.FAIL,
	 * "Credit Band for Customer is not displayed successfully"); }
	 * 
	 * defaultCreditFlag =
	 * verifyElementDisplayed(driver.findElement(defaultCreditBand)); if
	 * (defaultCreditFlag == true) {
	 * tcConfig.updateTestReporter("CustomerDetailsPage",
	 * "validateCustomerCreditBand", Status.PASS,
	 * "Default Credit Band for Customer is displayed successfully"); } else {
	 * tcConfig.updateTestReporter("CustomerDetailsPage",
	 * "validateCustomerCreditBand", Status.FAIL,
	 * "Default Credit Band for Customer is not displayed successfully"); } }
	 * catch (Exception e) { e.getMessage(); } }
	 */

	public void validateTourProgress() {
		String strTour[] = testData.get("TourState").split(";");
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(arrowBtn))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(arrowBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> list = driver.findElements(tourStepsCnt);
				for (int i = 1; i <= list.size(); i++) {
					String strTourState = driver
							.findElement(By.xpath(
									"(//a[text()='Tour Progress']//following::li[@class='nav-item']/a)[" + i + "]"))
							.getText();
					if (strTourState.equals(strTour[i - 1])) {
						tcConfig.updateTestReporter("CustomerDetailsPage", "validateTourProgress", Status.PASS,
								"Tour State " + strTour[i - 1] + "is displayed successfully");
					} else {
						tcConfig.updateTestReporter("CustomerDetailsPage", "validateTourProgress", Status.PASS,
								"Tour State " + strTour[i - 1] + "is not displayed successfully");
					}

				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateTourProgress", Status.PASS,
					"Tour State is not displayed successfully");
		}
	}

	public void validateUSCustomerHardScoreFinancial() {
		String customerName = testData.get("CustomerName");
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strCustomerName = driver.findElement(VCCustomerNameTxt).getText();
			if (strCustomerName.equals(customerName)) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.PASS,
						"Customer name is displayed successfully in Customer Information section");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.FAIL,
						"Customer name is not displayed successfully in Customer Information section");
			}

			if (verifyElementDisplayed(driver.findElement(ViewCRHardScore))) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.PASS,
						"Hard score is considered for financial Calculations and Soft score is displayed successfully");

			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.FAIL,
						"Hard score is considered for financial Calculations and Soft score is not displayed successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditInformation", Status.FAIL,
					"Hard score is considered for financial Calculations and Soft score is not displayed successfully");
		}
	}

	public void hardScoreValidation_InternatinalCustomer() throws ParseException {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(hardScIntCust))) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation_SalesCentreScore",
						Status.PASS, "Sales centre Score for Hard score is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation_SalesCentreScore",
						Status.FAIL, "Sales centre Score for Hard score is not displayed successfully");
			}
		} catch (NoSuchElementException e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "customerDetailsValidation_SalesCentreScore",
					Status.FAIL, "Object not found" + e.getMessage());
		}
	}

	protected By WSMileStoneimg = By.xpath("//button[@id='button-basic']");
	// protected By wsaUnderWorkMile = By.xpath("//h3[contains(text(),'Worksheet
	// milestones')]//following::table[@class='table
	// custom-table']//td[contains(text(),'"+wsaNumberOrignal+"')]");
	protected By btnWorkMileCompleted = By.xpath("//td[contains(.,'Completed')]");
	protected By btnWorkMileRecalled = By.xpath("(//td[contains(.,'Recalled')])[1]");

	public void click_MileStoneButton_And_Proceed() {
		/*
		 * String wsaNumberValidation = ""; String wsaNumberOrignal =
		 * CWAPackageWSAPage.strwsaRefNum;
		 */

		
			//waitUntilElementVisibleBy(driver, WSMileStoneimg, "ExplicitWaitLongestTime");
		     //scrollDowntoXPath(WSMileStoneimg, (RemoteWebDriver) driver);
		getElementInView(WSMileStoneimg);
			driver.findElement(WSMileStoneimg).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * wsaNumberValidation = driver.findElement(By .xpath(
			 * "//h3[contains(text(),'Worksheet Milestones')]//following::table[@class='table custom-table']//td[contains(text(),'"
			 * + wsaNumberOrignal + "')]")) .getText();
			 */
			String mileStoneStatus = driver.findElement(btnWorkMileCompleted).getText();

			if (mileStoneStatus.equals("Completed")) {
				driver.findElement(btnWorkMileCompleted).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("CustomerDetailsPage", "mileStone_Compleated_Status", Status.PASS,
						"milestone widget status as completed is displayed and clicked succesfully");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "mileStone_Compleated_Status", Status.FAIL,
						"milestone widget status as completed is  not displayed and clicked succesfully");
			}

	}

	public void milestoneStatus() {
		String wsaNumberValidation = "";
		String wsaNumberOrignal = CWAPackageWSAPage.wsaRefNumber;
		System.out.println(CWAPackageWSAPage.wsaRefNumber);
		try {
			waitUntilElementVisibleBy(driver, WSMileStoneimg, "ExplicitWaitLongestTime");
			driver.findElement(WSMileStoneimg).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			wsaNumberValidation = driver.findElement(By
					.xpath("//h3[contains(text(),'Worksheet Milestones')]//following::table[@class='table custom-table']//td[contains(text(),'"
							+ wsaNumberOrignal + "')]"))
					.getText();
			String mileStoneStatus = driver.findElement(btnWorkMileRecalled).getText();

			if (wsaNumberValidation.equals(wsaNumberOrignal)) {

				if (mileStoneStatus.equals("Recalled")) {
					driver.findElement(btnWorkMileRecalled).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("CustomerDetailsPage", "mileStone_Recalled_Status", Status.PASS,
							"milestone widget status as recalled is displayed and clicked succesfully");
				}

			}

			else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "mileStone_Recalled_Status", Status.FAIL,
						"milestone widget status as recalled is  not displayed and clicked succesfully");
			}

		} catch (NoSuchElementException e) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "mileStone_Recalled_Status", Status.FAIL,
					"Object not found" + e.getMessage());
		}
	}

	// --------------------------------------------------Locators-------------------------------
	protected By nextPage = By.xpath("//a[contains(@aria-label, 'Next page')]");
	public By ECRlabel = By.xpath("//div[contains(@class, 'credit-summary')]/h3");
	String destinationCheck = "//input[contains(@id, 'destinations')]";
	String vccCheck = "//input[contains(@id, 'vcc')]";
	String rewardsVisa = "//input[contains(@id, 'visa')]";
	String vccSecon = "]/td[6]/div/div/input[contains(@id, 'vcc')]";
	String rewardSecon = "]/td[7]/div/div/input[contains(@id, 'visa')]";
	protected By eCreditLink = By.xpath("//button[text()='Start eCredit']");
	public By getStartedLink = By.xpath("//button[text()='Get Started']");
	protected By custOnTour = By.xpath("//tr[contains(@class, 'ng-star')]/td");
	protected By creditBandDest1 = By.xpath("(//div[contains(@class, 'custom')]/span/img)[1]");
	protected By creditBand = By.xpath("//div[contains(@class, 'details-card')]/img[@alt='credit-score']");
	String vCCAmtImg = "]/td[6]/div/div/div[contains(@class, 'credit-black')]/span";
	String destCredIcon = "]/td[5]/div/span/img[contains(@class, 'ews-tour')]";
	String vCCCredIcon = "]/td[6]//span/img[contains(@class, 'ews-tour')]";
	String rewardCredIcon = "]/td[7]/div/span/img[contains(@class, 'ews-tour')]";
	String rewardAmt = "]/td[7]/div/div/div[contains(@class, 'credit-black')]/span";
	protected By custName = By.xpath("//tbody/tr/td/div/div[contains(@class, 'media-body')]/h5");
	protected By addCusBtn = By.xpath("//span[contains(text(),'Add New Customer')]");
	protected By firstNameTxt = By.xpath("//input[contains(@placeholder, 'First Name')]");
	protected By lastNameTxt = By.xpath("//input[contains(@placeholder, 'Last Name')]");
	protected By addContBtn = By.xpath("//button[text()='Add & Continue']");
	protected By backHomeLink = By.xpath("//span[text()='Back to Homepage']");

	// -------------------------------------Functions---------------------------------------------------
	//// img[@alt='credit-score' and contains(@src,'hard.png')]
	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name (1): clickdestinationCheck()
	 * @Description : This function is used to click on the Wyndham Destination
	 *              Checkbox.
	 * @Date : March, 16, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void clickdestinationCheck() throws Exception {

		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, ECRlabel, "ExplicitWaitLongestTime");

		List<WebElement> listCustomer = driver.findElements(By.xpath(destinationCheck));
		int count = listCustomer.size();
		// Always click on the most recent/ last row customer
		clickElementJSWithWait(listCustomer.get(listCustomer.size() - 1));
		// System.out.println(listCustomer.get(listCustomer.size()-1).isSelected());

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// checkLoadingSpinnerEWS();
		tcConfig.updateTestReporter("CustomerDetailsPage", "clickdestinationCheck", Status.PASS,
				"Clicked on Wyndham Destination Financing checkbox");

	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name (2): checkVCC()
	 * @Description : This function is used to click on the VCC Checkbox.
	 * @Date : March, 16, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void checkVCC() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, ECRlabel, "ExplicitWaitLongestTime");
			List<WebElement> listVcc = driver.findElements(By.xpath(vccCheck));
			int count = listVcc.size();
			// Always click on the most recent/ last row customer
			clickElementJSWithWait(listVcc.get(listVcc.size() - 1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("CustomerDetailsPage", "checkVCC", Status.PASS, "Clicked on VCC checkbox");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("CustomerDetailsPage", "checkVCC", Status.FAIL,
					"Clicked on VCC checkbox unsuccessfull" + e.getMessage());

			// e.printStackTrace();
		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name :(3) checkRewards()
	 * @Description : This function is used to click on the Rewards Visa
	 *              Checkbox.
	 * @Date : March, 16, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void checkRewards() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> listVisa = driver.findElements(By.xpath(rewardsVisa));
			int count = listVisa.size();
			// Always click on the most recent/ last row customer
			clickElementJSWithWait(listVisa.get(listVisa.size() - 1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// checkLoadingSpinnerEWS();
			tcConfig.updateTestReporter("CustomerDetailsPage", "checkRewards", Status.PASS,
					"Clicked on Rewards Visa checkbox");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("CustomerDetailsPage", "checkRewards", Status.FAIL,
					"Clicked on Rewards Visa checkbox unsuccessfull" + e.getMessage());

			// e.printStackTrace();
		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name (4): clickToStarteCredit()
	 * @Description : This function is used to click on the Start eCredit button
	 * @Date : March, 19, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void clickToStarteCredit() throws Exception {

		// waitForSometime(tcConfig.getConfig().get("LongWait"));
		pageDown();
		waitUntilElementVisibleBy(driver, eCreditLink, 60);

		if (verifyElementDisplayed(driver.findElement(eCreditLink))) {
			pageDown();
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToStarteCredit", Status.PASS,
					"Located StarteCredit successfully");
			driver.findElement(eCreditLink).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			checkLoadingSpinnerEWS();
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToStarteCredit", Status.PASS,
					"Clicked on Start eCredit link and User is successfully navigated");
		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToStarteCredit", Status.FAIL,
					"Clicked on eCredit link unsuccessful");
		}

	}

	/*
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (5): clickToGetStarted()
	 * 
	 * @Description : This function is used to click on the Get Started button
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void clickToGetStarted() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		// waitUntilElementVisibleBy(driver, getStartedLink, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(getStartedLink))) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToGetStarted", Status.PASS,
					"Landed on Page Get Started successfully");
			driver.findElement(getStartedLink).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			checkLoadingSpinnerEWS();
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToGetStarted", Status.PASS,
					"Click on Get Started successfully and User is navigated to Next page");
		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToGetStarted", Status.FAIL,
					"Unable to click Get Started & Navigate");
		}
		checkLoadingSpinnerEWS();
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	// No thanks
	public By resumeBtn = By.xpath("//button[text()='Resume eCredit']");

	public void clickToGetStartedAll() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// pageDown();
		waitUntilElementVisibleBy(driver, resumeBtn, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(resumeBtn))) {
			pageDown();

			driver.findElement(resumeBtn).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			checkLoadingSpinnerEWS();
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToStarteCredit", Status.PASS,
					"Started application for Rewards");
		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "clickToStarteCredit", Status.FAIL,
					"Failed: Started application for Rewards");
		}
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (6):findCustomerRow()
	 * 
	 * @Description : This function is used to find row number of the customer
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Params: Name, LOcator of Customer row
	 * 
	 * @Return: Integer value of row number
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public int findCustomerRow(String name) {
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		int row = 0;
		try {
			List<WebElement> custList = driver.findElements(custName);
			int count = custList.size();
			// Iterate through all rows
			for (int i = 0; i < count; i++) {
				String temp = custList.get(i).getText();

				if (temp.equalsIgnoreCase(name)) {
					row = i + 1;
					System.out.println(temp + "-------if-----matched------" + row);
				} else {
					System.out.println(temp + "-------else-----------" + row);
				}

			}

		} catch (Exception e) {
			e.getMessage();
		}
		return row;
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (7):validateDestCreditBand()
	 * 
	 * @Test data: First Name from Data sheet, Last Name from data sheet
	 * 
	 * @Description : This function is used to verify control panel displays the
	 * Barclays credit band under sales score section
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */
	public void validateDestCreditBand() {
		// waitUntilElementVisibleBy(driver, creditBand, "ExplicitWaitLongestTime");
		// waitUntilElementInVisible(driver, creditBand, "ExplicitWaitLongestTime");

		Boolean creditflag = false;
		int pos = 0;

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);

		// validates the row for which customer credit was applied,

		String destBand = "//tbody/tr[" + pos + destCredIcon;
		System.out.println(destBand + "found position");
		creditflag = verifyObjectDisplayed(By.xpath(destBand));
		WebElement img = driver.findElement(By.xpath(destBand));
		String src = img.getAttribute("src");
		Boolean flag2 = false;
		flag2 = src.contains("locked");
		System.out.println("the value inside the src tag  " + src);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (src.contains("a1") || src.contains("b-") || src.contains("a-") || src.contains("b1-") || src.contains("locked")) {

			tcConfig.updateTestReporter("CustomerDetailsPage", "validateDestCreditBand", Status.PASS,
					"Credit Band for Customer is displayed successfully");

		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateDestCreditBand", Status.FAIL,
					"Credit Band for Customer is not obtained successfully");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (8):valGivenCreditBandDest()
	 * 
	 * @Test data: First Name from Data sheet, Last Name from data sheet, credit
	 * band
	 * 
	 * @Description : This function is used to verify control panel displays the
	 * user given Destination Financing credit band under sales score section
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void valGivenCreditBandDest() {
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean creditflag = false;
		int pos = 0;
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		// always validates the first row
		String userCB = testData.get("CreditBand");
		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied,

		String destBand = "//tbody/tr[" + pos + destCredIcon;
		System.out.println(destBand + "found position");
		WebElement img = driver.findElement(By.xpath(destBand));
		creditflag = verifyElementDisplayed(img);

		String src = img.getAttribute("src");
		System.out.println("the value inside the src tag  " + src);
		if (creditflag == true && src.contains("a1") || src.contains("b1") || src.contains("b") || src.contains("a")) {
			// passOk = 1;
			// System.out.println(passOk + "Proceed");
			tcConfig.updateTestReporter("CustomerDetailsPage", "valGivenCreditBandBarc", Status.PASS,
					"User Given Credit Band for Customer is displayed successfully");
		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "valGivenCreditBandBarc", Status.FAIL,
					"User Given Credit Band for Customer is not displayed successfully");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (9):valGivenCreditBandVCC()
	 * 
	 * @Description : This function is used to verify control panel displays the
	 * declined pending or withold VCC credit band under sales score section
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Test data: First Name from Data sheet, Last Name from data sheet
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void validateCreditBandVCC() {
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean creditflag = false;
		int pos = 0;
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied,

		String vccBand = "//tbody/tr[" + pos + vCCCredIcon;
		System.out.println(vccBand + "found position");
		WebElement img = driver.findElement(By.xpath(vccBand));
		creditflag = verifyElementDisplayed(img);

		String src = img.getAttribute("src");
		System.out.println("the value inside the src tag  " + src);
		if (creditflag == true && src.contains("declined") || src.contains("pending") || src.contains("withold")) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "valGivenCreditBandVCC", Status.PASS,
					"Successful: VCC Credit Band for Customer is displayed declined/ pending/ withold");

		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "valGivenCreditBandVCC", Status.FAIL,
					"Unsuccessful: VCC Credit Band for Customer is displayed declined/ pending/ withold");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (10):validateVCCAprovedAmt()
	 * 
	 * @Description : This function is used to verify control panel displays the
	 * VCC approved amount under Salescore section
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Test data: FirstName, LastName
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */
	public void validateVCCAprovedAmt() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		Boolean flagAmt = false;
		Boolean flagBand = false;
		int pos = 0;

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied,
		// validates the row for which customer credit was applied
		String apprvAmt = "//tbody/tr[" + pos + vCCAmtImg;
		System.out.println(apprvAmt + "  amount found position");
		String vccBand = "//tbody/tr[" + pos + vCCCredIcon;
		System.out.println(vccBand + " bandfound position");
		flagAmt = verifyObjectDisplayed(By.xpath(apprvAmt));
		// flagAmt =
		// verifyElementDisplayed(driver.findElement(By.xpath(apprvAmt)));
		flagBand = verifyObjectDisplayed(By.xpath(vccBand));
		System.out.println(flagAmt + " - " + flagBand);
		if (flagAmt == true || flagBand == true) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateVCCAprovedAmt", Status.PASS,
					"Successfully applied VCC application");
		}

		else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateVCCAprovedAmt", Status.FAIL,
					"Verified Approval Amount Failed");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (11):valGivenCreditBandReward()
	 * 
	 * @Description : This function is used to verify control panel displays the
	 * declined pending or withold Barclays or Rewards credit band under sales
	 * score section
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Test data: First Name from Data sheet, Last Name from data sheet
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void valGivenCreditBandReward() {
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean creditflag = false;
		int pos = 0;
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied,

		String rewardBand = "//tbody/tr[" + pos + rewardCredIcon;
		System.out.println(rewardBand + "found position");
		WebElement img = driver.findElement(By.xpath(rewardBand));
		creditflag = verifyElementDisplayed(img);

		String src = img.getAttribute("src");
		System.out.println("the value inside the src tag  " + src);
		if (creditflag == true || src.contains("error") || src.contains("pending") || src.contains("declined")) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "valGivenCreditBandReward", Status.PASS,
					"Successful: Rewards Credit Band for Customer is displayed declined/ pending/ error");

		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "valGivenCreditBandReward", Status.FAIL,
					"Unsuccessful: Rewards Credit Band for Customer is displayed declined/ pending/ withold");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (12):validateCustomerCreditBand()
	 * 
	 * @Description : This function is used to verify control panel displays the
	 * default credit band after any soft scoring in the Tour details box
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void validateCustomerCreditBand() {
		boolean creditflag = false;
		boolean defaultCreditFlag = false;
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {

			creditflag = verifyElementDisplayed(driver.findElement(creditBand));
			WebElement img = driver.findElement(creditBand);
			String src = img.getAttribute("src");
			System.out.println("the value inside the src tag  " + src);
			if (creditflag == false || src.contains("no-score")) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditBand", Status.FAIL,
						"Credit Band for Customer is not displayed successfully");

			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateCustomerCreditBand", Status.PASS,
						"Credit Band for Customer is displayed successfully");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (13):addCustomer()
	 * 
	 * @Description : This function is used to add a new customer to an existing
	 * Tour
	 * 
	 * @Test Data:
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void addCustomerSecon() {
		waitUntilElementVisibleBy(driver, ECRlabel, "ExplicitWaitLongestTime");

		String firstName = testData.get("SecFirstName");
		String lastName = testData.get("SecLastName");
		hoverOnElement(driver.findElement(addCusBtn));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(firstNameTxt, firstName);
		fieldDataEnter(lastNameTxt, lastName);
		driver.findElement(firstNameTxt).clear();
		fieldDataEnter(firstNameTxt, firstName);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(addContBtn);
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("CustomerDetailsPage", "addCustomer", Status.PASS,
				"Successful- Added new customer");

	}

	public void addCustomerThird() {
		waitUntilElementVisibleBy(driver, ECRlabel, "ExplicitWaitLongestTime");

		String firstName = testData.get("ThirFirstName");
		String lastName = testData.get("ThirLastName");
		hoverOnElement(driver.findElement(addCusBtn));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(firstNameTxt, firstName);
		fieldDataEnter(lastNameTxt, lastName);
		driver.findElement(firstNameTxt).clear();
		fieldDataEnter(firstNameTxt, firstName);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(addContBtn);
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("CustomerDetailsPage", "addCustomerThird", Status.PASS,
				"Successful- Added new customer");

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (14):validateRewardsAmount()
	 * 
	 * @Description : This function is used to validate the rewads amount
	 * 
	 * @Test Data: FirstName, LastName
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void validateRewardsAmount() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		Boolean flagAmt = false;
		Boolean flagBand = false;
		int pos = 0;

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied,

		String apprvAmt = "//tbody/tr[" + pos + rewardAmt;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		System.out.println(apprvAmt + " found Rewards Amount position");

		String rewardBand = "//tbody/tr[" + pos + rewardCredIcon;
		System.out.println(rewardBand + "--found Rewards Band position");
		flagAmt = verifyObjectDisplayed(By.xpath(apprvAmt));
		// flagAmt =
		// verifyElementDisplayed(driver.findElement(By.xpath(apprvAmt)));
		flagBand = verifyObjectDisplayed(By.xpath(rewardBand));
		System.out.println(flagAmt + " - " + flagBand);
		if (flagAmt == true || flagBand == true) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateRewardsAmount", Status.PASS,
					"SUccessfully applied Barclays application");
		}

		else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateRewardsAmount", Status.FAIL,
					"Verified Rewards Approval Amount Failed");
		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name :(15) checkRewardsSecon()
	 * @Description : This function is used to click on the Rewards Visa of
	 *              Second Customer
	 * @Test data: FirstNameSecon, LastNameSecon
	 * @Date : March, 16, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void checkRewardsSecon() throws Exception {
		int pos = 0;

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String firstName = testData.get("SecFirstName");
		String lastName = testData.get("SecLastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied,
		// hardcoded the first part of xpath
		String checkBoxReward = "//tbody/tr[" + pos + rewardSecon;

		System.out.println(checkBoxReward + "found position");

		WebElement rewardChk = driver.findElement(By.xpath(checkBoxReward));
		clickElementJSWithWait(rewardChk);
		/*
		 * List<WebElement> listVisa =
		 * driver.findElements(By.xpath(rewardsVisa)); int count =
		 * listVisa.size(); // Always click on the most recent/ last row
		 * customer clickElementJSWithWait(listVisa.get(listVisa.size() - 1));
		 */
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// checkLoadingSpinnerEWS();
		tcConfig.updateTestReporter("CustomerDetailsPage", "checkRewardsSecon", Status.PASS,
				"Clicked on Seocnd Customer Rewards Visa checkbox");

	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name :(15) checkVCCSecon
	 * @Description : This function is used to click on the VCC Visa of Second
	 *              Customer
	 * @Test data: FirstNameSecon, LastNameSecon
	 * @Date : March, 16, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void checkVCCSecon(String cusNum) throws Exception {
		int pos = 0;

		String cust = cusNum;
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String firstName = testData.get(cust + "FirstName");
		String lastName = testData.get(cust + "LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied, the
		// first part of xpath
		// String checkBoxReward = "//tbody/tr[" + pos + rewardSecon; here
		String checkBoxReward = "//tbody/tr[" + pos + vccSecon;
		System.out.println(checkBoxReward + "found position");

		WebElement rewardChk = driver.findElement(By.xpath(checkBoxReward));
		clickElementJSWithWait(rewardChk);
		/*
		 * List<WebElement> listVisa =
		 * driver.findElements(By.xpath(rewardsVisa)); int count =
		 * listVisa.size(); // Always click on the most recent/ last row
		 * customer clickElementJSWithWait(listVisa.get(listVisa.size() - 1));
		 */
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// checkLoadingSpinnerEWS();
		tcConfig.updateTestReporter("CustomerDetailsPage", "checkVCCSecon", Status.PASS,
				"Clicked on Seocnd Customer Rewards Visa checkbox");

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (14):validateRewardsAmount()
	 * 
	 * @Description : This function is used to validate the rewads amount
	 * 
	 * @Test Data: FirstName, LastName
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void validateRewardsAmountMulti(String custNum) throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean flag = false;
		Boolean creditflag = false;
		int pos = 0;

		String cust = custNum;

		String firstName = testData.get("FirstName" + cust);
		String lastName = testData.get("LastName" + cust);
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);
		// validates the row for which customer credit was applied,

		String apprvAmt = "//tbody/tr[" + pos + rewardAmt;
		System.out.println(apprvAmt + "found Rewards position");
		flag = verifyElementDisplayed(driver.findElement(By.xpath(apprvAmt)));
		String rewardBand = "//tbody/tr[" + pos + rewardCredIcon;
		System.out.println(rewardBand + "found position");
		creditflag = verifyElementDisplayed(driver.findElement(By.xpath(rewardBand)));

		if (flag == true) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateRewardsAmountMulti", Status.PASS,
					"Second Customer Verified Rewards Approval Amount");
		} else if (creditflag == true) {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateRewardsAmountMulti", Status.PASS,
					"Second Customer Rewards applied but not approved amount");
		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateRewardsAmountMulti", Status.FAIL,
					"Second Verified Rewards Approval Amount Failed");
		}

	}

	public void validateVCCAmountMulti(String custNum) throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean flag = false;
		Boolean creditflag = false;
		int pos = 0;
		try {
			String cust = custNum;

			String firstName = testData.get(cust + "FirstName");
			String lastName = testData.get(cust + "LastName");
			String name = firstName + " " + lastName;
			System.out.println(name + "------------------");
			// Finds the row number for the customer whose score need to be
			// checked
			pos = findCustomerRow(name);
			// validates the row for which customer credit was applied,
			// hardcoded the first part of xpath
			String apprvAmt = "//tbody/tr[" + pos + vCCAmtImg;
			System.out.println(apprvAmt + "found VCC position");
			flag = verifyElementDisplayed(driver.findElement(By.xpath(apprvAmt)));
			WebElement img = driver.findElement(By.xpath(apprvAmt));
			String amount = img.getText();
			System.out.println(amount + "VCC approved Amount");
			flag = verifyElementDisplayed(driver.findElement(By.xpath(apprvAmt)));
			// if not approved
			String vccBand = "//tbody/tr[" + pos + vCCCredIcon;
			System.out.println(vccBand + "found band VCC position");
			creditflag = verifyElementDisplayed(driver.findElement(By.xpath(vccBand)));
			WebElement imgBand = driver.findElement(By.xpath(vccBand));
			String src = imgBand.getAttribute("src");
			System.out.println("the value inside the src tag  " + src);

			if (flag == true || amount != "" || creditflag == true) {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateVCCAmountMulti", Status.PASS,
						"Second Customer Verified VCC Approval Amount");
			} else {
				tcConfig.updateTestReporter("CustomerDetailsPage", "validateVCCAmountMulti", Status.FAIL,
						"Second Verified VCC Approval Amount Failed");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateVCCAmountMultii", Status.FAIL,
					"Verified VCC Approval Amount Failed" + e.getMessage());

			// e.printStackTrace();
		}

	}

	// Function: This functio is used to navigate to the Homepage from ECR
	public void navigateToHome() throws Exception {

		waitUntilElementVisibleBy(driver, backHomeLink, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(backHomeLink))) {
			clickElementJSWithWait(backHomeLink);
			// driver.findElement(backHomeLink).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			checkLoadingSpinnerEWS();
			tcConfig.updateTestReporter("CustomerDetailsPage", "navigateToHome", Status.PASS,
					"Click on Get Started successfully and User is navigated to Home Page DAshboard page");
		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "navigateToHome", Status.FAIL,
					"Unable to click Get Started & Navigate");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (7):validateDestCreditBandWith()
	 * 
	 * @Test data: First Name from Data sheet, Last Name from data sheet
	 * 
	 * @Description : This function is used to verify control panel displays the
	 * witheld credit band
	 * 
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */
	public void validateDestCreditBandWith() {
		// waitUntilElementVisibleBy(driver, creditBand, "ExplicitWaitLongestTime");
		// waitUntilElementInVisible(driver, creditBand, "ExplicitWaitLongestTime");

		Boolean creditflag = false;
		int pos = 0;

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		String firstName = testData.get("FirstName");
		String lastName = testData.get("LastName");
		String name = firstName + " " + lastName;
		System.out.println(name + "------------------");
		// Finds the row number for the customer whose score need to be
		// checked
		pos = findCustomerRow(name);

		// validates the row for which customer credit was applied,
		// hardcoded the first part of xpath
		String destBand = "//tbody/tr[" + pos + destCredIcon;
		System.out.println(destBand + "found position");
		creditflag = verifyElementDisplayed(driver.findElement(By.xpath(destBand)));
		WebElement img = driver.findElement(By.xpath(destBand));
		String src = img.getAttribute("src");

		System.out.println("the value inside the src tag  " + src);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (creditflag == true || src.contains("withheld")) {

			tcConfig.updateTestReporter("CustomerDetailsPage", "validateDestCreditBandWith", Status.PASS,
					"Witheld Credit Band for Customer is displayed successfully");

		} else {
			tcConfig.updateTestReporter("CustomerDetailsPage", "validateDestCreditBandWith", Status.FAIL,
					"Withheld Credit Band for Customer is not displayed successfully");
		}

	}

}
