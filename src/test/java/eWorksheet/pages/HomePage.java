package eWorksheet.pages;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class HomePage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public HomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	//
	protected By sideMenu = By.xpath("//img[@class='img-fluid' and @alt='icon-toggle-menu']");
	protected By defaultLocation = By.xpath("//div[@class='ews-gallery-card-overlay']//h4");
	protected By sideMenuClose = By.xpath("//a[@class='close-link']//img[@alt ='icon-close']");
	protected By changeLocation = By.xpath("//div[@class='ews-gallery-card-overlay']//a[contains(., Change)]");
	protected By selectLocation = By.xpath("//a[text()='" + testData.get("DefaultSalesSite") + "']");

	//
	protected By homeLogo = By.xpath("//img[@class='img-fluid' and @alt='eworksheet-logo']");
	protected By dashboardHeader = By.xpath("//a[contains(text(),'Homepage')]");
	protected By myToursLabel = By.xpath("//label[contains(text(),'MY TOURS')]");
	protected By alltoursButton = By.xpath("//a[contains(text(),'All Tours')]");
	protected By menuIcon = By.xpath("//img[@class='img-fluid' and @alt='icon-toggle-menu']");
	protected By menuIconClose = By.xpath("//a[@class='close-link']//img[@alt ='icon-close']");
	protected By defaultSalesSite = By.xpath("//h4[contains(text(),'" + testData.get("DefaultSalesSite") + "')]");
	protected By logOut = By.xpath("//button[@class='btn btn-logout' and text()='Logout']");
	protected By role = By.xpath("//h4[contains(text(),'" + testData.get("Role") + "')]");
	protected By loc = By.xpath("//a[contains(text(),'" + testData.get("SalesLocation") + "')]");
	protected By changeLoc = By.xpath("//a[contains(text(),' Change Location ')]");
	protected By changeLocButton = By.xpath(
			"//div[@class='custom-card ews-change-location-card open']//div[@class='custom-card-footer ews-change-location-card-footer']// button[text()='Set Location']");
	protected By selectTourDate = By
			.xpath("//div[@class='input-group']/input[@placeholder='Datepicker' and @type='text']");
	protected By tourHomePage = By.xpath("//div[@class='swiper-wrapper ']");
	protected By toggleMenu = By.xpath("//li[@class='nav-item ews-menu-item']/img");
	protected By changeLocationLink = By.xpath("//a[contains(text(),' Change Location ')]");
	protected By availableSiteList = By.xpath(
			/* "//label[contains(text(),'Your available locations:')])//following::a[@class='list-group-item list-group-item-action']" */
			"//label[contains(text(),'Your available locations:')]//following::a");
	protected By siteNameinGallery = By.xpath("//div[@class='ews-gallery-card']//following::h4");
	protected By myTourCount = By.xpath("//label[text()='MY TOURS ']/span");
	protected By creditScore = By.xpath(
			"//img[@alt='credit-score' and contains(@src,'credit-hard.png') or contains(@src,'credit-soft.png')]");
	protected By checkInTour = By.xpath("//p[text()='Time since check-in : ']/strong");
	protected By tourIdCount = By.xpath("//th[text()='Tour Id']/../../../tbody/tr/td[10]");
	protected By homeTab = By.xpath("//span[contains(.,'Home')]");
	protected By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	protected By menuicon = By.xpath("//button[contains(@class,'slds-button')]//div[contains(@class,'slds-icon')]");
	protected By txtSearchBy = By.xpath("//input[contains(@placeholder,'Search apps')]");
	protected By viewAllBtn = By.xpath("//button[text()='View All']");

	// Function to select the side drop-down menu
	public void selectSideMenu() {
		try {
			waitUntilElementVisibleBy(driver, sideMenu, "ExplicitWaitLongestTime");
			driver.findElement(sideMenu).click();
		} catch (Exception e) {
			e.getMessage();

		}
	}

	public void navigateToMenu(String Iterator) {
		 waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, homeTab, "ExplicitWaitLongestTime");
		// checkLoadingSpinnerEWS();
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, menuicon, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(menuicon))) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		clickElementBy(menuicon);
		waitUntilElementVisibleBy(driver, viewAllBtn, "ExplicitWaitLongestTime");
		clickElementBy(viewAllBtn);
		// waitUntilElementVisible(driver, txtSearchBy, "ExplicitWaitLongestTime");
		waitUntilElementVisibleBy(driver, txtSearchBy, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(txtSearchBy).sendKeys(testData.get("Menu" + Iterator));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//mark[text()='" + testData.get("Menu" + Iterator) + "']")).click();
		// waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void verifyHomeHeader() {

		checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, dashboardHeader, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(dashboardHeader))) {
			tcConfig.updateTestReporter("HomePage", "verifyHomeHeader", Status.PASS,
					"Navigated to eWorksheet successfully");

		} else {

			tcConfig.updateTestReporter("HomePage", "verifyHomeHeader", Status.FAIL, "Not Navigated to eWorksheet");
		}

	}

	public void verifymyToursLabel() {

		checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, myToursLabel, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(myToursLabel))) {
			tcConfig.updateTestReporter("HomePage", "verifymyToursLabel", Status.PASS, "My Tour label is displayed");

		} else {

			tcConfig.updateTestReporter("HomePage", "verifymyToursLabel", Status.FAIL,
					"My Tour label is not displayed");
		}

	}

	public void verifyRoles() {

		checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, menuIcon, "ExplicitWaitLongestTime");
		clickElementBy(menuIcon);
		waitUntilElementVisibleBy(driver, role, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(role))) {
			tcConfig.updateTestReporter("HomePage", "verifyRoles", Status.PASS, "Role is successfully displayed");

		} else {

			tcConfig.updateTestReporter("HomePage", "verifyRoles", Status.FAIL,
					"Role is not successfully displayed");
		}
		// clickElementBy(menuIconClose);
	}

	public void verifyDefaultSalesLocation() {

		checkLoadingSpinnerEWS();
		/*
		 * waitUntilElementVisibleBy(driver, toggleMenu, "ExplicitWaitLongestTime");
		 * driver.findElement(toggleMenu).click();
		 */
		waitUntilElementVisibleBy(driver, defaultSalesSite, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(driver.findElement(defaultSalesSite))) {
			tcConfig.updateTestReporter("HomePage", "verifyDefaultSalesLocation", Status.PASS,
					"Default sale site is displayed successfully");

		} else {

			tcConfig.updateTestReporter("HomePage", "verifyDefaultSalesLocation", Status.FAIL,
					"Default sale site is not displayed successfully");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(menuIconClose);
	}

	public void changeSalesLocation() {

		checkLoadingSpinnerEWS();
		String ChngLocation = driver.findElement(defaultSalesSite).getText();
		waitUntilElementVisibleBy(driver, changeLoc, "ExplicitWaitLongestTime");
		clickElementBy(changeLoc);
		waitUntilElementVisibleBy(driver, loc, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(driver.findElement(loc))) {
			clickElementBy(loc);
			waitUntilElementVisibleBy(driver, changeLocButton, "ExplicitWaitLongestTime");
			clickElementBy(changeLocButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(siteNameinGallery).getText() != ChngLocation) {
				tcConfig.updateTestReporter("HomePage", "changeSalesLocation", Status.PASS,
						"Sales location is changed successfully");

			} else {

				tcConfig.updateTestReporter("HomePage", "changeSalesLocation", Status.FAIL,
						"Sales location is not changed successfully");
			}
		} else {
			tcConfig.updateTestReporter("HomePage", "changeSalesLocation", Status.FAIL,
					"Selected sales location is not visible");
		}
		clickElementBy(menuIconClose);
	}

	public void changeDefaultLocation() {
		try {
			checkLoadingSpinnerEWS();
			waitUntilElementVisibleBy(driver, changeLoc, "ExplicitWaitLongestTime");
			clickElementBy(changeLoc);
			waitUntilElementVisibleBy(driver, loc, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(loc))) {
				clickElementBy(loc);
				waitUntilElementVisibleBy(driver, changeLocButton, "ExplicitWaitLongestTime");
				clickElementBy(changeLocButton);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				tcConfig.updateTestReporter("HomePage", "changeSalesLocation", Status.PASS,
						"Sales location is changed successfully");
			} else {
				tcConfig.updateTestReporter("HomePage", "changeSalesLocation", Status.FAIL,
						"Selected sales location is not visible");
			}
			clickElementBy(menuIconClose);
		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "changeSalesLocation", Status.FAIL,
					"Selected sales location is not visible" + e.getMessage());
		}

	}

	public void verifyAllTourButton() {

		waitUntilElementVisibleBy(driver, alltoursButton, "ExplicitWaitLongestTime");
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(alltoursButton))) {
			tcConfig.updateTestReporter("HomePage", "verifyAllTourButton", Status.PASS,
					"All Tour button is displayed");
			clickElementBy(alltoursButton);
			pageCheck();

		} else {

			tcConfig.updateTestReporter("HomePage", "verifyAllTourButton", Status.FAIL,
					"All Tour button is not displayed");
		}

	}

	
	protected By allTourTable = By.xpath("//table[contains(@class,'all-tours')]");

	public void verifyallToursTable() {

		waitUntilElementVisibleBy(driver, allTourTable, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(allTourTable))) {
			tcConfig.updateTestReporter("All Tour Dashboard Page", "verifyallToursTable", Status.PASS,
					"All Tour Dashboard table is displayed");
		} else {
			tcConfig.updateTestReporter("All Tour Dashboard Page", "verifyallToursTable", Status.FAIL,
					"All Tour Dashboard table is not displayed");
		}

	}

	public void verifyToSelectDatePicker() throws Exception {

		String strDate = testData.get("TourDate");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, selectTourDate, 240);
		if (selectDatePicker(selectTourDate, strDate)) {
			tcConfig.updateTestReporter("HomePage", "VerifyToSelectDatePicker", Status.PASS,
					"Date is selected from the date picker");
		} else {
			tcConfig.updateTestReporter("HomePage", "VerifyToSelectDatePicker", Status.FAIL,
					"Date is not selected from the date picker");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	public boolean selectDatePicker(By by, String date) throws Exception {
		boolean isSelected = false;
		try {
			String date_dd_MM_yyyy[] = (date.split("/"));
			int year = Integer.parseInt(date_dd_MM_yyyy[2]);
			int month = Integer.parseInt(date_dd_MM_yyyy[1]);
			int day = Integer.parseInt(date_dd_MM_yyyy[0]);
			WebElement selectDate = driver.findElement(by);
			waitUntilElementVisible(driver, selectDate, 120);
			clickElementWithJS(selectDate);
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM");
			LocalDateTime now = LocalDateTime.now();
			int monthsDiff = (Integer.parseInt(dtf.format(now)) - month);
			if (monthsDiff == 1 || monthsDiff == -11) {
				WebElement previousLink = driver.findElement(By.xpath("//button[@class='previous']"));
				waitUntilElementVisible(driver, previousLink, 120);
				previousLink.click();
			}

			// get all dates from calendar to select correct one
			List<WebElement> list_AllDateToBook = driver.findElements(By.xpath(
					"//div[@class='bs-datepicker-body']//table//tbody//td[not(contains(@class,'week ng-star-inserted'))]/span[not(contains(@class,'disabled')) and not(contains(@class,'is-other-month'))]"));
			for (WebElement webElement : list_AllDateToBook) {
				if (webElement.getText().equalsIgnoreCase(date_dd_MM_yyyy[0])) {
					clickElementWithJS(webElement);
					break;
				}
			}
			isSelected = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSelected;

	}

	public void selectTourByTourID() {
		String strTourID = testData.get("TourID");

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, tourHomePage, "ExplicitWaitLongestTime");
		List<WebElement> allTourDashboardDetails = driver.findElements(tourHomePage);
		for (WebElement element : allTourDashboardDetails) {
			waitUntilElementLocated(driver, tourHomePage, 240);
			String attrTourID = element.getAttribute("id");
			if (attrTourID.equals(strTourID)) {
				clickElementJSWithWait(element);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				break;
			}
		}

	}

	public void verifyTourID() {
		String strTourID = LeadsPage.createdTourID;

		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> allTourDashboardDetails = driver.findElements(tourHomePage);
			for (WebElement element : allTourDashboardDetails) {
				waitUntilElementLocated(driver, tourHomePage, 240);
				String attrTourID = element.getAttribute("id");
				if (attrTourID.equals(strTourID)) {
					tcConfig.updateTestReporter("HomePage", "verifyTourID", Status.PASS,
							"Tour ID" + LeadsPage.createdTourID + " is successfully assigned to Sales Agent");
					break;
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "verifyTourID", Status.FAIL,
					"Tour ID" + LeadsPage.createdTourID + " is not successfully assigned to Sales Agent");
		}

	}

	protected By backLink = By.xpath("(//a[@class='back-link']//img)[2]");

	public void getSitesLocationName() {
		boolean isSitesSorted = false;
		String strSiteName = "";
		ArrayList<String> allList = new ArrayList<String>();
		try {
			checkLoadingSpinnerEWS();
			waitUntilElementVisibleBy(driver, toggleMenu, "ExplicitWaitLongestTime");
			driver.findElement(toggleMenu).click();
			waitUntilElementVisibleBy(driver, changeLocationLink, "ExplicitWaitLongestTime");
			driver.findElement(changeLocationLink).click();
			List<WebElement> allSiteAvailableList = driver.findElements(availableSiteList);
			for (WebElement element : allSiteAvailableList) {
				strSiteName = element.getText();
				allList.add(strSiteName);

			}
			isSitesSorted = verifySiteNameInAlphabeticalOrder(allList);
			if (isSitesSorted == false) {
				tcConfig.updateTestReporter("HomePage", "getSitesLocationName", Status.PASS,
						"Sites name is in Alphabetical Order");
			} else {
				tcConfig.updateTestReporter("HomePage", "getSitesLocationName", Status.FAIL,
						"Sites name is not in Alphabetical Order");
			}

			
			clickElementBy(backLink);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, changeLocationLink, "ExplicitWaitLongestTime");
			clickElementBy(menuIconClose);

		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "getSitesLocationName", Status.FAIL,
					"Sites name is not in Alphabetical Order");
		}
	}

	public boolean verifySiteNameInAlphabeticalOrder(ArrayList<String> arraylist) {
		boolean isSorted = true;
		try {
			for (int i = 1; i < arraylist.size(); i++) {
				String str = arraylist.get(i - 1);
				String str2 = arraylist.get(i);
				if (arraylist.get(i - 1).compareToIgnoreCase(arraylist.get(i)) > 0) {
					isSorted = false;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isSorted;

	}

	public void validateCurrentDate() {
		String strMaxdateTime = "";
		String strDateTime = "";
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			strMaxdateTime = driver.findElement(selectTourDate).getAttribute("ng-reflect-max-date").substring(0, 24);
			strDateTime = driver.findElement(selectTourDate).getAttribute("ng-reflect-min-date").substring(0, 24);
			SimpleDateFormat parser = new SimpleDateFormat("EEE MMM d yyyy HH:mm:ss");
			Date maxdate = parser.parse(strMaxdateTime);
			Date mindate = parser.parse(strDateTime);
			String strMaxDatefromUI = formatter.format(maxdate);
			String strDatefromUI = formatter.format(mindate);
			Date d1 = formatter.parse(strDatefromUI);
			Date d2 = formatter.parse(strMaxDatefromUI);
			Date date1 = new Date();
			String strCurrentDate = formatter.format(date1);
			long numberOfDays = daysBetween(d1, d2);
			if ((strMaxDatefromUI.equals(strCurrentDate)) && numberOfDays <= 30)

			{
				tcConfig.updateTestReporter("HomePage", "validateCurrentDate", Status.PASS,
						"Todays date is displayed successfully in the date picker");
			} else {
				tcConfig.updateTestReporter("HomePage", "validateCurrentDate", Status.FAIL,
						"Todays date is not displayed successfully in the date picker");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	protected static long daysBetween(Date one, Date two) {
		long difference = (one.getTime() - two.getTime()) / 86400000;
		return Math.abs(difference);
	}

	public void myTourCountValidation() {
		try {
			String strMyTourCnt = driver.findElement(myTourCount).getText();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> allTourDashboardDetails = driver.findElements(tourHomePage);
			int tourCount = allTourDashboardDetails.size();
			if (Integer.parseInt(strMyTourCnt) == tourCount | tourCount == 1) {
				tcConfig.updateTestReporter("HomePage", "myTourCountValidation", Status.PASS,
						"Tour ID count" + tourCount + "is displayed successfully");
			} else {
				tcConfig.updateTestReporter("HomePage", "myTourCountValidation", Status.FAIL,
						"Tour ID count" + tourCount + "is not displayed successfully");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void scoreCheck() {
		boolean flag = false;
		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> allTourDashboardDetails = driver.findElements(tourHomePage);
			for (int i = 1; i <= allTourDashboardDetails.size(); i++) {
				flag = verifyElementDisplayed(driver.findElement(creditScore));
				if (flag == true) {
					tcConfig.updateTestReporter("HomePage", "scoreCheck", Status.PASS,
							"Credit score is displayed successfully for Tour" + " " + i);
				} else {
					tcConfig.updateTestReporter("HomePage", "scoreCheck", Status.FAIL,
							"Credit score is not displayed successfully for Tour" + " " + i);
				}
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void validateTourCheckInTime() {
		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> list = driver.findElements(checkInTour);
			for (int i = 1; i <= list.size(); i++) {
				String strCheckIn = driver
						.findElement(By.xpath("(//p[text()='Time since check-in : ']/strong)[" + i + "]")).getText();
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (strCheckIn.isEmpty() == false) {
					tcConfig.updateTestReporter("HomePage", "scoreCheck", Status.PASS,
							"Tour checkin time is displayed successfully for tour" + " " + i);
				} else {
					tcConfig.updateTestReporter("HomePage", "scoreCheck", Status.FAIL,
							"Tour checkin time is not displayed successfully for tour" + " " + i);
				}
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void validateTourAssignToUser() {
		String strTourID[] = testData.get("TourID").split(";");

		try {
			int i;
			List<WebElement> list = driver.findElements(tourIdCount);
			for (i = 1; i <= list.size(); i++) {
				String strGetTourID = driver
						.findElement(By.xpath("//th[text()='Tour Id']/../../../tbody/tr[" + i + "]/td[10]")).getText();
				if (strGetTourID.equals(strTourID[i - 1])) {
					tcConfig.updateTestReporter("HomePage", "validateTourAssignToUser", Status.PASS,
							"User is able to view assigned active Tour ID " + strGetTourID + " " + "Successfully");
				} else {
					tcConfig.updateTestReporter("HomePage", "validateTourAssignToUser", Status.FAIL,
							"User is not able to view assigned active Tour ID " + strGetTourID + " " + "Successfully");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "validateTourAssignToUser", Status.FAIL,
					"User is not able to view assigned active Tour ID Successfully");
		}
	}

	public void verifyandChangeDefaultLocation() {
		String defaultSetLocation = "";
		try {
			waitUntilElementVisibleBy(driver, sideMenu, "ExplicitWaitLongestTime");
			driver.findElement(sideMenu).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			defaultSetLocation = driver.findElement(defaultLocation).getText();
			if (defaultSetLocation.equals("WYNDHAM DAYTONA")) {
				tcConfig.updateTestReporter("HomePage", "verifyandChangeDefaultLocation", Status.PASS,
						defaultSetLocation + " is selected " + "Successfully");
				driver.findElement(sideMenuClose).click();

			} else {
				waitUntilElementVisibleBy(driver, changeLocation, "ExplicitWaitLongestTime");
				driver.findElement(changeLocation).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(selectLocation).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(changeLocButton).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				defaultSetLocation = driver.findElement(defaultLocation).getText();
				if (defaultSetLocation.equals("WYNDHAM DAYTONA")) {
					tcConfig.updateTestReporter("HomePage", "verifyandChangeDefaultLocation", Status.PASS,
							defaultSetLocation + " is selected " + "Successfully");
					driver.findElement(sideMenuClose).click();
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "verifyandChangeDefaultLocation", Status.FAIL,
					"Failed in method " + e.getMessage());
		}
	}

	public void verifyandchangeLocation() {
		String defaultSetLocation = "";
		String strSiteName = "";
		String selectedSitName = "";
		try {

			waitUntilElementVisibleBy(driver, sideMenu, "ExplicitWaitLongestTime");
			driver.findElement(sideMenu).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			defaultSetLocation = driver.findElement(defaultLocation).getText().trim();
			if (!defaultSetLocation.equals("")) {
				tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.PASS,
						"Default Location" + defaultSetLocation + " is Displayed ");
			}
			waitUntilElementVisibleBy(driver, changeLocation, "ExplicitWaitLongestTime");
			driver.findElement(changeLocation).click();
			waitUntilElementVisibleBy(driver, driver.findElement(availableSiteList), "ExplicitWaitLongestTime");
			List<WebElement> allSiteAvailableList = driver.findElements(availableSiteList);
			for (int i = 0; i <= allSiteAvailableList.size();) {
				strSiteName = allSiteAvailableList.get(i).getText().trim();
				if (!defaultSetLocation.equals(strSiteName)) {
					allSiteAvailableList.get(i).click();
					tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.PASS,
							allSiteAvailableList.get(i).getText() + " is selected " + "Successfully");
					selectedSitName = allSiteAvailableList.get(i).getText();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(changeLocButton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					defaultSetLocation = driver.findElement(defaultLocation).getText();
					if (defaultSetLocation.equals(selectedSitName)) {
						tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.PASS,
								selectedSitName + " is displayed " + "Successfully");
						driver.findElement(sideMenuClose).click();
					} else {
						tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.FAIL,
								selectedSitName + " is not displayed " + "Successfully");
					}

				} else {
					allSiteAvailableList.get(i + 1).click();
					tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.PASS,
							allSiteAvailableList.get(i + 1).getText() + " is selected " + "Successfully");
					selectedSitName = allSiteAvailableList.get(i + 1).getText();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(changeLocButton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					defaultSetLocation = driver.findElement(defaultLocation).getText();
					if (defaultSetLocation.equals(selectedSitName)) {
						tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.PASS,
								selectedSitName + " is displayed " + "Successfully");
						driver.findElement(sideMenuClose).click();
					} else {
						tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.FAIL,
								selectedSitName + " is not displayed " + "Successfully");
					}
				}
				break;
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "verifyandchangeLocation", Status.FAIL,
					"Failed in method " + e.getMessage());

		}
	}

	protected By assignTeams = By.xpath("//a[contains(text(),' Assign Team ')]");

	public void clickAssignTeamOption() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(assignTeams);
		
	}

}
