package eWorksheet.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import eWorksheet.pages.CWAPackageWSAPage;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CWAPackageWSAPage_Ipad extends CWAPackageWSAPage {

	public static final Logger log = Logger.getLogger(CWAPackageWSAPage_Ipad.class);

	public CWAPackageWSAPage_Ipad(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By bonusDetailsPopUp = By.xpath("//p[text()='Bonus Points Details']");
	protected By bonusInfoLink = By.xpath("//td[text()=' Bonus Points']/i");
	protected By SelectLink = By.xpath("//li[@role='menuitem']/a[contains(text(),'Select')]");
	protected By selectBtn = By.xpath("//button[text()='Select']");
	protected By WSALabel = By.xpath("//span[starts-with(text(),'WSA')]");
	protected By cwaPackagePoints = By
			.xpath("//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong");
	protected By retrieveLink = By.xpath("//a[text()='Retrieve']");
	protected By lookUpDrpDwn = By.xpath("//select[@name='searchType']");
	protected By refNumTxt = By.xpath("//input[@id='controlNumber']");
	protected By submitBtn = By.xpath("//button[@name='lu-submit']");
	protected By refNumColVal = By.xpath("//table[@id='lookupResultsTable']/tbody/tr/td[1]");
	protected By clickToView = By.xpath("//table[@id='lookupResultsTable']/tbody/tr/td[6]//img[@title='Click to View']");
	protected By summaryLabel = By.xpath("//div[contains(text(),'Summary')]");
	protected By privacyLabel = By.xpath("//div[@class='down-payment-footer']/p");
	protected By ownersPrivacyLink = By.xpath("(//div[@class='legal-name-footer']/p)[1]");
	protected By clickHereLink = By.xpath("//div[@class='down-payment-footer']/p/a");
	protected By OwnerclickHereLink = By.xpath("(//div[@class='legal-name-footer']/p/a)[1]");
	protected By privacyModalWindow = By.xpath("//h3[text()=' Wyndham Privacy Statement ']");
	protected By iconCloseBtn = By.xpath("//h3[text()=' Wyndham Privacy Statement ']//following::img[@alt='icon-close']");
	protected By cashBtn = By.xpath("(//ul[@class='list-inline select-payment-list']/li/span)[3]");
	protected By addPayBtn = By.xpath(
			"(//div[@class='card-footer down-payment-card-footer']//following-sibling::button[text()='Add Payment'])[3]");
	protected By continueFinanceBtn = By.xpath("//button[text()='Continue to Financing']");
	protected By continueOwnBtn = By.xpath("//button[text()='Continue to Owners']");
	protected By addressLink = By
			.xpath("(//div[contains(@class,'personal-information')]//following::button[text()=' Address '])[1]");
	protected By contactInfoLink = By.xpath("//button[text()=' Contact Information ']");
			/*"//tab[@ng-reflect-active='true']//button[text()=' Contact Information ']"*/
	protected By custAddChkBox = By.xpath(
			"//label[text()='Continue with the Customer supplied address:']//following::input[@id='0']/../label");
	
	protected By selectPrimaryPhone = By.xpath("//select[@formcontrolname='primaryPhoneType']");
	protected By selectAlterPhone = By.xpath("//select[@formcontrolname='alternatePhoneType']");
	protected By saveContinueBtn = By.xpath(/*"//button[text()=' Save and Continue ']"*/
			"//tab[@ng-reflect-active='true']//button[text()=' Save and Continue ']");
	protected By continueSummary = By.xpath("//button[text()=' Continue to Summary ']");
	protected By standardBenefitChkBx = By.xpath("//label[@for='standard-benefits-checkbox']");
	protected By addNewOwner = By.xpath("//span[text()='Owner Information']//following::a[text()='Add New Owner']");
	protected By continueBtn = By.xpath("(//button[text()='Continue'])[2]");
	protected By firstName = By.xpath("//tab[@ng-reflect-active='true']//following::input[@ng-reflect-name='firstName']");
	protected By lastName = By.xpath("//tab[@ng-reflect-active='true']//following::input[@ng-reflect-name='lastName']");
	protected By dateOfBirth = By
			.xpath("//tab[@ng-reflect-active='true']//following::input[@ng-reflect-name='dateOfBirth']");
	protected By ssn = By.xpath("//tab[@ng-reflect-active='true']//following::input[@id='ssn']");
	protected By genderSelectDrpDown = By
			.xpath("//tab[@ng-reflect-active='true']//following::select[@ng-reflect-name='gender']");
	protected By country = By.xpath("//tab[@ng-reflect-active='true']//select[@ng-reflect-name='countryCode']");
	protected By streetAdd = By.xpath("//tab[@ng-reflect-active='true']//input[@ng-reflect-name='addressLine1']");
	protected By postalCode = By.xpath("//tab[@ng-reflect-active='true']//input[@ng-reflect-name='postalCode']");
	protected By city = By.xpath("//tab[@ng-reflect-active='true']//input[@ng-reflect-name='city']");
	protected By copyAddDrpDwn = By.xpath("//tab[@ng-reflect-active='true']//select[@class='custom-select green']");
	protected By primaryNum = By.xpath("//tab[@ng-reflect-active='true']//input[@name='primaryPhoneNumber']");
	protected By alterNum = By.xpath("//tab[@ng-reflect-active='true']//input[@name='alternatePhoneNumber']");
	protected By emailTxt = By.xpath("//tab[@ng-reflect-active='true']//input[@name='email']");
	protected By editCustomerInfo = By.xpath("//button[text()='Edit']");
	protected By ownerList = By.xpath("//div[@class='package-points-card']//span[text()='Secondary']");
	protected By removeIcon = By.xpath("//a[text()='Remove Owner']/i");
	protected By yesBtn = By.xpath("//button[text()='Yes']");
	protected By infoSaveMsg = By.xpath("//h3/span[text()='Owner Information']/../../p");
	protected By autoPayCredit = By.xpath("//label[@for='autopay-credit']");
	protected By summaryReview = By.xpath("//label[@for='summary-checkbox1']");
	protected By standardBenChkBox = By.xpath("//label[@for='standard-benefits-checkbox']");
	protected By saveBtn = By.xpath("//button[text()=' Save ']");
	protected By saveWorksheetPopUp = By.xpath("//p[text()='Save Worksheet']");
	protected By okBtn = By.xpath("//button[text()='Ok']");
	protected By loanDecOkBtn = By.xpath("//button[text()='OK']");
	protected By submitWS = By.xpath("//button[text()=' Submit Worksheet ']");
	protected By tourHomePage = By.xpath("//div[@class='swiper-wrapper ']/div");
	protected By packagePoints = By
			.xpath("//div[contains(@class,'package-summary-card')]//div[@class='card-img-overlay']/strong");
	protected By purchasePriceTxt = By.xpath("//div[@class='card-body']/table/thead/tr/th[2]");
	protected By downPayTxt = By.xpath("//div[@class='card-body']/table/tbody/tr[1]/td[2]");
	protected By financedAmt = By.xpath("//div[@class='card-body']/table/tbody/tr[3]/td[2]");
	protected By salesIcon = By
			.xpath("(//a[contains(@class,'dropdown-item')]/img[@alt='icon-user-circle-outline-grey'])[2]");
	protected By leadName = By.xpath("//h3[@class='ews-page-title']");
	protected By paymentMethod = By.xpath("//ul[contains(@class,'select-payment-list')]/li/span");
	protected By selectCreditType = By.xpath("//select[@ng-reflect-model='-- Select Card Type --']");
	protected By verifyACHPage = By.xpath("//h3[text()='CHECK (ACH)']//..//label[text()='Payment Amount']");
	protected By verifyCashPage = By.xpath("//h3[text()='CASH']/../label[text()='Payment Amount']");
	protected By cancelBtn = By.xpath("(//div[contains(@class,'down-payment-card-footer')]//button[text()='Cancel'])[4]");
	protected By achCancelBtn = By
			.xpath("(//div[contains(@class,'down-payment-card-footer')]//button[text()='Cancel'])[2]");
	protected By cashCancelBtn = By
			.xpath("(//div[contains(@class,'down-payment-card-footer')]//button[text()='Cancel'])[3]");
	protected By empIdSpan = By.xpath("//button[contains(@class,'dropdown-item')]/span");
	protected By addBtn = By.xpath("//button[text()='ADD']");
	protected By exitButton = By.xpath("//a[text()='Exit']");
	protected By pointsPurchaseSummary = By
			.xpath("//h3[text()='POINTS PURCHASED']//following::table[1]/tbody/tr[2]/td[2]/strong");
	protected By maintainanceFee = By
			.xpath("//h3[text()='POINTS PURCHASED']//following::table[2]/tbody/tr[2]/td[2]/strong");
	protected By financeReviewPage = By.xpath("(//app-summary-financial-information//h3)[1]");
	protected By downPayReviewPage = By
			.xpath("(//app-summary-financial-information//h3)[1]//following::table[1]/thead/tr/th[2]/strong");
	protected By financeAmtReviewPage = By
			.xpath("(//app-summary-financial-information//h3)[2]//following::table[1]/thead//tr/th[2]/strong");
	protected By standardBenefiCheck = By.xpath("//h3[text()='STANDARD BENEFITS']");
	protected By congratsMsg = By.xpath("//span[text()='Congratulations,']");
	protected By loanDecision = By.xpath("//li[text()=' Loan Declined. Please revisit loan criteria. ']");
	protected By empID = By.xpath("//input[@id='employee-id']");
	protected By corouselCheck = By.xpath("(//div[contains(@class,'slideInUp')])[1]");
	protected By VIPLevelStandard = By.xpath("//li[text()=' Standard ']");
	protected By privilageProgram = By.xpath("//li[text()='standard']");
	protected By VIPLevelSilver = By.xpath("//li[text()=' Silver ']");
	protected By wsaRefNum = By.xpath(
			"((//h3[text()='Worksheet Milestones']//following::table)[1]/tbody/tr/td[1]//descendant::strong)[6]");
	protected By gender = By.xpath("//tab[@ng-reflect-active='true']//select[@ng-reflect-name='gender']");
	protected By stateDrpDwn = By.xpath("//select[@formcontrolname='stateProvinceCode']");

	public static String strwsaRefNum;
	public String packgePoints;
	public String purchasePrice;
	public double downPayment;
	public double financedAmountUI;
	public String strfinancedAmt;
	public String strNoOfPayments;
	public String strInterestRate;
	public String strLoanPayment;
	public String strMonMntcFee;
	public static String wsaRefNumber;

	protected By picDisclaimerText = By
			.xpath("//p[@class='help-text' and contains(text(),'*All amounts set forth are estimates for display')]");

	public void generateWSARefNumber() {
		String strDisclaimerTxt = testData.get("DisclaimerText");
		waitUntilElementVisibleBy(driver, SelectLink, "ExplicitWaitLongestTime");
		driver.findElement(SelectLink).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
				"CWA Package is selected successfully");
 		waitUntilElementVisibleBy(driver, corouselCheck, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(corouselCheck))) {
			tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
					"Corousel for CWA Point Package is displayed successfully");
		} else {
			tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.FAIL,
					"Corousel for CWA Point Package is not displayed successfully");
		}
		/*if (strDisclaimerTxt.equals("Y")) {
			waitUntilElementVisibleBy(driver, picDisclaimerText, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(picDisclaimerText))) {
				tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
						"PIC disclaimer Text is displayed " + driver.findElement(picDisclaimerText).getText());
			} else {
				tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.FAIL,
						"PIC disclaimer Text is not displayed");
			}
		}*/

		waitUntilElementVisibleBy(driver, selectBtn, "ExplicitWaitLongestTime");
		driver.findElement(selectBtn).click();
		tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
				"CWA Point Package is selected successfully");
		waitUntilElementVisibleBy(driver, WSALabel, "ExplicitWaitLongestTime");
		String str[] = driver.findElement(WSALabel).getText().substring(5).split(" ");
		strwsaRefNum = str[0];
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (strwsaRefNum.isEmpty() == false) {
			tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
					"WSA Reference number is generated successfully");
		} else {
			tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.FAIL,
					"WSA Reference number is not generated successfully");
		}

	}

	public void validateLoanDecisionMsg() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(SelectLink).click();
			waitUntilElementVisibleBy(driver, loanDecision, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(loanDecision))) {
				tcConfig.updateTestReporter("CWAPackageWSAPage", "validateLoanDecisionMsg", Status.PASS,
						"Loan Decision warning message is displayed successfully");
				clickElementBy(loanDecOkBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPackageWSAPage", "validateLoanDecisionMsg", Status.FAIL,
					"Loan Decision warning message is not displayed successfully" + e.getMessage());
		}
	}

	protected By creditCardPaymentAmtTxt = By.xpath("(//label[text()='Payment Amount']//following::input[1])[4]");

	public void verifyDifferentPaymentMethod() {
		String strMethodName[] = testData.get("PaymentMethod").split(";");
		String strPmtMethod = "";
		try {

			List<WebElement> pmtList = driver.findElements(paymentMethod);
			for (int i = 1; i <= pmtList.size(); i++) {
				strPmtMethod = driver
						.findElement(By.xpath("(//ul[contains(@class,'select-payment-list')]/li/span)[" + i + "]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (strPmtMethod.equals(strMethodName[i - 1])) {
					tcConfig.updateTestReporter("Down Payment Summary Page", "verifyDifferentPaymentMethod",
							Status.PASS, "Payment method " + strPmtMethod + " is displayed successfully");
					if (strPmtMethod.equals("Credit Card")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.findElement(
								By.xpath("(//ul[contains(@class,'select-payment-list')]/li/span)[" + i + "]")).click();
						waitUntilElementVisibleBy(driver, selectCreditType, "ExplicitWaitLongestTime");
						new Select(driver.findElement(selectCreditType)).selectByVisibleText(testData.get("CardType"));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						fieldDataEnter(creditCardTxt, "5432");
						driver.findElement(creditCardPaymentAmtTxt).click();
						driver.findElement(creditCardPaymentAmtTxt).clear();
						fieldDataEnter(creditCardPaymentAmtTxt, "1000");

						tcConfig.updateTestReporter("Down Payment Summary Page", "verifyDifferentPaymentMethod",
								Status.PASS, strPmtMethod + " payment page is displayed successfully");
						clickElementBy(cancelBtn);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else if (strPmtMethod.equals("Check (ACH)")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.findElement(
								By.xpath("(//ul[contains(@class,'select-payment-list')]/li/span)[" + i + "]")).click();
						waitUntilElementVisibleBy(driver, verifyACHPage, "ExplicitWaitLongestTime");
						if (verifyElementDisplayed(driver.findElement(verifyACHPage))) {
							tcConfig.updateTestReporter("Down Payment Summary Page", "verifyDifferentPaymentMethod",
									Status.PASS, strPmtMethod + " payment page is displayed successfully");
						}
						clickElementBy(achCancelBtn);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else if (strPmtMethod.equals("Cash")) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.findElement(
								By.xpath("(//ul[contains(@class,'select-payment-list')]/li/span)[" + i + "]")).click();
						waitUntilElementVisibleBy(driver, verifyCashPage, "ExplicitWaitLongestTime");
						if (verifyElementDisplayed(driver.findElement(verifyCashPage))) {
							tcConfig.updateTestReporter("Down Payment Summary Page", "verifyDifferentPaymentMethod",
									Status.PASS, strPmtMethod + " payment page is displayed successfully");
						}
						clickElementBy(cashCancelBtn);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
				} else {
					tcConfig.updateTestReporter("Down Payment Summary Page", "verifyDifferentPaymentMethod",
							Status.FAIL, "Payment method " + strPmtMethod + " is not displayed successfully");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Down Payment Summary Page", "verifyDifferentPaymentMethod", Status.FAIL,
					"Payment method is not displayed successfully" + e.getMessage());
		}
	}

	protected By creditCardTxt = By.xpath("//label[text()='Last 4 of CC#']//following::input[1]");

	public void retainCreditCardDetails() {
		String strMethodName = testData.get("PaymentMethod");
		String strPmtMethod = "";
		try {

			strPmtMethod = driver
					.findElement(By.xpath(
							"//ul[contains(@class,'select-payment-list')]/li/span[text()='" + strMethodName + "']"))
					.getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (strPmtMethod.equals("Credit Card")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By
						.xpath("//ul[contains(@class,'select-payment-list')]/li/span[text()='" + strMethodName + "']"))
						.click();
				waitUntilElementVisibleBy(driver, selectCreditType, "ExplicitWaitLongestTime");
				new Select(driver.findElement(selectCreditType)).selectByVisibleText(testData.get("CardType"));
				tcConfig.updateTestReporter("Down Payment Summary Page", "retainCreditCardDetails", Status.PASS,
						strPmtMethod + " payment page is displayed successfully");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(creditCardTxt, "5432");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(addPayBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By
						.xpath("//ul[contains(@class,'select-payment-list')]/li/span[text()='" + strMethodName + "']"))
						.click();
				waitUntilElementVisibleBy(driver, selectCreditType, "ExplicitWaitLongestTime");
				String strselectOption = new Select(driver.findElement(selectCreditType)).getFirstSelectedOption()
						.getText();
				String strCardDet = driver.findElement(creditCardTxt).getAttribute("value");
				if (strselectOption.equals("-- Select Card Type --") && strCardDet.equals("")) {
					tcConfig.updateTestReporter("Down Payment Summary Page", "retainCreditCardDetails", Status.PASS,
							"The last 4 digit of credit card is not retained from the previous credit card");
				} else {
					tcConfig.updateTestReporter("Down Payment Summary Page", "retainCreditCardDetails", Status.FAIL,
							"The last 4 digit of credit card is retained from the previous credit card");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Down Payment Summary Page", "retainCreditCardDetails", Status.FAIL,
					"The last 4 digit of credit card is retained from the previous credit card" + e.getMessage());
		}
	}

	public void getFinanceValuefromCWAPackage() {
		String strPoint = testData.get("TourPoints");
		try {
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By
							.xpath("(//div[@class='card-footer']/a[1][contains(text(),' More Details ')])[" + j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPackageWSAPage", "clickCWAPackage", Status.PASS,
							"More Details link is clicked successfully");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("(//li/a/span[contains(text(),'Financial')])[" + j + "]")).click();
					tcConfig.updateTestReporter("CWAPackageWSAPage", "clickCWAPackage", Status.PASS,
							"Financial Tab is clicked successfully");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement baseTable = driver
							.findElement(By.xpath("(//tab[@ng-reflect-heading='Financial'])[" + j + "]/table/tbody"));
					strfinancedAmt = getValuefromTable(baseTable, 0, 1).substring(1).replace(",", "").trim();
					strNoOfPayments = getValuefromTable(baseTable, 1, 1).trim();
					strInterestRate = getValuefromTable(baseTable, 2, 1).trim();
					strLoanPayment = getValuefromTable(baseTable, 3, 1).substring(1).replace(",", "").trim();
					strMonMntcFee = getValuefromTable(baseTable, 4, 1).substring(1).replace(",", "").trim();

					break;
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("CWAPackageWSAPage", "getFinanceValuefromCWAPackage",
					Status.FAIL, "Failed due to : " + e.getMessage());
		}
	}

	public void tourLookUp() {

		try {
			driver.findElement(retrieveLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Select lookUpObj = new Select(driver.findElement(lookUpDrpDwn));
			lookUpObj.selectByVisibleText("Reference Number");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(refNumTxt))) {
				driver.findElement(refNumTxt).sendKeys(strwsaRefNum);
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
						"Reference Number entered Successfully");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Reference Number not entered Successfully");
			}
			driver.findElement(submitBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(refNumColVal).getText().trim().equals(strwsaRefNum)) {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
						"Reference Number lookup result is displayed successfully");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(clickToView).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (verifyElementDisplayed(driver.findElement(summaryLabel))) {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
							"New Proposal page is displayed successfully");
				} else {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
							"New Proposal page is not displayed successfully");
				}
				String str[] = driver.findElement(By.xpath("//h4[text()='Loan Information']/../div[2]/p")).getText()
						.split("\n");
				String str1[] = str[2].split(" ");
				String str2[] = str[1].split(" ");
				String str3[] = str[3].split(" ");
				double financeAmt = Double.parseDouble(str1[3].substring(1).replace(",", ""));
				int financeAmtUI = (int) Math.round(financeAmt);
				String financedAmountUI = String.valueOf(financeAmtUI);
				String strInterestRt = str2[3].trim();
				String strTerm = str3[3].trim();
				if (financedAmountUI.equals(strfinancedAmt) && strInterestRt.equals(strInterestRate)
						&& strNoOfPayments.equals(strTerm)) {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
							"WSA Reference Number is displayed Successfully");
				} else {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
							"WSA Reference Number is not displayed Successfully");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
					"WSA Reference Number is not displayed Successfully");
		}
	}

	public void validatetourLookUp() {

		try {
			driver.findElement(retrieveLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Select lookUpObj = new Select(driver.findElement(lookUpDrpDwn));
			lookUpObj.selectByVisibleText("Reference Number");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(refNumTxt))) {
				driver.findElement(refNumTxt).sendKeys(strwsaRefNum);
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
						"Reference Number entered Successfully");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Reference Number not entered Successfully");
			}
			driver.findElement(submitBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(refNumColVal).getText().trim().equals(strwsaRefNum)) {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
						"Reference Number lookup result is displayed successfully");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(clickToView).click();
				// waitForSometime(tcConfig.getConfig().get("LongWait"));
				waitUntilElementVisibleBy(driver, summaryLabel, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(summaryLabel))) {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
							"New Proposal page for " + strwsaRefNum + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
							"New Proposal page for " + strwsaRefNum + " is not displayed successfully");
				}

				/*
				 * String str[] = driver.findElement(By.xpath(
				 * "//h4[text()='Loan Information']/../div[2]/p")).getText()
				 * .split("\n"); String str1[] = str[2].split(" "); String
				 * str2[] = str[1].split(" "); String str3[] = str[3].split(" "
				 * ); double financeAmt =
				 * Double.parseDouble(str1[3].substring(1).replace(",", ""));
				 * int financeAmtUI = (int) Math.round(financeAmt); String
				 * financedAmountUI = String.valueOf(financeAmtUI); String
				 * strInterestRt = str2[3].trim(); String strTerm =
				 * str3[3].trim(); if (financedAmountUI.equals(strfinancedAmt)
				 * && strInterestRt.equals(strInterestRate) &&
				 * strNoOfPayments.equals(strTerm)) {
				 * tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()",
				 * Status.PASS, "WSA Reference Number "+strwsaRefNum+
				 * " is displayed Successfully"); } else {
				 * tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()",
				 * Status.FAIL, "WSA Reference Number "+strwsaRefNum+
				 * " is not displayed Successfully"); }
				 */

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
					"WSA Reference Number is not displayed Successfully");
		}
	}

	public void checkLinkDownPayPage() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			privacyModalWindow();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(cashBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(addPayBtn);

		} catch (Exception e) {
			tcConfig.updateTestReporter("Down Payment Page", "checkLinkDownPayPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}

	public void financingSummaryPage() {

		try {
			waitUntilElementVisibleBy(driver, continueFinanceBtn, "ExplicitWaitLongestTime");
			if (driver.findElement(continueFinanceBtn).isEnabled()) {
				clickElementBy(continueFinanceBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("Finance Summary Page", "financingSummaryPage()", Status.PASS,
						"Page Successfully redirected to Finance Page");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			privacyModalWindow();
		} catch (Exception e) {
			tcConfig.updateTestReporter("Finance Summary Page", "financingSummaryPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}
	
	public void OwnerInformationPage() {
		try {
			if (driver.findElement(continueOwnBtn).isEnabled()) {
				clickElementBy(continueOwnBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ownerPrivacyModalWindow();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				new Select(driver.findElement(genderSelectDrpDown)).selectByVisibleText("Male");
				waitUntilElementVisibleBy(driver, addressLink, "ExplicitWaitLongestTime");
				clickElementBy(addressLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ownerPrivacyModalWindow();
				clickElementBy(contactInfoLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ownerPrivacyModalWindow();
				clickElementBy(custAddChkBox);
				if (driver.findElement(contactInfoLink).isEnabled()) {
					clickElementBy(contactInfoLink);
					ownerPrivacyModalWindow();
					new Select(driver.findElement(selectPrimaryPhone)).selectByIndex(1);
					fieldDataEnter(primaryNum, testData.get("FirstCusHomePhone"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					new Select(driver.findElement(selectAlterPhone)).selectByIndex(2);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(saveContinueBtn);
				}
			}
			waitUntilElementVisibleBy(driver, continueSummary, "ExplicitWaitLongestTime");
			tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.PASS,
					"User is able to go to Summary Page");
			clickElementBy(continueSummary);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}

	public void summaryPage() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			privacyModalWindow();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> nextList = driver.findElements(By.xpath("//button[text()=' Next ']"));
			for (int i = 0; i < nextList.size(); i++) {
				nextList.get(i).click();
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Summary Page", "summaryPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}

	public void privacyModalWindow() {
		String strPrivacyLinkLabel;
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getElementInView(privacyLabel);
			if (verifyObjectDisplayed(privacyLabel)) {
				strPrivacyLinkLabel = driver.findElement(privacyLabel).getText().toUpperCase().trim();
				System.out.println(strPrivacyLinkLabel);
				if (strPrivacyLinkLabel.contains("FOR WYNDHAM’S PRIVACY NOTICE, CLICK HERE")) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Label for Privacy link is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.FAIL,
							"Label for Privacy link is not displayed successfully");
				}
				clickElementBy(clickHereLink);
				waitUntilElementVisibleBy(driver, privacyModalWindow, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(privacyModalWindow))) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Privacy Modal Window is displayed successfully");
					clickElementBy(iconCloseBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Summary Page", "privacyModalWindow()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}

	public void ownerPrivacyModalWindow() {
		String strPrivacyLinkLabel;
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(ownersPrivacyLink)) {
				strPrivacyLinkLabel = driver.findElement(ownersPrivacyLink).getText().toUpperCase().trim();
				System.out.println(strPrivacyLinkLabel);
				if (strPrivacyLinkLabel.contains("FOR WYNDHAM’S PRIVACY NOTICE, CLICK HERE")) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Label for Privacy link is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.FAIL,
							"Label for Privacy link is not displayed successfully");
				}
				clickElementBy(OwnerclickHereLink);
				waitUntilElementVisibleBy(driver, privacyModalWindow, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(privacyModalWindow))) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Privacy Modal Window is displayed successfully");
					clickElementBy(iconCloseBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Summary Page", "privacyModalWindow()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}

	protected By selectCountry = By.xpath("//tab[@ng-reflect-active='true']//select[@formcontrolname='countryCode']");
	protected By selectGender = By.xpath("//select[@ng-reflect-name='gender']");
	// protected By successMsgOwnerAdd = By.xpath("//p[contains(text(),' Sadsad
	// Asdsad has been successfully saved')]");

	public void addSecondaryOwnertoWorksheet() {
		String strFirstName;
		String strLastName;

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(cashBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(addPayBtn);
			waitUntilElementVisibleBy(driver, continueFinanceBtn, "ExplicitWaitLongestTime");
			if (driver.findElement(continueFinanceBtn).isEnabled()) {
				clickElementBy(continueFinanceBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("Finance Summary Page", "financingSummaryPage()", Status.PASS,
						"Page Successfully redirected to Finance Page");
				waitUntilElementVisibleBy(driver, continueOwnBtn, "ExplicitWaitLongestTime");
				if (driver.findElement(continueOwnBtn).isEnabled()) {
					clickElementBy(continueOwnBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					new Select(driver.findElement(genderSelectDrpDown)).selectByVisibleText("Male");
					waitUntilElementVisibleBy(driver, addressLink, "ExplicitWaitLongestTime");
					clickElementBy(addressLink);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<WebElement> contactList = driver.findElements(contactInfoLink);
					contactList.get(0).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(custAddChkBox);
					waitUntilElementVisibleBy(driver, contactInfoLink, "ExplicitWaitLongestTime");
					if (driver.findElement(contactInfoLink).isEnabled()) {
						clickElementBy(contactInfoLink);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.findElement(primaryNum).sendKeys(testData.get("FirstCusHomePhone"));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						new Select(driver.findElement(selectPrimaryPhone)).selectByIndex(1);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						new Select(driver.findElement(selectAlterPhone)).selectByIndex(2);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementBy(saveContinueBtn);
					}
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, addNewOwner, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(addNewOwner))) {
					clickElementJSWithWait(addNewOwner);
					tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()",
							Status.PASS, "Add new Owner is button is clicked");
				}
				strFirstName = getRandomString(6, "upper");
				strLastName = getRandomString(7, "upper");
				String strFullName = strFirstName + " " + strLastName;
				fieldDataEnter(firstName, strFirstName);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(lastName, strLastName);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// fieldDataEnter(dateOfBirth, testData.get("DOB"));
				driver.findElement(dateOfBirth).sendKeys(testData.get("DOBSecon"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(ssn, testData.get("FirstCusSSN"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				new Select(driver.findElement(genderSelectDrpDown)).selectByVisibleText("Male");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()", Status.PASS,
						"Personal Information is entered successfully");
				waitUntilElementVisibleBy(driver,
						driver.findElement(By
								.xpath("//tab[@ng-reflect-active='true']//div[contains(@class,'personal-information')]//following::button[text()=' Address ']")),
						"ExplicitWaitLongestTime");
				driver.findElement(By
						.xpath("//tab[@ng-reflect-active='true']//div[contains(@class,'personal-information')]//following::button[text()=' Address ']"))
						.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				new Select(driver.findElement(copyAddDrpDwn)).selectByIndex(1);
				if (!driver.findElement(selectCountry).getAttribute("ng-reflect-model").equals("")) {
					tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()",
							Status.PASS, "Address is copied successfully");
				} else {
					tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()",
							Status.FAIL, "Address is not copied");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> contactList = driver.findElements(contactInfoLink);
				clickElementJSWithWait(contactList.get(1));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(custAddChkBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.findElement(contactInfoLink).isEnabled()) {
					//List<WebElement> contactList1 = driver.findElements(contactInfoLink);
					clickElementJSWithWait(contactList.get(1));
					waitUntilElementVisibleBy(driver, primaryNum, "ExplicitWaitLongestTime");
					fieldDataEnter(primaryNum, testData.get("FirstCusHomePhone"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<WebElement> primaryPh = driver.findElements(selectPrimaryPhone);
					List<WebElement> alternatePh = driver.findElements(selectAlterPhone);
					new Select(primaryPh.get(1)).selectByIndex(1);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					fieldDataEnter(alterNum, testData.get("FirstCusHomePhone"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					new Select(alternatePh.get(1)).selectByIndex(2);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					fieldDataEnter(emailTxt, testData.get("CusEmail"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()",
							Status.PASS, "Contact Information is entered successfully");
					waitUntilElementVisibleBy(driver, saveContinueBtn, "ExplicitWaitLongestTime");
					clickElementBy(saveContinueBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyElementDisplayed(driver.findElement(
							By.xpath("//p[contains(text(),'has been successfully saved')]")))) {
						tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()",
								Status.PASS, "Secondary Owner " + strFullName + " has been added Successfully");
					} else {
						tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()",
								Status.FAIL, "Secondary Owner is not added");
					}
				}
			}
			driver.findElement(continueSummary).click();

		} catch (Exception e) {
			tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()", Status.FAIL,
					"Secondary owner is not create successfully" + e.getMessage());
		}
	}

	@FindBy(xpath = "//span[text()='Points Usage']")
	protected WebElement pointsUsage;

	@FindBy(xpath = "//span[@class='usage-date']/span")
	protected WebElement usageDate;

	@FindBy(xpath = "//span[@class='usage-date']/strong")
	protected WebElement usageDateTxt;

	@FindBy(xpath = "//table/thead/tr/th/strong[text()='Year 1']")
	protected WebElement year1txt;

	@FindBy(xpath = "//table/thead/tr/th/strong[text()='Year 1']/../span")
	protected WebElement startMonth;

	@FindBy(xpath = "//td[text()='Annual Points']")
	protected WebElement annualPointsTxt;

	@FindBy(xpath = "(//td[text()='Annual Points']/..//span)[1]")
	protected WebElement annualPoint;

	@FindBy(xpath = "//span[text()='VIP']")
	protected WebElement VIPPrivilagesTab;

	public void submitWorksheet(String strPointsUsage, String strVIPLevel) {
		try {
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, cashBtn, "ExplicitWaitLongestTime");
			clickElementBy(cashBtn);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, addPayBtn, "ExplicitWaitLongestTime");
			clickElementBy(addPayBtn);
			tcConfig.updateTestReporter("Payment Method Screen", "submitWorksheet()", Status.PASS,
					"User is bale to select the Payment Method successfully");
			waitUntilElementVisibleBy(driver, continueFinanceBtn, "ExplicitWaitLongestTime");
			if (driver.findElement(continueFinanceBtn).isEnabled()) {
				clickElementBy(continueFinanceBtn);
				waitUntilElementVisibleBy(driver, autoPayCredit, "ExplicitWaitLongestTime");
				clickElementBy(autoPayCredit);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("Finance Summary Page", "financingSummaryPage()", Status.PASS,
						"Page Successfully redirected to Finance Page");
			}
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, continueOwnBtn, "ExplicitWaitLongestTime");
			if (driver.findElement(continueOwnBtn).isEnabled()) {
				clickElementBy(continueOwnBtn);
				waitUntilElementVisibleBy(driver, gender, "ExplicitWaitLongestTime");
				new Select(driver.findElement(gender)).selectByIndex(1);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*
				 * Pattern digitPattern = Pattern.compile("\\d{4}"); String
				 * strSSN[] =
				 * driver.findElement(ssn).getAttribute("ng-reflect-model").
				 * split("-"); if(digitPattern.matcher(strSSN[2]).matches()) {
				 * tcConfig.updateTestReporter("Owner Information Page",
				 * "OwnerInformationPage()", Status.PASS,
				 * "User is able to see the 4 digit SSN number successfully");
				 * }else{ tcConfig.updateTestReporter("Owner Information Page",
				 * "OwnerInformationPage()", Status.FAIL,
				 * "User is not able to see the 4 digit SSN number"); }
				 */
				waitUntilElementVisibleBy(driver, ssn, "ExplicitWaitLongestTime");
				Pattern format = Pattern.compile("XXX-XX-XXXX");
				String strSSN = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression");
				if (format.matcher(strSSN).matches()) {
					tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.PASS,
							"User is able to see the masked SSN number successfully");
				} else {
					tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.FAIL,
							"User is not able to see the masked SSN number");
				}
				int firstVal = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression").split("-")[0]
						.length();
				int secondVal = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression").split("-")[1]
						.length();
				int thirdVal = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression").split("-")[2]
						.length();
				if (firstVal == 3 && secondVal == 2 && thirdVal == 4) {
					tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
							"User is not able to enter more than 9 digits for SSN field");
				} else {
					tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.FAIL,
							"User is able to enter more than 9 digits for SSN field");
				}
				waitUntilElementVisibleBy(driver, addressLink, "ExplicitWaitLongestTime");
				clickElementBy(addressLink);
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, stateDrpDwn, "ExplicitWaitLongestTime");
				new Select(driver.findElement(stateDrpDwn)).selectByIndex(5);
				tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
						"User is able to select the state from Drop Down");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(contactInfoLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, custAddChkBox, "ExplicitWaitLongestTime");
				clickElementBy(custAddChkBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.findElement(contactInfoLink).isEnabled()) {
					clickElementBy(contactInfoLink);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					int firstpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[0].length();
					int secondpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[1].length();
					int thirdpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[2].length();
					if (firstpatt == 3 && secondpatt == 3 && thirdpatt == 4) {
						tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
								"User is able to perform validation for International numbers using plugin");
					} else {
						tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.FAIL,
								"User is not able to perform validation for International numbers using plugin");
					}
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					new Select(driver.findElement(selectPrimaryPhone)).selectByIndex(1);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					new Select(driver.findElement(selectAlterPhone)).selectByIndex(2);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(saveContinueBtn);

				}
			}
			waitUntilElementVisibleBy(driver, continueSummary, "ExplicitWaitLongestTime");
			tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.PASS,
					"User is able to go to Summary Page");
			clickElementBy(continueSummary);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, summaryReview, "ExplicitWaitLongestTime");
			// Values to check
			packgePoints = driver.findElement(packagePoints).getText().trim();
			downPayment = Double
					.parseDouble(driver.findElement(downPayTxt).getText().trim().substring(1).replace(",", ""));
			financedAmountUI = Double
					.parseDouble(driver.findElement(financedAmt).getText().trim().substring(1).replace(",", ""));

			clickElementBy(summaryReview);
			/*
			 * String primaryOwner = LeadsPage.strLeadFullName.toLowerCase(); if
			 * (driver.findElement(By.xpath("//strong[text()='" + primaryOwner +
			 * "']")).getText() .equals(primaryOwner))
			 * (driver.findElement(By.xpath(
			 * "//strong[text()='angelina zzbrcrn']")).getText() .equals(
			 * "angelina zzbrcrn")) { tcConfig.updateTestReporter(
			 * "Ownership Review Page", "submitWorksheet()", Status.PASS,
			 * "Primary Owner name is displayed successfully"); }
			 */

			if (packgePoints.equals(driver.findElement(pointsPurchaseSummary).getText())) {
				tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
						"Point package purchase amount is displayed successfully");
			}
			if (verifyElementDisplayed(driver.findElement(maintainanceFee))) {
				tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
						"Maintainance Fee is displayed successfully");
			}
			List<WebElement> reviewList = driver.findElements(By.xpath("//label[contains(@for,'review-checkbox')]"));
			for (int i = 1; i <= reviewList.size(); i++) {
				driver.findElement(By.xpath("(//label[contains(@for,'review-checkbox')])[" + i + "]")).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			List<WebElement> nextList = driver.findElements(By.xpath("//button[text()=' Next ']"));
			for (int j = 1; j <= nextList.size(); j++) {
				if (j == 1) {
					driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (driver.findElement(financeReviewPage).getText().equals("FINANCIAL INFORMATION TODAY")) {
						Double downPaymnet = Double.parseDouble(
								driver.findElement(downPayReviewPage).getText().substring(1).replace(",", ""));
						if (Math.round(downPaymnet) == downPayment) {
							tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
									"Down Payment Today is displayed successfully in Summary Page");
						}

						/*
						 * double financedAmount = Double.parseDouble(
						 * driver.findElement(financeAmtReviewPage).getText().
						 * substring(1).replace(",", "")); if
						 * (Math.round(financedAmount) == financedAmountUI) {
						 * tcConfig.updateTestReporter("Ownership Review Page",
						 * "submitWorksheet()", Status.PASS,
						 * "Financed Amount is displayed successfully in Summary Page"
						 * ); }
						 */

						List<WebElement> list = driver
								.findElements(By.xpath("//label[contains(@for,'financial-information-checkbox')]"));
						for (int k = 1; k <= list.size(); k++) {
							driver.findElement(
									By.xpath("(//label[contains(@for,'financial-information-checkbox')])[" + k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
					}
				} else if (j == 2) {
					driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyElementDisplayed(driver.findElement(standardBenefiCheck))) {
						tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet()", Status.PASS,
								"Standard Benefits are displayed successfully in Summary Page");
					}
					clickElementBy(standardBenChkBox);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (strPointsUsage.equals("No Bonus Point")) {
						if (verifyElementDisplayed(pointsUsage)) {
							tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
									"Points Usage label is displayed");
						} else {
							tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.FAIL,
									"Points Usage label is not displayed");
						}
						String strUsageDtTxt;
						if (verifyElementDisplayed(usageDate)) {
							strUsageDtTxt = usageDateTxt.getText().trim();
							System.out.println(strUsageDtTxt);
							tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
									"Usage Date " + strUsageDtTxt + " is displayed successfully");
							/*
							 * if (verifyElementDisplayed(year1txt)) { String
							 * strStartMonth[] =
							 * startMonth.getText().trim().split("-");
							 * System.out.println(strStartMonth[0]); if
							 * (strUsageDtTxt.equals(strStartMonth[0].length()-1
							 * )) { tcConfig.updateTestReporter(
							 * "Standard Benefit Page", "submitWorksheet",
							 * Status.PASS, "First month of the quarter " +
							 * strStartMonth + " is displayed successfully");
							 * SimpleDateFormat formatter = new
							 * SimpleDateFormat("MMM dd, yyyy"); Date date =
							 * formatter.parse(strStartMonth[0]); Calendar cal =
							 * Calendar.getInstance(); cal.setTime(date);
							 * cal.add(Calendar.YEAR, 1); Date nextYear =
							 * cal.getTime();
							 * System.out.println(nextYear.toString()); if
							 * (strStartMonth[1].equals(nextYear.toString())) {
							 * tcConfig.updateTestReporter(
							 * "Standard Benefit Page", "submitWorksheet",
							 * Status.PASS, "End date of the year " +
							 * strStartMonth + " is displayed successfully"); }
							 * else { tcConfig.updateTestReporter(
							 * "Standard Benefit Page", "submitWorksheet",
							 * Status.FAIL, "End date of the year " +
							 * strStartMonth + " is not displayed successfully"
							 * ); }
							 * 
							 * } }
							 */

							if (verifyElementDisplayed(annualPointsTxt)) {
								tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
										"Annual Points text is displayed successfully for year 1");

								if (CWAPitchBenefitDetailsPage_Ipad.strPoint.equals(annualPoint.getText().trim())) {
									tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet",
											Status.PASS, "Annual Points for CWA package " + annualPoint.getText()
													+ " is displayed successfully for year 1");
								} else {
									tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet",
											Status.FAIL, "Annual Points for CWA package " + annualPoint.getText()
													+ " is not displayed successfully for year 1");
								}

							}
						}
					}

					if (strVIPLevel.equals("Standard")) {
						if (verifyElementDisplayed(VIPPrivilagesTab)) {
							VIPPrivilagesTab.click();
							if (verifyElementDisplayed(driver.findElement(VIPLevelStandard))) {
								tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet()",
										Status.PASS, "Standard Owner is displayed successfully in Summary Page");
							} else {
								tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet()",
										Status.FAIL, "Standard Owner is not displayed successfully in Summary Page");
							}

							if (verifyElementDisplayed(driver.findElement(privilageProgram))) {
								tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet()",
										Status.PASS, "Privilage Program is displayed successfully in Summary Page");
							} else {
								tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet()",
										Status.FAIL,
										"Privilage Program is not displayed successfully in the summary page");
							}

						}

					}

				}

			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(saveBtn).isEnabled()) {
				clickElementBy(saveBtn);
			}
			waitUntilElementVisibleBy(driver, saveWorksheetPopUp, "ExplicitWaitLongestTime");
			clickElementBy(okBtn);
			// addSalesParticipants();
			waitUntilElementVisibleBy(driver, submitWS, "ExplicitWaitLongestTime");
			if (driver.findElement(submitWS).isEnabled()) {
				clickElementBy(submitWS);
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Owner Information Page", "addSecondaryOwnertoWorksheet()", Status.FAIL,
					"Secondary owner is not create successfully" + e.getMessage());
		}
	}

	public void validateBonusPointDisclaimer() {
		try {
			clickElementBy(bonusInfoLink);
			waitUntilElementVisibleBy(driver, bonusDetailsPopUp, "ExplicitWaitLongestTime");
			tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointDisclaimer()", Status.PASS,
					"Bonus Point details pop up is displayed successfully in Summary Page");
			clickElementBy(okBtn);
		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointDisclaimer()", Status.FAIL,
					"Bonus Point details pop up is not displayed successfully in Summary Page" + e.getMessage());
		}
	}

	public void validateCongratsMessage() {
		waitUntilElementVisibleBy(driver, congratsMsg, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(congratsMsg))) {
			tcConfig.updateTestReporter("Sales Participants Page", "validateCongratsMessage()", Status.PASS,
					"Congratulations message is displayed after submitting the worksheet");
		} else {
			tcConfig.updateTestReporter("Sales Participants Page", "validateCongratsMessage()", Status.FAIL,
					"Congratulations message is not displayed after submitting the worksheet");
		}
	}

	public void addSalesParticipants() {
		String strSalesAgentID[] = testData.get("AssignSalesAgent").split(";");
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(CWAPitchBenefitDetailsPage_Ipad.sideMenuLink).click();
			waitUntilElementVisibleBy(driver, salesIcon, "ExplicitWaitLongestTime");
			driver.findElement(salesIcon).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (/* LeadsPage.strLeadFullName */
			/* ("ANGELINA ZZBRCRN") */(testData.get("FirstName") + " " + testData.get("LastName")).toUpperCase()
					.equals(driver.findElement(leadName).getText().toUpperCase())) {
				tcConfig.updateTestReporter("Sales Participants Page", "addSalesParticipants()", Status.PASS,
						"Lead name is displayed successfully");
			}
			/*
			 * if(verifyElementDisplayed(driver.findElement(By.xpath(
			 * "//p[contains(text(),'"+LeadsPage.createdTourID+"')]")))) {
			 * tcConfig.updateTestReporter("Sales Participants Page",
			 * "addSalesParticipants()", Status.PASS,
			 * "Tour ID is displayed successfully"); }
			 */
			// driver.findElement(By.xpath("//input[@id='employee-id']")).sendKeys(testData.get(strSalesAgentID[0]));
			Select s = new Select(driver.findElement(By.id("employee-function")));
			List<WebElement> list = s.getOptions();
			Set<String> listNames = new HashSet<String>(list.size());

			for (WebElement element : list) {
				// Set will not allow to add duplicate value
				if (listNames.add(element.getText()) == false) {
					tcConfig.updateTestReporter("Sales Participants Page", "addSalesParticipants()", Status.FAIL,
							"Employee function dropdown is having any duplicate entries " + element.getText());
				}
				tcConfig.updateTestReporter("Sales Participants Page", "addSalesParticipants()", Status.PASS,
						"Employee function dropdown is not having any duplicate entries " + element.getText());
			}

			/*
			 * waitForSometime(tcConfig.getConfig().get("LongWait"));
			 * waitUntilElementVisibleBy(driver, empID, "ExplicitWaitLongestTime"); String emp1 =
			 * strSalesAgentID[1]; fieldDataEnter(empID, emp1);
			 * waitUntilElementVisibleBy(driver, empIdSpan, "ExplicitWaitLongestTime");
			 * clickElementBy(empIdSpan);
			 * waitForSometime(tcConfig.getConfig().get("MedWait")); new
			 * Select(driver.findElement(By.id("employee-function"))).
			 * selectByValue("2: MANAGER_1");
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * clickElementBy(addBtn);
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * List<WebElement> empRowCount = driver.findElements(By.xpath(
			 * "//div[contains(@class,'employee-list')]//table/tbody/tr"));
			 * if(empRowCount.size()==2) { tcConfig.updateTestReporter(
			 * "Sales Participants Page", "addSalesParticipants()",
			 * Status.PASS,
			 * "Sales Participants Manager 1 is added successfully"); }else{
			 * tcConfig.updateTestReporter("Sales Participants Page",
			 * "addSalesParticipants()", Status.FAIL,
			 * "Sales Participants Manager 1 is not added successfully"); }
			 */
			// waitUntilElementVisibleBy(driver, saveWorksheetPopUp, "ExplicitWaitLongestTime");
			// driver.findElement(By.xpath("//input[@id='employee-id']")).sendKeys(testData.get(strSalesAgentID[1]));
			/*
			 * String emp2 = strSalesAgentID[1]; fieldDataEnter(empID, emp2);
			 * waitUntilElementVisibleBy(driver, empIdSpan, "ExplicitWaitLongestTime");
			 * clickElementBy(empIdSpan); new
			 * Select(driver.findElement(By.id("employee-function"))).
			 * selectByValue("8: SALES_ASSOCIATE");
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * clickElementBy(addBtn);
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * List<WebElement> empRowCount1 = driver.findElements(By.xpath(
			 * "//div[contains(@class,'employee-list')]//table/tbody/tr"));
			 * if(empRowCount1.size()==2) { tcConfig.updateTestReporter(
			 * "Sales Participants Page", "addSalesParticipants()",
			 * Status.PASS,
			 * "Sales Participants for Sales Agent is added successfully"); }
			 */
			/*
			 * String emp3 = strSalesAgentID[2]; fieldDataEnter(empID, emp3);
			 * waitUntilElementVisibleBy(driver, empIdSpan, "ExplicitWaitLongestTime");
			 * clickElementBy(empIdSpan); new
			 * Select(driver.findElement(By.id("employee-function"))).
			 * selectByValue("19: TO");
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * clickElementBy(addBtn);
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * List<WebElement> empRowCount2 = driver.findElements(By.xpath(
			 * "//div[contains(@class,'employee-list')]//table/tbody/tr"));
			 * if(empRowCount2.size()==3) { tcConfig.updateTestReporter(
			 * "Sales Participants Page", "addSalesParticipants()",
			 * Status.PASS, "Sales Participants for TO is added successfully"
			 * ); }
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(exitButton);
		} catch (Exception e) {
			tcConfig.updateTestReporter("Sales Participants Page", "addSalesParticipants()", Status.FAIL,
					"Sales Participants is not getting added successfully" + e.getMessage());
		}
	}

	public void selectTourByTourID() {
		try {
			waitUntilElementVisibleBy(driver, tourHomePage, "ExplicitWaitLongestTime");
			List<WebElement> allTourDashboardDetails = driver.findElements(tourHomePage);
			for (WebElement element : allTourDashboardDetails) {
				String attrTourID = element.getAttribute("id");
				if (attrTourID.equals(testData.get("TourID"))) {
					element.click();
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected By imgCheck = By.xpath(
			"(//h3[text()='Worksheet Milestones']//following::table)[1]/tbody/tr/td[2]//following::img[contains(@alt,'icon-checked')]");

	// protected By milestoneName = By.xpath("(//h3[text()='Worksheet
	// Milestones']//following::table)[1]/tbody/tr/td[1]//descendant::strong");

	public void worksheetProgressBarCheck() {
		String milestoneName;
		try {
			for (int i = 1; i <= 5; i++) {

				milestoneName = driver.findElement(By
						.xpath("((//h3[text()='Worksheet Milestones']//following::table)[1]/tbody/tr/td[1]//descendant::strong)["
								+ i + "]"))
						.getText();
				if (verifyElementDisplayed(driver.findElement(imgCheck))) {
					wsaRefNumber = driver.findElement(wsaRefNum).getText();
					tcConfig.updateTestReporter("Worksheet Milestone Widget", "worksheetProgressBarCheck()",
							Status.PASS,
							"Progress steps for" + milestoneName + " is displayed successfully for " + wsaRefNumber);
				} else {
					tcConfig.updateTestReporter("Worksheet Milestone Widget", "worksheetProgressBarCheck()",
							Status.FAIL, "Progress steps for" + milestoneName + " is not displayed successfully");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Worksheet Milestone Widget", "worksheetProgressBarCheck()", Status.FAIL,
					"Progress steps for Inventory, Down payment, Loan Details and Summay is not displayed successfully"
							+ e.getMessage());
		}
	}

	
	protected By matIcon = By.xpath("//button/mat-icon[text()=' collections_bookmark']");

	public void validateWSAnumber() {
		try {
			waitUntilElementVisibleBy(driver, matIcon, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(matIcon))) {
				clickElementBy(matIcon);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				wsaRefNumber = driver.findElement(wsaRefNum).getText();
				if (!wsaRefNumber.equals("")) {
					tcConfig.updateTestReporter("Worksheet Milestone Widget", "validateWSAnumber", Status.PASS,
							"WSA reference number" + wsaRefNumber + " is generated Successfully");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Worksheet Milestone Widget", "validateWSAnumber", Status.FAIL,
					"WSA reference number" + wsaRefNumber + " is not generated Successfully" + e.getMessage());
		}
	}

}
