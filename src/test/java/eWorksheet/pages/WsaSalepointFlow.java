package eWorksheet.pages;
/*package com.wvo.eWorksheet.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;

public class WsaSalepointFlow extends FunctionalComponents {

	public static String createdPitchID;
	public static String customerLastName;
	public static final Logger log = Logger.getLogger(WsaSalepointFlow.class);
	public String selectedCustName;

	public WsaSalepointFlow(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(name = "j_username")
	WebElement textUsername;

	
	 * enter user name
	 
	protected void setUserNameSP(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, textUsername, "ExplicitWaitLongestTime");
        driver.manage().deleteAllCookies();
		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, textUsername, "ExplicitWaitLongestTime");
		textUsername.sendKeys(strUserName);
		tcConfig.updateTestReporter("SalePointLogin", "setUserName", Status.PASS,
				"Username is entered for SalePoint Login");
	}

	@FindBy(name = "j_password")
	WebElement textPassword;

	*//**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 *//*
	protected void setPasswordSP(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		textPassword.sendKeys(dString);
		tcConfig.updateTestReporter("SalePointLogin", "setPassword", Status.PASS,
				"Entered password for SalePoint login");
	}

	@FindBy(xpath = "//input[@name='logon']")
	WebElement buttonLogIn;
	@FindBy(xpath = "//table[@class='client_table']/tbody/tr[2]/td")
	WebElement buildVersion;
	
	@FindBy(xpath = "//div[@id='error' and contains(.,'Password does not match')]")
	WebElement loginError;
	
	public void loginToSP(String strBrowser) throws Exception {

		this.driver=checkAndInitBrowser(strBrowser);

		driver.navigate().to(testData.get("URL_SALE"));
		driver.manage().window().maximize();
		waitUntilElementVisibleBy(driver,buildVersion,"ExplicitWaitLongestTime");
		if (verifyElementDisplayed(buildVersion)){
			tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.PASS, "Salepoint launched-Build version:" + buildVersion.getText().substring(53) );
		}else {
			tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.FAIL, "Salepoint buildversion text not displayed");
		}
		waitUntilElementVisibleBy(driver, textUsername, "ExplicitWaitLongestTime");
		setUserNameSP(testData.get("SP_UserID"));
		setPasswordSP(testData.get("SP_Password"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", buttonLogIn);

		// buttonLogIn.click();
	//	tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.PASS, "Log in button clicked");

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		try{
		waitUntilElementVisibleBy(driver, loginError, 80);
		tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.PASS, "1st Login attempt is failed");

		
		if (loginError.isDisplayed()){
			
			driver.navigate().to(testData.get("URL_SALE"));
			driver.manage().window().maximize();
			waitUntilElementVisibleBy(driver, textUsername, "ExplicitWaitLongestTime");
			
			
			setUserNameSP(testData.get("SP_UserID"));
			setPasswordSP(testData.get("SP_Password"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			JavascriptExecutor executor_repeat = (JavascriptExecutor) driver;
			executor_repeat.executeScript("arguments[0].click();", buttonLogIn);
	
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.PASS, "2nd Login attempt is made");

			}
		}
		catch(Exception e){
			
			tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.PASS, "1st Login attemp is successful");

		}
		
		
		waitUntilElementVisibleBy(driver, companySelect, "ExplicitWaitLongestTime");
		
		if (driver.findElement(companySelect).isDisplayed()){
			
			tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.PASS, "Log is successful");


		}
		
		
		
		
		
	}


	
	protected By locationSelect =By.xpath("//select[@name='location']");//name location
    protected By saveButton=By.xpath("//input[@value='Save']");//input value Save
    protected By mainMenButton=By.xpath("//input[@value='Main Menu']");//input value Main Menu
    protected By serviceEntityChange=By.xpath("//select[@name='serviceEntity']");//select name serviceEntity valuew 000 001 002



public void changeProfileLocation(){
    
    driver.navigate().to(testData.get("myProfileURL"));
    
    waitUntilElementVisibleBy(driver, locationSelect, "ExplicitWaitLongestTime");
    
    if(verifyObjectDisplayed(serviceEntityChange)){
        tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.PASS,
                     "serviceEntityChange Field present");
        
        String strCompany= testData.get("ServiceEntity");
        
        if (strCompany.equalsIgnoreCase("WBW")) {
			new Select(driver.findElement(serviceEntityChange)).selectByVisibleText("Worldmark By Wyndham");
        }        
        else if (strCompany.equalsIgnoreCase("WVR")) {
		new Select(driver.findElement(serviceEntityChange)).selectByVisibleText("Wyndham Vacation Resorts");
        } else if (strCompany.equalsIgnoreCase("WVRAP")) {
		new Select(driver.findElement(serviceEntityChange)).selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
        } else {
		tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.FAIL,
				"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
        }
      
    }
    waitForSometime(tcConfig.getConfig().get("LongWait"));

    if(verifyObjectDisplayed(locationSelect)){
           tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.PASS,
                        "Location Field present");
           
           String strLocation= testData.get("strLocation");
                 
           new Select(driver.findElement(locationSelect))
           .selectByVisibleText(strLocation);
           
           tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.PASS,
                        "Location Selected");
           
           clickElementJS(saveButton);
           waitForSometime(tcConfig.getConfig().get("MedWait"));
           waitUntilElementVisibleBy(driver, mainMenButton, "ExplicitWaitLongestTime");
          
           
    }
           
    else{
           tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.FAIL,
                        "Location Field not present");
    }

 
}
	By companySelect= By.xpath("//select[@id='companyList']");
	
	

	public void companySelectSalePoint() throws Exception {
		
		
		waitUntilElementVisibleIE(driver, companySelect, "ExplicitWaitLongestTime");

		String strCompany = testData.get("Entity");

		if (strCompany.equalsIgnoreCase("WBW")) {

			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Worldmark By Wyndham");

		} else if (strCompany.equalsIgnoreCase("WVR")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts");
		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//driver.switchTo().activeElement().sendKeys(Keys.TAB);
		

		tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.PASS,
				"Company Selected as: " + strCompany);
		
		clickElementJS(okBtn);
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Actions act = new Actions(driver);
		
		if(strCompany.equalsIgnoreCase("WVRAP")) {
			System.out.println("No Alert Present");
			
		}else {
		  
			try{
			new WebDriverWait(driver, "ExplicitWaitLongestTime").until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
			}catch(Exception e){
				System.out.println("No Alert Present after Login");
			}
		}
		tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.PASS,
				"HomePage Navigation Successful");
	}
	
	@FindBy(xpath="//span[text()='Contracts']") WebElement contractsLink;
	@FindBy(xpath="//div[contains(text(),'Contract')]") WebElement SummaryPage;
	
	public void searchNewContract() throws Exception {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.get("https://salepointtest2.corproot.com/webapp/ccis/contracts/contracts_customer_entry.jsp");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, customerNo, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(customerNo)) {

				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"New Contract Page Navigation Successful");
				customerNo.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				customerNo.sendKeys(LeadsPage.createdTourIDtestData.get("TourID"));
				clickElementJS(next1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				try {
					new WebDriverWait(driver, "ExplicitWaitLongestTime").until(ExpectedConditions.alertIsPresent());
					String stralertMsg = driver.switchTo().alert().getText();
					System.out.println(stralertMsg);
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} catch (Exception e) {
					System.out.println("No Alert Present");
				}
				if(verifyElementDisplayed(SummaryPage))
				{
					tcConfig.updateTestReporter("SalePointWVRMenuPage", "searchNewContract", Status.PASS,
							"WSA ref number"+CWAPackageWSAPage.wsaRefNumber+" is retrieved Successfully");
				}else{
					tcConfig.updateTestReporter("SalePointWVRMenuPage", "searchNewContract", Status.FAIL,
							"WSA ref number"+CWAPackageWSAPage.wsaRefNumber+" is not retrieved Successfully");
				}

			} else {

				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Page Navigation Error");

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.FAIL,
					"Page Navigation Error");
		}
	}
	




	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;
	// socialSecurityNumber
	@FindBy(xpath = "//input[@name='salesman']")
	WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	//@FindBy(xpath = "//input[@value='Save and Print']")
	@FindBy(xpath = "//input[@value='Save and Print' and contains(@onclick,'Print')]")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	WebElement conNo;
	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	String strContractNo = "";
	String strMemberNo = "";

	@FindBy(id = "customerNumber")
	WebElement customerNo;

	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;
	
	@FindBy(xpath = "//input[@value='Update']")
	WebElement updateVal;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	WebElement saleType;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	WebElement udiSelect;

	@FindBy(id = "Print")
	WebElement print;

	@FindBy(id = "newPoints")
	WebElement newPoints;

	@FindBy(xpath = "//input[@value='Calculate']")
	WebElement calculate;

	By nextClick = By.xpath("//input[@value='Next']");

	By customerNoInput = By.id("customerNumber");

	By saleTypeSelect = By.xpath("//input[@name='fvl_saleType' and @value='M']");

	By deleteCheck = By.xpath("//a[contains(.,'Delete')]");

	By udiClick = By.xpath("//input[@name='sales_types' and @value='UDI']");

	By newPointsCheck = By.id("newPoints");

	By calculateClick = By.xpath("//input[@value='Calculate']");

	By salePersonSelect = By.xpath("//input[@name='salesman']");

	//By saveClick = By.xpath("//input[@value='Save and Print']");
	By saveClick = By.xpath("//input[@value='Save and Print' and contains(@onclick,'Print')]");

	By printClick = By.id("Print");

	By memberNoCheck = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");

	By ssnSelect = By.xpath("//input[@name='ssn']");

	By genderSelect = By.xpath("//input[@name='sex' and @value='M']");

	By cashSelect = By.xpath("//td[contains(.,'Cash Sale')]/input[@type='radio']");
	By ccSelect = By.xpath("//td[contains(.,'CC Auto')]/input[@type='radio']");


	By achSelect = By.xpath("//td[contains(.,'ACH Auto Pay')]/input[@type='radio']");

	By headerContractProcessOwner = By.xpath("//div[contains(text(),'Contract Processing - Owners')]");

	By headerCustInfo = By.xpath("//div[contains(text(),'Contract Processing - Customer Information')]");
	
	By editPrimaryOwner = By.xpath("//div[contains(text(),'Edit Primary Owner')]");


	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By StatusOwner = By.xpath("//a[contains(.,'Status')]");

	By MonthFromDrpDwn = By.xpath("//select[@name='dobmonth']");
	By DateFromDrpDwn = By.xpath("//select[@name='dobday']");
	By YearFromDrpDwn = By.xpath("//select[@name='dobyear']");

	By primaryOwnerEdit = By.xpath("//input[@class='button' and @id='primaryOwner']");
	
	By PaymentEdit = By.xpath("//input[@class='button' and @id='loan']");

	
	By updateButton = By.xpath("//input[@class='button' and @value='Update']");
	
	By home_Phone = By.xpath("//input[@class='textBox' and @name='home_phone']");

	@FindBy(xpath = "//input[@class='textBox' and @name='home_phone']")
	WebElement home_Phone_fld;
	
		
	By inventory_Site =By.xpath("//select[@id='inventorySite']");
	
	
	@FindBy(xpath = "//input[contains(@name,'cc_payment_member_name')]")
	WebElement ccName;
	
	@FindBy(xpath = "//input[contains(@name,'cc_payment_account_number')]")
	WebElement ccNumber;
	
	
	By ccType = By.xpath("//select[contains(@name,'cc_payment_type')]");
	
	By ccMonth = By.xpath("//select[contains(@name,'cc_payment_exp_month')]");
	
	By ccYear = By.xpath("//select[contains(@name,'cc_payment_exp_year')]");



	By deleteListOfOwner = By.xpath("(//a[contains(.,'Delete')])[2]");
	By deleteFirstOwner = By.xpath("(//a[contains(.,'Delete')])[1]");
	
	By deleteonesecOwner = By.xpath("(//a[contains(.,'Delete')])");


	By statusListOfOwner = By.xpath("(//a[contains(.,'Status')])[1]");


	
	
	By relationshipStatus =By.xpath("//select[@name='relationship']");
	
	
	@FindBy(xpath = "//input[@name='ssn']")
	WebElement ssnField;
	
	@FindBy(xpath = "//input[@name='ficoBand']")
	WebElement fico;
	
//	@FindBy(xpath="//td[contains(.,'Primary Owner')]/./tr[@class='evenRow']/td[1][contains(.,*)]")
//	WebElement pName;

	@FindBy(xpath="//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	WebElement pName;

	//td[contains(.,'Primary Owner')]//tr[@class='evenRow']")[0]
	
	By docSign = By.xpath("//select[@name='justification']");
	
	public void sp_New_ContractFlow_Automation() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		//waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitWaitLongestTime");
		waitUntilElementVisibleIE(driver, customerNo, "ExplicitWaitLongestTime");
		
		int noOfSecOwn =0;
		List<WebElement> secOwnlist =null;
		List<WebElement> secStatus =null;

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("TourNumber"));

			clickElementJS(next1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.FAIL,
					"Page Navigation Error");

		}
		
		
	//	try{
			
			WebDriverWait wait = new WebDriverWait(driver,"ExplicitWaitLongestTime");
			wait.until(ExpectedConditions.alertIsPresent());
			
			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();
					tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
							"Automation Pop Up message");


				} catch (Exception e) {
					System.out.println("no Alert is displayed");
				}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, saveClick, "ExplicitWaitLongestTime");

			if (verifyObjectDisplayed(primaryOwnerEdit)) {

				driver.findElement(primaryOwnerEdit).click();

	
				
				//Owner Page - Primary Owner and Secondary Owner
				
				if (verifyObjectDisplayed(headerContractProcessOwner)) {
					
				//check the secondary owner count with delete buttons 
					
					if (verifyObjectDisplayed(deleteOwner)) {
						secOwnlist = driver.findElements(deleteOwner);
						
						noOfSecOwn= secOwnlist.size();
						
					}
					
					

					
					//check primary owner
					
					if (verifyElementDisplayed(pName)){
					
					System.out.println("Primary Owner Exists");
					
					//delete all secondary owner
					for (int i=0;i<noOfSecOwn;i++){
						
						clickElementJSWithWait(deleteFirstOwner);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						
					}

					
					}else{
						
					System.out.println("No Primary Owner Exists");
			
					//delete all secondary owner except one
					
						
					for (int i=0;i<(noOfSecOwn-1);i++){
						
						//clickElementJSWithWait(secOwnlist.get(0));
						clickElementJSWithWait(deleteListOfOwner);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
							
						}	
					
					//make one secondary to primary
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(statusListOfOwner)){
					clickElementJSWithWait(statusListOfOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
					
				}
						
				}

				
				
				
				if(verifyObjectDisplayed(editPrimaryOwner)) {
					

					try {
						
					//select the relationship
						
					waitForSometime(tcConfig.getConfig().get("MedWait"));

						
					new Select(driver.findElement(relationshipStatus)).selectByIndex(2);

						
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					clickElementJS(driver.findElement(genderSelect));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					
					//select sale type
					
					if (verifyElementDisplayed(saleType)) {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
								"Customer Info page Displayed");

						//JavascriptExecutor executor = (JavascriptExecutor) driver;
						clickElementJS(saleType);

						driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						

					}else {
						System.out.println("SaleType is not visible ");

					}
					
					//ssn field 
					
					if (ssnField.isEnabled()==false){
						
						System.out.println("SSN Not Required User Can Proceed");

					}else{
						if (ssnField.getAttribute("value").isEmpty()==true){
						ssnField.clear();
						ssnField.sendKeys(testData.get("ssnNumber"));
						}
					}
					
					//DateOf Birth
					
					
					
					 String dob = testData.get("DOB");
					 String dobmonth = "", dobDay = "";
					 String arr[] = dob.split("/");
					
					 // MonthFormation
					 if (arr[0].length() == 1) {
						dobmonth = "0" + arr[0];
					 } else {
						dobmonth = arr[0];
					 }
					// DayFormation
					 if (arr[1].length() == 1) {
						dobDay = "0" + arr[1];
					 } else {
						dobDay = arr[1];
					 }
					// Year
					 
					 String dobYear = arr[2];
					 
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					
					if (driver.findElement(MonthFromDrpDwn).getAttribute("value").equals("")){
						//new Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);
						new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(dobmonth);

					}
					
					
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));


					if (driver.findElement(DateFromDrpDwn).getAttribute("value").equals("")){
						//new Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);
						new Select(driver.findElement(DateFromDrpDwn)).selectByValue(dobDay);

					}
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));


					if (driver.findElement(YearFromDrpDwn).getAttribute("value").equals("")) {
						//new Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);
						new Select(driver.findElement(YearFromDrpDwn)).selectByValue(dobYear);

					}
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					
					if (home_Phone_fld.getAttribute("value").equals("")){
						home_Phone_fld.sendKeys(testData.get("homeNumber"));

					}
					
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					
					clickElementJS(updateVal);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					}catch(Exception e){
						System.out.println("SSN Cannot Be Entered");
					}


				}	
				
				
				if (verifyObjectDisplayed(headerContractProcessOwner)) {

					
				//check the secondary owner count with delete buttons 
					
					if (verifyObjectDisplayed(deleteOwner)) {
						secOwnlist = driver.findElements(deleteOwner);
						
						noOfSecOwn= secOwnlist.size();
						
					}
					
		

					
					//check primary owner
					
					//if (verifyElementDisplayed(pName)){
					
					//System.out.println("Primary Owner Exists");
					
					//delete all secondary owner
					for (int i=0;i<noOfSecOwn;i++){
						
						clickElementJSWithWait(deleteonesecOwner);
						waitForSometime(tcConfig.getConfig().get("MedWait"));	
					
						if (ExpectedConditions.alertIsPresent() != null) {
						try {
			
							driver.switchTo().alert().accept();
			
							} catch (Exception e) {
								System.out.println(e);
							}
						}
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						
					//}

					
					}			
					
					

				if (verifyElementDisplayed(nextVal)){			
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(nextVal);
				}
				}
				
				

			} else {
				System.out.println("No Primary owner edit button");
			}


			waitUntilElementVisibleIE(driver, saveClick, "ExplicitWaitLongestTime");

			
			if (verifyObjectDisplayed(PaymentEdit)) {

				driver.findElement(PaymentEdit).click();		
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no alert is displayed");
					}
				}
				
				waitUntilElementVisibleIE(driver, ccSelect, "ExplicitWaitLongestTime");				
				
				clickElementJS(driver.findElement(ccSelect));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no Alert is displayed");
					}
				
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				ccName.clear();				
				ccName.click();
				ccName.sendKeys(testData.get("CCName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(ccType).click();
				String cardType = testData.get("CCType");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'"+cardType+"')]")).click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				ccNumber.clear();
				ccNumber.click();				
				ccNumber.sendKeys(testData.get("CCNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				driver.findElement(ccMonth).click();
				String expMnth = testData.get("CCExpMnth");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'"+expMnth+"')]")).click();

				
				driver.findElement(ccYear).click();
				String expYr = testData.get("CCExpYear");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'"+expYr+"')]")).click();


				

				//new Select(driver.findElement(ccYear)).selectByValue(testData.get("CCExpYear"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				
				waitUntilElementVisibleIE(driver, nextVal, "ExplicitWaitLongestTime");
				clickElementJS(nextVal);
				
				
				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no Alert is displayed");
					}
				
				}
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleIE(driver, calculate, "ExplicitWaitLongestTime");
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));


				if (verifyElementDisplayed(calculate)) {
					tcConfig.updateTestReporter("SalePointCalculationPage", "sp_New_ContractFlow_Automation", Status.PASS,
							"Money Screen present");

					clickElementJS(calculate);
					System.out.println("clicked calculate button");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					//driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleIE(driver, nextVal, "ExplicitWaitLongestTime");
					nextVal.click();
					//clickElementJS(nextVal);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					System.out.println("clicked Next button after calculate");
				}
					else
					{ System.out.println("calculate element not diaplyed");
				} 
				
				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no Alert is displayed");
					}
				
				}
				

			}			
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleIE(driver, savecontract, "ExplicitWaitLongestTime");

			
			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("ContractSavePage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("ContractSavePage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Contract Save Page Navigation Error");

			}

			// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, printClick, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(print)) {
				clickElementJSWithWait(print);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText().trim();

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Member number could not be retrieved");
			}

		} else {

			tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
					"Automation Pop Up is not available");
			
			
		}
					
				
	}

		
	

	
//	@FindBy(xpath = "//a[contains(.,'Status')]")
//	WebElement statusSec;

	public void sp_New_ContractFlow_NOpopUp() throws Exception {
		
		int noOfSecOwn =0;
		List<WebElement> secOwnlist =null;
		List<WebElement> secStatus =null;

		
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		

		waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitWaitLongestTime");
		
		
		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		//if worksheet is from different site, pop up  
		
		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		
		//Owner Page - Primary Owner and Secondary Owner
		
		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			
		//check the secondary owner count with delete buttons 
			
			if (verifyObjectDisplayed(deleteOwner)) {
				secOwnlist = driver.findElements(deleteOwner);
				
				noOfSecOwn= secOwnlist.size();
				
			}
			
			
//			if (verifyObjectDisplayed(StatusOwner)) {
//				secStatus = driver.findElements(StatusOwner);
//								
//			}
			
			//check primary owner
			
			if (verifyElementDisplayed(pName)){
			
			System.out.println("Primary Owner Exists");
			
			//delete all secondary owner
			for (int i=0;i<noOfSecOwn;i++){
				
				clickElementJSWithWait(deleteFirstOwner);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
			}

			
			}else{
				
			System.out.println("No Primary Owner Exists");
	
			//delete all secondary owner except one
			
				
			for (int i=0;i<(noOfSecOwn-1);i++){
				
				//clickElementJSWithWait(secOwnlist.get(0));
				clickElementJSWithWait(deleteListOfOwner);

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
					
				}	
			
			//make one secondary to primary
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(statusListOfOwner)){
			clickElementJSWithWait(statusListOfOwner);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
			
		}
				
		}

		
		
		
		if(verifyObjectDisplayed(headerCustInfo)) {
			

			try {
				
			//select the relationship
				
			waitForSometime(tcConfig.getConfig().get("MedWait"));

				
			new Select(driver.findElement(relationshipStatus)).selectByIndex(2);

				
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(ssnSelect).click();
			driver.findElement(ssnSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(driver.findElement(genderSelect));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			//select sale type
			
			if (verifyElementDisplayed(saleType)) {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
						"Customer Info page Displayed");

				//JavascriptExecutor executor = (JavascriptExecutor) driver;
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				

			}else {
				System.out.println("MainLine  Not Present User Can Proceed");

			}
			
			//ssn field 
			
			if (ssnField.isEnabled()==false){
				
				System.out.println("SSN Not Required User Can Proceed");

			}else{
				
				ssnField.clear();
				ssnField.sendKeys(testData.get("ssnNumber"));
				
			}
			
			//DateOf Birth
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			
			if (driver.findElement(MonthFromDrpDwn).getText() ==""){
				new Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);;

			}
			
			
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));


			if (driver.findElement(DateFromDrpDwn).getText() ==""){
				new Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);;

			}
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));


			if (driver.findElement(YearFromDrpDwn).getText() ==""){
				new Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);;

			}
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			
			if (home_Phone_fld.getText() ==""){
				home_Phone_fld.sendKeys(testData.get("homeNumber"));

			}
			
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			}catch(Exception e){
				System.out.println("SSN Cannot Be Entered");
			}
			//waitForSometime(tcConfig.getConfig().get("MedWait"));
		
			//clickElementJS(nextVal);

		}	
		

		
		if (verifyObjectDisplayed(headerContractProcessOwner)) {


		if (verifyElementDisplayed(nextVal)){			
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextVal);
		}
		}
		

				
			
		
	//	waitForSometime(tcConfig.getConfig().get("LongWait"));	
		

	
		waitUntilElementVisibleIE(driver, udiClick, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(udiSelect)) {
			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL, "UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitWaitLongestTime");
		
		if (verifyElementDisplayed(newPoints)) {
			
			//select inventory

			//new Select(driver.findElement(inventory_Site)).selectByValue(testData.get("invSite"));
			

			String invName = testData.get("invSite");
			driver.findElement(inventory_Site).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			

			driver.findElement(By.xpath("//select/option[contains(.,'"+invName+"')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));
			


		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitWaitLongestTime");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitWaitLongestTime");

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitWaitLongestTime");

		if(verifyObjectDisplayed(cashSelect)){
			clickElementJS(driver.findElement(cashSelect));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			clickElementJS(driver.findElement(cashSelect));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
		}else{
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Cash Select not present");
		}
		
		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, "ExplicitWaitLongestTime");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitWaitLongestTime");

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		//driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		//waitUntilElementVisibleIE(driver, printClick, "ExplicitWaitLongestTime");
		waitUntilElementVisibleIE(driver, print, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Member number could not be retrieved");
		}

	}
	
	
	
	
	
	
	
	
	
	
	By discoveryMember=By.xpath("//input[@name='member_number']");
	By searchBtn=By.xpath("//input[@name='search']");
	By upgradeButton=By.xpath("//input[@value='Process Upgrade']");
	
	
	
	By tradeContract= By.xpath("//input[@name='contract_number']");
	By viewtradeContracts=By.xpath("//input[@value='View Traded Contracts']");
	By processTrade=By.xpath("//input[@value='Process Trades']");
	
	
	
	

	@FindBy(xpath = "//input[@value='Experience']")
	WebElement experienceClick;

	@FindBy(xpath = "//input[@value='WorldMark']")
	WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	WebElement finalizeButton;
	
	@FindBy(xpath = "//input[@value='Void']")
	WebElement voidButton;

	@FindBy(xpath = "//a[contains(.,'ALL BY DATE')]")
	WebElement selectAllByDate;

	@FindBy(xpath = "//select[@name='startmonth']")
	WebElement startmonth;

	@FindBy(xpath = "//select[@name='startday']")
	WebElement startday;

	@FindBy(xpath = "//input[@name='getData']")
	WebElement getData;

	@FindBy(xpath = "//input[@value='Change Status']")
	WebElement changeStatus;
	
	@FindBy(xpath = "//input[@value='Transmit']")
	WebElement Transmit;
	
	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	WebElement returnBack;
	
	@FindBy(xpath = "//tr/td[contains(.,'T')]")
	WebElement validateStatus;
	
	
	By searchFilter=By.xpath("//a[contains(.,'ALL BY DATE')]");
	
	By statusChangeClick=By.xpath("//input[@value='Change Status']");
	
	By transmitClick=By.xpath("//input[@value='Transmit']");
	
	By reportBack =By.xpath("//a[contains(.,'Return to Batch Report')]");
	
	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	WebElement ReturnReport;
	
	By StatusVali= By.xpath("//tr/td[contains(.,'T')]");
	
	//By StatusVoid= By.xpath("//tr/td[contains(.,'V')]");

	@FindBy(xpath = "//td[contains(.,'V')]")
	WebElement StatusVoid;
	
	@FindBy(xpath = "//input[@value='Void']")
	WebElement Void;
	
	By voidClick=By.xpath("//input[@value='Void']");
	
	By VoidElementWVR = By.xpath("//td[contains(.,'V')]");

	public void sp_New_contractSearch_and_VOID() throws Exception {
		
		List<WebElement> voidListWVR =null;

		int voidCountBefore =0;
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		waitUntilElementVisibleIE(driver, searchFilter, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(selectAllByDate)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.PASS,
					"Contract in batch report Page Displayed");

			clickElementJS(selectAllByDate);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			 //LocalDate today= LocalDate.now();
			Calendar cal =  Calendar.getInstance();
			DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			
			Date today=cal.getTime();
				//cal.MONTH;
			
				int monthVal= (cal.get(Calendar.MONTH));
			
			String strMonth= String.valueOf(monthVal).trim();
			String strDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH)).trim();
			
			waitUntilElementVisibleIE(driver, By.name("startmonth"), "ExplicitWaitLongestTime");
			
			
				new Select(driver.findElement(By.name("startmonth"))).selectByValue(strMonth);
			
				new Select(driver.findElement(By.name("startday"))).selectByValue(strDate);
				//new Select(driver.findElement(By.name("startday"))).selectByValue("1");

			
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			
				clickElementJS(getData);
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'"+strContractNo+"')]"), "ExplicitWaitLongestTime");
			
			
			if (verifyObjectDisplayed(VoidElementWVR)){
				
				voidListWVR= driver.findElements(VoidElementWVR);
				
				 voidCountBefore= voidListWVR.size();
				
			}

			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'"+strContractNo+"')]")))) {

				tcConfig.updateTestReporter("SalePointContractSeacrh", "sp_New_contractSearch_and_VOID", Status.PASS,
						"Contract No Verified and is: " + strContractNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'"+strContractNo+"')]"));

				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointContractSeacrh", "sp_New_contractSearch_and_VOID", Status.FAIL,
						"Contract No Not Verified");

			}

			waitUntilElementVisibleIE(driver, statusChangeClick, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(changeStatus)) {
				tcConfig.updateTestReporter("SalePointWVRReportPage", "sp_New_contractSearch_and_VOID", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				//clickElementJSWithWait(changeStatus);
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",changeStatus);

				
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				waitUntilElementVisibleIE(driver, voidClick, "ExplicitWaitLongestTime");

				if (verifyElementDisplayed(Void)) {

					//clickElementJSWithWait(Void);
					Void.click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					//driver.switchTo().alert().accept();
					try {

						driver.switchTo().alert().accept();
						tcConfig.updateTestReporter("SalePointWVRReportPage", "sp_New_contractSearch_and_VOID", Status.PASS,
								"Contract Status update to VOID is performed");

					} catch (Exception e) {
						System.out.println("");
					}

					waitUntilElementVisibleIE(driver, ReturnReport, "ExplicitWaitLongestTime");
					
					if(verifyElementDisplayed(ReturnReport)) {
						tcConfig.updateTestReporter("SalePointWVRReportPage", "sp_New_contractSearch_and_VOID", Status.PASS,
								"Status Change Successful");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						//ReturnReport.click();
						//clickElementJSWithWait(ReturnReport);
						((JavascriptExecutor) driver).executeScript("arguments[0].click();",ReturnReport);

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if(verifyElementDisplayed(ReturnReport)){
							//clickElementJS(ReturnReport);
							//((JavascriptExecutor) driver).executeScript("arguments[0].click();",ReturnReport);
							ReturnReport.click();
						}
						
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						
						
					}else {
						tcConfig.updateTestReporter("SalePointWVRReportPage", "sp_New_contractSearch_and_VOID", Status.FAIL,
								"Status Change UnSuccessful");
					}
					

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					waitUntilElementVisibleIE(driver, VoidElementWVR, "ExplicitWaitLongestTime");
					
					
					
					if(verifyElementDisplayed(StatusVoid)){
						
						
						tcConfig.updateTestReporter("SalePointWVRReportPage", "sp_New_contractSearch_and_VOID", Status.PASS,
								"Back to Report is successful");
					}
							
					
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'V')]"));

					voidListWVR.clear();
					voidListWVR= driver.findElements(VoidElementWVR);
						
					int voidCountAfter= voidListWVR.size();
					
					if ((list2.get(0).getText().contains("V"))&&(voidCountAfter>voidCountBefore)) {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.PASS,
								"Contract Void is completed");
					} else {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.FAIL,
								"Contract Void is not completed");

					}
					
					waitForSometime(tcConfig.getConfig().get("MedWait"));


				}

			} else {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	
	
	
	}
	

	By experienceSelect = By.xpath("//input[@value='Experience']");

	By summaryPage = By.xpath("//tr[@class='page_title']//div");

	By worldMarkSelect = By.xpath("//input[@value='WorldMark']");

	By nextValue = By.xpath("//input[@value='Next']");
	
	By MonthFromDrpDwnWBW=By.xpath("//select[@name='beginDateMonth']");
	By DateFromDrpDwnWBW=By.xpath("//select[@name='beginDateDay']");
	By YearFromDrpDwnWBW=By.xpath("//select[@name='beginDateYear']");
	
	By VoidElement = By.xpath("//td[contains(.,'Void')]");


	public void sp_Ex_contractSearchAndVoid() throws Exception {
		
		String fromDate=addDateInSpecificFormat("MM/dd/yyyy",-1);
		String[] arrDate=fromDate.split("/");
		String strMonthFromDrpDwn=arrDate[0];
		String strDateFromDrpDwn=arrDate[1];
		String strYearFromDrpDwn=arrDate[2];
		List<WebElement> voidList =null;

		int voidCountBefore =0;
		
		if (strMonthFromDrpDwn.startsWith("0")){
			strMonthFromDrpDwn=strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")){
			strDateFromDrpDwn=strDateFromDrpDwn.replace("0", "");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, experienceSelect, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(experienceClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
					"Contract Management Page Displayed");
			
			
			new Select(driver.findElement(MonthFromDrpDwnWBW)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwnWBW)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwnWBW)).selectByValue(strYearFromDrpDwn);
			

			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(experienceClick);
			
			//clickElementJS(worldmarkClick);
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), "ExplicitWaitLongestTime");
			
			if (verifyObjectDisplayed(VoidElement)){
				
				 voidList= driver.findElements(VoidElement);
				
				 voidCountBefore= voidList.size();
				
			}

			
			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryPage, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(voidButton)) {

					clickElementJSWithWait(voidButton);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					driver.switchTo().alert().accept();

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Void')]"), "ExplicitWaitLongestTime");
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Void')]"));
					//List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'" + strMemberNo + "')]"));
					
					voidList.clear();
					voidList= driver.findElements(VoidElement);
						
					int voidCountAfter= voidList.size();

					if ((list2.get(0).getText().contains("Void")) && (voidCountAfter> voidCountBefore)) {
						
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
								"Contract Void");
					} else {						

						
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
								"Contract was not Void");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}
	
	

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtnwbw;

	@FindBy(name = "tourID")
	WebElement tourIDwbw;

	@FindBy(name = "tourTime")
	WebElement tourTimewbw;

	@FindBy(xpath = "//input[@name='next']")
	WebElement nextOwner;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next2;

	// socialSecurityNumber
//	@FindBy(id = "wbwcommissions_salesMan")
//	WebElement saleper;
//
//	@FindBy(xpath = "//a[contains(.,'Delete')]")
//	WebElement deleteSec;
//
//	@FindBy(id = "saveContract")
//	WebElement savecontract;
//
//	@FindBy(xpath = "//input[@name='print']")
//	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

//	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
//	WebElement conNo;
//
//	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
//	WebElement memberNo;
//
//	@FindBy(xpath = "//span[contains(text(), 'Contracts')]")
//	WebElement contracts;
//
//	String strContractNo = "";
//	String strMemberNo = "";

	By tourIdEnter = By.name("tourID");

	//By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By paymentSelect = By.name("paymentType");

	//By nextClick = By.xpath("//input[@name='next']");

	By salePersonId = By.id("wbwcommissions_salesMan");

	By saveContractClick = By.id("saveContract");
	
	@FindBy(xpath = "//input[@id='saveContract']")
	WebElement savecontractWBW;

	//By printClick = By.xpath("//input[@name='print']");

	//By memberNoCheck = By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]");
	
	
	By editMemberWBW = By.xpath("//tr/td[contains(text(),'Owner Information')]/../td/input[@type='button']");
	
	By editPaymentWBW = By.xpath("//tr/td[contains(text(),'Finance Information')]/../td/input[@type='button']");

	@FindBy(xpath = "//tr/td[contains(text(),'Finance Information')]/../td/input[@type='button']")
	WebElement editPaymentWBWelmnt;

	@FindBy(xpath = "///tr/td[contains(text(),'Owner Information')]/../td/input[@type='button']")
	WebElement editMemberWBWelmnt;
	
	By homePhoneSelect = By.xpath("//input[@name='homePhone']");
	
	@FindBy(xpath = "//input[@name='homePhone']")
	WebElement homePhoneWBW;

	
	By primaryOwnerPage= By.xpath("//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]");
	
	By financePage= By.xpath("//tr[@class='page_title']//div[contains(.,'Finance Information')]");
	
	By ownerDetailsPage =By.xpath("//tr[@class='page_title']//div[contains(.,'Owners')]");
	
	By ssnWBW = By.xpath("//input[@name='socialSecurityNumber']");
	
	@FindBy(xpath = "//input[@name='socialSecurityNumber']")
	WebElement ssnWBWSelect;

//	By MonthFromDrpDwn=By.xpath("//select[@name='beginDateMonth']");
//	By DateFromDrpDwn=By.xpath("//select[@name='beginDateDay']");
//	By YearFromDrpDwn=By.xpath("//select[@name='beginDateYear']");
	
	

	@FindBy(xpath="//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	WebElement pNameWBW;
	By editPOwnerWBW = By.xpath("//a[contains(.,'Edit')][1]");
	By deleteOwnerWBW = By.xpath("//a[contains(.,'Delete')]");
	By deleteFirstOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[1]");
	By deleteListOfOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[2]");
	
	By MakePrimaryWBW = By.xpath("//a[contains(.,'Make Primary')]");
	
	@FindBy(xpath="//select[contains(@id,'loanPACId')]")
	WebElement contractPayMethod;

	@FindBy(xpath = "//input[contains(@name,'ccPacNameOnCard')]")
	WebElement ccNameWBW;
	
	@FindBy(xpath = "//input[contains(@name,'ccPacCardNum')]")
	WebElement ccNumberWBW;
	
	
	By ccTypeWBW = By.xpath("//select[contains(@name,'ccPacCardType')]");
	
	By ccMonthWBW = By.xpath("//select[contains(@name,'ccPacCardExpiresMonth')]");
	
	By ccYearWBW = By.xpath("//select[contains(@name,'ccPacCardExpiresYear')]");
	
	@FindBy(xpath = "//input[@value='Next']")
	WebElement financeNext;
	
	
	public void SalePoint_Exp_Automation_Flow() throws Exception {
		
		int noOfSecOwnWBW =0;
		List<WebElement> secOwnlistWBW =null;

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourID, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("TourNumber"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			
			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));		
		
		WebDriverWait wait = new WebDriverWait(driver,"ExplicitWaitLongestTime");
		wait.until(ExpectedConditions.alertIsPresent());
		
		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
						"Automation Pop Up message is displayed");


			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
	
			waitUntilElementVisibleIE(driver, saveContractClick, "ExplicitWaitLongestTime");
	////////		
			
			//Edit the Owner 
			
			if (verifyObjectDisplayed(editMemberWBW)) {
				
				driver.findElement(editMemberWBW).click();
								
				if(verifyObjectDisplayed(primaryOwnerPage)){
					
				
					//ssn Field 					
					
					if (ssnWBWSelect.isEnabled()==false){
						
						System.out.println("SSN Not Required User Can Proceed");

					}else{
						if (ssnWBWSelect.getAttribute("value").isEmpty()==true){
							ssnWBWSelect.clear();
							ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
						}
					}
					
					if (homePhoneWBW.getAttribute("value").equals("")){
						homePhoneWBW.sendKeys(testData.get("homeNumber"));

					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					
					clickElementJS(nextOwner);
					
					
					
				}else if(verifyElementDisplayed(driver.findElement(ownerDetailsPage))){
					System.out.println("OwnerDetails Page Directly Opened");
					
					
	//check the secondary owner count with delete buttons 
					
					if (verifyObjectDisplayed(deleteOwnerWBW)) {
						secOwnlistWBW = driver.findElements(deleteOwnerWBW);
						
						noOfSecOwnWBW= secOwnlistWBW.size();
						
					}
					
						
					//check primary owner
					
					if (verifyElementDisplayed(pNameWBW)){
					
					//System.out.println("Primary Owner Exists");
					
					//delete all secondary owner
					for (int i=0;i<noOfSecOwnWBW;i++){
						
						clickElementJSWithWait(deleteFirstOwnerWBW);
						waitForSometime(tcConfig.getConfig().get("MedWait"));	
					
						if (ExpectedConditions.alertIsPresent() != null) {
						try {
			
							driver.switchTo().alert().accept();
			
							} catch (Exception e) {
								System.out.println(e);
							}
						}
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						
					}			
				}else {
						System.out.println("Primary Owner Does not exist");
						
							
						//delete all secondary owner except one
						
						
						for (int i=0;i<(noOfSecOwnWBW-1);i++){
							
							//clickElementJSWithWait(secOwnlist.get(0));
							clickElementJSWithWait(deleteListOfOwnerWBW);

							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
								
							}	
						
						//make one secondary to primary
						
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (verifyObjectDisplayed(MakePrimaryWBW)){
						clickElementJSWithWait(MakePrimaryWBW);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
						
					}
			
					if (verifyObjectDisplayed(editPOwnerWBW)) {

						clickElementJSWithWait(editPOwnerWBW);					
					
					if(verifyObjectDisplayed(primaryOwnerPage)){
						if (ssnWBWSelect.isEnabled()==false){
							
							System.out.println("SSN Not Required User Can Proceed");

						}else{
							if (ssnWBWSelect.getAttribute("value").isEmpty()==true){
								ssnWBWSelect.clear();
								ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
							}
						}
						
						if (homePhoneWBW.getAttribute("value").equals("")){
							homePhoneWBW.sendKeys(testData.get("homeNumber"));

						}
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						
						clickElementJS(nextOwner);
						
					}
						
					}
					
			} else {
				System.out.println("No Primary owner edit button");
			}
		
		
		
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, nextOwner, "ExplicitWaitLongestTime");
			

			clickElementJS(nextOwner);

		
			waitUntilElementVisibleIE(driver, editPaymentWBW, "ExplicitWaitLongestTime");				
			
			clickElementJS(driver.findElement(editPaymentWBW));
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			//
			
			
			waitUntilElementVisibleIE(driver, financePage, "ExplicitWaitLongestTime");	
			
			waitUntilElementVisibleBy(driver, contractPayMethod, "ExplicitWaitLongestTime");
						
			
			if (verifyElementDisplayed(contractPayMethod)){
				
				
				contractPayMethod.click();
				String payMethod = testData.get("PayMethod");

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//select[contains(@id,'loanPACId')]/option[contains(.,'"+payMethod+"')]")).click();
				
			}
						
			
			waitUntilElementVisibleBy(driver, ccNameWBW, "ExplicitWaitLongestTime");
			
			ccNameWBW.clear();				
			ccNameWBW.click();
			ccNameWBW.sendKeys(testData.get("CCName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(ccTypeWBW).click();
			String cardType = testData.get("CCType");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select[contains(@name,'ccPacCardType')]/option[contains(.,'"+cardType+"')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			ccNumberWBW.clear();
			ccNumberWBW.click();				
			ccNumberWBW.sendKeys(testData.get("CCNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			driver.findElement(ccMonthWBW).click();
			String expMnth = testData.get("CCExpMnth");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select[contains(@name,'ccPacCardExpiresMonth')]/option[contains(.,'"+expMnth+"')]")).click();

			
			driver.findElement(ccYearWBW).click();
			String expYr = testData.get("CCExpYear");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select[contains(@name,'ccPacCardExpiresYear')]/option[contains(.,'"+expYr+"')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			
			waitUntilElementVisibleIE(driver, financeNext, "ExplicitWaitLongestTime");
			clickElementJS(financeNext);
			
			waitUntilElementVisibleIE(driver, saveContractClick, "ExplicitWaitLongestTime");


			if (verifyElementDisplayed(savecontractWBW)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontractWBW);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Contract Save Page Navigation Error");

			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, generate, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(generate)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Review Page Displayed and Generate Button Clicked");
				//clickElementJSWithWait(generate);
				clickElementJS(generate);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Generate contract Page Navigation Error");
			}

			//waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), "ExplicitWaitLongestTime");
			waitUntilElementVisibleIE(driver, conNo, "ExplicitWaitLongestTime");

			
			
			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText();

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Member number could not be retrieved");
				}

							
			}

			}else {
				
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Automation Pop Up Not Available");
				
				
	}

	
	}
	
	public void sp_New_ContractFlow_Automation_DOCUSIGN() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		//waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitWaitLongestTime");
		waitUntilElementVisibleIE(driver, customerNo, "ExplicitWaitLongestTime");
		
		int noOfSecOwn =0;
		List<WebElement> secOwnlist =null;
		List<WebElement> secStatus =null;

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("TourNumber"));

			clickElementJS(next1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.FAIL,
					"Page Navigation Error");

		}
		
		
	//	try{
			
			WebDriverWait wait = new WebDriverWait(driver,"ExplicitWaitLongestTime");
			wait.until(ExpectedConditions.alertIsPresent());
			
			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();
					tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
							"Automation Pop Up message");


				} catch (Exception e) {
					System.out.println("no Alert is displayed");
					
					//retry once
					
					waitUntilElementVisibleBy(driver, headerContractProcessOwner, "ExplicitWaitLongestTime");

					
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					driver.navigate().to(testData.get("MenuURL"));

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					//waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitWaitLongestTime");
					waitUntilElementVisibleIE(driver, customerNo, "ExplicitWaitLongestTime");
					
					if (verifyElementDisplayed(customerNo)) {

						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
								"New Contract Page Navigation Successful");
						customerNo.click();
						customerNo.sendKeys(testData.get("TourNumber"));

						clickElementJS(next1);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} else {

						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.FAIL,
								"Page Navigation Error");

					}
					
						
						WebDriverWait waitSecondTime = new WebDriverWait(driver,"ExplicitWaitLongestTime");
						waitSecondTime.until(ExpectedConditions.alertIsPresent());
						
						if (ExpectedConditions.alertIsPresent() != null) {
							try {

								driver.switchTo().alert().accept();
								tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
										"Automation Pop Up message");


							} catch (Exception e1) {
								System.out.println("no Alert is displayed");
		
											
							}	
						
						
					}
					
				}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, saveClick, "ExplicitWaitLongestTime");

			if (verifyObjectDisplayed(primaryOwnerEdit)) {

				driver.findElement(primaryOwnerEdit).click();

	
				
				//Owner Page - Primary Owner and Secondary Owner
				
				if (verifyObjectDisplayed(headerContractProcessOwner)) {
					
				//check the secondary owner count with delete buttons 
					
					if (verifyObjectDisplayed(deleteOwner)) {
						secOwnlist = driver.findElements(deleteOwner);
						
						noOfSecOwn= secOwnlist.size();
						
					}
					
					

					
					//check primary owner
					
					if (verifyElementDisplayed(pName)){
					
					System.out.println("Primary Owner Exists");
					
					//delete all secondary owner
					for (int i=0;i<noOfSecOwn;i++){
						
						clickElementJSWithWait(deleteFirstOwner);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						
					}

					
					}else{
						
					System.out.println("No Primary Owner Exists");
			
					//delete all secondary owner except one
					
						
					for (int i=0;i<(noOfSecOwn-1);i++){
						
						//clickElementJSWithWait(secOwnlist.get(0));
						clickElementJSWithWait(deleteListOfOwner);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
							
						}	
					
					//make one secondary to primary
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(statusListOfOwner)){
					clickElementJSWithWait(statusListOfOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
					
				}
						
				}

				
				
				
				if(verifyObjectDisplayed(editPrimaryOwner)) {
					

					try {
						
					//select the relationship
						
					waitForSometime(tcConfig.getConfig().get("MedWait"));

						
					new Select(driver.findElement(relationshipStatus)).selectByIndex(2);

						
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					clickElementJS(driver.findElement(genderSelect));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					
					//select sale type
					
					if (verifyElementDisplayed(saleType)) {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
								"Customer Info page Displayed");

						//JavascriptExecutor executor = (JavascriptExecutor) driver;
						clickElementJS(saleType);

						driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						

					}else {
						System.out.println("SaleType is not visible ");

					}
					
					//ssn field 
					
					if (ssnField.isEnabled()==false){
						
						System.out.println("SSN Not Required User Can Proceed");

					}else{
						if (ssnField.getAttribute("value").isEmpty()==true){
						ssnField.clear();
						ssnField.sendKeys(testData.get("ssnNumber"));
						}
					}
					
					//DateOf Birth
					
					
					
					 String dob = testData.get("DOB");
					 String dobmonth = "", dobDay = "";
					 String arr[] = dob.split("/");
					
					 // MonthFormation
					 if (arr[0].length() == 1) {
						dobmonth = "0" + arr[0];
					 } else {
						dobmonth = arr[0];
					 }
					// DayFormation
					 if (arr[1].length() == 1) {
						dobDay = "0" + arr[1];
					 } else {
						dobDay = arr[1];
					 }
					// Year
					 
					 String dobYear = arr[2];
					 
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					
					if (driver.findElement(MonthFromDrpDwn).getAttribute("value").equals("")){
						//new Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);
						new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(dobmonth);

					}
					
					
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));


					if (driver.findElement(DateFromDrpDwn).getAttribute("value").equals("")){
						//new Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);
						new Select(driver.findElement(DateFromDrpDwn)).selectByValue(dobDay);

					}
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));


					if (driver.findElement(YearFromDrpDwn).getAttribute("value").equals("")) {
						//new Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);
						new Select(driver.findElement(YearFromDrpDwn)).selectByValue(dobYear);

					}
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					
					if (home_Phone_fld.getAttribute("value").equals("")){
						home_Phone_fld.sendKeys(testData.get("homeNumber"));

					}
					
					
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					
					clickElementJS(updateVal);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					}catch(Exception e){
						System.out.println("SSN Cannot Be Entered");
					}


				}	
				
				
				if (verifyObjectDisplayed(headerContractProcessOwner)) {

					
				//check the secondary owner count with delete buttons 
					
					if (verifyObjectDisplayed(deleteOwner)) {
						secOwnlist = driver.findElements(deleteOwner);
						
						noOfSecOwn= secOwnlist.size();
						
					}
					
		

					
					//check primary owner
					
					//if (verifyElementDisplayed(pName)){
					
					//System.out.println("Primary Owner Exists");
					
					//delete all secondary owner
					for (int i=0;i<noOfSecOwn;i++){
						
						clickElementJSWithWait(deleteonesecOwner);
						waitForSometime(tcConfig.getConfig().get("MedWait"));	
					
						if (ExpectedConditions.alertIsPresent() != null) {
						try {
			
							driver.switchTo().alert().accept();
			
							} catch (Exception e) {
								System.out.println(e);
							}
						}
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						
					//}

					
					}			
					
					

				if (verifyElementDisplayed(nextVal)){			
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(nextVal);
				}
				}
				
				

			} else {
				System.out.println("No Primary owner edit button");
			}


			waitUntilElementVisibleIE(driver, saveClick, "ExplicitWaitLongestTime");

			
//			if (verifyObjectDisplayed(PaymentEdit)) {
//
//				driver.findElement(PaymentEdit).click();		
//				waitForSometime(tcConfig.getConfig().get("MedWait"));
//
//				if (ExpectedConditions.alertIsPresent() != null) {
//					try {
//
//						driver.switchTo().alert().accept();
//
//					} catch (Exception e) {
//						System.out.println("no alert is displayed");
//					}
//				}
//				
//				waitUntilElementVisibleIE(driver, ccSelect, "ExplicitWaitLongestTime");				
//				
//				clickElementJS(driver.findElement(ccSelect));
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//				
//				if (ExpectedConditions.alertIsPresent() != null) {
//					try {
//
//						driver.switchTo().alert().accept();
//
//					} catch (Exception e) {
//						System.out.println("no Alert is displayed");
//					}
//				
//				}
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//
//				ccName.clear();				
//				ccName.click();
//				ccName.sendKeys(testData.get("CCName"));
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//
//				driver.findElement(ccType).click();
//				String cardType = testData.get("CCType");
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//				driver.findElement(By.xpath("//select/option[contains(.,'"+cardType+"')]")).click();
//
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//
//				ccNumber.clear();
//				ccNumber.click();				
//				ccNumber.sendKeys(testData.get("CCNumber"));
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//				
//				driver.findElement(ccMonth).click();
//				String expMnth = testData.get("CCExpMnth");
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//				driver.findElement(By.xpath("//select/option[contains(.,'"+expMnth+"')]")).click();
//
//				
//				driver.findElement(ccYear).click();
//				String expYr = testData.get("CCExpYear");
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//				driver.findElement(By.xpath("//select/option[contains(.,'"+expYr+"')]")).click();
//
//
//				
//
//				//new Select(driver.findElement(ccYear)).selectByValue(testData.get("CCExpYear"));
//				waitForSometime(tcConfig.getConfig().get("LowWait"));
//
//				
//				waitUntilElementVisibleIE(driver, nextVal, "ExplicitWaitLongestTime");
//				clickElementJS(nextVal);
//				
//				
//				if (ExpectedConditions.alertIsPresent() != null) {
//					try {
//
//						driver.switchTo().alert().accept();
//
//					} catch (Exception e) {
//						System.out.println("no Alert is displayed");
//					}
//				
//				}
//				
//				waitForSometime(tcConfig.getConfig().get("MedWait"));
//
//				waitUntilElementVisibleIE(driver, calculate, "ExplicitWaitLongestTime");
//				
//				waitForSometime(tcConfig.getConfig().get("MedWait"));
//
//
//				if (verifyElementDisplayed(calculate)) {
//					tcConfig.updateTestReporter("SalePointCalculationPage", "sp_New_ContractFlow_Automation", Status.PASS,
//							"Money Screen present");
//
//					clickElementJS(calculate);
//					System.out.println("clicked calculate button");
//					waitForSometime(tcConfig.getConfig().get("LowWait"));
//					//driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
//					waitForSometime(tcConfig.getConfig().get("LowWait"));
//					waitUntilElementVisibleIE(driver, nextVal, "ExplicitWaitLongestTime");
//					nextVal.click();
//					//clickElementJS(nextVal);
//					waitForSometime(tcConfig.getConfig().get("MedWait"));
//
//					System.out.println("clicked Next button after calculate");
//				}
//					else
//					{ System.out.println("calculate element not diaplyed");
//				} 
//				
//				if (ExpectedConditions.alertIsPresent() != null) {
//					try {
//
//						driver.switchTo().alert().accept();
//
//					} catch (Exception e) {
//						System.out.println("no Alert is displayed");
//					}
//				
//				}
//				
//
//			}			
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleIE(driver, savecontract, "ExplicitWaitLongestTime");

						if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("ContractSavePage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("ContractSavePage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Contract Save Page Navigation Error");

			}

			// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, printClick, "ExplicitWaitLongestTime");

			waitForSometime(tcConfig.getConfig().get("MedWait"));


			
			
			waitUntilElementVisibleIE(driver, print, "ExplicitWaitLongestTime");

			
			if (verifyElementDisplayed(print)) {
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				clickElementJSWithWait(print);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			waitUntilElementVisibleIE(driver, docSign, "ExplicitWaitLongestTime");

			if (verifyObjectDisplayed(docSign)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				new Select(driver.findElement(docSign)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

							
				
			}
			
			if (verifyElementDisplayed(print)) {
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				clickElementJSWithWait(print);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
					
			
			
			waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText().trim();

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Member number could not be retrieved");
			}

		} else {

			tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
					"Automation Pop Up is not available");
			
			
		}
					
				
	}

	
	
	@FindBy(xpath = "//img[@src='/webapp/ccis/images/log_off.gif']")
	WebElement LogoutBtnSP;

	public void LogoutSP() throws Exception {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", LogoutBtnSP);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(buttonLogIn)) {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL, "Log Out Failed");
			}
			
		} catch (Exception e) {
			tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL,
					"Log Out Failed" + e.getMessage());
		}
		// driver.close();
		// driver.quit();
	}
	
	
	
	
	
	
	
	
@FindBy(xpath="//input[@id='fldUsername']") WebElement textUsernameWSA;
	
	
	 * enter user name
	 
	protected void setUserNameWSA(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, textUsernameWSA, "ExplicitWaitLongestTime");
        driver.manage().deleteAllCookies();
		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, textUsernameWSA, "ExplicitWaitLongestTime");
		textUsernameWSA.sendKeys(strUserName);		
		tcConfig.updateTestReporter("WSALoginPage","setUserName",Status.PASS, "Username is entered for WSA Login");
	}
	
	
	@FindBy(xpath="//input[@id='fldPassword']") WebElement textPasswordWSA;

	*//**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 *//*
	protected void setPasswordWSA(String strPassword) throws Exception {
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString,"UTF-8");
		
		textPasswordWSA.sendKeys(dString);
		tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.PASS, "Password is entered for WSA Login");
	}

	@FindBy(xpath="//button[@name='cn-submit']") WebElement buttonLogInWSA;
	//@FindBy(xpath="//label[text()='Manage Inventory Availability']") WebElement linkMangInv;
	
	
	 * Launch WSA application
	 
	
	public void launchAppWSA(String strBrowser) {
		
		this.driver=checkAndInitBrowser(strBrowser);
		String url=testData.get("URL");
		driver.navigate().to(url);
		driver.manage().window().maximize();
	}
	
	
	@FindBy(xpath="//img[@title='Change Sales Site']") WebElement changeSiteiconWSA;
	
	 * Login WSA application
	 
	public void loginToWSA(String strBrowser) throws Exception {

		
		//testData.put("testTry", "Monideep");
		launchAppWSA(strBrowser);
		String userid = testData.get("WSA_UserID");
		String password = testData.get("WSA_Password");
		setUserNameWSA(userid);
		setPasswordWSA(password);
		buttonLogInWSA.click();
		waitUntilElementVisibleBy(driver, changeSiteiconWSA, "ExplicitWaitLongestTime");
		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, changeSiteiconWSA, "ExplicitWaitLongestTime");
		
		if(verifyElementDisplayed(changeSiteiconWSA)) {
			tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.PASS, "Log in button clicked");
		}else{
			launchAppWSA(strBrowser);
			
			setUserNameWSA(userid);
			setPasswordWSA(password);
			buttonLogIn.click();			
			waitUntilElementVisibleBy(driver, changeSiteicon, "ExplicitWaitLongestTime");
			driver.navigate().refresh();
			waitUntilElementVisibleBy(driver, changeSiteicon, "ExplicitWaitLongestTime");


			if(verifyElementDisplayed(changeSiteicon)) {
				tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.PASS, "Log in button clicked");
			}else{
				tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.FAIL, "Log in button clicked : But application not opened");
			}
		}
	}
	
	
	
	@FindBy(xpath="//img[@title='Change Sales Site']") WebElement changeSiteicon;
	@FindBy(xpath="//button[@name='cs-submit']") WebElement confirmOKBtn;
	@FindBy(xpath="//button[@id='submit-btn']") WebElement saveBtn;
	@FindBy(xpath="//p[text()='Your site was updated successfully.']") WebElement txtSuccessMsg;	
	
	public final By radioBtnWVRServiceEntity=By.xpath("(//input[@name='selectedServiceEntity'])[1]"); 
	public final By radioBtnWBWServiceEntity=By.xpath("(//input[@name='selectedServiceEntity'])[2]"); 
	public final By radioBtnWVRAPServiceEntity=By.xpath("(//input[@name='selectedServiceEntity'])[3]"); 
	public final By siteDropDown=By.xpath("//select[@id='selectedSite']");
	public final By closePopupIcon=By.xpath("(//img[@alt='Close this window'])[2]");
	

	
	function name: selectEntity()
	Purpose : Set Service entity from homepage
		
	
	public void selectEntityWSA() throws Exception {
		if(verifyElementDisplayed(changeSiteicon)) {
			changeSiteicon.click();
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA", Status.PASS, "Sales Site Icon Clicked");
		}else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA", Status.FAIL, "Sales Site Icon Not Displayed");
		}
	
		if(verifyElementDisplayed(confirmOKBtn)) {
			confirmOKBtn.click();
		}
		waitUntilElementVisibleBy(driver, radioBtnWVRServiceEntity, "ExplicitWaitLongestTime");
		driver.findElement(radioBtnWVRServiceEntity).click();
		if(testData.get("ServiceEntity").equalsIgnoreCase("WVR")) {
			driver.findElement(radioBtnWVRServiceEntity).click();
		}else if(testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			driver.findElement(radioBtnWBWServiceEntity).click();
		}else if(testData.get("ServiceEntity").equalsIgnoreCase("WVRAP")) {
			driver.findElement(radioBtnWVRAPServiceEntity).click();
		}
		new Select(driver.findElement(siteDropDown)).selectByVisibleText("WYNDHAM DAYTONA (00033)");
		saveBtn.click();
		waitUntilElementVisibleBy(driver, closePopupIcon, "ExplicitWaitLongestTime");
		if(verifyElementDisplayed(txtSuccessMsg)) {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA()", Status.PASS, "Site changed to success message is diplayed properly");
			driver.findElement(closePopupIcon).click();
		}else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA()", Status.FAIL, "Site changed to success message is Not Displayed");
		}
	}
	
	
	
	function name: pitchTypeSelection()
	Purpose : Navigate to pitchtype --> subPitchType e.g New Propsal ---> UDI etc. 
		
	
	
	public void pitchTypeSelectionWSA() throws Exception {
		
		waitUntilElementVisibleBy(driver, driver.findElement(By.xpath("//a[text()='"+testData.get("PitchType")+"']")), "ExplicitWaitLongestTime");

		final WebElement pitchType=driver.findElement(By.xpath("//a[text()='"+testData.get("PitchType")+"']"));
		pitchType.click();
		//Actions action=new Actions(driver);
		//action.moveToElement(pitchType).click().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		final WebElement subPitchType=driver.findElement(By.xpath(" //a[text()='"+testData.get("PitchType")+"']/following-sibling::ul/descendant::a[contains(text(),'"+testData.get("SubPitchType")+"')]"));
		//subPitchType.click();
		if(verifyElementDisplayed(subPitchType)) {
			subPitchType.click();
			tcConfig.updateTestReporter("WSAHomePage", "pitchTypeSelectionWSA()", Status.PASS, testData.get("SubPitchType")+" selected from "+testData.get("PitchType")+" menu");
		}else {
			tcConfig.updateTestReporter("WSAHomePage", "pitchTypeSelectionWSA()", Status.FAIL, testData.get("SubPitchType")+" NOT selected from "+testData.get("PitchType")+" menu");
		}
	}
	

	@FindBy(name="tourNumber") WebElement txtTourNumber;
	@FindBy(xpath="//button[@name='cn-submit']") WebElement submitBtn;
	@FindBy(xpath="//button[@name='btn-continue']") WebElement continueBtn;
	@FindBy(xpath="//button[@name='btn-existingContinue']") WebElement existingContinueBtn;
	@FindBy(xpath="//button[@name='btn-newOwnerContinue']") WebElement newOwnerContinueBtn;
	
	@FindBy(xpath="//input[@name='memberNumber']") WebElement txtMemberNumber;
	@FindBy(xpath="//h1[contains(text(),'Co-owners Selection')]") WebElement coOwnerSelectionPageHeader;
	@FindBy(xpath="//h1[contains(text(),'Add New Owners')]") WebElement addOwnersHeader;
	
	
	public final By customerNametxt=By.xpath("(//form[@id='resultsForm']//p)[1]");
	public final By selectCustRadio=By.xpath("(//form[@id='resultsForm']//input[@type='radio'])[1]");
	
	//public String selectedCustName;
	
	function name: tourLookUpWSA()
	Purpose : Search by tour number and navigate to pitch creation page
		
	
	public void tourLookUpWSA() throws Exception {
		waitUntilElementVisibleBy(driver, txtTourNumber, "ExplicitWaitLongestTime");
		if(testData.get("ServiceEntity").equalsIgnoreCase("WVR") && testData.get("SubPitchType").equalsIgnoreCase("UDI")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()" , Status.PASS, "Tour Number entered");
			}else {
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()" , Status.FAIL, "Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisibleBy(driver, continueBtn, "ExplicitWaitLongestTime");
			
			// select the 
			
			String MemberName = testData.get("selectUser");
					
			//p[contains(.,'"+MemberName+"')]		
			
			//selectedCustName=driver.findElement(customerNametxt).getText();
			//driver.findElement(selectCustRadio).click();
			
			selectedCustName= MemberName;
			List<WebElement> listogelem = driver.findElements(By.xpath("//p[contains(.,'"+MemberName+"')]/../../..//input[@type='radio']"));
			listogelem.get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			continueBtn.click();
							
			
			waitUntilElementVisibleBy(driver, txtMemberNumber, "ExplicitWaitLongestTime");
			continueBtn.click();
			waitUntilElementVisibleBy(driver, coOwnerSelectionPageHeader, "ExplicitWaitLongestTime");
			existingContinueBtn.click();
			waitUntilElementVisibleBy(driver, addOwnersHeader, "ExplicitWaitLongestTime");
			newOwnerContinueBtn.click();
		}else if(testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()" , Status.PASS, "Tour Number entered");
			}else {
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()" , Status.FAIL, "Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisibleBy(driver, continueBtn, "ExplicitWaitLongestTime");
			selectedCustName=driver.findElement(customerNametxt).getText();
			driver.findElement(selectCustRadio).click();
			continueBtn.click();
			waitUntilElementVisibleBy(driver, addOwnersHeader, "ExplicitWaitLongestTime");
			newOwnerContinueBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
		}else if(testData.get("ServiceEntity").equalsIgnoreCase("WVR") && testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()" , Status.PASS, "Tour Number entered");
			}else {
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()" , Status.FAIL, "Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisibleBy(driver, continueBtn, "ExplicitWaitLongestTime");
			driver.findElement(selectCustRadio).click();
			continueBtn.click();
			waitUntilElementVisibleBy(driver, addOwnersHeader, "ExplicitWaitLongestTime");
			newOwnerContinueBtn.click();
		}
	}
	
	
	
	
	
	@FindBy(xpath="//a[text()='Commissions']") WebElement commisionsLink;
	@FindBy(xpath="//input[@id='salesAssociate']") WebElement salesAssociatetxt;
	@FindBy(xpath="//input[@id='sellingRep']") WebElement salesRepresentativetxt;
	@FindBy(xpath="//button[@name='commission-done']") WebElement comissionDoneBtn;
	@FindBy(xpath="//a[text()='Select Inventory']") WebElement linkSelectInv;
	@FindBy(xpath="//input[@id='newPoints1']") WebElement newPointstxt;
	@FindBy(xpath="//button[@name='inventory-done']") WebElement inventoryDoneBtn;
	@FindBy(xpath="(//a[contains(text(),'Click to adjust')])[1]") WebElement adjustLink;
	@FindBy(xpath="//button[@id='band-submit1']") WebElement paymentDoneBtn;
	@FindBy(xpath="(//button[@id='band-submit'])[6]") WebElement paymentSubmitBtn;
	
	@FindBy(xpath="//div[@id='requiredDownPayment1']") WebElement txtRequiredAmt;
	@FindBy(xpath="//div[@id='requiredToQualify1']") WebElement txtRequiredQlfyAmt;
	
	@FindBy(xpath="//input[@id='paymentTypeForm_offer1_downPaymentList_0__dpAmount']") WebElement txtAmountLine1;
								
	@FindBy(xpath="//input[@id='paymentTypeForm_offer1_downPaymentList_1__dpAmount']") WebElement txtAmountLine2;
	@FindBy(xpath="//button[@name='final-offer']") WebElement finalBtn;
	@FindBy(xpath="//button[@name='finalizeSummary']") WebElement finalBtnWBW;
	
	
	public final By titleDrpDwn=By.xpath("//select[@name='titleOption']");
	public final By depositTypeDropDown=By.xpath("//select[@id='paymentTypeForm_offer1_downPaymentList_1__paymentTypeName']");
	public final By selectFlagRadioBtn=By.xpath("(//input[@name='flag'])[1]");
	public final By selectFlagRadioBtnWBW=By.xpath("(//input[@name='flagForWorksheet1'])[1]");
	public final By CustNameTxt=By.xpath("//h2[contains(text(),'Name:')]");
	public final By pitchNumberHeader=By.xpath("//div[@id='page-body-container']//h1");
	
	//public static String createdPitchID;
	//public static String customerLastName;
	
	function name: createNewPitchWSA()
	Purpose : Fill up all the details and create a new pitch for various types of entity 
		
	
	public void createNewPitchWSA() throws Exception {
		double amountone,amounttwo;
		
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR") && testData.get("SubPitchType").equalsIgnoreCase("UDI")) {
			System.out.println("Entered create New Pitc");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, commisionsLink, "ExplicitWaitLongestTime");
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.click();
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.click();
			salesAssociatetxt.sendKeys(Keys.TAB);
			salesAssociatetxt.sendKeys(Keys.ENTER);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Sales Associate number entered");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			//mouseoverAndClick(comissionDoneBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyElementDisplayed(linkSelectInv)){
				linkSelectInv.click();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Select inventory link is displayed");
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "Select inventory link is NOT displayed");
			}
			
			newPointstxt.clear();
			newPointstxt.sendKeys(testData.get("Points"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Points entered");
			inventoryDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			if(verifyObjectDisplayed(titleDrpDwn)){
//				//new Select(driver.findElement(titleDrpDwn)).selectByVisibleText("Single Man");
//				new Select(driver.findElement(titleDrpDwn)).selectByIndex(1);
//
//				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Title Displayed");
//			}else {
//				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "Title NOT displayed");
//			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt=txtRequiredAmt.getText().trim();
			double dbl=Double.parseDouble(TotalAmt);
			amountone=dbl%100;
			amounttwo=(dbl-dbl%100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f",amounttwo);
			
			if(verifyElementDisplayed(txtAmountLine1)){
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Adjust Link clicked");
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "Adjust link not displayed");
			}
			
			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, strAmntOne+ " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, strAmntTwo+ " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Payment done button clicked");
			
			//03/08/2018
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Sales Associate number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			salesAssociatetxt.sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			//comissionDoneBtn.click();
			mouseoverAndClick(comissionDoneBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			//end 
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(titleDrpDwn)){
				//new Select(driver.findElement(titleDrpDwn)).selectByVisibleText("Single Man");
				new Select(driver.findElement(titleDrpDwn)).selectByIndex(1);

				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Title Displayed");
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "Title NOT displayed");
			}
			
			
			
			
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitUntilElementVisibleBy(driver, pitchNumberHeader, "ExplicitWaitLongestTime");
			if(verifyObjectDisplayed(pitchNumberHeader)){
				String pageheaderdetails=driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails=pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails=newheaderdetails.replace("-1 Selected", "");
				createdPitchID=headerdetails.trim();
				String CustName=driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr=CustName.split(" ");
				customerLastName=arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Pitch # "+headerdetails.trim()+" created" );
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "No pitch number is generated" );
			}
			
		}else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			waitUntilElementVisibleBy(driver, commisionsLink, "ExplicitWaitLongestTime");
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, salesRepresentativetxt, "ExplicitWaitLongestTime");
			salesRepresentativetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Sales Representative number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt=txtRequiredQlfyAmt.getText().trim();
			double dbl=Double.parseDouble(TotalAmt);
			amountone=dbl%100;
			amounttwo=(dbl-dbl%100);
			
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f",amounttwo);
			
			
			if(verifyElementDisplayed(txtAmountLine1)){
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Adjust Link clicked");
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "Adjust link not displayed");
			}
			
			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, strAmntOne+ " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, strAmntTwo+ " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSubmitBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtnWBW).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtnWBW.click();
			waitUntilElementVisibleBy(driver, pitchNumberHeader, "ExplicitWaitLongestTime");
			if(verifyObjectDisplayed(pitchNumberHeader)){
				String pageheaderdetails=driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails=pageheaderdetails.replace("Final Summary for Reference # ", "");
				createdPitchID=newheaderdetails.trim();
				String CustName=driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr=CustName.split(" ");
				customerLastName=arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Pitch # "+newheaderdetails.trim()+" created" );
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "No pitch number is generated" );
			}
		}else if(testData.get("ServiceEntity").equalsIgnoreCase("WVR") && testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			waitUntilElementVisibleBy(driver, commisionsLink, "ExplicitWaitLongestTime");
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Sales Associate number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt=txtRequiredAmt.getText().trim();
			double dbl=Double.parseDouble(TotalAmt);
			amountone=dbl%100;
			amounttwo=(dbl-dbl%100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f",amounttwo);
			
			if(verifyElementDisplayed(txtAmountLine1)){
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Adjust Link clicked");
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "Adjust link not displayed");
			}
			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, strAmntOne+ " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, strAmntTwo+ " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitUntilElementVisibleBy(driver, pitchNumberHeader, 240);
			if(verifyObjectDisplayed(pitchNumberHeader)){
				String pageheaderdetails=driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails=pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails=newheaderdetails.replace("-1 Selected", "");
				createdPitchID=headerdetails.trim();
				String CustName=driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr=CustName.split(" ");
				customerLastName=arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Pitch # "+headerdetails.trim()+" created" );
			}else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "No pitch number is generated" );
			}			
		}
	}
	
	
	@FindBy(xpath="//a[text()='Logout']") WebElement LogoutBtn;
	@FindBy(xpath="//div[@class='msg inform']") WebElement logoutMsg;
	@FindBy(xpath="//img[@title='Home']") WebElement homeIcon;
	
	
	public void WSALogoutApp() throws Exception {
		try{
		waitUntilElementVisibleBy(driver, LogoutBtn, "ExplicitWaitLongestTime");	
		LogoutBtn.click();
		waitUntilElementVisibleBy(driver, textUsername, "ExplicitWaitLongestTime");
		tcConfig.updateTestReporter("WSALoginPage","logOut",Status.PASS, "Log Out Succesful");
		}catch(Exception e){
			tcConfig.updateTestReporter("WSALoginPage","logOut",Status.FAIL, "Log Out Failed"+e.getMessage());
		}
	}
	
	@FindBy(xpath = "//a[text()='Retrieve']")
	WebElement retriveLink;
	@FindBy(xpath = "//input[@name='startDate']")
	WebElement txtStartDate;
	@FindBy(xpath = "//input[@name='endDate']")
	WebElement txtEndDate;
	@FindBy(xpath = "//input[@name='siteNumber']")
	WebElement txtSiteNumber;
	@FindBy(xpath = "//button[@name='lu-submit']")
	WebElement submitBtnRtrv;
	@FindBy(name = "tourNumber")
	WebElement txtTourNumberRetrv;
	@FindBy(xpath="//input[@name='controlNumber']")
	WebElement txtReferenceNumber;

	public final By lookUpDrpDwn = By.xpath("//select[@id='pitch-lookup-type']");
	By NoOfResDrpdwn=By.xpath("//select[contains(@title,'Number of results to display per page')]");
	public void navigatetoretrievePitch() throws Exception {
		waitUntilElementVisibleBy(driver, retriveLink, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(retriveLink)) {
			retriveLink.click();
			tcConfig.updateTestReporter("RetrieveTourPage", "navigatetoretrievePitch()", Status.PASS,
					"Retrieve link clicked successfully");
		} else {
			tcConfig.updateTestReporter("RetrieveTourPage", "navigatetoretrievePitch()", Status.FAIL,
					"Retrieve link not displayed");
		}
		waitUntilElementVisibleBy(driver, lookUpDrpDwn, "ExplicitWaitLongestTime");
	}
	

	
	public void retrievePitch() throws Exception {
		// LocalDate today=LocalDate.now();
		String CheckinDate = "", strSiteNumber = null;

		CheckinDate = addDateInSpecificFormat("MM/dd/yyyy", 0);
		waitUntilElementVisibleBy(driver, lookUpDrpDwn,"ExplicitWaitLongestTime");

		new Select(driver.findElement(lookUpDrpDwn)).selectByVisibleText(testData.get("LookUpBy"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (testData.get("LookUpBy").equalsIgnoreCase("Date Range")) {
			if (verifyElementDisplayed(txtStartDate)) {
				txtStartDate.click();
				txtStartDate.clear();
				txtStartDate.sendKeys(CheckinDate);
				tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.PASS,
						"Start date entered as : " + CheckinDate);
			} else {
				tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.FAIL,
						"Start date txtbox is not displayed");
			}
			txtEndDate.click();
			txtEndDate.clear();
			txtEndDate.sendKeys(CheckinDate);
			// if(testData.get("SiteName").contains("WYNDHAM BONNET CREEK
			// (00064)")) {
			// strSiteNumber="00064";
			// }
			strSiteNumber = testData.get("SiteName").substring(testData.get("SiteName").indexOf("(") + 1,
					testData.get("SiteName").indexOf(")"));
			txtSiteNumber.click();
			txtSiteNumber.clear();
			txtSiteNumber.sendKeys(strSiteNumber);
			txtSiteNumber.sendKeys(Keys.TAB);
			submitBtnRtrv.click();
			waitUntilElementVisibleBy(driver, NoOfResDrpdwn,"ExplicitWaitLongestTime");
			new Select(driver.findElement(NoOfResDrpdwn)).selectByValue("50");
			
			waitUntilElementVisibleBy(driver, By.xpath("//td[text()='" + WsaSalepointFlow.createdPitchID + "']"),
					25);
			if (verifyObjectDisplayed(By.xpath("//td[text()='" + WsaSalepointFlow.createdPitchID + "']"))) {
				tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.PASS,
						"Pitch retrieve successfully");
			} else {
				tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.FAIL,
						"Unable to retrieve the pitch");
			}
		} else if (testData.get("LookUpBy").equalsIgnoreCase("Tour Number")) {
			if (verifyElementDisplayed(txtTourNumberRetrv)) {
				txtTourNumberRetrv.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter("TourLookUpPage", "retrievePitch()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "retrievePitch()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtnRtrv.click();
			waitUntilElementVisibleBy(driver, By.xpath("//td[text()='" + WsaSalepointFlow.createdPitchID + "']"),
					"ExplicitWaitLongestTime");
			if (verifyObjectDisplayed(By.xpath("//td[text()='" + WsaSalepointFlow.createdPitchID + "']"))) {
				tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.PASS,
						"Pitch #" + WsaSalepointFlow.createdPitchID + "retrieved successfully");
			} else {
				tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.FAIL,
						"Unable to retrieve the pitch #" + WsaSalepointFlow.createdPitchID);
			}
		}else if (testData.get("LookUpBy").equalsIgnoreCase("Reference Number")) {
			if (verifyElementDisplayed(txtReferenceNumber)) {
				txtReferenceNumber.sendKeys(WsaSalepointFlow.createdPitchID.trim());
				tcConfig.updateTestReporter("TourLookUpPage", "retrievePitch()", Status.PASS, "Reference number entered");

				submitBtnRtrv.click();
				waitUntilElementVisibleBy(driver, By.xpath("//td[text()='" + WsaSalepointFlow.createdPitchID + "']"),
						"ExplicitWaitLongestTime");
				if (verifyObjectDisplayed(By.xpath("//td[text()='" + WsaSalepointFlow.createdPitchID + "']"))) {
					tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.PASS,
							"Pitch #" + WsaSalepointFlow.createdPitchID + "retrieved successfully");
				} else {
					tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.FAIL,
							"Unable to retrieve the pitch #" + WsaSalepointFlow.createdPitchID);
				}		
				
			}else {
				tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()", Status.FAIL,
						"Unable to retrieve the pitch #" + WsaSalepointFlow.createdPitchID);
			}
		}

	}
	
}
*/