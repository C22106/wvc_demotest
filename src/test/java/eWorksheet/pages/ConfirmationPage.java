package eWorksheet.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class ConfirmationPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(ConfirmationPage.class);

	public ConfirmationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String ObtainedBand;
	public static String ObtainedBand1;
	public static String vccApprovedAmount;
	public static String rewardsApprovedAmount;
	
	protected By vccAmount = By.xpath("//div[@data-type='vcc']//div[@class='amount-approved']");
	protected By rewardsAmount = By.xpath("//div[@data-type='barclays']//div[@class='amount-approved']");
	
	protected By nextBtn = By.xpath("//button[@class='btn btn--inv next_btn float-right']");
	protected By cardColoumn = By.xpath("//div[@class='cell xsmall-9 medium-12 results-container vcc-container']");
//	protected By vccCheck = By.xpath("//div[@data-type='vcc']//span[@class='approve-check']");
	protected By equifaxCheck = By.xpath("//div[@data-type='equifax']//span[@class='approve-check']");
	protected By equifaxCheckCusName = By.xpath("//div[@data-type='equifax']//div[text()='Submission Successful']/../span");
	
	protected By barclaysCheck = By.xpath("//div[@data-type='barclays']//span[@class='approve-check']");
	protected By barclaysCheckCusName = By.xpath("//div[@data-type='barclays']//div[text()='Submission Successful']/../span");
	
	protected By vccCheck = By.xpath("//div[@data-type='vcc']//span[@class='approve-check']");
	protected By vccCheckCusName = By.xpath("//div[@data-type='vcc']//div[text()='Submission Successful']/../span");
	
	protected By triDot = By.xpath("//i[@class='i-tri-dot']");
	protected By creditBand = By.xpath("//a[contains(.,'Customer Credit Band')]");
	protected By bandInfo = By.xpath("//div[@class='band-info']//div[@class='name']");
	protected By closeButton = By.xpath("//button[@class='btn btn-close-delete js-modal-close']");
	protected By barclaysAgree = By.xpath("//div[@class='modal']//input[@name='agree']");
	protected By barclaysFin = By
			.xpath("//div[@class='modal']//button[@class='btn button-right btn-barclay-cma-finish']");
	

	protected By thankYouText = By.xpath("//p[@class='completed-copy-thankyou']");
	protected By vccDecline = By.xpath("//div[contains(@class,'vcc-container')]//p[@class='option-copy declined']");
	protected By vccpending = By.xpath("//div//p[@class='option-copy declined']");
	protected By barclaysDecline = By.xpath("//div[contains(@class,'wrc-container')]//p[@class='option-copy declined']");

	public void approvalConfirmation(String cardType) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (cardType.equalsIgnoreCase("Finance")) {
			if (verifyObjectDisplayed(equifaxCheck)) {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.PASS,
						"Ownership card has been approved");

				clickElementBy(triDot);

				if (verifyObjectDisplayed(creditBand)) {
					clickElementBy(creditBand);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(bandInfo)) {
						ObtainedBand = driver.findElement(bandInfo).getText();
						tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing ", Status.PASS,
								driver.findElement(bandInfo).getText());

						clickElementBy(closeButton);

					} else {

						tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
								"Band Info is not displayed");

					}

				} else {
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
							"Credit Band option not present");
				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationEquifax", Status.FAIL,
						"Getting some issue in ownership card approval");
			}
		}else if(cardType.equalsIgnoreCase("VacationClub")){
			if (verifyObjectDisplayed(vccCheck)) {

				vccApprovedAmount = driver.findElement(vccAmount).getText();
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationVCC", Status.PASS,
						"VCC card is approved, with amount: " + vccApprovedAmount);

						} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationVCC", Status.FAIL,
						"VCC card approval issue");
			}
		}else if(cardType.equalsIgnoreCase("Rewards")){
			if (verifyObjectDisplayed(barclaysCheck)) {

				rewardsApprovedAmount = driver.findElement(rewardsAmount).getText().trim();
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForRewards", Status.PASS,
						"Reward card is approved and Approved amount is: " + rewardsApprovedAmount);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {

				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForRewards", Status.FAIL,
						"Reward card is not approved");

			}
		}

		

	}

	
	public void confirmationForVacationClub() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, "ExplicitWaitLongestTime");

		if (verifyObjectDisplayed(vccCheck)) {

			vccApprovedAmount = driver.findElement(vccAmount).getText();
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationVCC", Status.PASS,
					"VCC card is approved, with amount: " + vccApprovedAmount);


		} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationVCC", Status.FAIL,
					"VCC card approval issue");
		}

	}

	public void confirmationForFinance() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, "ExplicitWaitLongestTime");

		String custName = driver.findElement(equifaxCheckCusName).getText();
		clickElementBy(triDot);

		if (verifyObjectDisplayed(creditBand)) {
			clickElementBy(creditBand);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(bandInfo)) {
				ObtainedBand = driver
						.findElement(By.xpath("//div[@class='band-info']//div[contains(text(),'" + custName + "')]"))
						.getText();
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing ", Status.PASS,
						"Obtained band is : " + ObtainedBand);

				clickElementBy(closeButton);

			} else {

				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
						"Band Info is not displayed");

			}

		} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
					"Credit Band option not present");
		}

	}
	
	public void confirmationForFinanceMultipleCus() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, "ExplicitWaitLongestTime");
		int totalcust= Integer.parseInt(testData.get("totalCust"));
//		for(int i=0;i<=totalcust;i++)
//		{
			List<WebElement> check= driver.findElements(equifaxCheck);
		
//		if (verifyObjectDisplayed(equifaxCheck)) 
		if (check.get(0).isDisplayed()) 
		{
			for(int i=0;i<=totalcust-1;i++)
			{
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.PASS,
					"Ownership card has been approved");
			String custName = driver.findElements(equifaxCheckCusName).get(i).getText();
			
			
			clickElementBy(triDot);

			if (verifyObjectDisplayed(creditBand)) {
				clickElementBy(creditBand);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

//				if (verifyObjectDisplayed(bandInfo))
				List <WebElement> bandinfolst=driver.findElements(bandInfo);
				if (bandinfolst.get(i).isDisplayed())
				{
					ObtainedBand = driver.findElement(By.xpath("//div[@class='band-info']//div[contains(text(),'"+custName+"')]")).getText();
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing ", Status.PASS,
							"Obtained band is : "+ObtainedBand);

					clickElementBy(closeButton);
					System.out.println("Button clicked for "+ i+" time");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				} else {

					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
							"Band Info is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
						"Credit Band option not present");
			}

			}
			} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEquifax", Status.FAIL,
					"Getting some issue in ownership card approval");
		}
		}
	

	
	
	
	
	
	public void confirmationForRewardsMultipleCus() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, "ExplicitWaitLongestTime");
		int totalcust= Integer.parseInt(testData.get("totalCust"));
		
		
		WebElement msg=driver.findElement(By.xpath("//div[contains(@class,'wrc')]//p[@class='option-copy declined']"));
		
		if(msg.getText().contains("Your application is being reviewed")){
			
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.PASS,
					"Ownership card has been declined");
			
		}
		else{
		
		
//		for(int i=0;i<=totalcust;i++)
//		{
			List<WebElement> check= driver.findElements(barclaysCheck);
		
//		if (verifyObjectDisplayed(equifaxCheck)) 
		if (check.get(0).isDisplayed()) 
		{
			for(int i=0;i<=totalcust-1;i++)
			{
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.PASS,
					"Ownership card has been approved");
			String custName = driver.findElements(barclaysCheckCusName).get(i).getText();
			
			
			clickElementBy(triDot);

			if (verifyObjectDisplayed(creditBand)) {
				clickElementBy(creditBand);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

//				if (verifyObjectDisplayed(bandInfo))
				List <WebElement> bandinfolst=driver.findElements(bandInfo);
				if (bandinfolst.get(i).isDisplayed())
				{
					ObtainedBand = driver.findElement(By.xpath("//div[@class='band-info']//div[contains(text(),'"+custName+"')]")).getText();
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing ", Status.PASS,
							"Obtained band is : "+ObtainedBand);

					clickElementBy(closeButton);
					System.out.println("Button clicked for "+ i+" time");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				} else {

					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
							"Band Info is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
						"Credit Band option not present");
			}

			}
			} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEquifax", Status.FAIL,
					"Getting some issue in ownership card approval");
		}
		}
		}
	

	public void confirmationForVCCMultipleCus() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, "ExplicitWaitLongestTime");
		int totalcust= Integer.parseInt(testData.get("totalCust"));
//		for(int i=0;i<=totalcust;i++)
		WebElement msg=driver.findElement(By.xpath("//div[contains(@class,'vcc')]//p[@class='option-copy declined']"));
		if(msg.getText().contains("This option is not available")){
			
			tcConfig.updateTestReporter("ConfirmationPage", "Confirmation for VCC", Status.FAIL,
					"The Request is declined");
			
			
		}
		else{
//		{
			List<WebElement> check= driver.findElements(vccCheck);
		
//		if (verifyObjectDisplayed(equifaxCheck)) 
		if (check.get(0).isDisplayed()) 
		{
			for(int i=0;i<=totalcust-1;i++)
			{
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.PASS,
					"Ownership card has been approved");
			String custName = driver.findElements(vccCheckCusName).get(i).getText();
			
			
			clickElementBy(triDot);

			if (verifyObjectDisplayed(creditBand)) {
				clickElementBy(creditBand);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

//				if (verifyObjectDisplayed(bandInfo))
				List <WebElement> bandinfolst=driver.findElements(bandInfo);
				if (bandinfolst.get(i).isDisplayed())
				{
					ObtainedBand = driver.findElement(By.xpath("//div[@class='band-info']//div[contains(text(),'"+custName+"')]")).getText();
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing ", Status.PASS,
							"Obtained band is : "+ObtainedBand);

					clickElementBy(closeButton);
					System.out.println("Button clicked for "+ i+" time");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				} else {

					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
							"Band Info is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
						"Credit Band option not present");
			}

			}
			} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEquifax", Status.FAIL,
					"Getting some issue in ownership card approval");
		}
		}
		}
	

	
	public void confirmationForFinanceVcc() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, "ExplicitWaitLongestTime");
		int totalcust= Integer.parseInt(testData.get("totalCust"));
//		for(int i=0;i<=totalcust;i++)
//		{
			List<WebElement> check= driver.findElements(equifaxCheck);
			List <WebElement>check1=driver.findElements(vccCheck);
//		if (verifyObjectDisplayed(equifaxCheck)) 
		if (check.get(0).isDisplayed() && check1.get(1).isDisplayed()) 
		{
			for(int i=0;i<=totalcust-1;i++)
			{
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.PASS,
					"Ownership card has been approved");
			String custName = driver.findElements(equifaxCheckCusName).get(i).getText();
			String custName1=driver.findElements(vccCheckCusName).get(i+1).getText();
			
			
			clickElementBy(triDot);

			if (verifyObjectDisplayed(creditBand)) {
				clickElementBy(creditBand);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

//				if (verifyObjectDisplayed(bandInfo))
				List <WebElement> bandinfolst=driver.findElements(bandInfo);
				if (bandinfolst.get(i).isDisplayed())
				{
					ObtainedBand = driver.findElement(By.xpath("//div[@class='band-info']//div[contains(text(),'"+custName+"')]")).getText();
					ObtainedBand1 = driver.findElement(By.xpath("//div[@class='band-info']//div[contains(text(),'"+custName1+"')]")).getText();
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing ", Status.PASS,
							"Obtained band is : "+ObtainedBand +"for "+ custName);
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing ", Status.PASS,
							"Obtained band is : "+ObtainedBand1 +" For "+custName1);

					clickElementBy(closeButton);
					System.out.println("Button clicked for "+ i+" time");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				} else {

					tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
							"Band Info is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationForFinancing", Status.FAIL,
						"Credit Band option not present");
			}

			}
			} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEquifax", Status.FAIL,
					"Getting some issue in ownership card approval");
		}
		}

	
	
	
	
	
	public void confirmationForRewards() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, barclaysCheck, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(barclaysCheck)) {
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			rewardsApprovedAmount = driver.findElement(rewardsAmount).getText().trim();
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForRewards", Status.PASS,
					"Reward card is approved and Approved amount is: " + rewardsApprovedAmount);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {

			tcConfig.updateTestReporter("ConfirmationPage", "confirmationForRewards", Status.FAIL,
					"Reward card is not approved");

		}

	}

	public void validateThankYouPage() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(nextBtn);
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		WebElement thanktext=driver.findElement(thankYouText);
		
		if (verifyObjectDisplayed(thankYouText))
//		if (thanktext.get(0).isDisplayed())
		{
			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("ConfirmationPage", "confirmation", Status.PASS,
					"Confirmation Text: " + driver.findElement(thankYouText).getText());
		
			
			} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmation", Status.FAIL,
					"Unable to get confirmation text");
		}
		
	}

	public void declineBarclays() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(barclaysDecline)) {

			tcConfig.updateTestReporter("declineBarclays", "declineBarclays", Status.PASS, "Rewards declined");
		}

		else {
			tcConfig.updateTestReporter("declineBarclays", "declineBarclays", Status.FAIL, "Rewards applied");
		}

	}

	public void verifyApplicationDeclinedForVacationClub() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(vccDecline)) {

			tcConfig.updateTestReporter("ConfirmationPage", "declineVCC", Status.PASS, "Rewards declined");
		}

		else {
			tcConfig.updateTestReporter("ConfirmationPage", "declineVCC", Status.FAIL, "Rewards applied");
		}

	}


	public void verifyApplicationPendingForVacationClub() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(vccpending)) {

			tcConfig.updateTestReporter("ConfirmationPage", "PendingVCC", Status.PASS, "Rewards Pending");
		}

		else {
			tcConfig.updateTestReporter("ConfirmationPage", "PendingCC", Status.FAIL, "Rewards Pending");
		}

	}

	
	public void confirmationEQBR() {
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, barclaysAgree, 20);

		if (verifyObjectDisplayed(barclaysAgree)) {

			clickElementBy(barclaysAgree);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(barclaysFin);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.FAIL,
					"Agreement checkbox not displayed");
		}

		if (verifyObjectDisplayed(barclaysCheck) && verifyObjectDisplayed(equifaxCheck)) {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.PASS,
					"Equifax and Barclays card is approved");

			clickElementBy(triDot);

			if (verifyObjectDisplayed(creditBand)) {
				clickElementBy(creditBand);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(bandInfo)) {
					ObtainedBand = driver.findElement(bandInfo).getText();
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.PASS,
							driver.findElement(bandInfo).getText());

					clickElementBy(closeButton);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementBy(nextBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(thankYouText)) {

						tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.PASS,
								"Confirmation Text: " + driver.findElement(thankYouText).getText());
					} else {
						tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.FAIL,
								"Error in getting confirmation text");
					}

				} else {

					tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.FAIL,
							"Band Info is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.FAIL,
						"Credit Band option not present");
			}

		} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQBR", Status.FAIL,
					"Equifax or Barclays card approval issue");
		}

	}

	public void confirmationEQVCC() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, 20);

		if (verifyObjectDisplayed(vccCheck) && verifyObjectDisplayed(equifaxCheck)) {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQVCC", Status.PASS,
					"Equifax and VCC card is approved, with amount: " + driver.findElement(vccAmount).getText());

			clickElementBy(triDot);

			if (verifyObjectDisplayed(creditBand)) {
				clickElementBy(creditBand);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(bandInfo)) {
					ObtainedBand = driver.findElement(bandInfo).getText();
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQVCC", Status.PASS,
							driver.findElement(bandInfo).getText());

					clickElementBy(closeButton);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementBy(nextBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(thankYouText)) {

						tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQVCC", Status.PASS,
								"Confirmation Text: " + driver.findElement(thankYouText).getText());
					} else {
						tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQVCC", Status.FAIL,
								"Error in getting confirmation text");
					}

				} else {

					tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQVCC", Status.FAIL,
							"Band Info is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQVCC", Status.FAIL,
						"Credit Band option not present");
			}

		} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationEQVCC", Status.FAIL,
					"Equifax or VCC card approval issue");
		}

	}

	// ***************************************************
	public void confirmationRewards() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(barclaysCheck)) {

			rewardsApprovedAmount = driver.findElement(rewardsAmount).getText().trim();
			tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.PASS,
					"barclays card is approved and Approved amount is: " + rewardsApprovedAmount);

			clickElementBy(triDot);

			if (verifyObjectDisplayed(creditBand)) {
				clickElementBy(creditBand);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(bandInfo)) {
					ObtainedBand = driver.findElement(bandInfo).getText();
					tcConfig.updateTestReporter("ConfirmationPage", "confirmationbarclays", Status.PASS,
							ObtainedBand);

					clickElementBy(closeButton);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementBy(nextBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(thankYouText)) {

						tcConfig.updateTestReporter("ConfirmationPage", "confirmationbarclays", Status.PASS,
								"Confirmation Text: " + driver.findElement(thankYouText).getText());
					} else {
						tcConfig.updateTestReporter("ConfirmationPage", "confirmationbarclays", Status.FAIL,
								"Error in getting confirmation text");
					}

				} else {

					tcConfig.updateTestReporter("ConfirmationPage", "confirmationbarclays", Status.FAIL,
							"Band Info is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("ConfirmationPage", "confirmationbarclays", Status.FAIL,
						"Credit Band option not present");
			}

		} else {
			tcConfig.updateTestReporter("ConfirmationPage", "confirmationbarclays", Status.FAIL,
					"barclays card approval issue");
		}

	}

	public void triDotNavigation() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, 20);

		if (verifyObjectDisplayed(triDot)) {

			clickElementBy(triDot);
			String menu = testData.get("eCreditMenu");
			if (verifyElementDisplayed(driver.findElement(By.xpath("//a[text()='" + menu + "']")))) {
				clickElementBy(By.xpath("//a[text()='" + menu + "']"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("triDotNavigation", "triDotNavigation", Status.PASS,
						"User navigated to menu item");

			} else {
				tcConfig.updateTestReporter("triDotNavigation", "triDotNavigation", Status.FAIL,
						"menu item not present");
			}

		} else {
			tcConfig.updateTestReporter("triDotNavigation", "triDotNavigation", Status.FAIL,
					"tri do menu icon is not present");
		}

	}

}
