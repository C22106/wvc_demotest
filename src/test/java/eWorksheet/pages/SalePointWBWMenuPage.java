package eWorksheet.pages;
import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointWBWMenuPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(SalePointWBWMenuPage.class);

	public SalePointWBWMenuPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;
	protected By next1= By.xpath("//input[@name='next']");

	@FindBy(xpath = "//input[@name='next']")
	WebElement next2;

	// socialSecurityNumber
	@FindBy(id = "wbwcommissions_salesMan")
	WebElement saleper;
	//For adding or reducing amount- gt attribute value= amount, deduct = amount-100
	@FindBy(id = "pymtAmt0")
	WebElement mainAmt;
	@FindBy(id = "pymtAmt1")
	WebElement deductAmt;
	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(id = "saveContract")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	WebElement conNo;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	@FindBy(xpath = "//span[contains(text(), 'Contracts')]")
	WebElement contracts;

	String strContractNo = "";
	String strMemberNo = "";

	By tourIdEnter = By.name("tourID");

	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By paymentSelect = By.name("paymentType");

	By nextClick = By.xpath("//input[@name='next']");

	By salePersonId = By.id("wbwcommissions_salesMan");

	By saveContractClick = By.id("saveContract");

	By printClick = By.xpath("//input[@name='print']");

	By memberNoCheck = By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]");

	By ssnSelect = By.xpath("//input[@name='socialSecurityNumber']");

	By homePhoneSelect = By.xpath("//input[@name='homePhone']");

	By primaryOwnerPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]");

	By ownerDetailsPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Owners')]");

	/* changes By Utsav */
	By MonthFromDrpDwn = By.xpath("//select[@name='beginDateMonth']");
	By DateFromDrpDwn = By.xpath("//select[@name='beginDateDay']");
	By YearFromDrpDwn = By.xpath("//select[@name='beginDateYear']");
	/* changes By Utsav */
	
	By EndMonthFromDrpDwn = By.xpath("//select[@name='endDateMonth']");
	By EndDateFromDrpDwn = By.xpath("//select[@name='endDateDay']");
	By EndYearFromDrpDwn = By.xpath("//select[@name='endDateYear']");
	
	
	@FindBy(xpath = "//input[@value='Experience']")
	WebElement experienceClick;
	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	@FindBy(xpath = "//input[@value='WorldMark']")
	WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	WebElement finalizeButton;

	By experienceSelect = By.xpath("//input[@value='Experience']");

	By summaryPage = By.xpath("//tr[@class='page_title']//div");

	By worldMarkSelect = By.xpath("//input[@value='WorldMark']");

	By nextValue = By.xpath("//input[@value='Next']");

	@FindBy(xpath = "//input[@name='memberNo']")
	WebElement Member_search;

	@FindBy(xpath = "//input[@value='Search']")
	WebElement search_btn;

	@FindBy(xpath = "//input[@name='tsc']")
	WebElement TSC_number;

	@FindBy(xpath = "//select[@id='duesPACId']")
	WebElement Dues_Payment_Method;

	@FindBy(xpath = "//input[@id='duesCCPACInfo.cardMemberName']")
	WebElement Account_holder_name;

	@FindBy(xpath = "//input[@id='duesCCPACInfo.cardNumber']")
	WebElement Card_number;

	@FindBy(xpath = "//select[@id='duesCCPACInfo.expireMonth']")
	WebElement Card_expireMonth;

	@FindBy(xpath = "//select[@id='duesCCPACInfo.expireYear']")
	WebElement Card_expireYear;
	
	
	By editButton = By.xpath("//input[@value='Edit']");

	public void sp_WM_ContractFlow() throws Exception {

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleBy(driver, tourIdEnter, "ExplicitWaitLongestTime");// to be deleted

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("TourID"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("WaveTime"));

			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		
		if(ExpectedConditions.alertIsPresent()!= null) {
			try {

				driver.switchTo().alert().accept();
			} catch (Exception e) {
				System.out.println("");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
	
			waitUntilElementVisibleBy(driver, saveContractClick, "ExplicitWaitLongestTime");
			
			List<WebElement> editButtons= driver.findElements(editButton);
			if(verifyElementDisplayed(editButtons.get(0))) {
				
				clickElementWb(editButtons.get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				waitUntilElementVisibleBy(driver, ownerDetailsPage, "ExplicitWaitLongestTime");

				if (verifyElementDisplayed(deleteSec)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> list = driver.findElements(deleteOwner);

					clickElementJSWithWait(list.get(0));

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					try {
						new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
						// driver.navigate().refresh();

						Alert alert = driver.switchTo().alert();
						alert.accept();
					} catch (Exception e1) {
						System.out.println("No Alert Prersent");
					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					System.out.println("No Secondary owner good to go");
				}

				clickElementJS(next1);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				
			}else {
				System.out.println("No Edit buttons");
				
			}
			
			

			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Contract Save Page Navigation Error");

			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, printClick, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(generate)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Review Page Displayed and Generate Button Clicked");
				clickElementJSWithWait(generate);
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Generate contract Page Navigation Error");
			}

			waitUntilElementVisibleBy(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText();

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Member number could not be retrieved");
			}

			
				
		

			}
	//Normal flow	
		else {
				
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Automation Pop Up No Available");
				
				
				
				
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, next1, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(driver.findElement(primaryOwnerPage))) {

				/*driver.findElement(ssnSelect).click();
				driver.findElement(ssnSelect).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(ssnSelect).sendKeys(testData.get("'SSN"));*/

				driver.findElement(homePhoneSelect).click();
				driver.findElement(homePhoneSelect).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(homePhoneSelect).sendKeys(testData.get("HomePhone"));
				//primaryPhoneNumber
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				clickElementJS(next1);

			} else if (verifyElementDisplayed(driver.findElement(ownerDetailsPage))) {
				System.out.println("OwnerDetails Page Directly Opened GTG");
			} else {
				System.out.println("Unhandled Error fow WBW Page");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleBy(driver, ownerDetailsPage, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(deleteSec)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> list = driver.findElements(deleteOwner);

				clickElementJSWithWait(list.get(0));

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				try {
					new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
					// driver.navigate().refresh();

					Alert alert = driver.switchTo().alert();
					alert.accept();
				} catch (Exception e1) {
					System.out.println("No Alert Prersent");
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				System.out.println("No Secondary owner good to go");
			}

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleBy(driver, paymentSelect, "ExplicitWaitLongestTime");
			List<WebElement> list1 = driver.findElements(paymentSelect);

			clickElementJS(list1.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(list1.get(1));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitUntilElementVisibleBy(driver, nextValue, "ExplicitWaitLongestTime");

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJS(nextValue);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			//Giving Sales Rep ID screen
			waitUntilElementVisibleBy(driver, salePersonId, "ExplicitWaitLongestTime");

			saleper.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			saleper.sendKeys(testData.get("strSalePer"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleBy(driver, saveContractClick, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e2) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Contract Save Page Navigation Error");

			}
			//Printing contract

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, printClick, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(generate)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Review Page Displayed and Generate Button Clicked");
				clickElementJSWithWait(generate);
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Generate contract Page Navigation Error");
			}

			waitUntilElementVisibleBy(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), "ExplicitWaitLongestTime");
//Contract number generated here
			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText();

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
						"Member number could not be retrieved");
			}

		}
	}

	

	
	public void sp_WM_ContractSearch() throws Exception{
		
		String fromDate=addDateInSpecificFormat("MM/dd/yyyy",-2);
		String[] arrDate=fromDate.split("/");
		String strMonthFromDrpDwn=arrDate[0];
		String strDateFromDrpDwn=arrDate[1];
		String strYearFromDrpDwn=arrDate[2];
		
		String tomDate=addDateInSpecificFormat("MM/dd/yyyy",+1);
		String[] splitDate=tomDate.split("/");
		String strendMonthFromDrpDwn=splitDate[0];
		String strendDateFromDrpDwn=splitDate[1];
		String strendYearFromDrpDwn=splitDate[2];
		
		if (strMonthFromDrpDwn.startsWith("0")){
			strMonthFromDrpDwn=strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")){
			strDateFromDrpDwn=strDateFromDrpDwn.replace("0", "");
		}
		
		if (strendMonthFromDrpDwn.startsWith("0")){
			strendMonthFromDrpDwn=strendMonthFromDrpDwn.replace("0", "");
		}
		if (strendDateFromDrpDwn.startsWith("0")){
			strendDateFromDrpDwn=strendDateFromDrpDwn.replace("0", "");
		}
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, worldMarkSelect, "ExplicitWaitLongestTime");
		

		if (verifyElementDisplayed(worldmarkClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
					"Contract Management Page Displayed");
			
						
			new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwn)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwn)).selectByValue(strYearFromDrpDwn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			new Select(driver.findElement(EndMonthFromDrpDwn)).selectByValue(strendMonthFromDrpDwn);
			new Select(driver.findElement(EndDateFromDrpDwn)).selectByValue(strendDateFromDrpDwn);
		
			
			
			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(worldmarkClick);

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleBy(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleBy(driver, summaryPage, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(finalizeButton)) {

					clickElementJSWithWait(finalizeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					// driver.switchTo().activeElement().sendKeys(Keys.ENTER);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleBy(driver, By.xpath("//tr/td[contains(.,'Finalize')]"), "ExplicitWaitLongestTime");
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));

					if (list2.get(0).getText().contains("Finalize")) {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
								"Contract Finalized");
					} else {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
								"Contract did notr finalize");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	
	}

	
	protected By searchCriteria = By.id("searchCriteria");
	protected By searchValue = By.id("searchValue");
	protected By searchButton =By.id("SearchBtn");
	protected By radioBtn = By.xpath("//input[@type='radio']"); 
	protected By authorizeBtn =By.xpath("//input[@value='Authorize']");
	
	public void wbwVCCPaymentApproval() {
		
		driver.navigate().to(testData.get("vccPaymentURL"));	
		
		waitUntilElementVisibleBy(driver, searchButton, "ExplicitWaitLongestTime");
		
		if(verifyObjectDisplayed(searchButton)) {
			
				new Select(driver.findElement(searchCriteria)).selectByVisibleText("Contract Number");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				driver.findElement(searchValue).click();
				driver.findElement(searchValue).sendKeys(strContractNo);				
				
				driver.findElement(searchButton).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				waitUntilElementVisibleBy(driver, authorizeBtn, "ExplicitWaitLongestTime");
				
				if(verifyObjectDisplayed(authorizeBtn)) {
					
					List<WebElement> radioBtnList= driver.findElements(radioBtn);				
					
					if(verifyElementDisplayed(radioBtnList.get(0))) {
						
						clickElementWb(radioBtnList.get(0));
						
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbwVCCPaymentApproval", Status.PASS,
								"Member Selected");
					
					}else {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbwVCCPaymentApproval", Status.FAIL,
								"Error in selecting any member");
					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(authorizeBtn).click();
						
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbwVCCPaymentApproval", Status.PASS,
								"Payment Authorized");
				}else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbwVCCPaymentApproval", Status.FAIL,
							"Authorize button is not displayed");
				}
				
			
		}else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbwVCCPaymentApproval", Status.FAIL,
					"Search button is not displayed");
		}
		
		
	}
	
}
