package eWorksheet.pages;
import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointWVR extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(SalePointWVR.class);

	public SalePointWVR(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;
	// socialSecurityNumber
	@FindBy(xpath = "//input[@name='salesman']")
	WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(xpath = "//input[@value='Save and Print']")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	WebElement conNo;
	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	String strContractNo = "";
	String strMemberNo = "";

	@FindBy(id = "customerNumber")
	WebElement customerNo;

	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	WebElement saleType;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	WebElement udiSelect;

	@FindBy(id = "Print")
	WebElement print;

	@FindBy(id = "newPoints")
	WebElement newPoints;

	@FindBy(xpath = "//input[@value='Calculate']")
	WebElement calculate;

	By nextClick = By.xpath("//input[@value='Next']");

	By customerNoInput = By.id("customerNumber");

	By saleTypeSelect = By.xpath("//input[@name='fvl_saleType' and @value='M']");

	By deleteCheck = By.xpath("//a[contains(.,'Delete')]");

	By udiClick = By.xpath("//input[@name='sales_types' and @value='UDI']");

	By newPointsCheck = By.id("newPoints");

	By calculateClick = By.xpath("//input[@value='Calculate']");

	By salePersonSelect = By.xpath("//input[@name='salesman']");

	By saveClick = By.xpath("//input[@value='Save and Print']");

	By printClick = By.id("Print");

	By memberNoCheck = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");

	By ssnSelect = By.xpath("//input[@name='ssn']");

	By genderSelect = By.xpath("//input[@name='sex' and @value='M']");

	By cashSelect = By.xpath("//td[contains(.,'Cash Sale')]/input[@type='radio']");

	By achSelect = By.xpath("//td[contains(.,'ACH Auto Pay')]/input[@type='radio']");

	By headerContractProcessOwner = By.xpath("//div[contains(text(),'Contract Processing - Owners')]");

	By headerCustInfo = By.xpath("//div[contains(text(),'Contract Processing - Customer Information')]");

	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By StatusOwner = By.xpath("//a[contains(.,'Status')]");

	By MonthFromDrpDwn = By.xpath("//select[@name='dobmonth']");
	By DateFromDrpDwn = By.xpath("//select[@name='dobday']");
	By YearFromDrpDwn = By.xpath("//select[@name='dobyear']");

	By primaryOwnerEdit = By.xpath("//input[@class='button' and @id='primaryOwner']");
	By updateButton = By.xpath("//input[@class='button' and @value='Update']");

	public void sp_New_ContractFlow() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, customerNoInput, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("TourID"));

			clickElementJS(next1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				System.out.println(e);
			}

			waitUntilElementVisibleBy(driver, saveClick, "ExplicitWaitLongestTime");

			if (verifyObjectDisplayed(primaryOwnerEdit)) {

				clickElementBy(primaryOwnerEdit);

				waitUntilElementVisibleBy(driver, genderSelect, 16);
				clickElementJSWithWait(genderSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(updateButton);

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				waitUntilElementVisibleBy(driver, nextClick, "ExplicitWaitLongestTime");
				if (verifyObjectDisplayed(headerContractProcessOwner)) {
					if (verifyElementDisplayed(deleteSec)) {
						List<WebElement> list = driver.findElements(deleteCheck);

						clickElementJSWithWait(list.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						System.out.println("No Secondary owner good to go");
					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(nextVal);

				} else {
					System.out.println("Owner Page Not Displayed");
				}

			} else {
				System.out.println("No Primary owner edit button");
			}


			waitUntilElementVisibleBy(driver, saveClick, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.FAIL,
						"Contract Save Page Navigation Error");

			}

			// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleBy(driver, printClick, 30);

			if (verifyElementDisplayed(print)) {
				clickElementJSWithWait(print);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			waitUntilElementVisibleBy(driver, memberNoCheck, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText().trim();

				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.FAIL,
						"Member number could not be retrieved");
			}

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow", Status.FAIL,
					"Automation Pop Up not available");
			/*
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LongWait"));
			 * 
			 * if (verifyObjectDisplayed(headerCustInfo)) {
			 * 
			 * try {
			 * 
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(ssnSelect).click(); driver.findElement(ssnSelect).clear();
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * clickElementJS(driver.findElement(genderSelect));
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); } catch (Exception e) {
			 * System.out.println("SSN Cannot Be Entered"); }
			 * 
			 * if (verifyElementDisplayed(saleType)) {
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.PASS, "Customer Info page Displayed");
			 * 
			 * // JavascriptExecutor executor = (JavascriptExecutor) driver;
			 * clickElementJS(saleType);
			 * 
			 * driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * } else { System.out.println("MainLine  Not Present User Can Proceed");
			 * 
			 * } clickElementJS(nextVal);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * } else { System.out.println("SSN Not Required User Can Proceed"); }
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LongWait"));
			 * 
			 * waitUntilElementVisibleBy(driver, nextClick, "ExplicitWaitLongestTime"); if
			 * (verifyObjectDisplayed(headerContractProcessOwner)) { if
			 * (verifyElementDisplayed(deleteSec)) { List<WebElement> list =
			 * driver.findElements(deleteCheck);
			 * 
			 * clickElementJSWithWait(list.get(0));
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * driver.switchTo().alert().accept();
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * } else { System.out.println("No Secondary owner good to go"); }
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * clickElementJS(nextVal);
			 * 
			 * } else { System.out.println("Owner Page Not Displayed"); } //
			 * clickElementJS(nextVal);
			 * 
			 * // waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * waitUntilElementVisibleBy(driver, udiClick, "ExplicitWaitLongestTime");
			 * 
			 * if (verifyElementDisplayed(udiSelect)) { clickElementJSWithWait(udiSelect);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * clickElementJS(nextVal); } else {
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.FAIL, "UDI not present"); }
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * waitUntilElementVisibleBy(driver, newPointsCheck, "ExplicitWaitLongestTime");
			 * 
			 * if (verifyElementDisplayed(newPoints)) { newPoints.click();
			 * newPoints.clear(); newPoints.sendKeys(testData.get("strPoints"));
			 * 
			 * } else { tcConfig.updateTestReporter("SalePointWVRMenuPage",
			 * "sp_New_contractFlow", Status.FAIL, "NewPoints not present"); }
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * clickElementJS(nextVal);
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * waitUntilElementVisibleBy(driver, nextClick, "ExplicitWaitLongestTime");
			 * 
			 * clickElementJS(nextVal);
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * waitUntilElementVisibleBy(driver, nextClick, "ExplicitWaitLongestTime");
			 * 
			 * clickElementJS(nextVal);
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * waitUntilElementVisibleBy(driver, nextClick, "ExplicitWaitLongestTime");
			 * 
			 * if (verifyObjectDisplayed(cashSelect)) {
			 * clickElementJS(driver.findElement(cashSelect));
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * clickElementJS(driver.findElement(cashSelect));
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * } else { tcConfig.updateTestReporter("SalePointWVRMenuPage",
			 * "sp_New_contractFlow", Status.FAIL, "Cash Select not present"); }
			 * 
			 * clickElementJS(nextVal);
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * waitUntilElementVisibleBy(driver, calculateClick, "ExplicitWaitLongestTime");
			 * 
			 * if (verifyElementDisplayed(calculate)) {
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.PASS, "Money Screen present");
			 * 
			 * clickElementJS(calculate);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * clickElementJS(nextVal);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * driver.switchTo().alert().accept();
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else {
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.FAIL, "Money Screen not present"); }
			 * 
			 * waitUntilElementVisibleBy(driver, salePersonSelect, 50);
			 * 
			 * saleper.click(); saleper.sendKeys(testData.get("strSalePer"));
			 * driver.switchTo().activeElement().sendKeys(Keys.TAB);
			 * clickElementJS(nextVal); waitUntilElementVisibleBy(driver, nextClick, "ExplicitWaitLongestTime");
			 * 
			 * clickElementJS(nextVal);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * waitUntilElementVisibleBy(driver, saveClick, "ExplicitWaitLongestTime");
			 * 
			 * if (verifyElementDisplayed(savecontract)) {
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.PASS, "Contract Review Page Displayed and Save Button Clicked");
			 * 
			 * clickElementJS(savecontract);
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); try {
			 * 
			 * driver.switchTo().alert().accept();
			 * 
			 * } catch (Exception e) { System.out.println(""); } } else {
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.FAIL, "Contract Save Page Navigation Error");
			 * 
			 * }
			 * 
			 * // driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * 
			 * waitUntilElementVisibleBy(driver, printClick, 30);
			 * 
			 * if (verifyElementDisplayed(print)) { clickElementJSWithWait(print);
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); }
			 * 
			 * waitUntilElementVisibleBy(driver, memberNoCheck, "ExplicitWaitLongestTime");
			 * 
			 * if (verifyElementDisplayed(conNo)) {
			 * 
			 * // testData.put("strContract", conNo.getText()); strContractNo =
			 * conNo.getText().trim();
			 * 
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.PASS, "Contract Created with number: " + conNo.getText()); } else {
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.FAIL, "Contract Creation failed "); }
			 * 
			 * if (verifyElementDisplayed(memberNo)) {
			 * 
			 * strMemberNo = memberNo.getText().split("\\:")[1].trim();
			 * System.out.println("MemberNo " + strMemberNo);
			 * 
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.PASS, "Member No: " + strMemberNo);
			 * 
			 * } else {
			 * 
			 * tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractFlow",
			 * Status.FAIL, "Member number could not be retrieved"); }
			 */}
	}

	WebElement experienceClick;

	@FindBy(xpath = "//input[@value='WorldMark']")
	WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	WebElement finalizeButton;

	@FindBy(xpath = "//a[contains(.,'ALL BY DATE')]")
	WebElement selectAllByDate;

	@FindBy(xpath = "//select[@name='startmonth']")
	WebElement startmonth;

	@FindBy(xpath = "//select[@name='startday']")
	WebElement startday;

	@FindBy(xpath = "//input[@name='getData']")
	WebElement getData;

	@FindBy(xpath = "//input[@value='Change Status']")
	WebElement changeStatus;

	@FindBy(xpath = "//input[@value='Transmit']")
	WebElement Transmit;

	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	WebElement returnBack;

	@FindBy(xpath = "//tr/td[contains(.,'T')]")
	WebElement validateStatus;

	By searchFilter = By.xpath("//a[contains(.,'ALL BY DATE')]");

	By statusChangeClick = By.xpath("//input[@value='Change Status']");

	By transmitClick = By.xpath("//input[@value='Transmit']");

	By reportBack = By.xpath("//a[contains(.,'Return to Batch Report')]");

	By StatusVali = By.xpath("//tr/td[contains(.,'T')]");

	public void sp_New_contractSearch() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, searchFilter, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(selectAllByDate)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.PASS,
					"Contract in batch report Page Displayed");

			clickElementJS(selectAllByDate);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// LocalDate today= LocalDate.now();
			Calendar cal = Calendar.getInstance();
			DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			Date today = cal.getTime();
			// cal.MONTH;

			int monthVal = (cal.get(Calendar.MONTH));

			String strMonth = String.valueOf(monthVal).trim();
			String strDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH)).trim();

			String tomDate = addDateInSpecificFormat("MM/dd/yyyy", +1);
			String[] splitDate = tomDate.split("/");
			String strendMonthFromDrpDwn = splitDate[0];
			String strendDateFromDrpDwn = splitDate[1];
			String strendYearFromDrpDwn = splitDate[2];

			if (strendMonthFromDrpDwn.startsWith("0")) {
				strendMonthFromDrpDwn = strendMonthFromDrpDwn.replace("0", "");
			}
			if (strendDateFromDrpDwn.startsWith("0")) {
				strendDateFromDrpDwn = strendDateFromDrpDwn.replace("0", "");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, By.name("startmonth"), "ExplicitWaitLongestTime");

			new Select(driver.findElement(By.name("startmonth"))).selectByValue(strMonth);

			new Select(driver.findElement(By.name("startday"))).selectByValue(strDate);

			new Select(driver.findElement(By.name("endmonth"))).selectByValue(strendMonthFromDrpDwn);

			new Select(driver.findElement(By.name("endday"))).selectByValue(strendDateFromDrpDwn);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(getData);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"), "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(
					driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.PASS,
						"Contract No Verified and is: " + strContractNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"));

				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.FAIL,
						"Contract No Not Verified");

			}

			waitUntilElementVisibleBy(driver, statusChangeClick, 50);

			if (verifyElementDisplayed(changeStatus)) {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				clickElementJSWithWait(changeStatus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisibleBy(driver, transmitClick, "ExplicitWaitLongestTime");

				if (verifyElementDisplayed(Transmit)) {

					clickElementJSWithWait(Transmit);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();

					waitUntilElementVisibleBy(driver, reportBack, "ExplicitWaitLongestTime");

					if (verifyElementDisplayed(returnBack)) {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.PASS,
								"Status Change Successful");
						clickElementJSWithWait(returnBack);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.FAIL,
								"Status Change UnSuccessful");
					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleBy(driver, StatusVali, "ExplicitWaitLongestTime");

					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'T')]"));

					if (list2.get(0).getText().contains("T")) {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.PASS,
								"Contract Transmitted");
					} else {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.FAIL,
								"Contract did not Transmitted");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}
	
	protected By searchCriteria = By.id("searchCriteria");
	protected By searchValue = By.id("searchValue");
	protected By searchButton =By.id("SearchBtn");
	protected By radioBtn = By.xpath("//input[@type='radio']"); 
	protected By authorizeBtn =By.xpath("//input[@value='Authorize']");
	
	public void wvrVCCPaymentApproval() {
		
		driver.navigate().to(testData.get("vccPaymentURL"));	
		
		waitUntilElementVisibleBy(driver, searchButton, "ExplicitWaitLongestTime");
		
		if(verifyObjectDisplayed(searchButton)) {
			
				new Select(driver.findElement(searchCriteria)).selectByVisibleText("Contract Number");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				driver.findElement(searchValue).click();
				driver.findElement(searchValue).sendKeys(strContractNo);				
				
				driver.findElement(searchButton).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				waitUntilElementVisibleBy(driver, authorizeBtn, "ExplicitWaitLongestTime");
				
				if(verifyObjectDisplayed(authorizeBtn)) {
					
					List<WebElement> radioBtnList= driver.findElements(radioBtn);				
					
					if(verifyElementDisplayed(radioBtnList.get(0))) {
						
						clickElementWb(radioBtnList.get(0));
						
						tcConfig.updateTestReporter("SalePointWVR", "wvrVCCPaymentApproval", Status.PASS,
								"Member Selected");
					
					}else {
						tcConfig.updateTestReporter("SalePointWVR", "wvrVCCPaymentApproval", Status.FAIL,
								"Error in selecting any member");
					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(authorizeBtn).click();
						
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("SalePointWVR", "wvrVCCPaymentApproval", Status.PASS,
								"Payment Authorized");
				}else {
					tcConfig.updateTestReporter("SalePointWVR", "wvrVCCPaymentApproval", Status.FAIL,
							"Authorize button is not displayed");
				}
				
			
		}else {
			tcConfig.updateTestReporter("SalePointWVR", "wvrVCCPaymentApproval", Status.FAIL,
					"Search button is not displayed");
		}
		
		
	}

}
