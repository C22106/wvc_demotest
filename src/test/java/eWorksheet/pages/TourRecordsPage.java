package eWorksheet.pages;

import java.awt.Robot;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class TourRecordsPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public TourRecordsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	LeadsPage leadPage = new LeadsPage(tcConfig);
	public static String salesAgentName;
	public static String newTourId;
	public static String strTourAppTime = "";
	public By createdTourLink = By.xpath("(//table[contains(@class,'uiVirtualDataTable')]/tbody/tr/th//a)[1]");
	public By newBtn = By.xpath("//div[text()='New']");
	public By latestTour = By.xpath("//span[text()='Tour Record']//div");
	// public By
	// ownerTourRecordRadioBtn=By.xpath("//div[@class='changeRecordTypeOptionRightColumn']/span[text()='Owner
	// VOI Tour Record']");
	public By nextBtn = By.xpath("//button/span[text()='Next']");
	public By txtSalesStore = By.xpath("//input[contains(@title,'Sales Stores')]");
	public By itemStoreOption = By.xpath("(//li[contains(@class,'default uiAutocompleteOption')]/a)[2]");
	public By txtAppointmentLoc = By.xpath("//input[contains(@title,'Locations')]");
	public By txtLeads = By.xpath("//input[contains(@title,'Leads')]");
	public By txtOwners = By.xpath("//input[contains(@title,'Owners')]");
	public By txtMarkettingAgent = By.xpath("//span[text()='Marketing Agent']/../../../../div//input");
	public By lblTourID = By.xpath("//span[text()='Tour Record Number']/../div//span");
	public By saveBtn = By.xpath("//button[@title='Save']");
	public By bookOptionSelect = By.xpath("//span[text()='Book Appointment']");
	public By Incentivestab = By.xpath("//span[text()='Incentives']");
	public By addPromissedItem = By
			.xpath("//div[@class='slds-col slds-size_1-of-3']/button[contains(.,'Add Promised Item')]");
	public By addPromissedPage = By.xpath("//header[@class='slds-modal__header']/h2[contains(.,'Add Promised Item')]");
	public By addPromissedsearch = By.xpath("//input[@placeholder='Search Product']");
	public By addPromPlus = By.xpath("//label[@class='slds-checkbox_faux']");
	public By addBtn = By.xpath("//button[text()='Add']");
	public By addProdPop = By.xpath("//h2[text()='Add product Value']");
	// public By addProdPopvalue=By.xpath("//div[@class='slds-truncate
	// voucherNumber']");
	public By addProdPopvalue = By.xpath("//div[@class='slds-form-element__control slds-grow']/input");
	public By addProdPopAdd = By.xpath("(//footer[@class='slds-modal__footer']/button)[2]");
	public By checkAll = By.xpath("(//table/thead/tr/th/div/input[@type='checkbox'])[1]");
	public By distributeBtn = By.xpath("//div[@class='slds-col']/button");
	public By voucherInput = By.xpath("(//div[@class='slds-form-element__control slds-grow']/input)[1]");
	public By succesMsg = By.xpath("(//div[@class='slds-notify__content']/h2)[2]");

	// public By getAppointmentsBtn=By.xpath("//button[text()='Get
	// Appointments']");
	public By drpDwnMarketingStrtgy = By.xpath("(//select[@name='marketingStrategy'])[1]");
	public By drpDwnMarketingSrcType = By.xpath("(//select[@name='marketingSourceType'])[1]");
	public By lblAvailableSlot = By.xpath("(//span[text()='Available Slots'])[1]");
	public By yesBtn = By.xpath("//button[text()='YES']");
	public By txtAppointmentConfirm = By
			.xpath("//h2[text()='Appointment Confirmed']/../../div//span/div[contains(.,'Success')]");
	public By okBtn = By.xpath("//button[text()='OK']");
	public By qScoreDropdwn = By.xpath("//span[text()='Qualification Status']/../..//a[@class='select']");
	
	protected By getAppointmentsBtn = By.xpath("//button[text()='Get Appointments']");

	public By removeSalesStoreIcon = By.xpath("(//button[contains(@title,'Remove')])[1]");
	public By removeAppointmentLocIcon = By.xpath("(//button[contains(@title,'Remove')])[2]");

	public By reSalesStoretxt = By.xpath("//div[contains(@class,'SalesStoreSelect')]//input");
	public By reBookLocation = By.xpath("//div[contains(@class,'BookLocationSelect')]//input");
	public By globalSearchEdit = By.xpath("//input[contains(@title,'Search Tour Records and more')]");
	protected By ownerSearchBoxEdit = By.xpath("//input[contains(@name,'search-input')]");
	protected By salesStoreDrpDwn = By.xpath("//select[@class='slds-select']");
	protected By guestSelectionChkBx = By.xpath("//input[contains(@id,'tour-table_checkbox')]/../label");
	protected By checkInBtn = By.xpath("//button[contains(text(),'Check-In')]");
	protected By skipBtn = By.xpath("//button[contains(text(),'Skip')]");
	protected By checkedInLink = By.xpath("//a[contains(text(),'Checked-In')]");
	protected By selectGuestCheckedIn = By.xpath("//div[@class='slds-form-element__control']/input[@type='checkbox']");
	protected By assignRepsBtn = By.xpath("//button[text()='Assign Reps']");
	protected By onTourLink = By.xpath("//a[contains(text(),'On Tour')]");
	protected By completeTourBtn = By.xpath("(//button[text()='Complete Tour'])[1]");
	protected By qaRepEdit = By.xpath("//span[text()='QA Rep']/../..//input");
	protected By selectDidTourdrpDwn = By.xpath("//select[@name='didTheyTour']");
	protected By saveDispositionBtn = By.xpath("//button[text()='Save']");
	protected By dispositionDrpdwn = By.xpath("//select[@name='dispositionStatus']");
	protected By quickSearchEdit = By.xpath("//input[@placeholder='Quick Search']");
	public By saleStorDD = By.xpath("//select[@name='selectItem']");
	public By drpmarketingsubsource = By.xpath("(//select[@name='marketingSubSource'])[1]");
	public By drpMarketingcamp = By.xpath(
			"((//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select'])[2])[1]");
	public By drpBrand = By.xpath(
			"((//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select'])[1])[1]");
	public By iconCancelled = By
			.xpath("//li[contains(@class,'canceled')]//span[@class='slds-path__title' and text()='Cancelled']");
	public By iconCheckedIn = By.xpath("//li[contains(@class,'incomplete')]//span[@class='slds-path__title' and text()='Checked In']");
	public By btnMarkComlete = By.xpath("//button[contains(text(),'Mark Status as Complete')]");
	public By btnPopUpYes = By.xpath("//button[text()='Yes']");
	public By iconDownKey = By
			.xpath("//table[@role='grid']//lightning-icon[contains(@class,'down')]/lightning-primitive-icon");
	protected By deleteTourBtn = By.xpath("(//a[@title='Delete'])[1]");
	protected By reDeleteBtn = By.xpath("//span[text()='Delete']");
	protected By deleteMsgPopUp = By.xpath("//span[contains(@class,'toastMessage')]");
	protected By datePicker = By.xpath("(//a[@class='datePicker-openIcon display'])[1]");
	protected By selectYear = By.xpath("//label/select[@class='slds-select picklist__label']");
	protected By nextMonthLink = By.xpath("//a[@title='Go to next month']");
	protected By todayLink = By.xpath("//button[text()='Today']");
	protected By salesManagerEditBtn = By.xpath("//button[@title='Edit Sales Manager']");
	protected By salesAgentEditBtn = By.xpath("//button[@title='Edit Sales Agent 1']");
	protected By salesMinputTxt = By.xpath("(//span[text()='Sales Manager'])[2]//following::input[@title='Search People'][1]");
	protected By salesAinputTxt = By.xpath("(//span[text()='Sales Agent 1'])[2]//following::input[@title='Search People'][1]");
	protected By salesManagerSelect = By.xpath("//span[contains(@title,'"+testData.get("AssignSalesManager")+"')]");
	protected By salesAgentSelect = By.xpath("//span[contains(@title,'"+testData.get("AssignSalesAgent")+"')]");
	protected By salesManagerLink = By.xpath("//div[text()='People']//following::a[@data-refid='recordId']");
	protected By checkinTimeDatepIck = By.xpath("(//span[text()='Tour Milestone Timestamps'])[4]//following::span[text()='Check In Time']//following::a[@class='datePicker-openIcon display'][1]");
	protected By saveButton = By.xpath("(//span[text()='Save'])[2]");

	public void verifyTourRecord() {
		waitUntilElementVisibleBy(driver, createdTourLink, "ExplicitWaitLongestTime");
		String strTourNumber = driver.findElement(createdTourLink).getAttribute("title");
		// clickElementBy(createdTourLink);
		if (verifyObjectDisplayed(createdTourLink)) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.PASS,
					"New tour created ; Tour ID : " + strTourNumber);
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.FAIL,
					"Unable to create new Tour Record");
		}
	}

	public void openCreatedNewTour() {
		try {
			// waitUntilElementVisibleBy(driver, createdTourLink, "ExplicitWaitLongestTime");
			// clickElementBy(createdTourLink);
			waitUntilElementVisibleBy(driver, iconCancelled, "ExplicitWaitLongestTime");
			clickElementBy(iconCancelled);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnMarkComlete, "ExplicitWaitLongestTime");
			clickElementBy(btnMarkComlete);
			waitUntilElementVisibleBy(driver, btnPopUpYes, "ExplicitWaitLongestTime");
			clickElementBy(btnPopUpYes);
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "openCreatedNewTour", Status.FAIL,
					"Failed due to :" + e.getMessage());
		}
	}

	public void cancelNativeTour() {
		try {
			// waitUntilElementVisibleBy(driver, createdTourLink, "ExplicitWaitLongestTime");
			// clickElementBy(createdTourLink);
			waitUntilElementVisibleBy(driver, iconCancelled, "ExplicitWaitLongestTime");
			clickElementBy(iconCancelled);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnMarkComlete, "ExplicitWaitLongestTime");
			clickElementBy(btnMarkComlete);
			waitUntilElementVisibleBy(driver, btnPopUpYes, "ExplicitWaitLongestTime");
			clickElementJSWithWait(btnPopUpYes);
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "openCreatedNewTour", Status.FAIL,
					"Failed due to :" + e.getMessage());
		}
	}
	
	public void checkinNativeTour() {
		try {
			// waitUntilElementVisibleBy(driver, createdTourLink, "ExplicitWaitLongestTime");
			// clickElementBy(createdTourLink);
			waitUntilElementVisibleBy(driver, iconCheckedIn, "ExplicitWaitLongestTime");
			clickElementBy(iconCheckedIn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnMarkComlete, "ExplicitWaitLongestTime");
			clickElementBy(btnMarkComlete);
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "checkinNativeTour", Status.FAIL,
					"Failed due to :" + e.getMessage());
		}
	}

	public void openCreatedTourToVerify() {
		boolean Flag = false;
		clickElementBy(createdTourLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> errorMsgList = driver
				.findElements(By.xpath("//span[contains(text(),'not eligible to Tour')]/../../../span"));
		if (errorMsgList.size() == 0) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.FAIL,
					"No error message is displayed");
		}
		for (int iterator = 0; iterator < errorMsgList.size(); iterator++) {
			if (errorMsgList.get(iterator).getAttribute("style").contains("rgb(255, 0, 0);")) {
				Flag = true;
				break;
			} else {
				Flag = false;
			}
		}
		if (Flag == true) {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.PASS,
					"Error message" + " is displayed in RED color");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.FAIL,
					"Error message is NOT" + " displayed in RED color");
		}
	}

	public void selectTourType() {
		waitUntilElementVisibleBy(driver, createdTourLink, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(newBtn)) {
			tcConfig.updateTestReporter("TourRecordsPage", "selectTourType", Status.PASS,
					"User is navigated to Tour Records Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "selectTourType", Status.FAIL,
					"User is Not navigated to Tour Records Page");
		}
		clickElementBy(newBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement ownerTourRecordRadioBtn = driver.findElement(By.xpath(
				"//div[@class='changeRecordTypeOptionRightColumn']/span[text()='" + testData.get("TourType") + "']"));
		ownerTourRecordRadioBtn.click();
		clickElementBy(nextBtn);
	}

	public void createNewTour() {

		// waitUntilElementVisibleBy(driver, txtSalesStore, "ExplicitWaitLongestTime");
		// //if( testData.get("NoSalesStore").equalsIgnoreCase("N")){
		// fieldDataEnter(txtSalesStore, testData.get("SalesStore"));
		// clickElementBy(By.xpath("(//div[contains(@title,'"+testData.get("SalesStore")+"')])[1]"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// //}
		waitUntilElementVisibleBy(driver, txtAppointmentLoc, "ExplicitWaitLongestTime");
		fieldDataEnter(txtAppointmentLoc, testData.get("AppointmentLoc"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement appLocOption = driver
				.findElement(By.xpath("(//div[contains(@title,'" + testData.get("AppointmentLoc") + "')])[1]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		appLocOption.click();
		/*
		 * clickElementBy(qScoreDropdwn); WebElement
		 * qScoreValue=driver.findElement(By.xpath("//a[@title='"+testData.get(
		 * "QualificationScore")+"']")); qScoreValue.click();
		 */
		if (testData.get("TourType").contains("Lead")) {
			fieldDataEnter(txtLeads, testData.get("LeadName"));
		} else if (testData.get("TourType").contains("Owner")) {
			fieldDataEnter(txtOwners, testData.get("LeadName"));
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement txtLeadsOption = driver.findElement(By.xpath("//div[@title='" + testData.get("LeadName") + "']"));
		txtLeadsOption.click();
		fieldDataEnter(txtMarkettingAgent, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement marketingAgentOption = driver
				.findElement(By.xpath("//div[@title='" + testData.get("AgentName") + "']"));
		marketingAgentOption.click();
		clickElementBy(saveBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lblTourID, "ExplicitWaitLongestTime");
		System.out.println("Tour ID :" + driver.findElement(lblTourID).getText().trim());
		if (verifyObjectDisplayed(lblTourID)) {
			newTourId = driver.findElement(lblTourID).getText().trim();
			tcConfig.updateTestReporter("TourRecordsPage", "createNewTour", Status.PASS,
					"Tour ID : " + driver.findElement(lblTourID).getText().trim() + " has been created");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "createNewTour", Status.FAIL, "No tour has been created");
		}

	}
	/*
	 * public void bookAppointment(){ waitUntilElementVisibleBy(driver,
	 * bookOptionSelect, "ExplicitWaitLongestTime"); clickElementBy(bookOptionSelect);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * if(testData.get("NoSalesStore").equalsIgnoreCase("Y")){
	 * clickElementBy(removeSalesStoreIcon);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * clickElementBy(removeSalesStoreIcon);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); try{
	 * if(getAppointmentsBtn.isEnabled()==false ||
	 * getAppointmentsBtn.getAttribute("disabled")=="true"){
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.PASS,
	 * "User is unable to book appointment if Sales store is blank"); }else{
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL,
	 * "User is able to book appointment if Sales store is blank"); }
	 * }catch(NoSuchElementException e){ System.out.println("catched");
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL,
	 * "User is able to book appointment if Sales store is blank"); } }else{
	 * clickElementBy(removeSalesStoreIcon);
	 * fieldDataEnter(reSalesStoretxt,testData.get("SalesStore"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(By.xpath("//span[contains(text(),'"+testData.get(
	 * "SalesStore")+"')][1]")).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * clickElementBy(removeAppointmentLocIcon);
	 * fieldDataEnter(reBookLocation,testData.get("AppointmentLoc"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(By.xpath("//span[contains(text(),'"+testData.get(
	 * "AppointmentLoc")+"')]")).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * 
	 * new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * mouseoverAndClick(getAppointmentsBtn);
	 * //clickElementBy(getAppointmentsBtn); waitUntilElementVisibleBy(driver,
	 * lblAvailableSlot, "ExplicitWaitLongestTime"); clickElementBy(lblAvailableSlot);
	 * waitUntilElementVisibleBy(driver, yesBtn, "ExplicitWaitLongestTime"); clickElementBy(yesBtn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * waitUntilElementVisibleBy(driver, txtAppointmentConfirm,"ExplicitWaitLongestTime");
	 * if(verifyObjectDisplayed(txtAppointmentConfirm)){
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.PASS, "New appointment has been created"); }else{
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL, "User is unable to book an appointment"); }
	 * waitUntilElementVisibleBy(driver, okBtn,"ExplicitWaitLongestTime"); clickElementBy(okBtn);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); }
	 * 
	 * }
	 */

	public void addPromiseItemAndDistribute() {
		waitUntilElementVisibleBy(driver, Incentivestab, "ExplicitWaitLongestTime");
		clickElementBy(Incentivestab);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(addPromissedItem)) {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
					"User is unable to book appointment if Sales store is blank");
			clickElementBy(addPromissedItem);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		}

		waitUntilElementVisibleBy(driver, addPromissedPage, "ExplicitWaitLongestTime");
		clickElementBy(addPromissedsearch);
		fieldDataEnter(addPromissedsearch, testData.get("Product"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(addPromPlus).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(addBtn).click();
		waitUntilElementVisibleBy(driver, addProdPop, "ExplicitWaitLongestTime");
		driver.findElement(addProdPopvalue).click();
		driver.findElement(addProdPopvalue).clear();
		fieldDataEnter(addProdPopvalue, testData.get("voucherNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(addProdPopAdd).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(checkAll).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(distributeBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// driver.findElement(voucherInput).click();
		// driver.findElement(voucherInput).clear();
		fieldDataEnter(voucherInput, testData.get("voucherNumber"));
		driver.findElement(addProdPopAdd).click();
		waitUntilElementVisibleBy(driver, succesMsg, "ExplicitWaitLongestTime");

		if (verifyObjectDisplayed(succesMsg)) {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
					"New appointment has been created");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
					"User is unable to book an appointment");
		}

	}
	
	@FindBy(xpath = "//span[text()='Sales Agent 1']//following::a[1]") protected WebElement getSalesAgentName;
	@FindBy(xpath = "//span[text()='Disposition Details']")
	protected WebElement dispositionDetails;
	public void assignSalesAgentorManager(String strRole) {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			/*do{
				Robot rob = new Robot();
				rob.mouseWheel(10);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}while(!driver.findElement(By.xpath("//span[text()='Disposition Details']")).isDisplayed());*/
			
			By elements = By.xpath("//span[text()='Tour Milestone Timestamps']");
	
			scrollDownForElementJSBy(elements);
			if (strRole.equals("Manager")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				//ScrollByVisibleElement(driver.findElement(salesManagerEditBtn));
				clickElementBy(salesManagerEditBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(salesMinputTxt, testData.get("AssignSalesManager"));
				waitUntilElementVisibleBy(driver, salesManagerSelect, "ExplicitWaitLongestTime");
				clickElementBy(salesManagerSelect);
				waitUntilElementVisibleBy(driver, salesManagerLink, "ExplicitWaitLongestTime");
				clickElementBy(salesManagerLink);
				tcConfig.updateTestReporter("TourRecordsPage", "assignSalesAgent", Status.PASS,
						"Sales Manager" + testData.get("AssignSalesManager") + " is assigned successfully");
				waitUntilElementVisibleBy(driver, saveButton, "ExplicitWaitLongestTime");
				clickElementBy(saveButton);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				pageUp();pageUp();pageUp();
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}
			
			else if (strRole.equals("Agent")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				//ScrollByVisibleElement(driver.findElement(salesAgentEditBtn));
				clickElementBy(salesAgentEditBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(salesAinputTxt, testData.get("AssignSalesAgent"));
				waitUntilElementVisibleBy(driver, salesAgentSelect, "ExplicitWaitLongestTime");
				clickElementBy(salesAgentSelect);
				waitUntilElementVisibleBy(driver, salesManagerLink, "ExplicitWaitLongestTime");
				clickElementBy(salesManagerLink);
				tcConfig.updateTestReporter("TourRecordsPage", "assignSalesAgent", Status.PASS,
						"Sales Agent" + testData.get("AssignSalesAgent") + " is assigned successfully");
				salesAgentName = getSalesAgentName.getText();
				waitUntilElementVisibleBy(driver, saveButton, "ExplicitWaitLongestTime");
				clickElementBy(saveButton);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				//ScrollUp(driver.findElement(bookOptionSelect));
				/*do{
					Robot rob = new Robot();
					rob.mouseWheel(-18);
				}while(!driver.findElement(bookOptionSelect).isDisplayed());
				waitForSometime(tcConfig.getConfig().get("MedWait"));*/
				
				pageUp();pageUp();pageUp();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}
			
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecordsPage", "assignSalesAgent", Status.FAIL, "Sales Agent"
					+ testData.get("AssignSalesAgent") + " is not assigned successfully" + e.getMessage());
		}
	}

	/*public void bookAppointment() {
		
		waitUntilElementVisibleBy(driver, bookOptionSelect, "ExplicitWaitLongestTime");
		clickElementBy(bookOptionSelect);
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("NoSalesStore").equalsIgnoreCase("Y")) {
			clickElementBy(removeSalesStoreIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			
			
			 * clickElementBy(removeSalesStoreIcon);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 
			try {
				if (getAppointmentsBtn.isEnabled() == false || getAppointmentsBtn.getAttribute("disabled") == "true") {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
							"User is unable to book appointment if Sales store is blank");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
							"User is able to book appointment if Sales store is blank");
				}
			} catch (NoSuchElementException e) {
				System.out.println("catched");
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is able to book appointment if Sales store is blank");
			}
		} else {

			waitUntilElementVisibleBy(driver, removeSalesStoreIcon, "ExplicitWaitLongestTime");
			clickElementBy(removeSalesStoreIcon);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			fieldDataEnter(reSalesStoretxt, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]"),
					"ExplicitWaitLongestTime");
			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, removeAppointmentLocIcon, "ExplicitWaitLongestTime");
			clickElementBy(removeAppointmentLocIcon);
			fieldDataEnter(reBookLocation, testData.get("AppointmentLoc"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver,
					By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"), "ExplicitWaitLongestTime");

			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"))
					.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(drpBrand)).selectByVisibleText(testData.get("Brand"));
			;
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
				new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				 * new
				 * Select(driver.findElement(drpMarketingcamp)).selectByIndex(1)
				 * ; waitForSometime(tcConfig.getConfig().get("MedWait"));
				 
			}

			waitUntilElementVisibleBy(driver, getAppointmentsBtn, "ExplicitWaitLongestTime");
			mouseoverAndClick(getAppointmentsBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lblAvailableSlot, "ExplicitWaitLongestTime");
			driver.findElement(lblAvailableSlot).click();
			waitUntilElementVisibleBy(driver, yesBtn, "ExplicitWaitLongestTime");
			clickElementBy(yesBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, txtAppointmentConfirm, "ExplicitWaitLongestTime");
			if (verifyObjectDisplayed(txtAppointmentConfirm)) {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
						"New appointment has been created");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is unable to book an appointment");
			}
			waitUntilElementVisibleBy(driver, okBtn, "ExplicitWaitLongestTime");
			clickElementBy(okBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

	}*/
	
	@FindBy(xpath = "//span[text()='Home']") WebElement homeTab;
	public void bookAppointment() {
		//waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*int i=1;
		if(i==1)
		{
			waitUntilElementVisibleBy(driver, By.xpath("(//span[text()='Book Appointment'])["+i+"]"), "ExplicitWaitLongestTime");
			clickElementBy(By.xpath("(//span[text()='Book Appointment'])["+i+"]"));
		}else{
			waitUntilElementVisibleBy(driver, By.xpath("(//span[text()='Book Appointment'])[2]"), "ExplicitWaitLongestTime");
			clickElementBy(By.xpath("(//span[text()='Book Appointment'])[2]"));
		}*/
		
		waitUntilElementVisibleBy(driver, bookOptionSelect, "ExplicitWaitLongestTime");
		clickElementBy(bookOptionSelect);
		
		//waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("NoSalesStore").equalsIgnoreCase("Y")) {
			clickElementBy(removeSalesStoreIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * clickElementBy(removeSalesStoreIcon);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */
			try {
				if (driver.findElement(getAppointmentsBtn).isEnabled() == false || driver.findElement(getAppointmentsBtn).getAttribute("disabled") == "true") {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
							"User is unable to book appointment if Sales store is blank");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
							"User is able to book appointment if Sales store is blank");
				}
			} catch (NoSuchElementException e) {
				System.out.println("catched");
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is able to book appointment if Sales store is blank");
			}
		} else {

			/*waitUntilElementVisibleBy(driver, removeSalesStoreIcon, "ExplicitWaitLongestTime");
			clickElementBy(removeSalesStoreIcon);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			fieldDataEnter(reSalesStoretxt, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]"),
					"ExplicitWaitLongestTime");
			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));*/

			/*waitUntilElementVisibleBy(driver, removeAppointmentLocIcon, "ExplicitWaitLongestTime");
			clickElementBy(removeAppointmentLocIcon);
			fieldDataEnter(reBookLocation, testData.get("AppointmentLoc"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver,
					By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"), "ExplicitWaitLongestTime");

			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"))
					.click();*/
			/*waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(drpBrand)).selectByVisibleText(testData.get("Brand"));
			;
			waitForSometime(tcConfig.getConfig().get("MedWait"));*/
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, drpDwnMarketingStrtgy, "ExplicitWaitLongestTime");
			new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, drpDwnMarketingSrcType, "ExplicitWaitLongestTime");
			new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
			//waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
				new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*
				 * new
				 * Select(driver.findElement(drpMarketingcamp)).selectByIndex(1)
				 * ; waitForSometime(tcConfig.getConfig().get("MedWait"));
				 */
			}

			waitUntilElementVisibleBy(driver, getAppointmentsBtn, "ExplicitWaitLongestTime");
			mouseoverAndClick(getAppointmentsBtn);
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lblAvailableSlot, "ExplicitWaitLongestTime");
			driver.findElement(lblAvailableSlot).click();
			waitUntilElementVisibleBy(driver, yesBtn, "ExplicitWaitLongestTime");
			clickElementBy(yesBtn);
			//waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, txtAppointmentConfirm, "ExplicitWaitLongestTime");
			/*if (verifyObjectDisplayed(txtAppointmentConfirm)) {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
						"New appointment has been created");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is unable to book an appointment");
			}*/
			waitUntilElementVisibleBy(driver, okBtn, "ExplicitWaitLongestTime");
			clickElementBy(okBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			
		}

	}

	public void searchForTour() {

		waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(ownerSearchBoxEdit)) {
			tcConfig.updateTestReporter("TourRecordsPage", "searchForTour", Status.PASS,
					"User is navigated to Tour Records Page");
			if (testData.get("TourNo") == null || testData.get("TourNo").isEmpty()
					|| testData.get("TourNo").contentEquals("")) {
				if (newTourId == null || newTourId == "" || newTourId.isEmpty()) {
					if (LeadsPage.createdTourID == null || LeadsPage.createdTourID.isEmpty()
							|| LeadsPage.createdTourID == "") {
						/*fieldDataEnter(globalSearchEdit, CommunityLeadsPage.createdTourId);
						clickElementBy(globalSearchEdit);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(globalSearchEdit).sendKeys(Keys.ENTER);*/
						waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, "ExplicitWaitLongestTime");
						System.out.println(LeadsPage.objMap.get(testData.get("AutomationID")));
						fieldDataEnter(ownerSearchBoxEdit, /*LeadsPage.createdTourID*/LeadsPage.objMap.get(testData.get("AutomationID")));
						// waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(ownerSearchBoxEdit);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);

					} else {
						waitUntilElementVisibleBy(driver, ownerSearchBoxEdit, "ExplicitWaitLongestTime");
						fieldDataEnter(ownerSearchBoxEdit, /*LeadsPage.createdTourID*/LeadsPage.objMap.get(testData.get("AutomationID")));
						// waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(ownerSearchBoxEdit);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
					}
				} else {
					fieldDataEnter(ownerSearchBoxEdit, newTourId);
					driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
				}
			} else {
				fieldDataEnter(ownerSearchBoxEdit, testData.get("TourNo"));
				driver.findElement(ownerSearchBoxEdit).sendKeys(Keys.ENTER);
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// WebElement
			// seacrhResOwnerLink=driver.findElement(By.xpath("//th/span/a[contains(text(),'"+testData.get("TourNo")+"')
			// or contains(text(),'"+newTourId+"')]"));
			// seacrhResOwnerLink.click();
		}
	}

	// public void getlatestTour(){
	//
	// waitUntilElementVisibleBy(driver, latestTour, "ExplicitWaitLongestTime");
	// newTourId = driver.findElement(latestTour).getText();
	// System.out.println(newTourId);
	// driver.findElement(latestTour).click();
	// }

	public void assignSaleRep() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, quickSearchEdit, "ExplicitWaitLongestTime");
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitUntilElementVisibleBy(driver, quickSearchEdit, "ExplicitWaitLongestTime");
		fieldDataEnter(quickSearchEdit, newTourId);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + newTourId + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in Sales Rep Assignment Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Tour has NOT been displayed in Sales Rep Assignment Page");
		}
		clickElementBy(guestSelectionChkBx);
		clickElementBy(checkInBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(checkedInLink);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(selectGuestCheckedIn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(assignRepsBtn);
		/*
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(onTourLink);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(completeTourBtn); waitUntilElementVisibleBy(driver,
		 * saveDispositionBtn, "ExplicitWaitLongestTime"); fieldDataEnter(qaRepEdit,
		 * testData.get("AgentName"));
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * driver.findElement(By.xpath("//span[text()='"+
		 * testData.get("AgentName")+"']")).click(); new
		 * Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText(
		 * "Yes"); new
		 * Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(
		 * testData.get("DispositionStatus"));
		 * clickElementBy(saveDispositionBtn);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 */
	}

	public By saleRep = By.xpath("//table[@id='sortable1']/tbody/tr[2]/td[1]");
	public By addRight = By.xpath("//div[@class='add']");
	public By moveUp = By.xpath("//span[@class='up']");
	public String saleRepname;

	public void salesRepRotation() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, "ExplicitWaitLongestTime");
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		saleRepname = driver.findElement(saleRep).getText();
		driver.findElement(saleRep).click();
		driver.findElement(addRight).click();
		if (verifyObjectDisplayed(
				By.xpath("//table[@id='sortable2']/tbody/tr/td[contains(text(),'" + saleRepname + "')]"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "salesRepRotation", Status.PASS,
					"sale rep has been toggeled");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "salesRepRotation", Status.FAIL,
					"sale rep has been not been toggeled");
		}
		driver.findElement(By.xpath("//table[@id='sortable2']/tbody/tr/td[contains(text(),'" + saleRepname + "')]"))
				.click();
		driver.findElement(moveUp).click();
		tcConfig.updateTestReporter("TourRecordsPage", "salesRepRotation", Status.PASS,
				"sale rep has been toggeled");
	}

	String agentNameinput= testData.get("AgentName");
	public void dispositionTour() {
		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, "ExplicitWaitLongestTime");
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		//newTourId = LeadsPage.createdTourID;
		fieldDataEnter(quickSearchEdit, LeadsPage.objMap.get(testData.get("AutomationID")));
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + LeadsPage.objMap.get(testData.get("AutomationID")) + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in Sales Rep Assignment Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Tour has NOT been displayed in Sales Rep Assignment Page");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(guestSelectionChkBx);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(checkInBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		clickElementJSWithWait(skipBtn);
		/*List<WebElement> popup = driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
		if (popup.size() > 0) {
			popup.get(0).click();
		}*/
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(checkedInLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(selectGuestCheckedIn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(assignRepsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(onTourLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(completeTourBtn);
		waitUntilElementVisibleBy(driver, saveDispositionBtn, "ExplicitWaitLongestTime");
		
		driver.findElement(qaRepEdit).sendKeys(agentNameinput+ Keys.ENTER);
		/*fieldDataEnter(qaRepEdit, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//span[text()='" + testData.get("AgentName") + "']")).click();*/
		new Select(driver.findElement(selectDidTourdrpDwn)).selectByVisibleText("Yes");
		new Select(driver.findElement(dispositionDrpdwn)).selectByVisibleText(testData.get("DispositionStatus"));
		clickElementJSWithWait(saveDispositionBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	public void checkinTour() {
		waitUntilElementVisibleBy(driver, salesStoreDrpDwn, "ExplicitWaitLongestTime");
		new Select(driver.findElement(salesStoreDrpDwn)).selectByVisibleText(testData.get("SalesStore"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		newTourId = LeadsPage.createdTourID;
		fieldDataEnter(quickSearchEdit, newTourId);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(By.xpath("//div[@title='Tour Record Number']/a[text()='" + newTourId + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in Sales Rep Assignment Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.FAIL,
					"Tour has not been displayed in Sales Rep Assignment Page");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(guestSelectionChkBx);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(checkInBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> popup = driver.findElements(By.xpath("//button[contains(text(),'Skip')]"));
		if (popup.size() > 0) {
			popup.get(0).click();
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(checkedInLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(selectGuestCheckedIn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(assignRepsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(onTourLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		/*if(driver.findElement(By.xpath("(//a[text()='"+LeadsPage.createdTourID+"'])[2]")).isDisplayed())
		{
			tcConfig.updateTestReporter("TourRecordsPage", "dispositionTour", Status.PASS,
					"Tour has been displayed in On tour tab Page");
		}*/
	}

	public void validateTourStatus() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(
				By.xpath("//table/tbody/tr/td/span/span[text()='" + testData.get("TourStatus") + "']"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatus", Status.PASS,
					"Tour status has been changed To : " + testData.get("TourStatus"));
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatus", Status.FAIL,
					"tour status not changed to : " + testData.get("TourStatus"));
		}
	}

	public void validateTourStatusOnGrid() {
		String tourStatus = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilElementVisibleBy(driver,
		// By.xpath("//table/tbody/tr[1]/th/span/a[@title='"+LeadsPage.createdTourID+"']"),
		// "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed((By.xpath("(//table/tbody/tr[1]/td[4]/span/span)[2]")))) {
			tourStatus = driver.findElement(By.xpath("(//table/tbody/tr[1]/td[4]/span/span)[2]")).getText().trim();
		} else if (verifyObjectDisplayed(By.xpath("//table/tbody/tr[1]/td[4]/span/span"))) {
			tourStatus = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]/span/span")).getText().trim();
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.FAIL,
					"tour is not retrieved");

		}

		if (tourStatus.equalsIgnoreCase(testData.get("TourStatus"))) {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.PASS,
					"Tour status has been changed To : " + testData.get("TourStatus"));
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.FAIL,
					"tour status not changed to : " + testData.get("TourStatus"));
		}
	}
	
	public void validateRescheduleTour() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		String strCurrentDate = dateFormat.format(cal.getTime());
		if (verifyObjectDisplayed((By.xpath("//table/tbody/tr[1]/td[2]/span/span")))) {
			String strTourApp[] = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/span/span")).getText()
					.split(" ");
			if (strCurrentDate.equals(strTourApp[0])) {
				tcConfig.updateTestReporter("TourRecordsPage", "validateRescheduleTour", Status.PASS,
						"Tour reschedule has been successfully done");
			}else{
				tcConfig.updateTestReporter("TourRecordsPage", "validateRescheduleTour", Status.FAIL,
						"Tour reschedule has not been successfully done");
			}
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.FAIL,
					"tour is not retrieved");

		}
	}
	
	public void getScheduleTime() {
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String strDateFormat = "HH:mm a";
	    SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		if (verifyObjectDisplayed((By.xpath("//table/tbody/tr[1]/td[2]/span/span")))) {
			strTourAppTime = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/span/span")).getText().substring(11)
					.trim();
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.PASS,
					"Tour wavetime is displayed successfully " + strTourAppTime);
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.FAIL,
					"tour is not retrieved");

		}

	}

	public void deleteTour() {
		clickElementBy(iconDownKey);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(deleteTourBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, reDeleteBtn, "ExplicitWaitLongestTime");
		clickElementBy(reDeleteBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(deleteMsgPopUp)) {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.PASS,
					"Tour ID : " + LeadsPage.createdTourID + " has been deleted successfully");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "validateTourStatusOnGrid", Status.FAIL,
					"Failed to delete the Tour ID : " + LeadsPage.createdTourID);
		}

	}

	public void bookAppointmentforFutureDate() {
		waitUntilElementVisibleBy(driver, bookOptionSelect, "ExplicitWaitLongestTime");
		clickElementBy(bookOptionSelect);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("NoSalesStore").equalsIgnoreCase("Y")) {
			clickElementBy(removeSalesStoreIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * clickElementBy(removeSalesStoreIcon);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */
			try {
				if (driver.findElement(getAppointmentsBtn).isEnabled() == false || driver.findElement(getAppointmentsBtn).getAttribute("disabled") == "true") {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
							"User is unable to book appointment if Sales store is blank");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
							"User is able to book appointment if Sales store is blank");
				}
			} catch (NoSuchElementException e) {
				System.out.println("catched");
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is able to book appointment if Sales store is blank");
			}
		} else {

			waitUntilElementVisibleBy(driver, removeSalesStoreIcon, "ExplicitWaitLongestTime");
			clickElementBy(removeSalesStoreIcon);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			fieldDataEnter(reSalesStoretxt, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]"),
					"ExplicitWaitLongestTime");
			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("SalesStore") + "')][1]")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, removeAppointmentLocIcon, "ExplicitWaitLongestTime");
			clickElementBy(removeAppointmentLocIcon);
			fieldDataEnter(reBookLocation, testData.get("AppointmentLoc"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver,
					By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"), "ExplicitWaitLongestTime");

			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("AppointmentLoc") + "')][1]"))
					.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(drpBrand)).selectByVisibleText(testData.get("Brand"));
			;
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
				new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*
				 * new
				 * Select(driver.findElement(drpMarketingcamp)).selectByIndex(1)
				 * ; waitForSometime(tcConfig.getConfig().get("MedWait"));
				 */
			}

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DATE, 2);
			String newDate = dateFormat.format(cal.getTime());
			// Select the future date
			String dateTime = newDate;
			String date_MM_dd_yyyy[] = (dateTime.split("/"));
			String year = date_MM_dd_yyyy[2];
			// button to open calendar
			waitUntilElementVisibleBy(driver, datePicker, 240);
			hoverOnElement(driver.findElement(datePicker));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			int yearDiff = Integer.parseInt(date_MM_dd_yyyy[2]) - Calendar.getInstance().get(Calendar.YEAR);
			int MonthDiff = Integer.parseInt(date_MM_dd_yyyy[0]) - Calendar.getInstance().get(Calendar.MONTH);
			if (yearDiff != 0) {
				if (yearDiff > 0) {

					Select objYear = new Select(driver.findElement(selectYear));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					objYear.selectByValue(year);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			}
			if (MonthDiff != 0) {
				if (MonthDiff > 0) {
					for (int i = 1; i <= MonthDiff - 1; i++) {
						driver.findElement(nextMonthLink).click();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
				}
			}

			List<WebElement> list_AllDateToBook = driver.findElements(By.xpath(
					"//table[@class='calGrid']//td[not(contains(@class,'slds-disabled-text uiDayInMonthCell')) and not(contains(@role,'button')) and not(contains(@class,'slds-disabled-text slds-is-today uiDayInMonthCell'))]/span"));

			for (WebElement webElement : list_AllDateToBook) {
				if (webElement.getText().equalsIgnoreCase(date_MM_dd_yyyy[1])) {
					webElement.click();
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
							"New appointment has been created with Future Date");
					break;
				}
			}

			waitUntilElementVisibleBy(driver, getAppointmentsBtn, "ExplicitWaitLongestTime");
			mouseoverAndClick(getAppointmentsBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lblAvailableSlot, "ExplicitWaitLongestTime");
			driver.findElement(lblAvailableSlot).click();
			waitUntilElementVisibleBy(driver, yesBtn, "ExplicitWaitLongestTime");
			clickElementBy(yesBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, txtAppointmentConfirm, "ExplicitWaitLongestTime");
			if (verifyObjectDisplayed(txtAppointmentConfirm)) {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
						"New appointment has been created");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is unable to book an appointment");
			}
			waitUntilElementVisibleBy(driver, okBtn, "ExplicitWaitLongestTime");
			clickElementBy(okBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

	}
	
	public void rescheduleAppointment() {
		try{
			waitUntilElementVisibleBy(driver, bookOptionSelect, "ExplicitWaitLongestTime");
			clickElementBy(bookOptionSelect);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
				new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				 new Select(driver.findElement(drpMarketingcamp)).selectByIndex(1);
				 waitForSometime(tcConfig.getConfig().get("MedWait"));
				 
			}
			waitUntilElementVisibleBy(driver, datePicker, 240);
			hoverOnElement(driver.findElement(datePicker));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(todayLink).click();
			waitUntilElementVisibleBy(driver, getAppointmentsBtn, "ExplicitWaitLongestTime");
			mouseoverAndClick(getAppointmentsBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lblAvailableSlot, "ExplicitWaitLongestTime");
			driver.findElement(lblAvailableSlot).click();
			waitUntilElementVisibleBy(driver, yesBtn, "ExplicitWaitLongestTime");
			clickElementBy(yesBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, txtAppointmentConfirm, "ExplicitWaitLongestTime");
			if (verifyObjectDisplayed(txtAppointmentConfirm)) {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
						"New appointment has been created");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is unable to book an appointment");
			}
			waitUntilElementVisibleBy(driver, okBtn, "ExplicitWaitLongestTime");
			clickElementBy(okBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
					"User is unable to book an appointment"+e.getMessage());
		}
		

	}

}
