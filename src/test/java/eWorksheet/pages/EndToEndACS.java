package eWorksheet.pages;

import java.io.IOException;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class EndToEndACS extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(EndToEndACS.class);
	ReadConfigFile config = new ReadConfigFile();

	public EndToEndACS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String scoreBand;
	public static String scoreType;
	public static String contractNum;
	protected By username = By.xpath("//input[@id='fldUsername']");
	protected By password = By.xpath("//input[@id='fldPassword']");
	protected By loginButton = By.id("login-btn");
	protected By logOut = By.xpath("//a[text()='Logout']");
	protected By logoutConf = By.xpath("//div[@class='msg inform' and contains(text(),'You have been logged off. ')]");
	protected By roleSelectionTab = By.xpath("//li[contains(text(),'Role Selection')]");
	protected By serviceEntityDrpDD = By.xpath("//select[@name='serviceEntity']");
	protected By nextBtn = By.xpath("//button[@id='next-btn']");
	protected By tourIdTxt = By.xpath("//input[@name='id']");
	protected By retCustBtn = By.xpath("//button[@id='getcustomer-btn']");
	protected By retCustomerBtn = By.xpath("//button[text()=' Retrieve Customer' and @type='submit']");
	protected By reviewAndScore = By.xpath("(//td[@class='center'])[1]");
	protected By ssn1 = By.xpath("//input[@name='ssn1']");
	protected By ssn2 = By.xpath("//input[@name='ssn2']");
	protected By ssn3 = By.xpath("//input[@name='ssn3']");
	protected By fName = By.xpath("//input[@name='firstname']");
	protected By lName = By.xpath("//input[@name='lastname']");
	protected By dobMonth = By.xpath("//select[@name='dob_month']");
	protected By dobDay = By.xpath("//select[@name='dob_day']");
	protected By dobYear = By.xpath("//select[@name='dob_year']");
	protected By gender = By.xpath("//select[@name='gender']");
	protected By streetnum = By.xpath("//input[@name='address_no']");
	protected By streettype = By.xpath("//select[@name='address_type']");
	protected By streetname = By.xpath("//input[@name='address_name']");
	protected By city = By.xpath("//input[@name='address_city']");
	protected By state = By.xpath("//select[@name='address_state']");
	protected By postcode = By.xpath("//input[@name='address_postal_code']");
	protected By country = By.xpath("//select[@name='address_country']");
	protected By homephone = By.xpath("//input[@name='home_phone']");
	protected By mobilephone = By.xpath("//input[@name='mobile_phone']");
	protected By annualIncome = By.xpath("//input[@name='totalIncome']");
	protected By rentRequired = By.xpath("//input[@name='rent']");
	protected By getScoreBtn = By.xpath("//button[@name='scoreBtn']");
	protected By scoreband = By.xpath("//div[@id='credit_band']");
	protected By scoretype = By.xpath("//div[@id='credit_band']");
	protected By reviewBtn = By.xpath("//input[@id='reviewBtn']");
	protected By useOriginalBtn = By.xpath("//input[@value='Score Original Customer']");
	protected By locationName = By.xpath("//select[@name='locationName']");
	protected By crsID = By.xpath("//input[@name='tourid']");
	protected By submitAppBtn = By.xpath("//button[text()=' Submit Application' and @id='rescore-btn']");
	protected By finalSubmitBtn = By.xpath("//button[text()=' Submit Application' and @type='submit']");
	protected By getScoreText = By.xpath("//form[@id='formCustomerDetail']//h1");
	protected By zipCode = By.xpath("//input[@name='zipcode']");

	protected void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, 10);
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("ACS_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	protected void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("ACS_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	public void launchAcs(String strBrowserInUse, String strBrowserName, String strModel,
			String strPlatformName, String strPlatformVR, String strBrowserVersion,String strLocation) {

		this.driver = checkAndInitBrowser(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,
				strBrowserVersion,strLocation);
		String url = testData.get("ACS_Url").trim();
		driver.navigate().to(url);
		driver.manage().window().maximize();

	}

	public void loginToAcs() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String ACS_userid = testData.get("SalesManagerID");
		String ACS_password = testData.get("SalesManagerPassword");

		setUserName(ACS_userid);
		setPassword(ACS_password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(loginButton).click();
		checkLoadingSpinnerEWS();
		tcConfig.updateTestReporter("ACS_LoginPage", "loginToACS", Status.PASS, "Log in button clicked");

	}

	public void logoutfromAcs() throws Exception {

		try {

			waitUntilElementVisibleBy(driver, logOut, "ExplicitWaitLongestTime");
			driver.findElement(logOut).click();

			waitUntilElementVisibleBy(driver, logoutConf, 10);
			if (verifyObjectDisplayed(logoutConf)) {
				tcConfig.updateTestReporter("ACSLoginPage", "ACSLogout", Status.PASS, "Log out Successful");
			} else {
				tcConfig.updateTestReporter("ACSLoginPage", "ACSLogout", Status.FAIL, "Log Off unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("ACSLoginPage", "ACSLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	public void homePageValidationAcs() {
		waitUntilElementVisibleBy(driver, roleSelectionTab, "ExplicitWaitLongestTime");

		tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Homepage Navigated");

		if (verifyObjectDisplayed(roleSelectionTab)) {
			clickElementBy(roleSelectionTab);
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Leads Tab clicked");
		} else {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Leads Tab not present");
		}

	}

	public void hardScoringfromAcs() {
		waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
		clickElementBy(serviceEntityDrpDD);
		waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
		new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntity"));
		waitUntilElementVisibleBy(driver, nextBtn, "ExplicitWaitLongestTime");
		clickElementBy(nextBtn);
		waitUntilElementVisibleBy(driver, tourIdTxt, "ExplicitWaitLongestTime");

		// String createdTourid= testData.get("createdTourID");
		String createdTourid = LeadsPage.createdTourID;
		sendKeysBy(tourIdTxt, createdTourid);
		waitUntilElementVisibleBy(driver, retCustBtn, "ExplicitWaitLongestTime");
		clickElementBy(retCustBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(reviewAndScore)) {
			tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
					"User is navigated to Role Selection Page");
			clickElementBy(reviewAndScore);

			tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
					"review And Score button clicked");

		} else {
			tcConfig.updateTestReporter("RoleSelectionPage", "Role Selection", Status.PASS,
					"User is navigated to Role Selection Page");
		}

		waitUntilElementVisibleBy(driver, getScoreBtn, "ExplicitWaitLongestTime");
		String SSSN = testData.get("FirstCusSSN");

		fieldDataEnter(ssn1, SSSN.substring(0, 3));
		fieldDataEnter(ssn2, SSSN.substring(3, 5));
		fieldDataEnter(ssn3, SSSN.substring(5, 9));
		fieldDataEnter(fName, testData.get("FirstName"));
		fieldDataEnter(lName, testData.get("LastName"));

		String dob = testData.get("DOB");
		String DobMonth = "", DobDay = "";
		String DobYear = "";
		String arr[] = dob.split("/");

		// MonthFormation
		if (arr[0].length() == 1) {
			DobMonth = "0" + arr[0];
		} else {
			DobMonth = arr[0];
		}

		// DayFormation
		if (arr[1].length() == 1) {
			DobDay = "0" + arr[1];
		} else {
			DobDay = arr[1];
		}

		// YearFormation

		DobYear = arr[2];

		clickElementBy(dobMonth);
		new Select(driver.findElement(dobMonth)).selectByValue(DobMonth);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(dobDay);
		new Select(driver.findElement(dobDay)).selectByVisibleText(DobDay);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(dobYear);
		new Select(driver.findElement(dobYear)).selectByVisibleText(DobYear);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(gender);
		new Select(driver.findElement(gender)).selectByVisibleText(testData.get("Gender"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(streetnum, testData.get("StreetNum"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(streetname, testData.get("StreetName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(streettype);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(streettype)).selectByVisibleText(testData.get("StreetType"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(city, testData.get("City"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(state);
		new Select(driver.findElement(state)).selectByVisibleText(testData.get("State").toUpperCase());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(postcode, testData.get("ZIP"));
		clickElementBy(country);
		new Select(driver.findElement(country)).selectByValue(testData.get("Country"));
		fieldDataEnter(homephone, testData.get("FirstCusHomePhone"));
		fieldDataEnter(mobilephone, testData.get("FirstCusHomePhone"));
		fieldDataEnter(annualIncome, testData.get("AnnualIncome"));
		fieldDataEnter(rentRequired, testData.get("RentRequired"));

		String winHandleBefore = driver.getWindowHandle();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(getScoreBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (verifyObjectDisplayed(useOriginalBtn)) {
			clickElementBy(useOriginalBtn);
		} else {
			clickElementBy(reviewBtn);
		}

		// driver.close();
		driver.switchTo().window(winHandleBefore);

		scoreBand = driver.findElement(scoreband).getText().replace("Credit Band\n", "");
		// scoreType = driver.findElement(scoretype).getText().trim();
		tcConfig.updateTestReporter("ScoreCreationPage", "ScoreCreation", Status.PASS, "Score Generated");
	}

	public void softScoringfromAcs() {
		waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
		clickElementBy(serviceEntityDrpDD);
		waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
		new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntityname"));
		waitUntilElementVisibleBy(driver, nextBtn, "ExplicitWaitLongestTime");
		clickElementBy(nextBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(By.xpath("//select[@name='siteState']"))).selectByValue("FL");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(By.xpath("//select[@name='siteCity']"))).selectByValue("DAYTONA");
		waitUntilElementVisibleBy(driver, locationName, "ExplicitWaitLongestTime");
		new Select(driver.findElement(locationName)).selectByIndex(2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(By.xpath("//select[@name='tourSite']"))).selectByVisibleText("WYNDHAM DAYTONA");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(nextBtn);
		waitUntilElementVisibleBy(driver, crsID, "ExplicitWaitLongestTime");
		String createdTourid = LeadsPage.createdTourID;
		sendKeysBy(crsID, createdTourid);
		waitUntilElementVisibleBy(driver, retCustomerBtn, "ExplicitWaitLongestTime");
		clickElementBy(retCustomerBtn);
		waitUntilElementVisibleBy(driver, submitAppBtn, "ExplicitWaitLongestTime");
		clickElementBy(submitAppBtn);
		waitUntilElementVisibleBy(driver, zipCode, "ExplicitWaitLongestTime");
		driver.findElement(zipCode).clear();
		sendKeysBy(zipCode, testData.get("ZIP"));
		waitUntilElementVisibleBy(driver, finalSubmitBtn, "ExplicitWaitLongestTime");
		clickElementBy(finalSubmitBtn);
		waitUntilElementVisibleBy(driver, getScoreText, "ExplicitWaitLongestTime");
		String strGetScore = driver.findElement(getScoreText).getText().trim();
		if (strGetScore.equals("A")) {
			tcConfig.updateTestReporter("Customer Application Results Page", "softScoringfromAcs", Status.PASS,
					"Scoring " + strGetScore + " has been done successfully");
		}

	}

	protected By homeBtn = By.xpath("//img[@title='Home']");

	public void clickToHome() {
		waitUntilElementVisibleBy(driver, homeBtn, "ExplicitWaitLongestTime");
		driver.findElement(homeBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*public void updateScoreToSheet(String testName)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		FunctionalComponents.writeDataInExcel(testName, "ScoreBand", scoreBand);
		// FunctionalComponents.writeDataInExcel(testName,"ScoreType",scoreType);
	}*/

	public void retrieveWVRTourDetails() {
		try {
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			clickElementBy(serviceEntityDrpDD);
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntity"));
			waitUntilElementVisibleBy(driver, nextBtn, "ExplicitWaitLongestTime");
			clickElementBy(nextBtn);
			waitUntilElementVisibleBy(driver, tourIdTxt, "ExplicitWaitLongestTime");
			String createdTourid = testData.get("TourID");
			sendKeysBy(tourIdTxt, createdTourid);
			waitUntilElementVisibleBy(driver, retCustBtn, "ExplicitWaitLongestTime");
			clickElementBy(retCustBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(reviewAndScore)) {
				tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
						"User is navigated to Role Selection Page");
				clickElementBy(reviewAndScore);

				tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
						"review And Score button clicked");

			} else {
				
				tcConfig.updateTestReporter("RoleSelectionPage", "Role Selection", Status.PASS,
						"User is navigated to Role Selection Page");
			}
			waitUntilElementVisibleBy(driver, scoreband, "ExplicitWaitLongestTime");
			scoreBand = driver.findElement(scoreband).getText().replace("Credit Band\n", "");
			if (scoreBand.equals("C (0)")) {
				tcConfig.updateTestReporter("retrieveWVRTourDetails", "retrieveWVRTourDetails", Status.PASS,
						"Credit band is " + scoreBand);
			} else {
				tcConfig.updateTestReporter("retrieveWVRTourDetails", "retrieveWVRTourDetails", Status.FAIL,
						"Credit band is not generated");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("retrieveWVRTourDetails", "retrieveWVRTourDetails", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}
	
	
	
	public void retrieveWBWTourDetails() {
		try {
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			clickElementBy(serviceEntityDrpDD);
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntity"));
			waitUntilElementVisibleBy(driver, nextBtn, "ExplicitWaitLongestTime");
			clickElementBy(nextBtn);
			waitUntilElementVisibleBy(driver, tourIdTxt, "ExplicitWaitLongestTime");
			String createdTourid = testData.get("TourID");
			sendKeysBy(tourIdTxt, createdTourid);
			waitUntilElementVisibleBy(driver, retCustBtn, "ExplicitWaitLongestTime");
			clickElementBy(retCustBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(reviewAndScore)) {
				tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
						"User is navigated to Role Selection Page");
				clickElementBy(reviewAndScore);

				tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
						"review And Score button clicked");

			} else if(verifyElementDisplayed(driver.findElement(scoreband))){
				
				tcConfig.updateTestReporter("RoleSelectionPage", "Role Selection", Status.PASS,
						"User is navigated to Verify & Score Customer Screen");
			}
			else{
				waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
				clickElementBy(serviceEntityDrpDD);
				waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
				new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntity"));
				waitUntilElementVisibleBy(driver, retCustBtn, "ExplicitWaitLongestTime");
				clickElementBy(retCustBtn);
			}
			waitUntilElementVisibleBy(driver, scoreband, "ExplicitWaitLongestTime");
			scoreBand = driver.findElement(scoreband).getText().replace("Credit Band\n", "");
			System.out.println("Score Band in ACS  "+scoreBand);
			if (scoreBand.equals("C (0)")) {
				tcConfig.updateTestReporter("retrieveWBWTourDetails", "retrieveWBWTourDetails", Status.PASS,
						"Credit band is " + scoreBand);
			} else {
				tcConfig.updateTestReporter("retrieveWBWTourDetails", "retrieveWBWTourDetails", Status.FAIL,
						"Credit band is not generated");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("retrieveWBWTourDetails", "retrieveWBWTourDetails", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	protected By searchLOCLink = By.xpath("//a[text()='Search LOC Applications']");
	protected By tourSearchTxt = By.xpath("//input[@id='appSearch-tourNumber']");
	protected By searchApplicationBtn = By.xpath("//button[@id='continueBtn']");
	public By acsID = By.xpath("//input[@id='appSearch-appID']");
	/*protected By linkACSID = By
			.xpath("//button[@name='creditApplicationID' and text()='" + testData.get("ACSId") + "']");*/
	protected By linkACSID = By
			.xpath("//button[@name='creditApplicationID']");
	protected By remBalanceTxt = By.xpath("//input[@id='appSearch-remainingBal']");

	public void searchLOCAppDetails() {
		try {
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			clickElementBy(serviceEntityDrpDD);
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntity"));
			waitUntilElementVisibleBy(driver, nextBtn, "ExplicitWaitLongestTime");
			clickElementBy(nextBtn);
			waitUntilElementVisibleBy(driver, searchLOCLink, "ExplicitWaitLongestTime");
			driver.findElement(searchLOCLink).click();
			waitUntilElementVisibleBy(driver, tourSearchTxt, "ExplicitWaitLongestTime");
			fieldDataEnter(tourSearchTxt, testData.get("TourID"));
			waitUntilElementVisibleBy(driver, searchApplicationBtn, "ExplicitWaitLongestTime");
			driver.findElement(searchApplicationBtn).click();
			waitUntilElementVisibleBy(driver, remBalanceTxt, "ExplicitWaitLongestTime");
			String strCreditLimit = driver.findElement(remBalanceTxt).getAttribute("value");
			if (AllTourPage.strVCCAppAmt.equals(strCreditLimit)) {
				tcConfig.updateTestReporter("searchLOCAppDetails", "searchLOCAppDetails", Status.PASS,
						"Credit Limit is displayed successfully" + strCreditLimit);
			} else {
				tcConfig.updateTestReporter("searchLOCAppDetails", "searchLOCAppDetails", Status.FAIL,
						"Credit Limit is not displayed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("searchLOCAppDetails", "searchLOCAppDetails", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}
	
	protected By creditLimitTxt = By.xpath("//input[@id='appSearch-creditLimit']");
	public void searchLOCAppDetailsforRewards() {
		try {
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			clickElementBy(serviceEntityDrpDD);
			waitUntilElementVisibleBy(driver, serviceEntityDrpDD, "ExplicitWaitLongestTime");
			new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntity"));
			waitUntilElementVisibleBy(driver, nextBtn, "ExplicitWaitLongestTime");
			clickElementBy(nextBtn);
			waitUntilElementVisibleBy(driver, searchLOCLink, "ExplicitWaitLongestTime");
			driver.findElement(searchLOCLink).click();
			waitUntilElementVisibleBy(driver, tourSearchTxt, "ExplicitWaitLongestTime");
			fieldDataEnter(tourSearchTxt, testData.get("TourID"));
			waitUntilElementVisibleBy(driver, searchApplicationBtn, "ExplicitWaitLongestTime");
			driver.findElement(searchApplicationBtn).click();
			/*waitUntilElementVisibleBy(driver, linkACSID, "ExplicitWaitLongestTime");
			driver.findElement(linkACSID).click();*/
			waitUntilElementVisibleBy(driver, creditLimitTxt, "ExplicitWaitLongestTime");
			String strCreditLimit = driver.findElement(creditLimitTxt).getAttribute("value");
			if (AllTourPage.strRewardsAppAmt.equals(strCreditLimit)) {
				tcConfig.updateTestReporter("searchLOCAppDetails", "searchLOCAppDetailsforRewards", Status.PASS,
						"Rewards VISA is displayed successfully" + strCreditLimit);
			} else {
				tcConfig.updateTestReporter("searchLOCAppDetails", "searchLOCAppDetailsforRewards", Status.FAIL,
						"Rewards VISA is not displayed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("searchLOCAppDetails", "searchLOCAppDetailsforRewards", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

}
