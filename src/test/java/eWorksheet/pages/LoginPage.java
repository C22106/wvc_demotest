package eWorksheet.pages;

import java.awt.Robot;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class LoginPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(LoginPage.class);

	public LoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By txtUserName = By.id("okta-signin-username");// txtUserName
	protected By txtPassword = By.id("okta-signin-password");// txtPassword
	protected By loginButton = By.xpath("//input[@class='button button-primary' and @id='okta-signin-submit']");
	protected By eWorksheetlink = By.xpath("(//img[contains(@alt,'Navigator Test 1')])[1]");
	protected By eWorksheetStackAlink = By.xpath("(//img[contains(@alt,'Navigator Test2')])[1]");
	protected By MenuIcon = By.xpath("//li[@class='nav-item ews-menu-item']/img");
	/* protected By logOut = By.xpath("//button[text()='Logout']"); */
	@FindBy(xpath = "//button[text()='Logout']")
	protected WebElement logOut;
	protected By homeLogo = By.xpath("//img[@alt='eworksheet-logo']");
	protected By OktaLogOutMenu = By.xpath("(//span[@class='icon-dm'])[2]");

	@FindBy(xpath = "//img[@title='User']")
	WebElement viewProfile;
	protected By username = By.xpath("//input[@id='username']");
	protected By password = By.xpath("//input[@id='password']");
	protected By salesLoginButton = By.id("Login");
	protected By saleslogOut = By.xpath("//a[text()='Log Out']");
	protected By signInBtn = By.id("okta-signin-submit");
	protected By languageSelect = By.xpath("//span[@class='header-links']");
	protected By salesSelect = By.id("salesline");
	protected By usernameeCredit = By.name("username");
	protected By passwordeCredit = By.name("password");
	protected By signInBtneCredit = By.id("okta-signin-submit");
	protected By userClick = By.xpath("//span[@class='agent--username agent--dropdown']");
	protected By searchApp = By.xpath("//input[@placeholder='Launch App']");
	protected By eCreditLogOut = By.xpath("//li/a[text()='Logout']");
	protected By eCreditLink = By.xpath("//img[@alt='Graphic Link eCredit - QA']");
	protected By oktaApplications = By.xpath("//p[contains(@class,'app-button-name')]");

	protected void setUserName(String strUserName) throws Exception {
		try {
			waitUntilElementVisibleBy(driver, txtUserName, "ExplicitWaitLongestTime");
			driver.findElement(txtUserName).sendKeys(strUserName);
			tcConfig.updateTestReporter("eWorksheetLoginPage", "setUserName", Status.PASS,
					"Username is entered for Login");
		} catch (Exception e) {
			tcConfig.updateTestReporter("eWorksheetLoginPage", "setUserName", Status.FAIL,
					"Username is field data entry failed");
		}
	}

	protected void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		waitUntilElementVisibleBy(driver, txtPassword, "ExplicitWaitLongestTime");
		driver.findElement(txtPassword).sendKeys(dString);
		tcConfig.updateTestReporter("eWorksheetLoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	public void launchApplication(String strBrowserInUse, String strBrowserName, String strModel,
			String strPlatformName, String strPlatformVR, String strBrowserVersion,String strLocation) {
		this.driver = checkAndSetBrowser(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,
				strBrowserVersion,strLocation);
		getBrowserName();
		navigateToURL(testData.get("eWorksheetURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtUserName, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(txtUserName), "Failed to Launch Application");
		tcConfig.updateTestReporter("EWSLoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}
	
	
	
	

	protected void setUserNameSales(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, "ExplicitWaitLongestTime");
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("SF_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	protected void setPasswordSales(String strPassword) throws Exception {

		// look for th prob;
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		waitUntilElementVisibleBy(driver, password, "ExplicitWaitLongestTime");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("SF_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	public void launchSalesForceApp(String strBrowserInUse, String strBrowserName, String strModel,
			String strPlatformName, String strPlatformVR, String strBrowserVersion,String strLocation) {
		try {
			this.driver = checkAndInitBrowser(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,
					strBrowserVersion,strLocation);
			String url = testData.get("SalesforceURL");
			driver.navigate().to(url);
			driver.manage().window().maximize();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void loginToSalesForce() throws Exception {

		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = testData.get("SalesUserId");
		String password = testData.get("SalesPassword");

		setUserNameSales(userid);
		setPasswordSales(password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, salesLoginButton, "ExplicitWaitLongestTime");
		driver.findElement(salesLoginButton).click();
		// checkLoadingSpinnerEWS();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");

	}

	

	public void loginToOKTA() throws Exception {
		String strUserId = testData.get("SalesManagerID");
		String strPassword = testData.get("SalesManagerPassword");
		try {
			if (getList(oktaApplications).size() > 0) {
				log.info("Already Logged In");
			} else {
				setUserName(strUserId);
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				setPassword(strPassword);
				waitUntilElementVisibleBy(driver, loginButton, "ExplicitWaitLongestTime");
				driver.findElement(loginButton).click();
				// checkLoadingSpinnerEWS();
				tcConfig.updateTestReporter("eWorksheetLoginPage", "loginToeWorksheetApp", Status.PASS,
						"Log in button clicked");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	

	public void loginToeWorksheet() throws Exception {
		if(tcConfig.getConfig().get("ENVIRONMENT").equalsIgnoreCase("STACK_B"))
		{
			waitUntilElementVisibleBy(driver, eWorksheetlink, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(eWorksheetlink))) {
				tcConfig.updateTestReporter("eWorksheetLoginPage", "clickToeWorksheet", Status.PASS,
						"Login in to Okta Preview page successfully");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(eWorksheetlink);
				tcConfig.updateTestReporter("eWorksheetLoginPage", "clickToeWorksheet", Status.PASS,
						"Clicked on eWorksheet link and User is successfully logged in");
			}
		}else{
			waitUntilElementVisibleBy(driver, eWorksheetlink, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(eWorksheetStackAlink))) {
				tcConfig.updateTestReporter("eWorksheetLoginPage", "clickToeWorksheet", Status.PASS,
						"Login in to Okta Preview page successfully");
				/* driver.findElement(eWorksheetlink).click(); */
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(eWorksheetStackAlink);
				tcConfig.updateTestReporter("eWorksheetLoginPage", "clickToeWorksheet", Status.PASS,
						"Clicked on eWorksheet link and User is successfully logged in");
			}
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String parentWinHandle = driver.getWindowHandle();
		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for (String handle : winHandles) {
			if (!handle.equals(parentWinHandle)) {
				driver.switchTo().window(handle);

			}
		}
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (verifyObjectDisplayed(homeLogo)) {
			System.out.println("App logged in");
			checkLoadingSpinnerEWS();
			tcConfig.updateTestReporter("eWorksheetLoginPage", "loginToeWorksheetApp", Status.PASS,
					"Clicked on eWorksheet link and User is successfully logged in");

		}
	}

	public void eWorksheetLogout() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, MenuIcon, "ExplicitWaitLongestTime");
		driver.findElement(MenuIcon).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", logOut);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(logOut)) {
			clickElementWithJS(logOut);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("LoginPage", "eWorksheetLogout", Status.PASS, "Log Off Successful");
		}
	}

	public void switchToTab() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tab.get(1));
			driver.close();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().window(tab.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			e.getMessage();
		}
	}

	// old dashboard
	protected By signOutLink = By.xpath("(//span[@class='icon-dm'])[2]//following::li[2]/a[@data-se='logout-link']");

	public void oktaLogout() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(OktaLogOutMenu);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(signOutLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("LoginPage", "oktaLogout", Status.PASS,
					"Log Off from Okta Preview Successful");

		} catch (Exception e) {
			tcConfig.updateTestReporter("LoginPage", "oktaLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}
	
	public void salesForceLogout() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			mouseoverAndClick(viewProfile);
			waitUntilElementVisibleBy(driver, saleslogOut, "ExplicitWaitLongestTime");
			driver.findElement(saleslogOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

}
