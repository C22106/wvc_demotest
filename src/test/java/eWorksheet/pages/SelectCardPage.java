package eWorksheet.pages;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SelectCardPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(SelectCardPage.class);

	public SelectCardPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String ApprovalAmount;
	protected By sliderSelect = By.xpath("//span[@class='slider round']");
	protected By vccSlider = By.xpath("//div[@data-type='vcc']//span[@class='slider round']");
	protected By financeSlider = By.xpath("//div[@data-type='finance']//span[@class='slider round']");

	protected By barclaysSlider = By.xpath("//div[@data-type='wrc']//span[@class='slider round']");
	protected By termsBarclays = By.xpath("//u[contains(.,'Customer Service')]");
	protected By nextBtn = By.xpath("//button[@class='btn btn--inv next_btn float-right']");
	protected By titleHeader = By.xpath("//h1[@class='title']");
	protected By ssnNumber = By.name("ssn");
	protected By viewTerms = By.xpath("//th[@class='reasons-table-header align-left' and contains(.,'Other')]");
	protected By signature = By.id("printSignature");
	protected By agreeBtn = By.xpath("//button[@class='btn btn--lgray-border agree']");
	protected By VCCApprovalAmount = By.xpath("//div[@class='amount-approved']");
	protected By WynLogo = By.xpath("//img[@class='financial-logo-WYND']");

	public void selectCardTypeFinancing() {

		waitUntilElementVisibleBy(driver, financeSlider, 20);

		if (verifyObjectDisplayed(financeSlider)) {

			clickElementBy(financeSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeOwnership", Status.PASS,
					"Ownership card selected");

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeOwnership", Status.FAIL,
					"Unable to select ownership card");
		}
	}

	public void selectCardTypeFinanceFirstCus() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String firstCusName = testData.get("FirstName") + " " + testData.get("LastName");

		WebElement finanace = driver
				.findElement(By.xpath("//div[@data-type='finance']//span[text()='" + firstCusName + "']/.."));

		waitUntilElementVisible(driver, finanace, 120);

		if (verifyElementDisplayed(finanace)) {

			clickElementWb(finanace);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeFinancingPrimary", Status.PASS,
					"Finanace card for primary customer selected");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeFinancingPrimary", Status.FAIL,
					"Finanace card Slider is not present");
		}

	}

	public void selectCardTypeFinancingSecondCus() {
		String secondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");

		WebElement finanace = driver
				.findElement(By.xpath("//div[@data-type='finance']//span[text()='" + secondCusName + "']/.."));

		waitUntilElementVisible(driver, finanace, 120);

		if (verifyElementDisplayed(finanace)) {

			clickElementWb(finanace);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
					"VCC Slider is not present");
		}

	}


	public void selectCardTypeFinanceThirdCus() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String fthirdCusName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");

		WebElement finanace = driver
				.findElement(By.xpath("//div[@data-type='finance']//span[text()='" + fthirdCusName + "']/.."));

		waitUntilElementVisible(driver, finanace, 120);

		if (verifyElementDisplayed(finanace)) {

			clickElementWb(finanace);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeFinancingPrimary", Status.PASS,
					"Finanace card for primary customer selected");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeFinancingPrimary", Status.FAIL,
					"Finanace card Slider is not present");
		}

	}

	
	public void selectCardTypeVacationClub() {

		waitUntilElementVisibleBy(driver, vccSlider, 20);

		if (verifyObjectDisplayed(vccSlider)) {

			clickElementBy(vccSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
					"VCC Slider is not present");
		}

	}

	public void selectCardTypeVacationClubFirstCus() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String firstCusName = testData.get("FirstName") + " " + testData.get("LastName");
		WebElement vacationClub = driver
				.findElement(By.xpath("//div[@data-type='vcc']//span[text()='" + firstCusName + "']/.."));

		waitUntilElementVisible(driver, vacationClub, 120);

		if (verifyElementDisplayed(vacationClub)) {

			clickElementWb(vacationClub);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
					"VCC Slider is not present");
		}

	}

	public void selectCardTypeVacationClubSecondCus() {
		String secondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");

		WebElement vacationClub = driver
				.findElement(By.xpath("//div[@data-type='vcc']//span[text()='" + secondCusName + "']/.."));

		waitUntilElementVisible(driver, vacationClub, 120);

		if (verifyElementDisplayed(vacationClub)) {

			clickElementWb(vacationClub);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
					"VCC Slider is not present");
		}

	}
	
	public void selectCardTypeVacationClubThirdCus() {
		String thirdCusName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");

		WebElement vacationClub = driver
				.findElement(By.xpath("//div[@data-type='vcc']//span[text()='" + thirdCusName + "']/.."));

		waitUntilElementVisible(driver, vacationClub, 120);

		if (verifyElementDisplayed(vacationClub)) {

			clickElementWb(vacationClub);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
					"VCC Slider is not present");
		}

	}

	public void selectCardTypeRewards() {

		waitUntilElementVisibleBy(driver, barclaysSlider, 20);

		if (verifyObjectDisplayed(barclaysSlider)) {

			clickElementBy(barclaysSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("selectCardPage", "selectCardTypeRewards", Status.PASS,
					"Rewardcard is selected");
		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewards", Status.FAIL,
					"Slider for reward cardis not present");
		}
	}

	public void selectCardTypeRewardsFirstCus() {
		String firstCusName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");
		WebElement reward = driver
				.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + firstCusName + "']/.."));

		waitUntilElementVisible(driver, reward, 120);

		if (verifyElementDisplayed(reward)) {

			clickElementWb(reward);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.PASS,
					"reward card selected for primary customer");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.FAIL,
					"reward card Slider is not present");
		}

	}

	public void selectCardTypeRewardsSecondCus() {
		String secondCuName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");

		WebElement reward = driver
				.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + secondCuName + "']/.."));

		waitUntilElementVisible(driver, reward, 120);

		if (verifyElementDisplayed(reward)) {

			clickElementWb(reward);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.PASS,
					"reward card selected for secondary customer");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.FAIL,
					"Slider for reward card is not present");
		}

	}

	public void selectCardTypeRewardsThirdCus() {
		String thirdCuName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");

		WebElement reward = driver
				.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + thirdCuName + "']/.."));

		waitUntilElementVisible(driver, reward, 120);

		if (verifyElementDisplayed(reward)) {

			clickElementWb(reward);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.PASS,
					"reward card selected for secondary customer");

		} else {
			tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.FAIL,
					"Slider for reward card is not present");
		}

	}

	public void navigateToAgreementPage() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(nextBtn);
		checkLoadingSpinnerEWS();

	}

	public void selectCardType(String cardType) {

		if (cardType.equalsIgnoreCase("VCC")) {

			waitUntilElementVisibleBy(driver, vccSlider, 20);

			if (verifyObjectDisplayed(vccSlider)) {

				clickElementBy(vccSlider);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.PASS, "VCC selected");

				clickElementBy(nextBtn);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.FAIL,
						"Ownership Slider not present");
			}

		} else if (cardType.equalsIgnoreCase("Ownership")) {
			waitUntilElementVisibleBy(driver, financeSlider, 20);

			if (verifyObjectDisplayed(financeSlider)) {

				clickElementBy(financeSlider);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS, "Equifax selected");

				clickElementBy(nextBtn);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.FAIL,
						"VCC Slider not present");
			}
		} else if (cardType.equalsIgnoreCase("Barclays")) {
			waitUntilElementVisibleBy(driver, barclaysSlider, 20);

			if (verifyObjectDisplayed(barclaysSlider)) {

				clickElementBy(barclaysSlider);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.PASS,
						"barclays selected");

				clickElementBy(nextBtn);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.FAIL,
						"barclays Slider not present");
			}
		}
	}

	public void selectCardTypeforFirstCustomer(String cardType) {
		String firstCusName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");

		if (cardType.equalsIgnoreCase("Finance")) {

			WebElement finanacefirstCus = driver
					.findElement(By.xpath("//div[@data-type='finance']//span[text()='" + firstCusName + "']/.."));

			waitUntilElementVisible(driver, finanacefirstCus, 120);

			if (verifyElementDisplayed(finanacefirstCus)) {

				clickElementWb(finanacefirstCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsfirstary", Status.PASS,
						"reward card selected for firstary customer");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsfirstary", Status.FAIL,
						"Slider for reward card is not present");
			}
		} else if (cardType.equalsIgnoreCase("VacationClub")) {

			WebElement vccfirstCus = driver
					.findElement(By.xpath("//div[@data-type='vcc']//span[text()='" + firstCusName + "']/.."));

			waitUntilElementVisible(driver, vccfirstCus, 120);

			if (verifyElementDisplayed(vccfirstCus)) {

				clickElementWb(vccfirstCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
						"VCC Slider is not present");
			}
		} else if (cardType.equalsIgnoreCase("Rewards")) {
			WebElement rewardsfirstCus = driver
					.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + firstCusName + "']/.."));

			waitUntilElementVisible(driver, rewardsfirstCus, 120);

			if (verifyElementDisplayed(rewardsfirstCus)) {

				clickElementWb(rewardsfirstCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.PASS,
						"reward card selected for primary customer");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.FAIL,
						"reward card Slider is not present");
			}
		}

	}

	public void selectCardTypeforThirCustomer(String cardType) {
		String thirdCusName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");

		if (cardType.equalsIgnoreCase("Finanace")) {

			WebElement finanaceThirdCus = driver
					.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + thirdCusName + "']/.."));

			waitUntilElementVisible(driver, finanaceThirdCus, 120);

			if (verifyElementDisplayed(finanaceThirdCus)) {

				clickElementWb(finanaceThirdCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.PASS,
						"reward card selected for secondary customer");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.FAIL,
						"Slider for reward card is not present");
			}
		} else if (cardType.equalsIgnoreCase("VacationClub")) {

			WebElement vccThirdCus = driver
					.findElement(By.xpath("//div[@data-type='vcc']//span[text()='" + thirdCusName + "']/.."));

			waitUntilElementVisible(driver, vccThirdCus, 120);

			if (verifyElementDisplayed(vccThirdCus)) {

				clickElementWb(vccThirdCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
						"VCC Slider is not present");
			}
		} else if (cardType.equalsIgnoreCase("Rewards")) {
			WebElement rewardsThirdCus = driver
					.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + thirdCusName + "']/.."));

			waitUntilElementVisible(driver, rewardsThirdCus, 120);

			if (verifyElementDisplayed(rewardsThirdCus)) {

				clickElementWb(rewardsThirdCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.PASS,
						"reward card selected for primary customer");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.FAIL,
						"reward card Slider is not present");
			}
		}

	}

	public void selectCardTypeforSecondCustomer(String cardType) {
		String secondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");

		if (cardType.equalsIgnoreCase("Finanace")) {

			WebElement finanacesecondCus = driver
					.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + secondCusName + "']/.."));

			waitUntilElementVisible(driver, finanacesecondCus, 120);

			if (verifyElementDisplayed(finanacesecondCus)) {

				clickElementWb(finanacesecondCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.PASS,
						"reward card selected for secondary customer");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsSecondary", Status.FAIL,
						"Slider for reward card is not present");
			}
		} else if (cardType.equalsIgnoreCase("VacationClub")) {

			WebElement vccsecondCus = driver
					.findElement(By.xpath("//div[@data-type='vcc']//span[text()='" + secondCusName + "']/.."));

			waitUntilElementVisible(driver, vccsecondCus, 120);

			if (verifyElementDisplayed(vccsecondCus)) {

				clickElementWb(vccsecondCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.PASS, "VCC card selected");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeVCC", Status.FAIL,
						"VCC Slider is not present");
			}
		} else if (cardType.equalsIgnoreCase("Rewards")) {
			WebElement rewardssecondCus = driver
					.findElement(By.xpath("//div[@data-type='wrc']//span[text()='" + secondCusName + "']/.."));

			waitUntilElementVisible(driver, rewardssecondCus, 120);

			if (verifyElementDisplayed(rewardssecondCus)) {

				clickElementWb(rewardssecondCus);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.PASS,
						"reward card selected for primary customer");

			} else {
				tcConfig.updateTestReporter("SelectCardPage", "selectCardTypeRewardsPrimary", Status.FAIL,
						"reward card Slider is not present");
			}
		}

	}

	// ****************************************************

	public void selectCardVCC() {

		waitUntilElementVisibleBy(driver, vccSlider, 20);

		if (verifyObjectDisplayed(vccSlider)) {

			clickElementBy(vccSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.PASS, "VCC selected");

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, titleHeader, 20);
			if (verifyObjectDisplayed(titleHeader)) {

				tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.PASS,
						"Card Type Selected as: " + driver.findElement(titleHeader).getText()); // clickElementBy(WynLogo);
				// pageDown(); fieldDataEnter(ssnNumber,
				// testData.get("ssnNumber"));

				WebElement viewTermselement = driver.findElement(viewTerms);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(signature);
				Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(60, 60).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.FAIL,
							"Agree Button Not Enabled");

				}
				if (verifyObjectDisplayed(VCCApprovalAmount)) {

					ApprovalAmount = driver.findElement(VCCApprovalAmount).getText().trim();
					tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.PASS,
							"VCC Approval amount is: " + ApprovalAmount);

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.FAIL,
							"Approval ammount is not displayed");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.FAIL,
						"Error in Loading terms page");
			}

		} else {
			tcConfig.updateTestReporter("SelectCardType", "selectCardVCC", Status.FAIL,
					"VCC slider option not present");
		}

	}

	public void selectCardEquifax() {

		waitUntilElementVisibleBy(driver, financeSlider, 20);

		if (verifyObjectDisplayed(financeSlider)) {

			clickElementBy(financeSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS, "Equifax selected");

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, titleHeader, 20);
			if (verifyObjectDisplayed(titleHeader)) {

				tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS,
						"Card Type Selected as: " + driver.findElement(titleHeader).getText());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(WynLogo);
				pageDown();
				fieldDataEnter(ssnNumber, testData.get("ssnNumber"));
				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(signature);
				Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS,
						"SSN and Signature Entered");
				if (verifyObjectDisplayed(agreeBtn)) {

					// mouseoverAndClick(driver.findElement(agreeBtn));

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait")); //
					hoverOnElement(driver.findElement(nextBtn));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.FAIL,
						"Error in Loading terms page");
			}

		} else {
			tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.FAIL,
					"Finance Slider not present");
		}

	}

	public void selectCardbarclays() {

		waitUntilElementVisibleBy(driver, barclaysSlider, 20);

		if (verifyObjectDisplayed(barclaysSlider)) {

			clickElementBy(barclaysSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.PASS, "barclays selected");

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, titleHeader, 20);
			if (verifyObjectDisplayed(titleHeader)) {

				tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.PASS,
						"Card Type Selected as: " + driver.findElement(titleHeader).getText());

				// fieldDataEnter(ssnNumber, testData.get("ssnNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement viewTermselement = driver.findElement(termsBarclays);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(signature);
				Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.FAIL,
						"Error in Loading terms page");
			}

		} else {
			tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.FAIL,
					"barclays Slider not present");
		}

	}

	public void selectCardEQVCC() {

		// List<WebElement> cardSlider = driver.findElements(sliderSelect);

		waitUntilElementVisibleBy(driver, financeSlider, 20);

		if (verifyObjectDisplayed(financeSlider)) {

			clickElementBy(financeSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardEQVCC", Status.PASS, "Equifax selected");

			if (verifyObjectDisplayed(vccSlider)) {

				clickElementBy(vccSlider);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQVCC", Status.PASS, "VCC selected");

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQVCC", Status.FAIL,
						"VCC could not be selected");
			}

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, titleHeader, 20);
			if (verifyObjectDisplayed(titleHeader)) {

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"Card Type Selected as: " + driver.findElement(titleHeader).getText());

				fieldDataEnter(ssnNumber, testData.get("ssnNumber"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(signature);
				Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
						"Error in Loading terms page");
			}

			waitUntilElementVisibleBy(driver, viewTerms, 20);
			if (verifyObjectDisplayed(viewTerms)) {

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"Card Type Selected as: " + driver.findElement(titleHeader).getText());
				List<WebElement> ssnList = driver.findElements(ssnNumber);
				// fieldDataEnter(ssnList.get(1), testData.get("ssnNumber"));

				ssnList.get(1).sendKeys(testData.get("ssnNumber"));

				WebElement viewTermselement = driver.findElement(viewTerms);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);

				List<WebElement> signList = driver.findElements(signature);
				Actions builder = new Actions(driver);
				// WebElement canvasElement = driver.findElement(signature);
				Action drawAction = builder.moveToElement(signList.get(1), 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
						"Error in Loading terms page");
			}

		} else {
			tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
					"Finance Slider not present");
		}

	}

	public void selectCardEQBR() {

		// List<WebElement> cardSlider = driver.findElements(sliderSelect);

		waitUntilElementVisibleBy(driver, financeSlider, 20);

		if (verifyObjectDisplayed(financeSlider)) {

			clickElementBy(financeSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardEQBAR", Status.PASS, "Equifax selected");

			if (verifyObjectDisplayed(barclaysSlider)) {

				clickElementBy(barclaysSlider);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBAR", Status.PASS, "barclays selected");

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBAR", Status.FAIL,
						"Barclays could not be selected");
			}

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, titleHeader, 20);
			if (verifyObjectDisplayed(titleHeader)) {

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.PASS,
						"Card Type Selected as: " + driver.findElement(titleHeader).getText());

				fieldDataEnter(ssnNumber, testData.get("ssnNumber"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(signature);
				Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
						"Error in Loading terms page");
			}

			waitUntilElementVisibleBy(driver, termsBarclays, 20);
			if (verifyObjectDisplayed(termsBarclays)) {

				/*
				 * tcConfig.updateTestReporter("SelectCardType",
				 * "selectCardEQBR", Status.PASS, "Card Type Selected as: " +
				 * driver.findElement(titleHeader).getText());
				 */
				// fieldDataEnter(ssnNumber, testData.get("ssnNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement viewTermselement = driver.findElement(termsBarclays);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> sigList = driver.findElements(signature);

				Actions builder = new Actions(driver);
				// WebElement canvasElement =
				// driver.findElements(sigList.get(1));
				Action drawAction = builder.moveToElement(sigList.get(1), 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
						"Error in Loading terms page");
			}

		} else {
			tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
					"Finance Slider not present");
		}

	}

}
