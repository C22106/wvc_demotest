package eWorksheet.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class AllTourPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(AllTourPage.class);

	public AllTourPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By allTourLabel = By.xpath("//a[@class='nav-link active' and contains(text(),'All Tours')]");
	protected By saleSiteDrpdwn = By
			.xpath("//div[@class='ews-search-wrapper']//following-sibling::select[contains(@class,'user-site')]");
	protected By searchTour = By.xpath("//div[@class='input-group']/input[@placeholder='Filter by Tour Id or SA name']");
	protected By attachTour = By.xpath("//table[@class='mat-table all-tours-table mat-table']/tbody/tr/td[1]/button");
	protected By salesAgentName = By.xpath("//table[@class='mat-table all-tours-table mat-table']/tbody/tr/td[1]/span");
	protected By customerName = By.xpath(
			"//table[@class='mat-table all-tours-table mat-table']/tbody/tr/td[3]/div[@class='dropdown custom-dropdown']/button[@id='button-basic']");
	protected By attachTourMessage = By.xpath("//div[@id='myModal']//following::p[@class='alert-popup-text']");
	protected By attachTourNameasSSA = By.xpath(
			"//input[@name='attach_tour' and @ng-reflect-value='Split Sales Associate' and @id='radio1']/../label[@for='radio1']");
	protected By attachTourNameasTO = By
			.xpath("//input[@name='attach_tour' and @ng-reflect-value='TO' and @id='radio2']/../label[@for='radio2']");
	protected By attachTourNameasTM = By.xpath(
			"//input[@name='attach_tour' and @ng-reflect-value='Team Manager' and @id='radio3']/../label[@for='radio3']");
	protected By attachTourNameasSM = By.xpath(
			"//input[@name='attach_tour' and @ng-reflect-value='Sr.Manager 1' and @id='radio4']/../label[@for='radio4']");
	protected By attachTourNameasPresenter = By.xpath(
			"//input[@name='attach_tour' and @ng-reflect-value='Presenter' and @id='radio5']/../label[@for='radio5']");
	protected By saveButton = By.xpath("//button[contains(text(),'Save and go to tour') and @type='button']");
	protected By attachTourAsViewOnly = By
			.xpath("//label[contains(text(),'View Only')]//following::span[@class='slider round']");
	protected By customerDetailsNav = By.xpath("//a[@class='nav-link active' and contains(text(),'Customer Details')]");
	protected By tourIDfromTDC = By
			.xpath("(//div[@class='ews-tour-details-card ']/div[@class='ews-tour-body']//p)[1]/strong");
	protected By gotoWorksheetButton = By.xpath(
			"//button[not(contains(@class,'btn w-100 btn-black-outline disabled')) and contains(text(),'Go to Worksheet')]");
	protected By gotoWorksheetButtonViewOnly = By.xpath(
			"//button[contains(@class,'btn w-100 btn-black-outline disabled') and contains(text(),'Go to Worksheet')]");
	protected By backToHomePageLink = By.xpath("//span[contains(text(),'Back to Homepage')]");
	protected By tourHomePage = By.xpath("//div[@class='swiper-wrapper ']/div");
	protected By verifyTourID = By.xpath("(//table[@class='mat-table all-tours-table mat-table']/tbody/tr/td[10])[1]");
	protected By verifyCreditBand = By
			.xpath("(//table[@class='mat-table all-tours-table mat-table']/tbody/tr/td[9])[1]/img");
	protected By verifyWavetime = By.xpath("(//table[@class='mat-table all-tours-table mat-table']/tbody/tr/td[4])[1]");
	protected By verifyTourDate = By.xpath("//div[@class='ews-datepicker-wrapper']//input");
	protected By tourAssignment = By.xpath("//label[contains(text(),'Filters:')]//following::select[2]");
	protected By tourStatus = By.xpath("//label[contains(text(),'Filters:')]//following::select[3]");
	protected By waveTime = By.xpath("//label[contains(text(),'Filters:')]//following::select[1]");
	protected By nationality = By.xpath("//label[contains(text(),'Filters:')]//following::select[4]");
	protected By refreshButton = By.xpath("//li[@class='nav-item ews-refresh-item']/img");
	protected By salesAgntName = By.xpath("//th[text()='Tour Id']/../../../tbody/tr[1]/td[1]/span");
	protected By startWSBtn = By.xpath("//button[text()='Start Worksheet']");
	protected By addBtn = By.xpath("//button[text()='Add']");
	protected By addMessageBtn = By.xpath("//button[text()='Add/Exit']");
	protected By backToHome = By.xpath("//span[text()='Back to Homepage']");

	// protected By loc =
	// By.xpath("//a[contains(text(),'"+testData.get("SalesLocation")+"')]");

	public void verifyallTourLabel() {

		checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, allTourLabel, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(allTourLabel))) {
			tcConfig.updateTestReporter("AllTourPage", "verifyallTourLabel", Status.PASS,
					"Navigated to All tour page successfully");

		} else {

			tcConfig.updateTestReporter("AllTourPage", "verifyallTourLabel", Status.FAIL,
					"Not navigated to All tour page successfully");
		}

	}

	public void selectSaleSiteAllTour() {

		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, saleSiteDrpdwn, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(saleSiteDrpdwn))) {
			selectByText(saleSiteDrpdwn, testData.get("AllTourSalesSite"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("AllTourPage", "selectSaleSiteAllTour", Status.PASS,
					"Sale site is selected successfully");

		} else {

			tcConfig.updateTestReporter("AllTourPage", "selectSaleSiteAllTour", Status.FAIL,
					"Sale site is not selected successfully");
		}

	}

	public void verifySearchTourByID() {
		String strTourID = testData.get("TourID");

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(searchTour))) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(searchTour).sendKeys(strTourID);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.PASS,
					"Tour ID is placed in Search box Successfully");
		} else {
			tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.FAIL,
					"Tour ID is not placed in Search box Successfully");
		}

	}

	protected By exitBtn = By.xpath("//a[text()='Exit']");
	protected By empFuncDrpDwn = By.xpath("//select[@id='employee-function']");
	protected By empName = By.xpath("//input[@id='employee-name']");
    protected By empID = By.xpath("//input[@id='employee-id']");
    
	public void verifySelfAttachTour() {
		
		String empNameinput = testData.get("empName");
		String empIdinput = testData.get("empIdIn");

		waitUntilElementVisibleBy(driver, attachTour, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(attachTour))) {
			driver.findElement(attachTour).click();
			// waitUntilElementLocated(driver, empFuncDrpDwn, "ExplicitWaitLongestTime");
			/* waitForSometime(tcConfig.getConfig().get("MedWait")); */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			fieldDataEnter(empID, empIdinput);
			fieldDataEnter(empName, empNameinput);
			
			
			
			
			waitUntilElementVisibleBy(driver, empFuncDrpDwn, "ExplicitWaitLongestTime");
			new Select(driver.findElement(empFuncDrpDwn)).selectByValue(testData.get("selectEmpFunction"));
			tcConfig.updateTestReporter("AllTourPage", "verifySelfAttachTour", Status.PASS,
					"Employee Function is selected Successfully");
			waitUntilElementVisibleBy(driver, addBtn, "ExplicitWaitLongestTime");
			clickElementBy(addBtn);
			waitUntilElementVisibleBy(driver, strSalesAgent, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(strSalesAgent))) {
				tcConfig.updateTestReporter("AllTourPage", "verifySelfAttachTour", Status.PASS,
						"Sales Associate is added successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySelfAttachTour", Status.FAIL,
						"Sales Associate is not added");
			}
			waitUntilElementVisibleBy(driver, exitBtn, "ExplicitWaitLongestTime");
			clickElementBy(exitBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}

	}

	protected By empIdTxt = By.xpath("//input[@id='employee-id']");
	protected By empNameTxt = By.xpath("//input[@id='employee-name']");
	protected By empIDErrorMsg = By
			.xpath("//input[@id='employee-id']/../div/div[text()='Not assigned to Sales Store']");
	protected By empNameErrorMsg = By
			.xpath("//input[@id='employee-name']/../div/div[text()='Not assigned to Sales Store']");

	public void addSalesParticipant() {
		try {
			waitUntilElementVisibleBy(driver, attachTour, "ExplicitWaitLongestTime");
			driver.findElement(attachTour).click();
			waitUntilElementVisibleBy(driver, empIdTxt, "ExplicitWaitLongestTime");
			driver.findElement(empIdTxt).clear();
			driver.findElement(empIdTxt).sendKeys(testData.get("AssignSalesAgent"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(empNameTxt).clear();
			driver.findElement(empNameTxt).sendKeys(testData.get("selectEmpFunction"));

			new Select(driver.findElement(By.xpath("//select[@id='employee-function']")))
					.selectByValue("8: SALES_ASSOCIATE");
			waitUntilElementVisibleBy(driver, addBtn, "ExplicitWaitLongestTime");
			clickElementBy(addBtn);
			waitUntilElementVisibleBy(driver, empIDErrorMsg, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(empIDErrorMsg))
					&& verifyElementDisplayed(driver.findElement(empNameErrorMsg))) {
				tcConfig.updateTestReporter("AllTourPage", "verifySelfAttachTour", Status.PASS,
						"Error message is displayed for employee ID and Name which is not associated with the sales store");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySelfAttachTour", Status.FAIL,
						"Error message is not displayed for employee ID and Name which is not associated with the sales store");
			}
			clickElementBy(exitBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "verifySelfAttachTour", Status.FAIL,
					"Error message is not displayed for employee ID and Name which is not associated with the sales store"
							+ e.getMessage());
		}
	}

	protected By strSalesAgent = By.xpath("//span[text()='Sales Associate']");
	@FindBy(xpath = "//span[@class='employee-name']")
	protected WebElement getEmpName;

	public void getEmployeeFunction() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(attachTour).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(strSalesAgent))) {
				tcConfig.updateTestReporter("AllTourPage", "getEmployeeFunction", Status.PASS,
						"Sales Agent is added successfully");
				if (getEmpName.getText().equalsIgnoreCase(testData.get("AgentName"))) {
					tcConfig.updateTestReporter("AllTourPage", "getEmployeeFunction", Status.PASS,
							"Sales Agentname is displayed successfully");
				}
			} else {
				tcConfig.updateTestReporter("AllTourPage", "getEmployeeFunction", Status.FAIL,
						"Sales Agent is not added successfully");
			}

			clickElementBy(exitBtn);
		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "getEmployeeFunction", Status.FAIL,
					"Sales Agent is not added successfully" + e.getMessage());
		}
	}

	public void verifyRoleAttachToTour() {
		try {
			waitUntilElementVisibleBy(driver, customerDetailsNav, "ExplicitWaitLongestTime");
			String strWindowTitle = driver.findElement(customerDetailsNav).getText();
			if (strWindowTitle.equalsIgnoreCase("Customer Details")) {
				tcConfig.updateTestReporter("AllTourPage", "verifyRoleAttachToTour", Status.PASS,
						"Page is redirected to Customer Details page after selecting the attach tour role");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifyRoleAttachToTour", Status.FAIL,
						"Page is not redirected to Customer Details page after selecting the attach tour role");
			}

			waitUntilElementVisibleBy(driver, tourIDfromTDC, "ExplicitWaitLongestTime");
			String strTourID = driver.findElement(tourIDfromTDC).getText().trim();

			waitUntilElementVisibleBy(driver, backToHomePageLink, "ExplicitWaitLongestTime");
			driver.findElement(backToHomePageLink).click();
			waitUntilElementVisibleBy(driver, tourHomePage, "ExplicitWaitLongestTime");
			List<WebElement> allTourDashboardDetails = driver.findElements(tourHomePage);
			for (WebElement element : allTourDashboardDetails) {
				String attrTourID = element.getAttribute("id");
				if (attrTourID.equals(strTourID)) {
					tcConfig.updateTestReporter("AllTourPage", "verifyRoleAttachToTour", Status.PASS,
							"Self Tour " + attrTourID + " is successfully attached");
					break;
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "verifyRoleAttachToTour", Status.FAIL,
					"Self Tour is not attached");
		}
	}
	// verifyCreditBand

	public void verifyBestCreditBand() {
		String strGetCreditBand;
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			strGetCreditBand = driver.findElement(verifyCreditBand).getAttribute("src");
			if (strGetCreditBand.contains("credit-hard")) {
				tcConfig.updateTestReporter("AllTourPage", "verifyBestCreditBand", Status.PASS,
						"Best Credit Band is displayed");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifyBestCreditBand", Status.FAIL,
						"Best Credit Band is not displayed");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "verifyBestCreditBand", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	/*public void verifySiteTourFilter() {
		String strSearchFilter = testData.get("SiteFilterName");
		String strTourID = testData.get("TourID");
		String strTourDate = testData.get("TourDate");
		String strTourAssignDropVal = testData.get("TourAssignDropVal");
		String strTourStatusDropVal = testData.get("TourStatusDropVal");
		String strWaveTimeDropVal = testData.get("WaveTimeDropVal");
		String strNationalityDropVal = testData.get("NationalityDropVal");
		String getTourID = "";
		String getWaveTime = "";

		try {

			verifySearchTourByID();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getTourID = driver.findElement(verifyTourID).getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (strTourID.equals(getTourID)) {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
						"Filter value is displayed properly based on the Tour ID");
				if (verifyElementDisplayed(driver.findElement(refreshButton))) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(refreshButton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (driver.findElement(searchTour).getAttribute("ng-reflect-model").equals(strTourID)) {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
								"Filter value is displayed properly after clicking on the refresh button");
					} else {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
								"Filter value is not displayed properly after clicking on the refresh button");
					}
				}
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
						"Filter value is not displayed properly based on the Tour ID");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strTourDatefromDP = driver.findElement(verifyTourDate).getAttribute("ng-reflect-model").substring(0,
					15);
			SimpleDateFormat parser = new SimpleDateFormat("EEE MMM d yyyy");
			Date date = parser.parse(strTourDatefromDP);
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String formattedDate = formatter.format(date);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (strTourDate.equals(formattedDate)) {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
						"Filter value is displayed properly based on the Tour Date");
				if (verifyElementDisplayed(driver.findElement(refreshButton))) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(refreshButton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (strTourDate.equals(formattedDate)) {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
								"Filter value is displayed properly after clicking on the refresh button");
					} else {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
								"Filter value is not displayed properly after clicking on the refresh button");
					}
				} else {
					tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
							"Filter value is not displayed properly based on the Tour Date");
				}
			}

			if (strTourAssignDropVal.equals("Site Tours")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(tourAssignment).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				selectByValue(tourAssignment, strTourAssignDropVal);

				if (verifyElementDisplayed(driver.findElement(verifyTourID))) {
					tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
							"Filter value is displayed properly based on the Site Tour");
					if (verifyElementDisplayed(driver.findElement(refreshButton))) {
						driver.findElement(refreshButton).click();
						String strDropVal = getFirstSelectedOption(tourAssignment);
						if (strDropVal.equals(strTourAssignDropVal)) {
							tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
									"Filter value is displayed properly after clicking on the refresh button");
						} else {
							tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
									"Filter value is not displayed properly after clicking on the refresh button");
						}
					}
				} else {
					tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
							"Filter value is not displayed properly based on the Site Tour");
				}

			} else if (strTourAssignDropVal.equals("Team Tours")) {

				driver.findElement(tourAssignment).click();
				selectByValue(tourAssignment, strTourAssignDropVal);
				getTourID = driver.findElement(verifyTourID).getText();
				if (strTourID.equals(getTourID)) {
					tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
							"Filter value is displayed properly based on the Site Tour");
					if (verifyElementDisplayed(driver.findElement(refreshButton))) {
						driver.findElement(refreshButton).click();
						String strDropVal = getFirstSelectedOption(tourAssignment);
						if (strDropVal.equals(strTourAssignDropVal)) {
							tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
									"Filter value is displayed properly after clicking on the refresh button");
						} else {
							tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
									"Filter value is not displayed properly after clicking on the refresh button");
						}
					}
				} else {
					tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
							"Filter value is not displayed properly based on the Site Tour");
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(tourStatus).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectByValue(tourStatus, strTourStatusDropVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getTourID = driver.findElement(verifyTourID).getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (strTourID.equals(getTourID)) {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
						"Filter value is displayed properly based on the Tour Status");
				if (verifyElementDisplayed(driver.findElement(refreshButton))) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(refreshButton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String strDropVal = getFirstSelectedOption(tourStatus);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					 * if (strDropVal.equals(strTourStatusDropVal)) {
					 * tcConfig.updateTestReporter("AllTourPage",
					 * "verifySiteTourFilter", Status.PASS,
					 * "Filter value is displayed properly after clicking on the refresh button"
					 * ); } else { tcConfig.updateTestReporter("AllTourPage",
					 * "verifySiteTourFilter", Status.FAIL,
					 * "Filter value is not displayed properly after clicking on the refresh button"
					 * ); }
					 
				}
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
						"Filter value is not displayed properly based on the Tour Status");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(waveTime).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectByText(waveTime, strWaveTimeDropVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getWaveTime = driver.findElement(verifyWavetime).getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getTourID = driver.findElement(verifyTourID).getText();
			if (strTourID.equals(getTourID) && getWaveTime.equals(strWaveTimeDropVal)) {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
						"Filter value is displayed properly based on the Wave time");
				if (verifyElementDisplayed(driver.findElement(refreshButton))) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(refreshButton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String strDropVal = getFirstSelectedOption(waveTime);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (strDropVal.equals(strWaveTimeDropVal)) {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
								"Filter value is displayed properly after clicking on the refresh button");
					} else {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
								"Filter value is not displayed properly after clicking on the refresh button");
					}
				}
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
						"Filter value is not displayed properly based on the Wave time");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(nationality).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectByText(nationality, strNationalityDropVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getTourID = driver.findElement(verifyTourID).getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(verifyTourID))) {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
						"Filter value is displayed properly based on the Nationality");
				if (verifyElementDisplayed(driver.findElement(refreshButton))) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(refreshButton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String strDropVal = getFirstSelectedOption(nationality);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (strDropVal.equals(strNationalityDropVal)) {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.PASS,
								"Filter value is displayed properly after clicking on the refresh button");
					} else {
						tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
								"Filter value is not displayed properly after clicking on the refresh button");
					}
				}
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySiteTourFilter", Status.FAIL,
						"Filter value is not displayed properly based on the Nationality");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public void validateAllTourSalesAgentName() {
		String strSalesAgntName = testData.get("SalesAgentName");
		try {
			String strName = driver.findElement(salesAgntName).getText();
			if (strName.equals(strSalesAgntName)) {
				tcConfig.updateTestReporter("AllTourPage", "validateAllTourSalesAgentName", Status.PASS,
						"Sales Agent name is displayed successfully for the Tour ID");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateAllTourSalesAgentName", Status.FAIL,
						"Sales Agent name is not displayed successfully for the Tour ID");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	// ---------------------------------------------------------Locators-------------------------------------
	CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcConfig);
	public static String strVCCAppAmt = "";
	public static String strRewardsAppAmt = "";
	// public By allTourLabel = By.xpath("//a[@class='nav-link active' and
	// contains(text(),'All Tours')]");
	public By salesAgentLst = By.xpath("//td[contains(@class, 'salesAgent')]/span[contains(@class, 'agent')]");
	// public By searchTour =
	// By.xpath("//div[@class='input-group']/input[@placeholder='Filter by Tour
	// Id or SA name']");
	// public By attachTour = By.xpath("//table[@class='mat-table
	// all-tours-table mat-table']/tbody/tr/td[1]/button");
	// public By backToHome = By.xpath("//span[text()='Back to Homepage']");
	// public By verifyTourDate =
	// By.xpath("//div[@class='ews-datepicker-wrapper']//input");
	public By guestTBlBtn = By.xpath("//button[contains(@id, 'basic')]/span");
	public By firstObtained = By.xpath("//tbody/tr[1]/td[7]/p/span");
	public By custDetailLink = By.xpath("//a[@class='table-link' and contains(text(),'Customer Detail')]/img");
	public By dropDown = By
			.xpath("//table[contains(@class,'all-tours-table')]/tbody/tr/td[3]//button[@id='button-basic']");
	public By labelMarketingScr = By.xpath("//th/strong[text()='Marketing Score']");
	public By creditSoftScore = By.xpath("(//img[@alt='credit-score' and contains(@src,'soft')])[1]");
	public By salesSiteDropDown = By.xpath("//select[contains(@class,'user-site')]");
	public By sitetourDropDown = By.xpath("//select[contains(@class,'tour-assignment')]");
	public By creditLockScore = By.xpath("(//img[@alt='credit-score' and contains(@src,'locked')])[1]");
	public By creditLockSoftScore = By.xpath("(//img[@alt='credit-score' and contains(@src,'locked-soft')])[1]");
	public By withHoldCreditBand = By.xpath("(//img[@alt='event-status' and contains(@src,'hold')])[1]");
	public By vccApprovalAmt = By
			.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td[5])[1]/div/span/strong");
	// public By declinedCreditBandStatusVCC =
	// By.xpath("(//img[@alt='event-status' and
	// contains(@src,'declined')])[1]");
	public By creditBandStatusVCC = By.xpath("(//td[5]/div/img[contains(@class, 'ews-tour')])[1]");
	// public By declinedCreditBandStatusRewards =
	// By.xpath("(//img[@alt='event-status' and
	// contains(@src,'declined')])[2]");
	public By creditBandStatusRewards = By.xpath("(//td[6]/div/img[contains(@class, 'ews-tour')])[1]");
	public By rewardsApprovalAmt = By
			.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td[6])[1]/div/span/strong");
	public By marketingScoreVCCStatus = By.xpath(
			"(//table[contains(@class,'tours-credit-table')]/tbody/tr/td[3])[1]/div/img[contains(@src,'decline')]");
	public By marketingScoreVCCStatusNoScore = By
			.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td[3])[1]/span");

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : navigateToControlPanel()
	 * @Description : This function is click on the Customer Detail of Tour
	 *              searched & navigate to ECR Control Panel
	 * @Date : April 8, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void navigateToControlPanel() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		try {
			clickElementJS(guestTBlBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(custDetailLink))) {
				clickElementBy(custDetailLink);
				checkLoadingSpinnerEWS();
				waitUntilElementVisibleBy(driver, cusDetPage.ECRlabel, "ExplicitWaitLongestTime");
				tcConfig.updateTestReporter("AllTourPage", "navigateToControlPanel", Status.PASS,
						"Navigated Successfully to ECR Control panel");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "navigateToControlPanel", Status.FAIL,
						"User did not Navigate Successfully to ECR Control panel");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : verifySearchTourByID()
	 * @Description : This function is enter a Tour ID in search box & search in
	 *              All Tours
	 * @Test Data : TourID
	 * @Author : Prattusha Dutt
	 ***********************************************************************
	 */

	/*
	 * public void verifySearchTourByID() { String strTourID =
	 * testData.get("TourID"); try { System.out.println(strTourID +
	 * "-----------------------------------------");
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
	 * (verifyElementDisplayed(driver.findElement(searchTour))) {
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * driver.findElement(searchTour).sendKeys(strTourID);
	 * checkLoadingSpinnerEWS();
	 * waitForSometime(tcConfig.getConfig().get("LongWait"));
	 * //waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID",
	 * Status.PASS, "Tour ID is placed in Search box Successfully"); } else {
	 * tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID",
	 * Status.FAIL, "Tour ID is not placed in Search box Successfully"); }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 */

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : validateSoftScoreCreditBand()
	 * @Description : This function is validate the obtained credit band by
	 *              softscore
	 * @Date : April 8, 2020
	 * @Author : Priya Das
	 ***********************************************************************
	 */

	public void validateSoftScoreCreditBand() {
		try {
			waitUntilElementVisibleBy(driver, dropDown, "ExplicitWaitLongestTime");
			driver.findElement(dropDown).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, labelMarketingScr, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(creditSoftScore))) {
				tcConfig.updateTestReporter("AllTourPage", "guestNameDropDown", Status.PASS,
						"Soft Score Credit band is displayed");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "guestNameDropDown", Status.FAIL,
						"Soft Score Credit band is not displayed");
			}
			clickElementJS(guestTBlBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "guestNameDropDown", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : validateLockCreditBand()
	 * @Description : This function is validate the obtained credit band by
	 *              softscore
	 * @Date : April 8, 2020
	 * @Author : Priya Das
	 ***********************************************************************
	 */

	public void validateLockWithCreditBand() {
		Boolean withFlag = false;
		try {
			waitUntilElementVisibleBy(driver, dropDown, "ExplicitWaitLongestTime");
			driver.findElement(dropDown).click();
			waitUntilElementVisibleBy(driver, labelMarketingScr, "ExplicitWaitLongestTime");
			// withFlag=
			// verifyElementDisplayed(driver.findElement(withHoldCreditBand));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			if (verifyElementDisplayed(driver.findElement(creditLockScore))
					|| verifyElementDisplayed(driver.findElement(creditLockSoftScore))
					|| verifyElementDisplayed(driver.findElement(withHoldCreditBand))) {
				tcConfig.updateTestReporter("AllTourPage", "validateLockCreditBand", Status.PASS,
						"Lock Score Credit band/ Withold is displayed");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateLockCreditBand", Status.FAIL,
						"Lock Score Credit band/ Withold is not displayed");
			}
			clickElementJS(guestTBlBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateLockCreditBand", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name :verifyObtainedByName()
	 * 
	 * @Description : This function is used to Verify that Obtained by name
	 * should be displaying a user that obtained first approval (Credit Band,
	 * VCC or Rewards) for that customer
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : updated BY Prattusha Dutta
	 ***********************************************************************
	 */
	public void verifyObtainedByName() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean flag = false;

		String strUser = testData.get("NameSalesManager");
		clickElementJS(guestTBlBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		flag = verifyElementDisplayed(driver.findElement(firstObtained));
		String name = driver.findElement(firstObtained).getText();
		if (flag == true && name.equalsIgnoreCase(strUser)) {

			tcConfig.updateTestReporter("AllTourPage", "validateAllTourSalesAgentName", Status.FAIL,
					"User name that obtained approval is displayed successfully for the Tour ID");
		} else {

			tcConfig.updateTestReporter("AllTourPage", "validateAllTourSalesAgentName", Status.PASS,
					"User ame that obtained is displayed successfully for the Tour ID");
		}

		clickElementJS(guestTBlBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	// Function to Search Tour when Tour ID passed as parameter
	public void verifySearchTourByIDDP(String TourID) {

		try {
			String strTourID = TourID;
			System.out.println(strTourID + "-----------------------------------------");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(searchTour))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(searchTour).sendKeys(strTourID);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.PASS,
						"Tour ID is placed in Search box Successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.FAIL,
						"Tour ID is not placed in Search box Successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : validateVCCApprovalAmount()
	 * @Description : This function is validate the obtained VCC amount
	 * @Date : April 8, 2020
	 * @Author : Priya Das
	 ***********************************************************************
	 */
	public void validateVCCApprovalAmount() {
		try {
			waitUntilElementVisibleBy(driver, dropDown, "ExplicitWaitLongestTime");
			driver.findElement(dropDown).click();
			waitUntilElementVisibleBy(driver, labelMarketingScr, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(vccApprovalAmt))) {
				strVCCAppAmt = driver.findElement(vccApprovalAmt).getText();
				if (strVCCAppAmt != "") {
					tcConfig.updateTestReporter("AllTourPage", "validateVCCApprovalAmount", Status.PASS,
							"VCC Approval AMount is displayed" + strVCCAppAmt);
				} else {
					tcConfig.updateTestReporter("AllTourPage", "validateWithHoldCreditBand", Status.FAIL,
							"VCC Approval Amount is not displayed");
				}

			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateWithHoldCreditBand", Status.FAIL,
						"VCC Approval Amount is not displayed");
			}
			clickElementJS(guestTBlBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateWithHoldCreditBand", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : validateVCCStatusCreditBand()
	 * @Description : This function is validate the obtained VCC status is
	 *              declined/ pending
	 * @Date : April 8, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void validateVCCStatusCreditBand() {
		// public void validateWithDeclinedStatusCreditBand() {
		try {
			waitUntilElementVisibleBy(driver, dropDown, "ExplicitWaitLongestTime");
			driver.findElement(dropDown).click();
			waitUntilElementVisibleBy(driver, labelMarketingScr, "ExplicitWaitLongestTime");
			Boolean vccFlag = false;
			vccFlag = verifyElementDisplayed(driver.findElement(creditBandStatusVCC));
			WebElement img = driver.findElement(creditBandStatusVCC);
			String src = img.getAttribute("src");
			System.out.println("the value inside the src tag  " + src);
			if (vccFlag == true && src.contains("declined") || src.contains("pending") || src.contains("submitted")) {
				/*
				 * if (verifyElementDisplayed(driver.findElement(
				 * creditBandStatusRewards)) &&
				 * verifyElementDisplayed(driver.findElement(creditBandStatusVCC
				 * ))) {
				 */
				tcConfig.updateTestReporter("AllTourPage", "validateVCCStatusCreditBand", Status.PASS,
						"Credit band with Declined/Pending status for VCC is displayed");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateVCCStatusCreditBand", Status.FAIL,
						"Credit band with Declined/Pending status for VCCis not displayed");
			}
			clickElementJS(guestTBlBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateVCCStatusCreditBand", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : validateRewardStatusCreditBand()
	 * @Description : This function is validate the obtained Rewads status is
	 *              declined/ pending
	 * @Date : April 8, 2020
	 * @Author : Priya Das
	 ***********************************************************************
	 */
	public void validateRewardStatusCreditBand() {
		// public void validateWithDeclinedStatusCreditBand() {
		try {
			waitUntilElementVisibleBy(driver, dropDown, "ExplicitWaitLongestTime");
			driver.findElement(dropDown).click();
			waitUntilElementVisibleBy(driver, labelMarketingScr, "ExplicitWaitLongestTime");
			Boolean rewardsFlag = false;
			rewardsFlag = verifyElementDisplayed(driver.findElement(creditBandStatusRewards));
			WebElement img = driver.findElement(creditBandStatusRewards);
			String src = img.getAttribute("src");
			System.out.println("the value inside the src tag  " + src);

			if (rewardsFlag == true && src.contains("declined") || src.contains("pending")) {
				/*
				 * if (verifyElementDisplayed(driver.findElement(
				 * creditBandStatusRewards)) &&
				 * verifyElementDisplayed(driver.findElement(creditBandStatusVCC
				 * ))) {
				 */
				tcConfig.updateTestReporter("AllTourPage", "validateRewardStatusCreditBand", Status.PASS,
						"Credit band with Declined/Pending status for Rewards is displayed");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateRewardStatusCreditBand", Status.FAIL,
						"Credit band with Declined/Pending status for Rewards is not displayed");
			}
			clickElementJS(guestTBlBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateRewardStatusCreditBand", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	/**
	 **********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name : validateRewardsApprovalAmount()
	 * @Description : This function is validate the obtained Rewads status is
	 *              declined/ pending
	 * @Date : April 8, 2020
	 * @Author : Priya Das
	 ***********************************************************************
	 */
	public void validateRewardsApprovalAmount() {
		try {
			waitUntilElementVisibleBy(driver, dropDown, "ExplicitWaitLongestTime");
			driver.findElement(dropDown).click();
			waitUntilElementVisibleBy(driver, labelMarketingScr, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(rewardsApprovalAmt))) {
				strRewardsAppAmt = driver.findElement(rewardsApprovalAmt).getText();
				if (strRewardsAppAmt != "") {
					tcConfig.updateTestReporter("AllTourPage", "validateRewardsApprovalAmount", Status.PASS,
							"Rewards Approval AMount is displayed" + strRewardsAppAmt);
				} else {
					tcConfig.updateTestReporter("AllTourPage", "validateRewardsApprovalAmount", Status.FAIL,
							"Rewards Approval AMount is not displayed");
				}

			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateRewardsApprovalAmount", Status.FAIL,
						"VCC Approval AMount is not displayed");
			}
			clickElementJS(guestTBlBtn);
		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateRewardsApprovalAmount", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	public void validateMarketingScoreVCCstatus(String strVCCStatus) {

		waitUntilElementVisibleBy(driver, dropDown, "ExplicitWaitLongestTime");
		driver.findElement(dropDown).click();
		waitUntilElementVisibleBy(driver, labelMarketingScr, "ExplicitWaitLongestTime");

		if (verifyElementDisplayed(driver.findElement(marketingScoreVCCStatusNoScore))) {

			tcConfig.updateTestReporter("AllTourPage", "validateMarketingScoreVCCstatus", Status.PASS,
					"VCC for Marketing score not displayed");

		} else {
			tcConfig.updateTestReporter("AllTourPage", "validateMarketingScoreVCCstatus", Status.FAIL,
					"VCC for Marketing score is displayed");
		}

		clickElementJS(guestTBlBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	protected By creditNoScore = By.xpath("//td[contains(@class, 'credit mat')]/img[contains(@src, 'no-score')]");
	protected By creditLocked = By.xpath("//td[contains(@class, 'credit mat')]/img[contains(@src, 'locked')]");

	public void validateCreditColumn() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(creditNoScore))
				|| verifyElementDisplayed(driver.findElement(creditLocked))) {
			tcConfig.updateTestReporter("AllTourPage", "validateCreditColumn", Status.PASS,
					"Credit band column is displayed as NoScore or Locked");
		} else {
			tcConfig.updateTestReporter("AllTourPage", "validateCreditColumn", Status.FAIL,
					"Credit band column is displayed as NoScore or Locked");
		}

	}

	public void verifyTeamAllTours() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String teamInput = testData.get("Team");
		Boolean teamFlag = false;
		teamFlag = verifyElementDisplayed(driver.findElement(By.xpath("//h6/p[text()='" + teamInput + "']")));
		if (teamFlag == true) {
			tcConfig.updateTestReporter("AllTourPage", "verifyTeamAllTours", Status.PASS,
					"Team has ben assigned successfully");
		} else {
			tcConfig.updateTestReporter("AllTourPage", "validateCreditColumn", Status.FAIL,
					"Team assignmnet failure");
		}

	}
}
