package eWorksheet.pages;

//import java.util.Base64;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WSALoginPage extends eWorksheetBasePage {
	
	public static final Logger log = Logger.getLogger(WSALoginPage.class);

	public WSALoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	
	
	
	@FindBy(xpath="//input[@id='fldUsername']") WebElement textUsername;
	
	/*
	 * enter user name
	 */
	protected void setUserName(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsername, 120);
		textUsername.sendKeys(strUserName);		
		tcConfig.updateTestReporter("WSALoginPage","setUserName",Status.PASS, "Username is entered for WSA Login");
	}
	
	
	@FindBy(xpath="//input[@id='fldPassword']") WebElement textPassword;

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	protected void setPassword(String strPassword) throws Exception {
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString,"UTF-8");
		
		textPassword.sendKeys(dString);
		tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.PASS, "Password is entered for WSA Login");
	}

	protected By buttonLogIn = By.xpath("//button[@name='cn-submit']");
	
	/*
	 * Launch WSA application
	 */
	
	public void launchApp(String strBrowser) {
		
		this.driver=checkAndInitBrowser(strBrowser);
		String url=testData.get("WSAUrl");
		driver.navigate().to(url);
		driver.manage().window().maximize();
	}
	

	@FindBy(xpath="//img[@title='Change Sales Site']") WebElement changeSiteicon;
	/*
	 * Login WSA application
	 */
	public void loginToWSAApp(String strBrowser) throws Exception {

		
		//testData.put("testTry", "Monideep");
		launchApp(strBrowser);
		String userid = tcConfig.getConfig().get("WSA_UserID");
		String password = tcConfig.getConfig().get("WSA_Password");
		setUserName(userid);
		setPassword(password);
		driver.findElement(buttonLogIn).click();
		
		if(verifyElementDisplayed(changeSiteicon)) {
			tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.PASS, "Log in button clicked");
		}else{
			launchApp(strBrowser);
			
			setUserName(userid);
			setPassword(password);
			driver.findElement(buttonLogIn).click();
			if(verifyElementDisplayed(changeSiteicon)) {
				tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.PASS, "Log in button clicked");
			}else{
				tcConfig.updateTestReporter("WSALoginPage","setPassword",Status.FAIL, "Log in button clicked : But application not opened");
			}
		}
	}
	

	

	@FindBy(xpath="//a[text()='Logout']") WebElement LogoutBtn;
	@FindBy(xpath="//div[@class='msg inform']") WebElement logoutMsg;
	@FindBy(xpath="//img[@title='Home']") WebElement homeIcon;
	
	
	public void WSALogout() throws Exception {
		try{
		waitUntilElementVisible(driver, LogoutBtn, 120);
		LogoutBtn.click();
		waitUntilElementVisible(driver, textUsername, 120);
		tcConfig.updateTestReporter("WSALoginPage","logOut",Status.PASS, "Log Out Succesful");
		}catch(Exception e){
			tcConfig.updateTestReporter("WSALoginPage","logOut",Status.FAIL, "Log Out Failed"+e.getMessage());
		}
	}
	
}
