package eWorksheet.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class AddPackageInventoryPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(AddPackageInventoryPage.class);

	public AddPackageInventoryPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	protected By applyPackagesBtn = By.xpath("//button[text()='Apply Packages']");
	protected By letsBeginBtn = By.xpath("//button[text()='Let’s Begin']");

	public void selectCWAPackagesfromInventory() throws Exception {

		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver,
				By.xpath(
						"//h4[text()='Select CWA Points Package Amounts to present']/..//div[@class='package-points-card']/strong[text()='"
								+ testData.get("TourPointsSelect") + "']/..//label"),
				"ExplicitWaitLongestTime");
		driver.findElement(By
				.xpath("//h4[text()='Select CWA Points Package Amounts to present']/..//div[@class='package-points-card']/strong[text()='"
						+ testData.get("TourPointsSelect") + "']/..//label"))
				.click();
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, applyPackagesBtn, "ExplicitWaitLongestTime");
		driver.findElement(applyPackagesBtn).click();
		tcConfig.updateTestReporter("AddPackageInventoryPage", "selectCWAPackagesfromInventory", Status.PASS,
				"Apply Package button is clicked successfully");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(letsBeginBtn))) {
			tcConfig.updateTestReporter("AddPackageInventoryPage", "selectCWAPackagesfromInventory", Status.PASS,
					"Lets Begin button is displayed successfully");
			clickElementJSWithWait(letsBeginBtn);
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

	}

	public void clickLetsBegin() {
		waitUntilElementVisibleBy(driver, applyPackagesBtn, "ExplicitWaitLongestTime");
		driver.findElement(applyPackagesBtn).click();
		tcConfig.updateTestReporter("AddPackageInventoryPage", "selectCWAPackagesfromInventory", Status.PASS,
				"Apply Package button is clicked successfully");

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, letsBeginBtn, "ExplicitWaitLongestTime");
		tcConfig.updateTestReporter("AddPackageInventoryPage", "selectCWAPackagesfromInventory", Status.PASS,
				"Lets Begin button is clicked successfully");
		clickElementJSWithWait(letsBeginBtn);

	}

}
