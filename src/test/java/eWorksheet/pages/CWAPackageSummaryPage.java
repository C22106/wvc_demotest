package eWorksheet.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CWAPackageSummaryPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(CWAPackageSummaryPage.class);

	public CWAPackageSummaryPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public String strwsaRefNum;
	public String packgePoints;
	public String purchasePrice;
	public double downPayment;
	public double financedAmountUI;
	public String strfinancedAmt;
	public String strNoOfPayments;
	public String strInterestRate;
	public String strLoanPayment;
	public String strMonMntcFee;
	public static String wsaRefNumber;

	protected By cashBtn = By.xpath("(//ul[@class='list-inline select-payment-list']/li/span)[3]");
	protected By addPayBtn = By.xpath(
			"(//div[@class='card-footer down-payment-card-footer']//following-sibling::button[text()='Add Payment'])[3]");
	protected By continueFinanceBtn = By.xpath("//button[text()='Continue to Financing']");
	protected By continueOwnBtn = By.xpath("//button[text()='Continue to Owners']");
	protected By addressLink = By
			.xpath("(//div[contains(@class,'personal-information')]//following::button[text()=' Address '])[1]");
	protected By contactInfoLink = By.xpath("//button[text()=' Contact Information ']");
	protected By custAddChkBox = By.xpath(
			"//label[text()='Continue with the Customer supplied address:']//following::input[@id='0']/../label");
	@FindBy(xpath = "//select[@formcontrolname='primaryPhoneType']")
	WebElement selectPrimaryPhone;
	@FindBy(xpath = "//select[@formcontrolname='alternatePhoneType']")
	WebElement selectAlterPhone;
	protected By saveContinueBtn = By.xpath("//button[text()=' Save and Continue ']");
	protected By continueSummary = By.xpath("//button[text()=' Continue to Summary ']");
	protected By ssn = By.xpath("//tab[@ng-reflect-active='true']//following::input[@id='ssn']");
	protected By primaryNum = By.xpath("//tab[@ng-reflect-active='true']//input[@name='primaryPhoneNumber']");
	protected By autoPayCredit = By.xpath("//label[@for='autopay-credit']");
	protected By summaryReview = By.xpath("//label[@for='summary-checkbox1']");
	protected By standardBenChkBox = By.xpath("//label[@for='standard-benefits-checkbox']");
	protected By packagePoints = By
			.xpath("//div[contains(@class,'package-summary-card')]//div[@class='card-img-overlay']/strong");
	protected By downPayTxt = By.xpath("//div[@class='card-body']/table/tbody/tr[1]/td[2]");
	protected By financedAmt = By.xpath("//div[@class='card-body']/table/tbody/tr[3]/td[2]");
	protected By pointsPurchaseSummary = By
			.xpath("//h3[text()='POINTS PURCHASED']//following::table[1]/tbody/tr[2]/td[2]/strong");
	protected By maintainanceFee = By
			.xpath("//h3[text()='POINTS PURCHASED']//following::table[2]/tbody/tr[2]/td[2]/strong");
	protected By financeReviewPage = By.xpath("(//app-summary-financial-information//h3)[1]");
	protected By downPayReviewPage = By
			.xpath("(//app-summary-financial-information//h3)[1]//following::table[1]/thead/tr/th[2]/strong");
	protected By financeAmtReviewPage = By
			.xpath("(//app-summary-financial-information//h3)[2]//following::table[1]/thead/tr/th[2]/strong");
	protected By standardBenefiCheck = By.xpath("//h3[text()='STANDARD BENEFITS']");
	protected By gender = By.xpath("//tab[@ng-reflect-active='true']//select[@ng-reflect-name='gender']");
	protected By stateDrpDwn = By.xpath("//select[@formcontrolname='stateProvinceCode']");

	@FindBy(xpath = "(//table/thead/tr/th/strong[text()=' Year 3-5 '])[1]")
	protected WebElement year3txt;

	@FindBy(xpath = "(//table/thead/tr/th/strong[text()='5+ '])[1]")
	protected WebElement year5txt;

	protected By bonusDetailsPopUp = By.xpath("//p[text()='Bonus Points Details']");
	protected By picPlusDetPopUp = By.xpath("//p[contains(text(),'PIC Plus Details')]");
	protected By picExpressDetPopUp = By.xpath("//p[text()='PIC Express Details']");

	protected By okBtn = By.xpath("//button[text()='Ok']");

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='Bonus Points']/../..//following::td/span)[1]")
	protected WebElement bonusPointTxt;

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='PIC Plus']/../..//following::td/span)[1]")
	protected WebElement picPlusTxt;
	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='PIC Plus']/../..//following::td/span)[2]")
	protected WebElement picPlusYear2;

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='Bonus Points']/../..//following::td//span)[2]")
	protected WebElement bonusPointforLessDP;

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='Annual Points']/../..//following::td/span)[1]")
	protected WebElement annualPoint;

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='Annual Points']/../..//following::td/span)[2]")
	protected WebElement annualPointYear2;

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='Annual Points']/../..//following::td/span)[3]")
	protected WebElement annualPointYear3;

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='Annual Points']/../..//following::td/span)[4]")
	protected WebElement annualPointYear5;

	@FindBy(xpath = "(//tab[@id='point-usage']//strong[text()='Total Points']/..//following::td/strong)[1]")
	protected WebElement totalPointTxt;

	@FindBy(xpath = "(//strong[text()='Total Points']/../../td/strong)[3]")
	protected WebElement totalPointYear1;

	@FindBy(xpath = "(//tab[@id='point-usage']//strong[text()='Total Points']/..//following::td/strong)[2]")
	protected WebElement year2TotalPointstxt;

	@FindBy(xpath = "(//tab[@id='point-usage']//strong[text()='Total Points']/..//following::td/strong)[3]")
	protected WebElement year3TotalPointstxt;

	@FindBy(xpath = "(//tab[@id='point-usage']//strong[text()='Total Points']/..//following::td/strong)[2]")
	protected WebElement year3TotalPointstxtforLessDP;

	@FindBy(xpath = "(//tab[@id='point-usage']//strong[text()='Total Points']/..//following::td/strong)[4]")
	protected WebElement year5TotalPointstxt;

	protected By addNewOwner = By.xpath("//a[text()='Add New Owner']");

	@FindBy(xpath = "//span[text()='Primary']")
	protected WebElement primaryOwner;

	
	protected By notSavedMsgPopUp = By.xpath("//span[text()='Current Prospect has unsaved data which will be lost.']");

	protected By noBtn = By.xpath("//button[contains(text(),'No')]");

	public void validateOwnerInformationSaveDet() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(cashBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(addPayBtn);
			tcConfig.updateTestReporter("Payment Method Screen", "submitWorksheet()", Status.PASS,
					"User is bale to select the Payment Method successfully");
			waitUntilElementVisibleBy(driver, continueFinanceBtn, "ExplicitWaitLongestTime");
			if (driver.findElement(continueFinanceBtn).isEnabled()) {
				clickElementBy(continueFinanceBtn);
				waitUntilElementVisibleBy(driver, autoPayCredit, "ExplicitWaitLongestTime");
				clickElementBy(autoPayCredit);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("Finance Summary Page", "validateSummaryPage", Status.PASS,
						"Page Successfully redirected to Finance Page");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(continueOwnBtn).isEnabled()) {
				clickElementBy(continueOwnBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, addNewOwner, "ExplicitWaitLongestTime");
				clickElementBy(addNewOwner);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				primaryOwner.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, notSavedMsgPopUp, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(notSavedMsgPopUp))) {
					tcConfig.updateTestReporter("Owner Information Page", "validateSummaryPage", Status.PASS,
							"Warning Message is displayed while not saving the Owner Field Data");
					clickElementBy(noBtn);
				} else {
					tcConfig.updateTestReporter("Owner Information Page", "validateSummaryPage", Status.FAIL,
							"Warning Message is not displayed while not saving the Owner Field Data");
				}

			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Owner Information Page", "validateSummaryPage", Status.FAIL,
					"Failed in Message" + e.getMessage());
		}
	}

	public void validateSummaryPage() {

		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, cashBtn, "ExplicitWaitLongestTime");
		clickElementBy(cashBtn);
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, addPayBtn, "ExplicitWaitLongestTime");
		clickElementBy(addPayBtn);
		tcConfig.updateTestReporter("Payment Method Screen", "submitWorksheet()", Status.PASS,
				"User is bale to select the Payment Method successfully");
		waitUntilElementVisibleBy(driver, continueFinanceBtn, "ExplicitWaitLongestTime");
		if (driver.findElement(continueFinanceBtn).isEnabled()) {
			clickElementBy(continueFinanceBtn);
			/*
			 * waitUntilElementVisibleBy(driver, autoPayCredit, "ExplicitWaitLongestTime");
			 * clickElementBy(autoPayCredit);
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("Finance Summary Page", "validateSummaryPage", Status.PASS,
					"Page Successfully redirected to Finance Page");
		}
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, continueOwnBtn, "ExplicitWaitLongestTime");
		if (driver.findElement(continueOwnBtn).isEnabled()) {
			clickElementBy(continueOwnBtn);
			waitUntilElementVisibleBy(driver, gender, "ExplicitWaitLongestTime");
			new Select(driver.findElement(gender)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Pattern format = Pattern.compile("XXX-XX-XXXX");
			String strSSN = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression");
			if (format.matcher(strSSN).matches()) {
				tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.PASS,
						"User is able to see the masked SSN number successfully");
			} else {
				tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.FAIL,
						"User is not able to see the masked SSN number");
			}
			int firstVal = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression").split("-")[0].length();
			int secondVal = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression").split("-")[1].length();
			int thirdVal = driver.findElement(ssn).getAttribute("ng-reflect-mask-expression").split("-")[2].length();
			if (firstVal == 3 && secondVal == 2 && thirdVal == 4) {
				tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
						"User is not able to enter more than 9 digits for SSN field");
			} else {
				tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.FAIL,
						"User is able to enter more than 9 digits for SSN field");
			}
			waitUntilElementVisibleBy(driver, addressLink, "ExplicitWaitLongestTime");
			clickElementBy(addressLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(stateDrpDwn)).selectByIndex(5);
			tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
					"User is able to select the state from Drop Down");
			waitUntilElementVisibleBy(driver, contactInfoLink, "ExplicitWaitLongestTime");
			clickElementBy(contactInfoLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, custAddChkBox, "ExplicitWaitLongestTime");
			clickElementBy(custAddChkBox);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, contactInfoLink, "ExplicitWaitLongestTime");
			if (driver.findElement(contactInfoLink).isEnabled()) {
				clickElementBy(contactInfoLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				int firstpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[0].length();
				int secondpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[1].length();
				int thirdpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[2].length();
				if (firstpatt == 3 && secondpatt == 3 && thirdpatt == 4) {
					tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
							"User is able to perform validation for International numbers using plugin");
				} else {
					tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.FAIL,
							"User is not able to perform validation for International numbers using plugin");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				new Select(selectPrimaryPhone).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				new Select(selectAlterPhone).selectByIndex(2);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(saveContinueBtn);

			}
		}
		waitUntilElementVisibleBy(driver, continueSummary, "ExplicitWaitLongestTime");
		tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.PASS,
				"User is able to go to Summary Page");
		clickElementBy(continueSummary);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, summaryReview, "ExplicitWaitLongestTime");
		// Values to check
		/*packgePoints = driver.findElement(packagePoints).getText().trim();
		downPayment = Double.parseDouble(driver.findElement(downPayTxt).getText().trim().substring(1).replace(",", ""));
		if (testData.get("DownPaymentPercentage").equals("100%")) {
			String strFinance = driver.findElement(By.xpath("//div[@class='card-body']/table/tbody/tr[3]/td[2]/strong"))
					.getText().trim();
			if (strFinance.equals("-")) {
				System.out.println("No Finance AMount required");
			}
		} else {
			financedAmountUI = Double
					.parseDouble(driver.findElement(financedAmt).getText().trim().substring(1).replace(",", ""));
		}*/

		clickElementBy(summaryReview);
		// String primaryOwner = LeadsPage.strLeadFullName.toLowerCase();
		/*
		 * if (driver.findElement(By.xpath("//strong[text()='" + primaryOwner +
		 * "']")).getText() .equals(primaryOwner)) {
		 * tcConfig.updateTestReporter("Ownership Review Page",
		 * "submitWorksheet()", Status.PASS,
		 * "Primary Owner name is displayed successfully"); }
		 */
		/*waitUntilElementVisibleBy(driver, pointsPurchaseSummary, "ExplicitWaitLongestTime");
		if (packgePoints.equals(driver.findElement(pointsPurchaseSummary).getText())) {
			tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
					"Point package purchase amount is displayed successfully");
		}
		if (verifyElementDisplayed(driver.findElement(maintainanceFee))) {
			tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
					"Maintainance Fee is displayed successfully");
		}*/
		List<WebElement> reviewList = driver.findElements(By.xpath("//label[contains(@for,'review-checkbox')]"));
		for (int i = 1; i <= reviewList.size(); i++) {
			driver.findElement(By.xpath("(//label[contains(@for,'review-checkbox')])[" + i + "]")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> nextList = driver.findElements(By.xpath("//button[text()=' Next ']"));
		for (int j = 1; j <= nextList.size(); j++) {
			if (j == 1) {
				driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (driver.findElement(financeReviewPage).getText().equals("FINANCIAL INFORMATION TODAY")) {
					Double downPaymnet = Double
							.parseDouble(driver.findElement(downPayReviewPage).getText().substring(1).replace(",", ""));
					if (Math.round(downPaymnet) == downPayment) {
						tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
								"Down Payment Today is displayed successfully in Summary Page");
					}

					/*
					 * double financedAmount = Double.parseDouble(
					 * driver.findElement(financeAmtReviewPage).getText().
					 * substring(1).replace(",", "")); if
					 * (Math.round(financedAmount) == s) {
					 * tcConfig.updateTestReporter("Ownership Review Page",
					 * "submitWorksheet()", Status.PASS,
					 * "Financed Amount is displayed successfully in Summary Page"
					 * ); }
					 */

					List<WebElement> list = driver
							.findElements(By.xpath("//label[contains(@for,'financial-information-checkbox')]"));
					for (int k = 1; k <= list.size(); k++) {
						driver.findElement(
								By.xpath("(//label[contains(@for,'financial-information-checkbox')])[" + k + "]"))
								.click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
				}
			} else if (j == 2) {
				driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, standardBenefiCheck, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(standardBenefiCheck))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet()", Status.PASS,
							"Standard Benefits are displayed successfully in Summary Page");
				}

				clickElementBy(standardBenChkBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}
	}
	
	
	protected By dateOfBirth = By.xpath("//input[contains(@placeholder, 'Date of Birth')]");
	protected By ssnNumber = By.xpath("//input[contains(@placeholder, 'Enter SSN')]");
	protected By primaryPhnTxtBox = By.xpath("//input[contains(@name, 'primaryPhoneNumber')]");
	
	
	public void validateSummaryPageNoScore() {
		
		String cusDOB = testData.get("DOB");
		String ssnNumbInput = testData.get("FirstCusSSN");
		String phoneInput = testData.get("primaryPhoneNumber");

		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, cashBtn, "ExplicitWaitLongestTime");
		clickElementBy(cashBtn);
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, addPayBtn, "ExplicitWaitLongestTime");
		clickElementBy(addPayBtn);
		tcConfig.updateTestReporter("Payment Method Screen", "submitWorksheet()", Status.PASS,
				"User is bale to select the Payment Method successfully");
		waitUntilElementVisibleBy(driver, continueFinanceBtn, "ExplicitWaitLongestTime");
		if (driver.findElement(continueFinanceBtn).isEnabled()) {
			clickElementBy(continueFinanceBtn);
			/*
			 * waitUntilElementVisibleBy(driver, autoPayCredit, "ExplicitWaitLongestTime");
			 * clickElementBy(autoPayCredit);
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("Finance Summary Page", "validateSummaryPage", Status.PASS,
					"Page Successfully redirected to Finance Page");
		}
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, continueOwnBtn, "ExplicitWaitLongestTime");
		if (driver.findElement(continueOwnBtn).isEnabled()) {
			clickElementBy(continueOwnBtn);
			waitUntilElementVisibleBy(driver, gender, "ExplicitWaitLongestTime");
			new Select(driver.findElement(gender)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(dateOfBirth);
			driver.findElement(dateOfBirth).sendKeys(cusDOB);
			//fieldDataEnter(dateOfBirth, cusDOB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// ----trying to use javascript---------------
			WebElement wbel = driver.findElement(ssnNumber);
			// sendElementJSWithWait(wbel);

			// ---------trying with jscript click & normal send
			// keys-------------------------
			clickElementJS(wbel);
			wbel.sendKeys(ssnNumbInput);
		
			waitUntilElementVisibleBy(driver, addressLink, "ExplicitWaitLongestTime");
			clickElementBy(addressLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(stateDrpDwn)).selectByIndex(5);
			tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
					"User is able to select the state from Drop Down");
			waitUntilElementVisibleBy(driver, contactInfoLink, "ExplicitWaitLongestTime");
			clickElementBy(contactInfoLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, custAddChkBox, "ExplicitWaitLongestTime");
			clickElementBy(custAddChkBox);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, contactInfoLink, "ExplicitWaitLongestTime");
			if (driver.findElement(contactInfoLink).isEnabled()) {
				clickElementBy(contactInfoLink);
				/*waitForSometime(tcConfig.getConfig().get("MedWait"));
				int firstpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[0].length();
				int secondpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[1].length();
				int thirdpatt = driver.findElement(primaryNum).getAttribute("value").split("-")[2].length();
				if (firstpatt == 3 && secondpatt == 3 && thirdpatt == 4) {
					tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.PASS,
							"User is able to perform validation for International numbers using plugin");
				} else {
					tcConfig.updateTestReporter("Contact Information Page", "submitWorksheet()", Status.FAIL,
							"User is not able to perform validation for International numbers using plugin");
				}*/
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				new Select(selectPrimaryPhone).selectByIndex(1);
				fieldDataEnter(primaryPhnTxtBox, phoneInput);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*new Select(selectAlterPhone).selectByIndex(2);
				waitForSometime(tcConfig.getConfig().get("MedWait"));*/
				clickElementBy(saveContinueBtn);

			}
		}
		waitUntilElementVisibleBy(driver, continueSummary, "ExplicitWaitLongestTime");
		tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.PASS,
				"User is able to go to Summary Page");
		clickElementBy(continueSummary);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, summaryReview, "ExplicitWaitLongestTime");
		// Values to check
		/*packgePoints = driver.findElement(packagePoints).getText().trim();
		downPayment = Double.parseDouble(driver.findElement(downPayTxt).getText().trim().substring(1).replace(",", ""));
		if (testData.get("DownPaymentPercentage").equals("100%")) {
			String strFinance = driver.findElement(By.xpath("//div[@class='card-body']/table/tbody/tr[3]/td[2]/strong"))
					.getText().trim();
			if (strFinance.equals("-")) {
				System.out.println("No Finance AMount required");
			}
		} else {
			financedAmountUI = Double
					.parseDouble(driver.findElement(financedAmt).getText().trim().substring(1).replace(",", ""));
		}*/

		clickElementBy(summaryReview);
		// String primaryOwner = LeadsPage.strLeadFullName.toLowerCase();
		/*
		 * if (driver.findElement(By.xpath("//strong[text()='" + primaryOwner +
		 * "']")).getText() .equals(primaryOwner)) {
		 * tcConfig.updateTestReporter("Ownership Review Page",
		 * "submitWorksheet()", Status.PASS,
		 * "Primary Owner name is displayed successfully"); }
		 */
		/*waitUntilElementVisibleBy(driver, pointsPurchaseSummary, "ExplicitWaitLongestTime");
		if (packgePoints.equals(driver.findElement(pointsPurchaseSummary).getText())) {
			tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
					"Point package purchase amount is displayed successfully");
		}
		if (verifyElementDisplayed(driver.findElement(maintainanceFee))) {
			tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
					"Maintainance Fee is displayed successfully");
		}*/
		List<WebElement> reviewList = driver.findElements(By.xpath("//label[contains(@for,'review-checkbox')]"));
		for (int i = 1; i <= reviewList.size(); i++) {
			driver.findElement(By.xpath("(//label[contains(@for,'review-checkbox')])[" + i + "]")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> nextList = driver.findElements(By.xpath("//button[text()=' Next ']"));
		for (int j = 1; j <= nextList.size(); j++) {
			if (j == 1) {
				driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (driver.findElement(financeReviewPage).getText().equals("FINANCIAL INFORMATION TODAY")) {
					Double downPaymnet = Double
							.parseDouble(driver.findElement(downPayReviewPage).getText().substring(1).replace(",", ""));
					if (Math.round(downPaymnet) == downPayment) {
						tcConfig.updateTestReporter("Ownership Review Page", "submitWorksheet()", Status.PASS,
								"Down Payment Today is displayed successfully in Summary Page");
					}

					/*
					 * double financedAmount = Double.parseDouble(
					 * driver.findElement(financeAmtReviewPage).getText().
					 * substring(1).replace(",", "")); if
					 * (Math.round(financedAmount) == s) {
					 * tcConfig.updateTestReporter("Ownership Review Page",
					 * "submitWorksheet()", Status.PASS,
					 * "Financed Amount is displayed successfully in Summary Page"
					 * ); }
					 */

					List<WebElement> list = driver
							.findElements(By.xpath("//label[contains(@for,'financial-information-checkbox')]"));
					for (int k = 1; k <= list.size(); k++) {
						driver.findElement(
								By.xpath("(//label[contains(@for,'financial-information-checkbox')])[" + k + "]"))
								.click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
				}
			} else if (j == 2) {
				driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, standardBenefiCheck, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(standardBenefiCheck))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet()", Status.PASS,
							"Standard Benefits are displayed successfully in Summary Page");
				}

				clickElementBy(standardBenChkBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@FindBy(xpath = "(//table/thead/tr/th/strong[text()='Year 1'])[1]")
	protected WebElement year1txt;

	@FindBy(xpath = "(//table/thead/tr/th/strong[text()='Year 2 '])[1]")
	protected WebElement year2txt;

	@FindBy(xpath = "(//table/thead/tr/th/strong[text()=' Year 3 '])[1]")
	protected WebElement year3;

	@FindBy(xpath = "(//span[text()='Bonus Points']/../../i)[1]")
	protected WebElement bonusInfoLink;

	@FindBy(xpath = "(//tab[@id='point-usage']//span[text()='Bonus Points']/../..//following::span)[1]")
	protected WebElement delayText;

	@FindBy(xpath = "//strong[text()='Total Points']/../../td/div/strong[2]")
	protected WebElement totalAmtYear1ForLessDp;

	@FindBy(xpath = "(//tab[@id='point-usage']//strong[text()='Total Points']/..//following::td/strong)[1]")
	protected WebElement totalAmtYear2forLesDP;

	public void validateBonusPointRow(String strDownPay) {
		try {
			if (strDownPay.equals("DownPayGreaterEqual20")) {
				if (verifyElementDisplayed(year1txt) && verifyElementDisplayed(year2txt)
						&& verifyElementDisplayed(year3)) {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validateBonusPointRow", Status.PASS,
							"Years are displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validateBonusPointRow", Status.FAIL,
							"Years are not displayed successfully");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementWb(bonusInfoLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, bonusDetailsPopUp, "ExplicitWaitLongestTime");
				tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
						"Bonus Point details pop up is displayed successfully in Summary Page");
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPoint.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is displayed successfully for year 1");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.FAIL,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is not displayed successfully for year 1");
				}

				if (CWAPitchBenefitDetailsPage.strPoint.equals(bonusPointTxt.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
							"Bonus Points for CWA package " + bonusPointTxt.getText()
									+ " is displayed successfully for year 1");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.FAIL,
							"Bonus Points for CWA package " + bonusPointTxt.getText()
									+ " is not displayed successfully for year 1");
				}

				if (verifyElementDisplayed(
						driver.findElement(By.xpath("(//div[@class='custom-arrow left-align'])[1]")))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
							"Indicator for Bonus Points available in Year 1 will carry over into Year 2 if not used ");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
							"Indicator for Bonus Points available in Year 1 will not over into Year 2 if not used ");
				}

				int strAnnualPointValue = Integer.parseInt(annualPoint.getText().replace(",", ""));
				int strBonusPointValue = Integer.parseInt(bonusPointTxt.getText().replace(",", ""));

				int totalPoints = strAnnualPointValue + strBonusPointValue;

				if (Integer.parseInt(totalPointTxt.getText().replace(",", "")) == totalPoints) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
							"Total Points " + totalPoints + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
							"Total Points " + totalPoints + " is not displayed successfully");
				}
				if (totalPointTxt.getText().trim().equals(year2TotalPointstxt.getText().trim())) {
					/*
					 * if (verifyElementDisplayed(driver.findElement(By.xpath(
					 * "(//strong[text()='≤'])[1]")))) {
					 * tcConfig.updateTestReporter("Standard Benefit Page",
					 * "validateBonusPointRow", Status.PASS,
					 * "Total Points for year 2 " +
					 * year2TotalPointstxt.getText() +
					 * " is displayed successfully"); }else{
					 * tcConfig.updateTestReporter("Standard Benefit Page",
					 * "validateBonusPointRow", Status.FAIL,
					 * "Total Points for year 2 " +
					 * year2TotalPointstxt.getText() +
					 * " is not displayed successfully"); }
					 */
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
							"Total Points for year 2 " + year2TotalPointstxt.getText() + " is displayed successfully");

				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
							"Total Points for year 2 " + year2TotalPointstxt.getText()
									+ " is not displayed successfully");
				}

				if (CWAPitchBenefitDetailsPage.strPoint.equals(year3TotalPointstxt.getText())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
							"Total Points for year 3 " + year3TotalPointstxt.getText() + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
							"Total Points for year 3 " + year3TotalPointstxt.getText()
									+ " is not displayed successfully");
				}
			} else if (strDownPay.equals("DownPayLessEqual20")) {
				if (verifyElementDisplayed(year1txt) && verifyElementDisplayed(year2txt)
						&& verifyElementDisplayed(year3)) {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validateBonusPointRow", Status.PASS,
							"Years are displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validateBonusPointRow", Status.FAIL,
							"Years are not displayed successfully");
				}

				clickElementWb(bonusInfoLink);
				waitUntilElementVisibleBy(driver, bonusDetailsPopUp, "ExplicitWaitLongestTime");
				tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
						"Bonus Point details pop up is displayed successfully in Summary Page");
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPoint.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is displayed successfully for year 1");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.FAIL,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is not displayed successfully for year 1");
				}

			/*	if (delayText.getText().trim().equals("7 Months Delay")) {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
							"7 Month Delay is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.FAIL,
							"7 Month Delay is not displayed successfully");
				}*/

				/*if (CWAPitchBenefitDetailsPage.strPoint.equals(bonusPointforLessDP.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.PASS,
							"Bonus Points for CWA package " + bonusPointforLessDP.getText()
									+ " is displayed successfully for year 1");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "submitWorksheet", Status.FAIL,
							"Bonus Points for CWA package " + bonusPointforLessDP.getText()
									+ " is not displayed successfully for year 1");
				}*/

				/*
				 * if (CWAPitchBenefitDetailsPage.strPoint.equals(totalPointTxt.
				 * getText())) { tcConfig.updateTestReporter(
				 * "Standard Benefit Page", "validateBonusPointRow",
				 * Status.PASS, "Total Points for 7 months delay " +
				 * totalPointTxt.getText() + " is displayed successfully"); }
				 * else { tcConfig.updateTestReporter("Standard Benefit Page",
				 * "validateBonusPointRow", Status.FAIL,
				 * "Total Points for 7 months delay " + totalPointTxt.getText()
				 * + " is not displayed successfully"); }
				 */
//07/2020
/*				int strAnnualPointValue = Integer.parseInt(annualPoint.getText().replace(",", ""));
				int strBonusPointValue = Integer.parseInt(bonusPointforLessDP.getText().replace(",", ""));

				int totalPoints = strAnnualPointValue + strBonusPointValue;

				if (Integer.parseInt(totalAmtYear1ForLessDp.getText().replace(",", "")) == totalPoints) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
							"Total Points " + totalPoints + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
							"Total Points " + totalPoints + " is not displayed successfully");
				}

				if (Integer.parseInt(totalAmtYear1ForLessDp.getText().replace(",", "")) == Integer
						.parseInt(totalAmtYear2forLesDP.getText().replace(",", ""))) {

					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
							"Total Points for year 2 " + totalAmtYear2forLesDP.getText()
									+ " is displayed successfully");

				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
							"Total Points for year 2 " + totalAmtYear2forLesDP.getText()
									+ " is not displayed successfully");
				}

				if (CWAPitchBenefitDetailsPage.strPoint.equals(year3TotalPointstxtforLessDP.getText())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.PASS,
							"Total Points for year 3 " + year3TotalPointstxtforLessDP.getText()
									+ " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
							"Total Points for year 3 " + year3TotalPointstxtforLessDP.getText()
									+ " is not displayed successfully");
				}*/

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validateBonusPointRow", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	@FindBy(xpath = "(//span[text()='PIC Plus']/../../i)[1]")
	protected WebElement picPlusLink;

	public void validatePICPlusRow(String strDownPay) {
		try {
			if (strDownPay.equals("DownPayGreaterEqual20")) {
				if (verifyElementDisplayed(year1txt) && verifyElementDisplayed(year2txt)) {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validatePICPlusRow", Status.PASS,
							"Years are displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validatePICPlusRow", Status.FAIL,
							"Years are not displayed successfully");
				}

				clickElementWb(picPlusLink);
				waitUntilElementVisibleBy(driver, picPlusDetPopUp, "ExplicitWaitLongestTime");
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.PASS,
						"Pic Plus details pop up is displayed successfully in Summary Page");
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPoint.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.PASS,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is displayed successfully for year 1");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.FAIL,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is not displayed successfully for year 1");
				}

				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPointYear2.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.PASS,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is displayed successfully for year 2");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.FAIL,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is not displayed successfully for year 2");
				}

				if (/*
					 * TourBenefitPage.strStudioTxt.equals(picPlusTxt.getText().
					 * trim())
					 */
				verifyElementDisplayed(picPlusTxt)) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.PASS,
							"Pic Plus Points for CWA package " + picPlusTxt.getText()
									+ " is displayed successfully for year 1");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.FAIL,
							"Bonus Points for CWA package " + picPlusTxt.getText()
									+ " is not displayed successfully for year 1");
				}

				if (/*
					 * TourBenefitPage.strStudioTxt.equals(picPlusYear2.getText(
					 * ).trim())
					 */
				verifyElementDisplayed(picPlusYear2)) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.PASS,
							"Pic Plus Points for CWA package " + picPlusYear2.getText()
									+ " is displayed successfully for year 2");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.FAIL,
							"Bonus Points for CWA package " + picPlusYear2.getText()
									+ " is not displayed successfully for year 2");
				}

				int strAnnualPointValueYr1 = Integer.parseInt(annualPoint.getText().replace(",", ""));
				int strPICPlusValueYr1 = Integer.parseInt(picPlusTxt.getText().replace(",", ""));

				int strAnnualPointValueYr2 = Integer.parseInt(annualPointYear2.getText().replace(",", ""));
				int strPICPlusValueYr2 = Integer.parseInt(picPlusYear2.getText().replace(",", ""));

				int totalPointsYr1 = strAnnualPointValueYr1 + strPICPlusValueYr1;
				int totalPointsYr2 = strAnnualPointValueYr2 + strPICPlusValueYr2;

				if (Integer.parseInt(totalPointTxt.getText().replace(",", "")) == totalPointsYr1) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.PASS,
							"Total Points " + totalPointsYr1 + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.FAIL,
							"Total Points " + totalPointsYr1 + " is not displayed successfully");
				}

				if (Integer.parseInt(year2TotalPointstxt.getText().replace(",", "")) == totalPointsYr2) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.PASS,
							"Total Points " + totalPointsYr2 + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.FAIL,
							"Total Points " + totalPointsYr2 + " is not displayed successfully");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validatePICPlusRow", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	@FindBy(xpath = "(//span[text()='PIC Express']/../../i)[1]")
	protected WebElement picExpressLink;

	public void validatePICExpressRow(String strDownPay) {
		try {
			if (strDownPay.equals("DownPayGreaterEqual20")) {
				if (verifyElementDisplayed(year1txt) && verifyElementDisplayed(year2txt)
						&& verifyElementDisplayed(year3txt) && verifyElementDisplayed(year5txt)) {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validatePICExpressRow", Status.PASS,
							"Years are displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWA Package Summary Page", "validatePICExpressRow", Status.FAIL,
							"Years are not displayed successfully");
				}

				clickElementWb(picExpressLink);
				waitUntilElementVisibleBy(driver, picExpressDetPopUp, "ExplicitWaitLongestTime");
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
						"Pic Express details pop up is displayed successfully in Summary Page");
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPoint.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is displayed successfully for year 1");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Annual Points for CWA package " + annualPoint.getText()
									+ " is not displayed successfully for year 1");
				}

				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPointYear2.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is displayed successfully for year 2");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is not displayed successfully for year 2");
				}

				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPointYear3.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is displayed successfully for year 3");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is not displayed successfully for year 3");
				}

				if (CWAPitchBenefitDetailsPage.strPoint.equals(annualPointYear5.getText().trim())) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is displayed successfully for year 5");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Annual Points for CWA package " + annualPointYear2.getText()
									+ " is not displayed successfully for year 5");
				}

				String totalPointsYr1 = CWAPitchBenefitDetailsPage.strPoint;
				String totalPointsYr2 = CWAPitchBenefitDetailsPage.strPoint;
				String totalPointsYr3 = CWAPitchBenefitDetailsPage.strPoint;
				String totalPointsYr5 = CWAPitchBenefitDetailsPage.strPoint;

				if (totalPointTxt.getText().trim().equals(totalPointsYr1)) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Total Points " + totalPointsYr1 + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Total Points " + totalPointsYr1 + " is not displayed successfully");
				}

				if (year2TotalPointstxt.getText().trim().equals(totalPointsYr2)) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Total Points " + totalPointsYr2 + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Total Points " + totalPointsYr2 + " is not displayed successfully");
				}

				if (year3TotalPointstxt.getText().trim().equals(totalPointsYr3)) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Total Points " + totalPointsYr3 + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Total Points " + totalPointsYr3 + " is not displayed successfully");
				}

				if (year5TotalPointstxt.getText().trim().equals(totalPointsYr5)) {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.PASS,
							"Total Points " + totalPointsYr5 + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
							"Total Points " + totalPointsYr5 + " is not displayed successfully");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressRow", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	
	protected By vipProgramTab = By.xpath("//span[text()='VIP']");
	protected By VIPTier = By.xpath("//li[text()=' silver ']");
	protected By priTierChamp = By.xpath("//li[text()='champion']");
	protected By priTierStandard = By.xpath("//li[text()='standard']");

	// protected By bonusInfoLink = By.xpath("(//i[@id='bonusPoints'])[2]");
	public void VIPProgramforBonusPoint() {
		try {
			waitUntilElementVisibleBy(driver, vipProgramTab, "ExplicitWaitLongestTime");
			clickElementBy(vipProgramTab);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * waitUntilElementVisibleBy(driver, bonusInfoLink, "ExplicitWaitLongestTime");
			 * clickElementWb(bonusInfoLink); waitUntilElementVisibleBy(driver,
			 * bonusDetailsPopUp, "ExplicitWaitLongestTime"); tcConfig.updateTestReporter(
			 * "Standard Benefit Page", "validateBonusPointRow", Status.PASS,
			 * "Bonus Point details pop up is displayed successfully in Summary Page"
			 * ); waitUntilElementVisibleBy(driver, okBtn, "ExplicitWaitLongestTime");
			 * driver.findElement(okBtn).click();
			 */

			if (CWAPitchBenefitDetailsPage.strPoint.equals("200,000")) {
				if (verifyElementDisplayed(driver.findElement(VIPTier))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.PASS,
							"VIP Tier " + getElementText(VIPTier) + " is displayed");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
							"VIP Tier " + getElementText(VIPTier) + " is not displayed");
				}

				if (verifyElementDisplayed(driver.findElement(priTierChamp)) && verifyElementDisplayed(driver.findElement(priTierStandard))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.PASS,
							"Privilage Tier " + getElementText(priTierChamp) + " and " + getElementText(priTierStandard)
									+ " is displayed");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
							"Privilage Tier " + getElementText(priTierChamp) + " and " + getElementText(priTierStandard)
									+ " is not displayed");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}


	protected By vipTierStandard = By.xpath("//li[text()=' Standard ']");

	public void VIPProgramforPICPlus() {
		try {
			clickElementBy(vipProgramTab);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (CWAPitchBenefitDetailsPage.strPoint.equals("200,000")) {
				if (verifyElementDisplayed(driver.findElement(vipTierStandard))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.PASS,
							"VIP Tier " + getElementText(vipTierStandard) + " is displayed");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
							"VIP Tier " + getElementText(vipTierStandard) + " is not displayed");
				}

				if (verifyElementDisplayed(driver.findElement(priTierStandard))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.PASS,
							"Privilage Tier" + getElementText(priTierStandard) + " is displayed");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
							"Privilage Tier " + getElementText(priTierStandard) + " is not displayed");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	public void VIPProgramforPICExpress() {
		try {
			clickElementBy(vipProgramTab);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (CWAPitchBenefitDetailsPage.strPoint.equals("200,000")) {
				if (verifyElementDisplayed(driver.findElement(vipTierStandard))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.PASS,
							"VIP Tier " + getElementText(vipTierStandard) + " is displayed");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
							"VIP Tier " + getElementText(vipTierStandard) + " is not displayed");
				}

				if (verifyElementDisplayed(driver.findElement(priTierStandard))) {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.PASS,
							"Privilage Tier" + getElementText(priTierStandard) + " is displayed");
				} else {
					tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
							"Privilage Tier " + getElementText(priTierStandard) + " is not displayed");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "VIPProgramforBonusPoint", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	protected By closeIcon = By.xpath("//li[contains(@class,'close-icon')]");
	protected By labelRCI = By.xpath("//table[contains(@class,'tours-credit-table')]/thead/tr/th[1]/span");
	protected By labelResortName = By.xpath("//table[contains(@class,'tours-credit-table')]/thead/tr/th[2]/span");
	protected By labelCountry = By.xpath("//table[contains(@class,'tours-credit-table')]/thead/tr/th[3]/span");
	protected By labelFrequency = By.xpath("//table[contains(@class,'tours-credit-table')]/thead/tr/th[4]/span");
	protected By labelUnitType = By.xpath("//table[contains(@class,'tours-credit-table')]/thead/tr/th[5]/span");
	protected By labelUnitCongfig = By.xpath("//table[contains(@class,'tours-credit-table')]/thead/tr/th[6]/span");

	protected By resortValue = By.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td/strong)[1]");
	protected By resortName = By.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td/strong)[2]");
	protected By countryName = By.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td/strong)[3]");
	protected By frequencyValue = By.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td/strong)[4]");
	protected By unitType = By.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td/strong)[5]");
	protected By unitConfig = By.xpath("(//table[contains(@class,'tours-credit-table')]/tbody/tr/td/strong)[6]");

	public void validatePICExpressAtSummary() {
		String resortNumber = CWAPitchBenefitDetailsPage.resortIDNumber
				.get(testData.get("ResortID"));
		try {
			waitUntilElementVisibleBy(driver,
					By.xpath("(//span[contains(.,'" + resortNumber + "')])[1]"), "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver
					.findElement(By.xpath("(//span[contains(.,'" + resortNumber + "')])[1]")))) {
				driver.findElement(By.xpath("(//span[contains(.,'" + resortNumber + "')])[1]"))
						.click();
				tcConfig.updateTestReporter("Standard Benefit Page", "click_resubmit_button", Status.PASS,
						"Resort ID " + resortNumber + " is displayed");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String RCI = driver.findElement(labelRCI).getText();
			if (RCI.equals(testData.get("RCI")) && verifyElementDisplayed(driver.findElement(labelRCI))
					&& driver.findElement(resortValue).getText().equals(resortNumber)) {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.PASS,
						"RCI label with Value " + resortNumber + " is displayed succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.FAIL,
						"RCI label is not displayed succesfully");
			}

			String ResortName = driver.findElement(labelResortName).getText();
			if (ResortName.equals(testData.get("ResortName1"))
					&& verifyElementDisplayed(driver.findElement(labelResortName))
					&& driver.findElement(resortName).getText().equals(testData.get("strResortName"))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.PASS,
						"labelResortName label with the Value " + driver.findElement(resortName).getText()
								+ " is displayed succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.FAIL,
						"labelResortName label is not displayed succesfully");
			}

			String Country = driver.findElement(labelCountry).getText();
			if (Country.equals(testData.get("CountryLabel")) && verifyElementDisplayed(driver.findElement(labelCountry))
					&& driver.findElement(countryName).getText().equals(testData.get("strCountry"))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.PASS,
						"Country Name label with the Value " + driver.findElement(countryName).getText()
								+ "is displayed succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.FAIL,
						"Country Name label is not displayed succesfully");
			}

			String Frequency = driver.findElement(labelFrequency).getText();
			if (Frequency.equals(testData.get("Frequency"))
					&& verifyElementDisplayed(driver.findElement(labelFrequency))
					&& driver.findElement(frequencyValue).getText().equals("Annual")) {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.PASS,
						"Frequency label with the Value " + driver.findElement(frequencyValue).getText()
								+ " is displayed succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.FAIL,
						"Frequency label is not displayed succesfully");
			}

			String UnitType = driver.findElement(labelUnitType).getText();
			if (UnitType.equals(testData.get("UnitType")) && verifyElementDisplayed(driver.findElement(labelUnitType))
					&& driver.findElement(unitType).getText().equals(testData.get("RoomType"))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.PASS,
						"UnitType label with the value " + driver.findElement(unitType).getText()
								+ " is displayed succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.FAIL,
						"UnitType label is not displayed succesfully");
			}

			String UnitConfig = driver.findElement(labelUnitCongfig).getText();
			if (UnitConfig.equals(testData.get("strUnitConfig"))
					&& verifyElementDisplayed(driver.findElement(labelUnitCongfig))
					&& driver.findElement(unitConfig).getText().equals(testData.get("UnitConfig"))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.PASS,
						"UnitConfig label with the value " + driver.findElement(unitConfig).getText()
								+ " is displayed succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.FAIL,
						"UnitConfig label is not displayed succesfully");
			}

			/*
			 * * String Points = driver.findElement(labelPoints).getText();
			 * if(Points.equals(testData.get("UnitConfig")) &&
			 * verifyElementDisplayed(driver.findElement(labelPoints))){
			 * tcConfig.updateTestReporter("Standard Benefit Page",
			 * "validatePICExpressAtSummary", Status.PASS,
			 * "Points label is displayed succesfully"); } else {
			 * tcConfig.updateTestReporter("Standard Benefit Page",
			 * "validatePICExpressAtSummary", Status.FAIL,
			 * "Points label is not displayed succesfully"); }
			 */

			driver.findElement(closeIcon).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validatePICExpressAtSummary", Status.FAIL,
					"failed in validatePICExpressAtSummary" + e.getMessage());

		}

	}

	@FindBy(xpath = "")
	protected By resubmitWorksheetButton = By.xpath("//button[contains(.,' Re-Submit Worksheet ')]");

	
	protected By recallButton = By.xpath("//button[contains(.,' Recall Worksheet ')]");

	public void recallWorksheet() {

		List<WebElement> nextList = driver.findElements(By.xpath("//button[text()=' Next ']"));
		for (int j = 1; j <= nextList.size(); j++) {

			driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}
		waitUntilElementVisibleBy(driver, recallButton, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(recallButton))) {
			tcConfig.updateTestReporter("Standard Benefit Page", "RecallWorksheet", Status.PASS,
					getElementText(recallButton)+ " is displayed");
			clickElementBy(recallButton);
		} else {
			tcConfig.updateTestReporter("Standard Benefit Page", "RecallWorksheet", Status.FAIL,
					getElementText(recallButton) + " is not displayed");
		}
	}

	
	protected By recallWindow = By.xpath("//span[contains(.,'Do you want to Recall this Worksheet?')]");

	
	protected By buttonRecallToSummary = By.xpath("//button[contains(.,'Yes - Recall & Go to Summary')]");

	protected By buttonRecallToPitchTile = By.xpath("//button[contains(.,'Yes - Recall & Go to Presentation Tile')]");
	protected By buttonNoBackToSummary = By.xpath("//button[contains(.,'No - Go Back to Summary')]");

	public void validate_recall_to_summary() {
		try {
			waitUntilElementVisibleBy(driver, recallWindow, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(recallWindow))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "validate recall screen", Status.PASS,
						getElementText(recallWindow) + " is displayed");
			}

			clickElementBy(buttonNoBackToSummary);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(recallButton))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "RecallWorksheet", Status.PASS,
						"no back to summary button is displayed succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "RecallWorksheet", Status.FAIL,
						"no back to summary button is not displayed");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(recallButton);
		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validate_recall_screen", Status.FAIL,
					"Failed in Method" + e.getMessage());

		}

	}

	protected By yesRecallSummaryBtn = By.xpath("//button[text()='Yes - Recall & Go to Summary']");

	public void recallSummary() {
		try {
			waitUntilElementVisibleBy(driver, yesRecallSummaryBtn, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(yesRecallSummaryBtn))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "recallSummary", Status.PASS,
						"Recall to summary is displayed");
			}
			driver.findElement(yesRecallSummaryBtn).click();
		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "recallSummary", Status.FAIL,
					"Recall to summary is not displayed" + e.getMessage());
		}
	}

	protected By yesRecallPresentationBtn = By.xpath("//button[text()='Yes - Recall & Go to Presentation Tile']");

	public void recalltoPresentation() {
		try {
			waitUntilElementVisibleBy(driver, yesRecallPresentationBtn, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(yesRecallPresentationBtn))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "recallSummary", Status.PASS,
						"Recall to Presentation button is displayed");
			}
			driver.findElement(yesRecallPresentationBtn).click();
		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "recallSummary", Status.FAIL,
					"Recall to Presentation button is not displayed" + e.getMessage());
		}
	}

	public void validate_recall() {
		try {
			waitUntilElementVisibleBy(driver, buttonRecallToSummary, "ExplicitWaitLongestTime");
			clickElementBy(buttonRecallToSummary);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> nextList = driver.findElements(By.xpath("//button[text()=' Next ']"));
			for (int j = 1; j <= nextList.size(); j++) {

				driver.findElement(By.xpath("(//button[text()=' Next '])[" + j + "]")).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}
			waitUntilElementVisibleBy(driver, resubmitWorksheetButton, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(resubmitWorksheetButton))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "RecallWorksheet", Status.PASS,
						"clicked resubmit worksheet succesfully");
			} else {
				tcConfig.updateTestReporter("Standard Benefit Page", "RecallWorksheet", Status.FAIL,
						"not clicked resubmit worksheet succesfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validate_recall_screen", Status.FAIL,
					"Failed in Method" + e.getMessage());

		}

	}

	public void validate_recall_to_workbook() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, buttonRecallToPitchTile, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(buttonRecallToPitchTile))) {
			tcConfig.updateTestReporter("Standard Benefit Page", "validate_recall_to_workbook", Status.PASS,
					"clicked return to pitch tile succesfully");
		}
		clickElementBy(buttonRecallToPitchTile);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	protected By contiueSummary = By.xpath("//button[contains(.,' Submit Worksheet ')]");

	public void click_submit_resubmit_button() {

		waitUntilElementVisibleBy(driver, contiueSummary, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(contiueSummary))) {
			tcConfig.updateTestReporter("Standard Benefit Page", "click_submit_button", Status.PASS,
					"submit button clicked succesfully");
			clickElementBy(contiueSummary);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

		else if (verifyElementDisplayed(driver.findElement(resubmitWorksheetButton))) {
			tcConfig.updateTestReporter("Standard Benefit Page", "click_resubmit_button", Status.PASS,
					"re-submit button clicked succesfully");
			clickElementBy(resubmitWorksheetButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

	}

	public void click_resubmit_button() {
		try {
			waitUntilElementVisibleBy(driver, resubmitWorksheetButton, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(resubmitWorksheetButton))) {
				tcConfig.updateTestReporter("Standard Benefit Page", "click_resubmit_button", Status.PASS,
						"re-submit button clicked succesfully");
				clickElementBy(resubmitWorksheetButton);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Standard Benefit Page", "click_submit_button", Status.FAIL,
					"submit/re-submit button not clicked succesfully" + e.getMessage());

		}

	}
}
