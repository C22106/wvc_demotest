package eWorksheet.pages;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointLoginPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(SalePointLoginPage.class);

	public SalePointLoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(name = "j_username")
	WebElement textUsername;
	String strusernameBk="";
	/*
	 * enter user name
	 */
	protected void setUserName(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsername, 10);
		textUsername.sendKeys(strUserName);
		strusernameBk=strUserName;
		tcConfig.updateTestReporter("SalePointLoginPage", "setUserName", Status.PASS,
				"Username is entered for SalePoint Login");
	}

	@FindBy(name = "j_password")
	WebElement textPassword;

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	protected void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		
		
		textPassword.sendKeys(dString);
	
		tcConfig.updateTestReporter("SalePointLoginPage", "setPassword", Status.PASS,
				"Entered password for SalePoint login");
	}

	@FindBy(xpath = "//input[@name='logon']")
	WebElement buttonLogIn;

	public void loginToApp(String strBrowser) throws Exception {
		System.out.println("");
		// testData.put("testTry", "Monideep");
		// String strURL = testData.get("URL");
		// String strUserName = testData.get("UserID");
		this.driver=checkAndInitBrowser(strBrowser);

		driver.navigate().to(testData.get("URLSP"));
		//driver.manage().window().maximize();
		//waitUntilElementVisible(driver, textUsername, 15);
		setUserName(testData.get("UserIDSP"));
		setPassword(testData.get("PasswordSP"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", buttonLogIn);

		// buttonLogIn.click();
		tcConfig.updateTestReporter("SalePointLoginPage", "loginToApp", Status.PASS, "Log in button clicked");
		// waitUntilElementVisible(tcConfig.getDriver(), linkMangInv, 10000);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
	}

	@FindBy(xpath = "//img[@src='/webapp/ccis/images/log_off.gif']")
	WebElement LogoutBtn;

	public void Logout() throws Exception {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", LogoutBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(buttonLogIn)) {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL, "Log Out Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL,
					"Log Out Failed" + e.getMessage());
		}
		// driver.close();
		// driver.quit();
	}

}
