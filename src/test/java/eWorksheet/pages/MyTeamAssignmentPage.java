package eWorksheet.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class MyTeamAssignmentPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(MyTeamAssignmentPage.class);

	public MyTeamAssignmentPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	HomePage home = new HomePage(tcConfig);
	protected By salesStoreDropDwn = By.xpath("//h3[text()='Team Identification']/..//select");

	public void selectSalesStore() {
		try {
			waitUntilElementVisibleBy(driver, salesStoreDropDwn, 120);
			if (verifyElementDisplayed(driver.findElement(salesStoreDropDwn))) {
				new Select(driver.findElement(salesStoreDropDwn)).selectByVisibleText(testData.get("DefaultSalesSite"));
				tcConfig.updateTestReporter("MyTeamAssignmentPage", "selectSalesStore", Status.PASS,
						"Sales Store is selected as"
								+ new Select(driver.findElement(salesStoreDropDwn)).getFirstSelectedOption());
			} else {
				tcConfig.updateTestReporter("MyTeamAssignmentPage", "selectSalesStore", Status.FAIL,
						"Sales Store is not selected");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("MyTeamAssignmentPage", "selectSalesStore", Status.FAIL,
					"Sales Store is not selected" + e.getMessage());
		}
	}

	protected By txtTeamName = By.xpath("//input[@id='sales-team-name']");

	public void selectTeamName() {
		try {
			driver.findElement(txtTeamName).clear();
			driver.findElement(txtTeamName).sendKeys("eCredit");
		} catch (Exception e) {
			e.getMessage();
		}
	}

	protected By teamEndDate = By.xpath("//input[@formcontrolname='teamEndDate']");

	public void teamEndDate() {
		String strDate = testData.get("TourEndDate");
		try {
			home.selectDatePicker(teamEndDate, strDate);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	protected By teamColor = By.xpath("//span[@class='checkmark yellow']");

	public void selectTeamColor() {
		try {
			driver.findElement(teamColor).click();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	protected By salesAgentList = By.xpath("//div[contains(@class,'sales-agent')]/p");
	protected By arrowBtn = By.xpath("//button/span[text()='>']");
	protected By saveBtn = By.xpath("//button[text()='Save']");
	public void selectSalesAgent() {
		try {
			waitUntilElementVisibleBy(driver, salesAgentList, 120);
			if (verifyElementDisplayed(driver.findElement(salesAgentList))) {
				List<WebElement> allList = driver.findElements(salesAgentList);
				for (int i = 1; i <= 2; i++) {
					allList.get(i).click();
					tcConfig.updateTestReporter("MyTeamAssignmentPage", "selectSalesAgent", Status.PASS,
							"Sales agent is selected " + allList.get(i).getText());
				}

			}else{
				tcConfig.updateTestReporter("MyTeamAssignmentPage", "selectSalesAgent", Status.FAIL,
						"Not bale to select the Sales Agent name");
			}
			driver.findElement(arrowBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(saveBtn).click();
		} catch (Exception e) {
			tcConfig.updateTestReporter("MyTeamAssignmentPage", "selectSalesAgent", Status.FAIL,
					"Not bale to select the Sales Agent name"+e.getMessage());
		}
	}
	
	protected By homeLogoBtn = By.xpath("//img[@alt='navigator-logo']");
	protected By allToursBtn = By.xpath("//a[contains(text(),' All Tours')]");
	public void clickHomeButton()
	{
		try{
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, homeLogoBtn, 120);
			driver.findElement(homeLogoBtn).click();
			waitUntilElementVisibleBy(driver, allToursBtn, 120);
			
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("MyTeamAssignmentPage", "clickHomeButton", Status.FAIL,
					"Failed in Method"+e.getMessage());
		}
	}
}
