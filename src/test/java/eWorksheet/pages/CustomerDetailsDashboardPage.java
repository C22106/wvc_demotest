package eWorksheet.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CustomerDetailsDashboardPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(CustomerDetailsDashboardPage.class);

	public CustomerDetailsDashboardPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	
	
	
	protected By SelectLink = By.xpath("//li[@role='menuitem']/a[contains(text(),'Select')]");
	protected By selectBtn = By.xpath("//button[text()='Select']");
	protected By WSALabel = By.xpath("//span[starts-with(text(),'WSA')]");
	protected By cwaPackagePoints = By
			.xpath("//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong");
	protected By retrieveLink = By.xpath("//a[text()='Retrieve']");
	protected By lookUpDrpDwn = By.xpath("//select[@name='searchType']");
	protected By refNumTxt = By.xpath("//input[@id='controlNumber']");
	protected By submitBtn = By.xpath("//button[@name='lu-submit']");
	protected By refNumColVal = By.xpath("//table[@id='lookupResultsTable']/tbody/tr/td[1]");
	protected By clickToView = By.xpath("//table[@id='lookupResultsTable']/tbody/tr/td[6]//img[@title='Click to View']");
	protected By summaryLabel = By.xpath("//div[contains(text(),'Summary')]");
	protected By privacyLabel = By.xpath("//div[@class='down-payment-footer']/p");
	protected By ownersPrivacyLink = By.xpath("(//div[@class='legal-name-footer']/p)[1]");
	protected By clickHereLink = By.xpath("//div[@class='down-payment-footer']/p/a");
	protected By OwnerclickHereLink = By.xpath("(//div[@class='legal-name-footer']/p/a)[1]");
	protected By privacyModalWindow = By.xpath("//h3[text()=' Wyndham Privacy Statement ']");
	protected By iconCloseBtn = By.xpath("//h3[text()=' Wyndham Privacy Statement ']//following::img[@alt='icon-close']");
	protected By cashBtn = By.xpath("(//ul[@class='list-inline select-payment-list']/li/span)[3]");
	protected By addPayBtn = By.xpath("(//div[@class='card-footer down-payment-card-footer']//following-sibling::button[text()='Add Payment'])[3]");
	protected By continueFinanceBtn = By.xpath("//button[text()='Continue to Financing']");
	protected By continueOwnBtn = By.xpath("//button[text()='Continue to Owners']");
	protected By addressLink = By.xpath("(//div[contains(@class,'personal-information')]//following::button[text()=' Address '])[1]");
	protected By contactInfoLink = By.xpath("//button[text()=' Contact Information ']");
	protected By custAddChkBox = By.xpath("//label[text()='Continue with the Customer supplied address:']//following::input[@id='0']/../label");
	@FindBy(xpath="//select[@formcontrolname='primaryPhoneType']") WebElement selectPrimaryPhone;
	@FindBy(xpath="//select[@formcontrolname='alternatePhoneType']") WebElement selectAlterPhone;
	protected By saveContinueBtn = By.xpath("//button[text()=' Save and Continue ']");
	protected By continueSummary = By.xpath("//button[text()=' Continue to Summary ']");
	protected By standardBenefitChkBx = By.xpath("//label[@for='standard-benefits-checkbox']");
	
	public String strwsaRefNum;
	public String strfinancedAmt;
	public String strNoOfPayments;
	public String strInterestRate;
	public String strLoanPayment;
	public String strMonMntcFee;
	
	
	public void generateWSARefNumber() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(SelectLink).click();
			tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
					"CWA Package is selected successfully");
			waitUntilElementLocated(driver, selectBtn, 120);
			driver.findElement(selectBtn).click();
			tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
					"CWA Point Package is selected successfully");
			waitUntilElementLocated(driver, WSALabel, 120);
			String str[] = driver.findElement(WSALabel).getText().substring(5).split(" ");
			strwsaRefNum = str[0];
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (strwsaRefNum.isEmpty() == false) {
				tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.PASS,
						"WSA Reference number is generated successfully");
			} else {
				tcConfig.updateTestReporter("CWAPackageWSAPage", "generateWSARefNumber", Status.FAIL,
						"WSA Reference number is not generated successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("CWAPackageWSAPage", "generateWSARefNumber", Status.FAIL,
					"Failed due to : " + e.getMessage());

		}

	}
	
	public void getFinanceValuefromCWAPackage() {
		String strPoint = testData.get("TourPoints");
		try {
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By
							.xpath("(//div[@class='card-footer']/a[1][contains(text(),' More Details ')])[" + j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPackageWSAPage", "clickCWAPackage", Status.PASS,
							"More Details link is clicked successfully");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("(//li/a/span[contains(text(),'Financial')])[" + j + "]")).click();
					tcConfig.updateTestReporter("CWAPackageWSAPage", "clickCWAPackage", Status.PASS,
							"Financial Tab is clicked successfully");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement baseTable = driver
							.findElement(By.xpath("(//tab[@ng-reflect-heading='Financial'])[" + j + "]/table/tbody"));
					strfinancedAmt = 	getValuefromTable(baseTable, 0, 1).substring(1).replace(",", "").trim();
					strNoOfPayments = 	getValuefromTable(baseTable, 1, 1).trim();
					strInterestRate = 	getValuefromTable(baseTable, 2, 1).trim();
					strLoanPayment = 	getValuefromTable(baseTable, 3, 1).substring(1).replace(",", "").trim();
					strMonMntcFee = 	getValuefromTable(baseTable, 4, 1).substring(1).replace(",", "").trim();

					break;
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("CWAPackageWSAPage", "getFinanceValuefromCWAPackage",
					Status.FAIL, "Failed due to : " + e.getMessage());
		}
	}
	
	public void tourLookUp() {
		
		try {
			driver.findElement(retrieveLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Select lookUpObj = new Select(driver.findElement(lookUpDrpDwn));
			lookUpObj.selectByVisibleText("Reference Number");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(refNumTxt))) {
				driver.findElement(refNumTxt).sendKeys(strwsaRefNum);
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
						"Reference Number entered Successfully");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Reference Number not entered Successfully");
			}
			driver.findElement(submitBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (driver.findElement(refNumColVal).getText().trim().equals(strwsaRefNum)) {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
						"Reference Number lookup result is displayed successfully");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(clickToView).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (verifyElementDisplayed(driver.findElement(summaryLabel))) {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
							"New Proposal page is displayed successfully");
				} else {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
							"New Proposal page is not displayed successfully");
				}
				String str[] = driver.findElement(By.xpath("//h4[text()='Loan Information']/../div[2]/p")).getText()
						.split("\n");
				String str1[] = str[2].split(" ");
				String str2[] = str[1].split(" ");
				String str3[] = str[3].split(" ");
				double financeAmt = Double.parseDouble(str1[3].substring(1).replace(",", ""));
				int financeAmtUI = (int)Math.round(financeAmt);
				String financedAmountUI = String.valueOf(financeAmtUI);
				String strInterestRt = str2[3].trim();
				String strTerm = str3[3].trim();
				if (financedAmountUI.equals(strfinancedAmt) && strInterestRt.equals(strInterestRate)
						&& strNoOfPayments.equals(strTerm)) {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS,
							"WSA Reference Number is displayed Successfully");
				} else {
					tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
							"WSA Reference Number is not displayed Successfully");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
					"WSA Reference Number is not displayed Successfully");
		}
	}
	
	public void checkLinkDownPayPage() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			privacyModalWindow();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(cashBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(addPayBtn);

		} catch (Exception e) {
			tcConfig.updateTestReporter("Down Payment Page", "checkLinkDownPayPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}
	
	public void financingSummaryPage() {

		try {
			waitUntilElementVisibleBy(driver, continueFinanceBtn, 120);
			if (driver.findElement(continueFinanceBtn).isEnabled()) {
				clickElementBy(continueFinanceBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("Finance Summary Page", "financingSummaryPage()", Status.PASS,
						"Page Successfully redirected to Finance Page");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			privacyModalWindow();
		} catch (Exception e) {
			tcConfig.updateTestReporter("Finance Summary Page", "financingSummaryPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}
	
	public void OwnerInformationPage() {
		try {
			if (driver.findElement(continueOwnBtn).isEnabled()) {
				clickElementBy(continueOwnBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ownerPrivacyModalWindow();
				waitUntilElementVisibleBy(driver, addressLink, 120);
				clickElementBy(addressLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ownerPrivacyModalWindow();
				clickElementBy(contactInfoLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ownerPrivacyModalWindow();
				clickElementBy(custAddChkBox);
				if (driver.findElement(contactInfoLink).isEnabled()) {
					clickElementBy(contactInfoLink);
					ownerPrivacyModalWindow();
					new Select(selectPrimaryPhone).selectByIndex(1);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					new Select(selectAlterPhone).selectByIndex(2);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(saveContinueBtn);
				}
			}
			waitUntilElementVisibleBy(driver, continueSummary, 120);
			tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.PASS,
					"User is able to go to Summary Page");
			clickElementBy(continueSummary);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("Owner Information Page", "OwnerInformationPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}
	
	public void summaryPage()
	{
		try{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			privacyModalWindow();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> nextList = driver.findElements(By.xpath("//button[text()=' Next ']"));
			for(int i=0;i<nextList.size();i++)
			{
				nextList.get(i).click();
			}
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("Summary Page", "summaryPage()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}
	
	public void privacyModalWindow()
	{
		String strPrivacyLinkLabel;
		try{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyElementDisplayed(driver.findElement(privacyLabel)))
			{
				strPrivacyLinkLabel = driver.findElement(privacyLabel).getText().trim();
				System.out.println(strPrivacyLinkLabel);
				if (strPrivacyLinkLabel.equals("For Wyndham’s Privacy Notice, click here")) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Label for Privacy link is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.FAIL,
							"Label for Privacy link is not displayed successfully");
				}
				clickElementBy(clickHereLink);
				waitUntilElementVisibleBy(driver, privacyModalWindow, 120);
				if (verifyElementDisplayed(driver.findElement(privacyModalWindow))) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Privacy Modal Window is displayed successfully");
					clickElementBy(iconCloseBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			}
			
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("Summary Page", "privacyModalWindow()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}
	
	public void ownerPrivacyModalWindow()
	{
		String strPrivacyLinkLabel;
		try{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyElementDisplayed(driver.findElement(ownersPrivacyLink)))
			{
				strPrivacyLinkLabel = driver.findElement(ownersPrivacyLink).getText().trim();
				System.out.println(strPrivacyLinkLabel);
				if (strPrivacyLinkLabel.equals("For Wyndham’s Privacy Notice, click here")) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Label for Privacy link is displayed successfully");
				} else {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.FAIL,
							"Label for Privacy link is not displayed successfully");
				}
				clickElementBy(OwnerclickHereLink);
				waitUntilElementVisibleBy(driver, privacyModalWindow, 120);
				if (verifyElementDisplayed(driver.findElement(privacyModalWindow))) {
					tcConfig.updateTestReporter("Privacy Modal Window", "privacyModalWindow()", Status.PASS,
							"Privacy Modal Window is displayed successfully");
					clickElementBy(iconCloseBtn);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			}
			
		}catch(Exception e)
		{
			tcConfig.updateTestReporter("Summary Page", "privacyModalWindow()", Status.FAIL,
					"Privacy Modal Window is not displayed successfully");
		}
	}

}
