package eWorksheet.pages;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import eWorksheet.pages.CustomerDetailsPage;
import eWorksheet.pages.PersonalInformationPage;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class PersonalInformationPage_Ipad extends PersonalInformationPage {

	public static final Logger log = Logger.getLogger(PersonalInformationPage_Ipad.class);

	public PersonalInformationPage_Ipad(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcConfig);
	protected By tabCount = By.xpath("//div[@class='prospect-btn-row']/button");
	protected By primaryTab = By.xpath("//div[@class='prospect-btn-row']/button[@class='btn prospect enabled primary']");
	protected By popupmsg = By.xpath("//div[@class='modal']//div[@class='modal__inner']//div/p");
	protected By emptyStatusInfo = By.xpath("//button[@class='btn prospect enabled primary']");
	protected By completeStatusInfo = By.xpath("//button[@class='btn prospect primary disabled completed']");

	// ----------------------------------------Locators-------------------------------------------
	// ------------------------------------All--------------------------------------------------------------------
	public By brandLabel = By.xpath("//div[contains(@class, 'header')]/nav/li/a/img[contains(@src, 'logo')]");
	public By custNameLogo = By.xpath("/div[contains(@class, 'points-card')]/strong");
	public By triDot = By.xpath("//a/div/button[contains(@class, 'dropdown-toggle')]/img");
	public By continueSecLbl = By.xpath("//div[contains(@class, 'landing-footer')]/p");
	public By checkSecondCus = By.xpath("//div[contains(@class, 'checkbox')]/label");
	// div[contains(@class, 'checkbox')]/input
	// div[contains(@class, 'checkbox')]/label
	// ----------------------------------PI----------------------------------------------------------------------
	public By personalInfoLbl = By.xpath("//div/h6[text()='Personal Information']");
	public By dateOfBirth = By.xpath("//input[@formcontrolname='dateOfBirth']");
	public By ssnNumber = By.xpath("//input[@id='ssn']");
	public By verifySSN = By.xpath("//input[@id='verifyssn']");
	public By mothersMdnName = By.xpath("//input[contains(@placeholder, 'Enter Mother')]");
	public By mothersNameErr = By.xpath("//div[contains(@class, 'alert-danger')]");
	public By withholdBtn = By.xpath("//a[text()='Withhold Customer']");
	public By withHoldCnfrm = By.xpath("//div/button[text()='Withhold']");
	// -------------------------Finance-------------------------------
	public By financialBtn = By.xpath("//button[text()=' Financial details ']");
	public By financialDetLbl = By.xpath("//div/h6[text()='Financial Details']");
	public By sourceOfIncome = By.xpath("//div/select[contains(@ng-reflect-name, 'employer')]");
	public By occupationSelect = By.xpath("//div/select[contains(@ng-reflect-name,'occupation')]");
	public By individualIncomInput = By.xpath("//input[contains(@placeholder,'Individual Income')]");
	public By annualIncomeInput = By.xpath("//input[contains(@ng-reflect-name,'annualIncome')]");
	public By bankAccSelect = By.xpath("//select[contains(@ng-reflect-name, 'bank')]");
	// -------------------------Address-------------------------------------
	public By addressBtn = By.xpath("(//button[text()=' Address '])[1]");
	public By addressDetLbl = By.xpath("//div/h6[text()='Address']");
	public By addres1TxtBox = By.xpath("//input[contains(@placeholder, 'Address1')]");
	public By postCodeTxtBox = By.xpath("//input[contains(@placeholder, 'Postal')]");
	public By cityTxtBox = By.xpath("//input[contains(@placeholder, 'City')]");
	public By countryDrpDwn = By.xpath("//select[@ng-reflect-name='country']");
	public By stateDrpDwn = By.xpath("//select[contains(@ng-reflect-name, 'state')]");
	public By yearsTxtBox = By.xpath("//input[contains(@placeholder, 'Years')]");
	public By monthsTxtBox = By.xpath("//input[contains(@placeholder, 'Months')]");
	public By phoneBtn = By.xpath("//button[text()=' Phone/email ']");
	public By phoneLbl = By.xpath("//div/h6[text()='Phone/email']");
	public By primaryPhnTxtBox = By.xpath("//input[contains(@name, 'primaryPhoneNumber')]");
	public By typePhoneDrpDwn = By.xpath("//select[contains(@ng-reflect-name, 'primaryPhoneType')]");
	public By primaryEmailTxtBox = By.xpath("//input[contains(@name, 'primaryEmail')]");
	public By saveBtn = By.xpath("//button[text()=' Save ']");
	public By residenceDrpDwn = By.xpath("//select[contains(@ng-reflect-name, 'residence')]");
	public By rentTxtBox = By.xpath("//input[contains(@placeholder, 'Payment')]");
	// --------------------Terms Conditions------------------------
	public By termsConBtn = By.xpath("//button[text()=' Terms & Conditions ']");

	// -------------------------------
	// SIgnature------------------------------------
	public By termsLbl = By.xpath("//label[text()='Accept Terms & Conditions ']");
	public By termsChkBox = By.xpath("//label[@id='terms']/span");
	public By signOutpara = By.xpath("//div[contains(@class, 'form-group')]/p");
	public By signatureLbl = By.xpath("//div/h3[text()='Wyndham Destinations Financing Signature']");
	public By vccSignLbl = By.xpath("//div/h3[text()='Vacation Club Credit Signature']");
	public By signatureBtn = By.xpath("//button[text()=' Signature ']");
	public By signatureSSN = By.xpath("//input[contains(@placeholder, 'Please verify')]");
	public By signImgBtn = By.xpath("//button/img[contains(@src, 'icon-sign')]");
	public By applySigBtn = By.xpath("//button[text()='Apply Signature']");
	public By agreeBtn = By.xpath("//button[text()=' Agree & Apply ']");
	public By closePop = By.xpath("//img[contains(@src, 'close')]");
	public By successMsg = By.xpath("//div[contains(@class, 'landing-header')]/h2");
	// -------------------------Results-----------------------------
	public By triDotIcon = By.xpath("//button[contains(@class, 'dropdown')]/img");
	public By menuList = By.xpath("//ul[contains(@id, 'dropdown-basic')]/li/a");
	public By custCheck = By.xpath("//div[contains(@class, 'checkbox')]/label[contains(@class, 'custom-control')]");
	// div[contains(@class, 'checkbox')]/label
	// div[contains(@class, 'checkbox')]/input
	public By viewResBtnDest = By
			.xpath("//p[text()='Wyndham Destinations Financing']/following-sibling::div/button[text()='View Results']");
	public By viewResBtnVCC = By.xpath(
			"//p[text()='Wyndham Destinations Financing']/following-sibling::div/button[text()='View Results']//p[text()='Vacation Club Credit']/following-sibling::div/button[text()='View Results']");
	public By viewResBtnRew = By.xpath(
			"//p[text()='Wyndham Reward Visa Application']/following-sibling::div/button[text()='View Results']");
	public By closeBtn = By.xpath("//button[text()=' Close ']");
	public By resText = By.xpath("//div[contains(@class, 'alert-popup')]/p");
	public By resAppLimit = By.xpath("//p[text()='Vacation Club Credit Account Amount: ']/b");
	public By vCCViewed = By.xpath("//p[text()='Vacation Club Credit']/following-sibling::div/button[text()='Viewed']");
	public By destViewed = By
			.xpath("//p[text()='Wyndham Destinations Financing']/following-sibling::div/button[text()='Viewed']");
	public By rewardViewed = By
			.xpath("//p[text()='Wyndham Reward Visa Application']/following-sibling::div/button[text()='Viewed']");
	// ---------------------------------------------VCC
	// Rewards------------------------------------------
	public By formVCC = By.xpath("//form/div[contains(@class, 'vcc-terms-wrapper')]");
	public By contBtn = By.xpath("//button[text()=' Continue ']");
	public By acceptCheckBox = By.xpath("//label/input[contains(@ng-reflect-name, 'terms')]/following-sibling::span");
	// public By rewardsForm = By.xpath("//form[contains(@class,
	// 'custom-form')]/div/p/section");
	public By rewardsForm = By.xpath("//form/div/p//section/div/div/div/h1[text()='Terms and Conditions']");

	public By rewardsTCTxt = By.xpath("//label[text()='Accept Terms & Conditions ']/span");
	public By acknowForm = By.xpath("//form/div[contains(@class, 'acknowledge')]");
	public By acknwldgeBtn = By.xpath("//ul/following-sibling::button[text()=' Acknowledgement ']");
	public By consentCheckBox = By.xpath("//form/div[contains(@class, 'acknowledge')]/div/label/span");
	public By rewardSubmitTxt = By.xpath("//div[contains(@class, 'alert-popup')]/div/p/span");

	// ------------------------------------------Functions--------------------------------------

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name :clickOkButton
	 * 
	 * @Description : This function is used to close okay popup if there is any
	 * issue in submitting rewards application saved or not for Barclays & VCC
	 * 
	 * @Date : June, 30, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	protected By rewardsOk = By.xpath("//button[text()='OK']");

	public void clickOkButton() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (verifyElementDisplayed(driver.findElement(rewardsOk))) {
			clickElementJSWithWait(rewardsOk);
		} else {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}
	
	

    public static void scrollKeys(RemoteWebDriver driver, String[] list) {
        System.out.println("Starting the process");
        for (int i = 0; i < list.length; i++) {
            WebElement we = driver.findElement(By.xpath("//XCUIElementTypePickerWheel["+(i+1)+"]"));
            we.sendKeys(list[i]);
        }
        System.out.println("Ending Process");
    }

	
	
	
	
	

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name(1) : formFillUpPI
	 * 
	 * @Description : This function is used to fill up the personal information.
	 * 
	 * @Test Data: DOB, SSN, MothersMdnName
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	protected By yearDropdown = By.xpath("//XCUIElementTypeButton[@label='Show year picker']");
	protected By selectDate = By.xpath("//XCUIElementTypeButton[contains(@label,' 9')]");
	protected By okCalender = By.xpath("//XCUIElementTypeButton[@label='OK']");
	
	
	public void formFillUpPI() {

		waitUntilElementVisibleBy(driver, personalInfoLbl, "ExplicitWaitLongestTime");

		if (verifyObjectDisplayed(personalInfoLbl)) {

			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpPI", Status.PASS,
					"Primary Customer Information Page Displayed");
			String cusDOB = testData.get("DOB");
			String ssnNumbInput = testData.get("FirstCusSSN");
			String mothersMdnNameInput = testData.get("MothersMdnName");
			System.out.println("-------dob--" + cusDOB + "----ssn--" + ssnNumbInput);
			
			
		    
			Map<String, Object> params = new HashMap<>();
            params.put("label", "Public:automation_images/Apple_iPad Pro 9.7_210304_192718.png");
            params.put("match", "Bounded");
            ((JavascriptExecutor) driver).executeScript("mobile:button-image:click", params);
            
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            switchToContext(driver, "NATIVE_APP");
            
            clickElementBy(selectDate);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            clickElementBy(yearDropdown);
            waitForSometime(tcConfig.getConfig().get("MedWair"));
          //Select the DOB
            scrollKeys((RemoteWebDriver) driver, new String[]{"April","1979"});
            
            clickElementBy(yearDropdown);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
            clickElementBy(okCalender);
            
            switchToContext(driver, "WEBVIEW");
			
			// ---------trying use click & send keys-------------
            enterDataKeyboard(ssnNumber, ssnNumbInput);
            enterDataKeyboard(verifySSN, ssnNumbInput);

            /* WebElement we=driver.findElement(ssnNumber);
             we.click();
             JavascriptExecutor js = (JavascriptExecutor)driver;
             js.executeScript("arguments[0].value= '666034349';", we);
             
             we.sendKeys(Keys.TAB);
             
             WebElement we1=driver.findElement(verifySSN);
             we1.click();
             JavascriptExecutor js1 = (JavascriptExecutor)driver;
             js1.executeScript("arguments[0].value= '666034349';", we1);
            
             we1.sendKeys(Keys.TAB);*/
            
			fieldDataEnter(mothersMdnName, mothersMdnNameInput);
			 
			 
			
	       
			
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpPI", Status.PASS,
					"Successfully All personal Information for the Customer are provided");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpPI", Status.FAIL,
					"Getting some error while proding Personal information");

		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (2):navigateToFinancialDetails
	 * 
	 * @Description : This function is used to navigate to the Financial Details
	 * page
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void navigateToFinancialDetails() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, financialBtn, "ExplicitWaitLongestTime");
		clickElementJS(financialBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//checkLoadingSpinnerEWS();
		// verify user is navigated to Finacial details page
		if (verifyObjectDisplayed(financialDetLbl)) {

			tcConfig.updateTestReporter("PersonalInformationPage", "navigateToFinancialDetails", Status.PASS,
					"User navigated to Financial Details page");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "navigateToFinancialDetails", Status.PASS,
					"User navigated to Financial Details page");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (3): formFillUpFinancial
	 * 
	 * @Description : This function is used to fill up the Financial
	 * Information.
	 * 
	 * @Test Data: Employer, Occupation, IndvidualIncome, AnnualIncome, BankType
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */

	public void formFillUpFinancial() {

		waitUntilElementVisibleBy(driver, financialDetLbl, "ExplicitWaitLongestTime");

		String sourceIncome = testData.get("Employer");
		String occupation = testData.get("Occupation");
		String individualIn = testData.get("IndividualIncome");
		String annualIncome = testData.get("AnnualIncome");
		String bankAccount = testData.get("BankType");
		System.out.println(individualIn + "incomeindividul-------bankType--" + bankAccount + "-----employer---"
				+ sourceIncome + "----occupation----" + occupation);
		// data input common function called: parameters(locator, test data)
		selectByText(sourceOfIncome, sourceIncome);
		selectByText(occupationSelect, occupation);
		/*clickElementJS(individualIncomInput);
		clearElementBy(individualIncomInput);
	  
		sendKeysBy(individualIncomInput, individualIn);*/
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		javaScriptSendkeys( individualIncomInput, individualIn);
		javaScriptSendkeys(annualIncomeInput, annualIncome);
		
	    //fieldDataEnter(individualIncomInput, individualIn);
	
		selectByText(bankAccSelect, bankAccount);

		clickElementJS(addressBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();
		// verify all details are provided & user is navigated to Address page
		if (verifyObjectDisplayed(addressDetLbl)) {

			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpFinancial", Status.PASS,
					"All financial Information for the Customer are provided");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpFinancial", Status.FAIL,
					"Getting some error while providing financial information");

		}
	}
/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (4): formFillUpAddress
	 * 
	 * @Description : This function is used to fill up the Address Information.
	 * 
	 * @Date of update: July, 10, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */
	public void formFillUpAddress() {
		waitUntilElementVisibleBy(driver, addressDetLbl, "ExplicitWaitLongestTime");
		try{
		String addressInput = testData.get("CustomerAddress");
		String countryInput = testData.get("Country");
		String stateInput = testData.get("State");
		String cityInput = testData.get("City");
		String postCodeInpt = testData.get("ZIP");
		String residenceInpt = testData.get("ResidenceType");
		String yearsAddInpt = testData.get("YearsAtRes");
		String monthAddInpt = testData.get("MonthsAtRes");
		
		// data input common function called: parameters(locator, test data)
		// selectByValue(countryDrpDwn, countryInput);
		javaScriptSendkeys(addres1TxtBox, addressInput);
		javaScriptSendkeys(postCodeTxtBox, postCodeInpt);
		javaScriptSendkeys(cityTxtBox, cityInput);
		selectByValue(stateDrpDwn, stateInput);
		selectByValue(residenceDrpDwn, residenceInpt);
		javaScriptSendkeys(yearsTxtBox, yearsAddInpt);
		javaScriptSendkeys(monthsTxtBox, monthAddInpt);
		if(residenceInpt=="T"){
			String monthRentInpt = testData.get("'RentRequired");
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			javaScriptSendkeys(rentTxtBox, monthRentInpt);
		}
		else{
			System.out.println("No rent required");
		}

			// Click on phone/email link to navigate to next page of the form
		clickElementJS(phoneBtn);
			//checkLoadingSpinnerEWS();
			waitUntilElementVisibleBy(driver, phoneLbl, "ExplicitWaitLongestTime");
			// verify all details are provided & user is navigated to Phone page
			if (verifyObjectDisplayed(phoneLbl)) {

				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpAddress()", Status.PASS,
						"All Address Information for the Customer are provided");

			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpAddress()", Status.FAIL,
						"Getting some error while providing Address information");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpAddress()", Status.FAIL,
					"Getting some error while providing Address information");
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (5):formFillUContact()
	 * 
	 * @Description : This function is used to fill up the Contact Information.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void formFillUpContact() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		String phoneInput = testData.get("primaryPhoneNumber");
		String phoneTypeInpt = testData.get("PhoneType");
		String emailInput = testData.get("CusEmail");

		// data input common function called: parameters(locator, test data)
		// selectByValue(countryDrpDwn, countryInput);
		
		javaScriptSendkeys(primaryPhnTxtBox, phoneInput);
		selectByValue(typePhoneDrpDwn, phoneTypeInpt);
		
		javaScriptSendkeys(primaryEmailTxtBox, emailInput);

		// Click on phone/email link to navigate to next page after saving
		// information
		if (verifyElementDisplayed(driver.findElement(saveBtn))) {
			clickElementJSWithWait(saveBtn);
			//checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpContact()", Status.PASS,
					"All Contact Information for the Customer are saved");
		}

		else {
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUContact()", Status.FAIL,
					"Getting some error while saving Contact information");

		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (6):navigateToSignatureDest()
	 * 
	 * @Description : This function is used to review Destinations T&C &
	 * navigate to signature page.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */
	public void navigateToSignatureDest() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, cusDetPage.getStartedLink, "ExplicitWaitLongestTime");
		// String type = testData.get("Type");
		Boolean flag = false;

		driver.findElement(cusDetPage.getStartedLink).click();
		//checkLoadingSpinnerEWS();
		// page navigated to Terms Conditions
		waitUntilElementVisibleBy(driver, termsLbl, "ExplicitWaitLongestTime");
		clickElementJSWithWait(termsChkBox);
		// navigate to next page for SSN confirmation
		clickElementJSWithWait(signatureBtn);
		//checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, signOutpara, "ExplicitWaitLongestTime");
		flag = verifyElementDisplayed(driver.findElement(signOutpara));
		/*
		 * if(type.equalsIgnoreCase("Destintion")){
		 * waitUntilElementVisibleBy(driver, signatureLbl, "ExplicitWaitLongestTime"); flag =
		 * verifyElementDisplayed(driver.findElement(signatureLbl)); } else
		 * if(type.equalsIgnoreCase("VCC")){ waitUntilElementVisibleBy(driver,
		 * vccSignLbl, "ExplicitWaitLongestTime"); flag =
		 * verifyElementDisplayed(driver.findElement(vccSignLbl)); } else{
		 * waitUntilElementVisibleBy(driver, signatureLbl, "ExplicitWaitLongestTime"); flag =
		 * verifyElementDisplayed(driver.findElement(signatureLbl)); }
		 */
		// verify user navigated to the signature page
		if (flag == true) {

			tcConfig.updateTestReporter("PersonalInformationPage", "navigateToSignatureDest", Status.PASS,
					"User succesfully navigated to Signature page");
		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "navigateToSignatureDest", Status.FAIL,
					"User not succesfully navigated to Signature page");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (7a):formSign()
	 * 
	 * @Description : This function is used to review T&C then sign form.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */
	 public void formSign() {
	        waitForSometime(tcConfig.getConfig().get("MedWait"));
	        waitUntilElementVisibleBy(driver, signatureSSN, "ExplicitWaitLongestTime");

	 

	        try {
	            String signSSNInput = testData.get("FirstCusSSN");
	            System.out.println("SSN----" + signSSNInput);

	 

	            String strSubmittedMsg;
	            waitForSometime(tcConfig.getConfig().get("MedWait"));
	            javaScriptSendkeys(signatureSSN, signSSNInput);

	 

	            WebElement txtSSN = driver.findElement(signatureSSN);

	 

	            waitForSometime(tcConfig.getConfig().get("MedWait"));
	            // click outside on the para of text to activate sign image button
	            driver.findElement(signOutpara).click();
	            //checkLoadingSpinnerEWS();
	            waitForSometime(tcConfig.getConfig().get("MedWait"));
	            waitUntilElementVisibleBy(driver, signImgBtn, "ExplicitWaitLongestTime");
	            clickElementJSWithWait(signImgBtn);
	            waitUntilElementVisibleBy(driver, applySigBtn, "ExplicitWaitLongestTime");
	            // click on anywhere on screen
	            // do a signature
	            WebElement element = driver.findElement(By.xpath("//canvas[@height='400']"));
	            
	            drag_horizontal();
	            clickElementJS(applySigBtn);
	          
	            waitUntilElementVisibleBy(driver, agreeBtn, "ExplicitWaitLongestTime");
	            if (verifyElementDisplayed(driver.findElement(agreeBtn))) {
	                tcConfig.updateTestReporter("PersonalInformationPage", "formSign()", Status.PASS,
	                        "Customer Form signed successfully");
	                clickElementJS(agreeBtn);
	                Thread.sleep(2000);
	                waitForSometime(tcConfig.getConfig().get("LongWait"));
	            } else {
	                tcConfig.updateTestReporter("PersonalInformationPage", "formSign()", Status.FAIL,
	                        "Customer Information save & submit unsuccessful");
	            }
	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	            tcConfig.updateTestReporter("PersonalInformationPage", "formSign()", Status.FAIL,
	                    "Customer Information save & submit unsuccessful" + e);
	        }
	    }

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (7b):formSignMulti()
	 * 
	 * @Description : This function is used to review T&C then sign form for
	 * second or third customer.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void formSignMulti() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, signatureSSN, "ExplicitWaitLongestTime");

		try {
			// String signSSNInput= testData.get("SSNSecon");
			// System.out.println("SSN----" + signSSNInput);

			String strSubmittedMsg;
			clickElementJS(signatureSSN);
			//System.out.println("Pass 1");
			// driver.findElement(signatureSSN).sendKeys(signSSNInput);

			WebElement txtSSN = driver.findElement(signatureSSN);

			String text = testData.get("SSNSecon");
			StringSelection stringSelection = new StringSelection(text);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(stringSelection, stringSelection);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);

			// fieldDataEnter(signatureSSN, signSSNInput);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// click outside on the para of text to activate sign image button
			driver.findElement(signOutpara).click();
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, signImgBtn, "ExplicitWaitLongestTime");
			clickElementJS(signImgBtn);
			waitUntilElementVisibleBy(driver, applySigBtn, "ExplicitWaitLongestTime");
			// click on anywhere on screen
			// do a signature
			WebElement element = driver.findElement(By.xpath("//canvas[@height='400']"));

			Actions builder = new Actions(driver);
			Action drawAction = builder.moveToElement(element, 135, 15) // start
																		// points
																		// x
																		// axis
																		// and y
																		// axis.
					.click().moveByOffset(200, 60) // 2nd points (x1,y1)
					.click().build();
			drawAction.perform();

			clickElementJS(applySigBtn);
			checkLoadingSpinnerEWS();
			waitUntilElementVisibleBy(driver, agreeBtn, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(agreeBtn))) {
				tcConfig.updateTestReporter("PersonalInformationPage", "formSign()", Status.PASS,
						"Customer Form signed successfully");
				mouseoverAndClick(agreeBtn);
				checkLoadingSpinnerEWS();
				// checkLoadingForCUI();
				Thread.sleep(2000);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "formSign()", Status.FAIL,
						"Customer Information save & submit unsuccessful");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "formSign()", Status.FAIL,
					"Customer Information save & submit unsuccessful" + e.getMessage());
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (8):selectCustResult()
	 * 
	 * @Description : This function is used to select the Customer Result
	 * dropdown
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void selectCustResult() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		int count = 0;
		int resultPos = 0;
		// Boolean flag = false;
		try {
			mouseoverAndClick(triDot);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// waitUntilElementVisibleBy(driver, triDotIcon, 100);
			// clickElementJS(triDotIcon);
			// clickElementBy(triDotIcon);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> menuDotList = driver.findElements(menuList);
			count = menuDotList.size();
			resultPos = count - 1;
			System.out.println("Size of the menu dropdown" + count);
			clickElementJSWithWait(menuDotList.get(menuDotList.size() - resultPos));
			checkLoadingSpinnerEWS();
			// using action class to click on the dropdown option
			/*
			 * Actions act = new Actions(driver); Action seriesOfActions = act
			 * .moveToElement(menuDotList.get(menuDotList.size()-resultPos))
			 * .click() .build(); seriesOfActions.perform() ;
			 */
			// click on Customer results using JS element
			// clickElementJS(menuDotList.get(menuDotList.size()-resultPos));

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// checkLoadingSpinnerEWS();
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, custCheck, "ExplicitWaitLongestTime");

			tcConfig.updateTestReporter("PersonalInformationPage", "selectCustResult", Status.PASS,
					"User navigated to View Results page successfully");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "selectCustResult", Status.FAIL,
					"User not navigated to View Results page successfully" + e.getMessage());
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (9):viewResultDest()
	 * 
	 * @Description : This function is used to result & navigate to ECR control
	 * panel
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void viewResultDest() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean resFlag = false;
		try {

			mouseoverAndClick(triDot);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> listmenu = driver.findElements(menuList);
			// Always click on the first element
			clickElementJSWithWait(listmenu.get(2));
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJSWithWait((custCheck));

			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJSWithWait((viewResBtnDest));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(closeBtn))) {
				tcConfig.updateTestReporter("PersonalInformationPage", "viewResultDes", Status.PASS,
						"User viewed results successfully");
				clickElementJSWithWait(closeBtn);

			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "viewResultDes", Status.FAIL,
						"Failed to View - User Resuts");
			}
			// end of if-else
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			pageUp();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "viewResultDes", Status.FAIL,
					"User did not User viewed results successfull");
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (10):navigateToECR()
	 * 
	 * @Description : This function is used to result & navigate to ECR control
	 * panel
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void navigateToECR() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Boolean flag = false;

		clickElementJS(triDot);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> listmenu = driver.findElements(menuList);
		// Always click on the first element
		clickElementJSWithWait(listmenu.get(0));
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleBy(driver, cusDetPage.ECRlabel, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(cusDetPage.ECRlabel))) {
			tcConfig.updateTestReporter("PersonalInformationPage", "navigateToECR", Status.PASS,
					"user navigated to ECR Control Panel");
		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "navigateToECR", Status.FAIL,
					"user not navigated to ECR Control Panel");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (11):clickToContinue()
	 * 
	 * @Description : This function is used to click on continue in VCC form &
	 * navigate to Term & Conditions page panel
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void clickToContinue() {

		waitUntilElementVisibleBy(driver, contBtn, "ExplicitWaitLongestTime");
		mouseoverAndClick(contBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, termsConBtn, "ExplicitWaitLongestTime");
		// verify user is navigated to Terms & Conditions details page
		if (verifyElementDisplayed(driver.findElement(termsConBtn))) {

			tcConfig.updateTestReporter("PersonalInformationPage", "clickToContinue", Status.PASS,
					"User navigated to Terms & Conditions page");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "clickToContinue", Status.FAIL,
					"User not navigated to Terms & Conditions page");
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (12):clickToTermsCond()
	 * 
	 * @Description : This function is used to click on Term & Conditions button
	 * & navigate to Terms & Conditions Form panel
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void clickToTermsCond() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		mouseoverAndClick(termsConBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, formVCC, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(formVCC))) {

			tcConfig.updateTestReporter("PersonalInformationPage", "clickToTermsCond", Status.PASS,
					"User navigated to Terms & Conditions Form page");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "clickToTermsCond", Status.FAIL,
					"User not navigated to Terms & Conditions Form page");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (13):checkToAccept()
	 * 
	 * @Description : This function is used to check & accept terms & conditions
	 * & navigate to signature page
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void checkToAccept() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		Boolean flag = false;

		clickElementJSWithWait(formVCC);
		Actions actions = new Actions(driver);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(acceptCheckBox);
		waitUntilElementVisibleBy(driver, signatureBtn, "ExplicitWaitLongestTime");
		mouseoverAndClick(signatureBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();

		waitUntilElementVisibleBy(driver, vccSignLbl, "ExplicitWaitLongestTime");
		flag = verifyElementDisplayed(driver.findElement(vccSignLbl));

		// verify user navigated to the signature page

		if (flag == true) {

			tcConfig.updateTestReporter("PersonalInformationPage", "checkToAccept", Status.PASS,
					"User accepted TC signs & succesfully navigated to Signature page");
		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "checkToAccept", Status.FAIL,
					"User accepted TC signs & navigated to Signature page is Unsuccessful");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (14):viewResultVCC()
	 * 
	 * @Description : This function is used to view VCC result & verify approved
	 * limit
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void viewResultVCC() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			mouseoverAndClick(triDot);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> listmenu = driver.findElements(menuList);
			// Always click on the first element
			clickElementJSWithWait(listmenu.get(2));
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJSWithWait(custCheck);

			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJSWithWait(viewResBtnVCC);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(closeBtn))) {
				tcConfig.updateTestReporter("PersonalInformationPage", "viewResultVCC", Status.PASS,
						"User viewed results successfully");
				clickElementJSWithWait(closeBtn);

			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "viewResultVCC", Status.FAIL,
						"Failed to View - User Resuts");
			}
			// end of if-else
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			pageUp();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "viewResultVCC", Status.FAIL,
					"User did not User viewed results successfull");
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (15):checkToAcceptRewards()
	 * 
	 * @Description : This function is used to check & accept terms & conditions
	 * & navigate to signature page
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void checkToAcceptRewards() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		// waitUntilElementVisibleBy(driver, rewardsForm, "ExplicitWaitLongestTime");
		Boolean flag = false;
		try {
			// clickElementJSWithWait(rewardsForm);
			driver.findElement(rewardsForm);
			Actions actions = new Actions(driver);
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(acceptCheckBox);
			waitUntilElementVisibleBy(driver, acknwldgeBtn, "ExplicitWaitLongestTime");

			if (verifyElementDisplayed(driver.findElement(acknwldgeBtn))) {
				mouseoverAndClick(acknwldgeBtn);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				checkLoadingSpinnerEWS();
				tcConfig.updateTestReporter("PersonalInformationPage", "checkToAcceptRewards", Status.PASS,
						"User accepted TC signs & succesfully & clicked on accept button");
			}

			else {
				tcConfig.updateTestReporter("PersonalInformationPage", "checkToAcceptRewards", Status.FAIL,
						"User accepted TC signs & navigated to Signature page is Unsuccessful");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "checkToAcceptRewards", Status.FAIL,
					"User accepted TC signs & navigated to Signature page is Unsuccessful");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (16):checkAcceptAckw()
	 * 
	 * @Description : This function is used to check & accept terms & conditions
	 * & navigate to signature page
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void checkAcceptAckw() {

		waitUntilElementVisibleBy(driver, acknowForm, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean flag = false;
		try {
			// Find the list of elements
			List<WebElement> listChck = driver.findElements(consentCheckBox);
			int count = listChck.size();
			// Loop to click all check boxes
			for (int i = 0; i < count; i++) {
				clickElementJSWithWait(listChck.get(i));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			if (verifyElementDisplayed(driver.findElement(signatureBtn))) {
				mouseoverAndClick(signatureBtn);
				checkLoadingSpinnerEWS();
				tcConfig.updateTestReporter("PersonalInformationPage", "checkAcceptAckw", Status.PASS,
						"Successfull-User accepted Acknowledgement condition & clicked Signature button");
			}

			else {
				tcConfig.updateTestReporter("PersonalInformationPage", "checkAcceptAckw", Status.FAIL,
						" Unsuccessful-User accepted Acknowledgement condition & clicked Signature button");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "checkToAccept", Status.FAIL,
					"User accepted TC signs & navigated to Signature page is Unsuccessful");
		}

	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (17):verifyInfoSubmitted()
	 * 
	 * @Description : This function is used to verify all information submitted
	 * saved or not for Destinations , Barclays & VCC
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void verifyInfoSubmitted() {
		// to verify navigation to submitted screen
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, successMsg, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {

			String strSubmittedMsg = driver.findElement(successMsg).getText();
			Thread.sleep(1000);
			System.out.println("Submitted message" + strSubmittedMsg);
			if (strSubmittedMsg.contains("submitted")) {
				tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmitted", Status.PASS,
						"Customer Information submitted");
			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmitted", Status.FAIL,
						"Customer Information save & submit unsuccessful");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmitted", Status.FAIL,
					"Customer Information submit unsuccessful" + e.getMessage());
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name (18):verifyInfoSubmittedReward()
	 * @Description : This function is used to verify all information submitted
	 * saved or not for Barclays & VCC
	 * @Date of Update: July, 9, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void verifyInfoSubmittedReward() {
		// to verify navigation to submitted screen
		//clickOkButton();
		//waitUntilElementVisibleBy(driver, closeBtn, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Boolean flagOk= false;
			
				try {
					clickElementBy(closeBtn);
					checkLoadingSpinnerEWS();
					 Thread.sleep(3500);
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					flagOk= verifyObjectDisplayed(rewardsOk);
					clickElementJSWithWait(rewardsOk);
					
					/*if(flagOk==true)
					{	System.out.println("the ok button is coming");
						
						//clickElementBy(by);
						
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
					else{
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}*/

					
					//waitUntilElementVisibleBy(driver, successMsg, "ExplicitWaitLongestTime");
					
					
					//next step to validate navigated screen
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					String strSubmittedMsg = driver.findElement(successMsg).getText();
					Boolean flag = strSubmittedMsg.contains("submitted");
					System.out.println("Check if conatins work-----" + flag);
					System.out.println("Submitted message" + strSubmittedMsg);
					if (strSubmittedMsg.contains("submitted")) {
						tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmittedReward", Status.PASS,
								"SUccessful-Customer Information submitted");
					} else {
						tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmittedReward", Status.FAIL,
								"Customer Information submit unsuccessful");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmittedReward", Status.FAIL,
							"Customer Information submit unsuccessful");
				}
			/*} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmittedReward", Status.FAIL,
						"Unsuccessfull- Navigation to Submitted Screen");
			}*/
			

	
	}
	public void verifyInfoSubmittedRewardHorace() {
		// to verify navigation to submitted screen
		//clickOkButton();
		waitUntilElementVisibleBy(driver, closeBtn, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		//Boolean flagOk= false;
			
				clickElementBy(closeBtn);
				checkLoadingSpinnerEWS();
				// Thread.sleep(2000);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				//flagOk= verifyObjectDisplayed(rewardsOk);
				//flagOk= verifyElementDisplayed(driver.findElement(rewardsOk));
				/*if(flagOk==true)
				{	System.out.println("the ok button is coming");
					clickElementJSWithWait(driver.findElement(rewardsOk));
					//clickElementBy(by);
					
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				else{
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}	*/		
				
				waitUntilElementVisibleBy(driver, successMsg, "ExplicitWaitLongestTime");
				
				
				//next step to validate navigated screen
				//waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strSubmittedMsg = driver.findElement(successMsg).getText();
				Boolean flag = strSubmittedMsg.contains("submitted");
				System.out.println("Check if conatins work-----" + flag);
				System.out.println("Submitted message" + strSubmittedMsg);
				if (strSubmittedMsg.contains("submitted")) {
					tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmittedReward", Status.PASS,
							"SUccessful-Customer Information submitted");
				} else {
					tcConfig.updateTestReporter("PersonalInformationPage", "verifyInfoSubmittedReward", Status.FAIL,
							"Customer Information submit unsuccessful");
				}
			
			
	
	}
	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * @Function Name (18b):verifyOKRewardNotAccp()
	 * @Description : This function is used to verify that if data not accepted then OK button will be displayed on popup
	 * @Date : July, 9, 2020
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void verifyOKRewardNotAccp() {
		// to verify navigation to submitted screen
		//clickOkButton();
		Boolean flag = false;
		waitUntilElementVisibleBy(driver, closeBtn, "ExplicitWaitLongestTime");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
			
				clickElementBy(closeBtn);				
				
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				flag= verifyElementDisplayed(driver.findElement(rewardsOk));
				if(flag==true)
				{
				tcConfig.updateTestReporter("PersonalInformationPage", "verifyOKRewardNotAccp", Status.PASS,
						"Successfull- Presence of Ok button verified");
			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "verifyOKRewardNotAccp", Status.FAIL,
						"Failed to verify: Presence of Ok button");
			}
			//clickElementBy(rewardsOk);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
	
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (19):viewResultReward()
	 * 
	 * @Description : This function is used to result & navigate to ECR control
	 * panel
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void viewResultReward() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			// clickElementJS(custCheck);
			driver.findElement(custCheck);
			WebElement element = driver.findElement(custCheck);
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().perform();
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, viewResBtnRew, "ExplicitWaitLongestTime");
			clickElementBy(viewResBtnRew);
			waitUntilElementVisibleBy(driver, closeBtn, "ExplicitWaitLongestTime");
			String resultTxt = driver.findElement(resText).getText();
			Boolean resFlag = resultTxt.contains("Sales Agent");
			if (resFlag == true || resultTxt != "") {
				// flag = true;
				tcConfig.updateTestReporter("PersonalInformationPage", "viewResultReward", Status.PASS,
						"User viewed results successfully");
			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "viewResultReward", Status.FAIL,
						"User did not User viewed results successfull");
			}
			// end of if-else
			clickElementJS(closeBtn);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			checkLoadingSpinnerEWS();

			waitUntilElementVisibleBy(driver, rewardViewed, "ExplicitWaitLongestTime");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "viewResult()", Status.FAIL,
					"User did not User viewed results successfull");
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (20):checkbrandLogo()
	 * 
	 * @Description : This function is used to check the Brand Logo
	 * 
	 * @Test data: Brand
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void checkbrandLogo() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			pageUp();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Boolean brandFlag = false;
			WebElement wbel = driver.findElement(brandLabel);
			brandFlag = verifyElementDisplayed(wbel);
			String src = wbel.getAttribute("src");
			System.out.print("Fetched brand name----" + src);
			// verify brand logo with entered data
			if (brandFlag == true && src.contains("logo")) {

				tcConfig.updateTestReporter("PersonalInformationPage", "checkbrandLogo", Status.PASS,
						"Successful- Brand specific Logo displayed");
			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "checkbrandLogo", Status.FAIL,
						"Unsuccessful- Brand specific Logo displayed");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "checkbrandLogo", Status.FAIL,
					"Unsuccessful- Brand specific Logo displayed" + e.getMessage());
		}
	}
	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name(21) : formFillUpPIHalf
	 * 
	 * @Description : This function is used to fill up the personal information
	 * leaving Mandatory field blank & click submit.
	 * 
	 * @TestData: DOB, SSN
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void formFillUpPIHalf() {
		// just to test------------------------------------
		// remove these two lines after
		// clickElementJS(driver.findElement(By.xpath("//a[@id='pii-link']/span")));
		// checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, personalInfoLbl, "ExplicitWaitLongestTime");

		try {
			if (verifyObjectDisplayed(personalInfoLbl)) {

				tcConfig.updateTestReporter("InformationPage", "fillupFormForPrimaryCustomer", Status.PASS,
						"Primary Customer Information Page Displayed");
				String cusDOB = testData.get("DOB");
				String ssnNumbInput = testData.get("FirstCusSSN");
				// String mothersMdnNameInput = testData.get("MothersMdnName");
				System.out.println("-------dob--" + cusDOB + "----ssn--" + ssnNumbInput);
				clickElementJSWithWait(dateOfBirth);
				driver.findElement(dateOfBirth).sendKeys(cusDOB);
				// fieldDataEnter(dateOfBirth, cusDOB);
				// ----trying to use javascript---------------
				WebElement wbel = driver.findElement(ssnNumber);
				clickElementJS(wbel);
				wbel.sendKeys(ssnNumbInput);
				driver.findElement(verifySSN).clear();
				driver.findElement(verifySSN).sendKeys(ssnNumbInput);

				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpPIHalf", Status.PASS,
						"Successful: Mandatory fileds left blank");

			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpPIHalf", Status.FAIL,
						"Unsuccessful: Mandatory fileds left blank");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name(22) : verifyMandatoryError
	 * 
	 * @Description : This function is used to verify the missing mandatory
	 * field
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void verifyMandatoryError() {

		waitUntilElementVisibleBy(driver, financialBtn, "ExplicitWaitLongestTime");
		mouseoverAndClick(financialBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();

		if (verifyObjectDisplayed(mothersNameErr)) {

			tcConfig.updateTestReporter("PersonalInformationPage", "verifyMandatoryError", Status.PASS,
					"Successful: Verified All mandatory fileds need to be filled");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "verifyMandatoryError", Status.FAIL,
					"Unsuccessful: Verified All mandatory fileds need to be filled");
		}
		mouseoverAndClick(triDot);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> listmenu = driver.findElements(menuList);
		// Always click on the first element
		clickElementJSWithWait(listmenu.get(0));
		// System.out.println(listCustomer.get(listCustomer.size()-1).isSelected());
		// Thread.sleep(2000);
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	// Second Customer Form Fill Up
	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name(23) : formFillUpPISecon *
	 * 
	 * @Description : This function is used to fill up the personal information
	 * for Second Customer.
	 * 
	 * @Test Data: DOB, SSN, MothersMdnName
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void formFillUpPISecon() {
		// just to test------------------------------------
		// remove these two lines after
		// clickElementJS(driver.findElement(By.xpath("//a[@id='pii-link']/span")));
		// checkLoadingSpinnerEWS();
		pageDown();
		waitUntilElementVisibleBy(driver, continueSecLbl, "ExplicitWaitLongestTime");
		clickElementJSWithWait(checkSecondCus);
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, cusDetPage.getStartedLink, "ExplicitWaitLongestTime");
		driver.findElement(cusDetPage.getStartedLink).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		checkLoadingSpinnerEWS();
		waitUntilElementVisibleBy(driver, personalInfoLbl, "ExplicitWaitLongestTime");

		if (verifyObjectDisplayed(personalInfoLbl)) {

			tcConfig.updateTestReporter("InformationPage", "formFillUpPISecon", Status.PASS,
					"Second Customer Information Page Displayed");
			String cusDOB = testData.get("DOBSecon");
			String ssnNumbInput = testData.get("SSNSecon");
			String mothersMdnNameInput = testData.get("MothersMdnNameSecon");
			System.out.println("-------dob--" + cusDOB + "----ssn--" + ssnNumbInput);
			clickElementJSWithWait(dateOfBirth);
			driver.findElement(dateOfBirth).sendKeys(cusDOB);
			// fieldDataEnter(dateOfBirth, cusDOB);
			// ----trying to use javascript---------------
			WebElement wbel = driver.findElement(ssnNumber);
			// sendElementJSWithWait(wbel);

			// ---------trying with jscript click & normal send
			// keys-------------------------
			clickElementJS(wbel);
			wbel.sendKeys(ssnNumbInput);
			// ---------trying use click & send keys-------------
			// driver.findElement(ssnNumber).click();
			// driver.findElement(ssnNumber).sendKeys(ssnNumbInput);

			// -----trying to use clear & send keys hardcoded value
			driver.findElement(verifySSN).clear();
			driver.findElement(verifySSN).sendKeys(ssnNumbInput);

			fieldDataEnter(mothersMdnName, mothersMdnNameInput);

			// driver.findElement(verifySSN).sendKeys(Keys.TAB);

			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpPISecon", Status.PASS,
					"All personal Information for the Customer are provided");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpPISecon", Status.FAIL,
					"Getting some error while proding Personal information");

		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (24): formFillUpFinancialSecon
	 * 
	 * @Description : This function is used to fill up the Financial
	 * Information.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */

	public void formFillUpFinancialSecon() {

		waitUntilElementVisibleBy(driver, financialDetLbl, "ExplicitWaitLongestTime");

		// tcConfig.updateTestReporter("InformationPage",
		// "fillupFormForPrimaryCustomer", Status.PASS,
		// "Primary Customer Information Page Displayed");
		String sourceIncome = testData.get("EmployerSecon");
		String occupation = testData.get("OccupationSecon");
		String individualIn = testData.get("IndividualIncomeSecon");
		String annualIncome = testData.get("AnnualIncomeSecon");
		String bankAccount = testData.get("BankTypeSecon");
		System.out.println(individualIn + "incomeindividul-------bankType--" + bankAccount + "-----employer---"
				+ sourceIncome + "----occupation----" + occupation);
		// data input common function called: parameters(locator, test data)
		selectByText(sourceOfIncome, sourceIncome);
		selectByText(occupationSelect, occupation);
		// clickElementJS(individualIncomInput);
		clearElementBy(individualIncomInput);
		sendKeysBy(individualIncomInput, individualIn);
		// fieldDataEnter(individualIncomInput, individualIn);
		fieldDataEnter(annualIncomeInput, annualIncome);
		selectByText(bankAccSelect, bankAccount);

		mouseoverAndClick(addressBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();
		// verify all details are provided & user is navigated to Address page
		if (verifyObjectDisplayed(addressDetLbl)) {

			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpFinancialSecon", Status.PASS,
					"All financial Information for the Second Customer are provided");

		} else {
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpFinancialSecon", Status.FAIL,
					"Getting some error while providing financial information for Second Customer");

		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (25): formFillUpAddressSecon
	 * 
	 * @Description : This function is used to fill up the Address Information.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha utta
	 ***********************************************************************
	 */
	public void formFillUpAddressSecon() {
		waitUntilElementVisibleBy(driver, addressDetLbl, "ExplicitWaitLongestTime");
		try {
			String addressInput = testData.get("CustomerAddressSec");
			String countryInput = testData.get("CountrySec");
			String stateInput = testData.get("StateSec");
			String cityInput = testData.get("CitySec");
			String postCodeInpt = testData.get("ZIPSec");
			String residenceInpt = testData.get("ResidenceTypeSec");
			String yearsAddInpt = testData.get("YearsAtResSec");
			String monthAddInpt = testData.get("MonthsAtResSec");

			// data input common function called: parameters(locator, test data)
			// selectByValue(countryDrpDwn, countryInput);
			fieldDataEnter(addres1TxtBox, addressInput);
			fieldDataEnter(postCodeTxtBox, postCodeInpt);
			fieldDataEnter(cityTxtBox, cityInput);
			selectByValue(stateDrpDwn, stateInput);
			selectByValue(residenceDrpDwn, residenceInpt);
			fieldDataEnter(yearsTxtBox, yearsAddInpt);
			fieldDataEnter(monthsTxtBox, monthAddInpt);

			// Click on phone/email link to navigate to next page of the form
			mouseoverAndClick(phoneBtn);
			checkLoadingSpinnerEWS();
			waitUntilElementVisibleBy(driver, phoneLbl, "ExplicitWaitLongestTime");
			// verify all details are provided & user is navigated to Phone page
			if (verifyObjectDisplayed(phoneLbl)) {

				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpAddressSecon()", Status.PASS,
						"All Address Information for the Second Customer are provided");

			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpAddressSecon()", Status.FAIL,
						"Getting some error while providing Address information for Second ");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpAddress()", Status.FAIL,
					"Getting some error while providing Address information for Second");
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (26):formFillUContactSecon()
	 * 
	 * @Description : This function is used to fill up the Contact Information.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */
	public void formFillUpContactSecon() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			String phoneInput = testData.get("SecprimaryPhoneNumber");
			String phoneTypeInpt = testData.get("SecPhoneType");
			String emailInput = testData.get("CusEmailSec");
			String countryInput = testData.get("CountrySec");
			// data input common function called: parameters(locator, test data)
			selectByValue(countryDrpDwn, countryInput);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			driver.findElement(primaryPhnTxtBox).sendKeys(phoneInput);
			// fieldDataEnter(primaryPhnTxtBox, phoneInput);
			selectByValue(typePhoneDrpDwn, phoneTypeInpt);
			fieldDataEnter(primaryEmailTxtBox, emailInput);

			// Click on phone/email link to navigate to next page after saving
			// information
			mouseoverAndClick(saveBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// verify user is navigated to Get Started page
			waitUntilElementVisibleBy(driver, cusDetPage.getStartedLink, "ExplicitWaitLongestTime");
			if (verifyObjectDisplayed(cusDetPage.getStartedLink)) {

				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUpContactSecon()", Status.PASS,
						"All Contact Information for the Customer are saved");

			} else {
				tcConfig.updateTestReporter("PersonalInformationPage", "formFillUContactSecon()", Status.FAIL,
						"Getting some error while saving Contact information");

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tcConfig.updateTestReporter("PersonalInformationPage", "formFillUContactSecon()", Status.FAIL,
					"Getting some error while saving Contact information");
		}
	}

	/*
	 ***********************************************************************
	 * @Project Name : Wyndham Dev QA ecredit Regression.
	 * 
	 * @Function Name (27):withholdCustomer()
	 * 
	 * @Description : This function is used to withhold a tour.
	 * 
	 * @Date : March, 19, 2020
	 * 
	 * @Author : Prattusha Dutta
	 ***********************************************************************
	 */

	public void withholdCustomer() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		try {
			clickElementJS(withholdBtn);
			// clickElementBy(withholdBtn);
			waitUntilElementVisibleBy(driver, withHoldCnfrm, "ExplicitWaitLongestTime");
			// if (verifyElementDisplayed(driver.findElement(withHoldCnfrm))) {
			// if (verifyObjectDisplayed(withHoldCnfrm)) {
			tcConfig.updateTestReporter("PersonalInformationPage", "withholdCustomer", Status.PASS,
					"Withhold confirmation popup Page Displayed");
			// clickElementBy(withHoldCnfrm);
			clickElementJSWithWait(withHoldCnfrm);
			// driver.findElement(withHoldCnfrm).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * } else { tcConfig.updateTestReporter("InformationPage",
			 * "withholdSecondCustomer", Status.FAIL,
			 * "Withhold confirmation popup Page is not Displayed"); }
			 */

			clickElementBy(contBtn);
			tcConfig.updateTestReporter("PersonalInformationPage", "withholdCustomer", Status.PASS,
					"Withhold Continue BUtton Clicked");
			// clickElementJS(contBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinnerEWS();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}
	

	public void navigateToAllToursFrmResults() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Boolean flag = false;

		mouseoverAndClick(triDot);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> listmenu = driver.findElements(menuList);
		// Always click on the first element
		clickElementJSWithWait(listmenu.get(1));
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

}
