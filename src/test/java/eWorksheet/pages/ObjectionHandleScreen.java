package eWorksheet.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class ObjectionHandleScreen extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(ObjectionHandleScreen.class);

	public ObjectionHandleScreen(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}
	
	
	public static String strPoint = "";
	public static String packageVersion = "";
	public static int pckgVersion = 0;
	public String creditBand;
	public double InterestRt;
	public int cwaPitchTileCount;
	
	
	protected By OptionLink = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::ul/li[@role='menuitem']/a[contains(text(),'Options ')]");
	protected By proceedButton = By.xpath("//button[contains(text(),'Yes, proceed to options')]");
	protected By cwaPackagePoints = By
			.xpath("//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong");
	protected By applyBtn = By
			.xpath("//div[@class='cwa-points-package-footer-wrapper']/button[contains(text(),'Apply')]");
	protected By creditTab = By.xpath("(//span[contains(text(),'Credit')])[1]");
	protected By maxDiscountAllowed = By
			.xpath("//h3[contains(text(),'Select Discount Amount')]/../label[@class='cwa-label']");
	protected By DropPrice = By.xpath("(//td[text()='Discount:']//following::td)[1]");
	protected By purchasePrice = By.xpath("(//td[text()='Purchase Price:']//following::td)[1]");
	protected By todaysPriceUI = By.xpath("(//td[text()='Today’s Price:']//following::td)[1]");
	protected By secondDropCheckBox = By.xpath(
			"//label[contains(text(),'2nd drop Drop 100%')]/../div/strong/../div/label[@for='pricing-checkbox2']");
	public static By sideMenuLink = By.xpath("//img[@alt='icon-more']");
	protected By benefitTab = By.xpath("//span[text()='Benefits']");
	protected By selectCustCheckBox = By.xpath("((//table[contains(@class,'view-credit-table')])[2]/tbody/tr//descendant::Label)[1]");
	protected By refreshIcon = By.xpath("//img[@alt='icon-refresh-white']");
	
	public static String strStudioTxt;
	protected By onTourLabel = By.xpath("//h3[text()='On Tour Benefits']");
	protected By availableTourBenefitLabel = By.xpath("//h5[text()='Available On Tour Benefits']");
	protected By availableOnTourList = By.xpath("//h5[text()='Available On Tour Benefits']//following::div[@class='package-points-card full-card' or @class='package-points-card']/strong");
	protected By bonusPointChkBox = By.xpath("//strong[text()='Bonus Points']/../div/label[@for='benefits-checkbox4']");
	protected By matchRadioBtn = By.xpath("//strong[text()='Match']/../div/label[@for='benefits-radio1']");
	protected By updateBtn = By.xpath("//button[text()='Update' and @ type='button']");
	protected By CWPointsTxt = By.xpath("(//td[text()='CW Points']//following-sibling::td)[1]");
	protected By bonusPointsTxt = By.xpath("(//td[text()='Bonus Points']//following-sibling::td)[1]");

	
	public void selectOption() {
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	public void applyPriceDrop()
	{
		String strPurchasePrice = "";
		double intpurchasePrice;
		double inttodaysPrice;
		String strTodaysPrice = "";
		double inttodaysPriceUI = 0;
		int intPPPackageCard;
		double intMaxDiscount;

		String strPoint = testData.get("TourPoints");
		String strDiscount = testData.get("Discount");
		
		try{
			String strMaxDiscountLabel[] = driver.findElement(maxDiscountAllowed).getText().split(":");
			if (strDiscount.equals("Drop 100%")) {
				driver.findElement(secondDropCheckBox).click();
				String strDropPrice = driver.findElement(DropPrice).getText().substring(1).replace(",", "");
				double DropPrice = Double.parseDouble(strDropPrice);
				intMaxDiscount = Double.parseDouble(strMaxDiscountLabel[1].substring(2).replace(",", ""));
				double intsecondDrop100 = intMaxDiscount;
				if (intsecondDrop100 == DropPrice) {
					strPurchasePrice = driver.findElement(purchasePrice).getText().substring(1).replace(",", "");
					intpurchasePrice = Double.parseDouble(strPurchasePrice);
					inttodaysPrice = intpurchasePrice - DropPrice;
					strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
					inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
					if (inttodaysPriceUI == inttodaysPrice) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
								"Todays price is calculated correctly and displayed successfully");
					} else {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
								"Todays price is not calculated correctly and displayed successfully");
					}
				}
				/*driver.findElement(applyBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));*/
				List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
				for (int j = 1; j <= cwaPitchPoints.size(); j++) {
					String cwaPackagePoint = driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
									+ j + "]"))
							.getText();
					if (strPoint.equals(cwaPackagePoint)) {
						intPPPackageCard = Integer.parseInt(
								driver.findElement(By.xpath("(//div[@class='card-body']/h5/strong)[" + j + "]"))
										.getText().substring(1).replace(",", ""));
						if (intPPPackageCard == inttodaysPriceUI) {
							tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount",
									Status.PASS, "Purchase Price is displayed successfully");
							break;
						} else {
							tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount",
									Status.FAIL, "Purchase Price is not displayed successfully");
						}
					}
				}

			}
		}catch(Exception e)
		{
			e.getMessage();
		}
	}
	
	public void selectCreditBand()
	{
		try{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(refreshIcon))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
						"Refresh button is displayed successfully");
				creditBand = driver.findElement(By.xpath("//span[text()='Credit score based on:']//following::span[1]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(selectCustCheckBox);
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
						"Credit band is updated based on the available credit band from eCredit");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			
		}catch(Exception e)
		{
			e.getMessage();
		}
	}
	
	public void selectbenefitBonus()
	{
		String strAvlOnTourList[] = testData.get("AvailableOnTourList").split(";");
		int i;
		try{
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(onTourLabel))) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"On Tour Benefit Label is displayed successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"On Tour Benefit Label is not displayed successfully");
			}

			if (verifyElementDisplayed(driver.findElement(availableTourBenefitLabel))) {
				List<WebElement> allList = driver.findElements(availableOnTourList);
				for (i = 1; i <= allList.size(); i++) {
					String strName = driver.findElement(By
							.xpath("(//h5[text()='Available On Tour Benefits']//following::div[@class='package-points-card full-card' or @class='package-points-card']/strong)["
									+ i + "]"))
							.getText();
					if (strName.equals(strAvlOnTourList[i - 1])) {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
								"OnTour Benefit" + strName + " " + "is displayed successfully");
					} else {
						tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
								"OnTour Benefit" + strName + " " + "is not displayed successfully");
					}
				}
			}
			String strCWPointsTxt = driver.findElement(CWPointsTxt).getText().trim();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(bonusPointChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(matchRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(updateBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strBonusPointsTxt = driver.findElement(bonusPointsTxt).getText().trim();
			if (strCWPointsTxt.equals(strBonusPointsTxt)) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"Bonus Points is Updated successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"Bonus Points is not Updated successfully");
			}
		}catch(Exception e)
		{
			
		}
	}
	
	public void applyBtn_functionality()
	{
		try{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}catch(Exception e)
		{
			e.getMessage();
		}
	}

}
