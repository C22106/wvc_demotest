package eWorksheet.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CommunityLeadsPage  extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);
	public static String strLeadFullName;
	public CommunityLeadsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	public static String createdTourId;
	protected By txtFirstName=By.xpath("//label[text()='First Name']/..//input");
	protected By txtLastName=By.xpath("//label[text()='Last Name']/..//input");
	protected By txtMobile=By.xpath("//label[text()='Mobile #']/..//input");
	protected By txtEmail=By.xpath("//label[text()='Email']/..//input");
	protected By txtAddress=By.xpath("//label[text()='Address']/..//textarea");
	protected By drpdnCountry=By.xpath("//div[text()='Country']/..//select");
	protected By drpdnState=By.xpath("//div[text()='State']/..//select");
	protected By txtCity=By.xpath("//label[text()='City']/..//input");
	protected By txtZip=By.xpath("//label[text()='Zip']/..//input");
	protected By btnCreateGuest=By.xpath("//button[text()='CREATE GUEST']");
	protected By btnNext=By.xpath("//button[text()='Next']");
	protected By btnBookTour=By.xpath("//button[text()='BOOK TOUR']");
	protected By btnCreateTour=By.xpath("//button[text()='CREATE TOUR']");
	
	
	public void createCommunityLeadandTour(){
		String strLeadFirstName=getRandomString(6, "Upper");
		String strLeadLastName=getRandomString(7, "Upper");
		strLeadFullName=strLeadFirstName+" "+strLeadLastName;
		
		fieldDataEnter(txtFirstName, strLeadFirstName);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		fieldDataEnter(txtLastName, strLeadLastName);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		fieldDataEnter(txtMobile, testData.get("MobileNo"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		fieldDataEnter(txtEmail, testData.get("Email"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		fieldDataEnter(txtAddress, testData.get("StreetName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		
		new Select(driver.findElement(drpdnCountry)).selectByVisibleText(testData.get("Country"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		new Select(driver.findElement(drpdnState)).selectByVisibleText(testData.get("State"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(txtCity, testData.get("City"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		fieldDataEnter(txtZip, testData.get("ZIP"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnCreateGuest, "ExplicitWaitLongestTime");

		clickElementBy(btnCreateGuest);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnNext, "ExplicitWaitLongestTime");
		clickElementBy(btnNext);
		waitUntilElementVisibleBy(driver, btnBookTour, "ExplicitWaitLongestTime");
		if(verifyObjectDisplayed(btnBookTour)){
			tcConfig.updateTestReporter("CommunityLeadsPage", "createCommunityLeadandTour", Status.PASS,"New lead has "
					+ "been created with Lead Name : "+strLeadFullName);
		}else{
			tcConfig.updateTestReporter("CommunityLeadsPage", "createCommunityLeadandTour", Status.FAIL,"Failed to create new Lead");
		}
		
		clickElementBy(btnBookTour);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnCreateTour, "ExplicitWaitLongestTime");
		clickElementBy(btnCreateTour);
		waitUntilElementVisibleBy(driver, By.xpath("//h2[text()='"+testData.get("SalesStore")+"']"), "ExplicitWaitLongestTime");
	}
	
	protected By dpdnAppointmentLoc=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../.."
			+ "//label/span[text()='Booking Location']/../..//select");
	protected By dpdnBrand=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../.."
			+ "//label/span[text()='Brand']/../..//select");
	protected By dpdnTourType=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../.."
			+ "//label[text()='Tour Type']/../..//select");
	protected By dpdnMarktStrtgy=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../.."
			+ "//label/span[text()='Marketing Strategy']/../..//select");
	protected By dpdnMarketSrcType=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../.."
			+ "//label/span[text()='Marketing Source Type']/../..//select");
	protected By dpdnMarketingSubSrc=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../.."
			+ "//label/span[text()='Marketing Sub Source']/../..//select");
	protected By txtNoOfPeople=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../.."
			+ "//label[text()='# of people']/..//input");
	protected By avlblTimeBtn=By.xpath("(//h2[text()='"+testData.get("SalesStore")+"']/"
			+ "../../..//div[text()='Available Time']/.."
			+ "//div[@class='available-time-slot']//span[not(contains(@class,'disable'))]/..)[1]");
	protected By BtnNextNew=By.xpath("//h2[text()='"+testData.get("SalesStore")+"']/../../../.."
			+ "//button[contains(text(),'Next')]");
	protected By signaturePad=By.xpath("//canvas[@id='signature-pad']");
	protected By confirmBtn=By.xpath("//button[text()='CONFIRM & BOOK']");
	
	protected By reconfirmBtn=By.xpath("//button[text()='Confirm']");
	
	protected By printreceipt = By.xpath("//button[@title='Print Receipt']");
	
	public void bookAppointmentCommunity(){
	
			try{
				waitUntilElementVisibleBy(driver, dpdnAppointmentLoc, "ExplicitWaitLongestTime");

				new Select(driver.findElement(dpdnAppointmentLoc)).selectByVisibleText(testData.get("AppointmentLoc"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				new Select(driver.findElement(dpdnBrand)).selectByVisibleText(testData.get("Brand"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				new Select(driver.findElement(dpdnTourType)).selectByIndex(2);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				new Select(driver.findElement(dpdnMarktStrtgy)).selectByIndex(2);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				new Select(driver.findElement(dpdnMarketSrcType)).selectByIndex(2);
				//new Select(driver.findElement(dpdnMarketingSubSrc)).selectByIndex(2);
				fieldDataEnter(txtNoOfPeople, getRandomString(1));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, avlblTimeBtn, "ExplicitWaitLongestTime");

				clickElementBy(avlblTimeBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, BtnNextNew, "ExplicitWaitLongestTime");

				clickElementBy(BtnNextNew);
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisibleBy(driver, btnNext, "ExplicitWaitLongestTime");
					clickElementBy(btnNext);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					if (verifyObjectDisplayed(btnNext)) {
						waitUntilElementVisibleBy(driver, btnNext, "ExplicitWaitLongestTime");
						clickElementBy(btnNext);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
					waitUntilElementVisibleBy(driver, signaturePad, "ExplicitWaitLongestTime");
					 List<WebElement> listItem=driver.findElements(By.xpath("//div[@class='slds-grid slds-wrap cTMCMAEligibilityCriteriaItem']//button"));
					  
					  for (int i=0;i<=listItem.size()-1;i++){
						  listItem.get(i).click();
						  waitForSometime(tcConfig.getConfig().get("LowWait"));
					  }	 
					JavascriptExecutor jse = (JavascriptExecutor)driver;
			        jse.executeScript("window.scrollBy(0,500)", "");
			        
				  Actions builder = new Actions(driver);
				  WebElement canvasElement = driver.findElement(signaturePad);
				  Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
				                .moveByOffset(50, 20).release().build();
				  drawAction.perform();
				  waitForSometime(tcConfig.getConfig().get("LowWait"));
				  waitUntilElementVisibleBy(driver, confirmBtn, "ExplicitWaitLongestTime");
				  clickElementBy(confirmBtn);
				  waitForSometime(tcConfig.getConfig().get("MedWait"));
				  waitUntilElementVisibleBy(driver, reconfirmBtn, "ExplicitWaitLongestTime");
				  if(verifyObjectDisplayed(reconfirmBtn)){
				    	clickElementBy(reconfirmBtn);
				    	waitForSometime(tcConfig.getConfig().get("LowWait"));
				    }
			    else{
				    	System.out.println("No Re-Confirm Button");
				    }
				  //Abhijeet---Page refresh and again fill the form
				  waitUntilElementVisibleBy(driver, printreceipt, "ExplicitWaitLongestTime");
				  if (verifyObjectDisplayed(printreceipt)) {
					 System.out.println("Successfully navigated to next page");
				}
				  else{
					  System.out.println("Next page navigation is not successful");

					  driver.navigate().refresh();
					  waitUntilElementVisibleBy(driver, signaturePad, "ExplicitWaitLongestTime");
						 List<WebElement> listItem1=driver.findElements(By.xpath("//div[@class='slds-grid slds-wrap cTMCMAEligibilityCriteriaItem']//button"));
						  
						  for (int i=0;i<=listItem1.size()-1;i++){
							  listItem1.get(i).click();
							  waitForSometime(tcConfig.getConfig().get("LowWait"));
						  }	 
						JavascriptExecutor jse1 = (JavascriptExecutor)driver;
				        jse1.executeScript("window.scrollBy(0,500)", "");
				        
					  Actions builder1 = new Actions(driver);
					  WebElement canvasElement1 = driver.findElement(signaturePad);
					  Action drawAction1 = builder1.moveToElement(canvasElement1, 20, 20).clickAndHold().moveByOffset(80, 80)
					                .moveByOffset(50, 20).release().build();
					  drawAction1.perform();
					  waitForSometime(tcConfig.getConfig().get("LowWait"));
					  waitUntilElementVisibleBy(driver, confirmBtn, "ExplicitWaitLongestTime");
					  clickElementBy(confirmBtn);
					  waitForSometime(tcConfig.getConfig().get("MedWait"));
					  waitUntilElementVisibleBy(driver, reconfirmBtn, "ExplicitWaitLongestTime");
					  if(verifyObjectDisplayed(reconfirmBtn)){
					    	clickElementBy(reconfirmBtn);
					    	waitForSometime(tcConfig.getConfig().get("LowWait"));
					    }
				    else{
					    	System.out.println("No Re-Confirm Button");
					    }
					
				  }
				  
				  //end of change
				  
				  /*waitUntilElementVisibleBy(driver, reconfirmBtn, "ExplicitWaitLongestTime");
					clickElementBy(reconfirmBtn);*/
				 tcConfig.updateTestReporter("CommunityLeadsPage", "bookAppointmentCommunity", Status.PASS,"Appoitment has been booked");
				}catch(Exception e){
					//tcConfig.updateTestReporter("CommunityLeadsPage", "bookAppointmentCommunity", Status.FAIL,"Failed to book appointment");
							
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			
	}
	
	protected By imgSearch=By.xpath("//a/img[contains(@src,'Search')]");
	protected By txtSearch=By.xpath("//input[@type='search']");
	protected By btnViewDetails=By.xpath("//button[text()='VIEW DETAILS']");
	protected By linkTours=By.xpath("//a[contains(text(),'Tours')]");
	protected By txtLinkTour=By.xpath("//table[contains(@class,'cTMCMALeadsTours')]//td[1]//a");
	protected By cancelTourBtn=By.xpath("//button[text()='Cancel Tour']");
	protected By yesBtn=By.xpath("//button[text()='YES']");
	protected By successMsgPopUp=By.xpath("//span[text()='Tour Cancelled Successfully']/../div[text()='Success']");
	public void cancelCommunityTour(){
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, imgSearch, "ExplicitWaitLongestTime");

			clickElementBy(imgSearch);
			waitUntilElementVisibleBy(driver, txtSearch, "ExplicitWaitLongestTime");
			fieldDataEnter(txtSearch, strLeadFullName);
			driver.switchTo().activeElement().sendKeys(Keys.ENTER);
			waitUntilElementVisibleBy(driver, btnViewDetails, "ExplicitWaitLongestTime");
			clickElementBy(btnViewDetails);
			waitUntilElementVisibleBy(driver, linkTours, "ExplicitWaitLongestTime");
			clickElementBy(linkTours);
			try{
				waitUntilElementVisibleBy(driver, txtLinkTour, "ExplicitWaitLongestTime");
				createdTourId=driver.findElement(txtLinkTour).getText().trim();
				tcConfig.updateTestReporter("CommunityLeadsPage", "cancelCommunityTour", Status.PASS,"Created Tour Id : "
						+ createdTourId);
			}catch(Exception e){
				tcConfig.updateTestReporter("CommunityLeadsPage", "cancelCommunityTour", Status.FAIL,"No Tour Has been Created");
			}
			
			clickElementBy(txtLinkTour);
			waitUntilElementVisibleBy(driver, cancelTourBtn, "ExplicitWaitLongestTime");
			clickElementBy(cancelTourBtn);
			
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, yesBtn, "ExplicitWaitLongestTime");
				clickElementBy(yesBtn);			
			
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, successMsgPopUp, "ExplicitWaitLongestTime");
			if(verifyObjectDisplayed(successMsgPopUp)){
				tcConfig.updateTestReporter("CommunityLeadsPage", "cancelCommunityTour", Status.PASS,"Tour Cancelled Suuccessfully");
			}else{
				tcConfig.updateTestReporter("CommunityLeadsPage", "cancelCommunityTour", Status.FAIL,"Unable to cancel Tour");
			}
		
	}
	
	
	
}
