package eWorksheet.pages;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class eWorksheetBasePage extends FunctionalComponents {

	public eWorksheetBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}
	
	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser Author : CTS Date : 2019 Change By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}
	
	/*
	 * *************************Method: checkAndSetBrowser ***
	 * ***************Description: Check Driver and Set Browser *********
	 */
	public WebDriver checkAndSetBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {

			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {

				log.info("Unable to launch driver : Error -" + e.getMessage());
			}
		}

		return tcConfig.getDriver();

	}
	
	/*
	 * *************************Method: mouseoverAndClick ***
	 * *************Description: mouse hover And Click**
	 */
	public void mouseoverAndClick(WebElement element) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			new Actions(driver).moveToElement(element).click().perform();
		} catch (Exception e) {
			log.info("unable to hover and click " + "element due to " + e.getMessage());
		}

	}
	
	/*
	 * *************************Method: mouseoverAndClick ***
	 * *************Description: mouse hover And Click**
	 */
	public void mouseoverAndClick(By by) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", getObject(by));
			new Actions(driver).moveToElement(getObject(by)).click().perform();
		} catch (Exception e) {
			log.info("unable to hover and click " + "element due to " + e.getMessage());
		}

	}

	/**
	 * Navigate to Application URL
	 * 
	 * @param applicationURl
	 */
	public void navigateToAppURL(String applicationURl) {
		driver.navigate().to(applicationURl);
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info("No Alert Present");
		}
	}
	
	protected By loadSpinner = By.xpath("//div[@class='loader__tick']");
	/*
	 * *************************Method: Specific to EWS ***
	 * *************Description: **
	 */

	public void checkLoadingSpinnerEWS() {
		waitUntilElementInVisible(driver, loadSpinner, 120);
	}

	public void pageDown() {
		driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
	}

	public void pageUp() {
		driver.switchTo().activeElement().sendKeys(Keys.PAGE_UP);
	}

	public String getValuefromTable(WebElement element, int row, int col) {
		String celtext = "";
		try {
			// To locate table.
			WebElement mytable = element;
			// To locate rows of table.
			List<WebElement> rows_table = mytable.findElements(By.tagName("tr"));
			// To locate columns(cells) of that specific row.
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
			// To retrieve text from that specific cell.
			celtext = Columns_row.get(col).getText();
			System.out.println("Cell Value of row number " + row + " and column number " + col + " Is " + celtext);
			System.out.println("-------------------------------------------------- ");

		} catch (Exception e) {
			tcConfig.updateTestReporter("VerifyTable", "getValuefromTable", Status.FAIL,
					"Dropdown Value " + celtext + " " + "is not retrieved successfully");

		}
		return celtext;

	}

	public boolean verifySelectOptions(By by, String testName) {
		boolean flag = false;
		int i;
		String strOption = "";
		try {

			Select select = new Select(driver.findElement(by));
			List<WebElement> allOptions = select.getOptions();
			for (i = 0; i <= allOptions.size(); i++) {
				strOption = allOptions.get(i).getText().replace(" ", "");
				if (strOption.equals(testName)) {
					flag = true;
					break;
				}
			}
			if (flag) {
				tcConfig.updateTestReporter("Down Payment Page", "verifySelectOptions", Status.PASS,
						"Dropdown Value " + strOption + " " + "is displayed successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Down Payment Page", "verifySelectOptions", Status.FAIL,
					"Dropdown Value " + strOption + " " + "is not displayed successfully");
		}
		return flag;
	}

	/**
	 **********************************************************************
	 * @Project Name : eWorksheet Regression Suite.
	 * @Function Name : isSorted()
	 * @Description : This function is used to sort the list
	 * @param :
	 *            List<String> listOfStrings - List needs to be passed here
	 * @Return :
	 * @Date : September, 16, 2019
	 * @Author : Priya Das
	 ***********************************************************************
	 */

	public static boolean isSorted(List<String> listOfStrings) {
		if (listOfStrings.size() == 0 || listOfStrings.size() == 1) {
			return true;
		}

		Iterator<String> iter = listOfStrings.iterator();
		String current, previous = iter.next();
		while (iter.hasNext()) {
			current = iter.next();
			if (previous.compareTo(current) > 0) {
				return true;
			}
			previous = current;
		}
		return true;
	}

	public static void scrollDowntoXPath(By by, RemoteWebDriver driver) {
	    boolean flag=true;
	    int count=1;
	    while(flag){
	        try {
	            driver.findElement(by);
	            flag=false;
	            break;
	        }
	        catch(Exception NoSuchElementException) {
	            count=count+1;
	            Map<String, Object> params = new HashMap<>();
	            params.put("start","40%,90%");
	            params.put("end","40%,20%");
	            params.put("duration","2");
	            Object res= driver.executeScript("mobile:touch:swipe",params);
	        if(count==5)
	        {
	            break;
	        }
	        }
	    }
	}
	
	
	
	public void drag_horizontal( )
    {
         Map<String, Object> params = new HashMap<>();
         params.put("location", "20%,55%,40%,55%");
         params.put("auxiliary", "tap");
         params.put("duration", "3");
         ((RemoteWebDriver)driver).executeScript("mobile:touch:drag", params);
    }
	
	
	public void javaScriptSendkeys( By by, String text) {

        String jsScript = "let input = arguments[0]; \r\n" + "let lastValue = input.value;\r\n"

                + "input.value = arguments[1];\r\n" + "let event = new Event('input', { bubbles: true });\r\n"

                + "event.simulated = true;\r\n"

                + "let tracker = input._valueTracker;\r\n" + "if (tracker) {\r\n" + "  tracker.setValue(lastValue);\r\n"

                + "}\r\n" + "input.dispatchEvent(event);";

       ((RemoteWebDriver)driver).executeScript(jsScript, driver.findElement(by), text);

	}

	@SuppressWarnings("deprecation")
	public void enterDataKeyboard(By by,String strData)
			{
			clickElementBy(by);
			waitForSometime(tcConfig.getConfig().get("WaitForASec"));
			clearElementBy(by);
			waitForSometime(tcConfig.getConfig().get("WaitForASec"));
			
			((RemoteWebDriver) driver).getKeyboard().sendKeys(strData);
			
			}
	
}
