package eWorksheet.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CWAPitchBenefitDetailsPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(CWAPitchBenefitDetailsPage.class);

	public CWAPitchBenefitDetailsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String resortID = "";
	public static String strPoint = "";
	public static String packageVersion = "";
	public static int pckgVersion = 0;
	public String creditBand;
	public double InterestRt;
	public int cwaPitchTileCount;
	protected By cwaPackage = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@ng-reflect-klass='card package-card ']");
	protected By cwaPackageSelect = By.xpath("//div[@ng-reflect-klass='card package-card ']");
	protected By cwaPackagePoints = By
			.xpath("//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong");
	protected By cwaBenefitsAccess = By
			.xpath("//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']");
	protected By benefitsTab = By.xpath(
			"//div[@class='col card-expanded-body']//following-sibling::li/a[@id='tab1-link']/span[contains(text(),'Benefits')]");
	protected By financialTab = By.xpath(
			"//div[@class='col card-expanded-body']//following-sibling::li/a/span[contains(text(),'Financial')]");
	protected By ClubWynBenefitsLink = By.xpath(
			"//div[@class='col card-expanded-body']//following-sibling::div[@class='tab-content']/tab[@heading='Benefits']//following-sibling::li/a[@id='club-wyndham-link']");
	protected By estMonPayLabel = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='row package-card-row']//following-sibling::div[@class='card-body']//following-sibling::p");
	protected By gotoWorksheetButton = By.xpath(
			"//button[not(contains(@class,'btn w-100 btn-black-outline disabled')) and contains(text(),'Go to Worksheet')]");
	protected By OptionLink = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::ul/li[@role='menuitem']/a[contains(text(),'Options ')]");
	protected By proceedButton = By.xpath("//button[contains(text(),'Yes, proceed to options')]");
	protected By pointsTab = By.xpath("(//span[contains(text(),'Points')])[2]");
	// protected By minimumPoints = By.xpath("//h3[contains(text(),'Select CWA
	// Points Package Amount')]/../label");
	protected By selectTourPoint = By.xpath(
			"(//h3[contains(text(),'Select CWA Points Package Amount')]//following::div[@class='package-points-card-wrapper'])[1]//following-sibling::div[@ng-reflect-klass='package-points-card']/strong");
	protected By newBtn = By.xpath("//button[contains(text(),'New')]");
	protected By removeBtn = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::ul/li[@role='menuitem']/a[contains(text(),'Remove')]");
	protected By removeButton = By
			.xpath("//div[@class='card-header']//following::ul/li[@role='menuitem']/a[contains(text(),'Remove')]");
	protected By RecycleBinLink = By.xpath("//a[@class='nav-link']/img[@alt='icon-drive-white-bg-blue']");
	protected By moreLink = By.xpath(
			"//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::button[@id='button-basic']");
	protected By moreLinkClick = By.xpath("//div[@class='card-header']//following::button[@id='button-basic']");
	protected By errorMsgForOnePackage = By.xpath("//h2[contains(text(),'Operation not allowed!')]");
	protected By tourPointsinRecycleCard = By.xpath("//div[@class='card-img-overlay']/strong");
	protected By customDropPrice = By.xpath("//input[@placeholder='Custom Drop ($)' and @type='text']");
	protected By custompriceDropCheckBox = By.xpath(
			"//input[@placeholder='Custom Drop ($)' and @type='text']//following::input[@type='checkbox' and @id='pricing-checkbox3']");
	protected By applyBtn = By
			.xpath("//div[@class='cwa-points-package-footer-wrapper']/button[contains(text(),'Apply')]");
	protected By selectDiscountAmtLabel = By.xpath("//h3[contains(text(),'Select Discount Amount')]");
	protected By purchasePriceLabel = By
			.xpath("(//table[@class='table custom-table']/tbody/tr/td[contains(text(),'Purchase Price')])[1]");
	protected By noDropPriceLabel = By.xpath("//label[contains(text(),'No Drop 0%')]/../div/strong");
	protected By creditTab = By.xpath("(//span[contains(text(),'Credit')])[1]");
	protected By cusProRatingA1 = By.xpath(
			"//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='A1']");
	protected By cusProRatingA = By.xpath(
			"//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='A']");
	protected By cusProRatingB1 = By.xpath(
			"//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='B1']");
	protected By cusProRatingB = By.xpath(
			"//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='B']");
	protected By cusProRatingC1 = By.xpath(
			"//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='C1']");
	protected By cusProRatingC = By.xpath(
			"//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='C']");
	protected By cusNOBand = By.xpath(
			"//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='No Band']");
	protected By maxDiscountAllowed = By
			.xpath("//h3[contains(text(),'Select Discount Amount')]/../label[@class='cwa-label']");
	protected By noDropCheckBox = By.xpath(
			"//label[contains(text(),'No Drop 0%')]/../div/strong/../div/input[@id='pricing-checkbox0' and @type='checkbox']");
	protected By DropPrice = By.xpath("(//td[text()='Discount:']//following::td)[1]");
	protected By purchasePrice = By.xpath("(//td[text()='Purchase Price:']//following::td)[1]");
	protected By todaysPriceUI = By.xpath("(//td[text()='Today’s Price:']//following::td)[1]");
	protected By txtPurchasePrice = By.xpath("//h5[text()='Purchase Price ']/strong");
	protected By firstDropCheckBox = By.xpath(
			"//label[contains(text(),'1st drop Drop 50%')]/../div/strong/../div/label[@for='pricing-checkbox1']");
	protected By secondDropCheckBox = By.xpath(
			"//label[contains(text(),'2nd drop Drop 100%')]/../div/strong/../div/label[@for='pricing-checkbox2']");
	protected By secondDropcheckboxselected = By.xpath
			("//label[contains(text(),'2nd drop Drop 100%')]/../div/strong/../div/label[@for='pricing-checkbox2']/../input");
	protected By txtTodaysPrice = By.xpath("//div[@class='card-body']/h5/strong");
	public static By sideMenuLink = By.xpath("//img[@alt='icon-more']");
	protected By exitBtn = By.xpath("//a[@class='dropdown-item']/img[@alt='icon-logout-grey']");
	protected By dashBoardBtn = By.xpath("//a[text()=' Dashboard ']/img[@alt='icon-user-circle-outline-grey']");
	protected By exitSureMessage = By.xpath("//button[contains(text(),'Yes, I’m sure')]");
	protected By comparePckgBtn = By.xpath("//button[text()='Compare Packages']");
	protected By financeTab = By.xpath("//span[text()='Financial']");
	protected By benefitTab = By.xpath("//span[text()='Benefits']");
	protected By financeRowCount = By.xpath("//tab[@id='financial']/table/tbody/tr");
	protected By benefitRowCount = By.xpath("//tab[@id='club-wyndham']/table/tbody/tr");
	protected By packageExist = By.xpath("//div[@ng-reflect-klass='card package-card']");
	protected By restoreOption = By.xpath("//div[@class='card-header']//following::button[@id='button-basic']");
	protected By restoretoScrnLink = By.xpath("//a[text()=' Restore to Screen ']");
	protected By financingTab = By.xpath("//span[contains(text(),'Financing')]");
	protected By downPaymentHeader = By.xpath("//h3[text()='Down Payment']");
	protected By minDownPaymentTxt = By.xpath("//h3[text()='Down Payment']/../label");
	protected By dpSelectChkBox = By.xpath("(//strong[text()='" + testData.get("DownPaymentPercentage")
			+ "']/..//following-sibling::div)[1]/label[contains(@for,'downPay')]");
	protected By todaysPriceforDp = By.xpath("(//td[@class='finance'])[1]");
	protected By downPayment = By.xpath("//td[text()='Down Payment Amount:']//following-sibling::td");
	protected By customDownPayment = By.xpath("//input[@id='downpay-custom' and @type='text']");
	protected By downPayChkBox = By.xpath("//label[@for='downPayCustom']");
	protected By downPaymentPercent = By.xpath("(//td[@class='finance'])[2]");
	protected By todaysPrice = By.xpath("(//td[text()='Today’s price:']//following::td)[1]");
	protected By financedAmttxt = By.xpath("//td[text()='Financed Amount:']//following-sibling::td");
	protected By firstDueDate = By.xpath("//td[text()='First Payment Due Date:']//following-sibling::td");
	protected By minLoanTermTxt = By.xpath("(//h3[text()='Loan Terms']//following::label)[1]");
	protected By yearsMonthsLabel = By.xpath("//label[text()='Years / Months']/..//following-sibling::div[1]/strong");
	protected By usBankActLabel = By.xpath("//span[text()='*US Bank Accounts Only']");
	protected By interestRate = By.xpath("//td[text()='Interest rate:']//following-sibling::td");
	protected By interestRateReduction = By.xpath("//td[text()='Interest rate reduction:']//following-sibling::td");
	protected By finalInterestRt = By.xpath("//td[text()='Final interest rate']//following-sibling::td");
	protected By ACHChkBox = By.xpath("//input[@id='autopay-ach' and @type='checkbox']/../label");
	protected By monthlyLoanPay = By.xpath("//td[text()='Monthly loan payment']//following-sibling::td");
	protected By usuryLimitLabel = By.xpath("//label[text()=' Usury Limit: 18.00% ']");
	protected By promotionalDropdown = By.xpath("//select[@id='promoRate']");
	protected By promotionalLabel = By.xpath("//h4[text()='Today’s Promotional ']");
	protected By customTermLabel = By.xpath("//input[@id='terms-custom']");
	protected By customTermChkBox = By.xpath("//label[@for='loanTermsCustom']");
	protected By fiveYrDefaultLabel = By.xpath("(//label[text()='Years / Months']/..//following-sibling::input)[1]");
	protected By rowCount = By.xpath("//tab[@id='club-wyndham']/table/tbody/tr/td");
	protected By pointsCheckBox = By.xpath(
			"((//h3[contains(text(),'Select CWA Points Package Amount')]//following::div[@class='package-points-card-wrapper'])[1]//following-sibling::div[@ng-reflect-klass='package-points-card']/div/label)[1]");
	protected By pricingTab = By.xpath("//span[text()='Pricing']");
	protected By downPayToday = By.xpath("(//p[contains(text(),'Down Payment Today')])[1]");
	protected By customTermErrorMsg = By.xpath("//h2[text()='Please decrease Loan Term value']");
	protected By okBtn = By.xpath("//button[text()='OK']");
	protected By minDownPaymentCheck = By.xpath("(//strong[text()='" + testData.get("DownPaymentPercentage")
			+ "']/..//following-sibling::div)[1]/input[@type='checkbox']");
	protected By selectCustCheckBox = By
			.xpath("((//table[contains(@class,'view-credit-table')])[2]/tbody/tr//descendant::Label)[1]");
	protected By eWLogo = By.xpath("//img[@alt='eworksheet-logo']");
	protected By refreshIcon = By.xpath("//img[@alt='icon-refresh-white']");
	protected By emptyBinMsg = By.xpath("//h2[text()='Recycle Bin is empty']");
	public static HashMap<String, String> resortIDNumber = new HashMap<String, String>();
	
	protected By purhcaseplusIcon = By.xpath("//span[@class = 'custom-icon']//ancestor::h3");
	protected By purchaseIncentivedrpdown = By.xpath("//select[@id= 'purchaseIncentive']");
	protected By priceAdjustment = By.xpath("//input[@id= 'priceAdjustment']"); 
	protected By incentiveApply = By.xpath("//button[text()= 'Apply']");


	public void verifyPitchBenefitTilePlacedSideBySide() {

		try {
			/*
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(gotoWorksheetButton).click();
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchTileList = driver.findElements(cwaPackage);
			cwaPitchTileCount = cwaPitchTileList.size();
			for (int i = 1; i <= cwaPitchTileCount; i++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyElementDisplayed(driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@ng-reflect-klass='card package-card '])["
								+ i + "]")))) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyPitchBenefitTilePlacedSideBySide",
							Status.PASS, "Pitch Benefit Tile" + " " + i + " " + "is displayed in Side by Side ");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyPitchBenefitTilePlacedSideBySide",
							Status.FAIL, "Pitch Benefit Tile" + " " + i + " " + "is not displayed in Side by Side ");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void accessManagerDashboard() {
		try {
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, sideMenuLink, "ExplicitWaitLongestTime");
			driver.findElement(sideMenuLink).click();
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, dashBoardBtn, "ExplicitWaitLongestTime");
			clickElementBy(dashBoardBtn);
			waitUntilElementVisibleBy(driver, eWLogo, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(eWLogo))) {
				tcConfig.updateTestReporter("All Tour Dashboard Page", "accessManagerDashboard", Status.PASS,
						"Page is redirected to Manager dashboard page successfully");
			} else {
				tcConfig.updateTestReporter("All Tour Dashboard Page", "accessManagerDashboard", Status.FAIL,
						"Page is not redirected to Manager dashboard page successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("All Tour Dashboard Page", "accessManagerDashboard", Status.FAIL,
					"Page is not redirected to Manager dashboard page successfully" + e.getMessage());
		}
	}

	public void verifyPitchBenefitTileIncOrder() {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchTileList = driver.findElements(cwaPackage);
			cwaPitchTileCount = cwaPitchTileList.size();
			for (int i = 1; i <= cwaPitchTileCount; i++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyElementDisplayed(driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@ng-reflect-klass='card package-card '])["
								+ i + "]")))) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyPitchBenefitTilePlacedSideBySide",
							Status.PASS, "Pitch Benefit Tile" + " " + i + " " + "is displayed in Side by Side ");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyPitchBenefitTilePlacedSideBySide",
							Status.FAIL, "Pitch Benefit Tile" + " " + i + " " + "is not displayed in Side by Side ");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyEstimatedMonthlyLabel() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> allListLabel = driver.findElements(estMonPayLabel);
			for (int l = 2; l <= allListLabel.size(); l += 2) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String estMonthlyPayLabel[] = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='row package-card-row']//following-sibling::div[@class='card-body']//following-sibling::p)["
								+ l + "]"))
						.getText().split("\n");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (estMonthlyPayLabel[0].equals("Estimated Monthly Payments")) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyEstimatedMonthlyLabel",
							Status.PASS, "Estimated Monthly Payment label is displayed successfully for PitchTile");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyEstimatedMonthlyLabel",
							Status.FAIL,
							"Estimated Monthly Payment label is not displayed successfully for PitchTile");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyCWAPackagePointsInOrder() {
		boolean isPitchListSorted;
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			List<String> allList = new ArrayList<String>();
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*
				 * String cwaPackagePointFormat =
				 * ft.format(driver.findElement(By .xpath(
				 * "(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
				 * + j + "]")) .getText());
				 */
				String cwaPackagePointFormat = String.format("###,###",
						driver.findElement(By
								.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
										+ j + "]"))
								.getText());
				if (cwaPackagePointFormat.matches("###,###")) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyCWAPackagePointsInOrder",
							Status.PASS,
							"Pitch Benefit Point of tile" + " " + j + " " + "is displayed in Proper format ");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyCWAPackagePointsInOrder",
							Status.FAIL,
							"Pitch Benefit Point of tile" + " " + j + " " + "is not displayed in Proper format ");
				}
				String cwaPitchWynClubPoints = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText().replace(",", "");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				allList.add(cwaPitchWynClubPoints);
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			isPitchListSorted = isSorted(allList);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (isPitchListSorted == true) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyCWAPackagePointsInOrder",
						Status.PASS, "The order of Pitch tiles is in Lower to Higher");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "verifyCWAPackagePointsInOrder",
						Status.FAIL, "The order of Pitch tiles is not in Lower to Higher");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void validateWynBenefitAccess() {
		try {
			/*
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * driver.findElement(gotoWorksheetButton).click();
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> benefitList = driver.findElements(cwaBenefitsAccess);
			for (int k = 1; k <= benefitList.size(); k++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strBenefits = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header'])["
								+ k + "]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (strBenefits.equals("CLUB WYNDHAM ACCESS")) {
					driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
									+ k + "]"))
							.click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyElementDisplayed(
							driver.findElement(By.xpath("(//li/a[@id='club-wyndham-link'])[" + k + "]")))) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						String strBenefit1 = driver
								.findElement(By.xpath(
										"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[1]"))
								.getText();
						waitForSometime(tcConfig.getConfig().get("LongWait"));
						String strBenefit2 = driver
								.findElement(By.xpath(
										"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[2]"))
								.getText();
						waitForSometime(tcConfig.getConfig().get("LongWait"));
						String strBenefit3 = driver
								.findElement(By.xpath(
										"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[3]"))
								.getText();
						waitForSometime(tcConfig.getConfig().get("LongWait"));
						String strBenefit4 = driver
								.findElement(By.xpath(
										"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[4]"))
								.getText();
						waitForSometime(tcConfig.getConfig().get("LongWait"));
						String strBenefit5 = driver
								.findElement(By.xpath(
										"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[5]"))
								.getText();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						String strBenefit6 = driver
								.findElement(By.xpath(
										"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[6]"))
								.getText();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						String strBenefit7 = driver
								.findElement(By.xpath(
										"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[7]"))
								.getText();
						waitForSometime(tcConfig.getConfig().get("LongWait"));
						if (strBenefit1.equals("RCI") && strBenefit2.equals("Vacation Sidekick")
								&& strBenefit3.equals("Club Pass")
								&& strBenefit4.equals("Advanced Reservation Priority")
								&& strBenefit5.equals("Wyndham Rewards") && strBenefit6.equals("Points Example")
								&& strBenefit7.equals("Plus Partners")) {
							waitForSometime(tcConfig.getConfig().get("LongWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']/a/img[@alt='icon-close-green'])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("LongWait"));
							tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess",
									Status.PASS,
									"Wyndham Club Benefits " + strBenefit1 + "," + strBenefit2 + "," + strBenefit3 + ","
											+ strBenefit4 + "," + strBenefit5 + "," + strBenefit6 + "," + strBenefit7
											+ " is displayed and closed successfully");
						} else {
							tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess",
									Status.FAIL, "Wyndham Club Benefits is not displayed successfully");
						}
					}

				}

			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess", Status.FAIL,
					"Wyndham Club Benefits is not displayed successfully");
		}
	}

	@FindBy(xpath = "//img[@alt='icon-close']")
	protected WebElement iconClose;

	public void validateWynCollateralBenefitAccess() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> benefitList = driver.findElements(cwaBenefitsAccess);
			for (int k = 1; k <= benefitList.size(); k++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strBenefits = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header'])["
								+ k + "]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (strBenefits.equals("CLUB WYNDHAM ACCESS")) {
					driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
									+ k + "]"))
							.click();
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyElementDisplayed(
							driver.findElement(By.xpath("(//li/a[@id='club-wyndham-link'])[" + k + "]")))) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						if (k == 1) {
							String strBenefit1 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[1]"))
									.getText();
							driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[1]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(
									driver.findElement(By.xpath("//iframe[contains(@src,'rci-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit1 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit1 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[2]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit1 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit1 + " is not viewed");
							}
							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit2 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[3]"))
									.getText();
							driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[3]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(driver.findElement(
									By.xpath("//iframe[contains(@src,'vacation-sidekick-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit2 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit2 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[4]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit2 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit2 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit3 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[5]"))
									.getText();
							driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[5]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(
									driver.findElement(By.xpath("//iframe[contains(@src,'club-pass-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit3 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit3 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[6]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit3 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit3 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit4 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[7]"))
									.getText();
							driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[7]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(
									driver.findElement(By.xpath("//iframe[contains(@src,'arp-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit4 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit4 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[8]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit4 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit4 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit5 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[9]"))
									.getText();
							driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[9]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(driver
									.findElement(By.xpath("//iframe[contains(@src,'wyndham-rewards-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit5 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit5 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[10]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit5 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit5 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							String strBenefit6 = driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[11]"))
									.getText();
							driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[11]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(
									driver.findElement(By.xpath("//iframe[contains(@src,'points-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit6 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit6 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[12]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit6 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit6 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							String strBenefit7 = driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[13]"))
									.getText();
							driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[13]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(driver
									.findElement(By.xpath("//iframe[contains(@src,'plus-partners-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit7 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit7 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[14]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit7 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit7 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							if (strBenefit1.equals("RCI") && strBenefit2.equals("Vacation Sidekick")
									&& strBenefit3.equals("Club Pass")
									&& strBenefit4.equals("Advanced Reservation Priority")
									&& strBenefit5.equals("Wyndham Rewards") && strBenefit6.equals("Points Example")
									&& strBenefit7.equals("Plus Partners")) {
								waitForSometime(tcConfig.getConfig().get("LongWait"));
								driver.findElement(By
										.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']/a/img[@alt='icon-close-green'])["
												+ k + "]"))
										.click();
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess",
										Status.PASS, "Wyndham Club Benefits is displayed and closed successfully");
							} else {
								tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess",
										Status.FAIL, "Wyndham Club Benefits is not displayed successfully");
							}
						} else {
							String strBenefit1 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[1]"))
									.getText();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[2]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit1 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit1 + " is not viewed");
							}
							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit2 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[3]"))
									.getText();

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[4]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit2 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit2 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit3 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[5]"))
									.getText();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[6]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit3 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit3 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit4 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[7]"))
									.getText();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[8]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit4 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit4 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							String strBenefit5 = driver.findElement(By
									.xpath("((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[9]"))
									.getText();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[10]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit5 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit5 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							String strBenefit6 = driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[11]"))
									.getText();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[11]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (verifyElementDisplayed(
									driver.findElement(By.xpath("//iframe[contains(@src,'points-eworksheet')]")))) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit6 + " is displayed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit6 + " is not displayed");
							}
							iconClose.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By
									.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-footer']/a[1][contains(text(),' More Details ')])["
											+ k + "]"))
									.click();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.findElement(By.xpath("(//span[text()='Benefits'])[" + k + "]")).click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if (driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[12]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit6 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit6 + " is not viewed");
							}
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							String strBenefit7 = driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[13]"))
									.getText();
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							if (driver.findElement(By.xpath(
									"((//tab[@id='club-wyndham'])[" + k + "]//following-sibling::li/a/span)[14]"))
									.getText().equals("Viewed")) {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.PASS,
										"Collateral view for Benefit " + strBenefit7 + " is viewed");
							} else {
								tcConfig.updateTestReporter("CWA Pitch Benefit Page",
										"validateWynCollateralBenefitAccess", Status.FAIL,
										"Collateral view for Benefit " + strBenefit7 + " is not viewed");
							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							if (strBenefit1.equals("RCI") && strBenefit2.equals("Vacation Sidekick")
									&& strBenefit3.equals("Club Pass")
									&& strBenefit4.equals("Advanced Reservation Priority")
									&& strBenefit5.equals("Wyndham Rewards") && strBenefit6.equals("Points Example")
									&& strBenefit7.equals("Plus Partners")) {
								waitForSometime(tcConfig.getConfig().get("LongWait"));
								driver.findElement(By
										.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']/a/img[@alt='icon-close-green'])["
												+ k + "]"))
										.click();
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess",
										Status.PASS, "Wyndham Club Benefits is displayed and closed successfully");
							} else {
								tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess",
										Status.FAIL, "Wyndham Club Benefits is not displayed successfully");
							}

						}

					}

				}

			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateWynBenefitAccess", Status.FAIL,
					"Wyndham Club Benefits is not displayed successfully");
		}
	}

	public void searchTourPackage() {
		strPoint = testData.get("TourPoints");
		pageCheck();
		waitUntilElementVisibleBy(driver, cwaPackagePoints, "ExplicitWaitLongestTime");
		List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
		cwaPitchTileCount = cwaPitchPoints.size();
		for (int j = 1; j <= cwaPitchPoints.size(); j++) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String cwaPackagePoint = driver.findElement(By
					.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
							+ j + "]"))
					.getText();
			if (strPoint.equals(cwaPackagePoint)) {
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				packageVersion = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@ng-reflect-klass='card package-card ']/div[@class='card-header']/span)["
								+ j + "]"))
						.getText();
				if (packageVersion.equals("")) {
					packageVersion = "0";
				}
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::button[@id='button-basic'])["
								+ j + "]"))
						.click();
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
						"Tour Point is displayed successfully");
				break;
			}

		}

	}

	public void clickMoreDetails() {
		try {
			driver.findElement(By
					.xpath("//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong[text()='"
							+ testData.get("TourPoints") + "']/../../div/a[1]"))
					.click();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@FindBy(xpath = "(//li/a[@id='enhancements-link'])[1]")
	protected WebElement enhancementLink;

	protected By year1txt = By.xpath("(//strong[text()='" + testData.get("TourPoints")
			+ "']/../../..//table/thead/tr/th/strong[text()='Year 1'])[1]");
	protected By year2txt = By.xpath("(//strong[text()='" + testData.get("TourPoints")
			+ "']/../../..//table/thead/tr/th/strong[text()='Year 2'])[1]");
	protected By year3txt = By.xpath("(//strong[text()='" + testData.get("TourPoints")
			+ "']/../../..//table/thead/tr/th/strong[text()='Year 3'])[1]");
	protected By annualPointTxt = By
			.xpath("(//strong[text()='" + testData.get("TourPoints") + "']/../../..//span[text()='Annual Points'])[1]");
	protected By picPlusTxt = By
			.xpath("(//strong[text()='" + testData.get("TourPoints") + "']/../../..//span[text()='Pic Plus'])[1]");
	protected By BonusPointTxt = By
			.xpath("(//strong[text()='" + testData.get("TourPoints") + "']/../../..//span[text()='Bonus Points'])[1]");

	public void validateBenefitinPitchTile() {
		try {
			driver.findElement(By.xpath("//strong[text()='" + testData.get("TourPoints")
					+ "']/../../../div[2]//following-sibling::li/a/span[text()='Benefits']")).click();
			enhancementLink.click();
			if (verifyElementDisplayed(driver.findElement(year1txt))
					&& verifyElementDisplayed(driver.findElement(year2txt))
					&& verifyElementDisplayed(driver.findElement(year3txt))) {
				tcConfig.updateTestReporter("CWA Package Summary Page", "validateBenefitinPitchTile", Status.PASS,
						"Years are displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWA Package Summary Page", "validateBenefitinPitchTile", Status.FAIL,
						"Years are not displayed successfully");
			}
			if (verifyElementDisplayed(driver.findElement(annualPointTxt))
					&& verifyElementDisplayed(driver.findElement(picPlusTxt))
					&& verifyElementDisplayed(driver.findElement(BonusPointTxt))) {
				tcConfig.updateTestReporter("CWA Package Summary Page", "validateBenefitinPitchTile", Status.PASS,
						"Benefits are displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWA Package Summary Page", "validateBenefitinPitchTile", Status.FAIL,
						"Benefits are not displayed successfully");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void searchTourPckg() {
		String strPoint = testData.get("TourPoints");
		try {
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					pckgVersion = Integer.parseInt(driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@ng-reflect-klass='card package-card ']/div[@class='card-header']/span)["
									+ j + "]"))
							.getText());
					driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::button[@id='button-basic'])["
									+ j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
							"Tour Point is displayed successfully");
					break;
				}

			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void searchSelectedTourPackage() {
		String strPoint = testData.get("TourPointsSelect");
		try {
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					packageVersion = driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@ng-reflect-klass='card package-card ']/div[@class='card-header']/span)["
									+ j + "]"))
							.getText();
					driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-header']//following::button[@id='button-basic'])["
									+ j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
							"Tour Point is displayed successfully");
					break;
				}

			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void addorDeleteCWAPackage() {
		String strPoint = testData.get("TourPointsAdd");
		String strPackage = testData.get("PackageSelect");
		try {
			// driver.findElement(gotoWorksheetButton).click();
			if (strPackage.equals("Add")) {
				driver.findElement(OptionLink).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(proceedButton).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, pointsTab, 240);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(pointsTab).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> tourPointList = driver.findElements(selectTourPoint);
				for (int i = 1; i <= tourPointList.size(); i++) {
					String strTourPoint = driver.findElement(By
							.xpath("((//h3[contains(text(),'Select CWA Points Package Amount')]//following::div[@class='package-points-card-wrapper'])[1]//following-sibling::div[@ng-reflect-klass='package-points-card']/strong)["
									+ i + "]"))
							.getText();
					if (strTourPoint.equals(strPoint)) {
						driver.findElement(By
								.xpath("((//h3[contains(text(),'Select CWA Points Package Amount')]//following::div[@class='package-points-card-wrapper'])[1]//following-sibling::div[@ng-reflect-klass='package-points-card']/div/label)["
										+ i + "]"))
								.click();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "addorDeleteCWAPackage",
								Status.PASS, "Tour Point is selected successfully");
						break;
					}
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(newBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
				for (int j = 1; j <= cwaPitchPoints.size(); j++) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String cwaPackagePoint = driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
									+ j + "]"))
							.getText();
					if (strPoint.equals(cwaPackagePoint)) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
								"Tour Point is added successfully");
						break;
					}

				}

			} else if (strPackage.equals("delete")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(removeBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
				for (int j = 1; j <= cwaPitchPoints.size(); j++) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String cwaPackagePoint = driver.findElement(By
							.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
									+ j + "]"))
							.getText();
					if (strPoint.equals(cwaPackagePoint)) {

						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.FAIL,
								"Tour Point is not get deleted from Tour Pitch");
						break;
					}

				}
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
						"Tour Point is deleted from Point Package Screen");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void validateRecycleBinAccess() {
		String strPoint = testData.get("TourPoints");
		try {
			/*
			 * waitForSometime(tcConfig.getConfig().get("LongWait"));
			 * driver.findElement(gotoWorksheetButton).click();
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchTileList = driver.findElements(cwaPackageSelect);
			int cwaPitchTileCount = cwaPitchTileList.size();
			if (cwaPitchTileCount == 1) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(moreLinkClick).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(removeButton).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				waitUntilElementVisibleBy(driver, errorMsgForOnePackage, "ExplicitWaitLongestTime");
				if (verifyElementDisplayed(driver.findElement(errorMsgForOnePackage))) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateRecycleBinAccess",
							Status.PASS, "Tour Package is not got deleted as there is only one tour package exists");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateRecycleBinAccess",
							Status.FAIL, "Tour Package is got deleted as there is only one tour package exists");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				searchTourPackage();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				addorDeleteCWAPackage();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(RecycleBinLink).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPitchPoints = driver.findElement(tourPointsinRecycleCard).getText();
				if (strPoint.equals(cwaPitchPoints)) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateRecycleBinAccess",
							Status.PASS, "Tour Package is moved to Recycle bin successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateRecycleBinAccess",
							Status.FAIL, "Tour Package is not moved to Recycle bin successfully");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateRecycleBinAccess", Status.FAIL,
					"Tour Package is not moved to Recycle bin successfully");
		}
	}

	public void validateEmptyRecycleBin() {
		try {
			waitUntilElementVisibleBy(driver, RecycleBinLink, "ExplicitWaitLongestTime");
			driver.findElement(RecycleBinLink).click();
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, emptyBinMsg, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(emptyBinMsg))) {
				tcConfig.updateTestReporter("Recycle Bin Page", "validateEmptyRecycleBin", Status.PASS,
						"Empty Recycle Bin Pop Up message is displayed successfully");
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("Recycle Bin Page", "validateEmptyRecycleBin", Status.FAIL,
						"Empty Recycle Bin Pop Up message is not displayed successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Recycle Bin Page", "validateEmptyRecycleBin", Status.FAIL,
					"Empty Recycle Bin Pop Up message is not displayed successfully" + e.getMessage());
		}
	}

	public void packageRestoreFromRecycleBin() {
		String strPoint = testData.get("TourPoints");
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(restoreOption).click();
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageRestoreFromRecycleBin", Status.PASS,
					"Tour Package is selected successfully");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(restoretoScrnLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageRestoreFromRecycleBin",
							Status.PASS, "Tour Package is moved to Pitch Tile screen successfully");
					break;
				}
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void applyPriceDiscount() {

		String strPurchasePrice = "";
		double intpurchasePrice;
		double inttodaysPrice;
		String strTodaysPrice = "";
		double inttodaysPriceUI = 0;
		int intPPPackageCard;
		double intMaxDiscount;
		//String strmaxAmount = "";
		double strmaxAmountUI = 0;

		String strPoint = testData.get("TourPoints");
		String strCustomDropPrice = testData.get("CustomDropPrice");
		String strDiscount = testData.get("Discount");

		waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
		driver.findElement(OptionLink).click();
		waitUntilElementVisibleBy(driver, proceedButton, "ExplicitWaitLongestTime");
		driver.findElement(proceedButton).click();
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, selectDiscountAmtLabel, 240);
		String strMaxDiscountLabel[] = driver.findElement(maxDiscountAllowed).getText().split(":");
		
		if (verifyElementDisplayed(driver.findElement(selectDiscountAmtLabel))
				&& verifyElementDisplayed(driver.findElement(purchasePriceLabel))) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
					"Salect Discount Amount & Purchase Price label is displayed successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
					"Select Discount Amount & Purchase Price label is not displayed successfully");
		}
		if (strMaxDiscountLabel[0].equals("Max Discount Allowed")) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
					"Maximum Discount allowed label is displayed successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
					"Maximum Discount allowed label is not displayed successfully");
		}

		String strNoDropPriceVal = driver.findElement(noDropPriceLabel).getText();
		if (strNoDropPriceVal.equals("$0")) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
					"No drop price is being displyed to $0");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
					"No drop price is not being displyed to $0");
		}
		
		if (strDiscount.equals("No Drop 0%")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(noDropCheckBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strDropPrice = driver.findElement(DropPrice).getText().substring(1).replace(",", "");
			double noDropPrice = Double.parseDouble(strDropPrice);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			strPurchasePrice = driver.findElement(purchasePrice).getText().substring(1).replace(",", "");
			intpurchasePrice = Double.parseDouble(strPurchasePrice);
			inttodaysPrice = intpurchasePrice - noDropPrice;
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
			inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
			if (inttodaysPriceUI == inttodaysPrice) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
						"Todays price is calculated correctly and displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
						"Todays price is not calculated correctly and displayed successfully");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					intPPPackageCard = Integer
							.parseInt(driver.findElement(By.xpath("(//h5[text()='Purchase Price ']/strong)[" + j + "]"))
									.getText().substring(1).replace(",", ""));
					if (intPPPackageCard == inttodaysPriceUI) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
								"Purchase Price is displayed successfully");
						break;
					} else {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
								"Purchase Price is not displayed successfully");
					}
				}
			}

		} else if (strDiscount.equals("Drop 50%")) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(firstDropCheckBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strDropPrice = driver.findElement(DropPrice).getText().substring(1).replace(",", "");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			double DropPrice = Double.parseDouble(strDropPrice);
			intMaxDiscount = Double.parseDouble(strMaxDiscountLabel[1].substring(2).replace(",", ""));
			double intfirstDrop50 = (intMaxDiscount / 2);
			if (intfirstDrop50 == DropPrice) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strPurchasePrice = driver.findElement(purchasePrice).getText().substring(1).replace(",", "");
				intpurchasePrice = Double.parseDouble(strPurchasePrice);
				inttodaysPrice = intpurchasePrice - DropPrice;
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
				inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
				if (inttodaysPriceUI == inttodaysPrice) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
							"Todays price is calculated correctly and displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
							"Todays price is not calculated correctly and displayed successfully");
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					intPPPackageCard = Integer
							.parseInt(driver.findElement(By.xpath("(//div[@class='card-body']/h5/strong)[" + j + "]"))
									.getText().substring(1).replace(",", ""));
					if (intPPPackageCard == inttodaysPriceUI) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
								"Purchase Price is displayed successfully");
						break;
					} else {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
								"Purchase Price is not displayed successfully");
					}
				}
			}

		} else if (strDiscount.equals("Drop 100%")) {
			//Ankur
			String IncentiveEligible = testData.get("IncentiveEligible");
			if (IncentiveEligible.equals("Y")){
				String purchaseIncentive = testData.get("PurchaseIncentive");
				String prcAdjustment = testData.get("PriceAdjustment");
				intMaxDiscount = Double.parseDouble(strMaxDiscountLabel[1].substring(2).replace(",", ""));
				System.out.println(intMaxDiscount);
				
				
				//if second drop already checked
				
				if (driver.findElement(secondDropcheckboxselected).getAttribute("ng-reflect-model").equals("true")){
					
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
							"Second Drop Already selected");
				}
				
				else{
				
			
				verifyElementDisplayed(driver.findElement(purhcaseplusIcon));
				
				
				clickElementBy(purhcaseplusIcon);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				verifyElementDisplayed(driver.findElement(purchaseIncentivedrpdown));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				selectByText(purchaseIncentivedrpdown,purchaseIncentive );
				fieldDataEnter(priceAdjustment,prcAdjustment );
				sendKeyboardKeys(Keys.TAB);
				double priceAdjustment =  Double.parseDouble(prcAdjustment);
				double newmaxDiscount = intMaxDiscount-priceAdjustment;
		
				System.out.println(newmaxDiscount);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strMaxDiscountLabel1[] = driver.findElement(maxDiscountAllowed).getText().split(":");
				strmaxAmountUI = Double.parseDouble(strMaxDiscountLabel1[1].substring(2).replace(",", ""));
				System.out.println(strmaxAmountUI);
				 
				 if(strmaxAmountUI==newmaxDiscount){
					
					 System.out.println("Discount correctly displayed");
					 
				 }
				 else{
					 tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
								"Discount not correctly displayed");
					 
				 }
				
				
			}
				
			}

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			driver.findElement(secondDropCheckBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strDropPrice = driver.findElement(DropPrice).getText().substring(1).replace(",", "");
			double DropPrice = Double.parseDouble(strDropPrice);
			String strMaxDiscountLabel1[] = driver.findElement(maxDiscountAllowed).getText().split(":");
			strmaxAmountUI = Double.parseDouble(strMaxDiscountLabel1[1].substring(2).replace(",", ""));
			
			
			//intMaxDiscount = Double.parseDouble(strMaxDiscountLabel[1].substring(2).replace(",", ""));
			double intsecondDrop100 = strmaxAmountUI;
			if (intsecondDrop100 == DropPrice) {
				strPurchasePrice = driver.findElement(purchasePrice).getText().substring(1).replace(",", "");
				intpurchasePrice = Double.parseDouble(strPurchasePrice);
				inttodaysPrice = intpurchasePrice - DropPrice;
				strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
				inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
				if (inttodaysPriceUI == inttodaysPrice) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
							"Todays price is calculated correctly and displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
							"Todays price is not calculated correctly and displayed successfully");
				}
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("ExplicitLongWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					intPPPackageCard = Integer
							.parseInt(driver.findElement(By.xpath("(//div[@class='card-body']/h5/strong)[" + j + "]"))
									.getText().substring(1).replace(",", ""));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (intPPPackageCard == inttodaysPriceUI) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
								"Purchase Price is displayed successfully");
						break;
					} else {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
								"Purchase Price is not displayed successfully");
					}
				}
			}
			

		} else if (strDiscount.equals("Custom Drop")) {
			driver.findElement(customDropPrice).click();
			driver.findElement(customDropPrice).sendKeys(strCustomDropPrice);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(custompriceDropCheckBox).click();
			String custDropPrice = driver.findElement(customDropPrice).getText();
			if (custDropPrice.matches("$##,###")) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
						"Custom Dollar Amount is displayed in $##,### format in Custom Drop Price field");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
						"Custom Dollar Amount is not displayed in $##,### format in Custom Drop Price field");
			}
			String strDropPrice = driver.findElement(DropPrice).getText().substring(1).replace(",", "");
			double noDropPrice = Double.parseDouble(strDropPrice);
			strPurchasePrice = driver.findElement(purchasePrice).getText().substring(1).replace(",", "");
			intpurchasePrice = Double.parseDouble(strPurchasePrice);
			inttodaysPrice = intpurchasePrice - noDropPrice;
			strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
			inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
			if (inttodaysPriceUI == inttodaysPrice) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
						"Todays price is calculated correctly and displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
						"Todays price is not calculated correctly and displayed successfully");
			}
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint.equals(cwaPackagePoint)) {
					intPPPackageCard = Integer
							.parseInt(driver.findElement(By.xpath("(//div[@class='card-body']/h5/strong)[" + j + "]"))
									.getText().substring(1).replace(",", ""));
					if (intPPPackageCard == inttodaysPriceUI) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
								"Purchase Price is displayed successfully");
						break;
					} else {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
								"Purchase Price is not displayed successfully");
					}
				}
			}
		}
	}

	public void packageVersionUpgrade() {
		String strCreditBand = testData.get("CustomerCreditBand");

		int strPrePackageVer = Integer.parseInt(packageVersion);
		driver.findElement(OptionLink).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(proceedButton).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, creditTab, 240);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(creditTab).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(cusProRatingA1))
				&& verifyElementDisplayed(driver.findElement(cusProRatingA))
				&& verifyElementDisplayed(driver.findElement(cusProRatingB1))
				&& verifyElementDisplayed(driver.findElement(cusProRatingB))
				&& verifyElementDisplayed(driver.findElement(cusProRatingC1))
				&& verifyElementDisplayed(driver.findElement(cusProRatingC))) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(selectCustCheckBox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(okBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(
					By.xpath("//h4[contains(text(),'Customer Provided')]//following::label[@for='customer-checkbox1"
							+ strCreditBand + "']"))
					.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			creditBand = driver.findElement(By.xpath(/* "//span[text()='Credit score based on:']//following::span[1]" */
					"//span[contains(text(),'Credit Band:')]//following::span[1]")).getText();
			if (verifyElementDisplayed(driver.findElement(By.xpath("//span[text()='" + strCreditBand + "']")))) {

				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.PASS,
						"Customer Provided credit band " + strCreditBand + " is updated successfully");
				driver.findElement(applyBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}
		searchTourPackage();
		int strPostPackageVer = Integer.parseInt(packageVersion);
		int diff = strPostPackageVer - strPrePackageVer;
		if (diff == 1) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.PASS,
					"Tour Package version is updated successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.FAIL,
					"Tour Package version is not updated successfully");
		}
	}

	public void retainedCreditBand() {
		String preCreditBand = creditBand;
		String postCreditBand = "";

		try {
			waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, creditTab, 240);
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			postCreditBand = driver
					.findElement(By.xpath(/* "//span[text()='Credit score based on:']//following::span[1]" */
							"//span[contains(text(),'Credit Band:')]//following::span[1]"))
					.getText();
			if (preCreditBand.equals(postCreditBand)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.PASS,
						"Credit band is displayed successfully after clicking on the Refresh button");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.FAIL,
						"Credit band is not displayed successfully after clicking on the Refresh button");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.FAIL,
					"Credit band is not displayed successfully after clicking on the Refresh button" + e.getMessage());
		}
	}

	public void addNewPackagefromExisting() {

		String strTodaysPrice = "";
		double inttodaysPriceUI;
		double intPPPackageCard;

		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, pointsTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(pointsTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(pointsCheckBox).click();
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
					"Tour Point package is selected successfully");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(pricingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(firstDropCheckBox).click();
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
					"First Drop Checkbox is checked successfully");
			strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
			inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(newBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {

				intPPPackageCard = Double
						.parseDouble(driver.findElement(By.xpath("(//div[@class='card-body']/h5/strong)[" + j + "]"))
								.getText().substring(1).replace(",", ""));
				if (intPPPackageCard == inttodaysPriceUI) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.PASS,
							"Point package is added successfully");
					break;
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "applyPriceDiscount", Status.FAIL,
					"Point package is not added successfully");
		}
	}

	public void CWAPitchTile_Exit() {

		waitUntilElementVisibleBy(driver, sideMenuLink, "ExplicitWaitLongestTime");
		// driver.findElement(sideMenuLink).click();
		clickElementJSWithWait(sideMenuLink);
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, exitBtn, "ExplicitWaitLongestTime");
		driver.findElement(exitBtn).click();
		// waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, exitSureMessage, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(exitSureMessage))) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "CWAPitchTile_Exit", Status.PASS,
					"Exit Sure message is displayed successfully");
			driver.findElement(exitSureMessage).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "CWAPitchTile_Exit", Status.PASS,
					"User is able to exit from CWAPitch Tile page successfully");

		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "CWAPitchTile_Exit", Status.FAIL,
					"User is not able to exit from CWAPitch Tile page successfully");
		}
	}

	protected By backLink = By.xpath("//a[contains(@class,'ews-back-link')]/span[contains(text(),'Back')]");

	public void backToPointPackageScreen() {
		try {
			waitUntilElementVisibleBy(driver, driver.findElement(backLink), "ExplicitWaitLongestTime");
			clickElementBy(backLink);
		} catch (Exception e) {
			System.out.println("Not Clicked");
		}
	}

	public void validateMessageExit() {

		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, sideMenuLink, "ExplicitWaitLongestTime");
		driver.findElement(sideMenuLink).click();
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, exitBtn, "ExplicitWaitLongestTime");
		driver.findElement(exitBtn).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "CWAPitchTile_Exit", Status.PASS,
				"User is able to exit from CWAPitch Tile page successfully");
	}

	public void CWAPitchTile_Exit_BeforeSelectingPackage() {
		try {
			driver.findElement(sideMenuLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(exitBtn).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "CWAPitchTile_Exit", Status.PASS,
					"User is able to exit from CWAPitch Tile page successfully");
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void paymentPriceValidation() {
		String strPPPackageCard;
		String strPurchasePrice[] = testData.get("PurchasePrice").split(";");
		String strTDPPackageCard;
		String strDPPrice[] = testData.get("DownPayment").split(";");

		try {
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * waitUntilElementVisibleBy(driver, gotoWorksheetButton, "ExplicitWaitLongestTime");
			 * driver.findElement(gotoWorksheetButton).click();
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {

				strPPPackageCard = driver.findElement(By.xpath("(//div[@class='card-body']/h5/strong)[" + j + "]"))
						.getText().substring(1).replace(",", "");
				strTDPPackageCard = driver.findElement(By.xpath(
						"(//div[@class='card-body']/p[contains(text(),'Down Payment Today')]/strong)[" + j + "]"))
						.getText().substring(1).replace(",", "");
				if ((strPPPackageCard.equals(strPurchasePrice[j - 1]))
						&& (strTDPPackageCard.equals(strDPPrice[j - 1]))) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasePriceValidation", Status.PASS,
							"Purchase Price & Todays Down payment Price is displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasePriceValidation", Status.FAIL,
							"Purchase Price & Todays Down payment Price is not displayed successfully");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void comparePackageCollateralValidate() {
		String strPoint[] = testData.get("TourPoints").split(";");
		try {
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint[j - 1].equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(
							By.xpath("(//input[@name='custom-checkbox' and @type='checkbox']/../label)[" + j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
							"CWA Package is selected successfully");
				}
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(comparePckgBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(benefitTab))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(benefitTab).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> allBenefitList = driver.findElements(rowCount);
				// List<String> benefitsTerm = new ArrayList<String>();
				for (int j = 1; j <= allBenefitList.size(); j++) {
					String strValue = driver.findElement(By.xpath(
							"(//tab[@id='club-wyndham']/table/tbody/tr/td/div/span[@class='custom-text'])[" + j + "]"))
							.getText();
					// benefitsTerm.add(strValue);
					if (driver
							.findElement(
									By.xpath("(//tab[@id='club-wyndham']/table/tbody/tr/td/div/span[2])[" + j + "]"))
							.getText().equals("Viewed")) {
						driver.findElement(
								By.xpath("(//tab[@id='club-wyndham']/table/tbody/tr/td/div/span[@class='custom-text'])["
										+ j + "]"))
								.click();
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
								"Sales collateral view is getting displayed for " + strValue);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						iconClose.click();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.FAIL,
								"Sales collateral view is not displayed");
					}

				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	public void comparePackage() {
		String strPoint[] = testData.get("TourPoints").split(";");
		String strgetValue = "";

		try {
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint[j - 1].equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(
							By.xpath("(//input[@name='custom-checkbox' and @type='checkbox']/../label)[" + j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
							"CWA Package is selected successfully");
				}
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(comparePckgBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> allFinanceList = driver.findElements(financeRowCount);
			List<String> financeTerm = new ArrayList<String>();
			for (int j = 1; j <= allFinanceList.size(); j++) {
				String strValue = driver.findElement(By.xpath("(//tab[@id='financial']/table/tbody/tr/td)[" + j + "]"))
						.getText();
				financeTerm.add(strValue);
			}

			if (verifyElementDisplayed(driver.findElement(financeTab))) {
				List<WebElement> allList = driver.findElements(financeRowCount);
				for (int i = 1; i <= allList.size(); i++) {
					strgetValue = driver.findElement(By
							.xpath("(((//tab[@id='financial']/table/tbody/tr)[1]/../../../../../../../../..//div[@ng-reflect-klass='card pitch-card pricing-card'])[1]//following::table/tbody/tr)["
									+ i + "]/td[1]"))
							.getText();
					if (strgetValue.isEmpty() == false) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
								financeTerm.get(i - 1) + " " + "of Package 1 is displayed" + strgetValue
										+ " successfully");
					}
				}
				for (int k = 1; k <= allList.size(); k++) {
					strgetValue = driver.findElement(By
							.xpath("(((//tab[@id='financial']/table/tbody/tr)[1]/../../../../../../../../..//div[@ng-reflect-klass='card pitch-card pricing-card'])[2]//following::table/tbody/tr)["
									+ k + "]/td[1]"))
							.getText();
					if (strgetValue.isEmpty() == false) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
								financeTerm.get(k - 1) + " " + "of Package 2 is displayed" + strgetValue
										+ " successfully");
					}
				}

			}
			if (verifyElementDisplayed(driver.findElement(benefitTab))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(benefitTab).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> allBenefitList = driver.findElements(rowCount);
				List<String> benefitsTerm = new ArrayList<String>();
				for (int j = 1; j <= allBenefitList.size(); j++) {
					String strValue = driver
							.findElement(By.xpath("(//tab[@id='club-wyndham']/table/tbody/tr/td)[" + j + "]"))
							.getText();
					benefitsTerm.add(strValue);
				}

				List<WebElement> allList1 = driver.findElements(rowCount);
				for (int i = 2; i <= allList1.size(); i++) {
					WebElement element = driver.findElement(By
							.xpath("(((//tab[@id='club-wyndham']/table/tbody/tr)[2]//following::div[@ng-reflect-klass='card pitch-card pricing-card'])[1]//following-sibling::table)[2]/tbody/tr["
									+ i + "]/td/img[contains(@src,'icon-checked')]"));
					if (element.isDisplayed() == true) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
								benefitsTerm.get(i - 2) + " " + "of Package 1 is displayed successfully");
					}
				}
				for (int k = 2; k <= allList1.size(); k++) {
					WebElement element1 = driver.findElement(By
							.xpath("(((//tab[@id='club-wyndham']/table/tbody/tr)[2]//following::div[@ng-reflect-klass='card pitch-card pricing-card'])[2]//following-sibling::table)[2]/tbody/tr["
									+ k + "]/td/img[contains(@src,'icon-checked')]"));
					if (element1.isDisplayed() == true) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
								benefitsTerm.get(k - 2) + " " + "of Package 2 is displayed successfully");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void financePitchTileValidation() {
		String strPoint[] = testData.get("TourPoints").split(";");
		String strgetValue = "";

		try {
			List<WebElement> cwaPitchPoints = driver.findElements(cwaPackagePoints);
			for (int j = 1; j <= cwaPitchPoints.size(); j++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String cwaPackagePoint = driver.findElement(By
						.xpath("(//swiper[@ng-reflect-klass='swiper-container']//following::div[@class='card-img-overlay']/strong)["
								+ j + "]"))
						.getText();
				if (strPoint[j - 1].equals(cwaPackagePoint)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(
							By.xpath("(//input[@name='custom-checkbox' and @type='checkbox']/../label)[" + j + "]"))
							.click();
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
							"CWA Package is selected successfully");
				}
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(comparePckgBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> allFinanceList = driver.findElements(financeRowCount);
			List<String> financeTerm = new ArrayList<String>();
			for (int j = 1; j <= allFinanceList.size(); j++) {
				String strValue = driver.findElement(By.xpath("(//tab[@id='financial']/table/tbody/tr/td)[" + j + "]"))
						.getText();
				financeTerm.add(strValue);
			}

			if (verifyElementDisplayed(driver.findElement(financeTab))) {
				List<WebElement> allList = driver.findElements(financeRowCount);
				for (int i = 1; i <= allList.size(); i++) {
					strgetValue = driver.findElement(By
							.xpath("(((//tab[@id='financial']/table/tbody/tr)[1]/../../../../../../../../..//div[@ng-reflect-klass='card pitch-card pricing-card'])[1]//following::table/tbody/tr)["
									+ i + "]/td[1]"))
							.getText();
					if (strgetValue.isEmpty() == false) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
								financeTerm.get(i - 1) + " " + "of Package 1 is displayed" + strgetValue
										+ " successfully");
					}
				}
				for (int k = 1; k <= allList.size(); k++) {
					strgetValue = driver.findElement(By
							.xpath("(((//tab[@id='financial']/table/tbody/tr)[1]/../../../../../../../../..//div[@ng-reflect-klass='card pitch-card pricing-card'])[2]//following::table/tbody/tr)["
									+ k + "]/td[1]"))
							.getText();
					if (strgetValue.isEmpty() == false) {
						tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "comparePackage", Status.PASS,
								financeTerm.get(k - 1) + " " + "of Package 2 is displayed" + strgetValue
										+ " successfully");
					}
				}

			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void downPaymentAdjustment() {
		String strDpPercentage[] = testData.get("DownPaymentPercentage").split("%");
		double inttodaysPriceforDp;
		double intProcessingFee = 349.00;
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(downPaymentHeader))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"User is able to see the DownPayment Header option for the selected Point Package");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"User is not able to see the DownPayment Header option for the selected Point Package");
			}

			/*
			 * String strMinDp[] =
			 * driver.findElement(minDownPaymentTxt).getText().split(" ");
			 * String flag = String.format("$###,###.###",strMinDp[1]); if
			 * (flag.matches("$###,###.###")) {
			 * 
			 * tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage",
			 * "downPaymentAdjustment", Status.PASS,
			 * "User is able to see the Minimum DownPayment option for the selected Point Package in Proper format"
			 * ); } else {
			 * tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage",
			 * "downPaymentAdjustment", Status.FAIL,
			 * "User is not able to see the Minimum DownPayment option for the selected Point Package in Proper format"
			 * ); }
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(dpSelectChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String PurchasePrice[] = driver.findElement(todaysPriceforDp).getText().split(" ");
			inttodaysPriceforDp = Double.parseDouble(PurchasePrice[2].replace(",", "").substring(1));
			double calculatedDownPayment = ((inttodaysPriceforDp + intProcessingFee)
					* Integer.parseInt(strDpPercentage[0]) / 100) + 25.00;
			double DPfromUI = Double
					.parseDouble(driver.findElement(downPayment).getText().replace(",", "").substring(1));
			if (calculatedDownPayment == DPfromUI) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"Minimum and maximum allowed section is displayed calculated value equals % of required down payment");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"Minimum and maximum allowed section is not displayed calculated value equals % of required down payment");
			}
			String strDownPay[] = driver.findElement(downPaymentPercent).getText().split(" ");
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * waitForSometime(tcConfig.getConfig().get("MedWait")); String
			 * strDpPay[] =
			 * driver.findElement(downPayToday).getText().trim().split(" ");
			 * String strDpPayment[] = strDpPay[3].split("\n");
			 * if(strDownPay[0].substring(1).trim().equals(strDpPayment[0].trim(
			 * ))){ tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage",
			 * "downPaymentAdjustment", Status.PASS,
			 * "Downpayment is displayed successfully"); }else{
			 * tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage",
			 * "downPaymentAdjustment", Status.FAIL,
			 * "Downpayment is not displayed successfully"); }
			 */
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
					"Minimum and maximum allowed section is not displayed calculated value equals % of required down payment");
		}
	}

	public void newDownPaymentAdjustment() {
		String strCustDP = testData.get("CustomDownPayment");

		try {
			
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);			 
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(customDownPayment).sendKeys(strCustDP);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(downPayChkBox).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			double DPfromUI = Double
					.parseDouble(driver.findElement(downPayment).getText().replace(",", "").substring(1));
			if (DPfromUI == Double.parseDouble(testData.get("CustomDownPayment"))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"Custom Down Payment is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"Custom Down Payment is not displayed successfully");
			}
			String PurchasePrice[] = driver.findElement(todaysPriceforDp).getText().split(" ");
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			double inttodaysPriceforDp = Double.parseDouble(PurchasePrice[2].replace(",", "").substring(1));
			Double downPaymentCalculated = (DPfromUI / inttodaysPriceforDp) * 100;
			int downPaymentPercentage = downPaymentCalculated.intValue();
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strDownPay[] = driver.findElement(downPaymentPercent).getText().split(" ");
			int DownPayUI = Integer.parseInt(strDownPay[0].substring(1, 3));
			if (DownPayUI == downPaymentPercentage) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"Downpayment percentage is displayed based on the custom down payment which is entered manually");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"Downpayment percentage is not displayed based on the custom down payment which is entered manually");
			}
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			Double dtodayPrice = Double
					.parseDouble(driver.findElement(todaysPrice).getText().replace(",", "").substring(1));
			Double dDownPayment = Double
					.parseDouble(driver.findElement(downPayment).getText().replace(",", "").substring(1));
			Double calculatedFinanceAmt = dtodayPrice - dDownPayment;
			Double dFinancedAmt = Double
					.parseDouble(driver.findElement(financedAmttxt).getText().replace(",", "").substring(1));
			if (dFinancedAmt.intValue() == calculatedFinanceAmt.intValue()
			/*
			 * &&
			 * driver.findElement(financedAmttxt).getText().substring(1).matches
			 * ("##,###.##")
			 */) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"User is able to view the financing amount in proper format");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"User is not able to view the financing amount in proper format");
			}
			if (verifyElementDisplayed(driver.findElement(minLoanTermTxt))) {
				if (driver.findElement(minLoanTermTxt).getText().contains("mo")) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
							"Max/Min Loan term is displayed in Months");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
							"Max/Min Loan term is displayed in Months");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
					"Max/Min Loan term is not displayed in Months");
		}
	}

	public void adjustLoanTerm() {
		String strYearMonTxt[] = testData.get("YearMonth").split(";");
		String strCustomTerm = testData.get("CustomTerm");
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String maxminTerm = driver.findElement(minLoanTermTxt).getText();
			if (maxminTerm.contains("Min/max terms:")) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
						"User is able to see pre-defined 'Loan term' options to choose from that fall within the ‘Term Month limits (min and max)’ for the Points Package being worked on.");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
						"User is not able to see pre-defined 'Loan term' options to choose from that fall within the ‘Term Month limits (min and max)’ for the Points Package being worked on.");
			}
			List<WebElement> list = driver.findElements(yearsMonthsLabel);
			for (int i = 1; i <= list.size(); i++) {
				String strYearMonth = driver
						.findElement(By.xpath(
								"(//label[text()='Years / Months']/..//following-sibling::div[1]/strong)[" + i + "]"))
						.getText();
				if (strYearMonth.equals(strYearMonTxt[i - 1])) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
							"Years/Months option " + strYearMonth + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
							"Years/Months option " + strYearMonth + "is not displayed successfully");
				}
			}
			DecimalFormat df = new DecimalFormat("#.##");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strMonthlyLoanPay = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPmt = Double.parseDouble(strMonthlyLoanPay);
			String doubleFormat = df.format(monthlyLoanPmt);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(customTermLabel).sendKeys("181");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(customTermChkBox).click();
			waitUntilElementVisibleBy(driver, customTermErrorMsg, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(customTermErrorMsg))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
						"Error message is displayed successfully for falling outside the Maximum and the Minimum Loan Terms");
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
						"Error message is not displayed successfully for falling outside the Maximum and the Minimum Loan Terms");
			}
			driver.findElement(customTermLabel).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(okBtn);
			driver.findElement(customTermLabel).sendKeys(strCustomTerm);
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
					"Custom term is entered successfullys");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(customTermChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String MonthlyLoanPmt = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPayUpdated = Double.parseDouble(MonthlyLoanPmt);
			String doubleFormatUI = df.format(monthlyLoanPayUpdated);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (!doubleFormat.equals(doubleFormatUI)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
						"Mothly payment is Updated successfully after entering the custom loan term");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
						"Mothly payment is not Updated successfully after entering the custom loan term");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
					"Mothly payment is not Updated successfully after entering the custom loan term" + e.getMessage());
		}
	}

	public void defaultTermYear() {
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "defaultTermYear", Status.PASS,
					"The default of a 5 Year term is displayed successfully");
			/*
			 * String strYearMonth =
			 * driver.findElement(fiveYrDefaultLabel).getAttribute(
			 * "ng-reflect-model"); if (strYearMonth.equals("true")) {
			 * tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage",
			 * "defaultTermYear", Status.PASS,
			 * "The default of a 5 Year term is displayed successfully"); } else
			 * { tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage",
			 * "defaultTermYear", Status.FAIL,
			 * "The default of a 5 Year term is not displayed successfully"); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void financialDetails_AutoPayment() {
		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strLabel = driver.findElement(usBankActLabel).getText();
			if (verifyElementDisplayed(driver.findElement(usBankActLabel))) {

				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.PASS, "Secondary Label " + strLabel + " " + "is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.FAIL, "Secondary Label " + strLabel + " " + "is not displayed successfully");
			}

			String strInterestRate[] = driver.findElement(interestRate).getText().split("%");
			double InterestRt = Double.parseDouble(strInterestRate[0]);
			String strInterestRtReduction[] = driver.findElement(interestRateReduction).getText().split("%");
			double interestRtReduction = Double.parseDouble(strInterestRtReduction[0]);
			String strFinalIntRate[] = driver.findElement(finalInterestRt).getText().split("%");
			double finalIntRt = Double.parseDouble(strFinalIntRate[0]);
			String strMonthlyLoanPay = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPmt = Double.parseDouble(strMonthlyLoanPay);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(ACHChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String InterestRate[] = driver.findElement(interestRate).getText().split("%");
			double InterestRtUpdated = Double.parseDouble(InterestRate[0]);
			String InterestRtReduction[] = driver.findElement(interestRateReduction).getText().split("%");
			double interestRtReductionUpdated = Double.parseDouble(InterestRtReduction[0]);
			String FinalIntRate[] = driver.findElement(finalInterestRt).getText().split("%");
			double finalIntRtUpdated = Double.parseDouble(FinalIntRate[0]);
			String MonthlyLoanPmt = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPayUpdated = Double.parseDouble(MonthlyLoanPmt);

			if (/* !(InterestRt == InterestRtUpdated) && */!(interestRtReduction == interestRtReductionUpdated)
					&& !(finalIntRt == finalIntRtUpdated) && !(monthlyLoanPmt == monthlyLoanPayUpdated)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.PASS, "Discount amount permitted to be applied to the Interest Rate (0.5%).");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.FAIL, "Discount amount is not permitted to be applied to the Interest Rate (0.5%).");
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void interestRate_Disabled_Functionality() {
		try {
			waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(dpSelectChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String InterestRate[] = driver.findElement(interestRate).getText().split("%");
			if (InterestRate[0].equals("0.00")) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "interestRate_Disabled_Functionality",
						Status.PASS, "Interest Rate is displayed 0 while selecting 100 % downpayment");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "interestRate_Disabled_Functionality",
						Status.FAIL, "Interest Rate is not displayed 0 while selecting 100 % downpayment");
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void interestRateUsuryLimit() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(usuryLimitLabel))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "interestRateUsuryLimit", Status.PASS,
						"Interest Rate Usury Limit is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "interestRateUsuryLimit", Status.FAIL,
						"Interest Rate Usury Limit is not displayed successfully");
			}
			if (verifyElementDisplayed(driver.findElement(promotionalLabel))) {
				verifySelectOptions(promotionalDropdown, "None");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				verifySelectOptions(promotionalDropdown, "P1");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				verifySelectOptions(promotionalDropdown, "P2");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				verifySelectOptions(promotionalDropdown, "P3");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("CWAPitchBenefitDetailsPage", "interestRateUsuryLimit",
					Status.FAIL, "Interest Rate Usury Limit is not displayed successfully");
		}
	}

	public void selectInterestRate() {
		String strInterestRate = testData.get("InterestRate");
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(promotionalLabel))) {
				selectByText(promotionalDropdown, strInterestRate);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.PASS,
						"Interest Rate Usury Limit" + " " + strInterestRate + " " + "is selected successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.FAIL,
						"Interest Rate Usury Limit" + " " + strInterestRate + " " + "is not selected successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("Financing_DownPayment Page", "selectInterestRate",
					Status.FAIL, "Failed due to : " + e.getMessage());
		}
	}

	public void validateDisplayValue() {
		String strTodaysPrice = null;
		String strInterestRt = null;
		String strMonLoanPay = null;
		try {
			if (verifyElementDisplayed(driver.findElement(todaysPrice))) {
				strTodaysPrice = driver.findElement(todaysPrice).getText().trim();
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.PASS,
						"Todays Price" + " " + strTodaysPrice + " " + "is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.FAIL,
						"Todays Price" + " " + strTodaysPrice + " " + "is not displayed successfully");

			}
			if (verifyElementDisplayed(driver.findElement(interestRate))) {
				strInterestRt = driver.findElement(interestRate).getText().trim();
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.PASS,
						"Interest Rate" + " " + strInterestRt + " " + "is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.FAIL,
						"Interest Rate" + " " + strInterestRt + " " + "is not displayed successfully");
			}
			if (verifyElementDisplayed(driver.findElement(monthlyLoanPay))) {
				strMonLoanPay = driver.findElement(monthlyLoanPay).getText().trim();
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.PASS,
						"Estimated Monthly Loan Pay" + " " + strMonLoanPay + " " + "is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectInterestRate", Status.FAIL,
						"Estimated Monthly Loan Pay" + " " + strMonLoanPay + " " + "is not displayed successfully");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("Financing_DownPayment Page", "validateDisplayValue",
					Status.FAIL, "Failed due to : " + e.getMessage());
		}
	}

	public void validateInterestRatebasedOnCreditBand() {
		String strCreditBand = testData.get("CustomerCreditBand");
		try {
			waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strInterestRate[] = driver.findElement(interestRate).getText().split("%");
			double InterestRt = Double.parseDouble(strInterestRate[0]);
			String strFinalIntRate[] = driver.findElement(finalInterestRt).getText().split("%");
			double finalIntRt = Double.parseDouble(strFinalIntRate[0]);
			String strMonthlyLoanPay = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPmt = Double.parseDouble(strMonthlyLoanPay);
			tcConfig.updateTestReporter("Customer Sales View Page", "validateInterestRatebasedOnCreditBand",
					Status.PASS,
					"Financial Details is displayed successfully before customer provided credit band is selected");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(cusProRatingA1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingA))
					&& verifyElementDisplayed(driver.findElement(cusProRatingB1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingB))
					&& verifyElementDisplayed(driver.findElement(cusProRatingC1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingC))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(selectCustCheckBox).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(okBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(
						By.xpath("//h4[contains(text(),'Customer Provided')]//following::label[@for='customer-checkbox1"
								+ strCreditBand + "']"))
						.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				creditBand = driver.findElement(By.xpath(/* "//span[text()='Credit score based on:']//following::span[1]" */
						"//span[contains(text(),'Credit Band:')]//following::span[1]")).getText();
				if (verifyElementDisplayed(driver.findElement(By.xpath("//span[text()='" + strCreditBand + "']")))) {

					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.PASS,
							"Customer Provided credit band " + strCreditBand + " is updated successfully");
					//driver.findElement(applyBtn).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				
			}
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String InterestRate[] = driver.findElement(interestRate).getText().split("%");
			double InterestRtUpdated = Double.parseDouble(InterestRate[0]);
			String FinalIntRate[] = driver.findElement(finalInterestRt).getText().split("%");
			double finalIntRtUpdated = Double.parseDouble(FinalIntRate[0]);
			String MonthlyLoanPmt = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPayUpdated = Double.parseDouble(MonthlyLoanPmt);

			if (!(InterestRt == InterestRtUpdated) && !(finalIntRt == finalIntRtUpdated)
					&& !(monthlyLoanPmt == monthlyLoanPayUpdated)) {
				tcConfig.updateTestReporter("Customer Sales View Page", "validateInterestRatebasedOnCreditBand",
						Status.PASS,
						"Interest Rate is updated successfully after credit band is selected by the Customer");
			} else {
				tcConfig.updateTestReporter("Customer Sales View Page", "validateInterestRatebasedOnCreditBand",
						Status.FAIL,
						"Interest Rate is not updated successfully after credit band is selected by the Customer");
			}

			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("Customer Sales View Page",
					"validateInterestRatebasedOnCreditBand", Status.FAIL, "Failed due to : " + e.getMessage());
		}
	}

	public void financialDetailsforMindownPayment() {
		String strDpPercentage[] = testData.get("DownPaymentPercentage").split("%");
		double inttodaysPriceforDp;
		double intProcessingFee = 349.00;
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(downPaymentHeader))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"User is able to see the DownPayment Header option for the selected Point Package");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"User is not able to see the DownPayment Header option for the selected Point Package");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strAttr = driver.findElement(minDownPaymentCheck).getAttribute("ng-reflect-model");
			if (strAttr.equals("true")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(dpSelectChkBox).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String PurchasePrice[] = driver.findElement(todaysPriceforDp).getText().split(" ");
				inttodaysPriceforDp = Double.parseDouble(PurchasePrice[2].replace(",", "").substring(1));
				double calculatedDownPayment = ((inttodaysPriceforDp + intProcessingFee)
						* Integer.parseInt(strDpPercentage[0]) / 100) + 25.00;
				double DPfromUI = Double
						.parseDouble(driver.findElement(downPayment).getText().replace(",", "").substring(1));
				if (calculatedDownPayment == DPfromUI) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
							"Minimum allowed section is displayed calculated value equals % of required down payment");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
							"Minimum allowed section is not displayed calculated value equals % of required down payment");
				}
				String strDownPay[] = driver.findElement(downPaymentPercent).getText().split(" ");
				driver.findElement(applyBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strDpPay[] = driver.findElement(downPayToday).getText().trim().split(" ");
				String strDpPayment[] = strDpPay[3].split("\n");
				if (strDownPay[0].substring(1).trim().equals(strDpPayment[0].trim())) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
							"Downpayment is displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
							"Downpayment is not displayed successfully");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
					"Minimum and maximum allowed section is not displayed calculated value equals % of required down payment");
		}
	}

	public void selectCreditBand() {

		int postPitchTileCount;
		int prePitchTileCount = cwaPitchTileCount;
		int strPrePackageVer = Integer.parseInt(packageVersion);
		waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
		driver.findElement(OptionLink).click();
		waitUntilElementVisibleBy(driver, proceedButton, "ExplicitWaitLongestTime");
		driver.findElement(proceedButton).click();
		pageCheck();
		waitForSometime(tcConfig.getConfig().get("ExplicitLongWait"));
		/*
		 * waitUntilElementVisibleBy(driver, creditTab, 240);
		 * waitForSometime(tcConfig.getConfig().get("LongWait"));
		 * driver.findElement(financingTab).click();
		 * waitForSometime(tcConfig.getConfig().get("MedWait")); String
		 * strInterestRate[] =
		 * driver.findElement(interestRate).getText().split("%"); InterestRt =
		 * Double.parseDouble(strInterestRate[0]);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 */
		driver.findElement(creditTab).click();
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, refreshIcon, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(refreshIcon)) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
					"Refresh button is displayed successfully");
			creditBand = driver.findElement(By.xpath("//span[text()='Credit Band:']//following::span[1]")).getText();
			waitUntilElementVisibleBy(driver, selectCustCheckBox, "ExplicitWaitLongestTime");
			clickElementBy(selectCustCheckBox);
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
					"Credit band is updated based on the available credit band from eCredit");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		waitForSometime(tcConfig.getConfig().get("LongWait"));      
		driver.findElement(applyBtn).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, cwaPackage, "ExplicitWaitLongestTime");
		List<WebElement> cwaPitchTileList = driver.findElements(cwaPackage);
		cwaPitchTileCount = cwaPitchTileList.size();
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		postPitchTileCount = cwaPitchTileCount;
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (prePitchTileCount == postPitchTileCount) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
					"Pitch tile count " + postPitchTileCount + " is displayed successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.FAIL,
					"Pitch tile count " + postPitchTileCount + " is not displayed successfully");
		}
		searchTourPackage();
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		int strPostPackageVer = Integer.parseInt(packageVersion);
		int diff = strPostPackageVer - strPrePackageVer;
		if (diff == 1) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.PASS,
					"Tour Package version is updated successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "packageVersionUpgrade", Status.FAIL,
					"Tour Package version is not updated successfully");
		}

	}

	public void selectCreditBandforNewPackage() {
		try {
			int postPitchTileCount;
			int prePitchTileCount = cwaPitchTileCount;
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, creditTab, 240);
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(refreshIcon))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
						"Refresh button is displayed successfully");
				creditBand = driver.findElement(By.xpath("//span[text()='Credit Band:']//following::span[1]"))
						.getText();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(selectCustCheckBox);
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
						"Credit band is updated based on the available credit band from eCredit");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			waitUntilElementVisibleBy(driver, financingTab, 240);
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strInterestRate[] = driver.findElement(interestRate).getText().split("%");
			InterestRt = Double.parseDouble(strInterestRate[0]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(newBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> cwaPitchTileList = driver.findElements(cwaPackage);
			cwaPitchTileCount = cwaPitchTileList.size();
			postPitchTileCount = cwaPitchTileCount;
			int diffPitchCount = postPitchTileCount - prePitchTileCount;
			if (diffPitchCount == 1) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.PASS,
						"Pitch tile count" + diffPitchCount + " is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.FAIL,
						"Pitch tile count" + diffPitchCount + " is not displayed successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectCreditBand", Status.FAIL,
					"Credit band is updated based on the available credit band from eCredit");
		}
	}

	public void validateIntRateforNewPackage() {
		double preInterestRate = InterestRt;
		double postInterestRate;
		try {
			searchTourPackage();
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strInterestRate[] = driver.findElement(interestRate).getText().split("%");
			InterestRt = Double.parseDouble(strInterestRate[0]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			postInterestRate = InterestRt;
			if (!(preInterestRate == postInterestRate)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateIntRateforNewPackage",
						Status.PASS,
						"Interest rate" + postInterestRate + " is displayed properly for newly added package");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateIntRateforNewPackage",
						Status.FAIL,
						"Interest rate" + postInterestRate + " is not displayed properly for newly added package");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "validateIntRateforNewPackage", Status.FAIL,
					"Interest rate is not displayed properly for newly added package" + e.getMessage());
		}
	}

	public void refreshFunctionality() {
		String preCreditBand = creditBand;
		String postCreditBand = "";
		double preInterestRt = InterestRt;
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, creditTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(refreshIcon);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			postCreditBand = driver.findElement(By.xpath("//span[text()='Credit Band:']//following::span[1]"))
					.getText();
			if (!preCreditBand.equals(postCreditBand)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.PASS,
						"Credit band is displayed successfully after clicking on the Refresh button");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.FAIL,
						"Credit band is not displayed successfully after clicking on the Refresh button");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strInterestRate[] = driver.findElement(interestRate).getText().split("%");
			InterestRt = Double.parseDouble(strInterestRate[0]);
			if (!(preInterestRt == InterestRt)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.PASS,
						"Interest Rate is displayed successfully after clicking on the Refresh button");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.FAIL,
						"Interest Rate is not displayed successfully after clicking on the Refresh button");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "refreshFunctionality", Status.FAIL,
					"Credit band is not displayed successfully after clicking on the Refresh button" + e.getMessage());
		}
	}

	public void viewLoanDetails() {
		String strDpPercentage[] = testData.get("DownPaymentPercentage").split("%");
		double inttodaysPriceforDp;
		double intProcessingFee = 349.00;
		String strYearMonTxt[] = testData.get("YearMonth").split(";");
		String strCustomTerm = testData.get("CustomTerm");
		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, financingTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(downPaymentHeader))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"User is able to see the DownPayment Header option for the selected Point Package");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"User is not able to see the DownPayment Header option for the selected Point Package");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(dpSelectChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String PurchasePrice[] = driver.findElement(todaysPriceforDp).getText().split(" ");
			inttodaysPriceforDp = Double.parseDouble(PurchasePrice[2].replace(",", "").substring(1));
			double calculatedDownPayment = ((inttodaysPriceforDp + intProcessingFee)
					* Integer.parseInt(strDpPercentage[0]) / 100) + 25.00;
			double DPfromUI = Double
					.parseDouble(driver.findElement(downPayment).getText().replace(",", "").substring(1));
			if (calculatedDownPayment == DPfromUI) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"Minimum and maximum allowed section is displayed calculated value equals % of required down payment");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"Minimum and maximum allowed section is not displayed calculated value equals % of required down payment");
			}
			double dtodayPrice = Double
					.parseDouble(driver.findElement(todaysPrice).getText().replace(",", "").substring(1));
			double dDownPayment = Double
					.parseDouble(driver.findElement(downPayment).getText().replace(",", "").substring(1));
			double calculatedFinanceAmt = dtodayPrice - dDownPayment;
			double dFinancedAmt = Double
					.parseDouble(driver.findElement(financedAmttxt).getText().replace(",", "").substring(1));
			if (dFinancedAmt == calculatedFinanceAmt) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"User is able to view the financing amount in proper format");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"User is not able to view the financing amount in proper format");
			}

			if (driver.findElement(firstDueDate).getText() != null) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"First Payment Due Dtae is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"First Payment Due Date is not displayed successfully");
			}

			// Loan Term

			String maxminTerm = driver.findElement(minLoanTermTxt).getText();
			if (maxminTerm.contains("Min/max terms: 1-120 mo")) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
						"User is able to see pre-defined 'Loan term' options to choose from that fall within the ‘Term Month limits (min and max)’ for the Points Package being worked on.");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
						"User is not able to see pre-defined 'Loan term' options to choose from that fall within the ‘Term Month limits (min and max)’ for the Points Package being worked on.");
			}
			List<WebElement> list = driver.findElements(yearsMonthsLabel);
			for (int i = 1; i <= list.size(); i++) {
				String strYearMonth = driver
						.findElement(By.xpath(
								"(//label[text()='Years / Months']/..//following-sibling::div[1]/strong)[" + i + "]"))
						.getText();
				if (strYearMonth.equals(strYearMonTxt[i - 1])) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
							"Years/Months option " + strYearMonth + " is displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
							"Years/Months option " + strYearMonth + "is not displayed successfully");
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(customTermLabel).sendKeys("181");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(customTermChkBox).click();
			waitUntilElementVisibleBy(driver, customTermErrorMsg, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(customTermErrorMsg))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
						"Error message is displayed successfully for falling outside the Maximum and the Minimum Loan Terms");
				clickElementBy(okBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.FAIL,
						"Error message is not displayed successfully for falling outside the Maximum and the Minimum Loan Terms");
			}
			driver.findElement(customTermLabel).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(okBtn);
			driver.findElement(customTermLabel).sendKeys(strCustomTerm);
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "adjustLoanTerm", Status.PASS,
					"Custom term is entered successfullys");
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			String strInterestRate[] = driver.findElement(interestRate).getText().split("%");
			double InterestRt = Double.parseDouble(strInterestRate[0]);
			String strFinalIntRate[] = driver.findElement(finalInterestRt).getText().split("%");
			double finalIntRt = Double.parseDouble(strFinalIntRate[0]);
			String strMonthlyLoanPaymentUI = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPaymentUI = Double.parseDouble(strMonthlyLoanPaymentUI);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(customTermChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String InterestRate[] = driver.findElement(interestRate).getText().split("%");
			double InterestRtUpdated = Double.parseDouble(InterestRate[0]);
			String FinalIntRate[] = driver.findElement(finalInterestRt).getText().split("%");
			double finalIntRtUpdated = Double.parseDouble(FinalIntRate[0]);
			String InterestRtReductionUI[] = driver.findElement(interestRateReduction).getText().split("%");
			double interestRtReductionUIUpdated = Double.parseDouble(InterestRtReductionUI[0]);
			String strMonthlyLoanPayment = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPayment = Double.parseDouble(strMonthlyLoanPayment);

			if ((InterestRt == InterestRtUpdated) && (finalIntRt == finalIntRtUpdated)
					&& !(monthlyLoanPaymentUI == monthlyLoanPayment)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.PASS,
						"Interest Rate, Interarest Rate Reduction, Final Interest rate and Monthly loan payment is updated successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.FAIL,
						"Interest Rate, Interarest Rate Reduction, Final Interest rate and Monthly loan payment is not updated successfully");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// ACH
			String strLabel = driver.findElement(usBankActLabel).getText();
			if (verifyElementDisplayed(driver.findElement(usBankActLabel))) {

				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.PASS, "Secondary Label " + strLabel + " " + "is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.FAIL, "Secondary Label " + strLabel + " " + "is not displayed successfully");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(ACHChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String InterestRateUI[] = driver.findElement(interestRate).getText().split("%");
			double InterestRtUIUpdated = Double.parseDouble(InterestRateUI[0]);
			String InterestRtReduction[] = driver.findElement(interestRateReduction).getText().split("%");
			double interestRtReductionUpdated = Double.parseDouble(InterestRtReduction[0]);
			String FinalIntRateUI[] = driver.findElement(finalInterestRt).getText().split("%");
			double finalIntRtUIUpdated = Double.parseDouble(FinalIntRateUI[0]);
			String MonthlyLoanPmt = driver.findElement(monthlyLoanPay).getText().replace(",", "").substring(1);
			double monthlyLoanPayUpdated = Double.parseDouble(MonthlyLoanPmt);

			if ((InterestRtUpdated == InterestRtUIUpdated)
					&& !(interestRtReductionUIUpdated == interestRtReductionUpdated)
					&& !(finalIntRtUIUpdated == finalIntRtUpdated) && !(monthlyLoanPayment == monthlyLoanPayUpdated)) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.PASS,
						"Interest Rate, Interarest Rate Reduction, Final Interest rate and Monthly loan payment is updated successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "financialDetails_AutoPayment",
						Status.FAIL,
						"Interest Rate, Interarest Rate Reduction, Final Interest rate and Monthly loan payment is not updated successfully");
			}

			String strDownPay[] = driver.findElement(downPaymentPercent).getText().split(" ");
			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strDpPay[] = driver.findElement(downPayToday).getText().trim().split(" ");
			String strDpPayment[] = strDpPay[3].split("\n");
			if (strDownPay[0].substring(1).trim().equals(strDpPayment[0].trim())) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.PASS,
						"Downpayment is displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
						"Downpayment is not displayed successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "downPaymentAdjustment", Status.FAIL,
					"Minimum and maximum allowed section is not displayed calculated value equals % of required down payment");
		}
	}

	protected By DownPay50Percent = By.xpath("//input[@id='downPay50' and @ng-reflect-model='true']");
	protected By loanTerm60 = By.xpath("//input[@id='loanTerms60' and @ng-reflect-model='true']");

	public void purchasingPointPackagewithNoCreditScore() {
		String strCreditBand = testData.get("CustomerCreditBand");
		String strPurchasePrice = "";
		double intpurchasePrice;
		double inttodaysPrice;
		String strTodaysPrice = "";
		double inttodaysPriceUI = 0;
		double intMaxDiscount;

		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, creditTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, selectDiscountAmtLabel, 240);
			String strMaxDiscountLabel[] = driver.findElement(maxDiscountAllowed).getText().split(":");
			driver.findElement(secondDropCheckBox).click();
			String strDropPrice = driver.findElement(DropPrice).getText().substring(1).replace(",", "");
			double DropPrice = Double.parseDouble(strDropPrice);
			intMaxDiscount = Double.parseDouble(strMaxDiscountLabel[1].substring(2).replace(",", ""));
			double intsecondDrop100 = intMaxDiscount;
			if (intsecondDrop100 == DropPrice) {
				strPurchasePrice = driver.findElement(purchasePrice).getText().substring(1).replace(",", "");
				intpurchasePrice = Double.parseDouble(strPurchasePrice);
				inttodaysPrice = intpurchasePrice - DropPrice;
				strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
				inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
				if (inttodaysPriceUI == inttodaysPrice) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
							Status.PASS, "Todays price is calculated correctly and displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
							Status.FAIL, "Todays price is not calculated correctly and displayed successfully");
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(cusProRatingA1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingA))
					&& verifyElementDisplayed(driver.findElement(cusProRatingB1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingB))
					&& verifyElementDisplayed(driver.findElement(cusProRatingC1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingC))
					&& verifyElementDisplayed(driver.findElement(cusNOBand))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(selectCustCheckBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(okBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By
						.xpath("//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']//following::label[@for='customer-checkbox-"
								+ strCreditBand + "']"))
						.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyElementDisplayed(driver.findElement(By.xpath("//span[contains(text(),'NO')]")))) {

					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
							Status.PASS,
							"Customer Provided credit band " + strCreditBand + " is updated successfully");
					/* driver.findElement(applyBtn).click(); */
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			}
			/*driver.findElement(financingTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String strInterestRate[] = driver.findElement(interestRate).getText().split("%");
			double InterestRt = Double.parseDouble(strInterestRate[0]);
			if (verifyElementDisplayed(driver.findElement(DownPay50Percent))
					&& verifyElementDisplayed(driver.findElement(loanTerm60)) && InterestRt == 17.99) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
						Status.PASS, "Financial Details are displayed successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
						Status.FAIL, "Financial Details are not displayed");
			}*/

			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
					Status.FAIL, "Failed in Method" + e.getMessage());
		}
	}
	
	protected By noBandcheck = By.xpath("//label[@for='customer-checkbox-no-score']");
	
	public void noCreditScore() {
		String strCreditBand = testData.get("CustomerCreditBand");
		String strPurchasePrice = "";
		double intpurchasePrice;
		double inttodaysPrice;
		String strTodaysPrice = "";
		double inttodaysPriceUI = 0;
		double intMaxDiscount;

		try {
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, creditTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, selectDiscountAmtLabel, 240);
			String strMaxDiscountLabel[] = driver.findElement(maxDiscountAllowed).getText().split(":");
			driver.findElement(secondDropCheckBox).click();
			String strDropPrice = driver.findElement(DropPrice).getText().substring(1).replace(",", "");
			double DropPrice = Double.parseDouble(strDropPrice);
			intMaxDiscount = Double.parseDouble(strMaxDiscountLabel[1].substring(2).replace(",", ""));
			double intsecondDrop100 = intMaxDiscount;
			if (intsecondDrop100 == DropPrice) {
				strPurchasePrice = driver.findElement(purchasePrice).getText().substring(1).replace(",", "");
				intpurchasePrice = Double.parseDouble(strPurchasePrice);
				inttodaysPrice = intpurchasePrice - DropPrice;
				strTodaysPrice = driver.findElement(todaysPriceUI).getText().substring(1).replace(",", "");
				inttodaysPriceUI = Double.parseDouble(strTodaysPrice);
				if (inttodaysPriceUI == inttodaysPrice) {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
							Status.PASS, "Todays price is calculated correctly and displayed successfully");
				} else {
					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
							Status.FAIL, "Todays price is not calculated correctly and displayed successfully");
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(creditTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(cusProRatingA1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingA))
					&& verifyElementDisplayed(driver.findElement(cusProRatingB1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingB))
					&& verifyElementDisplayed(driver.findElement(cusProRatingC1))
					&& verifyElementDisplayed(driver.findElement(cusProRatingC))
					&& verifyElementDisplayed(driver.findElement(cusNOBand))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(selectCustCheckBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(okBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*driver.findElement(By
						.xpath("//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']//following::label[@for='customer-checkbox-"
								+ strCreditBand + "']"))
						.click();*/
				getElementInView(noBandcheck);
				driver.findElement(noBandcheck).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyElementDisplayed(driver.findElement(By.xpath("//h4[contains(text(),'Customer Provided')]//following::div[@ng-reflect-klass='package-points-card']/strong[text()='No Band']")))) {

					tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
							Status.PASS,
							"Customer Provided credit band " + strCreditBand + " is updated successfully");
					/* driver.findElement(applyBtn).click(); */
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			}
		

			driver.findElement(applyBtn).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			searchTourPackage();

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "purchasingPointPackagewithNoCreditScore",
					Status.FAIL, "Failed in Method" + e.getMessage());
		}
	}
	
	
	
	
	

	protected By picExpress = By.xpath("//label[@for='benefits-checkbox5']");
	protected By addAnother = By.xpath("//button[contains(.,'Add Another')]");
	protected By lablePICExpress = By.xpath("//h3[contains(.,'PIC Express Enrollment - Resort Search')]");
	protected By filterResort = By.xpath("//input[@id='mat-input-0']");
	protected By lableResortID = By.xpath("//table[contains(@class,'resort-search')]//tbody/tr/td");
	protected By checkboxResort = By.xpath("//label[contains(@for,'resort-select')]");
	protected By buttonTotalPointUsage = By.xpath("//button[contains(.,' Points Usage Summary ')]");
	protected By buttonPresentation = By.xpath("//button[contains(.,'Go to Presentation Tile')]");
	protected By inventoryDetail = By.xpath("//button[contains(.,' Inventory Details ')]");
	protected By checkboxStandard = By.xpath("//label[@for='first-standard']");
	protected By checkboxAnnual = By.xpath("//label[@for='first-annual']");
	protected By checkboxStudio = By.xpath("//label[@for='first-studio']");
	protected By picPlusPointsAwd = By.xpath("(//td/span[text()='PIC Plus Points Awarded'])[1]/../../td[2]");

	public void selectPICExpressResort() {
		waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
		driver.findElement(OptionLink).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(proceedButton).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, benefitTab, 240);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(benefitTab).click();
		waitUntilElementVisibleBy(driver, picExpress, "ExplicitWaitLongestTime");
		driver.findElement(picExpress).click();
		waitUntilElementVisibleBy(driver, btnAddPIC, "ExplicitWaitLongestTime");
		clickElementBy(btnAddPIC);
		waitUntilElementVisibleBy(driver, lablePICExpress, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(lablePICExpress))) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.PASS,
					"PIC Express add another resort lable is displayed successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.FAIL,
					"PIC Express add another resort lable is not displayed successfully");
		}

		waitUntilElementVisibleBy(driver, strResortName, "ExplicitWaitLongestTime");
		driver.findElement(strResortName).sendKeys(testData.get("strResortName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lableResortID, "ExplicitWaitLongestTime");
		resortID = driver.findElement(lableResortID).getText();
		driver.findElement(checkboxResort).click();
		waitUntilElementVisibleBy(driver, inventoryDetail, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(inventoryDetail))) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.PASS,
					"PIC Express resort id " + resortID + " is added successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.FAIL,
					"PIC Express resort is  not added successfully");
		}
		driver.findElement(inventoryDetail).click();
		waitUntilElementVisibleBy(driver, checkboxAnnual, "ExplicitWaitLongestTime");
		driver.findElement(checkboxAnnual).click();
		waitUntilElementVisibleBy(driver, checkboxStandard, "ExplicitWaitLongestTime");
		driver.findElement(checkboxStandard).click();
		waitUntilElementVisibleBy(driver, checkboxStudio, "ExplicitWaitLongestTime");
		driver.findElement(checkboxStudio).click();
		if (!driver.findElement(picPlusPointsAwd).getText().equals("")) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.PASS,
					"PIC Express Points " + driver.findElement(picPlusPointsAwd).getText() + " is displayed");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.FAIL,
					"PIC Express Points " + driver.findElement(picPlusPointsAwd).getText() + " is not displayed");
		}
		waitUntilElementVisibleBy(driver, buttonTotalPointUsage, "ExplicitWaitLongestTime");
		driver.findElement(buttonTotalPointUsage).click();
		waitUntilElementVisibleBy(driver, buttonPresentation, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(buttonPresentation))) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.PASS,
					"PIC Express resort is added successfully");
		} else {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.FAIL,
					"PIC Express resort is  not added successfully");
		}
		driver.findElement(buttonPresentation).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
	}

	
	protected By btnAddPIC = By.xpath("//button[text()='Add PIC']");
	protected By strResortName = By.xpath("//input[@placeholder='Filter by Resort name']");
	protected By strCountry = By.xpath("//select[contains(@class,'sales-line')]");
	protected By selectCheckBox = By.xpath("//label[contains(@for,'select-checkbox')]");
	protected By linkInventoryDetails = By.xpath("//button[text()=' Inventory Details ']");
	protected By standardChkBox = By.xpath("//strong[text()='Standard']/../div/label[@for='first-standard']");
	protected By lockOffCheckBo = By.xpath("//strong[text()='Lock-Off']/../div/label[@for='first-lock-off']");
	protected By btnTotalPointsUsage = By.xpath("//button[text()=' Points Usage Summary ']");
	protected By btnPresentationTile = By.xpath("//button[text()='Go to Presentation Tile']");

	protected By roomTypes = By.xpath("//div[@class='row lock-off-studio-wrapper']//strong");
	protected By twocombinedBedroom = By.xpath("//label[@for='first-combined-two-bedroom']");
	protected By PICPlusChkBox = By.xpath("//strong[text()='PIC Plus']/../..//label[@for='benefits-checkbox2']");
	protected By RCIAnnualRadioBtn = By.xpath("//strong[text()='Annual']/../div/label[@for='first-annual']");
	protected By lockOffCheckBox = By.xpath("//strong[text()='Lock-Off']/../div/label[@for='first-lock-off']");

	public void selectOnTourBenefitDetailsPICPlus_LockOff() {
		try {
			waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(PICPlusChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(btnAddPIC);
			waitUntilElementVisibleBy(driver, strResortName, "ExplicitWaitLongestTime");
			driver.findElement(strResortName).sendKeys(testData.get("strResortName"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(strCountry)).selectByValue(testData.get("strCountry"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lableResortID, "ExplicitWaitLongestTime");
			resortID = driver.findElement(lableResortID).getText();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(selectCheckBox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, linkInventoryDetails, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(linkInventoryDetails))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.PASS,
						"PIC Plus resort id " + resortID + " is added successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.FAIL,
						"PIC Plus resort is  not added successfully");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(linkInventoryDetails);
			waitUntilElementVisibleBy(driver, RCIAnnualRadioBtn, "ExplicitWaitLongestTime");
			driver.findElement(RCIAnnualRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(lockOffCheckBox);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String strRoomTypes[] = testData.get("LockOffRoomType").split(";");
			List<WebElement> roomTypeList = driver.findElements(roomTypes);
			for (int j = 1; j <= roomTypeList.size(); j++) {
				String strRoomType = driver
						.findElement(By.xpath("(//div[@class='row lock-off-studio-wrapper']//strong)[" + j + "]"))
						.getText();

				if (strRoomType.equals(strRoomTypes[j - 1])) {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
							"Room Type is displayed " + strRoomType);
				} else {
					tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
							"Room type is not displayed");
				}
			}

			driver.findElement(twocombinedBedroom).click();
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
					"PIC Plus room is Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(btnTotalPointsUsage);
			waitUntilElementVisibleBy(driver, btnPresentationTile, "ExplicitWaitLongestTime");
			clickElementBy(btnPresentationTile);

			// applyBtn_functionality();
		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
					"PIC Plus Benefit details is not updated successfully");
		}
	}

	protected By onTourLabel = By.xpath("//h3[text()='On Tour Benefits']");
	protected By availableTourBenefitLabel = By.xpath("//h5[text()='Available On Tour Benefits']");
	protected By availableOnTourList = By.xpath(
			"//h5[text()='Available On Tour Benefits']//following::div[@class='package-points-card full-card' or @class='package-points-card']/strong");
	protected By benefirRadioBtn = By.xpath("//strong[contains(text(),'" + testData.get("RoomType")
			+ "')]/../div/label[contains(@for,'first-studio')]");

	public void selectOnTourBenefitDetails_PICPlus() {
		String strAvlOnTourList[] = testData.get("AvailableOnTourList").split(";");
		int i;
		try {
			waitUntilElementVisibleBy(driver, OptionLink, "ExplicitWaitLongestTime");
			driver.findElement(OptionLink).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(proceedButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, benefitTab, 240);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefitTab).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(driver.findElement(onTourLabel))) {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
						"On Tour Benefit Label is displayed successfully");
			} else {
				tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
						"On Tour Benefit Label is not displayed successfully");
			}

			/*
			 * if (verifyElementDisplayed(driver.findElement(
			 * availableTourBenefitLabel))) { List<WebElement> allList =
			 * driver.findElements(availableOnTourList); for (i = 1; i <=
			 * allList.size(); i++) { String strName = driver.findElement(By
			 * .xpath(
			 * "(//h5[text()='Available On Tour Benefits']//following::div[@class='package-points-card full-card' or @class='package-points-card']/strong)["
			 * + i + "]")) .getText(); if (strName.equals(strAvlOnTourList[i -
			 * 1])) { tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.PASS, "OnTour Benefit " +
			 * strName + " " + "is displayed successfully"); } else {
			 * tcConfig.updateTestReporter("OnTourBenefitDetailsPage",
			 * "onTourBenefitDetails", Status.FAIL, "OnTour Benefit" +
			 * strName + " " + "is not displayed successfully"); } } }
			 */

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(PICPlusChkBox).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(btnAddPIC);
			waitUntilElementVisibleBy(driver, strResortName, "ExplicitWaitLongestTime");
			driver.findElement(strResortName).sendKeys(testData.get("strResortName"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(strCountry)).selectByValue(testData.get("strCountry"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lableResortID, "ExplicitWaitLongestTime");
			resortID = driver.findElement(lableResortID).getText();
			resortIDNumber.put(testData.get("ResortID"), getElementText(lableResortID).trim());
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(selectCheckBox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, linkInventoryDetails, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(linkInventoryDetails))) {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.PASS,
						"PIC Plus resort id " + resortID + " is added successfully");
			} else {
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "selectPICExpressResort", Status.FAIL,
						"PIC Plus resort is  not added successfully");
			}

			clickElementBy(linkInventoryDetails);
			waitUntilElementVisibleBy(driver, RCIAnnualRadioBtn, "ExplicitWaitLongestTime");
			driver.findElement(RCIAnnualRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(standardChkBox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(benefirRadioBtn).click();
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.PASS,
					"PIC Plus" + testData.get("RoomType") + " points is Updated successfully");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(btnTotalPointsUsage);
			waitUntilElementVisibleBy(driver, btnPresentationTile, "ExplicitWaitLongestTime");
			clickElementBy(btnPresentationTile);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// applyBtn_functionality();
		} catch (Exception e) {
			tcConfig.updateTestReporter("OnTourBenefitDetailsPage", "onTourBenefitDetails", Status.FAIL,
					"PIC Plus Benefit details is not updated successfully");
		}
	}

	public void selectSingleTourPackage() {
		String strPoint = testData.get("TourPoints");
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String cwaPackagePoint = driver.findElement(By.xpath("//div[@class='card-img-overlay']/strong")).getText();
			if (strPoint.equals(cwaPackagePoint)) {
				driver.findElement(By.xpath("//div[@class='card-header']//following::button[@id='button-basic']"))
						.click();
				tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.PASS,
						"Tour Point is displayed successfully");
			}
			driver.findElement(By
					.xpath("//div[@class='card-header']//following::ul/li[@role='menuitem']/a[contains(text(),'Options ')]"))
					.click();
			waitUntilElementVisibleBy(driver, proceedButton, "ExplicitWaitLongestTime");
			driver.findElement(proceedButton).click();

		} catch (Exception e) {
			tcConfig.updateTestReporter("CWAPitchBenefitDetailsPage", "searchTourPackage", Status.FAIL,
					"Tour Point is displayed successfully" + e.getMessage());
		}
	}
}
