package eWorksheet.pages;

import java.awt.event.ActionEvent;
import javax.xml.bind.DatatypeConverter;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointHomePage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(SalePointHomePage.class);

	public SalePointHomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;
	// socialSecurityNumber
	@FindBy(id = "wbwcommissions_salesMan")
	WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(id = "saveContract")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	WebElement conNo;
	
	
	By companySelect= By.xpath("//SELECT[@id='companyList']");
	
	

	public void companySelect() throws Exception {
		
		
		waitUntilElementVisibleBy(driver, companySelect, "ExplicitWaitLongestTime");

		String strCompany = testData.get("ServiceEntity");

		if (strCompany.equalsIgnoreCase("WBW")) {

			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Worldmark By Wyndham");

		} else if (strCompany.equalsIgnoreCase("WVR")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts");
		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter("SalePointHomePage", "companySelect", Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//driver.switchTo().activeElement().sendKeys(Keys.TAB);
		

		tcConfig.updateTestReporter("SalePointHomePage", "companySelect", Status.PASS,
				"Company Selected as: " + strCompany);
		
		clickElementJS(okBtn);
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Actions act = new Actions(driver);
		
		if(strCompany.equalsIgnoreCase("WVRAP")) {
			System.out.println("No Alert Present");
			
		}else {
		  
			try{
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
			}catch(Exception e){
				System.out.println("No Alert Present after Login");
			}
		}
		tcConfig.updateTestReporter("SalePointHomePage", "companySelect", Status.PASS,
				"HomePage Navigation Successful");
	}

}
