package eWorksheet.pages;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class LeadsPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(LeadsPage.class);

	public LeadsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static Map<String, String> objMap = new HashMap<String, String>();

	public static String createdPID;
	public static String createdTourID;
	public static String strLeadFullName;
	protected By homeTab = By.xpath("//span[contains(.,'Home')]");
	protected By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	protected By newLeads = By.xpath("//div[@title='New']");
	protected By salutationField = By.xpath("//div[contains(@class,'salutation')]//a");
	protected By firstName = By.xpath("//input[@placeholder='First Name']");
	protected By lastName = By.xpath("//input[@placeholder='Last Name']");
	protected By mobile = By.xpath("//input[@name='MobilePhone']");
	protected By email = By.xpath("//input[@name='Email']");
	protected By leadName = By
			.xpath("//input[@name='Company']");
	protected By pidEnter = By.xpath("//span[text()='PID']/parent::label/following-sibling::input");
	protected By streetEnter = By.xpath("//textarea[@name='street']");
	protected By cityEnter = By.xpath("//input[@name='city']");
	protected By zipEnter = By.xpath("//input[@name='postalCode']");
	protected By stateEnter = By.xpath("//input[@name='province']");
	protected By countryEnter = By.xpath("//span[text()='Country']/parent::span/following-sibling::div//a");
	protected By saveButton = By.xpath("//button[contains(text(),'Save') and @name='SaveEdit']");
	protected By createNewTour = By.xpath("//button[contains(text(),'Create New Tour Record')]");
	protected By acsButton = By.id("tourCheckBTN");
	protected By tourRecords = By.xpath("//span[text()='Tour Records']");
	protected By tourlanguage = By.xpath("//span[contains(text(),'Language')]/parent::span/following-sibling::div//a");
	protected By qualificationStatus = By
			.xpath("//span[contains(text(),'Qualification')]/parent::span/following-sibling::div//a");
	protected By qSelect = By.xpath("//a[@title='Q']");
	protected By qualificationSave = By.xpath("//div[@class='modal-footer slds-modal__footer']//span[text()='Save']");
	protected By tourIdText = By.xpath("//a[@data-refid='recordId']");
	protected By txtDuplicateMsg = By.xpath("//div[contains(text(),'duplicate')]");
	protected By searchAddressLink = By.xpath("//span[text()='Search Address']");
	protected By enterAddressEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'uiInput--default')]");
	protected By addressOption = By.xpath("(//div[@class='option'])[1]");
	protected By globalSearchEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'default')]");
	protected By selectLeadItem = By.xpath("(//span[@class='uiImage']/img[contains(@class,'icon')])[1]");
	protected By commentsEdit = By.xpath("//span[text()='Comments']/../..//input");
	protected By giftEligbleBtn = By.xpath("//button[text()='Incentive Eligible']");
	protected By closePopUpBtn = By.xpath("//button[text()='Close']");
	protected By txtErrorMsg = By
			.xpath("//p[contains(text(),'This lead has an international address and does not require soft scoring')]");
	protected By txtErrorMsgCondition = By
			.xpath("//p[contains(text(),'Please select a Location for the Lead to check Qualification Score')]");
	protected By reqSoftScoreBtn = By.xpath("//button[text()='Request Soft Score']");
	protected By txtQualifiedMsg = By
			.xpath("//p[contains(text(),'This guest received an active score within the last 30 days')]");
	protected By latestTour = By.xpath("//span[text()='Tour Record']//div");

	protected By closeBanner = By.xpath("//div/span/button[@title='Close' and @type='button']");
	protected By searchTour = By.xpath("//div[@class='input-group']/input[@placeholder='Filter by Tour Id or SA name']");
	protected By verifyTour = By.xpath("//table[@class='mat-table all-tours-table mat-table']/tbody/tr");
	protected By tourHomePage = By.xpath("//div[@class='swiper-wrapper ']/div");
	protected By verifyWavetime = By.xpath("(//table[@class='mat-table all-tours-table mat-table']/tbody/tr/td[4])[1]");

	public void createLead(String addressEnter) {
		waitUntilElementVisibleBy(driver, newLeads, "ExplicitWaitLongestTime");

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, firstName, "ExplicitWaitLongestTime");

			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			// clickElementBy(salutationSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(firstName, testData.get("FirstName"));
			fieldDataEnter(lastName, testData.get("LastName"));
			fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			fieldDataEnter(leadName, testData.get("LeadName"));
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				clickElementBy(countryEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement countrySelect = driver
						.findElement(By.xpath("//a[text()='" + testData.get("Country").trim() + "']"));
				countrySelect.click();

				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));

				clickElementBy(stateEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
				stateSelect.click();

				// clickElementBy(stateSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, enterAddressEdit, "ExplicitWaitLongestTime");
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			clickElementBy(saveButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}
			} else {
				waitUntilElementVisibleBy(driver, createNewTour, "ExplicitWaitLongestTime");
				WebElement lblLeadConfirm = driver
						.findElement(By.xpath("//div[contains(@class,'page-header')]/span[contains(text(),'"
								+ testData.get("LeadName") + "')]"));

				if (verifyElementDisplayed(lblLeadConfirm)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"New Lead Created by Name : " + testData.get("LeadName"));
					// + "//with PID : "+createdPID);
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL, "Lead Creation Failed");
				}
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"User is unable to navigate to Leads creation page");
		}

	}

	public void searchForLead() {
		fieldDataEnter(globalSearchEdit, testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(selectLeadItem);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement lblLeadConfirm = driver.findElement(By.xpath(
				"//div[contains(@class,'page-header')]/span[contains(text(),'" + testData.get("LeadName") + "')]"));
		if (verifyElementDisplayed(lblLeadConfirm)) {
			tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.PASS,
					"Lead details is displayed after searching for lead : " + testData.get("LeadName"));
		} else {
			tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.FAIL,
					"Unable to find the Lead details after searching for lead : " + testData.get("LeadName"));
		}
	}

	public void createTourFromLead() {
		clickElementBy(createNewTour);
		/*
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, qualificationStatus, "ExplicitWaitLongestTime");
		 * if(driver.findElement(commentsEdit).getAttribute("value").contains(
		 * "This guest was not soft scored")){
		 * tcConfig.updateTestReporter("LeadsPage", "createTourFromLead",
		 * Status.PASS, "Error Message is displayed during creation of Tours"
		 * ); }else{ tcConfig.updateTestReporter("LeadsPage",
		 * "createTourFromLead", Status.FAIL,
		 * "Error Message is NOT displayed during creation of Tours"); }
		 * 
		 * clickElementBy(qualificationStatus);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(By.xpath("//a[@title='"+testData.get(
		 * "QualificationScore")+"']"));
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(qualificationSave);
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		/*
		 * waitUntilElementVisibleBy(driver, tourRecords, "ExplicitWaitLongestTime");
		 * 
		 * if(verifyObjectDisplayed(tourRecords)) { clickElementBy(tourRecords);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * List<WebElement> listtourRecord= driver.findElements(tourIdText);
		 * tcConfig.updateTestReporter("LeadsPage", "createLead",
		 * Status.PASS, "Tour Created with No. "
		 * +listtourRecord.get(0).getText());
		 * 
		 * }else { tcConfig.updateTestReporter("LeadsPage", "createLead",
		 * Status.FAIL, "Tour Creation Failed"); }
		 */
	}

	public void checkGiftEligibility() {
		waitUntilElementVisibleBy(driver, giftEligbleBtn, "ExplicitWaitLongestTime");
		clickElementBy(giftEligbleBtn);
		waitUntilElementVisibleBy(driver, closePopUpBtn, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(txtErrorMsg)) {
			clickElementBy(closePopUpBtn);
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.PASS,
					"Error message is displayed properly");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.FAIL,
					"Error message NOT displayed properly");
			clickElementBy(closePopUpBtn);
		}
	}

	public void checkGiftEligibilityLeads() {
		waitUntilElementVisibleBy(driver, giftEligbleBtn, "ExplicitWaitLongestTime");
		clickElementBy(giftEligbleBtn);
		waitUntilElementVisibleBy(driver, reqSoftScoreBtn, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(txtErrorMsgCondition)) {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibilityLeads", Status.PASS,
					"User is able to softscore new Lead");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibilityLeads", Status.FAIL,
					"User is unable to softscore");
		}
		clickElementBy(reqSoftScoreBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		clickElementBy(giftEligbleBtn);
		waitUntilElementVisibleBy(driver, closePopUpBtn, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(txtQualifiedMsg)) {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.PASS,
					"User is unable to softscore the Lead as it " + "is already softscored");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.FAIL,
					"User is able to softscore the Lead even if it " + "is already softscored");
		}
		clickElementBy(closePopUpBtn);

	}

	public void createLeadAndTour(String addressEnter) {
		String strLeadFirstName = getRandomString(6, "upper");
		String strLeadLastName = getRandomString(7, "upper");
		/*
		 * String strLeadFirstName = testData.get("FirstName"); String
		 * strLeadLastName = testData.get("LastName");
		 */
		strLeadFullName = strLeadFirstName + " " + strLeadLastName;
		waitUntilElementVisibleBy(driver, newLeads, "ExplicitWaitLongestTime");

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, firstName, "ExplicitWaitLongestTime");
			/*clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();*/
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(firstName, strLeadFirstName);
			fieldDataEnter(lastName, strLeadLastName);
			fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			fieldDataEnter(leadName, strLeadFullName);
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));
				clickElementBy(stateEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver
						.findElement(By.xpath("//lightning-base-combobox-item[@role='option' and @data-value= 'FL']"));
				stateSelect.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				waitUntilElementVisibleBy(driver, enterAddressEdit, "ExplicitWaitLongestTime");
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			clickElementBy(saveButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				waitUntilElementVisibleBy(driver, closeBanner, 45);
				clickElementBy(closeBanner);

			} catch (Exception e1) {

				System.out.println("No new banner is displayed");

			}

			waitUntilElementVisibleBy(driver, createNewTour, "ExplicitWaitLongestTime");
			try {
				tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
						"New lead has been created with Lead Name : " + strLeadFullName);
			} catch (Exception e) {
				tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
						"Failed to create new lead " + e.getMessage());
			}
			clickElementJSWithWait(createNewTour);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, tourlanguage, "ExplicitWaitLongestTime");

			// clickElementBy(qualificationStatus);
			// WebElement qualificationStatusSelect = driver
			// .findElement(By.xpath("//a[@title='" +
			// testData.get("QualificationScore") + "']"));
			// qualificationStatusSelect.click();
			clickElementBy(tourlanguage);
			WebElement tourlanguageSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
			tourlanguageSelect.click();
			clickElementBy(qualificationSave);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes"))
			 * { if (verifyObjectDisplayed(txtDuplicateMsg)) {
			 * tcConfig.updateTestReporter("LeadsPage", "createLead",
			 * Status.PASS,
			 * "Duplicate message is displayed if same data is used during Lead Creation"
			 * ); } else { tcConfig.updateTestReporter("LeadsPage",
			 * "createLead", Status.FAIL,
			 * "Duplicate message is NOT displayed if same data is used during Lead Creation"
			 * ); }
			 * 
			 * }
			 */
		}
	}

	protected By searchTextField = By.xpath("//input[@name='Lead-search-input']");

	public void createLeadAndToureCredit() {
		String strLeadFirstName = testData.get("FirstName");
		String strLeadLastName = testData.get("LastName");
		strLeadFullName = strLeadFirstName + " " + strLeadLastName;
		try {
			/*
			 * waitUntilElementVisibleBy(driver,
			 * By.xpath("//input[@name='Lead-search-input']"), "ExplicitWaitLongestTime");
			 */
			waitUntilElementVisibleBy(driver, searchTextField, "ExplicitWaitLongestTime");
			driver.findElement(searchTextField).click();
			driver.findElement(searchTextField).clear();
			driver.findElement(searchTextField).sendKeys(strLeadFullName);
			driver.findElement(searchTextField).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//table/tbody/tr/th/span/a[contains(text(),'" + strLeadFirstName + "')]"))
					.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, createNewTour, "ExplicitWaitLongestTime");
			/*
			 * try { tcConfig.updateTestReporter("LeadsPage", "createLead",
			 * Status.PASS, "New lead has been created with Lead Name : " +
			 * strLeadFullName); } catch (Exception e) {
			 * tcConfig.updateTestReporter("LeadsPage", "createLead",
			 * Status.FAIL, "Failed to create new lead " + e.getMessage()); }
			 */
			clickElementBy(createNewTour);
			/*
			 * waitUntilElementVisibleBy(driver, tourlanguage, "ExplicitWaitLongestTime");
			 * clickElementBy(tourlanguage); WebElement tourlanguageSelect =
			 * driver .findElement(By.xpath("//a[@title='" +
			 * testData.get("tourlanguage") + "']"));
			 * tourlanguageSelect.click();
			 */
			waitUntilElementVisibleBy(driver, qualificationSave, "ExplicitWaitLongestTime");
			clickElementBy(qualificationSave);
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			/*
			 * tcConfig.updateTestReporter("LeadsPage", "createLead",
			 * Status.FAIL,
			 * "Duplicate message is NOT displayed if same data is used during Lead Creation"
			 * );
			 */
		}
	}

	public void getlatestTour() {
		String strcreatedTourID = "";
		waitUntilElementVisibleBy(driver, latestTour, "ExplicitWaitLongestTime");
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(latestTour)) {
			strcreatedTourID = driver.findElement(latestTour).getText();
			objMap.put(testData.get("AutomationID"), strcreatedTourID);
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS,
					"created TourID: " + objMap.get(testData.get("AutomationID")));
			driver.findElement(latestTour).click();
		} else {
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
		}

	}

	/*public void updateTourToSheet(String testName)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		FunctionalComponents.writeDataInExcel(testName, "TourID", createdTourID);
	}*/

	public void validateCancelTourDisplay() {
		try {

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(searchTour))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(searchTour, /*LeadsPage.createdTourID*/LeadsPage.objMap.get(testData.get("AutomationID")));
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
/*				clickElementBy(searchTour);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(searchTour).sendKeys(Keys.ENTER);
				//driver.findElement(searchTour).sendKeys(createdTourID);
*/				waitForSometime(tcConfig.getConfig().get("LongWait"));
				tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.PASS,
						"Tour ID is placed in Search box Successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.FAIL,
						"Tour ID is not placed in Search box Successfully");
			}

			List<WebElement> rowTour = driver.findElements(verifyTour);
			int tourRecordCnt = rowTour.size();
			if (tourRecordCnt == 0) {
				tcConfig.updateTestReporter("LeadPage", "validateCancelTourDisplay", Status.PASS, "Tour ID "
						+ createdTourID + " is cancelled and is not displayed in All Tour Dashboard Page Successfully");
			} else {
				tcConfig.updateTestReporter("LeadPage", "validateCancelTourDisplay", Status.FAIL,
						"Tour ID " + createdTourID + " is not cancelled and is displayed in All Tour Dashboard Page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.FAIL,
					"Tour ID is not placed in Search box Successfully" + e.getMessage());
		}
	}

	public void validateDispositionTourDisplay() {
		try {

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(searchTour))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(searchTour, /*LeadsPage.createdTourID*/LeadsPage.objMap.get(testData.get("AutomationID")));
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.PASS,
						"Tour ID is placed in Search box Successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.FAIL,
						"Tour ID is not placed in Search box Successfully");
			}

			List<WebElement> rowTour = driver.findElements(verifyTour);
			int tourRecordCnt = rowTour.size();
			String strAttr = driver.findElement(verifyTour).getAttribute("class");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (tourRecordCnt == 1 && strAttr.equals("mat-row disabled ng-star-inserted")) {
				tcConfig.updateTestReporter("LeadPage", "validateCancelTourDisplay", Status.PASS,
						"Tour ID " + createdTourID + " is dispositioned and is displayed in grayout Successfully");
			} else {
				tcConfig.updateTestReporter("LeadPage", "validateCancelTourDisplay", Status.FAIL, "Tour ID "
						+ createdTourID + " is not dispositioned and is not displayed in grayout Successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "verifySearchTourByID", Status.FAIL,
					"Tour ID is not placed in Search box Successfully" + e.getMessage());
		}
	}

	public void validateRescheduleTourDisplay() {
		try {

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(searchTour))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(searchTour).sendKeys(createdTourID);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				tcConfig.updateTestReporter("AllTourPage", "validateRescheduleTourDisplay", Status.PASS,
						"Tour ID is placed in Search box Successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateRescheduleTourDisplay", Status.FAIL,
						"Tour ID is not placed in Search box Successfully");
			}

			List<WebElement> rowTour = driver.findElements(verifyTour);
			int tourRecordCnt = rowTour.size();
			if (tourRecordCnt == 1) {
				tcConfig.updateTestReporter("LeadPage", "validateRescheduleTourDisplay", Status.PASS,
						"Tour ID " + createdTourID + " is Rescheduled Successfully");
			} else {
				tcConfig.updateTestReporter("LeadPage", "validateRescheduleTourDisplay", Status.FAIL,
						"Tour ID " + createdTourID + " is not rescheduled");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateRescheduleTourDisplay", Status.FAIL,
					"Tour ID is not placed in Search box Successfully" + e.getMessage());
		}
	}

	public void validateTourDisplay() {
		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// waitUntilElementInVisible(driver, searchTour, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(searchTour))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(searchTour).sendKeys(createdTourID);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				tcConfig.updateTestReporter("AllTourPage", "validateTourDisplay", Status.PASS,
						"Tour ID is placed in Search box Successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateTourDisplay", Status.FAIL,
						"Tour ID is not placed in Search box Successfully");
			}

			List<WebElement> rowTour = driver.findElements(verifyTour);
			int tourRecordCnt = rowTour.size();
			if (tourRecordCnt == 1) {
				tcConfig.updateTestReporter("LeadPage", "validateTourDisplay", Status.PASS,
						"Tour ID " + createdTourID + " is displayed Successfully");
			} else {
				tcConfig.updateTestReporter("LeadPage", "validateTourDisplay", Status.FAIL,
						"Tour ID " + createdTourID + " is not displayed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateTourDisplay", Status.FAIL,
					"Tour ID is not placed in Search box Successfully" + e.getMessage());
		}
	}

	public void validateScheduleWavetime() {
		String getWaveTime = "";
		try {

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(driver.findElement(searchTour))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(searchTour).sendKeys(createdTourID);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				tcConfig.updateTestReporter("AllTourPage", "validateRescheduleTourDisplay", Status.PASS,
						"Tour ID is placed in Search box Successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateRescheduleTourDisplay", Status.FAIL,
						"Tour ID is not placed in Search box Successfully");
			}

			getWaveTime = driver.findElement(verifyWavetime).getText().trim();
			if (getWaveTime.contains(TourRecordsPage.strTourAppTime)) {
				tcConfig.updateTestReporter("AllTourPage", "validateScheduleWavetime", Status.PASS,
						"Wave time " + getWaveTime + "is displayed successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "validateScheduleWavetime", Status.FAIL,
						"Wave time " + getWaveTime + " is not displayed successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "validateScheduleWavetime", Status.FAIL,
					"Wave time " + getWaveTime + " is not displayed successfully" + e.getMessage());
		}
	}

	public void placeTourID() {
		try {
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, searchTour, "ExplicitWaitLongestTime");
			if (verifyElementDisplayed(driver.findElement(searchTour))) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(searchTour).sendKeys(testData.get("TourID"));
				// waitForSometime(tcConfig.getConfig().get("LongWait"));
				tcConfig.updateTestReporter("AllTourPage", "placeTourID", Status.PASS,
						"Tour ID is placed in Search box Successfully");
			} else {
				tcConfig.updateTestReporter("AllTourPage", "placeTourID", Status.FAIL,
						"Tour ID is not placed in Search box Successfully");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("AllTourPage", "placeTourID", Status.FAIL,
					"Tour ID is not placed in Search box Successfully" + e.getMessage());
		}
	}

	public void validateplaceTourID() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		//checkLoadingSpinnerEW();
		//waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, searchTour, "ExplicitWaitLongestTime");
		if (verifyElementDisplayed(driver.findElement(searchTour))) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(searchTour).sendKeys(testData.get("TourID"));
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			tcConfig.updateTestReporter("AllTourPage", "placeTourID", Status.PASS,
					"Tour ID is placed in Search box Successfully");
		} else {
			tcConfig.updateTestReporter("AllTourPage", "placeTourID", Status.FAIL,
					"Tour ID is not placed in Search box Successfully");
		}

	}
	
	
	protected By teamColor = By.xpath("//span[contains(@class,'agent-color')]");
public void validateTeamColor(){
	
	waitForSometime(tcConfig.getConfig().get("LongWait"));
	
	if (verifyElementDisplayed(driver.findElement(teamColor))) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	
		tcConfig.updateTestReporter("AllTourPage", "placeTourID", Status.PASS,
				"Team color is displayed");
	} else {
		tcConfig.updateTestReporter("AllTourPage", "placeTourID", Status.FAIL,
				"Team Color is not displayed");
	
	}
	
	
}
	public void searchTourID() {
		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> allTourDashboardDetails = driver.findElements(tourHomePage);
			for (WebElement element : allTourDashboardDetails) {
				waitUntilElementVisibleBy(driver, tourHomePage, "ExplicitWaitLongestTime");
				String attrTourID = element.getAttribute("id");
				if (attrTourID.equals(createdTourID)) {
					element.click();
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
	
		}
	}
}
