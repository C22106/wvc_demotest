package eWorksheet.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class AgreementPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(AgreementPage.class);

	public AgreementPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String ApprovalAmount;

	// protected By ssnNumber = By.name("ssn");
	// protected By signature = By.id("printSignature");

	protected By equifaxSSN = By.xpath("//div[contains(@class,'equifax')]//input[@name='ssn']");
	protected By vccSSN = By.xpath("//div[contains(@class,'vcc-authorization')]//input[@name='ssn']");

	protected By equifaxtitleHeader = By.xpath("//div[contains(@class,'equifax')]//h1[@class='title']");
	protected By vcctitleHeader = By.xpath("//div[contains(@class,'vcc')]//h1[@class='title']");
	protected By wrctitleHeader = By.xpath("//div[contains(@class,'wrc')]//h1[@class='title']");

	protected By equifaxsignature = By.xpath("//div[contains(@class,'equifax')]//canvas[@id='printSignature']");
	protected By vccsignature = By.xpath("//div[contains(@class,'vcc')]//canvas[@id='printSignature']");
	protected By wrcsignature = By.xpath("//div[contains(@class,'wrc')]//canvas[@id='printSignature']");

	protected By equifaxagreeBtn = By.xpath("//div[contains(@class,'equifax')]//button[text()='AGREE and APPLY']");
	protected By vccagreeBtn = By.xpath("//div[contains(@class,'vcc')]//button[text()='AGREE and APPLY']");
	protected By wrcagreeBtn = By.xpath("//div[contains(@class,'wrc')]//button[text()='AGREE and APPLY']");

	protected By rewardsTerms = By.xpath("//u[contains(.,'Customer Service')]");
	protected By vccTerms = By.xpath("//th[@class='reasons-table-header align-left' and contains(.,'Other')]");

	protected By vccPstalcode = By.name("postalCode");

	protected By vccSlider = By.xpath("//div[@data-type='vcc']//span[@class='slider round']");
	protected By financeSlider = By.xpath("//div[@data-type='finance']//span[@class='slider round']");
	protected By barclaysSlider = By.xpath("//div[@data-type='wrc']//span[@class='slider round']");

	protected By equifaxWithholdBtn = By.xpath("//div[contains(@class,'equifax')]//button[text()='Withhold']");
	protected By vccWithholdBtn = By.xpath("//div[contains(@class,'vcc')]//button[text()='Withhold']");
	protected By wrcWithholdBtn = By.xpath("//div[contains(@class,'wrc')]//button[text()='Withhold']");

	protected By nextBtn = By.xpath("//button[@class='btn btn--inv next_btn float-right']");

	protected By VCCApprovalAmount = By.xpath("//div[@class='amount-approved']");

	protected By agreeBtn = By.xpath("//button[@class='btn btn--lgray-border agree']");
	protected By signatureSecondary = By.xpath("//div[@class='authorization-information']//canvas[@id='printSignature']");
	protected By agreeBtnSecondary = By
			.xpath("//div[@class='authorization-information']//button[text()='AGREE and APPLY']");

	protected By WynLogo = By.xpath("//img[@class='financial-logo-WYND']");
	protected By barclaysAgree = By.xpath("//div[@class='modal']//input[@name='agree']");
	protected By barclaysFin = By.xpath("//div[@class='modal']//button[text()='Finish']");

	public void agreementForFinancing() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, equifaxtitleHeader, 20);
		if (verifyObjectDisplayed(equifaxtitleHeader)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancing", Status.PASS,
					"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			WebElement agreeBtnFinancing = driver.findElement(agreeBtn);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", agreeBtnFinancing);
			fieldDataEnter(equifaxSSN, testData.get("PrimarySSN"));

			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(equifaxsignature);
			Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(50, 20).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancing", Status.PASS,
					"SSN and Signature Entered");
			if (verifyObjectDisplayed(agreeBtn)) {

				clickElementBy(agreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancing", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancing", Status.FAIL,
					"Getting error in Loading terms and condition page");
		}

	}

	public void agreementForFirstCusFinance() {

		waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
		String firstCusName = testData.get("FirstName") + " " + testData.get("LastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement firstCusTab = driver.findElement(By.xpath("//button[text()='" + firstCusName + "']"));

		if (verifyElementDisplayed(firstCusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());

			WebElement agreementBtn = driver.findElement(equifaxagreeBtn);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", agreementBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(equifaxSSN, testData.get("FirstCusSSN"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(equifaxsignature);
			Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"SSN and Signature Entered");

			if (verifyObjectDisplayed(equifaxagreeBtn)) {

				clickElementBy(equifaxagreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForSecondCusFinance() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(equifaxtitleHeader)) {

			tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS,
					"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(WynLogo);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			pageDown();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(equifaxSSN, testData.get("SecondCusSSN"));
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(equifaxsignature);
			Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(20, 20)
					.moveByOffset(80, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS,
					"SSN and Signature Entered");
			WebElement agreebtn = driver.findElement(equifaxagreeBtn);

			// if (verifyObjectDisplayed(equifaxagreeBtn))
			if (agreebtn.isDisplayed()) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(equifaxagreeBtn);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingSecondary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingSecondary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForSecondCusFinanceMulti() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(equifaxtitleHeader)) {

			tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS,
					"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(WynLogo);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			pageDown();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> ssn = driver.findElements(equifaxSSN);
			ssn.get(1).sendKeys(testData.get("SecondCusSSN"));
			// fieldDataEnter(equifaxSSN, testData.get("SecondCusSSN"));
			Actions builder = new Actions(driver);
			List<WebElement> canvasElement = driver.findElements(equifaxsignature);
			WebElement canvas = canvasElement.get(1);
			Action drawAction = builder.moveToElement(canvas, 20, 20).clickAndHold().moveByOffset(20, 20)
					.moveByOffset(80, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS,
					"SSN and Signature Entered");
			List<WebElement> agreebtn = driver.findElements(equifaxagreeBtn);

			// if (verifyObjectDisplayed(equifaxagreeBtn))
			if (agreebtn.get(1).isDisplayed()) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// clickElementBy(equifaxagreeBtn);
				agreebtn.get(1).click();
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingSecondary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingSecondary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForThirdCusFinance() {

		// waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
		// String thirdCusName = testData.get("ThirdCusFirstName") + " " +
		// testData.get("ThirdCusLastName");
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// WebElement CusTab = driver.findElement(By.xpath("//button[text()='" +
		// thirdCusName + "']"));
		//
		// if (verifyElementDisplayed(CusTab)) {
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// tcConfig.updateTestReporter("AgreementPage",
		// "agreementForFinancingPrimary", Status.PASS,
		// "Card Type Selected as: " +
		// driver.findElement(equifaxtitleHeader).getText());
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// WebElement agreementBtn = driver.findElement(equifaxagreeBtn);
		// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].scrollIntoView();",
		// agreementBtn);
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
		if (verifyObjectDisplayed(equifaxtitleHeader)) {

			tcConfig.updateTestReporter("SelectCardType", "selectCardEquifax", Status.PASS,
					"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(WynLogo);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			pageDown();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// fieldDataEnter(equifaxSSN, testData.get("ThirdCusSSN"));
			List<WebElement> ssn = driver.findElements(equifaxSSN);
			ssn.get(0).sendKeys(testData.get("ThirdCusSSN"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			List<WebElement> canvasElement = driver.findElements(equifaxsignature);
			WebElement canvas = canvasElement.get(0);
			// WebElement canvasElement = driver.findElement(equifaxsignature);
			Action drawAction = builder.moveToElement(canvas, 20, 20).clickAndHold().moveByOffset(20, 20)
					.moveByOffset(80, 40).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"SSN and Signature Entered");
			List<WebElement> agreebtn = driver.findElements(equifaxagreeBtn);

			// if (verifyObjectDisplayed(equifaxagreeBtn))
			if (agreebtn.get(0).isDisplayed()) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				agreebtn.get(0).click();
				// clickElementBy(equifaxagreeBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void verifyAgreementPageForVacationClubFirstCus() {
		waitUntilElementVisibleBy(driver, vcctitleHeader, "ExplicitWaitLongestTime");

		String FirstCusName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement firstCusTab = driver.findElement(
				By.xpath("//div[contains(@class,'vcc-authorization')]//button[text()='" + FirstCusName + "']"));

		if (verifyElementDisplayed(firstCusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusFirstName") + "']"))
							.isDisplayed());
			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusLastName") + "']"))
							.isDisplayed());

			String[] date = driver.findElement(By.xpath("//input[@name='dateOfBirth']")).getAttribute("value")
					.split("-");
			String formatedDate = date[1] + date[2] + date[0];

			if (formatedDate.equalsIgnoreCase(testData.get("FirstCusDOB"))) {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"DOB verified");
			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"DOB Not Verified");
			}

			fieldDataEnter(vccSSN, testData.get("FirstCusSSN"));

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusHomePhone") + "']"))
							.isDisplayed());
			WebElement Pstalcode = driver.findElement(vccPstalcode);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Pstalcode);

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("AnnualIncome") + "']"))
							.isDisplayed());

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusAddress") + "']"))
							.isDisplayed());

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"First Name, Last Name, Home phone, Annual Incom or Address are verified");
		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
					"First Name, Last Name, Home phone, Annual Incom or Address is missing");
		}

	}

	public void verifyAgreementPageForVacationClubSecondCus() {
		waitUntilElementVisibleBy(driver, vcctitleHeader, "ExplicitWaitLongestTime");

		String SecondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement SecondCusTab = driver.findElement(
				By.xpath("//div[contains(@class,'vcc-authorization')]//button[text()='" + SecondCusName + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(SecondCusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

			Assert.assertEquals(true, driver.findElement(By
					.xpath("//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusFirstName") + "']"))
					.isDisplayed());
			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusLastName") + "']"))
							.isDisplayed());
			String[] date = driver.findElement(By.xpath("//input[@name='dateOfBirth']")).getAttribute("value")
					.split("-");
			String formatedDate = date[1] + date[2] + date[0];

			if (formatedDate.equalsIgnoreCase(testData.get("SecondCusDOB"))) {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"DOB verified");
			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"DOB Not Verified");
			}

			fieldDataEnter(vccSSN, testData.get("SecondCusSSN"));
			Assert.assertEquals(true, driver.findElement(By
					.xpath("//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusHomePhone") + "']"))
					.isDisplayed());

			WebElement Pstalcode = driver.findElement(vccPstalcode);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Pstalcode);

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("AnnualIncome") + "']"))
							.isDisplayed());

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusAddress") + "']"))
							.isDisplayed());

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"First Name, Last Name, Home phone, Annual Incom or Address are verified");
		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
					"First Name, Last Name, Home phone, Annual Incom or Address is missing");
		}

	}

	public void verifyAgreementPageForVacationClubSecondCusMulti() {
		waitUntilElementVisibleBy(driver, vcctitleHeader, "ExplicitWaitLongestTime");

		String SecondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement SecondCusTab = driver.findElement(
				By.xpath("//div[contains(@class,'vcc-authorization')]//button[text()='" + SecondCusName + "']"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(SecondCusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

			Assert.assertEquals(true, driver.findElement(By
					.xpath("//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusFirstName") + "']"))
					.isDisplayed());
			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusLastName") + "']"))
							.isDisplayed());
			fieldDataEnter(vccSSN, testData.get("SecondCusSSN"));
			Assert.assertEquals(true, driver.findElement(By
					.xpath("//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusHomePhone") + "']"))
					.isDisplayed());

			WebElement Pstalcode = driver.findElement(vccPstalcode);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Pstalcode);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// Assert.assertEquals(true,
			// driver.findElements(By.xpath(
			// "//div[contains(@class,'vcc')]//input[@name='annualIncome']")).get(1)
			// .isDisplayed());

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusAddress") + "']"))
							.isDisplayed());

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"First Name, Last Name, Home phone, Annual Incom or Address are verified");
		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
					"First Name, Last Name, Home phone, Annual Incom or Address is missing");
		}

	}

	public void verifyAgreementPageForVacationClubThirdCus() {
		waitUntilElementVisibleBy(driver, vcctitleHeader, "ExplicitWaitLongestTime");

		String ThirdCusName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement ThirdCusTab = driver.findElement(
				By.xpath("//div[contains(@class,'vcc-authorization')]//button[text()='" + ThirdCusName + "']"));

		if (verifyElementDisplayed(ThirdCusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusFirstName") + "']"))
							.isDisplayed());
			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusLastName") + "']"))
							.isDisplayed());
			fieldDataEnter(vccSSN, testData.get("ThirdCusSSN"));
			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusHomePhone") + "']"))
							.isDisplayed());

			WebElement Pstalcode = driver.findElement(vccPstalcode);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Pstalcode);

			// Assert.assertEquals(true,
			// driver.findElement(By.xpath(
			// "//div[contains(@class,'vcc')]//input[@value='" +
			// testData.get("AnnualIncome") + "']"))
			// .isDisplayed());

			Assert.assertEquals(true,
					driver.findElement(By.xpath(
							"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusAddress") + "']"))
							.isDisplayed());

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"First Name, Last Name, Home phone, Annual Incom or Address are verified");
		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
					"First Name, Last Name, Home phone, Annual Incom or Address is missing");
		}

	}

	public void agreementForVacationClubFirstCus() {

		// waitUntilElementVisibleBy(driver, titleHeader, 20);
		// if (verifyObjectDisplayed(titleHeader)) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// fieldDataEnter(vccSSN, testData.get("FirstCusSSN"));
		List<WebElement> ssn = driver.findElements(vccSSN);
		ssn.get(0).sendKeys(testData.get("FirstCusSSN"));

		List<WebElement> viewTermselement = driver.findElements(vccTerms);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement.get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions builder = new Actions(driver);
		List<WebElement> canvasElement = driver.findElements(vccsignature);
		WebElement canvas = canvasElement.get(0);
		Action drawAction = builder.moveToElement(canvas, 40, 40).clickAndHold().moveByOffset(80, 80)
				.moveByOffset(60, 60).release().build();
		drawAction.perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.PASS,
				"SSN and Signature Entered");

		List<WebElement> Agree = driver.findElements(vccagreeBtn);

		if (Agree.get(0).isDisplayed()) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(vccagreeBtn);
			Agree.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.FAIL,
					"Agree Button Not Enabled");

		}

	}

	public void agreementForVacationClubSecondCus() {

		// waitUntilElementVisibleBy(driver, titleHeader, 20);
		// if (verifyObjectDisplayed(titleHeader)) {

		// waitUntilElementVisibleBy(driver, titleHeader, 20);
		// if (verifyObjectDisplayed(titleHeader)) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		fieldDataEnter(vccSSN, testData.get("SecondCusSSN"));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(vccTerms));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions builder = new Actions(driver);
		WebElement canvas = driver.findElement(vccsignature);
		Action drawAction = builder.moveToElement(canvas, 40, 40).clickAndHold().moveByOffset(80, 80)
				.moveByOffset(60, 60).release().build();
		drawAction.perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.PASS,
				"SSN and Signature Entered");

		WebElement Agree = driver.findElement(vccagreeBtn);

		if (Agree.isDisplayed()) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(vccagreeBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.FAIL,
					"Agree Button Not Enabled");

		}

	}

	public void agreementForVacationClubSecondCusMulti() {

		// waitUntilElementVisibleBy(driver, titleHeader, 20);
		// if (verifyObjectDisplayed(titleHeader)) {

		// waitUntilElementVisibleBy(driver, titleHeader, 20);
		// if (verifyObjectDisplayed(titleHeader)) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// fieldDataEnter(vccSSN, testData.get("FirstCusSSN"));
		List<WebElement> ssn = driver.findElements(vccSSN);
		ssn.get(1).sendKeys(testData.get("SecondCusSSN"));

		List<WebElement> viewTermselement = driver.findElements(vccTerms);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions builder = new Actions(driver);
		List<WebElement> canvasElement = driver.findElements(vccsignature);
		WebElement canvas = canvasElement.get(1);
		// WebElement canvas=driver.findElement(vccsignature);
		Action drawAction = builder.moveToElement(canvas, 40, 40).clickAndHold().moveByOffset(80, 80)
				.moveByOffset(60, 60).release().build();
		drawAction.perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.PASS,
				"SSN and Signature Entered");

		List<WebElement> Agree = driver.findElements(vccagreeBtn);

		if (Agree.get(1).isDisplayed()) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(vccagreeBtn);
			Agree.get(1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.FAIL,
					"Agree Button Not Enabled");

		}

	}

	public void agreementForVacationClubThirdCus() {

		// waitUntilElementVisibleBy(driver, titleHeader, 20);
		// if (verifyObjectDisplayed(titleHeader)) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// fieldDataEnter(vccSSN, testData.get("FirstCusSSN"));
		List<WebElement> ssn = driver.findElements(vccSSN);
		ssn.get(2).sendKeys(testData.get("ThirdCusSSN"));

		List<WebElement> viewTermselement = driver.findElements(vccTerms);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement.get(2));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions builder = new Actions(driver);
		List<WebElement> canvasElement = driver.findElements(vccsignature);
		WebElement canvas = canvasElement.get(2);
		Action drawAction = builder.moveToElement(canvas, 40, 40).clickAndHold().moveByOffset(80, 80)
				.moveByOffset(60, 60).release().build();
		drawAction.perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.PASS,
				"SSN and Signature Entered");

		List<WebElement> Agree = driver.findElements(vccagreeBtn);

		if (Agree.get(2).isDisplayed()) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(vccagreeBtn);
			Agree.get(2).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClub", Status.FAIL,
					"Agree Button Not Enabled");

		}

	}

	public void agreementForVacationClub() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		WebElement viewTermselement = driver.findElement(vccTerms);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions builder = new Actions(driver);
		WebElement canvasElement = driver.findElement(vccsignature);
		Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
				.moveByOffset(60, 60).release().build();
		drawAction.perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
				"SSN and Signature Entered");

		if (verifyObjectDisplayed(vccagreeBtn)) {

			clickElementBy(vccagreeBtn);
			checkLoadingSpinnerEWS();

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
					"Agree Button Not Enabled");

		}

	}

	public void agreementForRewards() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
		if (verifyObjectDisplayed(wrctitleHeader)) {

			tcConfig.updateTestReporter("SelectCardType", "selectCardbarclays", Status.PASS,
					"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			WebElement viewTermselement = driver.findElement(rewardsTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(wrcsignature);
			Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(50, 20).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForRewards", Status.PASS, "Signature Entered");

			if (verifyObjectDisplayed(agreeBtn)) {

				clickElementBy(agreeBtn);
				checkLoadingSpinnerEWS();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForRewards", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForRewards", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForRewardsFirstCus() {

		waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
		String FirstCusName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement CuTab = driver.findElement(
				By.xpath("//div[contains(@class,'wrc-authorization')]//button[text()='" + FirstCusName + "']"));

		if (verifyElementDisplayed(CuTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

			WebElement viewTermselement = driver.findElement(rewardsTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(wrcsignature);
			Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Signature Entered");

			if (verifyObjectDisplayed(agreeBtn)) {

				clickElementBy(agreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForRewardsSecondCus() {

		waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
		String secondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement SecondCusTab = driver.findElement(
				By.xpath("//div[contains(@class,'wrc-authorization')]//button[text()='" + secondCusName + "']"));

		if (verifyElementDisplayed(SecondCusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

			WebElement viewTermselement = driver.findElement(rewardsTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(wrcsignature);
			Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Signature Entered");

			if (verifyObjectDisplayed(agreeBtn)) {

				clickElementBy(agreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForRewardsSecondCusMulti() {

		waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
		String secondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement SecondCusTab = driver.findElement(
				By.xpath("//div[contains(@class,'wrc-authorization')]//button[text()='" + secondCusName + "']"));

		if (verifyElementDisplayed(SecondCusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

			List<WebElement> viewTermselement = driver.findElements(rewardsTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			List<WebElement> canvasElement = driver.findElements(wrcsignature);
			Action drawAction = builder.moveToElement(canvasElement.get(1), 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Signature Entered");

			if (verifyObjectDisplayed(agreeBtn)) {

				clickElementBy(agreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForRewardsThirdCus() {
		String thirdCusName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");

		waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
		WebElement CusTab = driver.findElement(By.xpath("//button[text()='" + thirdCusName + "']"));

		if (verifyElementDisplayed(CusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

			WebElement viewTermselement = driver.findElement(rewardsTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(wrcsignature);
			Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Signature Entered");

			if (verifyObjectDisplayed(wrcagreeBtn)) {

				clickElementBy(wrcagreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementForRewardsThirdCusMulti() {
		String thirdCusName = testData.get("ThirdCusFirstName") + " " + testData.get("ThirdCusLastName");

		waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
		WebElement CusTab = driver.findElement(By.xpath("//button[text()='" + thirdCusName + "']"));

		if (verifyElementDisplayed(CusTab)) {

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

			List<WebElement> viewTermselement = driver.findElements(rewardsTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement.get(2));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			List<WebElement> canvasElement = driver.findElements(wrcsignature);
			Action drawAction = builder.moveToElement(canvasElement.get(2), 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
					"Signature Entered");

			if (verifyObjectDisplayed(agreeBtn)) {

				clickElementBy(agreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
					"Error in Loading terms page");
		}

	}

	public void agreementConfirmationForRewards() {
		
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, barclaysAgree, "ExplicitWaitLongestTime");

		if (verifyObjectDisplayed(barclaysAgree)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(barclaysAgree);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementConfirmationForRewards", Status.PASS,
					"Agreement checkbox displayed");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(barclaysFin);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("AgreementPage", "agreementConfirmationForRewards", Status.FAIL,
					"Agreement checkbox not displayed");
		}

	}

	public void navigateToConfirmationPage() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(nextBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerEWS();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath("//div[text()='Preferred Terms & Conditions Delivery Method:']/../button[text()='NEXT']")).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void agreementForFirstCus(String cardType) {

		String firstCusName = testData.get("FirstCusFirstName") + " " + testData.get("FirstCusLastName");

		if (cardType.equalsIgnoreCase("Finance")) {

			waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
			WebElement firstCusTab = driver.findElement(By.xpath("//button[text()='" + firstCusName + "']"));

			if (verifyElementDisplayed(firstCusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());

				WebElement agreementBtn = driver.findElement(equifaxagreeBtn);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", agreementBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				fieldDataEnter(equifaxSSN, testData.get("FirstCusSSN"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(equifaxsignature);
				Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(60, 60).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(equifaxagreeBtn)) {

					clickElementBy(equifaxagreeBtn);
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Error in Loading terms page");
			}

		} else if (cardType.equalsIgnoreCase("VacationClub")) {

			waitUntilElementVisibleBy(driver, vcctitleHeader, "ExplicitWaitLongestTime");

			WebElement firstCusTab = driver.findElement(
					By.xpath("//div[contains(@class,'vcc-authorization')]//button[text()='" + firstCusName + "']"));

			if (verifyElementDisplayed(firstCusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusFirstName") + "']"))
						.isDisplayed());
				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusLastName") + "']"))
						.isDisplayed());
				fieldDataEnter(vccSSN, testData.get("PrimarySSN"));
				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusHomePhone") + "']"))
						.isDisplayed());

				WebElement Pstalcode = driver.findElement(vccPstalcode);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Pstalcode);

				Assert.assertEquals(true,
						driver.findElement(By.xpath(
								"//div[contains(@class,'vcc')]//input[@value='" + testData.get("AnnualIncome") + "']"))
								.isDisplayed());

				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("FirstCusAddress") + "']"))
						.isDisplayed());

				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"First Name, Last Name, Home phone, Annual Incom or Address are verified");
			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"First Name, Last Name, Home phone, Annual Incom or Address is missing");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			WebElement viewTermselement = driver.findElement(vccTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(vccsignature);
			Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"SSN and Signature Entered");

			if (verifyObjectDisplayed(vccagreeBtn)) {

				clickElementBy(vccagreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}
		} else if (cardType.equalsIgnoreCase("Rewards")) {

			waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
			WebElement firsCusTab = driver.findElement(By.xpath("//button[text()='" + firstCusName + "']"));

			if (verifyElementDisplayed(firsCusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

				WebElement viewTermselement = driver.findElement(rewardsTerms);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(wrcsignature);
				Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(60, 60).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Signature Entered");

				if (verifyObjectDisplayed(wrcagreeBtn)) {

					clickElementBy(wrcagreeBtn);
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Error in Loading terms page");
			}

		}

	}

	public void agreementForSecondCus(String cardType) {

		String secondCusName = testData.get("SecondCusFirstName") + " " + testData.get("SecondCusLastName");

		if (cardType.equalsIgnoreCase("Finance")) {

			waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
			WebElement secondCusTab = driver.findElement(By.xpath("//button[text()='" + secondCusName + "']"));

			if (verifyElementDisplayed(secondCusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());

				WebElement agreementBtn = driver.findElement(equifaxagreeBtn);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", agreementBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				fieldDataEnter(equifaxSSN, testData.get("FirstCusSSN"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(equifaxsignature);
				Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(60, 60).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(equifaxagreeBtn)) {

					clickElementBy(equifaxagreeBtn);
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Error in Loading terms page");
			}

		} else if (cardType.equalsIgnoreCase("VacationClub")) {

			waitUntilElementVisibleBy(driver, vcctitleHeader, "ExplicitWaitLongestTime");

			WebElement secondCusTab = driver.findElement(
					By.xpath("//div[contains(@class,'vcc-authorization')]//button[text()='" + secondCusName + "']"));

			if (verifyElementDisplayed(secondCusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusFirstName") + "']"))
						.isDisplayed());
				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusLastName") + "']"))
						.isDisplayed());
				fieldDataEnter(vccSSN, testData.get("FirstCusSSN"));
				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusHomePhone") + "']"))
						.isDisplayed());

				WebElement Pstalcode = driver.findElement(vccPstalcode);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Pstalcode);

				Assert.assertEquals(true,
						driver.findElement(By.xpath(
								"//div[contains(@class,'vcc')]//input[@value='" + testData.get("AnnualIncome") + "']"))
								.isDisplayed());

				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("SecondCusAddress") + "']"))
						.isDisplayed());

				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"First Name, Last Name, Home phone, Annual Incom or Address are verified");
			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"First Name, Last Name, Home phone, Annual Incom or Address is missing");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			WebElement viewTermselement = driver.findElement(vccTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(vccsignature);
			Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"SSN and Signature Entered");

			if (verifyObjectDisplayed(vccagreeBtn)) {

				clickElementBy(vccagreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}
		} else if (cardType.equalsIgnoreCase("Rewards")) {

			waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
			WebElement secondCusTab = driver.findElement(By.xpath("//button[text()='" + secondCusName + "']"));

			if (verifyElementDisplayed(secondCusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

				WebElement viewTermselement = driver.findElement(rewardsTerms);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(wrcsignature);
				Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(60, 60).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Signature Entered");

				if (verifyObjectDisplayed(wrcagreeBtn)) {

					clickElementBy(wrcagreeBtn);
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Error in Loading terms page");
			}

		}

	}

	public void agreementForThirdCus(String cardType) {

		String thirdCusName = testData.get("ThirdFirstName") + " " + testData.get("ThirdLastName");

		if (cardType.equalsIgnoreCase("Finance")) {

			waitUntilElementVisibleBy(driver, equifaxtitleHeader, "ExplicitWaitLongestTime");
			WebElement CusTab = driver.findElement(By.xpath("//button[text()='" + thirdCusName + "']"));

			if (verifyElementDisplayed(CusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());

				WebElement agreementBtn = driver.findElement(equifaxagreeBtn);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", agreementBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				fieldDataEnter(equifaxSSN, testData.get("ThirdCusSSN"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(equifaxsignature);
				Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(60, 60).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(equifaxagreeBtn)) {

					clickElementBy(equifaxagreeBtn);
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Error in Loading terms page");
			}

		} else if (cardType.equalsIgnoreCase("VacationClub")) {

			waitUntilElementVisibleBy(driver, vcctitleHeader, "ExplicitWaitLongestTime");

			WebElement CusTab = driver.findElement(
					By.xpath("//div[contains(@class,'vcc-authorization')]//button[text()='" + thirdCusName + "']"));

			if (verifyElementDisplayed(CusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusFirstName") + "']"))
						.isDisplayed());
				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusLastName") + "']"))
						.isDisplayed());
				fieldDataEnter(vccSSN, testData.get("ThirdCusSSN"));
				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusHomePhone") + "']"))
						.isDisplayed());

				WebElement Pstalcode = driver.findElement(vccPstalcode);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Pstalcode);

				Assert.assertEquals(true,
						driver.findElement(By.xpath(
								"//div[contains(@class,'vcc')]//input[@value='" + testData.get("AnnualIncome") + "']"))
								.isDisplayed());

				Assert.assertEquals(true, driver.findElement(By.xpath(
						"//div[contains(@class,'vcc')]//input[@value='" + testData.get("ThirdCusAddress") + "']"))
						.isDisplayed());

				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
						"First Name, Last Name, Home phone, Annual Incom or Address are verified");
			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"First Name, Last Name, Home phone, Annual Incom or Address is missing");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			WebElement viewTermselement = driver.findElement(vccTerms);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(vccsignature);
			Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
					.moveByOffset(60, 60).release().build();
			drawAction.perform();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.PASS,
					"SSN and Signature Entered");

			if (verifyObjectDisplayed(vccagreeBtn)) {

				clickElementBy(vccagreeBtn);
				checkLoadingSpinnerEWS();

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForVacationClubPrimary", Status.FAIL,
						"Agree Button Not Enabled");

			}
		} else if (cardType.equalsIgnoreCase("Rewards")) {

			waitUntilElementVisibleBy(driver, wrctitleHeader, 20);
			WebElement CusTab = driver.findElement(By.xpath("//button[text()='" + thirdCusName + "']"));

			if (verifyElementDisplayed(CusTab)) {

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Card Type Selected as: " + driver.findElement(wrctitleHeader).getText());

				WebElement viewTermselement = driver.findElement(rewardsTerms);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(wrcsignature);
				Action drawAction = builder.moveToElement(canvasElement, 40, 40).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(60, 60).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.PASS,
						"Signature Entered");

				if (verifyObjectDisplayed(wrcagreeBtn)) {

					clickElementBy(wrcagreeBtn);
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("AgreementPage", "agreementForFinancingPrimary", Status.FAIL,
						"Error in Loading terms page");
			}

		}

	}

	// **************************************************************************

	public void selectCardEQVCC() {

		// List<WebElement> cardSlider = driver.findElements(sliderSelect);

		waitUntilElementVisibleBy(driver, financeSlider, 20);

		if (verifyObjectDisplayed(financeSlider)) {

			clickElementBy(financeSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardEQVCC", Status.PASS, "Equifax selected");

			if (verifyObjectDisplayed(vccSlider)) {

				clickElementBy(vccSlider);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQVCC", Status.PASS, "VCC selected");

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQVCC", Status.FAIL,
						"VCC could not be selected");
			}

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, vcctitleHeader, 20);
			if (verifyObjectDisplayed(vcctitleHeader)) {

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());

				fieldDataEnter(vccSSN, testData.get("ssnNumber"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(vccsignature);
				Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
						"Error in Loading terms page");
			}

			waitUntilElementVisibleBy(driver, vccTerms, 20);
			if (verifyObjectDisplayed(vccTerms)) {

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"Card Type Selected as: " + driver.findElement(vcctitleHeader).getText());
				List<WebElement> ssnList = driver.findElements(vccSSN);
				// fieldDataEnter(ssnList.get(1), testData.get("ssnNumber"));

				ssnList.get(1).sendKeys(testData.get("ssnNumber"));

				WebElement viewTermselement = driver.findElement(vccTerms);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);

				List<WebElement> signList = driver.findElements(vccsignature);
				Actions builder = new Actions(driver);
				// WebElement canvasElement = driver.findElement(signature);
				Action drawAction = builder.moveToElement(signList.get(1), 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
						"Error in Loading terms page");
			}

		} else {
			tcConfig.updateTestReporter("SelectCardType", "selecteCardEQVCC", Status.FAIL,
					"Finance Slider not present");
		}

	}

	public void selectCardEQBR() {

		// List<WebElement> cardSlider = driver.findElements(sliderSelect);

		waitUntilElementVisibleBy(driver, financeSlider, 20);

		if (verifyObjectDisplayed(financeSlider)) {

			clickElementBy(financeSlider);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SelectCardType", "selectCardEQBAR", Status.PASS, "Equifax selected");

			if (verifyObjectDisplayed(barclaysSlider)) {

				clickElementBy(barclaysSlider);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBAR", Status.PASS, "barclays selected");

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBAR", Status.FAIL,
						"Barclays could not be selected");
			}

			clickElementBy(nextBtn);
			checkLoadingSpinnerEWS();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleBy(driver, equifaxtitleHeader, 20);
			if (verifyObjectDisplayed(equifaxtitleHeader)) {

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.PASS,
						"Card Type Selected as: " + driver.findElement(equifaxtitleHeader).getText());

				fieldDataEnter(vccSSN, testData.get("ssnNumber"));

				Actions builder = new Actions(driver);
				WebElement canvasElement = driver.findElement(vccsignature);
				Action drawAction = builder.moveToElement(canvasElement, 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
						"Error in Loading terms page");
			}

			waitUntilElementVisibleBy(driver, rewardsTerms, 20);
			if (verifyObjectDisplayed(rewardsTerms)) {

				/*
				 * tcConfig.updateTestReporter("SelectCardType",
				 * "selectCardEQBR", Status.PASS, "Card Type Selected as: " +
				 * driver.findElement(titleHeader).getText());
				 */
				// fieldDataEnter(ssnNumber, testData.get("ssnNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement viewTermselement = driver.findElement(rewardsTerms);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", viewTermselement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> sigList = driver.findElements(wrcsignature);

				Actions builder = new Actions(driver);
				// WebElement canvasElement =
				// driver.findElements(sigList.get(1));
				Action drawAction = builder.moveToElement(sigList.get(1), 20, 20).clickAndHold().moveByOffset(80, 80)
						.moveByOffset(50, 20).release().build();
				drawAction.perform();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.PASS,
						"SSN and Signature Entered");

				if (verifyObjectDisplayed(agreeBtn)) {

					clickElementBy(agreeBtn);
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(nextBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					checkLoadingSpinnerEWS();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
							"Agree Button Not Enabled");

				}

			} else {
				tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
						"Error in Loading terms page");
			}

		} else {
			tcConfig.updateTestReporter("SelectCardType", "selectCardEQBR", Status.FAIL,
					"Finance Slider not present");
		}

	}

}
