package eWorksheet.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SearchTourPage extends eWorksheetBasePage {

	public static final Logger log = Logger.getLogger(SearchTourPage.class);

	public SearchTourPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By tourIDEnter = By.id("tourIdSearch");
	protected By searchButton = By.xpath("//button[@class='btn tour-search-btn']");
	protected By wyndhamLogo = By.xpath("//img[@alt='wyndham']");
	protected By salesLineConfirm = By.xpath("//span[@class='agent--salesLine']");
	protected By languageConfirm = By.xpath("//span[@class='header-links']");
	protected By userConfirm = By.xpath("//span[@class='agent--username agent--dropdown']");
	protected By dateConfirm = By.id("PrintDate");
	protected By triDot = By.xpath("//i[@class='i-tri-dot']");
	protected By resultFolder = By.xpath("//div[@class='cell small-3 medium-3 tour-result']");
	protected By getStartedBtn = By.xpath("//button[@class='btn start-process-btn' and contains(.,'Yes,')]");
	protected By confirmBtn = By.xpath("//button[@class='btn start-process-btn' and contains(.,'Let')]");

	public void navigateToMenu(String menu) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, triDot, 20);

		if (verifyObjectDisplayed(triDot)) {

			clickElementBy(triDot);
			if (verifyElementDisplayed(driver.findElement(By.xpath("//a[text()='" + menu + "']")))) {
				clickElementBy(By.xpath("//a[text()='" + menu + "']"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("SearchTourPage", "navigateToMenu", Status.PASS,
						"User navigated to menu item");

			} else {
				tcConfig.updateTestReporter("SearchTourPage", "navigateToMenu", Status.FAIL,
						"menu item not present");
			}

		} else {
			tcConfig.updateTestReporter("SearchTourPage", "navigateToMenu", Status.FAIL,
					"tri dot menu icon is not present");
		}

	}

	public void searchTour() {
		waitUntilElementVisibleBy(driver, tourIDEnter, 120);
		if (verifyObjectDisplayed(tourIDEnter)) {

			String tourId = LeadsPage.createdTourID;

			fieldDataEnter(tourIDEnter, tourId);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(searchButton).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			checkLoadingSpinnerEWS();

			tcConfig.updateTestReporter("SeacrhPTourage", "searchTour", Status.PASS, "Looking for tour: " + tourId);

			if (verifyElementDisplayed(driver.findElement(By.xpath("//div[@data-tourid='" + tourId + "']")))) {

				tcConfig.updateTestReporter("SeacrhPTourage", "searchTour", Status.PASS,
						"Tour search result displayed successfully");

			} else {
				tcConfig.updateTestReporter("SeacrhPTourage", "tourSeacrh", Status.FAIL,
						"Tour search result not displaying");

			}
		} else {
			tcConfig.updateTestReporter("SeacrhPTourage", "searchTour", Status.FAIL,
					"Unable to find Tour search field");
		}

	}

	public void verfiyTourSeacrhPage() {
		waitUntilElementVisibleBy(driver, tourIDEnter, 20);
		if (verifyObjectDisplayed(tourIDEnter)) {
			if (verifyObjectDisplayed(wyndhamLogo)) {
				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
						"Wyndham Logo Present and Clickable");

				String SalesLine = driver.findElement(salesLineConfirm).getText();
				String Language = driver.findElement(languageConfirm).getText();
				String Date = driver.findElement(dateConfirm).getText();
				String User = driver.findElement(userConfirm).getText();

				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
						" Sales Line is displayed as: " + SalesLine + " Language is displayed as: " + Language
								+ " Date is displayed as: " + Date + " User is displayed as: " + User);
			} else {

				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.FAIL, "Wyndham Logo not present");
			}
		} else {

			tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.FAIL, "Tour ID field not displayed");
		}

	}

	public void verifytourSeacrhResultforWithheldcus() {

		String TourNum = testData.get("TourId");
		String firstName = testData.get("PrimaryFirstName");
		String lastName = testData.get("PrimaryLastName");

		if (verifyElementDisplayed(driver.findElement(By.xpath("//div[@data-tourid='" + TourNum + "']")))) {
			String name = driver.findElement(By.xpath("//div[@data-tourid='" + TourNum + "']/p[1]")).getText().trim();

			if (name.equalsIgnoreCase(firstName + " " + lastName)) {
				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
						" Name verified and is displayed as: " + name);
			} else {
				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
						" Name is not verified and is displayed as: " + name);
			}

		} else {
			tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
					"Tour search result not displaying");

		}

	}

	public void verifytourSeacrhResult() {
		waitUntilElementVisibleBy(driver, tourIDEnter, 20);
		if (verifyObjectDisplayed(tourIDEnter)) {
			if (verifyObjectDisplayed(wyndhamLogo)) {
				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
						"Wyndham Logo Present and Clickable");

				String strSalesLine = driver.findElement(salesLineConfirm).getText();
				String strLanguage = driver.findElement(languageConfirm).getText();
				String strDate = driver.findElement(dateConfirm).getText();
				String strUser = driver.findElement(userConfirm).getText();
				String strTourId = testData.get("strTourId");
				String firstName = testData.get("strFirstName");
				String lastName = testData.get("strLastName");
				String strAddress = testData.get("strLastName");

				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
						" Sales Line is displayed as: " + strSalesLine + " Language is displayed as: " + strLanguage
								+ " Date is displayed as: " + strDate + " User is displayed as: " + strUser);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				fieldDataEnter(tourIDEnter, strTourId);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(searchButton).click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				checkLoadingSpinnerEWS();

				if (verifyElementDisplayed(driver.findElement(By.xpath("//div[@data-tourid='" + strTourId + "']")))) {
					String name = driver
							.findElement(By.xpath("//div[@data-tourid='" + testData.get("strTourId") + "']/p[1]"))
							.getText().trim();
					String address = driver
							.findElement(By.xpath("//div[@data-tourid='" + testData.get("strTourId") + "']/p[2]"))
							.getText().trim();
					if (name.equalsIgnoreCase(firstName + " " + lastName)) {
						tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
								" Name verified and is displayed as: " + name);
					} else {
						tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
								" Name is not verified and is displayed as: " + name);
					}

					if (address.equalsIgnoreCase(strAddress)) {
						tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
								" Address verified and is displayed as: " + address);
					} else {
						tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
								" Address is not verified and is displayed as: " + address);
					}

				} else {
					tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.PASS,
							"Tour search result not displaying");

				}

			} else {

				tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.FAIL, "Wyndham Logo not present");
			}
		} else {

			tcConfig.updateTestReporter("searchTourId", "tourSeacrh", Status.FAIL, "Tour ID field not displayed");
		}

	}

	public void getStarted() {

		waitUntilElementVisibleBy(driver, resultFolder, 120);

		if (verifyObjectDisplayed(resultFolder)) {

			tcConfig.updateTestReporter("searchTourId", "getStarted", Status.PASS,
					"Tour id is displayed on search page");

			clickElementBy(resultFolder);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(getStartedBtn)) {

				tcConfig.updateTestReporter("searchTourId", "getStarted", Status.PASS,
						"User navigated to getstarted page");
				clickElementBy(getStartedBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(confirmBtn)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("searchTourId", "getStarted", Status.PASS,
							"Confirmation popup to start tour process is displayed");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(confirmBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("searchTourId", "getStarted", Status.FAIL,
							"Confirmation popup to start tour process is not displayed");
				}

			} else {

				tcConfig.updateTestReporter("searchTourId", "getStarted", Status.FAIL,
						"User not navigated to getstarted page");
			}

		} else {
			tcConfig.updateTestReporter("searchTourId", "getStarted", Status.FAIL,
					"Tour is not displaying on search result page");

		}

	}

	public void validateLogo() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleBy(driver, wyndhamLogo, 10);
		if (verifyObjectDisplayed(wyndhamLogo)) {
			tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.PASS,
					"Brand Logo is present on the Search Tour page");
		} else {
			tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.FAIL,
					"Brand Logo is not present on the Search Tour page");
		}

	}

	public void validateLogoToCustomerPage() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleBy(driver, wyndhamLogo, 10);
		if (verifyObjectDisplayed(wyndhamLogo)) {
			tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.PASS,
					"Brand Logo is present on the Search Tour page");
		} else {
			tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.FAIL,
					"Brand Logo is not present on the Search Tour page");
		}

		searchTour();

		waitUntilElementVisibleBy(driver, resultFolder, 120);

		if (verifyObjectDisplayed(resultFolder)) {

			tcConfig.updateTestReporter("searchTourId", "getStarted", Status.PASS,
					"Tour id is displayed on search page");

			clickElementBy(resultFolder);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(wyndhamLogo)) {
				tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.PASS,
						"Brand Logo is present on the Search Tour page");
			} else {
				tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.FAIL,
						"Brand Logo is not present on the Search Tour page");
			}

			if (verifyObjectDisplayed(getStartedBtn)) {

				tcConfig.updateTestReporter("searchTourId", "getStarted", Status.PASS,
						"User navigated to getstarted page");
				clickElementBy(getStartedBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(confirmBtn)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("searchTourId", "getStarted", Status.PASS,
							"Confirmation popup to start tour process is displayed");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(confirmBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("searchTourId", "getStarted", Status.FAIL,
							"Confirmation popup to start tour process is not displayed");
				}

			} else {

				tcConfig.updateTestReporter("searchTourId", "getStarted", Status.FAIL,
						"User not navigated to getstarted page");
			}

			if (verifyObjectDisplayed(wyndhamLogo)) {
				tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.PASS,
						"Brand Logo is present on the Search Tour page");
			} else {
				tcConfig.updateTestReporter("SearchTourPage", "validateLogoAcrossTheApplication", Status.FAIL,
						"Brand Logo is not present on the Search Tour page");
			}

		}

	}
}
