package eWorksheet.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import automation.core.TestBase;
import eWorksheet.pages.AddPackageInventoryPage;
import eWorksheet.pages.AgreementPage;
import eWorksheet.pages.AllTourPage;
import eWorksheet.pages.CWAPackageSummaryPage;
import eWorksheet.pages.CWAPackageWSAPage;
import eWorksheet.pages.CWAPitchBenefitDetailsPage;
import eWorksheet.pages.ConfirmationPage;
import eWorksheet.pages.CustomerDetailsPage;
import eWorksheet.pages.EndToEndACS;
import eWorksheet.pages.HomePage;
import eWorksheet.pages.InformationPage;
import eWorksheet.pages.LeadsPage;
import eWorksheet.pages.LoginPage;
import eWorksheet.pages.MyTeamAssignmentPage;
import eWorksheet.pages.PersonalInformationPage;
import eWorksheet.pages.SearchTourPage;
import eWorksheet.pages.SelectCardPage;
import eWorksheet.pages.TourBenefitPage;
import eWorksheet.pages.TourRecordsPage;
import eWorksheet.pages.WSALoginPage;

public class eWorksheetScripts extends TestBase {

	public static final Logger log = Logger.getLogger(eWorksheetScripts.class);
	
	/*
	 * Name:Utsav Function/event: tc1_ACS_WBW_Hardscoring Description: ACS
	 * Application HardScoring
	 * 
	 */
	
	
	@Test(dataProvider = "testData")
	public void EW_TC001_ManagerLogin_HeaderBar_Validation(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		try {
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyHomeHeader();
			home.verifymyToursLabel();
			home.verifyRoles();
			home.verifyDefaultSalesLocation();
			home.getSitesLocationName();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	
	@Test(dataProvider = "testData")
	public void EW_TC002_AttachSelfTour_Vacation_Calculator_ErrorMessage_on_Personal_InfoPage(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		LeadsPage lead = new LeadsPage(tcconfig);
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		
		try {
			//System.out.println("EW_TC002_AttachSelfTour_Vacation_Calculator_ErrorMessage_on_Personal_InfoPage");
			//Login to eWS
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			allTour.verifyRoleAttachToTour();
			home.selectTourByTourID();
			cusDetPage.verifyVacationCalculatorEntry();
			home.selectTourByTourID();
			cusDetPage.verifyVacationCostEditButton();
			cusDetPage.clickdestinationCheck();
			cusDetPage.clickToStarteCredit();
			personalInfo.checkbrandLogo();
			cusDetPage.clickToGetStarted();
			personalInfo.formFillUpPIHalf();
			personalInfo.verifyMandatoryError();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC003_ApplyVacationOwnershipFinancing_CWAPitchBenefitTile_AccessRecycleBin_Restore(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		LeadsPage lead = new LeadsPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
				 
		try {
			//System.out.println("EW_TC003_ApplyVacationOwnershipFinancing_CWAPitchBenefitTile_AccessRecycleBin_Restore");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
			cusDetPage.clickToStarteCredit();
			cusDetPage.clickToGetStarted();
			personalInfo.formFillUpPI();
			personalInfo.navigateToFinancialDetails();
			personalInfo.formFillUpFinancial();
			personalInfo.formFillUpAddress();
			personalInfo.formFillUpContact();
			personalInfo.navigateToSignatureDest();
			personalInfo.formSign();
			personalInfo.verifyInfoSubmitted();
			personalInfo.viewResultDest();
			personalInfo.navigateToECR();
			// to check particular user data given Credit Band
			cusDetPage.validateDestCreditBand();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.verifyPitchBenefitTilePlacedSideBySide();
			pitchBenPage.verifyEstimatedMonthlyLabel();
			pitchBenPage.verifyCWAPackagePointsInOrder();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.validateRecycleBinAccess();
			pitchBenPage.packageRestoreFromRecycleBin();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC006_Validate_My_Tour_Section(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		try {
			//System.out.println("EW_TC038_Validate_My_Tour_Section");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.myTourCountValidation();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC010_ApplyWyndhamRewardsCard_Horace_CustomerCreditSalesView_Apply_PackageUpgradation(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
				 
		try {
			//System.out.println("EW_TC010_ApplyWyndhamRewardsCard_Horace_CustomerCreditSalesView_Apply_PackageUpgradation");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            // modification need to be done here
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedRewardHorace();
            // personalInfo.selectCustResult();
            // personalInfo.viewResultReward();
            personalInfo.navigateToECR();
            // Need to add rewrads validation
            cusDetPage.validateRewardsAmount();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.packageVersionUpgrade();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC005_WVR_ApplyWyndhamRewardsCard_CustomerCreditSalesView_New(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
				 
		try {
			//System.out.println("EW_TC005_WVR_ApplyWyndhamRewardsCard_CustomerCreditSalesView_New");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();			
			cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            //modification need to be done here
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();
            personalInfo.navigateToECR();
            //Need to add rewrads validation
            cusDetPage.validateRewardsAmount();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory(); 
			cusDetPage.validateTourProgress();
			pitchBenPage.searchTourPackage();
			pitchBenPage.addNewPackagefromExisting();
			pitchBenPage.CWAPitchTile_Exit();
			home.selectTourByTourID();
			cusDetPage.validateWorkSheetMilestoneWidget();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC004_ApplyVacationClubCredit_CWAPitchBenefitTile_FinanceTileAccess(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
				 
		try {
			//System.out.println("EW_TC004_ApplyVacationClubCredit_CWAPitchBenefitTile_FinanceTileAccess");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkVCC();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            // modification to be done here
            personalInfo.clickToContinue();
            personalInfo.clickToTermsCond();
            personalInfo.checkToAccept();
            personalInfo.checkVCCAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
            cusDetPage.validateVCCAprovedAmt();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.verifyPitchBenefitTilePlacedSideBySide();
			pitchBenPage.verifyEstimatedMonthlyLabel();
			pitchBenPage.verifyCWAPackagePointsInOrder();
			pitchBenPage.paymentPriceValidation();
			pitchBenPage.comparePackage();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC018_AllTours_VerifyApprovalAmount_Rewards_Financing_Details_DownPayment_Adjustments(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig);
        LeadsPage lead = new LeadsPage(tcconfig);
        EndToEndACS acs = new EndToEndACS(tcconfig);
        CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
        AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
        
	 
		try {
			//System.out.println("EW_TC018_AllTours_VerifyApprovalAmount_Rewards_Financing_Details_DownPayment_Adjustments");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            // modification need to be done here
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedRewardHorace();
            personalInfo.navigateToECR();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.downPaymentAdjustment();
			//tourBenefit.exit_functionality();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
            allTour.validateRewardsApprovalAmount();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			/*acs.launchAcs(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
            acs.loginToAcs();
            acs.searchLOCAppDetailsforRewards();
            acs.logoutfromAcs();*/

			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC020_AllTours_VerifyLockIconUnderMarketing_Equifax_Financing_Details_New_DownPayment_Adjustments_Selection(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig);
        LeadsPage lead = new LeadsPage(tcconfig);
        CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
        AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		
		try {
			//System.out.println("EW_TC020_AllTours_VerifyLockIconUnderMarketing_Equifax_Financing_Details_New_DownPayment_Adjustments_Selection");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.newDownPaymentAdjustment();
			tourBenefit.exit_functionality();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
            allTour.verifySearchTourByID();
            //allTour.validateLockWithCreditBand();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC024_AllTour_ECR_VerifyPending_VCC_Financing_Details_Default_Term_Year_With_AutoPayments(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
        TourRecordsPage trPage = new TourRecordsPage(tcconfig);
        AllTourPage allTour = new AllTourPage(tcconfig);
        CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
        AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);

		
		try {
			//System.out.println("EW_TC024_AllTour_ECR_VerifyPending_VCC_Financing_Details_Default_Term_Year_With_AutoPayments");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
            cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit(); 
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();
            personalInfo.navigateToECR();
            cusDetPage.valGivenCreditBandReward();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.defaultTermYear();
			pitchBenPage.financialDetails_AutoPayment();
			tourBenefit.exit_functionality();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC022_AllTour_ECR_VerifyDeclined_VCC_Financing_Details_InterestRate_Disabled_for_HundredPercent_DownPayment_with_Usury_Limit(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
	    TourRecordsPage trPage = new TourRecordsPage(tcconfig);
	    AllTourPage allTour = new AllTourPage(tcconfig);
	    CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
	    PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
	    AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);

				 
		try {
			//System.out.println("EW_TC022_AllTour_ECR_VerifyDeclined_VCC_Financing_Details_InterestRate_Disabled_for_HundredPercent_DownPayment_with_Usury_Limit");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkVCC();
			cusDetPage.clickToStarteCredit();
			cusDetPage.clickToGetStarted();
			personalInfo.formFillUpPI();
			personalInfo.navigateToFinancialDetails();
			personalInfo.formFillUpFinancial();
			personalInfo.formFillUpAddress();
			personalInfo.formFillUpContact();
			cusDetPage.clickToGetStarted();
			// modification to be done here
			personalInfo.clickToContinue();
			personalInfo.clickToTermsCond();
			personalInfo.checkToAccept();
			personalInfo.checkVCCAckw();
			personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
            cusDetPage.validateCreditBandVCC();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.interestRate_Disabled_Functionality();
			pitchBenPage.interestRateUsuryLimit();
			pitchBenPage.selectInterestRate();
			pitchBenPage.validateDisplayValue();
			tourBenefit.exit_functionality();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
            allTour.validateVCCStatusCreditBand();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC025_AllTour_ECR_VerifyPending_Barclays_with_InterestRate_Basedon_CreditBand(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
     	AllTourPage allTour = new AllTourPage(tcconfig);
        CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
        LeadsPage lead = new LeadsPage(tcconfig);
        AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);

		
		try {
			//System.out.println("EW_TC025_AllTour_ECR_VerifyPending_Barclays_with_InterestRate_Basedon_CreditBand");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            // modification need to be done here
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();
            personalInfo.navigateToECR();
            cusDetPage.valGivenCreditBandReward();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.validateInterestRatebasedOnCreditBand();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
        
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC023_AllTour_ECR_VerifyDeclined_Barclays_Financing_Details_Adjust_Loan_Term_ErrorMsg_Validation(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig);
        CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
        LeadsPage lead = new LeadsPage(tcconfig);

        AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		
		try {
			//System.out.println("EW_TC023_AllTour_ECR_VerifyDeclined_Barclays_Financing_Details_Adjust_Loan_Term_ErrorMsg_Validation");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            // modification need to be done here
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();
            personalInfo.navigateToECR();
            cusDetPage.valGivenCreditBandReward();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.adjustLoanTerm();
			tourBenefit.exit_functionality();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
            allTour.verifySearchTourByID();
            allTour.validateRewardStatusCreditBand();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC029_All_Tour_Dashboard_Cancel_Tour_Validation(Map<String, String> testData) throws Exception {
		

		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		
		try {
			login.launchSalesForceApp(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToSalesForce();
			home.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			trPage.cancelNativeTour();			
			home.navigateToMenu("2");
			trPage.searchForTour();
			trPage.validateTourStatusOnGrid();
			login.salesForceLogout();
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyAllTourButton();
			lead.validateCancelTourDisplay();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}
	
	@Test(dataProvider = "testData")
	public void EW_TC030_All_Tour_Dashboard_Disposition_Tour_Validation(Map<String, String> testData) throws Exception {
		

		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		
		try {
			login.launchSalesForceApp(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToSalesForce();
			home.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			home.navigateToMenu("2");
			trPage.dispositionTour();			
			home.navigateToMenu("4");
			trPage.searchForTour();
			trPage.validateTourStatus();
			login.salesForceLogout();
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyAllTourButton();
			lead.validateDispositionTourDisplay();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}
	
	
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC017_AllTours_VerifyDeclineUnderSales_VCC_Rewards_CWAPitchBenefitTile_InactivePitch_Delete_ErrorMessage(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig);
        LeadsPage lead = new LeadsPage(tcconfig);
        CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
        AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);

				 
		try {
			//System.out.println("EW_TC017_AllTours_VerifyDeclineUnderSales_VCC_Rewards_CWAPitchBenefitTile_InactivePitch_Delete_ErrorMessage");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkVCC();
            cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            cusDetPage.clickToGetStarted();
            personalInfo.clickToContinue();
            personalInfo.clickToTermsCond();
			personalInfo.checkToAccept();
			personalInfo.checkVCCAckw();
			personalInfo.formSign();
            cusDetPage.clickToGetStarted();
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();
            personalInfo.navigateToECR();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.clickLetsBegin();
            pitchBenPage.validateRecycleBinAccess();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
            allTour.validateVCCStatusCreditBand();
            allTour.validateRewardStatusCreditBand();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	/*Test data needs to be setup for A1 band*/
	@Test(dataProvider = "testData")
	public void EW_TC007_ApplyWyndham_Ownership_VCC_Rewards_Link_Wyndham_Privacy_Statement(Map<String, String> testData) throws Exception {
		

		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

		
		try {
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
            cusDetPage.checkVCC();
            cusDetPage.checkRewards();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            cusDetPage.clickToGetStarted();
            personalInfo.clickToContinue();
            personalInfo.clickToTermsCond();
            personalInfo.checkToAccept();
            personalInfo.checkVCCAckw();
            personalInfo.formSign();
            cusDetPage.clickToGetStarted();
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();
            personalInfo.navigateToECR();
            cusDetPage.validateDestCreditBand();
            cusDetPage.validateVCCAprovedAmt();
            // need to add Rewrads Validation
            cusDetPage.validateRewardsAmount();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			wsaPage.generateWSARefNumber();
			wsaPage.checkLinkDownPayPage();
			wsaPage.financingSummaryPage();
			wsaPage.OwnerInformationPage();
			wsaPage.summaryPage();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}
	
	@Test(dataProvider = "testData")
	public void EW_TC015_AuthnDashBoard_VerifyWithheldStatus_CWAPitch_View_Loan_Details(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
			 
		try {
			//System.out.println("EW_TC015_AuthnDashBoard_VerifyWithheldStatus_CWAPitch_View_Loan_Details");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
			cusDetPage.clickToStarteCredit();
			cusDetPage.clickToGetStarted();
			personalInfo.formFillUpPI();
			personalInfo.navigateToFinancialDetails();
			personalInfo.formFillUpFinancial();
			personalInfo.formFillUpAddress();
			personalInfo.formFillUpContact();
			personalInfo.navigateToSignatureDest();
			personalInfo.formSign();
			personalInfo.verifyInfoSubmitted();
			personalInfo.navigateToECR();
			cusDetPage.clickdestinationCheck();
			cusDetPage.clickToStarteCredit();
			cusDetPage.clickToGetStarted();
			personalInfo.withholdCustomer();
			cusDetPage.validateDestCreditBandWith();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.viewLoanDetails();
			tourBenefit.addPICBenefitValidation();
			pitchBenPage.searchTourPackage();
			tourBenefit.validateAddPICButtonFunctionality();
			tourBenefit.validateDeletePICBenefit();
			tourBenefit.exit_functionality();
			//pitchBenPage.accessManagerDashboard();
			//login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC019_AllTours_VerifyCreditBands_Equifax_Add_Additional_Secondary_Owner_to_Worksheet(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		try {
			//System.out.println("EW_TC019_AllTours_VerifyCreditBands_Equifax_Add_Additional_Secondary_Owner_to_Worksheet");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
            cusDetPage.clickdestinationCheck();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			wsaPage.generateWSARefNumber();
			wsaPage.addSecondaryOwnertoWorksheet();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
            allTour.validateSoftScoreCreditBand();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC026_AllTour_VerifyDeclineIconUnderSales_VCC_with_CWAPitch_Refresh_Creditband_View(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		SearchTourPage search = new SearchTourPage(tcconfig);
		InformationPage info = new InformationPage(tcconfig);
		SelectCardPage card = new SelectCardPage(tcconfig);
		AgreementPage agreement = new AgreementPage(tcconfig);
		ConfirmationPage confirmation = new ConfirmationPage(tcconfig);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);	
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		
		try {
			//System.out.println("EW_TC026_AllTour_VerifyDeclineIconUnderSales_VCC_with_CWAPitch_Refresh_Creditband_View");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.validateMarketingScoreVCCstatus("");
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			pitchBenPage.refreshFunctionality();
			tourBenefit.exit_functionality();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC057_CWAPitch_Display_attention_in_option(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		SearchTourPage search = new SearchTourPage(tcconfig);
		InformationPage info = new InformationPage(tcconfig);
		SelectCardPage card = new SelectCardPage(tcconfig);
		AgreementPage agreement = new AgreementPage(tcconfig);
		ConfirmationPage confirmation = new ConfirmationPage(tcconfig);
		
		try {
			//System.out.println("EW_TC057_CWAPitch_Display_attention_in_option");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.selectTourByTourID();
			pitchBenPage.verifyPitchBenefitTilePlacedSideBySide();
			pitchBenPage.searchSelectedTourPackage();
			wsaPage.validateLoanDecisionMsg();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();           
         
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC058_Assign_Sales_Agent_and_Checkin_Tour_TourRecordsTab(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
			 
		try {
			////System.out.println(TestName);
			login.launchSalesForceApp(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToSalesForce();
			home.navigateToMenu("1");
			lead.createLeadAndToureCredit();
			lead.getlatestTour();
			//lead.updateTourToSheet(TestName);
			trPage.assignSalesAgentorManager("Manager");
			trPage.bookAppointment();
			//trPage.checkinNativeTour();
			home.navigateToMenu("2");
			trPage.searchForTour();
			trPage.validateTourStatusOnGrid();
			login.salesForceLogout();
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyTourID();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			tcconfig.updateTestReporterWithoutScreenshot("Assign Sales Agent and Checkin Tour",
					"EW_TC058_Assign_Sales_Agent_and_Checkin_Tour_TourRecordsTab", Status.FAIL, "Failed due to : " + e.getMessage());

			//cleanUp();
		}

	}
	//--Change Request #EW-1649
	
	
	@Test(dataProvider = "testData")
	public void EW_TC016_AuthDashBoard_VerifyApprovalAmount_VCC_Credit_Score_update_retained(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		EndToEndACS acs = new EndToEndACS(tcconfig);
				 
		try {
			//System.out.println("EW_TC016_AuthDashBoard_VerifyApprovalAmount_VCC_Credit_Score_update_retained");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkVCC();
			cusDetPage.clickToStarteCredit();
			cusDetPage.clickToGetStarted();
			personalInfo.formFillUpPI();
			personalInfo.navigateToFinancialDetails();
			personalInfo.formFillUpFinancial();
			personalInfo.formFillUpAddress();
			personalInfo.formFillUpContact();
			cusDetPage.clickToGetStarted();
			// modification to be done here
			personalInfo.clickToContinue();
			personalInfo.clickToTermsCond();
			personalInfo.checkToAccept();
			personalInfo.checkVCCAckw();
			personalInfo.formSign();
			personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.packageVersionUpgrade();
			pitchBenPage.retainedCreditBand();
			tourBenefit.exit_functionality();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
	        allTour.validateVCCApprovalAmount();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			/*acs.launchAcs(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
            acs.loginToAcs();
            acs.searchLOCAppDetails();
            acs.logoutfromAcs();*/
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC031_Validate_Tour_Schedule_Time_in_EWS(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
			 
		try {
			//System.out.println("EW_TC031_Validate_Tour_Schedule_Time_in_EWS");
			login.launchSalesForceApp(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToSalesForce();
			home.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.bookAppointment();
			home.navigateToMenu("2");
			trPage.searchForTour();
			trPage.getScheduleTime();
			login.salesForceLogout();
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyAllTourButton();
			lead.validateScheduleWavetime();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			tcconfig.updateTestReporterWithoutScreenshot("All Tour Page",
					"EW_TC031_Validate_Tour_Schedule_Time_in_EWS", Status.FAIL, "Failed due to : " + e.getMessage());

			//cleanUp();
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC011_Assign_Sales_Agent_in_Journey_to_Validate_in_EWS_Sales_Agent(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
			 
		try {
			//System.out.println("EW_TC011_Assign_Sales_Agent_in_Journey_to_Validate_in_EWS_Sales_Agent");
			/*login.launchSalesForceApp(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToSalesForce();
			home.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			trPage.assignSalesAgentorManager("Agent");
			trPage.bookAppointment();
			trPage.checkinNativeTour();
			login.salesForceLogout();*/
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.placeTourID();
			allTour.getEmployeeFunction();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC032_Names_To_Match_in_Journey_to_Eworksheet(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig); 
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig); 
		AllTourPage allTour = new AllTourPage(tcconfig); 
			 
		try {
			//System.out.println("EW_TC032_Names_To_Match_in_Journey_to_Eworksheet");
			login.launchSalesForceApp(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToSalesForce();
			home.navigateToMenu("1");
			lead.createLeadAndTour("ManualAddress");
			lead.getlatestTour();
			//trPage.assignSalesAgentorManager("Agent");
			trPage.bookAppointment();
			trPage.checkinNativeTour();
			home.navigateToMenu("2");
			trPage.searchForTour();
			trPage.validateTourStatusOnGrid();
			login.salesForceLogout();
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyAllTourButton();
			lead.placeTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.validateCustomerName();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC033_Bonus_Point_Allotted_display_Summary_DownPay_EqualorMore_20(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		
		try {
			//System.out.println("EW_TC033_Bonus_Point_Allotted_display_Summary_DownPay_EqualorMore_20");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.noCreditScore();
			tourBenefit.selectOnTourBenefitDetails_BonusPoints();
			pitchBenPage.searchTourPackage();
			/*wsaPage.generateWSARefNumber();
			sumPage.validateSummaryPage();
			sumPage.validateBonusPointRow("DownPayGreaterEqual20");
			//sumPage.VIPProgramforBonusPoint();
*/			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC034_Bonus_Point_Allotted_display_Summary_DownPay_LessEqual_20(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		
		try {
			//System.out.println("EW_TC034_Bonus_Point_Allotted_display_Summary_DownPay_LessEqual_20");
			
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.downPaymentAdjustment();
			pitchBenPage.searchTourPackage();
			pitchBenPage.noCreditScore();
			tourBenefit.selectOnTourBenefitDetails_BonusPoints();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			sumPage.validateSummaryPageNoScore();
			sumPage.validateBonusPointRow("DownPayLessEqual20");
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC035_PIC_Plus_Allotted_display_Summary_DownPay_EqualorMore_20(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		
		try {
			//System.out.println("EW_TC035_PIC_Plus_Allotted_display_Summary_DownPay_EqualorMore_20");
			
			//Login to eWorksheet
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.noCreditScore();
			pitchBenPage.selectOnTourBenefitDetails_PICPlus();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			//wsaPage.verifyDifferentPaymentMethod();
			sumPage.validateSummaryPageNoScore();
			sumPage.validatePICPlusRow("DownPayGreaterEqual20");
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC036_PIC_Express_Allotted_display_Summary_DownPay_EqualorMore_20(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		
		try {
			//System.out.println("EW_TC036_PIC_Express_Allotted_display_Summary_DownPay_EqualorMore_20");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.noCreditScore();
		    pitchBenPage.selectPICExpressResort();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			sumPage.validateSummaryPageNoScore();
			sumPage.validatePICExpressRow("DownPayGreaterEqual20");
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC037_eWorksheet_Message_popup_window_updated_moving_from_purchaser(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		
		try {
			//System.out.println("EW_TC037_eWorksheet_Message_popup_window_updated_moving_from_purchaser");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			wsaPage.generateWSARefNumber();
			wsaPage.verifyDifferentPaymentMethod();
			sumPage.validateOwnerInformationSaveDet();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	@Test(dataProvider = "testData")
	public void EW_TC014_AuthDashBoard_VerifyLockIconUnderSalesCenter_Equifax_CWAPitchBenefitTile_Compare_Package_Collateral_BenefitsAccess(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
				 
		try {
			//System.out.println("EW_TC014_AuthDashBoard_VerifyLockIconUnderSalesCenter_Equifax_CWAPitchBenefitTile_Compare_Package_Collateral_BenefitsAccess");
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
			cusDetPage.clickToStarteCredit();
			cusDetPage.clickToGetStarted();
			personalInfo.formFillUpPI();
			personalInfo.navigateToFinancialDetails();
			personalInfo.formFillUpFinancial();
			personalInfo.formFillUpAddress();
			personalInfo.formFillUpContact();
			personalInfo.navigateToSignatureDest();
			personalInfo.formSign();
			personalInfo.verifyInfoSubmitted();
			personalInfo.viewResultDest();
			personalInfo.navigateToECR();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.validateWynCollateralBenefitAccess();
			pitchBenPage.comparePackageCollateralValidate();
			//wsaPage.addSalesParticipants();
			pitchBenPage.CWAPitchTile_Exit();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
            allTour.validateLockWithCreditBand();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	

	
	
	@Test(dataProvider = "testData")
	public void EW_TC012_AuthorizationDashboard_VerifyFirstApprovalName_Recall_Submitted_Worksheet_And_Edit(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		
		try {
			//System.out.println("EW_TC012_AuthorizationDashboard_VerifyFirstApprovalName_Recall_Submitted_Worksheet_And_Edit");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			wsaPage.generateWSARefNumber();
			sumPage.validateSummaryPage();
			sumPage.click_submit_resubmit_button();
			wsaPage.validateCongratsMessage();
			pitchBenPage.validateMessageExit();
			//home.selectTourByTourID();
			home.verifyAllTourButton();
			allTour.verifySearchTourByID();
			allTour.navigateToControlPanel();
			cusDetPage.click_MileStoneButton_And_Proceed();
			sumPage.recallWorksheet();
			sumPage.validate_recall_to_workbook();
			/*pitchBenPage.selectSingleTourPackage();
			pitchBenPage.newDownPaymentAdjustment();
			tourBenPage.applyBtn_functionality();*/			
			pitchBenPage.CWAPitchTile_Exit();
			/*home.verifyAllTourButton();
			allTour.verifySearchTourByID();
            allTour.verifyObtainedByName();*/
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC013_AuthorizationDashBoard_VerifyCreditBands_SoftScore_error_msg_for_wrong_Sales_Participant(Map<String, String> testData) throws Exception {
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
				 
		try {
			//System.out.println("EW_TC013_AuthorizationDashBoard_VerifyCreditBands_SoftScore_error_msg_for_wrong_Sales_Participant");
			//Login to eWS
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.validateSoftScoreCreditBand();
			allTour.addSalesParticipant();
			login.eWorksheetLogout();
			/*login.switchToTab();
			login.oktaLogout();*/
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC008_ApplyWyndham_Ownership_VCC_Rewards_MultiParticipant_Purchasing_100_percent_down_Payment_Deal(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

		
		try {
			//System.out.println("EW_TC008_ApplyWyndham_Ownership_VCC_Rewards_MultiParticipant_Purchasing_100_percent_down_Payment_Deal");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			//lead.searchTourID();
			cusDetPage.clickdestinationCheck();
            cusDetPage.checkVCC();
            // working on 1st customer
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            // for multi particpant- P for 1st customer
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            // cusDetPage.clickToGetStarted();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            // VCC checks
            cusDetPage.clickToGetStarted();
            personalInfo.clickToContinue();
            personalInfo.clickToTermsCond();
			personalInfo.checkToAccept();
			personalInfo.checkVCCAckw();
			personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            /*personalInfo.navigateToECR();
            cusDetPage.addCustomerSecon();
            cusDetPage.checkRewardsSecon();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            // form fill up second
            personalInfo.formFillUpPISecon();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancialSecon();
            personalInfo.formFillUpAddressSecon();
            personalInfo.formFillUpContactSecon();
            cusDetPage.clickToGetStarted();
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();*/
            personalInfo.navigateToECR();
            /*cusDetPage.validateDestCreditBand();
            cusDetPage.validateVCCAprovedAmt();
            cusDetPage.validateRewardsAmountMulti("Secon");*/
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			//pitchBenPage.selectCreditBand();
			pitchBenPage.downPaymentAdjustment();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			//sumPage.validateOwnerInformationSaveDet();
			//sumPage.validateSummaryPage();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC009_ApplyVacationClubCredit_AlwaysApprove_Purchasing_point_package_with_no_Credit_Score(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		
		
		try {
			//System.out.println("EW_TC009_ApplyVacationClubCredit_AlwaysApprove_Purchasing_point_package_with_no_Credit_Score");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.checkVCC();
			cusDetPage.clickToStarteCredit();
			cusDetPage.clickToGetStarted();
			personalInfo.formFillUpPI();
			personalInfo.navigateToFinancialDetails();
			personalInfo.formFillUpFinancial();
			personalInfo.formFillUpAddress();
			personalInfo.formFillUpContact();
			cusDetPage.clickToGetStarted();
			// modification to be done here
			personalInfo.clickToContinue();
			personalInfo.clickToTermsCond();
			personalInfo.checkToAccept();
			personalInfo.formSign();
			personalInfo.verifyInfoSubmitted();
			personalInfo.navigateToECR();
			cusDetPage.validateVCCAprovedAmt();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.purchasingPointPackagewithNoCreditScore();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			pitchBenPage.CWAPitchTile_Exit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC028_ApplyEquifaxVCCBarclays_3Participant_PIC_Express_RCI_Resort_Summary_Screen_Validation(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		
		try {
			//System.out.println("EW_TC028_ApplyEquifaxVCCBarclays_3Participant_PIC_Express_RCI_Resort_Summary_Screen_Validation");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
            cusDetPage.checkRewards();
            // Seond Customer only VCC
            cusDetPage.addCustomerSecon();
            // cusDetPage.checkRewards();
            cusDetPage.checkVCCSecon("Sec");
            // Add 3rd Customer
            cusDetPage.addCustomerThird();
            cusDetPage.checkVCCSecon("Thir");
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            // for multi particpant- PI for 1st customer
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            // cusDetPage.clickToGetStarted();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            // rewards check
            cusDetPage.clickToGetStarted();
            personalInfo.checkToAcceptRewards();
            personalInfo.checkAcceptAckw();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmittedReward();

            // form fill up second
            personalInfo.formFillUpPISecon();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancialSecon();
            personalInfo.formFillUpAddressSecon();
            personalInfo.formFillUpContactSecon();
            // calling this function here to reduce the over all wait time
            // VCC checks
            cusDetPage.clickToGetStarted();
            personalInfo.clickToContinue();
            personalInfo.clickToTermsCond();
			personalInfo.checkToAccept();
			personalInfo.checkVCCAckw();
            personalInfo.formSignMulti();
            personalInfo.verifyInfoSubmitted();

            // form fill up Third
            personalInfo.formFillUpPISecon();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancialSecon();
            personalInfo.formFillUpAddressSecon();
            personalInfo.formFillUpContactSecon();
            // VCC checks
            cusDetPage.clickToGetStarted();
            personalInfo.clickToContinue();
            personalInfo.clickToTermsCond();
            personalInfo.checkToAccept();
            personalInfo.formSignMulti();
            personalInfo.verifyInfoSubmitted();

            personalInfo.navigateToECR();
            cusDetPage.validateDestCreditBand();
            cusDetPage.validateRewardsAmount();
            cusDetPage.validateVCCAmountMulti("Sec");
            cusDetPage.validateVCCAmountMulti("Thir");

			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			pitchBenPage.selectPICExpressResort();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			sumPage.validateSummaryPage();
			sumPage.validatePICExpressAtSummary();
			sumPage.click_submit_resubmit_button();
			wsaPage.validateCongratsMessage();
			pitchBenPage.validateMessageExit();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC021_VerifyBrandSpecificHeaderOnEveryPage_PIC_Plus_RCI_Resort_Summary_Screen_Validation(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
		PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		
		try {
			//System.out.println("EW_TC021_VerifyBrandSpecificHeaderOnEveryPage_PIC_Plus_RCI_Resort_Summary_Screen_Validation");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
            cusDetPage.clickToStarteCredit();
            personalInfo.checkbrandLogo();
            cusDetPage.clickToGetStarted();
            personalInfo.checkbrandLogo();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();            
            personalInfo.checkbrandLogo();
            personalInfo.formFillUpFinancial();
            personalInfo.checkbrandLogo();
            personalInfo.formFillUpAddress();
            personalInfo.checkbrandLogo();
            personalInfo.formFillUpContact();
            personalInfo.checkbrandLogo();
            personalInfo.navigateToSignatureDest();
            personalInfo.checkbrandLogo();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
            cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			pitchBenPage.selectOnTourBenefitDetails_PICPlus();
			//tourBenefit.applyBtn_functionality();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			sumPage.validateSummaryPage();
			sumPage.validatePICExpressAtSummary();
			sumPage.click_submit_resubmit_button();
			wsaPage.validateCongratsMessage();
			pitchBenPage.validateMessageExit();
			home.selectTourByTourID();	
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData")
	public void EW_TC027_VerifyContractDetails_Equifax_with_PI_Inventory_Selection_LockOff_Unit_type(Map<String, String> testData) throws Exception {
		
		
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage home = new HomePage(tcconfig);
		AllTourPage allTour = new AllTourPage(tcconfig); 
		CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
		AddPackageInventoryPage addPckg = new AddPackageInventoryPage(tcconfig);
		CWAPitchBenefitDetailsPage pitchBenPage = new CWAPitchBenefitDetailsPage(tcconfig);
		CWAPackageWSAPage wsaPage = new CWAPackageWSAPage(tcconfig);
		LeadsPage lead = new LeadsPage(tcconfig);
		TourRecordsPage trPage = new TourRecordsPage(tcconfig);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		CWAPackageSummaryPage sumPage = new CWAPackageSummaryPage(tcconfig);
		TourBenefitPage tourBenefit = new TourBenefitPage(tcconfig);
        PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
		
		
		try {
			//System.out.println("EW_TC027_VerifyContractDetails_Equifax_with_PI_Inventory_Selection_LockOff_Unit_type");
			//Login to eWorksheet
			login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
			login.loginToOKTA();
			login.loginToeWorksheet();
			home.verifyToSelectDatePicker();
			home.verifyAllTourButton();
			lead.validateplaceTourID();
			allTour.verifySelfAttachTour();
			cusDetPage.clickdestinationCheck();
            cusDetPage.clickToStarteCredit();
            cusDetPage.clickToGetStarted();
            personalInfo.formFillUpPI();
            personalInfo.navigateToFinancialDetails();
            personalInfo.formFillUpFinancial();
            personalInfo.formFillUpAddress();
            personalInfo.formFillUpContact();
            personalInfo.navigateToSignatureDest();
            personalInfo.formSign();
            personalInfo.verifyInfoSubmitted();
            personalInfo.navigateToECR();
            cusDetPage.valGivenCreditBandDest();
			cusDetPage.SheetMilestoneWidgetInventoryProcessStatus();
			cusDetPage.startWorksheet_functionality();
			addPckg.selectCWAPackagesfromInventory();
			pitchBenPage.searchTourPackage();
			pitchBenPage.applyPriceDiscount();
			pitchBenPage.searchTourPackage();
			pitchBenPage.selectCreditBand();
			pitchBenPage.selectOnTourBenefitDetailsPICPlus_LockOff();
			pitchBenPage.searchTourPackage();
			wsaPage.generateWSARefNumber();
			sumPage.validateSummaryPage();
			//sumPage.validatePICExpressAtSummary();
			sumPage.click_submit_resubmit_button();
			wsaPage.validateCongratsMessage();
			pitchBenPage.validateMessageExit();
			home.selectTourByTourID();	
			wsaPage.worksheetProgressBarCheck();
			login.eWorksheetLogout();
			login.switchToTab();
			login.oktaLogout();
			/*driver.quit();
			tcconfig.setDriver(null);
			this.driver = tb.initDriver("IE");
            this.tcconfig.setDriver(this.driver);
            SalePointLoginPage spLogin = new SalePointLoginPage(this.tcconfig);
            SalePointHomePage spHome = new SalePointHomePage(tcconfig);
            SalePointWBWMenuPage spWBW = new SalePointWBWMenuPage(tcconfig);
            SalePointWVR spWVR = new SalePointWVR(tcconfig);
            spLogin.loginToApp("IE");
            spHome.companySelect();
            spWVR.sp_New_ContractFlow();
            spWVR.sp_New_contractSearch();
            spLogin.Logout();*/

			
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	
	
	

	
	
	
	// Test data Setup for ---------------------WVR scenarios------------------
		/*Tc01, Tc_02, Tc_03, TC_15, TC_16, Tc_18 Tc24_-----Same Tour ID- EDWIN CCUPUTB
		TC_04- Any Tour with wrong PI to locked declined pending
		TC_05- any Tour with 2nd as Edwin CCuPUTB
		TC_13, Tc21, 26 - Same Tour- Always Approve
		TC_14, Tc_23 - Horace Oorange
		Tc_19, tC_25 Tc27, Tc 22, TC_46-53- Any Tour with wrong data or TC_04
		TC_20- Any random Tour
		TC 44-45 ny random Tour
		TC_57- New Tour / or lead tour Gioria Sslxhp
		TC_65- Any New Tour for fail / Edwin CCuPUTB for pass
		random lead can be used- sally bbcez*/

		/*
		 * Function/event:TC01 WVR Apply Vacation Ownership Financing for WVR 
		 * Automation Case:TC01_WVR_ApplyVacationOwnershipFinancing_Regression
		 * Pre-Requisites: User should have a WVR Tour Booked
		 *  			   
		 */
		

		// Data Setup: Run after TC_46 use the same Tour ID or use TC_22 Tour ID
		// USe TC01 as Rewards will never get approved
		@Test(dataProvider = "testData")
		public void TC_52_WVR_AuthorizationDashBoard_VerifyPending_Rewards(Map<String, String> testData) throws Exception {

			
			setupTestData(testData);
			LoginPage login = new LoginPage(tcconfig);
			HomePage home = new HomePage(tcconfig);

			AllTourPage allTour = new AllTourPage(tcconfig);
			CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
			PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

			try {
				//System.out.println("TC_52_WVR_AuthorizationDashBoard_VerifyPending_Rewards");

				// ---------Login to eWorksheet------------
				login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
				login.loginToOKTA();
				login.loginToeWorksheet();
				// login.loginToOKTA();
				// home.verifymyToursLabel();
				home.verifyToSelectDatePicker();
				home.verifyAllTourButton();
				 allTour.verifySearchTourByID();
				//allTour.verifySearchTourByIDDP("57491636");
				allTour.validateRewardStatusCreditBand();
				allTour.navigateToControlPanel();
				login.eWorksheetLogout();
				login.switchToTab();
				login.oktaLogout();

			} catch (Exception e) {
				Assert.assertTrue(false,
						getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			}

		}

		// Data Setup: Run after TC_47 or Tc_22 to use the same Tour ID can be used
		// as its WVR
		@Test(dataProvider = "testData")
		public void TC_53_WVR_GuestDashBoard_VerifyPending_Rewards(Map<String, String> testData) throws Exception {

			
			setupTestData(testData);
			LoginPage login = new LoginPage(tcconfig);
			HomePage home = new HomePage(tcconfig);

			AllTourPage allTour = new AllTourPage(tcconfig);
			CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
			PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

			try {
				//System.out.println("TC_53_WVR_GuestDashBoard_VerifyPending_Rewards");

				// ---------Login to eWorksheet------------
				login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
				login.loginToOKTA();
				login.loginToeWorksheet();
				/*
				 * home.verifymyToursLabel(); // home.clickToCustDetPage();
				 * home.verifyToSelectDatePicker(); home.verifyAllTourButton();
				 * allTour.verifySearchTourByID(); allTour.navigateToControlPanel();
				 * login.batchloginToeWorksheetAsManager();
				 */
				home.verifyToSelectDatePicker();
				home.verifyAllTourButton();
				allTour.verifySearchTourByID();
				
				allTour.navigateToControlPanel();
				cusDetPage.valGivenCreditBandReward();
				login.eWorksheetLogout();
				login.switchToTab();
				login.oktaLogout();

			} catch (Exception e) {
				Assert.assertTrue(false,
						getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			}

		}


		// Sustainability--------------------------
		// Data Setup-Use any VCC approved Tets data- Tc_02 or Tc_13
		@Test(dataProvider = "testData")
		public void TC_57_WVR_AuthorizationBoard_BuyLimits_VCC(Map<String, String> testData) throws Exception {

			
			setupTestData(testData);
			LoginPage login = new LoginPage(tcconfig);
			HomePage home = new HomePage(tcconfig);
			AllTourPage allTour = new AllTourPage(tcconfig);
			CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
			PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

			try {
				//System.out.println("TC_57_WVR_AuthorizationBoard_BuyLimits_VCC");

				// ---------Login to eWorksheet------------
				login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
				login.loginToOKTA();
				login.loginToeWorksheet();
				// When Running in Batch- Uncomment below 1 line----Batch
				// login.loginToOKTA();

				// home.verifyToSelectDatePicker();
				home.verifyAllTourButton();
				allTour.verifySearchTourByID();

				/*
				 * allTour.navigateToControlPanel(); cusDetPage.checkVCC();
				 * cusDetPage.clickToStarteCredit(); cusDetPage.clickToGetStarted();
				 * personalInfo.formFillUpPI();
				 * personalInfo.navigateToFinancialDetails();
				 * personalInfo.formFillUpFinancial();
				 * personalInfo.formFillUpAddress();
				 * personalInfo.formFillUpContact(); cusDetPage.clickToGetStarted();
				 * // modification to be done here personalInfo.clickToContinue();
				 * personalInfo.clickToTermsCond(); personalInfo.checkToAccept();
				 * personalInfo.formSign(); personalInfo.verifyInfoSubmitted();
				 * personalInfo.selectCustResult(); personalInfo.viewResultVCC();
				 * personalInfo.navigateToECR(); cusDetPage.validateVCCAprovedAmt();
				 */

				allTour.navigateToControlPanel();
				login.eWorksheetLogout();

				/*// Login To Sailpoint-----------------------------------
				driver.quit();

				tcconfig.setDriver(null);

				this.driver = tb.initDriver("IE");
				this.tcconfig.setDriver(this.driver);

				SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
				SalePointHomePage spHome = new SalePointHomePage(tcconfig);
				SalePointWBWMenuPage spWBW = new SalePointWBWMenuPage(tcconfig);
				SalePointWVR spWVR = new SalePointWVR(tcconfig);

				spLogin.loginToApp(strBrowserInUse);
				spHome.companySelect();
				// spWBW.sp_WM_ContractFlow();
				// spWBW.wbwVCCPaymentApproval();
				spWVR.sp_New_ContractFlow();
				spWVR.wvrVCCPaymentApproval();
				spLogin.Logout();

				newBrowser("CHROME");
				// login.switchToTab();
				// login.oktaLogout();
*/
			} catch (Exception e) {
				Assert.assertTrue(false,
						getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			}

		}

		// dataetup- Ned to run 5 mins after Tc_57
		@Test(dataProvider = "testData")
		public void TC_66_WVR_AuthorizationBoard_BuyLimits_VCC(Map<String, String> testData) throws Exception {

			
			setupTestData(testData);
			LoginPage login = new LoginPage(tcconfig);
			HomePage home = new HomePage(tcconfig);
			AllTourPage allTour = new AllTourPage(tcconfig);
			LeadsPage lead = new LeadsPage(tcconfig);
			EndToEndACS acs = new EndToEndACS(tcconfig);

			try {
				//System.out.println("TC_66_WVR_AuthorizationBoard_BuyLimits_VCC");

				// ---------Login to eWorksheet------------
				login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
				login.loginToOKTA();
				login.loginToeWorksheet();
				// home.verifymyToursLabel();
				home.verifyToSelectDatePicker();
				home.verifyAllTourButton();
				lead.placeTourID();
				allTour.verifySearchTourByID();
				allTour.validateVCCApprovalAmount();
				login.eWorksheetLogout();
				login.switchToTab();
				login.oktaLogout();

			} catch (Exception e) {
				Assert.assertTrue(false,
						getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			}

		}

		//---------eCredit standalone WBW scripts --------
				//Test Data: Use any data that will score band- Edin or Guelermo
				@Test(dataProvider = "testData")
				public void EW_TC038_WBW_ApplyEquifax(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					TourRecordsPage trPage = new TourRecordsPage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC038_WBW_ApplyEquifax");
						// Tour Creation from Journey application----Logging to Salesforce

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();						 
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						cusDetPage.clickdestinationCheck();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();
						personalInfo.viewResultDest();
						personalInfo.navigateToECR();
						
						cusDetPage.validateDestCreditBand();
						// home.verifyAllTourButton();
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				//Test Data: Edwin
				@Test(dataProvider = "testData")
				public void EW_TC039_WBW_ApplyVCC(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC039_WBW_ApplyVCC");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.checkVCC();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.checkVCCAckw();

						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();
						personalInfo.navigateToECR();
						
						cusDetPage.validateVCCAprovedAmt();
						//changes from Sprint2
						personalInfo.checkVCCAckwtwo();
						personalInfo.navigateToECR();
						
		                login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				//Test Data: Any tour
				@Test(dataProvider = "testData")
				public void EW_TC040_WBW_ApplyBarclays(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC040_WBW_ApplyBarclays");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
					
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.checkRewards();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmittedReward();			
						personalInfo.navigateToECR();				
						
						cusDetPage.validateRewardsAmount();
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}

				// Test Data: Edwin or anyone softscored as A1
				@Test(dataProvider = "testData")
				public void EW_TC041_WBW_ApplyEquifax_CreditBand_A1(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC041_WBW_ApplyEquifax_CreditBand_A1");

						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();						 
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.clickdestinationCheck();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();				
						personalInfo.navigateToECR();
						
						

						cusDetPage.validateDestCreditBand();
						// home.verifyAllTourButton();
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();
						
					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				//Test Data: Always Approve
				@Test(dataProvider = "testData")
				public void EW_TC042_WBW_ApplyVCC_Always_Approve_AllTour_VerifyApp_Amount(Map<String, String> testData) throws Exception {
					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
					//EndToEndACS acs = new EndToEndACS(tcconfig);

					try {
						//System.out.println("EW_TC042_WBW_ApplyVCC_Always_Approve_AllTour_VerifyApp_Amount");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();

						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.checkVCC();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.checkVCCAckw();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();
						personalInfo.navigateToECR();
						cusDetPage.validateVCCAprovedAmt();
						cusDetPage.navigateToHome();
						home.verifyAllTourButton();
						//personalInfo.navigateToAllToursFrmResults();
						
						allTour.verifySearchTourByID();
						allTour.validateVCCApprovalAmount();
						//allTour.navigateToControlPanel();
						
						
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();
						
/*						acs.launchAcs(strBrowserInUse);
						acs.loginToAcs();
						acs.searchLOCAppDetails();
						acs.logoutfromAcs();
*/
					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}

				// Data setup-Horace
				@Test(dataProvider = "testData")
				public void EW_TC043_WBW_ApplyBarclays_Horace_Oorange_AllTour_VerifyApp_Amt(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
					EndToEndACS acs = new EndToEndACS(tcconfig);
					try {
						//System.out.println("EW_TC043_WBW_ApplyBarclays_Horace_Oorange_AllTour_VerifyApp_Amt");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.checkRewards();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmittedRewardHorace();
						//personalInfo.navigateToECR();
						personalInfo.navigateToAllToursFrmResults();
						
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						cusDetPage.validateRewardsAmount();
						
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();
						
						acs.launchAcs(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						acs.loginToAcs();
						acs.searchLOCAppDetailsforRewards();
						acs.logoutfromAcs();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				//Test Data: Guilermo
				@Test(dataProvider = "testData")
				public void EW_TC044_WBW_ApplyEquifax_VCC_Barclays(Map<String, String> testData) throws Exception {
					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					TourRecordsPage trPage = new TourRecordsPage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC044_WBW_ApplyEquifax_VCC_Barclays");
						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.clickdestinationCheck();
						cusDetPage.checkVCC();
						cusDetPage.checkRewards();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.checkVCCAckw();
						personalInfo.formSign();
						cusDetPage.clickToGetStarted();
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
						personalInfo.verifyOKRewardNotAccp();
						personalInfo.navigateToECR();
						
						cusDetPage.validateDestCreditBand();
						cusDetPage.validateVCCAprovedAmt();
						// need to add Rewrads Validation
						cusDetPage.validateRewardsAmount();
						
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();
						
					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}

				// Check any  tour and add a sales rep
				@Test(dataProvider = "testData")
				public void EW_TC045_ManageTeam_SetupTeam(Map<String, String> testData) throws Exception {

					cleanUp();
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					EndToEndACS acs = new EndToEndACS(tcconfig);
					MyTeamAssignmentPage assignPage = new MyTeamAssignmentPage(tcconfig);

					try {
						//System.out.println("EW_TC045_ManageTeam_SetupTeam");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.selectSideMenu();
						home.clickAssignTeamOption();
						// assignPage.selectSalesStore();
						assignPage.selectTeamName();
						assignPage.teamEndDate();
						assignPage.selectTeamColor();
						assignPage.selectSalesAgent();
						assignPage.clickHomeButton();
						/*home.verifyAllTourButton();
						lead.validateplaceTourID();
						allTour.verifySelfAttachTour();
						assignPage.clickHomeButton();
						home.verifyAllTourButton();
						lead.validateplaceTourID();
						lead.validateTeamColor();*/
						// home.refresh();
						/** //home.verifyToSelectDatePicker(); home.verifyAllTourButton();
						 * write code to select tour & attach sales rep
						 * */
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();
						// use a sales agent id & login
						
						

					} catch (Exception e) {
						tcconfig.updateTestReporterWithoutScreenshot("eWorksheetAllTourDashboardPage",
								"EW_TC045_ManageTeam_SetupTeam", Status.FAIL,
								"Failed due to : " + e.getMessage());

						cleanUp();
					}

				}

				// Use Data: Edwin or any soft scored tour
				@Test(dataProvider = "testData")
				public void EW_TC046_WBW_AllTour_VerifyCreditBands_SoftScoreFromACS(Map<String, String> testData)
						throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					//EndToEndACS acs = new EndToEndACS(tcconfig);

					try {
						//System.out.println("EW_TC046_WBW_AllTour_VerifyCreditBands_SoftScoreFromACS");
				
						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
					
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
				
						allTour.validateSoftScoreCreditBand();
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				//DAta: Any random tour with wrong data
				@Test(dataProvider = "testData")
				public void EW_TC047_WBW_AllTour_VerifyLockIconUnderSalesCente(Map<String, String> testData)
						throws Exception {

					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					//EndToEndACS acs = new EndToEndACS(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC047_WBW_AllTour_VerifyLockIconUnderSalesCente");
						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();						 
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.clickdestinationCheck();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();
						personalInfo.navigateToAllToursFrmResults();
						
						allTour.verifySearchTourByID();
						allTour.validateLockWithCreditBand();
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

						// Login to ACS
						/*acs.launchAcs(strBrowserInUse);
						acs.loginToAcs();
						acs.retrieveWBWTourDetails();
						acs.logoutfromAcs();*/

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}

				// Use any random tour
				@Test(dataProvider = "testData")
				public void EW_TC048_WBW_AllTour_VerifyWithheldStatus(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);

					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC048_WBW_AllTour_VerifyWithheldStatus");
						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
					
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.clickdestinationCheck();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();
						personalInfo.navigateToECR();
						cusDetPage.clickdestinationCheck();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.withholdCustomer();
						cusDetPage.validateDestCreditBandWith();
						
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}

				
				// Data: random tour with wrong data
				@Test(dataProvider = "testData")
				public void EW_TC049_WBW_AllTour_VerifyDeclineIconUnderSales_VCC_Rewards(Map<String, String> testData)
						throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC049_WBW_AllTour_VerifyDeclineIconUnderSales_VCC_Rewards");
						
						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
					
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.checkVCC();
						cusDetPage.checkRewards();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.checkVCCAckw();
						personalInfo.formSign();
						cusDetPage.clickToGetStarted();
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmittedReward();
						personalInfo.navigateToAllToursFrmResults();
					
						allTour.verifySearchTourByID();
						allTour.validateVCCStatusCreditBand();
						allTour.validateRewardStatusCreditBand();
						
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();
					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				
				// Create New Tour-or any random 
				@Test(dataProvider = "testData")
				public void EW_TC050_WBW_AllTours_VerifyNoScoreIconUndeCredit(Map<String, String> testData)
						throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC050_WBW_AllTours_VerifyNoScoreIconUndeCredit");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();

						// home.verifymyToursLabel();
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
										 
						allTour.validateCreditColumn();
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				
				// same Test data as TC_35 or Tc_33
				@Test(dataProvider = "testData")
				public void EW_TC051_WBW_AllTour_VCC_noPreApproval(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					

					try {
						//System.out.println("EW_TC051_WBW_AllTour_VCC_noPreApproval");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						
						allTour.validateMarketingScoreVCCstatus("NoScore");
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				
				// Data Setup: Any random tour
				@Test(dataProvider = "testData")
				public void EW_TC052_WBW_AllTours_VerifyDeclineIcon_VCC(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);

					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC052_WBW_AllTours_VerifyDeclineIcon_VCC");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.checkVCC();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.checkVCCAckw();

						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();
						personalInfo.navigateToAllToursFrmResults();
						
						allTour.verifySearchTourByID();				 
						allTour.validateVCCStatusCreditBand();

						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				
				// Data Setup: Any random Tour
				@Test(dataProvider = "testData")
				public void EW_TC053_WBW_AllTours_VerifyDeclineIcon_Rewards(Map<String, String> testData)
						throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);

					try {
						//System.out.println("EW_TC053_WBW_AllTours_VerifyDeclineIcon_Rewards");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();				
						allTour.navigateToControlPanel();
						
						cusDetPage.checkRewards();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						// modification need to be done here
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmittedReward();
						personalInfo.navigateToAllToursFrmResults();
						allTour.verifySearchTourByID();
						
						allTour.validateRewardStatusCreditBand();
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}

				// Data Setup: Any random Tour
				@Test(dataProvider = "testData")
				public void EW_TC054_WBW_ECR_Control_VerifyPending_Rewards(Map<String, String> testData)
						throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);

					try {
						//System.out.println("EW_TC054_WBW_ECR_Control_VerifyPending_Rewards");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();				
						allTour.navigateToControlPanel();
						
						cusDetPage.checkRewards();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmittedReward();
						personalInfo.navigateToECR();
						cusDetPage.valGivenCreditBandReward();
						
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				// Need to update the case for 3 customer
				@Test(dataProvider = "testData")
				public void EW_TC055_WBW_ApplyEquifaxVCCBarclays_3Participant(Map<String, String> testData) throws Exception {
					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					TourRecordsPage trPage = new TourRecordsPage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC055_WBW_ApplyEquifaxVCCBarclays_3Participant");
						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();

						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.clickdestinationCheck();
						cusDetPage.checkRewards();
						// Seond Customer only VCC
						cusDetPage.addCustomerSecon();
						cusDetPage.checkVCCSecon("Sec");
						// Add 3rd Customer
						cusDetPage.addCustomerThird();
						cusDetPage.checkVCCSecon("Thir");
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						// for multi particpant- PI for 1st customer
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						// cusDetPage.clickToGetStarted();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						// rewards check
						cusDetPage.clickToGetStarted();
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmittedReward();

						// form fill up second
						personalInfo.formFillUpPISecon();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancialSecon();
						personalInfo.formFillUpAddressSecon();
						personalInfo.formFillUpContactSecon();
						
						// VCC checks
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.checkVCCAckw();
						personalInfo.formSignMulti();
						personalInfo.verifyInfoSubmitted();

						// form fill up Third
						personalInfo.formFillUpPISecon();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancialSecon();
						personalInfo.formFillUpAddressSecon();
						personalInfo.formFillUpContactSecon();
						// VCC checks
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.checkVCCAckw();
						personalInfo.formSignMulti();
						personalInfo.verifyInfoSubmitted();

						personalInfo.navigateToECR();
						cusDetPage.validateDestCreditBand();
						cusDetPage.validateRewardsAmount();
						/*cusDetPage.validateVCCAmountMulti("Sec");
						cusDetPage.validateVCCAmountMulti("Thir");*/

						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}

				// Data: Guelermo
				@Test(dataProvider = "testData")
				public void EW_TC056_WBW_VerifyContractDetails_Equifax_VacationClub(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					TourRecordsPage trPage = new TourRecordsPage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);
					WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
					/*WSAHomePage wsaHome = new WSAHomePage(tcconfig);
					TourLookUpPage tourLook = new TourLookUpPage(tcconfig);
					NewProposalOptionsPage newProposal = new NewProposalOptionsPage(tcconfig);*/

					try {
						//System.out.println("EW_TC056_WBW_VerifyContractDetails_Equifax_VacationClub");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();

						// --when directly going from homepage--call these 2 functions
						// home.verifymyToursLabel();
						// home.scoreCheck();
						// home.clickToCustDetPage();
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						cusDetPage.clickdestinationCheck();
						cusDetPage.checkVCC();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						cusDetPage.clickToGetStarted();
						personalInfo.clickToContinue();
						personalInfo.clickToTermsCond();
						personalInfo.checkToAccept();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();

						personalInfo.navigateToECR();
						// cusDetPage.validateBarclaysCreditBand();
						// cusDetPage.validateBarclaysCreditBand();
						// to check particular user data given Credit Band
						cusDetPage.valGivenCreditBandDest();
						// home.verifyAllTourButton();*/
						login.eWorksheetLogout();
						
							// WSA
							/*wsaLogin.loginToWSAApp(strBrowserInUse);

							wsaHome.selectEntity();
							wsaHome.pitchTypeSelection();

							tourLook.tourLookUp();

							newProposal.createNewPitch();

							wsaLogin.WSALogout();*/

							/*
							 * driver.quit();
							 * 
							 * tcconfig.setDriver(null);
							 * 
							 * this.driver = tb.initDriver("IE");
							 * this.tcconfig.setDriver(this.driver); SalePointLoginPage
							 * spLogin = new SalePointLoginPage(this.tcconfig);
							 * SalePointHomePage spHome = new SalePointHomePage(tcconfig);
							 * SalePointWBWMenuPage spWBW = new
							 * SalePointWBWMenuPage(tcconfig);
							 * 
							 * spLogin.loginToApp(strBrowserInUse); spHome.companySelect();
							 * spWBW.sp_WM_ContractFlow(); spWBW.wbwVCCPaymentApproval();
							 * spWBW.sp_WM_ContractSearch(); spLogin.Logout();
							 * 
							 * newBrowser("CHROME");
							 */
						

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
	
//Test Data: Any tour
				@Test(dataProvider = "testData")
				public void EW_TC059_Verify_OK_Button_Rewards_App_NotAccepted(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC059_Verify_OK_Button_Rewards_App_NotAccepted");

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
					
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();
						
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						
						cusDetPage.checkRewards();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						cusDetPage.clickToGetStarted();
						personalInfo.checkToAcceptRewards();
						personalInfo.checkAcceptAckw();
						personalInfo.formSign();
					
						personalInfo.verifyOKRewardNotAccp();
						

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				
				@Test(dataProvider = "testData")
				public void EW_TC060_Others_House_Verify_Zero_DownPayment_Allowed(Map<String, String> testData) throws Exception {

					
					setupTestData(testData);
					LoginPage login = new LoginPage(tcconfig);
					HomePage home = new HomePage(tcconfig);
					LeadsPage lead = new LeadsPage(tcconfig);
					TourRecordsPage trPage = new TourRecordsPage(tcconfig);
					AllTourPage allTour = new AllTourPage(tcconfig);
					CustomerDetailsPage cusDetPage = new CustomerDetailsPage(tcconfig);
					PersonalInformationPage personalInfo = new PersonalInformationPage(tcconfig);

					try {
						//System.out.println("EW_TC060_Others_House_Verify_Zero_DownPayment_Allowed");
						

						// ---------Login to eWorksheet------------
						login.launchApplication(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,strBrowserVersion,strLocation);
						login.loginToOKTA();
						login.loginToeWorksheet();
						
						home.verifyToSelectDatePicker();
						home.verifyAllTourButton();						 
						allTour.verifySearchTourByID();
						allTour.navigateToControlPanel();
						cusDetPage.clickdestinationCheck();
						cusDetPage.clickToStarteCredit();
						cusDetPage.clickToGetStarted();
						personalInfo.formFillUpPI();
						personalInfo.navigateToFinancialDetails();
						personalInfo.formFillUpFinancial();
						personalInfo.formFillUpAddress();
						personalInfo.formFillUpContact();
						personalInfo.navigateToSignatureDest();
						personalInfo.formSign();
						personalInfo.verifyInfoSubmitted();
						personalInfo.navigateToECR();			
						
						// eWorksheet down payment validation here
						
						login.eWorksheetLogout();
						login.switchToTab();
						login.oktaLogout();

					} catch (Exception e) {
						Assert.assertTrue(false,
								getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
					}

				}
				
				


}
