package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIManageUsernamePopUp_IOS extends CUIManageUsernamePopUp_Web {

	public static final Logger log = Logger.getLogger(CUIManageUsernamePopUp_IOS.class);
	public By usernameHeader=By.xpath("//h5[text()='MANAGE USERNAME']");
	public CUIManageUsernamePopUp_IOS(TestConfig tcconfig) {
		super(tcconfig);
		usernameCancelCTA = By
				.xpath("//div[@id='username-modal' and @aria-hidden='false']//button[contains(@class,'cancel-changes-button')]");
		editUsernameLink = By.xpath("//div[contains(@class,'manage-username')]//button[contains(.,'Edit')]");
	}
	
	
	/*
	 * Method: editOldUsernameField Description: Edit username field in Username
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * Kamalesh Gupta
	 */
	public void editOldUsernameField() {
		try{
		getPopUpElementInView(newUsernameField, "Yes");
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "New Username", testData.get("CUI_username"),1);	
		tcConfig.updateTestReporter("CUIManageUsernamePopUp_IOS", "editOldUsernameField", Status.PASS,
				"Username field entered successfully");
		}catch (Exception ex){
			tcConfig.updateTestReporter("CUIManageUsernamePopUp_IOS", "editOldUsernameField", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	
	/*
	 * *************************Method: closePopUpModal ***
	 * *************Description Close Any Pop Up Modal**
	 */

	public void closePopUpModal(By closeCTAElement, By closedModalElement, By elementFromPopUp, Status status) {
		if (verifyObjectDisplayed(closeCTAElement)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
					"Cancel Cta present");
			clickElementBy(closeCTAElement);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (isAlertPresent()) {
				driver.switchTo().alert().accept();
				driver.switchTo().defaultContent();
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", Status.PASS,
						"Successfully accepetd the alert popup");
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", status,
						"No popup came while Closing the pop up");
			}
			waitUntilElementVisibleBy(driver, closedModalElement, "ExplicitLongWait");
			
			if (verifyObjectDisplayed(elementFromPopUp)) {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
						"Pop Up Modal did not Closed");
			} else {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
						"Pop Up Modal  Closed");
			}

		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
					"Cancel CTA not present");
		}
	}
	
	/*
	 * Method: takenUsernameData Description: Add taken username in Username Pop
	 * Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void takenUsernameData() {
		try{
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "New Username*", testData.get("Invalid_UserName"),1);
		waitForSometime(tcConfig.getConfig().get("WaitForASec"));
		tcConfig.updateTestReporter("CUIManageUsernamePopUp_IOS", "takenUsernameData", Status.PASS,
				"Enter the data in the field succesfully");
		clickElementBy(usernameHeader);
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIManageUsernamePopUp_IOS", "takenUsernameData", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	/*
	 * Method: saveUsernameChanges Description: Save Username Changes Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void saveUsernameChanges() {
		clickElementBy(usernameHeader);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(usernameSaveCta);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		waitUntilElementVisibleBy(driver, closedUsernameModal, "ExplicitLongWait");
		
		if (verifyObjectDisplayed(newUsernameField)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveUsernameChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveUsernameChanges", Status.PASS,
					"Pop Up Modal  Closed");
			savedChangesValidation(ownerUserName, "Username", "Username");
		}
	}
}