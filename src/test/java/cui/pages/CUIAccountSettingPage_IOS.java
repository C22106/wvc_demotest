package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import automation.core.TestConfig;

public class CUIAccountSettingPage_IOS extends CUIAccountSettingPage_Web {

	public static final Logger log = Logger.getLogger(CUIAccountSettingPage_IOS.class);

	protected By hamburgerMenu = By.xpath("//button[@class='hamburger-menu']");
	protected By logOutCTA = By.xpath("//a[@class='nav-logout-button' and span[text()='Log Out']]");
	protected By usernameText = By.xpath("//input[@id='username']//following-sibling::span[contains(.,'Username*')]");

	public CUIAccountSettingPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		personalInformationHeader = By.xpath("//h2[contains(.,'PERSONAL INFOR')]");
		editPersonalInformation = By.xpath("//div[@class='personal-info']//button[contains(.,'Edit')]");
		manageUsernameHeader = By.xpath("//h2[contains(.,'MANAGE USERNAME')]");
		editUsernameLink = By.xpath("//div[@class='manage-username']//button[contains(.,'Edit')]");
		securityQuestionHeader = By.xpath("//h2[contains(.,'SECURITY QUES')]");
		editSecurityLink = By.xpath("//div[@class='manage-security-questions']//button[contains(.,'Edit')]");
		managePasswordHeader = By.xpath("//h2[contains(.,'MANAGE PASSWORD')]");
		editPasswordLink = By.xpath("//div[@class='manage-password']//button[contains(.,'Edit')]");
		/* Profile Photo */
		profilePhotoSpace = By.xpath("//div[@class='manage-profile-photo']");
		noImageInitials = By.xpath(
				"//div[@class='personal-info']/parent::div/following-sibling::div//div[@class='manage-profile-photo']//h6");
		imageUploaded = By.xpath("//div[@class='manage-profile-photo']//img");
		uploadPhotoCTA = By.xpath("//div[@class='manage-profile-photo']//span");
	}

	/*
	 * Method: logOutApplication Description: Logout from application clicking
	 * the Hamburger menu Feb/2020 Author: Unnat Jain Changes By: Kamalesh Gupta
	 * (IOS)
	 * 
	 */
	public void logOutApplication() {
		// to be removed once log out is fixed
		clickElementBy(hamburgerMenu);
		waitUntilElementVisibleBy(driver, logOutCTA, "ExplicitLongWait");
		getElementInView(logOutCTA);
		clickElementBy(logOutCTA);
		waitUntilElementVisibleBy(driver, usernameText, "ExplicitLongWait");

	}
}
