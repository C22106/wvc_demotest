package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUIManageUsernamePopUp_Android extends CUIManageUsernamePopUp_Web {

	public static final Logger log = Logger.getLogger(CUIManageUsernamePopUp_Android.class);
	protected By manageUserNameLabel = By.xpath("//h5[contains(text(),'MANAGE USERNAME')]");

	public CUIManageUsernamePopUp_Android(TestConfig tcconfig) {
		super(tcconfig);
		editUsernameLink = By.xpath("//*[@class='manage-username']//button");
		usernameCancelCTA = By.xpath("//*[@id='username-modal']//button[contains(@class,'cancel-changes')]");
		closedUsernameModal = By.xpath("//div[@class='manage-username']//button[contains(.,'Edit')]");
	}

	/*
	 * Method: editOldUsernameField Description: Edit username field in Username
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void editOldUsernameField() {

		getElementInView(newUsernameField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "New Username*", testData.get("CUI_username"), 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * /* Method: usernameCancelCTA Description: Close Manage Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void usernameCancelCTA() {

		closePopUpModalAndroid(usernameCancelCTA);
	}
	
	/*
	 * Method: takenUsernameData Description: Add taken username in Username Pop
	 * Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void takenUsernameData() {

		getElementInView(newUsernameField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "New Username*", testData.get("Invalid_UserName"), 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(manageUserNameLabel);
	}
	
	/*
	 * Method: editNewUsernameField Description: Edit New Username Field in
	 * Username Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void editNewUsernameField() {
		//enterDataInPopUpField(newUsernameField, testData.get("Username"), "Yes");
		getElementInView(newUsernameField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "New Username*",testData.get("Username"), 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

}