package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SalepointPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(SalepointPage_Web.class);

	public SalepointPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By buildVersion = By.xpath("//table[@class='client_table']");
	protected By fieldUsername = By.xpath("//input[@id='fldUsername']");
	protected By fieldPassword = By.xpath("//input[@id='fldPassword']");
	protected By loginButton = By.xpath("//input[@name='logon']");
	protected By companySelect = By.xpath("//SELECT[@id='companyList']");
	protected By buttonLogout = By.xpath("//img[@src='/webapp/ccis/images/log_off.gif']");
	protected By logoutConf = By.xpath("//div[@class='msg inform' and contains(text(),'You have been logged off. ')]");
	protected By locationSelect = By.xpath("//select[@name='location']");
	protected By saveButton = By.xpath("//input[@value='Save']");
	protected By mainMenButton = By.xpath("//input[@value='Main Menu']");
	protected By serviceEntityChange = By.xpath("//select[@name='serviceEntity']");
	protected By okButton = By.xpath("//input[@name='ok']");
	protected By next1 = By.xpath("//input[@name='next']");
	protected By customerNo = By.id("customerNumber");
	protected By textDelete = By.xpath("//a[text() = 'Delete'][0]");
	protected By totalSecondaryOwner = By.xpath("//a[text() = 'Delete']");
	protected By textStatus = By.xpath("//a[text() = 'Status']");
	protected By selectCompany = By.xpath("//SELECT[@id='companyList']");
	protected By headerPaymentOption = By.xpath("//div[contains(text(),'Contract Processing - Payment Options')]");
	protected By cashSelect = By.xpath("//td[contains(.,'Cash Sale')]/input[@type='radio']");
	protected By headerMoneyScreen = By.xpath("//div[contains(text(),'New Contract - Money Screen - In House')]");
	protected By calculateClick = By.xpath("//input[@value='Calculate']");	
	protected By headerComissionScreen = By.xpath("//div[contains(text(),'Contract Processing - Commissions and Incentives')]");
	protected By salePersonSelect = By.xpath("//input[@name='salesman']");	
	protected By headerContractSummary = By.xpath("//div[contains(text(),'Contract Summary')]");
	protected By saveClick = By.xpath("//input[@value='Save and Print']");
	protected By printClick = By.id("Print");
	protected By headerContractPrinting = By.xpath("//div[contains(text(),'Contract Printing')]");
	protected By memberNumber = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");
	protected By contractNumber = By.xpath("//td[contains(.,'The Contract Number for this contract is:')]/b");
	protected By headerMiscellaneousInformation = By.xpath("//div[contains(text(),'Contract Processing - Miscellaneous Information')]");
	protected By headerFeesScreen = By.xpath("//div[contains(text(),'Contract Processing - Fees Screen')]");
	protected By headerCustomerInformation = By.xpath("//div[contains(text(),'Contract Processing - Customer Information')]");
	protected By relationshipStatus = By.xpath("//select[@name='relationship']");
	protected By maritalStatus = By.xpath("//select[@name='marital_status']");
	protected By inputMemberNum = By.xpath("//input[contains(@name,'memberNumber')]");
	protected By ssnField = By.xpath("//input[@name='ssn']");
	protected By fieldHomePhone = By.xpath("//input[@class='textBox' and @name='home_phone']");
	protected By monthFromDropDown = By.xpath("//select[@name='dobmonth']");
	protected By dayFromDropDown = By.xpath("//select[@name='dobday']");
	protected By yearFromDropDown = By.xpath("//select[@name='dobyear']");
	protected By genderSelect = By.xpath("//input[@name='sex' and @value='M']");
	protected By saleType = By.xpath("//input[@name='fvl_saleType' and @value='M']");
	protected By nextVal = By.xpath("//input[@value='Next']");
	protected By udiClick = By.xpath("//input[@name='sales_types' and @value='UDI']");
	protected By headerDataEntry = By.xpath("//div[contains(text(),'Contract Processing - Data Entry Selection')]");
	protected By saleSubType = By.xpath("//select[@id='udi_subtype_id']");
	protected By newPointsCheck = By.id("newPoints");
	protected By inventory_Site = By.xpath("//select[@id='inventorySite']");
	protected By inventory_Phase = By.xpath("//select[@id='inventoryPhase']");
	protected By headerPriceOverride = By.xpath("//div[contains(text(),'Contract Processing - Price Override')]");
	
	
	/*
	 * Method: launchSalepoint Description:Launch Salepoint Application
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void launchSalepoint() {
		getBrowserName();
		navigateToURL(tcConfig.getConfig().get("SalepointURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(fieldUsername),"User namefield not present");
		tcConfig.updateTestReporter("SalepointPage_Web", "launchSalepoint", Status.PASS,
				"Successfully Launched SalePoint URL");

	}
	
	/*
	 * Method: setUserName Description:Enter Username
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setUserName() throws Exception {
		String spUserID = tcConfig.getConfig().get("SP_UserID");
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitWaitLongerTime");
		fieldDataEnter(fieldUsername, spUserID);
		tcConfig.updateTestReporter("SalepointPage_Web", "setUserName", Status.PASS,
				"Username is entered for Login");
	}
	
	/*
	 * Method: setPassword Description:Enter Password
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setPassword() throws Exception {
		String spPassword = tcConfig.getConfig().get("SP_Password");
		String decodedPassword = decodePassword(spPassword);
		fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("SalepointPage_Web", "setPassword", Status.PASS, "Entered password for login");
	}
	
	/*
	 * Method: clickLoginButton Description:Click Login Button
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickLoginButton(){
		waitUntilElementVisibleBy(driver, loginButton, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			tcConfig.updateTestReporter("SalepointPage_Web", "clickLoginButton", Status.PASS, "Clicked on Login Button Successfully");
		}else{
			tcConfig.updateTestReporter("SalepointPage_Web", "clickLoginButton", Status.PASS, "Failed to click on Login Button");
		}
	}
	
	/*
	 * Method: companySelectSalePoint Description:Select Company from dropdown
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void companySelectSalePoint() throws Exception {
		waitUntilElementVisibleBy(driver, companySelect, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(companySelect),"Company Select not present");
		String strCompany = testData.get("ServiceEntity");
		if (strCompany.equalsIgnoreCase("WBW")) {
			selectByText(selectCompany , "Worldmark By Wyndham");
		} else if (strCompany.equalsIgnoreCase("WVR")) {
			selectByText(selectCompany , "Wyndham Vacation Resorts");
		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			selectByText(selectCompany , "Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.PASS,
				"Company Selected as: " + strCompany);
		clickElementJSWithWait(okButton);
		acceptAlert();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.PASS,
				"HomePage Navigation Successful");
	}
	
	/*
	 * Method: navigateToMenu Description:Navigate to menu
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void navigateToMenu(String Iterator) throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL" + Iterator));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	
	/*
	 * Method: contractProcessinglocateCustomer Description:Contract Processing Locate Customer
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessinglocateCustomer() throws Exception {
		waitUntilElementVisibleBy(driver, customerNo, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(customerNo),"Customer Number not present");
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessinglocateCustomer", Status.PASS,
					"contract Processing locate Customer Page Navigation Successful");
			String createdTourid = testData.get("AccountNumber");
			fieldDataEnter(customerNo, createdTourid);
			clickElementJSWithWait(next1);
			 acceptAlert();
			 delectSecondaryOwner();	
	}
	
	/*
	 * Method: delectSecondaryOwner Description:Delete any Secondary Owner
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void delectSecondaryOwner(){
		if (verifyObjectDisplayed(textDelete)) {
			if (getList(totalSecondaryOwner).size() > 0) {
				if (getList(totalSecondaryOwner).size() == 1) {
					clickElementJSWithWait(textStatus);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					acceptAlert();
			}else{
				for (int i = 0; i < getList(totalSecondaryOwner).size() -1; i++) {
                	clickElementJSWithWait(getList(totalSecondaryOwner).get(0));
                    waitForSometime(tcConfig.getConfig().get("MedWait"));
                    acceptAlert();
                    waitForSometime(tcConfig.getConfig().get("LowWait"));

    
				}
				clickElementJSWithWait(textStatus);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				acceptAlert();
			}
		} 
	}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		acceptAlert();
}
	/*
	 * Method: contractProcessingOwners Description:Enter data in Contract page
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	
	public void contractProcessingOwners() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(headerCustomerInformation),"Customer Info Header not present");
			try {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				selectByIndex(relationshipStatus , 2);
				selectByIndex(maritalStatus , 2);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*if (!getObject(inputMemberNum).isEnabled() == false) {
					fieldDataEnter(inputMemberNum, testData.get("memberNumber"));
				}*/
				if (!getObject(ssnField).isEnabled() == false) {
					fieldDataEnter(ssnField, testData.get("ssnNumber"));
				}
				if (!getObject(fieldHomePhone).isEnabled() == false) {
					fieldDataEnter(fieldHomePhone, testData.get("HomePhone"));
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(monthFromDropDown).isEnabled()) {
					selectByIndex(monthFromDropDown , 2);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					selectByIndex(dayFromDropDown , 2);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					selectByIndex(yearFromDropDown , 2);
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(getObject(genderSelect));
				clickElementJSWithWait(getObject(saleType));
				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(nextVal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(nextVal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractProcessingCustomerInformation", Status.PASS,
						"Customer Info page saved succesfully");

			} catch (Exception e) {
				tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingCustomerInformation", Status.FAIL,
						"Error in filling Contract Processing - Customer Information");
			}
	}
	
	/*
	 * Method: contractProcessingDataEntrySelection Description:Contract Processing Data Entry
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingDataEntrySelection() throws Exception {
		waitUntilElementVisibleBy(driver, udiClick, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerDataEntry),"Data Entry Header not present");
			String salestype = testData.get("SalesType");
			String subSalesType = testData.get("SubSalesType");
			WebElement type_Sales = getObject(By.xpath("//input[@name='sales_types' and @value='"+salestype+"']"));
			clickElementJSWithWait(type_Sales);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(saleSubType , subSalesType);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(nextVal);
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingDataEntrySelection", Status.PASS,
					"Successfully enetred Details in Data Entry Page");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	/*
	 * Method: contractProcessingInventrySelectionPhase Description:Contract Processing Inventory Selection Phase
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingInventrySelectionPhase() throws Exception {
		String invName = testData.get("invSite");
		String invPhase = testData.get("invPhase");
		String totalPoints = testData.get("totalNewPoint");
		waitUntilElementVisibleBy(driver, newPointsCheck, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(newPointsCheck),"New Points Check not present");
			clickElementJSWithWait(inventory_Site);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(getObject(By.xpath("//select/option[contains(.,'" + invName + "')]")));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(inventory_Phase);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(getObject(By.xpath("//select/option[contains(.,'" + invPhase + "')]")));
			fieldDataEnter(newPointsCheck , totalPoints);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(nextVal);
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingInventrySelectionPhase", Status.PASS,
					"NewPoints entered");
	}
	
	/*
	 * Method: contractPreocessingFeesScreen Description:Contract Processing Fees Screen
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	
	public void contractPreocessingFeesScreen(){
		waitUntilElementVisibleBy(driver, headerFeesScreen, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerFeesScreen),"Fees Header not present");
			tcConfig.updateTestReporter("SalepointPage_Web", "contractPreocessingFeesScreen", Status.PASS,
					"Navigated successfully to Contract Processing Fees Screen Page");
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	/*
	 * Method: contractProcessingPriceOverride Description:Contract Processing Price Overridden
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingPriceOverride(){
		waitUntilElementVisibleBy(driver, headerPriceOverride, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerPriceOverride),"Price Override not present");
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingPriceOverride", Status.PASS,
					"Navigated successfully to Contract Processing Price Override Page");
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	/*
	 * Method: contractProcessingPaymentOption Description:Contract Processing Payment Option
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingPaymentOption(){
		waitUntilElementVisibleBy(driver, headerPaymentOption, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerPaymentOption),"Payment Option header not present");
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingPaymentOption", Status.PASS,
					"Navigated successfully to Contract Processing Payment Option Page");
			if (verifyObjectDisplayed(cashSelect)) {
				clickElementJSWithWait(cashSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(cashSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingPaymentOption", Status.FAIL,
						"Cash Select not present");
			}
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
	
	/*
	 * Method: contractProcessingMoneyScreen Description:Contract Processing Money Screen
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingMoneyScreen(){
		waitUntilElementVisibleBy(driver, headerMoneyScreen, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerMoneyScreen),"Money Screen Header not present");
			waitUntilElementVisibleBy(driver, calculateClick, "ExplicitWaitLongerTime");
			if (verifyObjectDisplayed(calculateClick)) {
				tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingMoneyScreen", Status.PASS,
						"Money Screen present");
				clickElementJSWithWait(calculateClick);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				sendKeysToElement(headerMoneyScreen, Keys.PAGE_DOWN);
				//driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(nextVal);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				acceptAlert();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingMoneyScreen", Status.PASS,
						"Money Screen present");
			} else {
				tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingMoneyScreen", Status.FAIL,
						"Money Screen not present");
			}
	}
	
	/*
	 * Method: contractProcessingMoneyScreen Description:Contract Processing Comission Screen
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingComissionScreen(){
		String salesPerson = testData.get("strSalePer");
		waitUntilElementVisibleBy(driver, headerComissionScreen, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerComissionScreen),"Comission Screen Header not present");
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingComissionScreen", Status.PASS,
					"Comission Screen present");
			waitUntilElementVisibleBy(driver, salePersonSelect, "ExplicitWaitLongerTime");
			fieldDataEnter(salePersonSelect , salesPerson);
			sendKeysToElement(salePersonSelect, Keys.TAB);
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
	}
	
	/*
	 * Method: contractProcessingMoneyScreen Description:Contract Processing Miscelaneous Information
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contrctProcessingMiscellaneousInformation(){
		waitUntilElementVisibleBy(driver, headerMiscellaneousInformation, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerMiscellaneousInformation), "Miscelleneous Info Header not present");
			tcConfig.updateTestReporter("SalepointPage_Web", "contrctProcessingMiscellaneousInformation", Status.PASS,
					"Miscellaneous Information Screen present");
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
	
	/*
	 * Method: contractProcessingContractSummary Description:Contract Processing Contract Summary
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingContractSummary(){
		waitUntilElementVisibleBy(driver, headerContractSummary, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerContractSummary),"Contract Summary Headet not present");
			waitUntilElementVisibleBy(driver, saveClick, "ExplicitWaitLongerTime");
			if (verifyObjectDisplayed(saveClick)) {
				clickElementJSWithWait(saveClick);
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingContractSummary", Status.PASS,
					"Contract Summary Page Displayed and Save Button Clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingContractSummary", Status.FAIL,
					"Failed to click on Save and Print button");

		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, printClick, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(printClick)) {
			clickElementJSWithWait(printClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingContractSummary", Status.PASS,
					"Navigated to Print PDF Page and clicked on Print Button");
		}else{
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingContractSummary", Status.FAIL,
					"Failed to click Print PDF button");
		}
	}
	
	/*
	 * Method: contractProcessingContractPrinting Description:Contract Processing Contract Printing(Get Contract and Member Number)
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingContractPrinting(){
		waitUntilElementVisibleBy(driver, headerContractPrinting, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(headerContractPrinting),"Contract Printing Header not present");
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingContractPrinting", Status.PASS,
					"Contract Printing Screen present");
		waitUntilElementVisibleBy(driver, memberNumber, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(contractNumber),"Contract Number Not Present");
			String strContractNumber = getObject(contractNumber).getText().trim();
			tcConfig.updateTestReporter("SalepointPage_Web", "contractProcessingContractPrinting", Status.PASS,"Contract Number is: "+strContractNumber);
			Assert.assertTrue(verifyObjectDisplayed(memberNumber),"Member Number Not Present");
			String strMemberNumber = getObject(memberNumber).getText().trim();
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractProcessingContractPrinting", Status.PASS,strMemberNumber);
	}
	
	/*
	 * Method: logoutSalePoint Description:Logout from Salepoint Application
	 * field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void logoutSalePoint() throws Exception {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(buttonLogout);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(loginButton)) {
				tcConfig.updateTestReporter("SalepointPage_Web", "LogoutSP", Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter("SalepointPage_Web", "LogoutSP", Status.FAIL, "Log Out Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalepointPage_Web", "LogoutSP", Status.FAIL,"Log Out Failed" + e.getMessage());
		}
	}
}