package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIMyOwnershipPage_IOS extends CUIMyOwnershipPage_Web {

	public static final Logger log = Logger.getLogger(CUIMyOwnershipPage_IOS.class);

	public CUIMyOwnershipPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By hamburgerMenu = By.xpath("//button[@class='hamburger-menu']");
	protected By myOwnershipLink=By.xpath("//section[contains(@class, 'global-account--mobile')]//span[@class='account-navigation__text' and text()='My Ownership']");
			
	/*
	 * Method: navigateToMyOwnershipPage Description:Navigate to My Ownership
	 * Page Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void navigateToMyOwnershipPage() {
		clickElementBy(hamburgerMenu);
		waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		if (verifyObjectDisplayed(myOwnershipLink)) {
			clickElementBy(myOwnershipLink);
			waitUntilElementVisibleBy(driver, myOwnershipHeader, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader),"My Ownership header not present");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.PASS,
					"User Navigated to My Ownership page");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.FAIL,
					"User failed to Navigated to My Ownership page");
		}
	}
	
	

}
