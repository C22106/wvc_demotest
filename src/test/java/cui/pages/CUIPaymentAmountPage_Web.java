package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPaymentAmountPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIPaymentAmountPage_Web.class);

	public CUIPaymentAmountPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By textPaymentAmmount = By.xpath("//h1[contains(.,'Payment Amount')]");
	protected By textAmountToPaid = By.xpath("//span[contains(text(),'amount to be paid')]");
	protected By currentBalance = By.xpath("//label[@for = 'current-balance-only']");
	protected By textAdditionalPoint = By.xpath("//a[contains(text(),'Optional Additional Payment')]");
	
	protected By dueBalance = By.xpath("//label[@for = 'total-balance-due']");

	protected By fieldAdditionalPoint = By.xpath("//input[@id = 'additional-amount']");
	protected By buttonContinue = By.xpath("(//button[text() = 'Continue'])[1]");
	protected By loadingSpinner = By.xpath("//*[local-name()='svg' and @class='spinner -secondary undefined']");
	
	/*
	 * Method: validateHeaderPaymentAmount Description:validate Header Payment Amount
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHeaderPaymentAmount(){
		waitUntilElementVisibleBy(driver, currentBalance, "ExplicitLongWait");
		elementPresenceVal(textPaymentAmmount, "Header Payment Amount", "CUIPaymentAmountPage", "validateHeaderPaymentAmount");
		elementPresenceVal(textAmountToPaid, "Text: "+getElementText(textAmountToPaid), "CUIPaymentAmountPage", "validateHeaderPaymentAmount");
	}
	
	/*
	 * Method: selectCheckBoxCurrentBalance Description:select CheckBox Current Balance
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCheckBoxCurrentBalance(){
		if (verifyObjectDisplayed(currentBalance)) {
			clickElementJSWithWait(currentBalance);
			tcConfig.updateTestReporter("CUIPaymentAmountPage", "selectCheckBoxCurrentBalance", Status.PASS,
					"Text Displayed as: "+getElementText(currentBalance));
			
		}else if (verifyObjectDisplayed(dueBalance)) {
			clickElementJSWithWait(dueBalance);
			tcConfig.updateTestReporter("CUIPaymentAmountPage", "selectCheckBoxCurrentBalance", Status.PASS,
					"Due Balance Present");
		}
	}

	/*
	 * Method: addAdditionalPoint Description:add Additional Point
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void addAdditionalPoint(){
		String additionalAmount = testData.get("AdditionalAmount");
		clickElementJSWithWait(textAdditionalPoint);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldAdditionalPoint)) {
			getElementInView(fieldAdditionalPoint);
			clickElementJSWithWait(fieldAdditionalPoint);
			sendKeysBy(fieldAdditionalPoint, additionalAmount);
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}	
	}
	
	/*
	 * Method: clickContinueButton Description:click Continue Button
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void clickContinueButton(){
		if (verifyObjectDisplayed(buttonContinue)) {
			getElementInView(buttonContinue);
			clickElementJSWithWait(buttonContinue);
		}
		
	}
}
