package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIModifyTravelerPage_Android extends CUIModifyTravelerPage_Web {

	public static final Logger log = Logger.getLogger(CUIModifyTravelerPage_Android.class);

	public CUIModifyTravelerPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		stepsModify = By.xpath("//div[@class = 'booking-header__mobile']");
	}

	CUIModifyReservationPage_Android modifyPage_Android = new CUIModifyReservationPage_Android(tcConfig);
	/*
	 * Method: validateSectionReservationSummary Description:validate Section Reservation Summary Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateSectionReservationSummary(){
		modifyPage_Android.checkSectionReservationSummary();
		modifyPage_Android.checkReservationSummaryDetails();
	}
	
	/*
	 * Method: dataEntryForGuestOwner Description:data Entry For Guest Owner
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryForGuestOwner() {
		String guestFirstName = testData.get("guestFirstName");
		String guestLastName = testData.get("guestLastName");
		String guestEmail = testData.get("guestEmail");
		//String guestSelectFlag = testData.get("guestSelectFlag");
		String guestPhoneNumber = testData.get("guestPhoneNumber");
		String guestCountry = testData.get("guestCountry");
		String guestStreetAddress = testData.get("guestStreetAddress");
		String guestCity = testData.get("guestCity");
		String guestState = testData.get("guestState");
		String guestZipCode = testData.get("guestZipCode");
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldGuestFirstName)) {
			getElementInView(fieldGuestFirstName);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name*", guestFirstName, 1);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name*", guestLastName, 1);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address*", guestEmail, 1);
			
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Phone Number", guestPhoneNumber, 1);
			selectByText(fieldGuestCountry, guestCountry);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Street Address*", guestStreetAddress, 1);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "City*", guestCity, 1);

			selectByText(fieldGuestState, guestState);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Zip Code*", guestZipCode, 1);
	
			clickElementJSWithWait(guestAgree);
		}else{
			tcConfig.updateTestReporter("CUIModifyTravelPage", "dataEntryForGuestOwner", Status.FAIL,
					"Fields for Guest data Entry not displayed");
		}
	}
	
	/*
	 * Method: textStepModifyTraveler Description:text Step Modify Traveler Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textStepModifyTraveler(){
		String[] steps=getElementText(stepsModify).split(" ");
		if (getElementText(headerModifyTraveler).contains(steps[1].trim())) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "textStepModifyTraveler", Status.PASS,
					"Step Modify Traveler Present");
		}else{
			tcConfig.updateTestReporter("CUIModifyTravelPage", "textStepModifyTraveler", Status.FAIL,
					"Step Modify Traveler not Present");
		}
	}
	
}
