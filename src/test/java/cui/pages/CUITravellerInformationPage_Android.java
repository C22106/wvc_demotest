package cui.pages;

import org.apache.log4j.Logger;

import automation.core.TestConfig;

public class CUITravellerInformationPage_Android extends CUITravellerInformationPage_Web {

	public static final Logger log = Logger.getLogger(CUITravellerInformationPage_Android.class);

	public CUITravellerInformationPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}

	CUITravellerInformationPage_IOS travellerInformationIOSPage=new CUITravellerInformationPage_IOS(tcConfig) ;
	
	/*
	 * Method: fillTravellerInformation Description:Validate and fill Traveller
	 * Information Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * KG
	 */
	public void fillTravellerInformation() {
		travellerInformationIOSPage.fillTravellerInformation();
	}
	
	/*
	 * Method: selectAndPopulateGuestInfo Description:selectAndPopulateGuestInfo
	 * Date: June/2020 Author: Monideep Roychowdhury Roy Changes By: KG
	 */
	public void selectAndPopulateGuestInfo() {
		travellerInformationIOSPage.selectAndPopulateGuestInfo();
	}
}