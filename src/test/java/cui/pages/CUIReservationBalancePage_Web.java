package cui.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIReservationBalancePage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIReservationBalancePage_Web.class);

	public CUIReservationBalancePage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	CUIBookPage_Web bookPage = new CUIBookPage_Web(tcConfig);
	protected By currentUseYearHKAvailable = By.xpath(
			"//th[text() = 'HOUSEKEEPING CREDITS']/../../..//td[text() = 'Available Credits']/following-sibling::td");
	protected By rentPointsChecked = By.xpath("//label[@for='rentPoints' and contains(.,'I want to rent')]");
	protected By creditToBePurchaseTextBx = By.id("creditsToPurchaseInput");
	protected By creditToBeBorrowedTextBx = By.id("creditsToBorrowInput");
	protected By amountNeeded = By.xpath("//*[@class='remaining-balance']/td[2]");
	protected By rentPointsChkBox = By.xpath("//*[@for='rentPoints']");
	protected By pointsRentInput = By.id("pointsToRentInput");
	protected By reservationBalance = By.className("reservation-balance");
	protected By applyPointsBtn = By.xpath("//button[@name='pointsToRentInput']");
	protected By pointsAppliedBtn = By.xpath("//button[@name='pointsToRent']");
	protected By buttonBookingContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By removeButtonPointsToBorrow = By.xpath("//button[@name='pointsToBorrow']");
	protected By removeButtonPointsToRent = By.xpath("//button[@name='pointsToRent']");
	protected By removeButtonCreditsToBorrow = By.xpath("//button[@name='creditsToBorrow']");
	protected By removeButtonCreditsToPurchase = By.xpath("//button[@name='creditsToPurchase']");
	protected By headerReservationBalance = By.xpath(
			"//h1[contains(.,'Reservation Balance')] | //div[@class = 'steps__title' and contains(.,'Reservation Balance')]");
	protected By headerBorrowRentPoint = By.xpath("//h2[contains(.,'Borrow and/or Rent Points')]");
	protected By headerHousekeepingCredits = By
			.xpath("//h2[contains(.,'Borrow and/or Purchase Housekeeping Credits')]");
	protected By headerPurchaseReservationTransaction = By
			.xpath("//h2[contains(.,'Purchase Reservation Transaction')]");
	protected By headerReservationTransaction = By.xpath("//th[contains(.,'RESERVATION TRANSACTIONS')]");
	protected By headerReservationPoints = By.xpath("//th[contains(.,'RESERVATION POINTS')]");
	protected By ratelabelReservationTransaction = By.xpath("//span[text()='Rate per reservation']");
	protected By ratevalueReservationTransaction = By
			.xpath("//span[text()='Rate per reservation']//following-sibling::span");

	protected By backButtonToPreviuosPage = By.xpath("//*[@class='booking__footer']//a[contains(text(),'Back to')]");
	protected By bookingHeader = By.xpath("//div[@class='booking-header__desktop']//li/span");
	protected By housekeepingCreditHeader = By.xpath("//th[contains(.,'HOUSEKEEPING CREDITS')]");
	protected By purchaseHousekeepingCreditHeader = By
			.xpath("//h2[text()='Purchase Housekeeping Credits'] | //div[text()='Purchase Housekeeping Credits']");
	protected By creditAvailableLabel = By.xpath("//span[contains(.,'Credits Available')]");
	protected By errorMsgOnApplyingMoreValue = By.xpath("//p[@class='input-error']");
	protected By applyButtonCreditToBePurchase = By.xpath("//button[@name='creditsToPurchaseInput']");
	protected By applyButtonCreditToBeBorrowed = By.xpath("//button[@name='creditsToBorrowInput']");
	protected By creditAvailableValue = By.xpath("//span[contains(.,'Credits Available')]/following-sibling::span");
	protected By purchasingPoint_ReservationTransaction = By.xpath(
			"//th[contains(text(),'TRANSACTIONS')]/ancestor::table//td[contains(text(),'Purchasing')]//following-sibling::td");
	protected By amountNeededPoint_ReservationTransaction = By.xpath(
			"//th[contains(text(),'TRANSACTIONS')]/ancestor::table//td[contains(text(),'Amount')]//following-sibling::td");
	protected By messageForApplyingMorePoint = By.xpath("//span[contains(text(),'The amount you have entered')]");
	protected By amountNeededPoint_ReservationPoint = By.xpath(
			"//th[contains(text(),'POINTS')]/ancestor::table//td[contains(text(),'Amount')]//following-sibling::td");
	protected By borrowingPoint_ReservationPoint = By.xpath(
			"//th[contains(text(),'POINTS')]/ancestor::table//td[contains(text(),'Borrowing')]//following-sibling::td");
	protected By rentingPoint_ReservationPoint = By.xpath(
			"//th[contains(text(),'POINTS')]/ancestor::table//td[contains(text(),'Renting')]//following-sibling::td");
	protected By InstructionText_ReservationPoint = By.xpath(
			"//h4[contains(@class,'subtitle-3') and text()='Select how you would like to cover your remaining points balance:']");
	protected By InstructionText_HousekeepingCredits = By.xpath(
			"//div[contains(@class,'subtitle-3') and contains(text(),'purchase credits to cover your housekeeping credits')]");
	protected By InstructionText_ReservationTransactions = By.xpath(
			"//h4[contains(@class,'subtitle-3') and text()='Confirm that you would like to purchase a reservation transaction to complete your transaction:']");
	protected By nextUserYear = By.xpath("//span[contains(text(),'Next Use Year')]");
	protected By nextUserYearValue = By.xpath("//span[contains(text(),'Next Use Year')]//following-sibling::span");
	protected By pointAvailableText = By.xpath("//span[contains(text(),'Points Available')]");
	protected By pointAvailableValue = By.xpath("//span[contains(text(),'Points Available')]//following-sibling::span");
	protected By borrowCheckBox_ReservationPoints = By.xpath("//*[@id='borrow-points']/../label");
	protected By rentCheckBox_ReservationPoints = By.xpath("//*[@id='rentPoints']/../label");
	protected By purchaseResTranCheckBox = By.xpath("//*[@id='purchase-res-tran']/../label");
	protected By purchaseHouseKeeping = By.xpath("//*[@id='cleansToPurchaseInput']/../label");
	protected By borrowCheckBox_HousekeepingCredit = By.xpath("//*[@id='borrow-credits']/../label");
	protected By purchaseCheckBox_HousekeepingCredit = By.xpath("//*[@id='purchaseCredits']/../label");
	protected By borrowingPoint_HouseKeepingCredit = By.xpath(
			"//th[contains(text(),'HOUSEKEEPING')]/ancestor::table//td[contains(text(),'Borrowing')]//following-sibling::td");
	protected By amountNeededPoint_HouseKeepingCredit = By.xpath(
			"//th[contains(text(),'HOUSEKEEPING')]/ancestor::table//td[contains(text(),'Amount')]//following-sibling::td");
	protected By purchasingPoint_HouseKeepingCredit = By.xpath(
			"//th[contains(text(),'HOUSEKEEPING')]/ancestor::table//td[contains(text(),'Purchasing')]//following-sibling::td");

	protected By houseKeepingCreditCost = By
			.xpath("//section[contains(.,'Housekeeping Credits')]//tr[td[.='Required to Book']]/td[2]");
	protected By houseKeepingCreditAvailable = By
			.xpath("//section[contains(.,'Housekeeping Credits')]//tr[td[.='Available Credits']]/td[2]");
	protected By houseKeepingCreditRemaining = By
			.xpath("//section[contains(.,'Housekeeping Credits')]//tr[@class='remaining-balance']/td[2]");
	protected By borrowToolTip_ReservationPoints = By.xpath("//span[@id='borrow-points-tooltip']");
	protected By borrowToolTipPresence_ReservationPoints = By
			.xpath("//span[contains(text(),'The option to borrow')]/parent::div");
	protected By rentToolTip_ReservationPoints = By.xpath("//span[@id='rent-points-tooltip']");
	protected By rentToolTipPresence_ReservationPoints = By
			.xpath("//span[contains(text(),'If you need more points')]/parent::div");
	protected By rentToolTipText_ReservationPoints = By.xpath("//span[contains(text(),'If you need more points')]");
	protected By borrowToolTipText_ReservationPoints = By.xpath("//span[contains(text(),'The option to borrow')]");
	protected By purchaseToolTip_ReservationTransaction = By.xpath("//span[@id='purchase-res-tran-tooltip']");
	protected By borrowToolTip_HousekeepingCredit = By.xpath("//span[@id='borrow-credits-tooltip']");
	protected By purchaseToolTip_HousekeepingCredit = By.xpath("//span[@id='purchase-cleans-tooltip']");
	protected By purchaseToolTipPresence_ReservationTransaction = By
			.xpath("//span[contains(text(),'A Reservation Transaction')]/parent::div");
	protected By purchaseToolTipText_ReservationTransaction = By
			.xpath("//span[contains(text(),'A Reservation Transaction')]");
	protected By purchaseCreditToolTipText = By
			.xpath("//span[contains(text(),'When you do not have enough credits available, you may')]");
	protected By purchaseCreditToolTipTextPresence = By
			.xpath("//span[contains(text(),'When you do not have enough credits available, you may')]/parent::div");
	protected By borrowCreditToolTipText = By
			.xpath("//span[contains(text(),'When you do not have enough credits available, they may')]");
	protected By borrowCreditToolTipTextPresence = By
			.xpath("//span[contains(text(),'When you do not have enough credits available, they may')]/parent::div");
	protected By pointToBeBorrowedTextBx = By.id("pointsToBorrowInput");
	protected By applyButtonPointToBeBorrowed = By.xpath("//button[@name='pointsToBorrowInput']");
	protected By applyButtonPointToBeRented = By.xpath("//button[@name='pointsToRentInput']");
	protected By rentPointsTotalCost = By.xpath("//span[contains(text(),'Rent Total')]/following-sibling::span");
	protected By rentPointNoteText = By.xpath("//span[contains(text(),'Rate per 1,000 points')]");
	protected By rentPointNoteTextValue = By
			.xpath("//span[contains(text(),'Rate per 1,000 points')]/following-sibling::span");
	protected By purchaseCreditNoteText = By.xpath("//span[contains(text(),'Rate per credit')]");
	protected By purchaseCreditTextValue = By
			.xpath("//span[contains(text(),'Rate per credit')]/following-sibling::span");
	protected By purchaseCredit = By.xpath("//label[@for='purchaseCredits']");
	protected By houseKeepingPointsToBePurchasedTextBx = By.xpath("//input[@id='creditsToPurchaseInput']");
	protected By applyhouseKeepingPointsPurchaseBtn = By.xpath("//button[.='Apply']");
	protected By appliedhouseKeepingPointsPurchaseBtn = By.name("creditsToPurchase");

	protected By reservationTransactionChkbox = By.xpath("//label[@for='purchase-res-tran']");

	protected By purchaseHouseKeepingCost = By.xpath(
			"//section[contains(.,'housekeeping credits balance')]//span[.='Total Cost']/following-sibling::span");
	protected By totalRentedPrice = By.xpath("//span[text()='Rent Total']/following-sibling::span");

	protected By useYearPoints = By.xpath("//td[text()='Available Points Balance']/following-sibling::td");
	public String strAmountNeededReservationTransaction;
	protected String amountNeededPts;
	protected static String pointRented;
	protected static String pointRentedPrice;
	protected static String pointBorrowed;
	public static int availableHKCurrentYear;
	public static int purchasedHK;

	// updated
	protected By textTravelInfo = By.xpath("//div[contains(@class,'steps__title') and contains(.,'Traveler Info')]");

	/*
	 * Method: validateReservationBalancePage Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Monideep
	 * Changes By: Kamalesh (6/14/2020)
	 */
	public void validateReservationBalancePage() {
		waitUntilElementVisibleBy(driver, reservationBalance, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(reservationBalance), "Reservation Balance Page not visible");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "validateReservationBalancePage", Status.PASS,
				"Navigated to Reservation Balance page successfully");

		List<String> headerValue = getListString(bookingHeader);
		Assert.assertTrue((headerValue.contains("RESERVATION BALANCE") || headerValue.contains("Reservation Balance")),
				"Progress bar does not contains - RESERVATION BALANCE");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "checkProgressBarforReservationBalance",
				Status.PASS, "Progress bar contains - RESERVATION BALANCE");
	}

	/*
	 * Method: retrieveAmountNeeded Description:Validate Borrow Rent Point is
	 * Not displayed Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void retrieveAmountNeeded() {
		Assert.assertTrue(verifyObjectDisplayed(amountNeeded), "Amount needed not present");
		amountNeededPts = getElementText(amountNeeded).replace(",", "");
		tcConfig.getTestData().put("RentedPoints", amountNeededPts);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "retrieveAmountNeeded", Status.PASS,
				"Retrieved amount needed for reservation successfully, points required - " + amountNeededPts);
	}

	/*
	 * Method: selectRentPoints Description:Validate Date field Date: June/2020
	 * Author: Monideep Changes By: NA
	 */
	public void selectRentPoints() {

		if (verifyObjectDisplayed(rentPointsChkBox)) {
			clickElementBy(rentPointsChkBox);
			assertTrue(verifyObjectDisplayed(rentPointsChecked), "Rent points check box not checked");
			tcConfig.updateTestReporter("CUIReservationBalancePage", "selectRentPoints", Status.PASS,
					"Rent point option has been checked");
			tcConfig.getTestData().put("RentedPointsCost", getElementText(rentPointsTotalCost).trim());
			tcConfig.getTestData().put("RentedPoints", amountNeededPts);
			tcConfig.getTestData().put("RentPoint", "YES");
		}

	}

	/*
	 * Method: selectAndUpdateRentPoints Description:Validate Borrow Rent Point
	 * is Not displayed Date field Date: June/2020 Author: Monideep Changes By:
	 * NA
	 */
	public void selectAndUpdateRentPoints() {
		clickElementBy(rentPointsChkBox);
		waitUntilElementVisibleBy(driver, pointsRentInput, "ExplicitLowWait");
		tcConfig.getTestData().put("RentedPoints", amountNeededPts);
		int availablePoints = Integer.parseInt(getElementText(useYearPoints).replace(",", ""));
		tcConfig.getTestData().put("AccountPoints", availablePoints + "");
		sendKeysBy(pointsRentInput, amountNeededPts);
		sendKeysToElement(pointsRentInput, Keys.TAB);
		sendKeysToElement(applyPointsBtn, Keys.RETURN);
		waitUntilElementVisibleBy(driver, pointsAppliedBtn, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(pointsAppliedBtn), "Points Applied button not present");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectAndUpdateRentPoints", Status.PASS,
				"Rented points applied successfully");
		tcConfig.getTestData().put("RentedCost", getElementText(rentPointsTotalCost).trim());
	}

	/*
	 * Method: clickContinueBooking Description:Validate Borrow Rent Point is
	 * Not displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void clickContinueBooking() {
		clickContinueButton(buttonBookingContinue, "CUIReservationBalancePage");
	}

	/*
	 * Method: reservationBalanceNotDisplayed Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void reservationBalancePageNotDisplayed() {
		List<String> headerValue = getListString(bookingHeader);

		assertFalse(headerValue.contains("RESERVATION BALANCE"), "Progress bar contains - RESERVATION BALANCE");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "checkProgressBarforReservationBalance",
				Status.PASS, "Progress bar does not contains - RESERVATION BALANCE");

		assertFalse(verifyObjectDisplayed(headerReservationBalance), "Reservation Balance Header is displayed");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "checkBorrowRentPointNotPresent", Status.PASS,
				"Reservation Balance Header is not displayed");

		assertFalse(verifyObjectDisplayed(headerBorrowRentPoint), "Borrow/Rent Points section is displayed ");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "checkBorrowRentPointNotPresent", Status.PASS,
				"Borrow/Rent Points section is not displayed");

		assertFalse(verifyObjectDisplayed(headerPurchaseReservationTransaction),
				"Purchase Reservation Transaction section is displayed");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "checkPurchaseReservationTransactionNotPresent",
				Status.PASS, "Purchase Reservation Transaction section is not displayed");

		assertFalse(verifyObjectDisplayed(headerHousekeepingCredits), "House keeping Credits section is displayed");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "checkHousekeepingCreditsPresent", Status.PASS,
				"House keeping Credits section is not displayed");

	}

	/*
	 * Method: backToPreviuosPage Description:Navigating back to Previuos page
	 * Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void backToPreviuosPage() {
		Assert.assertTrue(verifyObjectDisplayed(backButtonToPreviuosPage));
		getElementInView(backButtonToPreviuosPage);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "backToPreviuosPage", Status.PASS,
				"Back button present");
		clickElementBy(backButtonToPreviuosPage);
		waitUntilElementVisibleBy(driver, textTravelInfo, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textTravelInfo), "Traveler info Text not present");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "backToPreviuosPage", Status.PASS,
				"User navigated to previuos page");
	}

	/*
	 * Method: verifyPurchaseReservationTransaction Description:Validate Borrow
	 * Rent Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */

	public void verifyPurchaseReservationTransaction() {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerPurchaseReservationTransaction),
				"Header Purchase Reservation transaction not present");
		getElementInView(headerPurchaseReservationTransaction);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseReservationTransaction", Status.PASS,
				"Purchase Reservation Transaction Header is displayed");
		Assert.assertTrue(verifyObjectDisplayed(headerReservationTransaction),
				"Reservation Transaction Header not present");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseReservationTransaction", Status.PASS,
				"Reservation Transaction Subheader is displayed");
	}

	/*
	 * Method: verifyBorrowRentPoints Description:Validate Borrow Rent Point is
	 * Not displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */

	public void verifyBorrowRentPointsTitle() {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerBorrowRentPoint), "Borrow Rent Header not present");
		getElementInView(headerBorrowRentPoint);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseReservationTransaction", Status.PASS,
				"Borrow Rent Point Header is displayed");
		Assert.assertTrue(verifyObjectDisplayed(headerReservationPoints));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseReservationTransaction", Status.PASS,
				"Reservation Points Subheader is displayed");
	}

	/*
	 * Method: verifyBorrowPurchaseHousekeepingCreditTitle Description:Validate
	 * Borrow Rent Point is Not displayed Date field Date: June/2020 Author:
	 * Kamalesh Changes By: NA
	 */

	public void verifyPurchaseHousekeepingCreditTitle() {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(purchaseHousekeepingCreditHeader));
		getElementInView(purchaseHousekeepingCreditHeader);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseHousekeepingCreditTitle",
				Status.PASS, "Purchase Housekeeping Credit Header is displayed");
		Assert.assertTrue(verifyObjectDisplayed(housekeepingCreditHeader));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseHousekeepingCreditTitle",
				Status.PASS, "Housekeeping Credit Subheader is displayed");
	}

	/*
	 * Method: validateFieldLabelAndValue Description:Validate Borrow Rent Point
	 * is Not displayed Date field Date: June/2020 Author: Kamalesh Changes By:
	 * NA
	 */

	public void validateFieldLabelAndValue(String subheader, String Label) {
		HashMap<String, String> hmap = new HashMap<String, String>();
		hmap.put("POINTS", "Reservation Points");
		hmap.put("HOUSEKEEPING", "Housekeeping Credits");
		hmap.put("TRANSACTIONS", "Reservation Transactions");
		hmap.put("Required", "Required to Book");
		hmap.put("Available", "Available Points Balance");
		hmap.put("Amount", "Amount Needed");
		hmap.put("Borrowing", "Borrowing");
		hmap.put("Renting", "Renting");
		hmap.put("Credits", "Available Credits");
		hmap.put("Purchasing", "Purchasing");

		if (verifyObjectDisplayed(By.xpath(
				"//th[contains(text(),'" + subheader + "')]/ancestor::table//td[contains(text(),'" + Label + "')]"))) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "validateFieldLabelAndValue", Status.PASS,
					hmap.get(Label) + " label is displayed for section - " + hmap.get(subheader));
			if (verifyObjectDisplayed(By.xpath("//th[contains(text(),'" + subheader
					+ "')]/ancestor::table//td[contains(text(),'" + Label + "')]//following-sibling::td"))) {
				tcConfig.updateTestReporter("CUIReservationBalancePage", "validateFieldLabelAndValue", Status.PASS,
						"Value for " + hmap.get(Label) + " is displayed");
			} else {
				tcConfig.updateTestReporter("CUIReservationBalancePage", "validateFieldLabelAndValue", Status.FAIL,
						"Value for " + hmap.get(Label) + " is not displayed");
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "validateFieldLabelAndValue", Status.FAIL,
					hmap.get(Label) + " label is not displayed for section - " + hmap.get(subheader));
		}
	}

	/*
	 * Method: verifyInstructionText Description:Validate Borrow Rent Point is
	 * Not displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyInstructionText(String subheader) {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		switch (subheader) {

		case "ReservationPoint":
			Assert.assertTrue(verifyObjectDisplayed(InstructionText_ReservationPoint));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyInstructionText", Status.PASS,
					"Instruction Text is displayed successfully under Reservation Point section");
			break;

		case "HousekeepingCredits":
			Assert.assertTrue(verifyObjectDisplayed(InstructionText_HousekeepingCredits));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyInstructionText", Status.PASS,
					"Instruction Text is displayed successfully under Housekeeping Credits section");
			break;

		case "ReservationTransactions":
			Assert.assertTrue(verifyObjectDisplayed(InstructionText_ReservationTransactions));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyInstructionText", Status.PASS,
					"Instruction Text is displayed successfully under Reservation Transactions section");
			break;

		}
	}

	/*
	 * Method: selectCheckBox_ReservationBalanace Description:Validate Borrow
	 * Rent Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */

	public void selectCheckBox_ReservationBalanace(String checkBx) {
		HashMap<String, By> hmap = new HashMap<String, By>();
		hmap.put("BorrowPoint", borrowCheckBox_ReservationPoints);
		hmap.put("RentPoint", rentCheckBox_ReservationPoints);
		hmap.put("PurchaseReservationTransaction", purchaseResTranCheckBox);
		hmap.put("BorrowCredit", borrowCheckBox_HousekeepingCredit);
		hmap.put("PurchaseCredit", purchaseCheckBox_HousekeepingCredit);
		Assert.assertTrue(verifyObjectDisplayed(hmap.get(checkBx)));
		getElementInView(hmap.get(checkBx));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(hmap.get(checkBx));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectBorrowCheckBox", Status.PASS,
				checkBx + " is displayed and it is selected");

	}

	/*
	 * Method: verifyToolTip Description:Validate Borrow Rent Point is Not
	 * displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyToolTip(String subheader) {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		switch (subheader) {

		case "Borrow-ReservationPoint":

			Assert.assertTrue(verifyObjectDisplayed(borrowToolTip_ReservationPoints));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyToolTip", Status.PASS,
					"Borrow Tool Tip Text is present under Reservation Point Section");
			break;

		case "Rent-ReservationPoint":

			Assert.assertTrue(verifyObjectDisplayed(rentToolTip_ReservationPoints));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyToolTip", Status.PASS,
					"Rent Tool Tip Text is present under Reservation Point Section");
			break;

		case "Purchase-ReservationTransaction":

			Assert.assertTrue(verifyObjectDisplayed(purchaseToolTip_ReservationTransaction));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyToolTip", Status.PASS,
					"Borrow Tool Tip Text is present under Reservation Transaction Section");
			break;

		case "Borrow-HousekeepingCredit":
			Assert.assertTrue(verifyObjectDisplayed(borrowToolTip_HousekeepingCredit));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyToolTip", Status.PASS,
					"Borrow Tool Tip Text is present under Reservation Transaction Section");
			break;

		case "Purchase-HousekeepingCredit":
			Assert.assertTrue(verifyObjectDisplayed(purchaseToolTip_HousekeepingCredit));
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyToolTip", Status.PASS,
					"Borrow Tool Tip Text is present under Reservation Transaction Section");
			break;
		}
	}

	/*
	 * Method: verifyNextUseYear Description:Validate Borrow Rent Point is Not
	 * displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyNextUseYear() {
		Assert.assertTrue(verifyObjectDisplayed(nextUserYear));
		getElementInView(nextUserYear);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyNextUseYear", Status.PASS,
				"Next Use Year label is present for Reservation Point");
		Assert.assertTrue(verifyObjectDisplayed(nextUserYearValue));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyNextUseYear", Status.PASS,
				"Next Use Year Value is present for Reservation Point");

		boolean flag = isValidDate(getElementText(nextUserYearValue).replace("/", "-"), "MM-dd-yy");
		if (flag) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyNextUseYear", Status.PASS,
					"Format for Next Use Year is MM-dd-yy");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyNextUseYear", Status.FAIL,
					"Format for Next Use Year is not MM-dd-yy");
		}

	}

	/*
	 * Method: verifyPointAvailable Description:Validate Borrow Rent Point is
	 * Not displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyPointAvailable() {
		Assert.assertTrue(verifyObjectDisplayed(pointAvailableText));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPointAvailable", Status.PASS,
				"Point available label is present for Reservation Point");
		Assert.assertTrue(verifyObjectDisplayed(pointAvailableValue));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPointAvailable", Status.PASS,
				"Point is available");

	}

	/*
	 * Method: verifyCreditAvailable Description:Validate Borrow Rent Point is
	 * Not displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyCreditAvailable() {
		Assert.assertTrue(verifyObjectDisplayed(creditAvailableLabel));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyCreditAvailable", Status.PASS,
				"Credit available label is present for Housekeeping Credit");
		Assert.assertTrue(verifyObjectDisplayed(creditAvailableValue));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyCreditAvailable", Status.PASS,
				"Credit value is available");

	}

	/*
	 * Method: FeildLevelValidationPointToBeBorrowed Description:Validate Borrow
	 * Rent Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void FeildLevelValidationPointToBeBorrowed() {
		textBoxAndPlaceHolderValidation("Point to be Borrowed", pointToBeBorrowedTextBx, "pointsToBorrowInput");

		if (getElementAttribute(pointToBeBorrowedTextBx, "type").equalsIgnoreCase("number")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "FeildLevelValidationPointToBeBorrowed",
					Status.PASS, "Point To Be Borrowed Text Box only accept numeric value");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "FeildLevelValidationPointToBeBorrowed",
					Status.FAIL, "Point To Be Borrowed Text Box does not accept value other numeric also");
		}
		Assert.assertFalse(verifyObjectEnabled(applyButtonPointToBeBorrowed),
				"Apply Button is enable without entering any value");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "FeildLevelValidationPointToBeBorrowed",
				Status.PASS, "Apply Button remains disable when no value is entered ihe text box");

	}

	/*
	 * Method: FeildLevelValidationPointToBeCredited Description:Validate Borrow
	 * Rent Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void FeildLevelValidationCreditToBeBorrowed() {
		textBoxAndPlaceHolderValidation("Credits to be Borrowed", creditToBeBorrowedTextBx, "creditsToBorrowInput");

		if (getElementAttribute(creditToBeBorrowedTextBx, "type").equalsIgnoreCase("number")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "FeildLevelValidationCreditToBeBorrowed",
					Status.PASS, "Credit To Be Borrowed Text Box only accept numeric value");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "FeildLevelValidationCreditToBeBorrowed",
					Status.FAIL, "Credit To Be Borrowed Text Box does not accept value other numeric also");
		}
		Assert.assertFalse(verifyObjectEnabled(applyButtonCreditToBeBorrowed),
				"Apply Button is enable without entering any value");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "FeildLevelValidationCreditToBeBorrowed",
				Status.PASS,
				"Apply Button remains disable when no value is entered in the Credit To Be Borrowed text box");

	}

	/*
	 * Method: applyForMorePointThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void applyForMorePointThanAvailable(String value) {
		String strAmountNeeded = getElementText(amountNeededPoint_ReservationPoint).replace(",", "");
		if (value.equalsIgnoreCase("Borrow")) {
			sendKeysBy(pointToBeBorrowedTextBx, String.valueOf(Integer.parseInt(strAmountNeeded) + 10));
			clickElementBy(applyButtonPointToBeBorrowed);
		} else if (value.equalsIgnoreCase("Rent")) {
			sendKeysBy(pointsRentInput, String.valueOf(Integer.parseInt(strAmountNeeded) + 10));
			clickElementBy(applyButtonPointToBeRented);
		}
		Assert.assertTrue(verifyObjectDisplayed(messageForApplyingMorePoint)
				|| verifyObjectDisplayed(errorMsgOnApplyingMoreValue));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "applyForMorePointThanAvailable", Status.PASS,
				"Warning/Error Message is displayed when user apply more points then available");
	}

	/*
	 * Method: applyForMoreCreditThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void applyForMoreCreditThanAvailable(String value) {
		String strAmountNeeded = getElementText(amountNeededPoint_HouseKeepingCredit).replace(",", "");
		if (value.equalsIgnoreCase("Borrow")) {
			sendKeysBy(creditToBeBorrowedTextBx, String.valueOf(Integer.parseInt(strAmountNeeded) + 10));
			clickElementBy(applyButtonCreditToBeBorrowed);
		} else if (value.equalsIgnoreCase("Purchase")) {
			sendKeysBy(creditToBePurchaseTextBx, String.valueOf(Integer.parseInt(strAmountNeeded) + 10));
			clickElementBy(applyButtonCreditToBePurchase);
		}
		Assert.assertTrue(verifyObjectDisplayed(messageForApplyingMorePoint)
				|| verifyObjectDisplayed(errorMsgOnApplyingMoreValue));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "applyForMorePointThanAvailable", Status.PASS,
				"Warning/Error Message is displayed when user apply more points then available");
	}

	/*
	 * Method: applyForMorePointThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void borrowValidPoint() {
		String strAmountNeeded = getElementText(amountNeededPoint_ReservationPoint);
		String strAmountNeededUpdate = strAmountNeeded.replace(",", "");
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = cap.getBrowserName().toUpperCase();
		if (browserName.contains("EDGE")) {
			sendkeysForEdge(pointToBeBorrowedTextBx, strAmountNeededUpdate);
		} else {
			sendKeysBy(pointToBeBorrowedTextBx, strAmountNeededUpdate);
		}
		clickElementBy(applyButtonPointToBeBorrowed);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.PASS,
				"Applied for Borrowing the Point");
		getElementInView(borrowingPoint_ReservationPoint);
		if (getElementText(borrowingPoint_ReservationPoint).equalsIgnoreCase(strAmountNeeded)
				&& getElementText(amountNeededPoint_ReservationPoint).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.PASS,
					"Amount borrowed and Borrowing Value and Amount Needed value updated successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.FAIL,
					"Error while applying for borrow point");
		}
		pointBorrowed = getElementText(borrowingPoint_ReservationPoint).trim();

	}

	/*
	 * Method: applyForMorePointThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void rentValidPoint() {
		String strAmountNeeded = getElementText(amountNeededPoint_ReservationPoint);
		String strAmountNeededUpdate = strAmountNeeded.replace(",", "");
		sendKeysBy(pointsRentInput, strAmountNeededUpdate);
		clickElementBy(applyButtonPointToBeRented);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.PASS,
				"Applied for Borrowing the Point");
		getElementInView(rentingPoint_ReservationPoint);
		if (getElementText(rentingPoint_ReservationPoint).equalsIgnoreCase(strAmountNeeded)
				&& getElementText(amountNeededPoint_ReservationPoint).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.PASS,
					"Amount rented and Renting Value and Amount Needed value updated successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.FAIL,
					"Error while applying for rent point");
		}
		pointRented = getElementText(rentingPoint_ReservationPoint).trim();
		pointRentedPrice = getElementText(totalRentedPrice).trim();
	}

	/*
	 * Method: applyForMorePointThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void borrowValidCredit() {
		String strAmountNeeded = getElementText(amountNeededPoint_HouseKeepingCredit);
		String strAmountNeededUpdate = strAmountNeeded.replace(",", "");
		sendKeysBy(creditToBeBorrowedTextBx, strAmountNeededUpdate);
		clickElementBy(applyButtonCreditToBeBorrowed);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidCredit", Status.PASS,
				"Applied for Borrowing the Credit");
		getElementInView(borrowingPoint_HouseKeepingCredit);
		if (getElementText(borrowingPoint_HouseKeepingCredit).equalsIgnoreCase(strAmountNeeded)
				&& getElementText(amountNeededPoint_HouseKeepingCredit).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidCredit", Status.PASS,
					"Credit borrowed and Borrowing Value and Amount Needed value updated successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidCredit", Status.FAIL,
					"Error while applying for borrow credit");
		}
	}

	/*
	 * Method: applyForMorePointThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void purchaseValidCredit() {
		String strAmountNeeded = getElementText(amountNeededPoint_HouseKeepingCredit);
		String strAmountNeededUpdate = strAmountNeeded.replace(",", "");
		sendKeysBy(creditToBePurchaseTextBx, strAmountNeededUpdate);
		clickElementBy(applyButtonCreditToBePurchase);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidCredit", Status.PASS,
				"Applied for Purchasing the Credit");
		getElementInView(purchasingPoint_HouseKeepingCredit);
		if (getElementText(purchasingPoint_HouseKeepingCredit).equalsIgnoreCase(strAmountNeeded)
				&& getElementText(amountNeededPoint_HouseKeepingCredit).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidCredit", Status.PASS,
					"Credit Purchased and Borrowing Value and Amount Needed value updated successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidCredit", Status.FAIL,
					"Error while applying for borrow credit");
		}
	}

	/*
	 * Method: VerifyRateReservationTransaction Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void VerifyRateReservationTransaction() {
		Assert.assertTrue(verifyObjectDisplayed(ratelabelReservationTransaction));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPointAvailable", Status.PASS,
				"Rate per Transaction label is present for Reservation Transaction");
		Assert.assertTrue(verifyObjectDisplayed(ratevalueReservationTransaction));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPointAvailable", Status.PASS,
				"Rate per Transaction value is present for Reservation Transaction");

		strAmountNeededReservationTransaction = getElementText(amountNeededPoint_ReservationTransaction);
	}

	/*
	 * Method: verifyChangesPurchaseReservationTransactionCheckBx
	 * Description:Validate Borrow Rent Point is Not displayed Date field Date:
	 * June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyChangesAfterSelectingReservationTransactionCheckBx() {

		if (getElementText(purchasingPoint_ReservationTransaction)
				.equalsIgnoreCase(strAmountNeededReservationTransaction)) {
			getElementInView(purchasingPoint_ReservationTransaction);
			tcConfig.updateTestReporter("CUIReservationBalancePage",
					"verifyChangesPurchaseReservationTransactionCheckBx", Status.PASS,
					"Purchase amount is displayed as " + getElementText(purchasingPoint_ReservationTransaction));
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage",
					"verifyChangesPurchaseReservationTransactionCheckBx", Status.FAIL,
					"Incorrect data not displayed");
		}

		if (getElementText(amountNeededPoint_ReservationTransaction).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage",
					"verifyChangesPurchaseReservationTransactionCheckBx", Status.PASS,
					"Amount Needed is updated as " + getElementText(amountNeededPoint_ReservationTransaction));
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage",
					"verifyChangesPurchaseReservationTransactionCheckBx", Status.FAIL, "Incorrect data displayed");
		}

	}

	/*
	 * Method: clickRemoveButton Description:Validate Borrow Rent Point is Not
	 * displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */

	public void clickRemoveButton(String buttonName) {
		switch (buttonName) {
		case "Remove-PointToBorrow":
			checkPresenseAndClickButton(removeButtonPointsToBorrow, buttonName);
			break;
		case "Remove-PointToRent":
			checkPresenseAndClickButton(removeButtonPointsToRent, buttonName);
			break;
		case "Remove-CreditToBorrow":
			checkPresenseAndClickButton(removeButtonCreditsToBorrow, buttonName);
			break;
		case "Remove-CreditToPurchase":
			checkPresenseAndClickButton(removeButtonCreditsToPurchase, buttonName);
			break;
		}
	}

	/*
	 * Method: checkPresenseAndClickButton Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void checkPresenseAndClickButton(By by, String buttonName) {
		waitUntilElementVisibleBy(driver, by, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(by));
		getElementInView(by);
		clickElementBy(by);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "removeAppliedCreditToBorrow", Status.PASS,
				buttonName + " button clicked");
	}

	/*
	 * Method: uncheckSelectedCheckBx Description:Validate Borrow Rent Point is
	 * Not displayed Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void uncheckSelectedCheckBx(String checkBoxName) {
		switch (checkBoxName) {
		case "Borrow-ReservationPoint":
			deSelectCheckBox(borrowCheckBox_ReservationPoints, nextUserYear);
			break;
		case "Rent-ReservationPoint":
			deSelectCheckBox(rentCheckBox_ReservationPoints, rentPointNoteTextValue);
			break;
		case "Borrow-HouseKeepingCredit":
			deSelectCheckBox(borrowCheckBox_HousekeepingCredit, nextUserYear);
			break;
		case "Purchase-HouseKeepingCredit":
			deSelectCheckBox(purchaseCheckBox_HousekeepingCredit, purchaseCreditNoteText);
			break;
		}
	}

	/*
	 * Method: verifyRentPointInputSection Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void verifyRentPointInputSection() {
		Assert.assertTrue(verifyObjectDisplayed(rentPointNoteText));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyRentPointInputSection", Status.PASS,
				"After selecting the Rent Checkbox, text is displayed as Rate per 1,000 points");
		Assert.assertTrue(verifyObjectDisplayed(rentPointNoteTextValue));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyRentPointInputSection", Status.PASS,
				"After selecting the Rent Checkbox, Rate is displayed");
		Assert.assertTrue(getElementAttribute(pointsRentInput, "placeholder").equalsIgnoreCase("pointsToRentInput"));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyRentPointInputSection", Status.PASS,
				"Placeholder value present");
	}

	/*
	 * Method: verifyPurchaseCreditInputSection Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void verifyPurchaseCreditInputSection() {
		Assert.assertTrue(verifyObjectDisplayed(purchaseCreditNoteText));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseCreditInputSection", Status.PASS,
				"After selecting the Purchase Credit Checkbox, text is displayed as Rate per credit");
		Assert.assertTrue(verifyObjectDisplayed(purchaseCreditTextValue));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseCreditInputSection", Status.PASS,
				"After selecting the Purchase Credit Checkbox, Rate is displayed");
		Assert.assertTrue(getElementAttribute(creditToBePurchaseTextBx, "placeholder")
				.equalsIgnoreCase("creditsToPurchaseInput"));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "verifyPurchaseCreditInputSection", Status.PASS,
				"Placeholder value present");
	}

	/*
	 * Method: selectAndUpdateBorrowPoints selectAndUpdateBorrowPoints Date
	 * field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void selectAndUpdateBorrowPoints() {

		clickElementBy(borrowCheckBox_ReservationPoints);
		waitUntilElementVisibleBy(driver, pointToBeBorrowedTextBx, "ExplicitLowWait");
		tcConfig.getTestData().put("BorrowedPoints", amountNeededPts);
		int availablePoints = Integer.parseInt(getElementText(useYearPoints).replace(",", ""));
		tcConfig.getTestData().put("AccountPoints", availablePoints + "");
		sendKeysBy(pointToBeBorrowedTextBx, amountNeededPts);
		sendKeysToElement(pointToBeBorrowedTextBx, Keys.TAB);
		sendKeysToElement(applyButtonPointToBeBorrowed, Keys.RETURN);
		waitUntilElementVisibleBy(driver, removeButtonPointsToBorrow, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(removeButtonPointsToBorrow),
				"Borrowed points not applied successfully");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectAndUpdateBorrowPoints", Status.PASS,
				"Borrowed points applied successfully");

	}

	/*
	 * Method: retrieveHouseKeepingCreditNeeded
	 * Description:retrieveHouseKeepingCreditNeeded Date field Date: June/2020
	 * Author: Monideep Changes By: NA
	 */
	public void retrieveHouseKeepingCreditNeeded() {

		tcConfig.getTestData().put("HouseKeepingCost", getElementText(houseKeepingCreditCost));
		tcConfig.getTestData().put("HouseKeepingCreditsAvailable", getElementText(houseKeepingCreditAvailable));

		assertTrue(Integer.parseInt(getElementText(houseKeepingCreditRemaining)) > 0,
				"House keeping credit more than cost");
		tcConfig.getTestData().put("HouseKeepingPointsNeeded", getElementText(houseKeepingCreditRemaining));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "retrieveHouseKeepingCreditNeeded", Status.PASS,
				"House keeping credits required for booking");

	}

	/*
	 * Method: selectAndPurchaseHouseKeepingCredits Description:
	 * selectAndPurchaseHouseKeepingCredits Date field Date: June/2020 Author:
	 * Monideep Changes By: NA
	 */
	public void selectAndPurchaseHouseKeepingCredits() {

		clickElementBy(purchaseCredit);
		sendKeysBy(houseKeepingPointsToBePurchasedTextBx, tcConfig.getTestData().get("HouseKeepingBorrowPoints"));
		sendKeysToElement(houseKeepingPointsToBePurchasedTextBx, Keys.TAB);
		sendKeysToElement(applyhouseKeepingPointsPurchaseBtn, Keys.RETURN);
		waitUntilElementVisibleBy(driver, appliedhouseKeepingPointsPurchaseBtn, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(appliedhouseKeepingPointsPurchaseBtn),
				"House keeping purchase points not applied successfully");
		tcConfig.getTestData().put("HousekeepingPurchaseCost", getElementText(purchaseHouseKeepingCost));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "purchaseHouseKeepingCredits", Status.PASS,
				"House keeping purchase points applied successfully, applied points : "
						+ tcConfig.getTestData().get("HouseKeepingBorrowPoints"));

	}

	/*
	 * Method: selectAndUpdateHouseKeepingBorrowPoints Description:
	 * selectAndUpdateHouseKeepingBorrowPoints Date field Date: June/2020
	 * Author: Monideep Changes By: NA
	 */
	public void selectAndUpdateHouseKeepingBorrowPoints() {

		clickElementBy(borrowCheckBox_HousekeepingCredit);
		sendKeysBy(creditToBeBorrowedTextBx, tcConfig.getTestData().get("HouseKeepingBorrowPoints"));
		sendKeysToElement(creditToBeBorrowedTextBx, Keys.TAB);
		sendKeysToElement(applyButtonCreditToBeBorrowed, Keys.RETURN);
		waitUntilElementVisibleBy(driver, removeButtonCreditsToBorrow, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(removeButtonCreditsToBorrow),
				"House keeping Borrowed points not applied successfully");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectAndUpdateHouseKeepingBorrowPoints",
				Status.PASS, "House keeping Borrowed points applied successfully, applied points : "
						+ tcConfig.getTestData().get("HouseKeepingBorrowPoints"));

	}

	/*
	 * Method: selectAndEnterHouseKeepingBorrowPoints Description:
	 * selectAndEnterHouseKeepingBorrowPoints Date field Date: June/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void selectAndEnterHouseKeepingBorrowPoints() {

		clickElementBy(borrowCheckBox_HousekeepingCredit);
		sendKeysBy(creditToBeBorrowedTextBx, testData.get("HouseKeepingBorrowPoints"));
		sendKeysToElement(creditToBeBorrowedTextBx, Keys.TAB);
		sendKeysToElement(applyButtonCreditToBeBorrowed, Keys.RETURN);
		waitUntilElementVisibleBy(driver, removeButtonCreditsToBorrow, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(removeButtonCreditsToBorrow),
				"House keeping Borrowed points not applied successfully");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectAndUpdateHouseKeepingBorrowPoints",
				Status.PASS, "House keeping Borrowed points applied successfully, applied points : "
						+ tcConfig.getTestData().get("HouseKeepingBorrowPoints"));

	}

	/*
	 * Method: buyReservationTransaction Description: buyReservationTransaction
	 * Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void buyReservationTransaction() {

		if (verifyObjectDisplayed(purchaseResTranCheckBox)) {
			clickElementBy(purchaseResTranCheckBox);
			tcConfig.getTestData().put("PurchaseReservationTransaction", "YES");
			tcConfig.updateTestReporter("CUIReservationBalancePage", "buyReservationTransaction", Status.PASS,
					"Reservation Transaction purchased");
		} else {
			tcConfig.getTestData().put("PurchaseReservationTransaction", "NO");
			tcConfig.updateTestReporter("CUIReservationBalancePage", "buyReservationTransaction", Status.PASS,
					"Reservation Transaction purchase not required");
		}

	}
	/*
	 * Method: buyReservationTransaction Description: buyReservationTransaction
	 * Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void buyHouseKeepingCredits() {

		if (verifyObjectDisplayed(purchaseHouseKeeping)) {
			clickElementBy(purchaseHouseKeeping);
			tcConfig.getTestData().put("PurchaseHouseKeeping", "YES");
			tcConfig.updateTestReporter("CUIReservationBalancePage", "buyHouseKeepingCredits", Status.PASS,
					"House Keeping Credits purchased");
		} else {
			tcConfig.getTestData().put("PurchaseHouseKeeping", "NO");
			tcConfig.updateTestReporter("CUIReservationBalancePage", "buyHouseKeepingCredits", Status.PASS,
					"House Keeping Credits not required");
		}

	}
	
	
	/*
	 * Method: getAvailableHKCredit Description: buyReservationTransaction
	 * Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void getAvailableHKCredit() {
		if (verifyObjectDisplayed(currentUseYearHKAvailable)) {
			availableHKCurrentYear = Integer.parseInt(getElementText(currentUseYearHKAvailable).trim());
			tcConfig.updateTestReporter("CUIReservationBalancePage", "getAvailableHKCredit", Status.PASS,
					"Available HK for current Use Year is: " + availableHKCurrentYear);
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "getAvailableHKCredit", Status.FAIL,
					"Available HK for current Use Year not displayed");
		}
	}
	
	/*
	 * Method: hkCreditAbsence Description: Verify HK should not be present
	 * Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void hkCreditAbsence() {
		if (verifyObjectDisplayed(currentUseYearHKAvailable)) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "getAvailableHKCredit", Status.FAIL,
					"Available HK for current Use Year is: " + availableHKCurrentYear);
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "getAvailableHKCredit", Status.PASS,
					"Available HK for current Use Year not displayed");
		}
	}

	/*
	 * Method: getPurchasedHKCredit Description: buyReservationTransaction
	 * Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void getPurchasedHKCredit() {
		if (verifyObjectDisplayed(amountNeededPoint_HouseKeepingCredit)) {
			purchasedHK = Integer.parseInt(getElementText(amountNeededPoint_HouseKeepingCredit).trim());
			tcConfig.updateTestReporter("CUIReservationBalancePage", "getAvailableHKCredit", Status.PASS,
					"Purchased HK : " + purchasedHK);
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "getAvailableHKCredit", Status.FAIL,
					"Purchased HK  not displayed");
		}
	}

	/*
	 * Method: setRentBorrowPoint Description: setRentBorrowPoint
	 * Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void setRentBorrowPoint() {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		selectCheckBox_ReservationBalanace("BorrowPoint");
		selectCheckBox_ReservationBalanace("RentPoint");

		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = cap.getBrowserName().toUpperCase();
		if (browserName.contains("EDGE")) {
			sendkeysForEdge(pointToBeBorrowedTextBx, testData.get("PointToBeBorrow"));
		} else {
			sendKeysBy(pointToBeBorrowedTextBx, testData.get("PointToBeBorrow"));
		}
		clickElementBy(applyButtonPointToBeBorrowed);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.PASS,
				"Applied for Borrowing the Point");

		sendKeysBy(pointsRentInput, testData.get("PointToBeRented"));
		clickElementBy(applyButtonPointToBeRented);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.PASS,
				"Applied for Renting the Point");
		getElementInView(rentingPoint_ReservationPoint);
		if (getElementText(amountNeededPoint_ReservationPoint).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.PASS,
					"Point Rented and Borrowed successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.FAIL,
					"Error while applying for rent/borrow point");
		}

	}
		

	/*
	 * Method: setRentBorrowHK Description: setRentBorrowHK
	 * Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void setRentBorrowHK() {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		selectCheckBox_ReservationBalanace("BorrowCredit");
		selectCheckBox_ReservationBalanace("PurchaseCredit");
		
		sendKeysBy(creditToBeBorrowedTextBx, testData.get("HKToBeBorrow"));
		clickElementBy(applyButtonCreditToBeBorrowed);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.PASS,
				"Applied for Borrowing the Housekeeping Credits");

		sendKeysBy(creditToBePurchaseTextBx, testData.get("HKToBePurchase"));
		clickElementBy(applyButtonCreditToBePurchase);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.PASS,
				"Applied for purchasing the Credit");
		
		getElementInView(rentingPoint_ReservationPoint);
		if (getElementText(amountNeededPoint_HouseKeepingCredit).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.PASS,
					"Housekeeping Credit Purchased and Borrowed successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.FAIL,
					"Error while applying for purchasing/borrow credit");
		}

	}
}
