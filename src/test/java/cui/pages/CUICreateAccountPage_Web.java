package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICreateAccountPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUICreateAccountPage_Web.class);

	public CUICreateAccountPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By memberNumberField = By.id("memberNumber");
	protected By firstNameField = By.id("firstName");
	protected By lastNameField = By.id("lastName");
	protected By contractNumberRadio = By.xpath("//label[@for = 'method_contract']");
	protected By phoneNumberRadio = By.xpath("//label[@for = 'method_phone']");
	protected By contractNumberField = By.id("contractNumber");
	protected By continueButton = By.xpath("//button[text() = 'Continue']");
	protected By selectCountryFlag = By.xpath("//div[@class = 'flag-select']");
	protected By phoneNumberField = By.id("phone");

	protected By registerNowLink = By.xpath("//a[contains(.,'Register Now')]");
	protected By suffixField = By.xpath("//select[@id='suffix']");
	protected By createAccountHeader = By.xpath("//h2[contains(.,'CREATE YOUR ACCOUNT')]");
	protected By createAccountDescription = By
			.xpath("//h2[contains(.,'CREATE YOUR ACCOUNT')]/following-sibling::div/p");
	protected By formHeader = By.xpath("//form//h2[contains(.,'Enter the details')]");
	protected By registerCTA = By.xpath("(//p//a[contains(.,'Here')] | //div//a[contains(.,'Here')])");
	protected By loginNowCTA = By.xpath("//div[@class='ctaSlice']//a[contains(.,'Login Now')]");
	protected By memberNumberText = By
			.xpath("//input[@id = 'memberNumber']/following-sibling::span[contains(.,'Member Number*')]");
	protected By firstNameText = By
			.xpath("//input[@id = 'firstName']/following-sibling::span[contains(.,'First Name*')]");
	protected By lastNameText = By.xpath("//input[@id = 'lastName']/following-sibling::span[contains(.,'Last Name*')]");
	protected By suffixText = By.xpath("//select[@id = 'suffix']/following-sibling::span[contains(.,'Suffix')]");
	protected By methodSelectHeader = By.xpath(
			"(//h2[contains(.,'Select a method')] | //div[contains(@class,'caption') and contains(.,'Select a method')])");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");

	protected By defaultFlag = By.xpath("//div[@class='flag-select']//span[contains(.,'United States')]");
	protected By countryCode = By.xpath("//div[@class='country-phone__entry']");
	protected By phoneNumberFieldError = By.xpath("//p[@class='input-error']");
	protected By incorrectDataError = By.id("error-message");
	protected By companyField = By.id("companyName");
	protected By companyFieldText = By
			.xpath("//input[@id = 'companyName']/following-sibling::span[contains(.,'Trust Name*')]");
	protected By memberTypePage = By
			.xpath("(//a[contains(.,'Here')]/parent::div | //a[contains(.,'Here')]/parent::div/parent::p)");
	String selectFlagPart1 = "//div[@class = 'flag-select__choices']//a[contains(text(),'";
	String selectFlagPart2 = "')]";

	/*
	 * Method: createAccountNav Description: Navigate to Create Acc Page Val
	 * Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void createAccountNavigation() {
		getElementInView(registerNowLink);

		clickElementBy(registerNowLink);

		waitUntilElementVisibleBy(driver, createAccountHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(createAccountHeader), "Create Account Header not present");
		elementPresenceVal(createAccountHeader, "Create Account Header", "CUICreateAccountPage",
				"createAccountNavigation");
		elementPresenceVal(createAccountDescription, "Create Account Description", "CUICreateAccountPage",
				"createAccountNavigation");

	}

	/*
	 * 
	 * /* Method: companyTrustNavigation Description: Navigate to Company Trust
	 * page Val Date: mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void companyTrustNavigation() {
		if (verifyObjectDisplayed(registerCTA)) {
			getElementInView(registerCTA);
			clickElementBy(registerCTA);
			waitUntilElementVisibleBy(driver, companyField, "ExplicitLongWait");

			sendKeyboardKeys(Keys.HOME);
			Assert.assertTrue(verifyObjectDisplayed(companyField), "Company Field is not present");

			tcConfig.updateTestReporter("CUICreateAccountPage", "companyTrustNavigation", Status.PASS,
					"Company Trust Register fields displayed");

		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "companyTrustNavigation", Status.FAIL,
					"Company Trust Register Link not present");
		}

	}

	/*
	 * Method: createAccountDataEnter Description:Create Account Enter Data
	 * Elements Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void createAccountDataEnter() {

		waitUntilElementVisibleBy(driver, memberNumberField, "ExplicitLongWait");
		if (verifyObjectDisplayed(memberNumberField)) {
			enterMemberNumber();
			if (getElementText(memberTypePage).toUpperCase().contains("COMPANY")) {
				enterFirstName();
				enterLastName();
				selectSuffix();

			} else if (getElementText(memberTypePage).toUpperCase().contains("INDIVIDUAL")) {
				enterCompanyFieldName();
			}

		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "createAccDataEnter", Status.FAIL,
					"Failed to enter data in create account fields");
		}

	}

	/*
	 * Method: enterMemberNumber Description:Enter Member Number Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */

	public void enterMemberNumber() {
		String memberNumber = testData.get("MemberNumber").trim(); // Data
		getElementInView(memberNumberField);
		getObject(memberNumberField).sendKeys(memberNumber);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterMemberNumber", Status.PASS,
				"Successfully entered member number and the member number is: " + memberNumber);
	}

	/*
	 * Method: enterFirstName Description:Enter First Name Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */

	public void enterFirstName() {
		String firstName = testData.get("FirstName").trim(); // First name Data
		getElementInView(firstNameField);
		fieldDataEnter(firstNameField, firstName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterFirstName", Status.PASS,
				"Successfully entered first name, which is: " + firstName);
	}

	/*
	 * Method: enterLastName Description:Enter Last Name Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void enterLastName() {
		String lastName = testData.get("LastName").trim(); // Last name Data
		getElementInView(lastNameField);
		fieldDataEnter(lastNameField, lastName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterLastName", Status.PASS,
				"Successfully entered last name, which is: " + lastName);
	}

	/*
	 * Method: selectSuffix Description: Select Suffix Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void selectSuffix() {
		try {
			getElementInView(suffixField);
			// Select Suffix
			String strSuffix = testData.get("Suffix").trim();

			if (strSuffix.length() > 0) {
				new Select(getObject(suffixField)).selectByValue(strSuffix);
				tcConfig.updateTestReporter("CUICreateAccountPage", "selectSuffix", Status.PASS,
						"Suffix entered as: " + strSuffix);
			} else {
				tcConfig.updateTestReporter("CUICreateAccountPage", "selectSuffix", Status.PASS,
						"No Suffix entered in test data");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUICreateAccountPage", "selectSuffix", Status.FAIL,
					"Suffix entered is Not present in the list");
		}
	}

	/*
	 * Method: enterCompanyFieldName Description:Enter Company Field Name Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterCompanyFieldName() {
		String companyName = testData.get("CompanyName").trim();
		getElementInView(companyField);
		fieldDataEnter(companyField, companyName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterCompanyFieldName", Status.PASS,
				"Successfully entered Company Trust name which is:  " + companyName);
	}

	/*
	 * Method: selectVerificationMethod Description:Select a verification
	 * methods Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void selectVerificationMethod() {
		// to slelect verification method
		String selectCreateAccountMethod = testData.get("Createaccmethod").trim();
		// Phone Number Data
		String phoneNumber = testData.get("PhoneNumber").trim();
		// Country Select
		String countrySelect = testData.get("Country").trim();
		// Contract Number Data
		String contractNumber = testData.get("ContractNumber").trim();
		if (selectCreateAccountMethod.equals("Phone")) {

			getElementInView(phoneNumberRadio);
			getObject(phoneNumberRadio).click();
			if (verifyObjectDisplayed(selectCountryFlag)) {
				selectCountryFlag(countrySelect);
				enterPhoneNumber(phoneNumber);
			} else {
				tcConfig.updateTestReporter("CUICreateAccountPage", "selectVerificationMethod", Status.FAIL,
						"Flag select field not present please check screenshot");
			}
		} else if (selectCreateAccountMethod.equals("Contract")) {
			getElementInView(contractNumberRadio);
			getObject(contractNumberRadio).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			enterContractNumber(contractNumber);
		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "selectVerificationMethod", Status.FAIL,
					"Please select method of verification from excel where the column name is Createaccmethod");
		}

	}

	/*
	 * Method: selectCountryFlag Description:To Select countr flag Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void selectCountryFlag(String countrySelect) {

		getObject(selectCountryFlag).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getObjectInRuntime(selectFlagPart1, countrySelect, selectFlagPart2).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: enterPhoneNumber Description:Enter Phone Number Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void enterPhoneNumber(String phoneNumber) {
		getElementInView(phoneNumberField);
		fieldDataEnter(phoneNumberField, phoneNumber);
		sendKeyboardKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterPhoneNumber", Status.PASS,
				"Successfully entered Phone Number and the Phone number is: " + phoneNumber);
	}

	/*
	 * Method: enterContractNumber Description:Enter Contract Number Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterContractNumber(String contractNumber) {

		getObject(contractNumberField).sendKeys(contractNumber);
		sendKeyboardKeys(Keys.TAB);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterContractNumber", Status.PASS,
				"Successfully entered Contract number and the Contract number is: " + contractNumber);
	}

	/*
	 * Method: clickContinue Description:Click Continue CTA Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void clickContinue() {

		if (verifyObjectDisplayed(continueButton)) {
			waitUntilElementVisibleBy(driver, continueButton, "ExplicitLongWait");
			getElementInView(continueButton);
			clickElementBy(continueButton);

		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "clickContinue", Status.FAIL,
					"Error in clicking Continue CTA");
		}

	}

	/*
	 * Method: phoneNumberFieldValidations Description:Account Phone Number
	 * Field validations Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void phoneNumberFieldValidations() {
		// Country Data
		String countrySelect = testData.get("Country").trim();
		// phone Number data
		String phoneNumber = testData.get("invalidData").trim();
		// More Than 10 Digits Number
		String incorrectPhoneNumber = phoneNumber + phoneNumber;
		// invalidPhoneNumber
		String invalidNumber = testData.get("invalidData");

		getElementInView(phoneNumberRadio);
		clickElementBy(phoneNumberRadio);

		waitUntilElementVisibleBy(driver, selectCountryFlag, "ExplicitLowWait");
		if (verifyObjectDisplayed(selectCountryFlag)) {
			if (verifyObjectDisplayed(defaultFlag)) {
				tcConfig.updateTestReporter("CUICreateAccountPage", "phoneNumberFieldValidations", Status.PASS,
						"Default Flag is Selected of United States with Country Code: " + getElementText(countryCode));
			} else {
				tcConfig.updateTestReporter("CUICreateAccountPage", "phoneNumberFieldValidations", Status.FAIL,
						"Default Flag is not Selected of United States");
			}
			selectCountryFlag(countrySelect);
			enterPhoneNumber(incorrectPhoneNumber);
			checkPhoneFieldError();
			enterPhoneNumber(invalidNumber);

		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "selectVeriMethod", Status.FAIL,
					"Flag select field not present please check screenshot");
		}
	}

	/*
	 * Method: checkDefaultFlag Description:Default flag check Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void checkDefaultFlag() {
		if (verifyObjectDisplayed(defaultFlag)) {
			tcConfig.updateTestReporter("CUICreateAccountPage", "checkDefaultFlag", Status.PASS,
					"Default Flag is Selected of United States with Country Code: " + getElementText(countryCode));
		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "checkDefaultFlag", Status.FAIL,
					"Default Flag is not Selected of United States");
		}
	}

	/*
	 * Method: checkPhoneFieldError Description:Check Error in Phone Field Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkPhoneFieldError() {
		waitUntilElementVisibleBy(driver, phoneNumberFieldError, "ExplicitLongWait");
		if (verifyObjectDisplayed(phoneNumberFieldError)) {
			tcConfig.updateTestReporter("CUICreateAccountPage", "checkPhoneFieldError", Status.PASS,
					"More than 10 digits entered error displayed");
		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "checkPhoneFieldError", Status.FAIL,
					"More than 10 digits entered error not displayed");
		}
	}

	/*
	 * Method: checkIncorrectError Description:Check Incorrect data error Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkIncorrectError() {
		waitUntilElementVisibleBy(driver, incorrectDataError, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(incorrectDataError), "Incorrect Data Error not present");

		tcConfig.updateTestReporter("CUICreateAccountPage", "checkIncorrectError", Status.PASS,
				"Incorret Data Entered and the error is displayed");

	}

	/*
	 * Method: contractDetailsModalValidation Description:Validate Create Acc
	 * Page Elements Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void contractDetailsModalValidation() {

		fieldPresenceVal(memberNumberField, "Member No Field", "CUICreateAccountPage",
				"contractDetailsModalValidations", "with text " + getElementText(memberNumberText));

		if (getElementText(memberTypePage).toUpperCase().contains("COMPANY")) {
			fieldPresenceVal(firstNameField, "First Name Field", "CUICreateAccountPage",
					"contractDetailsModalValidations", "with text " + getElementText(firstNameText));
			fieldPresenceVal(lastNameField, "Last Name Field", "CUICreateAccountPage",
					"contractDetailsModalValidations", "with text " + getElementText(lastNameText));
			fieldPresenceVal(suffixField, "Sufix Field", "CUICreateAccountPage", "contractDetailsModalValidations",
					"with text " + getElementText(suffixText));
		} else if (getElementText(memberTypePage).toUpperCase().contains("INDIVIDUAL")) {
			fieldPresenceVal(companyField, "Company Trust Field", "CUICreateAccountPage",
					"contractDetailsModalValidations", "with text " + getElementText(companyFieldText));
		}

		elementPresenceVal(contractNumberRadio, "Contract Number Option ", "CUICreateAccountPage",
				"contractDetailsModalValidations");
		elementPresenceVal(phoneNumberRadio, "Phone Number Option", "CUICreateAccountPage",
				"contractDetailsModalValidations");
		elementPresenceVal(methodSelectHeader, "Method Select Header", "CUICreateAccountPage",
				"contractDetailsModalValidations");
		elementPresenceVal(disabledContinueCTA, "Disabled continue CTA", "CUICreateAccountPage",
				"contractDetailsModalValidations");
		elementPresenceVal(registerCTA, "Company Trust, Register CTA", "CUICreateAccountPage",
				"contractDetailsModalValidations");

	}
}
