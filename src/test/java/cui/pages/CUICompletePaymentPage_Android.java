package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import automation.core.TestConfig;

public class CUICompletePaymentPage_Android extends CUICompletePaymentPage_Web {

	public static final Logger log = Logger.getLogger(CUICompletePaymentPage_Android.class);

	public CUICompletePaymentPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		buttonContinue = By.xpath("(//button[text() = 'Complete Payment'])");
	}
	CUIBookPage_IOS bookPageIOS=new CUIBookPage_IOS(tcConfig);
	/*
	 * Method: paymentViaPaypal Description:payment Via Paypal
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void paymentViaPaypal(){
		bookPageIOS.paymentViaPaypal();
	}
}