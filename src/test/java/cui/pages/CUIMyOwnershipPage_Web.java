package cui.pages;

import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIMyOwnershipPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIMyOwnershipPage_Web.class);

	public CUIMyOwnershipPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By ownerFullName = By.xpath("//h4[contains(.,'WELCOME')]/parent::div/h2");
	protected By myAccountDropDown = By
			.xpath("//div[@class='global-navigation__wrapper']//button[contains(@class,'container')]");
	protected By dropDownOwnership = By.xpath(
			"(//div[@class='top-bar-right']//div[@class='global-account__links']//li[contains(@class,'ownership')]/a | //div[@class='top-bar-right']//div[@class='global-account__links']//li[contains(@class,'membership')]/a)");
	protected By myOwnershipHeader = By.xpath(
			"(//h1[contains(@class,'title') and contains(.,'OWNERSHIP')]|//h1[contains(@class,'title') and contains(.,'MEMBERSHIP')])");

	protected By memberNumberHeader = By.xpath("//h2[contains(.,'Member Number')]/following-sibling::p");
	protected By memberNumberIcon = By.xpath("//h2[contains(.,'Member Number')]/parent::div/preceding-sibling::div");
	protected By membershipTypeHeader = By.xpath("//h2[contains(.,'Membership Type')]/following-sibling::p");
	protected By membershipTypeIcon = By
			.xpath("//h2[contains(.,'Membership Type')]/parent::div/preceding-sibling::div");
	protected By membershipExpiration = By
			.xpath("//h2[contains(.,'Membership Type')]/following-sibling::div[contains(.,'Expiration')]");
	protected By ownerStatus = By.xpath("//h2[contains(.,'Status')]/following-sibling::p");
	protected By ownerStatusIcon = By.xpath("//h2[contains(.,'Status')]/parent::div/preceding-sibling::div");
	protected By statusExpiration = By
			.xpath("//h2[contains(.,'Status')]/following-sibling::div[contains(.,'Expiration')]");
	protected By additionalProgramsHeader = By.xpath("//h2[contains(.,'Additional')]");
	protected By additionalPrograms = By.xpath("//h2[contains(.,'Additional')]/following-sibling::p");

	protected By financialDetailsHeader = By.xpath("// h1[contains(.,'Financial Details')]");
	protected By makePaymentHeader = By
			.xpath("//section[contains(@class,'financial-details')]//h2[contains(.,'Make a Payment')]");
	protected By financialDetailsInfo = By
			.xpath("//section[contains(@class,'financial-details')]//p[contains(.,'method of payment')]");
	protected By enrollButton = By
			.xpath("//section[contains(@class,'financial-details')]//input[contains(@value,'Enroll')]");
	protected By payPalLink = By.xpath("//section[contains(@class,'financial-details')]//a[contains(.,'PayPal')]");
	protected By hiddenValueXpath = By.xpath("//form[@id='westernUnionForm']//input");
	// input[@type='hidden' and @value='WVR' and @name='club' and @id='club']
	protected By speedPayLink = By.xpath("//form[@id='westernUnionForm']//input[contains(@value,'Speedpay')]");
	protected By speedPayNavigation = By.id("wvr_logo");

	protected By myContractHeader = By.xpath("// h1[contains(.,'My Contract')]");
	protected By contractHeaders = By.xpath("// div[contains(@class,'my-account-header')]/h2[contains(.,'Contract')]");
	protected By seeMoreContractCTA = By.xpath("// section[@class='contracts']//button");
	protected By contractOwnerName = By.xpath("// h2[contains(.,'Owner(s)')]/following-sibling::p");
	protected By contractPoints = By.xpath("// h2[contains(.,'Points Owned')]/following-sibling::p");
	protected By contractType = By.xpath("// h2[contains(.,'Contract Type')]/following-sibling::p");
	protected By homeResortLink = By.xpath("// h2[contains(.,'Home Resort')]/following-sibling::div//a");
	protected By homeResortText = By.xpath(
			"(// h2[contains(.,'Home Resort')]/following-sibling::p | // h2[contains(.,'Home Resort')]/following-sibling::div//a)");
	protected By resortPageNavigation = By.xpath("//h1[contains(@class,'title-1')]");
	protected By reciprocalResortText = By.xpath(
			"(// h2[contains(.,'Reciprocal Resort')]/following-sibling::p| // h2[contains(.,'Reciprocal Resort')]/following-sibling::div//a)");
	protected By reciprocalResortLink = By.xpath("// h2[contains(.,'Reciprocal Resort')]/following-sibling::div//a");
	protected By contractDetails = By.xpath("// h2[contains(.,'Contract Details')]/following-sibling::p");
	protected By contractUseYear = By.xpath("// h2[contains(.,'Use Year')]/following-sibling::p");
	protected By contractToolTip = By.xpath("// span[contains(@id,'contract-details-tooltip')]");
	protected By contractToolTipInfo = By.xpath("// div[contains(@class,'tooltip')]");

	protected By pointOfContactHeader = By.xpath("//section[contains(@class,'point-of-contact')]//h1");
	protected By pointOfContactInfo = By
			.xpath("//section[contains(@class,'point-of-contact')]//h1/following-sibling::p/span");

	protected By ownerName = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Owner')]/following-sibling::p");
	protected By ownerNameIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Owner')]/parent::div/preceding-sibling::div");
	protected By ownerAddress = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Address')]/following-sibling::p");
	protected By ownerAddressIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Address')]/parent::div/preceding-sibling::div");
	protected By ownerEmail = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Email')]/following-sibling::p");
	protected By ownerEmailIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Email')]/parent::div/preceding-sibling::div");
	protected By ownerPhone = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Phone')]/following-sibling::p");
	protected By ownerPhoneIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Phone')]/parent::div/preceding-sibling::div");

	protected By paymentType = By.xpath("//select[@id = 'payment-type']");

	/* CWAR - ASSESSMENTS Xpath */
	String totalHOAAssessmentTotalStr, programFeeAssessmentTotalStr, annualTotalAssessmentTotalStr,
			monthlyPaymentAssessmentTotalStr, ATPageannualTotal;
	double totalHOAAssessmentTotalStrValue, programFeeAssessmentTotalStrValue, annualTotalDouble,
			annualTotalInAssessmentTotal;
	protected By dashboardLoadingIcon = By.xpath("//div[contains(@class,'loading')]");
	protected By myOwnershipTab = By.xpath("//div[@class='accountNavigation']//following::span[text()='My Ownership']");
	protected By financialDetailsSection = By
			.xpath("//section[contains(@class, 'financial-details')]/h1[contains(.,'Financial Details')]");
	protected By summaryText = By.xpath(
			"//div[contains(@class, 'assessment-summary')]//p[text()='This summary includes the payment totals for all of your Club Wyndham contracts.']");
	protected By assessmentSummaryTitle = By
			.xpath("//div[contains(@class, 'assessment-summary-caption-1') and text()='ASSESSMENTS SUMMARY']");
	protected By assessmentSummaryText = By.xpath("//div[@class='assessment-summary-body-1']");
	protected By viewAssessmentsLink = By.xpath("//a[contains(@class, link) and text()='View My Assessments ']");
	protected By assessmentSummaryLoadingIcon = By
			.xpath("//div[contains(@class,'assessment-summary-body-1')]/following::div[@class='loading']");
	protected By hoaLabel = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-lable-body-1'])[1]");
	protected By hoaValue = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-value-body-1'])[1]");
	protected By programfee = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-lable-body-1'])[2]");
	protected By programfeeValue = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-value-body-1'])[2]");
	protected By AnnualTotal = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-lable-body-1'])[3]");
	protected By AnnualTotalValue = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-value-body-1'])[3]");
	protected By MonthlyPayment = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-lable-body-1'])[4]");
	protected By MonthlyPaymentValue = By
			.xpath("(//div[@class='assessment-summary-container']//div[@class='price-value-body-1'])[4]");
	protected By totalHoaAssessmentDetails = By
			.xpath("//div[contains(@class,'assessment-total-container')]//div[contains(@class,'totalHoa')]");
	protected By programFeeAssessmentDetails = By
			.xpath("//div[contains(@class,'assessment-total-container')]//div[contains(@class,'programFee')]");
	protected By annualTotalAssessmentDetails = By
			.xpath("//div[contains(@class,'assessment-total-container')]//div[contains(@class,'annualTotal')]");
	protected By monthlyPaymentAssessmentDetails = By
			.xpath("//div[contains(@class,'assessment-total-container')]//div[contains(@class,'monthlyPayment')]");

	protected By autoPayEnrollmentTitle = By
			.xpath("//div[contains(@class, 'auto-pay') and text()='AUTO-PAY ENROLLMENT STATUS']");
	protected By enrolledStatus = By.xpath("//div[contains(@class, 'enroll') and text()='Enrolled ']");
	protected By editEnrollmentLink = By
			.xpath("//div[contains(@class, 'edit-enrolled-container')]//input[@value='Edit Enrollment']");
	protected By notEnrolledStatus = By.xpath("//div[contains(@class, 'enroll') and text()='Not Enrolled ']");
	protected By enrollNowLink = By
			.xpath("//div[contains(@class, 'edit-enrolled-container')]//input[@value='Enroll Now']");
	// div[contains(@class, 'edit-enrolled-container')]//input[@value='Enroll
	// Now' and @style='display: none;']

	protected By assessmentDetailsModalTitle = By
			.xpath("//div[contains(@class,'popup-title-caption-text') and text()='Assessment Details']");
	protected By assessmentSummary = By
			.xpath("//div[contains(@class, 'modal-assessmentsummary-subtitle-text') and text()='Assessment Summary']");
	protected By contractNumberLabel = By
			.xpath("//div[contains(@class, 'modal-assessmentsummary-caption-text') and text()='Contract Number']");
	protected By contractNumberValueAssessmentSummary = By
			.xpath("//div[contains(.,'Assessment Summary')]//following::div[contains(@class, 'contractNumber')]");
	protected By annualTotalLabelAssessmentSummary = By
			.xpath("//div[contains(@class, 'modal-assessmentsummary-caption-text') and text()='Annual Total']");
	protected By annualTotalAssessmentSummary = By.xpath(
			"//div[contains(.,'Assessment Summary')]//following::div[contains(@class, 'modal-assessmentsummary-body-text annualTotal')]");

	protected By monthlyPaymentLabelAssessmentSummary = By
			.xpath("//div[contains(@class, 'modal-assessmentsummary-caption-text') and text()='Monthly Payment']");
	protected By monthlyPaymentAssessmentSummary = By.xpath(
			"//div[contains(.,'Assessment Summary')]//following::div[contains(@class, 'modal-assessmentsummary-body-text monthlyPayment')]");
	protected By descriptionLabelAssessmentSummary = By
			.xpath("//div[contains(@class, 'modal-assessmentsummary-caption-text') and text()='Description']");
	protected By descriptionAssessmentSummary = By.xpath(
			"//div[contains(.,'Assessment Summary')]//following::div[contains(@class, 'modal-assessmentsummary-body-text description')]");

	protected By closeButton = By.xpath("//button[@class='close-button']");
	protected By viewMyAssessmentsLink = By.xpath("//a[contains(text(),'View My Assessments')]");
	protected By myAssessmentsTitle = By
			.xpath("//div[contains(@class,'caption')]//following::div[contains(text(),'My Assessments')]");

	protected By hoaLabelAssessmentTotal = By
			.xpath("//div[@class='assessment-total-container ']//div[text()='Total HOA']");
	protected By programFeeLabelAssessmentTotal = By
			.xpath("//div[@class='assessment-total-container ']//div[text()='Program Fee']");
	protected By annualTotalLabelAssessmentTotal = By
			.xpath("//div[@class='assessment-total-container ']//div[text()='Annual Total']");
	protected By monthlyPaymentLabelAssessmentTotal = By
			.xpath("//div[@class='assessment-total-container ']//div[text()='Monthly Payment']");
	protected By totalHOAAssessmentTotal = By
			.xpath("//div[contains(@class,'assessment-total')]//following::div[contains(@class,'totalHoa')]");
	protected By programFeeAssessmentTotal = By
			.xpath("//div[contains(@class,'assessment-total')]//following::div[contains(@class,'programFee')]");
	protected By annualTotalAssessmentTotal = By
			.xpath("//div[contains(@class,'assessment-total')]//following::div[contains(@class,'annualTotal')]");
	protected By monthlyPaymentAssessmentTotal = By
			.xpath("//div[contains(@class,'assessment-total')]//following::div[contains(@class,'monthlyPayment')]");
	protected By contractList = By.xpath(
			"//div[contains(@class,'contract-table-content')]//following::div[contains(@class,'contract-row-container')]");
	protected By contractNumber = By.xpath("(//div[contains(@class,'contractNum')]/div)");
	protected By resortName = By.xpath("(//div[contains(@class,'contractDesc')]/div)");
	protected By monthlyPay = By.xpath("(//div[contains(@class,'MonthlyPay')]/div)");

	protected By assessmentDetailsTitle = By.xpath("//div[contains(@class,'caption') and text()='Assessment Details']");
	protected By assessmentSummarySubtitle = By
			.xpath("//div[contains(@class,'subtitle') and text()='Assessment Summary']");
	protected By summaryContractNumberLabel = By
			.xpath("//div[contains(@class,'assessmentsummary') and text()='Contract Number']");
	protected By summaryContractNumberValue = By
			.xpath("//div[contains(@class,'assessmentsummary')]//following::div[contains(@class,'contractNumber')]");
	protected By summaryAnnualTotalLabel = By
			.xpath("//div[contains(@class,'assessmentsummary')and text()='Annual Total']");
	protected By summaryAnnualTotalValue = By
			.xpath("//div[contains(@class,'assessmentsummary')]//following::div[contains(@class,'annualTotal')]");
	protected By summaryDescriptionLabel = By
			.xpath("//div[contains(@class,'assessmentsummary') and text()='Description']");
	protected By summaryDescriptionValue = By
			.xpath("//div[contains(@class,'assessmentsummary')]//following::div[contains(@class,'description')]");
	protected By summaryMonthlyPaymentLabel = By
			.xpath("//div[contains(@class,'assessmentsummary') and text()='Monthly Payment']");
	protected By summaryMonthlyPaymentValue = By
			.xpath("//div[contains(@class,'assessmentsummary')]//following::div[contains(@class,'monthlyPayment')]");

	protected By contractDetailsSubtitle = By
			.xpath("//div[contains(@class,'contract-details-subtitle') and text()='Contract Details']");
	protected By contractTypeLabel = By
			.xpath("//div[contains(@class,'contract-details-caption') and text()='Contract Type']");
	protected By contractTypeValue = By.xpath(
			"//div[contains(@class,'contract-details-subtitle')]//following::div[contains(@class,'contractType')]");
	protected By pointsOwnedLabel = By
			.xpath("//div[contains(@class,'contract-details-caption') and text()='Points Owned']");
	protected By pointsOwnedValue = By.xpath(
			"//div[contains(@class,'contract-details-subtitle')]//following::div[contains(@class,'pointsOwned')]");
	protected By paymentFrequencyLabel = By
			.xpath("//div[contains(@class,'contract-details-caption') and text()='Payment Frequency']");
	protected By paymentFrequencyValue = By.xpath(
			"//div[contains(@class,'contract-details-subtitle')]//following::div[contains(@class,'paymentFrequency')]");

	protected By homeOwnerAssocTitle = By.xpath("//div[contains(@class,'modal-homeowner-fees-caption')]");
	protected By totlaHOAFeeExpanded = By
			.xpath("//button[@class='accordion active']//div[contains(text(), 'Total HOA Fee')]");
	protected By totlaHOAFeeCollapsed = By
			.xpath("//button[@class='accordion']//div[contains(text(), 'Total HOA Fee')]");
	protected By programFeeExpanded = By
			.xpath("//button[@class='accordion active']//div[contains(text(), 'Program Fee')]");
	protected By programFeeCollapsed = By.xpath("//button[@class='accordion']//div[contains(text(), 'Program Fee')]");
	protected By monthlyFeeExpanded = By
			.xpath("//button[@class='accordion active']//div[contains(text(), 'Monthly Fee')]");
	protected By monthlyFeeCollapsed = By.xpath("//button[@class='accordion']//div[contains(text(), 'Monthly Fee')]");
	/*
	 * protected By totalHOAFeeMain = By .xpath(
	 * "//button[@class='accordion active']//div[contains(@class,'value-body-text homeOwnerTotalHOAFee')]"
	 * );
	 */
	protected By picBonusHOALabel = By
			.xpath("//div[contains(@class,'fee-billed-by-your-HOA-lbl') and text()='Fee billed by your HOA']");
	protected By picBonusHOAValue = By.xpath("//div[contains(@class,'value-body-text fee-billed-by-your-HOA')]");
	protected By totalHOAFeeMain = By.xpath(
			"//div[contains(@class,'modal-homeowner-fees-caption')]//following::div[contains(@class,' homeOwnerTotalHOAFee')][1]");
	protected By maintanenceFeesLabel = By
			.xpath("//div[@class='maintenance-fees-div']//div[contains(@class,'content-body-text maintenance-fees')]");
	protected By maintanenceFeesValue = By
			.xpath("//div[@class='maintenance-fees-div']//div[contains(@class,'value-body-text maintenance-fees')]");
	protected By reserveFundLabel = By
			.xpath("//div[@class='reserve-funds-div']//div[contains(@class,'content-body-text reserve-funds')]");
	protected By reserveFundValue = By
			.xpath("//div[@class='reserve-funds-div']//div[contains(@class,'value-body-text reserve-funds')]");
	protected By propertyTaxLabel = By
			.xpath("//div[@class='property-taxes-div']//div[contains(@class,'content-body-text property-taxes')]");
	protected By propertyTaxValue = By
			.xpath("//div[@class='property-taxes-div']//div[contains(@class,'value-body-text property-taxes')]");
	protected By localTaxValue = By
			.xpath("//div[@class='local-taxes-div']//div[contains(@class,'value-body-text local-taxes')]");

	protected By programFeeLabel = By.xpath("//div[contains(@class,'content-body-text program-fee-lbl')]");
	protected By programFeeExpandedValue = By.xpath("//div[contains(@class,'value-body-text program-fee-expand')]");
	protected By programFeeValue = By.xpath("(//div[contains(@class, 'value-body-text program-fee')])[1]");
	protected By annualFeeLabel = By.xpath("//div[contains(@class,'annual-fee')]//div[text()='Annual Fee']");
	protected By annualFeeValue = By
			.xpath("//div[contains(@class,'annual-fee-container')]//following::div[@class='annual-fee']");
	protected By monthlyFeeLabel = By.xpath("//div[contains(@class,'content-body-text monthly-fee-lbl')]");
	protected By monthlyFeeExpandedValue = By.xpath("//div[contains(@class,'value-body-text monthly-fee-expand')]");

	protected By monthlyFeeClosedValue = By
			.xpath("//button[@class='accordion']//div[contains(@class,'value-body-text monthly-fee')]");
	/*
	 * protected By asaa = By.xpath("");
	 */

	/*
	 * Method: navigateToMyOwnershipPage Description:Navigate to My Ownership
	 * Page Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void navigateToMyOwnershipPage() {
		waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		if (verifyObjectDisplayed(myAccountDropDown)) {
			clickElementBy(myAccountDropDown);
			waitUntilElementVisibleBy(driver, dropDownOwnership, "ExplicitLongWait");
			clickElementBy(dropDownOwnership);
			waitUntilElementVisibleBy(driver, myOwnershipHeader, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader), "My Ownership header not present");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.PASS,
					"User Navigated to My Ownership page");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.FAIL,
					"User failed to Navigated to My Ownership page");
		}
	}

	/*
	 * Method: verifyMemberNumber Description:verify Member Number Page Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyMemberNumber() {
		// Member Number
		String strMemberNumber = testData.get("MemberNumber");

		if (getElementText(memberNumberHeader).contains(strMemberNumber)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMemberNumber", Status.PASS,
					"Member Number Header present with number " + strMemberNumber);
			elementPresenceVal(memberNumberIcon, "Member Number Icon ", "CUIMyOwnershipPage_Web", "verifyMemberNumber");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMemberNumber", Status.FAIL,
					"Member Number Header not present with number " + strMemberNumber);
		}
	}

	/*
	 * Method: verifyMembershipType Description:verify Membership type Page Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyMembershipType() {
		// Membership type
		String strMemberType = testData.get("MemberType");

		if (getElementText(membershipTypeHeader).contains(strMemberType)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMemberNumber", Status.PASS,
					"Membership Type Header present with Type " + strMemberType);
			elementPresenceVal(membershipTypeIcon, "Membership Type Icon ", "CUIMyOwnershipPage_Web",
					"verifyMembershipType");
			if (verifyObjectDisplayed(membershipExpiration)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMembershipType", Status.PASS,
						"Membership Expiration dtae present " + getElementText(membershipExpiration));
			}
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMembershipType", Status.FAIL,
					"Membership Type Header not present with Type " + strMemberType);
		}
	}

	/*
	 * Method: verifyVIPStatusType Description:verify VIP Status type Page Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyVIPStatusType() {
		// Status type
		String strStatusType = testData.get("UserStatus");

		if (getElementText(ownerStatus).contains(strStatusType)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyVIPStatusType", Status.PASS,
					"Status Header present with Type " + strStatusType);
			if (verifyObjectDisplayed(statusExpiration)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyVIPStatusType", Status.PASS,
						"Status Expiration dtae present " + getElementText(statusExpiration));
			}
			elementPresenceVal(ownerStatusIcon, "Status Icon ", "CUIMyOwnershipPage_Web", "verifyVIPStatusType");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyVIPStatusType", Status.FAIL,
					"Status Header not present with Type " + strStatusType);
		}
	}

	/*
	 * Method: verifyAdditionalProgram Description:verify Additional Program Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyAdditionalProgram() {

		if (verifyObjectDisplayed(additionalProgramsHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAdditionalProgram", Status.PASS,
					"Additional Program Header present with total " + getList(additionalPrograms).size() + " programs");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAdditionalProgram", Status.WARNING,
					"Additional Program List Not Present");
		}
	}

	/*
	 * Method: financialDetailsHeader Description:verify Financial Details
	 * section * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void financialDetailsHeader() {
		getElementInView(financialDetailsHeader);
		if (verifyObjectDisplayed(financialDetailsHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.PASS,
					"Financial Details Section present in the page");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.FAIL,
					"Financial Details Section not present in the page");
		}
	}

	/*
	 * Method: myContractsHeader Description:verify My contract header section *
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void myContractsHeader() {
		getElementInView(myContractHeader);
		if (verifyObjectDisplayed(myContractHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.PASS,
					"My Contracts Header present in the page with Total contract " + totalContracts());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.FAIL,
					"My Contract Headers not present in the page");
		}
	}

	/*
	 * Method: expandContracts Description:Return Expand Total Contract Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void expandContracts() {
		if (totalContracts() > 2) {
			if (verifyObjectDisplayed(seeMoreContractCTA)) {
				getElementInView(seeMoreContractCTA);
				clickElementBy(seeMoreContractCTA);
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "expandContracts", Status.PASS,
						"See More Contracts CTA present and clicked as there are more than 2 contracts");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(myContractHeader);
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "expandContracts", Status.FAIL,
						"See More Contracts CTA not present even when there are more than 2 contracts");
			}
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "expandContracts", Status.PASS,
					"See More Contracts CTA not present as there are less than 2 contracts");
		}

	}

	/*
	 * Method: totalContractsValidations Description:Total contract match the
	 * number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalContractsValidations() {
		if (getList(contractHeaders).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "totalContractsValidations", Status.PASS,
					"Total Contract Number and Contract Card displayed are equal as: "
							+ getList(contractHeaders).size());
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "totalContractsValidations", Status.FAIL,
					"Total Contract Number and contract Cards count are different");
		}

	}

	/*
	 * Method: ownerNameValidations Description:Total owner name match the total
	 * contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void ownerNameValidations() {
		if (getList(contractOwnerName).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "ownerNameValidations", Status.PASS,
					"Owner Name present in all cards: " + getList(contractOwnerName).size());
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "ownerNameValidations", Status.FAIL,
					"Owner Name is not present in all contracts");
		}

	}

	/*
	 * Method: pointsOwnedValidations Description:Total Points Owned match the
	 * total contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsOwnedValidations() {
		if (getList(contractPoints).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "pointsOwnedValidations", Status.PASS,
					"Points Owned present in all cards: " + getList(contractPoints).size());
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "pointsOwnedValidations", Status.FAIL,
					"Points Owned is not present in all contracts");
		}

	}

	/*
	 * Method: contractDetailsValidations Description:Total Contract Details
	 * match the total contract number Date: Jun/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void contractDetailsValidations() {
		if (getList(contractDetails).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsValidations", Status.PASS,
					"Contract Details present in all cards: " + getList(contractType).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsValidations", Status.WARNING,
					"Contract Details is not present in all contracts");
		}

	}

	/*
	 * Method: contractDetailsToolTip Description:Contract Details Tool Tip
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void contractDetailsToolTip() {
		if (getList(contractDetails).size() > 0) {

			if (getList(contractToolTip).size() == getList(contractDetails).size()) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsToolTip", Status.PASS,
						"Tool tip present in all contract details card" + getList(contractType).size());
				getElementInView(getList(contractToolTip).get(0));
				getList(contractToolTip).get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				toolTipCTAValidations(0);
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsToolTip", Status.FAIL,
						"Tool tip not present in all contract details card");
			}
		}
	}

	/*
	 * Method: toolTipCTAValidations Description: Tool Tip CTA Validations Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void toolTipCTAValidations(int contractDetails) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String strFlag = getList(contractToolTipInfo).get(contractDetails).getAttribute("aria-hidden");
		if (strFlag.contains("false")) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "toolTipCTAValidations", Status.PASS,
					"Tool Tip opened");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "toolTipCTAValidations", Status.FAIL,
					"Failed to open Tool tip");
		}
	}

	/*
	 * Method: useYearValidations Description:Total Use year match the total
	 * contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void useYearValidations() {
		if (getList(contractUseYear).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearValidations", Status.PASS,
					"Use Year present in all cards: " + getList(contractUseYear).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearValidations", Status.FAIL,
					"Points Owned is not present in all contracts");
		}

	}

	/*
	 * Method: useYearDateFormat Description: Use year date format contract
	 * number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void useYearDateFormat() {
		if (getList(contractUseYear).size() > 0) {
			if (isValidDate(getList(contractUseYear).get(0).getText(), "MMMM dd")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearDateFormat", Status.PASS,
						"Use Year date displayed in correct format" + getList(contractUseYear).get(0).getText());

			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearDateFormat", Status.FAIL,
						"Use Year date not displayed in correct format" + getList(contractUseYear).get(0).getText());
			}
		}

	}

	/*
	 * Method: homeResortValidations Description:Total Home Resort match the
	 * total contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void homeResortValidations() {
		if (getList(homeResortText).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortValidations", Status.PASS,
					"Home Resort present in all cards" + getList(homeResortText).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortValidations", Status.WARNING,
					"Home Resort is not present in all contracts, total : " + getList(homeResortText).size());
		}

	}

	/*
	 * Method: homeResortLinkValidations Description:Total Home Resort match the
	 * total contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void homeResortLinkValidations() {
		if (getList(homeResortText).size() > 0) {
			if (verifyObjectDisplayed(getList(homeResortLink).get(0))) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortLinkValidations", Status.PASS,
						"Home Resort Link present in some cards" + getList(homeResortLink).size());
				homeResortFirstLinkValidation();

			} else if (getList(homeResortText).size() > 0) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortLinkValidations", Status.FAIL,
						"Home Resort is displayed as text in all the contracts card");
			}
		}
	}

	/*
	 * Method: homeResortFirstLinkValidation Description:Points Link Elements
	 * Validations * Date field Date: June/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void homeResortFirstLinkValidation() {
		String resortName = getList(homeResortLink).get(0).getText().toUpperCase();
		getElementText(getList(homeResortLink).get(0));
		clickElementBy(getList(homeResortLink).get(0));
		waitUntilElementVisibleBy(driver, resortPageNavigation, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(resortPageNavigation), "Resort page navigation failed");
		if (getElementText(resortPageNavigation).toUpperCase().contains(resortName)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortFirstLinkValidation", Status.PASS,
					"Home Resort Link Navigated");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortFirstLinkValidation", Status.FAIL,
					"Home Resort Link Navigation failed");
		}
		navigateBack();
		waitUntilElementVisibleBy(driver, myOwnershipHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader), "My Ownership header not present");

	}

	/*
	 * Method: reciprocalResortValidations Description:Total reciprocal Resort
	 * match the total contract number Date: Jun/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void reciprocalResortValidations() {

		if (getList(reciprocalResortText).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortValidations", Status.PASS,
					"Reciprocal Resort present in all cards" + getList(reciprocalResortText).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortValidations", Status.WARNING,
					"Reciprocal Resort is not present in all contracts, total : "
							+ getList(reciprocalResortText).size());
		}

	}

	/*
	 * Method: reciprocalResortLinkValidations Description:Total Reciprocal
	 * Resort match the total contract number Date: Jun/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void reciprocalResortLinkValidations() {
		if (getList(reciprocalResortText).size() > 0) {
			if (verifyObjectDisplayed(getList(reciprocalResortLink).get(0))) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortLinkValidations", Status.PASS,
						"Reciprocal Resort Link present in some cards" + getList(reciprocalResortLink).size());
				reciprocalResortFirstLinkValidation();

			} else if (getList(reciprocalResortText).size() > 0) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortLinkValidations", Status.WARNING,
						"Reciprocal Resort is displayed as text in all the contracts card");
			}
		}
	}

	/*
	 * Method: reciprocalResortFirstLinkValidation Description:Points Link
	 * Elements Validations * Date field Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void reciprocalResortFirstLinkValidation() {
		String resortName = getList(reciprocalResortLink).get(0).getText().toUpperCase();
		getElementText(getList(reciprocalResortLink).get(0));
		clickElementBy(getList(reciprocalResortLink).get(0));
		waitUntilElementVisibleBy(driver, resortPageNavigation, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(resortPageNavigation), "Resort Page navigation failed");
		if (getElementText(resortPageNavigation).toUpperCase().contains(resortName)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortFirstLinkValidation", Status.PASS,
					"Reciprocal Resort Link Navigated");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortFirstLinkValidation", Status.FAIL,
					"Reciprocal Resort Link Navigation failed");
		}
		navigateBack();
		waitUntilElementVisibleBy(driver, myOwnershipHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader), "My Ownership header not present");

	}

	/*
	 * Method: verifyPointOfContact Description:verify POC Val Date: Jun/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void verifyPointOfContact() {
		getElementInView(pointOfContactHeader);
		if (verifyObjectDisplayed(pointOfContactHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPointOfContact", Status.PASS,
					"Point fo Contact Header present");
			elementPresenceVal(pointOfContactInfo, "Point of Contact Info ", "CUIMyOwnershipPage_Web",
					"verifyPointOfContact");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPointOfContact", Status.FAIL,
					"Point fo Contact Header not present");
		}
	}

	/*
	 * Method: verifyOwnerName Description:verify Owner name Val Date: Jun/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void verifyOwnerName() {
		// Owner Name
		String strOwnerName = testData.get("CompanyName");

		if (getElementText(ownerName).contains(strOwnerName)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyOwnerName", Status.PASS,
					"Owner Name Header present with Name " + strOwnerName);
			elementPresenceVal(ownerNameIcon, "Owner Name Icon ", "CUIMyOwnershipPage_Web", "verifyOwnerName");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyOwnerName", Status.FAIL,
					"Owner Name Header not present with Name " + strOwnerName);
		}
	}

	/*
	 * Method: verifyEmail Description:verify Email Page Val Date: Jun/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void verifyEmail() {
		// Email
		String strEmail = testData.get("Email");

		if (getElementText(ownerEmail).contains(strEmail)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyEmail", Status.PASS,
					"Email Header present with Email " + strEmail);
			elementPresenceVal(ownerEmailIcon, "Email Icon ", "CUIMyOwnershipPage_Web", "verifyEmail");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyEmail", Status.FAIL,
					"Email Header not present with email " + strEmail);
		}
	}

	/*
	 * Method: verifyAddress Description:verify Address Val Date: Jun/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void verifyAddress() {
		// Member Number
		String strAddress = testData.get("Address");
		String applicationAddress = getElementText(ownerAddress);
		try {
			applicationAddress = getElementText(ownerAddress).replace("\n", "");
		} catch (Exception e) {
			Log.info("Adress does not contain next line");
		}

		if (applicationAddress.contains(strAddress)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAddress", Status.PASS,
					"Address Header present with address " + strAddress);

			elementPresenceVal(ownerAddressIcon, "Address Icon ", "CUIMyOwnershipPage_Web", "verifyAddress");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAddress", Status.FAIL,
					"Address Header not present with address " + strAddress);
		}
	}

	/*
	 * Method: verifyPhoneNumber Description:verify Phone Number Page Val Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyPhoneNumber() {
		// Phone Number
		String strPhoneNumber = testData.get("PhoneNumber");

		if (getElementText(ownerPhone).contains(strPhoneNumber)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPhoneNumber", Status.PASS,
					"Phone Number Header present with number " + strPhoneNumber);
			elementPresenceVal(ownerPhoneIcon, "Phone Number Icon ", "CUIMyOwnershipPage_Web", "verifyPhoneNumber");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPhoneNumber", Status.FAIL,
					"Phone Number Header not present with number " + strPhoneNumber);
		}
	}

	/*
	 * Method: totalContracts Description:Return Total Contract Number Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public int totalContracts() {
		// My Contracts String
		String totalContracts = getElementText(myContractHeader);
		String splitContract[] = totalContracts.split("\\(");
		totalContracts = splitContract[splitContract.length - 1];
		totalContracts = totalContracts.replace(")", "").trim();
		// Contracts in Integer
		int totalCotractNumber = Integer.parseInt(totalContracts);
		return totalCotractNumber;
	}

	/*
	 * Method: makePaymentHeader Description:verify make Payment header * Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void makePaymentHeader() {

		if (verifyObjectDisplayed(makePaymentHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "makePaymentHeader", Status.PASS,
					"Make Payment header present in the page");
			elementPresenceVal(financialDetailsInfo, "Financial Details Info Text ", "CUIMyOwnershipPage_Web",
					"makePaymentHeader");
			elementPresenceVal(enrollButton, "Enroll in Autopay Button ", "CUIMyOwnershipPage_Web",
					"makePaymentHeader");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "makePaymentHeader", Status.FAIL,
					"Make Payment header not present in the page");
		}
	}

	/*
	 * Method: speedPayLinkPresence Description:Speed Pay Link validations *
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void speedPayLinkPresence() {

		if (verifyObjectDisplayed(speedPayLink)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkPresence", Status.PASS,
					"Speed Pay Link Present");

			elementPresenceVal(payPalLink, "PayPal Link", "CUIMyOwnershipPage_Web", "speedPayLinkPresence");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkPresence", Status.FAIL,
					"Speed Pay Link not Present");
		}
	}

	/*
	 * Method: clickPayPalLink Description:Click Link PayPal present in My
	 * Ownership Page Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void clickPayPalLink() {
		if (verifyObjectDisplayed(payPalLink)) {
			clickElementJSWithWait(payPalLink);
			// waitUntilElementInVisible(driver, loadingSpinnerCUI,
			// "ExplicitLongWait");
			waitUntilElementVisibleBy(driver, paymentType, "ExplicitLongWait");
			if (verifyObjectDisplayed(paymentType)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickPayPalLink", Status.PASS,
						"Successfully clicked on Paypal Link");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickPayPalLink", Status.FAIL,
						"Failed to click on Paypal Link");
			}

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickPayPalLink", Status.FAIL,
					"PayPal Link Not present in My Ownership Page");
		}
	}

	/*
	 * Method: hiddenValueAttribute Description:Speed Pay Link WVR attribute
	 * validations * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void hiddenValueAttribute() {
		try {
			if (getList(hiddenValueXpath).get(0).getAttribute("value").contains("WVR")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.PASS,
						"Hidden Attribute has the value as WVR");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
						"Hidden Attribute does not have the value as WVR");
			}

			if (getList(hiddenValueXpath).get(0).getAttribute("name").contains("club")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.PASS,
						"Hidden Attribute has the name as club");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
						"Hidden Attribute does not have the name as club");
			}

			if (getList(hiddenValueXpath).get(0).getAttribute("id").contains("club")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.PASS,
						"Hidden Attribute has the id as club");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
						"Hidden Attribute does not have the id as club");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
					"Failed to validate hidden attribute xpath");
		}

	}

	/*
	 * Method: speedPayLinkValidation Description:Speed Pay Link validations *
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void speedPayLinkValidation() {
		getElementInView(speedPayLink);
		clickElementBy(speedPayLink);
		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(newWindow);
		waitUntilElementVisibleBy(driver, speedPayNavigation, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(speedPayNavigation), "Speed pay page navigation failed");
		if (driver.getCurrentUrl().contains(testData.get("newPageURL"))) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkValidation", Status.PASS,
					"Speed Pay Link Navigated");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkValidation", Status.FAIL,
					"Speedpay Link Navigation failed");
		}
		navigateToMainTab(newWindow);
		waitUntilElementVisibleBy(driver, myOwnershipHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader), "My Ownership header not present");

	}

	/*
	 * Method: validateMyOwnershipAssessmentContents Description: Validate
	 * contents in the Assessment section Date: Feb/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */

	String totalHoaAsummaryStr, programFeeAsummaryStr, annualTotalAsummaryStr, monthlyPaymentAsummaryStr,
			sumofHoaAndProgramFeeStr, ASPageannualTotal;
	double annualTotalAsummary, annualTotalAsummaryDbl;

	public void validateMyOwnershipAssessmentContents() {
		waitUntilElementInVisible(driver, assessmentSummaryLoadingIcon, 100);
		if (verifyObjectDisplayed(assessmentSummaryTitle)) {
			scrollDownViaKeyboard(2);
			;
			Assert.assertTrue(verifyObjectDisplayed(financialDetailsSection),
					"Financial Details section not displayed ");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateMyOwnershipAssessmentContents", Status.PASS,
					"Financial Details section displayed ");
			Assert.assertTrue(verifyObjectDisplayed(assessmentSummaryTitle),
					"“Assessment Summary” title not displayed ");
			validateLabelandValue(hoaLabel, hoaValue, "Total HOA");
			validateLabelandValue(programfee, programfeeValue, "Program Fee");
			validateLabelandValue(AnnualTotal, AnnualTotalValue, "Annua Total");
			validateLabelandValue(MonthlyPayment, MonthlyPaymentValue, "Monthly Payment");
			Assert.assertTrue(verifyObjectDisplayed(summaryText), "Assessment Summary text not displayed ");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateMyOwnershipAssessmentContents", Status.PASS,
					"SummaryText,Labels and values displayed under “Assessment Summary” section");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateMyOwnershipAssessmentContents", Status.FAIL,
					"Issue while getting assessment page details");
		}
	}

	public void validateAssessmentsSummary() {
		waitUntilElementInVisible(driver, assessmentSummaryLoadingIcon, 100);
		if (verifyObjectDisplayed(assessmentSummaryTitle)) {
			scrollDownViaKeyboard(2);
			totalHoaAsummaryStr = getElementText(hoaValue).replaceAll("[$,]", "");
			programFeeAsummaryStr = getElementText(programfeeValue).replaceAll("[$,]", "");
			annualTotalAsummaryStr = getElementText(AnnualTotalValue).replaceAll("[$,]", "");
			monthlyPaymentAsummaryStr = getElementText(MonthlyPaymentValue).replaceAll("[$,]", "");
			annualTotalAsummary = Double.parseDouble(totalHoaAsummaryStr) + Double.parseDouble(programFeeAsummaryStr);
			annualTotalAsummaryDbl = Math.round(annualTotalAsummary * 100.0) / 100.0;
			ASPageannualTotal = String.format("%.2f", annualTotalAsummaryDbl);
			Assert.assertEquals(ASPageannualTotal, annualTotalAsummaryStr,
					"Annual Total in Assessment Summary page is incorrect");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAssessmentsSummary", Status.PASS,
					"Annual Total in Assessment Summary page sum of Total HOA and Program fee: " + " HOA Value: "
							+ totalHoaAsummaryStr + " Program Fee Value: " + programFeeAsummaryStr
							+ " Annual Total Value: " + annualTotalAsummaryStr + " Monthly Payment Value: "
							+ monthlyPaymentAsummaryStr);
			calculateMonthlyFeeAssessment(annualTotalAsummaryDbl, monthlyPaymentAsummaryStr);
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAssessmentsSummary", Status.FAIL,
					"Issue while getting assessment summary details");
		}
	}
	
	

	/*
	 * Method: calculateMonthlyFeeAssessment Description: Calculate Monthly Fees
	 * in the Assessments section Date: March/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */

	public void calculateMonthlyFeeAssessment(double annualTotalAsummaryDbl, String monthlyPaymentStr) {
		monthlyFeeCalc = (annualTotalAsummaryDbl / 12.0);
		monthlyFeeCalcDbl = Math.floor(monthlyFeeCalc * 100.0) / 100.0;
		monthlyFeeCalcStr = String.format("%.2f", monthlyFeeCalcDbl);
		Assert.assertEquals(monthlyFeeCalcStr, monthlyPaymentStr,
				"Annual Payment divided by 12 does not equal Monthly Payment");
		tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "calculateMonthlyFeeAssessment", Status.PASS,
				"Annual Payment divided by 12 equals Monthly Payment");
	}

	/*
	 * Method: clickMyOwnershipTab Description: Click on Ownership tab from the
	 * left menu Date: Feb/2021 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void clickMyOwnershipTab() throws Exception {
		waitUntilElementInVisible(driver, dashboardLoadingIcon, 120);
		if (verifyObjectDisplayed(myOwnershipTab)) {
			clickElementJSWithWait(myOwnershipTab);
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickMyOwnershipTab", Status.PASS,
					"Clicked on My Ownership Tab");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickMyOwnershipTab", Status.FAIL,
					"Issue while getting assessment page details");
		}
	}

	/*
	 * Method: validateAssessmentTotal Description: Validate contents in the
	 * Assessment section Date: Feb/2021 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */

	public void validateAssessmentTotalLabels() throws Exception {
		waitUntilElementInVisible(driver, assessmentSummaryLoadingIcon, 120);
		if (verifyObjectDisplayed(viewMyAssessmentsLink)) {
			clickElementBy(viewMyAssessmentsLink);
			waitUntilElementVisibleBy(driver, myAssessmentsTitle, "ExplicitLongWait");
			scrollDownForElementJSBy(myAssessmentsTitle);
			Assert.assertTrue(verifyObjectDisplayed(myAssessmentsTitle), "My Assessments not displayed ");
			validateLabelandValue(hoaLabelAssessmentTotal, totalHOAAssessmentTotal, "HOA Total in Assessment Total");
			validateLabelandValue(programFeeLabelAssessmentTotal, programFeeAssessmentTotal,
					"Program Fee in Assessment Total");
			validateLabelandValue(annualTotalLabelAssessmentTotal, annualTotalAssessmentTotal,
					"Annual Total in Assessment Total");
			validateLabelandValue(monthlyPaymentLabelAssessmentTotal, monthlyPaymentAssessmentTotal,
					"Monthly Payment in Assessment Total");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAssessmentTotal", Status.PASS,
					"Labels and Values displayed in the Assessment Total section");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAssessmentTotal", Status.FAIL,
					"View Assessments link not present");
		}
	}

	/*
	 * Method: validateAssessmentTotalValues Description: Validate calculation
	 * in the Assessment section Date: Feb/2021 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void validateAssessmentTotalValues() {
		waitUntilElementInVisible(driver, assessmentSummaryLoadingIcon, 120);
		if (verifyObjectDisplayed(viewMyAssessmentsLink)) {
			clickElementBy(viewMyAssessmentsLink);
			waitUntilElementVisibleBy(driver, contractList, "ExplicitLongWait");
			totalHOAAssessmentTotalStr = getElementText(totalHOAAssessmentTotal).replaceAll("[$,]", "");
			programFeeAssessmentTotalStr = getElementText(programFeeAssessmentTotal).replaceAll("[$,]", "");
			annualTotalAssessmentTotalStr = getElementText(annualTotalAssessmentTotal).replaceAll("[$,]", "");
			monthlyPaymentAssessmentTotalStr = getElementText(monthlyPaymentAssessmentTotal).replaceAll("[$,]", "");
			annualTotalDouble = Double.parseDouble(totalHOAAssessmentTotalStr)
					+ Double.parseDouble(programFeeAssessmentTotalStr);
			annualTotalInAssessmentTotal = Math.round(annualTotalDouble * 100.0) / 100.0;
			ATPageannualTotal = String.format("%.2f", annualTotalInAssessmentTotal);
			// ATPageannualTotal = String.valueOf(annualTotalInAssessmentTotal);
			Assert.assertEquals(ATPageannualTotal, annualTotalAssessmentTotalStr,
					"Annual Total in assessment total is incorrect");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAssessmentTotalValues", Status.PASS,
					"Annual Total calculation displayed as sum of HOA and Program Fee " + "  HOA Value: "
							+ totalHOAAssessmentTotalStr + " program Fee Value: " + programFeeAssessmentTotalStr
							+ " Annual Total Value: " + annualTotalAssessmentTotalStr + " Monthly Payment Value: "
							+ monthlyPaymentAssessmentTotalStr);
			calculateMonthlyFeeAssessment(annualTotalInAssessmentTotal, monthlyPaymentAssessmentTotalStr);
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAssessmentTotalValues", Status.FAIL,
					"View Assessments link not present");
		}
	}

	/*
	 * Method: validateAutoEnrollmentStatus Description: Validate auto pay
	 * enrollment status section Date: Mar/2021 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void verifyAutoPayEnrollStatusTitle() throws Exception {
		if (verifyObjectDisplayed(autoPayEnrollmentTitle)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAutoPayEnrollStatusTitle", Status.PASS,
					"Auto Pay enrollment status section title is displayed");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAutoPayEnrollStatusTitle", Status.FAIL,
					"Auto Pay enrollment status section title is notdisplayed");
		}
	}

	/*
	 * Method: validateAutoEnrollmentStatus Description: Validate auto pay
	 * enrollment status section Date: Mar/2021 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void validateAutoEnrollmentStatus() throws Exception {
		verifyAutoPayEnrollStatusTitle();
		if (verifyObjectDisplayed(enrolledStatus) && verifyObjectDisplayed(editEnrollmentLink)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAutoEnrollmentStatus", Status.PASS,
					"Member in enrolled status. Edit enrollment link is displayed");
		} else if (verifyObjectDisplayed(notEnrolledStatus) && verifyObjectDisplayed(enrollNowLink)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAutoEnrollmentStatus", Status.PASS,
					"Member in Not Enrolled status. Enroll now link is displayed");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAutoEnrollmentStatus", Status.FAIL,
					"Issue with Auto-Pay enrollment status");
		}
	}

	/*
	 * Method: validateContract Description: Validate contents in the Contracts
	 * section Date: Feb/2021 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	String contractNumberContSection, resortNameContSection, monthlyPayContract;

	public void validateAssessmentByContract() throws Exception {
		if (verifyObjectDisplayed(contractList)) {
			List<WebElement> cont = getList(contractNumber);
			navigateContractList(cont, "Contract Number");
			List<WebElement> resort = getList(resortName);
			navigateContractList(resort, "Contract Description");
			List<WebElement> monthPay = getList(monthlyPay);
			navigateContractList(monthPay, "Monthly Fee");
		}
	}

	public void navigateContractList(List<WebElement> Li, String Type) {
		if (Li.size() > 0) {
			for (WebElement we : Li) {
				contractNumberContSection = getElementText(we);
				Assert.assertTrue(verifyObjectDisplayed(we), Type + " is not displayed ");
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateContractList", Status.PASS,
						Type + " is displayed: " + contractNumberContSection);
			}
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateContractList", Status.FAIL,
					"Contracts list section not displayed");
		}

	}

	/*
	 * Method: validateAssessmentSummaryModal Description: Validate labels and
	 * values displayed in the Assessment Modal Date: Feb/2021 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void validateAssessmentSummaryModal() throws Exception {
		waitUntilElementInVisible(driver, assessmentSummaryLoadingIcon, 120);
		if (verifyObjectDisplayed(viewMyAssessmentsLink)) {
			clickElementBy(viewMyAssessmentsLink);
			waitUntilElementVisibleBy(driver, contractList, "ExplicitLongWait");
			List<WebElement> cont = getList(contractList);
			if (cont.size() > 0) {
				for (WebElement we : cont) {
					contractNumberContSection = getElementText(we);
					Assert.assertTrue(verifyObjectDisplayed(we), cont + " is not displayed ");
					clickElementBy(we);
					validateAssessmentSummary();
					validatecontractDetails();
					validateHOAFeeSection();
					clickElementBy(closeButton);
					tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateContract", Status.PASS,
							"Contract is displayed: " + contractNumberContSection);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateContract", Status.FAIL,
					"Contracts list section not displayed");
		}

	}

	/*
	 * Method: calculateAssessmentSummaryModalValues Description: Validate fees
	 * calculation displayed in the Assessment Modal Date: Feb/2021 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void calculateAssessmentSummaryModalValues() throws Exception {
		waitUntilElementVisibleBy(driver, contractList, "ExplicitLongWait");
		if (verifyObjectDisplayed(contractList)) {
			List<WebElement> cont = getList(contractList);
			if (cont.size() > 0) {
				for (WebElement we : cont) {
					contractNumberContSection = getElementText(we);
					Assert.assertTrue(verifyObjectDisplayed(we), cont + " is not displayed ");
					clickElementBy(we);
					calculateTotalHoAFees();
					calculateTotalAnnualFees();
					calculateMonthlyFeeValue();
					clickElementBy(closeButton);
					tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateContract", Status.PASS,
							"Contract is displayed: " + contractNumberContSection);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateContract", Status.FAIL,
					"Contracts list section not displayed");
		}

	}

	/*
	 * Method: validateAssessmentSummary Description: Validate contents in the
	 * Assessment Summary section Date: Feb/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */

	public void validateAssessmentSummary() throws Exception {
		waitUntilElementVisibleBy(driver, assessmentDetailsTitle, "ExplicitLongWait");
		if (verifyObjectDisplayed(assessmentDetailsTitle)) {
			Assert.assertTrue(verifyObjectDisplayed(assessmentSummarySubtitle),
					"Assessment Summary Subtitle is not displayed ");
			Assert.assertTrue(verifyObjectDisplayed(summaryContractNumberLabel),
					"Contract Number Label is not displayed ");
			Assert.assertFalse(getElementText(summaryContractNumberValue).isEmpty(),
					"Contract Number Value is not displayed ");
			Assert.assertTrue(verifyObjectDisplayed(summaryAnnualTotalLabel), "Annual Total Label is not displayed ");
			Assert.assertFalse(getElementText(summaryAnnualTotalValue).isEmpty(),
					"Annual Total Value is not displayed ");
			Assert.assertTrue(verifyObjectDisplayed(summaryDescriptionLabel), "Description Label is not displayed ");
			Assert.assertFalse(getElementText(summaryDescriptionValue).isEmpty(),
					"Description Value is not displayed ");
			Assert.assertTrue(verifyObjectDisplayed(summaryMonthlyPaymentLabel),
					"MonthlyPaymentLabel is not displayed ");
			Assert.assertFalse(getElementText(summaryMonthlyPaymentValue).isEmpty(),
					"Monthly Payment Value is not displayed ");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAssessmentSummary", Status.PASS,
					"Assessment Summary section is displayed");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateContract", Status.FAIL,
					"Assessment Summary section not displayed");
		}
	}

	/*
	 * Method: validatecontractDetails Description: Validate contents in the
	 * Contract details section Date: Feb/2021 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */

	public void validatecontractDetails() throws Exception {
		waitUntilElementVisibleBy(driver, contractDetailsSubtitle, "ExplicitLongWait");
		if (verifyObjectDisplayed(contractDetailsSubtitle)) {
			Assert.assertTrue(verifyObjectDisplayed(contractTypeLabel), "Contract Type Label is not displayed ");
			Assert.assertFalse(getElementText(contractTypeValue).isEmpty(), "Contract Type Value is not displayed ");
			Assert.assertTrue(verifyObjectDisplayed(pointsOwnedLabel), "Points Owned Label is not displayed ");
			Assert.assertFalse(getElementText(pointsOwnedValue).isEmpty(), "Points Owned Value is not displayed ");
			Assert.assertTrue(verifyObjectDisplayed(paymentFrequencyLabel),
					"Payment Frequency Label is not displayed ");
			Assert.assertFalse(getElementText(paymentFrequencyValue).isEmpty(),
					"Payment Frequency Value is not displayed ");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validatecontractDetails", Status.PASS,
					"Contract Details section displayed Labels and Values for Contract Type, Points Owned and Payment Frequency");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateContract", Status.FAIL,
					"Contract details section not displayed");
		}
	}

	/*
	 * Method: validatehomeOwnerAssocTitle Description: Validate title in the
	 * Homeowner Association Fee section Date: March/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */

	String maintanenceFeeStr, reserveFundStr, propertyTaxStr, localTaxStr, totalHOAFeeStr;
	double totalHOAFeeValue, totalHOAFeeValueDbl;

	public void validatehomeOwnerAssocTitle() throws Exception {
		waitUntilElementVisibleBy(driver, homeOwnerAssocTitle, "ExplicitLongWait");
		scrollDownForElementJSBy(homeOwnerAssocTitle);
		if (verifyObjectDisplayed(homeOwnerAssocTitle)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validatehomeOwnerAssocTitle", Status.PASS,
					"HomeOwner's Association Fee title displayed");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validatehomeOwnerAssocTitle", Status.FAIL,
					"HomeOwner's Association Fee title not displayed");
		}
	}

	/*
	 * Method: validateHOAFeeSection Description: Validate HOA Fee section Date:
	 * March/2021 Author: Krishnaraj Kaliyamoorthy By: NA
	 */
	String contractTypeStr;

	public void validateHOAFeeSection() throws Exception {
		contractTypeStr = getElementText(contractTypeValue);
		if (contractTypeStr.contains("Personal Interval Choice (PIC)") || contractTypeStr.contains("Bonus")) {
			if (verifyObjectDisplayed(totlaHOAFeeCollapsed)) {
				validateLabelandValue(picBonusHOALabel, picBonusHOAValue,
						"Fee billed by your HOA not displayed for " + contractTypeStr);
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateHOAFeeSection", Status.PASS,
						"Fee billed by your HOA displayed for " + contractTypeStr);
			} else {
				clickElementBy(totlaHOAFeeExpanded);
				validateLabelandValue(picBonusHOALabel, picBonusHOAValue,
						"Fee billed by your HOA not displayed for " + contractTypeStr);
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateHOAFeeSection", Status.PASS,
						"Fee billed by your HOA displayed for " + contractTypeStr);
			}

		} else {
			validateFieldsHoaFeeSection();
			validateProgramFeeSection();
			validateAnnualFeeSection();
			validateMonthlyFeeSection();
		}
	}

	/*
	 * Method: validateFieldsHoaFeeSection Description: Validate fields in the
	 * HOA Fee section Date: March/2021 Author: Krishnaraj Kaliyamoorthy By: NA
	 */
	public void validateFieldsHoaFeeSection() throws Exception {
		validatehomeOwnerAssocTitle();
		if (verifyObjectDisplayed(totlaHOAFeeCollapsed)) {
			validateLabelandValue(maintanenceFeesLabel, maintanenceFeesValue, "Maintanence Fee");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateFieldsHoaFeeSection", Status.PASS,
					"Maintanence Fee label and value displayed");
			validateLabelandValue(reserveFundLabel, reserveFundValue, "Reserve Fund");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateFieldsHoaFeeSection", Status.PASS,
					"Reserve Fund label and value displayed");
			validateLabelandValue(propertyTaxLabel, propertyTaxValue, "Property Tax");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateFieldsHoaFeeSection", Status.PASS,
					"Property Tax and value displayed");
		} else {
			clickElementBy(totlaHOAFeeExpanded);
			validateLabelandValue(maintanenceFeesLabel, maintanenceFeesValue, "Maintanence Fee");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateFieldsHoaFeeSection", Status.PASS,
					"Maintanence Fee label and value displayed");
			validateLabelandValue(reserveFundLabel, reserveFundValue, "Reserve Fund");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateFieldsHoaFeeSection", Status.PASS,
					"Reserve Fund label and value displayed");
			validateLabelandValue(propertyTaxLabel, propertyTaxValue, "Property Tax");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateHomeOwnerAssociationFeeSection", Status.PASS,
					"Maintanence Fee section is expanded view and the label, value displayed");
		}

	}

	/*
	 * Method: validateProgramFeeSection Description: Validate fields in the
	 * Program Fee section Date: March/2021 Author: Krishnaraj Kaliyamoorthy By:
	 * NA
	 */
	public void validateProgramFeeSection() throws Exception {
		if (verifyObjectDisplayed(programFeeCollapsed)) {
			clickElementBy(programFeeCollapsed);
			validateLabelandValue(programFeeLabel, programFeeExpandedValue, "Program Fee");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateProgramFeeSection", Status.PASS,
					"Program Fee collapsed by default; label and value displayed");
		} else {
			validateLabelandValue(programFeeLabel, programFeeExpandedValue, "Program Fee");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateProgramFeeSection", Status.FAIL,
					"Program Fee not collapsed by default; label and value displayed");
		}

	}

	/*
	 * Method: validateAnnualFeeSection Description: Validate fields in the
	 * Annual Fee section Date: March/2021 Author: Krishnaraj Kaliyamoorthy By:
	 * NA
	 */
	public void validateAnnualFeeSection() throws Exception {
		if (verifyObjectDisplayed(annualFeeLabel)) {
			validateLabelandValue(annualFeeLabel, annualFeeValue, "Annual Fee");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAnnualFeeSection", Status.PASS,
					"Annual Fee label and value displayed");
		} else {
			validateLabelandValue(programFeeLabel, programFeeExpandedValue, "Program Fee");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateAnnualFeeSection", Status.FAIL,
					"Annual Fee label and value not displayed");
		}

	}

	/*
	 * Method: validateMonthlyFeeSection Description: Validate fields in the
	 * Monthly Fee section Date: March/2021 Author: Krishnaraj Kaliyamoorthy By:
	 * NA
	 */
	public void validateMonthlyFeeSection() throws Exception {
		if (verifyObjectDisplayed(monthlyFeeCollapsed)) {
			clickElementBy(monthlyFeeCollapsed);
			validateLabelandValue(monthlyFeeLabel, monthlyFeeExpandedValue, "Monthly Fee");
			clickElementBy(monthlyFeeExpanded);
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateMonthlyFeeSection", Status.PASS,
					"Monthly Fee label and value displayed");
		} else {
			validateLabelandValue(programFeeLabel, programFeeExpandedValue, "Program Fee");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validateMonthlyFeeSection", Status.PASS,
					"Monthly Fee label and value displayed in expanded view");
		}

	}

	/*
	 * Method: validateLabelandValue Description: Method to validate Labels and
	 * Values Date: March/2021 Author: Krishnaraj Kaliyamoorthy By: NA
	 */
	public void validateLabelandValue(By labelName, By value, String fieldName) {
		Assert.assertTrue(verifyObjectDisplayed(labelName), fieldName + " Label is not displayed ");
		Assert.assertFalse(getElementText(value).isEmpty(), fieldName + " Value is not displayed ");
	}

	/*
	 * Method: calculateTotalHoAFees Description: Calculate Total HOA Fees in
	 * the Homeowner Association Fee section Date: March/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	String programFeeStr, totalHOAFeeMainStr, totalAnnualFeeStr, annualFeeValueStr;
	double totalAnnualFee, totalAnnualFeeDbl;

	public void calculateTotalHoAFees() {
		if (verifyObjectDisplayed(totlaHOAFeeCollapsed)) {
			calculateHOAValues();
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "calculateTotalHoAFees", Status.PASS,
					"Total HOA displayed with the sum of fee values under HOA section");
		} else {
			clickElementBy(totlaHOAFeeExpanded);
			calculateHOAValues();
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "calculateTotalHoAFees", Status.PASS,
					"Total HOA displayed with the sum of fee values under HOA section");
		}
	}

	/*
	 * Method: calculateHOAValues Description: Calculate Total HOA Fees in the
	 * Homeowner Association Fee section Date: March/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void calculateHOAValues() {
		maintanenceFeeStr = getElementText(maintanenceFeesValue).replaceAll("[$,]", "");
		reserveFundStr = getElementText(reserveFundValue).replaceAll("[$,]", "");
		propertyTaxStr = getElementText(propertyTaxValue).replaceAll("[$,]", "");
		totalHOAFeeMainStr = getElementText(totalHOAFeeMain).replaceAll("[$,]", "");
		totalHOAFeeValue = Double.parseDouble(maintanenceFeeStr) + Double.parseDouble(reserveFundStr)
				+ Double.parseDouble(propertyTaxStr);
		totalHOAFeeValueDbl = Math.round(totalHOAFeeValue * 100.0) / 100.0;
		/*
		 * localTaxStr = getElementText(localTaxValue).replaceAll("[$,]", "");
		 * totalHOAFee = Double.parseDouble(maintanenceFeeStr) +
		 * Double.parseDouble(reserveFundStr) +
		 * Double.parseDouble(propertyTaxStr) + Double.parseDouble(localTaxStr);
		 */
		totalHOAFeeStr = String.format("%.2f", totalHOAFeeValueDbl);
		// totalHOAFeeStr = String.valueOf(totalHOAFeeValueDbl);
		System.out.println("totalHOAFee " + totalHOAFeeValue + " totalHOAFeeStr " + totalHOAFeeStr);
		Assert.assertEquals(totalHOAFeeStr, totalHOAFeeMainStr, "Totl HOA calculation is incorrect");
	}

	/*
	 * Method: calculateTotalAnnualFees Description: Calculate Total Annual Fees
	 * in the Homeowner Association Fee section Date: March/2021 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void calculateTotalAnnualFees() {
		scrollDownForElementJSBy(annualFeeValue);
		programFeeStr = getElementText(programFeeValue).replaceAll("[$,]", "");
		totalHOAFeeMainStr = getElementText(totalHOAFeeMain).replaceAll("[$,]", "");
		totalAnnualFee = Double.parseDouble(programFeeStr) + Double.parseDouble(totalHOAFeeMainStr);
		totalAnnualFeeDbl = Math.round(totalAnnualFee * 100.0) / 100.0;
		totalAnnualFeeStr = String.format("%.2f", totalAnnualFeeDbl);
		// totalAnnualFeeStr = String.valueOf(totalAnnualFeeDbl);
		annualFeeValueStr = getElementText(annualFeeValue).replaceAll("[$,]", "");
		System.out.println("totalHOAFee " + totalHOAFeeValue + " totalAnnualFeeStr " + totalAnnualFeeStr);
		Assert.assertEquals(totalAnnualFeeStr, annualFeeValueStr, "Annual Total in assessment total is incorrect");
		tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "calculateTotalAnnualFees", Status.PASS,
				"Fees displayed as expected");
	}

	/*
	 * Method: calculateMonthlyFeeValue Description: Calculate Monthly Fees in
	 * the Homeowner Association Fee section Date: March/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	double monthlyFeeCalc, monthlyFeeCalcDbl;
	String monthlyFeeCalcStr, monthlyFeeStr;

	public void calculateMonthlyFeeValue() {
		monthlyFeeStr = getElementText(monthlyFeeClosedValue).replaceAll("[$,]", "");
		monthlyFeeCalc = (totalAnnualFeeDbl / 12.0);
		monthlyFeeCalcDbl = Math.floor(monthlyFeeCalc * 100.0) / 100.0;
		monthlyFeeCalcStr = String.format("%.2f", monthlyFeeCalcDbl);
		System.out.println("monthlyFeeCalcStr " + monthlyFeeCalcStr + " monthlyFeeStr " + monthlyFeeStr);
		Assert.assertEquals(monthlyFeeCalcStr, monthlyFeeStr, "Monthly Fee value is incorrect");
	}

	/*
	 * Method: dateConvert Description: remove dollar from Price Date: Feb/2021
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public String removeDollarSign(String price) {
		return price.substring(1);
	}

	public Number formatFee(String price) throws ParseException {
		NumberFormat format = NumberFormat.getCurrencyInstance();
		Number fee = format.parse("price");
		return fee;
	}

}
