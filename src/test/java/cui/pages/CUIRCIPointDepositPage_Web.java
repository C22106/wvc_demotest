package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIRCIPointDepositPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIRCIPointDepositPage_Web.class);

	public CUIRCIPointDepositPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By pointDepositRCIProgessBar = By.xpath("//div[contains(@class,'booking-header')][2]");
	protected By backRCIandDepositCTA = By.xpath("//div[contains(@class,'back-cta')]/a");
	protected By headerSelectDeposit = By.xpath("//h1[contains(.,'Select Deposit')]");
	protected By areaPointChartRCI = By.xpath("//div[@id = 'rci-points-chart']//a");
	protected By tablePointChart = By.xpath("//div[@id = 'rci-points-chart']//table");
	protected By informationalTextDepositRCI = By.xpath("//div[@id = 'rci-points-chart']//p[contains(text(),'points required for your desired vacation')]");
	protected By disclaimerRCIDeposit = By.xpath("//div[@id = 'rci-points-chart']//span[contains(.,'estimates of RCI values')]");
	protected By headerDepositPoints = By.xpath("//h2[text() = 'Deposit Points']");
	protected By tooltipDepositPoint = By.xpath("//span[@id = 'deposit-points-tooltip']");
	protected By textTooltipDepositPoint = By.xpath("//div[contains(text(),'points into RCI')]");
	protected By instructionalTextUseYearDeposit = By.xpath("//h2[text() = 'Deposit Points']/../p[contains(text(),'Select the use year')]");
	protected By areaUseYear = By.xpath("//div[contains(@class,'form deposits-rci')]");
	protected By totalUseYear = By.xpath("//div[contains(@class,'form deposits-rci')]/div//label[contains(@for,'use-year')]");
	protected By buttonContinue = By.xpath("//button[text() = 'Continue']");
	/*
	 * Method: verifyProgressBar Description:Verify Progress Bar in Point Deposit Page
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyProgressBar(){
		elementPresenceVal(pointDepositRCIProgessBar, "Progress Bar", "CUIRCIPointDepositPage", "verifyProgressBar");
	}
	
	
	/*
	 * Method: verifyBackRCIandDepositCTA Description:Verify Back RCI and Deposit CTA
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyBackRCIandDepositCTA(){
		elementPresenceVal(backRCIandDepositCTA, "Back to RCI and Deposit Link", "CUIRCIPointDepositPage", "verifyBackRCIandDepositCTA");
	}
	
	/*
	 * Method: verifyTextSelectDeposit Description:verify Header Select Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyTextSelectDeposit(){
		
		elementPresenceVal(headerSelectDeposit, "Select Deposit Header", "CUIRCIPointDepositPage", "verifyTextSelectDeposit");
	}
	
	/*
	 * Method: verifyRCIPointChart Description:verify RCI Point Chart
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyRCIPointChart(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(areaPointChartRCI)) {
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyRCIPointChart", Status.PASS,
					"Area Point Chart is Displayed");
			clickElementJSWithWait(areaPointChartRCI);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			elementPresenceVal(informationalTextDepositRCI, "Informational Text: "+getObject(informationalTextDepositRCI).getText(), "CUIRCIPointDepositPage", "verifyRCIPointChart");
			elementPresenceVal(disclaimerRCIDeposit, "Disclamer Text: "+getObject(disclaimerRCIDeposit).getText(), "CUIRCIPointDepositPage", "verifyRCIPointChart");
			validateTableRCIPoint();
			clickElementJSWithWait(areaPointChartRCI);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}else{
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyRCIPointChart", Status.FAIL,
					"Area Point Chart not Displayed");
		}
	}
	
	/*
	 * Method: validateTableRCIPoint Description:validate Table RCI Point
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void validateTableRCIPoint(){
		if (verifyObjectDisplayed(tablePointChart)) {
			int totalHeader = getList(By.xpath("//table//tr//th")).size();
			int totalValue = getList(By.xpath("//table//tr//td")).size();
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "validateTableRCIPoint", Status.PASS,
					"Table for RCI Point Present with Total number of header as: "+totalHeader+" and total number of values as: "+totalValue);
		}
		tcConfig.updateTestReporter("CUIRCIPointDepositPage", "validateTableRCIPoint", Status.PASS,
				"Table for RCI Point not Present");
	}
	
	/*
	 * Method: verifyHeaderTotalDepositAndUseYear Description:verify Header Total Deposit And UseYear
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHeaderTotalDepositAndUseYear(){
		elementPresenceVal(headerDepositPoints, "Header Deposit Points", "CUIRCIPointDepositPage", "verifyHeaderTotalDepositAndUseYear");
		elementPresenceVal(instructionalTextUseYearDeposit, "Text: "+getObject(instructionalTextUseYearDeposit).getText(), "CUIRCIPointDepositPage", "verifyHeaderTotalDepositAndUseYear");

	}
	/*
	 * Method: tooltipTotalDeposit Description:tooltip Total Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void tooltipTotalDeposit(){
		toolTipValidations(tooltipDepositPoint, "HouseKeeping Credit", textTooltipDepositPoint);
	}
	
	/*
	 * Method: totalUseYear Description:total Use Year
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void totalUseYear(){
		waitUntilElementVisibleBy(driver, areaUseYear, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(areaUseYear),"Use YearArea not displayed");
			if (getList(totalUseYear).size() > 0) {
				tcConfig.updateTestReporter("CUIRCIPointDepositPage", "totalUseYear", Status.PASS,
						"Toal Number of Use Year is: "+getList(totalUseYear).size());
				for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
					String textUseYear = getList(totalUseYear).get(useYearIterator).getText();
					if (textUseYear.isEmpty() || textUseYear == null) {
						tcConfig.updateTestReporter("CUIRCIPointDepositPage", "totalUseYear", Status.FAIL,
								"Use Year Text not Present for index: "+useYearIterator);
					}else{
						tcConfig.updateTestReporter("CUIRCIPointDepositPage", "totalUseYear", Status.PASS,
								"Use Year is: "+textUseYear);
					}
				}
			}else{
				tcConfig.updateTestReporter("CUIRCIPointDepositPage", "totalUseYear", Status.FAIL,
						"Use Year Not Found");
			}
	}
	/*
	 * Method: verifyPointAvailableRCIDeposit Description:verify Point Available RCI Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyPointAvailableRCIDeposit(){
		if (getList(totalUseYear).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
				String textUseYear = getList(totalUseYear).get(useYearIterator).getText();
				String totalPointAvailable = getObject(By.xpath("(//span[contains(text(),'Points Available')]/..)["+(useYearIterator+1)+"]//span[2]")).getText();
				if (totalPointAvailable.isEmpty() || totalPointAvailable == null) {
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyPointAvailableRCIDeposit", Status.FAIL,
							"Available Point not Present for index: "+(useYearIterator+1));
				}else{
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyPointAvailableRCIDeposit", Status.PASS,
							"Points: "+totalPointAvailable+" is available for Use Year: "+textUseYear);
				}
			}
		}else{
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyPointAvailableRCIDeposit", Status.FAIL,
					"Use Year Not Found");
		}
	}
	
	/*
	 * Method: verifyHousekeepingCreditRCIDeposit Description:verify Housekeeping Credit RCI Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHousekeepingCreditRCIDeposit(){
		if (getList(totalUseYear).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
				String textUseYear = getList(totalUseYear).get(useYearIterator).getText();
				String totalHousekeepingCredit = getObject(By.xpath("(//span[contains(text(),'Points Available')]/../following-sibling::p//span/..)["+(useYearIterator+1)+"]//span[2]")).getText();
				if (totalHousekeepingCredit.isEmpty() || totalHousekeepingCredit == null) {
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditRCIDeposit", Status.FAIL,
							"HouseKeeping Credit not Present for index: "+(useYearIterator+1));
				}else{
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditRCIDeposit", Status.PASS,
							"Housekeeping credit: "+totalHousekeepingCredit+" is available for Use Year: "+textUseYear);
				}
			}
		}else{
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditRCIDeposit", Status.FAIL,
					"Use Year Not Found");
		}
	}
	
	/*
	 * Method: verifyHousekeepingCreditNotPresentRCIDeposit Description:verify Housekeeping Credit not displayed RCI Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHousekeepingCreditNotPresentRCIDeposit(){
		if (getList(totalUseYear).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
				if (!verifyObjectDisplayed(getObject(By.xpath("(//span[contains(text(),'Points Available')]/../following-sibling::p//span/..)["+(useYearIterator+1)+"]//span[2]")))) {
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditNotPresentRCIDeposit", Status.PASS,
							"HouseKeeping Not Present for VIP Owner");
				}else{
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditNotPresentRCIDeposit", Status.FAIL,
							"HouseKeeping Present for VIP Owner");
				}
			}
		}else{
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditRCIDeposit", Status.FAIL,
					"Use Year Not Found");
		}
	}
	
	/*
	 * Method: selectUseYearRCIDeposit Description:select Use Year RCI Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void selectUseYearRCIDeposit(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement useyearSelect = getObject(By.xpath("//div[contains(@class,'form deposits-rci')]/div//label[contains(.,'"+useYearArray[useyearIterator]+"')]"));
			getElementInView(useyearSelect);
			clickElementJSWithWait(useyearSelect);
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "selectUseYearRCIDeposit", Status.PASS,
					"Clicked on User Year and selected User Year is: " + useYearArray[useyearIterator]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}	
	}
	
	/*
	 * Method: verifyHeaderPointToDeposit Description:verify Header Point To Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHeaderPointToDeposit(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement headerPointToDepoit = getObject(By.xpath("(//h6[text() = 'Points to Deposit'])["+(useyearIterator+1)+"]"));
			WebElement instructionalTextPointToDeposit = getObject(By.xpath("(//span[contains(text(),'Enter the number of points')])["+(useyearIterator+1)+"]"));
			elementPresenceVal(headerPointToDepoit, "Point To Deposit Header for index: "+(useyearIterator+1), "CUIRCIPointDepositPage", "verifyHeaderPointToDeposit");
			elementPresenceVal(instructionalTextPointToDeposit, "Instructional Text for index: "+(useyearIterator+1), "CUIRCIPointDepositPage", "verifyHeaderPointToDeposit");

		}
	}
	
	/*
	 * Method: totalPointsToDeposit Description:total Points To Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void totalPointsToDeposit(){
		String totalDepositPoint = testData.get("PointsToDeposit").trim();
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement areaForDepositPoint = getObject(By.xpath("(//label[contains(@for,'points-to-be-deposited')])["+(useyearIterator+1)+"]"));
			getElementInView(areaForDepositPoint);
			clickElementJSWithWait(areaForDepositPoint);
			//fieldDataEnter(areaForDepositPoint, totalDepositPoint);
			sendKeysBy(areaForDepositPoint, totalDepositPoint);
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}
	
	/*
	 * Method: verifyHousekeepingSectionRCIDeposit Description:verify Housekeeping Section RCI Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHousekeepingSectionRCIDeposit(){
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement areaForHousekeeping = getObject(By.xpath("(//div[contains(@class,'housekeeping__credits')])["+(useyearIterator+1)+"]"));
			if (verifyObjectDisplayed(areaForHousekeeping)) {
				getElementInView(areaForHousekeeping);
				WebElement housekeepingRequired = getObject(By.xpath("(//span[text() = 'Require to Deposit']/..)["+(useyearIterator+1)+"]//span[2]"));
				WebElement housekeepingAvailable = getObject(By.xpath("(//div[contains(@class,'housekeeping__credits')]//span[text() = 'Housekeeping Credits Available']/..)["+(useyearIterator+1)+"]//span[2]"));
				if (getElementText(housekeepingRequired).isEmpty() || getElementText(housekeepingRequired) == null || getElementText(housekeepingAvailable).isEmpty() || getElementText(housekeepingAvailable) == null) {
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingSectionRCIDeposit", Status.FAIL,
							"Either HuseKeeping Availabe or required is not displayed");
				}else{
					if (Integer.parseInt(getElementText(housekeepingRequired)) > 0) {
						tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingSectionRCIDeposit", Status.PASS,
								"Housekeeping Required is for deposit is: "+getElementText(housekeepingRequired)+" and available is: "+getElementText(housekeepingAvailable));
					}else{
						tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingSectionRCIDeposit", Status.FAIL,
								"Aount Entered but housekeeping value displayed is :"+getElementText(housekeepingRequired));
					}
				}
			}else{
				tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingSectionRCIDeposit", Status.FAIL,
						"HouseKeeping Section not found");
			}
		}
	}
	
	/*
	 * Method: verifyHousekeepingCreditPurchased Description:verify Housekeeping Credit Purchased
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHousekeepingCreditPurchased(){
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement areaForHousekeeping = getObject(By.xpath("(//div[contains(@class,'housekeeping__credits')])["+(useyearIterator+1)+"]"));
			if (verifyObjectDisplayed(areaForHousekeeping)) {
				getElementInView(areaForHousekeeping);
				WebElement housekeepingPurchased = getObject(By.xpath("(//span[text() = 'Credits to Purchase'])["+(useyearIterator+1)+"]/following-sibling::span"));
				
				if (getElementText(housekeepingPurchased).isEmpty() || getElementText(housekeepingPurchased) == null) {
					tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditPurchased", Status.FAIL,
							"Housekeeping Purchased value is not displayed");
				}else{
					if (Integer.parseInt(getElementText(housekeepingPurchased)) > 0) {
						tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditPurchased", Status.PASS,
								"Housekeeping Purchased is: "+getElementText(housekeepingPurchased));
					}else{
						tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditPurchased", Status.FAIL,
								"Required Housekeeping purchased not dispalyed properly i.e value :"+getElementText(housekeepingPurchased));
					}
				}
			}else{
				tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyHousekeepingCreditPurchased", Status.FAIL,
						"HouseKeeping Section not found");
			}
		}
	}
	
	/*
	 * Method: verifyCostHouskeepingPurchasedDeclaration Description:verify Cost Houskeeping Purchased Declaration
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyCostHouskeepingPurchasedDeclaration(){
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement declarationHousekeepingPurchased = getObject(By.xpath("(//span[contains(text(),'housekeeping credits needed')])["+(useyearIterator+1)+"]"));
			if (verifyObjectDisplayed(declarationHousekeepingPurchased)) {
				tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyCostHouskeepingPurchasedDeclaration", Status.PASS,
						"Housekeeping Purchased declaration is shown as: "+getElementText(declarationHousekeepingPurchased));
			}else{
				tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyCostHouskeepingPurchasedDeclaration", Status.FAIL,
						"Housekeeping Purchased declaration not displayed");
			}
		}
	}
	
	/*
	 * Method: verifyTotalCostHouskeepingPurchased Description:verify Total Cost Houskeeping Purchased
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyTotalCostHouskeepingPurchased(){
		double totalBreakdownAmountCalculated = 0;
		double individualTotalCost;
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement totalCostHousekeepingPurchased = getObject(By.xpath("(//span[text() = 'Total Cost'])["+(useyearIterator+1)+"]/following-sibling::span"));
				individualTotalCost = Double.parseDouble(getElementText(totalCostHousekeepingPurchased).split("\\$")[1].trim().replace(",", ""));
				totalBreakdownAmountCalculated = totalBreakdownAmountCalculated + individualTotalCost;
		}
		if (totalBreakdownAmountCalculated > 0) {
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyCostHouskeepingPurchasedDeclaration", Status.PASS,
					"Total Housekeeping Purchased is: "+totalBreakdownAmountCalculated);
		}else{
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyCostHouskeepingPurchasedDeclaration", Status.PASS,
					"Failed to Calculate total Housekeeping Purchased");
		}
	}
	
	/*
	 * Method: clickContinueButton Description:click Continue Button
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void clickContinueButton(){
		if (verifyObjectDisplayed(buttonContinue)) {
			getElementInView(buttonContinue);
			clickElementJSWithWait(buttonContinue);
		}
		
	}
}
