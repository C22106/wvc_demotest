package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUILoanAssessmentSuccessPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUILoanAssessmentSuccessPage_Web.class);

	public CUILoanAssessmentSuccessPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By textConfirnationPayment = By.xpath("//h1[text() = 'Payment Confirmation']");
	protected By headerThankyou = By.xpath("//h1[text() = 'Payment Confirmation']/../h2");
	protected By textTotalAmountPaymentMade = By.xpath("//h1[text() = 'Payment Confirmation']/../p");
	protected By linkGoBackDashboardLink = By.xpath("//a[text() = 'Go to My Dashboard']");
	protected By headerPaymentSummary = By.xpath("//h1[text() = 'Payment Summary']");
	protected By textContractNumber = By.xpath("//h3[text() = 'Contract']/../p/span");
	protected By loadingSpinner = By.xpath("//*[local-name()='svg' and @class='spinner -secondary undefined']");
	protected By accountPayaPal = By.xpath("//h2[text() = 'Payment Charges']/..//th");
	protected By payPalIcon = By.xpath("//img[@alt = 'PayPal icon']");
	protected By textTotalChargeDue = By.xpath("//th[text() = 'Total Payment Due']/../td/strong");
	protected By totalPaymentBreakdown = By.xpath("//h2[text() = 'Payment Charges']/..//tr");
	protected By accountPaidThrough = By.xpath("//img[@alt = 'PayPal icon']/../following-sibling::p/span");

	/*
	 * Method: verifyHeadersInSuccessPage Description:verify Headers In Success
	 * Page Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeadersInSuccessPage() {

		waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
		waitUntilElementVisibleBy(driver, headerPaymentSummary, "ExplicitLongWait");
		elementPresenceVal(textConfirnationPayment, "Header Confirmation Payment", "CUILoanAssessmentSuccessPage",
				"verifyHeadersInSuccessPage");
		elementPresenceVal(headerThankyou, "Text: " + getElementText(headerThankyou), "CUILoanAssessmentSuccessPage",
				"verifyHeadersInSuccessPage");
		elementPresenceVal(textTotalAmountPaymentMade, "Text: " + getElementText(textTotalAmountPaymentMade),
				"CUILoanAssessmentSuccessPage", "verifyHeadersInSuccessPage");
		elementPresenceVal(linkGoBackDashboardLink, "Link Go Back To DashBoard", "CUILoanAssessmentSuccessPage",
				"verifyHeadersInSuccessPage");
		elementPresenceVal(headerPaymentSummary, "Header Payment Summary", "CUILoanAssessmentSuccessPage",
				"verifyHeadersInSuccessPage");
	}

	/*
	 * Method: verifyContractNumber Description:verify Contract Number Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyContractNumber() {
		String contractNumber = testData.get("ContractNumber");
		if (contractNumber.equals(getElementText(textContractNumber))) {
			tcConfig.updateTestReporter("CUILoanAssessmentSuccessPage", "verifyContractNumber", Status.PASS,
					"Contract Number matched and the contract is: " + getElementText(textContractNumber));
		}
	}

	/*
	 * Method: verifyPayPalPaymentAccount Description:verify PayPal Payment
	 * Account Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyPayPalPaymentAccount() {
		elementPresenceVal(accountPayaPal, "Account: " + getElementText(accountPayaPal), "CUILoanAssessmentSuccessPage",
				"verifyPayPalPaymentAccount");
		elementPresenceVal(payPalIcon, "PayPal Icon", "CUILoanAssessmentSuccessPage", "verifyPayPalPaymentAccount");
		elementPresenceVal(accountPaidThrough, "Amount Paid Through: " + getElementText(accountPaidThrough),
				"CUILoanAssessmentSuccessPage", "verifyPayPalPaymentAccount");

	}

	/*
	 * Method: totalPaymentBreakdown Description:total Payment Breakdown Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void totalPaymentBreakdown() {
		for (int iteratorpaybreak = 1; iteratorpaybreak < getList(totalPaymentBreakdown).size(); iteratorpaybreak++) {
			String labelFor = getElementText(
					getObject(By.xpath("(//h2[text() = 'Payment Charges']/..//tr)[" + (iteratorpaybreak + 1) + "]/th")))
							.trim();
			String chargeValue = getElementText(getObject(
					By.xpath("(//h2[text() = 'Payment Charges']/..//tr)[" + (iteratorpaybreak + 1) + "]/td/strong")))
							.trim();
			if (labelFor.isEmpty() || labelFor == null || chargeValue.isEmpty() || chargeValue == null) {
				tcConfig.updateTestReporter("CUICompletePaymentPage", "totalPaymentBreakdown", Status.FAIL,
						"Either label for or charge value is not displayed");
			} else {
				tcConfig.updateTestReporter("CUICompletePaymentPage", "totalPaymentBreakdown", Status.PASS,
						"Charge For: " + labelFor + " is displayed as: " + chargeValue);
			}
		}
	}

	/*
	 * Method: validateTotalChargeDue Description:validate Total Charge Due Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTotalChargeDue() {
		if (verifyObjectDisplayed(textTotalChargeDue)) {
			tcConfig.updateTestReporter("CUICompletePaymentPage", "validateTotalChargeDue", Status.PASS,
					"Total Charge Due is: " + getElementText(textTotalChargeDue));
		} else {
			tcConfig.updateTestReporter("CUICompletePaymentPage", "validateTotalChargeDue", Status.FAIL,
					"Total Charge is not displayed");
		}
	}
}
