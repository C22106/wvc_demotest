package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICompleteModificationPage_Android extends CUICompleteModificationPage_Web {

	public static final Logger log = Logger.getLogger(CUICompleteModificationPage_Android.class);

	public CUICompleteModificationPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	public By expandReservationDetails=By.xpath("//button[@class = 'reservation-summary__headline-arrow']");
	public CUIModifyReservationPage_Android modifyPage_Android = new CUIModifyReservationPage_Android(tcConfig);
	public CUIBookPage_IOS bookPageObjectIOS = new CUIBookPage_IOS(tcConfig);
	
	/*
	 * Method: makePayment
	 * Description: makePayment
	 * Date: June/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void makePayment(){
		driver.navigate().refresh();
		String paymentMethod = testData.get("PaymentBy");
		bookPageObject.selectPaymentMethod();
		if (paymentMethod.contains("PayPal")) {
			bookPageObjectIOS.paymentViaPaypal();
		}else if (paymentMethod.contains("CreditCard")) {
			bookPageObjectIOS.dataEntryForCreditCard();
		}	
	}
	
	
	/*
	 * Method: validateSectionReservationSummary Description:validate Section Reservation Summary Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateSectionReservationSummary(){
		modifyPage_Android.checkSectionReservationSummary();
		modifyPage_Android.checkReservationSummaryDetails();
	}
	
	/*
	 * Method: updatedDateValidation Description: Verify Date is Updated Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void updatedDateValidation(String updatedDate) {
		clickElementBy(expandReservationDetails);
		Assert.assertTrue(getElementText(modifiedDate).trim().contains(testData.get(updatedDate)),
				"Updated Date not displayed in right window ");
		tcConfig.updateTestReporter("CUICompleteModificationPage", "updatedDateValidation", Status.PASS,
				"Updated Date is displayed as " + getElementText(modifiedDate));
	}
}