package cui.pages;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICreateAccountDetailsPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUICreateAccountDetailsPage_Web.class);

	public CUICreateAccountDetailsPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	// Account Details Page
	protected By accountDetailsHeader = By.xpath("//h2[contains(.,'ALMOST THERE')]");
	protected By accountDetailsPageDescription = By.xpath("//h2[contains(.,'ALMOST THERE')]/following-sibling::div/p");
	protected By accountDetailsModalHeader = By
			.xpath("(//h2[contains(.,'Add your account detail')] | //form/div[contains(.,'Add your account detail')])");
	protected By securityQuestionHeader = By
			.xpath("(//h2[contains(.,'Security Question')]| //form/div[contains(.,'Security Question')])");
	protected By usernameFieldText = By
			.xpath("//input[@id = 'username']/following-sibling::span[contains(.,'Username*')]");
	protected By emailAddressText = By
			.xpath("//input[@id = 'email']/following-sibling::span[contains(.,'Email Address*')]");
	protected By passwordText = By.xpath("//input[@id = 'password']/following-sibling::span[contains(.,'Password *')]");
	protected By answerFieldText = By.xpath("//input[@id]/following-sibling::span[contains(.,'Your Answer*')]");
	protected By questionText = By.xpath("//Select[@id]/following-sibling::span[contains(.,'Security Question')]");
	protected By usernameError = By.xpath("//p[@id='error-username']");
	protected By availableUserName = By.xpath("//p[contains(.,'Available')]/a");
	protected By userNameField = By.id("username");
	protected By emailField = By.id("email");
	protected By passwordField = By.id("password");
	protected By securityQuestion1 = By.xpath("//select[@name = 'q1']");
	protected By securityAnswer1 = By.name("q1Answer");
	protected By securityAnswer2 = By.name("q2Answer");
	protected By securityAnswer3 = By.name("q3Answer");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");
	protected By selectQuestion1 = By.xpath("//select[@name = 'q1']");
	protected By selectQuestion2 = By.xpath("//select[@name = 'q2']");
	protected By selectQuestion3 = By.xpath("//select[@name = 'q3']");

	/*
	 * Method: enterUsername Description:Enter Username Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void enterUsername(String usernameData, String strClassName) {
		getElementInView(userNameField);
		fieldDataEnter(userNameField, usernameData);
		sendKeyboardKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			changeTakenUsername(strClassName);
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIAccountDetailsPage", "enterUsername", Status.FAIL,
					"Error in username validation");
		}
	}

	/*
	 * Method: enterPassword Description:Enter Password Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void enterPassword(String passwordData) {
		getElementInView(passwordField);
		fieldDataEnter(passwordField, passwordData);
	}

	/*
	 * Method: enterEmail Description:Enter Email Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void enterEmail(String emailData) {
		getElementInView(emailField);
		fieldDataEnter(emailField, emailData);

	}

	/*
	 * Method: enterSecurityAnswer1 Description:Enter Security Answer 1 Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityAnswer1(String answer1) {
		getElementInView(getObject(selectQuestion1));
		Select q1drop = new Select(getObject(selectQuestion1));
		q1drop.selectByIndex(1);
		fieldDataEnter(securityAnswer1, answer1);
	}

	/*
	 * Method: enterSecurityAnswer2 Description:Enter Security Answer 2 Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityAnswer2(String answer2) {
		getElementInView(getObject(selectQuestion2));
		Select q2drop = new Select(getObject(selectQuestion2));
		q2drop.selectByIndex(2);
		fieldDataEnter(securityAnswer2, answer2);
	}

	/*
	 * Method: enterSecurityAnswer3 Description:Enter Security Answer 3 Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityAnswer3(String answer3) {
		getElementInView(getObject(selectQuestion3));
		Select q3drop = new Select(getObject(selectQuestion3));
		q3drop.selectByIndex(3);
		fieldDataEnter(securityAnswer3, answer3);
	}

	/*
	 * Method: changeTakenUsername Description: Change Taken Username Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void changeTakenUsername(String strClass)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		if (verifyObjectDisplayed(usernameError)) {
			// Available Username Data
			String strUsername = getElementText(availableUserName);
			tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "changeTakenUsername", Status.PASS,
					"Entered username is taken, selecting available username as: " + getElementText(availableUserName));
			clickElementBy(availableUserName);

			//writeDataInExcel(strClass, CUIScripts_Web.strTestName, "CUI_username", strUsername);
			testData.put("CUI_username", strUsername);
			tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "changeTakenUsername", Status.PASS,
					"Username has been changed to available username : "+strUsername);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "changeTakenUsername", Status.PASS,
					"Entered Username is Unique");
		}

	}

	/*
	 * Method: accountDetailsDataEntry Description: Account Details Page Data
	 * Entry Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void accountDetailsDataEntry(String strClassName) throws Exception {
		// Username from testdata
		String username = testData.get("Username").trim();
		// Password from testdata
		String password = testData.get("Password").trim();
		// Emai from testdata
		String emailAddress = testData.get("Email").trim();
		// Answer from testdata
		String answer = testData.get("Answer").trim();
		if (strClassName.contains("_")) {
			strClassName = strClassName.split("_")[0];
		}

		waitUntilElementVisibleBy(driver, userNameField, "ExplicitLongWait");
		if (verifyObjectDisplayed(userNameField)) {
			enterUsername(username, strClassName);
			enterEmail(emailAddress);
			enterPassword(password);
			enterSecurityAnswer1(answer);
			enterSecurityAnswer2(answer);
			enterSecurityAnswer3(answer);

			tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "accountDetailsDataEntry", Status.PASS,
					"Entered all the data in the fields");
		}
	}

	/*
	 * Method: accountDetailsPageValidations Description: Account Details Page
	 * Val Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void accountDetailsPageValidations() {

		waitUntilElementVisibleBy(driver, accountDetailsHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(accountDetailsHeader),"Account Details Header not present");
		elementPresenceVal(accountDetailsHeader, "Account Details Page Header", "CUICreateAccountDetailsPage",
				"accountDetailsPageVal");
		elementPresenceVal(accountDetailsPageDescription, "Account details Page Desc", "CUICreateAccountDetailsPage",
				"accountDetailsPageVal");
		elementPresenceVal(accountDetailsModalHeader, "Account details modal header", "CUICreateAccountDetailsPage",
				"accountDetailsPageVal");
		fieldPresenceVal(userNameField, "Username field", "CUICreateAccountDetailsPage", "accountDetailsPageVal",
				"with text " + getElementText(usernameFieldText));

		fieldPresenceVal(passwordField, "Password field", "CUICreateAccountDetailsPage", "accountDetailsPageVal",
				"with text " + getElementText(passwordText));

		fieldPresenceVal(emailField, "Email field", "CUICreateAccountDetailsPage", "accountDetailsPageVal",
				"with text " + getElementText(emailAddressText));

		elementPresenceVal(securityQuestionHeader, "Security Questions header", "CUICreateAccountDetailsPage",
				"accountDetailsPageVal");

		tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "accountDetailsPageVal", Status.PASS,
				getList(questionText).size() + " Security questions fields present with pre text Security Question #");

		tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "accountDetailsPageVal", Status.PASS,
				getList(answerFieldText).size() + " Answer Fields are present with pre text Your Answer*");

		elementPresenceVal(disabledContinueCTA, "Disabled continue CTA", "CUICreateAccountDetailsPage",
				"createAccountPageVal");

	}

}
