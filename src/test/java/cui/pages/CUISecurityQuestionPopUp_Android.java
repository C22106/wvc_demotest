package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUISecurityQuestionPopUp_Android extends CUISecurityQuestionPopUp_Web {

	public static final Logger log = Logger.getLogger(CUISecurityQuestionPopUp_Android.class);
	protected By securityQuestionLabel = By.xpath("//h6[contains(text(),'Security Question')]");

	public CUISecurityQuestionPopUp_Android(TestConfig tcconfig) {
		super(tcconfig);
		editSecurityLink = By.xpath("//*[@class='manage-security-questions']//button");
		securityCancelCTA = By.xpath("//*[@id='security-questions-modal']//button[contains(@class,'cancel-changes')]");
		closedSecurityModal = By.xpath("//div[@class='manage-security-questions']//button[contains(.,'Edit')]");
	}

	/*
	 * 
	 * /* Method: editAnswer1 Description: Edit Answer 1 field Pop Up Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void editAnswer1() {
		getElementInView(answer1Field);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer*", testData.get("Sec Que 1"), 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * 
	 * /* Method: securityCancelCTA Description: Close Manage Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void securityCancelCTA() {

		closePopUpModalAndroid(securityCancelCTA);

	}
	
	/*
	 * Method: enterAnswer1 Description: Edit Security Answer 1 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer1() {
		getElementInView(answer1Field);
		//sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer*","", 1);
		clickElementByScriptExecutor("Your Answer*",1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(securityQuestionLabel);
	}
	
	/*
	 * Method: enterAnswer2 Description: Edit Security Answer 2 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer2() {

		getElementInView(answer2Field);
		clickElementByScriptExecutor("Your Answer*",2);
		//sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer*","", 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(securityQuestionLabel);
	}
	
	/*
	 * Method: enterAnswer3 Description: Edit Security Answer 3 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer3() {

		getElementInView(answer3Field);
		clickElementByScriptExecutor("Your Answer*",3);
		//sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer*","", 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(securityQuestionLabel);
	}
}
