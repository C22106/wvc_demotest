package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIUsernameEmailMethodPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIUsernameEmailMethodPage_Web.class);

	public CUIUsernameEmailMethodPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	
	protected By checkEmailHeader = By.xpath("//h1[contains(.,'CHECK YOUR EMAIL')]");
	protected By checkEmailDescription = By.xpath("//h1[contains(.,'CHECK YOUR EMAIL')]/following-sibling::div//p");
	protected By enterCodeHeader = By.xpath("(//h2[contains(.,'Enter Your Code')] | //form/div[contains(.,'Enter Your Code')])");
	protected By verificationCodeField = By.xpath("//input[@id='verificationCode']");
	protected By newCodeText = By.xpath("//p[contains(.,'request a new one or contact us')]");
	protected By errorBlock = By.xpath("//div[@class='error-block']");
	protected By twoTryError = By.xpath("//div[@class='error-block']//p[contains(.,'try again')]");
	protected By thirdTryError = By.xpath("//p[@id='error-message']//p[contains(.,'two more attempts')]");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");
	
	/* Method: emailPageValidations Description:Email Page Validations Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void emailPageValidations() {

		waitUntilElementVisibleBy(driver, verificationCodeField,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(verificationCodeField),"Verification code field not present");
		elementPresenceVal(checkEmailHeader, "Check Your Email Header", "CUIUsernameEmailPage", "emailPageValidations");
		elementPresenceVal(checkEmailDescription, "Check Your Email Description", "CUIUsernameEmailMethodPage",
				"emailPageValidations");
		elementPresenceVal(enterCodeHeader, "Modal header", "CUIUsernameEmailMethodPage", "emailPageValidations");
		elementPresenceVal(verificationCodeField, "Code Field", "CUIUsernameEmailMethodPage", "emailPageValidations");
		elementPresenceVal(newCodeText, "Resent Code Text", "CUIUsernameEmailMethodPage", "emailPageValidations");
		elementPresenceVal(disabledContinueCTA, "Disabled Continue CTA", "CUIUsernameEmailMethodPage", "emailPageValidations");

	}

	/*
	 * Method: verificationCodeErrors Description:Verirification Code Error Val
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verificationCodeErrors() {
		//invalid data for verification	
		String strInvalidCode = testData.get("invalidData");

		for (int noOfTries = 0; noOfTries < 3; noOfTries++) {
			fieldDataEnter(verificationCodeField, strInvalidCode);
			clickContinue();
			waitUntilElementVisibleBy(driver, errorBlock,  "ExplicitLongWait");
			if (noOfTries < 2) {
				fieldPresenceVal(twoTryError, (noOfTries+1) + " try error ", "CUIUsernameEmailMethodPage", "emailPageVal",
						getElementText(twoTryError));
			} else if (noOfTries >= 2) {
				fieldPresenceVal(thirdTryError, (noOfTries+1) + "rd try error ", "CUIUsernameEmailMethodPage", "emailPageVal",
						getElementText(thirdTryError));
			} else {
				tcConfig.updateTestReporter("CUIUsernameEmailMethodPage", "emailPageVal", Status.FAIL,
						"Error Validations Failed");
			}

		}

	}
}