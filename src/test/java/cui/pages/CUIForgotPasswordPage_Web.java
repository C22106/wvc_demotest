package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIForgotPasswordPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIForgotPasswordPage_Web.class);

	public CUIForgotPasswordPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By forgotPasswordLink = By.xpath("//a[text() = 'Forgot Password']");
	// forgotPassword Page
	protected By forgotPasswordPageHeader = By.xpath("//h2[contains(.,'TIME FOR A NEW PASS')]");
	protected By forgotPasswordDescription = By
			.xpath("//div[contains(@class,'white') and contains(.,'simple steps to get back')]");
	protected By enterUsernameHeader = By
			.xpath("(//h2[contains(.,'Enter your username')] | //form/div[contains(.,'Enter your username')]) ");
	protected By personalDetailsHeader = By.xpath(
			"(//h2[contains(.,'Enter your personal details')] | //form/div[contains(.,'Enter your personal details')])");
	protected By companyDetailsHeader = By
			.xpath("(//h2[contains(.,'Enter the company')]| //form/div[contains(.,'Enter the company')])");
	protected By usernameTextBox = By.xpath("//input[@id='username']");
	protected By usernamePreText = By.xpath("//input[@id='username']/following-sibling::span[contains(.,'Username*')]");
	protected By forgotUserNameLink = By.xpath("//a[contains(.,'Forgot your username?')]");
	protected By firstNameTextBox = By.xpath("//input[@id='firstName']");
	protected By firstNamePreText = By
			.xpath("//input[@id='firstName']/following-sibling::span[contains(.,'First Name*')]");
	protected By lastNametextBox = By.xpath("//input[@id='lastName']");
	protected By lastNamePreText = By
			.xpath("//input[@id='lastName']/following-sibling::span[contains(.,'Last Name*')]");
	protected By continueButton = By.xpath("//button[contains(.,'Continue')]");
	protected By companyTrustText = By
			.xpath("(//p[contains(.,'company or trust')]| //form//div[contains(.,'company or trust')])");
	protected By clickHereLink = By.xpath(
			"(//p[contains(.,'company or trust')]//a[contains(.,'Click Here')]|//div[contains(.,'company or trust')]//a[contains(.,'Click Here')])");
	protected By userNameError = By.xpath("//p[@id='error-username']");
	protected By firstNameError = By.xpath("//p[@id='error-firstName']");
	protected By lastNameError = By.xpath("//p[@id='error-lastName']");
	protected By companyNameTextBox = By.xpath("//input[@id='companyName']");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");
	protected By memberTypePage = By
			.xpath("(//a[contains(.,'Here')]/ancestor::p | //a[contains(.,'Here')]/parent::div)");

	// companyField
	protected By companyField = By.xpath("//input[@id = 'companyName']");
	protected By companyFieldText = By
			.xpath("//input[@id = 'companyName']/following-sibling::span[contains(.,'Trust Name*')]");
	protected By forgotUsernameHeader = By.xpath("//h1[contains(.,'FORGOT YOUR USERNAME')]");

	/*
	 * Method: forgotPasswordNavigation Description: CUI Forgot Password page
	 * Navigation Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void forgotPasswordNavigation() {

		clickElementBy(forgotPasswordLink);

		waitUntilElementVisibleBy(driver, forgotPasswordPageHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(forgotPasswordPageHeader), "Forgot Password Header not present");
		elementPresenceVal(forgotPasswordPageHeader, "Forgot Password Header", "CUIForgotPasswordPage",
				"forgotPasswordNavigation");
		elementPresenceVal(forgotPasswordDescription, "Forgot Password Description", "CUIForgotPasswordPage",
				"forgotPasswordNavigation");

	}

	/*
	 * Method: usernameLinkCheck Description: Forgot Username Link Check
	 * Date:Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void usernameLinkCheck() {
		getElementInView(forgotUserNameLink);
		clickElementBy(forgotUserNameLink);

		waitUntilElementVisibleBy(driver, forgotUsernameHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(forgotUsernameHeader), "Forgot Username Header not present");

		tcConfig.updateTestReporter("CUIForgotPasswordPage", "usernameLinkCheck", Status.PASS,
				"Forgot Username page navigated present");

		navigateBack();
		waitUntilElementVisibleBy(driver, forgotPasswordPageHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(forgotPasswordPageHeader), "Forgot Password Header not present");
	}

	/*
	 * Method: enterNameDetails Description: CUI Forgot Password flow data entry
	 * Date Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterNameDetails() {

		if (testData.get("AccountAssociated").contains("Individual")) {
			fieldDataEnter(firstNameTextBox, testData.get("FirstName"));
			fieldDataEnter(lastNametextBox, testData.get("LastName"));
		} else if (testData.get("AccountAssociated").contains("Trust")) {
			// clickElementBy(clickHereLink);
			try {
				fieldDataEnter(companyNameTextBox, testData.get("CompanyName"));
			} catch (Exception e) {
				clickElementBy(clickHereLink);
				waitUntilElementVisibleBy(driver, companyNameTextBox, "ExplicitLongWait");
				fieldDataEnter(companyNameTextBox, testData.get("CompanyName"));
			}

		}
	}

	/*
	 * Method: enterUsername Description: CUI Forgot Password flow datat
	 * entryDate Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterUsername() {
		waitUntilElementVisibleBy(driver, usernameTextBox, "ExplicitLongWait");
		getElementInView(usernameTextBox);
		fieldDataEnter(usernameTextBox, testData.get("CUI_username"));

	}

	/*
	 * Method: passwordDeatilsValidations Description: Verification Methods Page
	 * Val Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordDetailsValidations() {
		// waitUntilElementVisibleBy(driver, emailMethod, "ExplicitLongWait");
		elementPresenceVal(enterUsernameHeader, "Enter Your Username Header", "CUIForgotPasswordPage",
				"passwordDetailsValidations");

		fieldPresenceVal(usernameTextBox, "Username Text Box", "CUIForgotPasswordPage", "passwordDetailsValidations",
				" with field text " + getElementText(usernamePreText));
		elementPresenceVal(forgotUserNameLink, "Forgot Your Username Link", "CUIForgotPasswordPage",
				"passwordDeatilsVal");
		if (getElementText(memberTypePage).toUpperCase().contains("COMPANY")) {
			fieldPresenceVal(firstNameTextBox, "First Name Text Box", "CUIForgotPasswordPage",
					"passwordDetailsValidations", " with field text " + getElementText(firstNamePreText));
			fieldPresenceVal(lastNametextBox, "First Name Text Box", "CUIForgotPasswordPage",
					"passwordDetailsValidations", " with field text " + getElementText(lastNamePreText));
			elementPresenceVal(personalDetailsHeader, "Enter Your Personal details Header", "CUIForgotPasswordPage",
					"passwordDetailsValidations");
			elementPresenceVal(companyTrustText, "Company Trust Text", "passwordDetailsValidations",
					"verificationPageVal");

		} else if (getElementText(memberTypePage).toUpperCase().contains("INDIVIDUAL")) {
			fieldPresenceVal(companyField, "Company Trust Field", "CUICreateAccountPage", "contractDetailsModalVal",
					"with text " + getElementText(companyFieldText));
			elementPresenceVal(companyDetailsHeader, "Enter Your Personal details Header", "CUIForgotPasswordPage",
					"passwordDetailsValidations");

		}

		elementPresenceVal(disabledContinueCTA, "Disabled continue CTA", "passwordDetailsValidations",
				"createAccountPageVal");

	}

}