package cui.pages;

import static org.testng.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIReservationDetailsPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIReservationDetailsPage_Web.class);

	public CUIReservationDetailsPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	CUICancelReservation_Web cancelPage = new CUICancelReservation_Web(tcConfig);
	protected By hkCreditValue = By.xpath("//*[@id='non-vip-housekeeping-credits-tooltip']/../following-sibling::span");
	protected By hkCredit = By.id("non-vip-housekeeping-credits-tooltip");
	protected By rentedPointLabel = By.xpath("//span[contains(text(),'Rented Points (Modification)')]");
	protected By rentedPointValue = By
			.xpath("//span[contains(text(),'Rented Points (Modification)')]/..//following-sibling::p/span");
	protected By modifyVIPText = By.xpath(
			"(//div[contains(@id,'opt-in-out')]//div[@role = 'tabpanel']//span)[1] | (//div[contains(@id,'vip_instant')]//div[@role = 'tabpanel']//span)[1]");
	protected By instantUpgrade = By
			.xpath("(//h4[contains(.,'SUITE TYPE')]/following-sibling::p[contains(.,'Upgrade')])/span");
	protected By specialRequest = By
			.xpath("//h4[contains(.,'SUITE TYPE')]/following-sibling::p[contains(.,'SPECIAL REQUEST')]//span");
	protected By totalValueArea = By.xpath("//span[text() = 'Payment Total']/../..");
	protected By totalAmountValue = By.xpath("(//span[text() = 'Payment Total']/../..//p/span)");
	protected By listPaymentSection = By.xpath("(//div[contains(@class,'charge-section')]/div)");
	protected By accountPaypal = By.xpath("//div[contains(@class,'payment-header')]/p/span");
	protected By headerPayment = By.xpath("//div[contains(@class,'payment-header')]");
	protected By paypalImage = By.xpath("//img[@alt = 'Paypal logo']");
	protected By headerPaymentCharges = By.xpath("//h4[contains(.,'PAYMENT CHARGES')]");
	protected By sectionCharges = By.xpath("//div[contains(@class,'charge-section')]");
	protected By textReservationNumber = By.xpath("//h4[text() = 'CONFIRMATION NUMBER']/../p");
	protected By sectionReservationSummary = By.xpath("//section[@class = 'reservation-details-summary']");
	protected By cancelledResortHeader = By.xpath("// div[contains(@class,'reservation')]//h1/a");
	protected By cancellationHeader = By
			.xpath("// div[contains(@class,'show')]//header[contains(@class,'cancellation')]");
	protected By cancellationCheckmark = By
			.xpath("// div[contains(@class,'show')]//header[contains(@class,'cancellation')]//*[local-name()='svg']");
	protected By cancellationDate = By
			.xpath("// div[contains(@class,'show')]//span[contains(.,'Date Cancel')]/following-sibling::span");
	protected By cancellationBy = By
			.xpath("// div[contains(@class,'show')]//span[contains(.,'Cancelled By')]/following-sibling::span");
	protected By headerChargesSummary = By.xpath("//h2[text() = 'Charges Summary']");
	protected By headerMebershipCharges = By.xpath("//h4[text() = 'Membership Charges']");
	protected By totalPointAreaMembershipCharges = By.xpath("//button[@id = 'points-accordion-label']");
	protected By housekeepingAreaMembershipCharges = By.xpath("//button[@id = 'housekeeping-credits-label']");
	protected By totalPointValues = By.xpath("//button[@id = 'points-accordion-label']/span");
	protected By totalHousekeepingValues = By.xpath("//button[@id = 'housekeeping-credits-label']/span");
	protected By totalReservationTransactionValues = By
			.xpath("//span[text() = 'Reservation Transaction']/../../..//p/span");
	protected By totalpointbreakdownArea = By.xpath("//div[@id = 'points-accordion']");
	protected By housekeepingbreakdownArea = By.xpath("//div[@id = 'housekeeping-credits']");
	protected By tooltipHousekeeping = By.xpath("//span[contains(@id,'housekeeping-credits-tooltip')]");
	protected By textTooltipHousekeeping = By.xpath("//div[contains(text(),'Housekeeping Credits')]");
	protected By tooltipReservationTransaction = By.xpath("//span[contains(@id,'reservation-transaction-tooltip')]");
	protected By textTooltipReservationTransaction = By.xpath("//div[contains(text(),'Reservation Transaction')]");

	protected By reimbursementHeader = By.xpath("// h2[contains(.,'Reimbursement')]");
	protected By forfeitHeader = By.xpath("// h2[contains(.,'Forfeit')]");
	protected By reimbursedCharges = By.xpath("// h4[contains(.,'Reimbursed Charges')]");
	protected By membershipCharges = By.xpath("// h4[contains(.,'Membership Charges')]");
	protected By totalReimbursePoints = By.xpath("// span[contains(.,'Points')]/following-sibling::span");
	protected By reimbursementPayment = By.xpath("(// h4[contains(.,'Reimbursed Pay')]| // h4[contains(.,'PAYMENT')])");
	protected By totalReimburseAmount = By.xpath("//span[contains(.,'Payment Total')]/../following-sibling::p/span");
	protected By pointSummaryLink = By.xpath("// section[contains(@class,'charges')]//a[contains(.,'Points Summary')]");
	protected By pointsSummaryPage = By.xpath("// h1[contains(.,'POINTS SUMMARY')]");

	protected By pointsAccordionText = By.xpath("//div[@id='points-accordion']//p[1]");
	protected By pointsAccordionValue = By.xpath("//div[@id='points-accordion']//p[2]");

	protected By modifyLink = By.xpath("//div[contains(@class,'modify-cancel')]//a");
	protected By cancelLink = By.xpath("//div[contains(@class,'modify-cancel')]//button");

	protected By rentedPoints = By
			.xpath("//div[contains(@class,'charge-section')]//span[contains(.,'Rent')]/../following-sibling::p/span");
	protected By housekeepingCredits = By
			.xpath("//div[contains(@class,'charge-section')]//span[contains(.,'House')]/../following-sibling::p/span");
	protected By allChargesText = By
			.xpath("//div[contains(@class,'charge-section')]//span[contains(.,'$')]/parent::p/preceding-sibling::p");

	protected By breadCrumbLink = By.xpath("//nav[contains(@class,'breadcrumb')]//li");
	protected By reservationBreadcrumb = By.xpath("//nav[contains(@class,'breadcrumb')]//span");

	protected By resortName = By.xpath("//h1/a");
	protected By checkInDate = By.xpath("//span[contains(@class,'check-in')]");
	protected By checkOutDate = By.xpath("// h5[contains(.,'CHECK-OUT')]/following-sibling::p/span[1]");
	protected By checkInDateModified = By.xpath("// h5[contains(.,'CHECK-IN')]/following-sibling::p/span[1]");
	protected By resortAddress = By.xpath("//address[contains(@class,'address')]");
	protected By resortPhone = By.xpath("//a[contains(@class,'text') and contains(@href,'tel:')]");
	protected By viewMapLink = By.xpath("//a[contains(.,'View on Map')]");

	// reservationSummary
	protected By reservationSummaryHeader = By.xpath("//div[contains(@class,'cell')]//h2[.='Reservation Summary']");
	protected By travelerDetailsHeader = By.xpath("//h3[contains(.,'Traveler Details')]");
	protected By memberNumber = By.xpath("//h4[contains(.,'MEMBER NUMBER')]/following-sibling::p");
	protected By travellerName = By.xpath("//h4[contains(.,'Traveler')]/following-sibling::p");
	protected By email = By.xpath("//h4[contains(.,'EMAIL')]/following-sibling::p");

	protected By reservationDetailsHeader = By.xpath("//h3[contains(.,'Reservation Details')]");
	protected By reservationNumber = By.xpath("//h4[contains(.,'CONFIRMATION NUMBER')]/following-sibling::p");
	protected By suiteCost = By.xpath("//h4[contains(.,'SUITE COST')]/following-sibling::p");
	protected By suiteType = By.xpath("(//h4[contains(.,'SUITE TYPE')]/following-sibling::p[contains(.,'room')])");
	protected By pointsProtection = By.xpath(
			"//h4[contains(.,'POINTS PROTECTION')]/following-sibling::p | //h4[contains(.,'POINTS PROTECTION')]/..//div/p");
	protected By reservationMadeOn = By.xpath("//h4[contains(.,'MADE ON')]/following-sibling::p");
	protected By reservationMadeBy = By.xpath("//h4[contains(.,'MADE BY')]/following-sibling::p");
	protected By navigatedResortPage = By
			.xpath("(// h1[contains(@class,'title-1')] | // div[contains(@class,'title-1 m')])");
	protected By mapPageHeader = By.xpath("//div[@class='section-hero-header-title-description']//h1");

	// cancellation
	protected By cancellationPolicyHeader = By
			.xpath("//div[contains(@class,'show')]//h3[contains(.,'CANCELLATION POLICY')]");
	protected By cancellationPolicyDescription = By
			.xpath("//div[contains(@class,'show')]//h3[contains(.,'CANCELLATION POLICY')]/following-sibling::p");
	protected By viewCancellationPolicyLink = By.xpath("//div[contains(@class,'show')]//a[contains(.,'Cancellation')]");
	protected By fullCancelPolicyPage = By.xpath("//h3[contains(.,'Canceling')]");

	protected By houseKeepingCredits = By.xpath("//span[contains(.,'Housekeeping')]/../following-sibling::p/span");
	protected By reservationTransaction = By.xpath("//span[contains(.,'Transaction')]/../following-sibling::p/span");

	protected By paymentDoneBy = By.xpath("//div[contains(@class,'charges-summary__payment-header')]/p[1]//span");
	protected By paymentMethodImage = By.xpath("//div[contains(@class,'charges-summary__payment-header')]/p//img");
	protected By cardDetails = By.xpath("// div[contains(@class,'charges-summary__payment-header')]/p[2]//span");
	protected By chargedForText = By
			.xpath("//div[contains(@class,'charge-section')]//span[contains(.,'$')]/parent::p/preceding-sibling::p");
	protected By chargedAmount = By.xpath("//div[contains(@class,'charge-section')]//span[contains(.,'$')]");

	// modification

	protected By modificationHistoryList = By.xpath("//div[contains(@class,'holiday modification-history')]");
	protected By totalAccordion = By.xpath("//div[contains(@class,'accordion-item')]");
	protected By allModificationDates = By.xpath(
			"//div[contains(@class,'holiday modification-history')]//a[contains(@id,'accordion-label')]//p/span");
	protected By textModification = By.xpath("//p[text() = 'Modifications']");
	protected By modificationHistoryHeader = By.xpath("//h2[contains(.,'Modification History')]");

	// add Nights
	protected By addNightsAccordion = By.xpath("//div[@class='accordion-item']//div[text()='Add Nights']");
	protected By addNightsNotes = By
			.xpath("//div[text()='Add Nights']//ancestor::div[contains(@class,'accordion-item')]//div/p/span");
	protected By addNightsText = By.xpath(
			"//div[text()='Add Nights']//ancestor::div[contains(@class,'accordion-item')]//div/p/span[contains(.,'night')]");
	protected By modifiedByText = By.xpath(
			"//div[text()='Add Nights']//ancestor::div[contains(@class,'accordion-item')]//div/p/span[contains(.,'Modified')]");
	protected By pointsProtectionText = By.xpath(
			"//div[text()='Add Nights']//ancestor::div[contains(@class,'accordion-item')]//div/p/span[contains(.,'Points')]");
	protected By paymentTotalLabel = By.xpath("//span[text()='Payment Total']");
	protected By paymentTotalValue = By.xpath("//span[text()='Payment Total']/..//following-sibling::p/span");
	protected By paymentGrandTotalLabel = By.xpath("//span[text()='Payment Grand Total']");
	protected By paymentGrandTotalValue = By.xpath("//span[text()='Payment Grand Total']/..//following-sibling::p/span");
	
	protected By hkModificationLabel = By
			.xpath("//span[text()='Housekeeping Credits (Modification)']/..//following-sibling::p/span");
	protected By hkModificationValue = By.xpath("//span[text()='Housekeeping Credits (Modification)']");

	protected By ppModificationLabel = By
			.xpath("//span[text()='Points Protection (Modification)']/..//following-sibling::p/span");
	protected By ppModificationValue = By.xpath("//span[text()='Points Protection (Modification)']");

	/*
	 * Method: cancelledReservationName Description: Reservation Resort Name
	 * Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelledReservationName() {

		waitUntilElementVisibleBy(driver, cancelledResortHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(cancelledResortHeader), "Cancelled Resort header not present");
		// reservation Resort Name from test data
		String strReservationName = testData.get("ReservationResortName").toUpperCase();
		if (getElementText(cancelledResortHeader).toUpperCase().contains(strReservationName)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelledReservationName", Status.PASS,
					"Cancelled Reservation Resort Name Displayed correctly " + strReservationName);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelledReservationName", Status.FAIL,
					"Cancelled  Reservation Resort name not Displayed correctly " + strReservationName);
		}
	}

	/*
	 * Method: cancelledReservationContentBox Description: Reservation Content
	 * Box Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelledReservationContentBox() {
		elementPresenceVal(cancellationHeader, "Cancellation Header ", "CUIReservationDetailsPage",
				"cancelledReservationContentBox");
		elementPresenceVal(cancellationCheckmark, "Cancellation Check Mark  ", "CUIReservationDetailsPage",
				"cancelledReservationContentBox");

	}

	/*
	 * Method: cancelledReservationContentBox Description: Reservation Content
	 * Box Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelledDateValidation() throws Exception {
		String cancelledDate = getElementText(cancellationDate);
		Date convertedDate = convertStringToDate(cancelledDate, "MM/dd/yyyy");
		String cancelledOn = testData.get("ReservationCancelledDate");

		if (cancelledOn.equalsIgnoreCase("Today")) {
			currentDateValidation(cancelledDate);
		} else if (cancelledOn.contains("/")) {
			previousDateValidation(cancelledOn, convertedDate);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelledDateValidation", Status.FAIL,
					"Error in Verifying Cancelled Dates");
		}

	}

	/*
	 * Method: cancelledReservationContentBox Description: Reservation Content
	 * Box Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void currentDateValidation(String cancelledDate) throws Exception {
		if (dateDifference("MM/dd/yyyy", getCurrentDateInSpecificFormat("MM/dd/yyyy"), cancelledDate) == 0) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "currentDateValidation", Status.PASS,
					"Today's date is displayed Correctly");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "currentDateValidation", Status.FAIL,
					"Today's date is not displayed correctly");
		}
	}

	/*
	 * Method: previousDateValidation Description: Reservation Content Box Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void previousDateValidation(String cancelledOn, Date applicationDate) throws ParseException {
		Date cancelledDate = convertStringToDate(cancelledOn, "MM/dd/yyyy");

		if (cancelledDate.equals(applicationDate)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "previousDateValidation", Status.FAIL,
					"Cancelled Previous date is displayed Correctly");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "previousDateValidation", Status.FAIL,
					"Cancelled Previous is not displayed correctly");
		}
	}

	/*
	 * Method: reservationCancelledBy Description: Reservation Cancelled By
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationCancelledBy() {

		if (getElementText(cancellationBy).toUpperCase().contains(testData.get("FirstName").toUpperCase())) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "reservationCancelledBy", Status.PASS,
					"Reservation Cancelled By displayed correctly" + getElementText(cancellationBy));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "reservationCancelledBy", Status.FAIL,
					"Reservation Cancelled By is not displayed correctly " + getElementText(cancellationBy));
		}

	}

	/*
	 * /* Method: reimbursedCharges Description: Reimbursed Charges Validation
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reimbursedCharges() {

		Assert.assertTrue(verifyObjectDisplayed(reimbursementHeader), "Reimbursement header not present");
		getElementInView(reimbursementHeader);
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "reimbursedCharges", Status.PASS,
				"Reimbursed Header displayed");
		elementPresenceVal(reimbursedCharges, "Reimbursed Charges Header ", "CUIReservationDetailsPage",
				"reimbursedCharges");
		// totalPointsValidation();

	}

	/*
	 * /* Method: forfeitedCharges Description: Forfeited Charges Validation
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void forfeitedCharges() {

		Assert.assertTrue(verifyObjectDisplayed(forfeitHeader), "Forfeit Header not present");
		getElementInView(forfeitHeader);
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "forfeitedCharges", Status.PASS,
				"Forfeit Header displayed");
		elementPresenceVal(membershipCharges, "Forfeit Charges Header ", "CUIReservationDetailsPage",
				"forfeitedCharges");
		if (verifyObjectDisplayed(totalReimbursePoints)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "forfeitedCharges", Status.PASS,
					"Forfeit Points displayed as : " + getElementText(totalReimbursePoints));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "forfeitedCharges", Status.FAIL,
					"Forfeit Points not displayed" + getElementText(totalReimbursePoints));
		}

	}

	/*
	 * Method: reimbursedPayment Description: Reimbursed Payment Validation
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reimbursedPayment() {
		if (testData.get("PaymentReimburse").contains("Yes")) {
			Assert.assertTrue(verifyObjectDisplayed(reimbursementPayment), "Reimbursement Payment header not present");
			getElementInView(reimbursementPayment);
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "reimbursedPayment", Status.PASS,
					"Reimbursed Payment Header displayed");

			paymentTotalValidations();
		} else if (testData.get("PaymentReimburse").contains("No")) {
			paymentChargesNotPresent();
		}
	}

	/*
	 * Method: modifyLinkNotPresent Description: Verify that Modify Link is not
	 * present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void modifyLinkNotPresent() {
		if (verifyObjectDisplayed(modifyLink)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "modifyLinkNotPresent", Status.FAIL,
					"Modify Link present in the Page");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "modifyLinkNotPresent", Status.PASS,
					"Modify Link not present in the page");
		}
	}

	/*
	 * Method: cancelLinkNotPresent Description: Verify that Cancel Links is not
	 * present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelLinkNotPresent() {
		if (verifyObjectDisplayed(cancelLink)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelLinkNotPresent", Status.FAIL,
					"Cancel Link present in the page");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelLinkNotPresent", Status.PASS,
					"Cancel Link not present in the page");
		}
	}

	/*
	 * Method: pointsDropDownValidation Description: Points Drop Down
	 * Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsDropDownValidation() {

		clickElementBy(totalReimbursePoints);
		waitUntilElementVisibleBy(driver, pointsAccordionText, "ExplicitLongWait");
		List<WebElement> dropDownList = getList(pointsAccordionText);
		if (dropDownList.size() > 0) {
			for (int textCount = 0; textCount < dropDownList.size(); textCount++) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "pointsDropDownValidation", Status.PASS,
						"Points Drop Down Text displayed as: " + (dropDownList).get(textCount).getText()
								+ " with points "
								+ cancelPage.distributedPointsValidation(textCount, pointsAccordionValue));
			}
			pointsDropDownTextValidation();
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "pointsDropDownValidation", Status.FAIL,
					"Points Drop Down Text not present");
		}
	}

	/*
	 * Method: pointsDropDownTextValidation Description: Points Drop Down
	 * Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsDropDownTextValidation() {
		if (testData.get("PointsTextValidation").contains("Yes")) {
			List<WebElement> dropDownList = getList(pointsAccordionText);

			for (int textCount = 0; textCount < dropDownList.size(); textCount++) {
				if ((dropDownList).get(textCount).getText().toLowerCase()
						.contains(testData.get("PointsText").toLowerCase())) {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "pointsDropDownTextValidation",
							Status.PASS,
							"Points Drop Down Text displayed as: " + (dropDownList).get(textCount).getText()
									+ " with points "
									+ cancelPage.distributedPointsValidation(textCount, pointsAccordionValue));
				} else if (textCount + 1 == dropDownList.size()) {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "pointsDropDownTextValidation",
							Status.FAIL,
							testData.get("PointsTextValidation") + " Points Drop Down Text not present");
				}
			}
		}
	}

	/*
	 * Method: verifySectionReservationSummary Description: Verify Section
	 * Reservation Summary Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifySectionReservationSummary() {
		String reservationNumber = testData.get("ReservationNumber");
		if (verifyObjectDisplayed(sectionReservationSummary)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionReservationSummary", Status.PASS,
					"Reservation Summary Section Displayed Successfully");
			if (getElementText(textReservationNumber).trim().equals(reservationNumber)) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionReservationSummary",
						Status.PASS,
						"Reservation Number displayed correctly as: " + getElementText(textReservationNumber));
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionReservationSummary",
						Status.FAIL, "Reservation Number not displayed correctly ");
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionReservationSummary", Status.FAIL,
					"Reservation Summary Section not displayed");
		}
	}

	/*
	 * Method: verifySectionPaymentChargesNotDisplayed Description: Verify
	 * Section Payment Charges not Displayed Date: June/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	public void verifySectionPaymentChargesNotDisplayed() {
		if (!verifyObjectDisplayed(headerPaymentCharges)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionPaymentChargesNotDisplayed",
					Status.PASS, "Header Payment Charges Not Displayed as No Payment is Done");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionPaymentChargesNotDisplayed",
					Status.FAIL, "Header Payment Charges is Displayed even No Payment is Done");
		}

		if (!verifyObjectDisplayed(sectionCharges)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionPaymentChargesNotDisplayed",
					Status.PASS, "Section Payment Charges Not Displayed as No Payment is Done");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionPaymentChargesNotDisplayed",
					Status.FAIL, "Section Payment Charges is Displayed even No Payment is Done");
		}
	}

	/*
	 * Method: verifyHeaderPaymentChargesSectionDisplayed Description: Verify
	 * Section Payment Charges Displayed Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyHeaderPaymentChargesSectionDisplayed() {
		if (verifyObjectDisplayed(headerPaymentCharges)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyHeaderPaymentChargesSectionDisplayed",
					Status.PASS, "Header Payment Charges Displayed as Payment is Done");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyHeaderPaymentChargesSectionDisplayed",
					Status.FAIL, "Header Payment Charges not Displayed even Payment is Done");
		}
		Assert.assertTrue(verifyObjectDisplayed(sectionCharges), "Section Charges not present");
		getElementInView(sectionCharges);
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifySectionPaymentChargesNotDisplayed",
				Status.PASS, "Section Payment Charges is Displayed as Payment is Done");
	}

	/*
	 * Method: accountPaypalVerification Description: Paypal Account
	 * Verification Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void accountPaypalVerification() {
		if (verifyObjectDisplayed(headerPayment)) {
			getElementInView(headerPayment);
			if (getList(accountPaypal).get(1).getText().contains("PayPal")) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "accountPaypalVerification", Status.PASS,
						"Payment is Done By Paypal is Displayed");
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "accountPaypalVerification", Status.FAIL,
						"Payment is Done By Paypal is not Displayed, check");
			}

			if (verifyObjectDisplayed(getList(accountPaypal).get(0))) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "accountPaypalVerification", Status.PASS,
						"Payment Account Used is: " + getList(accountPaypal).get(0).getText());
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "accountPaypalVerification", Status.FAIL,
						"Payment Account Used is not displayed");
			}
			elementPresenceVal(paypalImage, "PayPal Image", "CUIReservationDetailsPage", "accountPaypalVerification");
		}

	}

	/*
	 * Method: verifyAmountsPaid Description: Amount Paid vai charges Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyAmountsPaid() {
		if (getList(listPaymentSection).size() > 0) {
			getElementInView(listPaymentSection);
			for (int iteratorPaymentPaid = 2; iteratorPaymentPaid <= getList(listPaymentSection)
					.size(); iteratorPaymentPaid++) {
				if (verifyObjectDisplayed(getList(
						By.xpath("(//div[contains(@class,'charge-section')]/div)[" + iteratorPaymentPaid + "]//span"))
								.get(0))
						&& verifyObjectDisplayed(getList(By.xpath(
								"(//div[contains(@class,'charge-section')]/div)[" + iteratorPaymentPaid + "]//span"))
										.get(1))) {
					String AmountFor = getElementText(getList(By
							.xpath("(//div[contains(@class,'charge-section')]/div)[" + iteratorPaymentPaid + "]//span"))
									.get(0));
					String Amount = getElementText(getList(By
							.xpath("(//div[contains(@class,'charge-section')]/div)[" + iteratorPaymentPaid + "]//span"))
									.get(1));
					if (AmountFor.isEmpty() || AmountFor == null || Amount.isEmpty() || Amount == null) {
						tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyAmountsPaidByPayPal",
								Status.FAIL, "Amount for or Amount may not be displayed in the List");
					} else {
						tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyAmountsPaidByPayPal",
								Status.PASS, "Amount :  " + Amount + " is Paid For : " + AmountFor);
					}

				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyAmountsPaidByPayPal",
							Status.FAIL, "Amount for or Amount may not be displayed in the List");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyAmountsPaidByPayPal", Status.FAIL,
					"Payment Section not displayed");
		}
	}

	/*
	 * Method: verifyTotalAmountDisplayedAndCalculated Description: Charges
	 * Aount paid and calculated Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyTotalAmountDisplayedAndCalculated() {
		double totalBreakdownAmountCalculated = 0;
		double amountPaid = 0;
		double breakdownAmount;
		if (verifyObjectDisplayed(totalValueArea)) {
			amountPaid = Double
					.parseDouble(getList(totalAmountValue).get(1).getText().split("\\$")[1].trim().replace(",", ""));
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalAmountDisplayedAndCalculated",
					Status.PASS, "Total Amount Paid : " + amountPaid);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalAmountDisplayedAndCalculated",
					Status.FAIL, "Total Amount Paid Area did not Displayed");
		}
		if (getList(listPaymentSection).size() > 0) {
			for (int iteratorPaymentPaid = 2; iteratorPaymentPaid <= getList(listPaymentSection)
					.size(); iteratorPaymentPaid++) {
				if (verifyObjectDisplayed(getList(
						By.xpath("(//div[contains(@class,'charge-section')]/div)[" + iteratorPaymentPaid + "]//span"))
								.get(1))) {
					breakdownAmount = Double.parseDouble(getList(By
							.xpath("(//div[contains(@class,'charge-section')]/div)[" + iteratorPaymentPaid + "]//span"))
									.get(1).getText().split("\\$")[1].trim().replace(",", ""));
					totalBreakdownAmountCalculated = totalBreakdownAmountCalculated + breakdownAmount;
				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalAmountDisplayedAndCalculated",
							Status.FAIL, "Paypal Amount not displayed in the List");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalAmountDisplayedAndCalculated",
					Status.FAIL, "Payment Section not displayed");
		}
		if (amountPaid == totalBreakdownAmountCalculated) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalAmountDisplayedAndCalculated",
					Status.PASS, "Total Amount Paid : " + amountPaid + " and Total Amount Calculated: "
							+ totalBreakdownAmountCalculated + " Matched");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalAmountDisplayedAndCalculated",
					Status.FAIL, "Total Amount Paid : " + amountPaid + " and Total Amount Calculated: "
							+ totalBreakdownAmountCalculated);
		}
	}

	/*
	 * Method: verifyHeaderChargesSummary Description: verify Header Charges
	 * Summary Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeaderChargesSummary() {
		elementPresenceVal(headerChargesSummary, "Charges Summary", "CUIReservationDetailsPage",
				"verifyHeaderChargesSummary");
	}

	/*
	 * Method: verifyHeaderMembershipCharges Description: verify Header
	 * Membership Charges Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeaderMembershipCharges() {
		elementPresenceVal(headerMebershipCharges, "Membership Charges", "CUIReservationDetailsPage",
				"verifyHeaderChargesSummary");

	}

	/*
	 * Method: membershipChargesTotalPointAreaCollapseForm
	 * Description:membership Charges Total Point Area Collapse Form Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void membershipChargesTotalPointAreaCollapseForm() {
		if (verifyObjectDisplayed(totalPointAreaMembershipCharges)) {
			getElementInView(totalPointAreaMembershipCharges);
			if (getObject(totalPointAreaMembershipCharges).getAttribute("aria-expanded").equals("false")) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointAreaCollapseForm",
						Status.PASS, "Total Point Area is in collapsed form by default");

			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointAreaCollapseForm",
						Status.FAIL, "Total Point Area is not in collapsed form by default");
			}
		}
	}

	/*
	 * Method: membershipChargesTotalPointValidation Description:membership
	 * Charges Total Point Validation Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void membershipChargesTotalPointValidation() {
		if (verifyObjectDisplayed(getList(totalPointValues).get(0))
				&& verifyObjectDisplayed(getList(totalPointValues).get(1))) {
			String textTotalPoints = getElementText(getList(totalPointValues).get(0));
			String valueTotalPoints = getElementText(getList(totalPointValues).get(1));
			if (textTotalPoints.isEmpty() || textTotalPoints == null || valueTotalPoints.isEmpty()
					|| valueTotalPoints == null) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointValidation",
						Status.FAIL, "Either Total Points text or Amount not displayed");
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointValidation",
						Status.PASS, textTotalPoints + " required: " + valueTotalPoints);
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointValidation",
					Status.FAIL, "Either Total Points text or Amount not displayed");
		}
	}

	/*
	 * Method: membershipChargesTotalPointBreakDown Description:membership
	 * Charges Total Point BreakDown Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void membershipChargesTotalPointBreakDown() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(totalPointAreaMembershipCharges);
		if (getList(totalpointbreakdownArea).size() > 0) {
			getElementInView(totalpointbreakdownArea);
			for (int totalPointIterator = 1; totalPointIterator < getList(totalpointbreakdownArea)
					.size(); totalPointIterator++) {
				if (verifyObjectDisplayed(
						getList(By.xpath("//div[@id = 'points-accordion']/div[" + totalPointIterator + "]//span"))
								.get(0))
						&& verifyObjectDisplayed(getList(
								By.xpath("//div[@id = 'points-accordion']/div[" + totalPointIterator + "]//span"))
										.get(1))) {
					String AmountForm = getElementText(
							getList(By.xpath("//div[@id = 'points-accordion']/div[" + totalPointIterator + "]//span"))
									.get(0));
					String Amount = getElementText(
							getList(By.xpath("//div[@id = 'points-accordion']/div[" + totalPointIterator + "]//span"))
									.get(1));
					if (AmountForm.isEmpty() || AmountForm == null || Amount.isEmpty() || Amount == null) {
						tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
								Status.FAIL, "Amount from or Amount may not be displayed in the List");
					} else {
						tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
								Status.PASS, "Amount :  " + Amount + " is Paid From : " + AmountForm);
					}

				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
							Status.FAIL, "Amount from or Amount may not be displayed in the List");
				}
			}
			clickElementJSWithWait(totalPointAreaMembershipCharges);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
					Status.FAIL, "Total Price BreakDown Area Not present");
		}
	}

	/*
	 * Method: membershipChargesHouseKeepingAreaCollapseForm
	 * Description:membership Charges HouseKeeping Area Collapse Form Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void membershipChargesHouseKeepingAreaCollapseForm() {
		if (verifyObjectDisplayed(housekeepingAreaMembershipCharges)) {
			getElementInView(housekeepingAreaMembershipCharges);
			if (getObject(housekeepingAreaMembershipCharges).getAttribute("aria-expanded").equals("false")) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage",
						"membershipChargesHouseKeepingAreaCollapseForm", Status.PASS,
						"Housekeeping Area is in collapsed form by default");

			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage",
						"membershipChargesHouseKeepingAreaCollapseForm", Status.FAIL,
						"Housekeeping Area is not in collapsed form by default");
			}
		}
	}

	/*
	 * Method: membershipChargesHousekeepingPointValidation
	 * Description:membership Charges Housekeeping Point Validation Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void membershipChargesHousekeepingPointValidation() {
		if (verifyObjectDisplayed(getList(totalHousekeepingValues).get(1))) {
			String valueHousekeepingPoints = getElementText(getList(totalHousekeepingValues).get(1));
			if (valueHousekeepingPoints.isEmpty() || valueHousekeepingPoints == null) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesHousekeepingPointValidation",
						Status.FAIL, "Total Housekeeping value required not displayed");
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesHousekeepingPointValidation",
						Status.PASS, "Total Housekeeping required is: " + valueHousekeepingPoints);
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesHousekeepingPointValidation",
					Status.FAIL, "Total Housekeeping value required not displayed");
		}
	}

	/*
	 * Method: membershipChargesHousekeepingPointBreakDown
	 * Description:membership Charges Housekeeping Point BreakDown Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void membershipChargesHousekeepingPointBreakDown() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(housekeepingAreaMembershipCharges);
		if (getList(housekeepingbreakdownArea).size() > 0) {
			getElementInView(housekeepingbreakdownArea);
			for (int housekeepingIterator = 1; housekeepingIterator < getList(housekeepingbreakdownArea)
					.size(); housekeepingIterator++) {
				if (verifyObjectDisplayed(
						getList(By.xpath("//div[@id = 'housekeeping-credits']/div[" + housekeepingIterator + "]//span"))
								.get(0))
						&& verifyObjectDisplayed(getList(
								By.xpath("//div[@id = 'points-accordion']/div[" + housekeepingIterator + "]//span"))
										.get(1))) {
					String AmountForm = getElementText(
							getList(By.xpath("//div[@id = 'points-accordion']/div[" + housekeepingIterator + "]//span"))
									.get(0));
					String Amount = getElementText(
							getList(By.xpath("//div[@id = 'points-accordion']/div[" + housekeepingIterator + "]//span"))
									.get(1));
					if (AmountForm.isEmpty() || AmountForm == null || Amount.isEmpty() || Amount == null) {
						tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
								Status.FAIL,
								"Housekeeping Amount from or Housekeeping Amount may not be displayed in the List");
					} else {
						tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
								Status.PASS, "Housekeeping Amount :  " + Amount + " is Paid From : " + AmountForm);
					}

				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
							Status.FAIL,
							"Housekeeping Amount from or Housekeeping Amount may not be displayed in the List");
				}
			}
			clickElementJSWithWait(housekeepingAreaMembershipCharges);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesTotalPointBreakDown",
					Status.FAIL, "Housekeeping Price BreakDown Area Not present");
		}
	}

	/*
	 * Method: membershipChargesReservationTransactionPointValidation
	 * Description:membership Charges Reservation Transaction Point Validation
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void membershipChargesReservationTransactionPointValidation() {
		if (verifyObjectDisplayed(getList(totalReservationTransactionValues).get(1))) {
			String valueReservationTransactionPoints = getElementText(
					getList(totalReservationTransactionValues).get(1));
			if (valueReservationTransactionPoints.isEmpty() || valueReservationTransactionPoints == null) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage",
						"membershipChargesReservationTransactionPointValidation", Status.FAIL,
						"Total Reservation Transaction value required not displayed");
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "membershipChargesHousekeepingPointValidation",
						Status.PASS,
						"Total Reservation Transaction required is: " + valueReservationTransactionPoints);
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage",
					"membershipChargesReservationTransactionPointValidation", Status.FAIL,
					"Total Reservation Transaction value required not displayed");
		}
	}

	/*
	 * Method: verifyToolTipHousekeepingCredit Description:verify ToolTip
	 * Housekeeping Credit Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyToolTipHousekeepingCredit() {
		toolTipValidations(tooltipHousekeeping, "HouseKeeping Credit", textTooltipHousekeeping);
	}

	/*
	 * Method: verifyToolTipReservationTransaction Description:verify ToolTip
	 * Reservation Transaction Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void verifyToolTipReservationTransaction() {
		toolTipValidations(tooltipReservationTransaction, "Reservation Transaction", textTooltipReservationTransaction);
	}

	/*
	 * Method: rentedPointsValidations Description: Verify Rented Points
	 * displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public double rentedPointsValidations() {
		if (testData.get("RentCharges").equalsIgnoreCase("Yes")) {
			if (verifyObjectDisplayed(rentedPoints)) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "rentedPointsValidations", Status.PASS,
						"Rented Points Displayed as: " + getDoubleValue(rentedPoints, "$"));
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "rentedPointsValidations", Status.FAIL,
						"Rented Points not displayed");
			}
			return getDoubleValue(rentedPoints, "$");
		} else {
			return 0.00;
		}
	}

	/*
	 * Method: housekeepingCreditsValidations Description: Verify Housekeeping
	 * Credits displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public double housekeepingCreditsValidations() {
		if (testData.get("HousekeepingCharges").equalsIgnoreCase("Yes")) {
			if (verifyObjectDisplayed(housekeepingCredits)) {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "housekeepingCreditsValidations",
						Status.PASS,
						"Housekeeping Credits Displayed as: " + getDoubleValue(housekeepingCredits, "$"));
			} else {
				tcConfig.updateTestReporter("CUIReservationDetailsPage", "housekeepingCreditsValidations",
						Status.FAIL, "Housekeeping Credits not displayed");
			}
			return getDoubleValue(housekeepingCredits, "$");

		} else {
			return 0.00;
		}
	}

	/*
	 * Method: totalRefundAmount Description: Verify Total Refund amount
	 * displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public double totalRefundAmount() {
		if (verifyObjectDisplayed(totalReimburseAmount)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalRefundAmount", Status.PASS,
					"Total Refund Amount Displayed as: " + getDoubleValue(totalReimburseAmount, "$"));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalRefundAmount", Status.FAIL,
					"Total Refund Amount not Displayed as: ");
		}
		return getDoubleValue(totalReimburseAmount, "$");
	}

	/*
	 * Method: paymentTotalValidations Description: Total Sum of Payment
	 * Reimbursement displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void paymentTotalValidations() {
		double rentedPay = rentedPointsValidations();
		double housekeepingPay = housekeepingCreditsValidations();
		double totalPay = totalRefundAmount();
		double addition = Math.round((rentedPay + housekeepingPay) * 100.0) / 100.0;
		if (addition == totalPay) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "paymentTotalValidations", Status.PASS,
					"Sum of payment distribution matches to that of total refund: " + addition);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "paymentTotalValidations", Status.FAIL,
					"Sum of payment distribution does not matches to that of total refund");
		}
	}

	/*
	 * Method: paymentChargesNotPresent Description: Verify that Payment Charges
	 * is not present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void paymentChargesNotPresent() {
		if (verifyObjectDisplayed(reimbursementPayment)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "paymentChargesNotPresent", Status.FAIL,
					"Payment Charges present in the page");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "paymentChargesNotPresent", Status.PASS,
					"Payment Charges not present in the page");
		}
	}

	/*
	 * Method: pointsSummaryLink Description:Validate Points Summary Navigation
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsSummaryLink() {

		getElementInView(pointSummaryLink);
		clickElementBy(pointSummaryLink);
		waitUntilElementVisibleBy(driver, pointsSummaryPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(pointsSummaryPage), "Points Summary Page not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "pointsSummaryLink", Status.PASS,
				"Points Summary Page Navigated");
	}

	/*
	 * Method: chargesTextValidation Description: Charges Text Validations Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void chargesTextIteration() {
		if (testData.get("PaymentReimburse").contains("Yes")) {
			List<WebElement> textList = getList(allChargesText);
			Boolean flag = false;
			for (int textCount = 0; textCount < textList.size(); textCount++) {
				if ((textList).get(textCount).getText().toLowerCase().contains("guest")
						|| (textList).get(textCount).getText().toLowerCase().contains("reservation")
						|| (textList).get(textCount).getText().toLowerCase().contains("protection")) {
					flag = false;
					break;
				} else {
					flag = true;
				}
			}
			chargesTextValidation(flag);
		}
	}

	/*
	 * Method: chargesTextValidation Description: Charges Text Validations Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void chargesTextValidation(Boolean flag) {

		if (flag = true) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "chargesTextValidation", Status.PASS,
					"No Guest Confirmation, Points Protection or Transaction Reservation Charges are being Reimbursed");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "chargesTextValidation", Status.FAIL,
					"Guest Confirmation or Points Protection or Transaction Reservation Charges are being Reimbursed");
		}

	}

	/*
	 * Method: validateBreadcrumb Description: Reservation Breadcrumb validation
	 * Check Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void validateBreadcrumb() {

		assertTrue(verifyObjectDisplayed(breadCrumbLink), "Breadcrumb not present");
		assertTrue(getElementText(reservationBreadcrumb).contains(tcConfig.getTestData().get("ReservationNumber")),
				"Reservation Numer did not match in Breadcrumb");

		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateBreadcrumb", Status.PASS,
				"Breadcrumb Validated Successfully");
	}
	/*
	 * Method: validateBookingDetails Description: Reservation Booking Details
	 * validation Check Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void validateBookingDetails() throws Exception {
		assertTrue(
				(getElementText(resortName).toUpperCase()
						.contains(tcConfig.getTestData().get("ReservationResortName").toUpperCase())),
				"Resort Name does not match");
		assertTrue(verifyObjectDisplayed(resortAddress), "Resort Address not present");
		assertTrue(verifyObjectDisplayed(resortPhone), "Resort Phone not present");
		assertTrue(getElementText(checkInDate).contains(dateConvert(tcConfig.getTestData().get("Checkindate"))),
				"Check In Date Not Match");
		assertTrue(getElementText(checkOutDate).contains(dateConvert(tcConfig.getTestData().get("Checkoutdate"))),
				"Check Out Date Not Match");

		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateBookingDetails", Status.PASS,
				"Booking details validated successfully");
	}

	/*
	 * Method: dateConvert Description: convertDate validation Check Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public String dateConvert(String strDate) throws Exception {
		if (strDate.contains("/")) {
			return strDate;
		} else {
			return convertDateFormat("dd MMM yyyy", "MM/dd/yyyy", strDate);
		}
	}

	/*
	 * Method: mapLinkValidations Description:Map Section Elements Validations *
	 * Date field Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void mapLinkValidations() {

		clickElementBy(viewMapLink);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(newWindow);
		waitUntilElementVisibleBy(driver, mapPageHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(mapPageHeader), "Map Page Header not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "mapLinkValidations", Status.PASS,
				"Navigated to Map page");
		navigateToMainTab(newWindow);
		waitUntilElementVisibleBy(driver, resortName, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(resortName), "Resort name not present");
	}

	/*
	 * Method: resortCardNavigation Description:Validate Resort card Navigation
	 * Date: Jul/2020 Author: Unnat Jain Changes By: NA
	 */
	public void resortCardNavigation() {
		// String get first resort name
		String strFirstResortName = getElementText(resortName).trim();
		clickElementBy(resortName);
		waitUntilElementVisibleBy(driver, navigatedResortPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(navigatedResortPage), "Resort page navigation failed");
		if (getElementText(navigatedResortPage).trim().contains(strFirstResortName)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "resortCardNavigation", Status.PASS,
					"Resort Navigated Successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "resortCardNavigation", Status.FAIL,
					"Resort Navigation Failed");
		}
		navigateBackAndWait(resortName);

	}

	/*
	 * Method: validateCancelModifyLink Description:Cancel Modify Link Check
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void validateCancelModifyLink() {
		assertTrue(verifyObjectDisplayed(cancelLink), "Cancel Link Not Present");
		assertTrue(verifyObjectDisplayed(modifyLink), "Modify Link Not Present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateCancelModifyLink", Status.PASS,
				"Cancel and Modify Link Present");
	}

	/*
	 * Method: validateSuiteDetails Description: Reservation Details validation
	 * Check Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void validateReservation() {
		assertTrue(getElementText(reservationMadeOn).contains(tcConfig.getTestData().get("ReservationDate")),
				"Reservation Made On Date Not Match");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateReservation", Status.PASS,
				"Reservation Made On Date Present as" + getElementText(reservationMadeOn));
		assertTrue(
				(getElementText(reservationMadeBy).toUpperCase()
						.contains(tcConfig.getTestData().get("ReservationBy").toUpperCase())),
				"Reservation Made By not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateReservation", Status.PASS,
				"Reservation Made By Present as" + getElementText(reservationMadeBy));
	}

	/*
	 * Method: cancelationPolicy Description:Cancel Policy Check Date: July/2020
	 * Author: Unnat Jain Changes By: NA
	 */

	public void cancelationPolicy() {
		getElementInView(cancellationPolicyHeader);
		assertTrue(verifyObjectDisplayed(cancellationPolicyHeader), "Cancel Policy Header Not Present");
		assertTrue(verifyObjectDisplayed(cancellationPolicyDescription), "Cancel Policy Desc  Not Present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelationPolicy", Status.PASS,
				"Cancelation Policy Verified");
	}

	/*
	 * Method: cancelationPolicyNotPresent Description:Cancel Policy Check Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void cancelationPolicyNotPresent() {

		if (verifyObjectDisplayed(cancellationPolicyHeader)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelationPolicyNotPresent", Status.FAIL,
					"Cancel Policy present in the page");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelationPolicyNotPresent", Status.PASS,
					"Cancel Policy not present in the page");
		}

	}

	/*
	 * Method: fullCancellationPolicy Description:Validate Cancellation Policy
	 * Navigation Date: Jul/2020 Author: Unnat Jain Changes By: NA
	 */
	public void fullCancellationPolicy() {

		clickElementBy(viewCancellationPolicyLink);
		waitUntilElementVisibleBy(driver, fullCancelPolicyPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(fullCancelPolicyPage), "Cancel Policy not displayed");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "fullCancellationPolicy", Status.PASS,
				"Cancelation Policy Page Navigated");
		navigateBackAndWait(resortName);

	}

	/*
	 * Method: reservationSummaryTraveler Description: Reservation Traveler
	 * Details validation Check Date: July/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	public void reservationSummaryTraveler() {
		getElementInView(reservationSummaryHeader);
		assertTrue(verifyObjectDisplayed(reservationSummaryHeader), "Reservation Summary Header not Present");
		assertTrue(verifyObjectDisplayed(travelerDetailsHeader), "Traveler Details Header not present");
		assertTrue(
				(getElementText(memberNumber).toUpperCase()
						.contains(tcConfig.getTestData().get("MemberNumber").toUpperCase())),
				"Member Number does not match");
		assertTrue(
				(getElementText(travellerName).toUpperCase()
						.contains(tcConfig.getTestData().get("TravelerName").toUpperCase())),
				"Traveler Name does not match");
		assertTrue(
				(getElementText(email).toUpperCase()
						.contains(tcConfig.getTestData().get("TravellerEmailID").toUpperCase())),
				"Email does not match");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "reservationSummaryTraveler", Status.PASS,
				"Traveler Details Verified Succesfully");
	}

	/*
	 * Method: validateReservationDetails Description: Reservation Details
	 * validation Check Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void validateReservationDetails() {
		assertTrue(verifyObjectDisplayed(reservationDetailsHeader), "Reservation Details Header not Present");
		assertTrue(verifyObjectDisplayed(travelerDetailsHeader), "Traveler Details Header not present");
		assertTrue(
				(getElementText(reservationNumber).toUpperCase()
						.contains(tcConfig.getTestData().get("ReservationNumber").toUpperCase())),
				"Reservation Number does not match");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateReservationDetails", Status.PASS,
				"Reservation Number Present as" + getElementText(reservationNumber));
		if (verifyObjectDisplayed(pointsProtection)) {
			assertTrue(getElementText(pointsProtection).length() > 0, "Points Protection Present");
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateBookingDetails", Status.PASS,
					"Points Potection is " + getElementText(pointsProtection));
		}

	}

	/*
	 * Method: validateSuiteDetails Description: Reservation Details validation
	 * Check Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void validateSuiteDetails() {
		assertTrue(getElementText(suiteCost).length() > 0, "Suite Cost not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateSuiteDetails", Status.PASS,
				"Suite Cost Present as " + getElementText(suiteCost));
		assertTrue((getElementText(suiteType).toUpperCase().contains("ROOM")), "Suite Type not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateSuiteDetails", Status.PASS,
				"Suite Type Present as " + getElementText(suiteType));
	}

	/*
	 * Method: validateSuiteUpgradeOpted Description: Validate Instant Upgrade
	 * Opted Check Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void validateSuiteUpgradeOpted() {
		elementPresenceVal(instantUpgrade, "Text is: " + getElementText(instantUpgrade), "CUIReservationDetailsPage",
				"validateSuiteUpgradeOpted");
	}

	/*
	 * Method: validateSpecialRequest Description: Validate Special Request
	 * Check Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateSpecialRequest() {
		elementPresenceVal(specialRequest, "" + getElementText(specialRequest), "CUIReservationDetailsPage",
				"validateSpecialRequest");
	}

	/*
	 * Method: totalPointsValidation Description: Verify Total Points present
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalPointsValidation() {
		if (getElementText(totalReimbursePoints).replace(",", "").contains(testData.get("TotalPoints"))) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalPointsValidation", Status.PASS,
					"Reimbursed Points displayed as : " + getElementText(totalReimbursePoints));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalPointsValidation", Status.FAIL,
					"Reimbursed Points not displayed");
		}
	}

	/*
	 * Method: totalPointsValidation Description: Verify Total Points present
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalPointsValidation(String totalPoints) {
		if (getElementText(totalReimbursePoints).contains(totalPoints.trim())) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalPointsValidation", Status.PASS,
					"Reimbursed Points displayed as : " + getElementText(totalReimbursePoints));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalPointsValidation", Status.FAIL,
					"Reimbursed Points not displayed");
		}
	}

	/*
	 * Method: pointsProtection Description: Reservation Details validation
	 * Check Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void pointsProtectionNotPresent() {
		if (verifyObjectDisplayed(pointsProtection)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "pointsProtectionNotPresent", Status.FAIL,
					"Points Potection is displayedc" + getElementText(pointsProtection));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "pointsProtectionNotPresent", Status.PASS,
					"Points Potection is not displayedc");
		}
	}

	/*
	 * Method: totalReservationTransaction Description: Verify Total Resr Trans
	 * present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalReservationTransaction() {
		if (getElementText(reservationTransaction).length() > 0) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalReservationTransaction", Status.PASS,
					"Reservation Transaction displayed as : " + getElementText(reservationTransaction));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalReservationTransaction", Status.FAIL,
					"Reservation Transaction not displayed");
		}
	}

	/*
	 * Method: totalHousekeepingValidation Description: Verify Total Credits
	 * present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalHousekeepingValidation() {
		if (getElementText(houseKeepingCredits).length() > 0) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalHousekeepingValidation", Status.PASS,
					"House keeping Credits displayed as : " + getElementText(houseKeepingCredits));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "totalHousekeepingValidation", Status.FAIL,
					"House keeping Credits not displayed");
		}
	}

	/*
	 * Method: paymentInformation Description: validated payment information
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void paymentInformation() {
		getElementInView(paymentDoneBy);
		fieldPresenceVal(paymentDoneBy, "Payment Done By Name ", "CUIReservationDetailsPage", "paymentInformation",
				"as: " + getElementText(paymentDoneBy));
		fieldPresenceVal(cardDetails, "Payment Card Number ", "CUIReservationDetailsPage", "paymentInformation",
				"as: " + getElementText(cardDetails));
		elementPresenceVal(paymentMethodImage, "Payment Card Image", "CUIReservationDetailsPage", "paymentInformation");
	}

	/*
	 * Method: matchTotalPay Description: Total Sum of Payment Reimbursement
	 * displayed Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void matchTotalPay() {
		double paymentList = totalPaymentValidation();
		double totalPay = totalRefundAmount();
		double addition = Math.round((paymentList) * 100.0) / 100.0;
		if (addition == totalPay) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "paymentTotalValidations", Status.PASS,
					"Sum of payment distribution matches to that of total refund: " + addition);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "paymentTotalValidations", Status.FAIL,
					"Sum of payment distribution does not matches to that of total refund");
		}

	}

	/*
	 * Method: validateHeaderModification Description:
	 * validateHeaderModification Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void validateCollapsedModifications() {
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator < getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				if (getList(totalAccordion).get(listmodificationiterator).getText().contains("active")) {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateCollapsedModifications",
							Status.FAIL, "Unable to collapse modification accordion");
				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateCollapsedModifications",
							Status.PASS, "All Modifications Accordion are collapsed");
				}

			}
		}
	}

	/*
	 * Method: verifyModificationSortingDate Description:
	 * verifyModificationSortingDate Date: July/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void verifyModificationSortingDate() throws Exception {
		descendingOrderDateCheck(getListString(allModificationDates), "MM/dd/yy", "Modification Dates");
	}

	/*
	 * Method: validateHeaderModification Description:
	 * validateHeaderModification Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void validateHeaderModification() {
		getElementInView(textModification);
		elementPresenceVal(modificationHistoryHeader, "Modification History Header", "CUIReservationDetailsPage",
				"validateHeaderModification");
		elementPresenceVal(textModification, "Header Modification", "CUIReservationDetailsPage",
				"validateHeaderModification");
	}

	/*
	 * Method: validatePointProtectionInModificationHistory Description:
	 * Date:June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validatePointProtectionInModificationHistory() {
		String pointProtectionPresence = null;
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//a[contains(@id,'accordion-label')]/div"));
				if (verifyObjectDisplayed(eachModificationTypeList)) {
					String modificationType = getElementText(eachModificationTypeList);
					if (modificationType.equalsIgnoreCase("Points Protection")) {
						getElementInView(eachModificationTypeList);
						String modifiedDate = getObject(By.xpath("//div[contains(@id,'points-protection')]//a//p/span"))
								.getText().trim();
						clickElementJSWithWait(eachModificationTypeList);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						String modifyText = getElementText(getObject(By
								.xpath("(//div[contains(@id,'points-protection')]//div[@role = 'tabpanel']//span)[1]")))
										.trim();
						if (modifyText.contains("Accepted")) {
							pointProtectionPresence = "Y";
							tcConfig.updateTestReporter("CUIReservationDetailsPage",
									"validatePointProtectionInModificationHistory", Status.PASS,
									"Point Protection modified on: " + modifiedDate + " and the Status is: "
											+ modifyText);
							break;
						} else {
							tcConfig.updateTestReporter("CUIReservationDetailsPage",
									"validatePointProtectionInModificationHistory", Status.FAIL,
									"Failed to Validate Status");
							break;
						}
					}

				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage",
							"checkModificationTypeInModificationHistory", Status.FAIL,
							"Modification Type not Present");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "checkModificationHistoryCollapsedMode",
					Status.FAIL, "No modification history");
		}
		if (pointProtectionPresence == "Y") {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "validatePointProtectionInModificationHistory",
					Status.PASS, "Point Protection Validation successfull");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "validatePointProtectionInModificationHistory",
					Status.FAIL,
					"Failed while Validating Point Protection Modification Type, Maybe Point Protection modified nor present in list");
		}
	}

	/*
	 * Method: validateTableModificationHistory
	 * Description:validateTableModificationHistory Date: June/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void validateTableModificationHistory() {
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//a[contains(@id,'accordion-label')]/div"));
				getElementInView(eachModificationTypeList);
				clickElementJSWithWait(eachModificationTypeList);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String modificationType = getElementText(eachModificationTypeList);
				String modifiedDate = getObject(By.xpath("//div[contains(@class,'holiday modification-history')]["
						+ listmodificationiterator + "]//a[contains(@id,'accordion-label')]//p/span")).getText().trim();
				String modifyText = getElementText(
						getObject(By.xpath("(//div[contains(@class,'holiday modification-history')]["
								+ listmodificationiterator + "]//div[@role = 'tabpanel']//span)[1]"))).trim();
				if (modificationType.isEmpty() || modificationType == null || modifiedDate.isEmpty()
						|| modifiedDate == null || modifyText.isEmpty() || modifyText == null) {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateTableModificationHistory",
							Status.FAIL,
							"Error Validating Modification History Content for index: " + listmodificationiterator);
				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateTableModificationHistory",
							Status.PASS,
							"" + modificationType + " Modified on: " + modifiedDate + " with Status: " + modifyText);
					clickElementJSWithWait(eachModificationTypeList);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateTableModificationHistory", Status.FAIL,
					"No modification history Table Present");
		}
	}

	/*
	 * Method: totalPaymentValidation Description: validated total payment Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public double totalPaymentValidation() {
		Double totalPay = 0.00;
		for (int payText = 0; payText < getList(chargedForText).size(); payText++) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "rentedPointsValidations", Status.PASS,
					getList(chargedForText).get(payText).getText() + " Displayed as: "
							+ getDoubleValue(getList(chargedAmount).get(payText), "$"));
			totalPay = totalPay + getDoubleValue(getList(chargedAmount).get(payText), "$");

		}
		return totalPay;
	}

	/*
	 * Method: verifyHeaderReimbursementSummary Description: validated total
	 * payment Date: July/2020 Author: Abhijeet Changes By: NA
	 */

	public void verifyHeaderReimbursementSummary() {

		if (verifyObjectDisplayed(reimbursementHeader) || verifyObjectDisplayed(forfeitHeader)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyHeaderReimbursementSummary", Status.PASS,
					"Reimbursement/Forfeit Summary Present");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyHeaderReimbursementSummary", Status.FAIL,
					"Reimbursement/Forfeit Summary not Present");
		}
	}

	/*
	 * Method: validateVIPUpgradeInModificationHistory Description:
	 * Date:June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateVIPUpgradeInModificationHistory() {
		String vipUpgradePresence = null;
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//a[contains(@id,'accordion-label')]/div"));
				if (verifyObjectDisplayed(eachModificationTypeList)) {
					String modificationType = getElementText(eachModificationTypeList);
					if (modificationType.contains("VIP Upgrade")) {
						getElementInView(eachModificationTypeList);
						String modifiedDate = getObject(By
								.xpath("//div[contains(@id,'opt-in-out')]//a//p/span | //div[contains(@id,'vip_instant')]//a//p/span"))
										.getText().trim();
						clickElementJSWithWait(eachModificationTypeList);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						String modifyText = getElementText(modifyVIPText).trim();
						if (modifiedDate.isEmpty() || modifiedDate == null || modifyText.isEmpty()
								|| modifyText == null) {
							tcConfig.updateTestReporter("CUIReservationDetailsPage",
									"validateVIPUpgradeInModificationHistory", Status.FAIL,
									"Error Validating Modification History Content for index: "
											+ listmodificationiterator);
						} else {
							tcConfig.updateTestReporter("CUIReservationDetailsPage",
									"validateVIPUpgradeInModificationHistory", Status.PASS, "" + modificationType
											+ " Modified on: " + modifiedDate + " with Status: " + modifyText);
							vipUpgradePresence = "Y";
							clickElementJSWithWait(eachModificationTypeList);
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							break;
						}
					}

				} else {
					tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateVIPUpgradeInModificationHistory",
							Status.FAIL, "Modification Type not Present");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateVIPUpgradeInModificationHistory",
					Status.FAIL, "No modification history");
		}
		if (vipUpgradePresence == "Y") {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateVIPUpgradeInModificationHistory",
					Status.PASS, "VIP Upgrade Validation successfull");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateVIPUpgradeInModificationHistory",
					Status.FAIL,
					"Failed while Validating VIP Upgrade Modification Type, Maybe VIP Upgrade modified not present in list");
		}
	}

	/*
	 * Method: verifyReservationNumber Description: Verify reserv Number
	 * Date:Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void verifyReservationNumber() {
		assertTrue(
				(getElementText(reservationNumber).toUpperCase()
						.contains(testData.get("ReservationNumber").toUpperCase())),
				"Reservation Number does not match");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "validateReservationDetails", Status.PASS,
				"Reservation Number Present as" + getElementText(reservationNumber));
	}

	/*
	 * Method: clickAddNightsHistory Description: Verify Add Nights History and
	 * click Date:Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void clickAddNightsHistory() {
		assertTrue(verifyObjectDisplayed(addNightsAccordion), "Add Nights Not Present in Modification History");
		getElementInView(addNightsAccordion);
		clickElementBy(addNightsAccordion);
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "clickAddNightsHistory", Status.PASS,
				"Add Nights Present in Modification History");
	}

	/*
	 * Method: clickAddNightsHistory Description: Verify Add Nights History and
	 * click Date:Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void verifyAddNights() {
		assertTrue(verifyObjectDisplayed(addNightsAccordion), "Add Nights Not Present in Modification History");
		getElementInView(addNightsAccordion);
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "clickAddNightsHistory", Status.PASS,
				"Add Nights Present in Modification History");
	}

	/*
	 * Method: reservationPageDateFormat Description: Change Reservation date
	 * format Date: Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public String reservationPageDateFormat(By xpath, String fromDateFormat) throws Exception {
		String getDisplayedDate = null;
		if (getElementText(xpath).contains(",")) {
			getDisplayedDate = getElementText(xpath).split("\\,")[1].trim();
		}
		return convertDateFormat(fromDateFormat, "MM/dd/yy", getDisplayedDate);
	}

	/*
	 * Method: verifyNoOfNights Description: Verify No Of Nights ADDED
	 * Date:Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void verifyNoOfNights_CheckOut() throws Exception {
		// get nights form application
		String getNights = getElementText(addNightsText).split("\\(")[1].trim();
		Long getNoNights = Long.parseLong(getNights.split(" ")[0].trim());
		// get nights from check in check out
		Long toatlNights = dateDifference("MM/dd/yyyy", CUIModifyReservationPage_Web.checkoutDate,
				reservationPageDateFormat(checkOutDate, "MM/dd/yyyy"));
		Assert.assertTrue(toatlNights.equals(getNoNights), "Total Nights Displayed incorrectly");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyNoOfNights", Status.PASS,
				"Total Nights Displayed Correctly");

	}

	/*
	 * Method: verifyNoOfNights_CheckIn Description: Verify No Of Nights ADDED
	 * Date:Aug/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyNoOfNights_CheckIn() throws Exception {
		// get nights form application
		String getNights = getElementText(addNightsText).split("\\(")[1].trim();
		Long getNoNights = Long.parseLong(getNights.split(" ")[0].trim());
		// get nights from check in check out
		Long toatlNights = dateDifference("MM/dd/yyyy", reservationPageDateFormat(checkInDateModified, "MM/dd/yyyy"),
				CUIModifyReservationPage_Web.checkinDate);
		Assert.assertTrue(toatlNights.equals(getNoNights), "Total Nights Displayed incorrectly");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyNoOfNights", Status.PASS,
				"Total Nights Displayed Correctly");

	}

	/*
	 * Method: verifyModifiedBy Description: Verify Add Nights History and
	 * Modified By Date:Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void verifyModifiedBy() {
		assertTrue(verifyObjectDisplayed(modifiedByText), "Modified By Not Present in Modification History");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyModifiedBy", Status.PASS,
				"Modified By Present in Add Nights History as: " + getElementText(modifiedByText));
	}

	/*
	 * Method: verifyPointsProtection Description: Verify Add Nights History and
	 * PPP Date:Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void verifyPointsProtection() {
		/*assertTrue(verifyObjectDisplayed(pointsProtectionText),
				"Points Protection Not Present in Modification History");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyPointsProtection", Status.PASS,
				"Points protection Present in Add Nights History as: " + getElementText(pointsProtectionText));
	*/}

	public void verifyPointsProtection(String Status) {
		/*assertTrue(
				verifyObjectDisplayed(pointsProtectionText)
						&& getElementText(pointsProtectionText).trim().contains(Status),
				"Points Protection Not Present in Modification History");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyPointsProtection", Status.PASS,
				"Points protection Present in Add Nights History as: " + getElementText(pointsProtectionText));*/
	}

	/*
	 * Method: checkCancelDB Description: Verify Cancel in DB Date: July/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */

	public void checkCancelDB() throws Exception {
		String reservationNumber = testData.get("ReservationNumber");
		String bookingCheckInDate = convertDateFormat("MM/dd/yy", "yyyy-MM-dd", testData.get("ModifyCheckInDate"))
				.toUpperCase();
		String bookingCheckOutDate = convertDateFormat("dd MMMM yyyy", "yyyy-MM-dd", testData.get("Checkoutdate"))
				.toUpperCase();
		String query = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND gst.stat_name = 'CANCELLED' AND gct.class_name = 'CONFIRM RESERVATION' AND rap.created_by = 'SYSTEM' AND rer.res_num in ('"
				+ reservationNumber + "')";
		DBConnectionOnly();
		String checkindate = ExecuteQueryAndFetchColumData(query, "CHK_IN_LOCAL_DATE").split(" ")[0].trim();
		String checkoutdate = ExecuteQueryAndFetchColumData(query, "CHK_OUT_LOCAL_DATE").split(" ")[0].trim();
		if (checkindate.equals(bookingCheckInDate)) {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.PASS,
					"DB Check In Date Validate Successfully");
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.FAIL,
					"DB Check In Date Validation Failed");
		}

		if (checkoutdate.equals(bookingCheckOutDate)) {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.PASS,
					"DB Check Out Date Validate Successfully");
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.FAIL,
					"DB Check Out Date Validate Successfully");
		}

		closeDB();
	}

	/*
	 * Method: verifyPurchaseHK Description:verify Purchase HK presence Date:
	 * Aug/2020 Author: Kamalesh Changes By: NA
	 */

	public void verifyPurchaseHK() {
		getElementInView(hkModificationLabel);
		assertTrue(verifyObjectDisplayed(hkModificationLabel), "Housekeeping Credits (Modification) not present");
		assertTrue(verifyObjectDisplayed(hkModificationValue), "Housekeeping Credits (Modification) not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyPurchaseHK", Status.PASS,
				"Housekeeping Credits (Modification) Label present and the value is  "
						+ getElementText(hkModificationValue));
	}

	/*
	 * Method: verifyTotalPayment Description:verify Total Payment presence
	 * Date: Aug/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyTotalPayment() {
		getElementInView(paymentTotalLabel);
		assertTrue(verifyObjectDisplayed(paymentTotalLabel), "Payment Total Label not present");
		assertTrue(verifyObjectDisplayed(paymentTotalValue), "Payment Total Value not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalPayment", Status.PASS,
				"Payment Total Label present and the value is  " + getElementText(paymentTotalValue));
	}

	

	/*
	 * Method: verifyTotalPayment Description:verify Total Payment presence
	 * Date: Aug/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyBorrowedHK() {
		getElementInView(hkCredit);
		assertTrue(verifyObjectDisplayed(hkCredit), "Payment Total Label not present");
		assertTrue(verifyObjectDisplayed(hkCreditValue), "Payment Total Value not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyBorrowedHK", Status.PASS,
				"HouseKeeping Credit Label present and the value is  " + getElementText(hkCreditValue));

	}

	/*
	 * Method: verifyPointsProtectionModification
	 * Description:verifyPointsProtectionModification Date: Aug/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	public void verifyPointsProtectionModification(String tier) {
		getElementInView(ppModificationLabel);
		assertTrue(verifyObjectDisplayed(ppModificationLabel) && verifyObjectDisplayed(ppModificationValue),
				"Points Protection Modification label and value not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyPointsProtectionModification", Status.PASS,
				"Points Protection Modification label and value present");

		assertTrue(
				verifyObjectDisplayed(paymentGrandTotalLabel) && verifyObjectDisplayed(paymentGrandTotalValue)
						&& getElementText(paymentGrandTotalValue).trim().equals(tier),
				"Payment Total Value  and label not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyTotalPayment", Status.PASS,
				"Payment Total Label present and the value is  " + getElementText(paymentGrandTotalValue));

	}

	/*
	 * Method: verifyPaymentCharges_RentedPoint
	 * verifyPaymentCharges_RentedPoint Date: Aug/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	

	public void verifyPaymentCharges_RentedPoint() {
		getElementInView(rentedPointLabel);
		assertTrue(verifyObjectDisplayed(rentedPointLabel) && verifyObjectDisplayed(rentedPointValue),
				"Points Protection Modification label and value not present");
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Rented Point Modification label and value present");

	}
}
