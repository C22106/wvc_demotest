package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIManagePasswordPopUp_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIManagePasswordPopUp_Web.class);

	public CUIManagePasswordPopUp_Web(TestConfig tcconfig) {
		super(tcconfig);
	

	}

	protected By editPasswordLink = By.xpath("//button[contains(.,'Edit Password')]");
	protected By passwordPopUpHeader = By
			.xpath("//div[@id='password-modal' and @aria-hidden='false']//p[contains(.,'MANAGE PASSWORD')]");
	/* Manage Password Pop Up Modal */
	protected By ownerPassword = By.xpath("//h5[contains(.,'PASSWORD')]/following-sibling::p");
	protected By passwordCloseCta = By
			.xpath("//div[@id='password-modal' and @aria-hidden='false']//button[@aria-label='Close modal']");
	protected By currentPasswordField = By.id("currentPassword");
	protected By newPasswordField = By.id("newPassword");
	protected By passwordCancelCTA = By
			.xpath("//div[@id='password-modal' and @aria-hidden='false']//div/button[contains(.,'Cancel')]");
	protected By passwordSaveCta = By
			.xpath("//div[@id='password-modal' and @aria-hidden='false']//div/button[contains(.,'Save')]");
	protected By closedPasswordModal = By.xpath("//div[@id='password-modal' and @aria-hidden='true']");
	protected By currentPassShowCta = By.xpath("//label[@for='currentPassword']//a[contains(.,'show')]");
	protected By currentPassHideCta = By.xpath("//label[@for='currentPassword']//a[contains(.,'hide')]");
	protected By newPasswordShowCta = By.xpath("//label[@for='newPassword']//a[contains(.,'show')]");
	protected By newPasswordHideCta = By.xpath("//label[@for='newPassword']//a[contains(.,'hide')]");
	protected By newPasswordFieldError = By
			.xpath("//label[@for='newPassword']/following-sibling::p[@class='input-error']");
	protected By globalError = By.id("error-message");

	/*
	 * Method: passwordPopUpModalNav Description: Navigate to Manage Password
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void passwordPopUpModalNavigation() {
		waitUntilElementVisibleBy(driver, editPasswordLink,  "ExplicitLongWait");
		getElementInView(editPasswordLink);
		clickElementBy(editPasswordLink);

		waitUntilElementVisibleBy(driver, passwordPopUpHeader,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(passwordPopUpHeader),"Password Pop Up Header not present");
		elementPresenceVal(passwordPopUpHeader, "Password Pop Up Modal with header", "CUIAccountSettingPage",
				"passwordPopUpModalNav");
	}

	/*
	 * Method: showCurrPasswordCTA Description: Show Cta Validations Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void showCurrentPasswordCTA() {
		if (verifyObjectDisplayed(currentPassShowCta)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "showCurrPasswordCTA", Status.PASS,
					"Show CTA is present in current password field");
			clickElementBy(currentPassShowCta);

			waitUntilElementVisibleBy(driver, currentPassHideCta,  "ExplicitLowWait");
			elementPresenceVal(currentPassHideCta, "In Current Password Field Hide CTA ", "CUIAccountSettingPage",
					"showCurrPasswordCTA");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "showCurrPasswordCTA", Status.FAIL,
					"Show CTA is not present in  current password field");
		}

	}

	/*
	 * Method: showNewPasswordCTA Description: Show Cta Validations Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void showNewPasswordCTA() {
		if (verifyObjectDisplayed(newPasswordShowCta)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "showNewPasswordCTA", Status.PASS,
					"Show CTA is present in new password field");
			clickElementBy(newPasswordShowCta);

			waitUntilElementVisibleBy(driver, newPasswordHideCta,  "ExplicitLowWait");
			elementPresenceVal(newPasswordHideCta, "In new Password Field Hide CTA ", "CUIAccountSettingPage",
					"showNewPasswordCTA");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "showNewPasswordCTA", Status.FAIL,
					"Show CTA is not present in new password field");
		}

	}

	/*
	 * Method: incorrectDataCurrent Description: Invalid Data Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void incorrectDataCurrent() {
		fieldDataEnter(currentPasswordField, testData.get("Invalid_Pass"));
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: invalidNewPassword Description: Invalid Data Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void invalidNewPassword() {
		fieldDataEnter(newPasswordField, testData.get("Password"));
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: newPasswordFieldError Description: New password fielderror in
	 * password Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void newPasswordFieldError() {
		waitUntilElementVisibleBy(driver, newPasswordFieldError,  "ExplicitLongWait");
		if (verifyObjectDisplayed(newPasswordFieldError)) {
			tcConfig.updateTestReporter("CUIManagePasswordPage", "newPasswordFieldError", Status.PASS,
					"New Password Field Error displayed");
		} else {
			tcConfig.updateTestReporter("CUIManagePasswordPage", "newPasswordFieldError", Status.FAIL,
					"New Password Field Error did not get displayed");
		}
	}

	/*
	 * Method: incorrectCurrentPassword Description: Incorrect Current Password
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void incorrectCurrentPassword() {
		waitUntilElementVisibleBy(driver, globalError,  "ExplicitLongWait");
		if (verifyObjectDisplayed(globalError)) {
			tcConfig.updateTestReporter("CUIManagePasswordPage", "incorrectCurrentPassword", Status.PASS,
					"Incorrect Current Password Error Displayed");
		} else {
			tcConfig.updateTestReporter("CUIManagePasswordPage", "incorrectCurrentPassword", Status.FAIL,
					"Incorrect Current Password Error not Displayed");
		}
	}

	/*
	 * Method: editNewPasswordField Description: Edit Password Field in Password
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void editNewPasswordField() throws Exception {
		setCurrentPassword(testData.get("CUI_password"));
		enterDataInPopUpField(newPasswordField, testData.get("Password"), "Yes");
	}

	/*
	 * Method: savePasswordChanges Description: Save Username Changes Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void savePasswordChanges() {
		clickElementBy(passwordSaveCta);

		waitUntilElementInVisible(driver, passwordSaveCta,  "ExplicitLongWait");
		sendKeyboardKeys(Keys.TAB);
		if (verifyObjectDisplayed(newPasswordField)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "savePasswordChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "savePasswordChanges", Status.PASS,
					"Pop Up Modal  Closed");
			try {
				passwordVerification("Password", "No");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				tcConfig.updateTestReporter("CUIAccountSettingPage", "savePasswordChanges", Status.FAIL,
						"Password Changes Verification failed");
			}
		}
	}

	/*
	 * Method: saveButtonClick Description: Save Password Changes Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void saveButtonClick() {
		waitUntilElementVisibleBy(driver, passwordSaveCta,  "ExplicitLongWait");
		clickElementBy(passwordSaveCta);

	}

	/*
	 * Method: setCurrentPassword Description: Set Current Password in Account
	 * setting page Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setCurrentPassword(String strPassword) throws Exception {
		// To Decode passwords
		String decodedPassword = decodePassword(strPassword);
		fieldDataEnter(currentPasswordField, decodedPassword);
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: passowrdVerification Description: Validate Owner Passwords in
	 * Account Setting Page Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordVerification(String testDataColumn, String decodePassword) throws Exception {
		waitUntilElementVisibleBy(driver, ownerPassword,  "ExplicitLongWait");
		// Owner passowrd in Test Data
		String strOwnerPassword = getElementText(ownerPassword);
		if (verifyObjectDisplayed(ownerPassword)) {
			String tempPassword = testData.get(testDataColumn);
			if (decodePassword.equalsIgnoreCase("Yes")) {
				// Decoded Password
				String decodedPassword = decodePassword(tempPassword);
				tempPassword = decodedPassword;
			}
			int tempSize = tempPassword.length();
			String encodePass = "•";
			for (int totalDot = 0; totalDot < tempSize - 1; totalDot++) {
				encodePass = encodePass.concat("•");
			}

			passwordDots(strOwnerPassword, encodePass);
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "passwordVerification", Status.FAIL,
					"Owner Password not present");
		}

	}

	/*
	 * /* Method: passwordDots Description: Password Dots Validations Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordDots(String strOwnerPassword, String encodePass) {
		if (strOwnerPassword.contains(encodePass)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "passwordDots", Status.PASS,
					"Owner Password header present and no of dots displayed correctly: " + strOwnerPassword);
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "passwordDots", Status.FAIL,
					"Owner Password Displayed, but no of dots displayed not correct" + strOwnerPassword);
		}
	}

	/*
	 * /* Method: passwordCancelCTAF Description: Close Manage Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordCancelCTA() {

		closePopUpModal(passwordCancelCTA, closedPasswordModal, newPasswordField, Status.FAIL);

	}

	/*
	 * /* Method: enterNewPassword Description: Enter New Password Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterNewPassword() throws Exception {
		// To Decode passwords
		String decodedPassword = decodePassword(testData.get("CUI_password"));
		enterDataInPopUpField(newPasswordField, decodedPassword, "Yes");
	}

	/*
	 * Method: passwordModalVal Description: Validate Manage Password Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordModalValidation() {
		elementPresenceVal(currentPasswordField, "New Password field", "CUIAccountSettingPage", "passwordModalVal");
		elementPresenceVal(newPasswordField, "New Password field", "CUIAccountSettingPage", "passwordModalVal");
		elementPresenceVal(passwordCancelCTA, "Cancel CTA", "CUIAccountSettingPage", "passwordModalVal");
		elementPresenceVal(passwordSaveCta, "SAVE CTA ", "CUIAccountSettingPage", "passwordModalVal");
		showCurrentPasswordCTA();
		showNewPasswordCTA();

		closePopUpModal(passwordCloseCta, closedPasswordModal, newPasswordField, Status.PASS);

	}

}
