package cui.pages;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBookPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIBookPage_Web.class);

	public CUIBookPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	public static List<String> ratePlanId = new ArrayList<String>();
	protected By headerBooking = By.xpath("//div[@class='complete-booking']//h1[contains(@class,'text-uppercase')] | //div[@class='complete-booking']/div[@class = 'steps__title']"); 
	protected By paymentMethodHeader = By.xpath("//h2[.='PAYMENT METHOD'] | //div[contains(@class,'steps') and contains(text(),'PAYMENT METHOD')]");
	protected By headerCompleteModification = By.xpath(
			"//h1[text() = 'Complete Modification'] | //div[@class = 'steps__title' and contains(.,'Complete Modification')]");
	protected By sectionMembershipCharges = By.xpath("//span[text() = 'Membership Charges']/..");
	protected By valueTotalPoints = By.xpath("(//span[text() = 'Total Points']/..//span)[2]");
	protected By valueHousekeepingCharges = By.xpath("//span[@id = 'house-keeping-tooltip']/../../strong");
	protected By valueTransactionCharges = By.xpath("(//section[contains(@class,'reservation-transaction')]/span)[2]");
	protected By buttonBookNow = By.xpath("//button[text() = 'Book Now']");
	protected By textConfirmationNumber = By.xpath("//h3[text() = 'Confirmation Number']/../p");
	protected By textReservationSummary = By.xpath("//h2[text() = 'Reservation Summary']");
	protected By fieldCreditCardFirstName = By.xpath("//input[@id = 'firstName']");
	protected By fieldCreditCardLastName = By.xpath("//input[@id = 'lastName']");
	protected By fieldCreditCardNumber = By.xpath("//input[@id = 'creditCardNumber']");
	protected By fieldCreditCardMonth = By.xpath("//select[@id = 'expiration-month']");
	protected By fieldCreditCardYear = By.xpath("//select[@id = 'expiration-year']");
	protected By fieldZipCode = By.xpath("//input[@id = 'zipCode']");

	protected By selectCreditCard = By.xpath("//label[@for = 'credit-card']");
	protected By selectPaypal = By.xpath("//label[@for = 'paypal']");
	protected By sectionPaymentmethod = By.xpath("//section[@class = 'complete-booking__payment-method']");
	protected By headerCompleteBooking = By.xpath("//h1[contains(.,'Complete Booking')] | //div[@class = 'steps__title' and contains(.,'Complete Booking')]");

	protected By buttonBookingContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By textReservationBalance = By.xpath("//h1[contains(.,'Reservation Balance')]");
	protected By checkboxPurchaseReservationTransaction = By.xpath("//label[@for = 'purchase-res-tran']");

	protected By sectionPayementCharges = By.xpath("//section[contains(@class,'payment-charges')]");
	protected By headerPaymentMethod = By.xpath("//h2[text() = 'PAYMENT METHOD']");
	protected By sectionPaymentOptions = By.xpath("//section[contains(@class,'payment-method-form')]");
	protected By reservationBalanceHeader = By.xpath("//div[@class='complete-booking']//h2");
	protected By membershipChargesHeader = By
			.xpath("//div[@class='complete-booking']//section[@class='collapsible-table']/span");
	protected By totalPoints = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]//span");
	protected By totalPointsValue = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]//span//following-sibling::span");
	protected By totalPointsExp = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]//span/*");
	protected By totalPointsExpanded = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]//span/*[contains(@style,'transform')]");
	protected By userPointYear = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]/div[contains(@class,'dropdown-content')]//span");
	protected By userPointValue = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]/div[contains(@class,'dropdown-content')]//span/following-sibling::span");
	protected By rentedPointslabel = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]/div[contains(@class,'dropdown-content')]/div/span[.='Rented Points']");
	protected By rentedPointsValue = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]/div[contains(@class,'dropdown-content')]/div/span[.='Rented Points']/following-sibling::span");
	protected By borrowedPoints = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]/div[contains(@class,'dropdown-content')]/div/span[.='Borrowed']");

	protected By paymentCharges = By
			.xpath("//section[@class='complete-booking__payment-charges' and contains(.,'Payment Charges')]");

	protected By rentedPointCost = By
			.xpath("//section[contains(@class,'payment-charges')]/div/span[.='Points']/following-sibling::strong");
	protected By paypalPaymentMethod = By
			.xpath("//section[@class='complete-booking__payment-method']//label[@for='paypal']");
	protected By ccPaymentMethod = By
			.xpath("//section[@class='complete-booking__payment-method']//label[@for='credit-card']");
	protected By spclReqExp = By.xpath("//div[@id='special-requests']//a[@role='tab']");
	protected By roomNumber = By.xpath("//*[@id='room-number']");
	protected By cancellationPolicy = By.xpath("//div[@id='cancellation-policy']//a[@class='text-secondary']");
	protected By borrowedPointslabel = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]/div[contains(@class,'dropdown-content')]/div/span[contains(.,'Borrowed Points - Next Use Year')]");
	protected By borrowedPointsValue = By.xpath(
			"//div[@class='complete-booking']//section[contains(@class,'collapsible-table') and contains(@class,'dropdown-trigger')]/div[contains(@class,'dropdown-content')]/div/span[contains(.,'Borrowed Points - Next Use Year')]/following-sibling::span");
	protected By ppPaymentCharges = By
			.xpath("//div[contains(@class,'complete-booking__payment-charges-points-protection')]/strong");

	protected By housekeepingToolkit = By.xpath("//span[@id='house-keeping-tooltip']");
	protected By housekeepingCreditsRequired = By
			.xpath("//span[@id='house-keeping-tooltip']/parent::span/following-sibling::strong");
	protected By housekeepingToolkitMessage = By.xpath(
			"//div[text()='Housekeeping Credits are the method of payment used for the cleaning of a suite prior to your arrival. Housekeeping Credits are deducted along with your points when you make a reservation.' and @data-is-active='true']");
	protected By housekeepingTransactionExp = By
			.xpath("//span[@id='house-keeping-tooltip']/parent::span/*[@stroke and not(contains(@style,'180'))]");
	protected By housekeepingAvailableCurrentUseYearLabel = By.xpath(
			"// span[@id='house-keeping-tooltip']/ancestor::section/div/div/span[text()='Available - Current Use Year']");
	protected By housekeepingAvailableCurrentUseYearValue = By.xpath(
			"// span[@id='house-keeping-tooltip']/ancestor::section/div/div/span[text()='Available - Current Use Year']/following-sibling::span");
	protected By housekeepingPurchasedLabel = By.xpath(
			"// span[@id='house-keeping-tooltip']/ancestor::section/div/div/span[text()='Purchased Housekeeping Credits']");
	protected By housekeepingPurchasedValue = By.xpath(
			"// span[@id='house-keeping-tooltip']/ancestor::section/div/div/span[text()='Purchased Housekeeping Credits']/following-sibling::span");
	protected By housekeepingTransactionExpanded = By
			.xpath("//span[@id='house-keeping-tooltip']/parent::span/*[@stroke and contains(@style,'180')]");
	protected By housekeepingPurchaseCharges = By.xpath(
			"//section[@class='complete-booking__payment-charges']//span[.='Housekeeping Credits']/following-sibling::strong");

	protected By paypalCheckoutBtn = By.xpath("//div[contains(@class,'paypal-button') and @role='button']");
	protected By emailField = By.id("email");
	protected By passwordField = By.id("password");
	protected By nextBtn = By.id("btnNext");
	protected By loginBtn = By.id("btnLogin");
	protected By paymentContinueBtn = By.id("payment-submit-btn");
	protected By paypalPaymentConfirmationHeader = By.xpath("//span[text()='PayPal Account Verified']");
	protected By paypalFrame = By.xpath("//iframe[@class='zoid-component-frame zoid-visible']");
	protected By amountPayPalPaidBy = By.xpath("//span[text()='PayPal Account Verified']/../following-sibling::p/span");

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButtonForBooking() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(buttonBookingContinue);
		clickElementJSWithWait(buttonBookingContinue);
	}

	/*
	 * Method: detailsReservationBalance Description:Details Resevation Balance
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void detailsReservationBalance() {
		waitUntilElementVisibleBy(driver, textReservationBalance, "ExplicitLongWait");
		if (verifyObjectDisplayed(textReservationBalance)) {
			getElementInView(checkboxPurchaseReservationTransaction);
			clickElementJSWithWait(checkboxPurchaseReservationTransaction);
			clickContinueButtonForBooking();
			tcConfig.updateTestReporter("CUIBookPage", "detailsReservationBalance", Status.PASS,
					"Successfully filled details on Reservation Balance Page");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "detailsReservationBalance", Status.FAIL,
					"Reservation Balance Page not present");
		}
	}

	/*
	 * Method: selectPaymentMethod Description:Select payment Option Date field
	 * Date: June/2020 Author: Abhijeet Roy Changes By: Kamalesh
	 */
	public void selectPaymentMethod() {
		waitUntilElementVisibleBy(driver, headerCompleteBooking, "ExplicitLongWait");
		Assert.assertTrue(
				verifyObjectDisplayed(headerCompleteBooking) || verifyObjectDisplayed(headerCompleteModification), "Complete Booking Header Not Displayed");
		if (verifyObjectDisplayed(sectionPaymentmethod) || verifyObjectDisplayed(paymentMethodHeader)) {
			String paymentBy = testData.get("PaymentBy");
			if (paymentBy.equalsIgnoreCase("CreditCard")) {
				clickElementJSWithWait(selectCreditCard);
				tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.PASS,
						"Payment Method is selected as: " + paymentBy);
			} else if (paymentBy.equalsIgnoreCase("Paypal")) {
				getElementInView(selectPaypal);
				clickElementJSWithWait(selectPaypal);
				tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.PASS,
						"Payment Method is selected as: " + paymentBy);
			} else {
				tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.FAIL,
						"Provide which payment method to use");
			}

		} else {
			tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.FAIL,
					"Payment section not visible");
		}
	}

	/*
	 * Method: paymentViaPaypal Description:Select payment Option Date field
	 * Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void paymentViaPaypal() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		switchToFrame(paypalFrame);
		waitUntilElementVisibleBy(driver, paypalCheckoutBtn, "ExplicitLongWait");
		getElementInView(paypalCheckoutBtn);
		Assert.assertTrue(verifyObjectDisplayed(paypalCheckoutBtn), "Paypal Check Out button not displayed");
		clickElementBy(paypalCheckoutBtn);

		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(newWindow);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, emailField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(emailField), "Email field not displayed");
		fieldDataEnter(emailField, testData.get("PaypalEmail"));
		clickElementBy(nextBtn);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, passwordField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(passwordField), "Password field not displayeds");
		fieldDataEnter(passwordField, testData.get("PaypalPassword"));
		clickElementBy(loginBtn);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, paymentContinueBtn, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(paymentContinueBtn), "Continue Button not displayed");
		getElementInView(paymentContinueBtn);
		clickElementJSWithWait(paymentContinueBtn);

		ArrayList<String> updatedWindow = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(updatedWindow.get(0));
		
		driver.switchTo().defaultContent();

		waitUntilElementVisibleBy(driver, paypalPaymentConfirmationHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(paypalPaymentConfirmationHeader), "Payment Header not displayed");
		tcConfig.updateTestReporter("CUIBookPage", "paymentViaPaypal", Status.PASS,
				"Payment made via Paypal and account used is: " + getElementText(amountPayPalPaidBy));

	}

	/*
	 * Method: dataEntryForCreditCard Description:Fill Details for Credit Card
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryForCreditCard() {
		
		clickElementJSWithWait(selectCreditCard);
		String ccFirstName = testData.get("ccFirstName");
		String ccLastName = testData.get("ccLastName");
		String creditCardNumber = testData.get("creditCardNumber");
		String ccMonth = testData.get("ccMonth");
		String ccYear = testData.get("ccYear");
		String ccZipCode = testData.get("ccZipCode");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldCreditCardFirstName)) {
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = cap.getBrowserName().toUpperCase();
			if (browserName.contains("EDGE")|| browserName.contains("SAFARI")) {
				clickElementBy(fieldCreditCardFirstName);
				getObject(fieldCreditCardFirstName).sendKeys(testData.get("ccFirstName") + Keys.TAB);
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				clickElementBy(fieldCreditCardLastName);
				getObject(fieldCreditCardLastName).sendKeys(testData.get("ccLastName") + Keys.TAB);
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				clickElementBy(fieldCreditCardNumber);
				getObject(fieldCreditCardNumber).sendKeys(testData.get("creditCardNumber") + Keys.TAB);	
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				if (browserName.contains("EDGE")) {
					getObject(fieldCreditCardMonth).sendKeys(testData.get("ccMonth") + Keys.TAB);
					getObject(fieldCreditCardYear).sendKeys(testData.get("ccYear") + Keys.TAB);		
				}else{
					selectByText(fieldCreditCardMonth, ccMonth);
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					selectByText(fieldCreditCardYear, ccYear);
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
				}
				clickElementBy(fieldZipCode);
				getObject(fieldZipCode).sendKeys(testData.get("ccZipCode") + Keys.TAB);
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}else{
			fieldDataEnter(fieldCreditCardFirstName, ccFirstName);
			fieldDataEnter(fieldCreditCardLastName, ccLastName);
			fieldDataEnter(fieldCreditCardNumber, creditCardNumber);
			selectByText(fieldCreditCardMonth, ccMonth);
			selectByText(fieldCreditCardYear, ccYear);
			fieldDataEnter(fieldZipCode, ccZipCode);
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "dataEntryForCreditCard", Status.FAIL,
					"Fields for Credit Card data Entry not displayed");
		}

	}

	/*
	 * Method: clickBookNowButton Description:Click Book Now Button Date field
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickBookNowButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(buttonBookNow);
		clickElementJSWithWait(buttonBookNow);
		tcConfig.updateTestReporter("CUIBookPage", "clickBookNowButton", Status.PASS,
				"Successfully clicked on Book Now Button");
	}
	

	/*
	 * Method: getReservationNumber Description:Pick Reservation Number in
	 * Booking summary page Date field Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public String reservationNumber() {
		waitUntilElementVisibleBy(driver, textReservationSummary, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textConfirmationNumber), "Confirmation Number Not Displayed");
		tcConfig.updateTestReporter("CUIBookPage", "getReservationNumber", Status.PASS,
				"Reservation Number is: " + getObject(textConfirmationNumber).getText().trim());
		return getObject(textConfirmationNumber).getText().trim();
	}

	/*
	 * Method: validateMembershipCharges Description:verify section Membership
	 * Charges Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateMembershipCharges() {
		waitUntilElementVisibleBy(driver, headerCompleteBooking, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerCompleteBooking), "Complete Booking Header not displayed");
		if (verifyObjectDisplayed(sectionMembershipCharges)) {
			getElementInView(sectionMembershipCharges);
			tcConfig.updateTestReporter("CUIBookPage", "validateMembershipCharges", Status.PASS,
					"Section Membership Charges is Displayed");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateMembershipCharges", Status.FAIL,
					"Section Membership Charges not Displayed");
		}
	}

	/*
	 * Method: validateTotalPoints Description:verify Total Points required for
	 * reservation Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void validateTotalPoints() {
		if (verifyObjectDisplayed(valueTotalPoints)) {
			tcConfig.updateTestReporter("CUIBookPage", "validateTotalPoints", Status.PASS,
					"Total Points required for reservation is : " + getObject(valueTotalPoints).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateTotalPoints", Status.FAIL,
					"Total Points required for reservation is not Displayed");
		}
	}

	/*
	 * Method: getTotalPoints Description:verify Total Points required for
	 * reservation Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public String getTotalPoints() {
		if (verifyObjectDisplayed(valueTotalPoints)) {
			tcConfig.updateTestReporter("CUIBookPage", "validateTotalPoints", Status.PASS,
					"Total Points required for reservation is : " + getObject(valueTotalPoints).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateTotalPoints", Status.FAIL,
					"Total Points required for reservation is not Displayed");
		}

		return getElementText(valueTotalPoints);
	}

	/*
	 * Method: validateHousekeepingCharges Description:verify Housekeeping
	 * required for reservation Date field Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void validateHousekeepingCharges() {
		if (verifyObjectDisplayed(valueHousekeepingCharges)) {
			tcConfig.updateTestReporter("CUIBookPage", "validateHousekeepingCharges", Status.PASS,
					"Housekeeping required for reservation is : "
							+ getObject(valueHousekeepingCharges).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateHousekeepingCharges", Status.FAIL,
					"Housekeeping charges required for reservation is not Displayed");
		}
	}

	/*
	 * Method: validateReservationTransactionCharges Description:verify
	 * Reservation Transaction charges required for reservation Date field Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateReservationTransactionCharges() {
		if (verifyObjectDisplayed(valueTransactionCharges)) {
			tcConfig.updateTestReporter("CUIBookPage", "validateReservationTransactionCharges", Status.PASS,
					"Reservation Transaction charges required for reservation is : "
							+ getObject(valueTransactionCharges).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateReservationTransactionCharges", Status.FAIL,
					"Reservation Transaction charges required for reservation is not Displayed");
		}
	}

	/*
	 * Method: verifyPaymentChargesSectionNotPresent Description:verify Payment
	 * Charges Section not displayed as no payment is required Date field Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyPaymentChargesSectionNotPresent() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (!verifyObjectDisplayed(sectionPayementCharges)) {
			tcConfig.updateTestReporter("CUIBookPage", "verifyPaymentChargesSectionNotPresent", Status.PASS,
					"Payment Charges Section not displayed as no payment is required");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "verifyPaymentChargesSectionNotPresent", Status.FAIL,
					"Payment Charges Section is displayed even no payment is required, check data");
		}
	}

	/*
	 * Method: verifyPaymentMethodHeaderNotPresent Description:verify Payment
	 * Method Header not displayed as no payment is required Date field Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyPaymentMethodHeaderNotPresent() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (!verifyObjectDisplayed(headerPaymentMethod)) {
			tcConfig.updateTestReporter("CUIBookPage", "verifyPaymentMethodHeaderNotPresent", Status.PASS,
					"Payment Method Header not displayed as no payment is required");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "verifyPaymentMethodHeaderNotPresent", Status.FAIL,
					"Payment Method Header is displayed even no payment is required, check data");
		}
	}

	/*
	 * Method: verifyPaymentOptionsNotPresent Description:verify Payment Options
	 * not displayed as no payment is required Date field Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyPaymentOptionsNotPresent() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (!verifyObjectDisplayed(sectionPaymentOptions)) {
			tcConfig.updateTestReporter("CUIBookPage", "verifyPaymentOptionsNotPresent", Status.PASS,
					"Payment Options not displayed as no payment is required");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "verifyPaymentOptionsNotPresent", Status.FAIL,
					"Payment Options is displayed even no payment is required, check data");
		}
	}

	/*
	 * Method: validateBookingheader Description:verify booking header section
	 * with step number is present Date field Date: June/2020 Author: Monideep
	 * Roy Changes By: NA
	 */
	public void validateBookingHeader(String stepNumber) {


		waitUntilElementVisibleBy(driver, headerBooking, "ExplicitLongWait");

		if (getElementText(headerBooking).contains(stepNumber)
				&& getElementText(headerBooking).contains("COMPLETE BOOKING")) {
			tcConfig.updateTestReporter("CUIBookPage", "validateBookingHeader", Status.PASS,
					"Header displayed as expected : " + getElementText(headerBooking));
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateBookingHeader", Status.FAIL,
					"Header displayed not as expected : " + getElementText(headerBooking));
		}

	
	}

	/*
	 * Method: validateReservationBalanceHeader Description:verify Reservation
	 * balance Header Date field Date: June/2020 Author: Monideep Roy Changes
	 * By: NA
	 */
	public void validateReservationBalanceHeader() {

		if (getElementText(reservationBalanceHeader).toUpperCase().contains("RESERVATION BALANCE")) {
			tcConfig.updateTestReporter("CUIBookPage", "validateReservationBalanceHeader", Status.PASS,
					"Header displayed as expected : " + getElementText(reservationBalanceHeader));
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateReservationBalanceHeader", Status.FAIL,
					"Header displayed not as expected : " + getElementText(reservationBalanceHeader));
		}
	}

	/*
	 * Method: validateBookingHeaderNonVIP Description:verify booking header
	 * section with step number is present Date field Date: June/2020 Author:
	 * Monideep Roy Changes By: NA
	 */
	public void validateBookingHeaderNonVIP(String steps) {

		waitUntilElementVisibleBy(driver, headerBooking, "ExplicitLongWait");

		if (getElementText(headerBooking).contains(steps) && getElementText(headerBooking).contains("COMPLETE BOOKING")) {
			tcConfig.updateTestReporter("CUIBookPage", "validateBookingHeaderNonVIP", Status.PASS,
					"Header displayed as expected : " + getElementText(headerBooking));
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateBookingHeaderNonVIP", Status.FAIL,
					"Header displayed not as expected : " + getElementText(headerBooking));
		}

	}

	/*
	 * Method: validateMembershipChargesHeader Description:verify Membership
	 * charges Header Date field Date: June/2020 Author: Monideep Roychowdhury
	 * Changes By: NA
	 */

	public void validateMembershipChargesHeader() {

		if (getElementText(membershipChargesHeader).toUpperCase().contains("MEMBERSHIP CHARGES")) {
			tcConfig.updateTestReporter("CUIBookPage", "validateMembershipChargesHeader", Status.PASS,
					"Header displayed as expected : " + getElementText(membershipChargesHeader));
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateMembershipChargesHeader", Status.FAIL,
					"Header displayed not as expected : " + getElementText(membershipChargesHeader));
		}
	}

	/*
	 * Method: validateTotalPointsValue Description: validate Total points
	 * section field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */

	public void validateTotalPointsValue() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getElementText(totalPoints).trim().toUpperCase().equalsIgnoreCase("TOTAL POINTS")) {
			tcConfig.updateTestReporter("CUIBookPage", "validateTotalPointsValue", Status.PASS,
					"Total points section available");
			assertEquals(Integer.parseInt(getElementText(totalPointsValue).trim().replace(",", "")),
					Integer.parseInt(tcConfig.getTestData().get("UnitPoints")),
					"Total points displayed seems to be incorrect, Displayed : "
							+ Integer.parseInt(getElementText(totalPointsValue).trim().replace(",", "")));
			tcConfig.updateTestReporter("CUIBookPage", "validateTotalPointsValue", Status.PASS,
					"Total points value displayed correctly");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateTotalPointsValue", Status.FAIL,
					"Total points section not available");
		}

	}

	/*
	 * Method: validateUserPointsValue Description: validate user points section
	 * field Date: June/2020 Author: Monideep Roychowdhury Changes By: NA
	 */

	public void validateUserPointsValue() {

		assertTrue(verifyObjectDisplayed(totalPointsExp), "Total points drop down not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateUserPointsValue", Status.PASS,
				"Total points dropdown exists");
		clickElementBy(totalPointsExp);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(totalPointsExpanded), "Total points not expanded");

		if (getElementText(userPointYear).trim().toUpperCase()
				.contains(tcConfig.getTestData().get("UseYear").toUpperCase())) {
			tcConfig.updateTestReporter("CUIBookPage", "validateUserPointsValue", Status.PASS,
					"User points section available in total points dropdown");
			assertEquals(Integer.parseInt(getElementText(userPointValue).trim().replace(",", "")),
					Integer.parseInt(tcConfig.getTestData().get("AccountPoints")),
					"User points used seems to be incorrect, Displayed : "
							+ Integer.parseInt(getElementText(userPointValue).trim().replace(",", "")));
			tcConfig.updateTestReporter("CUIBookPage", "validateUserPointsValue", Status.PASS,
					"Used points value displayed correctly");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateUserPointsValue", Status.FAIL,
					"User points section not available in total points dropdown");
		}
	}

	/*
	 * Method: validateRentedPointsValue Description: validate rented points
	 * section field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */
	public void validateRentedPointsValue() {

		assertTrue(verifyObjectDisplayed(rentedPointslabel), "Rented Points label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateRentedPointsValue", Status.PASS,
				"Rented points section available in total points dropdown");

		assertEquals(Integer.parseInt(getElementText(rentedPointsValue).trim().replace(",", "")),
				Integer.parseInt(tcConfig.getTestData().get("RentedPoints")),
				"Rented points value seems to be incorrect");
		tcConfig.updateTestReporter("CUIBookPage", "validateRentedPointsValue", Status.PASS,
				"Rented points value displayed correctly, Displayed : "
						+ Integer.parseInt(getElementText(rentedPointsValue).trim().replace(",", "")));

		assertFalse(verifyObjectDisplayed(borrowedPoints));
		tcConfig.updateTestReporter("CUIBookPage", "validateRentedPointsValue", Status.PASS,
				"Borrowed points section not available in total points dropdown");

		clickElementBy(totalPointsExpanded);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(totalPointsExp), "Total points still expanded");
		assertFalse(verifyObjectDisplayed(rentedPointslabel),
				"Rented points shouldn't be visible after collapsing the dropdown");

	}

	/*
	 * Method: validatePaymentcharges Description: validate payment charges
	 * section field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */
	public void validatePaymentcharges() {

		assertTrue(verifyObjectDisplayed(paymentCharges), "Payment Charges section not present");
		tcConfig.updateTestReporter("CUIBookPage", "validatePaymentcharges", Status.PASS,
				"Payment charges section available");
	}

	/*
	 * Method: validatePaymentcharges Description: validate payment charges
	 * section field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */
	public void validateRentedPointsPaymentcharges() {

		assertEquals(getElementText(rentedPointCost), tcConfig.getTestData().get("RentedCost"),
				"Rented points cost dont match the value retrieved, which is : "
						+ tcConfig.getTestData().get("RentedCost"));
		tcConfig.updateTestReporter("CUIBookPage", "validateRentedPointsPaymentcharges", Status.PASS,
				"Rented point charges reflected correctly : " + getElementText(rentedPointCost));
	}

	/*
	 * Method: validatePaymentsection Description: validate payment method
	 * section field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */
	public void validatePaymentsection() {

		assertTrue(verifyObjectDisplayed(paypalPaymentMethod), "Paypal payment method not visible");
		tcConfig.updateTestReporter("CUIBookPage", "validatePaymentsection", Status.PASS,
				"Paypal payment method available");

		assertTrue(verifyObjectDisplayed(ccPaymentMethod), "CC payment method not visible");
		tcConfig.updateTestReporter("CUIBookPage", "validatePaymentsection", Status.PASS,
				"CC payment method available");

	}

	/*
	 * Method: validateAndEnterSpecialRequest Description: validate and enter
	 * special Request section field Date: June/2020 Author: Monideep
	 * Roychowdhury Changes By: Kamalesh
	 */
	public void validateAndEnterSpecialRequest() {

		assertTrue(verifyObjectDisplayed(spclReqExp), "Special Request drop down not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateAndEnterSpecialRequest", Status.PASS,
				"Special Request dropdown available");
		getElementInView(spclReqExp);
		clickElementJSWithWait(spclReqExp);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(roomNumber), "Room number field not present under special request");

		sendKeysBy(roomNumber, "A10");
		tcConfig.updateTestReporter("CUIBookPage", "validateAndEnterSpecialRequest", Status.PASS,
				"Value enter under Special Request section");

	}

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: Kamalesh
	 * Gupta
	 */
	public void verifyCompleteBookingPage() {
		waitUntilElementVisibleBy(driver, headerCompleteBooking, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerCompleteBooking),"Complete Booking page is not displayed");
		tcConfig.updateTestReporter("CUIBookPage", "verifyCompleteBookingPage", Status.PASS,
				"Complete Booking page is displayed");
	}

	/*
	 * Method: validateBorrowedPointsValue Description: validate borrowed points
	 * section field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */
	public void validateBorrowedPointsValue() {

		assertTrue(verifyObjectDisplayed(borrowedPointslabel), "Borrow Points label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateBorrowedPointsValue", Status.PASS,
				"Borrow points section available in total points dropdown");

		assertEquals(Integer.parseInt(getElementText(borrowedPointsValue).trim().replace(",", "")),
				Integer.parseInt(tcConfig.getTestData().get("BorrowedPoints")),
				"Borrow points value seems to be incorrect");
		tcConfig.updateTestReporter("CUIBookPage", "validateBorrowedPointsValue", Status.PASS,
				"Borrow points value displayed correctly, Displayed : "
						+ Integer.parseInt(getElementText(borrowedPointsValue).trim().replace(",", "")));

		assertFalse(verifyObjectDisplayed(rentedPointslabel));
		tcConfig.updateTestReporter("CUIBookPage", "validateBorrowedPointsValue", Status.PASS,
				"Borrowed points section not available in total points dropdown");

		clickElementBy(totalPointsExpanded);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(totalPointsExp), "Total points still expanded");
		assertFalse(verifyObjectDisplayed(borrowedPointslabel),
				"Rented points shouldn't be visible after collapsing the dropdown");

	}

	/*
	 * Method: validateHousekeepingPurchaseCharges Description: validate
	 * Housekeeping Purchase Charges Date: June/2020 Author: Monideep
	 * Roychowdhury Changes By: NA
	 */

	public void validatePPCharges() {
/*
		assertTrue(verifyObjectDisplayed(ppPaymentCharges), "Points Protection Payment Charges section not present");
		tcConfig.updateTestReporter("CUIBookPage", "validatePaymentcharges", Status.PASS,
				"Points Protection Payment Charges section available");
*/
	}

	/*
	 * Method: validateHousekeepingChargesNonVIP Description: validate House
	 * keeping Charges for NonVIP Date: June/2020 Author: Monideep Roychowdhury
	 * Changes By: NA
	 */

	public void validateHousekeepingChargesNonVIP() {

		assertTrue(verifyObjectDisplayed(housekeepingToolkit), "Housekeeping toolkit not present");
		assertEquals(getElementText(housekeepingCreditsRequired), tcConfig.getTestData().get("HouseKeepingCost"));
		hoverOnElement(housekeepingToolkit);

		assertTrue(verifyObjectDisplayed(housekeepingToolkitMessage), "Housekeeping toolkit message not present");

		assertTrue(verifyObjectDisplayed(housekeepingTransactionExp), "Housekeeping dropdown not present");
		clickElementBy(housekeepingTransactionExp);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		assertTrue(verifyObjectDisplayed(housekeepingAvailableCurrentUseYearLabel),
				"Housekeeping Available - Current Use Year label present and Housekeeping charges section expanded");
		assertEquals(getElementText(housekeepingAvailableCurrentUseYearValue),
				tcConfig.getTestData().get("HouseKeepingCreditsAvailable"));

		assertTrue(verifyObjectDisplayed(housekeepingPurchasedLabel), "Purchased Housekeeping Credits label present");
		assertEquals(getElementText(housekeepingPurchasedValue),
				tcConfig.getTestData().get("HouseKeepingPointsNeeded"));

		clickElementBy(housekeepingTransactionExpanded);
		assertTrue(verifyObjectDisplayed(housekeepingTransactionExp), "Housekeeping charges section is not collapsed");

		tcConfig.updateTestReporter("CUIBookPage", "validateHousekeepingChargesNonVIP", Status.PASS,
				"Non VIP Housekeeping charges section validated");
	}

	/*
	 * Method: validateHousekeepingPurchaseCharges Description: validate
	 * Housekeeping Purchase Charges Date: June/2020 Author: Monideep
	 * Roychowdhury Changes By: NA
	 */

	public void validateHousekeepingPurchaseCharges() {

		assertEquals(getElementText(housekeepingPurchaseCharges),
				tcConfig.getTestData().get("HousekeepingPurchaseCost"));

		tcConfig.updateTestReporter("CUIBookPage", "validateHousekeepingPurchaseCharges", Status.PASS,
				"Non VIP Housekeeping charges not appearing in payment section");
	}

	/*
	 * Method: validateRentedPointsIfSelected Description: validate Rented
	 * Points If Selected Date: June/2020 Author: Monideep Roychowdhury Changes
	 * By: NA
	 */
	public void validateRentedPointsIfSelected() {

		if (verifyObjectDisplayed(rentedPointslabel)) {

			assertEquals(Integer.parseInt(getElementText(rentedPointsValue).trim().replace(",", "")),
					Integer.parseInt(tcConfig.getTestData().get("RentedPoints")),
					"Rented points value seems to be incorrect");
			tcConfig.updateTestReporter("CUIBookPage", "validateRentedPointsValue", Status.PASS,
					"Rented points value displayed correctly, Displayed : "
							+ Integer.parseInt(getElementText(rentedPointsValue).trim().replace(",", "")));
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "validateRentedPointsIfSelected", Status.PASS,
					"Rent point option not selected");
		}

	}

	/*
	 * Method: validateAbsenceOfSpecialRequest Description:validate Absence Of
	 * Special Request Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */
	public void validateAbsenceOfSpecialRequest() {

		assertFalse(verifyObjectDisplayed(spclReqExp), "Special Request drop down not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateAndEnterSpecialRequest", Status.PASS,
				"Special Request dropdown not available");

	}

	/*
	 * Method: validateCancellationPolicyLink Description: validate cancellation
	 * policy link section field Date: June/2020 Author: Monideep Roychowdhury
	 * Changes By: NA
	 */
	public void validateCancellationPolicyLink() {

		assertTrue(verifyObjectDisplayed(cancellationPolicy), "Cancellation policy link not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateCancellationPolicyLink", Status.PASS,
				"Cancellation policy link  present");
	}

	/*
	 * Method: textCompleteBooking Description:Select payment Option Date field
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textCompleteBooking() {
		waitUntilElementVisibleBy(driver, headerCompleteBooking, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerCompleteBooking), "Complete Booking Header not displayed");
		tcConfig.updateTestReporter("CUIBookPage", "clickBookNowButton", Status.PASS,
				"Successfully Navigated to Complete Booking Page");
	}
	
	/**
	 * Method: checkRatePlanInDB Description: Validate Rate Plan value in
	 * Datebase Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	
	public void checkRatePlanInDB() throws Exception {
		List<String> roomBkgId = new ArrayList<String>();

		String queryToGetRoomBkgId = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND rer.res_num in ('"
				+ testData.get("ReservationNumber")+ "')";
		DBConnectionOnly();
		roomBkgId = ExecuteQueryAndFetchListColumData(queryToGetRoomBkgId, "ROOM_BKG_ID");

		String queryToGetRatePlanID = "select ROOM_RATE_PLAN_ID from RESERVATION.RH_ROOM_BKG_CHNG where ROOM_BKG_ID = '"
				+ roomBkgId.get(0) + "'";
		ratePlanId = ExecuteQueryAndFetchListColumData(queryToGetRatePlanID, "ROOM_RATE_PLAN_ID");

		tcConfig.updateTestReporter("CUIBookPage", "checkRatePlanInDB", Status.PASS,
				"Rate Plan before modification is " + ratePlanId.get(0));

		closeDB();
	}
	
	/*
	 * Method: getReservationNumber Description:Pick Reservation Number in
	 * Booking summary page Date field Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void getReservationNumber() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, textConfirmationNumber, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textConfirmationNumber), "Confirmation Number Not Displayed");
		tcConfig.updateTestReporter("CUIBookPage", "getReservationNumber", Status.PASS,
				"Reservation Number is: " + getObject(textConfirmationNumber).getText().trim());
		tcConfig.getTestData().put("ReservationNumber",getObject(textConfirmationNumber).getText().trim());
	}
}
