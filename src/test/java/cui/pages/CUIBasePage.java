package cui.pages;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CUIBasePage extends FunctionalComponents {
	
	Connection dbConnection;

	public CUIBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}
	
	/*
	 * *************************Method: checkAndSetBrowser ***
	 * ***************Description: Check Driver and Set Browser *********
	 */
	public WebDriver checkAndSetBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {

			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {

				log.info("Unable to launch driver : Error -" + e.getMessage());
			}
		}

		return tcConfig.getDriver();

	}
	
	/*
	 * *************************Method: DBConnection ***
	 * *************Description - Database connection
	 */

	public List<String> DBConnection(String query, String columnname) throws UnsupportedEncodingException {
		String hostname = tcConfig.getConfig().get("Hostname");
		int port = Integer.parseInt(tcConfig.getConfig().get("PORT"));
		String servicename = tcConfig.getConfig().get("SERVICE_NAME");
		String user = tcConfig.getConfig().get("USER");
		String password = tcConfig.getConfig().get("PASSWORD");
		String decodedPassword = decodePassword(password);
		List<String> stringList = new ArrayList<String>();
		try {
			// step1 load the driver class
			Class.forName("oracle.jdbc.driver.OracleDriver");

			// step2 create the connection object
			Connection con = DriverManager.getConnection(
					"jdbc:oracle:thin:@//" + hostname + ":" + port + "/" + servicename, user, decodedPassword);
			log.info("Database Connection established");

			// step3 create the statement object
			Statement stmt = con.createStatement();

			// step4 execute query
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				stringList.add(rs.getString(columnname));
			}

			// step5 close the connection object
			con.close();

		} catch (Exception e) {
			log.info(e);
		}
		return stringList;
	}

	/*
	 * *************************Method: DBConnection ***
	 * *************Description - Database connection
	 */

	public Connection DBConnectionOnly() throws UnsupportedEncodingException {
		String hostname = tcConfig.getConfig().get("Hostname");
		int port = Integer.parseInt(tcConfig.getConfig().get("PORT"));
		String servicename = tcConfig.getConfig().get("SERVICE_NAME");
		String user = tcConfig.getConfig().get("USER");
		String password = tcConfig.getConfig().get("PASSWORD");
		String decodedPassword = decodePassword(password);
		try {
			// step1 load the driver class
			Class.forName("oracle.jdbc.driver.OracleDriver");

			// step2 create the connection object
			dbConnection = DriverManager.getConnection(
					"jdbc:oracle:thin:@//" + hostname + ":" + port + "/" + servicename, user, decodedPassword);

		} catch (Exception e) {
			log.info(e);
		}
		return dbConnection;
	}

	/*
	 * *************************Method: ExecuteQueryAndFetchColumData ***
	 * *************Description - Execute DB Query And Fetch Colum Data
	 */
	public String ExecuteQueryAndFetchColumData(String query, String columnname) throws UnsupportedEncodingException {
		String value = null;
		try {
			// step3 create the statement object
			Statement dbStatement = dbConnection.createStatement();

			// step4 execute query
			ResultSet rs = dbStatement.executeQuery(query);
			while (rs.next()) {
				value = (rs.getString(columnname));
			}
		} catch (Exception e) {
			log.info(e);
		}
		return value;
	}

	/*
	 * *************************Method: ExecuteQueryAndFetchColumData ***
	 * *************Description - Execute DB Query And Fetching the list of
	 * Column Data
	 */
	public List<String> ExecuteQueryAndFetchListColumData(String query, String columnname)
			throws UnsupportedEncodingException {
		List<String> stringList = new ArrayList<String>();
		try {
			// step3 create the statement object
			Statement dbStatement = dbConnection.createStatement();

			// step4 execute query
			ResultSet rs = dbStatement.executeQuery(query);
			while (rs.next()) {
				stringList.add(rs.getString(columnname).trim());
			}
		} catch (Exception e) {
			log.info(e);
		}
		return stringList;
	}

	/*
	 * *************************Method: closeDB *** *************Description -
	 * Close Database connection
	 */
	public void closeDB() throws SQLException {
		try {
			dbConnection.close();
		} catch (Exception e) {
			log.info(e);
		}

	}
	
	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */
	/*
	 * ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	 * ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	 * *************************Method: checkAndSetBrowser ***
	 * ***************Description: Check Driver and Set Browser *********
	 */

	public WebDriver checkAndSetBrowser(String strBrowser) {
		if (tcConfig.getDriver() == null) {

			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser);
				tcConfig.setDriver(driver);
			} catch (Exception e) {

				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}
	
	/*
	 * *************************Method: closePopUpModal ***
	 * *************Description Close Any Pop Up Modal**
	 */

	public void closePopUpModalAndroid(By closeCTAElement) {
		if (verifyObjectDisplayed(closeCTAElement)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Cancel Cta present");
			clickElementBy(closeCTAElement);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Cancel CTA was closed successfully");

			try {
				clickElementByScriptExecutor("OK", 1);
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Pop Up Modal was present and was successfully closed");

			} catch (Exception e) {
				log.info("Pop Up Modal was either not present or was not closed");
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cancel CTA not present");
		}
	}
	
	/*
	 * *************************Method: closePopUpModal ***
	 * *************Description Close Any Pop Up Modal**
	 */

	public void closePopUpModal(By closeCTAElement, By closedModalElement, By elementFromPopUp, Status status) {
		if (verifyObjectDisplayed(closeCTAElement)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Cancel Cta present");
			clickElementBy(closeCTAElement);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (isAlertPresent()) {
				driver.switchTo().alert().accept();
				driver.switchTo().defaultContent();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Successfully accepetd the alert popup");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), status,
						"No popup came while Closing the pop up");
			}
			waitUntilElementVisibleBy(driver, closedModalElement, "ExplicitLongWait");
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = cap.getBrowserName().toUpperCase();
			if (browserName.contains("SAFARI")) {

			} else {
				sendKeyboardKeys(Keys.TAB);
			}
			if (verifyObjectDisplayed(elementFromPopUp)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Pop Up Modal did not Closed");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Pop Up Modal  Closed");
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cancel CTA not present");
		}
	}
	
	/*
	 * *************************Method: savedChangesValidation ***
	 * *************Description Save Any Pop Up Modal**
	 */

	public void savedChangesValidation(By dataField, String testDataColumn, String validationField) {
		waitUntilElementVisibleBy(driver, dataField, "ExplicitLongWait");
		String strOwnerAdd = getElementText(dataField).trim().toUpperCase();

		if (verifyObjectDisplayed(dataField)) {
			String tempAdd = testData.get(testDataColumn);
			tempAdd = tempAdd.toUpperCase();

			if (strOwnerAdd.contains(tempAdd)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						validationField + " Changes Saved Successfully, Change: " + strOwnerAdd);
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						validationField + " changes could not be validated");
			}
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Field is not present");
		}

	}

	protected By loadingSpinnerCUI = By.xpath("//div[@class='loading']");
	/*
	 * *************************Method: checkLoadingSpinnerCUI ***
	 * *************Description: CUI Loading Spinner**
	 */
	public void checkLoadingSpinnerCUI() {
		waitUntilElementInVisible(driver, loadingSpinnerCUI, 120);
	}
	
	/*
	 * * *************************Method: toolTipValidations ***
	 * ***************Description: toolTipValidations****
	 */

	public void toolTipValidations(By tootlTipXpath, String toolTipFor, By tootlTipTextXpath) {

		if (verifyObjectDisplayed(tootlTipXpath)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Tool Tip Present in the page for : " + toolTipFor);
			getElementInView(tootlTipXpath);
			clickElementJSWithWait(tootlTipXpath);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			toolTipCTAValidations(tootlTipTextXpath, toolTipFor);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Tool Tip not Present in the page for : " + toolTipFor);
		}

	}
	
	/*
	 * Method: toolTipCTAValidations Description: Tool Tip CTA Validations
	 * 
	 */
	public void toolTipCTAValidations(By tootlTipTextXpath, String toolTipFor) {

		String strFlag = getElementAttribute(tootlTipTextXpath, "aria-hidden");
		if (strFlag.contains("false")) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Tool Tip opened for " + toolTipFor);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Failed to open Tool tip for" + toolTipFor);
		}
	}

	protected By continueButtonCUI = By.xpath("//button[text() = 'Continue']");

	/*
	 * *************************Method: clickContinue ***
	 * *************Description: Click Continue Button uin CUI**
	 */

	public void clickContinue() {

		if (verifyObjectDisplayed(continueButtonCUI)) {
			getElementInView(continueButtonCUI);
			getObject(continueButtonCUI).click();

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Error in clicking Continue CTA");
		}

	}


}
