package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import automation.core.TestConfig;

public class CUIReservationBalancePage_Android extends CUIReservationBalancePage_Web {

	public static final Logger log = Logger.getLogger(CUIReservationBalancePage_Android.class);

	public CUIReservationBalancePage_Android(TestConfig tcconfig) {
		super(tcconfig);
		bookingHeader = By.xpath("//div[@class='booking-header__mobile']//p");
	}

	CUIReservationBalancePage_IOS reservationBlnIOSPage = new CUIReservationBalancePage_IOS(tcConfig);

	public By pageHeader = By.xpath("//div[contains(@class,'booking')]//div[contains(@class,'title')]");

	/*
	 * Method: validateReservationBalancePage Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Monideep
	 * Changes By: Kamalesh
	 */
	public void validateReservationBalancePage() {
		reservationBlnIOSPage.validateReservationBalancePage();
	}

	/*
	 * Method: selectAndUpdateBorrowPoints selectAndUpdateBorrowPoints Date
	 * field Date: June/2020 Author: Monideep Changes By: Kamalesh
	 */
	public void selectAndUpdateBorrowPoints() {
		reservationBlnIOSPage.selectAndUpdateBorrowPoints();

	}

	/*
	 * Method: selectAndUpdateRentPoints Description:Validate Borrow Rent Point
	 * is Not displayed Date field Date: June/2020 Author: Monideep Changes By:
	 * NA
	 */
	public void selectAndUpdateRentPoints() {
		reservationBlnIOSPage.selectAndUpdateRentPoints();
	}

	/*
	 * Method: selectAndUpdateHouseKeepingBorrowPoints Description:
	 * selectAndUpdateHouseKeepingBorrowPoints Date field Date: June/2020
	 * Author: Monideep Changes By: KG
	 */
	public void selectAndUpdateHouseKeepingBorrowPoints() {
		reservationBlnIOSPage.selectAndUpdateHouseKeepingBorrowPoints();
	}

	/*
	 * Method: borrowValidPoint Description: borrowValidPoint Date field Date:
	 * June/2020 Author: Kamalesh
	 */
	public void borrowValidPoint() {
		reservationBlnIOSPage.borrowValidPoint();
	}

	/*
	 * Method: rentValidPoint Description: rentValidPoint Date field Date:
	 * June/2020 Author: Kamalesh
	 */
	public void rentValidPoint() {
		reservationBlnIOSPage.rentValidPoint();
	}

	/*
	 * Method: setRentBorrowPoint Description: setRentBorrowPoint Date field
	 * Date: June/2020 Author: Kamalesh
	 */
	public void setRentBorrowPoint() {
		reservationBlnIOSPage.setRentBorrowPoint();
	}

	/*
	 * Method: setRentBorrowHK Description: setRentBorrowHK Date field Date:
	 * June/2020 Author: Kamalesh
	 */
	public void setRentBorrowHK() {
		reservationBlnIOSPage.setRentBorrowHK();
	}

}