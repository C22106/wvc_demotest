package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIModifyReservationPage_IOS extends CUIModifyReservationPage_Web {

	public static final Logger log = Logger.getLogger(CUIModifyReservationPage_IOS.class);

	public CUIModifyReservationPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}
	public By expandReservationDetails=By.xpath("//button[@class = 'reservation-summary__headline-arrow']");
	
	/*
	 * Method: checkSectionReservationSummary Description:check Section
	 * Reservation Summary Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkSectionReservationSummary() {
		getElementInView(headerReservationDetails);
		clickElementBy(expandReservationDetails);
		waitForSometime(tcConfig.getConfig().get("MedWait"));	
		waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
		if (verifyObjectDisplayed(sectionReservationSummary)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkSectionReservationSummary", Status.PASS,
					"Reservation section is present in the Modify reservation page");
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkSectionReservationSummary", Status.PASS,
					"Reservation section is not present in the Modify reservation page");
		}
	}
}
