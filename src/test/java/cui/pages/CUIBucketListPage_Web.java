package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBucketListPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIBucketListPage_Web.class);

	public CUIBucketListPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By ownerFullName = By.xpath("//h4[contains(.,'WELCOME')]/parent::div/h2");
	protected By bucketListNavigationLink = By
			.xpath("//nav[contains(@class,'side')]//span[@class='account-navigation__text' and contains(.,'List')]");
	protected By bucketListHeader = By.xpath("// h1[contains(.,'BUCKET LIST')]");
	protected By totalBucketList = By.xpath("// h1[contains(.,'BUCKET LIST')]/following-sibling::p");
	protected By bucketListCards = By.xpath("// div[contains(@class,'component')]/a");
	protected By wishlistedHeartIcons = By
			.xpath("// section[contains(@class,'wish-list')]//button[contains(@aria-label,'Remove')]");
	protected By resortImages = By.xpath("// section[contains(@class,'wish-list')]//figure[contains(@class,'image')]");
	protected By resortNames = By.xpath("// section[contains(@class,'wish-list')]//a[contains(@class,'subtitle')]");
	protected By resortLocations = By.xpath("// section[contains(@class,'wish-list')]//p/span");
	protected By navigatedResortPage = By.xpath("(// h1[contains(@class,'title-1')] | // div[contains(@class,'title-1 m')])");
	protected By whereToNextCard = By.xpath("// div[contains(@class,'wish-list')]/h3[contains(.,'Where to Next')]");
	protected By whereToNextText = By
			.xpath("// h3[contains(.,'Where to Next')]/following-sibling::p[contains(.,'Time to build')]");
	protected By exploreResortCTA = By.xpath("// div[contains(@class,'wish-list')]//a[contains(@class,'button')]");
	protected By notWishlishtedCard = By
			.xpath("// button[contains(@aria-label,'Add')]/ancestor::div[@class='resort-card']//h2/a");
	protected By notWishlistedIcon = By.xpath("// button[contains(@aria-label,'Add')]");
	protected By locationField = By.xpath("//input[contains(@placeholder,'location')]");

	/*
	 * Method: navigateToBucketListPage Description:Navigate to Bucket List Page
	 * Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void navigateToBucketListPage() {
		waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		if (verifyObjectDisplayed(bucketListNavigationLink)) {
			clickElementBy(bucketListNavigationLink);
			waitUntilElementVisibleBy(driver, bucketListHeader, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(bucketListHeader),"Bucket List Header Not Displayed");
			tcConfig.updateTestReporter("CUIBucketListPage", "navigateToBucketListPage", Status.PASS,
					"User Navigated to Bucket list page with " + getElementText(totalBucketList));

		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "navigateToBucketListPage", Status.FAIL,
					"User failed to Navigated to Bucket List page");
		}
	}

	/*
	 * Method: totalResortsWishlisted Description:Get Total Resort Numbers Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public int totalResortsWishlisted() {

		// to store total resorts
		int totalResort = 0;
		// resort text
		String strResortsText = getElementText(totalBucketList).trim();
		strResortsText = strResortsText.split(" ")[0];
		totalResort = Integer.parseInt(strResortsText);

		return totalResort;
	}

	/*
	 * Method: resortCardsNumbers Description:Validate total resort cards Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void resortCardsNumbers() {
		getElementInView(getList(bucketListCards).get(9));
		if (getList(bucketListCards).size() == totalResortsWishlisted()) {
			tcConfig.updateTestReporter("CUIBucketListPage", "resortCardsNumbers", Status.PASS,
					"Total " + totalResortsWishlisted() + " Resort cards displyed correctly as a link");
		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "resortCardsNumbers", Status.FAIL,
					"Total Resort displyed numbers did not match ");
		}

	}

	/*
	 * Method: resortCardsValidations Description:Validate total resort cards
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void resortCardContentValidations() {
		elementListPresenceVal(resortImages, totalResortsWishlisted(), "Resort Images ", "CUIBucketListPage",
				"resortCardContentValidations");
		elementListPresenceVal(resortNames, totalResortsWishlisted(), "Resort Names ", "CUIBucketListPage",
				"resortCardContentValidations");
		elementListPresenceVal(wishlistedHeartIcons, totalResortsWishlisted(), "Resort Heart Icon ",
				"CUIBucketListPage", "resortCardContentValidations");
		elementListPresenceVal(resortLocations, totalResortsWishlisted(), "Resort Location ", "CUIBucketListPage",
				"resortCardContentValidations");

	}

	/*
	 * Method: whereToNextCard Description:Validate Where To Next cards Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void whereToNextCard() {
		getElementInView(whereToNextCard);
		elementPresenceVal(whereToNextCard, "Where To Next Card", "CUIBucketListPage", "whereToNextCard");
		elementPresenceVal(whereToNextText, "Where To Next Description", "CUIBucketListPage", "whereToNextCard");
		elementPresenceVal(exploreResortCTA, "Explore Resort CTA ", "CUIBucketListPage", "whereToNextCard");

	}

	/*
	 * Method: resortCardNavigation Description:Validate Resort card Navigation
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void resortCardNavigation_UJ() {
		// String get first resort name
		
		String strFirstResortName = getList(resortNames).get(0).getText().trim();
		getElementInView(getList(resortNames).get(0));
		getList(resortNames).get(0).click();
		waitUntilElementVisibleBy(driver, navigatedResortPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(navigatedResortPage), "Resort Page navigation failed");
		if (getElementText(navigatedResortPage).trim().contains(strFirstResortName)) {
			tcConfig.updateTestReporter("CUIBucketListPage", "resortCardNavigation", Status.PASS,
					"Resort Navigated Successfully");
		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "resortCardNavigation", Status.FAIL,
					"Resort Navigation Failed");
		}
		navigateBackAndWait(bucketListHeader);

	}

	/*
	 * Method: exploreCTANavigation Description:Validate Explore CTA Navigation
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void exploreCTANavigation() {
		clickElementBy(exploreResortCTA);
		waitUntilElementVisibleBy(driver, locationField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(locationField), "Location Field Not Displayed");
		tcConfig.updateTestReporter("CUIBucketListPage", "exploreAllResortValdation", Status.PASS,
				"Navigated To Resorts Page");

	}

	/*
	 * Method: wishListFirstCard Description: WishList First Card Date: Jun/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public String wishListFirstCard() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getElementInView(getList(notWishlishtedCard).get(0));
		// get Name of first card that is not wishlisted
		String strFirstCard = getList(notWishlishtedCard).get(0).getText();
		getList(notWishlistedIcon).get(0).click();
		tcConfig.updateTestReporter("CUIBucketListPage", "wishListFirstCard", Status.PASS, "Wishlisted first cards");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		return strFirstCard;
	}

	/*
	 * Method: wishListedCardValidation Description: WishListed First Card Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListedCardValidation() {

		String strFirstCard = wishListFirstCard();
		navigateBackAndWait(bucketListHeader);
		sendKeyboardKeys(Keys.HOME);
		if (getList(resortNames).get(0).getText().contains(strFirstCard)) {
			tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.PASS,
					"The recently wishlisted card is displayed at first");
		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.FAIL,
					"The recently wishlisted card is not displayed at first");
		}

	}

	/*
	 * Method: removeWishlishtedCard Description: Remove Wishlisted Card Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void removeWishlishtedCard() {
		int resortsBefore = totalResortsWishlisted();
		String strFirstCard = getList(resortNames).get(0).getText();
		clickElementBy(getList(wishlistedHeartIcons).get(0));
		acceptAlert();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		refreshPage();
		int resortsAfter = totalResortsWishlisted();
		if (resortsBefore == (resortsAfter+1)) {
			if (getList(resortNames).get(0).getText().contains(strFirstCard)) {
				tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.FAIL,
						"The removed wishlisted card is still displayed");
			} else {
				tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.PASS,
						"The removed wishlisted card is not displayed");
			}
		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.FAIL,
					"Numbers did not change after removing one card, Previous: " + resortsBefore + ", resorts after "
							+ resortsAfter);
		}

	}

}
