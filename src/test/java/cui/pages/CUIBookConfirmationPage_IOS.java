package cui.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBookConfirmationPage_IOS extends CUIBookConfirmationPage_Web {
	public static final Logger log = Logger.getLogger(CUIBookConfirmationPage_IOS.class);

	public CUIBookConfirmationPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		
		cancellationPolicyDetailsLink = By.xpath(
				"(//h3[.='Cancellation Policy']/parent::div/a[contains(@href,'canceling-reservation') and contains(.,'View')])[2]");
		cancellationPolicyText = By.xpath(
				"//*[contains(text(),'If your reservation is cancelled 15 days or more prior to the check-in date')]");
		upcomingVacationsLink = By
				.xpath("//section[contains(@class,'global-account--mobile')]//a[contains(@href,'/vacations')]/span");
	}
	public By cancelHeader=By.xpath("//h3[.='CANCELLATION POLICY']");
	public By hamburger = By.xpath("//button[contains(@class,'hamburger')]");
	/*
	 * Method: navigateToUpcomingVacations Description: Navigate to upcoming
	 * booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Monideep Roychowdhury Changes By: NA
	 */

	public void navigateToUpcomingVacations() {

		clickElementBy(hamburger);

		getElementInView(upcomingVacationsLink);
		assertTrue(verifyObjectDisplayed(upcomingVacationsLink), "Upcoming Vacations link not present");
		clickElementBy(upcomingVacationsLink);

		waitUntilElementVisibleBy(driver, reservationCards, "ExplicitMedWait");
		assertTrue(verifyObjectDisplayed(reservationCards), "Unable to navigate to upcoming vacations page");

		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "navigateToUpcomingVacations", Status.PASS,
				"Successfully navigated to Upcoming vacations page");
	}

	/*
	 * Method: validateChargesSummary Description: validate Charges Summary:
	 * June/2020 Author: Monideep Roychowdhury Changes By:
	 */
	public void validateChargesSummary() {

		assertTrue(verifyObjectDisplayed(chargerSummaryHeader), "Charges summary header not present");
		assertTrue(verifyObjectDisplayed(membershipChargesHeader), "Membership charges header not present");

		assertTrue(getElementText(houseKeepingCost).equalsIgnoreCase(tcConfig.getTestData().get("HouseKeepingCost")),
				"House keeping credits dont match");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateChargesSummary", Status.PASS,
				"House keeping credits required for booking matches");

		//assertTrue(verifyObjectDisplayed(totalHouseKeepingExp), "Total House keeping drop down not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Total House keeping dropdown exists");

		//clickElementBy(totalHouseKeepingExp);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(totalHouseKeepingExpanded), "Total House keeping drop down not expanded");

		assertTrue(
				getElementText(houseKeepingCurrentYear)
						.equalsIgnoreCase(tcConfig.getTestData().get("HouseKeepingCreditsAvailable")),
				"Current year house keeping credits does not matches");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Current year house keeping credits matches");

		assertTrue(
				getElementText(houseKeepingborrowed)
						.equalsIgnoreCase(tcConfig.getTestData().get("HouseKeepingBorrowPoints")),
				"Borrowed house keeping credits does not matches");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Borrowed house keeping credits matches");

		clickElementBy(reservationTransactionTooltip);
		assertTrue(verifyObjectDisplayed(reservationTransactionText), "Tooltip text not present");

		assertTrue(getElementText(reservationTransaction).equalsIgnoreCase("1"),
				"Reservation transaction value does not matches");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Reservation transaction value matches");

	}

	/*
	 * Method: validateCancellationPolicy Description: Navigate to upcoming
	 * booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Monideep Roychowdhury Changes By: Kamalesh
	 */

	public void validateCancellationPolicy() {
		getElementInView(cancelHeader);
		assertTrue(verifyObjectDisplayed(getList(cancellationPolicyText).get(1)),
				"Cancellation Text not present");
		assertTrue(verifyObjectDisplayed(cancellationPolicyDetailsLink),
				"Cancellation policy details link not present");

		clickElementBy(cancellationPolicyDetailsLink);
		waitUntilElementVisibleBy(driver, cancellationPolicyHeader, "ExplicitLongWait");
		tcConfig.updateTestReporter("CUIBookPage", "validateCancellationPolicy", Status.PASS,
				"Cancellation policy text and link validated sucessfully");

	}
	
	/*
	 * Method: validateReservationSummaryDetails Description: Navigate to
	 * upcoming booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Kamalesh Changes By:
	 */

	public void validateReservationSummaryDetails() {

		assertTrue(verifyObjectDisplayed(reservationSummaryHeader), "Reservation summary header not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Summary header and message present");

		assertTrue(verifyObjectDisplayed(reservationNumberHeader), "Reservation number header not present");
		assertTrue(getElementText(reservationNumber).length() > 0, "Reservation number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Number present : " + getElementText(reservationNumber));

		assertTrue(verifyObjectDisplayed(memberNumberHeader), "Member number header not present");
		assertTrue(getElementText(memberNumber).length() > 0, "Member number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Member Number present : " + getElementText(memberNumber));


	}

}
