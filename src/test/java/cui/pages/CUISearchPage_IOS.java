package cui.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;


public class CUISearchPage_IOS extends CUISearchPage_Web {

	public static final Logger log = Logger.getLogger(CUISearchPage_IOS.class);
	public By showMap = By.xpath("//button[text()='Show Map']");
	public By showList = By.xpath("//button[text()='Show List']");
	public By clearButton = By.xpath("//button[@class='input-clear-button']");
	public By hamburgerMenu = By.xpath("//button[contains(@class,'hamburger')]");
	public CUISearchPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		buttonFilterSelect = By.xpath("//button[@class = 'button -full view-resorts']");
		flexibleResultDatePill = By.xpath(
				"//div[contains(@class,'pill-container')]//div[@class = 'pill']/a//span[@class='mobile'or @class='desktop']");
		objcheckFlexDateText = By.xpath("//h1[contains(text(),'Flexible')]");
		accountPoints = By.xpath("//section[contains(@class,'global-account--mobile')]//strong[@class='owner-points']");
	}

	/*
	 * Method: validateMapPresent Description:Check Map Displayed Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: Kamalesh
	 */
	public void validateMapPresent() {
		clickElementBy(showMap);
		waitUntilElementVisibleBy(driver, map, "ExplicitLongWait");
		if (verifyObjectDisplayed(map)) {
			tcConfig.updateTestReporter("CUISearchPage", "validateMapPresent", Status.PASS,
					"Map Present in Search Page");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateMapPresent", Status.FAIL,
					"Map not Present in Search Page");
		}
		clickElementBy(showList);
	}

	/*
	 * Method: verifyFirstResort Description:Verify First Resort Found
	 * Date:Mar/2020 Author: Abhijeet Roy Changes By: Kamalesh
	 */
	public void verifyFirstResort() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(showMap);
		waitUntilElementVisibleBy(driver, map, "ExplicitLongWait");
		clickElementBy(showList);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String location = testData.get("CUI_Location").trim();
		String locationFirst = getObject(firstResort).getText().trim();
		if (location.equalsIgnoreCase(locationFirst)) {
			tcConfig.updateTestReporter("CUISearchPage", "verifyFirstResort", Status.PASS,
					"First resort name matched with search result and the resort is : " + locationFirst);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyFirstResort", Status.FAIL,
					"First resort after searching does not match with searched one");
		}

	}

	/*
	 * Method: validateSearchResult Description:Validate Seacrh Result Date
	 * field Date: Mar/2020 Author: Abhijeet Roy Changes By: Kamalesh
	 */
	public void validateSearchResult() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(showMap);
		waitUntilElementVisibleBy(driver, map, "ExplicitLongWait");
		clickElementBy(showList);
		if (verifyObjectDisplayed(SearchResult)) {
			String result = getObject(SearchResult).getText().split(" ")[0].trim();
			int resultFinal = Integer.parseInt(result);
			if (resultFinal > 0) {
				if (getList(resortCard).size() == resultFinal) {
					tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.PASS,
							"Total number of Exact resort is: " + getList(resortCard).size());
				} else {
					tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.FAIL,
							"Total number of resort not matching");
				}
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.FAIL,
						"No search result found");
			}
		}
	}

	/*
	 * Method: changeLocation Description:Select Location Date field Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void changeLocation() {
		String newlocation = testData.get("newLocation").trim();
		clickElementBy(showMap);
		waitUntilElementVisibleBy(driver, map, "ExplicitLongWait");
		clickElementBy(showList);
		if (verifyObjectDisplayed(locationChange)) {
			clickElementBy(locationChange);
			clickElementBy(clearButton);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Enter a location", newlocation, 1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysToElement(locationChange, Keys.ARROW_DOWN);
			sendKeysToElement(locationChange, Keys.ENTER);
			tcConfig.updateTestReporter("CUISearchPage", "changeLocation", Status.PASS,
					"Location changed successfully and Location selected is: " + newlocation);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "changeLocation", Status.FAIL,
					"Failed while changing Location");
		}
	}

	/*
	 * Method: changeCheckinDate Description:Select New Check in date from
	 * calander Date field Date: Mar/2020 Author: Abhijeet Roy Changes By:
	 * Kamalesh
	 */
	public void changeCheckinDate() {
		String changedCheckinDate = testData.get("changedCheckindate").trim();

		if (verifyObjectDisplayed(textBoxDateSelect)) {
			clickElementBy(textBoxDateSelect);
			tcConfig.updateTestReporter("CUISearchPage", "changeCheckinDate()", Status.PASS,
					"Successfully clicked on select date text box");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "changeCheckinDate()", Status.FAIL,
					"Failed while clicking on select date text box");
		}

		String day = (changedCheckinDate.split(" ")[0]).trim();
		String month = (changedCheckinDate.split(" ")[1]).trim();
		String year = (changedCheckinDate.split(" ")[2]).trim();
		String monthyear = month.concat(" ").concat(year);
		myaccountPage.selectDate(monthyear, day, changedCheckinDate, "Changed Checkin");

	}

	/*
	 * Method: returnConvertedDate Description:Return converted flexible date
	 * into String Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public List<String> returnConvertedDate(List<String> strDate, String addYear) {
		// List for updated flexible dates displayed in UI
		List<String> updatedDate = new ArrayList<String>();
		try {
			for (int totdalDate = 0; totdalDate < strDate.size(); totdalDate++) {
				// Check In Date from UI in format(Monday 4/13 → Thursday 4/16)
				String strCheckIn = strDate.get(totdalDate).split("→")[0];
				strCheckIn = strCheckIn.toLowerCase().split(" ")[1].trim();
				strCheckIn = strCheckIn.concat("/" + addYear);
				updatedDate.add(strCheckIn);

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISearcPage", "returnConvertedDate", Status.FAIL,
					"Date format not supported");
		}

		return updatedDate;

	}

	/*
	 * Method: verifyCheckinDateSelected Description:compare checkin date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyCheckinDateSelected() throws Exception {
		String checkInDate = testData.get("Checkindate").trim();
		if (verifyObjectDisplayed(textBoxDateSelect)) {
			String checkInDateScreen = (getObject(textBoxDateSelect).getAttribute("value").split("→")[0]).trim();
			// String checkInDateScreen =
			// (getObject(textBoxDateSelect).getAttribute("value").split("â†’")[0]).trim();
			String convertedCheckInDate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkInDate);
			if (checkInDateScreen.compareTo(convertedCheckInDate) == 0) {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.PASS,
						"Check in date selected is : " + checkInDate + " and from screen is: " + convertedCheckInDate);
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckinDateSelected", Status.FAIL,
						"Check in date selected and in screen did not matched");
			}

		} else {
			tcConfig.updateTestReporter("CUISearcPage", "verifyResortCheckinDateSelected", Status.FAIL,
					"Date Field in Search Page not Present");
		}

	}

	/*
	 * Method: verifyCheckoutDateSelected Description:compare checkout date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyCheckoutDateSelected() throws Exception {
		String checkOutDate = testData.get("Checkoutdate").trim();
		if (verifyObjectDisplayed(textBoxDateSelect)) {
			// String checkInDateScreen =
			// (getObject(textBoxDateSelect).getAttribute("value").split("â†’")[1]).trim();

			String checkInDateScreen = (getObject(textBoxDateSelect).getAttribute("value").split("→")[1]).trim();
			String convertedCheckOutDate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkOutDate);
			if (checkInDateScreen.compareTo(convertedCheckOutDate) == 0) {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.PASS,
						"Check out date selected is : " + checkOutDate + " and from screen is: "
								+ convertedCheckOutDate);
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.FAIL,
						"Check out date selected and in screen did not matched");
			}

		} else {
			tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.FAIL,
					"Date Field in Search Page not Present");
		}

	}


	
	/*
	 * Method: clickBookButtonOfHigherCostThanAccountPoints
	 * Description:clickBookButtonOfHigherCostThanAccountPoints Date field Date:
	 * June/2020 Author: Monideep Roy Changes By: Kamalesh
	 */
	public void clickBookButtonOfHigherCostThanAccountPoints() {

		boolean blnUnitFoundFlag = false;
		clickElementBy(hamburgerMenu);
		int availablePoints = parsePoints(driver.findElement(accountPoints));
		tcConfig.getTestData().put("AccountPoints", availablePoints + "");
		clickElementBy(hamburgerMenu);
		
		List<WebElement> listOfPrices = getList(unitPriceObj);

		if (listOfPrices.size() == 0) {
			listOfPrices = getList(unitPriceObjNoDiscount);
		}

		for (int i = 0; i < listOfPrices.size(); i++) {

			int unitPricePoint = parsePoints(listOfPrices.get(i));
			if (unitPricePoint > availablePoints) {

				driver.findElement(By.xpath("(//a[text()='Book'])[" + (i + 1) + "]")).click();
				waitUntilElementVisibleBy(driver, textBookDetails, "ExplicitLongWait");
				if (verifyObjectDisplayed(textBookDetails)) {
					tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.PASS,
							"Successfully clicked on Book Button, Unit Value : " + unitPricePoint);
				} else {
					tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.FAIL,
							"Failed to click on Book Button");
				}
				blnUnitFoundFlag = true;
				tcConfig.getTestData().put("UnitPoints", unitPricePoint + "");
				break;
			}
		}

		if (!blnUnitFoundFlag) {
			tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.FAIL,
					"Failed to find unit with cost higher than account balance");
		}

	}
}
