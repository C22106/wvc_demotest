package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUINewPasswordPage_Android extends CUINewPasswordPage_Web {

	public static final Logger log = Logger.getLogger(CUINewPasswordPage_Android.class);

	public CUINewPasswordPage_Android(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method: newPasswordDataEnter Description: New Password Page Val Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newPasswordDataEnter() {
		waitUntilElementVisibleBy(driver, newPasswordField, "ExplicitLongWait");
		getElementInView(newPasswordField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password", testData.get("Password"), 3);
	}

}