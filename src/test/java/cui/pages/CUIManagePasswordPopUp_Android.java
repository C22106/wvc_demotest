package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUIManagePasswordPopUp_Android extends CUIManagePasswordPopUp_Web {

	public static final Logger log = Logger.getLogger(CUIManagePasswordPopUp_Android.class);
	
	protected By passwordlabel = By.xpath("//h5[contains(text(),'PASSWORD')]");

	public CUIManagePasswordPopUp_Android(TestConfig tcconfig) {
		super(tcconfig);
		editPasswordLink = By.xpath("//*[@class='manage-password']//button");
		passwordCancelCTA = By.xpath("//*[@id='password-modal']//button[contains(@class,'cancel-changes')]");
		closedPasswordModal = By.xpath("//div[@class='manage-password']//button[contains(.,'Edit')]");

	}
	
	
	/*
	 * /* Method: enterNewPassword Description: Enter New Password Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterNewPassword() throws Exception {
		// To Decode passwords
		String decodedPassword = decodePassword(testData.get("CUI_password"));
		
		getElementInView(newPasswordField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "New Password",decodedPassword, 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(passwordlabel);
	}
	
	/*
	 * /* Method: passwordCancelCTAF Description: Close Manage Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordCancelCTA() {

		closePopUpModalAndroid(passwordCancelCTA);

	}
	
	/*
	 * Method: invalidNewPassword Description: Invalid Data Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void invalidNewPassword() {
		
		getElementInView(newPasswordField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "New Password",testData.get("Password"), 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(passwordlabel);
	}
	
	/*
	 * Method: incorrectDataCurrent Description: Invalid Data Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void incorrectDataCurrent() {
		getElementInView(currentPasswordField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Current Password",testData.get("Invalid_Pass"), 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(passwordlabel);
	}
	

}


