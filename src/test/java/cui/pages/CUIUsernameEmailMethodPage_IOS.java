package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIUsernameEmailMethodPage_IOS extends CUIUsernameEmailMethodPage_Web {

	public static final Logger log = Logger.getLogger(CUIUsernameEmailMethodPage_IOS.class);

	public CUIUsernameEmailMethodPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		

	}

	/*
	 * Method: verificationCodeErrors Description:Verirification Code Error Val
	 * Date: Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void verificationCodeErrors() {
		//invalid data for verification	
		String strInvalidCode = testData.get("invalidData");

		for (int noOfTries = 0; noOfTries < 3; noOfTries++) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Verification Code", strInvalidCode,2);
			//fieldDataEnter(verificationCodeField, strInvalidCode);
			clickContinue();
			waitUntilElementVisibleBy(driver, errorBlock, "ExplicitLongWait");
			if (noOfTries < 2) {
				fieldPresenceVal(twoTryError, (noOfTries+1) + " try error ", "CUIUsernameEmailMethodPage", "emailPageVal",
						getElementText(twoTryError));
			} else if (noOfTries >= 2) {
				fieldPresenceVal(thirdTryError, (noOfTries+1) + "rd try error ", "CUIUsernameEmailMethodPage", "emailPageVal",
						getElementText(thirdTryError));
			} else {
				tcConfig.updateTestReporter("CUIUsernameEmailMethodPage", "emailPageVal", Status.FAIL,
						"Error Validations Failed");
			}

		}

	}
}