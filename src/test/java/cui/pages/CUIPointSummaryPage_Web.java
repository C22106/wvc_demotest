package cui.pages;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPointSummaryPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIPointSummaryPage_Web.class);

	public CUIPointSummaryPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By availableHK = By.xpath("//span[contains(@class,'h4 use-year')]");
	protected By availablePoints = By.xpath("//span[contains(@class,'h3 use-year')]");
	protected By ownerFullName = By.xpath("//h4[contains(.,'WELCOME')]/parent::div/h2");
	protected By pointsSummaryNavigationLink = By
			.xpath("//nav[contains(@class,'side')]//span[@class='account-navigation__text' and contains(.,'Points')]");
	protected static HashMap<String, String> currentUseYear = new HashMap<String, String>();
	protected static HashMap<String, String> nextUseYear = new HashMap<String, String>();
	protected By pointsSummaryHeader = By.xpath("//h1[contains(.,'POINTS SUMMARY')]");
	protected By currentBalanceHeader = By.xpath("//h2[contains(.,'Current Balance')]");
	protected By useYearHeader = By.xpath("//h5[contains(@class,'current-use-year')]");
	protected By useYearDates = By.xpath("//h5[contains(@class,'use-year--dates')]");
	protected By useYearBar = By
			.xpath("//div[contains(@class,'current-use-year')]//span[contains(@class,'value points')]");
	protected By useYearUnlimitedPoints = By.xpath("//p[contains(.,'Use')]/preceding-sibling::h6");
	protected By useYearRemaining = By.xpath("//p[contains(.,'Available Use Year Points')]/preceding-sibling::div/h6");
	protected By useYearTotalPoints = By
			.xpath("//p[contains(.,'Use')]/preceding-sibling::div/span[contains(@class,'use-year')]");
	protected By currentUseYearHK = By
			.xpath("//p[text()='Available Housekeeping Credits']/preceding-sibling::div[1]/h6");
	protected By houseKeepingBar = By
			.xpath("//div[contains(@class,'current-use-year')]//span[contains(@class,'value credits')]");
	protected By houseKeepingHeader = By.xpath(
			"(//p[contains(.,'Use')]/following-sibling::div/p[contains(.,'Housekeeping')]| //p[contains(.,'Use')]/following-sibling::p[contains(.,'Housekeeping')])");
	protected By houseKeepingUnlimited = By.xpath("//p[contains(.,'Housekeeping')]/preceding-sibling::h6");
	protected By houseKeepingRemaining = By.xpath("//p[contains(.,'Use')]/following-sibling::div/h6");
	protected By houseKeepingTotal = By
			.xpath("//p[contains(.,'Use')]/following-sibling::div/span[contains(@class,'use-year')]");

	protected By reservationTransaction = By.xpath("//div[@class='reservation']//h5");
	protected By unlimitedReservation = By.xpath("//div[@class='reservation']//p[contains(.,'Unlimited')]");
	protected By reservationNumbers = By.xpath(
			"//div[@class='reservation']//*[local-name() = 'svg' and @id='meter']//*[local-name()='tspan' and contains(@class,'use-year')]");
	protected By reservationProgressBar = By
			.xpath("//div[@class='reservation']//*[local-name() = 'svg' and @id='meter']//*[local-name()='g']");
	protected By reservationToolTip = By.xpath("//span[@id='reservation']");
	protected By toolTipReservationText = By.xpath("//span[contains(.,'Reservation Transaction')]/parent::div");

	protected By confirmationCredit = By.xpath("//div[@class='confirmation']//h5");
	protected By unlimitedConfirmation = By.xpath("//div[@class='confirmation']//p[contains(.,'Unlimited')]");
	protected By confirmationNumbers = By.xpath(
			"//div[@class='confirmation']//*[local-name() = 'svg' and @id='meter']//*[local-name()='tspan' and contains(@class,'use-year')]");
	protected By confirmationProgressBar = By
			.xpath("//div[@class='confirmation']//*[local-name() = 'svg' and @id='meter']//*[local-name()='g']");
	protected By confirmationToolTip = By.xpath("//span[@id='confirmation']");
	protected By toolTipConfirmationText = By.xpath("//b[contains(.,'Confirmation Credit')]/parent::div");

	protected By futureUseYear = By.xpath("// div[contains(@class,'accordion-item')]/a");
	protected By totalDate = By.xpath("// div[contains(@class,'accordion-content')]//h1[contains(@class,'dates')]");
	protected By unlimitedPoints = By
			.xpath("// div[contains(@class,'accordion-content')]//p[contains(.,'Use')]/preceding-sibling::h1");
	protected By allRemainingPoints = By.xpath(
			"// div[contains(@class,'accordion-content')]//div[contains(@class,'points')]//span[contains(@class,'h3')]");
	protected By totalPointsList = By
			.xpath("//span[contains(@class,'h3')]/following-sibling::span[contains(@class,'right')]");
	protected By availableUserYearText = By
			.xpath("// div[contains(@class,'accordion-content')]//p[contains(.,'Use Year')]");
	protected By allRemainingHousecredits = By.xpath(
			"// div[contains(@class,'accordion-content')]//div[contains(@class,'points')]//span[contains(@class,'h4')]");
	protected By totalHouseCreditsList = By
			.xpath("//span[contains(@class,'h4')]/following-sibling::span[contains(@class,'right')]");
	protected By unlimitedHouseCredits = By
			.xpath("// div[contains(@class,'accordion-content')]//p[contains(.,'Housekeeping')]/preceding-sibling::h1");
	protected By houseKeepingText = By
			.xpath("// div[contains(@class,'accordion-content')]//p[contains(.,'Housekeeping')]");

	protected By transactionHistoryHeader = By.xpath("// div[@class='transaction-history']//h2");
	protected By selectedYear = By.xpath("// div[@class='transaction-history__select']//label");
	protected By selectDropDown = By.xpath("// select[@id='transaction-history-select']");
	protected By selectDropDownOptions = By.xpath("// select[@id='transaction-history-select']//optgroup");
	protected By futureUseYearOption = By.xpath("//optgroup[contains(@label,'Future')]");
	protected By pastUseYearOption = By.xpath("//optgroup[contains(@label,'Past')]");
	protected By selectedBox = By.xpath("// div[@class='transaction-history__select']");
	protected By tableDateHeader = By.xpath("// table[@class='transaction-history__table']//th[@class='dateHeading']");
	protected By tableTypeHeader = By.xpath("// table[@class='transaction-history__table']//th[@class='typeHeading']");
	protected By tablePointsHeader = By
			.xpath("// table[@class='transaction-history__table']//th[@class='pointsHeading']");
	protected By selectTypeFilter = By.xpath("//select[@class='dropdown-select']");
	protected By totalTableRows = By.xpath("// tr[contains(@class,'table-body-row')]");
	protected By totalTableDates = By.xpath("// tr[contains(@class,'table-body-row')]//td[@class='date']");
	protected By totalTableType = By.xpath("// tr[contains(@class,'table-body-row')]//td[@class='type']");
	protected By totalTablePoints = By.xpath("// tr[contains(@class,'table-body-row')]//td[contains(@class,'points')]");
	protected By totalPointsText = By.xpath("// tfoot//td/p[contains(.,'Total')]");
	protected By totalPointsValue = By.xpath("// tfoot//td[3]/p");
	protected By expanderList = By.xpath("// tr[contains(@class,'table-body-row')]//td[@class='expander']/button");

	protected By pointsEducationHeader = By.xpath("// h2[contains(.,'Points Education')]");
	protected By educationHeader = By.xpath("// div[@id='points-education']//h6");
	protected By educationAnswers = By.xpath("// div[@class='contentSlice']//div/p");
	protected By learnMoreLink = By.xpath("// div[@id='points-education']//div/a");
	protected By pointsLinkNavigation = By.xpath("//div[@class='contentSlice']//div[contains(@class,'title')]");

	protected By pendingPointsText = By.xpath("//div[contains(@class,'pending-points')]//p");
	protected By noTransactionIcon = By.xpath("//div[contains(@class,'no-transaction')]//*[local-name()='svg']");
	protected By noTransactionHeader = By.xpath("//div[contains(@class,'no-transaction')]//h4");
	protected By noTransactionText = By.xpath("//div[contains(@class,'no-transaction')]//p");

	protected By creditGuestValue = By.xpath(
			"//div[@class='confirmation']//*[local-name() = 'svg' and @id='meter']//*[local-name()='tspan' and contains(@class,'use-year')]");
	protected By linkNavigateToPointSummary = By.xpath("//span[text() = 'Points Summary']");
	protected By textPointSummary = By.xpath("//h1[text() = 'POINTS SUMMARY']");

	protected int preGuestCreditValue;
	protected int postGuestCreditValue;
	protected int preUserPointValue;
	protected int preUserHKValue;
	protected int postUserPointValue;
	protected int preFutureUseYearPointValue;
	protected int preFutureUseYearHKValue;
	protected int postFutureUseYearPointValue;
	protected int postFutureUseYearHKValue;
	protected int preCurrentUseYearHK;
	protected int preFutureHKValue;
	protected By reservationTypeCanceled = By
			.xpath("//td[@class='type' and contains(.,'Cancel')]/following-sibling::td[@class='expander']/button");
	protected By canceledReservationNumber = By.xpath("//p[@class='additional-info' and contains(.,'Confirmation')]");
	protected By resortName = By.xpath("//h1/a");

	/*
	 * Method: navigateToPointsSummaryPage Description:Navigate to Points
	 * Summary Page Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void navigateToPointsSummaryPage() {
		waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		if (verifyObjectDisplayed(pointsSummaryNavigationLink)) {
			clickElementBy(pointsSummaryNavigationLink);
			waitUntilElementVisibleBy(driver, pointsSummaryHeader, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(pointsSummaryHeader),
					"Points Summary Page header is not displayed");
			tcConfig.updateTestReporter("CUIPointSummaryPage", "navigateToPointsSummaryPage", Status.PASS,
					"User Navigated to Points Summary page");

		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "navigateToPointsSummaryPage", Status.FAIL,
					"User failed to Navigated to Points Summary page");
		}
	}

	/*
	 * Method: preUseYearPointsValue Description:pre Use Year Points Value
	 * displayed Val Date:July/2020 Author: Abhijeet Roy Changes By:NA
	 */
	public void preUseYearPointsValue() {
		waitUntilElementVisibleBy(driver, useYearRemaining, "ExplicitLongWait");
		if (verifyObjectDisplayed(useYearRemaining)) {
			preUserPointValue = Integer.parseInt(getObject(useYearRemaining).getText().trim().replace(",", ""));
			tcConfig.updateTestReporter("CUIPointSummaryPage", "preUseYearPointsValue", Status.PASS,
					"Pre User Point: " + preUserPointValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "preUseYearPointsValue", Status.FAIL,
					"User Points not Available");
		}

	}

	/*
	 * Method: preUseYearPointsValue Description:pre Use Year Points Value
	 * displayed Val Date:July/2020 Author: Abhijeet Roy Changes By:NA
	 */
	public void preUseYearHKValue() {
		waitUntilElementVisibleBy(driver, currentUseYearHK, "ExplicitLongWait");
		if (verifyObjectDisplayed(currentUseYearHK)) {
			preUserHKValue = Integer.parseInt(getObject(currentUseYearHK).getText().trim().replace(",", ""));
			tcConfig.updateTestReporter("CUIPointSummaryPage", "preUseYearHKValue", Status.PASS,
					"Pre User Point: " + preUserHKValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "preUseYearHKValue", Status.FAIL,
					"User Points not Available");
		}

	}

	/*
	 * Method: navigateToPointSummary Description:navigate To Point Summary
	 * displayed Val Date:July/2020 Author: Abhijeet Roy Changes By:NA
	 */
	public void navigateToPointSummary() {
		waitUntilElementVisibleBy(driver, linkNavigateToPointSummary, "ExplicitLongWait");
		clickElementJSWithWait(linkNavigateToPointSummary);
		waitUntilElementVisibleBy(driver, textPointSummary, "ExplicitLongWait");
		if (verifyObjectDisplayed(textPointSummary)) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "navigateToPointSummary", Status.PASS,
					"Successfully Navigated to Upcoming Vacations Page");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "navigateToPointSummary", Status.FAIL,
					"Failed to Navigated to Upcoming Vacations Page");
		}
	}

	/*
	 * Method: preGuestConfirmationValue Description:pre Guest Confirmation
	 * Value displayed Val Date:July/2020 Author: Abhijeet Roy Changes By:NA
	 */
	public void preGuestConfirmationValue() {
		waitUntilElementVisibleBy(driver, creditGuestValue, "ExplicitLongWait");
		if (verifyObjectDisplayed(creditGuestValue)) {
			preGuestCreditValue = Integer.parseInt(getList(creditGuestValue).get(0).getText().trim());
			tcConfig.updateTestReporter("CUIPointSummaryPage", "preGuestConfirmationValue", Status.PASS,
					"Pre Guest Credit Value is: " + preGuestCreditValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "navigateToPointSummary", Status.FAIL,
					"Guest Confirmation Credit value not present");
		}

	}

	/*
	 * Method: postGuestConfirmationValue Description:post Guest Confirmation
	 * Value displayed Val Date:July/2020 Author: Abhijeet Roy Changes By:NA
	 */
	public void postGuestConfirmationValue() {
		waitUntilElementVisibleBy(driver, creditGuestValue, "ExplicitLongWait");
		if (verifyObjectDisplayed(creditGuestValue)) {
			postGuestCreditValue = Integer.parseInt(getList(creditGuestValue).get(0).getText().trim());
			tcConfig.updateTestReporter("CUIPointSummaryPage", "postGuestConfirmationValue", Status.PASS,
					"Post Guest Credit Value is: " + postGuestCreditValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "navigateToPointSummary", Status.FAIL,
					"Guest Confirmation Credit value not present");
		}

	}

	/*
	 * Method: validateGuestCreditValueAfterModification Description:Validate
	 * Used Guest Value do not returned Summary Page Val Date: Jun/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void validateGuestCreditNotReturned() {
		if (preGuestCreditValue == postGuestCreditValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validateGuestCreditNotReturned", Status.PASS,
					"Guest Confirmation Credit value not returned after modify guest to owner");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validateGuestCreditNotReturned", Status.FAIL,
					"Pre Guest Credit Value is: " + preGuestCreditValue + " ,Post Guest Credit Value is: "
							+ postGuestCreditValue);
		}
	}

	/*
	 * Method: validateGuestCreditValueAfterModification Description:validate
	 * Guest Credit Value After Modification displayed Val Date:July/2020
	 * Author: Abhijeet Roy Changes By:NA
	 */
	public void validateGuestCreditValueAfterModification() {
		if (preGuestCreditValue > postGuestCreditValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validateGuestCreditValueAfterModification",
					Status.PASS, "Guest Confirmation Credit value decreased after modify owner to guest");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validateGuestCreditValueAfterModification",
					Status.FAIL, "Pre Guest Credit Value is: " + preGuestCreditValue
							+ " ,Post Guest Credit Value is: " + postGuestCreditValue);
		}
	}

	/*
	 * Method: pointSummaryPageSection Description:Points Summary Page section
	 * Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointSummaryPageSection() {
		elementPresenceVal(currentBalanceHeader, "Current Balance Header ", "CUIPointSummaryPage",
				"pointSummaryPageSection");
		elementPresenceVal(transactionHistoryHeader, "Transaction History Header ", "CUIPointSummaryPage",
				"pointSummaryPageSection");
		elementPresenceVal(pointsEducationHeader, "Points Education Header ", "CUIPointSummaryPage",
				"pointSummaryPageSection");
	}

	/*
	 * Method: pendingCurrentBalance Description:Pending Owner Current Balance
	 * section Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pendingCurrentBalance() {
		elementPresenceVal(pendingPointsText, "Current Balance Pending Owner Text ", "CUIPointSummaryPage",
				"pendingCurrentBalance");

	}

	/*
	 * Method: currentUseYearSection Description:Current Use Year Page section
	 * Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void currentUseYearSection() {
		elementPresenceVal(useYearHeader, "Current Use year Header ", "CUIPointSummaryPage", "currentUseYearSection");
		fieldPresenceVal(useYearDates, "Use Year Dates ", "CUIPointSummaryPage", "currentUseYearSection",
				" as " + getElementText(useYearDates));
		elementPresenceVal(useYearBar, "Use Year Progress Bar ", "CUIPointSummaryPage", "currentUseYearSection");
	}

	/*
	 * Method: currentUseYearDateFormat Description:Current Use Year Page
	 * section Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */

	public void currentUseYearDateFormat() {
		// Split Date
		String[] dateSplit = getElementText(useYearDates).split("\\-");
		// Start Date
		String strStartDate = dateSplit[0].trim();
		// End Date
		String strEndDate = dateSplit[dateSplit.length - 1].trim();

		validDateFormat(strStartDate, "MM/dd/yyyy");
		validDateFormat(strEndDate, "MM/dd/yy");

	}

	/*
	 * Method: currentUseYearPoints Description:Current Use Year Points Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void currentUseYearPoints() {
		if (verifyObjectDisplayed(useYearUnlimitedPoints)) {
			elementPresenceVal(useYearUnlimitedPoints, "Unlimited Use Year Points ", "CUIPointSummaryPage",
					"currentUseYearPoints");
		} else {
			fieldPresenceVal(useYearRemaining, "Remaining Use year Points ", "CUIPointSummaryPage",
					"currentUseYearPoints", " and is " + getElementText(useYearRemaining));
			fieldPresenceVal(useYearTotalPoints, "Total Use year Points ", "CUIPointSummaryPage",
					"currentUseYearPoints", " and is " + getElementText(useYearTotalPoints));

		}

	}

	/*
	 * Method: houseKeepingCreditsSection Description:House Keeping page section
	 * Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void houseKeepingCreditsSection() {
		elementPresenceVal(houseKeepingHeader, "House Keeping Header ", "CUIPointSummaryPage",
				"houseKeepingCreditsSection");
		elementPresenceVal(houseKeepingBar, "Housekeeping Progress Bar ", "CUIPointSummaryPage",
				"houseKeepingCreditsSection");
	}

	/*
	 * Method: houseKeepingPoints Description:Current Housekeeping Points Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void houseKeepingPoints() {
		if (testData.get("UserStatus").toUpperCase().contains("NON VIP")) {
			fieldPresenceVal(houseKeepingRemaining, "Remaining Housekeeping Points ", "CUIPointSummaryPage",
					"houseKeepingPoints", " and is " + getElementText(houseKeepingRemaining));
			fieldPresenceVal(houseKeepingTotal, "Total Housekeeping Points ", "CUIPointSummaryPage",
					"houseKeepingPoints", " and is " + getElementText(houseKeepingTotal));
		} else {
			elementPresenceVal(houseKeepingUnlimited, "Unlimited Housekeeping Points ", "CUIPointSummaryPage",
					"houseKeepingPoints");

		}

	}

	/*
	 * Method: reservationTransactionHeader Description:Reservation Transaction
	 * Section Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */

	public void reservationTransactionHeader() {
		// get Current Year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		// convert to string
		String strCurrentYear = Integer.toString(currentYear);
		if (getElementText(reservationTransaction).contains(strCurrentYear)) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "reservationTransactionHeader", Status.PASS,
					"Reservation Transaction Header present with current year");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "reservationTransactionHeader", Status.FAIL,
					"Reservation Transaction Header not present with current year "
							+ getElementText(reservationTransaction));
		}

		elementPresenceVal(reservationProgressBar, "Reservation Progress Bar", "CUIPointSummaryPage",
				"reservationTransactionHeader");
		// toolTipValidations(reservationToolTip, "Reservation Transaction",
		// toolTipReservationText);
	}

	/*
	 * Method:reservationTransactionLeft Description:Reservation Transaction
	 * Left Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationTransactionLeft() {
		if (testData.get("UserStatus").toUpperCase().contains("NON VIP")) {
			fieldPresenceVal(getList(reservationNumbers).get(0), "Remaining Reservation Transaction ",
					"CUIPointSummaryPage", "reservationTransactionLeft",
					" and is " + getList(reservationNumbers).get(0).getText());
			fieldPresenceVal(getList(reservationNumbers).get(1), "Total Reservation Transaction ",
					"CUIPointSummaryPage", "reservationTransactionLeft",
					" and is " + getList(reservationNumbers).get(1).getText());
		} else {
			elementPresenceVal(unlimitedReservation, "Unlimited Reservation Transaction ", "CUIPointSummaryPage",
					"reservationTransactionLeft");

		}

	}

	/*
	 * Method: confirmationCreditHeader Description:Confirmation Credit Section
	 * Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */

	public void confirmationCreditHeader() {
		// get Current Year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		// convert to string
		String strCurrentYear = Integer.toString(currentYear);
		if (getElementText(confirmationCredit).contains(strCurrentYear)) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "confirmationCreditHeader", Status.PASS,
					"Confirmation Credit Header present with current year");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "confirmationCreditHeader", Status.FAIL,
					"Confirmation Credit Header not present with current year " + getElementText(confirmationCredit));
		}
		elementPresenceVal(confirmationProgressBar, " Confirmation Progress Bar", "CUIPointSummaryPage",
				"confirmationCreditHeader");
		// toolTipValidations(confirmationToolTip, "Confirmation Credit",
		// toolTipConfirmationText);
	}

	/*
	 * Method:confirmationCreditLeft Description:Confirmation Credit Left Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void confirmationCreditLeft() {
		if (verifyObjectDisplayed(unlimitedConfirmation)) {
			elementPresenceVal(unlimitedConfirmation, "Unlimited Confirmation Credit ", "CUIPointSummaryPage",
					"confirmationCreditLeft");
		} else {
			fieldPresenceVal(getList(confirmationNumbers).get(0), "Remaining Confirmation Credit ",
					"CUIPointSummaryPage", "confirmationCreditLeft",
					" and is " + getList(confirmationNumbers).get(0).getText());
			fieldPresenceVal(getList(confirmationNumbers).get(1), "Total Confirmation Credit ", "CUIPointSummaryPage",
					"confirmationCreditLeft", " and is " + getList(confirmationNumbers).get(1).getText());

		}

	}

	/*
	 * Method: futureUseYearAccordion Description:Future User Year Accordion
	 * section Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void futureUseYearAccordion(String strAccordion) {
		elementPresenceVal(futureUseYear, "Future Use Year Accordion ", "CUIPointSummaryPage",
				"futureUseYearAccordion");

		getElementInView(futureUseYear);
		clickElementBy(futureUseYear);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (getElementAttribute(futureUseYear, "aria-expanded").contains(strAccordion)) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "futureUseYearAccordion", Status.PASS,
					"Accordion Clicked");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "futureUseYearAccordion", Status.FAIL,
					"failed to click Accordion");
		}

	}

	/*
	 * Method: futureUseYearAccordionContent Description:Future User Year
	 * Accordion section Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void futureUseYearAccordionContent() {
		if (getList(totalDate).size() > 0) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "futureUseYearAccordion", Status.PASS,
					"Total " + getList(totalDate).size() + " future use year present in the accordion");
			elementListPresenceVal(availableUserYearText, getList(totalDate).size(), "Use Year Header ",
					"CUIPointSummaryPage", "futureUseYearAccordion");
			elementListPresenceVal(houseKeepingText, getList(totalDate).size(), "Housekeeping Header ",
					"CUIPointSummaryPage", "futureUseYearAccordion");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "futureUseYearAccordion", Status.FAIL,
					"No future use year present in the accordion");
		}
	}

	/*
	 * Method: futureUseYearAccordionContent Description:Future User Year
	 * Accordion section Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void futureUseYearDateFormat() throws Exception {
		// Use Year Date List form App
		List<WebElement> userYearDateList = getList(totalDate);
		// new list to save date in string
		List<String> startDateList = new ArrayList<String>();
		for (int dateCount = 0; dateCount < userYearDateList.size(); dateCount++) {
			String[] dateSplit = userYearDateList.get(dateCount).getText().split("\\-");
			// Start Date
			String strStartDate = dateSplit[0].trim();
			// End Date
			String strEndDate = dateSplit[dateSplit.length - 1].trim();

			validDateFormat(strStartDate, "MM/dd/yy");
			validDateFormat(strEndDate, "MM/dd/yy");

			startDateList.add(strStartDate);
		}

		ascendingOrderDateCheck(startDateList, "MM/dd/yy", "Future Use Year Dates");
	}

	/*
	 * Method: accordionUseYearPoints Description:Future User Year Accordion
	 * section Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */

	public void accordionUseYearPoints() {
		// Use Year Date List form App
		List<WebElement> userYearDateList = getList(totalDate);
		for (int dateCount = 0; dateCount < userYearDateList.size(); dateCount++) {
			if (verifyObjectDisplayed(unlimitedPoints)) {
				fieldPresenceVal(getList(unlimitedPoints).get(dateCount), "Unlimited Use year Points ",
						"CUIPointSummaryPage", "accordionUseYearPoints",
						" and is " + getElementText(getList(unlimitedPoints).get(dateCount)));
			} else {
				fieldPresenceVal(getList(allRemainingPoints).get(dateCount), "Remaining Use year Points ",
						"CUIPointSummaryPage", "accordionUseYearPoints",
						" and is " + getElementText(getList(allRemainingPoints).get(dateCount)));
				fieldPresenceVal(getList(totalPointsList).get(dateCount), "Total Use year Points ",
						"CUIPointSummaryPage", "accordionUseYearPoints",
						" and is " + getElementText(getList(totalPointsList).get(dateCount)));

			}

		}

	}

	/*
	 * Method: accordionHousekeepingPoints Description: Future User Year
	 * Accordion* section Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void accordionHousekeepingPoints() {
		// Use Year Date List form App
		List<WebElement> userYearDateList = getList(totalDate);
		for (int dateCount = 0; dateCount < userYearDateList.size(); dateCount++) {
			if (testData.get("UserStatus").toUpperCase().contains("NON VIP")) {
				fieldPresenceVal(getList(allRemainingHousecredits).get(dateCount), "Remaining Housekeeping Points ",
						"CUIPointSummaryPage", "accordionHousekeepingPoints",
						" and is " + getElementText(getList(allRemainingHousecredits).get(dateCount)));
				fieldPresenceVal(getList(totalHouseCreditsList).get(dateCount), "Total Housekeeping Points ",
						"CUIPointSummaryPage", "accordionHousekeepingPoints",
						" and is " + getElementText(getList(totalHouseCreditsList).get(dateCount)));
			} else {
				fieldPresenceVal(getList(unlimitedHouseCredits).get(dateCount), "Unlimited Housekeeping Points ",
						"CUIPointSummaryPage", "accordionHousekeepingPoints",
						" and is " + getElementText(getList(unlimitedHouseCredits).get(dateCount)));

			}

		}

	}

	/*
	 * Method: defaultTransactionHistory Description: Default Transaction
	 * History * section Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void defaultTransactionHistory() {
		if (verifyObjectDisplayed(selectDropDown)) {
			getElementInView(selectDropDown);
			dropdownOptions();
			if (getElementText(selectedYear).toLowerCase().contains("current")) {
				tcConfig.updateTestReporter("CUIPointSummaryPage", "defaultTransactionHistory", Status.PASS,
						"Default Value Selected as Current User Year");
			} else {
				tcConfig.updateTestReporter("CUIPointSummaryPage", "defaultTransactionHistory", Status.FAIL,
						"Default Value is not Selected as Current User Year");
			}

		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "defaultTransactionHistory", Status.FAIL,
					"User Year Drop Down not present");
		}
	}

	/*
	 * Method: dropdownOptions Description: Default Drop Down Options * section
	 * Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void dropdownOptions() {
		for (int selectOption = 0; selectOption < getList(selectDropDownOptions).size(); selectOption++) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "defaultTransactionHistory", Status.PASS,
					"Select Option present as: "
							+ getElementAttribute(getList(selectDropDownOptions).get(selectOption), "label"));
		}
	}

	/*
	 * Method: futureUseYearSelectOption Description: Future Use Year Option
	 * Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void futureUseYearSelectOption() {
		if (verifyObjectDisplayed(futureUseYearOption)) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "futureUseYearSelectOption", Status.FAIL,
					"Future User Year Transaction History Present for Pending Owner");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "futureUseYearSelectOption", Status.PASS,
					"Future User Year Transaction History not Present for Pending Owner");
		}
	}

	/*
	 * Method: pastUseYearSelectOption Description: past Use Year Option
	 */
	public void pastUseYearSelectOption() {
		if (verifyObjectDisplayed(pastUseYearOption)) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pastUseYearSelectOption", Status.FAIL,
					"Past User Year Transaction History Present for Pending Owner");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pastUseYearSelectOption", Status.PASS,
					"Past User Year Transaction History not Present for Pending Owner");
		}
	}

	/*
	 * Method: noTransactionDisplayed Description: No Transaction Displayed for
	 * pending owners
	 */
	public void noTransactionDisplayed() {
		if (getList(totalTableRows).size() == 0) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "noTransactionDisplayed", Status.PASS,
					"No Transactions Displayed");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "noTransactionDisplayed", Status.FAIL,
					"Transactions Displayed for Pending owner");
		}
	}

	/*
	 * Method: pendingTransactionDropDown Description: Default Transaction
	 * History for Pending owner * section Val Date:Jun/2020 Author: Unnat Jain
	 * Changes By:NA
	 */
	public void pendingTransactionDropDown() {
		getElementInView(transactionHistoryHeader);
		if (verifyObjectDisplayed(selectDropDown)) {

			if (getElementText(selectedYear).toLowerCase().contains("current")) {
				tcConfig.updateTestReporter("CUIPointSummaryPage", "defaultTransactionHistory", Status.PASS,
						"Default Value Selected as Current User Year");
			} else {
				tcConfig.updateTestReporter("CUIPointSummaryPage", "defaultTransactionHistory", Status.FAIL,
						"Default Value is not Selected as Current User Year");
			}
			futureUseYearSelectOption();
			pastUseYearSelectOption();

		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "defaultTransactionHistory", Status.PASS,
					"Transaction History not present for this owner");
		}
	}

	/*
	 * Method: selectUseYearTransaction Description: Select Use Year Value *
	 * section Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void selectUseYearTransaction(String strSelect) throws ParseException {

		if (strSelect.equalsIgnoreCase("NA")) {
			tcConfig.updateTestReporter("CUIPointsSummaryPage", "selectUseYearTransaction", Status.PASS,
					"Use Year Option Not Present");
		} else {
			if (getCurrentBrowserName().toUpperCase().contains("FIREFOX")
					|| getCurrentBrowserName().toUpperCase().contains("SAFARI")) {
				clickElementBy(selectedBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				WebElement select = driver.findElement(By.id("transaction-history-select"));
				((JavascriptExecutor) driver).executeScript(
						"var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }",
						select, strSelect);
			} else {
				getElementInView(selectDropDown);
				selectByText(selectDropDown, strSelect);
			}
			tcConfig.updateTestReporter("CUIPointsSummaryPage", "selectUseYearTransaction", Status.PASS,
					"Use Year Selected as " + getElementText(selectedYear));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifySelectedOptionApplied(strSelect);
		}
	}

	/*
	 * Method: verifySelectedOptionApplied Description: Verify yhat the selected
	 * use year option is applied Val Date:Jun/2020 Author: Unnat Jain Changes
	 * By:NA
	 */

	public void verifySelectedOptionApplied(String strSelect) throws ParseException {

		String[] dateSplit = strSelect.split("\\-");
		// Start Date
		String strStartDate = dateSplit[0].trim();
		// End Date
		String strEndDate = dateSplit[dateSplit.length - 1].trim();

		convertStringToDate(strEndDate, "MM/dd/yy");
		verifyDateRange(convertStringToDate(strStartDate, "MM/dd/yy"), convertStringToDate(strEndDate, "MM/dd/yy"));
	}

	/*
	 * Method: checkDateRange Description: Verify that the selected use year
	 * option is applied Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void verifyDateRange(Date startDate, Date endDate) throws ParseException {
		// Use Year Date List form App
		List<WebElement> getDates = getList(totalTableDates);
		int totdalDates = 0;
		@SuppressWarnings("unused")
		Boolean flag = false;
		if (getDates.size() > 5) {
			totdalDates = 5;
		}

		for (int compareDate = 0; compareDate < totdalDates; compareDate++) {
			Date dateConvert = convertStringToDate(getDates.get(compareDate).getText(), "MM/dd/yyyy");

			if (checkDateRange(startDate, endDate, dateConvert)) {
				flag = true;
			} else {
				flag = false;
				break;
			}
		}

		if (flag = true) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "verifyDateRange", Status.PASS,
					"Dates are displayed within selected date range");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "verifyDateRange", Status.FAIL,
					"Dates are not displayed within selected date range");
		}

	}

	/*
	 * Method: tableHeaderValidation Description: Verify that table headers are
	 * displayed Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void tableHeaderValidation() {
		elementPresenceVal(tableDateHeader, "Table Date Header ", "CUIPointSummaryPage", "tableHeaderValidation");
		elementPresenceVal(tableTypeHeader, "Table Type Header ", "CUIPointSummaryPage", "tableHeaderValidation");
		elementPresenceVal(tablePointsHeader, "Table Points Header ", "CUIPointSummaryPage", "tableHeaderValidation");
	}

	/*
	 * Method: expanderValidations Description: Expander present in all rows
	 * displayed Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void expanderValidations() {
		if (getList(totalTableRows).size() == getList(expanderList).size()) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "expanderValidations", Status.PASS,
					"Expander Present in all the rows");
			getElementInView(getList(expanderList).get(0));
			getList(expanderList).get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			expanderOpenValidations(0);

		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "expanderValidations", Status.FAIL,
					"Expander not Present in all the rows");
		}
	}

	/*
	 * Method: expanderValidations Description: Expander present in all rows
	 * displayed Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void expanderOpenValidations(int expandedRow) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (getElementAttribute(getList(totalTableRows).get(expandedRow), "class").contains("open")) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "expanderValidations", Status.PASS, "Row Expanded");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "expanderValidations", Status.FAIL,
					"Error in expanding row");
		}
	}

	/*
	 * Method: totalPointsValidation Description: Total Points Presence Val
	 * displayed Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void totalPointsValidation() {
		fieldPresenceVal(totalPointsText, "Total Points Row ", "CUIPointSummaryPage", "totalPointsValidation",
				"  as " + getElementText(totalPointsValue));
	}

	/*
	 * Method: dateSortValidation Description: Table Dates Sorting Val displayed
	 * Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void dateSortValidation() throws Exception {

		clickElementBy(tableDateHeader);
		// Date List
		List<String> tableDateList = new ArrayList<String>();
		for (int dateCount = 0; dateCount < getList(totalTableDates).size(); dateCount++) {
			String strRowDate = getList(totalTableDates).get(dateCount).getText();
			tableDateList.add(strRowDate);
		}

		ascendingOrderDateCheck(tableDateList, "MM/dd/yyyy", "Table Dates ");

	}

	/*
	 * Method: pointSortValidation Description: Table Point Sorting Val
	 * displayed Val Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void pointSortValidation() throws Exception {
		getElementInView(tablePointsHeader);

		clickElementBy(tablePointsHeader);
		// Point List
		List<Integer> tablePointList = new ArrayList<Integer>();
		for (int pointCount = 0; pointCount < getList(totalTablePoints).size(); pointCount++) {
			String strRowPoint = getList(totalTablePoints).get(pointCount).getText().trim();
			if (strRowPoint.contains(",")) {
				strRowPoint = strRowPoint.replace(",", "");
			}
			int rowPoint = Integer.parseInt(strRowPoint);
			tablePointList.add(rowPoint);
		}

		numberOrder(tablePointList, "Points", 6);

	}

	/*
	 * Method: tableTypeFilterValidation Description: Table Type Filter Val Val
	 * Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */
	public void tableTypeFilterValidation() throws Exception {

		selectByText(selectTypeFilter, testData.get("TypeFilter"));
		// Flag
		Boolean flag = false;
		for (int typeCount = 0; typeCount < getList(totalTableType).size(); typeCount++) {
			String strRowType = getList(totalTableType).get(typeCount).getText().trim();

			if (strRowType.contains(testData.get("TypeFilter"))) {
				flag = true;
			} else {
				flag = false;
				break;
			}

		}

		flagValidations(flag, "Type Filter", " filtered with " + testData.get("TypeFilter"));

	}

	/*
	 * Method: pointsEducationHeaders Description: Points Education Section val
	 * Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void pointsEducationHeaders() {
		getElementInView(pointsEducationHeader);
		if (getList(educationHeader).size() == 3) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationSection", Status.PASS,
					"3 Poinst Education Headers present in the page");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationSection", Status.FAIL,
					"3 Poinst Education Headers not present in the page");
		}
	}

	/*
	 * Method: pointsEducationAnswers Description: Points Education Section val
	 * Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void pointsEducationAnswers() {
		if (getList(educationAnswers).size() == 3) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationSection", Status.PASS,
					"3 Poinst Education Answers present in the page");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationSection", Status.FAIL,
					"3 Poinst Education Answers not present in the page");
		}
	}

	/*
	 * Method: pointsEducationLinks Description: Points Education Section val
	 * Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void pointsEducationLinks() {

		if (getList(learnMoreLink).size() == 3) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationSection", Status.PASS,
					"3 Poinst Education Links present in the page");
			pointsLinkNavigations();
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationSection", Status.FAIL,
					"3 Poinst Education Links not present in the page");
		}
	}

	/*
	 * Method: pointsLinkNavigations Description: Points Education Section val
	 * Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void pointsLinkNavigations() {
		for (int linksCount = 0; linksCount < getList(learnMoreLink).size(); linksCount++) {
			getElementInView(getList(learnMoreLink).get(linksCount));
			getList(learnMoreLink).get(linksCount).click();
			ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
			newTabNavigations(newWindow);
			waitUntilElementVisibleBy(driver, pointsLinkNavigation, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(pointsLinkNavigation), "Points Link navigation failed");

			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationLinks", Status.PASS,
					"Navigated to Education Link page");
			navigateToMainTab(newWindow);
			waitUntilElementVisibleBy(driver, pointsSummaryHeader, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(pointsSummaryHeader), "Points Summary Header not present");

		}
	}

	/*
	 * Method: cancelReservationExpander Description: Click On Cancel
	 * Reservation Expander Date:Jul/2020 Author: Unnat Jain Changes By:NA
	 */

	public void cancelReservationExpanderSelect() {
		if (getList(reservationTypeCanceled).size() > 0) {
			for (int totalCanceled = 0; totalCanceled < getList(reservationTypeCanceled).size(); totalCanceled++) {
				getElementInView(getList(reservationTypeCanceled).get(totalCanceled));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getList(reservationTypeCanceled).get(totalCanceled).click();
				waitUntilElementVisibleWb(driver, getList(canceledReservationNumber).get(totalCanceled),
						"ExplicitLongWait");
				if (getList(canceledReservationNumber).get(totalCanceled).getText()
						.contains(testData.get("ReservationNumber"))) {
					clickElementBy(By.xpath("// p[@class='additional-info' and contains(.,'"
							+ testData.get("ReservationNumber") + "')]/following-sibling::a"));
					waitUntilElementVisibleBy(driver, resortName, "ExplicitLongWait");
					Assert.assertTrue(verifyObjectDisplayed(resortName), "Navigated to Reservation Details Page");
					break;
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "cancelReservationExpander", Status.FAIL,
					"No Canceled Reservation Present in the table");
		}
	}

	/*
	 * Method: postUseYearPointsValue Description:post Use Year Points Value
	 * displayed Val Date:July/2020 Author: Abhijeet Roy Changes By:NA
	 */
	public void postUseYearPointsValue() {
		waitUntilElementVisibleBy(driver, useYearRemaining, "ExplicitLongWait");
		if (verifyObjectDisplayed(useYearRemaining)) {
			postUserPointValue = Integer.parseInt(getObject(useYearRemaining).getText().trim().replace(",", ""));
			tcConfig.updateTestReporter("CUIPointSummaryPage", "postUseYearPointsValue", Status.PASS,
					"Post User Point: " + postUserPointValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "postUseYearPointsValue", Status.FAIL,
					"User Points not Available");
		}
	}

	/*
	 * Method: validatePointsAfterDeposit Description:validate Points After
	 * Deposit displayed Val Date:July/2020 Author: Abhijeet Roy Changes By:NA
	 */
	public void validatePointsAfterDeposit() {
		int depositedValue = Integer.parseInt(testData.get("PointsToDeposit"));
		int calculatedPostValue = preUserPointValue - depositedValue;
		if (postUserPointValue == calculatedPostValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterDeposit", Status.PASS,
					"User Point Displayed is: " + postUserPointValue + " and Calculated Value is: "
							+ calculatedPostValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterDeposit", Status.FAIL,
					"User Point Displayed and Calculated Value did not matched");
		}
	}

	/*
	 * Method: PreFutureUseYearPointsValue Description:pre Use Year Points Value
	 * displayed Val Date:July/2020 Author: Kamalesh Gupta Changes By:NA
	 */
	public void preFutureUseYearPointsValue() {
		waitUntilElementVisibleBy(driver, useYearRemaining, "ExplicitLongWait");
		String useYear = testData.get("futureUseYear").trim();
		Assert.assertTrue(verifyObjectDisplayed(futureUseYear), "Future Use Year section not present");
		getElementInView(futureUseYear);
		clickElementBy(futureUseYear);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> userYearDateList = getList(totalDate);
		if (userYearDateList.size() > 0) {
			for (int dateCount = 0; dateCount < userYearDateList.size(); dateCount++) {
				List<String> useYearValue = getListString(totalDate);
				if (useYearValue.get(dateCount).trim().equals(useYear)) {
					preFutureUseYearPointValue = Integer
							.parseInt(getListString(availablePoints).get(dateCount).trim().replace(",", ""));
					tcConfig.updateTestReporter("CUIPointSummaryPage", "PreFutureUseYearPointsValue", Status.PASS,
							"Pre Future Use Year Point value for the year " + useYear + " is :"
									+ preFutureUseYearPointValue);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "PreFutureUseYearPointsValue", Status.FAIL,
					"Future User Year Point not Available");
		}
	}

	/*
	 * Method: PreFutureUseYearPointsValue Description:pre Use Year Points Value
	 * displayed Val Date:July/2020 Author: Kamalesh Gupta Changes By:NA
	 */
	public void preFutureUseYearHKValue() {
		waitUntilElementVisibleBy(driver, useYearRemaining, "ExplicitLongWait");
		String useYear = testData.get("futureUseYear").trim();
		Assert.assertTrue(verifyObjectDisplayed(futureUseYear), "Future Use Year section not present");
		getElementInView(futureUseYear);
		// clickElementBy(futureUseYear);
		List<WebElement> userYearDateList = getList(totalDate);
		if (userYearDateList.size() > 0) {
			for (int dateCount = 0; dateCount < userYearDateList.size(); dateCount++) {
				List<String> useYearValue = getListString(totalDate);
				if (useYearValue.get(dateCount).trim().equals(useYear)) {
					preFutureUseYearHKValue = Integer
							.parseInt(getListString(availableHK).get(dateCount).trim().replace(",", ""));
					tcConfig.updateTestReporter("CUIPointSummaryPage", "preFutureUseYearHKValue", Status.PASS,
							"Pre Future Use Year HK value for the year " + useYear + " is :" + preFutureUseYearHKValue);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "preFutureUseYearHKValue", Status.FAIL,
					"Future User Year HK not Available");
		}
	}

	/*
	 * Method: postFutureUseYearPointsValue Description:post Use Year Points
	 * Value displayed Val Date:July/2020 Author: Kamalesh Gupta Changes By:NA
	 */
	public void postFutureUseYearPointsValue() {
		waitUntilElementVisibleBy(driver, useYearRemaining, "ExplicitLongWait");
		String useYear = testData.get("futureUseYear").trim();
		Assert.assertTrue(verifyObjectDisplayed(futureUseYear), "Future Use Year section not present");
		getElementInView(futureUseYear);
		clickElementBy(futureUseYear);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> userYearDateList = getList(totalDate);
		if (userYearDateList.size() > 0) {
			for (int dateCount = 0; dateCount < userYearDateList.size(); dateCount++) {
				List<String> useYearValue = getListString(totalDate);
				if (useYearValue.get(dateCount).trim().equals(useYear)) {
					postFutureUseYearPointValue = Integer
							.parseInt(getListString(availablePoints).get(dateCount).trim().replace(",", ""));
					tcConfig.updateTestReporter("CUIPointSummaryPage", "postFutureUseYearPointsValue", Status.PASS,
							"Post Future User Year Point for the year " + useYear + " is :"
									+ postFutureUseYearPointValue);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "postFutureUseYearPointsValue", Status.FAIL,
					"Future User Year not Available");
		}
	}

	/*
	 * Method: postFutureUseYearPointsValue Description:post Use Year Points
	 * Value displayed Val Date:July/2020 Author: Kamalesh Gupta Changes By:NA
	 */
	public void postFutureUseYearHKValue() {
		waitUntilElementVisibleBy(driver, useYearRemaining, "ExplicitLongWait");
		String useYear = testData.get("futureUseYear").trim();
		Assert.assertTrue(verifyObjectDisplayed(futureUseYear), "Future Use Year section not present");
		getElementInView(futureUseYear);
		// clickElementBy(futureUseYear);
		List<WebElement> userYearDateList = getList(totalDate);
		if (userYearDateList.size() > 0) {
			for (int dateCount = 0; dateCount < userYearDateList.size(); dateCount++) {
				List<String> useYearValue = getListString(totalDate);
				if (useYearValue.get(dateCount).trim().equals(useYear)) {
					postFutureUseYearHKValue = Integer
							.parseInt(getListString(availableHK).get(dateCount).trim().replace(",", ""));
					tcConfig.updateTestReporter("CUIPointSummaryPage", "postFutureUseYearPointsValue", Status.PASS,
							"Post Future User Year HK for the year " + useYear + " is :" + postFutureUseYearHKValue);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "postFutureUseYearPointsValue", Status.FAIL,
					"Future Use Year HK not Available");
		}
	}

	/*
	 * Method: validatePointsAfterDepositForFutureUseYear Description:validate
	 * Points After Deposit in case of Future Use Year displayed Val
	 * Date:July/2020 Author: Kamalesh Changes By:NA
	 */

	public void validatePointsAfterDepositForFutureUseYear() {
		int depositedValue = Integer.parseInt(testData.get("PointsToDeposit"));
		int calculatedDepositeValue = postFutureUseYearPointValue - preFutureUseYearPointValue;
		if (depositedValue == calculatedDepositeValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterDepositForFutureUseYear",
					Status.PASS, "For Future User Point Displayed is: " + postFutureUseYearPointValue
							+ " and Calculated Deposite value is: " + calculatedDepositeValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterDepositForFutureUseYear",
					Status.FAIL, "User Point Displayed and Calculated Value did not matched");
		}

	}

	/*
	 * Method: validatePointsAfterDepositForFutureUseYear Description:validate
	 * Points After Deposit in case of Future Use Year displayed Val
	 * Date:July/2020 Author: Kamalesh Changes By:NA
	 */

	public void validatePointsAfterTransactionForFutureUseYear() {
		int transactionPoint = Integer.parseInt(CUIModifySuccessPage_Web.strModifyingCost.trim().replace(",", ""));
		int calculatedValue = preFutureUseYearPointValue - postFutureUseYearPointValue;
		if (transactionPoint == calculatedValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterTransactionForFutureUseYear",
					Status.PASS,
					"Post Transaction Future Use Year Point displayed is: " + postFutureUseYearPointValue + " and "
							+ calculatedValue + " points are deducted from the future use year.");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterTransactionForFutureUseYear",
					Status.FAIL, "User Point Displayed and Calculated Value did not matched");
		}

	}

	/*
	 * Method: validatePointsAfterDepositForFutureUseYear Description:validate
	 * Points After Deposit in case of Future Use Year displayed Val
	 * Date:July/2020 Author: Kamalesh Changes By:NA
	 */

	public void validatePointsAfterCancellationForFutureUseYear() {

		if (preFutureUseYearPointValue == postFutureUseYearPointValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterCancellationForFutureUseYear",
					Status.PASS, "Post Cancellation Future Use Year Point refunded" + postFutureUseYearPointValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterCancellationForFutureUseYear",
					Status.FAIL, "Post Cancellation Future Use Year Point not refunded");
		}

		if (preFutureUseYearHKValue == postFutureUseYearHKValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterCancellationForFutureUseYear",
					Status.PASS, "Post Cancellation Future Use Year HK refunded" + postFutureUseYearHKValue);
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterCancellationForFutureUseYear",
					Status.FAIL, "Post Cancellation Future Use Year HK not refunded");
		}
	}

	/*
	 * Method: validatePointsAfterDepositForFutureUseYear Description:validate
	 * Points After Deposit in case of Future Use Year displayed Val
	 * Date:July/2020 Author: Kamalesh Changes By:NA
	 */

	public void validatePointsAfterTransactionForCurrentUseYear() {
		int transactionPoint = Integer.parseInt(CUIModifySuccessPage_Web.strModifyingCost.trim().replace(",", ""));
		int calculatedValue = preUserPointValue - postUserPointValue;
		if (transactionPoint == calculatedValue) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterTransactionForFutureUseYear",
					Status.PASS, "Post Transaction Current Use Year Point displayed is: " + postUserPointValue
							+ " and " + calculatedValue + " points are deducted from the current use year.");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "validatePointsAfterTransactionForFutureUseYear",
					Status.FAIL, "User Point Displayed and Calculated Value did not matched");
		}

	}

	/*
	 * Method: validatePointsAfterDepositForFutureUseYear Description:validate
	 * Points Get Current use Year Value Date:August/2020 Author: Abhijeet Roy
	 * Changes By:NA
	 */
	public void getPreCurrentYearHouskeepingValue() {
		if (verifyObjectDisplayed(houseKeepingRemaining)) {
			preCurrentUseYearHK = Integer.parseInt(getElementText(houseKeepingRemaining).trim());
			tcConfig.updateTestReporter("CUIPointSummaryPage", "getPreCurrentYearHouskeepingValue", Status.PASS,
					"Pre Current User Year HouseKeeping value is: " + preCurrentUseYearHK);

		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "getPreCurrentYearHouskeepingValue", Status.FAIL,
					"Current User Year HouseKeeping value not displayed");
		}
	}

	/*
	 * Method: getFutureYearHouskeepingValue Description:validate Points Get
	 * Future Use Year HouseKeeping value Date:August/2020 Author: Abhijeet Roy
	 * Changes By:NA
	 */
	public int getFutureYearHouskeepingValue(String UseYear) {
		int futureUserYearHK = 0;
		if (verifyObjectDisplayed(getObject(By.xpath("//div[contains(@class,'accordion-content')]//h1[text() = '"
				+ UseYear + "']/..//div[contains(@class,'points')]//span[contains(@class,'h4')]")))) {
			futureUserYearHK = Integer.parseInt(
					getElementText(getObject(By.xpath("//div[contains(@class,'accordion-content')]//h1[text() = '"
							+ UseYear + "']/..//div[contains(@class,'points')]//span[contains(@class,'h4')]"))).trim());

		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "getPreCurrentYearHouskeepingValue", Status.FAIL,
					"Houskeeping Value not displayed for Use Year: " + UseYear);
		}
		return futureUserYearHK;
	}

	/*
	 * Method: getFutureYearHouskeepingValue Description:validate Points Get
	 * Future Use Year HouseKeeping value Date:August/2020 Author: Abhijeet Roy
	 * Changes By:NA
	 */
	public void getPreFutureYearHouskeepingValue(String UseYear) {
		preFutureHKValue = getFutureYearHouskeepingValue(UseYear);
		tcConfig.updateTestReporter("CUIPointSummaryPage", "getPreFutureYearHouskeepingValue", Status.PASS,
				"Houskeeping Value for Use Year: " + UseYear + " is: " + preFutureHKValue);
	}

	/*
	 * Method: getPostCurrentYearHouskeepingValueAndValidate
	 * Description:Validate Deducted HK Date:August/2020 Author: Abhijeet Roy
	 * Changes By:NA
	 */
	public void getPostCurrentYearHouskeepingValueAndValidate() {
		if (verifyObjectDisplayed(houseKeepingRemaining)) {
			int postCurrentUserYearHK = Integer.parseInt(getElementText(houseKeepingRemaining).trim());
			if (postCurrentUserYearHK == (preCurrentUseYearHK - CUIReservationBalancePage_Web.availableHKCurrentYear)) {
				tcConfig.updateTestReporter("CUIPointSummaryPage", "getPostCurrentYearHouskeepingValueAndValidate",
						Status.PASS, "Housekeeping successfully validated after modification for current use year");
			} else {
				tcConfig.updateTestReporter("CUIPointSummaryPage", "getPostCurrentYearHouskeepingValueAndValidate",
						Status.FAIL, "Housekeeping failed to  validate after modification for current use year");
			}

		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "getPreCurrentYearHouskeepingValue", Status.FAIL,
					"Current User Year HouseKeeping value not displayed");
		}
	}

	/*
	 * Method: getFutureYearHouskeepingValue Description:validate Points Get
	 * Future Use Year HouseKeeping value Date:August/2020 Author: Abhijeet Roy
	 * Changes By:NA
	 */
	public void getPostFutureYearHouskeepingValueAndValidate(String UseYear) {
		int postFutureHKValue = getFutureYearHouskeepingValue(UseYear);
		if (postFutureHKValue == (preFutureHKValue
				- Integer.parseInt(testData.get("HouseKeepingBorrowPoints").trim()))) {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "getPostCurrentYearHouskeepingValueAndValidate",
					Status.PASS, "Housekeeping successfully validated after modification for Future use year");
		} else {
			tcConfig.updateTestReporter("CUIPointSummaryPage", "getPostCurrentYearHouskeepingValueAndValidate",
					Status.FAIL, "Housekeeping failed to  validate after modification for Future use year");
		}
	}

	/*
	 * Method: fetchCurrentYearPoints Description: Fetch the Current Use Year
	 * Date: July/2020 Author: Kamalesh Changes By:
	 */

	public void fetchCurrentYearPoints(String iterator) {
		waitUntilElementVisibleBy(driver, useYearRemaining, "ExplicitLongWait");
		String getUserYear = testData.get("CurrentUseYear");
		currentUseYear.put(iterator,
				getElementText(By
						.xpath("//th[text()='" + getUserYear + "']/following-sibling::td[contains(@class,'ptsAvail')]"))
								.trim());
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "compareCurrentYearPoints", Status.PASS,
				"Current Use Year Points are displayed as: " + getElementText(By
						.xpath("//th[text()='" + getUserYear + "']/following-sibling::td[contains(@class,'ptsAvail')]"))
								.trim());
	}
}
