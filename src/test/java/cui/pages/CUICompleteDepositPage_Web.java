package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICompleteDepositPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUICompleteDepositPage_Web.class);

	public CUICompleteDepositPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected CUIBookPage_Web bookPageObject = new CUIBookPage_Web(tcConfig);
	protected By fieldEmail = By.xpath("//input[@id = 'deposit-email']");
	protected By feePointDeposit = By.xpath("//span[text() = 'Points Deposit Fee']/../../../../td/strong");
	protected By textReviewDeposit = By.xpath("//h5[text() = 'Review Deposit']");
	protected By listChargesForSection = By.xpath("//h2[text() = 'Payment Charges']/..//tr");
	protected By textPaymentMethod = By.xpath("//h2[text() = 'Payment Method']");
	protected By sectionPaymentmethod = By.xpath("//section[contains(@class,'payment-method')]");
	protected By selectCreditCard = By.xpath("//label[@for = 'credit-card']");
	protected By selectPaypal = By.xpath("//label[@for = 'paypal']");
	protected By fieldCreditCardFirstName = By.xpath("//input[@id = 'firstName']");
	protected By fieldCreditCardLastName = By.xpath("//input[@id = 'lastName']");
	protected By fieldCreditCardNumber = By.xpath("//input[@id = 'creditCardNumber']");
	protected By fieldCreditCardMonth = By.xpath("//select[@id = 'expiration-month']");
	protected By fieldCreditCardYear = By.xpath("//select[@id = 'expiration-year']");
	protected By fieldZipCode = By.xpath("//input[@id = 'zipCode']");
	//-----------------------
	protected By totalPaymentDue = By.xpath("//th[text() = 'Total Payment Due']/..//strong");
	protected By textCompleteDeposit = By.xpath("//div[text() = 'Complete Deposit']");
	protected By textPaymentCharges = By.xpath("//h2[text() = 'Payment Charges']");
	protected By loadingSpinner = By.xpath("//*[local-name()='svg' and @class='spinner -secondary undefined']");
	protected By sectionDepositDetails = By.xpath("//div[contains(@class,'deposit-details')]");
	protected By headerDepositDetails = By.xpath("//h2[text() = 'Deposit Details']");
	protected By totalPointArea = By.xpath("//span[contains(text(),'Total Points')]/..");
	protected By valueTotalPoints = By.xpath("//span[contains(text(),'Total Points')]/../span");
	protected By valueHousekeeping = By.xpath("//span[text() = 'Housekeeping Credits']/../../span");
	protected By reservationTransactionValue = By.xpath("//span[text() = 'Reservation Transaction']/../../../span");
	protected By houskeepingArea = By.xpath("//span[text() = 'Housekeeping Credits']/../..");
	protected By reservationTransactionArea = By.xpath("//span[text() = 'Reservation Transaction']/../../..");
	protected By totalDepositBreakdown = By.xpath("//span[text() = 'Total Points']/../..//span[contains(text(),'Use Year')]/..");
	protected By totalPointsBreakdownLabel = By.xpath("//span[text() = 'Total Points']/../..//span[contains(text(),'Use Year')]");
	protected By totalPointsBreakdownValue = By.xpath("//span[text() = 'Total Points']/../..//span[contains(text(),'Use Year')]/following-sibling::span");
	protected By expandPointBreakdown = By.xpath("//span[text() = 'Total Points']/../button");
	protected By totalhousekeepingBreakdown = By.xpath("//span[text() = 'Housekeeping Credits']/../../..//span[contains(text(),'Use Year') or contains(text(),'Purchased')]/..");
	protected By housekeepingBreakdownLabel = By.xpath("//span[text() = 'Housekeeping Credits']/../../..//span[contains(text(),'Use Year') or contains(text(),'Purchased')]");
	protected By housekeepingBreakdownValue = By.xpath("//span[text() = 'Housekeeping Credits']/../../..//span[contains(text(),'Use Year') or contains(text(),'Purchased')]/following-sibling::span");
	protected By expandHousekeepingBreakdown = By.xpath("//span[text() = 'Housekeeping Credits']/../../button");
	protected By headerEmail = By.xpath("//h3[text() = 'Email Confirmation']");
	protected By textEmailinformation = By.xpath("//span[contains(.,'confirmation purposes')]");
	protected By buttonConfirmDeposit = By.xpath("//button[text() = 'Confirm Deposit']");
	protected By useyearToDeposit = By.xpath("(//span[text() = 'Use Year']/..//span)[2]");
	
	/*
	 * Method: headerCompleteDeposit Description:Header Complete Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void headerCompleteDeposit(){
		//waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
		waitUntilElementVisibleBy(driver, textCompleteDeposit, "ExplicitLongWait");
		elementPresenceVal(textCompleteDeposit, "Complete Deposit Header", "CUICompleteDepositPage", "headerCompleteDeposit");
	}
	
	/*
	 * Method: verifyHeaderReviewDeposit Description:verify Header Review
	 * Deposit payment Date field Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHeaderReviewDeposit() {
		elementPresenceVal(textReviewDeposit, "Review Deposit", "CUICompleteDepositPage", "verifyHeaderReviewDeposit");

	}

	/*
	 * Method: verifySectionDepositDetails Description:verify Section Deposit Details
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifySectionDepositDetails(){
		waitUntilElementVisibleBy(driver, sectionDepositDetails, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(sectionDepositDetails), "Deposit Detail Section not present");
		tcConfig.updateTestReporter("CUICompleteDepositPage", "verifySectionDepositDetails", Status.PASS,
				"Deposit Details Section Present");
		if (verifyObjectDisplayed(headerDepositDetails)) {
			tcConfig.updateTestReporter("CUICompleteDepositPage", "verifySectionDepositDetails", Status.PASS,
					"Text Deposit details Present");
		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "verifySectionDepositDetails", Status.FAIL,
					"Text Deposit details Not Present");
		}
	}
	/*
	 * Method: verifyTotalPoints Description:verify Total Points
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyTotalPoints(){
		if (verifyObjectDisplayed(totalPointArea)) {
			getElementInView(totalPointArea);
			String totalValueDepoisted = getList(valueTotalPoints).get(1).getText();
			if (totalValueDepoisted.isEmpty() || totalValueDepoisted == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyTotalPoints", Status.FAIL,
						"Total Points Deposited not displayed");
			}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyTotalPoints", Status.PASS,
						"Total Points Deposited: "+totalValueDepoisted);	
			}
				
		}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyTotalPoints", Status.FAIL,
						"Total Point Area not displayed");
			}
		}
	/*
	 * Method: validateTotalPointsBreakdown Description:validate Total Points Breakdown
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTotalPointsBreakdown(){
		clickElementJSWithWait(expandPointBreakdown);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int totalPointBreakdown = 0; totalPointBreakdown < getList(totalDepositBreakdown).size(); totalPointBreakdown++) {
			String breakdownLabel = getList(totalPointsBreakdownLabel).get(totalPointBreakdown).getText();
			String breakdownValue = getList(totalPointsBreakdownValue).get(totalPointBreakdown).getText();
			if (breakdownLabel.isEmpty() || breakdownLabel == null || breakdownValue.isEmpty() || breakdownValue == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "validateTotalPointsBreakdown", Status.FAIL,
						"Breakdown Deposit Point either label aur value not displayed");
			}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "validateTotalPointsBreakdown", Status.PASS,
						"For "+breakdownLabel+" value deposited is: "+breakdownValue);
			}
		}
	}
	/*
	 * Method: verifyHousekeepingCredit Description:verify Housekeeping Credit
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHousekeepingCredit(){
		if (verifyObjectDisplayed(houskeepingArea)) {
			String housekeepingcredit = getList(valueHousekeeping).get(2).getText();
			if (housekeepingcredit.isEmpty() || housekeepingcredit == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyHousekeepingCredit", Status.FAIL,
						"Housekeeping Deposited not displayed");
			}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyHousekeepingCredit", Status.PASS,
						"Housekeeping Deposited: "+housekeepingcredit);	
			}
				
		}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyHousekeepingCredit", Status.FAIL,
						"Housekeeping Area not displayed");
			}
		}
	
	/*
	 * Method: verifyHousekeepingCreditNotPresent Description:verify Housekeeping Credit Not Present
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHousekeepingCreditNotPresent(){
		if (!verifyObjectDisplayed(houskeepingArea)) {
			tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyTotalPoints", Status.PASS,
					"Housekeeping Area not displayed");
		}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyTotalPoints", Status.FAIL,
						"Housekeeping Area displayed");
			}
		}
	/*
	 * Method: validateHousekeepingcreditBreakdown Description:validate Housekeeping credit Breakdown
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHousekeepingcreditBreakdown(){
		clickElementJSWithWait(expandHousekeepingBreakdown);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int housekeepingBreakdown = 0; housekeepingBreakdown < getList(totalhousekeepingBreakdown).size(); housekeepingBreakdown++) {
			String breakdownLabel = getList(housekeepingBreakdownLabel).get(housekeepingBreakdown).getText();
			String breakdownValue = getList(housekeepingBreakdownValue).get(housekeepingBreakdown).getText();
			if (breakdownLabel.isEmpty() || breakdownLabel == null || breakdownValue.isEmpty() || breakdownValue == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "validateHousekeepingcreditBreakdown", Status.FAIL,
						"Breakdown Deposit Point either label aur value not displayed");
			}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "validateHousekeepingcreditBreakdown", Status.PASS,
						"For "+breakdownLabel+" value deposited is: "+breakdownValue);
			}
		}
	}
	/*
	 * Method: verifyReservationTransaction Description:verify Reservation Transaction
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */	
	public void verifyReservationTransaction(){
		if (verifyObjectDisplayed(reservationTransactionArea)) {
			String reservationTransactionvalue = getList(reservationTransactionValue).get(1).getText();
			if (reservationTransactionvalue.isEmpty() || reservationTransactionvalue == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyReservationTransaction", Status.FAIL,
						"Total Points Deposited not displayed");
			}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyReservationTransaction", Status.PASS,
						"Total Points Deposited: "+reservationTransactionvalue);	
			}
				
		}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyReservationTransaction", Status.FAIL,
						"Total Point Area not displayed");
			}
		}
	/*
	 * Method: verifyHeaderPaymentCharges Description:verify Header Payment Charges
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeaderPaymentCharges(){
		elementPresenceVal(textPaymentCharges, "Payment Charges Header", "CUICompleteDepositPage", "verifyHeaderPaymentCharges");	
	}
	
	
	/*
	 * Method: verifyPayemetForRCIDeposit Description:verify Payment For RCI Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyPaymentForRCIDeposit(){
		if (getList(listChargesForSection).size() > 0) {
			for (int iteratorCharges = 1; iteratorCharges <= getList(listChargesForSection).size(); iteratorCharges++) {
				String chargesFor = getElementText(getObject(By.xpath("(//h2[text() = 'Payment Charges']/..//span)["+iteratorCharges+"]")));
				String charges = getElementText(getObject(By.xpath("(//h2[text() = 'Payment Charges']/..//strong)["+iteratorCharges+"]")));
				if (chargesFor.isEmpty() || chargesFor == null || charges.isEmpty() || charges == null) {
					tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyPayemetForRCIDeposit",
							Status.FAIL, "Charges for or Charges may not be displayed in the List");
				} else {
					tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyPayemetForRCIDeposit",
							Status.PASS, "Charges :  " + charges + " is Paid For : " + chargesFor);
				}
			}
		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyPayemetForRCIDeposit",
					Status.FAIL, "Charges Section not displayed");
		}
	}
	
	/*
	 * Method: totalChargesDueVerfication Description:total Charges Due Verification
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void totalChargesDueVerification(){
		if (verifyObjectDisplayed(totalPaymentDue)) {
			tcConfig.updateTestReporter("CUICompleteDepositPage", "totalChargesDueVerfication",
					Status.PASS, "Total Payment Due is: "+getElementText(totalPaymentDue));
		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "totalChargesDueVerfication",
					Status.FAIL, "Total Payment Due is not displayed");
		}
	}
	
	/*
	 * Method: selectPaymentMethod Description:Select payment Option Date field
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectPaymentMethod() {
		waitUntilElementVisibleBy(driver, textPaymentMethod, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(sectionPaymentmethod), "Payment method section not present");
		if (verifyObjectDisplayed(sectionPaymentmethod)) {
			String paymentBy = testData.get("PaymentBy");
			if (paymentBy.equalsIgnoreCase("CreditCard")) {
				clickElementJSWithWait(selectCreditCard);
				tcConfig.updateTestReporter("CUICompleteDepositPage", "selectPaymentMethod", Status.PASS,
						"Payment Method is selected as: " + paymentBy);
			} else if (paymentBy.equalsIgnoreCase("Paypal")) {
				clickElementJSWithWait(selectPaypal);
				tcConfig.updateTestReporter("CUICompleteDepositPage", "selectPaymentMethod", Status.PASS,
						"Payment Method is selected as: " + paymentBy);
			} else {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "selectPaymentMethod", Status.FAIL,
						"Provide which payment method to use");
			}

		} else {
			tcConfig.updateTestReporter("CUICompleteDepositPage", "selectPaymentMethod", Status.FAIL,
					"Payment section not visible");
		}
	}
	
	/*
	 * Method: dataEntryForCreditCard Description:Fill Details for Credit Card
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryForCreditCard() {
		bookPageObject.dataEntryForCreditCard();

	}
	/*
	 * Method: dataEntryEmail Description:Fill Details for Email
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryEmail(){
		String emailID = testData.get("Email");
		if (verifyObjectDisplayed(fieldEmail)) {
			elementPresenceVal(headerEmail, "Email Header", "CUICompleteDepositPage", "dataEntryEmail");
			elementPresenceVal(textEmailinformation, "Email Information", "CUICompleteDepositPage", "dataEntryEmail");	
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = cap.getBrowserName().toUpperCase();
			if (browserName.contains("EDGE")|| browserName.contains("SAFARI")) {
				getObject(fieldEmail).sendKeys(emailID + Keys.TAB);
			}else{
				fieldDataEnter(fieldEmail, emailID);
				sendKeyboardKeys(Keys.TAB);
			}		
			

		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "dataEntryEmail", Status.FAIL,
					"Fields for Email data Entry not displayed");
		}
	}
	/*
	 * Method: clickConfirmDeposit Description:click Confirm Deposit
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickConfirmDeposit(){
		scrollUpByPixel(100);
		if (verifyObjectDisplayed(buttonConfirmDeposit)) {
			getElementInView(buttonConfirmDeposit);
			clickElementJSWithWait(buttonConfirmDeposit);
			tcConfig.updateTestReporter("CUICompleteDepositPage", "clickConfirmDeposit", Status.PASS,
					"Successfully Clicked On confirm deposit Button");
			waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "clickConfirmDeposit", Status.FAIL,
					"Failed to click on Confirm Deposit Button");
		}
	}
	
	/*
	 * Method: verifyUseYearToDeposit Description:verify UseYear To Deposit for
	 * Club Wyndham payment Date field Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyUseYearToDeposit() {
		if (verifyObjectDisplayed(useyearToDeposit)) {
			if (getElementText(useyearToDeposit).isEmpty() || getElementText(useyearToDeposit) == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyUseYearToDeposit", Status.FAIL,
						"Use Year To Deposit may not be displayed");

			} else {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyUseYearToDeposit", Status.PASS,
						"Use Year to Deposit is: " + getElementText(useyearToDeposit));
			}
		} else {
			tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyUseYearToDeposit", Status.FAIL,
					"Use Year To Deposit Area may not be displayed");
		}
	}
	
	/*
	 * Method: costpointDepositCW Description:cost point Deposit for Club
	 * Wyndham payment Date field Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */

	public void costpointDepositCW() {
		if (verifyObjectDisplayed(feePointDeposit)) {
			if (getElementText(feePointDeposit).isEmpty() || getElementText(feePointDeposit) == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "costpointDepositCW", Status.FAIL,
						"Deposit Point Fee is not displayed");
			} else {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "costpointDepositCW", Status.PASS,
						"Deposit Point Fee is displayed and value is: " + getElementText(feePointDeposit));
			}
		}
	}
}
