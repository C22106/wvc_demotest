package cui.pages;

import org.apache.log4j.Logger;

import automation.core.TestConfig;

public class CUIBookPage_Android extends CUIBookPage_Web {

	public static final Logger log = Logger.getLogger(CUIBookPage_Android.class);

	public CUIBookPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}
	CUIBookPage_IOS bookPageIOS = new CUIBookPage_IOS(tcConfig);
	
	
	
	/*
	 * Method: dataEntryForCreditCard Description:Fill Details for Credit Card
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryForCreditCard() {
		bookPageIOS.dataEntryForCreditCard();
		
	}
	
	/*
	 * Method: validateAndEnterSpecialRequest Description: validate and enter
	 * special Request section field Date: June/2020 Author: Monideep
	 * Roychowdhury Changes By: Kamalesh
	 */
	public void validateAndEnterSpecialRequest() {
		bookPageIOS.validateAndEnterSpecialRequest();

	}
	
	/*
	 * Method: validateAndEnterSpecialRequest Description: validate and enter
	 * special Request section field Date: June/2020 Author: Monideep
	 * Roychowdhury Changes By: Kamalesh
	 */
	public void paymentViaPaypal() {
		bookPageIOS.paymentViaPaypal();

	}
	

}
