package cui.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUISecurityQuestionPopUp_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUISecurityQuestionPopUp_Web.class);

	public CUISecurityQuestionPopUp_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By securityPopUpHdr = By
			.xpath("//div[@id='security-questions-modal' and @aria-hidden='false']//p[contains(.,'SECURITY')]");
	protected By editSecurityLink = By.xpath("//button[contains(.,'Edit Question')]");
	/* Manage Sec Questions */
	protected By securityQuestion1 = By.xpath("//h5[contains(.,'QUESTION 1')]/following-sibling::p");
	protected By securityCloseCta = By
			.xpath("//div[@id='security-questions-modal' and @aria-hidden='false']//button[@aria-label='Close modal']");
	protected By securityDescription = By
			.xpath("//div[@id='security-questions-modal']//p[contains(.,'Pick 3 questions from our list')]");
	protected By allSecurityHeader = By.xpath("//div[@id='security-questions-modal']//h6");
	protected By question1 = By.xpath("//div[@id='security-questions-modal']//select[@id='question1']");
	protected By question2 = By.xpath("//div[@id='security-questions-modal']//select[@id='question2']");
	protected By question3 = By.xpath("//div[@id='security-questions-modal']//select[@id='question3']");
	protected By answer1Field = By.xpath("//div[@id='security-questions-modal']//input[@id='question1Answer']");
	protected By answer2Field = By.xpath("//div[@id='security-questions-modal']//input[@id='question2Answer']");
	protected By answer3Field = By.xpath("//div[@id='security-questions-modal']//input[@id='question3Answer']");
	protected By securityCancelCTA = By
			.xpath("//div[@id='security-questions-modal' and @aria-hidden='false']//div/button[contains(.,'Cancel')]");
	protected By securitySaveCta = By
			.xpath("//div[@id='security-questions-modal' and @aria-hidden='false']//div/button[contains(.,'Save')]");
	protected By closedSecurityModal = By.xpath("//div[@id='security-questions-modal' and @aria-hidden='true']");
	protected By question1Select = By.xpath("//select[@id='question1']//option[@value]");
	protected By question2Select = By.xpath("//select[@id='question2']//option[@value]");
	protected By question3Select = By.xpath("//select[@id='question3']//option[@value]");
	protected By answerField1Error = By.id("error-question1Answer");
	protected By answerField2Error = By.id("error-question2Answer");
	protected By answerField3Error = By.id("error-question3Answer");

	/*
	 * Method: secQuesPopUpModalVal Description: Navigate to Manage Security
	 * Ques Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void securityQuestionPopUpModalNavigation() {
		waitUntilElementVisibleBy(driver, editSecurityLink, "ExplicitLongWait");
		getElementInView(editSecurityLink);
		clickElementBy(editSecurityLink);

		waitUntilElementVisibleBy(driver, question1, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(securityPopUpHdr),"Security Pop Header not present");
		elementPresenceVal(securityPopUpHdr, "Security Ques Pop Up Modal  with header", "CUIAccountSettingPage",
				"secQuesPopUpModalVal");
	}

	/*
	 * Method: editSecurityQuestion1Field Description: Edit Security Question 1
	 * Field in Manage Sec Pop Up Account Setting Page Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void editSecurityQuestion1Field() {
		getPopUpElementInView(question1, "Yes");
		selectByText(question1, testData.get("Sec Que 1"));
		fieldDataEnter(answer1Field, testData.get("Answer"));
		sendKeyboardKeys(Keys.TAB);

	}

	/*
	 * Method: checkSelectQuestion1 Description: Edit Security Question 1 Field
	 * in Manage Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void checkSelectQuestion1() {
		List<WebElement> questionsList = getList(question1Select);
		int selectedQuestion1 = Integer.parseInt(questionsList.get(questionsList.size() - 1).getAttribute("value"));

		try {

			String tempString = Integer.toString(selectedQuestion1);
			getPopUpElementInView(question1, "Yes");
			selectByValue(question1, tempString);
			sendKeyboardKeys(Keys.TAB);

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISecurityQuestionPopUp", "checkSelectQuestion1", Status.FAIL,
					"Error in selecting question");
		}

	}

	/*
	 * Method: checkSelectQuestion2 Description: Edit Security Question 2 Field
	 * in Manage Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void checkSelectQuestion2() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> questionsList = getList(question2Select);
		int selectedQuestion2 = Integer.parseInt(questionsList.get(questionsList.size() - 2).getAttribute("value"));

		try {

			String tempString = Integer.toString(selectedQuestion2);
			getPopUpElementInView(question2, "Yes");
			selectByValue(question2, tempString);
			sendKeyboardKeys(Keys.TAB);

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISecurityQuestionPopUp", "checkSelectQuestion2", Status.FAIL,
					"Error in selecting question");
		}

	}

	/*
	 * Method: checkSelectQuestion3 Description: Edit Security Question 3 Field
	 * in Manage Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void checkSelectQuestion3() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> questionsList = getList(question3Select);
		int selectedQuestion3 = Integer.parseInt(questionsList.get(questionsList.size() - 3).getAttribute("value"));

		try {

			String tempString = Integer.toString(selectedQuestion3);
			getPopUpElementInView(question3, "Yes");
			selectByValue(question3, tempString);
			sendKeyboardKeys(Keys.TAB);

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISecurityQuestionPopUp", "checkSelectQuestion3", Status.FAIL,
					"Error in selecting question");
		}

	}

	/*
	 * Method: enterAnswer1 Description: Edit Security Answer 1 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer1() {
		fieldDataEnter(answer1Field, testData.get("Answer"));
		waitForSometime(tcConfig.getConfig().get("WaitForASec"));
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: enterAnswer2 Description: Edit Security Answer 2 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer2() {
		fieldDataEnter(answer2Field, testData.get("Answer"));
		waitForSometime(tcConfig.getConfig().get("WaitForASec"));
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: enterAnswer3 Description: Edit Security Answer 3 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer3() {
		fieldDataEnter(answer3Field, testData.get("Answer"));
		waitForSometime(tcConfig.getConfig().get("WaitForASec"));
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: emptyAnswer1Error Description: Empty Answer 1 Field in Seq Ques
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void emptyAnswer1Error() {
		waitUntilElementVisibleBy(driver, answerField1Error, "ExplicitLongWait");
		if (verifyObjectDisplayed(answerField1Error)) {
			tcConfig.updateTestReporter("CUISecurityQuestionPage", "emptyAnswer1Error", Status.PASS,
					"Empty Answer1 Error displayed");
		} else {
			tcConfig.updateTestReporter("CUISecurityQuestionPage", "emptyAnswer1Error", Status.FAIL,
					"Empty Answer1 Error did not get displayed");
		}
	}

	/*
	 * Method: emptyAnswer2Error Description: Empty Answer 2 Field in Seq Ques
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void emptyAnswer2Error() {
		waitUntilElementVisibleBy(driver, answerField2Error, "ExplicitLongWait");
		if (verifyObjectDisplayed(answerField2Error)) {
			tcConfig.updateTestReporter("CUISecurityQuestionPage", "emptyAnswer2Error", Status.PASS,
					"Empty Answer2 Error displayed");
		} else {
			tcConfig.updateTestReporter("CUISecurityQuestionPage", "emptyAnswer2Error", Status.FAIL,
					"Empty Answer2 Error did not get displayed");
		}
	}

	/*
	 * Method: emptyAnswer3Error Description: Empty Answer 3 Field in Seq Ques
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void emptyAnswer3Error() {
		waitUntilElementVisibleBy(driver, answerField3Error, "ExplicitLongWait");
		if (verifyObjectDisplayed(answerField3Error)) {
			tcConfig.updateTestReporter("CUISecurityQuestionPage", "emptyAnswer3Error", Status.PASS,
					"Empty Answer3 Error displayed");
		} else {
			tcConfig.updateTestReporter("CUISecurityQuestionPage", "emptyAnswer3Error", Status.FAIL,
					"Empty Answer3 Error did not get displayed");
		}
	}

	/*
	 * Method: saveSecurityChanges Description: Save Security Question Changes
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void saveSecurityChanges() {
		clickElementBy(securitySaveCta);

		waitUntilElementInVisible(driver, securitySaveCta, "ExplicitLongWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeyboardKeys(Keys.TAB);

		if (verifyObjectDisplayed(answer1Field)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveSecurityChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveSecurityChanges", Status.PASS,
					"Pop Up Modal  Closed");
			savedChangesValidation(securityQuestion1, "Sec Que 1", "Security Question");
		}
	}

	/*
	 * 
	 * /* Method: securityCancelCTA Description: Close Manage Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void securityCancelCTA() {

		closePopUpModal(securityCancelCTA, closedSecurityModal, answer1Field, Status.FAIL);

	}

	/*
	 * 
	 * /* Method: editAnswer1 Description: Edit Answer 1 field Pop Up Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void editAnswer1() {
		getElementInView(answer1Field);
		scrollUpViaKeyboard(3);
		fieldDataEnter(answer1Field, testData.get("Sec Que 1"));
	}

	/*
	 * Method: secQuesModalVal Description: Validate Manage Sec Ques Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void securityQuestionModalValidation() {
		elementPresenceVal(securityDescription, "Security Question Description", "CUIAccountSettingPage",
				"secQuesModalVal");

		List<WebElement> secHdrList = getList(allSecurityHeader);
		if (secHdrList.size() == 3) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "secQuesModalVal", Status.PASS,
					"3 Security Question Hdr Present");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "secQuesModalVal", Status.FAIL,
					"3 Security Question Hdr  not Present");
		}

		elementPresenceVal(question1, "Security Question 1 Field", "CUIAccountSettingPage",
				"securityQuestionModalValidation");
		elementPresenceVal(question2, "Security Question 2 Field", "CUIAccountSettingPage",
				"securityQuestionModalValidation");
		elementPresenceVal(question3, "Security Question 3 Field", "CUIAccountSettingPage",
				"securityQuestionModalValidation");

		elementPresenceVal(answer1Field, "Security Answer 1 Field", "CUIAccountSettingPage",
				"securityQuestionModalValidation");
		elementPresenceVal(answer2Field, "Security Answer 2 Field", "CUIAccountSettingPage",
				"securityQuestionModalValidation");
		elementPresenceVal(answer3Field, "Security Answer 3 Field", "CUIAccountSettingPage",
				"securityQuestionModalValidation");

		elementPresenceVal(securityCancelCTA, "Cancel CTA", "CUIAccountSettingPage", "securityQuestionModalValidation");
		elementPresenceVal(securitySaveCta, "SAVE CTA", "CUIAccountSettingPage", "securityQuestionModalValidation");
		closePopUpModal(securityCloseCta, closedSecurityModal, question1, Status.PASS);

	}

	public void enterAnswer1EmptyValue() {
		

	}

	public void enterAnswer2EmptyValue() {
	

	}

	public void enterAnswer3EmptyValue() {
		

	}

}
