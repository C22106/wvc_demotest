package cui.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUI_RCIAndDepositPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUI_RCIAndDepositPage_Web.class);

	public CUI_RCIAndDepositPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By headerSelectDeposit = By.xpath(
			"//h1[contains(.,'Select Deposit')] | //div[@class = 'steps__title' and contains(.,'Select Deposit')]");
	protected By buttonCancel = By.xpath("//button[text() = 'Cancel']");
	protected By linknavigateDepositandRCI = By
			.xpath("//div[@class='accountNavigation']//a[contains(.,'Deposits & RCI')]");
	protected By textDepositandRCI = By.xpath("//h1[contains(.,'DEPOSITS & RCI')]");
	protected By headerCWPointDeposit = By.xpath("//span[text() = 'Club Wyndham Points Deposit']");
	protected By sectionRCI = By.xpath("//span[text() = 'RCI']/..");
	protected By sectionPointDeposit = By
			.xpath("//span[contains(text(),'Deposit all or portion of your current Use Year points')]/..");
	protected By pointDepositLearnMoreCTA = By.xpath("//span[contains(text(),'Club Wyndham Points Deposit')]/../a");
	protected By RCILearnMoreCTA = By.xpath("//span[contains(text(),'RCI')]/../a");
	protected By pointDepositInformationPage = By.xpath("//h3[text() = 'Points Deposit Feature']");
	protected By headerRCIOverview = By.xpath("//h3[text() = 'RCI Overview']");
	protected By loadingSpinner = By.xpath("//*[local-name()='svg' and @class='spinner -secondary undefined']");
	protected By linkCWDepositPointCTA = By.xpath("//a[@id = 'deposit-cw-link']");
	protected By linkRCIDepositPointCTA = By.xpath("//a[@id = 'deposit-rci-link']");
	protected By headerRCIDepositText = By.xpath("//h1[text() = 'RCI Points Deposits']");
	protected By headerPointToDeposit = By.xpath("//h6[contains(text(),'Points to Deposit')]");
	protected By buttonExploreRCIResorts = By.xpath("//button[contains(text(),'Explore RCI Resorts')]");
	protected By headerExploreRCIResort = By.xpath("//p[@id = 'exploreRciLabel']");
	protected By checkBoxAcceptRCITerms = By.xpath("//label[@for = 'acceptRciTermsAndCond']");
	protected By buttonContinueRCI = By.xpath("//input[@value = 'Continue to RCI.com']");
	protected By linkRCIPage = By.xpath("//img[@alt = 'Club Wyndham Plus']");

	/*
	 * Method: navigateToDepositandRCIPage Description:Navigate to Deposit and
	 * RCI Page Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void navigateToDepositandRCIPage() {
		waitUntilElementVisibleBy(driver, linknavigateDepositandRCI, "ExplicitLongWait");
		clickElementJSWithWait(linknavigateDepositandRCI);
		// waitUntilElementInVisible(driver, loadingSpinner,
		// "ExplicitLongWait");
		waitUntilElementVisibleBy(driver, textDepositandRCI, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textDepositandRCI), "RCI and Depoist Text not displayed");
		tcConfig.updateTestReporter("CUI_RCI_DepositPage", "navigateToDepositandRCIPage", Status.PASS,
				"Successfully Navigated to RCI and Deposit Page");

	}

	/*
	 * Method: navigateToDepositandRCIPage Description:Navigate to Deposit and
	 * RCI Page Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyOptionsInRCIandDepositPage() {
		elementPresenceVal(sectionPointDeposit, "Section Point Deosit", "CUI_RCI_DepositPage",
				"verifyOptionsInRCIandDepositPage");
		elementPresenceVal(sectionRCI, "Section RCI", "CUI_RCI_DepositPage", "verifyOptionsInRCIandDepositPage");
		elementPresenceVal(headerCWPointDeposit, "Header Club Wyndham Point Deposit", "CUI_RCI_DepositPage",
				"verifyOptionsInRCIandDepositPage");

	}

	/*
	 * Method: validatePointDepositLearnMoreCTA Description:Validate Point
	 * Deposit Learn More CTA Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void validatePointDepositLearnMoreCTA() {
		if (verifyObjectDisplayed(pointDepositLearnMoreCTA)) {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.PASS,
					"Learn More CTA for Point Deposit is present in RCI and Deposit Page");
			clickElementJSWithWait(pointDepositLearnMoreCTA);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, pointDepositInformationPage, "ExplicitLongWait");
			if (verifyObjectDisplayed(pointDepositInformationPage)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.PASS,
						"Successfully clicked on Learn CTA and navigated to Information Page");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.FAIL,
						"Failed to navigate point deposit information page");
			}
		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.FAIL,
					"Learn More CTA for Point Deposit is not present in RCI and Deposit Page");
		}
	}

	/*
	 * Method: clickCTACWPointDeposit Description:Click Club Wyndham Point
	 * deposit CTA Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickCTACWPointDeposit() {
		if (verifyObjectDisplayed(linkCWDepositPointCTA)) {
			clickElementJSWithWait(linkCWDepositPointCTA);
			waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
			waitUntilElementVisibleBy(driver, headerSelectDeposit, "ExplicitLongWait");
			if (verifyObjectDisplayed(headerSelectDeposit) && verifyObjectDisplayed(headerPointToDeposit)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTACWPointDeposit", Status.PASS,
						"Successfully clicked on Club Wyndham Point Deposit Link and navigated to Point Deposit Page");
			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTACWPointDeposit", Status.FAIL,
						"Failed to navigated Point Deposit Page");
			}

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTACWPointDeposit", Status.FAIL,
					"Failed to navigated Point Deposit Page");
		}

	}

	/*
	 * Method: pagenavigateBack Description:Navigate Back Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void pagenavigateBack() {
		navigateBack();
		waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
	}

	/*
	 * Method: validateRCILearnMoreCTA Description:Validate RCI Learn More CTA
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateRCILearnMoreCTA() {
		if (verifyObjectDisplayed(RCILearnMoreCTA)) {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.PASS,
					"Learn More CTA for RCI is present in RCI and Deposit Page");
			clickElementJSWithWait(RCILearnMoreCTA);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, headerRCIOverview, "ExplicitLongWait");
			if (verifyObjectDisplayed(headerRCIOverview)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.PASS,
						"Successfully clicked on Learn CTA and navigated to RCI Overview Page");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.FAIL,
						"Failed to navigate RCI Overview page");
			}
		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.FAIL,
					"Learn More CTA for RCI is not present in RCI and Deposit Page");
		}
	}

	/*
	 * Method: clickButtonRCIExploreResort Description:Click Button RCI Explore
	 * Resort Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickButtonRCIExploreResort() {
		waitUntilElementVisibleBy(driver, buttonExploreRCIResorts, "ExplicitLongWait");
		if (verifyObjectDisplayed(buttonExploreRCIResorts)) {
			clickElementJSWithWait(buttonExploreRCIResorts);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, headerExploreRCIResort, "ExplicitLongWait");
			if (verifyObjectDisplayed(headerExploreRCIResort)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickButtonRCIExploreResort", Status.PASS,
						"Successfully clicked on RCI explore Resort Button");
			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickButtonRCIExploreResort", Status.FAIL,
						"Failed to click on RCI explore Resort Button");
			}

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickButtonRCIExploreResort", Status.FAIL,
					"Button Explore Resort not present in RCI and Deposit Point");
		}
	}

	/*
	 * Method: acceptRCITermsandContinue Description:Accept RCI Terms and
	 * Condition and Continue Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void acceptRCITermsandContinue() {
		if (verifyObjectDisplayed(checkBoxAcceptRCITerms)) {
			getElementInView(checkBoxAcceptRCITerms);
			clickElementJSWithWait(checkBoxAcceptRCITerms);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getObject(buttonContinueRCI).getAttribute("class").contains("disabled")) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "acceptRCITermsandContinue", Status.FAIL,
						"Continue to RCI.com button not disabled even terms and condition checkbox not checked");
			} else {
				clickElementJSWithWait(buttonContinueRCI);
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "acceptRCITermsandContinue", Status.PASS,
						"Successfuly clicked on Continue to RCI.com Button");
			}
		}
	}

	/*
	 * Method: verifyRCILink Description:Verify Page RCI.com page Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyRCILink() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(tabs);
		waitUntilElementVisibleBy(driver, linkRCIPage, "ExplicitLongWait");
		if (verifyObjectDisplayed(linkRCIPage)) {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "verifyRCILink", Status.PASS,
					"Successfully navigated to RCI Page ");
			navigateToMainTab(tabs);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(buttonCancel);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

	}

	/*
	 * Method: clickCTARCIPointDeposit Description:Click CTA RCI Point Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickCTARCIPointDeposit() {
		if (verifyObjectDisplayed(linkRCIDepositPointCTA)) {
			clickElementJSWithWait(linkRCIDepositPointCTA);
			waitUntilElementVisibleBy(driver, headerRCIDepositText, "ExplicitLongWait");
			if (verifyObjectDisplayed(headerRCIDepositText)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTARCIPointDeposit", Status.PASS,
						"Successfully clicked on RCI Point Deposit Link and navigated to Point Deposit Page");
			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTARCIPointDeposit", Status.FAIL,
						"Failed to navigated Point Deposit Page");
			}

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTARCIPointDeposit", Status.FAIL,
					"Link RCI Deposit Point not found");
		}

	}
}
