package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import automation.core.TestConfig;

public class CUIUpcomingVacationPage_IOS extends CUIUpcomingVacationPage_Web {

	public static final Logger log = Logger.getLogger(CUIUpcomingVacationPage_IOS.class);

	public CUIUpcomingVacationPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}

	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadAllReservations() {
		String reservationNumber = testData.get("ReservationNumber").trim();
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber + "']");
		do {
			scrollDownByPixel(400);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} while (!verifyObjectDisplayed(modifyReservation));
	}
	

	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadAllReservations(String reservationNumber) {
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber + "']");
		do {
			scrollDownByPixel(400);;
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} while (!verifyObjectDisplayed(modifyReservation));
	}
	}
