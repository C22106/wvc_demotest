package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUIForgotPasswordPage_Android extends CUIForgotPasswordPage_Web {

	public static final Logger log = Logger.getLogger(CUIForgotPasswordPage_Android.class);
	
	protected By headerVerification = By.xpath("//h2[contains(text(),'Enter your personal details')]");
	protected By companyHeaderVerification = By.xpath("//h2[contains(text(),'Enter the company or trust name')]");

	public CUIForgotPasswordPage_Android(TestConfig tcconfig) {
		super(tcconfig);

		forgotPasswordDescription = By.xpath(
				"//h2[text()='TIME FOR A NEW PASSWORD']/following-sibling::div/p[contains(text(),'worry, just complete a few simple steps to get back in action')]");
	}
	
	/*
	 * Method: enterUsername Description: CUI Forgot Password flow datat
	 * entryDate Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterUsername() {
		waitUntilElementVisibleBy(driver, usernameTextBox, "ExplicitLongWait");
		getElementInView(usernameTextBox);
		sendKeysByScriptExecutor((RemoteWebDriver)driver,"Username*",testData.get("CUI_username"),2);
	}
	
	/*
	 * Method: enterNameDetails Description: CUI Forgot Password flow data entry
	 * Date Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterNameDetails() {

		if (testData.get("AccountAssociated").contains("Individual")) {
			sendKeysByScriptExecutor((RemoteWebDriver)driver,"First Name*",testData.get("FirstName"));
			sendKeysByScriptExecutor((RemoteWebDriver)driver,"Last Name*",testData.get("LastName"));
			getObject(headerVerification).click();
		} else if (testData.get("AccountAssociated").contains("Trust")) {
			// clickElementBy(clickHereLink);
			try {
				sendKeysByScriptExecutor((RemoteWebDriver)driver,"Trust Name*",testData.get("CompanyName"),2);
				getObject(companyHeaderVerification).click();
			} catch (Exception e) {
				clickElementBy(clickHereLink);
				waitUntilElementVisibleBy(driver, companyNameTextBox, "ExplicitLongWait");
				sendKeysByScriptExecutor((RemoteWebDriver)driver,"Trust Name*",testData.get("CompanyName"));
				getObject(companyHeaderVerification).click();
			}

		}
	}

}