package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import automation.core.TestConfig;

public class CUIPasswordResetMethodPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIPasswordResetMethodPage_Web.class);

	public CUIPasswordResetMethodPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	// verification method page
	protected By almostThereHeader = By.xpath("//h2[contains(.,'ALMOST THERE')]");
	protected By almostThereDescription = By.xpath("//h2[contains(.,'ALMOST THERE')]//following-sibling::div/p");
	protected By verificationModalHeader = By.xpath("(//h2[contains(.,'resetting your password')] | //form/div[contains(.,'resetting your password')])");
	protected By contactInformation = By.xpath("//span[@class='wyn-type-disclaimer']");
	protected By emailField = By.xpath("//input[@id='emailInput']");
	protected By emailMethod = By.xpath("//label[@for = 'method_email']");
	protected By securityMethod = By.xpath("//label[@for = 'method_questions']");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");

	/*
	 * 
	 * Method: verificationPageVal Description: Verification Methods Page Val
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verificationPageValidation() {
		waitUntilElementVisibleBy(driver, emailMethod,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(emailMethod),"Email method not present");
		elementPresenceVal(almostThereHeader, "Verification Method Select Page Header", "CUIPasswordResetMethodPage",
				"verificationPageVal");
		elementPresenceVal(almostThereDescription, "Verification Method Selects Page Desc",
				"CUIPasswordResetMethodPage", "verificationPageVal");
		elementPresenceVal(verificationModalHeader, "Verification Method modal header", "CUIPasswordResetMethodPage",
				"verificationPageVal");
		elementPresenceVal(emailMethod, "Email Verification Method option", "CUIPasswordResetMethodPage",
				"verificationPageVal");
		elementPresenceVal(securityMethod, "Sec Quest Verification Method option", "CUIPasswordResetMethodPage",
				"verificationPageVal");
		elementPresenceVal(disabledContinueCTA, "Disabled continue CTA", "CUIPasswordResetMethodPage",
				"createAccountPageVal");

	}

}