package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUILoginPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUILoginPage_Web.class);

	public CUILoginPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By headerNavigation = By.xpath("//div[@class='global-navigation__wrapper']");
	protected By headerLogo = By
			.xpath("//div[@class='global-navigation__wrapper']//li[@class='global-navigation__logo']");
	protected By headerLogoUpdated = By.xpath("//div[@class='global-navigation__wrapper']//img");
	protected By logOutNextGen = By.xpath(
			"(//form[@id='leadForm'] | //input[@id='username']//following-sibling::span[contains(.,'Username*')] )");
	protected By footerNavigation = By.xpath("//div[contains(@class,'footer-holiday')]");
	protected By footerLogo = By
			.xpath("//div[contains(@class,'footer-holiday')]//a[@data-eventlabel='FooterLogo']/img");
	protected By usernameText = By.xpath("//input[@id='username']//following-sibling::span[contains(.,'Username*')]");
	protected By passwordText = By.xpath("//input[@id='password']//following-sibling::span[contains(.,'Password *')]");
	protected By modalText = By.xpath(
			"(//p[contains(.,'ARE YOU A CLUB WYNDHAM OWNER')] | //div[contains(@class,'caption') and contains(.,'ARE YOU A CLUB')])");
	protected By modalHeader = By
			.xpath("(//h1[contains(.,'GET YOU ON VACATION')] | //div[contains(@id,'loginHeadline')])");
	protected By showCTA = By.xpath("//input[@id='password']//following-sibling::a[contains(.,'show')]");
	protected By createAccountDescription = By.xpath(
			"//h3[contains(.,'Need To Create An Account')]//following-sibling::p | //div[contains(@class,'caption') and contains(.,'Create An Account')]//following-sibling::div/p");
	protected By notAnOwnerYetDescription = By.xpath(
			"(//h3[contains(.,'Not An Owner Yet')]//following-sibling::p | //div[text() = 'Not An Owner Yet? ']//following-sibling::div/p)");
	protected By welcomeHeader = By.xpath("//h4[contains(.,'WELCOME')]|//div[contains(text(),'WELCOME')]");
	protected By errorMessage = By.id("error-message");
	protected By invalidPassword = By.xpath(
			"(//p[@id='error-message']//p[contains(.,'Username and Password combination is incorrect')] | //div[@id='error-message']//p[contains(.,'Username and Password combination is incorrect')])");
	protected By accountLocked = By.xpath(
			"(//p[@id='error-message']//p[contains(.,'Your account has been temporarily locked')] | //div[@id='error-message']//p[contains(.,'Your account has been temporarily locked')])");
	protected By invalidLoginError = By
			.xpath("//div[@class='error-block']//p[@id and contains(.,'Something unexpected just happened')]");
	protected By clickFeedbackNo = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By linkForgotPassword = By.xpath("//a[text() = 'Forgot Password']");
	protected By linkForgotUsername = By.xpath("//a[text() = 'Forgot Username']");
	protected By linkCreateAccount = By.xpath(
			"(//h3[text() ='Need To Create An Account?'] | //div[contains(@class,'caption') and contains(.,'Create An Account')])");
	protected By linkRegisterNow = By.xpath("//a[contains(.,'Register Now')]");
	protected By linkNotAnOwner = By
			.xpath("(//h3[text() = 'Not An Owner Yet? '] | //div[text() = 'Not An Owner Yet? '])");
	protected By linkLearnAboutWyndham = By.xpath("//a[contains(.,'Learn More About Club Wyndham')]");
	protected By fieldUsername = By.xpath("//input[@id = 'username']");
	protected By fieldPassword = By.xpath("//input[@id = 'password']");
	protected By loginButton = By.xpath("//button[text()='Login']");
	protected By ownerFullName = By
			.xpath("//h4[contains(.,'WELCOME')]/parent::div/h2 | //div[contains(text(),'WELCOME')]/parent::div/div[2]");
	protected By loginUsername = By.xpath("//input[@id = 'username']");
	protected By cookieAccept = By.xpath("//button[contains(.,'Accept')]");
	protected By logOutCTA = By.xpath("//span[@class='account-navigation__text' and contains(.,'Out')]");
	protected By myAccountDropDown = By
			.xpath("//div[@class='global-navigation__wrapper']//button[contains(@class,'container')]");
	protected By dropDownLogOut = By.xpath(
			"//section[contains(@class,'desktop')]//div[@class='global-account__links']//a[contains(@class,'logout')]//span");
	protected By covidAlertClose = By.xpath("//button[contains(@class,'setCookie') and contains(.,'Close')]");
	protected By bannerAlertClose = By.xpath("//button[contains(@class,'close-button')]");
	protected By feedbackContainer = By.xpath("//div[@id = 'oo_invitation_prompt']");
	protected By FeedbackNoOption = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By captchaFrame = By.xpath("(//div[@class='recaptcha-wrapper']//iframe)[1]");
	// protected By captchaBox =
	// By.xpath("//div[@class='recaptcha-wrapper']//following::textarea");
	protected By captchaBox = By
			.xpath("//span[contains(@class, 'recaptcha-checkbox-unchecked')]//div[@class='recaptcha-checkbox-border']");

	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void launchApplication(String strBrowser) {
		// to get the current browser
		this.driver = checkAndSetBrowser(strBrowser);
		getBrowserName();
		navigateToURL(tcConfig.getConfig().get("NewCUIUrl"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(loginUsername), "Username Field Not Present");
		tcConfig.updateTestReporter("CUILoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}

	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void launchApplication() {
		getBrowserName();
		navigateToURL(tcConfig.getConfig().get("NewCUIUrl"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(loginUsername), "Username Field Not Present");
		tcConfig.updateTestReporter("CUILoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}

	/*
	 * Method: checkFeedbackContainer Description: Feedbac No Date: Apr/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void checkFeedbackContainer() {
		try {
			waitUntilElementVisibleBy(driver, feedbackContainer, "ExplicitLowWait");
			if (verifyObjectDisplayed(feedbackContainer)) {
				getObject(FeedbackNoOption).click();

			}
		} catch (Exception e) {
			log.error("No feedback Container");
		}
	}

	/*
	 * Method: acceptCookie Description: Accept Cookie Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void acceptCookie() {
		waitUntilElementVisibleBy(driver, cookieAccept, "ExplicitMedWait");
		if (verifyObjectDisplayed(cookieAccept)) {
			clickElementBy(cookieAccept);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("CUILoginPage", "acceptCookie", Status.PASS, "Successfully accepted Cookie");
			closeBannerAlert();
			closeCovidAlert();
		}

	}

	/*
	 * Method: closeBannerAlert Description: Close Banner Date: May/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void closeBannerAlert() {
		try {
			WebElement bannerClose = getList(bannerAlertClose).get(0);
			if (verifyObjectDisplayed(bannerClose)) {
				clickElementBy(bannerClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CUILoginPage", "closeBannerAlert", Status.PASS,
						"Successfully Closed Alert");
			}

		} catch (Exception e) {
			log.error("Banner Message Not Present");

		}
	}

	/*
	 * Method: closeCovidAlert Description: Close Covid Date: Apr/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void closeCovidAlert() {
		try {
			WebElement covidClose = getList(covidAlertClose).get(0);
			if (verifyObjectDisplayed(covidClose)) {
				clickElementBy(covidClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CUILoginPage", "closeAlert", Status.PASS, "Successfully Closed Alert");
			}

		} catch (Exception e) {
			log.error("Covid Message Not Present");

		}
	}

	/*
	 * Method: loginBtnClick Description: Click On login cta Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void loginButtonClick() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			getElementInView(loginButton);
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			checkErrorClickLoginAgain();
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginButtonClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * /* Method: preEnteredUsernameVal Description: pre Entered Username Val
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void preEnteredUsernameValidation(String className) throws Exception {
		waitUntilElementVisibleBy(driver, loginUsername, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(loginUsername), "Username Field Not Present");
		String usernameValue = getObject(loginUsername).getAttribute("value").toUpperCase();
		if (className.contains("_")) {
			className = className.split("_")[0];
		}
		if (usernameValue.length() > 1) {
			tcConfig.updateTestReporter("CUILoginPage", "preEnteredUsernameValidation", Status.PASS,
					"Username pre populated as" + usernameValue);
			testData.put("CUI_username", usernameValue);
			// writeDataInExcel(className, CUIScripts_Web.strTestName,
			// "CUI_username", usernameValue);

			clickElementBy(loginUsername);
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "preEnteredUsernameValidation", Status.FAIL,
					"Username was not pre populated");
		}

	}

	/*
	 * Method: enterPasswordOnly Description: pre Entered Username password
	 * enter Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterPasswordOnly() throws Exception {
		waitUntilElementVisibleBy(driver, fieldPassword, "ExplicitLongWait");
		// password data
		String password = testData.get("CUI_password").trim();
		String decodedPassword = decodePassword(password);
		clickElementBy(fieldUsername);
		sendKeyboardKeys(Keys.TAB);
		getObject(fieldPassword).sendKeys(decodedPassword);
		sendKeyboardKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: loginModalHdrs Description: Modal Headers and Text Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void loginModalHeaders() {

		if (verifyObjectDisplayed(modalHeader)) {
			tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.PASS,
					"Modal Header present as: " + getElementText(modalHeader));

			if (verifyObjectDisplayed(modalText)) {
				tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.PASS,
						"Modal Inline text present as: " + getElementText(modalHeader));
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.FAIL,
						"Modal Inline Text not present");
			}
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.FAIL, "Modal Headers not present");
		}

	}

	/*
	 * Method: globalHdrPresence Description: Login page global Header and logo
	 * Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void globalHeaderPresence() {

		if (verifyObjectDisplayed(headerNavigation)) {
			tcConfig.updateTestReporter("CUILoginPage", "globalHeaderPresence", Status.PASS,
					"Global Header is present in the login page");

			elementPresenceVal(headerLogoUpdated, "Header Logo ", "CUILoginPage", "globalHeaderPresence");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "globalHeaderPresence", Status.FAIL,
					"Global Header is not present in the login page");
		}

	}

	/*
	 * Method: globalFooterPresence Description: Login page global footer and
	 * logo Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void globalFooterPresence() {

		if (verifyObjectDisplayed(footerNavigation)) {
			tcConfig.updateTestReporter("CUILoginPage", "globalFooterPresence", Status.PASS,
					"Global Footer is present in the login page");

			elementPresenceVal(footerLogo, "Footer Logo ", "CUILoginPage", "globalFooterPresence");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "globalFooterPresence", Status.FAIL,
					"Global Footer is not present in the login page");
		}

	}

	/*
	 * Method: usernameFieldVal Description: Username Field Presence and text
	 * Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void usernameFieldValidation() {
		waitUntilElementVisibleBy(driver, linkForgotPassword, "ExplicitLongWait");
		if (verifyObjectDisplayed(fieldUsername)) {
			tcConfig.updateTestReporter("CUILoginPage", "usernameFieldValidation", Status.PASS,
					"Username Field present with pretext " + getElementText(usernameText));
			elementPresenceVal(linkForgotUsername, "Forgot Username ", "CUILoginPage", "usernameFieldValidation");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "usernameFieldValidation", Status.FAIL,
					"Username field not present");
		}

	}

	/*
	 * Method: passwordFieldVal Description: Password Field Presence and text
	 * Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordFieldValidation() {

		if (verifyObjectDisplayed(fieldPassword)) {
			tcConfig.updateTestReporter("CUILoginPage", "passwordFieldValidation", Status.PASS,
					"Password Field present with pretext " + getElementText(passwordText));
			elementPresenceVal(showCTA, "Show CTA ", "CUILoginPage", "passwordFieldValidation");
			elementPresenceVal(linkForgotPassword, "Forgot Password Link", "CUILoginPage", "passwordFieldValidation");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "passwordFieldValidation", Status.FAIL,
					"Password field not present");
		}

	}

	/*
	 * Method: newAccountSection Description: New Account Section Val Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newAccountSection() {

		waitUntilElementVisibleBy(driver, linkCreateAccount, "ExplicitLongWait");
		getElementInView(linkCreateAccount);
		if (verifyObjectDisplayed(linkCreateAccount)) {
			tcConfig.updateTestReporter("CUILoginPage", "newAccountSection", Status.PASS,
					"Create Account Link present");
			elementPresenceVal(createAccountDescription, "Create Account Description", "CUILoginPage",
					"newAccountSection");
			elementPresenceVal(linkRegisterNow, "Register Now Link ", "CUILoginPage", "newAccountSection");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "newAccountSection", Status.FAIL,
					"Create Account Link not present");
		}

	}

	/*
	 * Method: multilpeLoginClick Description: Click On login cta Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void multilpeLoginClick() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			getElementInView(loginButton);
			clickElementBy(loginButton);
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginButtonClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * Method: notAnOwnerSection Description: Not an owner Section Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void notAnOwnerSection() {
		// getElementInView(link_notanOwner);
		if (verifyObjectDisplayed(linkNotAnOwner)) {
			tcConfig.updateTestReporter("CUILoginPage", "notAnOwnerSection", Status.PASS, "Not an Owner link present");

			elementPresenceVal(notAnOwnerYetDescription, "Not an Owner Desc", "CUILoginPage", "notAnOwnerSection");
			elementPresenceVal(linkLearnAboutWyndham, "Learn About Wyndham Link ", "CUILoginPage", "notAnOwnerSection");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "notAnOwnerSection", Status.FAIL,
					"Not an Owner link not present");
		}

	}

	/*
	 * Method: clearFieldRefresh Description: Clear Field and refresh Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clearFieldRefresh() throws Exception {

		clearField(fieldUsername);
		refreshPage();
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");

	}

	/*
	 * Method: invalidLoginDataError Description: Error Login CTA Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void invalidLoginDataError() throws Exception {
		refreshPage();
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
	}

	/*
	 * Method: clickLoginCTAError Description: Error Login CTA Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clickLoginCTAError() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			sendKeyboardKeys(Keys.TAB);
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "clickLoginCTAError", Status.FAIL, "Login CTA error");
		}
	}

	/*
	 * Method: errorMsg Description: Error Login CTA Val Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void errorMessageValidation(String strInvalid) throws Exception {
		waitUntilElementVisibleBy(driver, errorMessage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(errorMessage), "Error Message not present");

		tcConfig.updateTestReporter("CUILoginPage", "errorMessageValidation", Status.PASS,
				"Error Message Displayed while inputting invalid " + strInvalid);

		clearField(fieldUsername);
		clearField(fieldPassword);
	}

	/*
	 * Method: loginCTAClick Description: Click On login cta Date: Feb/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void loginCTAClick() throws Exception {
		scrollDownViaKeyboard(2);
		waitUntilElementVisibleBy(driver, loginButton, "ExplicitLongWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			acceptAlert();
			checkFeedbackContainer();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, welcomeHeader, "ExplicitLongWait");
			getElementInView(welcomeHeader);
			Assert.assertTrue(verifyObjectDisplayed(welcomeHeader), "Welcome Header not present");
			if (verifyObjectDisplayed(welcomeHeader)) {
				tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.PASS,
						"User Login successful as user navigated to the landing page successfully");
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.FAIL, "Welcome Header not present");
			}

		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	public void checkErrorClickLoginAgain() {
		try {
			waitUntilElementVisibleBy(driver, errorMessage, "ExplicitLowWait");
			if (verifyObjectDisplayed(errorMessage)) {
				clickElementJSWithWait(loginButton);
			}
		} catch (Exception e) {

		}
	}

	/*
	 * Method: setUserName Description: Set Username with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
		getElementInView(modalHeader);
		// Username entry
		String strUserName = testData.get("CUI_username");
		fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "setUserName", Status.PASS,
				"Username is entered for CUI Login as " + strUserName);
	}

	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setPassword() throws Exception {
		// password data
		String password = testData.get("CUI_password");
		// decodeed password
		String decodedPassword = decodePassword(password);
		fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("CUILoginPage", "setPassword", Status.PASS, "Entered password for CUI login ");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method: selectCaptcha Description: Set Password with correct data Date:
	 * Feb/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void selectCaptcha() throws Exception {
		if (verifyObjectDisplayed(captchaFrame)) {
			switchToFrame(captchaFrame);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(captchaBox);
			driver.switchTo().defaultContent();
			tcConfig.updateTestReporter("CUILoginPage", "selectCaptcha", Status.PASS, "Captcha selected");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "selectCaptcha", Status.PASS, "Captcha not Present");
		}
	}

	/*
	 * Method: loginCTADisabled Description: Disabled login cta val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void loginCTADisabled() throws Exception {

		if (verifyObjectDisplayed(loginButton)) {
			String LoginBtnStatus = getObject(loginButton).getAttribute("disabled");
			if (LoginBtnStatus.equalsIgnoreCase("TRUE")) {
				tcConfig.updateTestReporter("CUILoginPage", "loginCTADisabled", Status.PASS, "Login Button disabled");

			} else {
				tcConfig.updateTestReporter("CUILoginPage", "loginCTADisabled", Status.FAIL,
						"Login button is enabled even username or password field is blank");
			}
		}

	}

	/*
	 * Method: invalidUsername Description: Set Username with incorrect data
	 * Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void invalidUsername() {
		// invalid username
		String strUserName = testData.get("Invalid_UserName");
		fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "invalidUsername", Status.PASS,
				"Username is entered for CUI Login");
	}

	/*
	 * Method: invalidPassword Description: Set password with incorrect data
	 * Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void invalidPassword() throws Exception {
		// invalid password
		String invalidPassword = testData.get("Invalid_Pass");
		// decodeed password
		String decodedPassword = decodePassword(invalidPassword);
		fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("CUILoginPage", "invalidPassword", Status.PASS, "Entered password for CUI login");
	}

	/*
	 * Method: verifyLoginError Description: To Verify LoginErrors Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyLoginError(int attemptCount) {
		// total count of login attempt
		waitUntilElementVisibleBy(driver, errorMessage, "ExplicitMedWait");
		if (verifyObjectDisplayed(invalidPassword) && attemptCount < 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyError", Status.PASS,
					"Login Failed due to invalida data, error displayed");
		} else if (verifyObjectDisplayed(accountLocked) && attemptCount == 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.PASS,
					"Account Locked for more than 5 tries");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.FAIL,
					"Failed to validate invalid login");
		}
	}

	/*
	 * Method: verifyLoginError Description: To Verify LoginErrors Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyAccountLocked() {
		// total count of login attempt
		int attemptCount = Integer.parseInt(testData.get("Attempts"));
		waitForElementError(attemptCount);
		if (verifyObjectDisplayed(accountLocked) && attemptCount == 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyError", Status.PASS,
					"Account is Locked and User Cannt Login");
		} else if (verifyObjectDisplayed(welcomeHeader) && attemptCount < 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.PASS,
					"Account is not locked after 4 attempts");
			logOutApplicationViaDashboard();
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.FAIL,
					"Failed to validate account locked login");
		}
	}

	/*
	 * Method: waitForElementError Description: waiting for element depending on
	 * error number Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void waitForElementError(int attemptCount) {
		if (attemptCount < 5) {
			waitUntilElementVisibleBy(driver, welcomeHeader, "ExplicitMedWait");
		} else if (attemptCount == 5) {
			waitUntilElementVisibleBy(driver, accountLocked, "ExplicitMedWait");
		}
	}

	/*
	 * Method: multipleInvalidLogin Description: Account Lock Validations Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void multipleInvalidLogin() throws Exception {
		// total login attempt count
		int loginAttempt = Integer.parseInt(testData.get("Attempts").trim());
		for (int attempt = 1; attempt <= loginAttempt; attempt++) {
			waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
			getElementInView(modalHeader);
			setUserName();
			invalidPassword();
			waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
			multilpeLoginClick();
			verifyLoginError(attempt);
		}
	}

	/*
	 * Method: logOutApplicationViaDropDown Description: Account Log out via
	 * dropdown page Validations Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void logOutApplicationViaDropDown() {
		if (verifyObjectDisplayed(myAccountDropDown)) {
			clickElementBy(myAccountDropDown);
			waitUntilElementVisibleBy(driver, dropDownLogOut, "ExplicitLongWait");
			getElementInView(dropDownLogOut);
			clickElementBy(dropDownLogOut);
			waitUntilElementVisibleBy(driver, logOutNextGen, "ExplicitLongWait");
			tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS, "User Logged Out");
		} else if (verifyObjectDisplayed(loginButton)) {
			tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS, "No Need to Log Out");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.FAIL, "Logging Out Failed");
		}
	}

	/*
	 * Method: logOutApplicationViaDashboard Description: Account Log out via
	 * dasboard page Validations Date: Apr/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void logOutApplicationViaDashboard() {
		navigateToURL(tcConfig.getConfig().get("NewCUILogOutUrl"));
		waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName), "Owner Full Name not present");
		if (getList(logOutCTA).size() == 1) {
			getElementInView(getList(logOutCTA).get(0));
			clickElementBy(getList(logOutCTA).get(0));
		} else if (getList(logOutCTA).size() == 3) {
			getElementInView(getList(logOutCTA).get(2));
			clickElementBy(getList(logOutCTA).get(2));
		}
		waitUntilElementVisibleBy(driver, usernameText, "ExplicitLongWait");
		tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS, "User Logged Out");
	}

	public void logOutApplication() {
		// TODO Auto-generated method stub

	}

	public void errorMessasgeValidation(String string) {
		// TODO Auto-generated method stub

	}

	public void clearCache() {
		// TODO Auto-generated method stub

	}

}
