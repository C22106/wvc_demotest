package cui.pages;

import org.apache.log4j.Logger;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBucketListPage_Android extends CUIBucketListPage_Web {

	public static final Logger log = Logger.getLogger(CUIBucketListPage_Android.class);

	public CUIBucketListPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}

	public String URL;
	
	/*
	 * Method: resortCardsNumbers Description:Validate total resort cards Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void resortCardsNumbers() {
		getElementInView(getList(bucketListCards).get(9));
		if (getList(bucketListCards).size() == totalResortsWishlisted()) {
			tcConfig.updateTestReporter("CUIBucketListPage", "resortCardsNumbers", Status.PASS,
					"Total " + totalResortsWishlisted() + " Resort cards displyed correctly as a link");
		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "resortCardsNumbers", Status.FAIL,
					"Total Resort displyed numbers did not match ");
		}

	}
	/*
	 * Method: exploreCTANavigation Description:Validate Explore CTA Navigation
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void exploreCTANavigation() {
		URL=driver.getCurrentUrl();
		clickElementBy(exploreResortCTA);
		waitUntilElementVisibleBy(driver, locationField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(locationField));
		tcConfig.updateTestReporter("CUIBucketListPage", "exploreAllResortValdation", Status.PASS,
				"Navigated To Resorts Page");

	}

	/*
	 * Method: wishListedCardValidation Description: WishListed First Card Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListedCardValidation() {
		String strFirstCard = wishListFirstCard();
		driver.get(URL);
		//navigateBackAndWait(bucketListHeader);
		//sendKeyboardKeys(Keys.HOME);
		if (getList(resortNames).get(0).getText().contains(strFirstCard)) {
			tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.PASS,
					"The recently wishlisted card is displayed at first");
		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.FAIL,
					"The recently wishlisted card is not displayed at first");
		}

	}
}
