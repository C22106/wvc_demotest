package cui.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUITravellerInformationPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUITravellerInformationPage_Web.class);

	public CUITravellerInformationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By headerReservationBalance = By.xpath(
			"//h1[contains(.,'Reservation Balance')] | //div[@class = 'steps__title' and contains(.,'Reservation Balance')]");
	protected By headerPointProtection = By.xpath(
			"//h1[contains(.,'Points Protection')] | //div[@class = 'steps__title' and contains(.,'Points Protection')]");
	protected By headerCompleteBooking = By.xpath(
			"//h1[contains(.,'Complete Booking')] | //div[@class = 'steps__title' and contains(.,'Complete Booking')]");
	protected By travelerGuest = By.xpath("//label[@for='traveler_guest']");
	protected By bookingHeader = By.xpath("//div[@class='booking-header__desktop']//li[@class='step -active']/span");
	protected By headerTravelerInfo = By.xpath(
			"//h1[contains(.,'Traveler Info')] | //div[contains(@class, 'steps') and contains(.,'Traveler Info')]");

	protected By textBookDetails = By.xpath("//span[text() = 'Reservation Details']");
	protected By travelerEmailField = By.xpath("//input[@id = 'travelerEmail']");
	protected By travelerNameField = By.id("travelerName");
	protected By buttonContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By termsAndConditionsCheckBx = By.xpath("//label[@for='discovery-agreement-checkbox']");
	protected By fieldGuestFirstName = By.xpath("//label[@for='guestFirstName'] | //*[@id='guestFirstName']");
	protected By fieldGuestLastName = By.xpath("//label[@for='guestLastName']| //*[@id='guestLastName']");
	protected By fieldGuestEmail = By.xpath("//label[@for='guestEmail']| //*[@id='guestEmail']");
	protected By fieldGuestPhone = By.xpath("//label[@for='guestPhone']|//*[@id='guestPhone']");
	protected By fieldGuestAddress = By.xpath("//label[@for='guestAddress']| //*[@id='guestAddress']");
	protected By fieldGuestCity = By.xpath("//label[@for='guestCity']|//*[@id='guestCity']");
	protected By fieldGuestZip = By.xpath("//label[@for='guestZip']| //*[@id='guestZip']");
	protected By guestAgree = By.xpath("//label[@for='agreeGuestIs21']");
	protected By fieldGuestCountry = By.id("guestCountry");
	protected By fieldGuestState = By.id("guestState");
	protected By headerGuestConfirmationCredits = By.xpath("//h1[text()='Guest Confirmation Credits']");
	protected By labelRequired = By
			.xpath("//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Required')]");
	protected By valueRequired = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Required')]/following-sibling::td");
	protected By labelAvailable = By
			.xpath("//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Available')]");
	protected By valueTotalCost = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/ancestor::div//th[contains(text(),'Total Cost')]/following-sibling::td");
	protected By labelTotalCost = By
			.xpath("//h1[text()='Guest Confirmation Credits']/ancestor::div//th[contains(text(),'Total Cost')]");

	protected By valueAvailable = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Available')]/following-sibling::td");
	protected By instructionTxtGuestConfirmationCredits = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/parent::div//p[contains(text(),'In order to have a guest')]");
	protected By selectGuestChkBox = By.xpath("//label[@for='traveler_guest']");
	protected By guestFirstName = By.id("guestFirstName");
	protected By guestLastName = By.id("guestLastName");
	protected By guestEmail = By.id("guestEmail");
	protected By guestPhn = By.id("guestPhone");
	protected By guestAddress = By.id("guestAddress");
	protected By guestCity = By.id("guestCity");
	protected By guestState = By.id("guestState");
	protected By guestZip = By.id("guestZip");
	protected By agreeGuest21ChkBox = By.xpath("//label[@for='agreeGuestIs21']");
	protected By discoveryChkBox = By.xpath("//label[@for='discovery-agreement-checkbox']");
	protected By totalUnitcost = By.xpath("//span[contains(.,'PTS') and not(contains(@class,'strikethrough'))]");;
	protected By buttonBookingContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected static String prepopulatedName;
	// updated
	protected By textTravelInfo = By.xpath("//div[contains(@class,'steps__title') and contains(.,'Traveler Info')]");

	/*
	 * Method: checkDiscoveryTermsChkBox Description:check Discovery Terms
	 * ChkBox Information Date field Date: June/2020 Author: Monideep Roy
	 * Changes By: NA
	 */
	public void checkDiscoveryTermsChkBox() {

		clickElementBy(discoveryChkBox);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "checkDiscoveryTermsChkBox", Status.PASS,
				"Discovery terms and agreement check box has been checked");
	}

	/*
	 * Method: fillTravellerInformation Description:Validate and fill Traveller
	 * Information Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	/*
	 * Method: fillTravellerInformation Description:Validate and fill Traveller
	 * Information Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void fillTravellerInformation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String emailtraveller = testData.get("TravellerEmailID");
		waitUntilElementVisibleBy(driver, textBookDetails, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textBookDetails), "Text Book Details not present");
		if (verifyObjectDisplayed(textTravelInfo)) {
			getElementInView(travelerEmailField);
			if (verifyObjectDisplayed(travelerEmailField)) {
				clickElementBy(travelerEmailField);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clearElementBy(travelerEmailField);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(travelerEmailField, emailtraveller);
				tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.PASS,
						"Fields successfully entered in Default Traveller Information Page");
			} else {
				tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.FAIL,
						"Email field not present");
			}

		} else {
			tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.FAIL,
					"Traveller Information Header Not present");
		}
	}

	/*
	 * Method: validateDefaultUserName Description:Validate default username
	 * matches account details Date field Date: June/2020 Author: Monideep
	 * Roychowdhury Roy Changes By: NA
	 */
	public void validateDefaultUserName() {

		waitUntilElementVisibleBy(driver, textBookDetails, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textBookDetails), "Text Book Details not present");
		String prePopulatedValue = getElementAttribute(travelerNameField, "value").trim();
		if (getElementAttribute(travelerNameField, "className").contains("select")) {
			Select nameSelect = new Select(getObject(travelerNameField));
			String valueSelect = nameSelect.getFirstSelectedOption().getText().trim();
			if (valueSelect.split(" ").length >= 3) {
				String[] arr = prePopulatedValue.split(" ");
				valueSelect = arr[0] + " " + arr[arr.length - 1];
			}

			Assert.assertTrue(valueSelect.equalsIgnoreCase(tcConfig.getTestData().get("OwnerNameFull")),
					"Primary Owner name not selected by default");
			tcConfig.updateTestReporter("CUITravellerInformationPage", "validateDefaultUserName", Status.PASS,
					"Primary Owner name not selected by default");
		} else if (getElementAttribute(travelerNameField, "className").contains("input")) {
			if (prePopulatedValue.split(" ").length >= 3) {
				String[] arr = prePopulatedValue.split(" ");
				prePopulatedValue = arr[0] + " " + arr[arr.length - 1];
			}
			Assert.assertTrue(prePopulatedValue.equalsIgnoreCase(tcConfig.getTestData().get("OwnerNameFull")),
					"Primary Owner name not selected by default");
			tcConfig.updateTestReporter("CUITravellerInformationPage", "validateDefaultUserName", Status.PASS,
					"Primary Owner name not selected by default");

		}
	}

	/*
	 * Method: selectAndPopulateGuestInfo Description:selectAndPopulateGuestInfo
	 * Date: June/2020 Author: Monideep Roychowdhury Roy Changes By: NA
	 */

	public void selectAndPopulateGuestInfo() {

		clickElementBy(selectGuestChkBox);
		waitUntilElementVisibleBy(driver, guestFirstName, "ExplicitMedWait");

		sendKeysBy(guestFirstName, "GuestFirst");
		sendKeysBy(guestLastName, "GuestLast");
		tcConfig.getTestData().put("GuestName", "GuestFirst GuestLast");
		sendKeysBy(guestEmail, "guest.email@gmail.com");
		tcConfig.getTestData().put("GuestEmail", "guest.email@gmail.com");
		sendKeysBy(guestPhn, "9962001374");
		sendKeysBy(guestAddress, "44 Center Grove Road");
		sendKeysBy(guestCity, "Randolph");
		selectByText(guestState, "New Jersey");
		sendKeysBy(guestZip, "07869");
		clickElementBy(agreeGuest21ChkBox);
	}

	/*
	 * Method: clickContinueBooking Description:click Continue Booking Date
	 * field Date: June/2020 Author: Kamalesh Roy Changes By: NA
	 */
	public void clickContinueBooking() {
		clickContinueButton(buttonContinue, "CUITravellerInformationPage");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(headerReservationBalance)
				|| verifyObjectDisplayed(headerPointProtection) || verifyObjectDisplayed(headerCompleteBooking),
				"Complete booking header not present");
		tcConfig.updateTestReporter("clickContinueBooking", "clickContinueBooking", Status.PASS,
				"Successfully navigated to Next Page");
	}

	/*
	 * Method: verifyPageHeaderAndBookingHeaderForTravelerInfo
	 * Description:Veriying the first step of the booking header Date field
	 * Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyFirstStep() {
		waitUntilElementVisibleBy(driver, headerTravelerInfo, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerTravelerInfo), "Traveler Info header not present");
		String headerValue = getElementText(bookingHeader);
		if (headerValue.contains("TRAVELER INFO") && getElementText(headerTravelerInfo).contains("1. TRAVELER INFO")) {
			tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyFirstStep", Status.PASS,
					"The First step of the booking header is " + headerValue + " and the page header is "
							+ getElementText(headerTravelerInfo));

		} else {
			tcConfig.updateTestReporter("CUITravellerInformationPage",
					"verifyPageHeaderAndBookingHeaderForTravelerInfo", Status.FAIL,
					headerValue + " page not displayed");
		}

	}

	/*
	 * Method: verifyOwnerNamePrepopulated Description:Veriying the first step
	 * of the booking header Date field Date: June/2020 Author: Kamalesh Changes
	 * By: NA
	 */

	public void verifyOwnerNamePrepopulated() {
		waitUntilElementVisibleBy(driver, travelerNameField, "ExplicitLongWait");
		prepopulatedName = getElementAttribute(travelerNameField, "value").trim();
		if (prepopulatedName.split(" ").length == CUIAccountSettingPage_Web.strMemberName.split(" ").length) {
			Assert.assertTrue(
					verifyObjectDisplayed(travelerNameField)
							&& prepopulatedName.equalsIgnoreCase(CUIAccountSettingPage_Web.strMemberName),
					"Owner name incorrect");
			tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyOwnerNamePrepopulated", Status.PASS,
					"Owner Name Field is present and is prepopulated with value "
							+ getElementAttribute(travelerNameField, "value").trim());
		} else {
			String[] arr = prepopulatedName.split(" ");
			if (arr.length >= 3) {
				assertTrue(CUIAccountSettingPage_Web.strMemberName.equalsIgnoreCase(arr[0] + " " + arr[arr.length - 1]),
						"Owner name incorrect");
				tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyOwnerNamePrepopulated",
						Status.PASS, "Owner Name Field is present and is prepopulated");
			} else if (arr.length < 3) {
				assertTrue(CUIAccountSettingPage_Web.strMemberName.equalsIgnoreCase(prepopulatedName),
						"Modified Traveler Name is incorrect in the Modification History");
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationTypeInModificationHistory",
						Status.PASS, "Modified Traveler Name is displayed correctly in the Modification History");
			}
		}

	}

	/*
	 * Method: verifyOwnerNamePrepopulatedSingleOwner Description:Veriying the
	 * first step of the booking header Date field Date: June/2020 Author:
	 * Kamalesh Changes By: NA
	 */

	public void verifyOwnerNamePrepopulatedSingleOwner() {
		waitUntilElementVisibleBy(driver, travelerNameField, "ExplicitLongWait");
		prepopulatedName = getElementAttribute(travelerNameField, "value").trim();
		Assert.assertTrue(verifyObjectDisplayed(travelerNameField)
				&& getElementAttribute(travelerNameField, "readOnly").equals("true"), "Owner Name error");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyOwnerNamePrepopulated", Status.PASS,
				"Owner Name Field is present and is prepopulated with value for Single Owner and it is not editable "
						+ getElementAttribute(travelerNameField, "value").trim());
	}

	/*
	 * Method: verifyOwnerEmailPrepopulated Description:Veriying the first step
	 * of the booking header Date field Date: June/2020 Author: Kamalesh Changes
	 * By: NA
	 */
	public void verifyOwnerEmailPrepopulated() {
		waitUntilElementVisibleBy(driver, travelerEmailField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(travelerEmailField) && getElementAttribute(travelerEmailField, "value")
				.trim().equalsIgnoreCase(CUIAccountSettingPage_Web.strOwnerEmail), "Owner Email not present");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyOwnerEmailPrepopulated", Status.PASS,
				"Owner Email Address Field is present and is prepopulated with value "
						+ getElementAttribute(travelerEmailField, "value").trim());

	}

	/*
	 * Method: verifyGuestOptionNotPresent Description:Veriying the first step
	 * of the booking header Date field Date: June/2020 Author: Kamalesh Changes
	 * By: NA
	 */
	public void verifyGuestOptionNotPresent() {
		waitUntilElementVisibleBy(driver, travelerEmailField, "ExplicitLongWait");
		Assert.assertFalse(verifyObjectDisplayed(travelerGuest), "Guest Option is displayed on the traveler page");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyGuestOptionNotPresent", Status.PASS,
				"Guest Option is not displayed on the traveler page");
	}

	/*
	 * Method: verifyGuestOptionNotPresent Description:Veriying the first step
	 * of the booking header Date field Date: June/2020 Author: Kamalesh Changes
	 * By: NA
	 */
	public void selectGuestOption() {
		waitUntilElementVisibleBy(driver, travelerGuest, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(travelerGuest), "Travel Guest not present");
		getElementInView(travelerGuest);
		clickElementBy(travelerGuest);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyGuestOptionNotPresent", Status.PASS,
				"Guest Option is displayed on the traveler page");
	}

	/*
	 * Method: selectTermsAndConditionsCheckBx Description:Veriying the first
	 * step of the booking header Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void selectTermsAndConditionsCheckBx() {
		waitUntilElementVisibleBy(driver, termsAndConditionsCheckBx, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(termsAndConditionsCheckBx),
				"Terms and Condition Check Box Not Present");
		clickElementBy(termsAndConditionsCheckBx);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "selectTermsAndConditionsCheckBx", Status.PASS,
				"Terms and Conditions Check Box is selected");

	}

	/*
	 * Method: selectAnotherValueFromDropdown Description:Veriying the first
	 * step of the booking header Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void selectAnotherValueFromDropdown() {
		Select select = new Select(driver.findElement(travelerNameField));
		java.util.List<WebElement> options = select.getOptions();
		for (int i = 0; i <= options.size() - 1; i++) {
			if (options.get(i).getText().equalsIgnoreCase(prepopulatedName)) {
				options.remove(i);
			}
		}
		select.selectByVisibleText(options.get(options.size() - 1).getText());
		Assert.assertFalse(getElementAttribute(travelerEmailField, "value").trim().isEmpty(),
				"Cound not select different owner");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "selectAnotherValueFromDropdown", Status.PASS,
				"Different owner is selected");
	}

	/*
	 * Method: verifyLabelAndTextGuestConfirmationCredits Description:Veriying
	 * the first step of the booking header Date field Date: June/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	public void verifyLabelAndTextGuestConfirmationCredits() {
		waitUntilElementVisibleBy(driver, headerGuestConfirmationCredits, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerGuestConfirmationCredits),
				"Guest Confirmation Credits not present");
		getElementInView(headerGuestConfirmationCredits);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Guest Confirmation Credits section present");

		Assert.assertTrue(verifyObjectDisplayed(labelRequired) && verifyObjectDisplayed(valueRequired),
				"Required label and Value present");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Required Label and Value is present");

		Assert.assertTrue(verifyObjectDisplayed(labelAvailable) && verifyObjectDisplayed(valueAvailable),
				"Value label not available");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Available Label and Value is present");

		Assert.assertTrue(verifyObjectDisplayed(instructionTxtGuestConfirmationCredits));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Instruction Text is present on the Guest Confirmation Credits section");
	}

	/*
	 * Method: verifyCreditRateSingleOwner Description:Veriying the first step
	 * of the booking header Date field Date: June/2020 Author: Kamalesh Changes
	 * By: NA
	 */
	public void verifyCreditRateSingleOwner() {
		Assert.assertTrue(verifyObjectDisplayed(labelTotalCost) && verifyObjectDisplayed(valueTotalCost),
				"Total Cost label not avialable");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Total Cost Label and Value is present");

	}

	/*
	 * Method: checkRelativeAndAvailableValue Description:Veriying the first
	 * step of the booking header Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void checkRequiredAndAvailableValue(String Owner) {
		switch (Owner) {
		case "single-Owner":
			Assert.assertTrue(getElementText(valueAvailable).equals("0") && getElementText(valueRequired).equals("1"),
					"Value did not match");
			tcConfig.updateTestReporter("CUITravellerInformationPage", "checkRequiredAndAvailableValue", Status.PASS,
					"Available value is 0 and Required Value is 1");

			break;

		case "multi-Owner":
			Assert.assertTrue(
					getElementText(valueRequired).equals("1") && Integer
							.parseInt(getElementText(valueAvailable)) > Integer.parseInt(getElementText(valueRequired)),
					"Value did not match");
			tcConfig.updateTestReporter("CUITravellerInformationPage", "checkRequiredAndAvailableValue", Status.PASS,
					"Guest has enough credit to apply");
			break;
		}

	}

	/*
	 * Method: enterGuestInfo Description:Veriying the first step of the booking
	 * header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void enterGuestInfo() {
		waitUntilElementVisibleBy(driver, fieldGuestFirstName, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(fieldGuestFirstName), "Guest First name not present");
		String guestFirstName = testData.get("guestFirstName");
		String guestLastName = testData.get("guestLastName");
		String guestEmail = testData.get("guestEmail");
		String guestPhoneNumber = testData.get("guestPhoneNumber");
		String guestCountry = testData.get("guestCountry");
		String guestStreetAddress = testData.get("guestStreetAddress");
		String guestCity = testData.get("guestCity");
		String guestState = testData.get("guestState");
		String guestZipCode = testData.get("guestZipCode");

		fieldDataEnter(fieldGuestFirstName, guestFirstName);
		fieldDataEnter(fieldGuestLastName, guestLastName);
		fieldDataEnter(fieldGuestEmail, guestEmail);
		fieldDataEnter(fieldGuestPhone, guestPhoneNumber);
		selectByText(fieldGuestCountry, guestCountry);
		fieldDataEnter(fieldGuestAddress, guestStreetAddress);
		fieldDataEnter(fieldGuestCity, guestCity);
		selectByText(fieldGuestState, guestState);
		fieldDataEnter(fieldGuestZip, guestZipCode);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "enterGuestInfo", Status.PASS,
				"Guest Information entered successfully");
	}

	/*
	 * Method: selectAgreeCheckBxGuestConfirmationCredits Description:Veriying
	 * the first step of the booking header Date field Date: June/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	public void selectAgreeCheckBxGuestConfirmationCredits() {
		waitUntilElementVisibleBy(driver, guestAgree, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(guestAgree), "Guest Agree box not present");
		clickElementBy(guestAgree);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "selectAgreeCheckBxGuestConfirmationCredits",
				Status.PASS, getElementText(guestAgree) + " Check Box is selected");

	}

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(buttonBookingContinue);
		clickElementJSWithWait(buttonBookingContinue);
	}

}