package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUISecurityQuestionPopUp_IOS extends CUISecurityQuestionPopUp_Web {

	public static final Logger log = Logger.getLogger(CUISecurityQuestionPopUp_IOS.class);

	public By securityQuestionHeader=By.xpath("//h6[text()='Security Question 1']");
	public CUISecurityQuestionPopUp_IOS(TestConfig tcconfig) {
		super(tcconfig);
		editSecurityLink = By.xpath("//div[contains(@class,'manage-security-questions')]//button[contains(.,'Edit')]");
		securityCancelCTA = By
				.xpath("//div[@id='security-questions-modal' and @aria-hidden='false']//button[contains(@class,'cancel-changes-button')]");

	}
	
	
	/*
	 * 
	 * /* Method: editAnswer1 Description: Edit Answer 1 field Pop Up Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta
	 */
	public void editAnswer1() {
		getElementInView(answer1Field);
		scrollUpViaKeyboard(3);
		try{
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Your Answer*", testData.get("Sec Que 1"),1);	
		tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "editAnswer1", Status.PASS,
				"Answer 1 field entered successfully");
		clickElementBy(securityQuestionHeader);
		}catch (Exception ex){
			tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "editAnswer1", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	/*
	 * Method: enterAnswer1 Description: Edit Security Answer 1 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer1EmptyValue() {
		getElementInView(answer1Field);
		try{
		clickElementBy(answer1Field);	
		tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "enterAnswer1", Status.PASS,
				"Answer 1 field entered successfully");
		clickElementBy(securityQuestionHeader);
		}catch (Exception ex){
			tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "enterAnswer1", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	/*
	 * Method: enterAnswer2 Description: Edit Security Answer 2 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer2EmptyValue() {
		
		try{
		clickElementBy(answer2Field);	
		tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "enterAnswer2", Status.PASS,
				"Answer 2 field entered successfully");
		clickElementBy(securityQuestionHeader);
		}catch (Exception ex){
			tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "enterAnswer2", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	/*
	 * Method: enterAnswer3 Description: Edit Security Answer 3 Field in Manage
	 * Sec Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void enterAnswer3EmptyValue() {
		
		try{
		clickElementBy(answer3Field);
		tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "enterAnswer3", Status.PASS,
				"Answer 3 field entered successfully");
		clickElementBy(securityQuestionHeader);
		}catch (Exception ex){
			tcConfig.updateTestReporter("CUISecurityQuestionPopUp_IOS", "enterAnswer3", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	/*
	 * *************************Method: closePopUpModal ***
	 * *************Description Close Any Pop Up Modal**
	 */

	public void closePopUpModal(By closeCTAElement, By closedModalElement, By elementFromPopUp, Status status) {
		if (verifyObjectDisplayed(closeCTAElement)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
					"Cancel Cta present");
			clickElementBy(closeCTAElement);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (isAlertPresent()) {
				driver.switchTo().alert().accept();
				driver.switchTo().defaultContent();
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", Status.PASS,
						"Successfully accepetd the alert popup");
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", status,
						"No popup came while Closing the pop up");
			}
			waitUntilElementVisibleBy(driver, closedModalElement, "ExplicitLongWait");
			if (verifyObjectDisplayed(elementFromPopUp)) {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
						"Pop Up Modal did not Closed");
			} else {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
						"Pop Up Modal  Closed");
			}

		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
					"Cancel CTA not present");
		}
	}
	
	/*
	 * Method: saveSecurityChanges Description: Save Security Question Changes
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void saveSecurityChanges() {
		clickElementBy(securitySaveCta);

		waitUntilElementVisibleBy(driver, closedSecurityModal, "ExplicitLongWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(answer1Field)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveSecurityChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveSecurityChanges", Status.PASS,
					"Pop Up Modal  Closed");
			savedChangesValidation(securityQuestion1, "Sec Que 1", "Security Question");
		}
	}
}
