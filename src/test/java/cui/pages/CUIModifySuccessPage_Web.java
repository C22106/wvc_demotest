package cui.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIModifySuccessPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIModifySuccessPage_Web.class);

	public CUIModifySuccessPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By expandTotalPoints = By.xpath("//th[text()='Total Points']/button[@class='clear button-svg']");
	protected By availablePointLabel = By.xpath("//span[contains(text(),'Available Points')]");
	protected By availablePointValue = By
			.xpath("//span[contains(text(),'Available Points')]/..//following-sibling::td");
	protected By BorrowedPointLabel = By.xpath("//span[contains(text(),'Borrowed Points')]");
	protected By BorrowedPointValue = By.xpath("//span[contains(text(),'Borrowed Points')]/..//following-sibling::td");
	protected By rentedPointLabel_MembershipCharges = By.xpath("//span[contains(text(),'Rented Points')]");
	protected By rentedPointValue_MembershipCharges = By
			.xpath("//span[contains(text(),'Rented Points')]/..//following-sibling::td");
	protected static String strModifyingCost;
	protected static String strModifyingCostHK;
	protected By purchasedHKLabel = By.xpath("//th[text()='Purchased Housekeeping Credits']");
	protected By purchasedHKValue = By.xpath("//th[text()='Purchased Housekeeping Credits']/following-sibling::td");
	protected By PaymentTotalLabel = By.xpath("//th[text()='Payment Total']");
	protected By PaymentTotalvalue = By.xpath("//th[text()='Payment Total']/following-sibling::td");
	protected By valueHKUsedCurrentUseYear = By
			.xpath("//span[text() = 'Housekeeping Credits - Current Use Year']/../../td");
	protected By valueHKBorrowedUseYear = By
			.xpath("//span[contains(text(),'Borrowed Housekeeping Credits - Next Use Year')]/../../td");
	protected By valueHKPurchasedUseYear = By.xpath("//span[text() = 'Purchased Housekeeping Credits']/../../td");
	protected By expandHousekeepingBreakdown = By.xpath("//span[text() = 'Housekeeping Credits']/..//button");
	protected By textModificationSummary = By
			.xpath("(//h1[text() = 'Modification Summary'] | //h2[text() = 'Modification Summary'])");
	protected By modifiedTravelerName = By
			.xpath("(//th[text() = 'Traveler']/../td/strong | //th[text() = 'Traveler']/../td/span)");
	protected By modifiedDate = By
			.xpath("(//th[text() = 'Date Modified']/../td/strong | //th[text() = 'Date Modified']/../td/span)");
	protected By modifiedBy = By.xpath(
			"(//th[contains(text(),'Modified by')]/../td/strong | //th[contains(text(),'Modified by')]/../td/span)");
	protected By modifyTravelConfirm = By.xpath("//h1[text() = 'Modify Traveler Confirmation'] | //div[text() = 'Modify Traveler Confirmation']");
	protected By textCongrats = By.xpath("//h2[contains(.,'Congratulations')] | //div[contains(@class,'confirmation__congrats')]");
	protected By confirmModifyTraveler = By.xpath("//p[contains(.,'changed the traveler')]");
	protected By textModificationDetails = By.xpath("//h2[text() = 'Modification Details'] | //div[text() = 'Modification Details']");
	protected By sectionCancelPolicy = By.xpath("//h2[text()  = 'Cancellation Policy']/..");
	protected By reservationDetailsCTA = By.xpath("//a[text() = 'View Reservation Details']");
	protected By sectionReservationSummary = By.xpath("//section[@class = 'reservation-details-summary']");
	protected By makeAnotherModification = By.xpath("//a[text() = 'Make Another Modification']");
	protected By chargesSummaryHeader = By.xpath("//h2[.='Charges Summary']");
	protected By paymentMethodIcon = By.xpath("//img[contains(@class,'icon')]");
	protected By paypalEmailID = By.xpath("//img[contains(@class,'icon')]/parent::div/following-sibling::p/span");
	protected By guestCreditLabel = By.xpath("//th[contains(text(),'Guest Confirmation Credit')]");
	protected By guestCreditValue = By
			.xpath("//th[contains(text(),'Guest Confirmation Credit')]/following-sibling::td/strong");
	protected By paymentTotalLabel = By.xpath("//th[contains(text(),'Payment Total')]");
	protected By paymentTotalValue = By.xpath("//th[contains(text(),'Payment Total')]/following-sibling::td");

	// add Nights
	protected By datesModified = By.xpath("//th[contains(.,'Dates')]/following-sibling::td");
	protected By pointsProtection = By.xpath("//th[text() = 'Points Protection']/../td/span");
	protected By totalPointsDisplayed = By.xpath("//th[contains(.,'Total Points')]/following-sibling::td");
	protected By paymentChargesHeader = By
			.xpath("//h2[.='Payment Charges'] | //div[contains(@class,'steps') and text()='Payment Charges']");
	protected By rentedPointLabel = By.xpath("//th[text()='Rented Points']");
	protected By rentedPointValue = By.xpath("//th[text()='Rented Points']/following-sibling::td");
	protected By hkCreditLabel = By.xpath("//span[contains(text(),'Housekeeping Credits')]");
	protected By hkCreditValue = By.xpath("//span[contains(text(),'Housekeeping Credits')]/../following-sibling::td");
	protected By purchaseHKCreditLabel = By.xpath("//th[text()='Purchased Housekeeping Credits']");
	protected By purchaseHKCreditValue = By
			.xpath("//th[text()='Purchased Housekeeping Credits']/following-sibling::td");
	protected By availableHKValue = By
			.xpath("//span[contains(text(),'Housekeeping Credits - Current')]/..//following-sibling::td");
	protected By BorrowedHKValue = By
			.xpath("//span[contains(text(),'Borrowed Housekeeping')]/..//following-sibling::td");

	/*
	 * Method: textModifyTravelConfirm Description:text Modify Travel Confirm
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textModifyTravelConfirm() {
		waitUntilElementVisibleBy(driver, modifyTravelConfirm, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(modifyTravelConfirm), "Modify Travel Confirmation text not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "textModifyTravelConfirm", Status.PASS,
				"Modify Travel Confirmation text present");
	}

	/*
	 * Method: textCongratsVerify Description:text Congrats Verify Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textCongratsVerify() {
		waitUntilElementVisibleBy(driver, textCongrats, "ExplicitLongWait");
		if (verifyObjectDisplayed(textCongrats)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textCongratsVerify", Status.PASS,
					"Congrats Present as : " + getElementText(textCongrats));
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textCongratsVerify", Status.FAIL,
					"Congrats text not present");
		}
	}

	/*
	 * Method: textModifyTravelerConfirm Description:text Modify Traveler
	 * Confirm Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textModifyTravelerConfirm() {
		if (verifyObjectDisplayed(confirmModifyTraveler)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModifyTravelerConfirm", Status.PASS,
					"" + getElementText(confirmModifyTraveler));
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModifyTravelerConfirm", Status.FAIL,
					"Modify Traveler confirmation not present");
		}
	}

	/*
	 * Method: textModificationSummary Description:text Modification Summary
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textModificationSummary() {
		if (verifyObjectDisplayed(textModificationSummary)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationSummary", Status.PASS,
					"Text Modification Summary Present");
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationSummary", Status.FAIL,
					"Text Modification Summary not Present");
		}
	}

	/*
	 * Method: textModificationDetails Description:text Modification Details
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textModificationDetails() {
		if (verifyObjectDisplayed(textModificationDetails)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationDetails", Status.PASS,
					"Text Modification Details Present");
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationDetails", Status.FAIL,
					"Text Modification Details not Present");
		}
	}

	/*
	 * Method: verifyModifiedName Description:verify Modified Name Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyModifiedName() {
		String modifyToOwner = testData.get("ModifyOwner");
		String modifyToGuest = testData.get("GuestFullName");
		if (getElementText(modifiedTravelerName).equals(modifyToOwner)
				|| getElementText(modifiedTravelerName).equals(modifyToGuest)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedName", Status.PASS,
					"Modified Traveler is: " + getElementText(modifiedTravelerName));
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedName", Status.FAIL,
					"Owner/Guest changed and modified traveler from screen did not match");
		}
	}

	/*
	 * Method: verifyModifiedDate Description:verify Modified Date Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyModifiedDate() {
		if (verifyObjectDisplayed(modifiedDate)) {
			if (isValidDate(getElementText(modifiedDate), "M/dd/yyyy")) {
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedDate", Status.PASS,
						"Modified date is: " + getElementText(modifiedDate));
			} else {
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedDate", Status.FAIL,
						"Modified date is not in required format");
			}
		}
	}

	/*
	 * Method: verifyModifiedBy Description:verify Modified By Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyModifiedBy() {
		if (verifyObjectDisplayed(modifiedBy)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedBy", Status.PASS,
					"Modified By: " + getElementText(modifiedBy));
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedDate", Status.FAIL,
					"Modified By field not found");
		}
	}

	/*
	 * Method: verifyReservationDetailsCTA Description:verify Reservation
	 * Details CTA Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyReservationDetailsCTA() {
		if (verifyObjectDisplayed(reservationDetailsCTA)) {
			clickElementJSWithWait(reservationDetailsCTA);
			waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
			if (verifyObjectDisplayed(sectionReservationSummary)) {
				navigateBack();
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyReservationDetailsCTA", Status.PASS,
						"Reservation Details CTA present and navigated to reservation page");
			} else {
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyReservationDetailsCTA", Status.FAIL,
						"Failed to navigate to reservation page");
			}
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyReservationDetailsCTA", Status.FAIL,
					"Reservation Details CTA not present");
		}
	}

	/*
	 * Method: verifySectionCancel Description:verify Section Cancel Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifySectionCancel() {
		waitUntilElementVisibleBy(driver, sectionCancelPolicy, "ExplicitLongWait");
		if (verifyObjectDisplayed(sectionCancelPolicy)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifySectionCancel", Status.PASS,
					"Cancel Policy Section Present");
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifySectionCancel", Status.FAIL,
					"Cancel Policy Section not Present");
		}
	}

	/*
	 * Method: verifyMakeAnotherModificationLink Description:verify Make Another
	 * Modification Link Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyMakeAnotherModificationLink() {
		waitUntilElementVisibleBy(driver, makeAnotherModification, "ExplicitLongWait");
		if (verifyObjectDisplayed(makeAnotherModification)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyMakeAnotherModificationLink", Status.PASS,
					"Make Another Modification link present");
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyMakeAnotherModificationLink", Status.FAIL,
					"Make Another Modification link not present");
		}
	}

	/*
	 * Method: verifyChargesSummarySection Description: verify Charges Summary
	 * Section Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyChargesSummarySection() {
		String paymentMethod = testData.get("PaymentBy");
		List<WebElement> allOptions = getList(chargesSummaryHeader);
		for (WebElement we : allOptions) {
			getElementInView(we);
			assertTrue(verifyObjectDisplayed(we), "Charge Summary Header not present");
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
					"Charge Summary Header present");
		}

		assertTrue(verifyObjectDisplayed(paymentMethodIcon), "Payment Icon not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Payment Icon present");

		if (paymentMethod.contains("PayPal")) {
			assertTrue(
					verifyObjectDisplayed(paypalEmailID)
							&& getElementText(paypalEmailID).trim().equals(testData.get("PaypalEmail")),
					"Paypal Email address not present");
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
					"Paypal Email address present and it is same used during the payment");

		} else if (paymentMethod.contains("CreditCard")) {
			assertTrue(verifyObjectDisplayed(paypalEmailID), "Credit Card Info not present");
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
					"Credit Card Info present : " + getElementText(paypalEmailID).trim());
		}

		assertTrue(verifyObjectDisplayed(guestCreditLabel), "Guest Confirmation Credit Label not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Guest Confirmation Credit Label present");

		assertTrue(verifyObjectDisplayed(guestCreditValue), "Guest Confirmation Credit Value  not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Guest Confirmation Credit Value present");

		assertTrue(verifyObjectDisplayed(paymentTotalLabel), "Payment Total Label not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Payment Total Label  present");

		assertTrue(verifyObjectDisplayed(paymentTotalValue), "Payment Total value not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Payment Total value present");
	}

	/*
	 * Method: totalPointsRequired Description: Total Points Required Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalPointsDeducted() {
		Assert.assertTrue(verifyObjectDisplayed(totalPointsDisplayed), "Total Points Deducted not displayed");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "totalPointsRequired", Status.PASS,
				"Total Points Deducted is displayed as " + getElementText(totalPointsDisplayed));
	}

	/*
	 * Method: verifyMembershipCharges Description: verify Membership Charges
	 * Date: Aug/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyMembershipCharges_BorrowRentPoint() {
		getElementInView(expandTotalPoints);
		clickElementBy(expandTotalPoints);
		Assert.assertTrue(
				verifyObjectDisplayed(availablePointLabel) && verifyObjectDisplayed(availablePointValue)
						&& verifyObjectDisplayed(BorrowedPointLabel) && verifyObjectDisplayed(BorrowedPointValue),
				"Available & Borrowed Point not displayed under Membership Charges");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "updatedDateValidation", Status.PASS,
				"Available & Borrowed Point displayed under Membership Charges");

		Assert.assertTrue(
				verifyObjectDisplayed(rentedPointLabel_MembershipCharges)
						&& verifyObjectDisplayed(rentedPointValue_MembershipCharges),
				"Rented Point not displayed under Membership Charges");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "updatedDateValidation", Status.PASS,
				"Rented Point displayed under Membership Charges");
	}

	
	/*
	 * Method: verifyMembershipCharges Description: verify Membership Charges
	 * Date: Aug/2020 Author: Kamalesh Changes By: NA
	 */
	public void getPointUsedForReservation(String useYear) {
		if (useYear.equalsIgnoreCase("Future")) {
			strModifyingCost = getElementText(BorrowedPointValue).trim();
			if (strModifyingCost.contains(",")) {
				strModifyingCost = strModifyingCost.replace(",", "");
			}
		} else if (useYear.equalsIgnoreCase("Current")) {
			clickElementBy(expandTotalPoints);
			strModifyingCost = getElementText(availablePointValue).trim();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (strModifyingCost.contains(",")) {
				strModifyingCost = strModifyingCost.replace(",", "");
			}
		}
	}

	/*
	 * Method: verifyMembershipCharges Description: verify Membership Charges
	 * Date: Aug/2020 Author: Kamalesh Changes By: NA
	 */
	public void getHKUsedForReservation(String useYear) {
		if (useYear.equalsIgnoreCase("Future")) {
			strModifyingCostHK = getElementText(BorrowedHKValue).trim();
			if (strModifyingCostHK.contains(",")) {
				strModifyingCostHK = strModifyingCostHK.replace(",", "");
			}
		} else if (useYear.equalsIgnoreCase("Current")) {
			clickElementBy(expandTotalPoints);
			strModifyingCostHK = getElementText(availableHKValue).trim();
			if (strModifyingCostHK.contains(",")) {
				strModifyingCostHK = strModifyingCostHK.replace(",", "");
			}
		}
	}

	/*
	 * Method: updatedDateValidation Description: Verify Date is Updated Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void updatedDateValidation(String updatedDate) {
		Assert.assertTrue(getElementText(datesModified).trim().contains(testData.get(updatedDate)),
				"Updated Date not displayed correctly");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "updatedDateValidation", Status.PASS,
				"Updated Date is displayed as " + getElementText(datesModified));
	}

	/*
	 * Method: verifyPointsProtection Description:verify Points Protection Date:
	 * Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void verifyPointsProtection() {
		/*if (verifyObjectDisplayed(pointsProtection)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPointsProtection", Status.PASS,
					"Points Protection is: " + getElementText(pointsProtection));
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPointsProtection", Status.FAIL,
					"Points Protection is not displayed");
		}*/

	}

	public void verifyPointsProtection(String Status) {
		/*if (verifyObjectDisplayed(pointsProtection)
				&& getElementText(pointsProtection).trim().equalsIgnoreCase(Status)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPointsProtection", Status.PASS,
					"Points Protection is: " + getElementText(pointsProtection));
		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPointsProtection", Status.FAIL,
					"Points Protection is not displayed");
		}*/

	}

	/*
	 * Method: verifyPaymentCharges Description: verifyPaymentCharges Date:
	 * June/2020 Author: Kamalesh Gupta Changes By: NA
	 */
		
	public void verifyPaymentCharges_RentedPoint() {
		getElementInView(paymentChargesHeader);
		assertTrue(verifyObjectDisplayed(paymentChargesHeader), "Payment Charges section  not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Payment Charges section present");

		assertTrue(verifyObjectDisplayed(rentedPointLabel) && verifyObjectDisplayed(rentedPointValue),
				"Guest Confirmation Credit not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Rented Point label and its value present");

	}

	/*
	 * Method: verifyPaymentCharges Description: verifyPaymentCharges Date:
	 * June/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void verifyPaymentCharges_HK() {
		getElementInView(paymentChargesHeader);
		assertTrue(verifyObjectDisplayed(paymentChargesHeader), "Payment Charges section  not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_HK", Status.PASS,
				"Payment Charges section present");

		assertTrue(verifyObjectDisplayed(purchaseHKCreditLabel) && verifyObjectDisplayed(purchaseHKCreditValue),
				"Purchased Housekeeping Credits label and its value not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Purchased Housekeeping Credits label and its value present");

	}

	/*
	 * Method: navigateToReservationDetailsPage Description:Navigate to
	 * reservation details page Date: Aug/2020 Author: Unnat jain Changes By: NA
	 */
	public void navigateToReservationDetailsPage() {
		getElementInView(reservationDetailsCTA);
		if (verifyObjectDisplayed(reservationDetailsCTA)) {
			clickElementJSWithWait(reservationDetailsCTA);
			waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(sectionReservationSummary),
					"Failed to navigate to reservation page");

			tcConfig.updateTestReporter("CUIModifySuccessPage", "navigateToReservationDetailsPage", Status.PASS,
					"Reservation Details CTA present and navigated to reservation page");

		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "navigateToReservationDetailsPage", Status.FAIL,
					"Reservation Details CTA not present");
		}
	}

	/**
	 * Method: checkRatePlanInDBAfterModification Description: Validate Rate
	 * Plan value in Datebase after modifications Date: July/2020 Author:
	 * Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	protected static List<String> ratePlanIdAferModification = new ArrayList<String>();

	public void checkRatePlanInDBAfterModification() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<String> roomBkgId = new ArrayList<String>();
		DBConnectionOnly();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String queryToGetRoomBkgId = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND rer.res_num in ('"
				+ testData.get("ReservationNumber") + "')";
		roomBkgId = ExecuteQueryAndFetchListColumData(queryToGetRoomBkgId, "ROOM_BKG_ID");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String queryToGetRatePlanID = "select ROOM_RATE_PLAN_ID from RESERVATION.RH_ROOM_BKG_CHNG where ROOM_BKG_ID = '"
				+ roomBkgId.get(0) + "'";
		ratePlanIdAferModification = ExecuteQueryAndFetchListColumData(queryToGetRatePlanID, "ROOM_RATE_PLAN_ID");

		closeDB();
	}

	/**
	 * Method: verifyRatePlanInDBBeforeAndAfterModification Description:
	 * Validate Rate Plan value in Datebase before and after modifications Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void verifyRatePlanInDBBeforeAndAfterModification() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		assertTrue(ratePlanIdAferModification.get(1).equals(CUIBookPage_Web.ratePlanId.get(0))
				|| ratePlanIdAferModification.get(0).equals(CUIBookPage_Web.ratePlanId.get(0)));
		assertFalse(ratePlanIdAferModification.get(0).equals(ratePlanIdAferModification.get(1)),
				"Rate Plan is not changing after modification");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyRatePlanInDBBeforeAndAfterModification",
				Status.PASS, "Rate Plan is changing after modification. After modification Rate plan value is "
						+ ratePlanIdAferModification.get(0));
	}

	/*
	 * Method: expandHousekeepingChages Description:Date: Aug/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void expandHousekeepingChages() {
		if (verifyObjectDisplayed(expandHousekeepingBreakdown)) {
			getElementInView(expandHousekeepingBreakdown);
			clickElementJSWithWait(expandHousekeepingBreakdown);
			tcConfig.updateTestReporter("CUIModifySuccessPage", "expandHousekeepingChages", Status.PASS,
					"Successfully Clicked on housekeeping expand area");

		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "expandHousekeepingChages", Status.FAIL,
					"Failed to click on housekeeping expand area");
		}
	}
	
	/*
	 * Method: expandHousekeepingChages Description:Date: Aug/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void verifyHousekeepingChagesAbsence() {
		if (verifyObjectDisplayed(expandHousekeepingBreakdown)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "expandHousekeepingChages", Status.FAIL,
					"Successfully Clicked on housekeeping expand area");

		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "expandHousekeepingChages", Status.PASS,
					"Failed to click on housekeeping expand area");
		}
	}

	/*
	 * Method: validateCurrentUseYearHKUsed Description:Date: Aug/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void validateCurrentUseYearHKUsed() {
		int valueHKCurrent = Integer.parseInt(getElementText(valueHKUsedCurrentUseYear).trim());
		if (valueHKCurrent == CUIReservationBalancePage_Web.availableHKCurrentYear) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "validateCurrentUseYearHKUsed", Status.PASS,
					"Current Use Year HK value Matched");

		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "validateCurrentUseYearHKUsed", Status.FAIL,
					"Current Use Year HK value validation failed");
		}
	}

	/*
	 * Method: validateUseYearHKPurchased Description:Date: Aug/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void validateUseYearHKBorrowed() {
		int valueHKBorrowed = Integer.parseInt(getElementText(valueHKBorrowedUseYear).trim());
		if (valueHKBorrowed == Integer.parseInt(testData.get("HouseKeepingBorrowPoints").trim())) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "validateUseYearHKBorrowed", Status.PASS,
					"Borrowed HK value Matched");

		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "validateUseYearHKBorrowed", Status.FAIL,
					"Borrowed HK value validation failed");
		}
	}

	/*
	 * Method: validateUseYearHKPurchased Description:Date: Aug/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void validateUseYearHKPurchased() {
		int valueHKPurchased = Integer.parseInt(getElementText(valueHKPurchasedUseYear).trim());
		if (valueHKPurchased == CUIReservationBalancePage_Web.purchasedHK) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "validateUseYearHKPurchased", Status.PASS,
					"Purchased HK value Matched");

		} else {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "validateUseYearHKPurchased", Status.FAIL,
					"Purchased HK value validation failed");
		}
	}

	/**
	 * Method: verifyRatePlanInDBBeforeAndAfterBenefitsModification Description:
	 * Validate Rate Plan value in Datebase before and after modifications Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void verifyRatePlanInDBBeforeAndAfterBenefitsModification() {
		assertFalse(ratePlanIdAferModification.get(0).equals(ratePlanIdAferModification.get(1)),
				"Rate Plan is not changing after modification");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyRatePlanInDBBeforeAndAfterBenefitsModification",
				Status.PASS, "Rate Plan is changing after modification. After modification Rate plan value is "
						+ ratePlanIdAferModification.get(0));
	}

	/*
	 * Method: verifyPurchaseHK Description:verify Purchase HK presence Date:
	 * Aug/2020 Author: Kamalesh Changes By: NA
	 */

	public void verifyPurchaseHK() {
		getElementInView(purchasedHKLabel);
		assertTrue(verifyObjectDisplayed(purchasedHKLabel), "Purchased HK Credit Label not present");
		assertTrue(verifyObjectDisplayed(purchasedHKValue), "Purchased HK Credit Value not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPurchaseHK", Status.PASS,
				"Purchased House Keeping Credit Label present and the value is  " + getElementText(purchasedHKValue));
	}

	/*
	 * Method: verifyTotalPayment Description:verify Total Payment presence
	 * Date: Aug/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyTotalPayment() {
		assertTrue(verifyObjectDisplayed(PaymentTotalLabel), "Payment Total Label not present");
		assertTrue(verifyObjectDisplayed(PaymentTotalvalue), "Payment Total Value not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyTotalPayment", Status.PASS,
				"Payment Total Label present and the value is  " + getElementText(PaymentTotalvalue));
	}

	/*
	 * Method: verifyTotalPayment Description:verify Total Payment presence
	 * Date: Aug/2020 Author: Kamalesh Changes By: NA
	 */


	public void verifyBorrowedHK() {
		getElementInView(hkCreditLabel);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(expandHousekeepingBreakdown);
		List<WebElement> label = getList(hkCreditLabel);
		List<WebElement> value = getList(hkCreditLabel);
		for (WebElement we_label : label) {
			Assert.assertTrue(verifyObjectDisplayed(we_label), "Label not present");
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyBorrowedHK", Status.PASS,
					"Label present as " + getElementText(we_label));
		}
		for (WebElement we_value : value) {
			Assert.assertTrue(verifyObjectDisplayed(we_value), "Value not present");
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyBorrowedHK", Status.PASS,
					"Value present as " + getElementText(we_value));
		}

	}

}
