package cui.pages;

import org.apache.log4j.Logger;

import automation.core.TestConfig;

public class CUILoanAssessmentSuccessPage_Android extends CUILoanAssessmentSuccessPage_Web {

	public static final Logger log = Logger.getLogger(CUILoanAssessmentSuccessPage_Android.class);

	public CUILoanAssessmentSuccessPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}
	
}
