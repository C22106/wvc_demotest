package cui.pages;

import org.apache.log4j.Logger;

import automation.core.TestConfig;

public class CUILoanAssessmentSuccessPage_IOS extends CUILoanAssessmentSuccessPage_Web {

	public static final Logger log = Logger.getLogger(CUILoanAssessmentSuccessPage_IOS.class);

	public CUILoanAssessmentSuccessPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}
	
}
