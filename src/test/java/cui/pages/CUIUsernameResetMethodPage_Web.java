package cui.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIUsernameResetMethodPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIUsernameResetMethodPage_Web.class);

	public CUIUsernameResetMethodPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By almostThereHeader = By.xpath("//h1[contains(.,'ALMOST THERE')]");
	protected By almostThereDescription = By.xpath("//h1[contains(.,'ALMOST THERE')]//following-sibling::div/p");
	protected By resetModalHeader = By.xpath("(//h2[contains(.,'receive your username')] | //form/div[contains(.,'receive your username')])");
	protected By contactInformation = By.xpath("//span[@class='wyn-type-disclaimer']");
	protected By displayedQuestion = By.xpath("//h3[contains(.,'Question')]/following-sibling::p");
	protected By answer1Text = By
			.xpath("//input[@id = 'question1Input']/following-sibling::span[contains(.,'Your Answer*')]");
	protected By answer2Text = By
			.xpath("//input[@id = 'question2Input']/following-sibling::span[contains(.,'Your Answer*')]");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");
	protected By emailField = By.xpath("//input[@id='emailInput']");
	protected By emailMethod = By.xpath("//label[@for = 'method_email']");
	protected By securityMethod = By.xpath("//label[@for = 'method_questions']");
	protected By memberNumberField = By.id("memberNumber");
	protected By firstNameField = By.id("firstName");
	protected By lastNameField = By.id("lastName");
	protected By contractNumberRadio = By.xpath("//input[@id = 'method_contract']");
	protected By phoneNumberRadio = By.xpath("//label[@for = 'method_phone']");
	protected By contractNumberField = By.id("contractNumber");
	protected By continueButton = By.xpath("//button[text() = 'Continue']");
	protected By resetMethodSelect = By.id("choose-reset-method");
	protected By question1Field = By.id("question1Input");
	protected By question2Field = By.id("question2Input");
	protected By errorBlock = By.xpath("//div[@class='error-block']");
	protected By twoTryError = By.xpath("//div[@class='error-block']//p[contains(.,'try again')]");
	protected By thirdTryError = By.xpath("//div[@class='error-block']//p[contains(.,'two more attempts')]");

	/*
	 * Method: selectVeriMethod Description:Select Verification methods Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verificationViaMethod() {
		// select method
		String selectMethod = testData.get("Method").trim().toUpperCase();
		// enter Security answer
		String securityAnswer = testData.get("SecurityAnswer").trim();
		// Email data
		String email = testData.get("Email").trim();
		waitUntilElementVisibleBy(driver, emailMethod,  "ExplicitLongWait");
		if (selectMethod.contains("EMAIL")) {
			selectEmailMethod();
			emailPresenceValidations(email);
		} else if (selectMethod.contains("SECURITY")) {
			selectSecurityMethod();
			List<WebElement> secQuesList = getList(displayedQuestion);
			if (secQuesList.size() == 2) {
				tcConfig.updateTestReporter("CUIUsernameResetMethod", "verificationViaMethod", Status.PASS,
						"2 Security Questions Present with Answer Fields");
				enterSecurityQuestion1(securityAnswer);
				enterSecurityQuestion2(securityAnswer);

				tcConfig.updateTestReporter("CUIUsernameResetMethod", "verificationViaMethod", Status.PASS,
						"Successfully entered the security answers and continue button is enabled");
			} else {
				tcConfig.updateTestReporter("CUIUsernameResetMethod", "verificationViaMethod", Status.FAIL,
						"Error in security questions answering");
			}

		} else {
			tcConfig.updateTestReporter("CUIUsernameResetMethod", "verificationViaMethod", Status.FAIL,
					"Please enter which method need to be selected?");
		}

	}

	/*
	 * Method: selectEmailMethod Description:Select Email Method Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void selectEmailMethod() {

		clickElementBy(emailMethod);
		tcConfig.updateTestReporter("CUIUsernameResetMethod", "verificationViaMethod", Status.PASS,
				"Method is selected as Email");

		elementPresenceVal(emailField, "Email field", "CUIUsernameResetMethod", "selectVeriMethod");

	}

	/*
	 * Method: emailPresenceValidations Description:Email Presence Validations
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void emailPresenceValidations(String emailAddress) {
		String emailValue = getElementValue(emailField).toLowerCase();

		if (emailValue.contains(".com")) {
			tcConfig.updateTestReporter("CUIUsernameResetMethod", "verificationViaMethod", Status.PASS,
					"Email Alreadey Present as: " + emailValue);
		} else {
			fieldDataEnter(emailField, emailAddress);
		}
	}

	/*
	 * Method: enterSecurityQuestion1 Description:Security Ques1 enter Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityQuestion1(String securityAnswer) {
		getElementInView(question1Field);
		clickElementBy(question1Field);
		getObject(question1Field).sendKeys(securityAnswer);
	}

	/*
	 * Method: enterSecurityQuestion2 Description:Security Ques2 enter Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityQuestion2(String securityAnswer) {
		getElementInView(question2Field);
		clickElementBy(question2Field);
		getObject(question2Field).sendKeys(securityAnswer);
	}

	/*
	 * Method: securityQuesErrorVal Description:Security Ques Error Validations
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void securityQuestionErrorValdation() {
		// Invalid Data enter
		String strInvalidCode = testData.get("invalidData");

		for (int noOfTries = 0; noOfTries < 3; noOfTries++) {
			// clearField(verificationCodeField);
			getElementInView(question1Field);
			fieldDataEnter(question1Field, strInvalidCode);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(question2Field, strInvalidCode);
			clickContinue();
			waitUntilElementVisibleBy(driver, errorBlock,  "ExplicitLowWait");
			Assert.assertTrue(verifyObjectDisplayed(errorBlock),"Error Block not present");
			if (noOfTries < 2) {
				fieldPresenceVal(twoTryError, (noOfTries + 1) + " try error ", "CUIUsernameResetMethod",
						"securityQuestionErrorValdation", getElementText(twoTryError));
			} else if (noOfTries >= 2) {
				fieldPresenceVal(thirdTryError, (noOfTries + 1) + "rd try error ", "CUIUsernameResetMethod",
						"securityQuestionErrorValdation", getElementText(thirdTryError));
			} else {
				tcConfig.updateTestReporter("CUIUsernameResetMethod", "securityQuestionErrorValdation", Status.FAIL,
						"Error Validations Failed");
			}

		}
	}

	/*
	 * Method: selectSecurityMethod Description:Select Security Ques Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void selectSecurityMethod() {
		waitUntilElementVisibleBy(driver, emailMethod,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(emailMethod),"Email Method not present");
		clickElementBy(securityMethod);
		tcConfig.updateTestReporter("CUIUsernameResetMethod", "selectSecurityMethod", Status.PASS,
				"Method is selected as Security Question");
		waitUntilElementVisibleBy(driver, question1Field,  "ExplicitLongWait");
	}

	/*
	 * Method: verificationPageVal Description: Verification Methods Page Val
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verificationPageValidation() {
		waitUntilElementVisibleBy(driver, emailMethod,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(emailMethod),"Email Method not present");
		elementPresenceVal(almostThereHeader, "Verification Method Select Page Header", "CUIUsernameResetMethod",
				"verificationPageValidation");
		elementPresenceVal(almostThereDescription, "Verification Method Selects Page Desc", "CUIUsernameResetMethod",
				"verificationPageValidation");
		elementPresenceVal(resetModalHeader, "Verification Method modal header", "CUIUsernameResetMethod",
				"verificationPageValidation");
		elementPresenceVal(emailMethod, "Email Verification Method option", "CUIUsernameResetMethod",
				"verificationPageVal");
		elementPresenceVal(securityMethod, "Sec Quest Verification Method option", "CUIUsernameResetMethod",
				"verificationPageValidation");

		elementPresenceVal(contactInformation, "Contact Info Link", "CUICreateAccountPage", "verificationPageValidation");

		elementPresenceVal(disabledContinueCTA, "Disabled continue CTA", "CUICreateAccountPage",
				"verificationPageValidation");

	}

}