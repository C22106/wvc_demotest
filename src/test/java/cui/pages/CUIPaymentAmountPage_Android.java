package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUIPaymentAmountPage_Android extends CUIPaymentAmountPage_Web {

	public static final Logger log = Logger.getLogger(CUIPaymentAmountPage_Android.class);

	public CUIPaymentAmountPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		buttonContinue = By.xpath("(//button[text() = 'Continue'])[2]");
	}

	/*
	 * Method: addAdditionalPoint Description:add Additional Point Date field
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void addAdditionalPoint() {
		String additionalAmount = testData.get("AdditionalAmount");
		clickElementJSWithWait(textAdditionalPoint);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldAdditionalPoint)) {
			getElementInView(fieldAdditionalPoint);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Additional Amount", additionalAmount, 2);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

}
