package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPersonalInformationPopUp_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIPersonalInformationPopUp_Web.class);

	public CUIPersonalInformationPopUp_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By phoneNumberHeader = By.xpath("//h5[contains(.,'PHONE NUMBER')]");
	protected By ownerAddress = By.xpath("//h5[contains(.,'ADDRESS')]/following-sibling::p");
	protected By currentUsernameField = By.id("currentUsername");
	protected By editPersonalInformation = By.xpath("//button[contains(.,'Edit Personal Info')]");
	protected By accountInfoPopUpHeader = By
			.xpath("//div[@id='personal-info-modal' and @aria-hidden='false']//p[contains(.,'Personal Info')]");
	/* Manage Personal Info Pop Up */

	protected By accountInfoNameHeader = By.xpath("//div[@id='personal-info-modal']//h5[contains(.,'NAME')]");
	protected By accountInfoNameField = By.xpath("//div[@id='personal-info-modal']//input[@id='name']");
	protected By accountInfoCallText = By.xpath("//div[@id='personal-info-modal']//p[contains(.,'please call')]");
	protected By accountInfoAddressHeader = By.xpath("//div[@id='personal-info-modal']//h5[text()='Address']");
	protected By accountInfoCountryField = By.xpath("//div[@id='personal-info-modal']//select[@id='country']");
	protected By accountInfoAddressLine1 = By.xpath("//div[@id='personal-info-modal']//input[@id='addressLine1']");
	protected By accountInfoAddressLine2 = By.xpath("//div[@id='personal-info-modal']//input[@id='addressLine2']");
	protected By accountInfoAddressLine3 = By.xpath("//div[@id='personal-info-modal']//input[@id='addressLine3']");
	protected By accountInfoCityField = By.xpath("//div[@id='personal-info-modal']//input[@id='city']");
	protected By accountInfoStateField = By.xpath("//div[@id='personal-info-modal']//select[@id='stateProvince']");
	protected By accountInfoZipField = By.xpath("//div[@id='personal-info-modal']//input[@id='zipPostalCode']");
	protected By accountInfoPhoneHeader = By.xpath("//div[@id='personal-info-modal']//h5[contains(.,'Phone Number')]");
	protected By accountInfoHomeFlag = By
			.xpath("//input[@id='countryPhoneHome']/ancestor::div[@class='country-phone']//div[@class='flag-select']");
	protected By accountInfoCellFlag = By
			.xpath("//input[@id='countryPhoneCell']/ancestor::div[@class='country-phone']//div[@class='flag-select']");
	protected By accountInfoWorkFlag = By
			.xpath("//input[@id='countryPhoneWork']/ancestor::div[@class='country-phone']//div[@class='flag-select']");
	protected By accountInfoHomeField = By.xpath("//input[@id='countryPhoneHome']");
	protected By accountInfoCellField = By.xpath("//input[@id='countryPhoneCell']");
	protected By accountInfoWorkField = By.xpath("//input[@id='countryPhoneWork']");
	protected By accountInfoEmailHeader = By.xpath("//div[@id='personal-info-modal']//h5[contains(.,'EMAIL')]");
	protected By accountInfoEmailField = By.xpath("//div[@id='personal-info-modal']//input[@id='email']");
	protected By accountInfoCancelCTA = By
			.xpath("//div[@id='personal-info-modal' and @aria-hidden='false']//div/button[contains(.,'Cancel')]");
	protected By accountInfoSaveCta = By
			.xpath("//div[@id='personal-info-modal' and @aria-hidden='false']//div/button[contains(.,'Save')]");
	protected By closedAccountInfoModal = By.xpath("//div[@id='personal-info-modal' and @aria-hidden='true']");
	protected By accountInfoCloseCta = By
			.xpath("//div[@id='personal-info-modal' and @aria-hidden='false']//button[@aria-label='Close modal']");
	protected By addressLine1Error = By.id("error-addressLine1");
	protected By emailError = By.id("error-email");

	/*
	 * Method: personalInfoModalVal Description: Navigate to Manage Personal
	 * Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void personalInformationModalNavigation() {
		waitUntilElementVisibleBy(driver, editPersonalInformation, "ExplicitLongWait");

		getElementInView(editPersonalInformation);
		clickElementBy(editPersonalInformation);

		waitUntilElementVisibleBy(driver, accountInfoPopUpHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(accountInfoPopUpHeader), "Account Info pop up header not present");
		elementPresenceVal(accountInfoPopUpHeader, "Personal Info Pop Up Modal with header", "CUIAccountSettingPage",
				"personalInformationModalNavigation");
	}

	/*
	 * Method: saveAccountInfoChanges Description: Save Account Info Changes Per
	 * Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void saveAccountInformationChanges() {
		clickElementBy(accountInfoSaveCta);

		waitUntilElementInVisible(driver, accountInfoSaveCta, "ExplicitLongWait");

		sendKeyboardKeys(Keys.TAB);
		if (verifyObjectDisplayed(accountInfoAddressLine1)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveAccountInformationChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveAccountInformationChanges", Status.PASS,
					"Pop Up Modal  Closed");
			savedChangesValidation(ownerAddress, "Address", "Personal Information");
		}
	}

	/*
	 * Method: editEmailField Description: Edit Email Field in Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void editEmailField() {
		enterDataInPopUpField(accountInfoEmailField, testData.get("Email"), "No");
	}

	/*
	 * Method: editAddress1Field Description: Edit Address Field in Per Info Pop
	 * Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void editAddress1Field() {
		enterDataInPopUpField(accountInfoAddressLine1, testData.get("Address"), "Yes");
	}

	/*
	 * Method: invalidEmailField Description: Invalid data in Email Field in Per
	 * Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidEmailField() {
		enterDataInPopUpField(accountInfoEmailField, testData.get("invalidEmail"), "No");
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: invalidAddress1Field Description: Invalid data Address Field in
	 * Per Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidAddress1Field() {
		enterDataInPopUpField(accountInfoAddressLine1, testData.get("invalidData"), "Yes");
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: invalidEmailError Description: Invalid data Email Field in Per
	 * Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidEmailError() {
		waitUntilElementVisibleBy(driver, emailError, "ExplicitLongWait");
		if (verifyObjectDisplayed(emailError)) {
			tcConfig.updateTestReporter("CUIPersonalInformationPage", "ivalidEmailError", Status.PASS,
					"Invalid Email Error displayed");
		} else {
			tcConfig.updateTestReporter("CUIPersonalInformationPage", "ivalidEmailError", Status.FAIL,
					"Invalid Email Error did not get displayed");
		}
	}

	/*
	 * Method: invalidAddressError Description: Invalid data Address 1Field in
	 * Per Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidAddressError() {
		waitUntilElementVisibleBy(driver, addressLine1Error, "ExplicitLongWait");
		if (verifyObjectDisplayed(addressLine1Error)) {
			tcConfig.updateTestReporter("CUIPersonalInformationPage", "invalidAddressError", Status.PASS,
					"Invalid Address Error displayed");
		} else {
			tcConfig.updateTestReporter("CUIPersonalInformationPage", "invalidAddressError", Status.FAIL,
					"Invalid Address Error did not get displayed");
		}
	}

	/*
	 * 
	 * /* Method: personalInfoCancelCTA Description: Close Manage Per Info Pop
	 * Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void personalInformationCancelCTA() {

		closePopUpModal(accountInfoCancelCTA, closedAccountInfoModal, accountInfoNameField, Status.FAIL);
	}

	/*
	 * Method: personalInfoModalVal Description: Validate Manage Personal Info
	 * Pop Up Account Setting Page Date: MAr/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void personalInformationModalValidation() {

		elementPresenceVal(accountInfoNameHeader, "Name Header ", "CUIAccountSettingPage", "personalInfoModalVal");
		// User first name and last name
		String userFullName = getElementValue(accountInfoNameField).toUpperCase();
		// user firs name
		String userFirstName = testData.get("FirstName").toUpperCase();
		if (userFullName.contains(userFirstName)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "personalInformationModalValidation", Status.PASS,
					"Name field present with value: " + getElementValue(currentUsernameField));
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "personalInformationModalValidation", Status.FAIL,
					"Name field validation error ");
		}

		elementPresenceVal(accountInfoCallText, "Help Desk Text and Number ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoAddressHeader, "Address Header", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoCountryField, "Country Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoAddressLine1, "Address Line 1 Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoAddressLine2, "Address Line 2 Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoAddressLine3, "Address Line 3 Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoCityField, "City Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoStateField, "State Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoZipField, "Zip Code Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoPhoneHeader, "Phone Number Header ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoHomeFlag, "Home Flag and Field", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoWorkFlag, "Work Flag and Field", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoCellFlag, "Cell Flag and Field", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoEmailHeader, "Email Header ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoEmailField, "Email Field ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoSaveCta, "Save CTA ", "CUIAccountSettingPage",
				"personalInformationModalValidation");
		elementPresenceVal(accountInfoCancelCTA, "Cancel CTA", "CUIAccountSettingPage",
				"personalInformationModalValidation");

		closePopUpModal(accountInfoCloseCta, closedAccountInfoModal, accountInfoNameField, Status.PASS);

	}

}
