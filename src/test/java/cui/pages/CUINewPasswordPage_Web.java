package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUINewPasswordPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUINewPasswordPage_Web.class);

	public CUINewPasswordPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	// New Passwrod Page
	protected By newPasswordPageHeader = By.xpath("//h1[contains(.,'SET YOUR NEW PASSWORD')]");
	protected By newPasswordDescription = By
			.xpath("//h1[contains(.,'SET YOUR NEW PASSWORD')]/following-sibling::div/p");
	protected By newPassModalHeader = By
			.xpath("(//h2[contains(.,'Enter Your New Password')] | //form/div[contains(.,'Enter Your New Password')])");
	protected By newPasswordField = By.id("newPassword");
	protected By showCTA = By.xpath("// a[contains(.,'show')]");
	protected By hideCTA = By.xpath("// a[contains(.,'hide')]");
	protected By passwordResetHeader = By.xpath("//div[@class='title-1' and contains(.,'Password Reset')]");
	protected By loginNowCTA = By.xpath("//a[contains(.,'Login Now')]");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");

	/*
	 * Method: newPasswordDataEnter Description: New Password Page Val Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newPasswordDataEnter() {
		waitUntilElementVisibleBy(driver, newPasswordField, "ExplicitLongWait");
		getElementInView(newPasswordField);

		fieldDataEnter(newPasswordField, testData.get("Password"));

	}

	/*
	 * Method: showPasswordCTA Description: Show Cta Validations Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void showPasswordCTA() {
		if (verifyObjectDisplayed(showCTA)) {
			tcConfig.updateTestReporter("CUINewPasswordPage", "showPasswordCTA", Status.PASS,
					"Show CTA is present in password field");
			clickElementBy(showCTA);
			hidePasswordCTA();
		} else {
			tcConfig.updateTestReporter("CUINewPasswordPage", "showPasswordCTA", Status.FAIL,
					"Show CTA is not present in password field");
		}

	}

	/*
	 * Method: hidePasswordCTA Description: Hide Cta Validations Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void hidePasswordCTA() {

		waitUntilElementVisibleBy(driver, hideCTA, "ExplicitLowWait");
		if (verifyObjectDisplayed(hideCTA)) {
			tcConfig.updateTestReporter("CUINewPasswordPage", "hidePasswordCTA", Status.PASS,
					"Hide CTA is displayed in password field");
		} else {
			tcConfig.updateTestReporter("CUINewPasswordPage", "hidePasswordCTA", Status.FAIL,
					"Hide CTA is not displayed in password field");
		}

	}

	/*
	 * Method: passwordResetPage Description: Password reset Success Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordResetPage() {

		waitUntilElementVisibleBy(driver, passwordResetHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(passwordResetHeader),"Password Reset header not present");
		tcConfig.updateTestReporter("CUIForgotPassword", "passwordResetPage", Status.PASS,
				"Password Reset Successful and header present");
		clickElementBy(loginNowCTA);
	}

	/*
	 * Method: newPasswordPageValidation Description: New Password Page Val
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newPasswordPageValidation() {
		waitUntilElementVisibleBy(driver, newPasswordField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(newPasswordField),"New Password field not present");
		elementPresenceVal(newPasswordPageHeader, "New Password Page Header", "CUINewPasswordPage",
				"newPasswordPageValidation");
		elementPresenceVal(newPasswordDescription, "New Password Page Description", "CUINewPasswordPage",
				"newPasswordPageValidation");
		elementPresenceVal(newPassModalHeader, "New password modal header", "CUINewPasswordPage",
				"newPasswordPageValidation");
		fieldPresenceVal(newPasswordField, "New Password Field", "CUINewPasswordPage", "newPasswordPageValidation", "");
		elementPresenceVal(disabledContinueCTA, "Disabled continue CTA", "CUINewPasswordPage", "CUINewPasswordPage");

	}

}