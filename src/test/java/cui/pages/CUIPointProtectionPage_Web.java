package cui.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPointProtectionPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIPointProtectionPage_Web.class);

	public CUIPointProtectionPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By bookingHeader = By.xpath("//div[@class='booking-header__desktop']//li[@class='step -active']/span");
	protected By header = By.xpath("//h1[contains(@class,'title-2 text')]");

	protected By backButtonToPreviuosPage = By.xpath("//div[@class='booking__footer']//a[contains(text(),'Back to')]");
	// protected CUIBookPage_Web bookPage = new CUIBookPage_Web(tcConfig);
	protected By buttonContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By buttonBookingContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By pointProtectionPage = By.xpath("//div[@class='points-protection-page']");
	protected By headerPointProtection = By.xpath(
			"(//h1[contains(.,'Points Protection')] | //div[@class = 'steps__title' and contains(.,'Points Protection')])");
	protected By introductorytextPointProtection = By
			.xpath("//h2[contains(.,'protect your points with Points Protection')]");
	protected By titleVacationProtect = By.xpath("//h1[contains(.,'PROTECT YOUR VACATION')]");
	protected By explanatorytextPointProtection = By.xpath("//p[contains(@class,'points-protection-description')]");
	protected By instructionaltextPointProtection = By
			.xpath("//h4[contains(text(),'Would you like to add Points Protection to this reservation')]");
	protected By yesPointProtection = By
			.xpath("//label[contains(text(),'I would like to add Points Protection to my reservation')]");
	protected By noPointProtection = By
			.xpath("//label[contains(text(),'I do not want to add Points Protection at this time')]");
	protected By iconToolTip = By.xpath("//span[@id = 'points_protection_tier_pricing']");
	protected By pointProtectionTirePricing = By.xpath("//th[text() = 'POINTS PROTECTION TIER PRICING']");
	protected By toolTipPricing = By.xpath("//div[contains(@class,'tooltip')]/table[@id]/tbody/tr");
	protected By ppChargesDetails = By.xpath("//span[@id='points_protection_tier_pricing']/following-sibling::span");
	// updated
	protected By textTravelInfo = By.xpath("//div[contains(@class,'steps__title') and contains(.,'Traveler Info')]");

	/*
	 * Method: selectPointProtection Description:Select Point Protection option
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectPointProtection() {
		/*
		 * String needPointProtection = testData.get("NeedPointProtection");
		 * waitUntilElementVisibleBy(driver, headerPointProtection,
		 * "ExplicitLowWait");
		 * Assert.assertTrue(verifyObjectDisplayed(headerPointProtection),
		 * "Points Protection header not present"); if
		 * (needPointProtection.equalsIgnoreCase("YES")) {
		 * getElementInView(yesPointProtection);
		 * clickElementJSWithWait(yesPointProtection);
		 * tcConfig.updateTestReporter("CUIBookPage", "selectPointProtection",
		 * Status.PASS, "Point Protection selected"); } else {
		 * getElementInView(noPointProtection);
		 * clickElementJSWithWait(noPointProtection);
		 * tcConfig.updateTestReporter("CUIBookPage", "selectPointProtection",
		 * Status.PASS, " Point Protection declined"); }
		 */

	}

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		/*
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * getElementInView(buttonBookingContinue);
		 * clickElementJSWithWait(buttonBookingContinue);
		 */
	}

	/*
	 * Method: verifyPointProtectionPageNotDisplayed Description:Select Point
	 * Protection option Date field Date: June/2020 Author: Kamalesh Roy Changes
	 * By: NA
	 */
	public void verifyPointProtectionPageDisplayed(String flag) {
		getElementInView(header);
		List<String> headerValue = getListString(bookingHeader);
		switch (flag) {
		case "Yes":
			Assert.assertTrue(verifyObjectDisplayed(headerPointProtection), "Point Protection Page not displayed");
			tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionPageNotDisplayed", Status.PASS,
					"Point Protection Page displayed");

			if (testData.get("User_Type").equalsIgnoreCase("Non-VIP")) {
				if (headerValue.contains("POINTS PROTECTION")
						&& getElementText(headerPointProtection).contains("2. POINTS PROTECTION")) {
					tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionPageDisplayed", Status.PASS,
							"The booking header is displayed as " + headerValue + " and the page header is "
									+ getElementText(headerPointProtection));

				} else {
					tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionPageDisplayed", Status.FAIL,
							headerValue + " page not displayed");
				}
			} else if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				if (headerValue.contains("POINTS PROTECTION")
						&& getElementText(headerPointProtection).contains("3. POINTS PROTECTION")) {
					tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionPageDisplayed", Status.PASS,
							"The booking header is displayed as " + headerValue + " and the page header is "
									+ getElementText(headerPointProtection));

				} else {
					tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionPageDisplayed", Status.FAIL,
							headerValue + " page not displayed");
				}
			}

			break;

		case "No":
			Assert.assertFalse(verifyObjectDisplayed(headerPointProtection), "Point Protection Page displayed");
			tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionPageNotDisplayed", Status.PASS,
					"Point Protection Page not displayed");
			break;
		}

	}

	/*
	 * Method: verifyReturnCTA Description:Navigating back to Previuos page and
	 * again return back Date field Date: June/2020 Author: Kamalesh Changes By:
	 * NA
	 */
	public void verifyReturnCTA() {
		Assert.assertTrue(verifyObjectDisplayed(backButtonToPreviuosPage), "Back button not present");
		getElementInView(backButtonToPreviuosPage);
		tcConfig.updateTestReporter("CUIBookPage", "backToPreviuosPage", Status.PASS, "Back button present");
		clickElementBy(backButtonToPreviuosPage);
		waitUntilElementVisibleBy(driver, textTravelInfo, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textTravelInfo), "Traveler Info page not present");
		tcConfig.updateTestReporter("CUIBookPage", "backToPreviuosPage", Status.PASS,
				"User navigated to previuos page");
		clickContinueButton(buttonContinue, "CUITravellerInformationPage");
		tcConfig.updateTestReporter("CUIBookPage", "backToPreviuosPage", Status.PASS,
				"User navigated  back to Point Protection page");
	}

	/*
	 * Method: verifyDetailsinPointProtection Description:Verify details in
	 * Point Protection Page Date field Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyDetailsinPointProtection() {
		waitUntilElementVisibleBy(driver, pointProtectionPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(pointProtectionPage), "Points protection page navigation failed");
		elementPresenceVal(headerPointProtection, "Point Protection Header", "CUIBookPage",
				"verifyDetailsinPointProtection");
		elementPresenceVal(introductorytextPointProtection,
				"Intoductory text : " + getElementText(introductorytextPointProtection), "CUIBookPage",
				"verifyDetailsinPointProtection");
		elementPresenceVal(titleVacationProtect, "Title Protect your vacation", "CUIBookPage",
				"verifyDetailsinPointProtection");
		elementPresenceVal(explanatorytextPointProtection,
				"Point Protection Description : " + getElementText(explanatorytextPointProtection), "CUIBookPage",
				"verifyDetailsinPointProtection");
		elementPresenceVal(yesPointProtection, "Yes Point Protection text", "CUIBookPage",
				"verifyDetailsinPointProtection");
		elementPresenceVal(noPointProtection, "No Point Protection text", "CUIBookPage",
				"verifyDetailsinPointProtection");
	}

	/*
	 * Method: verifyPointProtectionToolTip
	 * Description:verifyPointProtectionToolTip Date: June/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	public void verifyPointProtectionToolTip() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(iconToolTip)) {
			clickElementJSWithWait(iconToolTip);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionToolTip", Status.PASS,
					"Successfully clicked on ToolTip Icon");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "verifyPointProtectionToolTip", Status.FAIL,
					"Tool Tip Icon for Point Protection not present");
		}
	}

	/*
	 * Method: verifyToolTipContent Description: verifyToolTipContent Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyToolTipContent() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(pointProtectionTirePricing)) {
			tcConfig.updateTestReporter("CUIBookPage", "verifyToolTipContent", Status.PASS,
					"Header Point Protection Tire Pricing Header is present in Tool Tip Content");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "verifyToolTipContent", Status.FAIL,
					"Header Point Protection Tire Pricing Header is not present in Tool Tip Content");
		}
		if (verifyObjectDisplayed(toolTipPricing)) {
			for (int tooltippricingiterator = 1; tooltippricingiterator <= getList(toolTipPricing)
					.size(); tooltippricingiterator++) {
				WebElement pointProtectionRange = getObject(
						By.xpath("((//div[contains(@class,'tooltip')]/table[@id]/tbody/tr)[" + tooltippricingiterator
								+ "]/td)[1]"));
				WebElement pointProtectionPrice = getObject(
						By.xpath("((//div[contains(@class,'tooltip')]/table[@id]/tbody/tr)[" + tooltippricingiterator
								+ "]/td)[2]"));
				tcConfig.updateTestReporter("CUIBookPage", "verifyToolTipContent", Status.PASS, "For Range "
						+ pointProtectionRange.getText() + " Amount Reqired is : " + pointProtectionPrice.getText());
			}
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "verifyToolTipContent", Status.FAIL,
					"Tool Tip Pricing Area not Present");
		}
	}

	/*
	 * Method: checkPointProtectionContinueButtonEnable Description:Check
	 * Continue button Enabled in Point Protection Page Date field Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkPointProtectionContinueButtonEnable() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(buttonBookingContinue);
		if (verifyObjectEnabled(buttonBookingContinue)) {
			tcConfig.updateTestReporter("CUIBookPage", "detailsReservationBalance", Status.PASS,
					"Continue button enabled as yes point protection radio button is selectde by deafult");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "detailsReservationBalance", Status.FAIL,
					"Continue button not enabled, maybe yes point protection radio not selected by default please check");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(noPointProtection);
		if (verifyObjectEnabled(buttonBookingContinue)) {
			tcConfig.updateTestReporter("CUIBookPage", "detailsReservationBalance", Status.PASS,
					"Continue button enabled as no point protection radio button is selected");
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "detailsReservationBalance", Status.PASS,
					"Continue button not enabled even no point protection radio button is selected");
		}
	}

	/*
	 * Method: clickContinueBooking Description: clickContinueBooking Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueBooking() {
		clickContinueButton(buttonContinue, "CUIBookPage");
	}

	/*
	 * Method: validatePointProtectionTier Description: Date: July/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */

	public void validatePointProtectionTier(String tier) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(noPointProtection)) {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(yesPointProtection)) {
				clickElementJSWithWait(yesPointProtection);
				tcConfig.updateTestReporter("pointProtectionPage", "validatePointProtection", Status.PASS,
						"Yes Radio Button for Point Protection is selected");
				waitUntilElementVisibleBy(driver, ppChargesDetails, "ExplicitLongWait");
				Assert.assertTrue(getElementText(ppChargesDetails).trim().equals(tier),
						"Point Protection charges doesn't match the expected tier which should be : " + tier);
				tcConfig.updateTestReporter("pointProtectionPage", "validatePointProtection", Status.PASS,
						"Point Protection Tier selected is " + getElementText(ppChargesDetails));
				clickContinueButton(buttonContinue, "CUIBookPage");
			} else {
				tcConfig.updateTestReporter("pointProtectionPage", "validatePointProtection", Status.FAIL,
						"Yes Radio Button for Point Protection is not selected");
			}
		} else {
			tcConfig.updateTestReporter("pointProtectionPage", "validatePointProtection", Status.FAIL,
					"Point Protection Area not displayed");
		}

	}

}