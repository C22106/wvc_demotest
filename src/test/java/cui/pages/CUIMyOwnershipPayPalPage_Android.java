package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIMyOwnershipPayPalPage_Android extends CUIMyOwnershipPayPalPage_Web {

	public static final Logger log = Logger.getLogger(CUIMyOwnershipPayPalPage_Android.class);

	public CUIMyOwnershipPayPalPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		buttonContinue = By.xpath("(//button[text() = 'Continue'])[2]");
	}
	/*
	 * Method: verifyCurrentDueInContractCard Description:verify Current Due In
	 * Contract Card Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void verifyCurrentDueInContractCard() {
		for (int iteratorCurrentDue = 1; iteratorCurrentDue <= getList(totalContract).size(); iteratorCurrentDue++) {
			String contractNumber = getObject(By
					.xpath("(//input[@name = 'contract-multiple']/parent::div/label/p/span[contains(@class,'body-1')])["
							+ iteratorCurrentDue + "]")).getText().trim();
			WebElement textCurrentDue = getObject(By.xpath("((//input[@name = 'contract-multiple']/..)["
							+ iteratorCurrentDue + "]/label/p[contains(@class,'hide-for-medium') ])//span[contains(text(),'$')]"));
			if (verifyObjectDisplayed(textCurrentDue)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.PASS,
						"Current Due for contract: " + contractNumber + " is: " + getElementText(textCurrentDue));
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.FAIL, "Current Due for contract: " + contractNumber + " is not displayed");
			}
		}
	}
	
	/*
	 * Method: verifyDueDateInContractCard Description:verify Due Date In
	 * Contract Card Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void verifyDueDateInContractCard() {
		for (int iteratorCurrentDue = 1; iteratorCurrentDue <= getList(totalContract).size(); iteratorCurrentDue++) {
			String contractNumber = getObject(By
					.xpath("(//input[@name = 'contract-multiple']/parent::div/label/p/span[contains(@class,'body-1')])["
							+ iteratorCurrentDue + "]")).getText().trim();
			WebElement textDateDue = getObject(By.xpath("(//input[@name = 'contract-multiple']/..)["
					+ iteratorCurrentDue
					+ "]//span[contains(text(),'Due Date')]/parent::p[contains(@class,'hide')]/span[contains(text(),'Due Date')]/following-sibling::span[1]"));
			if (verifyObjectDisplayed(textDateDue)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.PASS,
						"Due Date for contract: " + contractNumber + " is: " + getElementText(textDateDue));
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.FAIL, "Due Date for contract: " + contractNumber + " is not displayed");
			}
		}
	}
}
