package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIAccountSettingPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIAccountSettingPage_Web.class);

	public CUIAccountSettingPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	/* Account Profile Elements */
	protected By accountProfileLink = By.xpath("//div[@class='accountNavigation']//a[contains(.,'Profile')]");
	protected By accountSettingsHeader = By.xpath("//h1[contains(.,'ACCOUNT SETTING')]");

	/* Personal Information */
	protected By personalInformationHeader = By.xpath("//h2[contains(.,'PERSONAL INFOR')]");
	protected By phoneNumberHeader = By.xpath("//h5[contains(.,'PHONE NUMBER')]");
	protected By editPersonalInformation = By.xpath("//button[contains(.,'Edit Personal Info')]");
	protected By memberName = By.xpath("//h5[contains(.,'MEMBER') or contains(.,'OWNER')]/following-sibling::p");
	protected By ownerName = By.xpath("//h5[contains(.,'OWNER')]/following-sibling::p");
	protected By ownerEmail = By.xpath("//h5[contains(.,'EMAIL')]/following-sibling::p");
	protected By ownerAddress = By.xpath("//h5[contains(.,'ADDRESS')]/following-sibling::p");
	protected By homePhoneNumber = By.xpath(
			"(//h6[contains(.,'Home')]/following-sibling::p | //h6[contains(.,'Home')]/following-sibling::button)");
	protected By cellPhoneNumber = By.xpath(
			"(//h6[contains(.,'Cell')]/following-sibling::p | //h6[contains(.,'Cell')]/following-sibling::button)");
	protected By workPhoneNumber = By.xpath(
			"(//h6[contains(.,'Work')]/following-sibling::p | //h6[contains(.,'Work')]/following-sibling::button)");

	/* Manage Username */
	protected By manageUsernameHeader = By.xpath("//h2[contains(.,'MANAGE USERNAME')]");
	protected By editUsernameLink = By.xpath("//button[contains(.,'Edit Username')]");
	protected By ownerUserName = By.xpath("//h5[contains(.,'USERNAME')]/following-sibling::p");

	/* Manage Password */
	protected By managePasswordHeader = By.xpath("//h2[contains(.,'MANAGE PASSWORD')]");
	protected By editPasswordLink = By.xpath("//button[contains(.,'Edit Password')]");
	protected By ownerPassword = By.xpath("//h5[contains(.,'PASSWORD')]/following-sibling::p");

	/* Manage Security Ques */
	protected By securityQuestionHeader = By.xpath("//h2[contains(.,'SECURITY QUES')]");
	protected By editSecurityLink = By.xpath("//button[contains(.,'Edit Question')]");
	protected By securityQuestion1 = By.xpath("//h5[contains(.,'QUESTION 1')]/following-sibling::p");
	protected By securityQuestion2 = By.xpath("//h5[contains(.,'Question 2')]/following-sibling::p");
	protected By securityQuestion3 = By.xpath("//h5[contains(.,'Question 3')]/following-sibling::p");

	/* Profile Photo */
	protected By profilePhotoSpace = By.xpath(
			"//div[@class='personal-info']/parent::div/following-sibling::div//div[@class='manage-profile-photo']");
	protected By noImageInitials = By.xpath(
			"//div[@class='personal-info']/parent::div/following-sibling::div//div[@class='manage-profile-photo']//h6");
	protected By imageUploaded = By.xpath(
			"//div[@class='personal-info']/parent::div/following-sibling::div//div[@class='manage-profile-photo']//img");
	protected By uploadPhotoCTA = By.xpath(
			"//div[@class='personal-info']/parent::div/following-sibling::div//div[@class='manage-profile-photo']//span");

	public static String strMemberName;
	public static String strOwnerEmail;

	/*
	 * Method: acountSettingsNavigation Description: Navigate to Account Setting
	 * Page Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void accountSettingsNavigation() {

		waitUntilElementVisibleBy(driver, accountProfileLink, "ExplicitLongWait");
		// clickElementBy(accountProfileLink);
		clickElementJSWithWait(accountProfileLink);
		waitUntilElementVisibleBy(driver, accountSettingsHeader, "ExplicitLongWait");

		Assert.assertTrue(verifyObjectDisplayed(accountSettingsHeader), "Account Setting Header not displayed");
		tcConfig.updateTestReporter("CUIAccountProfilePage", "accountSettingsNavigation", Status.PASS,
				"Account Settings page navigated and header is present");

	}

	/*
	 * Method: personalInfoSection Description: Validate Personal Info section
	 * in Account Setting Page Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void personalInformationSection() {

		waitUntilElementVisibleBy(driver, personalInformationHeader, "ExplicitLongWait");

		Assert.assertTrue(verifyObjectDisplayed(personalInformationHeader), "Personal Info Header not displayed");
		tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInformationSection", Status.PASS,
				"Personal Information header is present");

		if (verifyObjectDisplayed(editPersonalInformation)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInformationSection", Status.PASS,
					"Edit Personal Information link is present");
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInformationSection", Status.FAIL,
					"Edit Personal Information link not present");
		}

	}

	/*
	 * Method: personalInfoNameSec Description: Validate Personal Info Name
	 * section in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void personalInformationNameSection() {
		if (testData.get("User_Type").contains("Discovery") || testData.get("User_Type").contains("ClubGo")) {
			memberTextValidations();

		} else {
			ownerTextValidations();
		}
	}

	/*
	 * Method: personalInfoNameSec Description: Validate Personal Info Name
	 * section in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void personalInformationNameSectionSmoke() {
		if (verifyObjectDisplayed(memberName)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.PASS,
					"Member name header present and  displayed as: " + getElementText(memberName));

		} else if (verifyObjectDisplayed(ownerName)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.PASS,
					"Onwer name header present and  displayed as: " + getElementText(ownerName));
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSectionSmoke", Status.FAIL,
					"User name not present");
		}
	}

	/*
	 * Method: memberTextValidations Description: Validate Personal Info Name
	 * section in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void memberTextValidations() {
		// member name data
		String strMemberName = getElementText(memberName).trim().toUpperCase();
		if (verifyObjectDisplayed(memberName)) {
			// temporary name
			String tempName = testData.get("FirstName").concat(" " + testData.get("LastName"));
			tempName = tempName.toUpperCase();

			if (strMemberName.contains(tempName)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.PASS,
						"Member name header present and  displayed correctly: " + strMemberName);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.FAIL,
						"Member Name Displayed is not correct " + strMemberName);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.FAIL,
					"Member name not present");
		}
	}

	/*
	 * Method: ownerTextValidations Description: Validate Personal Info Name
	 * section in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void ownerTextValidations() {
		// owner Name data
		String strOwnerName = getElementText(ownerName).trim().toUpperCase();
		if (verifyObjectDisplayed(ownerName)) {
			String tempName = testData.get("FirstName").concat(" " + testData.get("LastName"));
			// temporary name
			tempName = tempName.toUpperCase();

			if (strOwnerName.contains(tempName)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.PASS,
						"Owner name header present and  displayed correctly: " + strOwnerName);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.FAIL,
						"Owner Name Displayed is not correct " + strOwnerName);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoNameSec", Status.FAIL,
					"Owner name not present");
		}

	}

	/*
	 * Method: personalInfoEmailSec Description: Validate Personal Info Email
	 * section in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void personalInformationEmailSection() {
		// Email Data
		String strOwnerEmail = getElementText(ownerEmail).trim().toUpperCase();
		if (verifyObjectDisplayed(ownerEmail)) {
			// temp Email data
			String tempEmail = testData.get("Email");
			tempEmail = tempEmail.toUpperCase();

			if (strOwnerEmail.contains(tempEmail)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoEmailSec", Status.PASS,
						"Owner Email header present and  displayed correctly: " + strOwnerEmail);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoEmailSec", Status.FAIL,
						"Owner Email Displayed is not correct " + strOwnerEmail);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoSectionVal", Status.FAIL,
					"Owner Email not present");
		}

	}

	/*
	 * Method: personalInfoEmailSectionSmoke Description: Validate Personal Info
	 * Email section in Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void personalInformationEmailSectionSmoke() {
		// Email Data
		if (verifyObjectDisplayed(ownerEmail)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoEmailSec", Status.PASS,
					"Owner Email header present and  displayed as: " + getElementText(ownerEmail));

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoSectionVal", Status.FAIL,
					"Owner Email not present");
		}

	}

	/*
	 * Method: personalInfoAddressSectionSmoke Description: Validate Personal
	 * Info Address section in Account Setting Page Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void personalInformationAddressSectionSmoke() {
		// Email Data
		if (verifyObjectDisplayed(ownerAddress)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoEmailSec", Status.PASS,
					"Owner Address header present and  displayed as: " + getElementText(ownerAddress));

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoSectionVal", Status.FAIL,
					"Owner Address not present");
		}

	}

	/*
	 * Method: personalInfoAddressSec Description: Validate Personal Info
	 * Address section in Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void personalInformationAddressSection() {
		// owner address data
		String strOwnerAddress = getElementText(ownerAddress).trim().toUpperCase();
		if (verifyObjectDisplayed(ownerAddress)) {
			// Temporary Address Data
			String tempAdd = testData.get("Address");
			tempAdd = tempAdd.toUpperCase();

			if (strOwnerAddress.contains(tempAdd)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoSectionVal", Status.PASS,
						"Owner Address header present and  displayed correctly: " + strOwnerAddress);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoSectionVal", Status.FAIL,
						"Owner Address Displayed is not correct " + strOwnerAddress);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoSectionVal", Status.FAIL,
					"Owner Address not present");
		}

	}

	/*
	 * Method: personalInfoPhoneSec Description: Validate Personal Info Phone
	 * section in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void personalInformationPhoneSection() {
		// Cell Phone number data
		String strOwnerCell = getElementText(cellPhoneNumber).trim().replace("-", "");
		String tempCell = testData.get("Cell Ph No");
		// Work phone number data
		String strOwnerWork = getElementText(workPhoneNumber).trim().replace("-", "");
		String tempWork = testData.get("Work Ph No");
		// Home Phone Number data
		String strOwnerHome = getElementText(homePhoneNumber).trim().replace("-", "");
		String tempHome = testData.get("Home Ph No");

		if (verifyObjectDisplayed(phoneNumberHeader)) {

			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.PASS,
					"Owner phone number header present");
			cellPhoneNumber(strOwnerCell, tempCell);
			workPhoneNumber(strOwnerWork, tempWork);
			homePhoneNumber(strOwnerHome, tempHome);

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.FAIL,
					"Owner phone number not present");
		}

	}

	/*
	 * Method: personalInfoPhoneSectionSmoke Description: Validate Personal Info
	 * Phone section in Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void personalInformationPhoneSectionSmoke() {
		if (verifyObjectDisplayed(phoneNumberHeader)) {

			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.PASS,
					"Owner phone number header present");
			fieldPresenceVal(cellPhoneNumber, "Cell Phone Number ", "CUIAccountProfilePage", "personalInfoPhoneSec",
					"as " + getElementText(cellPhoneNumber));

			fieldPresenceVal(workPhoneNumber, "Work Phone Number ", "CUIAccountProfilePage", "personalInfoPhoneSec",
					"as " + getElementText(workPhoneNumber));
			fieldPresenceVal(homePhoneNumber, "Home Phone Number ", "CUIAccountProfilePage", "personalInfoPhoneSec",
					"as " + getElementText(homePhoneNumber));
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.FAIL,
					"Owner phone number not present");
		}
	}

	/*
	 * Method: cellPhoneNumber Description: Validate Personal Info Phone section
	 * in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cellPhoneNumber(String strOwnerCell, String tempCell) {
		if (strOwnerCell.contains(tempCell)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.PASS,
					"Owner Cell Phone Number header present and  displayed correctly: " + strOwnerCell);
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.FAIL,
					"Owner Cell Phone Number Displayed is not correct " + strOwnerCell);
		}

	}

	/*
	 * Method: workPhoneNumber Description: Validate Personal Info Phone section
	 * in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void workPhoneNumber(String strOwnerWork, String tempWork) {
		if (strOwnerWork.contains(tempWork)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.PASS,
					"Owner Work Phone Number header present and  displayed correctly: " + strOwnerWork);
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.FAIL,
					"Owner Work Phone Number Displayed is not correct " + strOwnerWork);
		}
	}

	/*
	 * Method: homePhoneNumber Description: Validate Personal Info Phone section
	 * in Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void homePhoneNumber(String strOwnerHome, String tempHome) {
		if (strOwnerHome.contains(tempHome)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.PASS,
					"Owner Home Phone Number header present and  displayed correctly: " + strOwnerHome);
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "personalInfoPhoneSec", Status.FAIL,
					"Owner Home Phone Number Displayed is not correct " + strOwnerHome);
		}
	}

	/*
	 * Method: ownerProfilePicVal Description: Validate Owner Profile pic
	 * section in Account Setting Page Date: Feb/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void ownerProfilePicture() {

		waitUntilElementVisibleBy(driver, profilePhotoSpace, "ExplicitLongWait");
		if (verifyObjectDisplayed(profilePhotoSpace)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerProfilePicVal", Status.PASS,
					"Profile Picture space is present in the page");
			elementPresenceVal(uploadPhotoCTA, " Upload Photo Link ", "CUIAccountProfilePage", "ownerProfilePicVal");

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerProfilePicVal", Status.FAIL,
					"Profile Picture space is not present in the page");
		}

	}

	/*
	 * Method: imageUploadedvalidation Description: Validate image upload Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void imageUploadedvalidation() {
		if (verifyObjectDisplayed(noImageInitials)) {
			// Image Initials
			String strImageInitials = getElementText(noImageInitials).trim();
			// firstname data
			char tempFirstName = testData.get("FirstName").toUpperCase().charAt(0);
			// last name data
			char tempLastName = testData.get("LastName").toUpperCase().charAt(0);
			StringBuilder tempInitial = new StringBuilder().append(tempFirstName).append(tempLastName);

			if (strImageInitials.contains(tempInitial)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerProfilePicVal", Status.PASS,
						"Profile Pic not uploaded hence initials present as: " + tempInitial);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerProfilePicVal", Status.FAIL,
						"Initials not present when profile pic not uploaded");
			}

		} else if (verifyObjectDisplayed(imageUploaded)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerProfilePicVal", Status.PASS,
					"Profile Picture is Uploaded by user");
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerProfilePicVal", Status.FAIL,
					"Failed to validate profile picture");
		}
	}

	/*
	 * Method: ownerUsernameValidation Description: Validate Owner Username
	 * section in Account Setting Page Date: Feb/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	public void ownerUsernameValidation() {

		waitUntilElementVisibleBy(driver, manageUsernameHeader, "ExplicitLongWait");
		getElementInView(manageUsernameHeader);
		if (verifyObjectDisplayed(manageUsernameHeader)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerUsernameVal", Status.PASS,
					"Manage Username header is present");

			elementPresenceVal(editUsernameLink, "Edit Username Link ", "CUIAccountProfilePage", "ownerUsernameVal");

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerUsernameVal", Status.FAIL,
					"Manage Username header is not present");
		}
	}

	/*
	 * Method: userNameVerification Description: Validate Owner username
	 * verification Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void usernameVerification() {
		String strOwnerUserName = getElementText(ownerUserName).trim().toUpperCase();
		if (verifyObjectDisplayed(ownerUserName)) {
			String tempUserName = testData.get("CUI_username");
			tempUserName = tempUserName.toUpperCase();

			if (strOwnerUserName.contains(tempUserName)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "userNameVerification", Status.PASS,
						"Owner Username header present and  displayed correctly: " + strOwnerUserName);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "userNameVerification", Status.FAIL,
						"Owner UserName Displayed is not correct " + strOwnerUserName);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "userNameVerification", Status.FAIL,
					"Owner username not present");
		}
	}

	/*
	 * Method: ownerSecurityQuestionsValidation Description: Validate Owner
	 * SecurityQuestions section in Account Setting Page Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void ownerSecurityQuestionsValidation() {

		waitUntilElementVisibleBy(driver, securityQuestionHeader, "ExplicitLongWait");
		getElementInView(securityQuestionHeader);
		if (verifyObjectDisplayed(securityQuestionHeader)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerSecurityQuestionsVal", Status.PASS,
					"Manage Security Questions header is present");

			elementPresenceVal(editSecurityLink, "Edit Security Link ", "CUIAccountProfilePage",
					"ownerSecurityQuestionsValidation");

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerSecurityQuestionsVal", Status.FAIL,
					"Manage Security Questions header is not present");
		}

	}

	/*
	 * Method: securityQuestion1Validation Description: Validate Owner
	 * SecurityQuestions 1 section in Account Setting Page Date: Feb/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void securityQuestion1Validation() {
		// Security Question 1 data
		String strOwnerSecQues1 = getElementText(securityQuestion1).trim().toUpperCase();

		if (verifyObjectDisplayed(securityQuestion1)) {
			String tempSecQues1 = testData.get("Sec Que 1");
			tempSecQues1 = tempSecQues1.toUpperCase();

			if (strOwnerSecQues1.contains(tempSecQues1)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion1Validation", Status.PASS,
						"Owner Security Questions 1 header present and  displayed correctly: " + strOwnerSecQues1);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion1Validation", Status.FAIL,
						"Owner Security Questions 1 Displayed is not correct " + strOwnerSecQues1);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion1Validation", Status.FAIL,
					"Owner Security Questions 1 not present");
		}

	}

	/*
	 * Method: securityQuestion2Validation Description: Validate Owner
	 * SecurityQuestions 2 section in Account Setting Page Date: Feb/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void securityQuestion2Validation() {
		// Security Question 2 data
		String strOwnerSecQues2 = getElementText(securityQuestion2).trim().toUpperCase();

		if (verifyObjectDisplayed(securityQuestion2)) {
			String tempSecQues2 = testData.get("Sec Que 2");
			tempSecQues2 = tempSecQues2.toUpperCase();

			if (strOwnerSecQues2.contains(tempSecQues2)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion2Validation", Status.PASS,
						"Owner Security Questions 2 header present and  displayed correctly: " + strOwnerSecQues2);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion2Validation", Status.FAIL,
						"Owner Security Questions 2 Displayed is not correct " + strOwnerSecQues2);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion2Validation", Status.FAIL,
					"Owner Security Questions 2 not present");
		}
	}

	/*
	 * Method: securityQuestion3Validation Description: Validate Owner
	 * SecurityQuestions 3 section in Account Setting Page Date: Feb/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void securityQuestion3Validation() {
		// Security Question 3 data
		String strOwnerSecQues3 = getElementText(securityQuestion3).trim().toUpperCase();

		if (verifyObjectDisplayed(securityQuestion3)) {
			String tempSecQues3 = testData.get("Sec Que 3");
			tempSecQues3 = tempSecQues3.toUpperCase();

			if (strOwnerSecQues3.contains(tempSecQues3)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion3Validation", Status.PASS,
						"Owner Security Questions 3 header present and  displayed correctly: " + strOwnerSecQues3);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion3Validation", Status.FAIL,
						"Owner Security Questions 3 Displayed is not correct " + strOwnerSecQues3);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "securityQuestion3Validation", Status.FAIL,
					"Owner Security Questions 3 not present");
		}
	}

	/*
	 * Method: ownerSecurityQuestionsVal Description: Validate Owner
	 * SecurityQuestions section in Account Setting Page Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void ownerSecurityQuestionsValidationSmoke() {

		waitUntilElementVisibleBy(driver, securityQuestionHeader, "ExplicitLongWait");
		getElementInView(securityQuestionHeader);
		if (verifyObjectDisplayed(securityQuestionHeader)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerSecurityQuestionsValidationSmoke",
					Status.PASS, "Manage SecurityQuestions header is present");

			elementPresenceVal(editSecurityLink, "Edit Security Link ", "CUIAccountProfilePage",
					"ownerSecurityQuestionsValidationSmoke");
			fieldPresenceVal(securityQuestion1, "Security Question 1 ", "CUIAccountProfilePage",
					"ownerSecurityQuestionsValidationSmoke", " as " + getElementText(securityQuestion1));
			fieldPresenceVal(securityQuestion2, "Security Question 2 ", "CUIAccountProfilePage",
					"ownerSecurityQuestionsValidationSmoke", " as " + getElementText(securityQuestion2));
			fieldPresenceVal(securityQuestion3, "Security Question 3 ", "CUIAccountProfilePage",
					"ownerSecurityQuestionsValidationSmoke", " as " + getElementText(securityQuestion3));

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerSecurityQuestionsValidationSmoke",
					Status.FAIL, "Manage Security Questions header is not present");
		}

	}

	/*
	 * Method: ownerPasswordVal Description: Validate Owner Password section in
	 * Account Setting Page Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void ownerPasswordSection() throws Exception {

		waitUntilElementVisibleBy(driver, managePasswordHeader, "ExplicitLongWait");
		getElementInView(managePasswordHeader);
		if (verifyObjectDisplayed(managePasswordHeader)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerPasswordSecVal", Status.PASS,
					"Manage Password header is present");

			elementPresenceVal(editPasswordLink, "Edit Password Link ", "CUIAccountProfilePage",
					"ownerPasswordSection");

		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "ownerPasswordSecVal", Status.FAIL,
					"Manage Password header is not present");
		}

	}

	/*
	 * Method: passowrdVerification Description: Validate Owner Passwords in
	 * Account Setting Page Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordVerification(String testDataColumn, String decodePassword) throws Exception {
		waitUntilElementVisibleBy(driver, ownerPassword, "ExplicitMedWait");
		// Owner Password Data
		String strOwnerPassword = getElementText(ownerPassword);
		// To Decode Password
		if (verifyObjectDisplayed(ownerPassword)) {
			String tempPassword = testData.get(testDataColumn);
			if (decodePassword.equalsIgnoreCase("Yes")) {
				// decoded password store
				String decodedPassword = decodePassword(tempPassword);
				tempPassword = decodedPassword;
			}
			int tempSize = tempPassword.length();
			String encodePass = "•";
			for (int encodedLoop = 0; encodedLoop < tempSize - 1; encodedLoop++) {
				encodePass = encodePass.concat("•");
			}

			if (strOwnerPassword.contains(encodePass)) {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "passowrdVerification", Status.PASS,
						"Owner Password header present and no of dots displayed correctly: " + strOwnerPassword);
			} else {
				tcConfig.updateTestReporter("CUIAccountProfilePage", "passowrdVerification", Status.FAIL,
						"Owner Password Displayed, but no of dots displayed not correct" + strOwnerPassword);
			}
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "passowrdVerification", Status.FAIL,
					"Owner Password not present");
		}

	}

	/*
	 * Method: fetchMemberName Description: Validate Owner Password section in
	 * Account Setting Page Date: Feb/2020 Author: Kamalesh Changes By: NA
	 */
	public void fetchMemberName() throws Exception {

		waitUntilElementVisibleBy(driver, memberName, "ExplicitLongWait");
		getElementInView(memberName);
		Assert.assertTrue(verifyObjectDisplayed(memberName), "Member Name Not Displayed");
		strMemberName = getElementText(memberName).trim();
	}

	/*
	 * Method: fetchMemberName Description: Validate Owner Password section in
	 * Account Setting Page Date: Feb/2020 Author: Kamalesh Changes By: NA
	 */
	public void fetchOwnerEmail() throws Exception {
		waitUntilElementVisibleBy(driver, ownerEmail, "ExplicitLongWait");
		getElementInView(ownerEmail);
		Assert.assertTrue(verifyObjectDisplayed(ownerEmail), "Owner Email Not Displayed");
		strOwnerEmail = getElementText(ownerEmail).trim();
	}
}
