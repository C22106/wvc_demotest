package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBookingUpgradePage_IOS extends CUIBookingUpgradePage_Web {

	public static final Logger log = Logger.getLogger(CUIBookingUpgradePage_IOS.class);

	public CUIBookingUpgradePage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		bookingHeader = By.xpath("//div[@class='booking-header__mobile']/p");
	}

	/*
	 * Method: verifyFirstStepOnPageLoad Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes
	 * By: NA
	 */
	public void verifyFirstStepOnPageLoad() {
		String userType = testData.get("User_Type");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String[] headerValue = getElementText(bookingHeader).split(" ");
		if (userType.equalsIgnoreCase("VIP")) {
			Assert.assertTrue(verifyObjectDisplayed(headerVIPUpgrades), "VIP Upgrade Header not displayed");
			if (getElementText(headerVIPUpgrades).contains(headerValue[1].trim())) {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.PASS,
						"The First step of the booking header for VIP User type is " + headerValue
								+ " and the page header is " + getElementText(headerVIPUpgrades));
			} else {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.FAIL,
						"The First step for VIP User type is " + getElementText(headerVIPUpgrades));
			}
		} else if (userType.equalsIgnoreCase("Non-VIP")) {
			Assert.assertTrue(verifyObjectDisplayed(headerTravelerInfo), "Traveller Info header not displayed");
			if (getElementText(headerTravelerInfo).contains(headerValue[1].trim())) {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.PASS,
						"The First step for Non VIP User type is " + getElementText(headerTravelerInfo));
			} else {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.FAIL,
						"The First step for Non VIP User type is " + getElementText(headerTravelerInfo));
			}
		}
	}
}