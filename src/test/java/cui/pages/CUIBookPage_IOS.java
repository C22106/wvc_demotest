package cui.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBookPage_IOS extends CUIBookPage_Web {

	public static final Logger log = Logger.getLogger(CUIBookPage_IOS.class);

	public CUIBookPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		paymentMethodHeader = By.xpath("//h2[.='PAYMENT METHOD'] | //div[contains(@class,'steps') and contains(text(),'Payment Method')]");
	}
	
	
	
	/*
	 * Method: dataEntryForCreditCard Description:Fill Details for Credit Card
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryForCreditCard() {
		String ccFirstName = testData.get("ccFirstName");
		String ccLastName = testData.get("ccLastName");
		String creditCardNumber = testData.get("creditCardNumber");
		String ccMonth = testData.get("ccMonth");
		String ccYear = testData.get("ccYear");
		String ccZipCode = testData.get("ccZipCode");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldCreditCardFirstName)) {
			getElementInView(fieldCreditCardFirstName);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name*", ccFirstName, 1);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last", ccLastName, 1);
			selectByText(fieldCreditCardMonth, ccMonth);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Credit Card Number*", creditCardNumber, 1);
			selectByText(fieldCreditCardYear, ccYear);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Zip Code*", ccZipCode, 1);
		} else {
			tcConfig.updateTestReporter("CUIBookPage", "dataEntryForCreditCard", Status.FAIL,
					"Fields for Credit Card data Entry not displayed");
		}

	}
	
	/*
	 * Method: validateAndEnterSpecialRequest Description: validate and enter
	 * special Request section field Date: June/2020 Author: Monideep
	 * Roychowdhury Changes By: Kamalesh
	 */
	public void validateAndEnterSpecialRequest() {
		assertTrue(verifyObjectDisplayed(spclReqExp), "Special Request drop down not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateAndEnterSpecialRequest", Status.PASS,
				"Special Request available");
		getElementInView(spclReqExp);
		clickElementJSWithWait(spclReqExp);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(roomNumber), "Room number field not present under special request");	
		
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Room Number", "A10", 2);
		tcConfig.updateTestReporter("CUIBookPage", "validateAndEnterSpecialRequest", Status.PASS,
				"Value enter under Special Request section");
	}
	protected By payWithPayPal=By.xpath("//h2[text()='Pay with PayPal']");
	/*
	 * Method: paymentViaPaypal Description:Select payment Option Date field
	 * Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void paymentViaPaypal() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(payWithPayPal);
		clickElementByScriptExecutor("checkout",1);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, emailField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(emailField), "Email field not displayed");
		
		try {
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email or mobile number", testData.get("PaypalEmail"), 2);
			clickElementBy(nextBtn);
		} catch (Exception e) {
			log.info("Email or mobile number already present");
			/*clearField(emailField);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email or mobile number", testData.get("PaypalEmail"), 1);*/
		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, passwordField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(passwordField), "Password field not displayed");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password", testData.get("PaypalPassword"), 1);
		clickElementBy(loginBtn);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, paymentContinueBtn, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(paymentContinueBtn), "Continue Button not displayed");
		getElementInView(paymentContinueBtn);
		clickElementJSWithWait(paymentContinueBtn);

		waitUntilElementVisibleBy(driver, paypalPaymentConfirmationHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(paypalPaymentConfirmationHeader), "Payment Header not displayed");
		tcConfig.updateTestReporter("CUIBookPage", "paymentViaPaypal", Status.PASS,
				"Payment made via Paypal and account used is: " + getElementText(amountPayPalPaidBy));

	}

}
