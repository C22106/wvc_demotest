package cui.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIModifyNightsPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIModifyNightsPage_Web.class);

	public CUIModifyNightsPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	public static int modifyPrice;
	protected By notenoughPointErrorMsg = By.id("error-message");
	protected By ppChargesDetails = By.xpath("//span[@id='points_protection_tier_pricing']/following-sibling::span");

	protected By errorMsg=By.xpath("//*[@id='error-message']/span/p");
	protected By addNightsHeader = By.xpath("//div[text()='Add New Nights']");
	protected By continueButton = By.xpath("//button[text()='Continue']");
	protected By totalPointsRequired = By.xpath("//span[contains(.,'Points')]/following-sibling::span");
	protected By allCalendarDates = By.xpath("//div[@data-visible='true']//tr/td[@class]");
	protected By yesPointProtection = By
			.xpath("//label[contains(text(),'I would like to add Points Protection to my reservation')]");
	protected By noPointProtection = By
			.xpath("//label[contains(text(),'I do not want to add Points Protection at this time')]");
	protected By headerPointProtection = By.xpath(
			"(//h1[contains(.,'Points Protection')] | //div[@class = 'steps__title' and contains(.,'Points Protection')])");
	protected By allAvailableDates = By.xpath("//td[@aria-disabled = 'false']/div/span");
	protected By allFixedWeekDates = By.xpath("//td[@aria-disabled = 'false']");
	protected By fixedweekfirstdate = By.xpath("//td[contains(@aria-label,'start date')]/div/span");
	/*
	 * Method: modifyCheckOutDate Description: Modify Check Out Date Date:
	 * Aug/2020 Author: Unnat Jain Changes By: Kamalesh
	 */
	public void modifyCheckOutDate() {
		String modifiyCheckOutDayNew = null;
		waitUntilElementVisibleBy(driver, addNightsHeader, "ExplicitLongWait");
		String modifiyCheckOutDate = testData.get("ModifyCheckOutDate");
		String modifiyCheckOutDay = modifiyCheckOutDate.split("/")[1].trim();
		if (modifiyCheckOutDay.startsWith("0")) {
			modifiyCheckOutDayNew = modifiyCheckOutDay.split("0")[1].trim();
		}else{
			modifiyCheckOutDayNew = modifiyCheckOutDay;
		}
		clickElementJSWithWait(
				By.xpath("//td[contains(@class,'highlighted')]/div/span[text()='" + modifiyCheckOutDayNew + "']"));
		tcConfig.updateTestReporter("CUIModifyNightsPage_Web", "modifyCheckOutDate", Status.PASS,
				"CheckOut Date Modified To: " + modifiyCheckOutDate);
		checkLoadingSpinnerCUI();
	}

	/*
	 * Method: modifyCheckInDate Description: Modify Check In Date Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void modifyCheckInDate() {
		String modifiyCheckInDayNew = null;
		waitUntilElementVisibleBy(driver, addNightsHeader, "ExplicitLongWait");
		String modifiyCheckInDate = testData.get("ModifyCheckInDate");
		String modifiyCheckInDay = modifiyCheckInDate.split("/")[1].trim();
		if (modifiyCheckInDay.startsWith("0")) {
			 modifiyCheckInDayNew = modifiyCheckInDay.split("0")[1].trim();
		}else{
			 modifiyCheckInDayNew = modifiyCheckInDay;
		}
		clickElementJSWithWait(
				By.xpath("//td[contains(@class,'highlighted')]/div/span[text()='" + modifiyCheckInDayNew + "']"));
		tcConfig.updateTestReporter("CUIModifyNightsPage_Web", "modifyCheckInDate", Status.PASS,
				"CheckIn Date Modified To: " + modifiyCheckInDate);
		checkLoadingSpinnerCUI();
	}

	/*
	 * Method: clickContinueButton Description:click Continue Button Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clickContinueButton() {
		Assert.assertTrue(verifyObjectDisplayed(continueButton), "Continue button not present");
		clickElementJSWithWait(continueButton);
	}

	/*
	 * Method: selectPointProtection Description:Select Point Protection option
	 * Date field Date: Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void selectPointProtection() {
		/*new CUIPointProtectionPage_Web(tcConfig).selectPointProtection();*/
	}

	/*
	 * Method: getPriceForModify Description: Get Price For Modify Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void getPriceForModify() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, totalPointsRequired, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(totalPointsRequired), "Price for Modify not displayed");
		modifyPrice = Integer.parseInt(getElementText(totalPointsRequired).trim().split("\\+")[1].replace(",", ""));
		tcConfig.updateTestReporter("CUIModifyNightsPage_Web", "getPriceForModify", Status.PASS,
				"Modified Price: " + modifyPrice);
	}

	/*
	 * Method: checkPriceModify Description: check Price Modify Date: July/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void checkPriceModify() {
		if (modifyPrice == CUISearchPage_Web.price) {
			tcConfig.updateTestReporter("CUIModifyNightsPage_Web", "checkPriceModify", Status.PASS,
					"Price matched for modification");
		} else {
			tcConfig.updateTestReporter("CUIModifyNightsPage_Web", "checkPriceModify", Status.FAIL,
					"Price not matched for modification");
		}
	}

	/*
	 * Method: unavailableDatesAfterCheckOut Description: Verify that No Dates
	 * are available after checkout date Date: Aug/2020 Author: Unnat jain
	 * Changes By: NA
	 */
	public void unavailableDatesAfterCheckOut() {
		waitUntilElementVisibleBy(driver, addNightsHeader, "ExplicitLongWait");
		String strCheckOutDate = testData.get("Checkoutdate");
		strCheckOutDate = strCheckOutDate.split(" ")[0].trim();
		if (strCheckOutDate.startsWith("0")) {
			strCheckOutDate = strCheckOutDate.replace("0", "");
		}
		Boolean tempCompare = false;
		for (int allDates = 0; allDates < getList(allCalendarDates).size(); allDates++) {
			if (getList(allCalendarDates).get(allDates).getText().equals(strCheckOutDate)) {
				for (int temp = allDates; temp < allDates + 3; temp++) {
					if (getList(allCalendarDates).get(temp + 1).getAttribute("class").contains("blocked")) {
						tempCompare = true;

					} else {
						tempCompare = false;
						break;
					}
				}
				break;
			}
		}
		Assert.assertTrue(tempCompare == true, " Dates are enabled after check out date");
		tcConfig.updateTestReporter("CUIModifyNightsPage", "unavailableDatesAfterCheckOut", Status.PASS,
				" Dates are disabled after check out date");
	}

	/*
	 * Method: selectModifiedPointProtection Description:Select Modified Point
	 * Protection option Date field Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void selectModifiedPointProtection() {
		String needPointProtection = testData.get("NeedModifiedPointProtection");
		waitUntilElementVisibleBy(driver, headerPointProtection, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(headerPointProtection), "Points Protection header not present");
		if (needPointProtection.equalsIgnoreCase("YES")) {
			getElementInView(yesPointProtection);
			clickElementJSWithWait(yesPointProtection);
			tcConfig.updateTestReporter("CUIModifyNightsPage", "selectModifiedPointProtection", Status.PASS,
					"Point Protection selected");
		} else {
			getElementInView(noPointProtection);
			clickElementJSWithWait(noPointProtection);
			tcConfig.updateTestReporter("CUIModifyNightsPage", "selectModifiedPointProtection", Status.PASS,
					" Point Protection declined");
		}

	}

	/*
	 * Method: verifyNotEnoughPointErrorMsg
	 * Description:verifyNotEnoughPointErrorMsg Date field Date: August/2020
	 * Author: Kamalesh Roy Changes By: NA
	 */
	

	public void verifyNotEnoughPointErrorMsg() {
		waitUntilElementVisibleBy(driver, notenoughPointErrorMsg, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(notenoughPointErrorMsg), "Not Enough  PointError Msg not present");
		tcConfig.updateTestReporter("CUIModifyNightsPage", "verifyNotEnoughPointErrorMsg", Status.PASS,
				" Not Enough Point Error Msg Present");
	}

	/*
	 * Method: validatePPsectionSelected Description: validatePPsectionSelected
	 * Date: July/2020 Author: MOnideep Changes By: NA
	 */
	
	public void validatePPsectionSelected(String addcost) {
		waitUntilElementVisibleBy(driver, ppChargesDetails, "ExplicitLowWait");
		if (addcost.equalsIgnoreCase("$0.00")) {

			Assert.assertTrue(
					verifyObjectDisplayed(ppChargesDetails) && getElementText(ppChargesDetails).trim().equals(addcost),
					"Point protection cost hasn't been already covered");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePPsectionSelected", Status.PASS,
					"Points protection cost already covered");

		} else {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Assert.assertTrue(
					verifyObjectDisplayed(ppChargesDetails) && getElementText(ppChargesDetails).trim().equals(addcost),
					"Additional cost of : " + addcost + " not showing up correctly");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePPsectionSelected", Status.PASS,
					"Points protection section validated successfully. Additional cost required is "
							+ getElementText(ppChargesDetails).trim());
		}

	}
	/*
	 * Method: verifyOverlappingErrorMsg
	 * Description: verifyOverlappingErrorMsg
	 * Date: June/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void verifyOverlappingErrorMsg() {
	getElementInView(errorMsg);
	assertTrue(verifyObjectDisplayed(errorMsg), "Overlapping Error message not displayed");
	tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyOverlappingErrorMsg", Status.PASS,
			"Overlapping Error message displayed");
	}

	/*
	 * Method: onlyFixedWeekAvailableDates Description: Verify that only Fixed
	 * Week dates are available Date: Aug/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void onlyFixedWeekAvailableDates(){
		waitUntilElementVisibleBy(driver, addNightsHeader, "ExplicitLongWait");
		Assert.assertTrue(getList(allAvailableDates).size() == 7, "All Fixed Week Dates Are not enabled");
		String fixedWeekFirstDate = testData.get("fixedWeekFirstDate");
		String modifiyFirstDay = fixedWeekFirstDate.split("/")[1].trim();
		if (modifiyFirstDay.startsWith("0")) {
			modifiyFirstDay = modifiyFirstDay.replace("0", "");
		}

		String fixedWeekLastDate = testData.get("fixedWeekLastDate");
		String modifiyLastDay = fixedWeekLastDate.split("/")[1].trim();
		if (modifiyLastDay.startsWith("0")) {
			modifiyLastDay = modifiyLastDay.replace("0", "");
		}
		Assert.assertTrue(getObject(fixedweekfirstdate).getText().trim().equals(modifiyFirstDay),
				"Fixed Week First Date in not enabled");
		Assert.assertTrue(getList(allAvailableDates).get(6).getText().equals(modifiyLastDay),
				"Fixed Week Last Date in not enabled");
		
		tcConfig.updateTestReporter("CUIModifyNightsPage", "onlyFixedWeekAvailableDates", Status.PASS,
				"Only Fixed Week Dates are enabled");
		
	}
	
	/*
	 * Method: fixedWeekEnabledDates Description: Verify that only Fixed Week
	 * dates are available Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void fixedWeekEnabledDates() {
		Boolean tempCompare = false;
			if ((Integer.parseInt(getObject(fixedweekfirstdate).getText().trim())+1) == Integer.parseInt(getList(allAvailableDates).get(0).getText())) {
				for (int temp = 0; temp < 6; temp++) {
					if (getList(allFixedWeekDates).get(temp).getAttribute("class").contains("blocked")) {
						tempCompare = false;
						break;
					} else {
						tempCompare = true;
						
					}
				}
			}else{
				tcConfig.updateTestReporter("CUIModifyNightsPage", "fixedWeekEnabledDates", Status.FAIL,
						"Error while validating Fixed week enabled dates");
			}
		Assert.assertTrue(tempCompare == true, "All Fixed Week Dates are not enabled");
	}
}
