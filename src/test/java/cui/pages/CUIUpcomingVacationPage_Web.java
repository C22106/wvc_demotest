package cui.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIUpcomingVacationPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIUpcomingVacationPage_Web.class);

	public CUIUpcomingVacationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By reservationCards = By.xpath("//div[@class='upcoming-reservation-card__reservation-info']");
	protected By sectionReservationSummary = By.xpath("//section[@class = 'reservation-details-summary']");
	protected By linknavigateUpcomingVacation = By
			.xpath("//div[@class='accountNavigation']//a[contains(.,'Vacations')]");
	protected By textUpcomingVacations = By.xpath("//h1[text() = 'UPCOMING VACATIONS']");
	protected By textPastCancelReservation = By
			.xpath("//h3[contains(text(),'Looking for your past or cancelled reservations')]");
	protected By modifyReservationPage = By.xpath("//div[@class = 'modify-reservation']");
	protected By checkInDates = By.xpath("//p[contains(@class,'check-in')]");
	protected By checkOutDates = By.xpath("//h6[contains(.,'out')]/following-sibling::p");
	protected By resortNames = By.xpath("//div[contains(@class,'card__resort')]/a/h3");
	protected By resortsImage = By.xpath("//div[contains(@class,'card__resort')]/img");
	protected By resortAddress = By.xpath("//div[contains(@class,'card__resort')]/a/following-sibling::p");
	protected By mapLink = By.xpath("//div[contains(@class,'card__resort')]/a[contains(.,'View on Map')]");
	protected By reservationNumber = By.xpath("//h6[contains(.,'Confirm')]/following-sibling::p");
	protected By suiteType = By.xpath("//h6[contains(.,'Suite')]/following-sibling::p");
	protected By viewDetails = By.xpath("//div[contains(@class,'card__details')]/a[contains(.,'Details')]");
	protected By pastReservationHeader = By.xpath("//h3[contains(.,'past or cancelled')]");
	protected By transactionHistoryCTA = By.xpath("//h3[contains(.,'past or cancelled')]/following-sibling::div/a");
	protected By getInspiredSection = By.xpath("//h3[contains(.,'Get Inspired')]");
	protected By getInspiredCards = By.xpath("//div[@class='cardComponent']//div[contains(@class,'subtitle-2')]");
	protected By mapPageHeader = By.xpath("//div[@class='section-hero-header-title-description']//h1");
	protected By navigatedResortPage = By
			.xpath("(// h1[contains(@class,'title-1')] | // div[contains(@class,'title-1 m')])");
	protected By myReservationsLink = By.xpath("//a[text()='My Reservations']");
	// Cancel
	protected By cancelModalHeader = By.id("cancel-reservation-modal-label");
	protected By loadingSpinner = By.xpath("//*[local-name()='svg' and @class='spinner -secondary undefined']");

	protected By modificationHistory = By
			.xpath("//h2[contains(@class,'reservation-details') and text()='Modification History']");
	protected By expandModificationLineItems = By.xpath("//a[contains(@id,'accordion-label')]");
	protected By labelTPI = By.xpath("//span[contains(text(),'TPI')]");
	protected By extraHoliday = By.xpath("//span[contains(text(),'EXTRA HOLIDAY')]");
	/*
	 * Method: navigateToUpcomingVacation Description:Navigate to Upcoming
	 * Vacations Page Date field Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void navigateToUpcomingVacation() {
		waitUntilElementVisibleBy(driver, linknavigateUpcomingVacation, "ExplicitLongWait");
		clickElementJSWithWait(linknavigateUpcomingVacation);
		waitUntilElementVisibleBy(driver, textUpcomingVacations, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textUpcomingVacations),"Upcoming vacation text not present");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "navigateToUpcomingVacation", Status.PASS,
				"Successfully Navigated to Upcoming Vacations Page");
	}

	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadAllReservations() {
		String reservationNumber = testData.get("ReservationNumber");
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber + "']");
		do {
			sendKeyboardKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} while (!verifyObjectDisplayed(modifyReservation));
	}
	
	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadAllReservations(String reservationNumber) {
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber + "']");
		do {
			sendKeyboardKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} while (!verifyObjectDisplayed(modifyReservation));
	}

	/*
	 * Method: selectReservationToModify Description:Select Reservation To
	 * Modify Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectReservationToModify() {
		String reservationNumber = testData.get("ReservationNumber");
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber
				+ "']/ancestor::div[contains(@class,'reservation-info')]//div[contains(@class,'modify-cancel-buttons')]//a[contains(@class,'modify-button')]");
		getElementInView(modifyReservation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(modifyReservation);
		waitUntilElementVisibleBy(driver, modifyReservationPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(modifyReservationPage),"Modify Reservation Page not present");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "selectReservationToModify", Status.PASS,
				"Successfully Navigated to Modify Reservation Page");
		waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
	}

	/*
	 * Method: selectReservationToCancel Description:Select Reservation To
	 * Cancel Date field Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void selectReservationToCancel() {
		String reservationNumber = testData.get("ReservationNumber");
		By cancelReservation = By.xpath("//p[text() = '" + reservationNumber
				+ "']/ancestor::div[contains(@class,'reservation-info')]//div[contains(@class,'modify-cancel-buttons')]//button[contains(.,'Cancel')]");
		getElementInView(cancelReservation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(cancelReservation);
		waitUntilElementVisibleBy(driver, cancelModalHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(cancelModalHeader), "Cancel Header not present");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "selectReservationToCancel", Status.PASS,
				"Successfully Opened Cancel Reservation Modal");
		waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
	}

	/*
	 * Method: selectReservationDetails Description:Select Reservation Details
	 * Link Date: June/2020 Author: Abhijeet Changes By: NA
	 */
	public void selectReservationDetails() {
		String reservationNumber = testData.get("ReservationNumber");
		By detailsReservation = By.xpath("//p[text() = '" + reservationNumber
				+ "']/ancestor::div[contains(@class,'reservation-info')]//div[contains(@class,'details-buttons')]/a[contains(text(),'Details')]");
		getElementInView(detailsReservation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(detailsReservation);
		waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(sectionReservationSummary),"Reservation Summary Section not present");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "selectReservationToCancel", Status.PASS,
				"Successfully Opened Reservation Details Modal");
		waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
	}

	/*
	 * Method: selectReservationDetails Description:Select Reservation Details
	 * Link Date: June/2020 Author: Abhijeet Changes By: NA
	 */
	public void selectReservationDetails(String reservationNumber) {
		By detailsReservation = By.xpath("//p[text() = '" + reservationNumber
				+ "']/ancestor::div[contains(@class,'reservation-info')]//div[contains(@class,'details-buttons')]/a[contains(text(),'Details')]");
		getElementInView(detailsReservation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(detailsReservation);
		waitUntilElementVisibleBy(driver, cancelModalHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(sectionReservationSummary),"Reservation Section Summary not present");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "selectReservationToCancel", Status.PASS,
				"Successfully Opened Reservation Details Modal");
		waitUntilElementInVisible(driver, loadingSpinner, "ExplicitLongWait");
	}


	/*
	 * Method: validateUpgradeFlag Description:Validate the upgrade requested
	 * flag Date field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 * NA
	 */

	public void validateUpgradeFlag() {
		
		boolean blnFound = false;
		List<WebElement> listReservation = getList(reservationCards);

		int countOfReservation = listReservation.size();

		for (int i = 0; i < countOfReservation; i++) {
			scrollDownForElementJSWb(listReservation.get(countOfReservation-1));

			if (verifyObjectDisplayed(
					By.xpath("//div[@class='upcoming-reservation-card__reservation-info' and contains(.,'"
							+ tcConfig.getTestData().get("ReservationNumber")
							+ "')]/div[contains(@class,'upcoming-reservation-card__unit-type')]/p/span[2][contains(text(),'REQUESTED')]"))) {
				tcConfig.updateTestReporter("CUIUpcomingVacationPage", "validateUpgradeFlag",
						Status.PASS, "Upgrade requested flag validated successfully");
				blnFound = true;
				break;
			}
			listReservation = getList(reservationCards);
			countOfReservation=listReservation.size();
		}

		Assert.assertTrue(blnFound, "Upgrade requested flag not present");

	}
	/*
	 * Method: validateUpgradeNotRequestedFlag Description:Validate the upgrade
	 * not requested flag Date field Date: June/2020 Author: Monideep
	 * Roychowdhury Changes By: NA
	 */

	public void validateUpgradeNotRequestedFlag() {

		boolean blnFound = false;
		List<WebElement> listReservation = getList(reservationCards);

		int countOfReservation = listReservation.size();

		for (int i = 0; i < countOfReservation; i++) {
			scrollDownForElementJSWb(listReservation.get(countOfReservation-1));

			if (verifyObjectDisplayed(
					By.xpath("//div[@class='upcoming-reservation-card__reservation-info' and contains(.,'"
							+ tcConfig.getTestData().get("ReservationNumber")
							+ "')]/div[contains(@class,'upcoming-reservation-card__unit-type')]/p/span[2][contains(text(),'VIP Upgrade Not Requested')]"))) {
				tcConfig.updateTestReporter("CUIUpcomingVacationPage", "validateUpgradeNotRequestedFlag",
						Status.PASS, "Upgrade not requested flag validated successfully");
				blnFound = true;
				break;
			}
			listReservation = getList(reservationCards);
			countOfReservation=listReservation.size();
		}

		Assert.assertTrue(blnFound, "Upgrade not requested flag not present");

	}

	/*
	 * Method: verifyUpcomingReservationCardPresent Description:Verify No Modify
	 * CTA For Reservation Date field Date: June/2020 Author: Kamalesh Changes
	 * By: NA
	 */
	public void verifyUpcomingReservationCardPresent() {
		String reservationNumber = testData.get("ReservationNumber");
		By reservationCard = By
				.xpath("//p[text() = '" + reservationNumber + "']/ancestor::div[contains(@class,'reservation-info')]");
		Assert.assertTrue(verifyObjectDisplayed(reservationCard), "Reservation Card is not displayed");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "verifyUpcomingReservationCardPresent", Status.PASS,
				"Reservation Card is displayed in the Upcoming Vacation section");

	}

	/*
	 * Method: verifyNoModifyCTAForReservation Description:Verify No Modify CTA
	 * For Reservation Date field Date: June/2020 Author: Kamalesh Changes By:
	 * NA
	 */
	public void verifyNoModifyCTAForReservation() {
		String reservationNumber = testData.get("ReservationNumber");
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber
				+ "']/ancestor::div[contains(@class,'reservation-info')]//div[contains(@class,'modify-cancel-buttons')]//a[contains(@class,'modify-button')]");
		Assert.assertFalse(verifyObjectDisplayed(modifyReservation), "Modify CTA is displayed");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "verifyNoModifyCTAForReservation", Status.PASS,
				"Modify CTA is not displayed against the Reservation");

	}

	/*
	 * Method: verifyTPILabel Description:Verify No Modify CTA For Reservation
	 * Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyTPILabel() {
		getElementInView(modificationHistory);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> allOptions = getList(expandModificationLineItems);
		for (WebElement we : allOptions) {
			we.click();
		}
		Assert.assertTrue(verifyObjectDisplayed(labelTPI),"TPI label not present");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "verifyTPILabel", Status.PASS,
				"TPI Label is successfully displayed under the Reservation Modification History section");

	}
	
	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadReservationsTillEnd(int totalVacations) {
		do {
			sendKeyboardKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} while (getList(reservationCards).size() != totalVacations);
	}

	/*
	 * Method: verifyVacationCards Description:Verify All Vacation Reservation
	 * Cards Details Date field Date: June/2020 Author: Unnat Changes By: NA
	 */
	public void verifyVacationCards(int totalVacations) {
		elementListPresenceVal(resortNames, totalVacations, "Resort Names", "CUIUpcomingVacationPage",
				"verifyVacationCards");
		elementListPresenceVal(resortsImage, totalVacations, "Resort Image", "CUIUpcomingVacationPage",
				"verifyVacationCards");
		elementListPresenceVal(resortAddress, totalVacations, "Resort Address", "CUIUpcomingVacationPage",
				"verifyVacationCards");
		elementListPresenceVal(mapLink, totalVacations, "Resort Map Link", "CUIUpcomingVacationPage",
				"verifyVacationCards");
		elementListPresenceVal(checkInDates, totalVacations, "Resort Check In Date", "CUIUpcomingVacationPage",
				"verifyVacationCards");
		elementListPresenceVal(checkOutDates, totalVacations, "Resort Check Out Date", "CUIUpcomingVacationPage",
				"verifyVacationCards");
		elementListPresenceVal(reservationNumber, totalVacations, "Resort Reservation Number",
				"CUIUpcomingVacationPage", "verifyVacationCards");
		elementListPresenceVal(suiteType, totalVacations, "Resort Suite Type", "CUIUpcomingVacationPage",
				"verifyVacationCards");
		elementListPresenceVal(viewDetails, totalVacations, "Resort Details Link", "CUIUpcomingVacationPage",
				"verifyVacationCards");
	}

	/*
	 * Method: resortDatesSorting Description:Verify All Vacation Reservation
	 * Cards Sorting Date field Date: June/2020 Author: Unnat Changes By: NA
	 */
	public void resortDatesSorting() throws Exception {

		// Date List
		List<String> resortDateList = new ArrayList<String>();
		for (int dateCount = 0; dateCount < getList(checkInDates).size(); dateCount++) {
			String strRowDate = getList(checkInDates).get(dateCount).getText();
			strRowDate = strRowDate.split(" ")[1];
			resortDateList.add(strRowDate);
		}

		ascendingOrderDateCheck(resortDateList, "MM/dd/yyyy", "Resrvations ");

	}

	/*
	 * Method: pastReservationsSection Description: Past Reservation Section
	 * Date field Date: June/2020 Author: Unnat Changes By: NA
	 */
	public void pastReservationsSection() {
		elementPresenceVal(pastReservationHeader, "Past Reservation Section ", "CUIUpcomingVacationsPage",
				"pastReservationsSection");
		elementPresenceVal(transactionHistoryCTA, "Transaction History CTA", "CUIUpcomingVacationsPage",
				"pastReservationsSection");
	}

	/*
	 * Method: getInspiredSection Description: Get Inspired Section Date field
	 * Date: June/2020 Author: Unnat Changes By: NA
	 */
	public void getInspiredSection() {
		fieldPresenceVal(getInspiredSection, "Get Inspired Section", "CUIUpcomingVacationsPage", "getInspiredSection",
				"Total: " + Integer.toString(getList(getInspiredCards).size()));
		sendKeyboardKeys(Keys.HOME);

	}

	/*
	 * Method: mapLinkValidations Description:Map Section Elements Validations *
	 * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void mapLinkValidations() {
		getElementInView(getList(mapLink).get(0));
		clickElementBy(getList(mapLink).get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(newWindow);
		waitUntilElementVisibleBy(driver, mapPageHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(mapPageHeader),"Map page header not present");

		tcConfig.updateTestReporter("CUIUpcomingVacationsPage", "mapLinkValidations", Status.PASS,
				"Navigated to Map page");
		navigateToMainTab(newWindow);
		waitUntilElementVisibleBy(driver, textUpcomingVacations, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textUpcomingVacations),"Upcoming vacation text not present");
	}

	/*
	 * Method: resortCardNavigation Description:Validate Resort card Navigation
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void resortCardNavigation() {
		// String get first resort name
		String strFirstResortName = getList(resortNames).get(0).getText().trim();
		getElementInView(getList(resortNames).get(0));
		getList(resortNames).get(0).click();
		waitUntilElementVisibleBy(driver, navigatedResortPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(navigatedResortPage),"Reseort page navigation failed");
		if (getElementText(navigatedResortPage).trim().contains(strFirstResortName)) {
			tcConfig.updateTestReporter("CUIUpcomingVacationsPage", "resortCardNavigation", Status.PASS,
					"Resort Navigated Successfully");
		} else {
			tcConfig.updateTestReporter("CUIUpcomingVacationsPage", "resortCardNavigation", Status.FAIL,
					"Resort Navigation Failed");
		}
		navigateBackAndWait(textUpcomingVacations);

	}
	
	/*
	 * Method: verifyExtraHolidayLabel Description:Verify Extra Holiday For
	 * Reservation Date field Date: June/2020 Author: Unnat Changes By: NA
	 */
	public void verifyExtraHolidayLabel() {
		getElementInView(modificationHistory);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> allOptions = getList(expandModificationLineItems);
		for (WebElement we : allOptions) {
			we.click();
		}
		Assert.assertTrue(verifyObjectDisplayed(extraHoliday),"Extra Holidays not present");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "verifyTPILabel", Status.PASS,
				"Extra Holiday Label is successfully displayed under the Reservation Modification History section");

		navigateBackAndWait(textUpcomingVacations);
	}
	
	/*
	 * Method: verifyNoCancelCTAForReservation Description:Verify No Cancel CTA
	 * For Reservation Date field Date: June/2020 Author: Unnat Changes By: NA
	 */
	public void verifyNoCancelCTAForReservation() {
		By cancelReservation = By.xpath("//p[text() = '" + reservationNumber
				+ "']/ancestor::div[contains(@class,'reservation-info')]//div[contains(@class,'modify-cancel-buttons')]//button[contains(.,'Cancel')]");
		Assert.assertFalse(verifyObjectDisplayed(cancelReservation), "Cancel CTA is displayed");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "verifyNoCancelCTAForReservation", Status.PASS,
				"Cancel CTA is not displayed against the Reservation");

	}
	
	/*
	 * Method: clickMyReservationLink Description:Navigate to Upcoming Vacations
	 * Page via My Reservation Link Date field Date: July/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void clickMyReservationLink() {
		waitUntilElementVisibleBy(driver, myReservationsLink, "ExplicitLongWait");
		clickElementJSWithWait(myReservationsLink);
		waitUntilElementVisibleBy(driver, textUpcomingVacations, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textUpcomingVacations),"Upcoming Vacation page not displayed");
		tcConfig.updateTestReporter("CUIUpcomingVacationPage", "navigateToUpcomingVacation", Status.PASS,
				"Successfully Navigated to Upcoming Vacations Page");
	}

}
