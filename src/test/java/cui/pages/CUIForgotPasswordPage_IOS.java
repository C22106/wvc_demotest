package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUIForgotPasswordPage_IOS extends CUIForgotPasswordPage_Web {

	public static final Logger log = Logger.getLogger(CUIForgotPasswordPage_IOS.class);

	public CUIForgotPasswordPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		forgotPasswordDescription = By
				.xpath("//h2[text()='TIME FOR A NEW PASSWORD']");

	}

	
	/*
	 * Method: enterUsername Description: CUI Forgot Password flow datat
	 * entryDate Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta
	 */
	public void enterUsername() {
		waitUntilElementVisibleBy(driver, usernameTextBox, "ExplicitLongWait");
		getElementInView(usernameTextBox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//fieldDataEnter(usernameTextBox, testData.get("CUI_username"));
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Username", testData.get("CUI_username"),2);

	}
	
	/*
	 * Method: enterNameDetails Description: CUI Forgot Password flow data entry
	 * Date Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterNameDetails() {

		if (testData.get("AccountAssociated").contains("Individual")) {
			getElementInView(firstNameTextBox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "First Name*", testData.get("FirstName"));
			
			getElementInView(lastNametextBox);
			clickElementBy(lastNametextBox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Last Name*", testData.get("LastName"));
		} else if (testData.get("AccountAssociated").contains("Trust")) {
			// clickElementBy(clickHereLink);
			try {
				getElementInView(companyNameTextBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				sendKeysByScriptExecutor((RemoteWebDriver)driver, "Company or Trust Name", testData.get("CompanyName"),2);

				
			} catch (Exception e) {
				clickElementBy(clickHereLink);
				waitUntilElementVisibleBy(driver, companyNameTextBox, "ExplicitLongWait");
				getElementInView(companyNameTextBox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				sendKeysByScriptExecutor((RemoteWebDriver)driver, "Company or Trust Name", testData.get("CompanyName"),2);

				
			}

		}
	}
}