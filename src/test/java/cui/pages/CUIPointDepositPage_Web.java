
package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPointDepositPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIPointDepositPage_Web.class);

	public CUIPointDepositPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected String totalDepositPointCW;
	protected By fieldPointToDeposit = By.xpath("//input[@id = 'points-to-be-deposited']");
	protected By totalHousekeepingCost = By.xpath("//span[text() = 'Total Cost']/following-sibling::span");
	protected By textHousekeepingCredit = By
			.xpath("//div[contains(@class,'housekeeping__credits')]//span[text() = 'Housekeeping Credits']");
	protected By listHousekeeping = By.xpath("(//div[contains(@class,'housekeeping__credits')]//div)");
	protected By textHousekeepingInstructional = By.xpath("//span[contains(text(),'housekeeping credits needed')]");
	protected By totalHousekeepingCreditAvailable = By
			.xpath("//span[contains(@id,'housekeeping-credits')]/following-sibling::span[contains(@class,'right')]");
	protected By pointDepositRCIProgessBar = By.xpath("//div[contains(@class,'booking-header')][2]");
	protected By backRCIandDepositCTA = By.xpath("//div[contains(@class,'back-cta')]/a");
	protected By headerSelectDeposit = By.xpath(
			"//h1[contains(.,'Select Deposit')] | //div[@class = 'steps__title' and contains(.,'Select Deposit')]");
	protected By areaPointChartRCI = By.xpath("//div[@id = 'rci-points-chart']//a");
	protected By tablePointChart = By.xpath("//div[@id = 'rci-points-chart']//table");
	protected By informationalTextDepositRCI = By
			.xpath("//div[@id = 'rci-points-chart']//p[contains(text(),'points required for your desired vacation')]");
	protected By disclaimerRCIDeposit = By
			.xpath("//div[@id = 'rci-points-chart']//span[contains(.,'estimates of RCI values')]");
	protected By headerDepositPoints = By.xpath("//h2[text() = 'Deposit Points']");
	protected By tooltipDepositPoint = By.xpath("//span[@id = 'deposit-points-tooltip']");
	protected By textTooltipDepositPoint = By.xpath("//div[contains(text(),'points into RCI')]");
	protected By instructionalTextUseYearDeposit = By
			.xpath("//h2[text() = 'Deposit Points']/../p[contains(text(),'Select the use year')]");
	protected By areaUseYear = By.xpath("//div[contains(@class,'form deposits-rci')]");
	protected By totalUseYear = By
			.xpath("//div[contains(@class,'form deposits-rci')]/div//label[contains(@for,'use-year')]");
	protected By totalUserYearCW = By
			.xpath("//div[contains(@class,'form deposits-rci')]/div//span[contains(text(),'Use Year')]");
	protected By buttonContinue = By.xpath("(//button[text() = 'Continue'])[1]");
	protected By headerDepositFrom = By.xpath("//h5[contains(text(),'Deposit From')]");
	protected By yearDepositFrom = By.xpath("//span[contains(text(),'Use year')]");
	protected By pointAvailableForDeposit = By.xpath(
			"//span[contains(text(),'Use year')]/..//span[contains(text(),'Points Available')]/following-sibling::span");
	protected By headerPointsToDeposit = By.xpath("//h6[contains(text(),'Points to Deposit')]");
	protected By instructionalTextPointsToDeposit = By
			.xpath("//span[contains(text(),'transfer to a future use year')]");
	protected By textDepositTo = By.xpath("//h5[contains(text(),'Deposit To')]");
	protected By informationPointToDeposit = By
			.xpath("//h5[text() = 'Points Deposits']/parent::div[contains(@class,'deposits-rci')]");

	/*
	 * Method: verifyProgressBar Description:Verify Progress Bar in Point
	 * Deposit Page Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyProgressBar() {
		elementPresenceVal(pointDepositRCIProgessBar, "Progress Bar", "CUIPointDepositPage", "verifyProgressBar");
	}

	/*
	 * Method: verifyBackRCIandDepositCTA Description:Verify Back RCI and
	 * Deposit CTA Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyBackRCIandDepositCTA() {
		elementPresenceVal(backRCIandDepositCTA, "Back to RCI and Deposit Link", "CUIPointDepositPage",
				"verifyBackRCIandDepositCTA");
	}

	/*
	 * Method: verifyTextSelectDeposit Description:verify Header Select Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyTextSelectDeposit() {

		elementPresenceVal(headerSelectDeposit, "Select Deposit Header", "CUIPointDepositPage",
				"verifyTextSelectDeposit");
	}

	/*
	 * Method: verifyRCIPointChart Description:RCI Point Chart Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyRCIPointChart() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(areaPointChartRCI)) {
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyRCIPointChart", Status.PASS,
					"Area Point Chart is Displayed");
			clickElementJSWithWait(areaPointChartRCI);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			elementPresenceVal(informationalTextDepositRCI,
					"Informational Text: " + getObject(informationalTextDepositRCI).getText(), "CUIPointDepositPage",
					"verifyRCIPointChart");
			elementPresenceVal(disclaimerRCIDeposit, "Disclamer Text: " + getObject(disclaimerRCIDeposit).getText(),
					"CUIPointDepositPage", "verifyRCIPointChart");
			validateTableRCIPoint();
			clickElementJSWithWait(areaPointChartRCI);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyRCIPointChart", Status.FAIL,
					"Area Point Chart not Displayed");
		}
	}

	/*
	 * Method: validateTableRCIPoint Description:Table Verification RCI Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTableRCIPoint() {
		if (verifyObjectDisplayed(tablePointChart)) {
			int totalHeader = getList(By.xpath("//table//tr//th")).size();
			int totalValue = getList(By.xpath("//table//tr//td")).size();
			tcConfig.updateTestReporter("CUIPointDepositPage", "validateTableRCIPoint", Status.PASS,
					"Table for RCI Point Present with Total number of header as: " + totalHeader
							+ " and total number of values as: " + totalValue);
		}
		tcConfig.updateTestReporter("CUIPointDepositPage", "validateTableRCIPoint", Status.PASS,
				"Table for RCI Point not Present");
	}

	/*
	 * Method: verifyHeaderTotalDepositAndUseYear Description:verify Header
	 * Total Deposit And UseYear Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHeaderTotalDepositAndUseYear() {
		elementPresenceVal(headerDepositPoints, "Header Deposit Points", "CUIPointDepositPage",
				"verifyHeaderTotalDepositAndUseYear");
		elementPresenceVal(instructionalTextUseYearDeposit,
				"Text: " + getObject(instructionalTextUseYearDeposit).getText(), "CUIPointDepositPage",
				"verifyHeaderTotalDepositAndUseYear");

	}

	/*
	 * Method: tooltipTotalDeposit Description:Tool Tip Verification Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void tooltipTotalDeposit() {
		toolTipValidations(tooltipDepositPoint, "HouseKeeping Credit", textTooltipDepositPoint);
	}

	/*
	 * Method: totalUseYear Description:Total Use Year Date: June/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void totalUseYear() {
		waitUntilElementVisibleBy(driver, areaUseYear, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(areaUseYear), "Use Year Area not present");
		if (getList(totalUseYear).size() > 0) {
			tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYear", Status.PASS,
					"Toal Number of Use Year is: " + getList(totalUseYear).size());
			for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
				String textUseYear = getList(totalUseYear).get(useYearIterator).getText();
				if (textUseYear.isEmpty() || textUseYear == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYear", Status.FAIL,
							"Use Year Text not Present for index: " + useYearIterator);
				} else {
					tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYear", Status.PASS,
							"Use Year is: " + textUseYear);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYear", Status.FAIL, "Use Year Not Found");
		}
	}

	/*
	 * Method: verifyPointAvailableRCIDeposit Description:verify Point Available
	 * RCIDeposit Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyPointAvailableRCIDeposit() {
		if (getList(totalUseYear).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
				String textUseYear = getList(totalUseYear).get(useYearIterator).getText();
				String totalPointAvailable = getObject(By.xpath(
						"(//span[contains(text(),'Points Available')]/..)[" + (useYearIterator + 1) + "]//span[2]"))
								.getText();
				if (totalPointAvailable.isEmpty() || totalPointAvailable == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyPointAvailableRCIDeposit", Status.FAIL,
							"Available Point not Present for index: " + (useYearIterator + 1));
				} else {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyPointAvailableRCIDeposit", Status.PASS,
							"Points: " + totalPointAvailable + " is available for Use Year: " + textUseYear);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyPointAvailableRCIDeposit", Status.FAIL,
					"Use Year Not Found");
		}
	}

	/*
	 * Method: verifyHousekeepingCreditRCIDeposit Description:verify
	 * Housekeeping Credit RCI Deposit Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyHousekeepingCreditRCIDeposit() {
		if (getList(totalUseYear).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
				String textUseYear = getList(totalUseYear).get(useYearIterator).getText();
				String totalHousekeepingCredit = getObject(
						By.xpath("(//span[contains(text(),'Points Available')]/../following-sibling::p//span/..)["
								+ (useYearIterator + 1) + "]//span[2]")).getText();
				if (totalHousekeepingCredit.isEmpty() || totalHousekeepingCredit == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditRCIDeposit",
							Status.FAIL, "HouseKeeping Credit not Present for index: " + (useYearIterator + 1));
				} else {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditRCIDeposit",
							Status.PASS, "Housekeeping credit: " + totalHousekeepingCredit
									+ " is available for Use Year: " + textUseYear);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditRCIDeposit", Status.FAIL,
					"Use Year Not Found");
		}
	}

	/*
	 * Method: verifyHousekeepingCreditNotPresentRCIDeposit Description:verify
	 * Housekeeping Credit Not Present RCI Deposit Date: June/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void verifyHousekeepingCreditNotPresentRCIDeposit() {
		if (getList(totalUseYear).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUseYear).size(); useYearIterator++) {
				if (!verifyObjectDisplayed(getObject(
						By.xpath("(//span[contains(text(),'Points Available')]/../following-sibling::p//span/..)["
								+ (useYearIterator + 1) + "]//span[2]")))) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditNotPresentRCIDeposit",
							Status.PASS, "HouseKeeping Not Present for VIP Owner");
				} else {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditNotPresentRCIDeposit",
							Status.FAIL, "HouseKeeping Present for VIP Owner");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditRCIDeposit", Status.FAIL,
					"Use Year Not Found");
		}
	}

	/*
	 * Method: selectUseYearRCIDeposit Description:selectUseYearRCIDeposit Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectUseYearRCIDeposit() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement useyearSelect = getObject(
					By.xpath("//div[contains(@class,'form deposits-rci')]/div//label[contains(.,'"
							+ useYearArray[useyearIterator] + "')]"));
			getElementInView(useyearSelect);
			clickElementJSWithWait(useyearSelect);
			tcConfig.updateTestReporter("CUIPointDepositPage", "selectUseYearRCIDeposit", Status.PASS,
					"Clicked on User Year and selected User Year is: " + useYearArray[useyearIterator]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

	/*
	 * Method: clickContinueButton Description:click Continue Button Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		if (verifyObjectDisplayed(buttonContinue)) {
			getElementInView(buttonContinue);
			clickElementJSWithWait(buttonContinue);
		}

	}

	/*
	 * Method: verifyHeaderDepositFrom Description:Verify Header Deposit From
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void verifyHeaderDepositFrom() {
		elementPresenceVal(headerDepositFrom, "Header Deposit Form", "CUIPointDepositPage", "verifyHeaderDepositFrom");

	}

	/*
	 * Method: verifyDepositFromYearAndPointAvailable Description:verify Deposit
	 * From Year And Point Available Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyDepositFromYearAndPointAvailable() {
		elementPresenceVal(yearDepositFrom, "Point Deposit from year: " + getElementText(yearDepositFrom),
				"CUIPointDepositPage", "verifyDepositFromYearAndPointAvailable");
		Assert.assertTrue(verifyObjectDisplayed(pointAvailableForDeposit), "Points not available for deposit");
		totalDepositPointCW = getElementText(pointAvailableForDeposit).trim();
		if (totalDepositPointCW.isEmpty() || totalDepositPointCW == null) {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyDepositFromYearAndPointAvailable", Status.FAIL,
					"Total Point available for deposit not displayed");
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyDepositFromYearAndPointAvailable", Status.PASS,
					"Total Point available for deposit: " + totalDepositPointCW);
		}
	}

	/*
	 * Method: verifyDepositFromYearAndPointAvailable Description:verify Deposit
	 * From Year And Point Available Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyHouseKeepingCreditAvailable() {
		Assert.assertTrue(verifyObjectDisplayed(totalHousekeepingCreditAvailable),
				"Housekeeping Credits not available");
		String totalHousekeepingCW = getElementText(totalHousekeepingCreditAvailable).trim();
		if (totalHousekeepingCW.isEmpty() || totalHousekeepingCW == null) {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyDepositFromYearAndPointAvailable", Status.FAIL,
					"Total Housekeeping not displayed");
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyDepositFromYearAndPointAvailable", Status.PASS,
					"Total Housekeeping Available is : " + totalHousekeepingCW);
		}
	}

	/*
	 * Method: verifyAreaPointsToDeposit Description:verify Area Points To
	 * Deposit Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void verifyAreaPointsToDeposit() {
		elementPresenceVal(headerPointsToDeposit, "Header Points To Deposit", "CUIPointDepositPage",
				"verifyAreaPointsToDeposit");
		elementPresenceVal(instructionalTextPointsToDeposit,
				"Instructional Text: " + getElementText(instructionalTextPointsToDeposit), "CUIPointDepositPage",
				"verifyAreaPointsToDeposit");
		elementPresenceVal(informationPointToDeposit, "Information Point To Deposit", "CUIPointDepositPage",
				"verifyAreaPointsToDeposit");

	}

	/*
	 * Method: pointToDepositCW Description:Club Wyndham Deposit point Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void pointToDepositCW() {
		String criteriaToDeposit = testData.get("AmountDepositUpto");
		if (verifyObjectDisplayed(fieldPointToDeposit)) {
			if (criteriaToDeposit.equalsIgnoreCase("Full")) {
				getElementInView(fieldPointToDeposit);
				clickElementJSWithWait(fieldPointToDeposit);
				sendKeysBy(fieldPointToDeposit, totalDepositPointCW);
				sendKeyboardKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				String totalDepositPoint = testData.get("PointsToDeposit").trim();
				getElementInView(fieldPointToDeposit);
				clickElementJSWithWait(fieldPointToDeposit);
				sendKeysBy(fieldPointToDeposit, totalDepositPoint);
				sendKeyboardKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}
	}

	/*
	 * Method: validateHouseKeepingArea Description:validate HouseKeeping Area
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHouseKeepingArea() {
		elementPresenceVal(textHousekeepingCredit, "Header HouseKeeping Credit", "CUIPointDepositPage",
				"validateHouseKeepingArea");
		elementPresenceVal(textHousekeepingInstructional, "Text: " + getElementText(textHousekeepingInstructional),
				"CUIPointDepositPage", "validateHouseKeepingArea");

		for (int iteratorHousekeeping = 1; iteratorHousekeeping <= getList(listHousekeeping)
				.size(); iteratorHousekeeping++) {
			String housekeepingLabel = getElementText(getList(By
					.xpath("(//div[contains(@class,'housekeeping__credits')]//div)[" + iteratorHousekeeping + "]/span"))
							.get(0)).trim();
			String houseKeepingValue = getElementText(getList(By
					.xpath("(//div[contains(@class,'housekeeping__credits')]//div)[" + iteratorHousekeeping + "]/span"))
							.get(1)).trim();
			if (housekeepingLabel.isEmpty() || housekeepingLabel == null || houseKeepingValue.isEmpty()
					|| houseKeepingValue == null) {
				tcConfig.updateTestReporter("CUIPointDepositPage", "validateHouseKeepingArea", Status.FAIL,
						"Either HouseKeeping or Value not present");
			} else {
				tcConfig.updateTestReporter("CUIPointDepositPage", "validateHouseKeepingArea", Status.PASS,
						"" + housekeepingLabel + " : " + houseKeepingValue);
			}
		}
	}

	/*
	 * Method: verifyHeaderDepositTo Description:Verify Header Deposit To Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeaderDepositTo() {
		elementPresenceVal(textDepositTo, "Header Deposit To", "CUIPointDepositPage", "verifyHeaderDepositTo");

	}

	/*
	 * Method: totalUseYearCW Description:Total Use Year Present Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void totalUseYearCW() {
		waitUntilElementVisibleBy(driver, areaUseYear, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(areaUseYear), "Use Year Area not present");
		if (getList(totalUserYearCW).size() > 0) {
			tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYearCW", Status.PASS,
					"Toal Number of Use Year is: " + getList(totalUserYearCW).size());
			for (int useYearIterator = 0; useYearIterator < getList(totalUserYearCW).size(); useYearIterator++) {
				String textUseYear = getList(totalUserYearCW).get(useYearIterator).getText();
				if (textUseYear.isEmpty() || textUseYear == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYearCW", Status.FAIL,
							"Use Year Text not Present for index: " + useYearIterator);
				} else {
					tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYearCW", Status.PASS,
							"Use Year is: " + textUseYear);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "totalUseYearCW", Status.FAIL, "Use Year Not Found");
		}
	}

	/*
	 * Method: verifyPointAvailableRCIDepositCW Description:verify Point
	 * Available RCIDeposit Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyPointAvailableDepositCW() {
		if (getList(totalUserYearCW).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUserYearCW).size(); useYearIterator++) {
				String textUseYear = getList(totalUserYearCW).get(useYearIterator).getText();
				String totalPointAvailable = getObject(By
						.xpath("(//div[contains(@class,'form deposits-rci')]/div//span[contains(text(),'Use Year')]/../..)["
								+ (useYearIterator + 1)
								+ "]//span[contains(text(),'Points Available')]/following-sibling::span")).getText();
				if (totalPointAvailable.isEmpty() || totalPointAvailable == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyPointAvailableDepositCW", Status.FAIL,
							"Available Point not Present for index: " + (useYearIterator + 1));
				} else {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyPointAvailableDepositCW", Status.PASS,
							"Points: " + totalPointAvailable + " is available for Use Year: " + textUseYear);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyPointAvailableDepositCW", Status.FAIL,
					"Use Year Not Found");
		}
	}

	/*
	 * Method: verifyHouseKeepingValueCW Description:verify HouseKeeping Value
	 * CW Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHouseKeepingValueCW() {
		if (getList(totalUserYearCW).size() > 0) {
			for (int useYearIterator = 0; useYearIterator < getList(totalUserYearCW).size(); useYearIterator++) {
				String textUseYear = getList(totalUserYearCW).get(useYearIterator).getText();
				String totalHouseKeepingAvailable = getObject(By
						.xpath("(//div[contains(@class,'form deposits-rci')]/div//span[contains(text(),'Use Year')]/../..)["
								+ (useYearIterator + 1)
								+ "]//span[contains(text(),'Housekeeping Credits Available')]/following-sibling::span"))
										.getText();
				if (totalHouseKeepingAvailable.isEmpty() || totalHouseKeepingAvailable == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHouseKeepingValueCW", Status.FAIL,
							"Housekeeping Value not Present for index: " + (useYearIterator + 1));
				} else {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyPointAvailableDepositCW", Status.PASS,
							"HouseKeeping Points: " + totalHouseKeepingAvailable + " is available for Use Year: "
									+ textUseYear);
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIRCIPointDepositPage", "verifyPointAvailableDepositCW", Status.FAIL,
					"Use Year Not Found");
		}
	}

	/*
	 * Method: totalCostHousekeepingForDepositCW Description:total Cost
	 * Housekeeping For Deposit CW Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */

	public void totalCostHousekeepingForDepositCW() {
		if (verifyObjectDisplayed(totalHousekeepingCost)) {
			String totalCost = getElementText(totalHousekeepingCost);
			if (totalCost.isEmpty() || totalCost == null) {
				tcConfig.updateTestReporter("CUIPointDepositPage", "totalCostHousekeepingFprDepositCW", Status.FAIL,
						"Total Housekeeping Cost Value is not displayed");
			} else {
				tcConfig.updateTestReporter("CUIPointDepositPage", "totalCostHousekeepingFprDepositCW", Status.PASS,
						"HouseKeeping Cost: " + totalCost);
			}
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "totalCostHousekeepingFprDepositCW", Status.FAIL,
					"Total Housekeeping Point Area is not displayed");
		}
	}

	/*
	 * Method: verifyHeaderPointToDeposit Description:verifyHeaderPointToDeposit
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeaderPointToDeposit() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			/*
			 * WebElement headerPointToDepoit = getObject( By.xpath(
			 * "(//h6[text() = 'Points to Deposit'])[" + (useyearIterator + 1) +
			 * "]"));
			 */
			WebElement headerPointToDepoit = getObject(By.xpath("//label[contains(.,'" + useYearArray[useyearIterator]
					+ "')]/..//h6[text() = 'Points to Deposit']"));
			/*
			 * WebElement instructionalTextPointToDeposit = getObject( By.xpath(
			 * "(//span[contains(text(),'Enter the number of points')])[" +
			 * (useyearIterator + 1) + "]"));
			 */
			WebElement instructionalTextPointToDeposit = getObject(By.xpath("//label[contains(.,'"
					+ useYearArray[useyearIterator] + "')]/..//span[contains(text(),'Enter the number of points')]"));
			elementPresenceVal(headerPointToDepoit,
					"Point To Deposit Header for useYear: " + useYearArray[useyearIterator], "CUIPointDepositPage",
					"verifyHeaderPointToDeposit");
			elementPresenceVal(instructionalTextPointToDeposit,
					"Instructional Text for useYear: " + useYearArray[useyearIterator], "CUIPointDepositPage",
					"verifyHeaderPointToDeposit");

		}
	}

	/*
	 * Method: totalPointsToDeposit Description:totalPointsToDeposit Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void totalPointsToDeposit() {
		String totalDepositPoint = testData.get("PointsToDeposit").trim();
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement areaForDepositPoint = getObject(By.xpath("//label[contains(.,'" + useYearArray[useyearIterator]
					+ "')]/..//input[contains(@id,'points-to-be-deposited')]"));
			getElementInView(areaForDepositPoint);
			clickElementJSWithWait(areaForDepositPoint);
			// fieldDataEnter(areaForDepositPoint, totalDepositPoint);
			sendKeysBy(areaForDepositPoint, totalDepositPoint);
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

	/*
	 * Method: verifyHousekeepingSectionRCIDeposit
	 * Description:verifyHousekeepingSectionRCIDeposit Date: June/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void verifyHousekeepingSectionRCIDeposit() {
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement areaForHousekeeping = getObject(By.xpath("//label[contains(.,'" + useYearArray[useyearIterator]
					+ "')]/..//div[contains(@class,'housekeeping__credits')]"));
			if (verifyObjectDisplayed(areaForHousekeeping)) {
				getElementInView(areaForHousekeeping);
				WebElement housekeepingRequired = getObject(By.xpath("//label[contains(.,'"
						+ useYearArray[useyearIterator] + "')]/..//span[text() = 'Require to Deposit']/..//span[2]"));
				WebElement housekeepingAvailable = getObject(By.xpath("//label[contains(.,'"
						+ useYearArray[useyearIterator]
						+ "')]/..//div[contains(@class,'housekeeping__credits')]//span[text() = 'Housekeeping Credits Available']/..//span[2]"));
				if (getElementText(housekeepingRequired).isEmpty() || getElementText(housekeepingRequired) == null
						|| getElementText(housekeepingAvailable).isEmpty()
						|| getElementText(housekeepingAvailable) == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingSectionRCIDeposit",
							Status.FAIL, "Either HuseKeeping Availabe or required is not displayed");
				} else {
					if (Integer.parseInt(getElementText(housekeepingRequired)) > 0) {
						tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingSectionRCIDeposit",
								Status.PASS,
								"Housekeeping Required is for deposit is: " + getElementText(housekeepingRequired)
										+ " and available is: " + getElementText(housekeepingAvailable));
					} else {
						tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingSectionRCIDeposit",
								Status.FAIL, "Aount Entered but housekeeping value displayed is :"
										+ getElementText(housekeepingRequired));
					}
				}
			} else {
				tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingSectionRCIDeposit",
						Status.FAIL, "HouseKeeping Section not found");
			}
		}
	}

	/*
	 * Method: verifyHousekeepingCreditPurchased
	 * Description:verifyHousekeepingCreditPurchased Date: June/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void verifyHousekeepingCreditPurchased() {
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement areaForHousekeeping = getObject(By.xpath("//label[contains(.,'" + useYearArray[useyearIterator]
					+ "')]/..//div[contains(@class,'housekeeping__credits')]"));
			if (verifyObjectDisplayed(areaForHousekeeping)) {
				getElementInView(areaForHousekeeping);
				WebElement housekeepingPurchased = getObject(By.xpath("(//span[text() = 'Credits to Purchase'])["
						+ (useyearIterator + 1) + "]/following-sibling::span"));

				if (getElementText(housekeepingPurchased).isEmpty() || getElementText(housekeepingPurchased) == null) {
					tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditPurchased",
							Status.FAIL, "Housekeeping Purchased value is not displayed");
				} else {
					if (Integer.parseInt(getElementText(housekeepingPurchased)) > 0) {
						tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditPurchased",
								Status.PASS, "Housekeeping Purchased is: " + getElementText(housekeepingPurchased));
					} else {
						tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditPurchased",
								Status.FAIL, "Required Housekeeping purchased not dispalyed properly i.e value :"
										+ getElementText(housekeepingPurchased));
					}
				}
			} else {
				tcConfig.updateTestReporter("CUIPointDepositPage", "verifyHousekeepingCreditPurchased", Status.FAIL,
						"HouseKeeping Section not found");
			}
		}
	}

	/*
	 * Method: verifyCostHouskeepingPurchasedDeclaration
	 * Description:verifyCostHouskeepingPurchasedDeclaration Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyCostHouskeepingPurchasedDeclaration() {
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement declarationHousekeepingPurchased = getObject(By.xpath("//label[contains(.,'"
					+ useYearArray[useyearIterator] + "')]/..//span[contains(text(),'housekeeping credits needed')]"));
			if (verifyObjectDisplayed(declarationHousekeepingPurchased)) {
				tcConfig.updateTestReporter("CUIPointDepositPage", "verifyCostHouskeepingPurchasedDeclaration",
						Status.PASS, "Housekeeping Purchased declaration is shown as: "
								+ getElementText(declarationHousekeepingPurchased));
			} else {
				tcConfig.updateTestReporter("CUIPointDepositPage", "verifyCostHouskeepingPurchasedDeclaration",
						Status.FAIL, "Housekeeping Purchased declaration not displayed");
			}
		}
	}

	/*
	 * Method: verifyTotalCostHouskeepingPurchased
	 * Description:verifyTotalCostHouskeepingPurchased Date: June/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void verifyTotalCostHouskeepingPurchased() {
		double totalBreakdownAmountCalculated = 0;
		double individualTotalCost;
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement totalCostHousekeepingPurchased = getObject(By.xpath("//label[contains(.,'"
					+ useYearArray[useyearIterator] + "')]/..//span[text() = 'Total Cost']/following-sibling::span"));
			individualTotalCost = Double.parseDouble(
					getElementText(totalCostHousekeepingPurchased).split("\\$")[1].trim().replace(",", ""));
			totalBreakdownAmountCalculated = totalBreakdownAmountCalculated + individualTotalCost;
		}
		if (totalBreakdownAmountCalculated > 0) {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyTotalCostHouskeepingPurchased", Status.PASS,
					"Total Housekeeping Purchased is: " + totalBreakdownAmountCalculated);
		} else {
			tcConfig.updateTestReporter("CUIPointDepositPage", "verifyTotalCostHouskeepingPurchased", Status.PASS,
					"Failed to Calculate total Housekeeping Purchased");
		}
	}
}
