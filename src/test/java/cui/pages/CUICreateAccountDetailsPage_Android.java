package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICreateAccountDetailsPage_Android extends CUICreateAccountDetailsPage_Web {

	public static final Logger log = Logger.getLogger(CUICreateAccountDetailsPage_Android.class);

	public CUICreateAccountDetailsPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}

	/*
	 * Method: enterUsername Description:Enter Username Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void enterUsername(String usernameData, String strClassName) {
		getElementInView(userNameField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Username*", usernameData, 1);
		clickElementBy(accountDetailsHeader);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			changeTakenUsername(strClassName);
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIAccountDetailsPage", "enterUsername", Status.FAIL,
					"Error in username validation");
		}
	}

	/*
	 * Method: enterEmail Description:Enter Email Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void enterEmail(String emailData) {
		getElementInView(emailField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address*", emailData, 1);

	}

	/*
	 * Method: enterPassword Description:Enter Password Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void enterPassword(String passwordData) {
		getElementInView(passwordField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password", passwordData, 1);
	}

	/*
	 * Method: enterSecurityAnswer1 Description:Enter Security Answer 1 Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityAnswer1(String answer1) {
		getElementInView(getObject(selectQuestion1));
		Select q1drop = new Select(getObject(selectQuestion1));
		q1drop.selectByIndex(1);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer", answer1, 1);
	}

	/*
	 * Method: enterSecurityAnswer2 Description:Enter Security Answer 2 Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityAnswer2(String answer2) {
		getElementInView(getObject(selectQuestion2));
		Select q2drop = new Select(getObject(selectQuestion2));
		q2drop.selectByIndex(2);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer", answer2, 1);
	}

	/*
	 * Method: enterSecurityAnswer3 Description:Enter Security Answer 3 Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterSecurityAnswer3(String answer3) {
		getElementInView(getObject(selectQuestion3));
		Select q3drop = new Select(getObject(selectQuestion3));
		q3drop.selectByIndex(3);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer", answer3, 1);
	}

	/*
	 * Method: changeTakenUsername Description: Change Taken Username Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void changeTakenUsername(String strClass) {
		if (verifyObjectDisplayed(usernameError)) {
			// Available Username Data
			String strUsername = getElementText(availableUserName);
			tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "changeTakenUsername", Status.PASS,
					"Entered username is taken, selecting available username as: " + getElementText(availableUserName));
			clickElementBy(availableUserName);
			testData.put("CUI_username", strUsername);
			tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "changeTakenUsername", Status.PASS,
					"Username has been changed to available username : "+strUsername);
			
/*			try {
				writeDataInExcel(strClass, CUIScripts_Web.strTestName, "CUI_username", strUsername);
			} catch (Exception e) {
				tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "changeTakenUsername", Status.PASS,
						"Couldn't write username to datasheet");
			}
*/
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("CUICreateAccountDetailsPage", "changeTakenUsername", Status.PASS,
					"Entered Username is Unique");
		}

	}
}
