package cui.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBookConfirmationPage_Android extends CUIBookConfirmationPage_Web {
	public static final Logger log = Logger.getLogger(CUIBookConfirmationPage_Android.class);
	
	public CUIBookConfirmationPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		cancellationPolicyDetailsLink = By.xpath(
				"(//h3[.='Cancellation Policy']/parent::div/a[contains(@href,'canceling-reservation') and contains(.,'View')])[2]");
		cancellationPolicyText = By.xpath(
				"//h3[.='Cancellation Policy']/parent::div/p/span[contains(text(),'If your reservation is cancelled 15 days or more prior to the check-in date')]");
		upcomingVacationsLink = By
				.xpath("//section[contains(@class,'global-account--mobile')]//a[contains(@href,'/vacations')]/span");
		
	}

	CUIBookConfirmationPage_IOS bookConfPage = new CUIBookConfirmationPage_IOS(tcConfig);
	
	/*
	 * Method: navigateToUpcomingVacations Description: Navigate to upcoming
	 * booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Monideep Roychowdhury Changes By: KG
	 */
	public void navigateToUpcomingVacations() {
		bookConfPage.navigateToUpcomingVacations();
	}
	
	/*
	 * Method: validateChargesSummary Description: validate Charges Summary:
	 * June/2020 Author: Monideep Roychowdhury Changes By:
	 */
	public void validateChargesSummary() {
		bookConfPage.validateChargesSummary();
	
	}

	/*
	 * Method: validateCancellationPolicy Description: Navigate to upcoming
	 * booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Monideep Roychowdhury Changes By: Kamalesh
	 */

	public void validateCancellationPolicy() {
		bookConfPage.validateCancellationPolicy();
		
	}
	
	/*
	 * Method: validateReservationSummaryDetails Description: Navigate to
	 * upcoming booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Kamalesh Changes By:
	 */

	public void validateReservationSummaryDetails() {

		assertTrue(verifyObjectDisplayed(reservationSummaryHeader), "Reservation summary header not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Summary header and message present");

		assertTrue(verifyObjectDisplayed(reservationNumberHeader), "Reservation number header not present");
		assertTrue(getElementText(reservationNumber).length() > 0, "Reservation number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Number present : " + getElementText(reservationNumber));

		assertTrue(verifyObjectDisplayed(memberNumberHeader), "Member number header not present");
		assertTrue(getElementText(memberNumber).length() > 0, "Member number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Member Number present : " + getElementText(memberNumber));


	}

	
}
