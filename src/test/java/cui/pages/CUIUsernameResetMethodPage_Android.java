package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIUsernameResetMethodPage_Android extends CUIUsernameResetMethodPage_Web {

	public static final Logger log = Logger.getLogger(CUIUsernameResetMethodPage_Android.class);

	public CUIUsernameResetMethodPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}

	/*
	 * Method: enterSecurityQuestion1 Description:Security Ques1 enter Date:
	 * Mar/2020 Author: Unnat Jain Changes By: Monideep(Android)
	 */
	public void enterSecurityQuestion1(String securityAnswer) {
		getElementInView(question1Field);
		// clickElementBy(question1Field);
		// getObject(question1Field).sendKeys(securityAnswer);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer", securityAnswer);
	}

	/*
	 * Method: enterSecurityQuestion2 Description:Security Ques2 enter Date:
	 * Mar/2020 Author: Unnat Jain Changes By: Monideep(Android)
	 */
	public void enterSecurityQuestion2(String securityAnswer) {
		getElementInView(question2Field);
		// clickElementBy(question2Field);
		// getObject(question2Field).sendKeys(securityAnswer);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer", securityAnswer, 2);
	}

	/*
	 * Method: securityQuesErrorVal Description:Security Ques Error Validations
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void securityQuestionErrorValdation() {
		// Invalid Data enter
		String strInvalidCode = testData.get("invalidData");

		for (int noOfTries = 0; noOfTries < 3; noOfTries++) {
			// clearField(verificationCodeField);
			getElementInView(question1Field);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer", strInvalidCode);

			getElementInView(question2Field);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Your Answer", strInvalidCode);
			clickContinue();
			waitUntilElementVisibleBy(driver, errorBlock, "ExplicitLongWait");
			if (noOfTries < 2) {
				fieldPresenceVal(twoTryError, (noOfTries + 1) + " try error ", "CUIUsernameResetMethod",
						"securityQuesErrorVal", getElementText(twoTryError));
			} else if (noOfTries >= 2) {
				fieldPresenceVal(thirdTryError, (noOfTries + 1) + "rd try error ", "CUIUsernameResetMethod",
						"securityQuesErrorVal", getElementText(thirdTryError));
			} else {
				tcConfig.updateTestReporter("CUIUsernameResetMethod", "emailPageVal", Status.FAIL,
						"Error Validations Failed");
			}

		}
	}

}