package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUIPersonalInformationPopUp_Android extends CUIPersonalInformationPopUp_Web {

	public static final Logger log = Logger.getLogger(CUIPersonalInformationPopUp_Android.class);

	protected By zipcode = By.id("zipPostalCode");
	protected By email = By.id("email");
	protected By newUsernameField = By.id("newUsername");
	protected By addressLabel = By.xpath("//h5[contains(text(),'Address')]");
	protected By emailLabel = By.xpath("//h5[contains(text(),'EMAIL ADDRESS')]");


	public CUIPersonalInformationPopUp_Android(TestConfig tcconfig) {
		super(tcconfig);
		editPersonalInformation = By.xpath("//*[@class='personal-info']//button");
		accountInfoCancelCTA = By.xpath("//*[@id='personal-info-modal']//button[contains(@class,'cancel-changes')]");
		closedAccountInfoModal = By.xpath("//div[@class='personal-info']//button[contains(.,'Edit')]");
		addressLine1Error = By.xpath("//*[@id='error-addressLine1']");
	}

	/*
	 * Method: editEmailField Description: Edit Email Field in Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void editEmailField() {
		getElementInView(email);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email", testData.get("Email"), 2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * 
	 * /* Method: personalInfoCancelCTA Description: Close Manage Per Info Pop
	 * Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void personalInformationCancelCTA() {
		closePopUpModalAndroid(accountInfoCancelCTA);
	}
	
	/*
	 * Method: invalidAddress1Field Description: Invalid data Address Field in
	 * Per Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidAddress1Field() {
		//getElementInView(accountInfoAddressLine1);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Address Line 1*", testData.get("invalidData"), 1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(addressLabel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
	
	
	/*
	 * Method: invalidEmailField Description: Invalid data in Email Field in Per
	 * Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidEmailField() {
	
		getElementInView(accountInfoEmailField);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email",testData.get("invalidEmail"), 2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(emailLabel);
	}


}
