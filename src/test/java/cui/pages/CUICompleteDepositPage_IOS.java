package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICompleteDepositPage_IOS extends CUICompleteDepositPage_Web {

	public static final Logger log = Logger.getLogger(CUICompleteDepositPage_IOS.class);

	public CUICompleteDepositPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}

	public CUIBookPage_IOS bookPageIOS=new CUIBookPage_IOS(tcConfig);
	/*
	 * Method: dataEntryForCreditCard Description:Fill Details for Credit Card
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryForCreditCard() {
		bookPageIOS.dataEntryForCreditCard();
	}
	
	/*
	 * Method: dataEntryEmail Description:Fill Details for Email
	 * payment Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryEmail(){
		String emailID = testData.get("Email");
		getElementInView(fieldEmail);
		if (verifyObjectDisplayed(fieldEmail)) {
			elementPresenceVal(headerEmail, "Email Header", "CUICompleteDepositPage", "dataEntryEmail");
			elementPresenceVal(textEmailinformation, "Email Information", "CUICompleteDepositPage", "dataEntryEmail");	
			//fieldDataEnter(fieldEmail, emailID);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address", emailID, 1);

		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "dataEntryEmail", Status.FAIL,
					"Fields for Email data Entry not displayed");
		}
	}
}
