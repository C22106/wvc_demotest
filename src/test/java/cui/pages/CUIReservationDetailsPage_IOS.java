package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIReservationDetailsPage_IOS extends CUIReservationDetailsPage_Web {

	public static final Logger log = Logger.getLogger(CUIReservationDetailsPage_IOS.class);

	public CUIReservationDetailsPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		cancellationPolicyHeader = By
				.xpath("//div[contains(@class,'hide-for-large')]/div/h3");
		cancellationPolicyDescription = By
				.xpath("//div[contains(@class,'hide-for-large')]/div/h3/following-sibling::p");
		cancellationHeader = By
				.xpath("// div[contains(@class,'hide')]//header[contains(@class,'cancellation')]");
		cancellationCheckmark = By
				.xpath("// div[contains(@class,'hide')]//header[contains(@class,'cancellation')]//*[local-name()='svg']");
		cancellationDate = By
				.xpath("// div[contains(@class,'hide')]//span[contains(.,'Date Cancel')]/following-sibling::span");
		cancellationBy = By
				.xpath("// div[contains(@class,'hide')]//span[contains(.,'Cancelled By')]/following-sibling::span");
		viewCancellationPolicyLink = By.xpath("//div[contains(@class,'hide-for-large')]//a[contains(.,'Cancellation')]");
	}

	/*
	 * Method: mapLinkValidations Description:Map Section Elements Validations *
	 * Date field Date: July/2020 Author: Unnat Jain Changes By: NA
	 */

	public void mapLinkValidations() {
		String URL=driver.getCurrentUrl();
		clickElementByScriptExecutor("View on Map", 1);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, mapPageHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(mapPageHeader));
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "mapLinkValidations", Status.PASS,
				"Navigated to Map page");
		driver.get(URL);
		waitUntilElementVisibleBy(driver, resortName, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(resortName));
	}
}
