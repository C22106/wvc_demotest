package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIAccountCreatedPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIAccountCreatedPage_Web.class);

	public CUIAccountCreatedPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By successPageHeader = By.xpath("//div[@class='cardBanner' and contains(.,'ALL SET')]");
	protected By loginNowCTA = By.xpath("//a[contains(.,'Login Now')]");

	/*
	 * Method: accountCreatedPage Description:Account Created Page Val Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void accountCreatedPage() {
		waitUntilElementVisibleBy(driver, successPageHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(successPageHeader), "Success Page Header Not Displayed");
		elementPresenceVal(successPageHeader, "Account created and Header", "CUIAccountCreatedPage",
				"accountCreatedPage");
		Assert.assertTrue(verifyObjectDisplayed(loginNowCTA), "Login Now CTA not displayed");
		tcConfig.updateTestReporter("CUIAccountCreatedPage", "accountCreatedPage", Status.PASS,
				"Login now CTA is present in the page");
		clickElementBy(loginNowCTA);
	}
}
