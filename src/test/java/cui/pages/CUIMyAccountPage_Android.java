package cui.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;

public class CUIMyAccountPage_Android extends CUIMyAccountPage_Web {

	public static final Logger log = Logger.getLogger(CUIMyAccountPage_Android.class);

	private String location = "";
	protected By searchAvailCloseButton = By.xpath("//*[@class='search-avail-close-button']");
	protected By locationObj = By.xpath("//span[@class='pac-matched' and contains(text(),'" + location + "')]");
	protected By viewAllReservations = By
			.xpath("//*[contains(@class,'upcoming-reservations')]//span[text()='View All']");
	protected By myDashboard = By.xpath(
			"//div[@class='accountNavigation']//li[not(@role='treeitem')]//a[@href='/us/en/owner/account.html']");
	protected By dealsOffers = By
			.xpath("//div[contains(@class,'hide-for-large')]//div[contains(text(),'Deals & Offers')]");
	protected By hamburgerMenu = By.xpath("//button[@class='hamburger-menu']");
	protected By dashboardBtn = By.xpath("//li[@role='treeitem']//a[@href='/us/en/owner/account']");
	protected By dashboardBtnlist = By.xpath("//ul[contains(@class,'mobile-navigation')]//a[@href='/us/en/owner/account']");
	protected By dashboardMsg = By.xpath("//h4[contains(text(),'WELCOME')]");
	protected By offerDetails = By
			.xpath("//div[contains(@class,'hide-for-large')]//div[contains(text(),'OFFER DETAILS')]");

	public CUIMyAccountPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		textSearchHeader = By.xpath("//div[@id='search-availability-form']/h2[text() = 'Find Your Next Vacation']");
		textLocationField = By.xpath("//span[contains(text(),'Search for a location')]");
		reservationDetailsLink = By.xpath("//div[@class='upcoming-reservation-card__details-buttons']/a");
		headerLogoUpdated = By.xpath("//img[@class='mobile-logo' and @alt='Wyndham']");
		tipsSectionAboveVacation=By.xpath("//h5[.='MY OWNERSHIP']/../../../../preceding-sibling::section[@id='my-account__component--new-owner']");
		tipsSectionBelowVacation=By.xpath("//div[contains(@class,'bannerAlert-bottom')]/../preceding-sibling::div//section[@id='my-account__component--new-owner']");
	}

	/*
	 * Method: enterLocation Description:Select Location Date field Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: Monideep Roychowdhury(Android)
	 */

	public void enterLocation() throws InterruptedException {
		location = testData.get("CUI_Location").trim();// To store
														// location from
														// excel
		waitUntilElementVisibleBy(driver, textLocation, "ExplicitLongWait");
		if (verifyObjectDisplayed(textLocation)) {
			getElementInView(textLocation);
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Search for a location", location);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getObject(locationObj).click();
			tcConfig.updateTestReporter("CUIMyAccountPage", "enterLocation()", Status.PASS,
					"Location entered successfully");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "enterLocation()", Status.FAIL,
					"Failed while entering Location");
		}
	}

	/*
	 * Method: upcomingReservationSection Description:Reservation Section
	 * Elements Validations * Date field Date: Apr/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void upcomingReservationSection() {

		elementPresenceVal(viewAllReservations, "View All Link ", "CUIMyAccountPage", "upcomingReservationSection");
		// getObject(viewAllReservations).click();
		waitUntilElementVisibleBy(driver, upcomingReservationCards, "ExplicitLongWait");
		int totalCards = getList(upcomingReservationCards).size();
		elementListPresenceVal(reservationName, totalCards, "Card Name ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(cardImage, totalCards, "Card Images ", "CUIMyAccountPage", "upcomingReservationSection");
		elementListPresenceVal(cardAddress, totalCards, "Card Address", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(cardMapLink, totalCards, "Card Map Link ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(checkInDate, totalCards, "Check In date ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(checkOutDate, totalCards, "Check out Date", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(unitType, totalCards, "Unit Type ", "CUIMyAccountPage", "upcomingReservationSection");
		elementListPresenceVal(confirmationNumber, totalCards, "Confirmation Number ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(reservationDetailsLink, totalCards, "Reservation Details Link ", "CUIMyAccountPage",
				"upcomingReservationSection");
		// getObject(myDashboard).click();
	}

	/*
	 * Method: mapLinkValidations Description:Map Section Elements Validations *
	 * Date field Date: Apr/2020 Author: Unnat Jain Changes By:
	 * Monideep(Android)
	 */

	public void mapLinkValidations() {
		getElementInView(getList(cardMapLink).get(0));
		clickElementBy(getList(cardMapLink).get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, mapPageHeader, "ExplicitLongWait");
		if (verifyObjectDisplayed(mapPageHeader)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "mapLinkValidations", Status.PASS,
					"Navigated to Map page");
			((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.BACK));
			waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "mapLinkValidations", Status.FAIL,
					"Failed to navigate to Map page");
		}
	}

	/*
	 * Method: travelDealsViewLink Description:Travel Deals Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void travelDealsViewLink() {

		if (verifyObjectDisplayed(viewAllDealsLink)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsViewLink", Status.PASS,
					"Travel Deals Link Present");
			clickElementBy(viewAllDealsLink);
			waitUntilElementVisibleBy(driver, dealsOffers, "ExplicitLongWait");
			if (verifyObjectDisplayed(dealsOffers)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsViewLink", Status.PASS,
						"Navigated To Respective page");
				goBackToDashboard();
				waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsViewLink", Status.FAIL,
						"Navigation To Respective page failed");
			}

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsViewLink", Status.FAIL,
					"Travel Deals View All Link not Present");
		}
	}

	protected void goBackToDashboardUsingListItem() {
		
		getElementInView(hamburgerMenu);
		clickElementBy(hamburgerMenu);

		//scrollDownByPixelforObject(dashboardBtn);
		if (verifyObjectDisplayed(dashboardBtnlist)) {
			getElementInView(dashboardBtnlist);
			clickElementBy(dashboardBtnlist);
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "goBackToDashboard", Status.FAIL,
					"Dashboard button not present in hamburger menu");
		}
	}
	
	protected void goBackToDashboard() {
		
		getElementInView(hamburgerMenu);
		clickElementBy(hamburgerMenu);

		//scrollDownByPixelforObject(dashboardBtn);
		if (verifyObjectDisplayed(dashboardBtnlist)) {
			getElementInView(dashboardBtnlist);
			clickElementBy(dashboardBtnlist);
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "goBackToDashboard", Status.FAIL,
					"Dashboard button not present in hamburger menu");
		}
	}


	/*
	 * Method: travelDealsImageValidations Description:Travel Deals Section
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void travelDealsCardsValidations() {
		int totalTravelDeals = getList(totalDeals).size();
		travelDealsImageValidations(totalTravelDeals);
		travelDealsNameValidations(totalTravelDeals);
		travelDealsDescriptionValidations(totalTravelDeals);
		travelDealsDetailsCTA(totalTravelDeals);
		// checkLinkNavigationList(travelBookNowCta, homeBreadCrumb,
		// ownerFullName);

		List<WebElement> allElements = getList(travelBookNowCta);
		getElementInView(allElements.get(0));
		allElements.get(0).click();

		if (verifyObjectDisplayed(headerLogoUpdated)) {
			tcConfig.updateTestReporter("List Element", "checkFirstElement", Status.PASS,
					"Navigated to Associated page");
			goBackToDashboard();
			waitUntilElementVisibleBy(driver, dashboardMsg, "ExplicitLongWait");
		} else {
			tcConfig.updateTestReporter("List Element", "checkFirstElement", Status.FAIL,
					"Navigation to Associated page failed");
		}

	}

	/*
	 * Method: poupularResortViewLink Description:Travel Deals Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void poupularResortViewLink() {
		if (verifyObjectDisplayed(viewAllResortsLink)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "poupularResortViewLink", Status.PASS,
					"Popular Resort Link present ");
			clickElementBy(viewAllResortsLink);
			waitUntilElementVisibleBy(driver, headerLogoUpdated, "ExplicitLongWait");
			if (verifyObjectDisplayed(headerLogoUpdated)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "poupularResortViewLink", Status.PASS,
						"Navigated To Respective page");
				goBackToDashboardUsingListItem();
				waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "poupularResortViewLink", Status.FAIL,
						"Navigation To Respective page failed");
			}

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "poupularResortViewLink", Status.FAIL,
					"Popular Resorts View All Link not Present");
		}
	}
	
	/*
	 * Method: exploreAllResortVal Description: Wish List Section Date: Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void exploreAllResortVal() {
		if (verifyObjectDisplayed(exploreAllResorts)) {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "wishListSectionVal", Status.PASS,
					"Explore Resort CTA present");
			clickElementBy(exploreAllResorts);
			waitUntilElementVisibleBy(driver, headerLogoUpdated, "ExplicitLongWait");
			if (verifyObjectDisplayed(headerLogoUpdated)) {
				tcConfig.updateTestReporter("CUIOwnerAccountPage", "wishListSectionVal", Status.PASS,
						"Navigated To Associated Page");
				goBackToDashboardUsingListItem();
				waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
			} else {
				tcConfig.updateTestReporter("CUIOwnerAccountPage", "wishListSectionVal", Status.FAIL,
						"Navigation To Associated Page failed");
			}
		} else {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "wishListSectionVal", Status.FAIL,
					"Explore Resort CTA not present");
		}
	}
	
	/*
	 * Method: newOwnerCompletedTips Description:New Owner Completed Tips
	 * Validation Date: June/2020 Author: Unnat Jain Changes By: Kamalesh
	 */
	public void newOwnerCompletedTips() {
		refreshPage();
		getElementInView(tipsSectionBelowVacation);
		if (verifyObjectDisplayed(tipsSectionBelowVacation)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.PASS,
					"Tips Section present above the footer section after completing ");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.FAIL,
					"Tips Section not present above the footer after completing ");
		}
	}

}
