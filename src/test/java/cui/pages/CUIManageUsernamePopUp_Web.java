package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIManageUsernamePopUp_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIManageUsernamePopUp_Web.class);

	public CUIManageUsernamePopUp_Web(TestConfig tcconfig) {
		super(tcconfig);
	

	}

	protected By editUsernameLink = By.xpath("//button[contains(.,'Edit Username')]");
	protected By usernamePopUpHeader = By
			.xpath("//div[@id='username-modal' and @aria-hidden='false']//p[contains(.,'MANAGE USERNAME')]");

	/* Manage Usrename Pop Up Modal */
	protected By usernameCloseCta = By
			.xpath("//div[@id='username-modal' and @aria-hidden='false']//button[@aria-label='Close modal']");
	protected By currentUsernameField = By.id("currentUsername");
	protected By newUsernameField = By.id("newUsername");
	protected By usernameCancelCTA = By
			.xpath("//div[@id='username-modal' and @aria-hidden='false']//div/button[contains(.,'Cancel')]");
	protected By usernameSaveCta = By
			.xpath("//div[@id='username-modal' and @aria-hidden='false']//div/button[contains(.,'Save')]");
	protected By closedUsernameModal = By.xpath("//div[@id='username-modal' and @aria-hidden='true']");
	protected By ownerUserName = By.xpath("//h5[contains(.,'USERNAME')]/following-sibling::p");
	protected By takenUsernameError = By.id("error-newUsername");
	protected By availableUsername = By.xpath("//p[contains(.,'Available Username')]//button");

	/*
	 * Method: usernamePopUpModalVal Description: Navigate to Manage username
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void usernamePopUpModalNavigation() {
		waitUntilElementVisibleBy(driver, editUsernameLink,  "ExplicitLongWait");
		getElementInView(editUsernameLink);
		clickElementBy(editUsernameLink);

		waitUntilElementVisibleBy(driver, usernamePopUpHeader,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(usernamePopUpHeader),"Username Pop Up Header not present");
		elementPresenceVal(usernamePopUpHeader, "Username Pop Up Modal with header", "CUIAccountSettingPage",
				"usernamePopUpModalNav");
	}

	/*
	 * Method: editNewUsernameField Description: Edit New Username Field in
	 * Username Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void editNewUsernameField() {
		enterDataInPopUpField(newUsernameField, testData.get("Username"), "Yes");
	}

	/*
	 * Method: takenUsernameData Description: Add taken username in Username Pop
	 * Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void takenUsernameData() {
		enterDataInPopUpField(newUsernameField, testData.get("Invalid_UserName"), "Yes");
		waitForSometime(tcConfig.getConfig().get("WaitForASec"));
		sendKeyboardKeys(Keys.TAB);
	}

	/*
	 * Method: takenUsernameError Description: aken Username error in Username
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void takenUsernameError() {
		waitUntilElementVisibleBy(driver, takenUsernameError,  "ExplicitLongWait");
		if (verifyObjectDisplayed(takenUsernameError)) {
			tcConfig.updateTestReporter("CUIManageUsernamePage", "takenUsernameError", Status.PASS,
					"Taken Username Error displayed");
		} else {
			tcConfig.updateTestReporter("CUIManageUsernamePage", "takenUsernameError", Status.FAIL,
					"Taken Username Error did not get displayed");
		}
	}

	/*
	 * Method: availableUsername Description:Available username validation in
	 * Username Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void availableUsername() {
		waitUntilElementVisibleBy(driver, availableUsername,  "ExplicitLongWait");
		if (verifyObjectDisplayed(availableUsername)) {
			tcConfig.updateTestReporter("CUIManageUsernamePage", "availaleUsername", Status.PASS,
					"Manage Username Suggetion displayed");
		} else {
			tcConfig.updateTestReporter("CUIManageUsernamePage", "availaleUsername", Status.FAIL,
					"Manage Username Suggetion not displayed");
		}
	}

	/*
	 * Method: saveUsernameChanges Description: Save Username Changes Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void saveUsernameChanges() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(usernameSaveCta);

		waitUntilElementInVisible(driver, usernameSaveCta,  "ExplicitLongWait");
		
		sendKeyboardKeys(Keys.TAB);
		if (verifyObjectDisplayed(newUsernameField)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveUsernameChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveUsernameChanges", Status.PASS,
					"Pop Up Modal  Closed");
			savedChangesValidation(ownerUserName, "Username", "Username");
		}
	}

	/*
	 * /* Method: usernameCancelCTA Description: Close Manage Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void usernameCancelCTA() {

		
		closePopUpModal(usernameCancelCTA, closedUsernameModal, newUsernameField, Status.FAIL);
	}

	/*
	 * Method: usernameModalVal Description: Validate Manage username Pop Up
	 * Account Setting Page Date: MAr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void usernameModalValidation() {
		if (getElementValue(currentUsernameField).equalsIgnoreCase(testData.get("CUI_username"))) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "usernameModalVal", Status.PASS,
					"Current username field present with value: " + getElementValue(currentUsernameField));
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "usernameModalVal", Status.FAIL,
					"Current username field validation error ");
		}

		elementPresenceVal(newUsernameField, "New Username field", "CUIAccountSettingPage", "usernameModalVal");
		elementPresenceVal(usernameCancelCTA, "Cancel CTA", "CUIAccountSettingPage", "usernameModalVal");
		elementPresenceVal(usernameSaveCta, "SAVE CTA", "CUIAccountSettingPage", "usernameModalVal");
		closePopUpModal(usernameCloseCta, closedUsernameModal, newUsernameField, Status.PASS);
	}

	/*
	 * Method: editOldUsernameField Description: Edit username field in Username
	 * Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void editOldUsernameField() {
		enterDataInPopUpField(newUsernameField, testData.get("CUI_username"), "Yes");
	}

	/*
	 * Method: saveChanges Description: Save Changes in Username Pop Up Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void saveChanges() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(usernameSaveCta);

		waitUntilElementVisibleBy(driver, closedUsernameModal,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(closedUsernameModal),"Close Username Modal not present");
	}
}