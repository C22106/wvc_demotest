package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIMyAccountPage_IOS extends CUIMyAccountPage_Web {

	public static final Logger log = Logger.getLogger(CUIMyAccountPage_IOS.class);
	public By modifyReservationHeader=By.xpath("//h1[text()='Modify Reservation']");
	public CUIMyAccountPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		textSearchHeader = By.xpath("//div[@id='search-availability-form']/h2[text() = 'Find Your Next Vacation']");
		textLocationField = By.xpath("//span[contains(text(), 'Search for a location')]");
		viewAllLink = By
				.xpath("//span[contains(text(),'View All')and contains(@class,'text--mobile')]");
		modifyReservation = By.xpath("//div[@class='upcoming-reservation-card__modify-cancel-buttons']/a");
		headerLogoUpdated=By.xpath("//img[@class='mobile-logo']");
		tipsSectionBelowVacation=By.xpath("//div[contains(@class,'bannerAlert-bottom')]/../preceding-sibling::div//section[@id='my-account__component--new-owner']");
		tipsSectionAboveVacation=By.xpath("//h5[.='MY OWNERSHIP']/../../../../preceding-sibling::section[@id='my-account__component--new-owner']");
	}
	/*
	 * Method: enterLocation Description:Select Location Date field Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: Kamalesh Gupta
	 */

	public void enterLocation() throws InterruptedException {
		String location = testData.get("CUI_Location").trim();// To store
																// location from
																// excel
		waitUntilElementVisibleBy(driver, textLocation, "ExplicitLongWait");
		if (verifyObjectDisplayed(textLocation)) {
			getElementInView(textLocation);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Search for a location", location);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getObject(textLocation).sendKeys(Keys.ARROW_DOWN);
			getObject(textLocation).sendKeys(Keys.ENTER);
			tcConfig.updateTestReporter("CUIMyAccountPage", "enterLocation()", Status.PASS,
					"Location entered successfully");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "enterLocation()", Status.FAIL,
					"Failed while entering Location");
		}
	}
	
	/*
	 * Method: mapLinkValidations Description:Map Section Elements Validations *
	 * Date field Date: Apr/2020 Author: Unnat Jain Changes By: Kamalesh
	 */

	public void mapLinkValidations() {
		getElementInView(getList(cardMapLink).get(0));
		clickElementByScriptExecutor("View on Map", 1);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, mapPageHeader, "ExplicitLongWait");
		if (verifyObjectDisplayed(mapPageHeader)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "mapLinkValidations", Status.PASS,
					"Navigated to Map page");
			driver.navigate().back();
			waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "mapLinkValidations", Status.FAIL,
					"Failed to navigate to Map page");
		}
	}
	
	/*
	 * Method: reservationLinkValidations Description:Reservation Link Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationLinkValidations() {
		String strURL=driver.getCurrentUrl();
		getElementInView(getList(reservationDetailsLink).get(0));
		clickElementBy(getList(reservationDetailsLink).get(0));
		waitUntilElementVisibleBy(driver, reservationSummaryPage, "ExplicitLongWait");
		if (verifyObjectDisplayed(reservationSummaryPage)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "reservationLinkValidations", Status.PASS,
					"Navigated to Details page");
			//navigateBack();
			driver.get(strURL);
			waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "reservationLinkValidations", Status.FAIL,
					"Failed to navigate to Details page");
		}
	}
	
	/*
	 * Method: modifyLinkValidations Description:Modify Link Section Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void modifyLinkValidations() {
		String strURL=driver.getCurrentUrl();
		try {
			getElementInView(getList(reservationDetailsLink).get(0));
			clickElementBy(getList(modifyReservation).get(0));
			waitUntilElementVisibleBy(driver, modifyReservationHeader, "ExplicitLongWait");
			if (verifyObjectDisplayed(modifyReservationHeader)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "modifyLinkValidations", Status.PASS,
						"Navigated to Modify page");
				driver.get(strURL);
				
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "modifyLinkValidations", Status.FAIL,
						"Failed to navigate to Modify page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "modifyLinkValidations", Status.PASS,
					"Modify Link Not Present in the card");
		}
	}

	
	/*
	 * Method: travelDealsImageValidations Description:Travel Deals Section
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void travelDealsCardsValidations() {
		int totalTravelDeals = getList(totalDeals).size();
		travelDealsImageValidations(totalTravelDeals);
		travelDealsNameValidations(totalTravelDeals);
		travelDealsDescriptionValidations(totalTravelDeals);
		travelDealsDetailsCTA(totalTravelDeals);
		checkLinkNavigationList(travelBookNowCta, headerLogoUpdated, ownerFullName);

	}
	
	/*
	 * Method: newOwnerCompletedTips Description:New Owner Completed Tips
	 * Validation Date: June/2020 Author: Unnat Jain Changes By: Kamalesh
	 */
	public void newOwnerCompletedTips() {
		refreshPage();
		getElementInView(tipsSectionBelowVacation);
		if (verifyObjectDisplayed(tipsSectionBelowVacation)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.PASS,
					"Tips Section present above the footer section after completing ");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.FAIL,
					"Tips Section not present above the footer after completing ");
		}
	}

}
