package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIManagePasswordPopUp_IOS extends CUIManagePasswordPopUp_Web {

	public static final Logger log = Logger.getLogger(CUIManagePasswordPopUp_IOS.class);
	public By passwordHeader=By.xpath("//fieldset/h5[text()='PASSWORD']");
	public CUIManagePasswordPopUp_IOS(TestConfig tcconfig) {
		super(tcconfig);
		passwordCancelCTA = By
				.xpath("//div[@id='password-modal' and @aria-hidden='false']//button[contains(@class,'cancel-changes-button')]");
		
		editPasswordLink = By.xpath("//div[contains(@class,'manage-password')]//button[contains(.,'Edit')]");

	}
	
	
	
	/*
	 * /* Method: enterNewPassword Description: Enter New Password Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta
	 */
	public void enterNewPassword() throws Exception {
		// To Decode passwords
		String decodedPassword = decodePassword(testData.get("CUI_password"));
		try {
			getPopUpElementInView(newPasswordField, "Yes");
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "New Password", decodedPassword,1);	
			tcConfig.updateTestReporter("CUIManagePasswordPopUp_IOS", "enterNewPassword", Status.PASS,
					"Password field entered successfully");
			clickElementBy(passwordHeader);
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIManagePasswordPopUp_IOS", "enterNewPassword", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	
	/*
	 * *************************Method: closePopUpModal ***
	 * *************Description Close Any Pop Up Modal**
	 */

	public void closePopUpModal(By closeCTAElement, By closedModalElement, By elementFromPopUp, Status status) {
		if (verifyObjectDisplayed(closeCTAElement)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
					"Cancel Cta present");
			clickElementBy(closeCTAElement);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (isAlertPresent()) {
				driver.switchTo().alert().accept();
				driver.switchTo().defaultContent();
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", Status.PASS,
						"Successfully accepetd the alert popup");
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", status,
						"No popup came while Closing the pop up");
			}
			waitUntilElementVisibleBy(driver, closedModalElement, "ExplicitLongWait");
			
			if (verifyObjectDisplayed(elementFromPopUp)) {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
						"Pop Up Modal did not Closed");
			} else {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
						"Pop Up Modal  Closed");
			}

		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
					"Cancel CTA not present");
		}
	}
	
	/*
	 * Method: invalidNewPassword Description: Invalid Data Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void invalidNewPassword() {
		//fieldDataEnter(newPasswordField, testData.get("Password"));
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "New Password", testData.get("Password"),1);
		clickElementBy(passwordHeader);
		
	}
	
	/*
	 * Method: incorrectDataCurrent Description: Invalid Data Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void incorrectDataCurrent() {
		
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Current Password*", testData.get("Invalid_Pass"),1);
		clickElementBy(passwordHeader);
	}
	
	/*
	 * Method: savePasswordChanges Description: Save Username Changes Account
	 * Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void savePasswordChanges() {
		clickElementBy(passwordHeader);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(passwordSaveCta);
		waitUntilElementVisibleBy(driver, closedPasswordModal, "ExplicitLongWait");
		if (verifyObjectDisplayed(newPasswordField)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "savePasswordChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "savePasswordChanges", Status.PASS,
					"Pop Up Modal  Closed");
			try {
				passwordVerification("Password", "No");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				tcConfig.updateTestReporter("CUIAccountSettingPage", "savePasswordChanges", Status.FAIL,
						"Password Changes Verification failed");
			}
		}
	}
}
