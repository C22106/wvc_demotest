package cui.pages;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import trip.pages.TRIPFindOwnerPage_Web;
import trip.pages.TRIPLoginPage_Web;
import trip.pages.TRIPProfileDashboardPage_Web;
import trip.pages.TRIPReservationDetailsPage_Web;
import trip.pages.TRIPReservationPage_Web;

public class TripPage_Web extends CUIBasePage {
	public static final Logger log = Logger.getLogger(TripPage_Web.class);

	public TripPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		
	}

	public TRIPLoginPage_Web tripLogIn=new TRIPLoginPage_Web(tcConfig);
	public TRIPFindOwnerPage_Web tripFindOwner=new TRIPFindOwnerPage_Web(tcConfig);
	public TRIPProfileDashboardPage_Web tripProfileDashboard=new TRIPProfileDashboardPage_Web(tcConfig);
	public TRIPReservationPage_Web tripReservationPage=new TRIPReservationPage_Web(tcConfig);
	public TRIPReservationDetailsPage_Web tripReservationDetailsPage=new TRIPReservationDetailsPage_Web(tcConfig);
	
	
	protected By fieldUsername = By.xpath("//input[@id = 'userName']");
	protected By fieldPassword = By.xpath("//input[@id = 'password']");
	protected By buttonSignIn = By.xpath("//input[@id = 'signIn']");
	protected By fieldMemberSearch = By.xpath("//input[@id = 'quickSearch']");
	protected By sidebarAvatar = By.id("sidebar-avatar");
	protected By buttonSignOut = By.id("signOut");
	
	/*
	 * Method: launchApplication Description: launch the TRIP Application
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void launchApplication() {
		getBrowserName();
		navigateToURL(tcConfig.getConfig().get("TRIPURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(fieldUsername), "Failed to Launch Application");
		tcConfig.updateTestReporter("TRIPLoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}
	
	/*
	 * Method: setUserName Description: Set UserName
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setUserName() {
		tripLogIn.setUserName();
	}
	
	/*
	 * Method: setPassword Description: Set Password
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setPassword() throws UnsupportedEncodingException {
		tripLogIn.setPassword();
	}
	
	/*
	 * Method: clickButtonLogin Description: Click Login Button
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickButtonLogin() {
		tripLogIn.clickButtonLogin();
	}
	
	/*
	 * Method: logoutApplication Description: Logout out From TRIP Application
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void logoutApplication() throws Exception {
		tripLogIn.logoutApplication();
	}
	
	/*
	 * Method: searchForMember Description: Search For Member
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void searchForMember(){
		tripFindOwner.searchForMember();
	}
	
	/*
	 * Method: clickSearchButton Description: Click Member Search Button
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickSearchButton(){
		tripFindOwner.clickSearchButton();
	}
	
	/*
	 * Method: selectMember Description: Select Member From List
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectMember() throws Exception {
		tripFindOwner.selectMember();
	}
	
	/*
	 * Method: profileHeaderClick Description: profile Header Click Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void profileHeaderClick() throws Exception {
		
		tripProfileDashboard.profileHeaderClick();
	}

	/*
	 * Method: reservationLinkClick Description: Reservation Link Click Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void reservationLinkClick() throws Exception {
		tripProfileDashboard.reservationLinkClick();
	}
	
	/*
	 * Method: feedReservationNumber Description: feed Reservation Number Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void feedReservationNumber(){
		tripReservationPage.feedReservationNumber();
	}
	
	/*
	 * Method: reservationSelect Description: reservation Select Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void reservationSelect() throws Exception {
		tripReservationPage.reservationSelect();
	}

	/**
	 * Method: fetchBenefits Description: Fetch the Benefits Details Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 */
	public void fetchBenefits(String iterator) {
		tripReservationDetailsPage.fetchBenefits(iterator);
	}
	
	/**
	 * Method: compareBenefits Description: compare Benefits before and after
	 * modifications Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void compareBenefits(String firstIterator, String secondIterator) {
		tripReservationDetailsPage.compareBenefits(firstIterator, secondIterator);
	}
}
