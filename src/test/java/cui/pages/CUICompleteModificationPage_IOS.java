package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICompleteModificationPage_IOS extends CUICompleteModificationPage_Web {

	public static final Logger log = Logger.getLogger(CUICompleteModificationPage_IOS.class);

	public CUICompleteModificationPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}
	public By expandReservationDetails=By.xpath("//button[@class = 'reservation-summary__headline-arrow']");
	public CUIBookPage_IOS bookPageObjectIOS = new CUIBookPage_IOS(tcConfig);

	public CUIModifyReservationPage_IOS modifyPage_IOS = new CUIModifyReservationPage_IOS(tcConfig);
	/*
	 * Method: validateSectionReservationSummary Description:validate Section Reservation Summary Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateSectionReservationSummary(){
		modifyPage_IOS.checkSectionReservationSummary();
		modifyPage_IOS.checkReservationSummaryDetails();
	}
	/*
	 * Method: makePayment
	 * Description: makePayment
	 * Date: June/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void makePayment(){
		String paymentMethod = testData.get("PaymentBy");
		bookPageObject.selectPaymentMethod();
		if (paymentMethod.contains("PayPal")) {
			bookPageObjectIOS.paymentViaPaypal();
		}else if (paymentMethod.contains("CreditCard")) {
			bookPageObjectIOS.dataEntryForCreditCard();
		}	
	}
	
	/*
	 * Method: updatedDateValidation Description: Verify Date is Updated Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void updatedDateValidation(String updatedDate) {
		clickElementBy(expandReservationDetails);
		Assert.assertTrue(getElementText(modifiedDate).trim().contains(testData.get(updatedDate)),
				"Updated Date not displayed in right window ");
		tcConfig.updateTestReporter("CUICompleteModificationPage", "updatedDateValidation", Status.PASS,
				"Updated Date is displayed as " + getElementText(modifiedDate));
	}
}