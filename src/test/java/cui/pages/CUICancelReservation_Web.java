package cui.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICancelReservation_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUICancelReservation_Web.class);

	public CUICancelReservation_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By reservationNumber = By.xpath("//h3[contains(@class,'reservation-id')]");
	protected By reservationName = By.xpath("//h3[contains(@class,'reservation-name')]");
	protected By checkInDate = By.xpath("//section[contains(@class,'check-in')]/span/following-sibling::span");
	protected By checkOutDate = By.xpath("//section[contains(@class,'check-out')]/span/following-sibling::span");
	protected By reimbursementTitle = By.xpath("//main/h5[contains(.,'Reimbursement')]");
	protected By membershipReimburse = By.xpath("//span[contains(.,'Membership Reimbursement')]");
	protected By membershipCharges = By.xpath("//span[contains(.,'Membership Charge')]");
	protected By pointsDropdown = By.xpath("(//h3/span[contains(.,'points')] | //h3/span[contains(.,'Points')])");
	protected By totalPoints = By.xpath("//span[contains(.,'oints')]/parent::h3/following-sibling::span");
	protected By pointsDropDownText = By
			.xpath("//span[contains(.,'oints')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div");
	protected By pointsDropDownValue = By.xpath(
			"//span[contains(.,'oints')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span");
	protected By pointsDistributionText = By.xpath(
			"//span[contains(.,'oints')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span[1]");
	protected By totalPointsDistribution = By.xpath(
			"//span[contains(.,'oints')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span[2]");
	protected By houseKeepingCredits = By.xpath("//h3//span[contains(@class,'has-tip')]");
	protected By houseKeepingPoints = By
			.xpath("//span[contains(@class,'has-tip')]/ancestor::div[contains(@class,'dropdown')]/span");
	protected By houseKeepingDropdownText = By.xpath(
			"//span[contains(@class,'has-tip')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div");
	protected By houseKeepingDropdownValue = By.xpath(
			"//span[contains(@class,'has-tip')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span");
	protected By creditsDistributionText = By.xpath(
			"//span[contains(@class,'has-tip')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span[1]");
	protected By totalCreditsDistribution = By.xpath(
			"//span[contains(@class,'has-tip')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span[2]");
	protected By nonRefundableText = By.xpath(
			"//span[contains(@class,'has-tip')]/ancestor::section[contains(@class,'dropdown')]/following-sibling::div/small");

	protected By policyHeader = By.xpath("// section[contains(@class,'policy')]//h5");
	protected By policyText = By.xpath("// section[contains(@class,'policy')]//p");
	protected By policyCTA = By.xpath("// section[contains(@class,'policy')]//a");
	protected By cancelCTA = By.xpath("// footer[contains(@class,'cancel')]//a[contains(.,'Cancel')]");

	protected By cancelledResortHeader = By.xpath("// div[contains(@class,'reservation')]//h1/a");
	public static String pointsReimbursed;

	protected By forfeitError = By.xpath("//p[@id='error-message']/span[contains(.,'forfeited')]");
	protected By forfeitHeader = By.xpath("//h5[contains(.,'Forfeit Summary')]");

	protected By paymentReimburse = By.xpath("//span[contains(.,'Payment Reimbursement')]");
	protected By paymentImage = By
			.xpath("//span[contains(.,'Payment')]/../div//img[contains(@class,'cancel-reservation')]");
	protected By creditCardNumber = By.xpath(
			"//span[contains(.,'Payment')]/../div//img[contains(@class,'cancel-reservation')]/../span[contains(.,'Ending')]");
	protected By rentedPoints = By
			.xpath("//span[contains(.,'Payment')]/../div//span[contains(.,'Rented')]/following-sibling::span");
	protected By housekeepingCredits = By
			.xpath("//span[contains(.,'Payment')]/../div//span[contains(.,'Housekeeping')]/following-sibling::span");
	protected By totalPayment = By
			.xpath("// div[contains(@class,'footer')]//span[contains(.,'Total')]/following-sibling::span");

	/*
	 * Method: reservationNumberCheck Description: Reservation Number Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationNumberCheck() {
		// reservation number from test data
		String strReservationID = testData.get("ReservationNumber").toUpperCase();
		if (getElementText(reservationNumber).toUpperCase().contains(strReservationID)) {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationNumberCheck", Status.PASS,
					"Reservation Number Displayed correctly " + strReservationID);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationNumberCheck", Status.FAIL,
					"Reservation Number not Displayed correctly " + strReservationID);
		}

	}

	/*
	 * Method: reservationResortName Description: Reservation Resort Name Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationResortName() {
		// reservation Resort Name from test data
		String strReservationName = testData.get("ReservationResortName").toUpperCase();
		if (getElementText(reservationName).toUpperCase().contains(strReservationName)) {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationResortName", Status.PASS,
					"Reservation Resort Name Displayed correctly " + strReservationName);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationResortName", Status.FAIL,
					"Reservation Resort name not Displayed correctly " + strReservationName);
		}
	}

	/*
	 * Method: checkInDateCheck Description: Check In Date check Date: June/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void checkInDateCheck() {
		// reservation Check In from test data
		String strReservationChekcInDate = testData.get("Checkindate");
		if (getElementText(checkInDate).contains(strReservationChekcInDate)) {
			tcConfig.updateTestReporter("CUICancelReservation", "checkInDateCheck", Status.PASS,
					"Reservation Check In Date Displayed correctly " + strReservationChekcInDate);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "checkInDateCheck", Status.FAIL,
					"Reservation Check In Date not Displayed correctly " + strReservationChekcInDate);
		}
	}

	/*
	 * Method: checkOutDateCheck Description: Reservation Resort Name Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkOutDateCheck() {
		// reservation Check Out from test data
		String strReservationCheckOutDate = testData.get("Checkoutdate").toUpperCase();
		if (getElementText(checkOutDate).contains(strReservationCheckOutDate)) {
			tcConfig.updateTestReporter("CUICancelReservation", "checkOutDateCheck", Status.PASS,
					"Reservation  Check Out Date Displayed correctly " + strReservationCheckOutDate);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "checkOutDateCheck", Status.FAIL,
					"Reservation  Check Out Date not Displayed correctly " + strReservationCheckOutDate);
		}
	}

	/*
	 * Method: reimbursementSectionHeader Description: Reimbursement Section
	 * Headers validation Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reimbursementSectionHeader() {
		elementPresenceVal(reimbursementTitle, "Reimbursement Header ", "CUICancelReservation",
				"reimbursementSectionHeader");

	}

	/*
	 * Method: forfeitSectionHeader Description: Forfeit Section Headers
	 * validation Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void forfeitSectionHeader() {
		elementPresenceVal(forfeitHeader, "Forfeit Header ", "CUICancelReservation", "forfeitSectionHeader");
		elementPresenceVal(forfeitError, "Forfeit Error ", "CUICancelReservation", "forfeitSectionHeader");

	}

	/*
	 * Method: cancellationSection Description: Cancellation Section validation
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancellationSection() {
		getPopUpElementInView(policyHeader, "No");
		elementPresenceVal(policyHeader, "Cancellation Policy Header ", "CUICancelReservation", "cancellationSection");
		elementPresenceVal(policyText, "Cancellation Policy Description ", "CUICancelReservation",
				"cancellationSection");
		elementPresenceVal(policyCTA, "Cancellation Policy CTA ", "CUICancelReservation", "cancellationSection");

	}

	/*
	 * Method: membershipReimbursementSection Description: Membership
	 * reimbursement Section validation Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void membershipReimbursementSection() {
		pointsReimbursed = getElementText(totalPoints);
		elementPresenceVal(membershipReimburse, "Membership Reimburse Header ", "CUICancelReservation",
				"membershipReimbursementSection");
		fieldPresenceVal(pointsDropdown, "Total Reimburse Points header ", "CUICancelReservation",
				"membershipReimbursementSection", " with points " + pointsReimbursed);
		fieldPresenceVal(houseKeepingCredits, "Total Housekeeping Credits header ", "CUICancelReservation",
				"membershipReimbursementSection", " with Credits: " + getElementText(houseKeepingPoints));
		elementPresenceVal(nonRefundableText, "Non Refundable text ", "CUICancelReservation",
				"membershipReimbursementSection");

	}

	/*
	 * Method: membershipChargesSection Description: Membership Charges Section
	 * validation Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void membershipChargesSection() {
		pointsReimbursed = getElementText(totalPoints);
		elementPresenceVal(membershipCharges, "Membership Charges Header ", "CUICancelReservation",
				"membershipChargesSection");
		fieldPresenceVal(pointsDropdown, "Total Reimburse Points header ", "CUICancelReservation",
				"membershipChargesSection", " with points " + pointsReimbursed);
		fieldPresenceVal(houseKeepingCredits, "Total Housekeeping Credits header ", "CUICancelReservation",
				"membershipChargesSection", " with Credits: " + getElementText(houseKeepingPoints));
		elementPresenceVal(nonRefundableText, "Non Refundable text ", "CUICancelReservation",
				"membershipChargesSection");

	}

	/*
	 * Method: cancelButtonClick Description: Cancel Button Click Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelButtonClick() {
		Assert.assertTrue(verifyObjectDisplayed(cancelCTA), "Cancel CTA not present");
		tcConfig.updateTestReporter("CUICancelReservation", "cancelButtonClick", Status.PASS,
				"Cancel Cta present in the Pop Up");
		clickElementBy(cancelCTA);
		waitUntilElementVisibleBy(driver, cancelledResortHeader, "ExplicitLongWait");

	}

	/*
	 * Method: pointsDropDownValidation Description: Points Drop Down
	 * Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsDropDownValidation() {
		getPopUpElementInView(pointsDropdown, "Yes");
		clickElementBy(pointsDropdown);
		waitUntilElementVisibleBy(driver, pointsDropDownText, "ExplicitLongWait");
		List<WebElement> dropDownList = getList(pointsDistributionText);
		if (dropDownList.size() > 0) {
			for (int textCount = 0; textCount < dropDownList.size(); textCount++) {
				tcConfig.updateTestReporter("CUICancelReservation", "pointsDropDownValidation", Status.PASS,
						"Points Drop Down Text displayed as: " + (dropDownList).get(textCount).getText()
								+ " with points " + distributedPointsValidation(textCount, totalPointsDistribution));
			}
			pointsDropDownTextValidation();
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "pointsDropDownValidation", Status.FAIL,
					"Points Drop Down Text not present");
		}
	}

	/*
	 * Method: pointsDropDownTextValidation Description: Points Drop Down
	 * Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsDropDownTextValidation() {
		if (testData.get("PointsTextValidation").contains("Yes")) {
			List<WebElement> dropDownList = getList(pointsDistributionText);

			for (int textCount = 0; textCount < dropDownList.size(); textCount++) {
				if ((dropDownList).get(textCount).getText().toLowerCase()
						.contains(testData.get("PointsText").toLowerCase())) {
					tcConfig.updateTestReporter("CUICancelReservation", "pointsDropDownTextValidation", Status.PASS,
							"Points Drop Down Text displayed as: " + (dropDownList).get(textCount).getText()
									+ " with points "
									+ distributedPointsValidation(textCount, totalPointsDistribution));
				} else if (textCount + 1 == dropDownList.size()) {
					tcConfig.updateTestReporter("CUICancelReservation", "pointsDropDownTextValidation", Status.FAIL,
							testData.get("PointsTextValidation") + " Points Drop Down Text not present");
				}
			}
		}
	}

	/*
	 * Method: distributedPointsValidation Description: Drop Down Validations
	 * for all Points/ Credits Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public int distributedPointsValidation(int pointsCount, By element) {
		String dropDownList = getList(element).get(pointsCount).getText().trim();
		if (dropDownList.contains(",")) {
			dropDownList = dropDownList.replace(",", "");
		}
		int pointsInt = Integer.parseInt(dropDownList);
		
		return pointsInt;
	}

	/*
	 * Method: houseKeepingDropDownValidation Description: Housekeeping Drop
	 * Down Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void houseKeepingDropDownValidation() {
		getPopUpElementInView(houseKeepingCredits, "Yes");
		clickElementJSWithWait(houseKeepingCredits);
		waitUntilElementVisibleBy(driver, houseKeepingDropdownText, "ExplicitLongWait");
		List<WebElement> dropDownList = getList(creditsDistributionText);
		if (dropDownList.size() > 0) {
			for (int textCount = 0; textCount < dropDownList.size(); textCount++) {
				tcConfig.updateTestReporter("CUICancelReservation", "houseKeepingDropDownValidation", Status.PASS,
						"Credits Drop Down Text displayed as: " + (dropDownList).get(textCount).getText()
								+ " with points " + distributedPointsValidation(textCount, totalCreditsDistribution));
			}
			housekeepingDropDownTextValidation();
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "houseKeepingDropDownValidation", Status.FAIL,
					"Housekeeping Drop Down Text not present");
		}
	}

	/*
	 * Method: housekeepingDropDownTextValidation Description: Housekeeping Drop
	 * Down Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void housekeepingDropDownTextValidation() {
		if (testData.get("HousekeepingTextValidation").contains("Yes")) {
			List<WebElement> dropDownList = getList(creditsDistributionText);

			for (int textCount = 0; textCount < dropDownList.size(); textCount++) {
				if ((dropDownList).get(textCount).getText().toLowerCase()
						.contains(testData.get("HousekeepingText").toLowerCase())) {
					tcConfig.updateTestReporter("CUICancelReservation", "housekeepingDropDownTextValidation",
							Status.PASS,
							"Housekeeping Drop Down Text displayed as: " + (dropDownList).get(textCount).getText()
									+ " with points "
									+ distributedPointsValidation(textCount, totalCreditsDistribution));
				} else if (textCount + 1 == dropDownList.size()) {
					tcConfig.updateTestReporter("CUICancelReservation", "housekeepingDropDownTextValidation",
							Status.FAIL,
							testData.get("PointsTextValidation") + " Housekeeping Drop Down Text not present");
				}
			}
		}
	}

	/*
	 * Method: forfeitSummaryNotPresent Description: Verify that Forfeit Summary
	 * is not present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void forfeitSummaryNotPresent() {
		if (verifyObjectDisplayed(forfeitHeader)) {
			tcConfig.updateTestReporter("CUICancelReservation", "forfeitSummaryNotPresent", Status.FAIL,
					"Forfeit Header present in the pop up");
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "forfeitSummaryNotPresent", Status.PASS,
					"Forfeit Header not present in the pop up");
		}
	}

	/*
	 * Method: reimburseSummaryNotPresent Description: Verify that Reimburse
	 * Summary is not present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reimburseSummaryNotPresent() {
		if (verifyObjectDisplayed(reimbursementTitle)) {
			tcConfig.updateTestReporter("CUICancelReservation", "reimburseSummaryNotPresent", Status.FAIL,
					"Reimburse Header present in the pop up");
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "reimburseSummaryNotPresent", Status.PASS,
					"Reimburse Header not present in the pop up");
		}
	}

	/*
	 * Method: paymentRemimbursementSectionValidation Description: Verify that
	 * Payment Reimbursement Summary is presemt/not present Date: June/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void paymentRemimbursementSectionValidation() {
		if (testData.get("PaymentReimburse").contains("Yes")) {
			paymentRemimbursementSectionPresent();
		} else {
			paymentRemimbursementSectionNotPresent();
		}
	}

	/*
	 * Method: paymentRemimbursementSectionNotPresent Description: Verify that
	 * Payment Reimbursement Summary is not present Date: June/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void paymentRemimbursementSectionNotPresent() {
		if (verifyObjectDisplayed(paymentReimburse)) {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentRemimbursementSectionNotPresent",
					Status.FAIL, "Payment Reimbursement Section present in the pop up");
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentRemimbursementSectionNotPresent",
					Status.PASS, "Payment Reimbursement Section not present in the pop up");
		}
	}

	/*
	 * Method: paymentRemimbursementSectionPresent Description: Verify that
	 * Payment Reimbursement Summary is present Date: June/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void paymentRemimbursementSectionPresent() {
		getPopUpElementInView(paymentReimburse, "Yes");
		if (verifyObjectDisplayed(paymentReimburse)) {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentRemimbursementSectionPresent", Status.PASS,
					"Payment Reimbursement Section present in the pop up");
			paymentMethodImage();
			paymentTotalValidations();
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentRemimbursementSectionPresent", Status.FAIL,
					"Payment Reimbursement Section not present in the pop up");
		}
	}

	/*
	 * Method: paymentMethodImage Description: Verify that Payment Reimbursement
	 * Summary is present Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void paymentMethodImage() {
		if (verifyObjectDisplayed(paymentImage)) {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentMethodImage", Status.PASS,
					"Payment Method Image displayed with ending number: " + getElementText(creditCardNumber));
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentMethodImage", Status.FAIL,
					"Payment Method Image not displayed");
		}
	}

	/*
	 * Method: rentedPointsValidations Description: Verify Rented Points
	 * displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public double rentedPointsValidations() {
		if (testData.get("RentCharges").equalsIgnoreCase("Yes")) {
			if (verifyObjectDisplayed(rentedPoints)) {
				tcConfig.updateTestReporter("CUICancelReservation", "rentedPointsValidations", Status.PASS,
						"Rented Points Displayed as: " + getDoubleValue(rentedPoints, "$"));
			} else {
				tcConfig.updateTestReporter("CUICancelReservation", "rentedPointsValidations", Status.FAIL,
						"Rented Points not displayed");
			}
			return getDoubleValue(rentedPoints, "$");

		} else {
			return 0.00;
		}
	}

	/*
	 * Method: housekeepingCreditsValidations Description: Verify Housekeeping
	 * Credits displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public double housekeepingCreditsValidations() {
		if (testData.get("HousekeepingCharges").equalsIgnoreCase("Yes")) {
			if (verifyObjectDisplayed(housekeepingCredits)) {
				tcConfig.updateTestReporter("CUICancelReservation", "housekeepingCreditsValidations", Status.PASS,
						"Housekeeping Credits Displayed as: " + getDoubleValue(housekeepingCredits, "$"));
			} else {
				tcConfig.updateTestReporter("CUICancelReservation", "housekeepingCreditsValidations", Status.FAIL,
						"Housekeeping Credits not displayed");
			}
			return getDoubleValue(housekeepingCredits, "$");
		} else {
			return 0.00;
		}
	}
	

	/*
	 * Method: forfeitChargesSection Description: Membership Charges Section
	 * validation Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void forfeitChargesSection() {
		pointsReimbursed = getElementText(totalPoints);
		fieldPresenceVal(pointsDropdown, "Total Reimburse Points header ", "CUICancelReservation",
				"membershipChargesSection", " with points " + pointsReimbursed);
		fieldPresenceVal(houseKeepingCredits, "Total Housekeeping Credits header ", "CUICancelReservation",
				"membershipChargesSection", " with Credits: " + getElementText(houseKeepingPoints));
		elementPresenceVal(nonRefundableText, "Non Refundable text ", "CUICancelReservation",
				"membershipChargesSection");

	}

	/*
	 * Method: totalRefundAmount Description: Verify Total Refund amount
	 * displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public double totalRefundAmount() {
		if (verifyObjectDisplayed(totalPayment)) {
			tcConfig.updateTestReporter("CUICancelReservation", "totalRefundAmount", Status.PASS,
					"Total Refund Amount Displayed as: " + getDoubleValue(totalPayment, "+$"));
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "totalRefundAmount", Status.FAIL,
					"Total Refund Amount not Displayed as: ");
		}
		return getDoubleValue(totalPayment, "+$");
	}

	/*
	 * Method: paymentTotalValidations Description: Total Sum of Payment
	 * Reimbursement displayed Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void paymentTotalValidations() {
		double rentedPay = rentedPointsValidations();
		double housekeepingPay = housekeepingCreditsValidations();
		double totalPay = totalRefundAmount();
		double addition = Math.round((rentedPay + housekeepingPay) * 100.0) / 100.0;
		if (addition == totalPay) {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentTotalValidations", Status.PASS,
					"Sum of payment distribution matches to that of total refund");
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "paymentTotalValidations", Status.FAIL,
					"Sum of payment distribution does not matches to that of total refund");
		}
	}
}
