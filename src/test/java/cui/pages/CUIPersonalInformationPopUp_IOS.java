package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPersonalInformationPopUp_IOS extends CUIPersonalInformationPopUp_Web {

	public static final Logger log = Logger.getLogger(CUIPersonalInformationPopUp_IOS.class);
	public By city=By.id("city");
	public CUIPersonalInformationPopUp_IOS(TestConfig tcconfig) {
		super(tcconfig);
		
		editPersonalInformation = By.xpath("//div[contains(@class,'personal-info')]//button[contains(.,'Edit')]");
		accountInfoCancelCTA = By
				.xpath("//div[@id='personal-info-modal' and @aria-hidden='false']//button[contains(@class,'cancel-changes-button')]");
		
		/*accountInfoCancelCTA = By
				.xpath("//*[@id='personal-info-modal']//*[@class='close-button']");*/

	}
	
	/*
	 * Method: invalidAddress1Field Description: Invalid data Address Field in
	 * Per Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidAddress1Field() {
		try {
			//getPopUpElementInView(accountInfoAddressLine1, "Yes");
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Address Line 1*", testData.get("invalidData"),1);
			tcConfig.updateTestReporter("CUIPersonalInformationPopUp_IOS", "invalidAddress1Field", Status.PASS,
					"Data entered successfully");
			clickElementBy(city);
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIPersonalInformationPopUp_IOS", "invalidAddress1Field", Status.FAIL,
					"Could Not Enter the data in the field");
		}
		
	}
	
	/*
	 * Method: invalidEmailField Description: Invalid data in Email Field in Per
	 * Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void invalidEmailField() {
		try {
			getPopUpElementInView(accountInfoEmailField, "No");
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Email", testData.get("invalidData"),2);
			tcConfig.updateTestReporter("CUIPersonalInformationPopUp_IOS", "invalidEmailField", Status.PASS,
					"Data entered successfully");
			clickElementBy(city);
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIPersonalInformationPopUp_IOS", "invalidEmailField", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}
	
	/*
	 * Method: editEmailField Description: Edit Email Field in Per Info Pop Up
	 * Account Setting Page Date: Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void editEmailField() {
		try {
			getPopUpElementInView(accountInfoEmailField, "No");
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Email", testData.get("Email"),2);	
			tcConfig.updateTestReporter("CUIPersonalInformationPopUp_IOS", "editEmailField", Status.PASS,
					"Email field edited successfully");
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIPersonalInformationPopUp_IOS", "editEmailField", Status.FAIL,
					"Could Not Enter the data in the field");
		}
	}

	
	/*
	 * *************************Method: closePopUpModal ***
	 * *************Description Close Any Pop Up Modal**
	 */

	public void closePopUpModal(By closeCTAElement, By closedModalElement, By elementFromPopUp, Status status) {
		if (verifyObjectDisplayed(closeCTAElement)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
					"Cancel Cta present");
			//clickElementBy(closeCTAElement);
			driver.findElement(closeCTAElement).click();

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (isAlertPresent()) {
				driver.switchTo().alert().accept();
				driver.switchTo().defaultContent();
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", Status.PASS,
						"Successfully accepetd the alert popup");
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "closePopUpModal", status,
						"No popup came while Closing the pop up");
			}
			waitUntilElementVisibleBy(driver, closedModalElement, "ExplicitLongWait");
			if (verifyObjectDisplayed(elementFromPopUp)) {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
						"Pop Up Modal did not Closed");
			} else {
				tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.PASS,
						"Pop Up Modal  Closed");
			}

		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "closePopUpModal", Status.FAIL,
					"Cancel CTA not present");
		}
	}
	
	/*
	 * Method: saveAccountInfoChanges Description: Save Account Info Changes Per
	 * Info Pop Up Account Setting Page Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void saveAccountInfoChanges() {
		clickElementBy(accountInfoSaveCta);

		waitUntilElementVisibleBy(driver, closedAccountInfoModal, "ExplicitLongWait");
		if (verifyObjectDisplayed(accountInfoAddressLine1)) {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveAccountInfoChanges", Status.FAIL,
					"Pop Up Modal did not Closed");
		} else {
			tcConfig.updateTestReporter("CUIAccountSettingPage", "saveAccountInfoChanges", Status.PASS,
					"Pop Up Modal  Closed");
			savedChangesValidation(ownerAddress, "Address", "Personal Information");
		}
	}
}
