package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPointSummaryPage_Android extends CUIPointSummaryPage_Web {

	public static final Logger log = Logger.getLogger(CUIPointSummaryPage_Android.class);

	public CUIPointSummaryPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}

	public By expandAll = By.xpath("//div[contains(@class,'accordionExpand')]");

	/*
	 * Method: pointsLinkNavigations Description: Points Education Section val
	 * Date:Jun/2020 Author: Unnat Jain Changes By:NA
	 */

	public void pointsLinkNavigations() {
		for (int linksCount = 0; linksCount < getList(learnMoreLink).size(); linksCount++) {
			String URL = driver.getCurrentUrl();
			clickElementBy(expandAll);
			//getElementInView(getList(learnMoreLink).get(linksCount));
			clickElementByScriptExecutor("Learn More", linksCount+1);
			waitUntilElementVisibleBy(driver, pointsLinkNavigation, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(pointsLinkNavigation));

			tcConfig.updateTestReporter("CUIPointSummaryPage", "pointsEducationLinks", Status.PASS,
					"Navigated to Education Link page");
			driver.get(URL);
			waitUntilElementVisibleBy(driver, pointsSummaryHeader, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(pointsSummaryHeader));

		}
	}
	
}
