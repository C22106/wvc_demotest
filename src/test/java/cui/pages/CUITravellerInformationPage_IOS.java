package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUITravellerInformationPage_IOS extends CUITravellerInformationPage_Web {

	public static final Logger log = Logger.getLogger(CUITravellerInformationPage_IOS.class);

	public CUITravellerInformationPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	/*
	 * Method: fillTravellerInformation Description:Validate and fill Traveller
	 * Information Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void fillTravellerInformation() {
		String emailtraveller = testData.get("TravellerEmailID");
		waitUntilElementVisibleBy(driver, textBookDetails, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textBookDetails));
		if (verifyObjectDisplayed(textTravelInfo)) {
			getElementInView(travelerEmailField);
			if (verifyObjectDisplayed(travelerEmailField)) {
				sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address*", emailtraveller, 1);
				tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.PASS,
						"Fields successfully entered in Default Traveller Information Page");
			} else {
				tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.FAIL,
						"Email field not present");
			}

		} else {
			tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.FAIL,
					"Traveller Information Header Not present");
		}
	}
	
	/*
	 * Method: selectAndPopulateGuestInfo Description:selectAndPopulateGuestInfo
	 * Date: June/2020 Author: Monideep Roychowdhury Roy Changes By: KG
	 */

	public void selectAndPopulateGuestInfo() {
		clickElementBy(selectGuestChkBox);
		waitUntilElementVisibleBy(driver, guestFirstName, "ExplicitMedWait");

		sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name*", "GuestFirst", 1);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name*", "GuestLast", 1);
		tcConfig.getTestData().put("GuestName", "GuestFirst GuestLast");
		
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address*", "guest.email@gmail.com", 1);
		tcConfig.getTestData().put("GuestEmail", "guest.email@gmail.com");
		
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Phone Number", "9962001374", 1);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Street Address*", "44 Center Grove Road", 1);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "City*", "Randolph", 1);
		
		selectByText(guestState, "New Jersey");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Zip Code*", "07869", 1);
		clickElementBy(agreeGuest21ChkBox);
	}
	
	

}