package cui.pages;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIMyOwnershipPayPalPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIMyOwnershipPayPalPage_Web.class);

	public CUIMyOwnershipPayPalPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By linkBackToOwnership = By.xpath("//a[@id = 'back-to-landing-page']");
	protected By textPaymentType = By.xpath("//h1[contains(.,'Payment Type')]");
	protected By textLikeToPay = By.xpath("//span[contains(text(),'like to pay')]");
	protected By paymentType = By.xpath("//select[@id = 'payment-type']");
	protected By optionAssessmentPaymentType = By.xpath("//option[@value = 'Assessment']");
	protected By optionLoanPaymentType = By.xpath("//option[@value = 'Loan']");
	protected By textContractAvailable = By.xpath("//h5[text() = 'Contracts Available']");
	protected By textContractSelection = By.xpath("//span[contains(text(),'contract would you like')]");
	protected By textContractNumber = By
			.xpath("//input[@name = 'contract-multiple']/parent::div/label/p/span[contains(@class,'body-1')]");
	protected By totalContract = By.xpath("//input[@name = 'contract-multiple']/..");
	protected By buttonContinue = By.xpath("(//button[text() = 'Continue'])[1]");

	/*
	 * Method: validateLinkBackToOwnership Description:Check Link Back To
	 * Ownership Presence Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateLinkBackToOwnership() {
		elementPresenceVal(linkBackToOwnership, "Link Back To Ownership", "CUIMyOwnershipPayPalPage",
				"validateLinkBackToOwnership");
	}

	/*
	 * Method: verifyHeaderPaymentType Description:verify Header Payment Type
	 * Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeaderPaymentType() {
		elementPresenceVal(textPaymentType, "Header Payment Type", "CUIMyOwnershipPayPalPage",
				"verifyHeaderPaymentType");
	}

	/*
	 * Method: validatePaymentTypeTextAndField Description:validate Payment Type
	 * Text And Field Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validatePaymentTypeTextAndField() {
		elementPresenceVal(textLikeToPay, "Header: " + getElementText(textLikeToPay), "CUIMyOwnershipPayPalPage",
				"validatePaymentTypeTextAndField");
		Assert.assertTrue(verifyObjectDisplayed(paymentType), "Payment type field not present");
		tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "validatePaymentTypeTextAndField", Status.PASS,
				"Field Payment Type is Present");
	}

	/*
	 * Method: checkOptionsInPaymentTypeField Description:check OptionsIn
	 * Payment Type Field Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkOptionsInPaymentTypeField() {
		clickElementJSWithWait(paymentType);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(optionAssessmentPaymentType)) {
			selectByText(paymentType, "Assessment");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "checkOptionsInPaymentTypeField", Status.PASS,
					"Assessment Option is Present");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "checkOptionsInPaymentTypeField", Status.FAIL,
					"Assessment Option is not Present");
		}

		if (verifyObjectDisplayed(optionLoanPaymentType)) {
			selectByText(paymentType, "Loan");
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "checkOptionsInPaymentTypeField", Status.PASS,
					"Loan Option is Present");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "checkOptionsInPaymentTypeField", Status.FAIL,
					"Loan Option is not Present");
		}
	}

	/*
	 * Method: selectPaymentType Description:select Payment Type Date: Jun/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void selectPaymentType() {
		String paymentTypeAs = testData.get("PaymentTypeSelect");
		selectByText(paymentType, paymentTypeAs);
	}

	/*
	 * Method: validateHeaderContractAvailable Description:validate Header Contract Available Date: Jun/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHeaderContractAvailable() {
		elementPresenceVal(textContractAvailable, "Header Contract Available", "CUIMyOwnershipPayPalPage",
				"validateHeaderContractAvailable");
		elementPresenceVal(textContractSelection, "Text: " + getElementText(textContractSelection),
				"CUIMyOwnershipPayPalPage", "validateHeaderContractAvailable");

	}

	/*
	 * Method: contractsInAscendingOrder Description:contracts In Ascending
	 * Order Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractsInAscendingOrder() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<Integer> obtainedList = new ArrayList<>();
		for (WebElement contractNumber : getList(textContractNumber)) {
			obtainedList.add(Integer.parseInt(contractNumber.getText()));
		}
		ArrayList<Integer> sortedList = new ArrayList<>();
		for (int contractToSort : obtainedList) {
			sortedList.add(contractToSort);
		}

		Collections.sort(sortedList);
		try {
			Assert.assertTrue(sortedList.equals(obtainedList),"List Not Sorted");
			tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "contractsInAscendingOrder", Status.PASS,
					"Sorted In Ascending Order" + sortedList);
		} catch (AssertionError error) {
			tcConfig.updateTestReporter("CUISearchPage", "verifysortInAZ", Status.FAIL,
					"Not Sorted In Ascending Order" + error.getMessage());
		}
	}

	/*
	 * Method: verifyCurrentDueInContractCard Description:verify Current Due In
	 * Contract Card Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void verifyCurrentDueInContractCard() {
		for (int iteratorCurrentDue = 1; iteratorCurrentDue <= getList(totalContract).size(); iteratorCurrentDue++) {
			String contractNumber = getObject(By
					.xpath("(//input[@name = 'contract-multiple']/parent::div/label/p/span[contains(@class,'body-1')])["
							+ iteratorCurrentDue + "]")).getText().trim();
			WebElement textCurrentDue = getObject(By.xpath("((//input[@name = 'contract-multiple']/..)["
					+ iteratorCurrentDue + "]/label/p/span[contains(@class,'show') and contains(text(),'$')])"));
			if (verifyObjectDisplayed(textCurrentDue)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.PASS,
						"Current Due for contract: " + contractNumber + " is: " + getElementText(textCurrentDue));
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.FAIL, "Current Due for contract: " + contractNumber + " is not displayed");
			}
		}
	}

	/*
	 * Method: verifyDueDateInContractCard Description:verify Due Date In
	 * Contract Card Date field Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void verifyDueDateInContractCard() {
		for (int iteratorCurrentDue = 1; iteratorCurrentDue <= getList(totalContract).size(); iteratorCurrentDue++) {
			String contractNumber = getObject(By
					.xpath("(//input[@name = 'contract-multiple']/parent::div/label/p/span[contains(@class,'body-1')])["
							+ iteratorCurrentDue + "]")).getText().trim();
			WebElement textDateDue = getObject(By.xpath("(//input[@name = 'contract-multiple']/..)["
					+ iteratorCurrentDue
					+ "]//span[contains(text(),'Due Date')]/parent::p[contains(@class,'show')]/span[contains(text(),'Due Date')]/following-sibling::span"));
			if (verifyObjectDisplayed(textDateDue)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.PASS,
						"Due Date for contract: " + contractNumber + " is: " + getElementText(textDateDue));
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard",
						Status.FAIL, "Due Date for contract: " + contractNumber + " is not displayed");
			}
		}
	}

	/*
	 * Method: selectContract Description:select Contract Date field Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectContract() {
		String contractNumber = testData.get("ContractNumber");
		WebElement contractToSelect = getObject(By.xpath("//label[contains(@for,'" + contractNumber + "')]"));
		clickElementJSWithWait(contractToSelect);
		tcConfig.updateTestReporter("CUIMyOwnershipPayPalPage", "verifyCurrentDueInContractCard", Status.PASS,
				"Contract Selected for Loan: " + contractNumber);
	}

	/*
	 * Method: clickContinueButton Description:click Continue Button Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		if (verifyObjectDisplayed(buttonContinue)) {
			getElementInView(buttonContinue);
			clickElementJSWithWait(buttonContinue);
		}

	}
}
