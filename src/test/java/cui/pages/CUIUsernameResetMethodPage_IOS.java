package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIUsernameResetMethodPage_IOS extends CUIUsernameResetMethodPage_Web {

	public static final Logger log = Logger.getLogger(CUIUsernameResetMethodPage_IOS.class);

	public CUIUsernameResetMethodPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		

	}
	
	/*
	 * Method: enterSecurityQuestion1 Description:Security Ques1 enter Date:
	 * Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void enterSecurityQuestion1(String securityAnswer) {
		//getElementInView(question1Field);
		clickElementBy(question1Field);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Your Answer", securityAnswer,1);
		//getObject(question1Field).sendKeys(securityAnswer);
	}

	/*
	 * Method: enterSecurityQuestion2 Description:Security Ques2 enter Date:
	 * Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void enterSecurityQuestion2(String securityAnswer) {
		//getElementInView(question2Field);
		clickElementBy(question2Field);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Your Answer", securityAnswer,2);
		//getObject(question2Field).sendKeys(securityAnswer);
	}
	
	/*
	 * Method: emailPresenceValidations Description:Email Presence Validations
	 * Date: Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void emailPresenceValidations(String emailAddress) {
		String emailValue = getElementValue(emailField).toLowerCase();

		if (emailValue.contains(".com")) {
			tcConfig.updateTestReporter("CUIUsernameResetMethod", "verificationViaMethod", Status.PASS,
					"Email Alreadey Present as: " + emailValue);
		} else {
			//fieldDataEnter(emailField, emailAddress);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Email", emailAddress,1);
		}
	}
	
	/*
	 * Method: securityQuesErrorVal Description:Security Ques Error Validations
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void securityQuesErrorVal() {
		// Invalid Data enter
		String strInvalidCode = testData.get("invalidData");
		
		for (int noOfTries = 0; noOfTries < 3; noOfTries++) {
			// clearField(verificationCodeField);
			
			getElementInView(question1Field);			
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Your Answer*", strInvalidCode,1);
			
			clickElementBy(securityMethod);
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Your Answer*", strInvalidCode,2);			
			clickElementBy(securityMethod);
			clickContinue();
			waitUntilElementVisibleBy(driver, errorBlock, "ExplicitLongWait");
			getElementInView(errorBlock);
			if (noOfTries < 2) {
				fieldPresenceVal(twoTryError, (noOfTries + 1) + " try error ", "CUIUsernameResetMethod",
						"securityQuesErrorVal", getElementText(twoTryError));
			} else if (noOfTries >= 2) {
				fieldPresenceVal(thirdTryError, (noOfTries + 1) + "rd try error ", "CUIUsernameResetMethod",
						"securityQuesErrorVal", getElementText(thirdTryError));
			} else {
				tcConfig.updateTestReporter("CUIUsernameResetMethod", "emailPageVal", Status.FAIL,
						"Error Validations Failed");
			}

		}
	}
}