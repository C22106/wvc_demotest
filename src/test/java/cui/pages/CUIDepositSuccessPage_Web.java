
package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIDepositSuccessPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIDepositSuccessPage_Web.class);

	public CUIDepositSuccessPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcConfig);
	protected By housekeppingCost = By.xpath("//h3[text() = 'PAYMENT CHARGES']/..//span[text() = 'Housekeeping Credits']/../../following-sibling::p/span");
	protected By textDepositConfirmation = By.xpath("//div[contains(@class,'rci-deposit-confirmation')]/h1");
	protected By textIntroduction = By.xpath("//div[contains(@class,'rci-deposit-confirmation')]/h2");
	protected By textSuccessfulConfirmation = By.xpath("//div[contains(@class,'rci-deposit-confirmation')]/p//span[contains(text(),'successfully deposited')]");
	protected By sectionConfirmationCharges = By.xpath("//section[contains(@class,'confirmation-charges')]");
	protected By textChargesSummary = By.xpath("//h2[text() = 'Charges Summary']");
	protected By totalPointArea = By.xpath("//span[contains(text(),'Total Points')]/..");
	protected By valueTotalPoints = By.xpath("//span[contains(text(),'Total Points')]/../span");
	protected By expandPointBreakdown = By.xpath("//span[contains(text(),'Total Points')]");
	protected By expandHousekeepingBreakdown = By.xpath("//span[text() = 'Housekeeping Credits']/..");
	protected By totalDepositBreakdown = By.xpath("//div[contains(@id,'points')]//span[contains(text(),'Use Year')]/../..");
	protected By totalHousekeepingBreakdown = By.xpath("//div[@id = 'housekeeping-credits']/div");
	protected By totalPointsBreakdownLabel = By.xpath("//div[contains(@id,'points')]//span[contains(text(),'Use Year')]");
	protected By totalPointsBreakdownValue = By.xpath("//span[text() = 'Total Points']/../..//span[contains(text(),'Use Year')]/following-sibling::span");
	protected By houskeepingArea = By.xpath("//span[text() = 'Housekeeping Credits']/../..");
	protected By valueHousekeeping = By.xpath("//span[text() = 'Housekeeping Credits']/../../span");
	protected By reservationTransactionValue = By.xpath("//span[text() = 'Reservation Transaction']/../../p");
	protected By totalChargesBreakdown = By.xpath("(//h3[text() = 'PAYMENT CHARGES']/../div[contains(@class,'payment')]/following-sibling::div)");
	protected By totalChargedValue = By.xpath("//span[text() = 'Total Charged']/following-sibling::span");
	protected By ccHolderName = By.xpath("//div[contains(@class,'confirmation-charges__payment-type')]/p[1]/span");
	protected By visaIcon = By.xpath("//img[contains(@alt,'Visa')]");
	protected By ccDetails = By.xpath("//img[contains(@alt,'Visa')]/../span");
	protected By feePointDeposit = By.xpath("//span[text() = 'Points Deposit Fee']/../../../../../p//span[contains(.,'$')]");
	protected By navigateDashBoard=By.xpath("//a[.='Go to My Dashboard']");
	
	/*
	 * Method: verifyDepositConfirmation Description:verify Deposit Confirmation
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyDepositConfirmation(){
		elementPresenceVal(textDepositConfirmation, "Text: "+getElementText(textDepositConfirmation), "CUIDepositSuccessPage", "verifyDepositConfirmation");
		elementPresenceVal(textIntroduction, "Text: "+getElementText(textIntroduction), "CUIDepositSuccessPage", "verifyDepositConfirmation");
		elementPresenceVal(textSuccessfulConfirmation, "Text: "+getElementText(textSuccessfulConfirmation), "CUIDepositSuccessPage", "verifyDepositConfirmation");
	}
	
	/*
	 * Method: validateNavigateToDashBoard Description:verify Navigate To DashBoard presence
	 * Summary Page Val Date: Jun/2020 Author: Kamalesh  Roy Changes By: NA
	 */
	public void validateNavigateToDashBoard(){
		Assert.assertTrue(verifyObjectDisplayed(navigateDashBoard), "Navigate to DashBoard CTA not Present");
		tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateNavigateToDashBoard", Status.PASS,
				"Navigate to DashBoard CTA Present");
		
	}
	
	/*
	 * Method: validateDepositDetails Description:validate Deposit Details
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateDepositDetails(){
		if (verifyObjectDisplayed(sectionConfirmationCharges)) {
			elementPresenceVal(textChargesSummary, "Charges Summary", "CUIDepositSuccessPage", "validateDepositDetails");
		}
	}
	
	/*
	 * Method: verifyTotalPointsSuccess Description:verify Total Points Success
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyTotalPointsSuccess(){
		completeDepositPage.verifyTotalPoints();
		}
	
	/*
	 * Method: validateTotalPointsBreakdownSuccess Description:validate Total Points Breakdown Success
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTotalPointsBreakdownSuccess(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(expandPointBreakdown);
		clickElementJSWithWait(expandPointBreakdown);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int totalPointBreakdown = 1; totalPointBreakdown <= getList(totalDepositBreakdown).size(); totalPointBreakdown++) {
			String breakdownLabel = getList(By.xpath("(//div[contains(@id,'points')]/div)["+totalPointBreakdown+"]//span")).get(0).getText();
			String breakdownValue = getList(By.xpath("(//div[contains(@id,'points')]/div)["+totalPointBreakdown+"]//span")).get(1).getText();
			if (breakdownLabel.isEmpty() || breakdownLabel == null || breakdownValue.isEmpty() || breakdownValue == null) {
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateTotalPointsBreakdownSuccess", Status.FAIL,
						"Breakdown Deposit Point either label or value not displayed");
			}else{
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateTotalPointsBreakdownSuccess", Status.PASS,
						"For "+breakdownLabel+" value deposited is: "+breakdownValue);
			}
		}
	}
	
	/*
	 * Method: verifyHousekeepingCredit Description:verify Housekeeping Credit
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHousekeepingCredit(){
		if (verifyObjectDisplayed(houskeepingArea)) {
			getElementInView(houskeepingArea);
			String housekeepingcredit = getList(valueHousekeeping).get(1).getText();
			if (housekeepingcredit.isEmpty() || housekeepingcredit == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "verifyHousekeepingCredit", Status.FAIL,
						"Housekeeping Deposited not displayed");
			}else{
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "verifyHousekeepingCredit", Status.PASS,
						"Housekeeping Deposited: "+housekeepingcredit);	
			}
				
		}else{
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "verifyHousekeepingCredit", Status.FAIL,
						"HouseKeeping Area not displayed");
			}
		}
	
	/*
	 * Method: validateHouskeepingBreakdownSuccess Description:validate Houskeeping Breakdown Success
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHouskeepingBreakdownSuccess(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(expandHousekeepingBreakdown);
		clickElementJSWithWait(expandHousekeepingBreakdown);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int totalPointBreakdown = 1; totalPointBreakdown <= getList(totalHousekeepingBreakdown).size(); totalPointBreakdown++) {
			String breakdownLabel = getList(By.xpath("(//div[@id = 'housekeeping-credits']/div)["+totalPointBreakdown+"]//span")).get(0).getText();
			String breakdownValue = getList(By.xpath("(//div[@id = 'housekeeping-credits']/div)["+totalPointBreakdown+"]//span")).get(1).getText();
			if (breakdownLabel.isEmpty() || breakdownLabel == null || breakdownValue.isEmpty() || breakdownValue == null) {
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateHouskeepingBreakdownSuccess", Status.FAIL,
						"Breakdown Housekeeping Point either label or value not displayed");
			}else{
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateHouskeepingBreakdownSuccess", Status.PASS,
						"For "+breakdownLabel+" value deposited is: "+breakdownValue);
			}
		}
	}
	
	/*
	 * Method: validateReservationTransactionSuccess Description:validate Reservation Transaction Success
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateReservationTransactionSuccess(){
		if (getList(reservationTransactionValue).size() > 0) {
			getElementInView(reservationTransactionValue);
			String valueReservationTransaction = getList(reservationTransactionValue).get(1).getText();
			tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateReservationTransactionSuccess", Status.PASS,
					"Value Required for reservation transaction is: "+valueReservationTransaction);
		}else{
			tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateReservationTransactionSuccess", Status.FAIL,
					"Value for Reservation Transaction is not displayed");
		}
	}
	
	/*
	 * Method: validateChargesRequiredForBreakDown Description:validate Charges Required For BreakDown
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateChargesRequiredForBreakDown(){
		for (int iteratorCharges = 1; iteratorCharges <= getList(totalChargesBreakdown).size(); iteratorCharges++) {
			String breakdownLabel = getList(By.xpath("(//h3[text() = 'PAYMENT CHARGES']/../div[contains(@class,'payment')]/following-sibling::div)["+iteratorCharges+"]//p")).get(0).getText();
			String breakdownValue = getList(By.xpath("(//h3[text() = 'PAYMENT CHARGES']/../div[contains(@class,'payment')]/following-sibling::div)["+iteratorCharges+"]//p")).get(1).getText();
			if (breakdownLabel.isEmpty() || breakdownLabel == null || breakdownValue.isEmpty() || breakdownValue == null) {
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateChargesRequiredForBreakDown", Status.FAIL,
						"Breakdown Housekeeping Point either label or value not displayed");
			}else{
				tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateChargesRequiredForBreakDown", Status.PASS,
						"For "+breakdownLabel+" value deposited is: "+breakdownValue);
			}
		}
	}
	
	/*
	 * Method: validateTotalValueCharged Description:validate Total Value Charged
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTotalValueCharged(){
		if (verifyObjectDisplayed(totalChargedValue)) {
			getElementInView(totalChargedValue);
			String valueTotalCharged = getElementText(totalChargedValue);
			tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateReservationTransactionSuccess", Status.PASS,
					"Value Total Charged is: "+valueTotalCharged);
		}else{
			tcConfig.updateTestReporter("CUIDepositSuccessPage", "validateReservationTransactionSuccess", Status.PASS,
					"Total value Charged not displayed");
		}
	}
	
	/*
	 * Method: validatePaidByCreditCard Description:validate PaidBy Credit Card
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validatePaidByCreditCard(){
		elementPresenceVal(ccHolderName, "Card Holder Name: "+getElementText(ccHolderName), "CUIDepositSuccessPage", "validatePaidByCreditCard");
		elementPresenceVal(visaIcon, "Credit Card Icon", "CUIDepositSuccessPage", "validatePaidByCreditCard");
		elementPresenceVal(ccDetails, "Credit Card Details: "+getElementText(ccDetails), "CUIDepositSuccessPage", "validatePaidByCreditCard");
	}
	
	/*
	 * Method: costpointDepositCWSuccess Description:cost point Deposit Club
	 * Wyndham Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void costpointDepositCWSuccess() {
		if (verifyObjectDisplayed(feePointDeposit)) {
			if (getElementText(feePointDeposit).isEmpty() || getElementText(feePointDeposit) == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "costpointDepositCWSuccess", Status.FAIL,
						"Deposit Point Fee is not displayed");
			} else {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "costpointDepositCWSuccess", Status.PASS,
						"Deposit Point Fee is displayed and value is: " + getElementText(feePointDeposit));
			}
		}
	}
	
	/*
	 * Method: validateHousekeepingCreditInPaymentCharges Description:validate Housekeeping Credit In  Payment Charges
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHousekeepingCreditInPaymentCharges(){
		if (verifyObjectDisplayed(housekeppingCost)) {
			if (getElementText(housekeppingCost).isEmpty() || getElementText(housekeppingCost) == null) {
				tcConfig.updateTestReporter("CUICompleteDepositPage", "validateHousekeepingCreditInPaymentCharges", Status.FAIL,
						"Housekeeping Fee is not displayed");
			}else{
				tcConfig.updateTestReporter("CUICompleteDepositPage", "validateHousekeepingCreditInPaymentCharges", Status.PASS,
						"Housekeeping Fee is: "+getElementText(housekeppingCost));
			}
		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "validateHousekeepingCreditInPaymentCharges", Status.FAIL,
					"Housekeeping Fee Area is not displayed");
		}
	}
	
	/*
	 * Method: checkHousekeepingPurchasedSuccess Description:check Housekeeping Purchased Success
	 * Summary Page Val Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkHousekeepingPurchasedSuccess(){
		String perenceHousekeepingPurchased = null;
		for (int totalPointBreakdown = 1; totalPointBreakdown <= getList(totalHousekeepingBreakdown).size(); totalPointBreakdown++) {
			String breakdownLabel = getList(By.xpath("(//div[@id = 'housekeeping-credits']/div)["+totalPointBreakdown+"]//span")).get(0).getText();
			if (breakdownLabel.contains("Purchased")) {
				perenceHousekeepingPurchased = "Y";
			}
		}
		
		if (perenceHousekeepingPurchased == "Y") {
			tcConfig.updateTestReporter("CUICompleteDepositPage", "checkHousekeepingPurchasedSuccess", Status.PASS,
					"Housekeeping is Purchased present in Success Page");
		}else{
			tcConfig.updateTestReporter("CUICompleteDepositPage", "checkHousekeepingPurchasedSuccess", Status.FAIL,
					"Housekeeping is Purchased but not present in Success Page");
		}
	}
	
}
