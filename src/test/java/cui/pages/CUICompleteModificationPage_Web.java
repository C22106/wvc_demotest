package cui.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICompleteModificationPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUICompleteModificationPage_Web.class);

	public CUICompleteModificationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	public CUIBookPage_Web bookPageObject = new CUIBookPage_Web(tcConfig);
	protected By selectCreditCard = By.xpath("//label[@for = 'credit-card']");
	protected By selectPaypal = By.xpath("//label[@for = 'paypal']");
	protected By sectionPaymentmethod = By.xpath("//section[contains(@class,'payment-method')]");

	protected CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcConfig);
	protected By textCongrats = By.xpath("//h2[contains(.,'Congratulations')] | //div[contains(@class,'congrats')]");
	protected By headerCompleteModification = By
			.xpath("(//h1[text() = 'Complete Modification'] | //div[text() = 'Complete Modification'])");
	protected By linkfullCancelPolicy = By
			.xpath("//h2[text() = 'Cancellation Policy']/../p/a | //div[text() = 'Cancellation Policy']/../p/a");
	protected By textTravelInfo = By.xpath("//h2[text() = 'Traveler Info'] | //div[text() = 'Traveler Info']");
	protected By textReviewCharges = By.xpath("(//h3[text() = 'Review Changes'] | //div[text() = 'Review Changes'])");
	protected By verifyCancelPolicy = By.xpath(
			"//h2[text() = 'Cancellation Policy']/..//span | //div[text() = 'Cancellation Policy']/..//div[contains(.,'cancelled')]");
	protected By paymentChargesHeader = By
			.xpath("//h2[.='Payment Charges'] | //div[contains(@class,'steps') and text()='Payment Charges']");
	protected By textCurrentOwner = By.xpath("//th[text() = 'Current']/../td");
	protected By textRevisedOwner = By.xpath("//th[text() = 'Revised']/../td");
	protected By fullCancelPolicy = By.xpath("//h3[text() = 'Canceling a Reservation']");
	protected By buttonConfirmCharges = By.xpath("//button[text() = 'Confirm Changes']");
	protected By modifyTravelConfirm = By.xpath("//h1[text() = 'Modify Traveler Confirmation']");
	protected By guestConfirmationCreditLabel = By.xpath("//th[contains(text(),'Guest Confirmation Credit')]");
	protected By guestConfirmationCreditValue = By
			.xpath("//th[contains(text(),'Guest Confirmation Credit')]/following-sibling::td");
	protected By totalPaymenDueLabel = By.xpath("//th[contains(text(),'Total Payment Due')]");
	protected By totalPaymenDueValue = By.xpath("//th[contains(text(),'Total Payment Due')]/following-sibling::td");

	// addNights
	protected By totalPointsDsiplayed = By.xpath("//th[contains(.,'Total Points')]/following-sibling::td");
	protected By totalPointsRequired = By.xpath("//span[contains(.,'Points')]/following-sibling::span");
	protected By modifiedDate = By.xpath("//time[contains(@class,'modified')]");
	protected By hkCreditLabel = By.xpath("//th[text()='Purchased Housekeeping Credits']");
	protected By hkCreditValue = By.xpath("//th[text()='Purchased Housekeeping Credits']/following-sibling::td");
	protected By rentedPointLabel = By.xpath("//th[text()='Rented Points']");
	protected By rentedPointValue = By.xpath("//th[text()='Rented Points']/following-sibling::td");


	/*
	 * Method: textReviewCharges Description:text Review Charges Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void textReviewCharges() {
		waitUntilElementVisibleBy(driver, headerCompleteModification, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerCompleteModification),
				"Complete Modification Header not present");
		if (verifyObjectDisplayed(headerCompleteModification)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textReviewCharges", Status.PASS,
					"Text Review Charges is Present");
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textReviewCharges", Status.FAIL,
					"Text Review Charges not Present");
		}
	}

	/*
	 * Method: verifyRevisedOwner Description:date Revised OWner Verification
	 * Date:June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyRevisedOwner() {
		String modifyToOwner = testData.get("ModifyOwner");
		if (verifyObjectDisplayed(textRevisedOwner)) {
			if (getElementText(textRevisedOwner).equals(modifyToOwner)) {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.PASS,
						"Revised Owner is: " + getElementText(textRevisedOwner));
			} else {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
						"Owner changed and revised from screen did not match");
			}

		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
					"Revised Owner text and value not present");
		}
	}

	/*
	 * Method: textTravelerInfo Description:text Traveler Info Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void textTravelerInfo() {
		if (verifyObjectDisplayed(textTravelInfo)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textTravelerInfo", Status.PASS,
					"Text Travel Info is Present");
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textTravelerInfo", Status.FAIL,
					"Text Travel Info not Present");
		}
	}

	/*
	 * Method: verifyCurrentOwner Description:verify Current Owner Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyCurrentOwner() {
		if (verifyObjectDisplayed(textCurrentOwner)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.PASS,
					"Current Owner is: " + getElementText(textCurrentOwner));
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
					"Current Owner text and value not present");
		}
	}

	/*
	 * Method: verifyRevisedTraveler Description:verify Revised Traveler Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyRevisedTraveler() {
		String travelerType = testData.get("ModifyTravelerTo").trim();
		String modifyToGuest = null;
		if (travelerType.equalsIgnoreCase("Owner")) {
			modifyToGuest = testData.get("ModifyOwner").trim();
		} else if (travelerType.equalsIgnoreCase("Guest")) {
			modifyToGuest = testData.get("GuestFullName").trim();
		}
		if (verifyObjectDisplayed(textRevisedOwner)) {
			if (getElementText(textRevisedOwner).equals(modifyToGuest)) {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.PASS,
						"Revised Travler is: " + modifyToGuest);
			} else {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
						"Travler changed and revised from screen did not match");
			}

		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
					"Revised Travler text and value not present");
		}
	}

	/*
	 * Method: validateCancelPolicy Description:validate Cancel Policy Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateCancelPolicy() {
		if (verifyObjectDisplayed(verifyCancelPolicy)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "validateCancelPolicy", Status.PASS,
					"Cancellation Policy text present with description as: " + getElementText(verifyCancelPolicy));
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "validateCancelPolicy", Status.PASS,
					"Failed to validate Cancellation Policy");
		}
	}

	/*
	 * Method: verifyFullCancelPolicy Description:verify Full Cancel Policy
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyFullCancelPolicy() {
		if (verifyObjectDisplayed(linkfullCancelPolicy)) {
			clickElementJSWithWait(linkfullCancelPolicy);
			waitUntilElementVisibleBy(driver, fullCancelPolicy, "ExplicitLongWait");
			if (verifyObjectDisplayed(fullCancelPolicy)) {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyFullCancelPolicy", Status.PASS,
						"Navigated to Full Cancel Page");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				navigateBack();
			} else {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyFullCancelPolicy", Status.FAIL,
						"Failed to Navigated to Full Cancel Page");
			}
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyFullCancelPolicy", Status.FAIL,
					"Link to navigate to full cancellation policy page not found");
		}
	}

	/*
	 * Method: buttonConfirmCharges Description:button Confirm Charges Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void buttonConfirmCharges() {
		if (verifyObjectDisplayed(buttonConfirmCharges)) {
			clickElementJSWithWait(buttonConfirmCharges);
			waitUntilElementVisibleBy(driver, textCongrats, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(textCongrats), "Congrats Text not dispalyed");
			tcConfig.updateTestReporter("CUICompleteModificationPage", "buttonConfirmCharges", Status.PASS,
					"Navigated to Modify Success Page");
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "buttonConfirmCharges", Status.FAIL,
					"Confirm Charges button not present");
		}
	}

	/*
	 * Method: validateSectionReservationSummary Description:validate Section
	 * Reservation Summary Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateSectionReservationSummary() {
		modifyPage.checkSectionReservationSummary();
		modifyPage.checkReservationSummaryDetails();
	}

	/*
	 * Method: makePayment Description: makePayment Date: June/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	public void makePayment() {
		String paymentMethod = testData.get("PaymentBy");
		selectPaymentMethod();
		if (paymentMethod.contains("PayPal")) {
			bookPageObject.paymentViaPaypal();
		} else if (paymentMethod.contains("CreditCard")) {
			bookPageObject.dataEntryForCreditCard();
		}
	}

	/*
	 * Method: selectPaymentMethod Description: selectPaymentMethod Date:
	 * June/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void selectPaymentMethod() {
		if (verifyObjectDisplayed(sectionPaymentmethod)) {
			String paymentBy = testData.get("PaymentBy");
			if (paymentBy.equalsIgnoreCase("CreditCard")) {
				clickElementJSWithWait(selectCreditCard);
				tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.PASS,
						"Payment Method is selected as: " + paymentBy);
			} else if (paymentBy.equalsIgnoreCase("Paypal")) {
				clickElementJSWithWait(selectPaypal);
				tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.PASS,
						"Payment Method is selected as: " + paymentBy);
			} else {
				tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.FAIL,
						"Provide which payment method to use");
			}

		} else {
			tcConfig.updateTestReporter("CUIBookPage", "selectPaymentMethod", Status.FAIL,
					"Payment section not visible");
		}
	}

	/*
	 * Method: verifyPaymentCharges Description: verifyPaymentCharges Date:
	 * June/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void verifyPaymentCharges() {
		getElementInView(paymentChargesHeader);
		assertTrue(verifyObjectDisplayed(paymentChargesHeader), "Payment Charges section  not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Payment Charges section present");

		assertTrue(
				verifyObjectDisplayed(guestConfirmationCreditLabel)
						&& verifyObjectDisplayed(guestConfirmationCreditValue),
				"Guest Confirmation Credit not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Guest Confirmation Credit label and its value present");

		assertTrue(verifyObjectDisplayed(totalPaymenDueLabel) && verifyObjectDisplayed(totalPaymenDueValue),
				"Total Paymen Due not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyChargesSummarySection", Status.PASS,
				"Total Paymen Due Label and its value present");

	}

	/*
	 * Method: verifyPaymentCharges Description: verifyPaymentCharges Date:
	 * June/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void verifyPaymentCharges_RentedPoint() {
		getElementInView(paymentChargesHeader);
		assertTrue(verifyObjectDisplayed(paymentChargesHeader), "Payment Charges section  not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Payment Charges section present");

		assertTrue(verifyObjectDisplayed(rentedPointLabel) && verifyObjectDisplayed(rentedPointValue),
				"Rented Point label and its value not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Rented Point label and its value present");

	}

	/*
	 * Method: verifyPaymentCharges Description: verifyPaymentCharges Date:
	 * June/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void verifyPaymentCharges_PurchaseHK() {
		getElementInView(paymentChargesHeader);
		assertTrue(verifyObjectDisplayed(paymentChargesHeader), "Payment Charges section  not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Payment Charges section present");

		assertTrue(verifyObjectDisplayed(hkCreditLabel) && verifyObjectDisplayed(hkCreditValue),
				"Purchased Housekeeping Credits label and its value not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyPaymentCharges_RentedPoint", Status.PASS,
				"Purchased Housekeeping Credits label and its value present");

	}

	/*
	 * Method: totalPointsRequired Description: Total Points Required Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalPointsRequired() {
		Assert.assertTrue(verifyObjectDisplayed(totalPointsRequired),
				"Total Points Required not displayed in Right pane");
		tcConfig.updateTestReporter("CUICompleteModificationPage", "totalPointsRequired", Status.PASS,
				"Total Points Requird is displayed in Right window");
	}

	/*
	 * Method: updatedDateValidation Description: Verify Date is Updated Date:
	 * Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void updatedDateValidation(String updatedDate) {

		Assert.assertTrue(getElementText(modifiedDate).trim().contains(testData.get(updatedDate)),
				"Updated Date not displayed in right window ");
		tcConfig.updateTestReporter("CUICompleteModificationPage", "updatedDateValidation", Status.PASS,
				"Updated Date is displayed as " + getElementText(modifiedDate));
	}

}