package cui.pages;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUILoginPage_IOS extends CUILoginPage_Web {

	public static final Logger log = Logger.getLogger(CUILoginPage_IOS.class);
	protected By hamburgerMenu = By.xpath("//button[@class='hamburger-menu']");
	protected By mobileCUILogo=By.xpath("//img[@class = 'mobile-logo']");
	public CUILoginPage_IOS(TestConfig tcconfig) {
		
		super(tcconfig);
		headerNavigation = By.xpath("//div[@class='hamburger-menu-container']");
		headerLogoUpdated = By
				.xpath("//div[@class='hamburger-menu-container']//img[@class='mobile-logo']");
		logOutCTA = By.xpath("//li[@role='treeitem']//a[@class='nav-logout-button' and span[text()='Log Out']]");
		invalidPassword = By
				.xpath("//p[contains(.,'Username and Password combination is incorrect')]");
		accountLocked = By
				.xpath("//p[contains(.,'Your account has been temporarily locked')]");
	}
	
	public void clearCache(){
		//ClearSafariCache(driver);
		clearCacheSafari();
		
		// Go to Home Page
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("keySequence", "HOME ");
		((JavascriptExecutor) driver).executeScript("mobile:presskey", param);

		// Launch Safari
		Map<String, Object> params1 = new HashMap<>();
		params1.put("identifier", "com.apple.mobilesafari");
	    ((JavascriptExecutor) driver).executeScript("mobile:application:open", params1);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		switchToContext(driver, "WEBVIEW");
	}
	
	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void launchApplication(String strBrowserInUse, String strBrowserName, String strModel,
			String strPlatformName, String strPlatformVR, String strBrowserVersion,String strLocation) {
		this.driver = checkAndSetBrowser(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,
				strBrowserVersion,strLocation);
		getBrowserName();
		navigateToURL(tcConfig.getConfig().get("NewCUIUrl"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(mobileCUILogo),"CUI Logo Not Present");
		tcConfig.updateTestReporter("CUILoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}
	
	/*
	 * Method: closeCovidAlert Description: Close Covid Date: Apr/"ExplicitLongWait""ExplicitLongWait" Author:
	 * Unnat Jain Changes By: Kamalesh
	 */
	
	public void closeCovidAlert() {
		try {
			WebElement covidClose = getList(covidAlertClose).get(1);
			if (verifyObjectDisplayed(covidClose)) {
				clickElementBy(covidClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CUILoginPage", "closeAlert", Status.PASS, "Successfully Closed Alert");
			}

		} catch (Exception e) {
			log.error("Covid Message Not Present");

		}
	}
	
	/*
	 * Method: setUserName Description: Set Username with correct data Date:
	 * Feb/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: Kamalesh Gupta(IOS)
	 */
	
	public void setUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
		getElementInView(modalHeader);
		// Username entry
		String strUserName = testData.get("CUI_username");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Username*",strUserName);
		//fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "setUserName()", Status.PASS,
				"Username is entered for CUI Login");
	}
	
	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * Feb/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	
	public void setPassword() throws Exception {
		// password data
		String password = testData.get("CUI_password");
		// decodeed password
		String decodedPassword = decodePassword(password);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password*",decodedPassword);
		//fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("CUILoginPage", "setPassword()", Status.PASS, "Entered password for CUI login");
	}
	
	/*
	 * Method: invalidUsername Description: Set Username with incorrect data
	 * Date: Feb/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	
	public void invalidUsername() {
		// invalid username
		String strUserName = testData.get("Invalid_UserName");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Username*",strUserName);
		//fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "invalidUsername()", Status.PASS,
				"Username is entered for CUI Login");
	}

	/*
	 * Method: invalidPassword Description: Set password with incorrect data
	 * Date: Feb/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	
	public void invalidPassword() throws Exception {
		// invalid password
		String invalidPassword = testData.get("Invalid_Pass");
		// decodeed password
		String decodedPassword = decodePassword(invalidPassword);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password*",decodedPassword);
		//fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("CUILoginPage", "invalidPassword()", Status.PASS,
				"Entered password for CUI login");
	}

	/*
	 * Method: loginCTAClick Description: Click On login cta Date: Feb/"ExplicitLongWait""ExplicitLongWait"
	 * Author: Unnat Jain Changes By: Kamalesh Gupta(IOS)
	 */
	
	public void loginCTAClick() throws Exception {
		waitUntilElementVisibleBy(driver, loginButton, "ExplicitLongWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)&&verifyObjectEnabled(loginButton)) {
			getElementInView(loginButton);
			clickElementBy(loginButton);
			acceptAlert();
			checkErrorClickLoginAgain();
			waitUntilElementVisibleBy(driver, welcomeHeader, "ExplicitLongWait");
			if (verifyObjectDisplayed(welcomeHeader)) {
				tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.PASS, "User Login successful");
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.FAIL, "Login failed");
			}

		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}
		
	/*
	 * /* Method: preEnteredUsernameVal Description: pre Entered Username Val
	 * Date: Mar/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void preEnteredUsernameValidation(String className) throws Exception {
		waitUntilElementVisibleBy(driver, loginUsername, "ExplicitLongWait");
		// username field value
//		if (className.contains("_")) {
//			className = className.split("\\_")[0];
//		}

		String usernameValue = getObject(loginUsername).getAttribute("value").toUpperCase();

		if (usernameValue.length() > 1) {
			tcConfig.updateTestReporter("CUILoginPage", "preEnteredUsernameValidation", Status.PASS,
					"Username pre populated as " + usernameValue);
			testData.put("CUI_username", usernameValue);
			//writeDataInExcel(className, CUIScripts_MobileIOS.testName, "CUI_username", usernameValue);

			clickElementBy(loginUsername);
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "preEnteredUsernameValidation", Status.FAIL,
					"Username was not pre populated");
		}

	}
	
	/*
	 * Method: enterPasswordOnly Description: pre Entered Username password
	 * enter Date: Mar/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void enterPasswordOnly() throws Exception {
		waitUntilElementVisibleBy(driver, fieldPassword, "ExplicitLongWait");
		// password data
		String password = testData.get("CUI_password").trim();
		String decodedPassword = decodePassword(password);
		clickElementBy(fieldUsername);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password*",decodedPassword);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	
	/*
	 * Method: logOutApplication Description: Logout from application clicking the Hamburger menu
	 * Feb/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: Kamalesh Gupta
	 */	
	public void logOutApplication() {
		// to be removed once log out is fixed
		clickElementBy(hamburgerMenu);
		waitUntilElementVisibleBy(driver, logOutCTA, "ExplicitLongWait");
		getElementInView(logOutCTA);
		clickElementBy(logOutCTA);
		//waitUntilElementVisibleBy(driver, usernameText, "ExplicitLongWait");
		tcConfig.updateTestReporter("CUILoginPage", "logOutApplication()", Status.PASS,
				"Page logged out successfully");

	}
	
	/*
	 * Method: logOutApplicationViaDashboard Description: Account Log out via
	 * dasboard page Validations Date: Apr/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By:
	 * NA
	 */
	public void logOutApplicationViaDashboard() {
		navigateToURL(tcConfig.getConfig().get("NewCUILogOutUrl"));
		waitUntilElementVisibleBy(driver, ownerFullName, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));	
		clickElementBy(hamburgerMenu);
		waitUntilElementVisibleBy(driver, logOutCTA, "ExplicitLongWait");
		getElementInView(logOutCTA);
		clickElementBy(logOutCTA);
		//waitUntilElementVisibleBy(driver, usernameText, "ExplicitLongWait");
		tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS, "User Logged Out");
	}
	/*
	 * Method: multipleInvalidLogin Description: Account Lock Validations Date:
	 * Mar/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: NA
	 */
	public void multipleInvalidLogin() throws Exception {
		// total login attempt count
		int loginAttempt = Integer.parseInt(testData.get("Attempts").trim());
		for (int attempt = 1; attempt <= loginAttempt; attempt++) {
			driver.navigate().refresh();
			waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
			getElementInView(modalHeader);
			setUserName();
			invalidPassword();
			waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
			multilpeLoginClick();
			verifyLoginError(attempt);
		}
	}
	
	
	/*
	 * Method: verifyLoginError Description: To Verify LoginErrors Date:
	 * Mar/"ExplicitLongWait""ExplicitLongWait" Author: Unnat Jain Changes By: NA
	 */
	public void verifyAccountLocked() {
		// total count of login attempt
		int attemptCount = Integer.parseInt(testData.get("Attempts"));
		waitForElementError(attemptCount);
		if (verifyObjectDisplayed(accountLocked) && attemptCount == 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyError", Status.PASS,
					"Account is Locked and User Cannt Login");
		} else if (verifyObjectDisplayed(welcomeHeader) && attemptCount < 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.PASS,
					"Account is not locked after 4 attempts");
			logOutApplication();
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.FAIL,
					"Failed to validate account locked login");
		}
	}
}
