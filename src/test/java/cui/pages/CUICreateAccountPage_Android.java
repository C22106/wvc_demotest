package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICreateAccountPage_Android extends CUICreateAccountPage_Web {

	public static final Logger log = Logger.getLogger(CUICreateAccountPage_Android.class);
	
	protected By headerVerification = By.xpath("//h2[contains(text(),'Select a method of verification')]");

	public CUICreateAccountPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		selectCountryFlag = By.xpath("//div[@class ='flag-select']/a");
	}
	

	/*
	 * Method: enterPhoneNumber Description:Enter Phone Number Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void enterPhoneNumber(String phoneNumber) {
		getElementInView(phoneNumberField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver,"Phone Number",phoneNumber,2);
		getElementInView(headerVerification);
		getObject(headerVerification).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterPhoneNumber", Status.PASS,
				"Successfully entered Phone Number and the Phone number is: " + phoneNumber);
	}

	/*
	 * Method: enterFirstName Description:Enter First Name Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */

	public void enterFirstName() {
		String firstName = testData.get("FirstName").trim(); // First name Data
		getElementInView(firstNameField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver,"First Name*",firstName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterFirstName", Status.PASS,
				"Successfully entered first name, which is: " + firstName);
	}

	/*
	 * Method: enterLastName Description:Enter Last Name Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void enterLastName() {
		String lastName = testData.get("LastName").trim(); // Last name Data
		getElementInView(lastNameField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver,"Last Name*",lastName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterLastName", Status.PASS,
				"Successfully entered last name, which is: " + lastName);
	}

	/*
	 * Method: selectSuffix Description: Select Suffix Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void selectSuffix() {
		try {
			getElementInView(suffixField);

			if (testData.get("Suffix").equalsIgnoreCase("Sr.")) {
				new Select(getObject(suffixField)).selectByValue(" Sr.");
			} else if (testData.get("Suffix").equalsIgnoreCase("Jr.")) {
				new Select(getObject(suffixField)).selectByValue("Jr. ");
			} else {
				new Select(getObject(suffixField)).selectByValue(testData.get("Suffix"));
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUICreateAccountPage", "selectSuffix", Status.PASS,
					"Suffix entered is Not Correct");
		}
	}

	
	/*
	 * Method: enterMemberNumber Description:Enter Member Number Date: Mar/2020
	 * Author: Unnat Jain Changes By: Monideep(android)
	 */

	public void enterMemberNumber() {
		String memberNumber = testData.get("MemberNumber").trim(); // Member
																	// Number
																	// Data
		getElementInView(memberNumberField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver,"Member Number*",memberNumber);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterMemberNumber", Status.PASS,
				"Successfully entered member number and the member number is: " + memberNumber);
	}
	
	/*
	 * Method: enterCompanyFieldName Description:Enter Company Field Name Date:
	 * Mar/2020 Author: Unnat Jain Changes By:  Monideep(Android)
	 */
	public void enterCompanyFieldName() {
		String companyName = testData.get("CompanyName").trim(); // Dompany name
																	// data
		getElementInView(companyField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver,"Trust Name*",companyName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterCompanyFieldName", Status.PASS,
				"Successfully entered Company Trust name which is:  " + companyName);
	}
	
	/*
	 * Method: enterCompanyFieldName Description:Enter Company Field Name Date:
	 * Mar/2020 Author: Unnat Jain Changes By:  Monideep(Android)
	 */
	public void enterContractNumber(String contractNumber) {
	
		sendKeysByScriptExecutor((RemoteWebDriver)driver,"Contract Number*",contractNumber,2);
		getElementInView(headerVerification);
		getObject(headerVerification).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterContractNumber", Status.PASS,
				"Successfully entered Contract Number and the Contract number is: " + contractNumber);
	}
	
	/*
	 * Method: selectVerificationMethod Description:Select a verification
	 * methods Date: Mar/2020 Author: Unnat Jain Changes By: Monideep(Android)
	 */
	public void selectVerificationMethod() {
		// to slelect verification method
		String selectCreateAccountMethod = testData.get("Createaccmethod").trim();
		// Phone Number Data
		String phoneNumber = testData.get("PhoneNumber").trim();
		// Country Select
		String countrySelect = testData.get("Country").trim();
		// Contract Number Data
		String contractNumber = testData.get("ContractNumber").trim();
		if (selectCreateAccountMethod.equals("Phone")) {

			getElementInView(phoneNumberRadio);
			getObject(phoneNumberRadio).click();
			if (verifyObjectDisplayed(selectCountryFlag)) {
				selectCountryFlag(countrySelect);
				enterPhoneNumber(phoneNumber);
			} else {
				tcConfig.updateTestReporter("CUICreateAccountPage", "selectVerificationMethod", Status.FAIL,
						"Flag select field not present please check screenshot");
			}
		} else if (selectCreateAccountMethod.equals("Contract")) {
			getElementInView(contractNumberRadio);
			getObject(contractNumberRadio).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			enterContractNumber(contractNumber);
		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "selectVerificationMethod", Status.FAIL,
					"Please select method of verification from excel where the column name is Createaccmethod");
		}

	}
}
