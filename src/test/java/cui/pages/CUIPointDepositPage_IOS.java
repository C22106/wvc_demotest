
package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUIPointDepositPage_IOS extends CUIPointDepositPage_Web {

	public static final Logger log = Logger.getLogger(CUIPointDepositPage_IOS.class);

	public CUIPointDepositPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		buttonContinue = By.xpath("(//button[text() = 'Continue'])[2]");
		pointDepositRCIProgessBar = By.xpath("//div[contains(@class,'booking-header__mobile')]");
	}

	public By houseKeepingHeader=By.xpath("(//span[.='Housekeeping Credits'])[1]");
	/*
	 * Method: totalPointsToDeposit Description:totalPointsToDeposit Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void totalPointsToDeposit() {
		String totalDepositPoint = testData.get("PointsToDeposit").trim();
		String useYearToDeposit = testData.get("DepositUseYear").trim();
		String useYearArray[] = useYearToDeposit.split(";");
		for (int useyearIterator = 0; useyearIterator < useYearArray.length; useyearIterator++) {
			WebElement areaForDepositPoint = getObject(
					By.xpath("(//label[contains(@for,'points-to-be-deposited')])[" + (useyearIterator + 1) + "]"));
			getElementInView(areaForDepositPoint);
			sendKeysByScriptExecutor((RemoteWebDriver)driver, "Points to be Deposited*", totalDepositPoint,1);
			
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(houseKeepingHeader);
		}
	}
	
	/*
	 * Method: clickContinueButton Description:click Continue Button Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		if (verifyObjectDisplayed(buttonContinue)) {
			getElementInView(buttonContinue);
			clickElementBy(buttonContinue);
		}

	}
}
