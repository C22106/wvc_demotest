package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;

public class CUINewPasswordPage_IOS extends CUINewPasswordPage_Web {

	public static final Logger log = Logger.getLogger(CUINewPasswordPage_IOS.class);

	public CUINewPasswordPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		

	}

	/*
	 * Method: newPasswordDataEnter Description: New Password Page Val Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newPasswordDataEnter() {
		waitUntilElementVisibleBy(driver, newPasswordField, "ExplicitLongWait");
		getElementInView(newPasswordField);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Password", testData.get("Password"),3);
		


	}
}