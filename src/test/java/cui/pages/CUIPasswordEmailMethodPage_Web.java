package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import automation.core.TestConfig;

public class CUIPasswordEmailMethodPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIPasswordEmailMethodPage_Web.class);

	public CUIPasswordEmailMethodPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	// Email Page
	protected By checkEmailHeader = By.xpath("//h2[contains(.,'CHECK YOUR EMAIL')]");
	protected By checkEmailDescription = By.xpath("//h2[contains(.,'CHECK YOUR EMAIL')]/following-sibling::div//p");
	protected By enterCodeHeader = By.xpath("(//h2[contains(.,'Enter Your Code')] | //form/div[contains(.,'Enter Your Code')])");
	protected By verificationCodeField = By.xpath("//input[@id='verificationCode']");
	protected By newCodeText = By.xpath("//p[contains(.,'request a new one or contact us')]");
	protected By disabledContinueCTA = By.xpath("//button[contains(.,'Continue') and @disabled]");

	/*
	 * Method: emailPageValidation Description:Email Page Validations Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void emailPageValidation() {

		waitUntilElementVisibleBy(driver, verificationCodeField,  "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(verificationCodeField),"Verification Code field not present");
		elementPresenceVal(checkEmailHeader, "Check Your Email Header", "CUIPasswordEmailMethodPage", "emailPageVal");
		elementPresenceVal(checkEmailDescription, "Check Your Email Description", "CUIPasswordEmailMethodPage",
				"emailPageVal");
		elementPresenceVal(enterCodeHeader, "Modal header", "CUIPasswordEmailMethodPage", "emailPageVal");
		elementPresenceVal(verificationCodeField, "Code Field", "CUIPasswordEmailMethodPage", "emailPageVal");
		elementPresenceVal(newCodeText, "Resent Code Text", "CUIPasswordEmailMethodPage", "emailPageVal");
		elementPresenceVal(disabledContinueCTA, "Disabled Continue CTA", "CUIPasswordEmailMethodPage", "emailPageVal");

	}

}