package cui.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

//import salepoint.pages.LogInPage;

public class CUISearchPage_Android extends CUISearchPage_Web {

	public static final Logger log = Logger.getLogger(CUISearchPage_Android.class);
	protected By mapbutton = By.xpath("//button[text()='Show Map']");

	public CUISearchPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		flexibleResultDatePill = By
				.xpath("//div[contains(@class,'pill-container')]//div[@class = 'pill']/a//span[@class='mobile']");
	}
	
	/*
	 * Method: returnConvertedDate Description:Return converted flexible date
	 * into String Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public List<String> returnConvertedDate(List<String> strDate, String addYear) {
		// List for updated flexible dates displayed in UI
		List<String> updatedDate = new ArrayList<String>();
		try {
			for (int totdalDate = 0; totdalDate < strDate.size(); totdalDate++) {
				// Check In Date from UI in format(Monday 4/13 → Thursday 4/16)
				String strCheckIn = strDate.get(totdalDate).split("→")[0];
				strCheckIn = strCheckIn.replace("\\n", "");
				strCheckIn = strCheckIn.split(" ")[1].trim();
				strCheckIn = strCheckIn.concat("/" + addYear);
				updatedDate.add(strCheckIn);

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISearcPage", "returnConvertedDate", Status.FAIL,
					"Date format not supported");
		}

		return updatedDate;

	}

	/*
	 * Method: validateMapPresent Description:Check Map Displayed Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void validateMapPresent() {
		waitUntilElementVisibleBy(driver, mapbutton, "ExplicitLongWait");
		getObject(mapbutton).click();
		waitUntilElementVisibleBy(driver, map, "ExplicitLongWait");
		if (verifyObjectDisplayed(map)) {
			tcConfig.updateTestReporter("CUISearchPage", "validateMapPresent", Status.PASS,
					"Map Present in Search Page");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateMapPresent", Status.FAIL,
					"Map not Present in Search Page");
		}
	}

	/*
	 * Method: checkWorldmark Description:Check Worldmark Resort Presence
	 * Date:Mar/2020 Author: Abhijeet Roy Changes By: Monideep
	 */
	public void checkWorldmark() {
		String resortWorldmarkPresent = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				resortWorldmarkPresent = "Y";
				int iteratorStartWorldmark = resortNameIterator;
				for (int IteratorWorldmarkResort = iteratorStartWorldmark; IteratorWorldmarkResort < getList(
						textResortName).size(); IteratorWorldmarkResort++) {
					String resortname1 = getList(textResortName).get(IteratorWorldmarkResort).getText().trim();
					if (resortname1.contains("Wyndham")) {
						tcConfig.updateTestReporter("CUISearchPage", "checkWorldmark", Status.FAIL,
								"WorldMark resort not present at last");

					} else {
						int indexWorldMarkResort = IteratorWorldmarkResort + 1;
						tcConfig.updateTestReporter("CUISearchPage", "checkWorldmark", Status.PASS,
								"WorldMark Resort with resort name: " + resortname1 + " present with index: "
										+ indexWorldMarkResort);

						try {
							WebElement worldmarkresort = getObject(
									By.xpath("(//div[contains(@class,'resort-card__content')])[" + indexWorldMarkResort
											+ "]/div/a[contains(.,'View')]"));
							if (verifyObjectDisplayed(worldmarkresort)) {
								getElementInView(worldmarkresort);
								tcConfig.updateTestReporter("CUISearchPage", "checkworldmark", Status.FAIL,
										"View Available unit type present");
							} else {
								tcConfig.updateTestReporter("CUISearchPage", "checkworldmark", Status.PASS,
										"View Available unit type not present for resort: " + resortname1);
							}
						} catch (Exception e) {
							tcConfig.updateTestReporter("CUISearchPage", "checkworldmark", Status.PASS,
									"View Available unit type not present for resort: " + resortname1);
						}

					}
				}
				break;
			}

		}
		if (resortWorldmarkPresent == "Y") {
			tcConfig.updateTestReporter("CUISearchPage", "checkworldmark", Status.PASS, "Worldmark Resort Present");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkworldmark", Status.FAIL,
					"Worldmark resort not present");
		}
	}
	
	/*
	 * Method: verifyCheckoutDateSelected Description:compare checkout date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyCheckoutDateSelected() throws Exception {
		String checkOutDate = testData.get("Checkoutdate").trim();
		if (verifyObjectDisplayed(textBoxDateSelect)) {
			String checkInDateScreen = (getObject(textBoxDateSelect).getAttribute("value").split("\u2192")[1]).trim();
			String convertedCheckOutDate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkOutDate);
			if (checkInDateScreen.compareTo(convertedCheckOutDate) == 0) {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.PASS,
						"Check out date selected is : " + checkOutDate + " and from screen is: "
								+ convertedCheckOutDate);
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.FAIL,
						"Check out date selected and in screen did not matched");
			}

		} else {
			tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.FAIL,
					"Date Field in Search Page not Present");
		}

	}
	
	/*
	 * Method: verifyCheckinDateSelected Description:compare checkin date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void verifyCheckinDateSelected() throws Exception {
		String checkInDate = testData.get("Checkindate").trim();
		if (verifyObjectDisplayed(textBoxDateSelect)) {
			String checkInDateScreen = (getObject(textBoxDateSelect).getAttribute("value").split("\u2192")[0]).trim();
			String convertedCheckInDate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkInDate);
			if (checkInDateScreen.compareTo(convertedCheckInDate) == 0) {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.PASS,
						"Check in date selected is : " + checkInDate + " and from screen is: " + convertedCheckInDate);
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckinDateSelected", Status.FAIL,
						"Check in date selected and in screen did not matched");
			}

		} else {
			tcConfig.updateTestReporter("CUISearcPage", "verifyResortCheckinDateSelected", Status.FAIL,
					"Date Field in Search Page not Present");
		}

	}

}
