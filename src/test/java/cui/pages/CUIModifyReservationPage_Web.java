package cui.pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIModifyReservationPage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIModifyReservationPage_Web.class);

	public CUIModifyReservationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	public static String checkoutDate;
	public static String checkinDate;

	protected By fixedWeekBookedOverlay = By
			.xpath("//div[@id = 'errorDetails']//div[contains(text(),'no availability for your suite')]");

	protected By dateInModificationTable = By.xpath(
			"//div[contains(@class,'holiday modification-history')]//a[contains(@id,'accordion-label')]//p/span");

	protected By vipUpgradeSection = By
			.xpath("//div[text()='VIP Upgrades']/ancestor::a[contains(@id,'accordion-label')]");
	protected By accessibleUnit = By.xpath(
			"//div[text()='VIP Upgrades']/ancestor::a[contains(@id,'accordion-label')]/following-sibling::div//span[contains(.,'Hearing Impaired')]");
	protected By pointProtectionSection = By
			.xpath("//div[text()='Points Protection']/ancestor::a[contains(@id,'accordion-label')]");
	protected By acceptedStatus = By.xpath(
			"//div[text()='Points Protection']/ancestor::a[contains(@id,'accordion-label')]/following-sibling::div//span[.='Accepted']");
	protected By headerReservationDetails = By.xpath("//span[text() = 'Reservation Details']");
	protected By sectionLocationContainer = By
			.xpath("//section[@class = 'reservation-summary__location-container']/div/h2");
	protected By sectionDateContainerCheckin = By
			.xpath("(//span[contains(@class,'reservation-summary__dates')]/time)[1]");
	protected By sectionDateContainerCheckout = By
			.xpath("(//span[contains(@class,'reservation-summary__dates')]/time)[2]");
	protected By sectionUnitTypeContainer = By
			.xpath("//section[@class = 'reservation-summary__unit-type-container']/span");
	protected By sectionTotalCostContainer = By
			.xpath("//section[@class = 'reservation-summary__unit-cost-container']/span");
	protected By headerModifyReservation = By.xpath("//h1[text() = 'Modify Reservation']");

	protected By cardModifyReservation = By.xpath(
			"//section[contains(@class,'modify-reservation__card notification')] | //a[text() = 'Modify Traveler']//../..");
	protected By textCardTitle = By.xpath("(//h3[text() = 'Whoâ€™s Traveling'] | //div[text() = 'Who’s Traveling'])");
	protected By buttonModifyTraveler = By.xpath("//a[text() = 'Modify Traveler']");
	protected By modificationHistoryList = By.xpath("//div[contains(@class,'holiday modification-history')]");
	protected By headerModification = By.xpath("//p[text() = 'Modifications']");
	protected By headerDate = By.xpath("//div[text() = 'Date']");
	protected By headerModificationType = By.xpath("//div[text() = 'Modification Type']");
	protected By sectionReservationSummary = By.xpath("//section[@class = 'reservation-summary']");
	protected By firstModifiedTravelerSection = By
			.xpath("(//div[text()='Modify Traveler']/ancestor::a[contains(@id,'accordion-label')])[1]");
	protected By firstModifiedTravelerName = By
			.xpath("(//div[contains(@id,'accordion') and @role='tabpanel']//p/span)[1]");
	protected By textRevisedOwner = By.xpath("//th[text() = 'Revised']/../td");
	
	// Add Nights
	protected By addNightsCTA = By.xpath("//button[contains(.,'Add Night')]");
	protected By reservationDatesHeader = By
			.xpath("(//h3[text() = 'Reservation Dates'] | //div[text() = 'Reservation Dates'])");
	protected By reservationDate = By
			.xpath("//p[text() = 'Dates']/following-sibling::p | //span[text() = 'Dates']/../following-sibling::p");
	protected By unableToAddNights = By.xpath("//div[@class='reveal-overlay']//div[text()='Unable to Add Nights']");
	protected By maxLengthOfStay = By
			.xpath("//div[@class='reveal-overlay']//p[contains(.,'you have reached the maximum length of stay')]");
	protected By backToModifyButton = By.xpath("//div[@class='reveal-overlay']//button[contains(.,'Back')]");

	protected By BackToModifyReservationButton = By.xpath("//button[contains(text(),'Back to Modify Reservation')]");
	protected By warningSymbol = By.xpath("//*[@id='icon/alert/warning_24px']");
	protected By closeModalCTA = By.xpath("//div[@id='errorDetails']//button[@class='close-button']");;
	protected By unableToAddNightsHeader = By
			.xpath("//div[contains(@class,'heading') and text()='Unable to Add Nights']");
	protected By closeBackToAvailability = By.xpath("//button[@class='close-button']");
	protected By unableToAddNightsText = By.xpath("//p[contains(text(),'your check-in date is within 48 hours')]");
	protected By currentTraveler = By.xpath(
			"(//p[text() = 'Traveler']/..//p)[2] | //span[text() = 'Traveler']/../..//p[contains(@class,'body')]");

	/*
	 * Method: verifyHeaderModifyReservation Description:verify Header Modify
	 * Reservation Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyHeaderModifyReservation() {
		waitUntilElementVisibleBy(driver, headerModifyReservation, "ExplicitLongWait");
		elementPresenceVal(headerModifyReservation, "Header Modify Reservation", "CUIModifyReservationPage",
				"verifyHeaderModifyReservation");
	}

	/*
	 * Method: verifyModifyCardDetails Description:verify Modify Card Details
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyModifyCardDetails() {
		if (verifyObjectDisplayed(cardModifyReservation)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.PASS,
					"Modify Reservation Card is Present");
			elementPresenceVal(textCardTitle, "Card Title", "CUIModifyReservationPage", "verifyModifyCardDetails");
			if (verifyObjectDisplayed(currentTraveler)) {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.PASS,
						"Current Traveler is: " + getObject(currentTraveler).getText().trim());
			} else {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.FAIL,
						"Current Traveler name not present");
			}
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.FAIL,
					"Card Modify Reservation not Present");
		}
	}

	/*
	 * Method: listModificationHistory Description:list Modification History
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void listModificationHistory() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		elementPresenceVal(headerModification, "Header Modification", "CUIModifyReservationPage",
				"listModificationHistory");
		if (getList(modificationHistoryList).size() > 0) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "listModificationHistory", Status.PASS,
					"Total Number of Modification done are: " + getList(modificationHistoryList).size());
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "listModificationHistory", Status.PASS,
					"No modification made");
		}
	}

	/*
	 * Method: checkModificationHistoryCollapsedMode Description:check
	 * Modification History Collapsed Mode Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void checkModificationHistoryCollapsedMode() {
		String modificationCollapse = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(modificationHistoryList);
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				WebElement eachModificationList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//div/a"));
				if (eachModificationList.getAttribute("aria-expanded").equalsIgnoreCase("false")) {
					getElementInView(eachModificationList);
					clickElementJSWithWait(eachModificationList);
					modificationCollapse = "Y";
					checkModificationDescription(listmodificationiterator);
					clickElementJSWithWait(eachModificationList);
				} else {
					modificationCollapse = "N";
					break;
				}
			}
			if (modificationCollapse.equals("Y")) {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode",
						Status.PASS, "All Modification History are in collapsed mode");
			} else {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode",
						Status.PASS, "All Modification History are not in collapsed mode");
			}
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode",
					Status.FAIL, "No modification history");
		}

	}

	/*
	 * Method: vaildateHeaderDateinModificationHistory Description:vaildate
	 * Header Date in Modification History Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void vaildateHeaderDateinModificationHistory() {
		getElementInView(headerDate);
		if (verifyObjectDisplayed(headerDate)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "vaildateHeaderDateinModificationHistory",
					Status.PASS, "Date Header is present in Modification History");
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "vaildateHeaderDateinModificationHistory",
					Status.FAIL, "Date Header is not present in Modification History");
		}
	}

	/*
	 * Method: checkDateInModificationHistory Description:check Date In
	 * Modification History Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkDateInModificationHistory() {
		String formatModificationHistoryDate = null;
		if (getList(modificationHistoryList).size() > 0) {
			getElementInView(modificationHistoryList);
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				String eachModificationDateList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//a[contains(@id,'accordion-label')]/p/span")).getText().trim();
				if (isValidDate(eachModificationDateList, "MM/dd/yy")) {
					formatModificationHistoryDate = "Y";
				} else {
					formatModificationHistoryDate = "N";
					break;
				}
			}
			if (formatModificationHistoryDate.equals("Y")) {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkDateInModificationHistory",
						Status.PASS, "All Modification Dates are in required Format");
			} else {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkDateInModificationHistory",
						Status.FAIL, "All Modification Dates are not in required Format");
			}
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode",
					Status.FAIL, "No modification history");
		}

	}

	/*
	 * Method: vaildateHeaderModificationTypeinModificationHistory
	 * Description:vaildateHeaderModificationTypeinModificationHistory Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void vaildateHeaderModificationTypeinModificationHistory() {
		if (verifyObjectDisplayed(headerModificationType)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage",
					"vaildateHeaderModificationTypeinModificationHistory", Status.PASS,
					"Modification Type Header is present in Modification History");
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage",
					"vaildateHeaderModificationTypeinModificationHistory", Status.FAIL,
					"Modification Type Header is not present in Modification History");
		}
	}
	
	

	/*
	 * Method: checkModificationTypeInModificationHistory Description:check
	 * Modification Type In Modification History Date: June/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void checkModificationTypeInModificationHistory() {
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//a[contains(@id,'accordion-label')]/div"));
				if (verifyObjectDisplayed(eachModificationTypeList)) {
					String modificationType = getElementText(eachModificationTypeList);
					tcConfig.updateTestReporter("CUIModifyReservationPage",
							"checkModificationTypeInModificationHistory", Status.PASS,
							"Modification Type is : " + modificationType);
				} else {
					tcConfig.updateTestReporter("CUIModifyReservationPage",
							"checkModificationTypeInModificationHistory", Status.FAIL,
							"Modification Type not Present");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode",
					Status.FAIL, "No modification history");
		}
	}

	/*
	 * Method: checkModificationDescription Description:check Modification
	 * Description Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkModificationDescription(int iterator) {
		WebElement eachModificationdescription = getObject(
				By.xpath("//div[contains(@class,'holiday modification-history')][" + iterator + "]//div/a"));
		if (verifyObjectDisplayed(eachModificationdescription)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationDescription", Status.PASS,
					"Modification Description present as: " + getElementText(eachModificationdescription));
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationDescription", Status.FAIL,
					"Modification Description not Present");
		}
	}

	/*
	 * Method: checkSectionReservationSummary Description:check Section
	 * Reservation Summary Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkSectionReservationSummary() {
		getElementInView(headerReservationDetails);
		// driver.findElement(By.xpath("//button[@class =
		// 'reservation-summary__headline-arrow']")).click();
		waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
		if (verifyObjectDisplayed(sectionReservationSummary)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkSectionReservationSummary", Status.PASS,
					"Reservation section is present in the Modify reservation page");
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkSectionReservationSummary", Status.PASS,
					"Reservation section is not present in the Modify reservation page");
		}
	}

	/*
	 * Method: checkReservationSummaryDetails Description:check Reservation
	 * Summary Details Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkReservationSummaryDetails() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		elementPresenceVal(headerReservationDetails, "Header Reservation Details", "CUIModifyReservationPage",
				"checkReservationSummaryDetails");
		elementPresenceVal(sectionLocationContainer, "Location " + getElementText(sectionLocationContainer),
				"CUIModifyReservationPage", "checkReservationSummaryDetails");
		elementPresenceVal(sectionDateContainerCheckin, "Checkin Date " + getElementText(sectionDateContainerCheckin),
				"CUIModifyReservationPage", "checkReservationSummaryDetails");
		elementPresenceVal(sectionDateContainerCheckout,
				"Checkout Date " + getElementText(sectionDateContainerCheckout), "CUIModifyReservationPage",
				"checkReservationSummaryDetails");
		elementPresenceVal(sectionUnitTypeContainer, "Unit Type " + getElementText(sectionUnitTypeContainer),
				"CUIModifyReservationPage", "checkReservationSummaryDetails");
		elementPresenceVal(sectionTotalCostContainer, "Total Cost " + getElementText(sectionTotalCostContainer),
				"CUIModifyReservationPage", "checkReservationSummaryDetails");
	}

	/*
	 * Method: clickModifyTravelerButton Description:click Modify Traveler
	 * Button Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickModifyTravelerButton() {
		Assert.assertTrue(verifyObjectDisplayed(buttonModifyTraveler), "Modify Traveler button not present");
		clickElementJSWithWait(buttonModifyTraveler);
	}

	/*
	 * Method: checkOwnerModifiedAfterModification Description:
	 * checkOwnerModifiedAfterModification Date: June/2020 Author: Abhijeet
	 * Changes By: Kamalesh
	 */

	public void checkOwnerModifiedAfterModification() {
		String modifyTraveler = null;
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//a[contains(@id,'accordion-label')]/div"));
				if (verifyObjectDisplayed(eachModificationTypeList)) {
					String modificationType = getElementText(eachModificationTypeList);
					if (modificationType.equalsIgnoreCase("Modify Traveler")) {
						modifyTraveler = "Y";
						validateModifiedTravlerName();
						break;
					} else {
						modifyTraveler = "N";
					}

				} else {
					tcConfig.updateTestReporter("CUIModifyReservationPage",
							"checkModificationTypeInModificationHistory", Status.FAIL,
							"Modification Type not Present");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode",
					Status.FAIL, "No modification history");
		}

		if (modifyTraveler == "Y") {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "checkOwnerModifiedAfterModification",
					Status.PASS, "Modification history list updated");
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "checkOwnerModifiedAfterModification",
					Status.FAIL, "Failed to Update modification History List");
		}
	}

	/*
	 * Method: validateModifiedTravlerName Description:
	 * validateModifiedTravlerName Date: June/2020 Author: Kamalesh Changes By:
	 */

	public void validateModifiedTravlerName() {
		String travlerType = testData.get("ModifyTravelerTo");
		String OwnerName = testData.get("ModifyOwner");
		clickElementBy(firstModifiedTravelerSection);
		getElementInView(firstModifiedTravelerSection);
		if (travlerType.equalsIgnoreCase("Guest")) {
			if (getElementText(firstModifiedTravelerName).contains(testData.get("GuestFullName"))) {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationTypeInModificationHistory",
						Status.PASS, "Modified Traveler Name is displayed correctly");
			}
		} else if (travlerType.equalsIgnoreCase("Owner")) {
			String[] arr = OwnerName.split(" ");
			if (arr.length >= 3) {
				assertTrue(getElementText(firstModifiedTravelerName).contains(arr[0] + " " + arr[arr.length - 1]) || getElementText(firstModifiedTravelerName).contains(OwnerName),
						"Modified Traveler Name is incorrect in the Modification History");
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationTypeInModificationHistory",
						Status.PASS, "Modified Traveler Name is displayed correctly in the Modification History");
			} else if (arr.length < 3) {
				assertTrue(getElementText(firstModifiedTravelerName).contains(OwnerName),
						"Modified Traveler Name is incorrect in the Modification History");
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationTypeInModificationHistory",
						Status.PASS, "Modified Traveler Name is displayed correctly in the Modification History");
			}

		}
	} 
	
	

	/*
	 * Method: checkPointProtectionSection Description:
	 * checkPointProtectionSection Date: June/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */
	public void checkPointProtectionSection() {
		getElementInView(pointProtectionSection);
		List<WebElement> allOptions = getList(pointProtectionSection);
		if (allOptions.size() > 0) {
			for (WebElement we : allOptions) {
				assertTrue(verifyObjectDisplayed(we), "Point Protection Section not present");
				clickElementBy(we);
				if (getElementText(acceptedStatus).equalsIgnoreCase("Accepted")) {
					tcConfig.updateTestReporter("CUICompleteModificationPage", "checkPointProtectionSection",
							Status.PASS, "Point Protection Section present and Status is Accepted");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage",
					"verifyVipUpgradeWithAccessibleUnitInModificationHistory", Status.FAIL,
					"VIP Upgrade Section Not displayed");
		}
	}

	/*
	 * Method: verifyVipUpgradeWithAccessibleUnitInModificationHistory
	 * Description: verifyVipUpgradeWithAccessibleUnitInModificationHistory
	 * Date: June/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void verifyVipUpgradeWithAccessibleUnitInModificationHistory() {
		getElementInView(vipUpgradeSection);
		List<WebElement> allOptions = getList(vipUpgradeSection);
		if (allOptions.size() > 0) {
			for (WebElement we : allOptions) {
				assertTrue(verifyObjectDisplayed(we), "VIP Upgrade Section not present");
				clickElementBy(we);
				if (getElementText(accessibleUnit).contains("Hearing Impaired")) {
					tcConfig.updateTestReporter("CUICompleteModificationPage",
							"verifyVipUpgradeWithAccessibleUnitInModificationHistory", Status.PASS,
							"VIP Upgrade Preference with accessible Unit is displayed");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage",
					"verifyVipUpgradeWithAccessibleUnitInModificationHistory", Status.FAIL,
					"VIP Upgrade Section Not displayed");
		}
	}

	/*
	 * Method: dateOldestToNewestModificationTable Description:date Oldest To
	 * Newest Modification Table Date:June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */

	public void dateOldestToNewestModificationTable() throws Exception {
		descendingOrderDateCheck(dateInModificationTable, "MM/dd/yy", "Dates in Modification Table");
	}

	/*
	 * Method: verifyRevisedOwner Description:date Revised OWner Verification
	 * Date:June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyRevisedOwner() {
		String modifyToOwner = testData.get("ModifyOwner");
		if (verifyObjectDisplayed(textRevisedOwner)) {
			if (getElementText(textRevisedOwner).equals(modifyToOwner)) {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.PASS,
						"Revised Owner is: " + getElementText(textRevisedOwner));
			} else {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
						"Owner changed and revised from screen did not match");
			}

		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
					"Revised Owner text and value not present");
		}
	}

	/*
	 * Method: verifyModifyNightsCardDetails Description:verify Modify Nights
	 * Card Details Date: Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyModifyNightsCardDetails() {
		if (verifyObjectDisplayed(addNightsCTA)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyNightsCardDetails", Status.PASS,
					"Modify Nights Reservation Card is Present");
			elementPresenceVal(reservationDatesHeader, "Reservation Date Header", "CUIModifyReservationPage",
					"verifyModifyNightsCardDetails");
			Assert.assertTrue(verifyObjectDisplayed(reservationDate), "Reservation Dates not present");
			tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyNightsCardDetails", Status.PASS,
					"Current Reservation date is: " + getObject(reservationDate).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyNightsCardDetails", Status.FAIL,
					"Card Modify Reservation Nights not Present");
		}
	}

	/*
	 * Method: clickModifyNightsButton Description:click Modify Nights Button
	 * Date: Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clickModifyNightsButton() {
		Assert.assertTrue(verifyObjectDisplayed(addNightsCTA), "Modify Nights button not present");
		clickElementJSWithWait(addNightsCTA);
	}

	/*
	 * Method: unableToModifyHeader Description:Unable To Modify Pop Up
	 * Validation Date: Aug/2020 Author: Unnat Jain Changes By: NA
	 */
	public void unableToModifyHeader() {
		waitUntilElementVisibleBy(driver, unableToAddNights, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(unableToAddNights), "Unable To Add Nights Header not displayed");
		Assert.assertTrue(verifyObjectDisplayed(maxLengthOfStay), "Pop Up Description not displayed");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "unableToModifyHeader", Status.PASS,
				"Unable To Add Nights Header displayed with proper description");
		clickElementBy(backToModifyButton);
	}

	/*
	 * Method: verifyBackToModifyReservation Description: verify Back To Modify
	 * Reservation Date: August/2020 Author: Kamalesh Roy Changes By: NA
	 */
	public void verifyBackToModifyReservation() {

		waitUntilElementVisibleBy(driver, BackToModifyReservationButton, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(unableToAddNightsHeader),
				"Unable To Add Nights Pop Up Modal is not displayed");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyBackToModifyReservation", Status.PASS,
				"Unable To Add Nights Pop Up Modal is displayed and the title is present as: "
						+ getElementText(unableToAddNightsHeader));

		Assert.assertTrue(verifyObjectDisplayed(unableToAddNightsText), "An instruction text is not present");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyBackToModifyReservation", Status.PASS,
				"User is unable to add night. An instruction text is present as: "
						+ getElementText(unableToAddNightsText));

		Assert.assertTrue(verifyObjectDisplayed(warningSymbol), "A warning sign is not present on the Pop Up Modal");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyBackToModifyReservation", Status.PASS,
				"A warning sign is present on the Pop Up Modal");

		Assert.assertTrue(
				verifyObjectDisplayed(
						getList(closeBackToAvailability).get(getList(closeBackToAvailability).size() - 1)),
				"Close symbol is not present");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyBackToModifyReservation", Status.PASS,
				"Close symbol is present to close the Pop Up Modal");

		Assert.assertTrue(verifyObjectDisplayed(BackToModifyReservationButton),
				"Back To Modify Reservation Button is not present");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyBackToModifyReservation", Status.PASS,
				"Back To ModifyReservation Button is present");
		clickElementBy(BackToModifyReservationButton);
		Assert.assertTrue(verifyObjectDisplayed(headerModifyReservation), "Back To Availability Button is not present");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyBackToModifyReservation", Status.PASS,
				"On Clicking Back To Modify Reservation Button, user navigated to the Modify Reservation page");
	}

	/*
	 * Method: getCheckOutDateFromAddNightsCard Description:
	 * getCheckOutDateFromAddNightsCard Date: August/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void getCheckOutDateFromAddNightsCard() {
		Assert.assertTrue(verifyObjectDisplayed(reservationDate), "Reservation Dates not present");
		checkoutDate = getElementText(reservationDate).split("→")[1].trim();
		tcConfig.updateTestReporter("CUIModifyReservationPage", "getCheckOutDateFromAddNightsCard", Status.PASS,
				"Check out date before modifying: " + checkoutDate);

	}

	/*
	 * Method: getCheckInDateFromAddNightsCard Description:
	 * getCheckInDateFromAddNightsCard Date: August/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */

	public void getCheckInDateFromAddNightsCard() {
		Assert.assertTrue(verifyObjectDisplayed(reservationDate), "Reservation Dates not present");
		checkinDate = getElementText(reservationDate).split("→")[0].trim();
		tcConfig.updateTestReporter("CUIModifyReservationPage", "getCheckInDateFromAddNightsCard", Status.PASS,
				"Check In date before modifying: " + checkinDate);

	}

	/*
	 * Method: validateOverlayTextWhenWholeFixedWeekIsBooked Description:
	 * validate Overlay Text When Whole Fixed Week Is Booked Date: August/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */

	public void validateOverlayTextWhenWholeFixedWeekIsBooked() {
		waitUntilElementVisibleBy(driver, fixedWeekBookedOverlay, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(fixedWeekBookedOverlay), "Fixed Week Overlay Message not present");
		tcConfig.updateTestReporter("CUIModifyReservationPage", "validateOverlayTextWhenWholeFixedWeekIsBooked",
				Status.PASS, "Fixed Week Message Overlay Displayed with correct text");
		clickElementJSWithWait(BackToModifyReservationButton);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

}
