package cui.pages;

import org.apache.log4j.Logger;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBucketListPage_IOS extends CUIBucketListPage_Web {

	public static final Logger log = Logger.getLogger(CUIBucketListPage_IOS.class);

	public CUIBucketListPage_IOS(TestConfig tcconfig) {
		super(tcconfig);

	}
	public String URL;
	/*
	 * Method: exploreCTANavigation Description:Validate Explore CTA Navigation
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void exploreCTANavigation() {
		URL=driver.getCurrentUrl();
		clickElementBy(exploreResortCTA);
		waitUntilElementVisibleBy(driver, locationField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(locationField));
		tcConfig.updateTestReporter("CUIBucketListPage", "exploreAllResortValdation", Status.PASS,
				"Navigated To Resorts Page");

	}

	/*
	 * Method: wishListedCardValidation Description: WishListed First Card Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListedCardValidation() {
		String strFirstCard = wishListFirstCard();
		driver.get(URL);
		//navigateBackAndWait(bucketListHeader);
		//sendKeyboardKeys(Keys.HOME);
		if (getList(resortNames).get(0).getText().contains(strFirstCard)) {
			tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.PASS,
					"The recently wishlisted card is displayed at first");
		} else {
			tcConfig.updateTestReporter("CUIBucketListPage", "wishListedCardValidation", Status.FAIL,
					"The recently wishlisted card is not displayed at first");
		}

	}

}
