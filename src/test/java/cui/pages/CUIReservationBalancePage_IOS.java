package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIReservationBalancePage_IOS extends CUIReservationBalancePage_Web {

	public static final Logger log = Logger.getLogger(CUIPointProtectionPage_IOS.class);

	public CUIReservationBalancePage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		bookingHeader = By.xpath("//div[@class='booking-header__mobile']//p");
	}

	public By pageHeader=By.xpath("//div[contains(@class,'booking')]//div[contains(@class,'title')]");
	/*
	 * Method: validateReservationBalancePage Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Monideep
	 * Changes By: Kamalesh
	 */
	public void validateReservationBalancePage() {
		waitUntilElementVisibleBy(driver, reservationBalance, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(reservationBalance), "Reservation Balance Page not visible");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "validateReservationBalancePage", Status.PASS,
				"Navigated to Reservation Balance page successfully");

		String[] headerValue = getElementText(bookingHeader).split(" ");
		String pageheaderValue = getElementText(pageHeader);
		
		Assert.assertTrue(pageheaderValue.contains(headerValue[1]),
				"Progress bar does not contains - RESERVATION BALANCE");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "checkProgressBarforReservationBalance",
				Status.PASS, "Progress bar contains - RESERVATION BALANCE");
	}
	
	/*
	 * Method: selectAndUpdateBorrowPoints selectAndUpdateBorrowPoints Date
	 * field Date: June/2020 Author: Monideep Changes By: Kamalesh
	 */
	public void selectAndUpdateBorrowPoints() {
		getElementInView(borrowCheckBox_ReservationPoints);
		clickElementBy(borrowCheckBox_ReservationPoints);
		waitUntilElementVisibleBy(driver, pointToBeBorrowedTextBx, "ExplicitLowWait");
		tcConfig.getTestData().put("BorrowedPoints", amountNeededPts);
		int availablePoints = Integer.parseInt(getElementText(useYearPoints).replace(",", ""));
		tcConfig.getTestData().put("AccountPoints", availablePoints + "");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Points to be borrowed", amountNeededPts, 1);
		sendKeysToElement(pointToBeBorrowedTextBx, Keys.TAB);
		clickElementBy(applyButtonPointToBeBorrowed);
		waitUntilElementVisibleBy(driver, removeButtonPointsToBorrow, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(removeButtonPointsToBorrow),
				"Borrowed points not applied successfully");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectAndUpdateBorrowPoints", Status.PASS,
				"Borrowed points applied successfully");

	}
	

	/*
	 * Method: selectAndUpdateRentPoints Description:Validate Borrow Rent Point
	 * is Not displayed Date field Date: June/2020 Author: Monideep Changes By:
	 * KG
	 */
	public void selectAndUpdateRentPoints() {
		getElementInView(rentPointsChkBox);
		clickElementBy(rentPointsChkBox);
		waitUntilElementVisibleBy(driver, pointsRentInput, "ExplicitLowWait");
		tcConfig.getTestData().put("RentedPoints", amountNeededPts);
		int availablePoints = Integer.parseInt(getElementText(useYearPoints).replace(",", ""));
		tcConfig.getTestData().put("AccountPoints", availablePoints + "");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Points to be rented", amountNeededPts, 1);
		sendKeysToElement(pointsRentInput, Keys.TAB);
		clickElementBy(applyPointsBtn);
		waitUntilElementVisibleBy(driver, pointsAppliedBtn, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(pointsAppliedBtn));
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectAndUpdateRentPoints", Status.PASS,
				"Rented points applied successfully");
		tcConfig.getTestData().put("RentedCost", getElementText(rentPointsTotalCost).trim());
	}

	
	/*
	 * Method: selectAndUpdateHouseKeepingBorrowPoints Description:
	 * selectAndUpdateHouseKeepingBorrowPoints Date field Date: June/2020
	 * Author: Monideep Changes By: KG
	 */
	public void selectAndUpdateHouseKeepingBorrowPoints() {
		getElementInView(borrowCheckBox_HousekeepingCredit);
		clickElementBy(borrowCheckBox_HousekeepingCredit);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Credits to be borrowed", tcConfig.getTestData().get("HouseKeepingBorrowPoints"), 1);
		
		sendKeysToElement(creditToBeBorrowedTextBx, Keys.TAB);
		clickElementBy(applyButtonCreditToBeBorrowed);
		waitUntilElementVisibleBy(driver, removeButtonCreditsToBorrow, "ExplicitLowWait");
		Assert.assertTrue(verifyObjectDisplayed(removeButtonCreditsToBorrow),
				"House keeping Borrowed points not applied successfully");
		tcConfig.updateTestReporter("CUIReservationBalancePage", "selectAndUpdateHouseKeepingBorrowPoints",
				Status.PASS, "House keeping Borrowed points applied successfully, applied points : "
						+ tcConfig.getTestData().get("HouseKeepingBorrowPoints"));

	}
	
	/*
	 * Method: applyForMorePointThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void borrowValidPoint() {
		String strAmountNeeded = getElementText(amountNeededPoint_ReservationPoint);
		String strAmountNeededUpdate = strAmountNeeded.replace(",", "");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Points to be borrowed", strAmountNeededUpdate, 1);
		clickElementBy(applyButtonPointToBeBorrowed);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.PASS,
				"Applied for Borrowing the Point");
		getElementInView(borrowingPoint_ReservationPoint);
		if (getElementText(borrowingPoint_ReservationPoint).equalsIgnoreCase(strAmountNeeded)
				&& getElementText(amountNeededPoint_ReservationPoint).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.PASS,
					"Amount borrowed and Borrowing Value and Amount Needed value updated successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.FAIL,
					"Error while applying for borrow point");
		}
		pointBorrowed = getElementText(borrowingPoint_ReservationPoint).trim();

	}
	
	/*
	 * Method: applyForMorePointThanAvailable Description:Validate Borrow Rent
	 * Point is Not displayed Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void rentValidPoint() {
		String strAmountNeeded = getElementText(amountNeededPoint_ReservationPoint);
		String strAmountNeededUpdate = strAmountNeeded.replace(",", "");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Points to be rented", strAmountNeededUpdate, 1);
		clickElementBy(applyButtonPointToBeRented);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "borrowValidPoint", Status.PASS,
				"Applied for renting the Point");
		getElementInView(rentingPoint_ReservationPoint);
		if (getElementText(rentingPoint_ReservationPoint).equalsIgnoreCase(strAmountNeeded)
				&& getElementText(amountNeededPoint_ReservationPoint).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "rentValidPoint", Status.PASS,
					"Amount rented and Renting Value and Amount Needed value updated successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "rentValidPoint", Status.FAIL,
					"Error while applying for rent point");
		}
		pointRented = getElementText(rentingPoint_ReservationPoint).trim();
		pointRentedPrice = getElementText(totalRentedPrice).trim();
	}
	
	/*
	 * Method: setRentBorrowPoint Description: setRentBorrowPoint
	 * Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void setRentBorrowPoint() {
		
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		
		selectCheckBox_ReservationBalanace("BorrowPoint");		
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Points to be borrowed", testData.get("PointToBeBorrow"), 1);
		clickElementBy(applyButtonPointToBeBorrowed);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.PASS,
				"Applied for Borrowing the Point");

		selectCheckBox_ReservationBalanace("RentPoint");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Points to be rented",  testData.get("PointToBeRented"), 1);
		clickElementBy(applyButtonPointToBeRented);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.PASS,
				"Applied for Renting the Point");
		getElementInView(rentingPoint_ReservationPoint);
		if (getElementText(amountNeededPoint_ReservationPoint).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.PASS,
					"Point Rented and Borrowed successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowPoint", Status.FAIL,
					"Error while applying for rent/borrow point");
		}

	}
	
	/*
	 * Method: setRentBorrowHK Description: setRentBorrowHK
	 * Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void setRentBorrowHK() {
		waitUntilElementVisibleBy(driver, headerReservationBalance, "ExplicitLongWait");
		
		selectCheckBox_ReservationBalanace("BorrowCredit");		
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Credits to be borrowed", testData.get("HKToBeBorrow"), 1);
		clickElementBy(applyButtonCreditToBeBorrowed);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.PASS,
				"Applied for Borrowing the Housekeeping Credits");
		
		selectCheckBox_ReservationBalanace("PurchaseCredit");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Credits to be purchased", testData.get("HKToBePurchase"), 1);
		clickElementBy(applyButtonCreditToBePurchase);
		tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.PASS,
				"Applied for purchasing the Credit");
		
		getElementInView(rentingPoint_ReservationPoint);
		if (getElementText(amountNeededPoint_HouseKeepingCredit).equalsIgnoreCase("0")) {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.PASS,
					"Housekeeping Credit Purchased and Borrowed successfully");
		} else {
			tcConfig.updateTestReporter("CUIReservationBalancePage", "setRentBorrowHK", Status.FAIL,
					"Error while applying for purchasing/borrow credit");
		}

	}
}
