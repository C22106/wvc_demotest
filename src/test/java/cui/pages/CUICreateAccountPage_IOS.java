package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICreateAccountPage_IOS extends CUICreateAccountPage_Web {

	public static final Logger log = Logger.getLogger(CUICreateAccountPage_IOS.class);

	public CUICreateAccountPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		
		selectCountryFlag = By.xpath("//div[@class ='flag-select']/a");
	}
	
	
	/*
	 * Method: enterMemberNumber Description:Enter Member Number Date: Mar/2020
	 * Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */

	public void enterMemberNumber() {
		String memberNumber = testData.get("MemberNumber").trim(); // Member
																	// Number
																	// Data
		getElementInView(memberNumberField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Member Number", memberNumber);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterMemberNumber", Status.PASS,
				"Successfully entered member number and the member number is: " + memberNumber);
	}

	/*
	 * Method: enterFirstName Description:Enter First Name Date: Mar/2020
	 * Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */

	public void enterFirstName() {
		String firstName = testData.get("FirstName").trim(); // First name Data
		getElementInView(firstNameField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "First Name*", firstName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterFirstName", Status.PASS,
				"Successfully entered first name, which is: " + firstName);
	}

	/*
	 * Method: enterLastName Description:Enter Last Name Date: Mar/2020 Author:
	 * Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void enterLastName() {
		String lastName = testData.get("LastName").trim(); // Last name Data
		getElementInView(lastNameField);
		clickElementBy(lastNameField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Last Name*", lastName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterLastName", Status.PASS,
				"Successfully entered last name, which is: " + lastName);
	}
	
	/*
	 * Method: enterCompanyFieldName Description:Enter Company Field Name Date:
	 * Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void enterCompanyFieldName() {
		String companyName = testData.get("CompanyName").trim(); // Dompany name
																	// data
		getElementInView(companyField);
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Company or Trust Name", companyName);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterCompanyFieldName", Status.PASS,
				"Successfully entered Company Trust name which is:  " + companyName);
	}
	
	/*
	 * Method: enterPhoneNumber Description:Enter Phone Number Date: Mar/2020
	 * Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void enterPhoneNumber(String phoneNumber) {
		getElementInView(phoneNumberField);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Phone Number", phoneNumber,2);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterPhoneNumber", Status.PASS,
				"Successfully entered Phone Number and the Phone number is: " + phoneNumber);
		getObject(phoneNumberRadio).click();
	}

	/*
	 * Method: enterContractNumber Description:Enter Contract Number Date:
	 * Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void enterContractNumber(String contractNumber) {

		sendKeysByScriptExecutor((RemoteWebDriver)driver, "Contract Number", contractNumber,2);
		sendKeyboardKeys(Keys.TAB);
		tcConfig.updateTestReporter("CUICreateAccountPage", "enterContractNumber", Status.PASS,
				"Successfully entered Contract number and the Contract number is: " + contractNumber);
	}
	
	/*
	 * Method: selectVerificationMethod Description:Select a verification
	 * methods Date: Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void selectVerificationMethod() {
		// to slelect verification method
		String selectCreateAccountMethod = testData.get("Createaccmethod").trim();
		// Phone Number Data
		String phoneNumber = testData.get("PhoneNumber").trim();
		// Country Select
		//String countrySelect = testData.get("Country").trim();
		// Contract Number Data
		String contractNumber = testData.get("ContractNumber").trim();
		if (selectCreateAccountMethod.equals("Phone")) {

			getElementInView(phoneNumberRadio);
			getObject(phoneNumberRadio).click();
			if (verifyObjectDisplayed(selectCountryFlag)) {
				//selectCountryFlag(countrySelect);
				
				enterPhoneNumber(phoneNumber);
				
				
			} else {
				tcConfig.updateTestReporter("CUICreateAccountPage", "selectVerificationMethod", Status.FAIL,
						"Flag select field not present please check screenshot");
			}
		} else if (selectCreateAccountMethod.equals("Contract")) {
			getElementInView(contractNumberRadio);
			getObject(contractNumberRadio).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			enterContractNumber(contractNumber);
		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "selectVerificationMethod", Status.FAIL,
					"Please select method of verification from excel where the column name is Createaccmethod");
		}

	}
	
	
	
	
	/*
	 * 
	 * /* Method: companyTrustNavigation Description: Navigate to Company Trust
	 * page Val Date: mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void companyTrustNavigation() {
		if (verifyObjectDisplayed(registerCTA)) {
			getElementInView(registerCTA);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementByScriptExecutor("Here", 1);
			
			waitUntilElementVisibleBy(driver, companyField, "ExplicitLongWait");

			if (verifyObjectDisplayed(companyField)) {
				tcConfig.updateTestReporter("CUICreateAccountPage", "companyTrustNavigation", Status.PASS,
						"Company Trust Register fields displayed");

			} else {
				tcConfig.updateTestReporter("CUICreateAccountPage", "companyTrustNavigation", Status.FAIL,
						"Company Trust Register Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "companyTrustNavigation", Status.FAIL,
					"Company Trust Register Link not present");
		}

	}
	
	
	
	/*
	 * Method: phoneNumberFieldValidations Description:Account Phone Number
	 * Field validations Date: Mar/2020 Author: Unnat Jain Changes By: Kamalesh Gupta (IOS)
	 */
	public void phoneNumberFieldValidations() {
		// Country Data
		String countrySelect = testData.get("Country").trim();
		// phone Number data
		String phoneNumber = testData.get("invalidData").trim();
		// More Than 10 Digits Number
		String incorrectPhoneNumber = phoneNumber + phoneNumber;
		// invalidPhoneNumber
		String invalidNumber = testData.get("invalidData");

		getElementInView(phoneNumberRadio);
		clickElementBy(phoneNumberRadio);

		waitUntilElementVisibleBy(driver, selectCountryFlag, "ExplicitLongWait");
		if (verifyObjectDisplayed(selectCountryFlag)) {
			if (verifyObjectDisplayed(defaultFlag)) {
				tcConfig.updateTestReporter("CUICreateAccountPage", "phoneNumberFieldValidations", Status.PASS,
						"Default Flag is Selected of United States with Country Code: " + getElementText(countryCode));
			} else {
				tcConfig.updateTestReporter("CUICreateAccountPage", "phoneNumberFieldValidations", Status.FAIL,
						"Default Flag is not Selected of United States");
			}
			selectCountryFlag(countrySelect);
			enterPhoneNumber(incorrectPhoneNumber);
			clickElementBy(lastNameField);
			checkPhoneFieldError();
			enterPhoneNumber(invalidNumber);

		} else {
			tcConfig.updateTestReporter("CUICreateAccountPage", "selectVeriMethod", Status.FAIL,
					"Flag select field not present please check screenshot");
		}
	}
}
