package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIForgotUserNamePage_Web extends CUIBasePage {

	public static final Logger log = Logger.getLogger(CUIForgotUserNamePage_Web.class);

	public CUIForgotUserNamePage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By forgotUsernameHeader = By.xpath("//h1[contains(.,'FORGOT YOUR USERNAME')]");
	protected By usernameDescription = By.xpath("//h1[contains(.,'FORGOT YOUR USERNAME')]/following-sibling::div/p");
	protected By continueButton = By.xpath("//button[text() = 'Continue']");
	protected By forgotUsernameLink = By.xpath("//a[text() = 'Forgot Username']");

	/*
	 * Method: forgotUsernameNavigation Description: Navigate to Forgot Username
	 * Page Val Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void forgotUsernameNavigation() {
		getElementInView(forgotUsernameLink);
		clickElementBy(forgotUsernameLink);
		waitUntilElementVisibleBy(driver, forgotUsernameHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(forgotUsernameHeader), "Forgot Username Header not present");
		elementPresenceVal(forgotUsernameHeader, "Forgot Username Header", "CUIForgotUsernamePage",
				"forgotUsernameNavigation");
		elementPresenceVal(usernameDescription, "Forgot Username Description", "CUIForgotUsernamePage",
				"forgotUsernameNavigation");

	}

	/*
	 * Method: clickContinue Description:Click Continue CTA Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void clickContinue() {

		if (verifyObjectDisplayed(continueButton)) {
			getElementInView(continueButton);
			clickElementBy(continueButton);

		} else {
			tcConfig.updateTestReporter("CUIForgotUsernamePage", "clickContinue", Status.FAIL,
					"Error in clicking Continue CTA");
		}

	}

}