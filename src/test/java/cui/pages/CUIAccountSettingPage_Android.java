package cui.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIAccountSettingPage_Android extends CUIAccountSettingPage_Web {

	public static final Logger log = Logger.getLogger(CUIAccountSettingPage_Android.class);

	protected By hamburgerMenu = By.xpath("//button[@class='hamburger-menu']");
	protected By logOutCTA = By.xpath("//a[@class='nav-logout-button' and span[text()='Log Out']]");
	protected By usernameText = By.xpath("//input[@id='username']//following-sibling::span[contains(.,'Username*')]");

	public CUIAccountSettingPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		personalInformationHeader = By.xpath("//h2[contains(.,'PERSONAL INFOR')]");
		editPersonalInformation = By.xpath("//div[@class='personal-info']//button[contains(.,'Edit')]");
		manageUsernameHeader = By.xpath("//h2[contains(.,'MANAGE USERNAME')]");
		editUsernameLink = By.xpath("//div[@class='manage-username']//button[contains(.,'Edit')]");
		securityQuestionHeader = By.xpath("//h2[contains(.,'SECURITY QUES')]");
		editSecurityLink = By.xpath("//div[@class='manage-security-questions']//button[contains(.,'Edit')]");
		managePasswordHeader = By.xpath("//h2[contains(.,'MANAGE PASSWORD')]");
		editPasswordLink = By.xpath("//div[@class='manage-password']//button[contains(.,'Edit')]");
		/* Profile Photo */
		profilePhotoSpace = By.xpath("//div[@class='manage-profile-photo']");
		noImageInitials = By.xpath(
				"//div[@class='personal-info']/parent::div/following-sibling::div//div[@class='manage-profile-photo']//h6");
		imageUploaded = By.xpath("//div[@class='manage-profile-photo']//img");
		uploadPhotoCTA = By.xpath("//div[@class='manage-profile-photo']//span");

		accountProfileLink = By.xpath("//*[@class='global-account__links']//a[span[text()='Account Profile']]");

	}

	/*
	 * Method: logOutApplication Description: Logout from application clicking
	 * the Hamburger menu Feb/2020 Author: Unnat Jain Changes By: Monideep
	 * Roychowdhury(Android)
	 */
	public void logOutApplication() {
		// to be removed once log out is fixed
		clickElementBy(hamburgerMenu);
		waitUntilElementVisibleBy(driver, logOutCTA, "ExplicitLongWait");
		getElementInView(logOutCTA);
		clickElementBy(logOutCTA);
		waitUntilElementVisibleBy(driver, usernameText, "ExplicitLongWait");

	}

	/*
	 * Method: acountSettingsNavigation Description: Navigate to Account Setting
	 * Page Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void accountSettingsNavigation() {

		clickElementBy(hamburgerMenu);

		waitUntilElementVisibleBy(driver, accountProfileLink, "ExplicitLongWait");
		clickElementBy(accountProfileLink);

		waitUntilElementVisibleBy(driver, accountSettingsHeader, "ExplicitLongWait");

		if (verifyObjectDisplayed(accountSettingsHeader)) {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "accountSettingsNavigation", Status.PASS,
					"Account Settings page navigated and header is present");
		} else {
			tcConfig.updateTestReporter("CUIAccountProfilePage", "accountSettingsNavigation", Status.FAIL,
					"Account Settings page navigation failed or the header is not present");
		}

	}
}
