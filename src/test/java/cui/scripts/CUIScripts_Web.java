package cui.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import cui.pages.CUIAccountCreatedPage_Web;
import cui.pages.CUIAccountSettingPage_Web;
import cui.pages.CUIBookConfirmationPage_Web;
import cui.pages.CUIBookPage_Web;
import cui.pages.CUIBookingUpgradePage_Web;
import cui.pages.CUIBucketListPage_Web;
import cui.pages.CUICancelReservation_Web;
import cui.pages.CUICompleteDepositPage_Web;
import cui.pages.CUICompleteModificationPage_Web;
import cui.pages.CUICompletePaymentPage_Web;
import cui.pages.CUICreateAccountDetailsPage_Web;
import cui.pages.CUICreateAccountPage_Web;
import cui.pages.CUIDepositSuccessPage_Web;
import cui.pages.CUIForgotPasswordPage_Web;
import cui.pages.CUIForgotUserNamePage_Web;
import cui.pages.CUILoanAssessmentSuccessPage_Web;
import cui.pages.CUILoginPage_Web;
import cui.pages.CUIManagePasswordPopUp_Web;
import cui.pages.CUIManageUsernamePopUp_Web;
import cui.pages.CUIModifyNightsPage_Web;
import cui.pages.CUIModifyReservationPage_Web;
import cui.pages.CUIModifySuccessPage_Web;
import cui.pages.CUIModifyTravelerPage_Web;
import cui.pages.CUIMyAccountPage_Web;
import cui.pages.CUIMyOwnershipPage_Web;
import cui.pages.CUIMyOwnershipPayPalPage_Web;
import cui.pages.CUINewPasswordPage_Web;
import cui.pages.CUIPasswordEmailMethodPage_Web;
import cui.pages.CUIPasswordResetMethodPage_Web;
import cui.pages.CUIPaymentAmountPage_Web;
import cui.pages.CUIPersonalInformationPopUp_Web;
import cui.pages.CUIPointDepositPage_Web;
import cui.pages.CUIPointProtectionPage_Web;
import cui.pages.CUIPointSummaryPage_Web;
import cui.pages.CUIReservationBalancePage_Web;
import cui.pages.CUIReservationDetailsPage_Web;
import cui.pages.CUISearchPage_Web;
import cui.pages.CUISecurityQuestionPopUp_Web;
import cui.pages.CUITravellerInformationPage_Web;
import cui.pages.CUIUpcomingVacationPage_Web;
import cui.pages.CUIUsernameEmailMethodPage_Web;
import cui.pages.CUIUsernameResetMethodPage_Web;
import cui.pages.CUI_RCIAndDepositPage_Web;
import cui.pages.SalepointPage_Web;
import cui.pages.TripPage_Web;

public class CUIScripts_Web extends TestBase {
	public static final Logger log = Logger.getLogger(CUIScripts_Web.class);
	public static String strTestName;

	/*
	 * Method: tc_01_CUISmoke_LoginPageDashboardValidations Description: CUI
	 * Login page Validations Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "smoke", "batch1" })

	public void tc_01_CUISmoke_LoginPageDashboardValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.globalHeaderPresence();
			loginPage.globalFooterPresence();
			loginPage.loginModalHeaders();
			loginPage.usernameFieldValidation();
			loginPage.passwordFieldValidation();
			loginPage.newAccountSection();
			loginPage.notAnOwnerSection();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.dashboardAccountInfoSection();
			myaccountPage.dashboardMyOwnerandPointsHeader();
			myaccountPage.validateSearchHeaderSmoke();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_02_CUISmoke_AccountSettingsPageval Description: CUI Account
	 * settings page Validations Date: Feb/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "smoke", "batch1" })

	public void tc_02_CUISmoke_AccountSettingsPageval(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.personalInformationSection();
			accountSettingPage.personalInformationNameSectionSmoke();
			accountSettingPage.personalInformationEmailSectionSmoke();
			accountSettingPage.personalInformationAddressSectionSmoke();
			accountSettingPage.personalInformationPhoneSectionSmoke();
			accountSettingPage.ownerProfilePicture();
			accountSettingPage.ownerUsernameValidation();
			accountSettingPage.usernameVerification();
			accountSettingPage.ownerSecurityQuestionsValidationSmoke();
			accountSettingPage.ownerPasswordSection();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_03_CUISmoke_SearchResultValidationWithFilter Description:
	 * Search Validation with no flex date Validations Date: Apr/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "smoke", "batch1" })

	public void tc_03_CUISmoke_SearchResultValidationWithFilter(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateNumberOfResortCard();
			searchPage.validateMapPresent();
			searchPage.filterButtonClick();
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifySortInAZ();
			searchPage.checkWorldmarkResortPreseceAtLast();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_001_CUISanity_CreateReservationModifyCancel Description:
	 * Sanity Test for Login Search Book Modify and Cancel Date: Jun/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "sanity", "batch1" })

	public void tc_001_CUISanity_CreateReservationModifyCancel(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.clickModifyTravelerButton();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedOwner();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.verifyModifiedName();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_001_CUI_LoginPageValidations Description: CUI Login page
	 * Validations Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1"})

	public void tc_001_CUI_LoginPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.globalHeaderPresence();
			loginPage.globalFooterPresence();
			loginPage.loginModalHeaders();
			loginPage.usernameFieldValidation();
			loginPage.passwordFieldValidation();
			loginPage.newAccountSection();
			loginPage.notAnOwnerSection();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_002_CUI_LoginErrorValidations Description: CUI Login page
	 * Error Validations Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_002_CUI_LoginErrorValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.loginCTADisabled();
			loginPage.setUserName();
			loginPage.loginCTADisabled();
			loginPage.clearFieldRefresh();
			loginPage.setPassword();
			loginPage.loginCTADisabled();

			loginPage.invalidLoginDataError();
			loginPage.invalidUsername();
			loginPage.setPassword();
			loginPage.clickLoginCTAError();
			loginPage.errorMessageValidation("Username");

			loginPage.invalidLoginDataError();
			loginPage.setUserName();
			loginPage.invalidPassword();
			loginPage.clickLoginCTAError();
			loginPage.errorMessageValidation("Password");

			loginPage.invalidLoginDataError();
			loginPage.invalidUsername();
			loginPage.invalidPassword();
			loginPage.clickLoginCTAError();
			loginPage.errorMessageValidation("Username and Password");
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_003_CUI_IndividualCreateAccount Description: Create Account
	 * for Individual Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_003_CUI_IndividualCreateAccount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUICreateAccountDetailsPage_Web accountDetails = new CUICreateAccountDetailsPage_Web(tcconfig);
		CUIAccountCreatedPage_Web accountCreated = new CUIAccountCreatedPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			createAccount.createAccountNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				accountDetails.accountDetailsPageValidations();
			}
			accountDetails.accountDetailsDataEntry(this.toString());
			createAccount.clickContinue();

			accountCreated.accountCreatedPage();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_004_CUI_CompanyTrustCreateAccount Description: Create Account
	 * for Company/Trust Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_004_CUI_CompanyCreateAccount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUICreateAccountDetailsPage_Web accountDetails = new CUICreateAccountDetailsPage_Web(tcconfig);
		CUIAccountCreatedPage_Web accountCreated = new CUIAccountCreatedPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			createAccount.createAccountNavigation();
			createAccount.companyTrustNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				accountDetails.accountDetailsPageValidations();
			}
			accountDetails.accountDetailsDataEntry(this.toString());
			createAccount.clickContinue();

			accountCreated.accountCreatedPage();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_005_CUI_IndividualForgotUsername Description: Username
	 * retrieval for Individual Account Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_005_CUI_IndividualForgotUsername(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Web(tcconfig);
		CUIUsernameEmailMethodPage_Web emailMethod = new CUIUsernameEmailMethodPage_Web(tcconfig);
		CUIUsernameResetMethodPage_Web resetmethod = new CUIUsernameResetMethodPage_Web(tcconfig);
		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			forgotUsername.forgotUsernameNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				resetmethod.verificationPageValidation();
			}
			resetmethod.verificationViaMethod();
			createAccount.clickContinue();
			loginPage.acceptCookie();
			if (methodSelect.contains("EMAIL")) {
				emailMethod.emailPageValidations();
				emailMethod.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				loginPage.preEnteredUsernameValidation(this.toString());
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplicationViaDashboard();
			}
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_006_CUI_CompanyForgotUsername Description: Username retrieval
	 * for Company/Trust Account Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_006_CUI_CompanyForgotUsername(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Web(tcconfig);
		CUIUsernameEmailMethodPage_Web emailMethod = new CUIUsernameEmailMethodPage_Web(tcconfig);
		CUIUsernameResetMethodPage_Web resetmethod = new CUIUsernameResetMethodPage_Web(tcconfig);
		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			forgotUsername.forgotUsernameNavigation();
			createAccount.companyTrustNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();
			loginPage.acceptCookie();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				resetmethod.verificationPageValidation();
			}
			resetmethod.verificationViaMethod();
			createAccount.clickContinue();
			if (methodSelect.contains("EMAIL")) {
				emailMethod.emailPageValidations();
				emailMethod.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				loginPage.preEnteredUsernameValidation(this.toString());
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplicationViaDashboard();
			}

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_007_CUI_UsernameSecurityQuestionError Description: Security
	 * Question Error Val in the Username Retrieval flow Account Date: Mar/2020
	 * Author: Unnat Jain Changes By:NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_007_CUI_UsernameSecurityQuestionError(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Web(tcconfig);
		CUIUsernameResetMethodPage_Web resetmethod = new CUIUsernameResetMethodPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotUsername.forgotUsernameNavigation();

			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			resetmethod.selectSecurityMethod();
			resetmethod.securityQuestionErrorValdation();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_008_CUI_PhoneNumberFieldValidation Description: Phone Number
	 * Field Val in the Username Retrieval flow Account Date: Mar/2020 Author:
	 * Unnat Jain Changes By:NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_008_CUI_PhoneNumberFieldValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotUsername.forgotUsernameNavigation();

			createAccount.createAccountDataEnter();
			createAccount.phoneNumberFieldValidations();

			createAccount.clickContinue();
			createAccount.checkIncorrectError();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_009_CUI_IndividualForgotPassword Description: CUI Forgot
	 * Password page Validations Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_009_CUI_IndividualForgotPassword(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUIForgotPasswordPage_Web forgotPassword = new CUIForgotPasswordPage_Web(tcconfig);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUsernameEmailMethodPage_Web usernameEmail = new CUIUsernameEmailMethodPage_Web(tcconfig);
		CUIUsernameResetMethodPage_Web usernameReset = new CUIUsernameResetMethodPage_Web(tcconfig);
		CUINewPasswordPage_Web newPassword = new CUINewPasswordPage_Web(tcconfig);
		CUIPasswordEmailMethodPage_Web passwordEmail = new CUIPasswordEmailMethodPage_Web(tcconfig);
		CUIPasswordResetMethodPage_Web passwordReset = new CUIPasswordResetMethodPage_Web(tcconfig);

		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotPassword.forgotPasswordNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				forgotPassword.passwordDetailsValidations();
				forgotPassword.usernameLinkCheck();
			}
			forgotPassword.enterUsername();
			forgotPassword.enterNameDetails();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				passwordReset.verificationPageValidation();
			}
			usernameReset.verificationViaMethod();
			createAccount.clickContinue();
			loginPage.acceptCookie();
			if (methodSelect.contains("EMAIL")) {
				passwordEmail.emailPageValidation();
				usernameEmail.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.newPasswordPageValidation();
				}
				newPassword.newPasswordDataEnter();
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.showPasswordCTA();
				}
				createAccount.clickContinue();
				newPassword.passwordResetPage();
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplicationViaDashboard();
			}
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_010_CUI_CompanyTrustPassword Description: CUI Forgot Password
	 * page Validations Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_010_CUI_CompanyTrustPassword(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUIForgotPasswordPage_Web forgotPassword = new CUIForgotPasswordPage_Web(tcconfig);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUsernameEmailMethodPage_Web usernameEmail = new CUIUsernameEmailMethodPage_Web(tcconfig);
		CUIUsernameResetMethodPage_Web usernameReset = new CUIUsernameResetMethodPage_Web(tcconfig);
		CUINewPasswordPage_Web newPassword = new CUINewPasswordPage_Web(tcconfig);
		CUIPasswordEmailMethodPage_Web passwordEmail = new CUIPasswordEmailMethodPage_Web(tcconfig);
		CUIPasswordResetMethodPage_Web passwordReset = new CUIPasswordResetMethodPage_Web(tcconfig);
		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotPassword.forgotPasswordNavigation();
			createAccount.companyTrustNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				forgotPassword.passwordDetailsValidations();

			}
			forgotPassword.enterUsername();
			forgotPassword.enterNameDetails();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				passwordReset.verificationPageValidation();
			}
			usernameReset.verificationViaMethod();
			createAccount.clickContinue();

			if (methodSelect.contains("EMAIL")) {
				passwordEmail.emailPageValidation();
				usernameEmail.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.newPasswordPageValidation();
				}
				newPassword.newPasswordDataEnter();
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.showPasswordCTA();
				}
				createAccount.clickContinue();
				newPassword.passwordResetPage();
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplicationViaDashboard();
			}
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_011_CUI_PasswordSecurityQuestionError Description: Security
	 * Question Error Val in the Password Retrieval flow Account Date: Mar/2020
	 * Author: Unnat Jain Changes By:NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_011_CUI_PasswordSecurityQuestionError(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Web(tcconfig);
		CUIForgotPasswordPage_Web forgotPassword = new CUIForgotPasswordPage_Web(tcconfig);
		CUIUsernameResetMethodPage_Web resetMethod = new CUIUsernameResetMethodPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotPassword.forgotPasswordNavigation();
			forgotPassword.enterUsername();
			forgotPassword.enterNameDetails();

			createAccount.clickContinue();

			resetMethod.selectSecurityMethod();
			resetMethod.securityQuestionErrorValdation();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_012_CUI_MultipleLoginError Description: multiple login error
	 * validation Date: Mar/2020 Author: Unnat Jain Changes By:NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_012_CUI_MultipleLoginError(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			loginPage.multipleInvalidLogin();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.multilpeLoginClick();
			loginPage.verifyAccountLocked();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_013_CUI_AccountSettingsPageValidation Description: CUI Account
	 * settings page Validations Date: Feb/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_013_CUI_AccountSettingsPageValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();

			accountSettingPage.personalInformationSection();
			accountSettingPage.personalInformationNameSection();
			accountSettingPage.personalInformationEmailSection();
			accountSettingPage.personalInformationAddressSection();
			accountSettingPage.personalInformationPhoneSection();

			accountSettingPage.ownerProfilePicture();
			accountSettingPage.imageUploadedvalidation();

			accountSettingPage.ownerUsernameValidation();
			accountSettingPage.usernameVerification();

			accountSettingPage.ownerSecurityQuestionsValidation();
			accountSettingPage.securityQuestion1Validation();
			accountSettingPage.securityQuestion2Validation();
			accountSettingPage.securityQuestion3Validation();

			accountSettingPage.ownerPasswordSection();
			accountSettingPage.passwordVerification("CUI_password", "Yes");
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_014_CUI_UsernamePopUpValidation Description: CUI Account
	 * settings page username pop up Validations Date: Feb/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_014_CUI_UsernamePopUpValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			manageUsername.usernamePopUpModalNavigation();
			manageUsername.usernameModalValidation();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_015_CUI_PasswordPopUpValidation Description: CUI Account
	 * settings page Password pop up Validations Date:Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_015_CUI_PasswordPopUpValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			managePassword.passwordPopUpModalNavigation();
			managePassword.passwordModalValidation();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_016_CUI_SecurityQuestionPopUpValidation Description: CUI
	 * Account settings page Sec Ques pop up Validations Date:Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_016_CUI_SecurityQuestionPopUpValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.securityQuestionModalValidation();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_017_CUI_PersonalInformationPopUpValidation Description: CUI
	 * Account settings page Sec Ques pop up Validations Date:Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_017_CUI_PersonalInformationPopUpValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUIPersonalInformationPopUp_Web personaleInformation = new CUIPersonalInformationPopUp_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			accountSettingPage.accountSettingsNavigation();
			personaleInformation.personalInformationModalNavigation();
			personaleInformation.personalInformationModalValidation();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_018_CUI_AccountInformationCancelCTAValidation Description: CUI
	 * Account settings page Sec Ques pop up Validations Date:Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_018_CUI_AccountInformationCancelCTAValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUIPersonalInformationPopUp_Web personalInformation = new CUIPersonalInformationPopUp_Web(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Web(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Web(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			personalInformation.personalInformationModalNavigation();
			personalInformation.editEmailField();
			personalInformation.personalInformationCancelCTA();
			manageUsername.usernamePopUpModalNavigation();
			manageUsername.editOldUsernameField();
			manageUsername.usernameCancelCTA();
			managePassword.passwordPopUpModalNavigation();
			managePassword.enterNewPassword();
			managePassword.passwordCancelCTA();
			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.editAnswer1();
			securityQuestion.securityCancelCTA();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_019_CUI_ProfilePopUpErrorValidations Description: CUI Account
	 * settings page pop up error validation Date:Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_019_CUI_ProfilePopUpErrorValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUIPersonalInformationPopUp_Web personaleInformation = new CUIPersonalInformationPopUp_Web(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Web(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Web(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();

			personaleInformation.personalInformationModalNavigation();
			personaleInformation.invalidAddress1Field();
			personaleInformation.invalidAddressError();
			personaleInformation.invalidEmailField();
			personaleInformation.invalidEmailError();
			personaleInformation.personalInformationCancelCTA();

			manageUsername.usernamePopUpModalNavigation();
			manageUsername.takenUsernameData();
			manageUsername.takenUsernameError();
			manageUsername.availableUsername();
			manageUsername.usernameCancelCTA();

			managePassword.passwordPopUpModalNavigation();
			managePassword.invalidNewPassword();
			managePassword.newPasswordFieldError();
			managePassword.incorrectDataCurrent();
			managePassword.enterNewPassword();
			managePassword.saveButtonClick();
			managePassword.incorrectCurrentPassword();
			managePassword.passwordCancelCTA();

			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.checkSelectQuestion1();
			securityQuestion.enterAnswer1();
			securityQuestion.emptyAnswer1Error();
			securityQuestion.checkSelectQuestion2();
			securityQuestion.enterAnswer2();
			securityQuestion.emptyAnswer2Error();
			securityQuestion.checkSelectQuestion3();
			securityQuestion.enterAnswer3();
			securityQuestion.emptyAnswer3Error();
			securityQuestion.securityCancelCTA();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_020_CUI_AccountInformationChanges Description: CUI Account
	 * settings page changes validation Date:Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_020_CUI_AccountInformationChanges(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUIPersonalInformationPopUp_Web personaleInformation = new CUIPersonalInformationPopUp_Web(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Web(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Web(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();

			personaleInformation.personalInformationModalNavigation();
			personaleInformation.editEmailField();
			personaleInformation.saveAccountInformationChanges();

			manageUsername.usernamePopUpModalNavigation();
			manageUsername.editNewUsernameField();
			manageUsername.saveUsernameChanges();

			managePassword.passwordPopUpModalNavigation();
			managePassword.editNewPasswordField();
			managePassword.savePasswordChanges();

			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.editSecurityQuestion1Field();
			securityQuestion.saveSecurityChanges();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_021_CUI_SearchFilterValidation_ZA_Experience_Express
	 * Description: CUI Serach Filter Validation with ZA sorting Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_021_CUI_SearchFilterValidation_ZA_Experience_Express(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				myaccountPage.validateTextLocationField();
				myaccountPage.validateTextDateField();
			}
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.selectExperience();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifysortInZAOnlyClubWyndhamResort();
			searchPage.checkExperienceSelectedPresent();
			searchPage.filterButtonClick();
			searchPage.resetFilter();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			searchPage.worldmarkAddress();
			searchPage.WorldMarkImage();
			searchPage.WorldMarkContact();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_022_CUI_SearchFilterValidationFlexDates Description: Flex and
	 * actual date validation Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_022_CUI_SearchFilterValidationFlexDates(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateNumberOfResortCard();
			searchPage.validateMapPresent();
			searchPage.checkFlexDateDisplayed();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_023_CUI_SearchValidationNoFlexDates Description: Search
	 * Validation with no flex date Validation Date: Mar/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_023_CUI_SearchValidationNoFlexDates(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.validateNumberOfResortCard();
			searchPage.validateMapPresent();
			searchPage.checkFlexDateNotDisplayed();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * /* Method: tc_024_CUI_SearchValidationWorldMarkResortAZDifferentCondition
	 * Description: Search Validation and sorting AZ for worldmark Validations
	 * Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_024_CUI_SearchValidationWorldMarkResortAZDifferentCondition(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifySortInAZ();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_025_CUI_SearchValidationWorldMarkResortZADifferentCondition
	 * Description: Search Validation and sorting ZA Club Wyndham , AZ Worldmark
	 * Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1" })

	public void tc_025_CUI_SearchValidationWorldMarkResortZADifferentCondition(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifysortInZAOnlyClubWyndham();
			searchPage.checkSortAZOnlyWorldmark();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * /* Method: tc_026_CUI_SearchFilterValidation Description: CUI Serach
	 * Filter Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_026_CUI_SearchFilterValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				myaccountPage.validateTextLocationField();
				myaccountPage.validateTextDateField();
			}
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.selectUnitType();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifySortInAZResort();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_027_CUI_SearchValidationSortByCollapseValidation Description:
	 * Validate Search Result and check sort by collapse area Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_027_CUI_SearchValidationSortByCollapseValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.checkSortByCollapse();
			searchPage.filterButtonClickAfterSelection();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_028_CUI_SearchValidationWorldMArkResortAZState_CWP
	 * Description: Silver Membership discount vlaidation Date: Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_028_CUI_MembershipDiscountValidation_Silver(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			if (testData.get("discountCheckin").equalsIgnoreCase("Resort")) {
				searchPage.validateSearchResultResort();
				searchPage.checkDiscountPriceResort();
			} else {
				searchPage.validateSearchResult();
				searchPage.checkDiscountMembershipPrice();
			}

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_029_CUI_MembershipDiscountValidation_Platinum
	 * Description:Validate Platinum Membership Discount Validation Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_029_CUI_MembershipDiscountValidation_Platinum(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			// searchPage.validateSearchResult();
			if (testData.get("discountCheckin").equalsIgnoreCase("Resort")) {
				searchPage.validateSearchResultResort();
				searchPage.checkDiscountPriceResort();
			} else {
				searchPage.validateSearchResult();
				searchPage.checkDiscountMembershipPrice();
			}

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_030_CUI_MembershipDiscountValidation_Gold Description:
	 * Validate Gold Member Discount Date: Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_030_CUI_MembershipDiscountValidation_Gold(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			if (testData.get("discountCheckin").equalsIgnoreCase("Resort")) {
				searchPage.validateSearchResultResort();
				searchPage.checkDiscountPriceResort();
			} else {
				searchPage.validateSearchResult();
				searchPage.checkDiscountMembershipPrice();
			}

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_031_CUI_changeLocationDateValidate Description: Validate
	 * changed location and date Date: Mar/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_031_CUI_changeLocationDateValidate(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.changeLocation();
			searchPage.changeCheckinDate();
			searchPage.changeCheckoutDate();
			searchPage.selectCalendarClose();
			searchPage.validateSearchResultResort();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_032_CUI_ExactResultClubPassUnitTypeNotAbleToBook
	 * Description:Club Pass Resort in Exact Search and not able to Book any
	 * unit type Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_032_CUI_ExactResultClubPassUnitTypeNotAbleToBook(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_033_CUI_SearchFilterNoAccessibleUnitValidation_Express
	 * Description: No Accessible Unit Type Present Validation Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_033_CUI_SearchFilterNoAccessibleUnitValidation_Express(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.filterButtonClick();
			searchPage.checkSortByCollapse();
			searchPage.checkUnitTypeCollapse();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkAccessibleUnitNotPresent();
			searchPage.filterButtonClick();
			searchPage.resetFilter();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_034_CUI_ResortNotEligibleForMemberValidaton Description:
	 * Validate User Not Eligible for Booking some Resort Message Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_034_CUI_ResortNotEligibleForMemberValidaton(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.resortNotAbleToBook();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_035_CUI_SeasonedOwnerValidations Description: To verify that
	 * the member logged in is a seasoned owner Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_035_CUI_SeasonedOwnerValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.seasonedOwnerHeader();
			myAccountPage.ownerWithPoints();
			myAccountPage.ownerWithPointsSearchForm();
			myAccountPage.validateTextLocationField();
			myAccountPage.validateTextDateField();
			myAccountPage.buttonSearchAvailabilityDisabled();
			myAccountPage.ownerWithReservations();
			myAccountPage.leftRailTextValidation();
			myAccountPage.vipStatusValidation();
			myAccountPage.membershipTextValidation();
			myAccountPage.educationTextValidation();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_036_CUI_PendingOwnerValidations Description: To verify that
	 * the member logged in is a Pending owner Date: Apr/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_036_CUI_PendingOwnerValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.newOwnerHeader();
			myAccountPage.ownerWithoutPoints();
			myAccountPage.pendingOwnerSearchForm();
			myAccountPage.ownerWithoutReservationsAndPoints();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_037_CUI_SeasonedOwnerReservationSection Description: To verify
	 * that Upcoming Reservation is displayed properly for seasoned owner Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_037_CUI_SeasonedOwnerReservationSection(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myAccountPage.ownerWithReservations();
			myAccountPage.upcomingReservationSection();
			myAccountPage.mapLinkValidations();
			myAccountPage.reservationLinkValidations();
			myAccountPage.modifyLinkValidations();
			myAccountPage.cancelLinkValidations();
			myAccountPage.reservationSortingAndPaginationCheck();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_038_CUI_dashboardPageSeasonedUser Description: To verify that
	 * Dashboard page is displayed properly for seasoned owner Date: Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_038_CUI_dashboardPageSeasonedUser(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myAccountPage.seasonedOwnerHeader();
			myAccountPage.userNameValidation();
			myAccountPage.imageValidations();
			myAccountPage.vipStatusValidation();
			myAccountPage.memberNumberValidation();
			myAccountPage.viewProfileValidations();
			myAccountPage.membershipTextValidation();
			myAccountPage.contractNumbersValidations();
			myAccountPage.myOwnershipLinkValidations();
			myAccountPage.pointsHeaderValidations();
			myAccountPage.ownerWithPoints();
			myAccountPage.ownerUseYearDates();
			myAccountPage.viewPointLinkValidations();
			myAccountPage.validateTextLocationField();
			myAccountPage.validateTextDateField();
			myAccountPage.buttonSearchAvailabilityDisabled();
			myAccountPage.ownerWithReservations();
			myAccountPage.travelDealsHeaderValidation();
			myAccountPage.travelDealsCardsValidations();
			myAccountPage.wishListHeaderValidations();
			myAccountPage.wishListSectionValidations();
			myAccountPage.educationTextValidation();
			myAccountPage.educationLinksValidations();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_039_CUI_dashboardPageNewUser Description: To verify that
	 * Dashboard page is displayed properly for New owner Date: May/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_039_CUI_dashboardPageNewUser(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myAccountPage.newOwnerHeader();
			myAccountPage.userNameValidation();
			myAccountPage.imageValidations();
			myAccountPage.vipStatusValidation();
			myAccountPage.memberNumberValidation();
			myAccountPage.viewProfileValidations();
			myAccountPage.membershipTextValidation();
			myAccountPage.contractNumbersValidations();
			myAccountPage.myOwnershipLinkValidations();
			myAccountPage.pointsHeaderValidations();
			myAccountPage.ownerWithPoints();
			myAccountPage.ownerUseYearDates();
			myAccountPage.viewPointLinkValidations();
			myAccountPage.validateTextLocationField();
			myAccountPage.validateTextDateField();
			myAccountPage.buttonSearchAvailabilityDisabled();
			myAccountPage.ownerWithoutReservationsButPoints();
			myAccountPage.travelDealsHeaderValidation();
			myAccountPage.travelDealsCardsValidations();
			myAccountPage.wishListHeaderValidations();
			myAccountPage.wishListSectionValidations();
			myAccountPage.newTipsTextValidation();
			myAccountPage.tipsLinksValidations();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_040_CUI_dashboardPagePendingUser Description: To verify that
	 * Dashboard page is displayed properly for Pending owner Date: May/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_040_CUI_dashboardPagePendingUser(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myAccountPage.newOwnerHeader();
			myAccountPage.userNameValidation();
			myAccountPage.imageValidations();
			myAccountPage.vipStatusValidation();
			myAccountPage.memberNumberValidation();
			myAccountPage.viewProfileValidations();
			myAccountPage.membershipTextValidation();
			myAccountPage.contractNumbersValidations();
			myAccountPage.myOwnershipLinkValidations();
			myAccountPage.pointsHeaderValidations();
			myAccountPage.ownerWithoutPoints();
			myAccountPage.pendingUseYearDates();
			myAccountPage.viewPointLinkValidations();
			myAccountPage.pendingOwnerSearchForm();
			myAccountPage.ownerWithoutReservationsAndPoints();
			myAccountPage.travelDealsHeaderValidation();
			myAccountPage.travelDealsCardsValidations();
			myAccountPage.wishListHeaderValidations();
			myAccountPage.wishListSectionValidations();
			myAccountPage.newTipsTextValidation();
			myAccountPage.tipsLinksValidations();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_041_CUI_ResortNotEligibleAndNotPresentForMemberValidaton
	 * Description: Some resort are not eligible for member and not present in
	 * Exact Search Validation Date: Mar/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_041_CUI_ResortNotEligibleAndNotPresentForMemberValidaton(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.resortNotAbleToBook();
			searchPage.resortSearchedNotFoundValidation();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_042_CUI_InventoryDiscountValidation Description:Validate
	 * Inventory Discount Reflection Date:Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_042_CUI_InventoryDiscountValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.inventoryDiscountPriceValidation();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_043_CUI_DiscountNotPresenceDifferentConditionWithNoInventory
	 * Description: Validate Discount not present if no inventory defined for
	 * more than 2 months Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_043_CUI_DiscountNotPresenceDifferentConditionWithNoInventory(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.validateDiscountNotPresentWithNoInventory();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_044_CUI_DiscountPresenceWithInventoryLessThanMembershipDiscount
	 * Description: Validate Discount present if inventory defined less than
	 * Membership Discount Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_044_CUI_DiscountPresenceWithInventoryLessThanMembershipDiscount(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateDiscountPresenceWithInventory();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_045_CUI_AlternateSearchWithCity Description: Validate
	 * Alternate Search with City Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_045_CUI_AlternateSearchWithCity(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_046_CUI_AlternateSearchAndFlexibleSelectedNoFlexResult
	 * Description: Validate Alternate Search when flexible checkbox selected
	 * but not flex result Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_046_CUI_AlternateSearchAndFlexibleSelectedNoFlexResult(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.flexibleResultNotPresent();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_047_CUI_NoAlternateResultWithExactAndFlexibleResultPresent
	 * Description: Validate No Alternate Result Present if Exact and Flexible
	 * result present Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_047_CUI_NoAlternateResultWithExactAndFlexibleResultPresent(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.flexibleSearchResult();
			searchPage.validateAlternateMessageNotPresent();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_048_CUI_NoAlternateResultWithNoExactResultPresentARP
	 * Description: Validate No Alternate Result with no Exact Result in ARP
	 * Window Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_048_CUI_NoAlternateResultWithNoExactResultPresentARP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessageNotPresent();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_049_CUI_ClubPassResortNotInAlternateResult Description: Club
	 * Pass Resort Not Present in Alternate Result Date: Apr/2020
	 * Author:Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_049_CUI_ClubPassResortNotInAlternateResult(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			searchPage.clubPassResortNotPresentInAlternateSearch();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_050_CUI_ExactWithNoFlexibleResult Description: Validate Exact
	 * result with no flexible result Date: Apr/2020 Author: Unnat Jain Changes
	 * By:NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_050_CUI_ExactWithNoFlexibleResult(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultPresence();
			searchPage.flexibleResultPresence();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_051_CUI_ExactWithFlexibleResultSorting Description: Validate
	 * Exact result with flexible result and sorting Date: Apr/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_051_CUI_ExactWithFlexibleResultSorting(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultPresence();
			searchPage.flexibleResultPresence();
			searchPage.flexibleDatesSorting();
			searchPage.checkInDateMinus3CheckWithoutHold();
			searchPage.checkInDatePlus3CheckWithoutHold();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_052_CUI_NoExactWithFlexibleResultCheck Description: Validate
	 * No Exact result with flexible result when hold is applied Date: Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_052_CUI_NoExactWithFlexibleResultCheck(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultPresence();
			searchPage.flexibleResultPresence();
			searchPage.flexibleDatesSorting();
			searchPage.flexibleDateCheckWithHold();
			searchPage.flexibleDateOnHoldNotPresentInUI();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_053_CUI_NoExactWithNoFlexibleResultCheck Description: Validate
	 * No Exact result with No flexible result when hold is applied Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_053_CUI_NoExactWithNoFlexibleResultCheck(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultNotDisplayed();
			searchPage.flexibleResultNotPresent();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_054_CUI_ExactResultWithTenOrMoreResorts Description: Validate
	 * Exact Result has more than 10 resort Date: Apr/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_054_CUI_ExactResultWithTenOrMoreResorts(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.validateMoreThanTenResults();
			searchPage.flexibleResultPresence();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_055_CUI_FlexibleResultsWithoutCheckbox Description: Validate
	 * flexible result when hold is applied and check box not checked Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_055_CUI_FlexibleResultsWithoutCheckbox(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultPresence();
			searchPage.flexibleResultPresencePillFormat();
			searchPage.flexibleDatesSortingPill();
			searchPage.flexibleDateCheckWithHoldPillFormat();
			searchPage.flexibleDateOnHoldNotPresentInUIPill();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_056_CUI_NoResortMessageWithNOExactFlexibleAlternateResult
	 * Description: Validate No Result Found Message if No Exact Flexible and
	 * Alternate Result is Found Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By:NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_056_CUI_NoResortMessageWithNOExactFlexibleAlternateResult(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.flexibleResultNotPresent();
			searchPage.validateAlternateMessageNotPresent();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_057_CUI_AlternateSearchWithState Description: Validate
	 * Alternate Search with State Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_057_CUI_AlternateSearchWithState(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_058_CUI_verifyClubPassResortinExactSearch Description:Validate
	 * Alternate Verify Club Pass Resort in Exact Search Date: Apr/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_058_CUI_verifyClubPassResortinExactSearch(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_059_CUI_InventoryDiscountValidationOnlyResortSearched
	 * Description: Validate Inventory Discount Reflection and validations for
	 * resort searched Date:Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_059_CUI_InventoryDiscountValidationOnlyResortSearched(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.inventoryDiscountPriceValidationOnlyResortSearched();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method:
	 * tc_060_CUI_SearchValidationWorldMArkResortDistanceDifferentCondition
	 * Description: Search Validation and sorting AZ for worldmark Validations
	 * Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_060_CUI_SearchValidationWorldMarkResortDistanceDifferentCondition(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			if (testData.get("User_Type").equalsIgnoreCase("Discovery")) {
				searchPage.checkNoWorldMarkResort();
			} else {
				searchPage.checkWorldmarkResortPreseceAtLast();
				searchPage.checkWorldmarkResortAvailableUnitNotPresent();
				searchPage.checkSortAZOnlyWorldmark();
			}
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_061_CUI_SearchFilterValidation_Popular_AccessibleUnit
	 * Description: Date:April/2020 Author: Ajib parida Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })
	public void tc_061_CUI_SearchFilterValidation_Popular_AccessibleUnit(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				myaccountPage.validateTextLocationField();
				myaccountPage.validateTextDateField();
			}
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.verifyFilterAndSortButtonPresence();
			searchPage.validateSearchResultResort();
			searchPage.filterButtonClick();
			searchPage.validateFilterHeader();
			searchPage.checkSortByCollapse();
			searchPage.checkUnitTypeCollapse();
			searchPage.checkExperiencesTypeCollapse();
			searchPage.selectSort();
			searchPage.includeAccessibleTypeSelect();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkAccessibleUnit();
			searchPage.filterButtonClick();
			searchPage.resetFilter();
			searchPage.filterButtonClickAfterSelection();
			searchPage.validateSearchResultResort();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_062_CUI_SearchAvailablitySearchResultsResponseNoResult
	 * Description: Response to load search result validation Date: April/2020
	 * Author: Ajib parida Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })
	public void tc_062_CUI_SearchAvailablitySearchResultsResponseNoResult(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication(strBrowserInUse);
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateProcessingImage();
			searchPage.validateSearchResultZero();
			searchPage.verifyFilterAndSortDisable();
			searchPage.validateHoldLoading();
			searchPage.changeLocation();
			searchPage.exactResultNotDisplayed();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_063_CUI_ValidateUnitDetails Description: Search Availablity
	 * Search Results Unit Details Window Date: April/2020 Author: Ajib parida
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })
	public void tc_063_CUI_ValidateUnitDetails(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			searchPage.clickFirstAvailableUnit();
			searchPage.getUnitTypeAndPrice();
			searchPage.verifyUnitDetails();
			searchPage.verifyRoomAmenities();
			searchPage.verifyUnitImageNumber();
			searchPage.checkPriceBreakDownAssociated();
			searchPage.validateCheckInDateInPriceBreakDownArea();
			searchPage.checkDateIntervalPriceBreakdown();
			searchPage.validateCheckoutDatePriceBreakDownArea();
			searchPage.validateFloorPlan();
			searchPage.verifyUnitDetailsCloseButton();
			searchPage.verifyUnitNextPrevoiusButton();
			searchPage.verifyUnitDetailsCloseButton();
			searchPage.hideAllUnitsOpened();
			searchPage.filterButtonClick();
			searchPage.validateFilterHeader();
			searchPage.includeAccessibleTypeSelect();
			searchPage.filterButtonClickAfterSelection();
			searchPage.validateSearchResultResort();
			searchPage.clickFirstAvailableAccessibleUnit();
			searchPage.validateAccessibleUnitDetailsInUnitDetailsPopup();
			searchPage.verifyUnitDetailsCloseButton();
			searchPage.clickBookButton();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_064_SP_ContractCreation Description: Create Contract in
	 * Salepoint Date: April/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })
	public void tc_064_SP_ContractCreation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		SalepointPage_Web salepointPage = new SalepointPage_Web(tcconfig);

		try {
			salepointPage.launchSalepoint();

			salepointPage.setUserName();
			salepointPage.setPassword();
			salepointPage.clickLoginButton();
			salepointPage.companySelectSalePoint();
			salepointPage.navigateToMenu("1");
			salepointPage.contractProcessinglocateCustomer();
			salepointPage.contractProcessingOwners();
			salepointPage.contractProcessingDataEntrySelection();
			salepointPage.contractProcessingInventrySelectionPhase();
			salepointPage.contractPreocessingFeesScreen();
			salepointPage.contractProcessingPriceOverride();
			salepointPage.contractProcessingPaymentOption();
			salepointPage.contractProcessingMoneyScreen();
			salepointPage.contractProcessingComissionScreen();
			salepointPage.contrctProcessingMiscellaneousInformation();
			salepointPage.contractProcessingContractSummary();
			salepointPage.contractProcessingContractPrinting();
			salepointPage.logoutSalePoint();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_065_CUI_AlternateResultwithFlexWhenFlexCheckboxNotSelected
	 * Description: Validate Alternate Search and Flex ResultPresent even when
	 * flex checkbox is not selected Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By:NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_065_CUI_AlternateResultwithFlexWhenFlexCheckboxNotSelected(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			searchPage.flexResultWhenCheckBoxNotSelected();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method:
	 * tc_066_CUI_verifyFlexResultPresentWithNoAlternateResultWhenFlexCheckboxNotSelected
	 * Description: Validate No Alternate Result and Flex Result Present even
	 * when flex chcekbox is not selected Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By:NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_066_CUI_verifyFlexResultPresentWithNoAlternateResultWhenFlexCheckboxNotSelected(
			Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessageNotPresent();
			searchPage.flexResultWhenCheckBoxNotSelected();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_067_CUI_verifySearchResultcheckindateLastmonthofBookingWindow
	 * Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_067_CUI_verifySearchResultcheckindateLastmonthofBookingWindow(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.verifyFirstResort();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_068_CUI_searchAvailablitySearchResultsUI Date: Apr/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_068_CUI_searchAvailablitySearchResultsUI(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.verifyFirstResort();
			searchPage.verifyFilterAndSortButtonPresence();
			searchPage.verifyCheckinDateSelected();
			searchPage.verifyCheckoutDateSelected();
			searchPage.validateExperienceInResortCard();
			searchPage.verifyResortNameInEachResortCard();
			searchPage.verifyResortAddressInEachResortCard();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			searchPage.CheckUnitPriceAssociatedWithUnitType();
			searchPage.clickFirstAvailableUnitView();
			searchPage.clickAvailableUnit();
			searchPage.verifyUnitDetailsCloseButton();
			searchPage.hideAllUnitsOpened();
			searchPage.validateMapPresent();
			searchPage.clickMapToogleButton();
			searchPage.CheckBookAssociatedWithUnitType();
			searchPage.clickFirstAvailableUnitView();
			searchPage.clickBookButton();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_069_CUI_SearchValidation_Popular_WorladMarkResortAtLast
	 * Description: search validation with club pass resort at last sorting with
	 * Popular Date:May/2020 Author: Abhijeet Roy Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })
	public void tc_069_CUI_SearchValidation_Popular_WorladMarkResortAtLast(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.verifyFilterAndSortButtonPresence();
			searchPage.filterButtonClick();
			searchPage.validateFilterHeader();
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_070_CUI_AlternateSearchWithMap_NoExactFlexResult Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_070_CUI_AlternateSearchWithMap_NoExactFlexResult(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			searchPage.validateMapPresent();
			searchPage.checkFlexDateNotDisplayed();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_071_Reservation_PlatinumVIP_RentedPoints Description: Booking
	 * - Complete Reservation - Reservation Charges - Platinum VIP Date:
	 * June/2020 Author: Monideep Roychowdhury By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_071_Reservation_PlatinumVIP_RentedPoints(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();
			upgradePage.verifyFirstStep();
			upgradePage.optForUpgrade();
			upgradePage.clickContinueButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();
			reservationBalancePage.selectAndUpdateRentPoints();
			reservationBalancePage.clickContinueBooking();
			pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.validateBookingHeader("4");
			bookPage.validateReservationBalanceHeader();
			bookPage.validateMembershipChargesHeader();
			bookPage.validateTotalPointsValue();
			bookPage.validateUserPointsValue();
			bookPage.validateRentedPointsValue();

			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.validatePaymentcharges();
			bookPage.validateRentedPointsPaymentcharges();
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.validateAndEnterSpecialRequest();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_072_Booking_VIP_Upgrades_Opt_In_To_Future_Upgrades
	 * Description: PHEC-5584 Booking - VIP Upgrades - Opt-In To Future Upgrades
	 * - VIP - OptIn - Instant Upgrade - Platinum VIP Date: June/2020 Author:
	 * Monideep Roychowdhury By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_072_Booking_VIP_Upgrades_Opt_In_To_Future_Upgrades(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingReservPage = new CUIUpcomingVacationPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStep();
			upgradePage.verifyAndOptForFutureUpgrade();
			upgradePage.clickContinueButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			/*
			 * reservationBalancePage.validateReservationBalancePage();
			 * reservationBalancePage.clickContinueBooking();
			 */
			pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookConfPage.navigateToUpcomingVacations();
			upcomingReservPage.validateUpgradeFlag();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_073_Booking_VIP_Upgrades_Opt_In_Decline_To_Future_Upgrades
	 * Description: PHEC-5584 Booking - VIP Upgrades - Opt-In To Future Upgrades
	 * - VIP - Opt In - Decline - VIP - OptIn - Instant Upgrade - Platinum VIP
	 * Date: June/2020 Author: Monideep Roychowdhury By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_073_Booking_VIP_Upgrades_Decline_Future_Upgrades(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingReservPage = new CUIUpcomingVacationPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStep();
			upgradePage.verifyAndDeclineFutureUpgrade();
			upgradePage.clickContinueButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			bookConfPage.navigateToUpcomingVacations();
			upcomingReservPage.validateUpgradeNotRequestedFlag();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_074_Reservation_VIP_BorrowedPoints Description: Booking -
	 * Complete Reservation - Reservation Charges - VIP Date: June/2020 Author:
	 * Monideep Roychowdhury By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_074_Reservation_VIP_BorrowedPoints(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();
			upgradePage.verifyFirstStep();
			upgradePage.optForUpgrade();
			upgradePage.clickContinueButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();

			reservationBalancePage.selectAndUpdateBorrowPoints();
			reservationBalancePage.clickContinueBooking();
			// pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.validateBookingHeader("4");
			bookPage.validateReservationBalanceHeader();
			bookPage.validateMembershipChargesHeader();
			bookPage.validateTotalPointsValue();
			bookPage.validateUserPointsValue();

			bookPage.validateBorrowedPointsValue();

			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			// bookPage.validatePaymentcharges();
			// bookPage.validatePPCharges();
			// bookPage.validatePaymentsection();
			// bookPage.selectPaymentMethod();
			// bookPage.dataEntryForCreditCard();
			bookPage.validateAndEnterSpecialRequest();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_075_Borrow_Housekeeping_Credits_Non_VIP_Add_Guest Description:
	 * Reservation Confirmation - Borrow Housekeeping Credits - Non VIP - Add
	 * Guest Date: June/2020 Author: Monideep Roychowdhury By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch3" })

	public void tc_075_Borrow_Housekeeping_Credits_Non_VIP_Add_Guest(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.viewProfileValidations();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();
			travelPage.validateDefaultUserName();
			travelPage.selectAndPopulateGuestInfo();
			travelPage.clickContinueBooking();
			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();
			reservationBalancePage.selectAndUpdateRentPoints();
			reservationBalancePage.retrieveHouseKeepingCreditNeeded();
			reservationBalancePage.buyHouseKeepingCredits();
			reservationBalancePage.buyReservationTransaction();
			reservationBalancePage.clickContinueBooking();
			// pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.validateBookingHeader("3");
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			bookingConfPage.validateConfirmationHeader();
			bookingConfPage.validateReservationSummary();
			bookingConfPage.validateBookingDetails();
			bookingConfPage.validateChargesSummary();
			bookingConfPage.validatePaymentCharges();
			bookingConfPage.validateReservationCharges();
			bookingConfPage.validateCancellationPolicy();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_076_Reservation_NonVIP Description: Booking - Complete
	 * Reservation - Reservation Charges - Non VIP Date: June/2020 Author:
	 * Monideep Roychowdhury By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_076_Reservation_NonVIP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();
			reservationBalancePage.selectAndUpdateRentPoints();
			reservationBalancePage.retrieveHouseKeepingCreditNeeded();
			reservationBalancePage.buyHouseKeepingCredits();
			reservationBalancePage.buyReservationTransaction();
			reservationBalancePage.clickContinueBooking();

			pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.validateBookingHeaderNonVIP("3");
			bookPage.validateReservationBalanceHeader();
			bookPage.validateMembershipChargesHeader();
			bookPage.validateTotalPointsValue();
			bookPage.validateUserPointsValue();

			bookPage.validateRentedPointsValue();

			bookPage.validateHousekeepingChargesNonVIP();
			bookPage.validateReservationTransactionCharges();
			bookPage.validatePaymentcharges();
			// bookPage.validatePPCharges();
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_077_Reservation_DiscoveryMember Description: Booking -
	 * Complete Reservation - Reservation Charges - Discovery Member Date:
	 * June/2020 Author: Monideep Roychowdhury By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_077_Reservation_DiscoveryMember(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();

			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();
			reservationBalancePage.selectRentPoints();
			reservationBalancePage.buyReservationTransaction();
			reservationBalancePage.clickContinueBooking();

			bookPage.validateBookingHeader("3");
			bookPage.validateReservationBalanceHeader();
			bookPage.validateMembershipChargesHeader();
			bookPage.validateTotalPointsValue();
			bookPage.validateUserPointsValue();
			bookPage.validateRentedPointsIfSelected();
			// bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.validatePaymentcharges();
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();

			bookPage.validateAbsenceOfSpecialRequest();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_078_Reservation_SilverVIP Description: Booking - Complete
	 * Reservation - Reservation Charges - Silver VIP Date: June/2020 Author:
	 * Monideep Roychowdhury By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_078_Reservation_SilverVIP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();
			upgradePage.verifyFirstStep();
			upgradePage.optForUpgrade();
			upgradePage.clickContinueButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();
			reservationBalancePage.selectAndUpdateRentPoints();
			reservationBalancePage.clickContinueBooking();
			pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.validateBookingHeader("4");
			bookPage.validateReservationBalanceHeader();
			bookPage.validateMembershipChargesHeader();
			bookPage.validateTotalPointsValue();
			bookPage.validateUserPointsValue();
			bookPage.validateRentedPointsValue();

			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.validatePaymentcharges();
			bookPage.validateRentedPointsPaymentcharges();
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.validateAbsenceOfSpecialRequest();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_079_Reservation_GoMember Description: Booking - Complete
	 * Reservation - Reservation Charges - Club Go Member Date: June/2020
	 * Author: Monideep Roychowdhury By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_079_Reservation_GoMember(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();

			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();
			reservationBalancePage.selectRentPoints();
			reservationBalancePage.buyReservationTransaction();
			reservationBalancePage.clickContinueBooking();

			bookPage.validateBookingHeader("3");
			bookPage.validateReservationBalanceHeader();
			bookPage.validateMembershipChargesHeader();
			bookPage.validateTotalPointsValue();
			bookPage.validateUserPointsValue();
			bookPage.validateRentedPointsIfSelected();
			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.validatePaymentcharges();
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();

			bookPage.validateAbsenceOfSpecialRequest();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_080_Validate_Unable_to_Book_warning_pop_up_modal_Loan_payments_not_up_to_date
	 * Description: WDQA_PHEC-4798_Booking Pre-Booking Validations_Pre Booking-
	 * Delinquency Validation_Validate Unable to Book warning pop up modal_Loan
	 * payments not up to date Date: June/2020 Author: Monideep Roychowdhury By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_080_Validate_Unable_to_Book_warning_pop_up_modal(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonWithoutValidation();
			searchPage.validateUnableToBookModal();
			searchPage.clickCloseModalCTA();
			searchPage.verifyFirstResort();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_081_CUI_ModifyReservation_ModificationHistory_VIPUpgradeAccessibleUnit
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_081_CUI_ModifyReservation_ModificationHistory_VIPUpgradeAccessibleUnit(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			// modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.verifyVipUpgradeWithAccessibleUnitInModificationHistory();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();

			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			// modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyVipUpgradeWithAccessibleUnitInModificationHistory();
			// modifyPage.checkOwnerModifiedAfterModification();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_082_CUI_ReservationBalanceSectionsDisplayed Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_082_CUI_ReservationBalanceSectionsDisplayed(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.verifyBorrowRentPointsTitle();
			reservationBalancePage.validateFieldLabelAndValue("POINTS", "Required");
			reservationBalancePage.validateFieldLabelAndValue("POINTS", "Available");
			reservationBalancePage.validateFieldLabelAndValue("POINTS", "Amount");
			reservationBalancePage.verifyInstructionText("ReservationPoint");
			reservationBalancePage.selectCheckBox_ReservationBalanace("BorrowPoint");
			reservationBalancePage.verifyToolTip("Borrow-ReservationPoint");
			reservationBalancePage.verifyNextUseYear();
			reservationBalancePage.verifyPointAvailable();
			reservationBalancePage.FeildLevelValidationPointToBeBorrowed();
			reservationBalancePage.applyForMorePointThanAvailable("Borrow");
			reservationBalancePage.validateFieldLabelAndValue("POINTS", "Borrowing");
			reservationBalancePage.clickRemoveButton("Remove-PointToBorrow");
			reservationBalancePage.borrowValidPoint();
			reservationBalancePage.clickRemoveButton("Remove-PointToBorrow");
			reservationBalancePage.uncheckSelectedCheckBx("Borrow-ReservationPoint");
			reservationBalancePage.verifyToolTip("Rent-ReservationPoint");
			reservationBalancePage.selectCheckBox_ReservationBalanace("RentPoint");
			reservationBalancePage.verifyRentPointInputSection();
			reservationBalancePage.applyForMorePointThanAvailable("Rent");
			reservationBalancePage.clickRemoveButton("Remove-PointToRent");
			reservationBalancePage.rentValidPoint();
			reservationBalancePage.uncheckSelectedCheckBx("Rent-ReservationPoint");

			reservationBalancePage.verifyPurchaseHousekeepingCreditTitle();
			reservationBalancePage.validateFieldLabelAndValue("HOUSEKEEPING", "Required");
			reservationBalancePage.validateFieldLabelAndValue("HOUSEKEEPING", "Credits");
			reservationBalancePage.validateFieldLabelAndValue("HOUSEKEEPING", "Amount");
			reservationBalancePage.verifyInstructionText("HousekeepingCredits");
			reservationBalancePage.verifyToolTip("Purchase-HousekeepingCredit");
			reservationBalancePage.buyHouseKeepingCredits();
			reservationBalancePage.validateFieldLabelAndValue("HOUSEKEEPING", "Purchasing");
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_083_CUI_ReservationBalanceSectionsNotDisplayed Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_083_CUI_ReservationBalanceSectionsNotDisplayed(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.reservationBalancePageNotDisplayed();
			reservationBalancePage.backToPreviuosPage();

			travelPage.clickContinueBooking();

			pointProtectionPage.selectPointProtection();

			// bookPage.clickContinueButtonForBooking();
			bookPage.verifyCompleteBookingPage();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_084_CUI_ReservationBalanceSelectBorrowedPoints Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_084_CUI_ReservationBalanceSelectBorrowedPoints(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.validateFieldLabelAndValue("POINTS", "Required");

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_085_CUI_ReservationBalanceRentPointsUnableToBorrow
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_085_CUI_ReservationBalanceRentPointsUnableToBorrow(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateFieldLabelAndValue("POINTS", "Required");

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_086_CUI_ReservationBalanceNotEnoughTransaction Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_086_CUI_ReservationBalanceNotEnoughTransaction(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();

			reservationBalancePage.verifyPurchaseReservationTransaction();
			reservationBalancePage.validateFieldLabelAndValue("TRANSACTIONS", "Required");
			reservationBalancePage.validateFieldLabelAndValue("TRANSACTIONS", "Credits");
			reservationBalancePage.validateFieldLabelAndValue("TRANSACTIONS", "Amount");
			reservationBalancePage.verifyInstructionText("ReservationTransactions");
			reservationBalancePage.verifyToolTip("Purchase-ReservationTransaction");
			reservationBalancePage.VerifyRateReservationTransaction();
			reservationBalancePage.selectCheckBox_ReservationBalanace("PurchaseReservationTransaction");
			reservationBalancePage.validateFieldLabelAndValue("TRANSACTIONS", "Purchasing");
			reservationBalancePage.verifyChangesAfterSelectingReservationTransactionCheckBx();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_087_CUI_VIPUpgradesOutsideInstantUpgradeWindow Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_087_CUI_VIPUpgradesOutsideInstantUpgradeWindow(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			upgradePage.verifyContinueBtnDisableOnPageLoad();
			upgradePage.verifyBetterSuiteInstructionTxt();
			upgradePage.verifyUpgradeMeRadioBtnPresence();
			upgradePage.verifyKeepMySelectedSuiteRadioBtnPresence();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_088_CUI_NoGuestOptionOnTravelerInfoForDiscoveryVIPMember
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_088_CUI_NoGuestOptionOnTravelerInfoForDiscoveryVIPMember(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.fetchMemberName();
			accountSettingPage.fetchOwnerEmail();

			myaccountPage.navigateToDashBoardPage();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			travelPage.verifyFirstStep();
			travelPage.verifyOwnerNamePrepopulated();
			travelPage.verifyOwnerEmailPrepopulated();
			travelPage.verifyGuestOptionNotPresent();
			travelPage.clickContinueBooking();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_089_CUI_VerifyGuestCreditInfoForMutltiOwnerMember Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_089_CUI_VerifyGuestCreditInfoForMutltiOwnerMember(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.fetchMemberName();
			accountSettingPage.fetchOwnerEmail();

			myaccountPage.navigateToDashBoardPage();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.verifyOwnerNamePrepopulated();
			travelPage.verifyOwnerEmailPrepopulated();
			travelPage.selectAnotherValueFromDropdown();
			travelPage.selectGuestOption();
			travelPage.verifyLabelAndTextGuestConfirmationCredits();
			travelPage.checkRequiredAndAvailableValue("multi-Owner");
			travelPage.enterGuestInfo();
			travelPage.selectAgreeCheckBxGuestConfirmationCredits();
			travelPage.clickContinueBooking();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_090_CUI_VerifyGuestCreditInfoForSingleOwnerMember Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_090_CUI_VerifyGuestCreditInfoForSingleOwnerMember(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.fetchMemberName();
			accountSettingPage.fetchOwnerEmail();

			myaccountPage.navigateToDashBoardPage();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.verifyOwnerNamePrepopulatedSingleOwner();
			travelPage.verifyOwnerEmailPrepopulated();
			travelPage.selectGuestOption();
			travelPage.verifyLabelAndTextGuestConfirmationCredits();
			travelPage.checkRequiredAndAvailableValue("single-Owner");
			travelPage.verifyCreditRateSingleOwner();
			travelPage.enterGuestInfo();
			travelPage.selectAgreeCheckBxGuestConfirmationCredits();
			travelPage.clickContinueBooking();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_091_CUI_VerifyPointProtectionPageNotPresent Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_091_CUI_VerifyPointProtectionPageNotPresent(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			pointProtectionPage.verifyPointProtectionPageDisplayed("No");
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_092_CUI_VerifyPointProtectionPagePresent Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_092_CUI_VerifyPointProtectionPagePresent(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			pointProtectionPage.verifyPointProtectionPageDisplayed("Yes");
			pointProtectionPage.verifyReturnCTA();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_093_CUI_VerifyBackToAvailabilityPopUpModel Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_093_CUI_VerifyBackToAvailabilityPopUpModel(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.verifyBackToAvailability();
			searchPage.verifyFirstResort();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_094_CUI_VerifyUpgradeNowWithSameMultiUnitForVipUser
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_094_CUI_VerifyUpgradeNowWithSameMultiUnitForVipUser(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStepOnPageLoad();
			upgradePage.verifyInstantUpgrade();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_095_CUI_VerifyModifyCTANotPresent_ReservationTypeAsTPI
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_095_CUI_VerifyModifyCTANotPresent_ReservationTypeAsTPI(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadAllReservations();
			upcomingVacation.verifyUpcomingReservationCardPresent();
			upcomingVacation.verifyNoModifyCTAForReservation();
			upcomingVacation.selectReservationDetails();
			upcomingVacation.verifyTPILabel();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_096_CUI_VerifySkipUpgradeOptionForFutureUpgrade Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_096_CUI_VerifySkipUpgradeOptionForFutureUpgrade(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStepOnPageLoad();
			upgradePage.verifySkipUpgradeOption();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_097_CUI_VerifyReservationConfirmationPage Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_097_CUI_VerifyReservationConfirmationPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.fetchMemberName();
			accountSettingPage.fetchOwnerEmail();

			myaccountPage.navigateToDashBoardPage();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.verifyOwnerNamePrepopulated();
			travelPage.verifyOwnerEmailPrepopulated();
			// travelPage.selectAnotherValueFromDropdown();
			travelPage.selectGuestOption();
			travelPage.enterGuestInfo();
			travelPage.selectAgreeCheckBxGuestConfirmationCredits();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.selectCheckBox_ReservationBalanace("BorrowPoint");
			reservationBalancePage.borrowValidPoint();
			reservationBalancePage.clickContinueBooking();

			// pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.verifyCompleteBookingPage();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			bookingConfPage.validateConfirmationHeader();
			bookingConfPage.validateReservationSummaryDetails();
			bookingConfPage.validateBookingDetails();
			bookingConfPage.validateChargesSummaryForBorrowingPoint();
			bookingConfPage.validateCancellationPolicy();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_098_CUI_VerifyReservationConfirmationPageForVIPGoldMemberAndInstantUpgrade
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_098_CUI_VerifyReservationConfirmationPageForVIPGoldMemberAndInstantUpgrade(
			Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			upgradePage.selectFirstUpgradeOption();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.selectGuestOption();
			travelPage.enterGuestInfo();
			travelPage.selectAgreeCheckBxGuestConfirmationCredits();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.selectCheckBox_ReservationBalanace("RentPoint");
			reservationBalancePage.rentValidPoint();
			reservationBalancePage.clickContinueBooking();

			pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.verifyCompleteBookingPage();
			bookPage.validateAndEnterSpecialRequest();
			bookPage.selectPaymentMethod();
			bookPage.paymentViaPaypal();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			bookingConfPage.validateConfirmationHeader();
			bookingConfPage.validateReservationSummaryDetails();
			bookingConfPage.validateBookingDetails();
			bookingConfPage.validateInstantUpgradeDetails();
			bookingConfPage.validateSpecialRequestSection();
			bookingConfPage.validateChargesSummaryForRentingPoint();
			bookingConfPage.validatePaymentChargesForPaypalPayment();
			bookingConfPage.validateRentedPointUnderPaymentCharges();
			bookingConfPage.validatePointProtectionUnderPaymentCharges("YES");
			bookingConfPage.validateCancellationPolicy();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_099_CUI_ModifyReservation_ModificationHistory_PointsProtection
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: July/2020 Author: Kamalesh Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_099_CUI_ModifyReservation_ModificationHistory_PointsProtection(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			// modifyPage.checkPointProtectionSection();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();

			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.verifyGuestInformationText();
			modifyTravelerPage.dataEntryForGuestOwner();
			modifyTravelerPage.verifyTextGuestConfirmationCredit();
			modifyTravelerPage.verifyGuestConfirmationCreditDetails();
			modifyTravelerPage.verifyGuestCreditRequiredText();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.verifyPaymentCharges();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyChargesSummarySection();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_100_CUI_ModifyReservation_OverlappingReservation_WarningMessage
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_100_CUI_ModifyReservation_OverlappingReservation_WarningMessage(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			// modifyPage.verifyModifyCardDetails();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();

			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.clickContinueButton();
			modifyTravelerPage.verifyOverlappingErrorMsg();
			modifyTravelerPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_101_CUI_CompanyVIP_OwnershipPage Description: Company VIP user
	 * Ownership page validations with SpeedPay Link check Date: June/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_101_CUI_CompanyVIP_OwnershipPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		String strUserType = testData.get("User_Type").toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myOwnershipPage.navigateToMyOwnershipPage();
			myOwnershipPage.verifyMemberNumber();
			myOwnershipPage.verifyMembershipType();
			myOwnershipPage.verifyVIPStatusType();
			myOwnershipPage.verifyAdditionalProgram();

			myOwnershipPage.financialDetailsHeader();
			myOwnershipPage.makePaymentHeader();
			myOwnershipPage.speedPayLinkPresence();
			myOwnershipPage.hiddenValueAttribute();
			myOwnershipPage.speedPayLinkValidation();

			myOwnershipPage.myContractsHeader();
			myOwnershipPage.expandContracts();
			myOwnershipPage.totalContractsValidations();
			myOwnershipPage.ownerNameValidations();
			myOwnershipPage.pointsOwnedValidations();
			myOwnershipPage.contractDetailsValidations();
			myOwnershipPage.contractDetailsToolTip();
			myOwnershipPage.useYearValidations();
			myOwnershipPage.useYearDateFormat();

			if (!strUserType.contains("BONUS") || strUserType.contains("DISCOVERY")) {
				myOwnershipPage.homeResortValidations();
				myOwnershipPage.homeResortLinkValidations();
				myOwnershipPage.expandContracts();
				myOwnershipPage.reciprocalResortValidations();
				myOwnershipPage.reciprocalResortLinkValidations();
			}

			myOwnershipPage.verifyPointOfContact();
			myOwnershipPage.verifyOwnerName();
			myOwnershipPage.verifyEmail();
			myOwnershipPage.verifyAddress();
			myOwnershipPage.verifyPhoneNumber();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_102_CUI_Seasoned_PointsSummaryPage Description: Seasoned Owner
	 * Points Summary Page page validations Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_102_CUI_Seasoned_PointsSummaryPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointsSummaryPage = new CUIPointSummaryPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointsSummaryPage.navigateToPointsSummaryPage();
			pointsSummaryPage.pointSummaryPageSection();
			pointsSummaryPage.currentUseYearSection();
			pointsSummaryPage.currentUseYearDateFormat();
			pointsSummaryPage.currentUseYearPoints();
			pointsSummaryPage.houseKeepingCreditsSection();
			pointsSummaryPage.houseKeepingPoints();

			pointsSummaryPage.reservationTransactionHeader();
			pointsSummaryPage.reservationTransactionLeft();
			pointsSummaryPage.confirmationCreditHeader();
			pointsSummaryPage.confirmationCreditLeft();

			pointsSummaryPage.futureUseYearAccordion("true");
			pointsSummaryPage.futureUseYearAccordionContent();
			pointsSummaryPage.futureUseYearDateFormat();
			pointsSummaryPage.accordionUseYearPoints();
			pointsSummaryPage.accordionHousekeepingPoints();
			pointsSummaryPage.futureUseYearAccordion("false");

			pointsSummaryPage.defaultTransactionHistory();
			pointsSummaryPage.selectUseYearTransaction(testData.get("pastUseYear"));
			pointsSummaryPage.tableHeaderValidation();
			pointsSummaryPage.pointSortValidation();
			pointsSummaryPage.dateSortValidation();
			pointsSummaryPage.tableTypeFilterValidation();
			pointsSummaryPage.expanderValidations();
			pointsSummaryPage.totalPointsValidation();
			pointsSummaryPage.selectUseYearTransaction(testData.get("futureUseYear"));
			pointsSummaryPage.selectUseYearTransaction(testData.get("currentUseYear"));

			pointsSummaryPage.pointsEducationLinks();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_103_CUI_Pending_PointsSummaryPage Description: Pending Owner
	 * Points Summary Page page validations Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_103_CUI_Pending_PointsSummaryPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointsSummaryPage = new CUIPointSummaryPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointsSummaryPage.navigateToPointsSummaryPage();
			pointsSummaryPage.pointSummaryPageSection();
			pointsSummaryPage.pendingCurrentBalance();
			pointsSummaryPage.noTransactionDisplayed();

			pointsSummaryPage.pointsEducationLinks();

			loginPage.logOutApplicationViaDropDown();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_104_CUI_BucketListPageValidations Description: Bucket List
	 * Page Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_104_CUI_BucketListPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIBucketListPage_Web bucketListPage = new CUIBucketListPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			bucketListPage.navigateToBucketListPage();
			bucketListPage.resortCardsNumbers();
			bucketListPage.resortCardContentValidations();
			bucketListPage.resortCardNavigation_UJ();
			bucketListPage.whereToNextCard();
			bucketListPage.exploreCTANavigation();
			bucketListPage.wishListedCardValidation();
			bucketListPage.removeWishlishtedCard();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_105_CUI_NewOwnerTipsValidation Description: New owner (With or
	 * Without Points) owner Tips Validation Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_105_CUI_NewOwnerTipsValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.newOwnerGetStartedTips();
			myAccountPage.newTipsTextValidation();
			myAccountPage.tipsLinksValidations();
			myAccountPage.tipsModalValidations();
			myAccountPage.newOwnerCompletedTips();
			myAccountPage.newTipsCompleteTextValidation();
			myAccountPage.tipCheckMarkedValidation();
			myAccountPage.allTipCompleteValidation();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_106_CUI_ReservationCancellationPrior15Days Description:Cancel
	 * Reservation Flow Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_106_CUI_ReservationCancellationPrior15Days(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadAllReservations();
			upcomingVacation.selectReservationToCancel();

			cancelReservation.reservationNumberCheck();
			cancelReservation.reservationResortName();
			cancelReservation.checkInDateCheck();
			cancelReservation.checkOutDateCheck();
			cancelReservation.reimbursementSectionHeader();
			cancelReservation.forfeitSummaryNotPresent();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.pointsDropDownValidation();
			// cancelReservation.houseKeepingDropDownValidation();
			cancelReservation.paymentRemimbursementSectionValidation();
			cancelReservation.cancellationSection();
			cancelReservation.cancelButtonClick();

			reservationDetails.cancelledReservationName();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.modifyLinkNotPresent();
			reservationDetails.cancelLinkNotPresent();
			reservationDetails.reimbursedCharges();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.reimbursedPayment();
			reservationDetails.chargesTextIteration();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_107_CUI_CancellationForfeitSummary Description:Cancel
	 * Reservation Flow with Forfeit summary Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_107_CUI_CancellationForfeitSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadAllReservations();
			upcomingVacation.selectReservationToCancel();

			cancelReservation.reservationNumberCheck();
			cancelReservation.reservationResortName();
			cancelReservation.checkInDateCheck();
			cancelReservation.checkOutDateCheck();
			cancelReservation.forfeitSectionHeader();
			cancelReservation.reimburseSummaryNotPresent();
			cancelReservation.forfeitChargesSection();
			cancelReservation.pointsDropDownValidation();
			cancelReservation.houseKeepingDropDownValidation();
			cancelReservation.paymentRemimbursementSectionValidation();
			cancelReservation.cancellationSection();
			cancelReservation.cancelButtonClick();

			reservationDetails.cancelledReservationName();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.modifyLinkNotPresent();
			reservationDetails.cancelLinkNotPresent();
			reservationDetails.forfeitedCharges();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.reimbursedPayment();
			reservationDetails.pointsSummaryLink();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_108_CUI_ReservationCancellationBefore14Days Description:Cancel
	 * Reservation Flow Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_108_CUI_ReservationCancellationBefore14Days(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadAllReservations();
			upcomingVacation.selectReservationToCancel();

			cancelReservation.reservationNumberCheck();
			cancelReservation.reservationResortName();
			cancelReservation.checkInDateCheck();
			cancelReservation.checkOutDateCheck();
			cancelReservation.reimbursementSectionHeader();
			cancelReservation.forfeitSummaryNotPresent();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.pointsDropDownValidation();
			cancelReservation.houseKeepingDropDownValidation();
			cancelReservation.paymentRemimbursementSectionValidation();
			cancelReservation.cancellationSection();
			cancelReservation.cancelButtonClick();

			reservationDetails.cancelledReservationName();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.modifyLinkNotPresent();
			reservationDetails.cancelLinkNotPresent();
			reservationDetails.reimbursedCharges();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.reimbursedPayment();
			reservationDetails.chargesTextIteration();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_109_CUI_UpcomingVacationPageValidation Description:Upcoming
	 * Vacation Page Validation Flow Date: June/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_109_CUI_UpcomingVacationPageValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		int totalVacations;
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			totalVacations = myAccountPage.totalUpcomingReservations();
			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadReservationsTillEnd(totalVacations);
			upcomingVacation.verifyVacationCards(totalVacations);
			upcomingVacation.resortDatesSorting();
			upcomingVacation.pastReservationsSection();
			upcomingVacation.getInspiredSection();

			upcomingVacation.selectReservationDetails();
			upcomingVacation.verifyExtraHolidayLabel();
			upcomingVacation.loadAllReservations();
			upcomingVacation.verifyNoModifyCTAForReservation();
			upcomingVacation.verifyNoCancelCTAForReservation();
			upcomingVacation.mapLinkValidations();
			upcomingVacation.resortCardNavigation();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_110_CUI_MultipleModificationsDetailsPageValidation
	 * Description:Multiple Modification in a reservation validations Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch4" })

	public void tc_110_CUI_MultipleModificationsDetailsPageValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);

		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.selectReservationDetails();

			reservationDetails.validateBreadcrumb();
			reservationDetails.validateBookingDetails();
			reservationDetails.mapLinkValidations();
			reservationDetails.resortCardNavigation();
			reservationDetails.validateCancelModifyLink();
			reservationDetails.cancelationPolicy();
			reservationDetails.fullCancellationPolicy();
			reservationDetails.reservationSummaryTraveler();
			reservationDetails.validateReservationDetails();
			reservationDetails.validateSuiteDetails();
			reservationDetails.validateReservation();

			reservationDetails.verifyHeaderChargesSummary();
			reservationDetails.verifyHeaderMembershipCharges();
			reservationDetails.totalPointsValidation();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.totalReservationTransaction();
			reservationDetails.totalHousekeepingValidation();
			if (testData.get("PaymentReimburse").equalsIgnoreCase("Yes")) {
				reservationDetails.paymentInformation();
				reservationDetails.matchTotalPay();
			}
			reservationDetails.validateHeaderModification();
			reservationDetails.verifyModificationSortingDate();
			reservationDetails.validateTableModificationHistory();
			reservationDetails.validateCollapsedModifications();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_111_CUI_CanceledReservationPageValidation Description:Canceled
	 * Reservation Page validations Date: June/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_111_CUI_CanceledReservationPageValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointsSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointsSummaryPage.navigateToPointsSummaryPage();
			pointsSummaryPage.cancelReservationExpanderSelect();

			reservationDetails.validateBreadcrumb();
			reservationDetails.validateBookingDetails();
			reservationDetails.mapLinkValidations();
			reservationDetails.resortCardNavigation();
			reservationDetails.modifyLinkNotPresent();
			reservationDetails.cancelLinkNotPresent();
			reservationDetails.cancelationPolicyNotPresent();
			reservationDetails.reservationSummaryTraveler();
			reservationDetails.validateReservationDetails();
			reservationDetails.validateSuiteDetails();
			reservationDetails.validateReservation();

			reservationDetails.verifyHeaderReimbursementSummary();
			reservationDetails.totalPointsValidation();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.totalHousekeepingValidation();
			if (testData.get("PaymentReimburse").equalsIgnoreCase("Yes")) {
				reservationDetails.paymentInformation();
				reservationDetails.matchTotalPay();
			}

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_112_CUI_BookingFlowWithDetailsPageValidations
	 * Description:Booking a reservation and verify the details page Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_112_CUI_BookingFlowWithDetailsPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		String totalPoints;
		String reservationNumber;
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			bookPage.clickContinueButtonForBooking();
			bookPage.validateMembershipCharges();
			totalPoints = bookPage.getTotalPoints();
			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.verifyPaymentChargesSectionNotPresent();
			bookPage.verifyPaymentMethodHeaderNotPresent();
			bookPage.verifyPaymentOptionsNotPresent();
			bookPage.clickBookNowButton();
			reservationNumber = bookPage.reservationNumber();

			upcomingVacation.clickMyReservationLink();
			upcomingVacation.loadAllReservations(reservationNumber);
			upcomingVacation.selectReservationDetails(reservationNumber);

			reservationDetails.validateBreadcrumb();
			reservationDetails.validateBookingDetails();
			reservationDetails.mapLinkValidations();
			reservationDetails.resortCardNavigation();
			reservationDetails.validateCancelModifyLink();
			reservationDetails.cancelationPolicy();
			reservationDetails.fullCancellationPolicy();
			reservationDetails.reservationSummaryTraveler();
			reservationDetails.validateReservationDetails();
			reservationDetails.pointsProtectionNotPresent();
			reservationDetails.validateSuiteDetails();
			reservationDetails.validateReservation();

			reservationDetails.verifyHeaderChargesSummary();
			reservationDetails.verifyHeaderMembershipCharges();
			reservationDetails.totalPointsValidation(totalPoints);
			reservationDetails.pointsDropDownValidation();
			reservationDetails.totalReservationTransaction();
			reservationDetails.totalHousekeepingValidation();
			if (testData.get("PaymentReimburse").equalsIgnoreCase("Yes")) {
				reservationDetails.paymentInformation();
				reservationDetails.matchTotalPay();
			}

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_113_CUI_PointProtectionPageValidations Description: Point
	 * Protection Page Validation Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_113_CUI_PointProtectionPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			travelPage.fillTravellerInformation();
			bookPage.clickContinueButtonForBooking();
			pointProtectionPage.verifyDetailsinPointProtection();
			pointProtectionPage.verifyPointProtectionToolTip();
			pointProtectionPage.checkPointProtectionContinueButtonEnable();
			pointProtectionPage.selectPointProtection();
			bookPage.clickContinueButtonForBooking();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_114_CUI_CreateReservationWithNoCharges Description: Create
	 * Reservation with No Charges Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_114_CUI_CreateReservationWithNoCharges(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			bookPage.clickContinueButtonForBooking();
			pointProtectionPage.selectPointProtection();
			bookPage.clickContinueButtonForBooking();
			bookPage.validateMembershipCharges();
			bookPage.validateTotalPoints();
			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.verifyPaymentChargesSectionNotPresent();
			bookPage.verifyPaymentMethodHeaderNotPresent();
			bookPage.verifyPaymentOptionsNotPresent();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_115_CUI_ModifyReservation_OwnerToOwner Description: Modify
	 * Reservation from Owner to Owner Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_115_CUI_ModifyReservation_OwnerToOwner(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.dateOldestToNewestModificationTable();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();
			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_116_CUI_ModifyReservation_OwnerToOwner_Discovery Description:
	 * Modify Reservation Owner to Owner with Discovery Member Date:Apr/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_116_CUI_ModifyReservation_OwnerToOwner_Discovery(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.dateOldestToNewestModificationTable();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();
			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresenceDiscovery();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_117_CUI_ModifyReservation_OwnerToGuest Description: Modify
	 * Reservation from Owner to Guest Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_117_CUI_ModifyReservation_OwnerToGuest(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preGuestConfirmationValue();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.dateOldestToNewestModificationTable();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();
			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.verifyGuestInformationText();
			modifyTravelerPage.verifytooltipGuestInformation();
			modifyTravelerPage.dataEntryForGuestOwner();
			modifyTravelerPage.verifyTextGuestConfirmationCredit();
			modifyTravelerPage.verifyGuestConfirmationCreditDetails();
			modifyTravelerPage.verifyGuestCreditRequiredText();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();
			myaccountPage.navigateToDashBoardPage();
			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.postGuestConfirmationValue();
			pointSummaryPage.validateGuestCreditValueAfterModification();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_118_CUI_NoMonetaryCharges_ReservationSummary Description: No
	 * monetary charges displayed in Reservation Summary Page Date: Apr/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_118_CUI_NoMonetaryCharges_ReservationSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationDetails();
			reservationDetails.verifySectionReservationSummary();
			reservationDetails.verifySectionPaymentChargesNotDisplayed();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_119_CUI_ModifyReservation_GuestToOwner Description: Modify
	 * Reservation from Guest to Owner Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_119_CUI_ModifyReservation_GuestToOwner(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preGuestConfirmationValue();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.dateOldestToNewestModificationTable();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();
			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();
			myaccountPage.navigateToDashBoardPage();
			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.postGuestConfirmationValue();
			pointSummaryPage.validateGuestCreditNotReturned();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_120_CUI_PaypalCharges_ReservationSummary Description: Paypal
	 * Charges Verfication Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_120_CUI_PaypalCharges_ReservationSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationDetails();
			reservationDetails.verifySectionReservationSummary();
			reservationDetails.verifyHeaderPaymentChargesSectionDisplayed();
			reservationDetails.accountPaypalVerification();
			reservationDetails.verifyAmountsPaid();
			reservationDetails.verifyTotalAmountDisplayedAndCalculated();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_121_CUI_ReservationDetails_ChargesSummary Description:
	 * Reservation Details Charges Summary Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_121_CUI_ReservationDetails_ChargesSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationDetails();
			reservationDetails.verifyHeaderMembershipCharges();
			reservationDetails.membershipChargesTotalPointAreaCollapseForm();
			reservationDetails.membershipChargesTotalPointValidation();
			reservationDetails.membershipChargesTotalPointBreakDown();
			reservationDetails.membershipChargesHouseKeepingAreaCollapseForm();
			reservationDetails.membershipChargesHousekeepingPointValidation();
			reservationDetails.membershipChargesHousekeepingPointBreakDown();
			reservationDetails.membershipChargesReservationTransactionPointValidation();
			reservationDetails.verifyToolTipHousekeepingCredit();
			reservationDetails.verifyToolTipReservationTransaction();
			reservationDetails.verifyAmountsPaid();
			reservationDetails.verifyTotalAmountDisplayedAndCalculated();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_122_CUI_VerifyRCIAndDepositPage Description: RCI and Deposit
	 * page validation Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_122_CUI_VerifyRCIAndDepositPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.verifyOptionsInRCIandDepositPage();
			RCIandDepositPage.validatePointDepositLearnMoreCTA();
			RCIandDepositPage.pagenavigateBack();
			// RCIandDepositPage.clickCTACWPointDeposit();
			// RCIandDepositPage.pagenavigateBack();
			RCIandDepositPage.validateRCILearnMoreCTA();
			RCIandDepositPage.pagenavigateBack();
			RCIandDepositPage.clickButtonRCIExploreResort();
			RCIandDepositPage.acceptRCITermsandContinue();
			RCIandDepositPage.verifyRCILink();
			RCIandDepositPage.clickCTARCIPointDeposit();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_123_CUI_RCI_PointDeposit_MultiuseYear Description: RCI point
	 * deposit multi use year Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_123_CUI_RCI_PointDeposit_MultiuseYear(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);
		CUIPointDepositPage_Web RCIPointDepositPage = new CUIPointDepositPage_Web(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTARCIPointDeposit();
			RCIPointDepositPage.verifyProgressBar();
			RCIPointDepositPage.verifyBackRCIandDepositCTA();
			RCIPointDepositPage.verifyTextSelectDeposit();
			RCIPointDepositPage.verifyRCIPointChart();
			RCIPointDepositPage.verifyHeaderTotalDepositAndUseYear();
			RCIPointDepositPage.totalUseYear();
			RCIPointDepositPage.verifyPointAvailableRCIDeposit();
			// RCIPointDepositPage.verifyHousekeepingCreditRCIDeposit();
			RCIPointDepositPage.selectUseYearRCIDeposit();
			RCIPointDepositPage.verifyHeaderPointToDeposit();
			RCIPointDepositPage.totalPointsToDeposit();
			// RCIPointDepositPage.verifyHousekeepingSectionRCIDeposit();
			RCIPointDepositPage.clickContinueButton();
			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			// completeDepositPage.verifyHousekeepingCredit();
			// completeDepositPage.validateHousekeepingcreditBreakdown();
			completeDepositPage.verifyReservationTransaction();
			completeDepositPage.verifyHeaderPaymentCharges();
			completeDepositPage.verifyPaymentForRCIDeposit();
			completeDepositPage.totalChargesDueVerification();
			completeDepositPage.selectPaymentMethod();
			completeDepositPage.dataEntryForCreditCard();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			// successDepositPage.verifyHousekeepingCredit();
			// successDepositPage.validateHouskeepingBreakdownSuccess();
			successDepositPage.validateReservationTransactionSuccess();
			successDepositPage.validateChargesRequiredForBreakDown();
			successDepositPage.validateTotalValueCharged();
			successDepositPage.validatePaidByCreditCard();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_124_CUI_RCI_PointDeposit_CurrentUseYear_VIP Description: RCI
	 * point deposit Current use year VIP Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_124_CUI_RCI_PointDeposit_CurrentUseYear_VIP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);
		CUIPointDepositPage_Web RCIPointDepositPage = new CUIPointDepositPage_Web(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTARCIPointDeposit();
			RCIPointDepositPage.verifyProgressBar();
			RCIPointDepositPage.verifyBackRCIandDepositCTA();
			RCIPointDepositPage.verifyTextSelectDeposit();
			RCIPointDepositPage.verifyRCIPointChart();
			RCIPointDepositPage.verifyHeaderTotalDepositAndUseYear();
			RCIPointDepositPage.totalUseYear();
			RCIPointDepositPage.verifyPointAvailableRCIDeposit();
			RCIPointDepositPage.selectUseYearRCIDeposit();
			RCIPointDepositPage.verifyHeaderPointToDeposit();
			RCIPointDepositPage.totalPointsToDeposit();
			RCIPointDepositPage.clickContinueButton();
			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			completeDepositPage.verifyHousekeepingCreditNotPresent();
			completeDepositPage.verifyReservationTransaction();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_125_CUI_RCI_PointDeposit_PurchaseHousekeeping_SingleUseYear
	 * Description: RCI point deposit single use year purchasing Housekeeping
	 * Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_125_CUI_RCI_PointDeposit_PurchaseHousekeeping_SingleUseYear(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);
		CUIPointDepositPage_Web RCIPointDepositPage = new CUIPointDepositPage_Web(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTARCIPointDeposit();
			RCIPointDepositPage.verifyProgressBar();
			RCIPointDepositPage.verifyBackRCIandDepositCTA();
			RCIPointDepositPage.verifyTextSelectDeposit();
			RCIPointDepositPage.verifyRCIPointChart();
			RCIPointDepositPage.verifyHeaderTotalDepositAndUseYear();
			RCIPointDepositPage.totalUseYear();
			RCIPointDepositPage.verifyPointAvailableRCIDeposit();
			// RCIPointDepositPage.verifyHousekeepingCreditRCIDeposit();
			RCIPointDepositPage.selectUseYearRCIDeposit();
			RCIPointDepositPage.verifyHeaderPointToDeposit();
			RCIPointDepositPage.totalPointsToDeposit();
			// RCIPointDepositPage.verifyHousekeepingSectionRCIDeposit();
			// RCIPointDepositPage.verifyHousekeepingCreditPurchased();
			// RCIPointDepositPage.verifyCostHouskeepingPurchasedDeclaration();
			// RCIPointDepositPage.verifyTotalCostHouskeepingPurchased();
			RCIPointDepositPage.clickContinueButton();
			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			// completeDepositPage.verifyHousekeepingCredit();
			// completeDepositPage.validateHousekeepingcreditBreakdown();
			completeDepositPage.verifyReservationTransaction();
			// completeDepositPage.verifyHeaderPaymentCharges();
			// completeDepositPage.verifyPaymentForRCIDeposit();
			// completeDepositPage.totalChargesDueVerification();
			// completeDepositPage.selectPaymentMethod();
			// completeDepositPage.dataEntryForCreditCard();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			// successDepositPage.verifyHousekeepingCredit();
			// successDepositPage.validateHouskeepingBreakdownSuccess();
			successDepositPage.validateReservationTransactionSuccess();
			// successDepositPage.validateChargesRequiredForBreakDown();
			successDepositPage.validateTotalValueCharged();
			// successDepositPage.validatePaidByCreditCard();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_126_CUI_CW_PointDeposit_FullAmount Description: Club Wyndham
	 * Deposit Full Amount Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_126_CUI_CW_PointDeposit_FullAmount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);
		CUIPointDepositPage_Web PointDepositPage = new CUIPointDepositPage_Web(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTACWPointDeposit();
			PointDepositPage.verifyProgressBar();
			PointDepositPage.verifyBackRCIandDepositCTA();
			PointDepositPage.verifyTextSelectDeposit();
			PointDepositPage.verifyHeaderDepositFrom();
			PointDepositPage.verifyDepositFromYearAndPointAvailable();
			PointDepositPage.verifyAreaPointsToDeposit();
			PointDepositPage.pointToDepositCW();
			PointDepositPage.verifyHeaderDepositTo();
			PointDepositPage.totalUseYearCW();
			PointDepositPage.verifyPointAvailableDepositCW();
			PointDepositPage.selectUseYearRCIDeposit();
			PointDepositPage.clickContinueButton();
			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifyHeaderReviewDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyUseYearToDeposit();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			completeDepositPage.verifyHeaderPaymentCharges();
			completeDepositPage.costpointDepositCW();
			completeDepositPage.totalChargesDueVerification();
			completeDepositPage.selectPaymentMethod();
			completeDepositPage.dataEntryForCreditCard();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			successDepositPage.validatePaidByCreditCard();
			successDepositPage.costpointDepositCWSuccess();
			successDepositPage.validateTotalValueCharged();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_127_CUI_PaymentwithPayPal_OwnershipPage Description: Ownership
	 * Page Payment with Paypal Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_127_CUI_PaymentwithPayPal_OwnershipPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Web(tcconfig);
		CUIPaymentAmountPage_Web paymentAmountPage = new CUIPaymentAmountPage_Web(tcconfig);
		CUICompletePaymentPage_Web paymentCompletePage = new CUICompletePaymentPage_Web(tcconfig);
		CUILoanAssessmentSuccessPage_Web loanAssesmentSuccessPage = new CUILoanAssessmentSuccessPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myOwnershipPage.navigateToMyOwnershipPage();
			myOwnershipPage.financialDetailsHeader();
			myOwnershipPage.makePaymentHeader();
			myOwnershipPage.speedPayLinkPresence();
			myOwnershipPage.clickPayPalLink();
			ownershipPayPal.validateLinkBackToOwnership();
			ownershipPayPal.verifyHeaderPaymentType();
			ownershipPayPal.validatePaymentTypeTextAndField();
			ownershipPayPal.checkOptionsInPaymentTypeField();
			ownershipPayPal.selectPaymentType();
			ownershipPayPal.contractsInAscendingOrder();
			ownershipPayPal.verifyCurrentDueInContractCard();
			ownershipPayPal.verifyDueDateInContractCard();
			ownershipPayPal.selectContract();
			ownershipPayPal.clickContinueButton();
			paymentAmountPage.validateHeaderPaymentAmount();
			paymentAmountPage.selectCheckBoxCurrentBalance();
			paymentAmountPage.addAdditionalPoint();
			paymentAmountPage.clickContinueButton();
			paymentCompletePage.verifyHeadersinCompletePaymentPage();
			paymentCompletePage.verifyContractNumber();
			paymentCompletePage.totalPaymentBreakdown();
			paymentCompletePage.validateTotalChargeDue();
			paymentCompletePage.paymentViaPaypal();
			paymentCompletePage.clickContinueButton();
			loanAssesmentSuccessPage.verifyHeadersInSuccessPage();
			loanAssesmentSuccessPage.verifyContractNumber();
			loanAssesmentSuccessPage.verifyPayPalPaymentAccount();
			loanAssesmentSuccessPage.totalPaymentBreakdown();
			loanAssesmentSuccessPage.validateTotalChargeDue();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_128_CUI_ModificationPointProtection_ReservationSummary
	 * Description: Verify Point Protection in Reservation Summary Page in
	 * Modification Table Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })
	public void tc_128_CUI_ModificationPointProtection_ReservationSummary(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationDetails();
			reservationDetails.verifySectionReservationSummary();
			reservationDetails.validateHeaderModification();
			reservationDetails.validatePointProtectionInModificationHistory();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_129_CUI_ModificationHistory_ReservationSummary Description:
	 * Verify Modification in Reservation Summary Page in Modification Table
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })
	public void tc_129_CUI_ModificationHistory_ReservationSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationDetails();
			reservationDetails.verifySectionReservationSummary();
			reservationDetails.validateHeaderModification();
			reservationDetails.validateTableModificationHistory();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_130_CUI_ModificationVIPUpgrade_ReservationSummary Description:
	 * Verify Modification VIP Upgrade in Reservation Summary Page in
	 * Modification Table Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })
	public void tc_130_CUI_ModificationVIPUpgrade_ReservationSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationDetails();
			reservationDetails.verifySectionReservationSummary();
			reservationDetails.validateHeaderModification();
			reservationDetails.validateVIPUpgradeInModificationHistory();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_131_CUI_CW_PointDeposit_PartialAmount_NonVip Description: Club
	 * Wyndham Deposit Partial Amount Non VIP Date: Apr/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_131_CUI_CW_PointDeposit_PartialAmount_NonVip(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);
		CUIPointDepositPage_Web PointDepositPage = new CUIPointDepositPage_Web(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preUseYearPointsValue();
			myaccountPage.navigateToDashBoardPage();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTACWPointDeposit();
			PointDepositPage.verifyProgressBar();
			PointDepositPage.verifyBackRCIandDepositCTA();
			PointDepositPage.verifyTextSelectDeposit();
			PointDepositPage.verifyHeaderDepositFrom();
			PointDepositPage.verifyDepositFromYearAndPointAvailable();
			PointDepositPage.verifyHouseKeepingCreditAvailable();
			PointDepositPage.verifyAreaPointsToDeposit();
			PointDepositPage.pointToDepositCW();
			PointDepositPage.validateHouseKeepingArea();
			PointDepositPage.verifyHeaderDepositTo();
			PointDepositPage.totalUseYearCW();
			PointDepositPage.verifyPointAvailableDepositCW();
			PointDepositPage.verifyHouseKeepingValueCW();
			PointDepositPage.selectUseYearRCIDeposit();
			PointDepositPage.clickContinueButton();
			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifyHeaderReviewDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyUseYearToDeposit();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			completeDepositPage.verifyHousekeepingCredit();
			completeDepositPage.validateHousekeepingcreditBreakdown();
			completeDepositPage.verifyHeaderPaymentCharges();
			completeDepositPage.costpointDepositCW();
			completeDepositPage.totalChargesDueVerification();
			completeDepositPage.selectPaymentMethod();
			completeDepositPage.dataEntryForCreditCard();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			successDepositPage.verifyHousekeepingCredit();
			successDepositPage.validateHouskeepingBreakdownSuccess();
			successDepositPage.validatePaidByCreditCard();
			successDepositPage.costpointDepositCWSuccess();
			successDepositPage.validateTotalValueCharged();
			myaccountPage.navigateToDashBoardPage();
			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.postUseYearPointsValue();
			pointSummaryPage.validatePointsAfterDeposit();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_132_CUI_CW_PointDeposit_WithNoHK_NonVip Description: Point
	 * Deposit with No HK Points Date: Apr/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_132_CUI_CW_PointDeposit_WithNoHK_NonVip(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);
		CUIPointDepositPage_Web PointDepositPage = new CUIPointDepositPage_Web(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTACWPointDeposit();
			PointDepositPage.verifyProgressBar();
			PointDepositPage.verifyBackRCIandDepositCTA();
			PointDepositPage.verifyTextSelectDeposit();
			PointDepositPage.verifyHeaderDepositFrom();
			PointDepositPage.verifyDepositFromYearAndPointAvailable();
			// PointDepositPage.verifyHouseKeepingCreditAvailable();
			PointDepositPage.verifyAreaPointsToDeposit();
			PointDepositPage.pointToDepositCW();
			// PointDepositPage.validateHouseKeepingArea();
			// PointDepositPage.totalCostHousekeepingForDepositCW();
			PointDepositPage.verifyHeaderDepositTo();
			PointDepositPage.totalUseYearCW();
			PointDepositPage.verifyPointAvailableDepositCW();
			// PointDepositPage.verifyHouseKeepingValueCW();
			PointDepositPage.selectUseYearRCIDeposit();
			PointDepositPage.clickContinueButton();
			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifyHeaderReviewDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyUseYearToDeposit();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			// completeDepositPage.verifyHousekeepingCredit();
			// completeDepositPage.validateHousekeepingcreditBreakdown();
			completeDepositPage.verifyHeaderPaymentCharges();
			completeDepositPage.costpointDepositCW();
			completeDepositPage.totalChargesDueVerification();
			// completeDepositPage.selectPaymentMethod();
			// completeDepositPage.dataEntryForCreditCard();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			// successDepositPage.verifyHousekeepingCredit();
			// successDepositPage.validateHouskeepingBreakdownSuccess();
			// successDepositPage.checkHousekeepingPurchasedSuccess();
			// successDepositPage.validatePaidByCreditCard();
			// successDepositPage.validateHousekeepingCreditInPaymentCharges();
			successDepositPage.costpointDepositCWSuccess();
			successDepositPage.validateTotalValueCharged();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_133_CUI_ReservationDetails_VIPAccessible
	 * Description:tc_150_CUI_ReservationDetails_VIPAccessible: July/2020
	 * Author: Abhijeet Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_133_CUI_ReservationDetails_VIPAccessible(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingVacation.selectReservationDetails();

			reservationDetails.validateBreadcrumb();
			reservationDetails.validateBookingDetails();
			reservationDetails.reservationSummaryTraveler();
			reservationDetails.validateReservationDetails();
			reservationDetails.validateSuiteDetails();
			reservationDetails.validateSuiteUpgradeOpted();
			// reservationDetails.validateSpecialRequest();
			reservationDetails.validateReservation();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_134_CUI_CW_PointDeposit_BiAnnualMembership_Vip Description:
	 * tc_134_CUI_CW_PointDeposit_BiAnnualMembership_Vip Date: June/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_134_CUI_CW_PointDeposit_BiAnnualMembership_Vip(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Web(tcconfig);
		CUIPointDepositPage_Web PointDepositPage = new CUIPointDepositPage_Web(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Web(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preUseYearPointsValue();
			pointSummaryPage.preFutureUseYearPointsValue();

			myaccountPage.navigateToDashBoardPage();

			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTACWPointDeposit();

			PointDepositPage.verifyProgressBar();
			PointDepositPage.verifyBackRCIandDepositCTA();
			PointDepositPage.verifyTextSelectDeposit();
			PointDepositPage.verifyHeaderDepositFrom();
			PointDepositPage.verifyDepositFromYearAndPointAvailable();
			PointDepositPage.verifyAreaPointsToDeposit();
			PointDepositPage.pointToDepositCW();
			PointDepositPage.verifyHeaderDepositTo();
			PointDepositPage.totalUseYearCW();
			PointDepositPage.verifyPointAvailableDepositCW();
			PointDepositPage.selectUseYearRCIDeposit();
			PointDepositPage.clickContinueButton();

			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifyHeaderReviewDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyUseYearToDeposit();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			completeDepositPage.verifyHeaderPaymentCharges();
			completeDepositPage.costpointDepositCW();
			completeDepositPage.totalChargesDueVerification();
			completeDepositPage.selectPaymentMethod();
			completeDepositPage.dataEntryForCreditCard();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();

			successDepositPage.validateNavigateToDashBoard();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			successDepositPage.validatePaidByCreditCard();
			successDepositPage.costpointDepositCWSuccess();
			successDepositPage.validateTotalValueCharged();

			myaccountPage.navigateToDashBoardPage();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.postUseYearPointsValue();
			pointSummaryPage.postFutureUseYearPointsValue();
			pointSummaryPage.validatePointsAfterDeposit();
			pointSummaryPage.validatePointsAfterDepositForFutureUseYear();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_135_CUI_ModifiedReservationTo14Nights Description: Verify
	 * Modifed Reservation to 14 Nights Date: Aug/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_135_CUI_ModifiedReservationTo14Nights(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			// pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();
			// modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckOutDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckOutDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyPage.unableToModifyHeader();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_136_CUI_VerifyAddNightsForChekoutOutsideARP Description:
	 * Verify Modifed Reservation to 14 Nights Date: Aug/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_136_CUI_VerifyAddNightsForChekoutOutsideARP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			// pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.unavailableDatesAfterCheckOut();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_137_CUI_CreateReservationModifyCancelAndValidateDB
	 * Description: tc_145_CUI_CreateReservationModifyCancelAndValidateDB Date:
	 * Aug/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_137_CUI_CreateReservationModifyCancelAndValidateDB(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.checkCancelDB();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_138_ModifiedReservationCheckOutDate_WithoutDiscount
	 * Description: Modified Reservation CheckOut Date Without Discount Date:
	 * Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_138_ModifiedReservationCheckOutDate_WithoutDiscount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.getPriceWithoutDiscountForSpeceficRoom();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.getPriceForModify();
			modifyNightsPage.checkPriceModify();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckOutDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckOutDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_139_ModifiedReservationCheckOutDate_MembershipDiscount
	 * Description: Modified Reservation CheckOut Date Membership Discount Date:
	 * Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_139_ModifiedReservationCheckOutDate_MembershipDiscount(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.getPriceWithDiscountForSpeceficRoom();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.getPriceForModify();
			modifyNightsPage.checkPriceModify();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckOutDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckOutDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_140_CUI_ModifiedReservationCheckInDate_WithRentBorrowHK
	 * Description: tc_148_CUI_ModifiedReservationCheckInDate_WithRentBorrowHK
	 * Date: Aug/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_140_CUI_ModifiedReservationCheckInDate_WithRentBorrowHK(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.getPreCurrentYearHouskeepingValue();
			pointSummaryPage.futureUseYearAccordion("true");
			// pointSummaryPage.getPreFutureYearHouskeepingValue("10/01/20 -
			// 09/30/21");
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			// reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.hkCreditAbsence();
			// reservationBalancePage.selectAndEnterHouseKeepingBorrowPoints();
			// reservationBalancePage.getPurchasedHKCredit();
			// reservationBalancePage.selectCheckBox_ReservationBalanace("PurchaseCredit");
			// reservationBalancePage.purchaseValidCredit();
			// reservationBalancePage.clickContinueBooking();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			// modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.verifyHousekeepingChagesAbsence();
			// modifySuccessPage.validateCurrentUseYearHKUsed();
			// modifySuccessPage.validateUseYearHKBorrowed();
			// modifySuccessPage.validateUseYearHKPurchased();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			// myaccountPage.navigateToDashBoardPage();
			// pointSummaryPage.navigateToPointSummary();
			// pointSummaryPage.getPostCurrentYearHouskeepingValueAndValidate();
			// pointSummaryPage.futureUseYearAccordion("true");
			// pointSummaryPage.getPostFutureYearHouskeepingValueAndValidate("10/01/20
			// - 09/30/21");
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_141_CUI_VerifyAddNightsForDiscoveryCheckOutinARP Description:
	 * tc_149_CUI_VerifyAddNightsForDiscoveryCheckOutinARP Date: Aug/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_141_CUI_VerifyAddNightsForDiscoveryCheckOutinARP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.unavailableDatesAfterCheckOut();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_142_CUI_FixedWeekDatesAvailability_FixedWeekNotFullyBooked
	 * Description: Fixed Week Dates Availability Fixed Week Not Fully Booked
	 * Date: Aug/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_142_CUI_FixedWeekDatesAvailability_FixedWeekNotFullyBooked(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.onlyFixedWeekAvailableDates();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_143_CUI_FixedWeekOverlayValidation_FixedWeekFullyBooked
	 * Description: Overlay Validation when fixed weeke is fully booked Date:
	 * Aug/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_143_CUI_FixedWeekOverlayValidation_FixedWeekFullyBooked(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.clickModifyNightsButton();
			modifyPage.validateOverlayTextWhenWholeFixedWeekIsBooked();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_144_CUI_UnableToModifyNight_CheckInDateWithin48HoursForExistingReservation_ORCMember
	 * Description: Unable To Modify Night Check In Date Within 48 Hours For
	 * Existing Reservation for ORCMember Date: Aug/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_144_CUI_UnableToModifyNight_CheckInDateWithin48HoursForExistingReservation_ORCMember(
			Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.clickModifyNightsButton();
			modifyPage.verifyBackToModifyReservation();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_145_CUI_VerifyRatePlanInDatabase_NightModification
	 * Description: Verify that if CWP Standard member modifies check in date
	 * for a reservation having existing reservation's check-in date on first
	 * day of standard booking window, rate plan will be chnaged Date: Aug/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_145_CUI_VerifyRatePlanInDatabase_NightModification(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.optionInstantUpgrade();

			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();

			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();

			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookPage.checkRatePlanInDB();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.checkRatePlanInDBAfterModification();
			modifySuccessPage.verifyRatePlanInDBBeforeAndAfterModification();
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_146_CUI_ModifyBenefitType Description: Verify that If CWP
	 * standard member has an existing reservation with Benefit type and user
	 * change the existing resort with 3 nights at the begining of check-in date
	 * then benefit type must change in to other benefit types. Date: Aug/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_146_CUI_ModifyBenefitType(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		TripPage_Web tripPage = new TripPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookPage.checkRatePlanInDB();
			loginPage.logOutApplicationViaDashboard();

			tripPage.launchApplication();

			tripPage.setUserName();
			tripPage.setPassword();
			tripPage.clickButtonLogin();
			tripPage.searchForMember();
			tripPage.clickSearchButton();
			tripPage.selectMember();
			tripPage.profileHeaderClick();
			tripPage.reservationLinkClick();
			tripPage.feedReservationNumber();
			tripPage.reservationSelect();
			tripPage.fetchBenefits("BeforeModification");
			tripPage.logoutApplication();

			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.checkRatePlanInDBAfterModification();
			modifySuccessPage.verifyRatePlanInDBBeforeAndAfterBenefitsModification();
			loginPage.logOutApplicationViaDashboard();

			tripPage.launchApplication();

			tripPage.setUserName();
			tripPage.setPassword();
			tripPage.clickButtonLogin();
			tripPage.searchForMember();
			tripPage.clickSearchButton();
			tripPage.selectMember();
			tripPage.profileHeaderClick();
			tripPage.reservationLinkClick();
			tripPage.feedReservationNumber();
			tripPage.reservationSelect();
			tripPage.fetchBenefits("AfterModification");
			tripPage.compareBenefits("BeforeModification", "AfterModification");
			tripPage.logoutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_147_CUI_VerifyTotalToPayAmount Description: Verify non VIP CWP
	 * member able to view valid "Total to Pay" amount in both expanded and
	 * collapsed view when user adds up to 3 nights after existing check-out
	 * date with rented points, rented housekeeping charges and modification
	 * changes the original point protection tier of the existing
	 * reservation_total reservation duration 8+ days and express booking window
	 * Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_147_CUI_VerifyTotalToPayAmount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();

			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();

			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();

			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_148_CUI_VerifyPPSelectionWhenNoPPInOriginalReservation
	 * Description: MGVC member able to select point protection for entire
	 * reservation for check-out date modification when original reservation
	 * does not have point protection_standard booking window outside 15 days
	 * Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_148_CUI_VerifyPPSelectionWhenNoPPInOriginalReservation(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();

			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();

			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectModifiedPointProtection();
			modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection("Accepted");
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection("Accepted");

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_149_CUI_AddNights_Modify_NotEnoughPoint_Error Description:
	 * MGVC member able to select point protection for entire reservation for
	 * check-out date modification when original reservation does not have point
	 * protection_standard booking window outside 15 days Date: Aug/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_149_CUI_AddNights_Modify_NotEnoughPoint_Error(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.verifyNotEnoughPointErrorMsg();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_150_CUI_AddNights_PP_Opt_Out_Exp_Booking Description: Verify
	 * non VIP CWP member able to view valid "Total to Pay" amount in both
	 * expanded and collapsed view when user adds up to 3 nights after existing
	 * check-out date with rented points, rented housekeeping charges and
	 * modification changes the original point protection tier of the existing
	 * reservation_total reservation duration 8+ days and express booking window
	 * Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_150_CUI_AddNights_PP_Opt_Out_Exp_Booking(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();

			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();

			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();

			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection("Declined");
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection("Declined");

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_151_CUI_AddNights_PP_Change_49_99_ORC_Member Description: Trip
	 * Add Nights scenarios Test Case : TC026_HCWMP-131_ORC member can modify
	 * check-in date with point protection when modification changes the point
	 * protection tier from $49 to $99_ standard window Date: Aug/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_151_CUI_AddNights_PP_Change_49_99_ORC_Member(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();

			modifyNightsPage.validatePPsectionSelected("$50.00");
			modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection("Accepted");
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection("Accepted");
			reservationDetails.verifyPointsProtectionModification("$99.00");

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_152_CUI_AddNights_PP_NoChange_69_Tier_CWA_Member Description:
	 * Trip Add Nights scenarios Test Case : TC021_HCWMP-131_CWA member able to
	 * modify check-in date with point protection when modification does not
	 * change $69 tier_standard window Date: Aug/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_152_CUI_AddNights_PP_NoChange_69_Tier_CWA_Member(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();

			pointProtectionPage.validatePointProtectionTier("$69.00");
			;

			bookPage.textCompleteBooking();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();

			modifyNightsPage.validatePPsectionSelected("$0.00");
			modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckOutDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckOutDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection("All Nights Accepted");
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection("Accepted");

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_153_CUI_AddNights_ModifyCheckOutDateBorrowAndRent Description:
	 * Verify Modifed Reservation (Next Use Year check in) deducts points from
	 * Current use Year Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_153_CUI_AddNights_ModifyCheckOutDateBorrowAndRent(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			myaccountPage.navigateToDashBoardPage();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preUseYearPointsValue();
			pointSummaryPage.preFutureUseYearPointsValue();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();
			reservationBalancePage.setRentBorrowPoint();
			reservationBalancePage.clickContinueBooking();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckOutDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.verifyPaymentCharges_RentedPoint();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckOutDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPaymentCharges_RentedPoint();
			modifySuccessPage.verifyMembershipCharges_BorrowRentPoint();
			modifySuccessPage.getPointUsedForReservation("Future");
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			reservationDetails.verifyPaymentCharges_RentedPoint();
			reservationDetails.pointsSummaryLink();
			pointSummaryPage.postFutureUseYearPointsValue();
			pointSummaryPage.validatePointsAfterTransactionForFutureUseYear();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_154_CUI_AddNights_ModifiedReservationCurrentYearPoints
	 * Description: Verify Modifed Reservation (Next Use Year check in) deducts
	 * points from Current Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_154_CUI_AddNights_ModifiedReservationCurrentYearPoints(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preUseYearPointsValue();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.getPointUsedForReservation("Current");
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			reservationDetails.pointsSummaryLink();
			pointSummaryPage.postUseYearPointsValue();
			pointSummaryPage.validatePointsAfterTransactionForCurrentUseYear();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_155_CUI_AddNights_ModifyCheckInDateBorrowAndRent Description:
	 * Verify Modifed Reservation (Next Use Year check in) deducts points from
	 * Current use Year Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_155_CUI_AddNights_ModifyCheckInDateBorrowAndRent(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preUseYearPointsValue();
			pointSummaryPage.preFutureUseYearPointsValue();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			reservationBalancePage.setRentBorrowPoint();
			reservationBalancePage.clickContinueBooking();
			modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.verifyPaymentCharges_RentedPoint();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPaymentCharges_RentedPoint();
			modifySuccessPage.verifyMembershipCharges_BorrowRentPoint();
			modifySuccessPage.getPointUsedForReservation("Future");
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			reservationDetails.verifyPaymentCharges_RentedPoint();
			reservationDetails.pointsSummaryLink();
			pointSummaryPage.postFutureUseYearPointsValue();
			pointSummaryPage.validatePointsAfterTransactionForFutureUseYear();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_156_CUI_AddNights_ModifiedReservationCheckOutDate_WithRentBorrowHK
	 * Description: Modified Reservation Check Out Date with Rent & Borrow HK
	 * Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_156_CUI_AddNights_ModifiedReservationCheckOutDate_WithRentBorrowHK(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preFutureUseYearPointsValue();
			pointSummaryPage.preFutureUseYearHKValue();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();
			reservationBalancePage.setRentBorrowPoint();
			reservationBalancePage.setRentBorrowHK();
			reservationBalancePage.clickContinueBooking();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.verifyPaymentCharges_RentedPoint();
			modifyCompletePage.verifyPaymentCharges_PurchaseHK();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPaymentCharges_RentedPoint();
			modifySuccessPage.verifyPaymentCharges_HK();
			modifySuccessPage.verifyMembershipCharges_BorrowRentPoint();
			modifySuccessPage.getPointUsedForReservation("Future");
			modifySuccessPage.verifyBorrowedHK();
			modifySuccessPage.getHKUsedForReservation("Future");
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.pointsSummaryLink();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.pointsSummaryLink();
			pointSummaryPage.postFutureUseYearPointsValue();
			pointSummaryPage.postFutureUseYearHKValue();
			pointSummaryPage.validatePointsAfterCancellationForFutureUseYear();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_157_CUI_AddNights_Modify_Cancel_HK_Borrow_Points_NonVIP
	 * Description: Modified Reservation Check Out Date with Rent & Borrow HK
	 * Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_157_CUI_AddNights_Modify_Cancel_HK_Borrow_Points_NonVIP(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preFutureUseYearPointsValue();
			pointSummaryPage.preFutureUseYearHKValue();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			reservationBalancePage.setRentBorrowPoint();
			reservationBalancePage.setRentBorrowHK();
			reservationBalancePage.clickContinueBooking();
			modifyNightsPage.validatePPsectionSelected("$0.00");
			modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.verifyPaymentCharges_RentedPoint();
			modifyCompletePage.verifyPaymentCharges_PurchaseHK();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.verifyPaymentCharges_RentedPoint();
			modifySuccessPage.verifyPaymentCharges_HK();
			modifySuccessPage.verifyMembershipCharges_BorrowRentPoint();
			modifySuccessPage.getPointUsedForReservation("Future");
			modifySuccessPage.verifyBorrowedHK();
			modifySuccessPage.getHKUsedForReservation("Future");
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			reservationDetails.pointsSummaryLink();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.pointsSummaryLink();
			pointSummaryPage.postFutureUseYearPointsValue();
			pointSummaryPage.postFutureUseYearHKValue();
			pointSummaryPage.validatePointsAfterCancellationForFutureUseYear();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_158_CUI_AddNights_VerifyOverlappingMessage Description: Verify
	 * Modifed Reservation (Next Use Year check in) deducts points from Current
	 * use Year Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch6" })

	public void tc_158_CUI_AddNights_VerifyOverlappingMessage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.fetchMemberName();
			accountSettingPage.fetchOwnerEmail();

			/*
			 * myaccountPage.navigateToDashBoardPage();
			 * myaccountPage.enterLocation(); myaccountPage.selectCheckinDate();
			 * myaccountPage.selectCheckoutDate();
			 * myaccountPage.selectCalendarDone();
			 * myaccountPage.clickSearchAvailabilityButton();
			 * searchPage.verifyFirstResort();
			 * searchPage.clickFirstAvailableUnit();
			 * searchPage.clickBookButton();
			 * travelPage.verifyOwnerNamePrepopulated();
			 * travelPage.selectAnotherValueFromDropdown();
			 * travelPage.clickContinueButton();
			 * pointProtectionPage.selectPointProtection();
			 * bookPage.textCompleteBooking(); bookPage.clickBookNowButton();
			 * bookPage.getReservationNumber();
			 */

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.verifyOverlappingErrorMsg();
			modifyNightsPage.clickContinueButton();
			// modifyNightsPage.selectPointProtection();
			// modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}
	
	/*
	 * Method: tc_159_CUI_AssessmentsSummary_sectionValidation Description: TC017_CWAR-122_Owner with multiple contracts_Assessment Summary
	 *Date: June/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_159_CUI_AssessmentsSummary_sectionValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Web(tcconfig);
		CUIPaymentAmountPage_Web paymentAmountPage = new CUIPaymentAmountPage_Web(tcconfig);
		CUICompletePaymentPage_Web paymentCompletePage = new CUICompletePaymentPage_Web(tcconfig);
		CUILoanAssessmentSuccessPage_Web loanAssesmentSuccessPage = new CUILoanAssessmentSuccessPage_Web(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.selectCaptcha();
			loginPage.loginCTAClick();
			myOwnershipPage.clickMyOwnershipTab();
			myOwnershipPage.validateMyOwnershipAssessmentContents();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}
	
	/*
	 * Method: tc_160_CUI_AutoPayEnroll_Assessments_Total_Validation Description: TC017_CWAR-122_Owner with multiple contracts_Assessment Summary
	 *Date: June/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })

	public void tc_160_CUI_AutoPayEnroll_Assessments_Total_Validation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Web(tcconfig);
		CUIPaymentAmountPage_Web paymentAmountPage = new CUIPaymentAmountPage_Web(tcconfig);
		CUICompletePaymentPage_Web paymentCompletePage = new CUICompletePaymentPage_Web(tcconfig);
		CUILoanAssessmentSuccessPage_Web loanAssesmentSuccessPage = new CUILoanAssessmentSuccessPage_Web(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.selectCaptcha();
			loginPage.loginCTAClick();	
			myOwnershipPage.clickMyOwnershipTab();
			myOwnershipPage.validateAssessmentTotalLabels();
			myOwnershipPage.validateAutoEnrollmentStatus();
			myOwnershipPage.validateAssessmentByContract();	
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}
	
	/*
	 * Method: tc_161_CUI_Assessment_Details_Modal_Validation Description: TC017_CWAR-122_Owner with multiple contracts_Assessment Summary
	 *Date: June/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })
	public void tc_161_CUI_Assessment_Details_Modal_Validation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Web(tcconfig);
		CUIPaymentAmountPage_Web paymentAmountPage = new CUIPaymentAmountPage_Web(tcconfig);
		CUICompletePaymentPage_Web paymentCompletePage = new CUICompletePaymentPage_Web(tcconfig);
		CUILoanAssessmentSuccessPage_Web loanAssesmentSuccessPage = new CUILoanAssessmentSuccessPage_Web(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.selectCaptcha();
			loginPage.loginCTAClick();	
			myOwnershipPage.clickMyOwnershipTab();
			myOwnershipPage.validateAssessmentSummaryModal();	
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}
	
	/*
	 * Method: tc_162_CUI_Assessment_contract_Fee_Validation Description: TC017_CWAR-122_Owner with multiple contracts_Assessment Summary
	 *Date: June/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch5" })
	public void tc_162_CUI_Assessment_contract_Fee_Validation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Web(tcconfig);
		CUIPaymentAmountPage_Web paymentAmountPage = new CUIPaymentAmountPage_Web(tcconfig);
		CUICompletePaymentPage_Web paymentCompletePage = new CUICompletePaymentPage_Web(tcconfig);
		CUILoanAssessmentSuccessPage_Web loanAssesmentSuccessPage = new CUILoanAssessmentSuccessPage_Web(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.selectCaptcha();
			loginPage.loginCTAClick();	
			myOwnershipPage.clickMyOwnershipTab();
			myOwnershipPage.validateAssessmentsSummary();
			myOwnershipPage.validateAssessmentTotalValues();
			myOwnershipPage.calculateAssessmentSummaryModalValues();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

}