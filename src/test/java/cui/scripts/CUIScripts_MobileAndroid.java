package cui.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import cui.pages.CUIAccountCreatedPage_Android;
import cui.pages.CUIAccountCreatedPage_Web;
import cui.pages.CUIAccountSettingPage_Android;
import cui.pages.CUIAccountSettingPage_Web;
import cui.pages.CUIBookConfirmationPage_Android;
import cui.pages.CUIBookConfirmationPage_Web;
import cui.pages.CUIBookPage_Android;
import cui.pages.CUIBookPage_Web;
import cui.pages.CUIBookingUpgradePage_Android;
import cui.pages.CUIBookingUpgradePage_Web;
import cui.pages.CUIBucketListPage_Android;
import cui.pages.CUIBucketListPage_Web;
import cui.pages.CUICancelReservation_Android;
import cui.pages.CUICancelReservation_Web;
import cui.pages.CUICompleteDepositPage_Android;
import cui.pages.CUICompleteDepositPage_Web;
import cui.pages.CUICompleteModificationPage_Android;
import cui.pages.CUICompleteModificationPage_Web;
import cui.pages.CUICompletePaymentPage_Android;
import cui.pages.CUICompletePaymentPage_Web;
import cui.pages.CUICreateAccountDetailsPage_Android;
import cui.pages.CUICreateAccountDetailsPage_Web;
import cui.pages.CUICreateAccountPage_Android;
import cui.pages.CUICreateAccountPage_Web;
import cui.pages.CUIDepositSuccessPage_Android;
import cui.pages.CUIDepositSuccessPage_Web;
import cui.pages.CUIForgotPasswordPage_Android;
import cui.pages.CUIForgotPasswordPage_Web;
import cui.pages.CUIForgotUserNamePage_Android;
import cui.pages.CUIForgotUserNamePage_Web;
import cui.pages.CUILoanAssessmentSuccessPage_Android;
import cui.pages.CUILoanAssessmentSuccessPage_Web;
import cui.pages.CUILoginPage_Android;
import cui.pages.CUILoginPage_Web;
import cui.pages.CUIManagePasswordPopUp_Android;
import cui.pages.CUIManagePasswordPopUp_Web;
import cui.pages.CUIManageUsernamePopUp_Android;
import cui.pages.CUIManageUsernamePopUp_Web;
import cui.pages.CUIModifyNightsPage_Android;
import cui.pages.CUIModifyNightsPage_Web;
import cui.pages.CUIModifyReservationPage_Android;
import cui.pages.CUIModifyReservationPage_Web;
import cui.pages.CUIModifySuccessPage_Android;
import cui.pages.CUIModifySuccessPage_Web;
import cui.pages.CUIModifyTravelerPage_Android;
import cui.pages.CUIModifyTravelerPage_Web;
import cui.pages.CUIMyAccountPage_Android;
import cui.pages.CUIMyAccountPage_Web;
import cui.pages.CUIMyOwnershipPage_Android;
import cui.pages.CUIMyOwnershipPage_Web;
import cui.pages.CUIMyOwnershipPayPalPage_Android;
import cui.pages.CUIMyOwnershipPayPalPage_Web;
import cui.pages.CUINewPasswordPage_Android;
import cui.pages.CUINewPasswordPage_Web;
import cui.pages.CUIPasswordEmailMethodPage_Android;
import cui.pages.CUIPasswordEmailMethodPage_Web;
import cui.pages.CUIPasswordResetMethodPage_Android;
import cui.pages.CUIPasswordResetMethodPage_Web;
import cui.pages.CUIPaymentAmountPage_Android;
import cui.pages.CUIPaymentAmountPage_Web;
import cui.pages.CUIPersonalInformationPopUp_Android;
import cui.pages.CUIPersonalInformationPopUp_Web;
import cui.pages.CUIPointDepositPage_Android;
import cui.pages.CUIPointDepositPage_Web;
import cui.pages.CUIPointProtectionPage_Android;
import cui.pages.CUIPointProtectionPage_Web;
import cui.pages.CUIPointSummaryPage_Android;
import cui.pages.CUIPointSummaryPage_Web;
import cui.pages.CUIReservationBalancePage_Android;
import cui.pages.CUIReservationBalancePage_Web;
import cui.pages.CUIReservationDetailsPage_Android;
import cui.pages.CUIReservationDetailsPage_Web;
import cui.pages.CUISearchPage_Android;
import cui.pages.CUISearchPage_Web;
import cui.pages.CUISecurityQuestionPopUp_Android;
import cui.pages.CUISecurityQuestionPopUp_Web;
import cui.pages.CUITravellerInformationPage_Android;
import cui.pages.CUITravellerInformationPage_Web;
import cui.pages.CUIUpcomingVacationPage_Android;
import cui.pages.CUIUpcomingVacationPage_Web;
import cui.pages.CUIUsernameEmailMethodPage_Android;
import cui.pages.CUIUsernameEmailMethodPage_Web;
import cui.pages.CUIUsernameResetMethodPage_Android;
import cui.pages.CUIUsernameResetMethodPage_Web;
import cui.pages.CUI_RCIAndDepositPage_Android;
import cui.pages.CUI_RCIAndDepositPage_Web;

public class CUIScripts_MobileAndroid extends TestBase {
	public static final Logger log = Logger.getLogger(CUIScripts_MobileAndroid.class);

	/*
	 * Method: tc_01_CUISmoke_LoginPageDashboardValidations Description: CUI
	 * Login page Validations Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"smokeGroup"})

	public void tc_01_CUISmoke_LoginPageDashboardValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.globalHeaderPresence();
			loginPage.globalFooterPresence();
			loginPage.loginModalHeaders();
			loginPage.usernameFieldValidation();
			loginPage.passwordFieldValidation();
			loginPage.newAccountSection();
			loginPage.notAnOwnerSection();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.dashboardAccountInfoSection();
			myaccountPage.dashboardMyOwnerandPointsHeader();
			myaccountPage.validateSearchHeaderSmoke();
			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_02_CUISmoke_AccountSettingsPageval Description: CUI Account
	 * settings page Validations Date: Feb/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups={"smokeGroup"})

	public void tc_02_CUISmoke_AccountSettingsPageval(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.personalInformationSection();
			accountSettingPage.personalInformationNameSectionSmoke();
			accountSettingPage.personalInformationEmailSectionSmoke();
			accountSettingPage.personalInformationAddressSectionSmoke();
			accountSettingPage.personalInformationPhoneSectionSmoke();
			accountSettingPage.ownerProfilePicture();
			accountSettingPage.ownerUsernameValidation();
			accountSettingPage.usernameVerification();
			accountSettingPage.ownerSecurityQuestionsValidationSmoke();
			accountSettingPage.ownerPasswordSection();
			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_03_CUISmoke_SearchResultValidationWithFilter Description:
	 * Search Validation with no flex date Validations Date: Apr/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"smokeGroup"})

	public void tc_03_CUISmoke_SearchResultValidationWithFilter(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateNumberOfResortCard();
			searchPage.validateMapPresent();
			searchPage.filterButtonClick();
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifySortInAZ();
			searchPage.checkWorldmarkResortPreseceAtLast();

			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_001_CUI_LoginPageValidations Description: CUI Login page
	 * Validations Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_001_CUI_LoginPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.globalHeaderPresence();

			loginPage.globalFooterPresence();
			loginPage.loginModalHeaders();
			loginPage.usernameFieldValidation();
			loginPage.passwordFieldValidation();
			loginPage.newAccountSection();
			loginPage.notAnOwnerSection();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_002_CUI_LoginErrorValidations Description: CUI Login page
	 * Error Validations Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_002_CUI_LoginErrorValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.loginCTADisabled();
			loginPage.setUserName();
			loginPage.loginCTADisabled();
			loginPage.clearFieldRefresh();
			loginPage.setPassword();
			loginPage.loginCTADisabled();

			loginPage.invalidLoginDataError();
			loginPage.invalidUsername();
			loginPage.setPassword();
			loginPage.clickLoginCTAError();
			loginPage.errorMessageValidation("Username");

			loginPage.invalidLoginDataError();
			loginPage.setUserName();
			loginPage.invalidPassword();
			loginPage.clickLoginCTAError();
			loginPage.errorMessageValidation("Password");

			loginPage.invalidLoginDataError();
			loginPage.invalidUsername();
			loginPage.invalidPassword();
			loginPage.clickLoginCTAError();
			loginPage.errorMessageValidation("Username and Password");
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_003_CUI_IndividualCreateAccount Description: Create Account
	 * for Individual Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_003_CUI_IndividualCreateAccount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUICreateAccountDetailsPage_Web accountDetails = new CUICreateAccountDetailsPage_Android(tcconfig);
		CUIAccountCreatedPage_Web accountCreated = new CUIAccountCreatedPage_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			createAccount.createAccountNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				accountDetails.accountDetailsPageValidations();
			}
			accountDetails.accountDetailsDataEntry(this.toString());
			createAccount.clickContinue();

			accountCreated.accountCreatedPage();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_004_CUI_CompanyTrustCreateAccount Description: Create Account
	 * for Company/Trust Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_004_CUI_CompanyCreateAccount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUICreateAccountDetailsPage_Web accountDetails = new CUICreateAccountDetailsPage_Android(tcconfig);
		CUIAccountCreatedPage_Web accountCreated = new CUIAccountCreatedPage_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			createAccount.createAccountNavigation();
			createAccount.companyTrustNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				accountDetails.accountDetailsPageValidations();
			}
			accountDetails.accountDetailsDataEntry(this.toString());
			createAccount.clickContinue();

			accountCreated.accountCreatedPage();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_005_CUI_IndividualForgotUsername Description: Username
	 * retrieval for Individual Account Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_005_CUI_IndividualForgotUsername(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Android(tcconfig);
		CUIUsernameEmailMethodPage_Web emailMethod = new CUIUsernameEmailMethodPage_Android(tcconfig);
		CUIUsernameResetMethodPage_Web resetmethod = new CUIUsernameResetMethodPage_Android(tcconfig);
		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			forgotUsername.forgotUsernameNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				resetmethod.verificationPageValidation();
			}
			resetmethod.verificationViaMethod();
			createAccount.clickContinue();

			if (methodSelect.contains("EMAIL")) {
				emailMethod.emailPageValidations();
				emailMethod.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				loginPage.preEnteredUsernameValidation(this.toString());
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplication();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_006_CUI_CompanyForgotUsername Description: Username retrieval
	 * for Company/Trust Account Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData")

	public void tc_006_CUI_CompanyForgotUsername(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Android(tcconfig);
		CUIUsernameEmailMethodPage_Web emailMethod = new CUIUsernameEmailMethodPage_Android(tcconfig);
		CUIUsernameResetMethodPage_Web resetmethod = new CUIUsernameResetMethodPage_Android(tcconfig);
		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			forgotUsername.forgotUsernameNavigation();
			createAccount.companyTrustNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				createAccount.contractDetailsModalValidation();
			}
			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();
			loginPage.acceptCookie();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				resetmethod.verificationPageValidation();
			}
			resetmethod.verificationViaMethod();
			createAccount.clickContinue();
			if (methodSelect.contains("EMAIL")) {
				emailMethod.emailPageValidations();
				emailMethod.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				loginPage.preEnteredUsernameValidation(this.toString());
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplication();
			}

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_007_CUI_SecurityQuesErrorVal Description: Security Question
	 * Error Val in the Username Retrieval flow Account Date: Mar/2020 Author:
	 * Unnat Jain Changes By:NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_007_CUI_UsernameSecurityQuesError(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Android(tcconfig);
		CUIUsernameResetMethodPage_Web resetmethod = new CUIUsernameResetMethodPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotUsername.forgotUsernameNavigation();

			createAccount.createAccountDataEnter();
			createAccount.selectVerificationMethod();
			createAccount.clickContinue();

			resetmethod.selectSecurityMethod();
			resetmethod.securityQuestionErrorValdation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_008_CUI_PhoneNumberFieldVal Description: Phone Number Field
	 * Val in the Username Retrieval flow Account Date: Mar/2020 Author: Unnat
	 * Jain Changes By:NA
	 */

	@Test(dataProvider = "testData")

	public void tc_008_CUI_PhoneNumberFieldVal(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUIForgotUserNamePage_Web forgotUsername = new CUIForgotUserNamePage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotUsername.forgotUsernameNavigation();

			createAccount.createAccountDataEnter();
			createAccount.phoneNumberFieldValidations();

			createAccount.clickContinue();
			createAccount.checkIncorrectError();
		} catch (Exception e) {

		}
	}

	/*
	 * Method: tc_009_CUI_IndividualForgotPassword Description: CUI Forgot
	 * Password page Validations Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData")

	public void tc_009_CUI_IndividualForgotPassword(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUIForgotPasswordPage_Web forgotPassword = new CUIForgotPasswordPage_Android(tcconfig);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUsernameEmailMethodPage_Web usernameEmail = new CUIUsernameEmailMethodPage_Android(tcconfig);
		CUIUsernameResetMethodPage_Web usernameReset = new CUIUsernameResetMethodPage_Android(tcconfig);
		CUINewPasswordPage_Web newPassword = new CUINewPasswordPage_Android(tcconfig);
		CUIPasswordEmailMethodPage_Web passwordEmail = new CUIPasswordEmailMethodPage_Android(tcconfig);
		CUIPasswordResetMethodPage_Web passwordReset = new CUIPasswordResetMethodPage_Android(tcconfig);

		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotPassword.forgotPasswordNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				forgotPassword.passwordDetailsValidations();
				forgotPassword.usernameLinkCheck();
			}
			forgotPassword.enterUsername();
			forgotPassword.enterNameDetails();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				passwordReset.verificationPageValidation();
			}
			usernameReset.verificationViaMethod();
			createAccount.clickContinue();

			if (methodSelect.contains("EMAIL")) {
				passwordEmail.emailPageValidation();
				usernameEmail.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.newPasswordPageValidation();
				}
				newPassword.newPasswordDataEnter();
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.showPasswordCTA();
				}
				createAccount.clickContinue();
				newPassword.passwordResetPage();
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplication();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_010_CUI_CompanyTrustPassword Description: CUI Forgot Password
	 * page Validations Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData")

	public void tc_010_CUI_CompanyTrustPassword(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUIForgotPasswordPage_Web forgotPassword = new CUIForgotPasswordPage_Android(tcconfig);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUsernameEmailMethodPage_Web usernameEmail = new CUIUsernameEmailMethodPage_Android(tcconfig);
		CUIUsernameResetMethodPage_Web usernameReset = new CUIUsernameResetMethodPage_Android(tcconfig);
		CUINewPasswordPage_Web newPassword = new CUINewPasswordPage_Android(tcconfig);
		CUIPasswordEmailMethodPage_Web passwordEmail = new CUIPasswordEmailMethodPage_Android(tcconfig);
		CUIPasswordResetMethodPage_Web passwordReset = new CUIPasswordResetMethodPage_Android(tcconfig);
		String methodSelect = testData.get("Method").trim().toUpperCase();
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotPassword.forgotPasswordNavigation();
			createAccount.companyTrustNavigation();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				forgotPassword.passwordDetailsValidations();

			}
			forgotPassword.enterUsername();
			forgotPassword.enterNameDetails();
			createAccount.clickContinue();

			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				passwordReset.verificationPageValidation();
			}
			usernameReset.verificationViaMethod();
			createAccount.clickContinue();

			if (methodSelect.contains("EMAIL")) {
				passwordEmail.emailPageValidation();
				usernameEmail.verificationCodeErrors();
			} else if (methodSelect.contains("SECURITY")) {
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.newPasswordPageValidation();
				}
				newPassword.newPasswordDataEnter();
				if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
					newPassword.showPasswordCTA();
				}
				createAccount.clickContinue();
				newPassword.passwordResetPage();
				loginPage.enterPasswordOnly();
				loginPage.loginCTAClick();
				loginPage.logOutApplication();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_011_CUI_PasswordSecurityQuesError Description: Security
	 * Question Error Val in the Password Retrieval flow Account Date: Mar/2020
	 * Author: Unnat Jain Changes By:NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_011_CUI_PasswordSecurityQuesError(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUICreateAccountPage_Web createAccount = new CUICreateAccountPage_Android(tcconfig);
		CUIForgotPasswordPage_Web forgotPassword = new CUIForgotPasswordPage_Android(tcconfig);
		CUIUsernameResetMethodPage_Web resetMethod = new CUIUsernameResetMethodPage_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			forgotPassword.forgotPasswordNavigation();
			forgotPassword.enterUsername();
			forgotPassword.enterNameDetails();

			createAccount.clickContinue();

			resetMethod.selectSecurityMethod();
			resetMethod.securityQuestionErrorValdation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_011_CUI_PasswordSecurityQuesError Description: Security
	 * Question Error Val in the Password Retrieval flow Account Date: Mar/2020
	 * Author: Unnat Jain Changes By:NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_012_CUI_MultipleLoginError(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();

			loginPage.multipleInvalidLogin();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.multilpeLoginClick();
			loginPage.verifyAccountLocked();

		} catch (Exception e) {

		}

	}

	/*
	 * Method: tc_013_CUI_AccountSettingsval Description: CUI Account settings
	 * page Validations Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData")

	public void tc_013_CUI_AccountSettingsPageval(Map<String, String> testData) throws Exception {

		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();

			accountSettingPage.personalInformationSection();
			accountSettingPage.personalInformationNameSection();
			accountSettingPage.personalInformationEmailSection();
			accountSettingPage.personalInformationAddressSection();
			accountSettingPage.personalInformationPhoneSection();

			accountSettingPage.ownerProfilePicture();
			accountSettingPage.imageUploadedvalidation();

			accountSettingPage.ownerUsernameValidation();
			accountSettingPage.usernameVerification();

			accountSettingPage.ownerSecurityQuestionsValidation();
			accountSettingPage.securityQuestion1Validation();
			accountSettingPage.securityQuestion2Validation();
			accountSettingPage.securityQuestion3Validation();

			accountSettingPage.ownerPasswordSection();
			accountSettingPage.passwordVerification("CUI_password", "Yes");
			loginPage.logOutApplication();
		} catch (Exception e) {

		}

	}

	/*
	 * Method: tc_014_CUI_UsernamePopUpVal Description: CUI Account settings
	 * page username pop up Validations Date: Feb/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_014_CUI_UsernamePopUpVal(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			manageUsername.usernamePopUpModalNavigation();
			manageUsername.usernameModalValidation();
			loginPage.logOutApplication();
		} catch (Exception e) {

		}

	}

	/*
	 * Method: tc_015_CUI_PasswordPopUpVal Description: CUI Account settings
	 * page Password pop up Validations Date:Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_015_CUI_PasswordPopUpVal(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			managePassword.passwordPopUpModalNavigation();
			managePassword.passwordModalValidation();
			loginPage.logOutApplication();
		} catch (Exception e) {

		}

	}

	/*
	 * Method: tc_016_CUI_SecQuesPopUpVal Description: CUI Account settings page
	 * Sec Ques pop up Validations Date:Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData")

	public void tc_016_CUI_SecQuesPopUpVal(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.securityQuestionModalValidation();
			loginPage.logOutApplication();
		} catch (Exception e) {

		}

	}

	/*
	 * Method: tc_017_CUI_PersonalInfoPopUpVal Description: CUI Account settings
	 * page Sec Ques pop up Validations Date:Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_017_CUI_PersonalInfoPopUpVal(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUIPersonalInformationPopUp_Web personalInformation = new CUIPersonalInformationPopUp_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			accountSettingPage.accountSettingsNavigation();
			personalInformation.personalInformationModalNavigation();
			personalInformation.personalInformationModalValidation();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_018_CUI_AccountInfoCancelCTAVal Description: CUI Account
	 * settings page Sec Ques pop up Validations Date:Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_018_CUI_AccountInfoCancelCTAVal(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUIPersonalInformationPopUp_Web personalInformation = new CUIPersonalInformationPopUp_Android(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Android(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Android(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			personalInformation.personalInformationModalNavigation();
			personalInformation.editEmailField();
			personalInformation.personalInformationCancelCTA();
			manageUsername.usernamePopUpModalNavigation();
			manageUsername.editOldUsernameField();
			manageUsername.usernameCancelCTA();
			managePassword.passwordPopUpModalNavigation();
			managePassword.enterNewPassword();
			managePassword.passwordCancelCTA();
			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.editAnswer1();
			securityQuestion.securityCancelCTA();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_019_CUI_ProfilePopUpErrorValidations Description: CUI Account
	 * settings page pop up error validation Date:Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch1"})

	public void tc_019_CUI_ProfilePopUpErrorValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUIPersonalInformationPopUp_Web personaleInformation = new CUIPersonalInformationPopUp_Android(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Android(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Android(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();

			personaleInformation.personalInformationModalNavigation();
			personaleInformation.invalidAddress1Field();
			personaleInformation.invalidAddressError();
			personaleInformation.invalidEmailField();
			personaleInformation.invalidEmailError();
			personaleInformation.personalInformationCancelCTA();

			manageUsername.usernamePopUpModalNavigation();
			manageUsername.takenUsernameData();
			manageUsername.takenUsernameError();
			manageUsername.availableUsername();
			manageUsername.usernameCancelCTA();

			managePassword.passwordPopUpModalNavigation();
			managePassword.invalidNewPassword();
			managePassword.newPasswordFieldError();
			managePassword.incorrectDataCurrent();
			managePassword.enterNewPassword();
			managePassword.saveButtonClick();
			managePassword.incorrectCurrentPassword();
			managePassword.passwordCancelCTA();

			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.checkSelectQuestion1();
			securityQuestion.enterAnswer1();
			securityQuestion.emptyAnswer1Error();
			securityQuestion.checkSelectQuestion2();
			securityQuestion.enterAnswer2();
			securityQuestion.emptyAnswer2Error();
			securityQuestion.checkSelectQuestion3();
			securityQuestion.enterAnswer3();
			securityQuestion.emptyAnswer3Error();
			securityQuestion.securityCancelCTA();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_020_CUI_AccountInformationChanges Description: CUI Account
	 * settings page changes validation Date:Mar/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_020_CUI_AccountInformationChanges(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUIPersonalInformationPopUp_Web personaleInformation = new CUIPersonalInformationPopUp_Android(tcconfig);
		CUIManageUsernamePopUp_Web manageUsername = new CUIManageUsernamePopUp_Android(tcconfig);
		CUIManagePasswordPopUp_Web managePassword = new CUIManagePasswordPopUp_Android(tcconfig);
		CUISecurityQuestionPopUp_Web securityQuestion = new CUISecurityQuestionPopUp_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();

			personaleInformation.personalInformationModalNavigation();
			personaleInformation.editEmailField();
			personaleInformation.saveAccountInformationChanges();

			manageUsername.usernamePopUpModalNavigation();
			manageUsername.editNewUsernameField();
			manageUsername.saveUsernameChanges();

			managePassword.passwordPopUpModalNavigation();
			managePassword.editNewPasswordField();
			managePassword.savePasswordChanges();

			securityQuestion.securityQuestionPopUpModalNavigation();
			securityQuestion.editSecurityQuestion1Field();
			securityQuestion.saveSecurityChanges();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_021_CUI_SearchFilterValidation_ZA_Exp_Express Description: CUI
	 * Serach Filter Validation with ZA sorting Date: Mar/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_021_CUI_SearchFilterValidation_ZA_Exp_Express(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				myaccountPage.validateTextLocationField();
				myaccountPage.validateTextDateField();
			}
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.selectExperience();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifysortInZAOnlyClubWyndham();
			searchPage.checkExperienceSelectedPresent();
			searchPage.filterButtonClick();
			searchPage.resetFilter();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			searchPage.worldmarkAddress();
			searchPage.WorldMarkImage();
			searchPage.WorldMarkContact();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_022_CUI_SearchFilterValidationFlexDates Description: Flex and
	 * actual date validation Validation Validations Date: Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_022_CUI_SearchFilterValidationFlexDates(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateNumberOfResortCard();
			searchPage.validateMapPresent();
			searchPage.checkFlexDateDisplayed();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_023_CUI_SearchValidationNoFlexDates Description: Search
	 * Validation with no flex date Validations Date: Mar/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_023_CUI_SearchValidationNoFlexDates(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateNumberOfResortCard();
			searchPage.validateMapPresent();
			searchPage.checkFlexDateNotDisplayed();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * /* Method: tc_024_CUI_SearchValidationWorldMArkResortAZDifferentCondition
	 * Description: Search Validation with City entered and sorting AZ for
	 * worldmark Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_024_CUI_SearchValidationWorldMArkResortAZDifferentCondition(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifySortInAZ();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_025_CUI_SearchValidationWorldMArkResortZADifferentCondition
	 * Description: Search Validation with State entered and sorting ZA for
	 * worldmark Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_025_CUI_SearchValidationWorldMArkResortZADifferentCondition(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.verifysortInZAOnlyClubWyndham();
			searchPage.checkSortAZOnlyWorldmark();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * /* Method: tc_026_CUI_SearchFilterValidation Description: CUI Serach
	 * Filter Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_026_CUI_SearchFilterValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				myaccountPage.validateTextLocationField();
				myaccountPage.validateTextDateField();
			}
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.selectUnitType();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifySortInAZ();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * /* Method: tc_027_CUI_SearchValidationSortByCollapseValidation
	 * Description: Search Validation with State entered and sorting AZ for
	 * worldmark Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_027_CUI_SearchValidationSortByCollapseValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.checkSortByCollapse();
			searchPage.filterButtonClickAfterSelection();
			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_028_CUI_SearchValidationWorldMArkResortAZState_CWP
	 * Description: Search Validation with State entered and sorting AZ for
	 * worldmark Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_028_CUI_MembershipDiscountValidation_Silver(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			if (testData.get("discountCheckin").equalsIgnoreCase("Resort")) {
				searchPage.checkDiscountPriceResort();
			} else {
				searchPage.checkDiscountMembershipPrice();
			}

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_029_CUI_MembershipDiscountValidation_Platinum Description:
	 * Validate Gold Member Discount worldmark Validations Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_029_CUI_MembershipDiscountValidation_Platinum(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			if (testData.get("discountCheckin").equalsIgnoreCase("Resort")) {
				searchPage.checkDiscountPriceResort();
			} else {
				searchPage.checkDiscountMembershipPrice();
			}

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_030_CUI_MembershipDiscountValidation_Gold Description:
	 * Validate Gold Member Discount worldmark Validations Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_030_CUI_MembershipDiscountValidation_Gold(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			if (testData.get("discountCheckin").equalsIgnoreCase("Resort")) {
				searchPage.checkDiscountPriceResort();
			} else {
				searchPage.checkDiscountMembershipPrice();
			}

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_031_CUI_changeLocationDateValidate Description: Validate
	 * changed location and date worldmark Validations Date: Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_031_CUI_changeLocationDateValidate(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.changeLocation();
			searchPage.changeCheckinDate();
			searchPage.changeCheckoutDate();
			myaccountPage.selectCalendarDone();
			// searchPage.selectCalendarClose();
			searchPage.validateSearchResult();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_032_CUI_ExactResultClubPassUnitTypeNotAbleToBook Description:
	 * Club Pass Resort in Exact Search and not able to Book any unit type
	 * worldmark Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_032_CUI_ExactResultClubPassUnitTypeNotAbleToBook(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_033_CUI_SearchFilterNoAccessibleUnitValidation_Express
	 * Description: CUI Serach Filter Validation with ZA sorting Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_033_CUI_SearchFilterNoAccessibleUnitValidation_Express(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.filterButtonClick();
			searchPage.checkSortByCollapse();
			searchPage.checkUnitTypeCollapse();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkAccessibleUnitNotPresent();
			searchPage.filterButtonClick();
			searchPage.resetFilter();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_034_CUI_ResortNotEligibleForMemberValidaton Description: CUI
	 * Serach Filter Validation with ZA sorting Date: Mar/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_034_CUI_ResortNotEligibleForMemberValidaton(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.resortNotAbleToBook();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_035_CUI_SeasonedOwnerValidations Description: To verify that
	 * the member logged in is a seasoned owner Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch2"})

	public void tc_035_CUI_SeasonedOwnerValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.seasonedOwnerHeader();
			myAccountPage.ownerWithPoints();
			myAccountPage.ownerWithPointsSearchForm();
			myAccountPage.validateTextLocationField();
			myAccountPage.validateTextDateField();
			myAccountPage.buttonSearchAvailabilityDisabled();
			myAccountPage.ownerWithReservations();

			myAccountPage.leftRailTextValidation();
			myAccountPage.vipStatusValidation();
			myAccountPage.membershipTextValidation();
			myAccountPage.educationTextValidation();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_036_CUI_PendingOwnerValidations Description: To verify that
	 * the member logged in is a Pending owner Date: Apr/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_036_CUI_PendingOwnerValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.newOwnerHeader();
			myAccountPage.ownerWithoutPoints();
			myAccountPage.pendingOwnerSearchForm();
			myAccountPage.ownerWithoutReservationsAndPoints();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_037_CUI_SeasonedOwnerReservationSection Description: To verify
	 * that Upcoming Reservation is displayed properly for seasoned owner Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_037_CUI_SeasonedOwnerReservationSection(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.ownerWithReservations();
			myAccountPage.upcomingReservationSection();
			myAccountPage.mapLinkValidations();
			myAccountPage.reservationLinkValidations();
			myAccountPage.modifyLinkValidations();
			myAccountPage.cancelLinkValidations();
			myAccountPage.reservationSortingAndPaginationCheck();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_038_CUI_DashboardPageSeasonedUser Description: To verify that
	 * Dashboard page is displayed properly for seasoned owner Date: Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_038_CUI_DashboardPageSeasonedUser(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.seasonedOwnerHeader();
			myAccountPage.userNameValidation();
			myAccountPage.imageValidations();
			myAccountPage.vipStatusValidation();
			myAccountPage.memberNumberValidation();
			myAccountPage.viewProfileValidations();

			myAccountPage.membershipTextValidation();
			myAccountPage.contractNumbersValidations();
			myAccountPage.myOwnershipLinkValidations();

			myAccountPage.pointsHeaderValidations();
			myAccountPage.ownerWithPoints();
			myAccountPage.ownerUseYearDates();
			myAccountPage.viewPointLinkValidations();

			myAccountPage.validateTextLocationField();
			myAccountPage.validateTextDateField();
			myAccountPage.buttonSearchAvailabilityDisabled();

			myAccountPage.ownerWithReservations();

			myAccountPage.travelDealsHeaderValidation();
			myAccountPage.travelDealsCardsValidations();

			myAccountPage.wishListHeaderValidations();
			myAccountPage.wishListSectionValidations();

			myAccountPage.educationTextValidation();
			myAccountPage.educationLinksValidations();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_039_CUI_DashboardPageNewUser Description: To verify that
	 * Dashboard page is displayed properly for New owner Date: May/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_039_CUI_DashboardPageNewUser(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.newOwnerHeader();
			myAccountPage.userNameValidation();
			myAccountPage.imageValidations();
			myAccountPage.vipStatusValidation();
			myAccountPage.memberNumberValidation();
			myAccountPage.viewProfileValidations();

			myAccountPage.membershipTextValidation();
			myAccountPage.contractNumbersValidations();
			myAccountPage.myOwnershipLinkValidations();

			myAccountPage.pointsHeaderValidations();
			myAccountPage.ownerWithPoints();
			myAccountPage.ownerUseYearDates();
			myAccountPage.viewPointLinkValidations();

			myAccountPage.validateTextLocationField();
			myAccountPage.validateTextDateField();
			myAccountPage.buttonSearchAvailabilityDisabled();

			myAccountPage.ownerWithoutReservationsButPoints();

			myAccountPage.travelDealsHeaderValidation();
			myAccountPage.travelDealsCardsValidations();

			myAccountPage.wishListHeaderValidations();
			myAccountPage.wishListSectionValidations();

			myAccountPage.newTipsTextValidation();
			myAccountPage.tipsLinksValidations();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_040_CUI_DashboardPagePendingUser Description: To verify that
	 * Dashboard page is displayed properly for Pending owner Date: May/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_040_CUI_DashboardPagePendingUser(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.newOwnerHeader();
			myAccountPage.userNameValidation();
			myAccountPage.imageValidations();
			myAccountPage.vipStatusValidation();
			myAccountPage.memberNumberValidation();
			myAccountPage.viewProfileValidations();

			myAccountPage.membershipTextValidation();
			myAccountPage.contractNumbersValidations();
			myAccountPage.myOwnershipLinkValidations();

			myAccountPage.pointsHeaderValidations();
			myAccountPage.ownerWithoutPoints();
			myAccountPage.pendingUseYearDates();
			myAccountPage.viewPointLinkValidations();

			myAccountPage.pendingOwnerSearchForm();

			myAccountPage.ownerWithoutReservationsAndPoints();

			myAccountPage.travelDealsHeaderValidation();
			myAccountPage.travelDealsCardsValidations();

			myAccountPage.wishListHeaderValidations();
			myAccountPage.wishListSectionValidations();

			myAccountPage.newTipsTextValidation();
			myAccountPage.tipsLinksValidations();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_041_CUI_ResortNotEligibleAndNotPresentForMemberValidaton
	 * Description: CUI Serach Filter Validation with ZA sorting Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_041_CUI_ResortNotEligibleAndNotPresentForMemberValidaton(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.resortNotAbleToBook();
			searchPage.resortSearchedNotFoundValidation();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_042_CUI_InventoryDiscountValidation_Silver Description:
	 * Validate Inventory Discount Reflection worldmark Validations Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_042_CUI_InventoryDiscountValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.inventoryDiscountPriceValidation();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_043_CUI_DiscountPresenceDifferentConditionWithNoInventory
	 * Description: Validate Discount not present if no inventory defined for
	 * more than 2 months Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_043_CUI_DiscountNotPresenceDifferentConditionWithNoInventory(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateDiscountNotPresentWithNoInventory();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_044_CUI_DiscountPresenceDifferentConditionWithInventory
	 * Description: Validate Discount not present if inventory defined for more
	 * than 2 months Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_044_CUI_DiscountPresenceWithInventoryLessThanMembershipDiscount(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateDiscountPresenceWithInventory();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_045_CUI_AlternateSearchWithCity Description: Validate
	 * Alternate Search with City Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_045_CUI_AlternateSearchWithCity(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_046_CUI_AlternateSearchWithCityAndFlexibleSelected
	 * Description: Validate Alternate Search with City selecting flexible dates
	 * Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_046_CUI_AlternateSearchWithCityAndFlexibleSelected(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.flexibleResultNotPresent();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_047_CUI_NoAlternateResultWithExactAndFlexibleResultPresent
	 * Description: Validate Alternate Search with City selecting flexible dates
	 * Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_047_CUI_NoAlternateResultWithExactAndFlexibleResultPresent(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.flexibleSearchResult();
			searchPage.validateAlternateMessageNotPresent();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_048_CUI_NoAlternateResultWithNoExactResultPresentARP
	 * Description: Validate Alternate Search with City selecting flexible dates
	 * Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_048_CUI_NoAlternateResultWithNoExactResultPresentARP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessageNotPresent();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_049_CUI_ClubPassResortNotInAlternateResult Description: Club
	 * Pass Resort Not Present in Alternate Result Date: Apr/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_049_CUI_ClubPassResortNotInAlternateResult(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectFlexDateCheckbox();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			searchPage.clubPassResortNotPresentInAlternateSearch();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_050_CUI_ExactWithNoFlexibleResult Description: Validate Exact
	 * result with no flexible result : Apr/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData")

	public void tc_050_CUI_ExactWithNoFlexibleResult(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultPresence();
			searchPage.flexibleResultPresence();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_051_CUI_ExactWithFlexibleResultSorting Description: Validate
	 * Exact result with flexible result and sorting : Apr/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_051_CUI_ExactWithFlexibleResultSorting(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultPresence();
			searchPage.flexibleResultPresence();
			searchPage.flexibleDatesSorting();
			searchPage.checkInDateMinus3CheckWithoutHold();
			searchPage.checkInDatePlus3CheckWithoutHold();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_052_CUI_NoExactWithFlexibleResultCheck Description: Validate
	 * No Exact result with flexible result when hold is applied : Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_052_CUI_NoExactWithFlexibleResultCheck(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultPresence();
			searchPage.flexibleResultPresence();
			searchPage.flexibleDatesSorting();
			searchPage.flexibleDateCheckWithHold();
			searchPage.flexibleDateOnHoldNotPresentInUI();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_053_CUI_NoExactWithNoFlexibleResultCheck Description: Validate
	 * No Exact result with No flexible result when hold is applied : Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_053_CUI_NoExactWithNoFlexibleResultCheck(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultNotDisplayed();
			searchPage.flexibleResultNotPresent();

			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_054_CUI_ExactResultWithTenOrMoreResorts Description: Validate
	 * No Exact result with No flexible result when hold is applied : Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch3"})

	public void tc_054_CUI_ExactResultWithTenOrMoreResorts(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.validateMoreThanTenResults();
			searchPage.flexibleResultPresence();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_055_CUI_FlexibleResultsWithoutCheckbox Description: Validate
	 * No Exact result with flexible result when hold is applied and check box
	 * not checked : Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_055_CUI_FlexibleResultsWithoutCheckbox(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.exactResultPresence();
			searchPage.flexibleResultPresencePillFormat();
			searchPage.flexibleDatesSortingPill();
			searchPage.flexibleDateCheckWithHoldPillFormat();
			searchPage.flexibleDateOnHoldNotPresentInUIPill();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_056_CUI_NoResortMessageWithNOExactFlexibleAlternateResult
	 * Description: Validate No Result Found Message if No Exact Flexible and
	 * Alternate Result is Found Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By:NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_056_CUI_NoResortMessageWithNOExactFlexibleAlternateResult(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.flexibleDateSelectOption();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.flexibleResultNotPresent();
			searchPage.validateAlternateMessageNotPresent();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_057_CUI_AlternateSearchWithState Description: Validate
	 * Alternate Search with State Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_057_CUI_AlternateSearchWithState(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_058_CUI_verifyClubPassResortinExactSearch Description:Validate
	 * Alternate Verify Club Pass Resort in Exact Search Date: Apr/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_058_CUI_verifyClubPassResortinExactSearch(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_059_CUI_InventoryDiscountValidationOnlyResortSearched
	 * Description: Validate Inventory Discount Reflection and validations for
	 * resort searched Date:Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_059_CUI_InventoryDiscountValidationOnlyResortSearched(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.inventoryDiscountPriceValidationOnlyResortSearched();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_060_CUI_SearchValidationWorldMArkResortDistanceDifferentCondition
	 * Description: Search Validation and sorting AZ for worldmark Validations
	 * Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_060_CUI_SearchValidationWorldMarkResortDistanceDifferentCondition(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.filterButtonClick();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				searchPage.validateFilterHeader();
			}
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			if (testData.get("User_Type").equalsIgnoreCase("Discovery")) {
				searchPage.checkNoWorldMarkResort();
			} else {
				searchPage.checkWorldmarkResortPreseceAtLast();
				searchPage.checkWorldmarkResortAvailableUnitNotPresent();
				searchPage.checkSortAZOnlyWorldmark();
			}
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_061_CUI_SearchFilterValidation_Popular_AccessibleUnit
	 * Description: Date:April/2020 Author: Ajib parida Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})
	public void tc_061_CUI_SearchFilterValidation_Popular_AccessibleUnit(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			if (testData.get("presenceValidations").equalsIgnoreCase("Yes")) {
				myaccountPage.validateTextLocationField();
				myaccountPage.validateTextDateField();
			}
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.verifyFilterAndSortButtonPresence();
			searchPage.validateSearchResult();
			searchPage.filterButtonClick();
			searchPage.validateFilterHeader();
			searchPage.checkSortByCollapse();
			searchPage.checkUnitTypeCollapse();
			searchPage.checkExperiencesTypeCollapse();
			searchPage.selectSort();
			searchPage.includeAccessibleTypeSelect();
			searchPage.filterButtonClickAfterSelection();
			searchPage.verifyResortFound();
			searchPage.checkAccessibleUnit();
			searchPage.filterButtonClick();
			searchPage.resetFilter();
			searchPage.filterButtonClickAfterSelection();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			searchPage.checkWorldmarkResortAvailableUnitNotPresent();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_062_CUI_SearchAvailablitySearchResultsResponseNoResult
	 * Description: Response to load search result validation Date: April/2020
	 * Author: Ajib parida Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})
	public void tc_062_CUI_SearchAvailablitySearchResultsResponseNoResult(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication(strBrowserInUse);
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateProcessingImage();
			searchPage.validateSearchResultZero();
			searchPage.verifyFilterAndSortDisable();
			searchPage.validateHoldLoading();
			searchPage.changeLocation();
			searchPage.exactResultNotDisplayed();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_063_CUI_ValidateUnitDetails Description: Search Availablity
	 * Search Results Unit Details Window Date: April/2020 Author: Ajib parida
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})
	public void tc_063_CUI_ValidateUnitDetails(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			searchPage.clickFirstAvailableUnit();
			searchPage.getUnitTypeAndPrice();
			searchPage.verifyUnitDetails();
			searchPage.verifyRoomAmenities();
			searchPage.validateFloorPlan();
			searchPage.verifyUnitDetailsCloseButton();
			searchPage.verifyUnitNextPrevoiusButton();
			searchPage.verifyUnitDetailsCloseButton();
			searchPage.clickBookButton();
			loginPage.logOutApplication();

		} catch (Exception e) {

		}
	}

	/*
	 * Method: tc_065_CUI_AlternateResultwithFlexWhenFlexCheckboxNotSelected
	 * Description: Validate Alternate Search and Flex ResultPresent even when
	 * flex checkbox is not selected Date: Apr/2020 Author: Abhijeet Roy Changes
	 * By:NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_065_CUI_AlternateResultwithFlexWhenFlexCheckboxNotSelected(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			searchPage.flexResultWhenCheckBoxNotSelected();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * tc_066_CUI_verifyFlexResultPresentWithNoAlternateResultWhenFlexCheckboxNotSelected
	 * Description: Validate No Alternate Result and Flex Result Present even
	 * when flex chcekbox is not selected Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By:NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_066_CUI_verifyFlexResultPresentWithNoAlternateResultWhenFlexCheckboxNotSelected(
			Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessageNotPresent();
			searchPage.flexResultWhenCheckBoxNotSelected();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_067_CUI_verifySearchResultcheckindateLastmonthofBookingWindow
	 * Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_067_CUI_verifySearchResultcheckindateLastmonthofBookingWindow(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.verifyFirstResort();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_068_CUI_searchAvailablitySearchResultsUI Date: Apr/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_068_CUI_searchAvailablitySearchResultsUI(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResult();
			searchPage.verifyFirstResort();
			searchPage.verifyFilterAndSortButtonPresence();
			searchPage.verifyCheckinDateSelected();
			searchPage.verifyCheckoutDateSelected();
			searchPage.validateExperienceInResortCard();
			searchPage.verifyResortNameInEachResortCard();
			searchPage.verifyResortAddressInEachResortCard();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			searchPage.CheckUnitPriceAssociatedWithUnitType();
			searchPage.clickFirstAvailableUnitView();
			searchPage.clickAvailableUnit();
			searchPage.verifyUnitDetailsCloseButton();
			searchPage.hideAllUnitsOpened();
			searchPage.validateMapPresent();
			searchPage.clickMapToogleButton();
			searchPage.CheckBookAssociatedWithUnitType();
			searchPage.clickFirstAvailableUnitView();
			searchPage.clickBookButton();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_069_CUI_SearchValidation_Popular_WorladMarkResortAtLast
	 * Description: search validation with club pass resort at last sorting with
	 * Popular Date:May/2020 Author: Abhijeet Roy Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})
	public void tc_069_CUI_SearchValidation_Popular_WorladMarkResortAtLast(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.verifyFilterAndSortButtonPresence();
			searchPage.filterButtonClick();
			searchPage.validateFilterHeader();
			searchPage.selectSort();
			searchPage.filterButtonClickAfterSelection();
			searchPage.validateSearchResult();
			searchPage.checkWorldmarkResortPreseceAtLast();
			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_070_CUI_AlternateSearchWithMap_NoExactFlexResult Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_070_CUI_AlternateSearchWithMap_NoExactFlexResult(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.exactResultNotDisplayed();
			searchPage.validateAlternateMessagePresent();
			searchPage.validateAlternateResult();
			searchPage.validateMapPresent();
			searchPage.checkFlexDateNotDisplayed();
			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_072_Booking_VIP_Upgrades_Opt_In_To_Future_Upgrades
	 * Description: PHEC-5584 Booking - VIP Upgrades - Opt-In To Future Upgrades
	 * - VIP - OptIn - Instant Upgrade - Platinum VIP Date: June/2020 Author:
	 * Monideep Roychowdhury By: Kamalesh
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_072_Booking_VIP_Upgrades_Opt_In_To_Future_Upgrades(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIBookConfirmationPage_Web bookConfPage = new CUIBookConfirmationPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingReservPage = new CUIUpcomingVacationPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStep();
			upgradePage.verifyAndOptForFutureUpgrade();
			upgradePage.clickContinueButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			/*
			 * reservationBalancePage.validateReservationBalancePage();
			 * reservationBalancePage.clickContinueBooking();
			 */
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookConfPage.navigateToUpcomingVacations();
			upcomingReservPage.validateUpgradeFlag();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method: tc_074_Reservation_VIP_BorrowedPoints Description: Booking -
	 * Complete Reservation - Reservation Charges - VIP Date: June/2020 Author:
	 * Monideep Roychowdhury By: Kamalesh
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_074_Reservation_VIP_BorrowedPoints(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();

			upgradePage.verifyFirstStep();
			upgradePage.optForUpgrade();
			upgradePage.clickContinueButton();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueBooking();
			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();

			reservationBalancePage.selectAndUpdateBorrowPoints();
			reservationBalancePage.clickContinueBooking();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueBooking();

			bookPage.validateBookingHeader("5");
			bookPage.validateReservationBalanceHeader();
			bookPage.validateMembershipChargesHeader();
			bookPage.validateTotalPointsValue();
			bookPage.validateUserPointsValue();

			bookPage.validateBorrowedPointsValue();

			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.validatePaymentcharges();
			bookPage.validatePPCharges();
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.validateAndEnterSpecialRequest();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method: tc_075_Borrow_Housekeeping_Credits_Non_VIP_Add_Guest Description:
	 * Reservation Confirmation - Borrow Housekeeping Credits - Non VIP - Add
	 * Guest Date: June/2020 Author: Monideep Roychowdhury By: KG
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch4"})

	public void tc_075_Borrow_Housekeeping_Credits_Non_VIP_Add_Guest(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Android(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.viewProfileValidations();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButtonOfHigherCostThanAccountPoints();
			travelPage.validateDefaultUserName();
			travelPage.selectAndPopulateGuestInfo();
			travelPage.clickContinueBooking();
			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.retrieveAmountNeeded();
			reservationBalancePage.selectAndUpdateRentPoints();
			reservationBalancePage.retrieveHouseKeepingCreditNeeded();
			reservationBalancePage.selectAndUpdateHouseKeepingBorrowPoints();
			reservationBalancePage.buyReservationTransaction();
			reservationBalancePage.clickContinueBooking();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueBooking();

			bookPage.validateBookingHeader("4");
			bookPage.validatePaymentsection();
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			bookingConfPage.validateConfirmationHeader();
			bookingConfPage.validateReservationSummary();
			bookingConfPage.validateBookingDetails();
			bookingConfPage.validateChargesSummary();
			bookingConfPage.validatePaymentCharges();
			bookingConfPage.validateReservationCharges();
			bookingConfPage.validateHousekeepingCreditChargesAbsence();
			bookingConfPage.validateCancellationPolicy();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method: tc_097_CUI_VerifyReservationConfirmationPage Description:
	 * Alternate Search with MAP when no exact and flex result present Date:
	 * Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_097_CUI_VerifyReservationConfirmationPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Android(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.fetchMemberName();
			accountSettingPage.fetchOwnerEmail();

			myaccountPage.navigateToDashBoardPage();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.verifyOwnerNamePrepopulated();
			travelPage.verifyOwnerEmailPrepopulated();
			travelPage.selectAnotherValueFromDropdown();
			travelPage.selectAndPopulateGuestInfo();
			travelPage.clickContinueBooking();

			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.selectCheckBox_ReservationBalanace("BorrowPoint");
			reservationBalancePage.borrowValidPoint();
			reservationBalancePage.clickContinueBooking();

			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueBooking();

			bookPage.verifyCompleteBookingPage();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			bookingConfPage.validateConfirmationHeader();
			bookingConfPage.validateReservationSummaryDetails();
			bookingConfPage.validateBookingDetails();
			bookingConfPage.validateChargesSummaryForBorrowingPoint();
			bookingConfPage.validateCancellationPolicy();

			loginPage.logOutApplication();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method:
	 * tc_098_CUI_VerifyReservationConfirmationPageForVIPGoldMemberAndInstantUpgrade
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_098_CUI_VerifyReservationConfirmationPageForVIPGoldMemberAndInstantUpgrade(
			Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Android(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Android(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			// upgradePage.verifyFirstStepOnPageLoad();
			upgradePage.selectFirstUpgradeOption();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.selectGuestOption();
			travelPage.selectAndPopulateGuestInfo();
			travelPage.clickContinueBooking();

			// reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.selectCheckBox_ReservationBalanace("RentPoint");
			reservationBalancePage.rentValidPoint();
			reservationBalancePage.clickContinueBooking();

			// pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueBooking();

			// bookPage.verifyCompleteBookingPage();
			bookPage.validateAndEnterSpecialRequest();
			bookPage.selectPaymentMethod();
			bookPage.paymentViaPaypal();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			bookingConfPage.validateConfirmationHeader();
			bookingConfPage.validateReservationSummaryDetails();
			bookingConfPage.validateBookingDetails();
			bookingConfPage.validateInstantUpgradeDetails();
			bookingConfPage.validateSpecialRequestSection();
			bookingConfPage.validateChargesSummaryForRentingPoint();
			bookingConfPage.validatePaymentChargesForPaypalPayment();
			bookingConfPage.validateRentedPointUnderPaymentCharges();
			bookingConfPage.validatePointProtectionUnderPaymentCharges("YES");
			bookingConfPage.validateCancellationPolicy();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method: tc_099_CUI_ModifyReservation_ModificationHistory_PointsProtection
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: July/2020 Author: Kamalesh Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_099_CUI_ModifyReservation_ModificationHistory_PointsProtection(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.checkPointProtectionSection();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();

			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.verifyGuestInformationText();
			modifyTravelerPage.dataEntryForGuestOwner();
			modifyTravelerPage.verifyTextGuestConfirmationCredit();
			modifyTravelerPage.verifyGuestConfirmationCreditDetails();
			modifyTravelerPage.verifyGuestCreditRequiredText();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.verifyPaymentCharges();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyChargesSummarySection();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method:
	 * tc_100_CUI_ModifyReservation_OverlappingReservation_WarningMessage
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_100_CUI_ModifyReservation_OverlappingReservation_WarningMessage(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();

			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.clickContinueButton();
			modifyTravelerPage.verifyOverlappingErrorMsg();
			modifyTravelerPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();

			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method: tc_102_CUI_Seasoned_PointsSummaryPage Description: Seasoned Owner
	 * Points Summary Page page validations Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_102_CUI_Seasoned_PointsSummaryPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIPointSummaryPage_Web pointsSummaryPage = new CUIPointSummaryPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointsSummaryPage.navigateToPointsSummaryPage();
			pointsSummaryPage.pointSummaryPageSection();
			pointsSummaryPage.currentUseYearSection();
			pointsSummaryPage.currentUseYearDateFormat();
			pointsSummaryPage.currentUseYearPoints();
			pointsSummaryPage.houseKeepingCreditsSection();
			pointsSummaryPage.houseKeepingPoints();

			pointsSummaryPage.reservationTransactionHeader();
			pointsSummaryPage.reservationTransactionLeft();
			pointsSummaryPage.confirmationCreditHeader();
			pointsSummaryPage.confirmationCreditLeft();

			pointsSummaryPage.futureUseYearAccordion("true");
			pointsSummaryPage.futureUseYearAccordionContent();
			pointsSummaryPage.futureUseYearDateFormat();
			pointsSummaryPage.accordionUseYearPoints();
			pointsSummaryPage.accordionHousekeepingPoints();
			pointsSummaryPage.futureUseYearAccordion("false");

			pointsSummaryPage.defaultTransactionHistory();
			pointsSummaryPage.selectUseYearTransaction(testData.get("pastUseYear"));
			pointsSummaryPage.tableHeaderValidation();
			pointsSummaryPage.pointSortValidation();
			pointsSummaryPage.dateSortValidation();
			pointsSummaryPage.tableTypeFilterValidation();
			pointsSummaryPage.expanderValidations();
			pointsSummaryPage.totalPointsValidation();
			pointsSummaryPage.selectUseYearTransaction(testData.get("futureUseYear"));
			pointsSummaryPage.selectUseYearTransaction(testData.get("currentUseYear"));

			pointsSummaryPage.pointsEducationLinks();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();

		}
	}

	/*
	 * Method: tc_104_CUI_BucketListPageValidations Description: Bucket List
	 * Page Validations Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_104_CUI_BucketListPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIBucketListPage_Web bucketListPage = new CUIBucketListPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			bucketListPage.navigateToBucketListPage();
			bucketListPage.resortCardsNumbers();
			bucketListPage.resortCardContentValidations();
			bucketListPage.resortCardNavigation_UJ();
			bucketListPage.whereToNextCard();
			bucketListPage.exploreCTANavigation();
			bucketListPage.wishListedCardValidation();
			bucketListPage.removeWishlishtedCard();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();

		}
	}

	/*
	 * Method: tc_105_CUI_NewOwnerTipsValidation Description: New owner (With or
	 * Without Points) owner Tips Validation Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_105_CUI_NewOwnerTipsValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myAccountPage.newOwnerGetStartedTips();
			myAccountPage.newTipsTextValidation();
			myAccountPage.tipsLinksValidations();
			myAccountPage.tipsModalValidations();
			myAccountPage.newOwnerCompletedTips();
			myAccountPage.newTipsCompleteTextValidation();
			myAccountPage.tipCheckMarkedValidation();
			myAccountPage.allTipCompleteValidation();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();

		}
	}

	/*
	 * Method: tc_106_CUI_ReservationCancellationPrior15Days Description:Cancel
	 * Reservation Flow Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_106_CUI_ReservationCancellationPrior15Days(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Android(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadAllReservations();
			upcomingVacation.selectReservationToCancel();

			cancelReservation.reservationNumberCheck();
			cancelReservation.reservationResortName();
			cancelReservation.checkInDateCheck();
			cancelReservation.checkOutDateCheck();
			cancelReservation.reimbursementSectionHeader();
			cancelReservation.forfeitSummaryNotPresent();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.pointsDropDownValidation();
			cancelReservation.houseKeepingDropDownValidation();
			cancelReservation.paymentRemimbursementSectionValidation();
			cancelReservation.cancellationSection();
			cancelReservation.cancelButtonClick();

			reservationDetails.cancelledReservationName();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.modifyLinkNotPresent();
			reservationDetails.cancelLinkNotPresent();
			reservationDetails.reimbursedCharges();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.reimbursedPayment();
			reservationDetails.chargesTextIteration();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			cleanUp();

		}
	}

	/*
	 * Method: tc_107_CUI_CancellationForfeitSummary Description:Cancel
	 * Reservation Flow with Forfeit summary Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_107_CUI_CancellationForfeitSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Android(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadAllReservations();
			upcomingVacation.selectReservationToCancel();

			cancelReservation.reservationNumberCheck();
			cancelReservation.reservationResortName();
			cancelReservation.checkInDateCheck();
			cancelReservation.checkOutDateCheck();
			cancelReservation.forfeitSectionHeader();
			cancelReservation.reimburseSummaryNotPresent();
			cancelReservation.forfeitChargesSection();
			cancelReservation.pointsDropDownValidation();
			cancelReservation.houseKeepingDropDownValidation();
			cancelReservation.paymentRemimbursementSectionValidation();
			cancelReservation.cancellationSection();
			cancelReservation.cancelButtonClick();

			reservationDetails.cancelledReservationName();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.modifyLinkNotPresent();
			reservationDetails.cancelLinkNotPresent();
			reservationDetails.forfeitedCharges();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.reimbursedPayment();
			reservationDetails.pointsSummaryLink();

			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();

		}
	}

	/*
	 * Method: tc_110_CUI_MultipleModificationsDetailsPageValidation
	 * Description:Multiple Modification in a reservation validations Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch5"})

	public void tc_110_CUI_MultipleModificationsDetailsPageValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingVacation.navigateToUpcomingVacation();
			upcomingVacation.loadAllReservations();
			upcomingVacation.selectReservationDetails();

			reservationDetails.validateBookingDetails();
			reservationDetails.mapLinkValidations();
			reservationDetails.resortCardNavigation();
			reservationDetails.validateCancelModifyLink();
			reservationDetails.cancelationPolicy();
			reservationDetails.fullCancellationPolicy();
			reservationDetails.reservationSummaryTraveler();
			reservationDetails.validateReservationDetails();
			reservationDetails.validateSuiteDetails();
			reservationDetails.validateReservation();

			reservationDetails.verifyHeaderChargesSummary();
			reservationDetails.verifyHeaderMembershipCharges();
			reservationDetails.totalPointsValidation();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.totalReservationTransaction();
			reservationDetails.totalHousekeepingValidation();
			if (testData.get("PaymentReimburse").equalsIgnoreCase("Yes")) {
				reservationDetails.paymentInformation();
				reservationDetails.matchTotalPay();
			}
			reservationDetails.validateHeaderModification();
			reservationDetails.verifyModificationSortingDate();
			reservationDetails.validateTableModificationHistory();
			reservationDetails.validateCollapsedModifications();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();

		}
	}

	/*
	 * Method: tc_111_CUI_CanceledReservationPageValidation Description:Canceled
	 * Reservation Page validations Date: June/2020 Author: Unnat Jain Changes
	 * By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_111_CUI_CanceledReservationPageValidation(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIPointSummaryPage_Web pointsSummaryPage = new CUIPointSummaryPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointsSummaryPage.navigateToPointsSummaryPage();
			pointsSummaryPage.cancelReservationExpanderSelect();

			reservationDetails.validateBookingDetails();
			reservationDetails.mapLinkValidations();
			reservationDetails.resortCardNavigation();
			reservationDetails.modifyLinkNotPresent();
			reservationDetails.cancelLinkNotPresent();
			reservationDetails.cancelationPolicyNotPresent();
			reservationDetails.reservationSummaryTraveler();
			reservationDetails.validateReservationDetails();
			reservationDetails.validateSuiteDetails();
			reservationDetails.validateReservation();

			reservationDetails.verifyHeaderReimbursementSummary();
			reservationDetails.totalPointsValidation();
			reservationDetails.pointsDropDownValidation();
			reservationDetails.totalHousekeepingValidation();
			if (testData.get("PaymentReimburse").equalsIgnoreCase("Yes")) {
				reservationDetails.paymentInformation();
				reservationDetails.matchTotalPay();
			}

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();

		}
	}

	/*
	 * Method: tc_112_CUI_BookingFlowWithDetailsPageValidations
	 * Description:Booking a reservation and verify the details page Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_112_CUI_BookingFlowWithDetailsPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		String totalPoints;
		String reservationNumber;
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacation = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			bookPage.clickContinueButtonForBooking();
			bookPage.validateMembershipCharges();
			totalPoints = bookPage.getTotalPoints();
			bookPage.validateHousekeepingCharges();
			bookPage.validateReservationTransactionCharges();
			bookPage.verifyPaymentChargesSectionNotPresent();
			bookPage.verifyPaymentMethodHeaderNotPresent();
			bookPage.verifyPaymentOptionsNotPresent();
			bookPage.clickBookNowButton();
			reservationNumber = bookPage.reservationNumber();

			upcomingVacation.clickMyReservationLink();
			upcomingVacation.loadAllReservations(reservationNumber);
			upcomingVacation.selectReservationDetails(reservationNumber);

			reservationDetails.validateBookingDetails();
			reservationDetails.mapLinkValidations();
			reservationDetails.resortCardNavigation();
			reservationDetails.validateCancelModifyLink();
			reservationDetails.cancelationPolicy();
			reservationDetails.fullCancellationPolicy();
			reservationDetails.reservationSummaryTraveler();
			reservationDetails.validateReservationDetails();
			reservationDetails.pointsProtectionNotPresent();
			reservationDetails.validateSuiteDetails();
			reservationDetails.validateReservation();

			reservationDetails.verifyHeaderChargesSummary();
			reservationDetails.verifyHeaderMembershipCharges();
			reservationDetails.totalPointsValidation(totalPoints);
			reservationDetails.pointsDropDownValidation();
			reservationDetails.totalReservationTransaction();
			reservationDetails.totalHousekeepingValidation();
			if (testData.get("PaymentReimburse").equalsIgnoreCase("Yes")) {
				reservationDetails.paymentInformation();
				reservationDetails.matchTotalPay();
			}

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			cleanUp();

		}
	}

	/*
	 * Method: tc_115_CUI_ModifyReservation_OwnerToOwner Description: Modify
	 * Reservation from Owner to Owner Date: Apr/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_115_CUI_ModifyReservation_OwnerToOwner(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyTravelerPage_Web modifyTravelerPage = new CUIModifyTravelerPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyCardDetails();
			modifyPage.listModificationHistory();
			modifyPage.dateOldestToNewestModificationTable();
			modifyPage.checkModificationHistoryCollapsedMode();
			modifyPage.vaildateHeaderDateinModificationHistory();
			modifyPage.checkDateInModificationHistory();
			modifyPage.vaildateHeaderModificationTypeinModificationHistory();
			modifyPage.checkModificationTypeInModificationHistory();
			modifyPage.checkSectionReservationSummary();
			modifyPage.checkReservationSummaryDetails();
			modifyPage.clickModifyTravelerButton();

			modifyTravelerPage.checkStepsModify();
			modifyTravelerPage.textStepModifyTraveler();
			modifyTravelerPage.checkCurrentTraveler();
			modifyTravelerPage.textWhoWillBeCheckingIn();
			modifyTravelerPage.checkModifyOwnerGuestRadioPresence();
			modifyTravelerPage.selectRadioModifyTraveler();
			modifyTravelerPage.selectOwnerfromList();
			modifyTravelerPage.backToModifyReservation();
			modifyTravelerPage.validateSectionReservationSummary();
			modifyTravelerPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.textTravelerInfo();
			modifyCompletePage.verifyCurrentOwner();
			modifyCompletePage.verifyRevisedTraveler();
			modifyCompletePage.validateCancelPolicy();
			modifyCompletePage.verifyFullCancelPolicy();
			modifyCompletePage.validateSectionReservationSummary();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textModifyTravelConfirm();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.textModifyTravelerConfirm();
			modifySuccessPage.textModificationSummary();
			modifySuccessPage.textModificationDetails();
			modifySuccessPage.verifyModifiedName();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifySectionCancel();
			modifySuccessPage.verifyReservationDetailsCTA();
			modifySuccessPage.verifyMakeAnotherModificationLink();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.checkOwnerModifiedAfterModification();

			loginPage.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// cleanUp();
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method: tc_120_CUI_PaypalCharges_ReservationSummary Description: Paypal
	 * Charges Verfication Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_120_CUI_PaypalCharges_ReservationSummary(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationDetails();
			reservationDetails.verifySectionReservationSummary();
			reservationDetails.verifyHeaderPaymentChargesSectionDisplayed();
			reservationDetails.accountPaypalVerification();
			reservationDetails.verifyAmountsPaid();
			reservationDetails.verifyTotalAmountDisplayedAndCalculated();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_125_CUI_RCI_PointDeposit_PurchaseHousekeeping_SingleUseYear
	 * Description: RCI point deposit single use year purchasing Housekeeping
	 * Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_125_CUI_RCI_PointDeposit_PurchaseHousekeeping_SingleUseYear(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUI_RCIAndDepositPage_Web RCIandDepositPage = new CUI_RCIAndDepositPage_Android(tcconfig);
		CUIPointDepositPage_Web RCIPointDepositPage = new CUIPointDepositPage_Android(tcconfig);
		CUICompleteDepositPage_Web completeDepositPage = new CUICompleteDepositPage_Android(tcconfig);
		CUIDepositSuccessPage_Web successDepositPage = new CUIDepositSuccessPage_Android(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			RCIandDepositPage.navigateToDepositandRCIPage();
			RCIandDepositPage.clickCTARCIPointDeposit();
			RCIPointDepositPage.verifyProgressBar();
			RCIPointDepositPage.verifyBackRCIandDepositCTA();
			RCIPointDepositPage.verifyTextSelectDeposit();
			RCIPointDepositPage.verifyRCIPointChart();
			RCIPointDepositPage.verifyHeaderTotalDepositAndUseYear();
			RCIPointDepositPage.totalUseYear();
			RCIPointDepositPage.verifyPointAvailableRCIDeposit();
			RCIPointDepositPage.verifyHousekeepingCreditRCIDeposit();
			RCIPointDepositPage.selectUseYearRCIDeposit();
			RCIPointDepositPage.verifyHeaderPointToDeposit();
			RCIPointDepositPage.totalPointsToDeposit();
			RCIPointDepositPage.verifyHousekeepingSectionRCIDeposit();
			RCIPointDepositPage.verifyHousekeepingCreditPurchased();
			RCIPointDepositPage.verifyCostHouskeepingPurchasedDeclaration();
			RCIPointDepositPage.verifyTotalCostHouskeepingPurchased();
			RCIPointDepositPage.clickContinueButton();
			completeDepositPage.headerCompleteDeposit();
			completeDepositPage.verifySectionDepositDetails();
			completeDepositPage.verifyTotalPoints();
			completeDepositPage.validateTotalPointsBreakdown();
			completeDepositPage.verifyHousekeepingCredit();
			completeDepositPage.validateHousekeepingcreditBreakdown();
			completeDepositPage.verifyReservationTransaction();
			completeDepositPage.verifyHeaderPaymentCharges();
			completeDepositPage.verifyPaymentForRCIDeposit();
			completeDepositPage.totalChargesDueVerification();
			completeDepositPage.selectPaymentMethod();
			completeDepositPage.dataEntryForCreditCard();
			completeDepositPage.dataEntryEmail();
			completeDepositPage.clickConfirmDeposit();
			successDepositPage.verifyDepositConfirmation();
			successDepositPage.validateDepositDetails();
			successDepositPage.verifyTotalPointsSuccess();
			successDepositPage.validateTotalPointsBreakdownSuccess();
			successDepositPage.verifyHousekeepingCredit();
			successDepositPage.validateHouskeepingBreakdownSuccess();
			successDepositPage.validateReservationTransactionSuccess();
			successDepositPage.validateChargesRequiredForBreakDown();
			successDepositPage.validateTotalValueCharged();
			successDepositPage.validatePaidByCreditCard();
			loginPage.logOutApplication();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_127_CUI_PaymentwithPayPal_OwnershipPage Description: Ownership
	 * Page Payment with Paypal Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_127_CUI_PaymentwithPayPal_OwnershipPage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Android(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Android(tcconfig);
		CUIPaymentAmountPage_Web paymentAmountPage = new CUIPaymentAmountPage_Android(tcconfig);
		CUICompletePaymentPage_Web paymentCompletePage = new CUICompletePaymentPage_Android(tcconfig);
		CUILoanAssessmentSuccessPage_Web loanAssesmentSuccessPage = new CUILoanAssessmentSuccessPage_Android(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myOwnershipPage.navigateToMyOwnershipPage();
			myOwnershipPage.financialDetailsHeader();
			myOwnershipPage.makePaymentHeader();
			myOwnershipPage.speedPayLinkPresence();
			myOwnershipPage.clickPayPalLink();
			ownershipPayPal.validateLinkBackToOwnership();
			ownershipPayPal.verifyHeaderPaymentType();
			ownershipPayPal.validatePaymentTypeTextAndField();
			ownershipPayPal.checkOptionsInPaymentTypeField();
			ownershipPayPal.selectPaymentType();
			ownershipPayPal.contractsInAscendingOrder();
			ownershipPayPal.verifyCurrentDueInContractCard();
			ownershipPayPal.verifyDueDateInContractCard();
			ownershipPayPal.selectContract();
			ownershipPayPal.clickContinueButton();
			paymentAmountPage.validateHeaderPaymentAmount();
			paymentAmountPage.selectCheckBoxCurrentBalance();
			paymentAmountPage.addAdditionalPoint();
			paymentAmountPage.clickContinueButton();
			paymentCompletePage.verifyHeadersinCompletePaymentPage();
			paymentCompletePage.verifyContractNumber();
			paymentCompletePage.totalPaymentBreakdown();
			paymentCompletePage.validateTotalChargeDue();
			paymentCompletePage.paymentViaPaypal();
			paymentCompletePage.clickContinueButton();
			loanAssesmentSuccessPage.verifyHeadersInSuccessPage();
			loanAssesmentSuccessPage.verifyContractNumber();
			loanAssesmentSuccessPage.verifyPayPalPaymentAccount();
			loanAssesmentSuccessPage.totalPaymentBreakdown();
			loanAssesmentSuccessPage.validateTotalChargeDue();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			cleanUp();

		}
	}

	/*
	 * Method: tc_135_CUI_ModifiedReservationTo14Nights Description: Verify
	 * Modifed Reservation to 14 Nights Date: Aug/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_135_CUI_ModifiedReservationTo14Nights(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);

		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.optionInstantUpgrade();
			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckOutDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckOutDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyPage.unableToModifyHeader();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			loginPage.logOutApplicationViaDashboard();
		}
	}

	/*
	 * Method: tc_145_CUI_VerifyRatePlanInDatabase_NightModification
	 * Description: Verify that if CWP Standard member modifies check in date
	 * for a reservation having existing reservation's check-in date on first
	 * day of standard booking window, rate plan will be chnaged Date: Aug/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_145_CUI_VerifyRatePlanInDatabase_NightModification(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();

			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();

			upgradePage.optionInstantUpgrade();

			travelPage.fillTravellerInformation();
			travelPage.clickContinueButton();

			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueButton();

			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookPage.checkRatePlanInDB();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.checkRatePlanInDBAfterModification();
			modifySuccessPage.verifyRatePlanInDBBeforeAndAfterModification();
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			loginPage.logOutApplicationViaDashboard();
		}
	}

	/*
	 * Method: tc_151_CUI_AddNights_PP_Change_49_99_ORC_Member Description: Trip
	 * Add Nights scenarios Test Case : TC026_HCWMP-131_ORC member can modify
	 * check-in date with point protection when modification changes the point
	 * protection tier from $49 to $99_ standard window Date: Aug/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_151_CUI_AddNights_PP_Change_49_99_ORC_Member(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();

			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();

			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.clickContinueButton();

			modifyNightsPage.validatePPsectionSelected("$50.00");
			modifyNightsPage.clickContinueButton();

			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();

			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection("Accepted");
			modifySuccessPage.navigateToReservationDetailsPage();

			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection("Accepted");
			reservationDetails.verifyPointsProtectionModification("$99.00");

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			loginPage.logOutApplicationViaDashboard();
		}
	}

	/*
	 * Method:
	 * tc_156_CUI_AddNights_ModifiedReservationCheckOutDate_WithRentBorrowHK
	 * Description: Modified Reservation Check Out Date with Rent & Borrow HK
	 * Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_156_CUI_AddNights_ModifiedReservationCheckOutDate_WithRentBorrowHK(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUIPointSummaryPage_Web pointSummaryPage = new CUIPointSummaryPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Android(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Android(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			pointSummaryPage.navigateToPointSummary();
			pointSummaryPage.preFutureUseYearPointsValue();
			pointSummaryPage.preFutureUseYearHKValue();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckOutDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckOutDate();
			modifyNightsPage.clickContinueButton();
			reservationBalancePage.setRentBorrowPoint();
			reservationBalancePage.setRentBorrowHK();
			reservationBalancePage.clickContinueBooking();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.verifyPaymentCharges_RentedPoint();
			modifyCompletePage.verifyPaymentCharges_PurchaseHK();
			modifyCompletePage.makePayment();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPaymentCharges_RentedPoint();
			modifySuccessPage.verifyPaymentCharges_HK();
			modifySuccessPage.verifyMembershipCharges_BorrowRentPoint();
			modifySuccessPage.getPointUsedForReservation("Future");
			modifySuccessPage.verifyBorrowedHK();
			modifySuccessPage.getHKUsedForReservation("Future");
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckOut();
			reservationDetails.verifyModifiedBy();
			reservationDetails.pointsSummaryLink();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			reservationDetails.pointsSummaryLink();
			pointSummaryPage.postFutureUseYearPointsValue();
			pointSummaryPage.postFutureUseYearHKValue();
			pointSummaryPage.validatePointsAfterCancellationForFutureUseYear();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			loginPage.logOutApplicationViaDashboard();
		}
	}

	/*
	 * Method: tc_158_CUI_AddNights_VerifyOverlappingMessage Description: Verify
	 * Modifed Reservation (Next Use Year check in) deducts points from Current
	 * use Year Date: Aug/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups={"CUI","regression","mobile","android", "batch6"})

	public void tc_158_CUI_AddNights_VerifyOverlappingMessage(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Android(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Android(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Android(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Android(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Android(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Android(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Android(tcconfig);
		CUIModifyNightsPage_Web modifyNightsPage = new CUIModifyNightsPage_Android(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Android(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Android(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Android(tcconfig);
		try {
			loginPage.launchApplication();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.fetchMemberName();
			accountSettingPage.fetchOwnerEmail();

			myaccountPage.navigateToDashBoardPage();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			travelPage.verifyOwnerNamePrepopulated();
			travelPage.selectAnotherValueFromDropdown();
			travelPage.clickContinueButton();
			pointProtectionPage.selectPointProtection();
			bookPage.textCompleteBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();

			myaccountPage.navigateToDashBoardPage();

			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.verifyModifyNightsCardDetails();
			modifyPage.getCheckInDateFromAddNightsCard();
			modifyPage.clickModifyNightsButton();
			modifyNightsPage.modifyCheckInDate();
			modifyNightsPage.verifyOverlappingErrorMsg();
			modifyNightsPage.clickContinueButton();
			modifyNightsPage.selectPointProtection();
			modifyNightsPage.clickContinueButton();
			modifyCompletePage.textReviewCharges();
			modifyCompletePage.updatedDateValidation("ModifyCheckInDate");
			modifyCompletePage.totalPointsRequired();
			modifyCompletePage.buttonConfirmCharges();
			modifySuccessPage.textCongratsVerify();
			modifySuccessPage.verifyModifiedBy();
			modifySuccessPage.verifyModifiedDate();
			modifySuccessPage.updatedDateValidation("ModifyCheckInDate");
			modifySuccessPage.totalPointsDeducted();
			modifySuccessPage.verifyPointsProtection();
			modifySuccessPage.navigateToReservationDetailsPage();
			reservationDetails.verifyReservationNumber();
			reservationDetails.clickAddNightsHistory();
			reservationDetails.verifyNoOfNights_CheckIn();
			reservationDetails.verifyModifiedBy();
			reservationDetails.verifyPointsProtection();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			loginPage.logOutApplicationViaDashboard();
		}
	}
}