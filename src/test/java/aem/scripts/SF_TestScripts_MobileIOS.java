package aem.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import aem.pages.CancelConfirmationPage;
import aem.pages.CancelConfirmationPage_IOS;
import aem.pages.ExistingTourPage;
import aem.pages.ExistingTourPage_IOS;
import aem.pages.LandingPage;
import aem.pages.LandingPage_IOS;
import aem.pages.ModifyTourPage;
import aem.pages.ModifyTourPage_IOS;
import aem.pages.OfferPage;
import aem.pages.OfferPage_IOS;
import aem.pages.PaymentPage;
import aem.pages.PaymentPage_IOS;
import aem.pages.SF_LoginPage;
import aem.pages.SF_LoginPage_IOS;
import aem.pages.TourConfirmationPage;
import aem.pages.TourConfirmationPage_IOS;
import aem.pages.TourSlotPage;
import aem.pages.TourSlotPage_IOS;
//import Destinations.scripts.DestinationsScripts_IOS;
import automation.core.TestBase;

public class SF_TestScripts_MobileIOS extends TestBase {

	public static final Logger log = Logger.getLogger(SF_TestScripts_MobileIOS.class);

	/*
	 * Function/event:
	 * AEM_01_Header_Footer_PositiveConfirmation_ReloginPageValidation Author:
	 * Syba Mallick Description: Verify Footer and Terms and condition link in
	 * all page
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem", "mobile", "ios" })
	public void AEM_01_Header_Footer_PositiveConfirmation_ReloginPageValidation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage_IOS(tcconfig);
		LandingPage landingPage = new LandingPage_IOS(tcconfig);
		OfferPage offerPage = new OfferPage_IOS(tcconfig);
		TourSlotPage tourSlotPage = new TourSlotPage_IOS(tcconfig);
		PaymentPage paymentPage = new PaymentPage_IOS(tcconfig);
		TourConfirmationPage tourConfirmationPage = new TourConfirmationPage_IOS(tcconfig);
		ExistingTourPage existingTourPage = new ExistingTourPage_IOS(tcconfig);
		try {

			login.launchUrl(strEnvironment);
			// Added as a Part of Video Player Validation
			/* Start */
			// landingPage.videoPlayerValidation();
			/* End */
			landingPage.verifyFooterAndTCLandingPage();
			landingPage.verifyTermsAndPrivacyLink();
			login.launchUrl(strEnvironment);
			landingPage.verifyOnclickGetStartedButton();
			landingPage.clickGetStartedBtn();
			offerPage.openTripSummary();
			offerPage.closeTripSummary();
			offerPage.selectPremium();
			offerPage.clickContinueOffers();
			tourSlotPage.clickTourDateBox();
			tourSlotPage.selectTourDate();
			tourSlotPage.selectTourTime();
			tourSlotPage.scrollDownTerms();
			tourSlotPage.clickTermsCheckBox();
			tourSlotPage.clickContinueTourSlot();
			paymentPage.providePaymentDetails();
			paymentPage.clickContinuePaymentPage();
			tourConfirmationPage.verifyConfirmationPageHeader();
			login.launchUrl(strEnvironment);
			landingPage.verifyOnclickGetStartedButton();
			landingPage.clickGetStartedBtn();
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyCancelHereLink();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: AEM_02_Update_CancelExistingTourScreenValidation Author:
	 * Syba Mallick Description: Cancel,Modify and Update existing tour flow
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem", "mobile", "ios" })
	public void AEM_02_Update_CancelExistingTourScreenValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage_IOS(tcconfig);
		LandingPage landingPage = new LandingPage_IOS(tcconfig);
		ExistingTourPage existingTourPage = new ExistingTourPage_IOS(tcconfig);
		ModifyTourPage modifyTourPage = new ModifyTourPage_IOS(tcconfig);
		TourSlotPage tourSlotPage = new TourSlotPage_IOS(tcconfig);
		CancelConfirmationPage cancelConfirmationPage = new CancelConfirmationPage_IOS(tcconfig);
		TourConfirmationPage TourConfirmationPage = new TourConfirmationPage_IOS(tcconfig);
		try {
			// login.clearCache();
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyExistingPremiumDetails();
			existingTourPage.clickEditTourLink();
			tourSlotPage.clickTourDateBox();
			String strTourDate = modifyTourPage.selectUpdatedTourDate();
			String strTourTime = modifyTourPage.selectUpdatedTourTime();
			modifyTourPage.clickSaveChanges();
			TourConfirmationPage.verifyConfirmationPageHeader();
			TourConfirmationPage.verifyScheduleDateAndTimeTourConfirmationPage(strTourDate, strTourTime);
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyCancelHereLink();
			existingTourPage.clickCancelHere();
			existingTourPage.clickYes();
			cancelConfirmationPage.verifyCancelConfirmationPageHeader();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

}
