package aem.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import aem.pages.CancelConfirmationPage;
import aem.pages.CancelConfirmationPage_Android;
import aem.pages.ExistingTourPage;
import aem.pages.ExistingTourPage_Android;
import aem.pages.LandingPage;
import aem.pages.LandingPage_Android;
import aem.pages.ModifyTourPage;
import aem.pages.ModifyTourPage_Android;
import aem.pages.OfferPage;
import aem.pages.OfferPage_Android;
import aem.pages.PaymentPage;
import aem.pages.PaymentPage_Android;
import aem.pages.SF_LoginPage;
import aem.pages.SF_LoginPage_Android;
import aem.pages.TourConfirmationPage;
import aem.pages.TourConfirmationPage_Android;
import aem.pages.TourSlotPage;
import aem.pages.TourSlotPage_Android;
//import Destinations.scripts.DestinationsScripts_Android;
import automation.core.TestBase;

public class SF_TestScripts_MobileAndroid extends TestBase {

	public static final Logger log = Logger.getLogger(SF_TestScripts_MobileAndroid.class);

	/*
	 * Function/event:
	 * AEM_01_Header_Footer_PositiveConfirmation_ReloginPageValidation Author:
	 * Syba Mallick Description: Verify Footer and Terms and condition link in
	 * all page
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem", "mobile", "android" })
	public void AEM_01_Header_Footer_PositiveConfirmation_ReloginPageValidation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage_Android(tcconfig);
		LandingPage landingPage = new LandingPage_Android(tcconfig);
		OfferPage offerPage = new OfferPage_Android(tcconfig);
		TourSlotPage tourSlotPage = new TourSlotPage_Android(tcconfig);
		PaymentPage paymentPage = new PaymentPage_Android(tcconfig);
		TourConfirmationPage tourConfirmationPage = new TourConfirmationPage_Android(tcconfig);
		ExistingTourPage existingTourPage = new ExistingTourPage_Android(tcconfig);
		try {
			login.launchUrl(strEnvironment);
			// Added as a Part of Video Player Validation
			/* Start */
			// landingPage.videoPlayerValidation();
			/* End */
			landingPage.verifyFooterAndTCLandingPage();
			landingPage.verifyTermsAndPrivacyLink();
			landingPage.enterReservationDetails();
			landingPage.clickGetStartedBtn();
			offerPage.selectPremium();
			offerPage.clickContinueOffers();
			tourSlotPage.selectTourDate();
			tourSlotPage.selectTourTime();
			tourSlotPage.scrollDownTerms();
			tourSlotPage.clickTermsCheckBox();
			tourSlotPage.clickContinueTourSlot();
			paymentPage.providePaymentDetails();
			paymentPage.clickContinuePaymentPage();
			tourConfirmationPage.verifyConfirmationPageHeader();
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyCancelHereLink();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: AEM_02_Update_CancelExistingTourScreenValidation Author:
	 * Syba Mallick Description: Cancel,Modify and Update existing tour flow
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem", "android" })
	public void AEM_02_Update_CancelExistingTourScreenValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage_Android(tcconfig);
		ExistingTourPage existingTourPage = new ExistingTourPage_Android(tcconfig);
		ModifyTourPage modifyTourPage = new ModifyTourPage_Android(tcconfig);
		CancelConfirmationPage cancelConfirmationPage = new CancelConfirmationPage_Android(tcconfig);
		TourConfirmationPage TourConfirmationPage = new TourConfirmationPage_Android(tcconfig);
		try {
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyExistingPremiumDetails();
			existingTourPage.clickEditTourLink();
			String strTourDate = modifyTourPage.selectUpdatedTourDate();
			String strTourTime = modifyTourPage.selectUpdatedTourTime();
			modifyTourPage.clickSaveChanges();
			TourConfirmationPage.verifyConfirmationPageHeader();
			TourConfirmationPage.verifyScheduleDateAndTimeTourConfirmationPage(strTourDate, strTourTime);
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyCancelHereLink();
			existingTourPage.clickCancelHere();
			existingTourPage.clickYes();
			cancelConfirmationPage.verifyCancelConfirmationPageHeader();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

}
