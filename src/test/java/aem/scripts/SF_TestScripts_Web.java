package aem.scripts;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import aem.pages.CancelConfirmationPage;
import aem.pages.ExistingTourPage;
import aem.pages.LandingPage;
import aem.pages.ModifyTourPage;
import aem.pages.OfferPage;
import aem.pages.PaymentPage;
import aem.pages.ReviewPage;
import aem.pages.SF_LoginPage;
import aem.pages.TourConfirmationPage;
import aem.pages.TourSlotPage;
import automation.core.TestBase;

public class SF_TestScripts_Web extends TestBase {

	public static final Logger log = Logger.getLogger(SF_TestScripts_Web.class);
	public List<String> names = null;

	/*
	 * Function/event:
	 * AEM_01_Header_Footer_PositiveConfirmation_ReloginPageValidation Author:
	 * Syba Mallick Description: Verify Footer and Terms and condition link in
	 * all page
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem" })
	public void AEM_01_Header_Footer_PositiveConfirmation_ReloginPageValidation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		LandingPage landingPage = new LandingPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		TourSlotPage tourSlotPage = new TourSlotPage(tcconfig);
		PaymentPage paymentPage = new PaymentPage(tcconfig);
		ReviewPage reviewPage = new ReviewPage(tcconfig);
		// TourConfirmationPage tourConfirmationPage = new
		// TourConfirmationPage(tcconfig);
		ExistingTourPage existingTourPage = new ExistingTourPage(tcconfig);
		try {
			login.launchUrl(strEnvironment);
			// Added as a Part of Video Player Validation
			/* Start */
			landingPage.videoPlayerValidation();
			/* End */
			landingPage.verifyFooterAndTCLandingPage();
			landingPage.verifyTermsAndPrivacyLink();
			landingPage.enterReservationDetails();
			landingPage.clickGetStartedBtn();
			offerPage.selectPremium();
			offerPage.clickContinueOffers();
			tourSlotPage.selectTourDate();
			tourSlotPage.selectTourTime();
			tourSlotPage.scrollDownTerms();
			tourSlotPage.clickTermsCheckBox();
			tourSlotPage.clickContinueTourSlot();
			paymentPage.providePaymentDetails();
			paymentPage.clickContinuePaymentPage();	
			login.launchUrl(strEnvironment);
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyCancelHereLink();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: AEM_02_Update_CancelExistingTourScreenValidation Author:
	 * Syba Mallick Description: Cancel,Modify and Update existing tour flow
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem" })
	public void AEM_02_Update_CancelExistingTourScreenValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		ExistingTourPage existingTourPage = new ExistingTourPage(tcconfig);
		ModifyTourPage modifyTourPage = new ModifyTourPage(tcconfig);
		CancelConfirmationPage cancelConfirmationPage = new CancelConfirmationPage(tcconfig);
		TourConfirmationPage TourConfirmationPage = new TourConfirmationPage(tcconfig);
		try {
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyExistingPremiumDetails();
			// existingTourPage.verifyExistingTourPage();
			existingTourPage.clickEditTourLink();
			String strTourDate = modifyTourPage.selectUpdatedTourDate();
			String strTourTime = modifyTourPage.selectUpdatedTourTime();
			modifyTourPage.clickSaveChanges();
			TourConfirmationPage.verifyConfirmationPageHeader();
			TourConfirmationPage.verifyScheduleDateAndTimeTourConfirmationPage(strTourDate, strTourTime);
			login.launchWithoutData(strEnvironment);
			existingTourPage.verifyExistingTourPage();
			existingTourPage.verifyCancelHereLink();
			existingTourPage.clickCancelHere();
			existingTourPage.verifyDoubleConfirmationPopup();
			existingTourPage.verifyOnclickNoNavigatesToExistingTour();
			existingTourPage.clickCancelHere();
			existingTourPage.clickYes();
			cancelConfirmationPage.verifyCancelConfirmationPageHeader();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * AEM_03_ErrorMessageValidation_BookingNotAllowed_Within48HrsOfCheckin
	 * Author: Syba Mallick Description: Verify when user try to book a tour
	 * within 48 hour of checkin date then error will come in landing page
	 * TestData: Provide a reservation for which today's date is coming within
	 * 48 hours of checkin
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem" })
	public void AEM_03_ErrorMessageValidation_BookingNotAllowed_Within48HrsOfCheckin(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		LandingPage landingPage = new LandingPage(tcconfig);
		try {
			login.launchWithoutData(strEnvironment);
			landingPage.LandingPageHeaderValidation();
			landingPage.verifyHeader();
			landingPage.verifySubHeader();
			landingPage.verifyLandingPageFields();
			landingPage.verifyLastNameAcceptAlphaNumericCharacters();
			landingPage.verifyMaxLengthLastName();
			landingPage.verifyCheckInDateIsPicker();
			landingPage.verifyCheckInDateFormat();
			landingPage.verifyErrorForTourBooking();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event:
	 * AEM_04_Offer_AdditionalGuest_RightRail_PaymentScreenValidation Author:
	 * Syba Mallick Description: Verify payment screen fields
	 */

	@Test(dataProvider = "testData", groups = { "sst", "aem" })
	public void AEM_04_Offer_AdditionalGuest_RightRail_PaymentScreenValidation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		LandingPage landingPage = new LandingPage(tcconfig);
		TourSlotPage tourSlotPage = new TourSlotPage(tcconfig);
		PaymentPage paymentPage = new PaymentPage(tcconfig);
		try {
			login.launchUrl(strEnvironment);
			landingPage.enterReservationDetails();
			landingPage.clickGetStartedBtn();
			offerPage.verifyOfferPageHeader();
			offerPage.verifyExclusiveOfferMsg();
			offerPage.verifyPremiumDisplayed();
			offerPage.verifyGetDetailsLink();
			List<String> selectedPremiums = offerPage.selectPremium();
			offerPage.clickContinueOffers();
			String tourDate = tourSlotPage.selectTourDate();
			String tourTime = tourSlotPage.selectTourTime();
			tourSlotPage.verifyPremiumNamesInRightRail(selectedPremiums);
			tourSlotPage.verifyRightRailTourSlotPage(tourDate, tourTime);
			tourSlotPage.verifyCheckinAndCheckOutDateEnabledOrDisabled();
			tourSlotPage.verifyAdditionalGuestField();
			String additionalGuestName = tourSlotPage.provideAdditionalGuestDetails();
			tourSlotPage.verifyAdditionalGuestInRightRail(additionalGuestName);
			tourSlotPage.selectTourDate();
			tourSlotPage.selectTourTime();
			tourSlotPage.scrollDownTerms();
			tourSlotPage.clickTermsCheckBox();
			tourSlotPage.clickContinueTourSlot();
			paymentPage.verifyHeaderPaymentScreen();
			paymentPage.verifyPaymentFields();
			// cleanUp();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

}
