package aem.pages;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CancelConfirmationPage extends AEMBasePage {

	public CancelConfirmationPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// CancelConfirmationLocators.class);

	}

	CancelConfirmationLocators CancelConfirmationLocators = new CancelConfirmationLocators(tcConfig);

	public void verifyCancelConfirmationPageHeader() {
		waitUntilElementVisibleWeb(driver, CancelConfirmationLocators.headerCancelConfirmation, 120);
		if (verifyElementDisplayed(CancelConfirmationLocators.headerCancelConfirmation)) {
			tcConfig.updateTestReporter("CancelConfirmationPage", "verifyCancelConfirmationPageHeader", Status.PASS,
					"Successfully navigates to cancel confirmation page");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("CancelConfirmationPage", "verifyCancelConfirmationPageHeader", Status.FAIL,
					"Cancel confirmation header is not present");
		}
	}
}
