package aem.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TourSlotPage_Android extends TourSlotPage {

	public static final Logger log = Logger.getLogger(ReservationPage_Android.class);

	public TourSlotPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// TourSlotPageLocators.class);
	}

	TourSlotPageLocators TourSlotPageLocators = new TourSlotPageLocators(tcConfig);

	public void verifyTourDateCalendar() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.fieldTourSelectDate)) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
						"Tour slot calendar is visble");

				TourSlotPageLocators.fieldTourSelectDateExpand.click();
				tcConfig.updateTestReporter("TourSlotPage", "Click on expand more to view tour dates", Status.PASS,
						"Expand more to reveal tour dates has been clicked");

				if (TourSlotPageLocators.listOfMonths.size() == 1) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
							"Tour slot calendar is showing user's booking month only");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
							"Tour slot calendar is not showing user's booking month");
				}
				String date = testData.get("CheckInDate");
				String month = "";
				try {
					Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(date);
					month = new SimpleDateFormat("MMMM").format(date1);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if (TourSlotPageLocators.tourMonthName.getText().contains(month)) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
							"Tour slot calendar is showing user's stay month");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
							"Tour slot calendar is not showing user's stay month");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
						"Tour slot calendar is not visble");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
					"Getting error while looking for tour date calendar" + e);
		}
	}

	@Override
	public void verifyCheckinAndCheckOutDateEnabledOrDisabled() {

		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);

			TourSlotPageLocators.fieldTourSelectDateExpand.click();
			tcConfig.updateTestReporter("TourSlotPage", "Click on expand more to view tour dates", Status.PASS,
					"Expand more to reveal tour dates has been clicked");

			if (verifyElementDisplayed(TourSlotPageLocators.divCheckinDate)
					&& verifyElementDisplayed(TourSlotPageLocators.divCheckOutDate)) {
				if (TourSlotPageLocators.divCheckinDate.getAttribute("aria-disabled").equals("true")
						&& TourSlotPageLocators.divCheckOutDate.getAttribute("aria-disabled").equals("true")) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.PASS,
							"CheckIn and CheckOut date are disabled");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.PASS,
							"CheckIn and CheckOut date are enabled");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.FAIL,
						"CheckIn and CheckOut date are not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.FAIL,
					"Getting error:" + e);
		}

	}

	@Override
	public void verifyPremiumNamesInRightRail(List<String> names) {

		try {
			TourSlotPageLocators.viewTripSummary.click();
			tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.PASS,
					"View Trip Summary button has been clicked");

			if (verifyElementDisplayed(TourSlotPageLocators.fieldSelectedIncentives.get(0))) {
				waitUntilElementVisibleWeb(driver, TourSlotPageLocators.fieldSelectedIncentives.get(0), 120);
				for (int i = 0; i < TourSlotPageLocators.fieldSelectedIncentives.size(); i++) {
					if (TourSlotPageLocators.fieldSelectedIncentives.get(i).getText().equals(names.get(i))) {
						tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.PASS,
								"Premiums are coming in right rail after selecting");
					} else {
						tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.FAIL,
								"Premiums are not coming in right rail after selecting");
					}
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.FAIL,
						"Premiums are not visible in right rail");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.FAIL,
					"Getting error" + e);
		}

	}

	@Override
	public String selectTourDate() {
		// TODO Auto-generated method stub
		try {
			TourSlotPageLocators.fieldTourSelectDateExpand.click();
			tcConfig.updateTestReporter("TourSlotPage", "selectTourDate", Status.PASS,
					"Expand more to reveal tour dates has been clicked");
			return super.selectTourDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("TourSlotPage", "selectTourDate", Status.FAIL,
					"Expand more to reveal tour dates couldn't be clicked clicked");
			return "";
		}
	}

	@Override
	public void verifyAdditionalGuestInRightRail(String name) {
		// TODO Auto-generated method stub
		try {
			TourSlotPageLocators.viewTripSummary.click();
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.PASS,
					"View Trip Summary has been clicked");
			super.verifyAdditionalGuestInRightRail(name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
					"View Trip Summary couldn't be clicked clicked");
		}
	}

	@Override
	public String provideAdditionalGuestDetails() {

		String name = "";
		try {
			String firstName = generateRandomString(5, CHARACTER_STRING);
			String lastName = generateRandomString(5, CHARACTER_STRING);
			TourSlotPageLocators.txtGuestFirstName.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeys((RemoteWebDriver) driver, "First Name", firstName);
			scrollUpByPixel(200);
			TourSlotPageLocators.txtGuestLastName.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeys((RemoteWebDriver) driver, "Last Name", lastName);
			TourSlotPageLocators.offerDetailsHeader.click();
			name = firstName + " " + lastName;
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestField", Status.FAIL,
					"Getting error: " + e);
			name = "";
		}
		return name;
	}

	public void verifyFooterAndTCTourSlotPage() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.footerTourSlotPage, 120);
			scrollDownForElementJSWb(TourSlotPageLocators.footerTourSlotPage);
			if (verifyElementDisplayed(TourSlotPageLocators.footerTourSlotPage)) {
				if (verifyElementDisplayed(TourSlotPageLocators.lnkTermsAndCondition)) {
					String window = driver.getWindowHandle();
					clickElementWb(TourSlotPageLocators.lnkTermsAndCondition);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyElementDisplayed(TourSlotPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					browserBack((RemoteWebDriver) driver);
					// driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
							"Terms & condition link is not present in Tour Slot page");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
						"Footer is not present in Tour Slot page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
					"Error while checking the footer" + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(TourSlotPageLocators.lnkTermsOfUse);
			links.add(TourSlotPageLocators.lnkPrivacyNotice);
			links.add(TourSlotPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(TourSlotPageLocators.headerOtherPages)
							&& TourSlotPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| TourSlotPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					// driver.close();
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					browserBack((RemoteWebDriver) driver);
					// driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
					"Getting Error: " + e);
		}
	}

}