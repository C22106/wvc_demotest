package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class ModifyTourPageLocators {

	public ModifyTourPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//h1[text()='Edit your Owner Update']")
	public WebElement headerModifyTourPage;

	@FindBy(xpath = "//div[@aria-disabled='false'][@aria-selected='false']")
	public List<WebElement> divEnabledDates;

	@FindBy(xpath = "//strong[contains(text(),'Select')]/../..//div[contains(@class,'self-service')]")
	public List<WebElement> divTourTime;

	@FindBy(xpath = "//div[@aria-selected='true']/div")
	public WebElement fieldSelectedDate;

	@FindBy(xpath = "//div[contains(@class,'slick-current')]//strong")
	public WebElement fieldSelectedTime;

	@FindBy(xpath = "//button[text()='Save changes']")
	public WebElement btnSaveChanges;

	@FindBy(xpath = "//button[text()='Cancel changes']")
	public WebElement btnCancelChanges;

	@FindBy(xpath = "//a[text()='Edit Owner Update']")
	public WebElement lnkEditTour;

	@FindBy(xpath = "//label[span[contains(text(),'Tour')]]/input")
	public WebElement selectedDatePickListField;

	@FindBy(xpath = "//span[contains(text(),'expand_more')]")
	public WebElement fieldTourSelectDateExpand;

	@FindBy(xpath = "//h1[text()='Ready. Set. Go.']")
	public WebElement tourConfirmationHeader;
}
