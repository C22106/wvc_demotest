package aem.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class AEMBasePage extends FunctionalComponents {

	public AEMBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	By loadingSpinnerSST = By.xpath("//div[@class='loader__tick']");

	public void checkLoadingSpinnerSST() {
		waitUntilElementInVisible(driver, loadingSpinnerSST, 60);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser Author : CTS Date : 2019 Change By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change By
	 * : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	final String ALPHA_NUMERIC_STRING = "ABCDEFGHI0123456789@#$%!";
	final String NUMERIC_STRING = "0123456789";
	final String CHARACTER_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public enum Format {
		ALPHA_NUMERIC_STRING("ABCDEFGHI0123456789@#$%!"), NUMERIC_STRING("0123456789"),
		CHARACTER_STRING("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"), NUMBER_STRING("123456789");

		private String format;

		Format(String fmat) {
			this.format = fmat;
		}

		public String getFormat() {
			return format;
		}
	}
}
