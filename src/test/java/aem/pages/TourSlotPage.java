package aem.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TourSlotPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(ReservationPage.class);

	public TourSlotPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// TourSlotPageLocators.class);

	}

	TourSlotPageLocators TourSlotPageLocators = new TourSlotPageLocators(tcConfig);

	protected final String CHARACTER_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public void verifyFooterAndTCTourSlotPage() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.footerTourSlotPage, 120);
			scrollDownForElementJSWb(TourSlotPageLocators.footerTourSlotPage);
			if (verifyElementDisplayed(TourSlotPageLocators.footerTourSlotPage)) {
				if (verifyElementDisplayed(TourSlotPageLocators.lnkTermsAndCondition)) {
					String window = driver.getWindowHandle();
					clickElementJSWithWait(TourSlotPageLocators.lnkTermsAndCondition);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					if (windows.size() == 2) {
						driver.switchTo().window(windows.get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyElementDisplayed(TourSlotPageLocators.termsAndConditionHeader)) {
							tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.PASS,
									"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
						} else {
							tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
									"Terms & condition link is not opening in another tab");
						}
						driver.close();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().window(window);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						if (verifyElementDisplayed(TourSlotPageLocators.termsAndConditionHeader)) {
							tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.PASS,
									"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
						} else {
							tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
									"Terms & condition link is not opening in another tab");
						}
						driver.navigate().back();
					}

				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
							"Terms & condition link is not present in Tour Slot page");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
						"Footer is not present in Tour Slot page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
					"Error while checking the footer" + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(TourSlotPageLocators.lnkTermsOfUse);
			links.add(TourSlotPageLocators.lnkPrivacyNotice);
			links.add(TourSlotPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(TourSlotPageLocators.headerOtherPages)
							&& TourSlotPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| TourSlotPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
					"Getting Error: " + e);
		}
	}

	public String selectTourDate() {
		String date = "";

		waitUntilElementVisibleWeb(driver, TourSlotPageLocators.divEnabledDates.get(0), 120);
		if (verifyElementDisplayed(TourSlotPageLocators.divEnabledDates.get(0))) {
			scrollDownForElementJSWb(TourSlotPageLocators.lblTourSlotHeader);
			if (TourSlotPageLocators.divEnabledDates.size() > 0) {
				clickElementJSWithWait(TourSlotPageLocators.divEnabledDates.get(0));
				/*
				 * waitForSometime(tcConfig.getConfig().get("LowWait")); date =
				 * TourSlotPageLocators.divEnabledDates.get(0).getText();
				 */
				tcConfig.updateTestReporter("TourSlotPage", "selectTourDate", Status.PASS,
						"TourSlot date is visible and clicked");
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "selectTourDate", Status.FAIL,
						"TourSlot date is not visible");
			}

		} else {
			tcConfig.updateTestReporter("TourSlotPage", "selectTourDate", Status.FAIL,
					"Tour slot page is not displayed");
		}

		return date;
	}

	public String selectTourTime() {
		String time = "";
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.divTourTime.get(0), 120);
			if (verifyElementDisplayed(TourSlotPageLocators.divTourTime.get(0))) {
				if (TourSlotPageLocators.divTourTime.get(0).isEnabled()) {
					clickElementWb(TourSlotPageLocators.divTourTime.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					time = TourSlotPageLocators.divTourTime.get(0).getText();
					tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.PASS,
							"TourSlot time is enabled and clicked");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.FAIL,
							"TourSlot time is not enabled");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.FAIL,
						"Tour Time is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.FAIL,
					"Getting error while selecting tour time" + e);
		}
		return time;
	}

	public void clickTermsCheckBox() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.chkBoxTermsAndCondition, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.chkBoxTermsAndCondition)) {
				if (TourSlotPageLocators.chkBoxTermsAndCondition.isEnabled()) {
					clickElementWb(TourSlotPageLocators.chkBoxTermsAndCondition);
					tcConfig.updateTestReporter("TourSlotPage", "clickTermsCheckBox", Status.PASS,
							"Terms and condition check box is enabled and clicked");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.FAIL,
							"Terms and condition check box is not enabled");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.FAIL,
						"Terms and condition checkbox is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.FAIL,
					"Getting error while clicking the checkbox" + e);
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void scrollDownTerms() {
		try {
			if (TourSlotPageLocators.chkBoxTermsAndCondition.getAttribute("style") != "") {
				tcConfig.updateTestReporter("TourSlotPage", "scrollDownTerms", Status.PASS,
						"Terms and condition check box is not enabled before scrolling the T&C");
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "scrollDownTerms", Status.FAIL,
						"Terms and condition check box is enabled before scrolling the T&C");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			scrollDownForElementJSWb(TourSlotPageLocators.fieldTermsAndConditionText);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "scrollDownTerms", Status.FAIL,
					"Getting error while scrolling the terms and condition" + e);
		}
	}

	public void clickContinueTourSlot() {

		waitUntilElementVisibleWeb(driver, TourSlotPageLocators.btnContinueTourSlot, 120);
		if (verifyElementDisplayed(TourSlotPageLocators.btnContinueTourSlot)) {
			if (TourSlotPageLocators.btnContinueTourSlot.isEnabled()) {
				clickElementWb(TourSlotPageLocators.btnContinueTourSlot);
				tcConfig.updateTestReporter("TourSlotPage", "clickContinueTourSlot", Status.PASS,
						"Continue button is enabled and clicked.");
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "clickContinueTourSlot", Status.FAIL,
						"Continue button is not enabled after filling all details.");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "clickContinueTourSlot", Status.FAIL,
					"Continue button is not visible");
		}

	}

	public void verifyTourSlotPageHeader() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.lblTourSlotHeader)) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourSlotPageHeader", Status.PASS,
						"User successfully navigates to tour page");
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourSlotPageHeader", Status.FAIL,
						"User failed to navigate to tour page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTourSlotPageHeader", Status.FAIL,
					"Gettint error while looking for tour slot header" + e);
		}
	}

	public void verifyTourDateCalendar() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.fieldTourDateCalendar)) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
						"Tour slot calendar is visble");
				if (TourSlotPageLocators.listOfMonths.size() == 2) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
							"Tour slot calendar is showing user's booking month and next month");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
							"Tour slot calendar is not showing user's booking month and next month");
				}
				String date = testData.get("CheckInDate");
				String month = "";
				try {
					Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(date);
					month = new SimpleDateFormat("MMMM").format(date1);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if (TourSlotPageLocators.tourMonthName.getText().contains(month)) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
							"Tour slot calendar is showing user's stay month");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
							"Tour slot calendar is not showing user's stay month");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
						"Tour slot calendar is not visble");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
					"Getting error while looking for tour date calendar" + e);
		}
	}

	public void verifyTouringDate() {
		String checkInDate = testData.get("CheckInDate");
		String checkOutDate = testData.get("CheckOutDate");
		String[] date = checkInDate.split("/");
		String[] date1 = checkOutDate.split("/");
		int day = Integer.parseInt(date[1]);
		int day1 = Integer.parseInt(date1[1]);
		int count = 0;
		SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			Date dateCheckin = myFormat.parse(checkInDate);
			Date dateCheckout = myFormat.parse(checkOutDate);
			long difference = dateCheckout.getTime() - dateCheckin.getTime();
			int daysBetween = (int) (difference / (1000 * 60 * 60 * 24));

			if (TourSlotPageLocators.divEnabledDates.size() == daysBetween - 1) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTouringDate", Status.PASS,
						"Only the user's resort stay dates are visible excluding checkIn and checkout");
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTouringDate", Status.FAIL,
						"Wrong dates are visible");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			tcConfig.updateTestReporter("TourSlotPage", "verifyTouringDate", Status.FAIL, "Getting error" + e);
		}

	}

	public void verifyTouringTimeDisabled() {
		String attValue = TourSlotPageLocators.divTourTime.get(0).getCssValue("color");
		if (attValue.trim().equals(testData.get("Color1").trim())) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTouringTimeDisabled", Status.PASS,
					"Touring time is disabled before selecting the date");
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTouringTimeDisabled", Status.FAIL,
					"Touring time is not disabled before selecting the date");
		}

		clickElementWb(TourSlotPageLocators.divEnabledDates.get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		attValue = TourSlotPageLocators.divTourTime.get(0).getCssValue("color");
		if (!attValue.contains(testData.get("Color1"))) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTouringTimeDisabled", Status.PASS,
					"Touring time is enabled after selecting the date");
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTouringTimeDisabled", Status.FAIL,
					"Touring time is not enabled before selecting the date");
		}
	}

	public void verifyTourCriteria() {
		if (TourSlotPageLocators.fieldTourCriteria.size() > 0) {
			for (WebElement e : TourSlotPageLocators.fieldTourCriteria) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourCriteria", Status.PASS,
						"TourCriteria is visible to user" + e.getText());
			}
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTourCriteria", Status.FAIL,
					"TourCriteria is not visible to user");
		}
	}

	public void verifyContinueButtonDisabled() {
		if (verifyElementDisplayed(TourSlotPageLocators.btnContinueTourSlot)) {
			if (!TourSlotPageLocators.btnContinueTourSlot.isEnabled()) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyContinueButtonDisabled", Status.PASS,
						"Continue button is disabled before selecting the tour date and time");
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyContinueButtonDisabled", Status.FAIL,
						"Continue button is enabled before selecting the tour date and time");
			}
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "verifyContinueButtonDisabled", Status.FAIL,
					"Continue button is not displayed");
		}

	}

	public void verifyCheckinAndCheckOutDateEnabledOrDisabled() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.divCheckinDate)
					&& verifyElementDisplayed(TourSlotPageLocators.divCheckOutDate)) {
				if (TourSlotPageLocators.divCheckinDate.getAttribute("aria-disabled").equals("true")
						&& TourSlotPageLocators.divCheckOutDate.getAttribute("aria-disabled").equals("true")) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.PASS,
							"CheckIn and CheckOut date are disabled");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.PASS,
							"CheckIn and CheckOut date are enabled");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.FAIL,
						"CheckIn and CheckOut date are not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.FAIL,
					"Getting error:" + e);
		}

	}

	public void verifyPremiumNamesInRightRail(List<String> names) {
		try {
			if (verifyElementDisplayed(TourSlotPageLocators.fieldSelectedIncentives.get(0))) {
				waitUntilElementVisibleWeb(driver, TourSlotPageLocators.fieldSelectedIncentives.get(0), 120);
				for (int i = 0; i < TourSlotPageLocators.fieldSelectedIncentives.size(); i++) {
					if (TourSlotPageLocators.fieldSelectedIncentives.get(i).getText().equals(names.get(i))) {
						tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.PASS,
								"Premiums are coming in right rail after selecting");
					} else {
						tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.FAIL,
								"Premiums are not coming in right rail after selecting");
					}
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.FAIL,
						"Premiums are not visible in right rail");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyPremiumNamesInRightRail", Status.FAIL,
					"Getting error" + e);
		}
	}

	public void verifyRightRailTourSlotPage(String tourDate, String tourTime) {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblIncentivesSelectedRightRail.get(0), 120);
			if (verifyElementDisplayed(TourSlotPageLocators.listSchedule.get(0))) {
				if (TourSlotPageLocators.listSchedule.get(0).getText().contains(tourDate)) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyRightRailTourSlotPage", Status.PASS,
							"Selected tour date is visible in right trail after selecting");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyRightRailTourSlotPage", Status.FAIL,
							"Selected tour date is not correct");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyRightRailTourSlotPage", Status.FAIL,
						"Selected tour date is not visible in right trail");
			}
			if (verifyElementDisplayed(TourSlotPageLocators.listSchedule.get(1))) {
				if (TourSlotPageLocators.listSchedule.get(1).getText().contains(tourTime)) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyRightRailTourSlotPage", Status.PASS,
							"Selected tour time is visible in right trail after selecting");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyRightRailTourSlotPage", Status.FAIL,
							"Selected tour time is correct");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyRightRailTourSlotPage", Status.FAIL,
						"Selected tour date is not visible in right trail");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyRightRailTourSlotPage", Status.FAIL,
					"Getting error: " + e);
		}

	}

	public void verifyAdditionalGuestField() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.txtGuestFirstName)
					&& verifyElementDisplayed(TourSlotPageLocators.txtGuestLastName)) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestField", Status.PASS,
						"Additional Guest: First Name and Last Name field are visible");
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestField", Status.FAIL,
						"Additional Guest: First Name and Last Name field are not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestField", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public String provideAdditionalGuestDetails() {
		String name = "";
		try {
			String firstName = generateRandomString(5, CHARACTER_STRING);
			String lastName = generateRandomString(5, CHARACTER_STRING);
			fieldDataEnter(TourSlotPageLocators.txtGuestFirstName, firstName);
			fieldDataEnter(TourSlotPageLocators.txtGuestLastName, lastName);
			name = firstName + " " + lastName;
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestField", Status.FAIL,
					"Getting error: " + e);
			name = "";
		}
		return name;
	}

	public void verifyAdditionalGuestInRightRail(String name) {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.fieldAdditionalGuestRightRail, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.fieldAdditionalGuestRightRail)) {
				if (TourSlotPageLocators.fieldAdditionalGuestRightRail.getText().equals(name)) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.PASS,
							"Additional Guest: First Name and Last Name field are visible in right rail");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
							"Additional Guest: First Name and Last Name field are not correct");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
						"Additional Guest: First Name and Last Name field are not visible n right rail");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void verifyAdditionalGuestNotMandatory() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(TourSlotPageLocators.lblPaymentHeader)) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestNotMandatory", Status.PASS,
					"Additional Guest: First Name and Last Name field are not mandatory");
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
					"Additional Guest: First Name and Last Name field are showing as mandatory");
		}

	}

	public void clickTourDateBox() {
		// TODO Auto-generated method stub

	}

}
