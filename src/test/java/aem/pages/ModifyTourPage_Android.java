package aem.pages;

import org.apache.log4j.Logger;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ModifyTourPage_Android extends ModifyTourPage {

	public static final Logger log = Logger.getLogger(HomePage_Android.class);

	public ModifyTourPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), ModifyTourPageLocators.class);

	}

	ModifyTourPageLocators ModifyTourPageLocators = new ModifyTourPageLocators(tcConfig);

	public void verifySelectedDateTimeAndSaveButton() {
		if (ModifyTourPageLocators.selectedDatePickListField.getAttribute("value").contains(testData.get("TourDate"))
				&& !(ModifyTourPageLocators.btnSaveChanges.isEnabled())) {
			tcConfig.updateTestReporter("ModifyTourPage", "verifySelectedDateAndTime", Status.PASS,
					"Previously selected date and time are preselected in modify tour screen and save changes button is disabled");
		} else {
			tcConfig.updateTestReporter("ModifyTourPage", "verifySelectedDateAndTime", Status.FAIL,
					"Either previous selected date and time are not preselected or save changes button is enabled");
		}
	}

	@Override
	public String selectUpdatedTourDate() {
		// TODO Auto-generated method stub
		try {
			ModifyTourPageLocators.fieldTourSelectDateExpand.click();
			tcConfig.updateTestReporter("TourSlotPage", "selectUpdatedTourDate", Status.PASS,
					"Expand more to reveal tour dates has been clicked");
			return super.selectUpdatedTourDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

	}

	@Override
	public void verifyCancelChangesButton() {

		waitUntilElementVisibleWeb(driver, ModifyTourPageLocators.btnCancelChanges, 120);
		if (verifyElementDisplayed(ModifyTourPageLocators.btnCancelChanges)) {
			scrollDownForElementJSWb(ModifyTourPageLocators.btnCancelChanges);
			// int size=driver.findElements(By.xpath("//button[text()='Cancel
			// changes']")).size();

			// clickElementWb(ModifyTourPageLocators.btnCancelChanges);
			clickElementJSWithWait(ModifyTourPageLocators.btnCancelChanges);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(ModifyTourPageLocators.tourConfirmationHeader)) {
				tcConfig.updateTestReporter("ModifyTourPage", "verifyCancelChangesButton", Status.PASS,
						"User is navigated to existing tour page after clicking cancel");
			} else {
				tcConfig.updateTestReporter("ModifyTourPage", "verifyCancelChangesButton", Status.FAIL,
						"User is not navigated to existing tour page after clicking cancel");
			}
		} else {
			tcConfig.updateTestReporter("ModifyTourPage", "verifyCancelChangesButton", Status.FAIL,
					"Cancel changes button is not visible to user");
		}
	}

}