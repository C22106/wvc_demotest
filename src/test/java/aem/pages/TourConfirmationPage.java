package aem.pages;

import java.util.List;

import org.apache.log4j.Logger;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TourConfirmationPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public TourConfirmationPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// TourConfirmationPageLocators.class);

	}

	TourConfirmationPageLocators TourConfirmationPageLocators = new TourConfirmationPageLocators(tcConfig);

	public void verifyConfirmationPageHeader() {

		waitUntilElementVisibleWeb(driver, TourConfirmationPageLocators.lblTourConfHeader, 120);
		if (verifyElementDisplayed(TourConfirmationPageLocators.lblTourConfHeader)) {
			tcConfig.updateTestReporter("TourConfirmationPage", "verifyConfirmationPageHeader", Status.PASS,
					"Tour confirmation header is displayed");
		} else {
			tcConfig.updateTestReporter("TourConfirmationPage", "verifyConfirmationPageHeader", Status.FAIL,
					"Tour confirmation header is not displayed");
		}
	}

	public void verifyPremiumNamesInTourConfirmationPage(List<String> names) {
		int count = 0;
		waitUntilElementVisibleWeb(driver, TourConfirmationPageLocators.fieldsIncentives.get(0), 120);
		for (int i = 0; i < TourConfirmationPageLocators.fieldsIncentives.size(); i++) {
			if (TourConfirmationPageLocators.fieldsIncentives.get(i).getText().equals(names.get(i))) {
				count++;
			} else {
				tcConfig.updateTestReporter("ReviewPage", "verifyPremiumNamesInReviewPage", Status.FAIL,
						"Premium name " + names.get(i) + "is not coming in review page after selecting");
			}
		}
		if (count == TourConfirmationPageLocators.fieldsIncentives.size()) {
			tcConfig.updateTestReporter("ReviewPage", "verifyPremiumNamesInReviewPage", Status.PASS,
					"All the selected premiums are displayed in review page");
		}
	}

	public void verifyScheduleDateAndTimeTourConfirmationPage(String tourDate, String tourTime) {
		boolean flag1 = false;
		boolean flag2 = false;
		if (TourConfirmationPageLocators.fieldScheduleDateAndTime.get(0).getText().contains(tourDate)) {
			flag1 = true;
		} else {
			tcConfig.updateTestReporter("ReviewPage", "verifyScheduleDateAndTimeReviewPage", Status.FAIL,
					"Selected tour date is not displayed in review page");
		}

		if (TourConfirmationPageLocators.fieldScheduleDateAndTime.get(1).getText().equals(tourTime)) {
			flag2 = true;
		} else {
			tcConfig.updateTestReporter("ReviewPage", "verifyScheduleDateAndTimeReviewPage", Status.FAIL,
					"Selected tour time is not displayed in review page");
		}

		if (flag1 && flag2) {
			tcConfig.updateTestReporter("ReviewPage", "verifyScheduleDateAndTimeReviewPage", Status.PASS,
					"Selected tour date and time is displayed in review page");
		}
	}
}
