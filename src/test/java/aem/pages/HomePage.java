package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class HomePage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public HomePage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), HomePageLocators.class);

	}

	HomePageLocators HomePageLocators = new HomePageLocators(tcConfig);
	protected By homeTab = By.xpath("//span[contains(.,'Home')]");
	protected By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	@FindBy(xpath = "//button[@class='slds-button']")
	WebElement menuIcon;
	@FindBy(xpath = "//input[@class='slds-input input']")
	WebElement txtSearchBy;
	protected By globalSearch = By.xpath("//input[@title='Search Salesforce']");

	public void homePageValidation() {
		waitUntilElementVisibleBy(driver, homeTab, 120);

		tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Homepage Navigated");

		if (verifyObjectDisplayed(leadsTab)) {
			clickElementBy(leadsTab);
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Leads Tab clicked");
		} else {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Leads Tab not present");
		}

	}

	public void globalSearch(String data) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			waitUntilElementVisibleWeb(driver, HomePageLocators.txtBoxGlobalSearch, 120);
			if (verifyElementDisplayed(HomePageLocators.txtBoxGlobalSearch)) {
				fieldDataEnter(HomePageLocators.txtBoxGlobalSearch, data);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> e = driver.findElements(By.xpath("//span[@title='" + data + "']/../../../a"));
				if (e.size() > 0) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementWb(e.get(0));
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS,
							"Search Result displayed and clicked");
				} else {
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
							"Search Result is not displayed");
				}
			} else {
				tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL,
						"Global search box is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Getting error:" + e);
			throw e;
		}

	}

	public void verifyMemberGlobalSearch() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement mem = driver.findElement(By.xpath("(//a[@title='" + testData.get("MmbershipNumber") + "'])[2]"));
		String member = mem.getText().trim();
		if (member.equalsIgnoreCase(testData.get("MmbershipNumber"))) {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}

	}

	public void navigateToMenu(String Iterator) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleWeb(driver, HomePageLocators.homeTab, 120);
		checkLoadingSpinnerSST();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(HomePageLocators.menuIcon)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		HomePageLocators.menuIcon.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleWeb(driver, HomePageLocators.txtSearchBy, 120);
		HomePageLocators.txtSearchBy.sendKeys(testData.get("Menu" + Iterator));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//mark[text()='" + testData.get("Menu" + Iterator) + "']")).click();

	}

	public void navigateToCommunity() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
//			waitUntilElementVisibleBy(driver, homeTab, 120);
		checkLoadingSpinnerSST();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(menuIcon)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		menuIcon.click();
		waitUntilElementVisible(driver, txtSearchBy, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//a[contains(text(),'Journey Community')]")).click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		SwitchtoWindow();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	public void navigateToUrl(String Iterator) throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("URL" + Iterator));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	protected By arivallist = By.xpath("(//img[@class='wn-left_custom_icon'])[3]");
	protected By clickadd = By.xpath("(//div[@title='expand'])[2]");

	public void arrivallistselect() {
		waitUntilElementVisibleBy(driver, arivallist, 120);
		driver.findElement(arivallist).click();
		waitUntilElementVisibleBy(driver, clickadd, 120);
		driver.findElement(clickadd).click();
	}

	protected By navigatejourneycomm = By.xpath("//a[contains(.,'Journey Community')]");
	protected By Homemarketer = By.xpath("//li//a//img[contains(@src,'Home')]");

	public void navigatatojourneycommunity() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, homeTab, 120);
		checkLoadingSpinnerSST();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(menuIcon)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		menuIcon.click();
		waitUntilElementVisibleBy(driver, navigatejourneycomm, 120);
		driver.findElement(navigatejourneycomm).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		/*
		 * ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		 * driver.switchTo().window(tabs.get(1));
		 */

	}

	protected By arlst = By.xpath("//b[text()='Arrival List']");

	public void selectarrivallist() {
		waitUntilElementVisibleBy(driver, arlst, 120);
		driver.findElement(arlst).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}

	protected By ressel = By.xpath(
			"//a[text()='Reservations']/ancestor::div[@class='slds-grid slds-grid--vertical slds-m-bottom_small']//a[text()='"
					+ testData.get("ReservationNumber") + "']");
	protected By travelchnl = By.xpath("//span[@title='Travel Channel']");
	protected By Bookbtn = By.xpath("//button[text()='Book Tour']");

	public void SelRes() {
		waitUntilElementVisibleBy(driver, ressel, 120);
		driver.findElement(ressel).click();
		waitUntilElementVisibleBy(driver, Bookbtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(driver.findElement(Bookbtn))) {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Clicked on Available Reservation");
		} else {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Reservation is not found");
		}
	}

	protected By ownersel = By.xpath(
			"//a[text()='Owners']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-card']//a[text()='"
					+ testData.get("LeadName") + "']");
	protected By ownerimg = By.xpath("//img[@title='Owner']");

	public void SelOwner() {
		waitUntilElementVisibleBy(driver, ownersel, 120);
		driver.findElement(ownersel).click();
		waitUntilElementVisibleBy(driver, ownerimg, 120);
		if (verifyElementDisplayed(driver.findElement(ownerimg))) {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Clicked on Available Owner");
		} else {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Owner is not found");
		}
	}

	protected By txtTourRecords = By.xpath("//input[@class='slds-input input']");

	public void searchTourRecords() {
		waitUntilElementVisibleBy(driver, txtTourRecords, 120);
		driver.findElement(txtTourRecords).click();
	}

	protected By searchBar = By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Search.svg']");
	protected By memberNumber = By.xpath("//input[@class='slds-input']");
	protected By btnbookTour = By.xpath("//button[text()='BOOK TOUR']");
	protected By btnCreateTour = By.xpath("//button[text()='CREATE TOUR']");
	protected By arrivalList = By
			.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Reservation_List.svg']");
	protected By txtSearchGuest = By.xpath("//input[@id='text-input-id-1']");
	protected By lnkGuestName = By.xpath("//a[text()='" + testData.get("LeadName") + "']");
	protected By arrivalIcon = By
			.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Reservation_List.svg'");
	protected By imgArrival = By.xpath("//img[contains(@src,'Reservation_List')]");

	public void searchReservaion() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.findElement(arrivalList).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(txtSearchGuest).sendKeys(testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(lnkGuestName).click();

	}

	public void ownerArrival() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyObjectDisplayed(imgArrival)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(imgArrival).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, "Search Result displayed");
		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.FAIL, "Search Result not displayed");
		}
		waitForSometime(tcConfig.getConfig().get("LongWait"));
	}

	public void globalSearchOwner() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, searchBar, 120);
		if (verifyObjectDisplayed(searchBar)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(memberNumber, testData.get("LeadName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(memberNumber).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.FAIL, "Search Result not displayed");
		}
	}

	public void toursToConfirm() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// WebElement searchBar=
		// driver.findElement(By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Search.svg']"));
		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		String name = testData.get("LeadName");
		waitUntilElementVisibleBy(driver, searchBar, 120);

		driver.findElement(searchBar).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(memberNumber).sendKeys(name);
		driver.findElement(memberNumber).sendKeys(Keys.ENTER);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnbookTour, 120);

		List<WebElement> button = driver.findElements(btnbookTour);
		int count = button.size();
		if (count > 0) {
			button.get(0).click();

		} else {
			tcConfig.updateTestReporter("HomePage", "toursToConfirm", Status.FAIL, "Book Tour button not displayed");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnCreateTour, 120);
		clickElementBy(btnCreateTour);
	}

	/*
	 * public void mouseoverAndClick(WebElement element){ try{ ((JavascriptExecutor)
	 * driver).executeScript("arguments[0].scrollIntoView(true);", element); new
	 * Actions(driver).moveToElement(element).click().perform(); }catch(Exception
	 * e){ tcConfig.updateTestReporter("", "mouseoverAndClick",Status.FAIL,
	 * "unable to hover and click " + "element due to "+ e.getMessage()); }
	 * 
	 * }
	 */

	protected By ReservationSearch = By.xpath("//input[@placeholder='Search Reservations and more...']");

	public void globalSearchRes() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleWeb(driver, HomePageLocators.ReservationSearch, 120);
		if (verifyElementDisplayed(HomePageLocators.ReservationSearch)) {
			fieldDataEnter(HomePageLocators.ReservationSearch, testData.get("ReservationNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
		WebElement e = driver.findElement(By.xpath("//span[@title='" + testData.get("ReservationNumber") + "']/../.."));
		e.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void globalSearch_Owner() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearch, 120);
		if (verifyObjectDisplayed(globalSearch)) {
			fieldDataEnter(globalSearch, testData.get("LeadName"));
			driver.findElement(globalSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
	}

	public By FirstName = By.xpath("//span[text()='Primary Guest First Name']//..//..//span[@class='uiOutputText']");
	public By LastName = By.xpath("//span[text()='Primary Guest Last Name']//..//..//span[@class='uiOutputText']");

	public void validateFN_LN() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, FirstName, 120);
		String Fname = driver.findElement(FirstName).getText();
		if (Fname.equalsIgnoreCase(testData.get("FirstName"))) {
			tcConfig.updateTestReporter("HomePage", "validateFN_LN", Status.PASS, "First Name Validated");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Error while validating first name");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, LastName, 120);
		String Lname = driver.findElement(LastName).getText();
		if (Lname.equalsIgnoreCase(testData.get("LastName"))) {
			tcConfig.updateTestReporter("HomePage", "validateFN_LN", Status.PASS, "Last Name Validated");

		} else {
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Error while validating last name");
		}
	}
}
