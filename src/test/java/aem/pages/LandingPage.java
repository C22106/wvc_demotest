package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class LandingPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public LandingPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), LandingPageLocators.class);

	}

	LandingPageLocators LandingPageLocators = new LandingPageLocators(tcConfig);

	final String ALPHA_NUMERIC_STRING = "ABCDEFGHI0123456789@#$%!";
	final String NUMERIC_STRING = "0123456789";
	final String CHARACTER_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	protected WebElement lblLandingHeader;

	@FindBy(xpath = "//strong[contains(text(),'verify your vacation information')]")
	protected WebElement lblLandingSubHeader;

	@FindBy(xpath = "//span[text()='Confirmation Number']/../input")
	protected WebElement inputConfirmationNumber;

	@FindBy(xpath = "//span[text()='Last Name']/../input")
	protected WebElement inputLastName;

	@FindBy(xpath = "//span[text()='Check-In Date']/../input")
	protected WebElement inputCheckInDate;

	@FindBy(xpath = "//button[@type='submit']")
	protected WebElement btnGetStarted;

	@FindBy(xpath = "//div[contains(@class,'validation')][contains(text(),'This is not a')]")
	protected WebElement lblErrorMsg;

	@FindBy(xpath = "//div[contains(@class,'DayPicker Selectable')]")
	protected WebElement calendarCheckInDate;

	@FindBy(xpath = "//div[contains(@class,'DayPicker-Day')][not(contains(@aria-disabled,'true'))]")
	protected List<WebElement> linkSelectDaysCheckInDate;

	@FindBy(xpath = "//div[contains(@class,'wyn-footer__advertisingdisclaimer')]")
	protected WebElement footerLandingPage;

	@FindBy(xpath = "//a[@title='terms-and-conditions']")
	protected WebElement lnkTermsAndCondition;

	@FindBy(xpath = "//span[text()='calendar_today']")
	protected WebElement imgCalendar;

	/*
	 * Function: verifyHeader Author: Soumya Tripathy Description: Verifying the
	 * header of a page Parameters: Header title
	 */
	public void verifyHeader() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.lblLandingHeader, 120);
		tcConfig.updateTestReporter("LandingPage", "LandingPageValidation", Status.PASS, "Navigated To LandPage");

		if (LandingPageLocators.lblLandingHeader.getText().length() > 0) {
			tcConfig.updateTestReporter("LandingPage", "LandingPageHeaderValidation", Status.PASS,
					"Landing page header is displayed correctly");
		} else {
			tcConfig.updateTestReporter("LandingPage", "LandingPageHeaderValidation", Status.FAIL,
					"Landing page header is not displayed");
		}
	}

	public void verifySubHeader() {
		if (LandingPageLocators.lblLandingSubHeader.isDisplayed()) {
			tcConfig.updateTestReporter("LandingPage", "LandingPageSubHeaderValidation", Status.PASS,
					"Landing page Subheader is displayed correctly "
							+ LandingPageLocators.lblLandingSubHeader.getText());
		} else {
			tcConfig.updateTestReporter("LandingPage", "LandingPageHeaderValidation", Status.FAIL,
					"Landing page Subheader is not displayed");
		}
	}

	public void verifyLandingPageFields() {
		if (LandingPageLocators.inputConfirmationNumber.isDisplayed() && LandingPageLocators.inputLastName.isDisplayed()
				&& LandingPageLocators.inputCheckInDate.isDisplayed()) {
			tcConfig.updateTestReporter("LandingPage", "LandingPageFieldValidation", Status.PASS,
					"Confirmation Number, Last Name & CheckIn date textbox is visible");
		} else {
			tcConfig.updateTestReporter("LandingPage", "LandingPageFieldValidation", Status.FAIL,
					"Some field/fields are not visible");
		}
	}

	public void verifyGetStartedButton() {
		if (LandingPageLocators.btnGetStarted.isDisplayed() && (!LandingPageLocators.btnGetStarted.isEnabled())) {
			tcConfig.updateTestReporter("LandingPage", "GetStartedButtonValidation", Status.PASS,
					"GetStarted button is visible and disabled");
		} else {
			tcConfig.updateTestReporter("LandingPage", "GetStartedButtonValidation", Status.FAIL,
					"GetStarted button is either not visible or not displayed");
		}
	}

	public void verifyLastNameAcceptAlphaNumericCharacters() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.inputLastName, 120);
		LandingPageLocators.inputLastName.sendKeys(generateRandomString(7, ALPHA_NUMERIC_STRING));
		// waitUntilElementVisibleWeb(driver, LandingPageLocators.lblErrorMsg,
		// 120);
		if (LandingPageLocators.lblErrorMsg.size() > 0) {
			tcConfig.updateTestReporter("LandingPage", "LastNameValidation", Status.PASS,
					"Last Name is not accepting alphanumeric charaters");
		} else {
			tcConfig.updateTestReporter("LandingPage", "LastNameValidation", Status.PASS,
					"Last Name is accepting alphanumeric charaters");
		}

		LandingPageLocators.inputLastName.clear();
		LandingPageLocators.inputLastName.sendKeys(generateRandomString(7, NUMERIC_STRING));

		if (LandingPageLocators.lblErrorMsg.size() > 0) {
			tcConfig.updateTestReporter("LandingPage", "LastNameValidation", Status.PASS,
					"Last Name is not accepting numeric charaters");
		} else {
			tcConfig.updateTestReporter("LandingPage", "LastNameValidation", Status.PASS,
					"Last Name is accepting numeric charaters");
		}

	}

	public void verifyMaxLengthLastName() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.inputLastName, 120);
		LandingPageLocators.inputLastName.sendKeys(generateRandomString(53, CHARACTER_STRING));
		String inputValue = LandingPageLocators.inputLastName.getAttribute("value");
		if (inputValue.length() == 50) {
			tcConfig.updateTestReporter("LandingPage", "LastNameMaxLengthValidation", Status.PASS,
					"Maximum Character functionality is working fine.");
		} else {
			tcConfig.updateTestReporter("LandingPage", "LastNameMaxLengthValidation", Status.FAIL,
					"Maximum Character functionality is not working fine.");
		}

		LandingPageLocators.inputLastName.clear();
		String maxLengthDefined = LandingPageLocators.inputLastName.getAttribute("maxlength");
		if (maxLengthDefined.equals("50")) {
			tcConfig.updateTestReporter("LandingPage", "LastNameMaxLengthValidation", Status.PASS,
					"Maximum Character limit is defined as 50");
		} else {
			tcConfig.updateTestReporter("LandingPage", "LastNameMaxLengthValidation", Status.FAIL,
					"Maximum Character limit is not defined as 50");
		}
	}

	public void verifyCheckInDateIsPicker() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.inputCheckInDate, 120);
		LandingPageLocators.inputCheckInDate.click();
		if (LandingPageLocators.calendarCheckInDate.isDisplayed()) {
			tcConfig.updateTestReporter("LandingPage", "CheckInDatePickerValidation", Status.PASS,
					"CheckIn date is a date picker");
		} else {
			tcConfig.updateTestReporter("LandingPage", "CheckInDatePickerValidation", Status.FAIL,
					"CheckIn date is not a date picker");
		}
		List<WebElement> e = LandingPageLocators.linkSelectDaysCheckInDate;
		if (e.size() > 0) {
			e.get(0).click();
		} else {
			LandingPageLocators.calendarArrow.click();
			List<WebElement> e1 = LandingPageLocators.linkSelectDaysCheckInDate;
			e1.get(0).click();
		}

		tcConfig.updateTestReporter("LandingPage", "CheckInDatePickerValidation", Status.PASS,
				"Selected date from DatePicker");
	}

	public void verifyCheckInDateFormat() {
		// String dateFormat = testData.get("DateFormat");
		LandingPageLocators.inputCheckInDate.sendKeys(Keys.LEFT_SHIFT, Keys.HOME, Keys.BACK_SPACE);
		// waitForSometime(tcConfig.getConfig().get("MedWait"));
		// LandingPageLocators.inputCheckInDate.sendKeys(dateFormat);
		if (LandingPageLocators.lblErrorMsg.size() > 0) {
			tcConfig.updateTestReporter("LandingPage", "CheckInDateFormatValidation", Status.PASS,
					LandingPageLocators.lblErrorMsg.get(0).getText());
		} else {
			tcConfig.updateTestReporter("LandingPage", "CheckInDateFormatValidation", Status.FAIL,
					"CheckInDate is accepting wrong date format");
		}
	}

	public void verifyOnclickGetStartedButton() {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWeb(driver, LandingPageLocators.inputConfirmationNumber, 120);
			if (!(LandingPageLocators.btnGetStarted.isEnabled())) {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.PASS,
						"GetStarted button is disabled before filling all mandatory details");
			} else {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.FAIL,
						"GetStarted button is enabled before filling all details");
			}

			LandingPageLocators.inputConfirmationNumber.sendKeys(testData.get("ConfirmationNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			LandingPageLocators.inputLastName.sendKeys(testData.get("LastName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			LandingPageLocators.inputCheckInDate.sendKeys(testData.get("CheckInDate"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (LandingPageLocators.btnGetStarted.isEnabled()) {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.PASS,
						"GetStarted button is getting enabled after filling all mandatory details");
			} else {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.FAIL,
						"GetStarted button is not getting enabled after filling all mandatory details");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.FAIL,
					"Getting error :" + e);
		}

	}

	public void verifyBookedTourBooking() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.inputConfirmationNumber, 120);
		LandingPageLocators.inputConfirmationNumber.sendKeys(testData.get("ConfirmationNumber"));
		LandingPageLocators.inputLastName.sendKeys(testData.get("LastName"));
		LandingPageLocators.inputCheckInDate.sendKeys(testData.get("CheckInDate"));
		LandingPageLocators.btnGetStarted.click();
	}

	public void enterReservationDetails() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.inputConfirmationNumber, 120);
		scrollUpByPixel(500);
		fieldDataEnter(LandingPageLocators.inputConfirmationNumber, testData.get("ConfirmationNumber"));
		fieldDataEnter(LandingPageLocators.inputLastName, testData.get("LastName"));
		fieldDataEnter(LandingPageLocators.inputCheckInDate, testData.get("CheckInDate"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// clickElementJS(imgCalendar);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void clickGetStartedBtn() {

		waitUntilObjectVisible(driver, LandingPageLocators.btnGetStarted, 120);
		clickElementJSWithWait(LandingPageLocators.btnGetStarted);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void verifyFooterAndTCLandingPage() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.footerLandingPage, 120);
		scrollDownForElementJSWb(LandingPageLocators.footerLandingPage);
		if (LandingPageLocators.footerLandingPage.isDisplayed()) {
			if (LandingPageLocators.lnkTermsAndCondition.isDisplayed()) {
				String window = driver.getWindowHandle();
				clickElementWb(LandingPageLocators.lnkTermsAndCondition);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<String> windows = new ArrayList<String>(driver.getWindowHandles());
				if (windows.size() == 2) {
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyElementDisplayed(LandingPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					if (verifyElementDisplayed(LandingPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					driver.navigate().back();
				}

			} else {
				tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
						"Terms & condition link is not present in landing page");
			}
		} else {
			tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
					"Footer is not present in landing page");
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(LandingPageLocators.lnkTermsOfUse);
			links.add(LandingPageLocators.lnkPrivacyNotice);
			links.add(LandingPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(LandingPageLocators.headerOtherPages)
							&& LandingPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| LandingPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
					"Getting Error: " + e);
		}
	}

	public String generateRandomString(int count, String type) {

		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * type.length());
			builder.append(type.charAt(character));
		}
		return builder.toString();

	}

	public void verifyErrorForTourBooking() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.lblErrorMsg.get(0), 120);
		if (LandingPageLocators.lblErrorMsg.size() > 0) {
			tcConfig.updateTestReporter("LandingPage", "verifyErrorForTourBooking", Status.PASS,
					"'" + LandingPageLocators.lblErrorMsg.get(0).getText()
							+ "' error is displayed for the current scenario");
		} else {
			tcConfig.updateTestReporter("LandingPage", "verifyErrorForTourBooking", Status.FAIL,
					"Error is not displayed for the current scenario");
		}
	}

	public void LandingPageHeaderValidation() {
		if (LandingPageLocators.logo.isDisplayed() && LandingPageLocators.labelContactUs.isDisplayed()
				&& LandingPageLocators.footerlogo.isDisplayed()) {
			tcConfig.updateTestReporter("LandingPage", "LandingPageHeaderValidation", Status.PASS,
					"Club Wyndham header and footer logo, Need Help & Contavt Us label is visible");
		} else {
			tcConfig.updateTestReporter("LandingPage", "LandingPageFieldValidation", Status.FAIL,
					"Club Wyndham header and footer logo, Need Help & Contavt Us label  are not visible");
		}
	}

	/*
	 * Method Name: videoPlayerValidation Description : This methods validates the
	 * SST Video player in Homepage Date: May 2020 Author: Priya Das
	 */

	protected By imageLogoTxt = By.xpath("//img[contains(@class,'logo')]");
	protected By videoPlayerBtn = By
			.xpath("//div[contains(@class,'wyn-self-service')]//button[contains(@class,'player-button')]");
	protected By videoTitle = By.xpath("//div[contains(text(),'Live Like You Mean It')]");
	protected By videoCloseBtn = By.xpath("//button[@class='close-button']");
	protected By videoFrame = By.xpath("//iframe[@id='videoPlayer']");
	protected By videoPlayBtn = By.xpath("//div[@class='vp-controls']//button[contains(@class,'play')]");

	public void videoPlayerValidation() {

		try {
			String strImgLogo = driver.findElement(imageLogoTxt).getAttribute("alt");
			String strImgLogoTxt[] = getCapitalizedWord(strImgLogo.toLowerCase()).split(" ");
			waitUntilElementVisibleBy(driver, videoPlayerBtn, 120);
			if (verifyElementDisplayed(driver.findElement(videoPlayerBtn))) {
				tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.PASS,
						"Video Player button is displayed");
				clickElementBy(videoPlayerBtn);
				waitUntilObjectVisible(driver, videoTitle, 240);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyElementDisplayed(driver.findElement(videoTitle))) {
					tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.PASS,
							"Video Title is displayed successfully");
					WebElement eleVideoPlayer = driver.findElement(By.id("videoPlayer"));
					driver.switchTo().frame(eleVideoPlayer);
					if (verifyElementDisplayed(driver.findElement(By.xpath("//a[contains(text(),'Self-Service Tours - "
							+ strImgLogoTxt[0].substring(0, 3) + "')]")))) {
						tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.PASS,
								"Video Player header title " + strImgLogo + " is displayed ");
						if (verifyElementDisplayed(driver.findElement(videoPlayBtn))) {
							tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.PASS,
									"Play button is present in the Video Player");
							driver.findElement(By.xpath("//a[contains(text(),'Self-Service Tours - "
									+ strImgLogoTxt[0].substring(0, 3) + "')]")).click();
							List<String> windows = new ArrayList<String>(driver.getWindowHandles());
							driver.switchTo().window(windows.get(1));
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							String strwindowTitle = driver.getTitle();
							System.out.println(strwindowTitle);
							if (strwindowTitle.toUpperCase().contains(
									strImgLogo.toUpperCase() + " " + testData.get("VideoTabTitle").toUpperCase())) {
								tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.PASS,
										"Video is opened successfully in another tab");
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								driver.close();
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								driver.switchTo().window(windows.get(0));

							} else {
								tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
										"Video is not opened");
							}
						} else {
							tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
									"Play button is not present in the Video Player");
						}
					} else {
						tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
								"Video Player header title is not displayed ");
					}

				} else {
					tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
							"Video Title is not displayed");
				}

				driver.switchTo().defaultContent();
				waitUntilObjectVisible(driver, videoCloseBtn, 120);
				clickElementBy(videoCloseBtn);

			} else {
				tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
						"Video Player button is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
					"Failed in method" + e.getMessage());
		}
	}

}
