package aem.pages;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class SF_LoginPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(SF_LoginPage.class);
	ReadConfigFile config = new ReadConfigFile();

	public SF_LoginPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), SF_LoginPageLocators.class);

	}

	SF_LoginPageLocators SF_LoginPageLocators = new SF_LoginPageLocators(tcConfig);

	protected By username = By.xpath("//input[@id='username']");
	protected By password = By.xpath("//input[@id='password']");
	protected By loginButton = By.id("Login");
	protected By logOut = By.xpath("//a[text()='Log Out']");
	protected By viewProfile = By.xpath("//img[@title='User']");
	protected By homeIcon = By.xpath("//li[@data-menu-item-id='0']");
	protected By profileName = By.xpath("//span[@class=' profileName']");
	protected By logOutMarketer = By.xpath("//a[@title='Logout']");
//	@FindBy(xpath="//img[@title='User']") WebElement viewProfile;
	protected By imageLogOut = By.xpath("//span[@class='uiImage']//img[@alt='User']"); // webelement list
	@FindBy(xpath = "//h1[contains(@class,'title')]")
	protected WebElement lblHeader;

	@FindBy(xpath = "//span[text()='Confirmation Number']/../input")
	public static WebElement inputConfirmationNumber;

	@FindBy(xpath = "//span[text()='Last Name']/../input")
	public static WebElement inputLastName;

	@FindBy(xpath = "//span[text()='Check-In Date']/../input")
	public static WebElement inputCheckInDate;

	@FindBy(xpath = "//button[@type='submit']")
	public static WebElement btnGetStarted;

	protected void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.username, 10);
		SF_LoginPageLocators.username.sendKeys(strUserName);
		tcConfig.updateTestReporter("SF_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	protected void setPassword(String strPassword) throws Exception {

		// look for th prob;
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		SF_LoginPageLocators.password.sendKeys(dString);
		tcConfig.updateTestReporter("SF_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	public void launchApp(){
		String url = config.get("SF_URL").trim();
		driver.navigate().to(url);
		// driver.manage().window().maximize();

	}

	public void launchWithoutData(String url){

		String appUrl = tcConfig.getTestData().get("URL");
		// String url=config.get("AEM_URL_QA").trim();
		driver.navigate().to(appUrl);
		// driver.manage().window().maximize();
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.lblHeader, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		enterReservationDetails();
		clickGetStartedBtn();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void launchUrl(String url){
		System.out.println("Launch");
		String appUrl = testData.get("URL");
		driver.navigate().to(appUrl);
		// driver.manage().window().maximize();
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.lblHeader, 120);
	}

	public void launchWithData(){
		driver.navigate().to(testData.get("URL"));
		// driver.manage().window().maximize();
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.lblHeader, 120);
	}

	public void enterReservationDetails() {
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.inputConfirmationNumber, 120);
		fieldDataEnter(SF_LoginPageLocators.inputConfirmationNumber, testData.get("ConfirmationNumber"));
		fieldDataEnter(SF_LoginPageLocators.inputLastName, testData.get("LastName"));
		fieldDataEnter(SF_LoginPageLocators.inputCheckInDate, testData.get("CheckInDate"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// clickElementJS(imgCalendar);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void clickGetStartedBtn() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(SF_LoginPageLocators.btnGetStarted);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void loginToSalesForce() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("SF_Corporate_UserID");
		String password = tcConfig.getConfig().get("SF_Corporate_Password");

		setUserName(userid);
		setPassword(password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		SF_LoginPageLocators.loginButton.click();
		checkLoadingSpinnerSST();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");

	}

	public void loginToSalesForceDiffUsers() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = testData.get("SF_UserID");
		String password = testData.get("SF_Password");

		setUserName(userid);
		setPassword(password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(loginButton).click();
		checkLoadingSpinnerSST();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");

	}

	public void salesForceLogoutMarketer() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, homeIcon, 120);
			clickElementBy(homeIcon);
			waitUntilElementVisibleBy(driver, profileName, 120);
			clickElementBy(profileName);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, logOutMarketer, 120);
			clickElementBy(logOutMarketer);

			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.PASS, "Log Off successful");

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	public void salesForceLogout() throws Exception {

		try {
			/*
			 * waitForSometime(tcConfig.getConfig().get("LongWait")); List<WebElement> img =
			 * driver.findElements(imageLogOut);
			 * 
			 * 
			 * waitForSometime(tcConfig.getConfig().get("lowWait"));
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); img.get(0).click();
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * //waitUntilObjectVisible(driver, viewProfile, 120);
			 * //driver.findElement(viewProfile).click();
			 */
//			mouseoverAndClick(viewProfile);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			clickElementBy(viewProfile);

			WebElement viewpro = driver.findElement(viewProfile);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", viewpro);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, logOut, 120);
			driver.findElement(logOut).click();

			waitUntilElementVisibleBy(driver, username, 10);
			if (verifyObjectDisplayed(username)) {
				tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.PASS,
						"Log outl Successful");
			} else {
				tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
						"Log Off unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	protected By logout_inhouse = By.xpath("//li[@class='logOut uiMenuItem']");

	@FindBy(xpath = "//img[@title='In-House Admin 2']")
	WebElement profileImg;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrw;

	public void inHouseAdmin2Logout() throws Exception {

		try {

			mouseoverAndClick(profileImg);

			waitUntilElementVisibleBy(driver, logout_inhouse, 120);
			driver.findElement(logout_inhouse).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	public void clearCache() {
		// ClearSafariCache(driver);
		clearCacheSafari();
		// Go to Home Page
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("keySequence", "HOME ");
		((JavascriptExecutor) driver).executeScript("mobile:presskey", param);

		// Launch Settings Menu
		Map<String, Object> params1 = new HashMap<>();
		params1.put("identifier", "com.apple.mobilesafari");
		((JavascriptExecutor) driver).executeScript("mobile:application:open", params1);
		startApp("Safari", driver);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		switchToContext(driver, "WEBVIEW");
	}

}