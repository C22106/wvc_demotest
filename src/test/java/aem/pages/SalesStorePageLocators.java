package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class SalesStorePageLocators {

	public SalesStorePageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//img[@title='Sales Store']")
	public WebElement imgSalesStoresPage;

	@FindBy(xpath = "//span[contains(text(),'Include Res Check-in')]/../../div[2]//img")
	public WebElement imgIncludeResCheckIn;

	@FindBy(xpath = "//span[contains(text(),'Include Res Check-Out')]/../../div[2]//img")
	public WebElement imgIncludeResCheckOut;

	@FindBy(xpath = "//a[@title='Edit']")
	public WebElement btnEdit;

	@FindBy(xpath = "//span[contains(text(),'Include Res Check-in')]/../../input")
	public WebElement chkBoxIncludeResCheckIn;

	@FindBy(xpath = "//span[contains(text(),'Include Res Check-Out')]/../../input")
	public WebElement chkBoxIncludeResCheckOut;

	@FindBy(xpath = "//button[@title='Save']")
	public WebElement btnSave;

	@FindBy(xpath = "//span[text()='Information']/../../../div/div//a[contains(@class,'textUnderline')]")
	public List<WebElement> lnkOperatingHours;

	@FindBy(xpath = "//a[@title='Tour Wave Slot Allocation']")
	public WebElement lnkTourWaveAlcn;

	@FindBy(xpath = "//button[contains(text(),'Self Service Allocation')]")
	public WebElement btnSelfServiceAllocation;

	@FindBy(xpath = "//label[text()='Waves']/..//button")
	public WebElement dropDwnWaves;

	@FindBy(xpath = "//li[@class='slds-dropdown__item']/a")
	public List<WebElement> lnkDrpDwnValues;

	@FindBy(xpath = "//input[@class='slds-input input']")
	public List<WebElement> txtBoxDates;

	@FindBy(xpath = "//label[text()='Day of Week']/..//button")
	public WebElement drpDwnDayOfWeek;

	@FindBy(xpath = "//input[@name='number']")
	public WebElement txtBoxTotalSlots;

	@FindBy(xpath = "//label[text()='Allotment Type']/..//button")
	public WebElement drpDwnAllotmentType;

	@FindBy(xpath = "//button[contains(text(),'Generate Tour Slots')]")
	public WebElement btnGenTourSlots;

	@FindBy(xpath = "//div[contains(@class,'slds-modal__content')]/p")
	public List<WebElement> divSuccessMsg;

	@FindBy(xpath = "//div[contains(@class,'slds-modal__content')]/..//button")
	public List<WebElement> btnOk;

	@FindBy(xpath = "//label[text()='Day of Week']/../div/div[1]/div//a")
	public List<WebElement> listDayOfWeek;

	@FindBy(xpath = "//label[text()='Allotment Type']/../div/div[1]/div//a")
	public List<WebElement> listAllotmentType;

	@FindBy(xpath = "//button[contains(text(),'Filter')]")
	public WebElement btnFilter;

	@FindBy(xpath = "//div[text()='Wave Time']/..//button")
	public WebElement btnWaveTimeFilter;

	@FindBy(xpath = "//tr[@class='slds-hint-parent']")
	public List<WebElement> lstFilterResult;

	@FindBy(xpath = "//td[@data-label='SST Allocation']//input")
	public List<WebElement> txtBoxSSTAllocation;

	@FindBy(xpath = "//span[@class='total-text']/../button[2]")
	public WebElement btnUpdate;

	@FindBy(xpath = "//td[@data-label='SST Authorized']//span")
	public List<WebElement> txtSstAuthorized;

}
