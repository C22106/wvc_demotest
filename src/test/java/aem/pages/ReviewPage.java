package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ReviewPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public ReviewPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), ReviewPageLocators.class);

	}

	protected ReviewPageLocators ReviewPageLocators = new ReviewPageLocators(tcConfig);

	public void verifyFooterAndTCReviewPage() {
		waitUntilElementVisibleWeb(driver, ReviewPageLocators.footerReviewPage, 120);
		if (verifyElementDisplayed(ReviewPageLocators.footerReviewPage)) {
			if (verifyElementDisplayed(ReviewPageLocators.lnkTermsAndCondition)) {
				String window = driver.getWindowHandle();
				clickElementWb(ReviewPageLocators.lnkTermsAndCondition);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<String> windows = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(windows.get(1));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(ReviewPageLocators.termsAndConditionHeader)) {
					tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.PASS,
							"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
				} else {
					tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.FAIL,
							"Terms & condition link is not opening in another tab");
				}
				driver.close();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(window);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.FAIL,
						"Terms & condition link is not present in Review page");
			}
		} else {
			tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.FAIL,
					"Footer is not present in Review page");
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(ReviewPageLocators.lnkTermsOfUse);
			links.add(ReviewPageLocators.lnkPrivacyNotice);
			links.add(ReviewPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(ReviewPageLocators.headerOtherPages)
							&& ReviewPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| ReviewPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}

	public void verifyReviewHeader() {
		waitUntilElementVisibleWeb(driver, ReviewPageLocators.lblReviewHeader, 120);

		if (verifyElementDisplayed(ReviewPageLocators.lblReviewHeader)) {
			tcConfig.updateTestReporter("ReviewPage", "verifyReviewHeader", Status.PASS,
					"Review page header is visible");
		} else {
			tcConfig.updateTestReporter("ReviewPage", "verifyReviewHeader", Status.FAIL,
					"Review page header is not visible");
		}
	}

	public void verifyPremiumNamesInReviewPage(List<String> names) {
		int count = 0;
		waitUntilElementVisibleWeb(driver, ReviewPageLocators.fieldsIncentives.get(0), 120);
		for (int i = 0; i < ReviewPageLocators.fieldsIncentives.size(); i++) {
			if (ReviewPageLocators.fieldsIncentives.get(i).getText().equals(names.get(i))) {
				count++;
			} else {
				tcConfig.updateTestReporter("ReviewPage", "verifyPremiumNamesInReviewPage", Status.FAIL,
						"Premium name " + names.get(i) + "is not coming in review page after selecting");
			}
		}
		if (count == ReviewPageLocators.fieldsIncentives.size()) {
			tcConfig.updateTestReporter("ReviewPage", "verifyPremiumNamesInReviewPage", Status.PASS,
					"All the selected premiums are displayed in review page");
		}
	}

	public void verifyScheduleDateAndTimeReviewPage(String tourDate, String tourTime) {
		boolean flag1 = false;
		boolean flag2 = false;
		if (ReviewPageLocators.fieldScheduleDateAndTime.get(0).getText().contains(tourDate)) {
			flag1 = true;
		} else {
			tcConfig.updateTestReporter("ReviewPage", "verifyScheduleDateAndTimeReviewPage", Status.FAIL,
					"Selected tour date is not displayed in review page");
		}

		if (ReviewPageLocators.fieldScheduleDateAndTime.get(1).getText().equals(tourTime)) {
			flag2 = true;
		} else {
			tcConfig.updateTestReporter("ReviewPage", "verifyScheduleDateAndTimeReviewPage", Status.FAIL,
					"Selected tour time is not displayed in review page");
		}

		if (flag1 && flag2) {
			tcConfig.updateTestReporter("ReviewPage", "verifyScheduleDateAndTimeReviewPage", Status.PASS,
					"Selected tour date and time is displayed in review page");
		}
	}

	public void clickBookNow() {
		if (verifyElementDisplayed(ReviewPageLocators.btnBookNow)) {
			clickElementWb(ReviewPageLocators.btnBookNow);
			tcConfig.updateTestReporter("ReviewPage", "clickBookNow", Status.PASS,
					"Book button is visible and clicked");
		} else {
			tcConfig.updateTestReporter("ReviewPage", "clickBookNow", Status.FAIL, "Book button is not visible");
		}
	}
}
