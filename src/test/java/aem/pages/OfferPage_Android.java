package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OfferPage_Android extends OfferPage {

	public static final Logger log = Logger.getLogger(ReservationPage_Android.class);

	public OfferPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), OfferPageLocators.class);

	}

	OfferPageLocators OfferPageLocators = new OfferPageLocators(tcConfig);

	public void verifyPremiumNamesInTripSummary(List<String> names) {
		try {
			OfferPageLocators.viewTripSummary.click();
			waitUntilElementVisibleWeb(driver, OfferPageLocators.fieldSelectedIncentives.get(0), 120);
			if (verifyElementDisplayed(OfferPageLocators.fieldSelectedIncentives.get(0))) {
				for (int i = 0; i < OfferPageLocators.fieldSelectedIncentives.size(); i++) {
					if (OfferPageLocators.fieldSelectedIncentives.get(i).getText().equals(names.get(i))) {
						tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.PASS,
								"Premiums are coming in right rail after selecting");
					} else {
						tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.FAIL,
								"Premiums selected are not correct");
					}
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.FAIL,
						"Premiums are not visible in right rail after selecting");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.FAIL,
					"Getting error:" + e);
		}
	}

	public void verifyGetDetailsLinkOnclick() {
		waitUntilElementVisibleWeb(driver, OfferPageLocators.lnkGetDetails, 120);
		clickElementJSWithWait(OfferPageLocators.lnkGetDetails);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (OfferPageLocators.fieldPremiumDescription.isDisplayed()) {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkOnclick", Status.PASS,
					"Premium disclosure section is visible after clicking the get details link");
		} else {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkOnclick", Status.FAIL,
					"Premium disclosure section is not visible after clicking the get details link");
		}
	}

	@Override
	public void verifyFooterAndTCOfferPage() {
		try {

			waitUntilElementVisibleWeb(driver, OfferPageLocators.chkBoxOffer.get(0), 120);
			if (verifyElementDisplayed(OfferPageLocators.footerOfferPage)) {
				scrollDownForElementJSWb(OfferPageLocators.footerOfferPage);
				if (verifyElementDisplayed(OfferPageLocators.lnkTermsAndCondition)) {
					String url = driver.getCurrentUrl();
					clickElementWb(OfferPageLocators.lnkTermsAndCondition);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
//					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
//					driver.switchTo().window(windows.get(1));
//					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyElementDisplayed(OfferPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
//					driver.close();
//					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.get(url);;
					browserBack((RemoteWebDriver) driver);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
							"Terms & condition link is not present in landing page");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
						"Footer is not present in landing page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL, "Getting error: " + e);
		}
	}

	@Override
	public void verifyTermsAndPrivacyLink() {
		driver.navigate().refresh();
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(OfferPageLocators.lnkTermsOfUse);
			links.add(OfferPageLocators.lnkPrivacyNotice);
			links.add(OfferPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String url = driver.getCurrentUrl();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
//					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
//					driver.switchTo().window(windows.get(1));
//					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(OfferPageLocators.headerOtherPages)
							&& OfferPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| OfferPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
//					driver.close();
//					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.get(url);
					browserBack((RemoteWebDriver) driver);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}

}