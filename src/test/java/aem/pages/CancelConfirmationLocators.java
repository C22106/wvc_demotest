package aem.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class CancelConfirmationLocators {

	public CancelConfirmationLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//h1[text()='We are sorry you are unable to attend']")
	public WebElement headerCancelConfirmation;
}
