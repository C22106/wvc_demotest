package aem.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class AudiencePage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(AudiencePage.class);

	public AudiencePage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), AudiencePageLocators.class);

	}

	AudiencePageLocators AudiencePageLocators = new AudiencePageLocators(tcConfig);

	public static String offerName = "";
	public static String strAudCode = "";
	final String CHARACTER_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	final String INTEGER_STRING = "0123456789";

	@FindBy(xpath = "//th[text()='Offer Code']/../../../tbody/tr[1]/th[1]//a")
	protected WebElement lnkOfferCode;

	public void clickOfferCodelnk() {
		waitUntilElementVisible(driver, AudiencePageLocators.lnkOfferCode, 120);
		Actions a = new Actions(driver);
		a.moveToElement(AudiencePageLocators.lnkOfferCode).click().build().perform();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void clickNew() {
		try {
			waitUntilElementVisibleWeb(driver, AudiencePageLocators.lnkNew, 120);
			clickElementWb(AudiencePageLocators.lnkNew);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("AudiencePage", "clickNew", Status.PASS, "Clicked new audience link");
		} catch (Exception e) {
			tcConfig.updateTestReporter("AudiencePage", "clickNew", Status.FAIL, "Getting error:" + e);
			throw e;
		}
	}

	public void createNewAudience() {
		try {
			if (testData.get("NewAudience").equals("")) {
				clickNew();
				waitUntilElementVisibleWeb(driver, AudiencePageLocators.txtBoxAudienceCode, 120);
				strAudCode = generateRandomString(3, CHARACTER_STRING) + generateRandomString(2, INTEGER_STRING);
				fieldDataEnter(AudiencePageLocators.txtBoxAudienceCode, strAudCode);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(AudiencePageLocators.txtBoxAudienceName, generateRandomString(6, CHARACTER_STRING));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(AudiencePageLocators.txtBoxAudienceDesc, generateRandomString(6, CHARACTER_STRING));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementWb(AudiencePageLocators.btnSave);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleWeb(driver, AudiencePageLocators.divSuccessMsg, 120);
				if (verifyElementDisplayed(AudiencePageLocators.divSuccessMsg)) {
					tcConfig.updateTestReporter("AudiencePage", "createNewAudience", Status.PASS,
							"New audience is created successfully:" + strAudCode);
				} else {
					tcConfig.updateTestReporter("AudiencePage", "createNewAudience", Status.FAIL,
							"Audience creation failed");
				}
			} else {
				tcConfig.updateTestReporter("AudiencePage", "createNewAudience", Status.PASS,
						"Proceeding with existing audience");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("AudiencePage", "createNewAudience", Status.FAIL, "Getting error:" + e);
			throw e;
		}
	}

	public void openExisitngAudience() {
		try {
			if (!testData.get("NewAudience").equals("")) {
				waitUntilElementVisibleWeb(driver, AudiencePageLocators.lnkNew, 120);
				List<WebElement> e = driver.findElements(By.xpath("//a[@title='" + testData.get("NewAudience") + "']"));
				if (e.size() > 0) {
					e.get(e.size() - 1).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("AudiencePage", "openNewAudience", Status.PASS,
							"Navigated to audience details page");
				} else {
					tcConfig.updateTestReporter("AudiencePage", "openNewAudience", Status.FAIL,
							"Newly created audience is not present");
				}
			} else {
				tcConfig.updateTestReporter("AudiencePage", "openNewAudience", Status.PASS,
						"New Audience is already opened");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("AudiencePage", "openNewAudience", Status.FAIL, "Getting error:" + e);
			throw e;
		}
	}

	public void linkOfferToAudience() {
		try {
			waitUntilElementVisibleWeb(driver, AudiencePageLocators.btnNewOffer, 120);
			clickElementJSWithWait(AudiencePageLocators.btnNewOffer);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWeb(driver, AudiencePageLocators.headerNewAudMangmnt, 120);
			if (testData.get("OfferCode").equals("")) {
				clickElementWb(AudiencePageLocators.txtBoxOfferCode);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollDownForElementJSWb(AudiencePageLocators.lnkNewOfferManagement);
				scrollUpByPixel(50);
				clickElementWb(AudiencePageLocators.lnkNewOfferManagement);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				createOffer();
				clickSave();
			} else {
				fieldDataEnter(AudiencePageLocators.txtBoxOfferCode, testData.get("OfferCode"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> e = driver
						.findElements(By.xpath("//div[@title='" + testData.get("OfferCode") + "']/../.."));
				if (e.size() > 0) {
					e.get(0).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("AudiencePage", "linkOfferToAudience", Status.FAIL,
							"Offer code is not available");
				}
			}

			clickElementWb(AudiencePageLocators.chkBoxActive);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(AudiencePageLocators.btnSave);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWeb(driver, AudiencePageLocators.divSuccessMsg, 120);
			if (verifyElementDisplayed(AudiencePageLocators.divSuccessMsg)) {
				waitUntilElementVisibleWeb(driver, AudiencePageLocators.lnkLinkedOfferCode, 120);
				if (verifyElementDisplayed(AudiencePageLocators.lnkLinkedOfferCode)) {
					tcConfig.updateTestReporter("AudiencePage", "linkOfferToAudience", Status.PASS,
							"Offer code is linked successfully");
				} else {
					tcConfig.updateTestReporter("AudiencePage", "linkOfferToAudience", Status.FAIL,
							"Offer linking failed");
				}
			} else {
				tcConfig.updateTestReporter("AudiencePage", "linkOfferToAudience", Status.FAIL,
						"Success message is not coming after updating");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("AudiencePage", "linkOfferToAudience", Status.FAIL, "Getting error:" + e);
			throw e;
		}
	}

	public void createOffer() {
		waitUntilElementVisibleWeb(driver, AudiencePageLocators.headerNewOfferPage, 120);
		if (verifyElementDisplayed(AudiencePageLocators.txtBoxSalesStore)) {
			try {
				fieldDataEnter(AudiencePageLocators.txtBoxSalesStore, testData.get("SalesStore"));
				List<WebElement> e = driver
						.findElements(By.xpath("//div[@title='" + testData.get("SalesStore") + "']/.."));
				if (e.size() > 0) {
					waitUntilElementVisibleWeb(driver, e.get(0), 120);
					e.get(0).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"Salestore is selected is " + testData.get("SalesStore"));
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"Selected salestore is not coming");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(AudiencePageLocators.txtBoxOfferName)) {
					offerName = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(AudiencePageLocators.txtBoxOfferName, offerName);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"Offer name provided is " + offerName);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"Offer Name textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(AudiencePageLocators.txtBoxEffectiveEndDate.get(0))) {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					Date d = new Date();
					String todayDate = sdf.format(d);
					fieldDataEnter(AudiencePageLocators.txtBoxEffectiveEndDate.get(0), todayDate);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"Effective Date is provided " + todayDate);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"Effective Date is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(AudiencePageLocators.txtBoxEffectiveEndDate.get(1))) {
					String endDate = getFutureDate(Integer.parseInt(testData.get("EndDateinDays")), "MM/dd/yyyy");
					fieldDataEnter(AudiencePageLocators.txtBoxEffectiveEndDate.get(1), endDate);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"End Date is provided " + endDate);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"End Date is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementWb(AudiencePageLocators.txtBoxOfferName);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(AudiencePageLocators.txtAreaOfferPaymentDetails)) {
					String offerPaymentDetails = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(AudiencePageLocators.txtAreaOfferPaymentDetails, offerPaymentDetails);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"Offer payment details provided is " + offerPaymentDetails);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"Offer payment details textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(AudiencePageLocators.txtBoxPaymentPageDetails)) {
					String PaymentDetails = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(AudiencePageLocators.txtBoxPaymentPageDetails, PaymentDetails);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"Offer payment details provided is " + PaymentDetails);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"Offer payment details textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(AudiencePageLocators.txtAreaOfferDescription)) {
					String offerDescription = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(AudiencePageLocators.txtAreaOfferDescription, offerDescription);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"Offer description provided is " + offerDescription);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"Offer description textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(AudiencePageLocators.listAvailableDays.get(0))) {
					for (int i = 0; i < Integer.parseInt(testData.get("AvailableDays")); i++) {
						clickElementWb(AudiencePageLocators.listAvailableDays.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementWb(
								AudiencePageLocators.listMoveBtn.get(AudiencePageLocators.listMoveBtn.size() - 2));
					}
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage", "createOffer", Status.PASS,
							"Selected available day");
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
							"Available day is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
						"Some fields are not visible");
			}

		} else {
			tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
					"Salestore field is not visible");
		}
	}

	public void clickSave() {
		if (verifyElementDisplayed(
				AudiencePageLocators.btnSaveOffer.get(AudiencePageLocators.btnSaveOffer.size() - 1))) {
			clickElementWb(AudiencePageLocators.btnSaveOffer.get(AudiencePageLocators.btnSaveOffer.size() - 1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("OfferManagementPage", "createOffer", Status.FAIL,
					"Save button is not visible");
		}
	}

//	public void testNG (){

//		
//	}
//	
}
