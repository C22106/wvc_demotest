package aem.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SalesStorePage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(SalesStorePage.class);

	public SalesStorePage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), SalesStorePageLocators.class);

	}

	SalesStorePageLocators SalesStorePageLocators = new SalesStorePageLocators(tcConfig);

	public static String startDate = "";
	public static String endDate = "";
	public static String totalSlots = "";

	public void includeCheckInAndCheckOutDates() {
		String value = "";
		String value1 = "";
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			waitUntilElementVisibleWeb(driver,
					driver.findElement(By.xpath(
							"//span[text()='Name']/../../div[2]//span[text()='" + testData.get("SalesStore") + "']")),
					120);
			if (verifyElementDisplayed(SalesStorePageLocators.imgIncludeResCheckIn)) {
				if (verifyElementDisplayed(SalesStorePageLocators.imgIncludeResCheckOut)) {
					tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.PASS,
							"Include reservation checkin and checkout checkboxes are visible");
					value = SalesStorePageLocators.imgIncludeResCheckIn.getAttribute("class");
					value1 = SalesStorePageLocators.imgIncludeResCheckOut.getAttribute("class");
					if (verifyElementDisplayed(SalesStorePageLocators.btnEdit)) {
						clickElementWb(SalesStorePageLocators.btnEdit);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						if (verifyElementDisplayed(SalesStorePageLocators.chkBoxIncludeResCheckIn)
								&& verifyElementDisplayed(SalesStorePageLocators.chkBoxIncludeResCheckOut)) {
							if (value.trim().equalsIgnoreCase("unchecked")) {
								clickElementWb(SalesStorePageLocators.chkBoxIncludeResCheckIn);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} else {
								tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.PASS,
										"Include reservation checkin checkbox is already checked");
							}
							if (value1.trim().equalsIgnoreCase("unchecked")) {
								clickElementWb(SalesStorePageLocators.chkBoxIncludeResCheckOut);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} else {
								tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.PASS,
										"Include reservation checkout checkbox is already checked");
							}
							if (verifyElementDisplayed(SalesStorePageLocators.btnSave)) {
								clickElementWb(SalesStorePageLocators.btnSave);
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								value = SalesStorePageLocators.imgIncludeResCheckIn.getAttribute("class");
								value1 = SalesStorePageLocators.imgIncludeResCheckOut.getAttribute("class");
								if ((!value.trim().equalsIgnoreCase("unchecked"))
										&& (!value1.trim().equalsIgnoreCase("unchecked"))) {
									tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates",
											Status.PASS,
											"Include reservation checkin and checkout checkbox are checked");
								} else {
									tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates",
											Status.FAIL,
											"Include reservation checkin and checkout checkbox are not already checked");
								}
							} else {
								tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.FAIL,
										"Save button is not visible");
							}
						} else {
							tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.FAIL,
									"Include reservation checkin or checkout checkboxes are not visible");
						}
					} else {
						tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.FAIL,
								"Edit button is not visible");
					}
				}

				else {
					tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.FAIL,
							"Include reservation checkout image is not visible");
				}
			} else {
				tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.FAIL,
						"Include reservation checkin image is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Salestore", "includeCheckInAndCheckOutDates", Status.FAIL,
					"Getting error" + e);
		}
	}

	public void clickSave() {
		try {
			waitUntilElementVisibleWeb(driver, SalesStorePageLocators.btnSave, 120);
			if (verifyElementDisplayed(SalesStorePageLocators.btnSave)) {
				clickElementWb(SalesStorePageLocators.btnSave);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("Salestore", "clickSave", Status.PASS, "Clicked on save button");
			} else {
				tcConfig.updateTestReporter("Salestore", "clickSave", Status.FAIL, "Save button is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Salestore", "clickSave", Status.FAIL, "Getting error" + e);
		}
	}

	public void navigateToTourSlotAllocation() {
		try {
			waitUntilElementVisibleWeb(driver, SalesStorePageLocators.btnEdit, 120);
			clickElementWb(
					SalesStorePageLocators.lnkOperatingHours.get(SalesStorePageLocators.lnkOperatingHours.size() - 1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleWeb(driver, SalesStorePageLocators.lnkTourWaveAlcn, 120);
			clickElementWb(SalesStorePageLocators.lnkTourWaveAlcn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("Salestore", "navigateToTourSlotAllocation", Status.FAIL, "Getting error" + e);
		}
	}

	public void tourSlotAllocation() {
		try {
			waitUntilElementVisibleWeb(driver, SalesStorePageLocators.btnSelfServiceAllocation, 120);
			clickElementWb(SalesStorePageLocators.dropDwnWaves);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(SalesStorePageLocators.lnkDrpDwnValues.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			startDate = getFutureDate(Integer.parseInt(testData.get("AvailableDays")) + 15, "MMM dd, yyyy");
			endDate = getFutureDate(Integer.parseInt(testData.get("EndDateinDays")) + 15, "MMM dd, yyyy");
			fieldDataEnter(SalesStorePageLocators.txtBoxDates.get(0), startDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(SalesStorePageLocators.txtBoxDates.get(1), endDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(SalesStorePageLocators.drpDwnDayOfWeek);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			for (int i = 0; i < SalesStorePageLocators.listDayOfWeek.size(); i++) {
				clickElementWb(SalesStorePageLocators.listDayOfWeek.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
			totalSlots = generateRandomString(1, Format.NUMBER_STRING.getFormat());
			fieldDataEnter(SalesStorePageLocators.txtBoxTotalSlots, totalSlots);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(SalesStorePageLocators.drpDwnAllotmentType);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(SalesStorePageLocators.listAllotmentType.get(2));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(SalesStorePageLocators.lnkTourWaveAlcn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (SalesStorePageLocators.btnGenTourSlots.isEnabled()) {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.PASS,
						"Generate tour slots button is getting enabled after filling all the details");
			} else {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.FAIL,
						"Generate tour slots button is not getting enabled after filling all the details");
			}

			clickElementJSWithWait(SalesStorePageLocators.btnGenTourSlots);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWeb(driver, SalesStorePageLocators.divSuccessMsg.get(0), 120);
			if (verifyElementDisplayed(SalesStorePageLocators.divSuccessMsg.get(0))) {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.PASS,
						"Tour slots are allocated successfully");
				clickElementWb(SalesStorePageLocators.btnOk.get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.FAIL,
						"Failed to allocate tour slots");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.FAIL, "Getting error" + e);
		}
	}

	public void clickSelfServiceAllocation() {
		waitUntilElementVisibleWeb(driver, SalesStorePageLocators.btnSelfServiceAllocation, 120);
		clickElementWb(SalesStorePageLocators.btnSelfServiceAllocation);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void applyFilters() {
		try {
			waitUntilElementVisibleWeb(driver, SalesStorePageLocators.btnFilter, 120);
			clickElementWb(SalesStorePageLocators.btnWaveTimeFilter);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(SalesStorePageLocators.lnkDrpDwnValues.get(0));
			fieldDataEnter(SalesStorePageLocators.txtBoxDates.get(0), startDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(SalesStorePageLocators.txtBoxDates.get(1), endDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementWb(SalesStorePageLocators.btnFilter);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (SalesStorePageLocators.lstFilterResult.size() == 7) {
				tcConfig.updateTestReporter("Salestore", "applyFilters", Status.PASS,
						"Correct data is visible after applying the filter");
			} else {
				tcConfig.updateTestReporter("Salestore", "applyFilters", Status.FAIL,
						"Correct data is not visible after applying the filter");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Salestore", "applyFilters", Status.FAIL, "Getting error" + e);
		}
	}

	public void setSstAllocation() {
		try {
			waitUntilElementVisibleWeb(driver, SalesStorePageLocators.txtBoxSSTAllocation.get(0), 120);
			fieldDataEnter(SalesStorePageLocators.txtBoxSSTAllocation.get(0), "75");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (SalesStorePageLocators.btnUpdate.isDisplayed()) {
				tcConfig.updateTestReporter("Salestore", "setSstAllocation", Status.PASS,
						"Update button is getting enabled after providing the SST allocation");
				scrollDownForElementJSWb(SalesStorePageLocators.btnUpdate);
				clickElementWb(SalesStorePageLocators.btnUpdate);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleWeb(driver, SalesStorePageLocators.divSuccessMsg.get(0), 120);
				if (verifyElementDisplayed(SalesStorePageLocators.divSuccessMsg.get(0))) {
					tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.PASS,
							"Tour slots are allocated successfully");
					clickElementWb(SalesStorePageLocators.btnOk.get(0));
				} else {
					tcConfig.updateTestReporter("Salestore", "tourSlotAllocation", Status.FAIL,
							"Failed to allocate tour slots");
				}
				int x = 75 * Integer.parseInt(totalSlots);
				int sstAuthorized = x / 100;
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (SalesStorePageLocators.txtSstAuthorized.get(0).getText().equals(Integer.toString(sstAuthorized))) {
					tcConfig.updateTestReporter("Salestore", "setSstAllocation", Status.PASS,
							"SST Authorized is rounding up to a whole number after updating");
				} else {
					tcConfig.updateTestReporter("Salestore", "setSstAllocation", Status.FAIL,
							"SST Authorized is not rounding up to a whole number after updating");
				}
			} else {
				tcConfig.updateTestReporter("Salestore", "setSstAllocation", Status.FAIL,
						"Update button is not getting enabled after providing the SST allocation");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Salestore", "setSstAllocation", Status.FAIL, "Getting error" + e);
		}
	}
}