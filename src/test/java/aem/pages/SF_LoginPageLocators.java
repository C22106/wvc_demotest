package aem.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class SF_LoginPageLocators {

	public SF_LoginPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	// public By username = By.xpath("//input[@id='username']");
	// private By password = By.xpath("//input[@id='password']");
	// private By loginButton = By.id("Login");
	private By logOut = By.xpath("//a[text()='Log Out']");
	private By viewProfile = By.xpath("//img[@title='User']");
	private By homeIcon = By.xpath("//li[@data-menu-item-id='0']");
	private By profileName = By.xpath("//span[@class=' profileName']");
	private By logOutMarketer = By.xpath("//a[@title='Logout']");
	// @FindBy(xpath="//img[@title='User']") WebElement viewProfile;
	private By imageLogOut = By.xpath("//span[@class='uiImage']//img[@alt='User']"); // webelement list

	private By logout_inhouse = By.xpath("//li[@class='logOut uiMenuItem']");

	@FindBy(xpath = "//img[@title='In-House Admin 2']")
	WebElement profileImg;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrw;
	@FindBy(xpath = "//img[@title='In-House Marketer 3']")
	WebElement profileImgMarAgent;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrwMarAgent;
	@FindBy(xpath = "//div//div//li//a[@title='Logout']")
	WebElement logOutMarketingAgent3;

	@FindBy(xpath = "//span[text()='Confirmation Number']/../input")
	public WebElement inputConfirmationNumber;

	@FindBy(xpath = "//span[text()='Last Name']/../input")
	public WebElement inputLastName;

	@FindBy(xpath = "//span[text()='Check-In Date']/../input")
	public WebElement inputCheckInDate;

	@FindBy(xpath = "//button[@type='submit']")
	public WebElement btnGetStarted;

	@FindBy(xpath = "//input[@id='username']")
	public WebElement username;

	@FindBy(xpath = "//input[@id='password']")
	public WebElement password;

	@FindBy(id = "Login")
	public WebElement loginButton;

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public WebElement lblHeader;
}
