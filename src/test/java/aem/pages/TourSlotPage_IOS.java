package aem.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TourSlotPage_IOS extends TourSlotPage {

	public static final Logger log = Logger.getLogger(ReservationPage_IOS.class);

	public TourSlotPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// TourSlotPageLocators.class);
	}

	public By tourDateTxtBx = By.xpath("//span[text()='Tour date*']/../input");
	public TourSlotPageLocators TourSlotPageLocators = new TourSlotPageLocators(tcConfig);

	public void clickTourDateBox() {
		clickElementBy(tourDateTxtBx);
	}

	public void verifyTourDateCalendar() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);
			clickElementBy(tourDateTxtBx);
			if (verifyElementDisplayed(TourSlotPageLocators.fieldTourDateCalendar)) {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
						"Tour slot calendar is visble");

				if (TourSlotPageLocators.listOfMonths.size() == 1 || TourSlotPageLocators.listOfMonths.size() == 2) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
							"Tour slot calendar is showing user's booking month");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
							"Tour slot calendar is not showing user's booking month");
				}
				String date = testData.get("CheckInDate");
				String month = "";
				try {
					Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(date);
					month = new SimpleDateFormat("MMMM").format(date1);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if (TourSlotPageLocators.tourMonthName.getText().contains(month)) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.PASS,
							"Tour slot calendar is showing user's stay month");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
							"Tour slot calendar is not showing user's stay month");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
						"Tour slot calendar is not visble");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTourDateCalendar", Status.FAIL,
					"Getting error while looking for tour date calendar" + e);
		}
	}

	public void verifyCheckinAndCheckOutDateEnabledOrDisabled() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.lblTourSlotHeader, 120);
			clickElementBy(tourDateTxtBx);
			if (verifyElementDisplayed(TourSlotPageLocators.divCheckinDate)
					&& verifyElementDisplayed(TourSlotPageLocators.divCheckOutDate)) {
				if (TourSlotPageLocators.divCheckinDate.getAttribute("aria-disabled").equals("true")
						&& TourSlotPageLocators.divCheckOutDate.getAttribute("aria-disabled").equals("true")) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.PASS,
							"CheckIn and CheckOut date are disabled");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.PASS,
							"CheckIn and CheckOut date are enabled");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.FAIL,
						"CheckIn and CheckOut date are not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyCheckInAndCheckOutDateDisabled", Status.FAIL,
					"Getting error:" + e);
		}

	}

	public void verifyFooterAndTCTourSlotPage() {
		try {
			waitUntilElementVisibleWeb(driver, TourSlotPageLocators.footerTourSlotPage, 120);
			scrollDownForElementJSWb(TourSlotPageLocators.footerTourSlotPage);
			if (verifyElementDisplayed(TourSlotPageLocators.footerTourSlotPage)) {
				if (verifyElementDisplayed(TourSlotPageLocators.lnkTermsAndCondition)) {
					// String window = driver.getWindowHandle();
					// clickElementWb(TourSlotPageLocators.lnkTermsAndCondition);
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					String URL = driver.getCurrentUrl();
					// clickElementWb(LandingPageLocators.lnkTermsAndCondition);
					scrollDownForElementJSWb(TourSlotPageLocators.lnkTermsAndCondition);
					clickIOSElement("our Terms and Conditions", 1, TourSlotPageLocators.lnkTermsAndCondition);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyElementDisplayed(TourSlotPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					driver.get(URL);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
							"Terms & condition link is not present in Tour Slot page");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
						"Footer is not present in Tour Slot page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyFooterAndTCTourSlotPage", Status.FAIL,
					"Error while checking the footer" + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(TourSlotPageLocators.lnkTermsOfUse);
			links.add(TourSlotPageLocators.lnkPrivacyNotice);
			links.add(TourSlotPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String URL = driver.getCurrentUrl();
					scrollDownForElementJSWb(links.get(i));
					scrollDownByPixel(50);
					if (links.get(i).getText().equalsIgnoreCase("Terms of Use")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Terms of Use", 1, links.get(i));
						} else {
							clickIOSElement("Terms of", 1, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("Privacy Notice")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Notice", 1, links.get(i));
						} else {
							clickIOSElement("Notice", 2, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("privacy Setting")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Setting", 1, links.get(i));
						} else {
							clickIOSElement("Setting", 1, links.get(i));
						}
					}

					// String window = driver.getWindowHandle();
					// clickElementWb(links.get(i));
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					// List<String> windows = new
					// ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(TourSlotPageLocators.headerOtherPages)
							&& TourSlotPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| TourSlotPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.get(URL);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyTermsAndPrivacyLink", Status.FAIL,
					"Getting Error: " + e);
		}
	}

	public String provideAdditionalGuestDetails() {
		// driver.navigate().refresh();
		String name = "";
		try {
			String firstName = generateRandomString(5, CHARACTER_STRING);
			String lastName = generateRandomString(5, CHARACTER_STRING);
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPhone")) {
				TourSlotPageLocators.txtGuestFirstName.click();
				scrollUpByPixel(150);
				sendKeys((RemoteWebDriver) driver, "First Name", firstName);
				scrollUpByPixel(150);
				TourSlotPageLocators.txtGuestLastName.click();
				sendKeys((RemoteWebDriver) driver, "Last Name", lastName);
			} else {
				TourSlotPageLocators.txtGuestFirstName.click();

				sendKeys((RemoteWebDriver) driver, "First Name", firstName);

				TourSlotPageLocators.txtGuestLastName.click();

				sendKeys((RemoteWebDriver) driver, "Last Name", lastName);
			}

			// fieldDataEnter(TourSlotPageLocators.txtGuestFirstName,
			// firstName);
			// fieldDataEnter(TourSlotPageLocators.txtGuestLastName, lastName);
			scrollUpByPixel(100);
			name = firstName + " " + lastName;
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestField", Status.FAIL,
					"Getting error: " + e);
			name = "";
		}
		return name;
	}

	public By addtionalDetailsLnk = By.xpath("//a[text()='View Trip Summary']");

	public void verifyAdditionalGuestInRightRail(String name) {
		// driver.findElement(addtionalDetailsLnk).click();
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// waitUntilElementVisibleWeb(driver,
			// TourSlotPageLocators.fieldAdditionalGuestRightRail, 120);
			if (verifyElementDisplayed(TourSlotPageLocators.fieldAdditionalGuestRightRail)) {
				if (TourSlotPageLocators.fieldAdditionalGuestRightRail.getText().equals(name)) {
					tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.PASS,
							"Additional Guest: First Name and Last Name field are visible in right rail");
				} else {
					tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
							"Additional Guest: First Name and Last Name field are not correct");
				}
			} else {
				tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
						"Additional Guest: First Name and Last Name field are not visible n right rail");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourSlotPage", "verifyAdditionalGuestInRightRail", Status.FAIL,
					"Getting error: " + e);
		}
	}

}