package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class LandingPage_IOS extends LandingPage {

	public static final Logger log = Logger.getLogger(LandingPage_IOS.class);
	LandingPageLocators LandingPageLocators = new LandingPageLocators(tcConfig);

	public LandingPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// LandingPageLocators.class);
	}

	@Override
	public void verifyOnclickGetStartedButton() {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWeb(driver, LandingPageLocators.inputConfirmationNumber, 120);
			if (!(LandingPageLocators.btnGetStarted.isEnabled())) {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.PASS,
						"GetStarted button is disabled before filling all mandatory details");
			} else {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.FAIL,
						"GetStarted button is enabled before filling all details");
			}
			LandingPageLocators.inputConfirmationNumber.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeys((RemoteWebDriver) driver, "ConfirmationNumber", testData.get("ConfirmationNumber"));
			LandingPageLocators.inputLastName.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeys((RemoteWebDriver) driver, "LastName", testData.get("LastName"));
			LandingPageLocators.inputCheckInDate.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeys((RemoteWebDriver) driver, "Check-in Date", testData.get("CheckInDate"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollDownForElementJSWb(LandingPageLocators.btnGetStarted);
			if (LandingPageLocators.btnGetStarted.isEnabled()) {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.PASS,
						"GetStarted button is getting enabled after filling all mandatory details");
			} else {
				tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.FAIL,
						"GetStarted button is not getting enabled after filling all mandatory details");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("LandingPage", "verifyOnclickGetStartedButton", Status.FAIL,
					"Getting error :" + e);
		}
	}

	@Override
	public void enterReservationDetails() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.inputConfirmationNumber, 120);
		scrollUpByPixel(500);
		LandingPageLocators.inputConfirmationNumber.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "ConfirmationNumber", testData.get("ConfirmationNumber"));
		LandingPageLocators.inputLastName.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "LastName", testData.get("LastName"));
		LandingPageLocators.inputCheckInDate.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "Check-in Date", testData.get("CheckInDate"));
		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		// clickElementJS(imgCalendar);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	@Override
	public void verifyFooterAndTCLandingPage() {
		waitUntilElementVisibleWeb(driver, LandingPageLocators.footerLandingPage, 120);
		scrollDownForElementJSWb(LandingPageLocators.footerLandingPage);
		if (LandingPageLocators.footerLandingPage.isDisplayed()) {
			if (LandingPageLocators.lnkTermsAndCondition.isDisplayed()) {
				String URL = driver.getCurrentUrl();
				scrollDownForElementJSWb(LandingPageLocators.lnkTermsAndCondition);
				clickIOSElement("Terms and Conditions", 1, LandingPageLocators.lnkTermsAndCondition);
				// clickElementWb(LandingPageLocators.lnkTermsAndCondition);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// List<String> windows = new
				// ArrayList<String>(driver.getWindowHandles());
				// driver.switchTo().window(windows.get(1));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(LandingPageLocators.termsAndConditionHeader)) {
					tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.PASS,
							"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
				} else {
					tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
							"Terms & condition link is not opening in another tab");
				}
				driver.get(URL);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// driver.switchTo().window(window);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
						"Terms & condition link is not present in landing page");
			}
		} else {
			tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
					"Footer is not present in landing page");
		}
	}

	@Override
	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(LandingPageLocators.lnkTermsOfUse);
			links.add(LandingPageLocators.lnkPrivacyNotice);
			links.add(LandingPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String URL = driver.getCurrentUrl();
					scrollDownForElementJSWb(links.get(i));
					scrollDownByPixel(50);
					if (links.get(i).getText().equalsIgnoreCase("Terms of Use")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Terms of Use", 1, links.get(i));
						} else {
							clickIOSElement("Terms of", 1, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("Privacy Notice")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Notice", 1, links.get(i));
						} else {
							clickIOSElement("Notice", 2, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("privacy Setting")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Setting", 1, links.get(i));
						} else {
							clickIOSElement("Setting", 1, links.get(i));
						}
					}

					// clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					// List<String> windows = new
					// ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(LandingPageLocators.headerOtherPages)
							&& LandingPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| LandingPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
								"Llink is not opening in another tab");
					}
					// driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.get(URL);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("LandingPage", "verifyFooterAndTCLandingPage", Status.FAIL,
					"Getting Error: " + e);
		}
	}

	@Override
	public void videoPlayerValidation() {

		try {
			String strImgLogo = driver.findElement(imageLogoTxt).getAttribute("alt");
			String strImgLogoTxt[] = getCapitalizedWord(strImgLogo.toLowerCase()).split(" ");
			waitUntilElementVisibleBy(driver, videoPlayerBtn, 120);
			if (verifyElementDisplayed(driver.findElement(videoPlayerBtn))) {
				tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.PASS,
						"Video Player button is displayed");
				clickElementBy(videoPlayerBtn);
				waitUntilObjectVisible(driver, videoTitle, 120);
				if (verifyElementDisplayed(driver.findElement(videoTitle))) {
					tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.PASS,
							"Video Title is displayed successfully");
					// driver.switchTo().frame(driver.findElement(By.id("videoPlayer")));
					/*
					 * if (verifyElementDisplayed(driver.findElement(By.
					 * xpath("//a[contains(text(),'Self-Service Tours - " +
					 * strImgLogoTxt[0].substring(0, 3) + "')]")))) {
					 * tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation",
					 * Status.PASS, "Video Player header title " + strImgLogo + " is displayed ");
					 * if (verifyElementDisplayed(driver.findElement(videoPlayBtn))) {
					 * tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation",
					 * Status.PASS, "Play button is present in the Video Player");
					 * driver.findElement( By.xpath("//a[contains(text(),'Self-Service Tours - " +
					 * strImgLogoTxt[0].substring(0, 3) + "')]")) .click(); List<String> windows =
					 * new ArrayList<String>(driver.getWindowHandles());
					 * driver.switchTo().window(windows.get(1));
					 * waitForSometime(tcConfig.getConfig().get("LowWait")); String strwindowTitle =
					 * driver.getTitle(); System.out.println(strwindowTitle); if
					 * (strwindowTitle.toUpperCase().contains(strImgLogo.toUpperCase()+" "+testData.
					 * get("VideoTabTitle").toUpperCase())) {
					 * tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation",
					 * Status.PASS, "Video is opened successfully in another tab");
					 * waitForSometime(tcConfig.getConfig().get("LowWait")); driver.close();
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * driver.switchTo().window(windows.get(0));
					 * 
					 * } else { tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation",
					 * Status.FAIL, "Video is not opened"); } } else {
					 * tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation",
					 * Status.FAIL, "Play button is not present in the Video Player"); } } else {
					 * tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation",
					 * Status.FAIL, "Video Player header title is not displayed "); }
					 */

				} else {
					tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
							"Video Title is not displayed");
				}

				// driver.switchTo().defaultContent();
				waitUntilObjectVisible(driver, videoCloseBtn, 120);
				clickElementBy(videoCloseBtn);

			} else {
				tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
						"Video Player button is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("LandingPage", "videoPlayerValidation", Status.FAIL,
					"Failed in method" + e.getMessage());
		}
	}

	@Override
	public void clickGetStartedBtn() {

		waitUntilObjectVisible(driver, LandingPageLocators.btnGetStarted, 120);
		LandingPageLocators.btnGetStarted.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

}