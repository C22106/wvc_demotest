package aem.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OfferManagementsPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(OfferManagementsPage.class);

	public OfferManagementsPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// OfferManagementsPageLocators.class);

	}

	OfferManagementsPageLocators OfferManagementsPageLocators = new OfferManagementsPageLocators(tcConfig);

	public static String offerName = "";

	final String CHARACTER_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public void clickLinkOfferIncentive() {
		waitUntilElementVisibleWeb(driver, OfferManagementsPageLocators.lnkLinkOfferIncentive, 120);
		clickElementWb(OfferManagementsPageLocators.lnkLinkOfferIncentive);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public String linkIncentiveToOffer() {
		String rndText = "";
		try {
			waitUntilElementVisibleWeb(driver, OfferManagementsPageLocators.txtIncentiveName, 120);
			fieldDataEnter(OfferManagementsPageLocators.txtIncentiveName, testData.get("IncentiveName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement e = driver.findElement(By.xpath("//span[@title='" + testData.get("IncentiveName") + "']/.."));
			e.click();
			Date todaysDate = Calendar.getInstance().getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			String today = sdf.format(todaysDate);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, 1);
			Date date = cal.getTime();
			String endDate = sdf.format(date);

			fieldDataEnter(OfferManagementsPageLocators.txtEffectiveDate, today);
			fieldDataEnter(OfferManagementsPageLocators.txtEndDate, endDate);
			new Select(OfferManagementsPageLocators.drpDwnChoiceGrouping).selectByIndex(1);
			fieldDataEnter(OfferManagementsPageLocators.txtQuantityOffered, "1");
			rndText = testData.get("IncentiveName") + getRandomString(2);
			fieldDataEnter(OfferManagementsPageLocators.txtDisplayName, rndText);
			clickElementWb(OfferManagementsPageLocators.btnSave.get(OfferManagementsPageLocators.btnSave.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleWeb(driver, OfferManagementsPageLocators.divSuccessMsg, 120);
			if (verifyElementDisplayed(OfferManagementsPageLocators.divSuccessMsg)) {
				tcConfig.updateTestReporter("OfferManagementPage", "linkIncentiveToOffer", Status.PASS,
						"Incentive is successfully linked to offer");
			} else {
				tcConfig.updateTestReporter("OfferManagementPage", "linkIncentiveToOffer", Status.FAIL,
						"Failed while linking incentive to offer");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferManagementPage", "linkIncentiveToOffer", Status.FAIL,
					"Getting error:" + e);
			if (rndText.equals("")) {
				rndText = "";
			}
		}
		return rndText;
	}

	public String editPremiumLinkedToOffer() {
		waitUntilElementVisibleWeb(driver, OfferManagementsPageLocators.lnkLinkOfferIncentive, 120);
		Actions a = new Actions(driver);
		a.moveToElement(OfferManagementsPageLocators.lnkShowMore.get(0)).click().build().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementWb(OfferManagementsPageLocators.lnkEdit.get(0));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		String oldText = OfferManagementsPageLocators.txtDisplayName.getAttribute("value");
		String newText = oldText + getRandomString(2);
		fieldDataEnter(OfferManagementsPageLocators.txtDisplayName, newText);
		clickElementWb(OfferManagementsPageLocators.btnSave.get(OfferManagementsPageLocators.btnSave.size() - 1));
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		return newText;
	}

	public void clickNewOfferBtn() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleWeb(driver, OfferManagementsPageLocators.btnNew, 120);
		clickElementWb(OfferManagementsPageLocators.btnNew);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void provideRefundableDepositAmount() {
		if (verifyElementDisplayed(OfferManagementsPageLocators.txtBoxRefundableDeposit)) {
			try {
				OfferManagementsPageLocators.txtBoxRefundableDeposit.click();
				OfferManagementsPageLocators.txtBoxRefundableDeposit.sendKeys(Keys.SHIFT, Keys.HOME, Keys.BACK_SPACE,
						"-50");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(OfferManagementsPageLocators.txtBoxRefundableDeposit, "-50");
				OfferManagementsPageLocators.txtBoxRefundableDeposit.sendKeys(Keys.ENTER);
				tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
						"verifyNonNegativeAmountNotAccepted", Status.PASS,
						"Provided negative value for refundable deposit");
			} catch (Exception e) {
				tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted", Status.FAIL,
						"Some fields are not visible");
			}

		} else {
			tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted", Status.FAIL,
					"Refundable deposit text box is not present");
		}
	}

	public void createOffer() {
		waitUntilElementVisibleWeb(driver, OfferManagementsPageLocators.headerNewOfferPage, 120);
		if (verifyElementDisplayed(OfferManagementsPageLocators.txtBoxSalesStore)) {
			try {
				fieldDataEnter(OfferManagementsPageLocators.txtBoxSalesStore, testData.get("SalesStore"));
				List<WebElement> e = driver
						.findElements(By.xpath("//div[@title='" + testData.get("SalesStore") + "']/.."));
				if (e.size() > 0) {
					waitUntilElementVisibleWeb(driver, e.get(0), 120);
					e.get(0).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS,
							"Salestore is selected is " + testData.get("SalesStore"));
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "Selected salestore is not coming");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(OfferManagementsPageLocators.txtBoxOfferName)) {
					offerName = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(OfferManagementsPageLocators.txtBoxOfferName, offerName);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS, "Offer name provided is " + offerName);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "Offer Name textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(OfferManagementsPageLocators.txtBoxEffectiveEndDate.get(0))) {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					Date d = new Date();
					String todayDate = sdf.format(d);
					fieldDataEnter(OfferManagementsPageLocators.txtBoxEffectiveEndDate.get(0), todayDate);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS,
							"Effective Date is provided " + todayDate);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "Effective Date is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(OfferManagementsPageLocators.txtBoxEffectiveEndDate.get(1))) {
					String endDate = getFutureDate(Integer.parseInt(testData.get("EndDateinDays")), "MM/dd/yyyy");
					fieldDataEnter(OfferManagementsPageLocators.txtBoxEffectiveEndDate.get(1), endDate);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS, "End Date is provided " + endDate);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "End Date is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementWb(OfferManagementsPageLocators.txtBoxOfferName);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(OfferManagementsPageLocators.txtAreaOfferPaymentDetails)) {
					String offerPaymentDetails = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(OfferManagementsPageLocators.txtAreaOfferPaymentDetails, offerPaymentDetails);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS,
							"Offer payment details provided is " + offerPaymentDetails);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "Offer payment details textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(OfferManagementsPageLocators.txtBoxPaymentPageDetails)) {
					String PaymentDetails = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(OfferManagementsPageLocators.txtBoxPaymentPageDetails, PaymentDetails);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS,
							"Offer payment details provided is " + PaymentDetails);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "Offer payment details textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(OfferManagementsPageLocators.txtAreaOfferDescription)) {
					String offerDescription = generateRandomString(5, CHARACTER_STRING);
					fieldDataEnter(OfferManagementsPageLocators.txtAreaOfferDescription, offerDescription);
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS,
							"Offer description provided is " + offerDescription);
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "Offer description textbox is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(OfferManagementsPageLocators.listAvailableDays.get(0))) {
					for (int i = 0; i < Integer.parseInt(testData.get("AvailableDays")); i++) {
						clickElementWb(OfferManagementsPageLocators.listAvailableDays.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementWb(OfferManagementsPageLocators.listMoveBtn
								.get(OfferManagementsPageLocators.listMoveBtn.size() - 2));
					}
					tcConfig.updateTestReporterWithoutScreenshot("OfferManagementPage",
							"verifyNonNegativeAmountNotAccepted", Status.PASS, "Selected available day");
				} else {
					tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted",
							Status.FAIL, "Available day is not present");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted", Status.FAIL,
						"Some fields are not visible");
			}

		} else {
			tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted", Status.FAIL,
					"Salestore field is not visible");
		}
	}

	public void clickSave() {
		if (verifyElementDisplayed(
				OfferManagementsPageLocators.btnSave.get(OfferManagementsPageLocators.btnSave.size() - 1))) {
			clickElementWb(OfferManagementsPageLocators.btnSave.get(OfferManagementsPageLocators.btnSave.size() - 1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted", Status.FAIL,
					"Save button is not visible");
		}
	}

	public void verifyNonNegativeAmountNotAccepted() {
		if (verifyElementDisplayed(OfferManagementsPageLocators.txtError)) {
			tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted", Status.PASS,
					"Getting error for negative refundable deposit " + OfferManagementsPageLocators.txtError.getText());
		} else {
			tcConfig.updateTestReporter("OfferManagementPage", "verifyNonNegativeAmountNotAccepted", Status.FAIL,
					"Not getting error for negative refundable deposit "
							+ OfferManagementsPageLocators.txtError.getText());
		}
	}

	public void verifyNewlyCreatedOffer() {
		try {
			waitUntilElementVisibleWeb(driver, OfferManagementsPageLocators.headerOfferManagement, 120);
			if (OfferManagementsPageLocators.lblOfferName.getText().equals(offerName)) {
				tcConfig.updateTestReporter("OfferManagementPage", "verifyNewlyCreatedOffer", Status.PASS,
						"New offer is created successfully");
			} else {
				tcConfig.updateTestReporter("OfferManagementPage", "verifyNewlyCreatedOffer", Status.FAIL,
						"Not able to create offer");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferManagementPage", "verifyNewlyCreatedOffer", Status.FAIL,
					"Getting error:" + e);
		}
	}
}
