package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class TourRecordPageLocators {

	public TourRecordPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//span[text()='Booked']/../../../th//a")
	public WebElement lnkBookedTour;

	@FindBy(xpath = "//span[contains(text(),'SS Guest First Name')]/../../div[2]/span")
	public WebElement txtGuestFirstName;

	@FindBy(xpath = "//span[contains(text(),'SS Guest Last Name')]/../../div[2]/span")
	public WebElement txtGuestLastName;

	@FindBy(xpath = "//span[text()='Booked']/../../../th//a")
	public List<WebElement> lnkBookedRecord;

	@FindBy(xpath = "//h1[@title='Tour Records']")
	public WebElement headerTourRecord;

	@FindBy(xpath = "//div[@class='slds-cell-fixed']//span[@title='Tour Record Number']")
	public WebElement lnkSortTourRecord;

	@FindBy(xpath = "//span[text()='Self Service Flag']/../../div[2]//img")
	public List<WebElement> chkSelfServiceFlag;

	@FindBy(xpath = "//button[text()='Book Tour']")
	public WebElement btnBookTour;

	@FindBy(xpath = "//a[contains(@href,'Tours__r')]//span[contains(@class,'view-all')]")
	public WebElement lnkViewAllTours;

	@FindBy(xpath = "//ul[contains(@class,'branding-actions')]//a[@title='Edit']")
	public WebElement lnkEdit;

	@FindBy(xpath = "//span[text()='Arrival Details']")
	public WebElement btnArrivalDetails;
}
