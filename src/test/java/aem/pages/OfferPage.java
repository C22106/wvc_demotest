package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OfferPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(ReservationPage.class);

	public OfferPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), OfferPageLocators.class);

	}

	OfferPageLocators OfferPageLocators = new OfferPageLocators(tcConfig);

	public void verifyOfferPageHeader() {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.lblOffersHeader, 120);
			if (verifyElementDisplayed(OfferPageLocators.lblOffersHeader)) {
				tcConfig.updateTestReporter("OfferPage", "verifyOfferPageHeader", Status.PASS,
						"Navigated To OfferPage");

				if (OfferPageLocators.lblOffersHeader.getText().contains(testData.get("FirstName"))) {
					tcConfig.updateTestReporter("OfferPage", "verifyOfferPageHeader", Status.PASS,
							"Offer page header is displayed correctly");
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyOfferPageHeader", Status.FAIL,
							"Offer page header is not displayed");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyOfferPageHeader", Status.FAIL,
						"Offer header is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyOfferPageHeader", Status.FAIL, "Getting error: " + e);
		}
	}

	public void verifyGetDetailsLink() {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.lnkGetDetails, 120);
			if (verifyElementDisplayed(OfferPageLocators.lnkGetDetails)) {
				tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLink", Status.PASS,
						"Get details link is displayed correctly");
				clickElementWb(OfferPageLocators.lnkGetDetails);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyElementDisplayed(OfferPageLocators.fieldPremiumDescription)) {
					tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLink", Status.PASS,
							"Get details link is navigating correctly to offer description");
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLink", Status.FAIL,
							"Get details link is not navigating to offer description");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLink", Status.FAIL,
						"Get details link is not displayed correctly");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLink", Status.FAIL, "Getting error: " + e);
		}
	}

	public void verifyExclusiveOfferMsg() {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.fieldUserMsg, 120);
			if (verifyElementDisplayed(OfferPageLocators.fieldUserMsg)) {
				if (OfferPageLocators.fieldUserMsg.getText()
						.contains(testData.get("FirstName") + " " + testData.get("LastName"))) {
					tcConfig.updateTestReporter("OfferPage", "verifyExclusiveOfferMsg", Status.PASS,
							"Exclusive offer message is displayed correctly");
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyExclusiveOfferMsg", Status.FAIL,
							"Exclusive offer message is not displayed correctly");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyExclusiveOfferMsg", Status.FAIL,
						"Exclusive offer message is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyExclusiveOfferMsg", Status.FAIL, "Getting error: " + e);
		}
	}

	public void verifyPremiumDisplayed() {
		if (OfferPageLocators.lblOffersName.size() > 0) {
			tcConfig.updateTestReporter("OfferPage", "verifyPremiumDisplayed", Status.PASS,
					"Premiums are displayed in offer page");
		} else {
			tcConfig.updateTestReporter("OfferPage", "verifyPremiumDisplayed", Status.FAIL,
					"Premiums are not displayed in offer page");
		}
	}

	public void verifyFooterAndTCOfferPage() {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.chkBoxOffer.get(0), 120);
			if (verifyElementDisplayed(OfferPageLocators.footerOfferPage)) {
				scrollDownForElementJSWb(OfferPageLocators.footerOfferPage);
				if (verifyElementDisplayed(OfferPageLocators.lnkTermsAndCondition)) {
					String window = driver.getWindowHandle();
					clickElementJSWithWait(OfferPageLocators.lnkTermsAndCondition);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					if (windows.size() == 2) {
						driver.switchTo().window(windows.get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyElementDisplayed(OfferPageLocators.termsAndConditionHeader)) {
							tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.PASS,
									"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
						} else {
							tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
									"Terms & condition link is not opening in another tab");
						}
						driver.close();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().window(window);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						if (verifyElementDisplayed(OfferPageLocators.termsAndConditionHeader)) {
							tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.PASS,
									"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
						} else {
							tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
									"Terms & condition link is not opening in another tab");
						}
						driver.navigate().back();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
							"Terms & condition link is not present in landing page");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
						"Footer is not present in landing page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL, "Getting error: " + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(OfferPageLocators.lnkTermsOfUse);
			links.add(OfferPageLocators.lnkPrivacyNotice);
			links.add(OfferPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(OfferPageLocators.headerOtherPages)
							&& OfferPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| OfferPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}

	public List<String> selectPremium() {
		List<String> premiumName = new ArrayList<String>();
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.fieldUserMsg, 120);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyElementDisplayed(OfferPageLocators.lblOffersHeader)) {
				scrollDownForElementJSWb(OfferPageLocators.lblOffersHeader);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				int size = OfferPageLocators.lstOfferSections.size();
				for (int i = 1; i <= size; i++) {
					if (!(driver.findElements(By.xpath("(//p[text()='Choose one']/..)[" + i + "]/div//span/../.."))
							.get(0).getAttribute("class").contains("active"))) {
						driver.findElements(By.xpath("(//p[text()='Choose one']/..)[" + i + "]/div//span")).get(0)
								.click();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						premiumName.add(driver
								.findElements(By.xpath("(//p[text()='Choose one']/..)[" + i + "]/div//div[2]//h4"))
								.get(0).getText());
					} else {
						premiumName.add(driver
								.findElements(By.xpath("(//p[text()='Choose one']/..)[" + i + "]/div//div[2]//h4"))
								.get(0).getText());
					}
				}
				tcConfig.updateTestReporter("OfferPage", "selectPremium", Status.PASS,
						"One premium is getting selected from each offer section");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("OfferPage", "selectPremium", Status.FAIL, "Offer page is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "selectPremium", Status.FAIL, "Getting error:" + e);
		}

		return premiumName;
	}

	public void clickContinueOffers() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.btnContinueOffer, 120);
			if (verifyElementDisplayed(OfferPageLocators.btnContinueOffer)) {
				js.executeScript("window.scrollBy(0,450)");
				if (OfferPageLocators.btnContinueOffer.isEnabled()) {
					tcConfig.updateTestReporter("OfferPage", "clickContinueOffers", Status.PASS,
							"Continue button is getting enabled after selecting premiums");
					clickElementWb(OfferPageLocators.btnContinueOffer);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("OfferPage", "clickContinueOffers", Status.FAIL,
							"Continue button is not getting enabled after selecting premiums");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "clickContinueOffers", Status.FAIL,
						"Continue button is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "clickContinueOffers", Status.FAIL, "Getting error:" + e);
		}

	}

	public void verifyPleaseClickHereLink() {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.lnkPleaseClickHere, 120);
			if (verifyElementDisplayed(OfferPageLocators.lnkPleaseClickHere)) {
				tcConfig.updateTestReporter("OfferPage", "verifyPleaseClickHereLink", Status.PASS,
						"Please click here link is presernt in offer page");
				clickElementWb(OfferPageLocators.lnkPleaseClickHere);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleWeb(driver, OfferPageLocators.lblLandingHeader, 120);
				tcConfig.updateTestReporter("LandingPage", "LandingPageValidation", Status.PASS,
						"Navigated To LandPage");

				if (OfferPageLocators.lblLandingHeader.isDisplayed()) {
					tcConfig.updateTestReporter("LandingPage", "LandingPageHeaderValidation", Status.PASS,
							"User is navigated to Landing page after clicking Click here link in offers page");
				} else {
					tcConfig.updateTestReporter("LandingPage", "LandingPageHeaderValidation", Status.FAIL,
							"User is not navigated to Landing page after clicking Click here link in offers page");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyPleaseClickHereLink", Status.FAIL,
						"Please click here link is not presernt in offer page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("LandingPage", "LandingPageHeaderValidation", Status.FAIL,
					"Getting error:" + e);
		}
	}

	public void verifyPremiumNamesInRightRail(List<String> names) {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.fieldSelectedIncentives.get(0), 120);
			if (verifyElementDisplayed(OfferPageLocators.fieldSelectedIncentives.get(0))) {
				for (int i = 0; i < OfferPageLocators.fieldSelectedIncentives.size(); i++) {
					if (OfferPageLocators.fieldSelectedIncentives.get(i).getText().equals(names.get(i))) {
						tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.PASS,
								"Premiums are coming in right rail after selecting");
					} else {
						tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.FAIL,
								"Premiums selected are not correct");
					}
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.FAIL,
						"Premiums are not visible in right rail after selecting");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyPremiumNamesInRightRail", Status.FAIL,
					"Getting error:" + e);
		}
	}

	public void verifyGetDetailsLinkOnclick() {
		waitUntilElementVisibleWeb(driver, OfferPageLocators.lnkGetDetails, 120);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Long value = (Long) js.executeScript("return window.pageYOffset;");
		js.executeScript("window.scrollBy(0,350)");
		clickElementWb(OfferPageLocators.lnkGetDetails);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Long value1 = (Long) js.executeScript("return window.pageYOffset;");
		if (value != value1 && OfferPageLocators.fieldPremiumDescription.isDisplayed()) {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkOnclick", Status.PASS,
					"Premium disclosure section is visible after clicking the get details link");
		} else {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkOnclick", Status.FAIL,
					"Premium disclosure section is not visible after clicking the get details link");
		}
	}

	public void verifyAddedPremium(String incentiveName) {
		int count = 0;
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.chkBoxOffer.get(0), 120);
			if (verifyElementDisplayed(OfferPageLocators.chkBoxOffer.get(0))) {
				int size = OfferPageLocators.lblOffersName.size();

				for (int i = 0; i < size; i++) {
					if (OfferPageLocators.lblOffersName.get(i).getText().equals(incentiveName)) {
						tcConfig.updateTestReporter("OfferPage", "verifyAddedPremium", Status.PASS,
								"Premium added in journey is reflecting in AEM.");
						break;
					} else {
						count++;
					}
				}
				if (count == size) {
					tcConfig.updateTestReporter("OfferPage", "verifyAddedPremium", Status.FAIL,
							"Premium added in journey is not reflecting in AEM.");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyAddedPremium", Status.FAIL, "Premiums are not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyAddedPremium", Status.FAIL, "Getting error:" + e);
		}
	}

	public void verifyRefundableDeposit(String flag) {
		waitUntilElementVisibleWeb(driver, OfferPageLocators.fieldUserMsg, 120);
		if (flag.equalsIgnoreCase("Y")) {
			if (verifyElementDisplayed(OfferPageLocators.fieldRefundableDeposit)) {
				tcConfig.updateTestReporter("OfferPage", "verifyRefundableDeposit", Status.PASS,
						"Refundable deposit is present for states other than California");
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyRefundableDeposit", Status.FAIL,
						"Refundable deposit is not present for states other than California");
			}
		} else {
			if (verifyElementDisplayed(OfferPageLocators.fieldRefundableMeal)) {
				tcConfig.updateTestReporter("OfferPage", "verifyRefundableDeposit", Status.PASS,
						"Refundable Meal deposit is present for California state");
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyRefundableDeposit", Status.FAIL,
						"Refundable Meal deposit is not present for California state");
			}
		}
	}

	public void verifyGetDetailsLinkNotPresent() {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.lblOffersHeader, 120);
			if (verifyElementDisplayed(OfferPageLocators.lnkGetDetails)) {
				tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkNotPresent", Status.PASS,
						"Get details link is not visible for premium with no premium disclosure");
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkNotPresent", Status.FAIL,
						"Get details link is visible for premium with no premium disclosure");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkNotPresent", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void openTripSummary() {
		// TODO Auto-generated method stub

	}

	public void closeTripSummary() {
		// TODO Auto-generated method stub

	}
}
