package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class AudiencePageLocators {

	public AudiencePageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//th[text()='Offer Code']/../../../tbody/tr[1]/th[1]//a")
	public WebElement lnkOfferCode;

	@FindBy(xpath = "//a[@title='New']")
	public WebElement lnkNew;

	@FindBy(xpath = "//h2[text()='New Audience']")
	public WebElement headerNewAud;

	@FindBy(xpath = "//span[text()='Audience Code']/../../input")
	public WebElement txtBoxAudienceCode;

	@FindBy(xpath = "//span[text()='Audience Name']/../../input")
	public WebElement txtBoxAudienceName;

	@FindBy(xpath = "//span[text()='Audience Description']/../../input")
	public WebElement txtBoxAudienceDesc;

	@FindBy(xpath = "//button[@title='Save']")
	public WebElement btnSave;

	@FindBy(xpath = "//div[@class='forceVisualMessageQueue']//div[@data-key='success']")
	public WebElement divSuccessMsg;

	@FindBy(xpath = "//a[@title='Edit']")
	public WebElement btnEdit;

	@FindBy(xpath = "//div[@class='actionsContainer']//a[@title='New']")
	public WebElement btnNewOffer;

	@FindBy(xpath = "//h2[text()='New Audience Management']")
	public WebElement headerNewAudMangmnt;

	@FindBy(xpath = "//input[contains(@title,'Search Offer Management')]")
	public WebElement txtBoxOfferCode;

	@FindBy(xpath = "//span[@title='New Offer Management']/..")
	public WebElement lnkNewOfferManagement;

	@FindBy(xpath = "//h2[text()='New Offer Management']")
	public WebElement headerNewOfferPage;

	@FindBy(xpath = "//input[@title='Search Sales Stores']")
	public WebElement txtBoxSalesStore;

	@FindBy(xpath = "//span[text()='Offer Name']/../../input")
	public WebElement txtBoxOfferName;

	@FindBy(xpath = "//div[contains(@class,'datetime')]/div/input")
	public List<WebElement> txtBoxEffectiveEndDate;

	@FindBy(xpath = "//span[text()='Offer Payment Details']/../../textarea")
	public WebElement txtAreaOfferPaymentDetails;

	@FindBy(xpath = "//span[contains(text(),'Payment Page')]/../../input")
	public WebElement txtBoxPaymentPageDetails;

	@FindBy(xpath = "//span[contains(text(),'Offer Long')]/../../textarea")
	public WebElement txtAreaOfferDescription;

	@FindBy(xpath = "//div[contains(@data-value,'day')]/..")
	public List<WebElement> listAvailableDays;

	@FindBy(xpath = "//button[contains(@title,'Move selection to')]")
	public List<WebElement> listMoveBtn;

	@FindBy(xpath = "//button[@title='Save'] | //button[text()='Save']")
	public List<WebElement> btnSaveOffer;

	@FindBy(xpath = "//span[text()='Active']/../../input")
	public WebElement chkBoxActive;

	@FindBy(xpath = "//div[contains(@class,'actionBarPlugin')]//a[contains(@class,'textUnderline')]")
	public WebElement lnkLinkedOfferCode;
}
