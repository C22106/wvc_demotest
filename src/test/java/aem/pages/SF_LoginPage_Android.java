package aem.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class SF_LoginPage_Android extends SF_LoginPage {

	public static final Logger log = Logger.getLogger(SF_LoginPage_Android.class);
	ReadConfigFile config = new ReadConfigFile();
	SF_LoginPageLocators SF_LoginPageLocators = new SF_LoginPageLocators(tcConfig);
	LandingPageLocators LandingPageLocators = new LandingPageLocators(tcConfig);

	public SF_LoginPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), SF_LoginPageLocators.class);
	}

	public void launchWithoutData(String strBrowser, String url) {
		this.driver = checkAndInitBrowser(strBrowser);
		String appUrl = "";
		switch (url) {
		case "QA":
			appUrl = config.get("AEM_URL_QA").trim();
			break;
		case "PROD_WM":
			appUrl = config.get("AEM_URL_PROD_WM").trim();
			break;
		case "PROD_CW":
			appUrl = config.get("AEM_URL_PROD_CW").trim();
			break;
		case "STG_WM":
			appUrl = config.get("AEM_URL_STG_WM").trim();
			break;
		case "STG_CW":
			appUrl = config.get("AEM_URL_STG_CW").trim();
			break;
		}
		// String url=config.get("AEM_URL_QA").trim();
		driver.navigate().to(appUrl);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().refresh();
		// driver.manage().window().maximize();
		// waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.lblHeader, 120);
		enterReservationDetails();
		clickGetStartedBtn();
	}

	public void enterReservationDetails() {
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.inputConfirmationNumber, 120);
		LandingPageLocators.inputConfirmationNumber.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "ConfirmationNumber", testData.get("ConfirmationNumber"));
		LandingPageLocators.inputLastName.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "LastName", testData.get("LastName"));
		LandingPageLocators.inputCheckInDate.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "Check-in Date", testData.get("CheckInDate"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		/*
		 * scrollDownForElementJSWb(LandingPageLocators.btnGetStarted);
		 * waitForSometime(tcConfig.getConfig().get("LowWaits"));
		 */
	}
}