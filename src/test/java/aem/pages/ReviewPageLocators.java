package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class ReviewPageLocators {

	public ReviewPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//a[@title='Terms and Conditions']")
	public WebElement lnkTermsAndCondition;

	@FindBy(xpath = "//div[contains(@class,'wyn-footer__advertisingdisclaimer')]")
	public WebElement footerReviewPage;

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public WebElement lblReviewHeader;

	@FindBy(xpath = "//span[text()='Schedule']/../../../h5 | //span[text()='TIME']/../../../h5")
	public List<WebElement> fieldScheduleDateAndTime;

	@FindBy(xpath = "//span[text()='Incentives']/../div//li | //span[text()='INCENTIVES']/../div//li")
	public List<WebElement> fieldsIncentives;

	@FindBy(xpath = "//button[text()='BOOK NOW']")
	public WebElement btnBookNow;

	@FindBy(xpath = "//div[@class='wyn-rich-text wyn-l-content']//h1")
	public WebElement termsAndConditionHeader;

	@FindBy(xpath = "//b[text()='Terms of Use']/..")
	public WebElement lnkTermsOfUse;

	@FindBy(xpath = "//b[text()='Privacy Notice']/..")
	public WebElement lnkPrivacyNotice;

	@FindBy(xpath = "//b[text()='Privacy Setting']/..")
	public WebElement lnkPrivacySetting;

	@FindBy(xpath = "//div[@id='main-content']//h1")
	public WebElement headerOtherPages;

	@FindBy(xpath = "//div[@class='grad']/h1")
	public WebElement headerPrivacySetting;
}
