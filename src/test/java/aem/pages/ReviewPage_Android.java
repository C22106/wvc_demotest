package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ReviewPage_Android extends ReviewPage {

	public static final Logger log = Logger.getLogger(HomePage_Android.class);

	public ReviewPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), ReviewPageLocators.class);

	}

	public void verifyFooterAndTCReviewPage() {
		waitUntilElementVisibleWeb(driver, ReviewPageLocators.footerReviewPage, 120);
		if (verifyElementDisplayed(ReviewPageLocators.footerReviewPage)) {
			if (verifyElementDisplayed(ReviewPageLocators.lnkTermsAndCondition)) {
				String window = driver.getWindowHandle();
				clickElementWb(ReviewPageLocators.lnkTermsAndCondition);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<String> windows = new ArrayList<String>(driver.getWindowHandles());
				/// driver.switchTo().window(windows.get(1));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(ReviewPageLocators.termsAndConditionHeader)) {
					tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.PASS,
							"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
				} else {
					tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.FAIL,
							"Terms & condition link is not opening in another tab");
				}
				// driver.close();
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				// driver.switchTo().window(window);
				browserBack((RemoteWebDriver) driver);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.FAIL,
						"Terms & condition link is not present in Review page");
			}
		} else {
			tcConfig.updateTestReporter("ReviewPage", "verifyFooterAndTCReviewPage", Status.FAIL,
					"Footer is not present in Review page");
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(ReviewPageLocators.lnkTermsOfUse);
			links.add(ReviewPageLocators.lnkPrivacyNotice);
			links.add(ReviewPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(ReviewPageLocators.headerOtherPages)
							&& ReviewPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| ReviewPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					// driver.close();
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					browserBack((RemoteWebDriver) driver);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("ReviewPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}
}