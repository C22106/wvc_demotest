package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class ExistingTourPageLocators {

	public ExistingTourPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//a[text()='Edit Owner Update']")
	public WebElement lnkEditTour;

	@FindBy(xpath = "//span[text()='Schedule']/../../../h5 | //h5[contains(@class,'wyn-type-title-2 wyn-l')]")
	public List<WebElement> fieldScheduledDateAndTime;

	@FindBy(xpath = "//span[text()='Additional Guest']/../../../h5/span")
	public WebElement fieldAdditionalGuest;

	@FindBy(xpath = "//span[text()='Incentives']/../div//strong | //div[starts-with(@class,'self-service__grey-area')]//strong")
	public List<WebElement> fieldSelectedIncentives;

	@FindBy(xpath = "//a[contains(@class,'cancel')]")
	public WebElement lnkCancelHere;

	@FindBy(xpath = "//div[contains(@class,'ReactModal__Content')]/h1")
	public WebElement divCancelPopup;

	@FindBy(xpath = "//div[contains(@class,'ReactModal__Content')]/div[1]/button")
	public WebElement btnYesCancel;

	@FindBy(xpath = "//div[contains(@class,'ReactModal__Content')]/div[2]/button")
	public WebElement btnNoCancel;
}
