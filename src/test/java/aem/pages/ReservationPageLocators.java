package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class ReservationPageLocators {

	public ReservationPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//span[text()='Audience']/../../div[2]//a")
	public WebElement lnkAudienceCode;

	@FindBy(xpath = "//span[text()='View All']/..")
	public List<WebElement> lnkViewAll;

	@FindBy(xpath = "//input[contains(@name,'SSSelfServiceFlag')]")
	public WebElement chkBoxSstFlag;

	@FindBy(xpath = "//input[contains(@name,'SSInteraction')]")
	public WebElement chkBoxSsIFlag;

	@FindBy(xpath = "//a[contains(@href,'Tours__r')]//span[contains(@class,'view-all')]")
	public WebElement lnkViewAllTours;

	@FindBy(xpath = "//ul[contains(@class,'branding-actions')]//a[@title='Edit']")
	public WebElement lnkEdit;

	@FindBy(xpath = "//span[text()='Arrival Details']")
	public WebElement btnArrivalDetails;

	@FindBy(xpath = "//span[text()='Booked']/../../../th//a")
	public List<WebElement> lnkBookedRecord;

	@FindBy(xpath = "//h1[@title='Tour Records']")
	public WebElement headerTourRecord;

	@FindBy(xpath = "//div[@class='slds-cell-fixed']//span[@title='Tour Record Number']")
	public WebElement lnkSortTourRecord;
}
