package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OfferPage_IOS extends OfferPage {

	public static final Logger log = Logger.getLogger(ReservationPage_IOS.class);

	public OfferPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), OfferPageLocators.class);

	}

	OfferPageLocators OfferPageLocators = new OfferPageLocators(tcConfig);

	public By viewTripSummary = By.xpath("//a[contains(text(),'View Trip Summary')]");

	public void openTripSummary() {
		waitUntilElementVisibleBy(driver, viewTripSummary, 120);
		driver.findElement(viewTripSummary).click();
	}

	public By closeTripSummary = By.xpath("//a[contains(@title,'Close Trip Summary')]");

	public void closeTripSummary() {
		waitUntilElementVisibleBy(driver, closeTripSummary, 120);
		driver.findElement(closeTripSummary).click();

	}

	public void verifyGetDetailsLinkOnclick() {
		waitUntilElementVisibleWeb(driver, OfferPageLocators.lnkGetDetails, 120);
		clickElementJSWithWait(OfferPageLocators.lnkGetDetails);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (OfferPageLocators.fieldPremiumDescription.isDisplayed()) {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkOnclick", Status.PASS,
					"Premium disclosure section is visible after clicking the get details link");
		} else {
			tcConfig.updateTestReporter("OfferPage", "verifyGetDetailsLinkOnclick", Status.FAIL,
					"Premium disclosure section is not visible after clicking the get details link");
		}
	}

	public void verifyFooterAndTCOfferPage() {
		try {
			waitUntilElementVisibleWeb(driver, OfferPageLocators.chkBoxOffer.get(0), 120);
			if (verifyElementDisplayed(OfferPageLocators.footerOfferPage)) {
				scrollDownForElementJSWb(OfferPageLocators.footerOfferPage);
				if (verifyElementDisplayed(OfferPageLocators.lnkTermsAndCondition)) {
					// String window = driver.getWindowHandle();
					String URL = driver.getCurrentUrl();
					scrollDownForElementJSWb(OfferPageLocators.lnkTermsAndCondition);
					clickIOSElement("our Terms and Conditions", 1, OfferPageLocators.lnkTermsAndCondition);
					// clickElementWb(OfferPageLocators.lnkTermsAndCondition);
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					// List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyElementDisplayed(OfferPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					// driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					driver.get(URL);
					// driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
							"Terms & condition link is not present in landing page");
				}
			} else {
				tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL,
						"Footer is not present in landing page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyFooterAndTCOfferPage", Status.FAIL, "Getting error: " + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(OfferPageLocators.lnkTermsOfUse);
			links.add(OfferPageLocators.lnkPrivacyNotice);
			links.add(OfferPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					// String window = driver.getWindowHandle();
					String URL = driver.getCurrentUrl();
					scrollDownForElementJSWb(links.get(i));
					scrollDownByPixel(50);
					if (links.get(i).getText().equalsIgnoreCase("Terms of Use")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Terms of Use", 1, links.get(i));
						} else {
							clickIOSElement("Terms of", 1, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("Privacy Notice")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Notice", 1, links.get(i));
						} else {
							clickIOSElement("Notice", 2, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("privacy Setting")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Setting", 1, links.get(i));
						} else {
							clickIOSElement("Setting", 1, links.get(i));
						}
					}
					// clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					// List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(OfferPageLocators.headerOtherPages)
							&& OfferPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| OfferPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.get(URL);
					// driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("OfferPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}
}