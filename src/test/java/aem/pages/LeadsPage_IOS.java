package aem.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class LeadsPage_IOS extends LeadsPage {

	public static final Logger log = Logger.getLogger(LeadsPage_IOS.class);

	public LeadsPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
}