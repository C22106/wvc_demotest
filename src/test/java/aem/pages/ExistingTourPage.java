package aem.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ExistingTourPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(ExistingTourPage.class);

	public ExistingTourPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// ExistingTourPageLocators.class);

	}

	@FindBy(xpath = "//a[text()='Edit tour']")
	protected WebElement lnkEditTour;

	ExistingTourPageLocators ExistingTourPageLocators = new ExistingTourPageLocators(tcConfig);

	public void verifyExistingTourPage() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisible(driver, ExistingTourPageLocators.lnkEditTour, 120);
		if (ExistingTourPageLocators.lnkEditTour.isDisplayed()) {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyExistingTourPage", Status.PASS,
					"Successfully navigated to Existing tour page");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyExistingTourPage", Status.FAIL,
					"Failed while navigating to existing tour page");
		}
	}

	public void clickEditTourLink() {
		waitUntilElementVisibleWeb(driver, ExistingTourPageLocators.lnkEditTour, 120);
		clickElementWb(ExistingTourPageLocators.lnkEditTour);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void verifyExistingTourDetails() {
		if (ExistingTourPageLocators.fieldScheduledDateAndTime.get(0).getText().contains(testData.get("TourDate"))
				&& ExistingTourPageLocators.fieldScheduledDateAndTime.get(1).getText()
						.equals(testData.get("TourTime"))) {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyExistingTourDetails", Status.PASS,
					"Selected tour date and time is visible in existing tour page");
		} else {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyExistingTourDetails", Status.FAIL,
					"Selected tour date and time is not visible in existing tour page");
		}
	}

	public void verifyExistingPremiumDetails() {
		int count = 0;
		//List<String> inc = new ArrayList<String>();
		String data = testData.get("IncentiveName");
		String[] datas = data.split(";");

		for (int i = 0; i < ExistingTourPageLocators.fieldSelectedIncentives.size(); i++) {
			for (String s : datas) {
				if (ExistingTourPageLocators.fieldSelectedIncentives.get(i).getText().contains(s)) {
					count++;
				}
			}
		}

		if (count == ExistingTourPageLocators.fieldSelectedIncentives.size()) {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyExistingTourDetails", Status.PASS,
					"Selected premiums are visible in existing tour page");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyExistingTourDetails", Status.FAIL,
					"Selected premiums are not visible in existing tour page");
		}
	}

	public void verifyAdditionalGuestDetails() {
		if (!testData.get("AdditionalGuest").equals("")) {
			if (ExistingTourPageLocators.fieldAdditionalGuest.getText().equals(testData.get("AdditionalGuest"))) {
				tcConfig.updateTestReporter("ExistingTourPage", "verifyAdditionalGuestDetails", Status.PASS,
						"Additional guest data is correct");
			} else {
				tcConfig.updateTestReporter("ExistingTourPage", "verifyAdditionalGuestDetails", Status.FAIL,
						"Additional guest data is not correct");
			}
		}
	}

	public void verifyCancelHereLink() {
		waitUntilElementVisibleWeb(driver, ExistingTourPageLocators.lnkCancelHere, 120);
		if (ExistingTourPageLocators.lnkCancelHere.isDisplayed()) {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyCancelHereLink", Status.PASS,
					"Cancel here link is displayed");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyCancelHereLink", Status.FAIL,
					"Cancel here link is not displayed");
		}
	}

	public void clickCancelHere() {
		clickElementWb(ExistingTourPageLocators.lnkCancelHere);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void verifyDoubleConfirmationPopup() {
		waitUntilElementVisibleWeb(driver, ExistingTourPageLocators.divCancelPopup, 120);
		if (verifyElementDisplayed(ExistingTourPageLocators.divCancelPopup)) {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyDoubleConfirmationPopup", Status.PASS,
					"Double confirmation popup is displayed");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyDoubleConfirmationPopup", Status.FAIL,
					"Double confirmation popup is not displayed");
		}
	}

	public void verifyOnclickNoNavigatesToExistingTour() {
		if (verifyElementDisplayed(ExistingTourPageLocators.btnNoCancel)) {
			clickElementWb(ExistingTourPageLocators.btnNoCancel);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(ExistingTourPageLocators.lnkEditTour)) {
				tcConfig.updateTestReporter("ExistingTourPage", "verifyOnclickNoNavigatesToExistingTour", Status.PASS,
						"User is navigating to existing tour page after clicking no");
			} else {
				tcConfig.updateTestReporter("ExistingTourPage", "verifyOnclickNoNavigatesToExistingTour", Status.FAIL,
						"User is not navigating to existing tour page after clicking no");
			}
		} else {
			tcConfig.updateTestReporter("ExistingTourPage", "verifyOnclickNoNavigatesToExistingTour", Status.FAIL,
					"No button is not visible in double confirmation popup");
		}
	}

	public void clickYes() {
		if (verifyElementDisplayed(ExistingTourPageLocators.btnYesCancel)) {
			clickElementWb(ExistingTourPageLocators.btnYesCancel);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("ExistingTourPage", "clickYes", Status.PASS,
					"Yes button is visible in double confirmation popup");
		} else {
			tcConfig.updateTestReporter("ExistingTourPage", "clickYes", Status.FAIL,
					"Yes button is not visible in double confirmation popup");
		}
	}
}
