package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PaymentPage_Android extends PaymentPage {

	public static final Logger log = Logger.getLogger(ReservationPage_Android.class);

	public PaymentPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// PaymentPageLocators.class);

	}

	public void verifyFooterAndTCPaymentPage() {
		try {
			waitUntilElementVisibleWeb(driver, PaymentPageLocators.footerPaymentPage, 120);

			if (verifyElementDisplayed(PaymentPageLocators.footerPaymentPage)) {
				scrollDownForElementJSWb(PaymentPageLocators.footerPaymentPage);
				if (verifyElementDisplayed(PaymentPageLocators.lnkTermsAndCondition)) {
					String window = driver.getWindowHandle();
					clickElementWb(PaymentPageLocators.lnkTermsAndCondition);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyElementDisplayed(PaymentPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					// driver.close();
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					browserBack((RemoteWebDriver) driver);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
							"Terms & condition link is not present in Payment page");
				}
			} else {
				tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
						"Footer is not present in Payment page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(PaymentPageLocators.lnkTermsOfUse);
			links.add(PaymentPageLocators.lnkPrivacyNotice);
			links.add(PaymentPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(PaymentPageLocators.headerOtherPages)
							&& PaymentPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| PaymentPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					// driver.close();
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					browserBack((RemoteWebDriver) driver);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}

	public void providePaymentDetails() {
		// WebDriverWait wait = new WebDriverWait(driver, 120);
		// wait.until(ExpectedConditions.textToBePresentInElement(PaymentPageLocators.lblPaymentHeader,
		// "Done"));
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleWeb(driver, PaymentPageLocators.lblPaymentHeader, 120);
			if (PaymentPageLocators.lblPaymentHeader.getText().contains("You're")) {
				waitUntilElementVisibleWeb(driver, PaymentPageLocators.txtNameOnCard, 120);
				scrollDownForElementJSWb(PaymentPageLocators.lblPaymentHeader);
				if (verifyElementDisplayed(PaymentPageLocators.txtNameOnCard)) {
					PaymentPageLocators.txtNameOnCard.click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					sendKeys((RemoteWebDriver) driver, "Cardholder First Name", testData.get("FirstNameOnCard"));

					if (verifyElementDisplayed(PaymentPageLocators.txtLastNameOnCard)) {
						PaymentPageLocators.txtLastNameOnCard.click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						sendKeys((RemoteWebDriver) driver, "Cardholder Last Name", testData.get("LastNameOnCard"));

						if (verifyElementDisplayed(PaymentPageLocators.txtCardNumber)) {
							PaymentPageLocators.txtCardNumber.click();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							sendKeys((RemoteWebDriver) driver, "Credit Card Number", testData.get("CreditCardNum"));

							if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryMonth)) {
								selectDropdown("Index", PaymentPageLocators.drpdwnCardExpiryMonth, "2");
								if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryYear)) {
									selectDropdown("Index", PaymentPageLocators.drpdwnCardExpiryYear, "5");
									if (verifyElementDisplayed(PaymentPageLocators.txtPostalCode)) {
										PaymentPageLocators.txtPostalCode.click();
										waitForSometime(tcConfig.getConfig().get("LowWait"));
										sendKeys((RemoteWebDriver) driver, "Postal Code", testData.get("PostalCode"));

										tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.PASS,
												"Entered all the payment details");
									} else {
										tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
												"Postal code textbox is not visible");
									}
								} else {
									tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
											"Year expiry dropdown is not visible");
								}
							} else {
								tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
										"Month epiry dropdown is not visible");
							}
						} else {
							tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
									"Card number textbox is not visible");
						}
					} else {
						tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
								"Cardholder last name textbox is not visible");
					}
				} else {
					tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
							"Cardholder first name textbox is not visible");
				}
				// fieldDataEnter(PaymentPageLocators.txtPostalCode,
				// testData.get("PostalCode"));
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL, "Getting Error : " + e);
		}
	}

}