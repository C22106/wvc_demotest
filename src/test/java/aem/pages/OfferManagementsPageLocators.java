package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class OfferManagementsPageLocators {

	public OfferManagementsPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//a[@title='Link Offer Incentive']")
	public WebElement lnkLinkOfferIncentive;

	@FindBy(xpath = "//label[text()='Incentive Name']/..//input")
	public WebElement txtIncentiveName;

	@FindBy(xpath = "//div[contains(text(),'Effective Date')]/..//input")
	public WebElement txtEffectiveDate;

	@FindBy(xpath = "//label[text()='Choice Grouping']/..//select")
	public WebElement drpDwnChoiceGrouping;

	@FindBy(xpath = "//div[contains(text(),'End Date')]/..//input")
	public WebElement txtEndDate;

	@FindBy(xpath = "//div[contains(text(),'Display Name')]/..//input")
	public WebElement txtDisplayName;

	@FindBy(xpath = "//div[contains(text(),'Quantity Offered')]/..//input")
	public WebElement txtQuantityOffered;

	@FindBy(xpath = "//button[@title='Save'] | //button[text()='Save']")
	public List<WebElement> btnSave;

	@FindBy(xpath = "//th[text()='Offer Incentive Code']/../../../tbody/tr/td[4]//a")
	public List<WebElement> lnkShowMore;

	@FindBy(xpath = "//div[contains(@class,'branding-actions')]//a")
	public List<WebElement> lnkEdit;

	@FindBy(xpath = "//a[@title='New']")
	public WebElement btnNew;

	@FindBy(xpath = "//h2[text()='New Offer Management']")
	public WebElement headerNewOfferPage;

	@FindBy(xpath = "//input[@title='Search Sales Stores']")
	public WebElement txtBoxSalesStore;

	@FindBy(xpath = "//span[text()='Offer Name']/../../input")
	public WebElement txtBoxOfferName;

	@FindBy(xpath = "//div[contains(@class,'datetime')]/div/input")
	public List<WebElement> txtBoxEffectiveEndDate;

	@FindBy(xpath = "//span[text()='Offer Payment Details']/../../textarea")
	public WebElement txtAreaOfferPaymentDetails;

	@FindBy(xpath = "//span[contains(text(),'Payment Page')]/../../input")
	public WebElement txtBoxPaymentPageDetails;

	@FindBy(xpath = "//span[contains(text(),'Offer Long')]/../../textarea")
	public WebElement txtAreaOfferDescription;

	@FindBy(xpath = "//div[contains(@data-value,'day')]/..")
	public List<WebElement> listAvailableDays;

	@FindBy(xpath = "//button[contains(@title,'Move selection to')]")
	public List<WebElement> listMoveBtn;

	@FindBy(xpath = "//span[text()='Refundable Deposit']/../../input")
	public WebElement txtBoxRefundableDeposit;

	@FindBy(xpath = "//ul[@class='errorsList']/li")
	public WebElement txtError;

	@FindBy(xpath = "//div[@class='slds-media__body']/h1")
	public WebElement headerOfferManagement;

	@FindBy(xpath = "//span[text()='Offer Name'][contains(@class,'test-id')]/../../div[2]/span/slot[1]/slot")
	public WebElement lblOfferName;

	@FindBy(xpath = "//div[@class='forceVisualMessageQueue']//div[@data-key='success']")
	public WebElement divSuccessMsg;
}
