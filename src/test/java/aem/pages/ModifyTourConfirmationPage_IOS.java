package aem.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class ModifyTourConfirmationPage_IOS extends ModifyTourConfirmationPage {

	public static final Logger log = Logger.getLogger(HomePage_IOS.class);

	public ModifyTourConfirmationPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
