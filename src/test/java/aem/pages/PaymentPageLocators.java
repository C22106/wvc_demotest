package aem.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class PaymentPageLocators {
	public PaymentPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public WebElement lblPaymentHeader;

	@FindBy(xpath = "//span[contains(text(),'Cardholder First Name')]/../input")
	public WebElement txtNameOnCard;

	@FindBy(xpath = "//span[contains(text(),'Cardholder Last Name')]/../input")
	public WebElement txtLastNameOnCard;

	@FindBy(xpath = "//span[contains(text(),'Card Number')]/../input")
	public WebElement txtCardNumber;

	@FindBy(xpath = "//span[text()='Month']/../select")
	public WebElement drpdwnCardExpiryMonth;

	@FindBy(xpath = "//span[text()='Year']/../select")
	public WebElement drpdwnCardExpiryYear;

	@FindBy(xpath = "//span[contains(text(),'Postal Code')]/../input")
	public WebElement txtPostalCode;

	@FindBy(xpath = "//a[@title='Terms and Conditions']")
	public WebElement lnkTermsAndCondition;

	@FindBy(xpath = "//div[contains(@class,'wyn-footer__advertisingdisclaimer')]")
	public WebElement footerPaymentPage;

	@FindBy(xpath = "//button[text()='CONTINUE']")
	public WebElement btnContinuePaymentPage;

	@FindBy(xpath = "//div[@class='wyn-rich-text wyn-l-content']//h1")
	public WebElement termsAndConditionHeader;

	@FindBy(xpath = "//b[text()='Terms of Use']/..")
	public WebElement lnkTermsOfUse;

	@FindBy(xpath = "//b[text()='Privacy Notice']/..")
	public WebElement lnkPrivacyNotice;

	@FindBy(xpath = "//b[text()='Privacy Setting']/..")
	public WebElement lnkPrivacySetting;

	@FindBy(xpath = "//div[@id='main-content']//h1")
	public WebElement headerOtherPages;

	@FindBy(xpath = "//div[@class='grad']/h1")
	public WebElement headerPrivacySetting;
	
	@FindBy(xpath = "//h1[contains(text(),'Ready. Set. Go.')] | //h1[contains(text(),'Kick back')]")
	public WebElement confirmationHeading;
}
