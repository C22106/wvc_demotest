package aem.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class HomePageLocators {

	public HomePageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//input[@title='Search Salesforce']")
	public WebElement txtBoxGlobalSearch;

	@FindBy(xpath = "//span[contains(.,'Home')]")
	public WebElement homeTab;

	@FindBy(xpath = "//button[@class='slds-button']")
	public WebElement menuIcon;

	@FindBy(xpath = "//input[@class='slds-input input']")
	public WebElement txtSearchBy;

	@FindBy(xpath = "//input[@placeholder='Search Reservations and more...']")
	public WebElement ReservationSearch;
}
