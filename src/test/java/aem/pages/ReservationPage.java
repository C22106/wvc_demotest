package aem.pages;

import org.apache.log4j.Logger;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ReservationPage extends AEMBasePage {
//	LeadsPage leadPage = new LeadsPage(tcConfig);
	public static final Logger log = Logger.getLogger(ReservationPage.class);

	public ReservationPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// ReservationPageLocators.class);

	}

	ReservationPageLocators ReservationPageLocators = new ReservationPageLocators(tcConfig);

	public void clickAudienceLnkAssociatedWithReservation() {
		waitUntilElementVisibleWeb(driver, ReservationPageLocators.lnkAudienceCode, 120);
		clickElementWb(ReservationPageLocators.lnkAudienceCode);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void navigateToResTourRecords() {
		waitUntilElementVisibleWeb(driver, ReservationPageLocators.lnkViewAllTours, 120);
		scrollDownForElementJSWb(ReservationPageLocators.btnArrivalDetails);
		scrollUpByPixel(300);
		clickElementJSWithWait(ReservationPageLocators.lnkViewAllTours);
		waitUntilElementVisibleWeb(driver, ReservationPageLocators.headerTourRecord, 120);
		if (ReservationPageLocators.lnkBookedRecord.size() > 0) {
			clickElementWb(ReservationPageLocators.lnkBookedRecord.get(0));
		} else {
			clickElementWb(ReservationPageLocators.lnkSortTourRecord);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementWb(ReservationPageLocators.lnkBookedRecord.get(0));
		}
	}

	public void verifySSTFlags() {
		try {
			waitUntilElementVisibleWeb(driver, ReservationPageLocators.chkBoxSstFlag, 120);
			scrollDownForElementJSWb(ReservationPageLocators.chkBoxSstFlag);
			scrollUpByPixel(50);
			if (ReservationPageLocators.chkBoxSstFlag.isSelected()) {
				tcConfig.updateTestReporter("Reservation", "verifySSTFlags", Status.PASS,
						"Self service flag is checked in the reservation page");
				waitUntilElementVisibleWeb(driver, ReservationPageLocators.chkBoxSsIFlag, 120);
				if (ReservationPageLocators.chkBoxSsIFlag.isSelected()) {
					tcConfig.updateTestReporter("Reservation", "verifySSTFlags", Status.PASS,
							"Self service interaction flag is checked in the reservation page");
				} else {
					tcConfig.updateTestReporter("Reservation", "verifySSTFlags", Status.FAIL,
							"Self service interaction flag is not checked in the reservation page");
				}
			} else {
				tcConfig.updateTestReporter("Reservation", "verifySSTFlags", Status.FAIL,
						"Self service flag is not checked in the reservation page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("Reservation", "verifySSTFlags", Status.FAIL, "Getting Error: " + e);
			throw e;
		}
	}

}
