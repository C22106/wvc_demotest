package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PaymentPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(ReservationPage.class);

	public PaymentPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(),
		// PaymentPageLocators.class);

	}

	protected PaymentPageLocators PaymentPageLocators = new PaymentPageLocators(tcConfig);

	public void verifyFooterAndTCPaymentPage() {
		try {
			waitUntilElementVisibleWeb(driver, PaymentPageLocators.footerPaymentPage, 120);

			if (verifyElementDisplayed(PaymentPageLocators.footerPaymentPage)) {
				scrollDownForElementJSWb(PaymentPageLocators.footerPaymentPage);
				if (verifyElementDisplayed(PaymentPageLocators.lnkTermsAndCondition)) {
					String window = driver.getWindowHandle();
					clickElementWb(PaymentPageLocators.lnkTermsAndCondition);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyElementDisplayed(PaymentPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
							"Terms & condition link is not present in Payment page");
				}
			} else {
				tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
						"Footer is not present in Payment page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(PaymentPageLocators.lnkTermsOfUse);
			links.add(PaymentPageLocators.lnkPrivacyNotice);
			links.add(PaymentPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String window = driver.getWindowHandle();
					clickElementWb(links.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					List<String> windows = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if ((verifyElementDisplayed(PaymentPageLocators.headerOtherPages)
							&& PaymentPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| PaymentPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}

	public void providePaymentDetails() {
		// WebDriverWait wait = new WebDriverWait(driver, 120);
		// wait.until(ExpectedConditions.textToBePresentInElement(PaymentPageLocators.lblPaymentHeader,
		// "Done"));
		System.out.println("In Payment page");
		waitForSometime(tcConfig.getConfig().get("ExplicitLongWait"));
		waitUntilElementVisibleWeb(driver, PaymentPageLocators.lblPaymentHeader, 240);
		if (PaymentPageLocators.lblPaymentHeader.getText().contains("You're")) {
			waitUntilElementVisibleWeb(driver, PaymentPageLocators.txtNameOnCard, 120);
			scrollDownForElementJSWb(PaymentPageLocators.lblPaymentHeader);
			waitUntilObjectVisible(driver, PaymentPageLocators.txtNameOnCard, 120);
			if (verifyElementDisplayed(PaymentPageLocators.txtNameOnCard)) {
				Actions a = new Actions(driver);
				if (browserName.equalsIgnoreCase("EDGE")) {
					PaymentPageLocators.txtNameOnCard.sendKeys(testData.get("FirstNameOnCard") + Keys.TAB);
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
				} else {
					/*
					 * a.sendKeys(PaymentPageLocators.txtNameOnCard,
					 * testData.get("FirstNameOnCard")).build() .perform();
					 */
					PaymentPageLocators.txtNameOnCard.sendKeys(testData.get("FirstNameOnCard"));
				}

				if (verifyElementDisplayed(PaymentPageLocators.txtLastNameOnCard)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (browserName.equalsIgnoreCase("EDGE")) {

						PaymentPageLocators.txtLastNameOnCard.sendKeys(testData.get("LastNameOnCard") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						/*
						 * a.sendKeys(PaymentPageLocators.txtLastNameOnCard,
						 * testData.get("LastNameOnCard")).build() .perform();
						 */
						PaymentPageLocators.txtLastNameOnCard.sendKeys(testData.get("LastNameOnCard"));
					}

					if (verifyElementDisplayed(PaymentPageLocators.txtCardNumber)) {
						fieldDataEnter(PaymentPageLocators.txtCardNumber, testData.get("CreditCardNum"));
						if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryMonth)) {
							selectDropdown("Index", PaymentPageLocators.drpdwnCardExpiryMonth, "2");
							if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryYear)) {
								selectDropdown("Index", PaymentPageLocators.drpdwnCardExpiryYear, "5");
								if (verifyElementDisplayed(PaymentPageLocators.txtPostalCode)) {
									if (browserName.equalsIgnoreCase("EDGE")) {

										PaymentPageLocators.txtPostalCode
												.sendKeys(testData.get("PostalCode") + Keys.TAB);
										driver.switchTo().activeElement().sendKeys(Keys.TAB);
									} else {
										/*
										 * a.sendKeys(PaymentPageLocators. txtPostalCode, testData.get("PostalCode"))
										 * .build().perform();
										 */
										PaymentPageLocators.txtPostalCode.sendKeys(testData.get("PostalCode"));
									}

									tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.PASS,
											"Entered all the payment details");
								} else {
									tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
											"Postal code textbox is not visible");
								}
							} else {
								tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
										"Year expiry dropdown is not visible");
							}
						} else {
							tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
									"Month epiry dropdown is not visible");
						}
					} else {
						tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
								"Card number textbox is not visible");
					}
				} else {
					tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
							"Cardholder last name textbox is not visible");
				}
			} else {
				tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
						"Cardholder first name textbox is not visible");
			}
			// fieldDataEnter(PaymentPageLocators.txtPostalCode,
			// testData.get("PostalCode"));
		}
	}

	public void clickContinuePaymentPage() {
		waitUntilElementVisibleWeb(driver, PaymentPageLocators.lblPaymentHeader, 120);
		if (PaymentPageLocators.lblPaymentHeader.getText().contains("You're")) {
			try {
				waitUntilElementVisibleWeb(driver, PaymentPageLocators.btnContinuePaymentPage, 120);
				if (verifyElementDisplayed(PaymentPageLocators.btnContinuePaymentPage)) {
					if (PaymentPageLocators.btnContinuePaymentPage.isEnabled()) {
						clickElementWb(PaymentPageLocators.btnContinuePaymentPage);
						tcConfig.updateTestReporter("PaymentPage", "clickContinuePaymentPage", Status.PASS,
								"Continue button is enabled and clicked");
					} else {
						tcConfig.updateTestReporter("PaymentPage", "clickContinuePaymentPage", Status.FAIL,
								"Continue button is not enabled");
					}
					waitUntilObjectVisible(driver, PaymentPageLocators.confirmationHeading, 240);
					tcConfig.updateTestReporter("PaymentPage", "clickContinuePaymentPage", Status.PASS,
							"Booking is completed");
				} else {
					tcConfig.updateTestReporter("PaymentPage", "clickContinuePaymentPage", Status.FAIL,
							"Continue button is not visible");
				}
			} catch (Exception e) {
				tcConfig.updateTestReporter("PaymentPage", "clickContinuePaymentPage", Status.FAIL,
						"Getting error: " + e);
			}
		}
	}

	public void verifyHeaderPaymentScreen() {
		try {
			waitUntilElementVisibleWeb(driver, PaymentPageLocators.lblPaymentHeader, 120);
			if (verifyElementDisplayed(PaymentPageLocators.lblPaymentHeader)) {
				tcConfig.updateTestReporter("PaymentPage", "verifyHeaderPaymentScreen", Status.PASS,
						"Payment page header is visible");
			} else {
				tcConfig.updateTestReporter("PaymentPage", "verifyHeaderPaymentScreen", Status.FAIL,
						"Payment page header is not visible");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "verifyHeaderPaymentScreen", Status.FAIL, "Getting error: " + e);
		}
	}

	public void verifyPaymentFields() {
		boolean flag1 = false;
		boolean flag2 = false;
		boolean flag3 = false;
		boolean flag4 = false;
		boolean flag5 = false;
		boolean flag6 = false;
		boolean flag7 = false;
		waitUntilElementVisibleWeb(driver, PaymentPageLocators.txtNameOnCard, 120);
		if (verifyElementDisplayed(PaymentPageLocators.txtNameOnCard)) {
			flag1 = true;
		} else {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"First Name field is not visible");
		}

		if (verifyElementDisplayed(PaymentPageLocators.txtLastNameOnCard)) {
			flag2 = true;
		} else {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"Last Name field is not visible");
		}

		if (verifyElementDisplayed(PaymentPageLocators.txtCardNumber)) {
			flag3 = true;
		} else {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"Card Number field is not visible");
		}

		if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryMonth)) {
			flag4 = true;
		} else {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"Expiry month dropdown is not visible");
		}

		if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryYear)) {
			flag5 = true;
		} else {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"Expiry year dropdown is not visible");
		}

		if (verifyElementDisplayed(PaymentPageLocators.txtPostalCode)) {
			flag6 = true;
		} else {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"Zipcode field is not visible");
		}

		if (PaymentPageLocators.btnContinuePaymentPage.isEnabled()) {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"Continue button is enabled before providing all payment details");
		} else {
			flag7 = true;
		}

		if (flag1 && flag2 && flag3 && flag4 && flag5 && flag6 && flag7) {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.PASS,
					"All payment fields are visible and the continue button is disabled");
		} else {
			tcConfig.updateTestReporter("PaymentPage", "verifyPaymentFields", Status.FAIL,
					"Some of the fields are not visible in page");
		}

	}
}
