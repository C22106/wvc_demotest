package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class TourSlotPageLocators {

	public TourSlotPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public WebElement lblTourSlotHeader;

	@FindBy(xpath = "//div[contains(@class,'start')]")
	public WebElement divCheckinDate;

	@FindBy(xpath = "//div[contains(@class,'end')]")
	public WebElement divCheckOutDate;

	@FindBy(xpath = "//div[@aria-disabled='false']")
	public List<WebElement> divEnabledDates;

	@FindBy(xpath = "//strong[contains(text(),'Select')]/../..//div[contains(@class,'self-service')]")
	public List<WebElement> divTourTime;

	@FindBy(xpath = "//span[text()='First Name']/../input | //span[text()='First name']/../input")
	public WebElement txtGuestFirstName;

	@FindBy(xpath = "//span[text()='Last Name']/../input | //span[text()='Last name']/../input")
	public WebElement txtGuestLastName;

	@FindBy(xpath = "//input[@id='termsCheckbox']/../span")
	public WebElement chkBoxTermsAndCondition;

	@FindBy(xpath = "//button[text()='CONTINUE' or text()='BOOK NOW']")
	public WebElement btnContinueTourSlot;

	@FindBy(xpath = "//div[contains(@class,'wyn-footer__advertisingdisclaimer')]")
	public WebElement footerTourSlotPage;

	@FindBy(xpath = "//a[@title='Terms and Conditions']")
	public WebElement lnkTermsAndCondition;

	@FindBy(xpath = "//div[contains(@class,'disclaimer')]//p/a[contains(@href,'privacy')]")
	public WebElement fieldTermsAndConditionText;

	@FindBy(xpath = "//div[@class='DayPicker-Months']")
	public WebElement fieldTourDateCalendar;

	@FindBy(xpath = "//div[@class='DayPicker-Months']/div")
	public List<WebElement> listOfMonths;

	@FindBy(xpath = "//div[@class='DayPicker-Months']/div[1]/div[1]/div")
	public WebElement tourMonthName;

	@FindBy(xpath = "//strong[text()='Owner Update Criteria']/../../ul/li")
	public List<WebElement> fieldTourCriteria;

	@FindBy(xpath = "//a[@title='Close Trip Summary']/../div/div[1]/div[2]/p")
	public List<WebElement> lblIncentivesSelectedRightRail;

	@FindBy(xpath = "//span[text()='Schedule']/../../p | //span[text()='TIME']/../../p")
	public List<WebElement> listSchedule;

	@FindBy(xpath = "//span[text()='Incentives']/../../p | //div[@class='step-2__section groups separator']//p")
	public List<WebElement> fieldSelectedIncentives;

	@FindBy(xpath = "//span[text()='Additional Guest']/../../p | //span[text()='ADDITIONAL GUEST']/../../p")
	public WebElement fieldAdditionalGuestRightRail;

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public WebElement lblPaymentHeader;

	@FindBy(xpath = "//div[@class='wyn-rich-text wyn-l-content']//h1")
	public WebElement termsAndConditionHeader;

	@FindBy(xpath = "//b[text()='Terms of Use']/..")
	public WebElement lnkTermsOfUse;

	@FindBy(xpath = "//b[text()='Privacy Notice']/..")
	public WebElement lnkPrivacyNotice;

	@FindBy(xpath = "//div[@class='grad']/h1")
	public WebElement headerPrivacySetting;

	@FindBy(xpath = "//label/span[contains(text(),'Tour date')]")
	public WebElement fieldTourSelectDate;

	@FindBy(xpath = "//span[contains(text(),'expand_more')]")
	public WebElement fieldTourSelectDateExpand;

	@FindBy(xpath = "//b[text()='Privacy Setting']/..")
	public WebElement lnkPrivacySetting;

	@FindBy(xpath = "//div[@id='main-content']//h1")
	public WebElement headerOtherPages;

	@FindBy(xpath = "//a[text()='View Trip Summary']")
	public WebElement viewTripSummary;

	@FindBy(xpath = "//h4/strong[text()='Offer Details:']")
	public WebElement offerDetailsHeader;

}
