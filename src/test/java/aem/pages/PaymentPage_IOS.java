package aem.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PaymentPage_IOS extends PaymentPage {

	public static final Logger log = Logger.getLogger(ReservationPage_IOS.class);

	public PaymentPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), PaymentPageLocators.class);

	}

	public void verifyFooterAndTCPaymentPage() {
		try {
			waitUntilElementVisibleWeb(driver, PaymentPageLocators.footerPaymentPage, 120);

			if (verifyElementDisplayed(PaymentPageLocators.footerPaymentPage)) {
				scrollDownForElementJSWb(PaymentPageLocators.footerPaymentPage);
				if (verifyElementDisplayed(PaymentPageLocators.lnkTermsAndCondition)) {
					String URL = driver.getCurrentUrl();
					scrollDownForElementJSWb(PaymentPageLocators.lnkTermsAndCondition);
					clickIOSElement("Terms and Conditions", 1, PaymentPageLocators.lnkTermsAndCondition);

					if (verifyElementDisplayed(PaymentPageLocators.termsAndConditionHeader)) {
						tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.PASS,
								"Footer and terms & condition link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
								"Terms & condition link is not opening in another tab");
					}
					driver.get(URL);
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
							"Terms & condition link is not present in Payment page");
				}
			} else {
				tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
						"Footer is not present in Payment page");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "verifyFooterAndTCPaymentPage", Status.FAIL,
					"Getting error: " + e);
		}
	}

	public void verifyTermsAndPrivacyLink() {
		try {
			List<WebElement> links = new ArrayList<WebElement>();
			List<String> header = new ArrayList<String>();
			links.add(PaymentPageLocators.lnkTermsOfUse);
			links.add(PaymentPageLocators.lnkPrivacyNotice);
			links.add(PaymentPageLocators.lnkPrivacySetting);
			header.add("Terms of Use");
			header.add("Privacy Notice");
			header.add("Interest Based Advertising Policies");
			for (int i = 0; i < links.size(); i++) {
				if (verifyElementDisplayed(links.get(i))) {
					String URL = driver.getCurrentUrl();
					scrollDownForElementJSWb(links.get(i));
					scrollDownByPixel(50);
					if (links.get(i).getText().equalsIgnoreCase("Terms of Use")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Terms of Use", 1, links.get(i));
						} else {
							clickIOSElement("Terms of", 1, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("Privacy Notice")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Notice", 1, links.get(i));
						} else {
							clickIOSElement("Notice", 2, links.get(i));
						}
					} else if (links.get(i).getText().equalsIgnoreCase("privacy Setting")) {
						if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
								.contains("iPad")) {
							clickIOSElement("Privacy Setting", 1, links.get(i));
						} else {
							clickIOSElement("Setting", 1, links.get(i));
						}
					}

					if ((verifyElementDisplayed(PaymentPageLocators.headerOtherPages)
							&& PaymentPageLocators.headerOtherPages.getText().equalsIgnoreCase(header.get(i)))
							|| PaymentPageLocators.headerPrivacySetting.getText().contains(header.get(i))) {
						tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.PASS,
								"Link is present in landing page and onclicking it is opening in another tab");
					} else {
						tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL,
								"Llink is not opening in another tab");
					}
					driver.get(URL);
					;
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					// driver.switchTo().window(window);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL,
							"Terms of use link is not present in landing page");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "verifyTermsAndPrivacyLink", Status.FAIL, "Getting Error: " + e);
		}
	}

	@Override
	public void providePaymentDetails() {
		// WebDriverWait wait = new WebDriverWait(driver, 120);
		// wait.until(ExpectedConditions.textToBePresentInElement(PaymentPageLocators.lblPaymentHeader,
		// "Done"));
		try {
			/*
			 * driver.navigate().refresh();
			 * scrollDownForElementJSWb(PaymentPageLocators.lblPaymentHeader);
			 */
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPhone")) {
				waitUntilElementVisibleWeb(driver, PaymentPageLocators.lblPaymentHeader, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			if (PaymentPageLocators.lblPaymentHeader.getText().contains("You're")) {
				waitUntilElementVisibleWeb(driver, PaymentPageLocators.txtNameOnCard, 120);

				scrollDownForElementJSWb(PaymentPageLocators.txtNameOnCard);
				// if(((RemoteWebDriver)driver).getCapabilities().getCapability("model").toString().contains("iPad")){
				scrollUpByPixel(200);

				if (verifyElementDisplayed(PaymentPageLocators.txtNameOnCard)) {
					sendKeys2((RemoteWebDriver) driver, "Cardholder First Name", testData.get("FirstNameOnCard"));
					/*
					 * PaymentPageLocators.txtNameOnCard.sendKeys(testData.get("FirstNameOnCard") +
					 * Keys.TAB); driver.switchTo().activeElement().sendKeys(Keys.TAB);
					 * if(browserName.equalsIgnoreCase("EDGE")){
					 * PaymentPageLocators.txtNameOnCard.sendKeys(testData.get("FirstNameOnCard") +
					 * Keys.TAB); driver.switchTo().activeElement().sendKeys(Keys.TAB); }else{
					 * a.sendKeys(PaymentPageLocators.txtNameOnCard,
					 * testData.get("FirstNameOnCard")).build().perform(); }
					 */

					if (verifyElementDisplayed(PaymentPageLocators.txtLastNameOnCard)) {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						sendKeys2((RemoteWebDriver) driver, "Cardholder Last Name", testData.get("LastNameOnCard"));
						/*
						 * PaymentPageLocators.txtLastNameOnCard.sendKeys(testData.get("LastNameOnCard")
						 * + Keys.TAB); driver.switchTo().activeElement().sendKeys(Keys.TAB);
						 * waitForSometime(tcConfig.getConfig().get("LowWait"));
						 * if(browserName.equalsIgnoreCase("EDGE")){
						 * 
						 * PaymentPageLocators.txtLastNameOnCard.sendKeys(testData.get("LastNameOnCard")
						 * + Keys.TAB); driver.switchTo().activeElement().sendKeys(Keys.TAB); }else{
						 * a.sendKeys(PaymentPageLocators.txtLastNameOnCard,
						 * testData.get("LastNameOnCard")).build().perform(); }
						 */

						if (verifyElementDisplayed(PaymentPageLocators.txtCardNumber)) {
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							sendKeys2((RemoteWebDriver) driver, "Credit Card Number", testData.get("CreditCardNum"));
							/*
							 * fieldDataEnter(PaymentPageLocators.txtCardNumber,
							 * testData.get("CreditCardNum"));
							 */

							if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryMonth)) {
								waitForSometime(tcConfig.getConfig().get("MedWait"));
								selectDropdown("Index", PaymentPageLocators.drpdwnCardExpiryMonth, "2");
								if (verifyElementDisplayed(PaymentPageLocators.drpdwnCardExpiryYear)) {
									selectDropdown("Index", PaymentPageLocators.drpdwnCardExpiryYear, "5");
									waitForSometime(tcConfig.getConfig().get("MedWait"));
									if (verifyElementDisplayed(PaymentPageLocators.txtPostalCode)) {
										if (((RemoteWebDriver) driver).getCapabilities().getCapability("model")
												.toString().contains("iPad")) {
											waitForSometime(tcConfig.getConfig().get("MedWait"));
											PaymentPageLocators.txtPostalCode.click();
											sendKeys((RemoteWebDriver) driver, "Postal Code",
													testData.get("PostalCode"));
											// fieldDataEnter(PaymentPageLocators.txtPostalCode,
											// testData.get("PostalCode"));
											waitForSometime(tcConfig.getConfig().get("MedWait"));
											scrollUpByPixel(100);
										} else {
											waitForSometime(tcConfig.getConfig().get("MedWait"));
											sendKeys2((RemoteWebDriver) driver, "Postal Code",
													testData.get("PostalCode"));
											waitForSometime(tcConfig.getConfig().get("MedWait"));
										}
										/*
										 * if(browserName.equalsIgnoreCase("EDGE")){
										 * 
										 * PaymentPageLocators.txtPostalCode.sendKeys(testData.get("PostalCode") +
										 * Keys.TAB); driver.switchTo().activeElement().sendKeys(Keys.TAB); }else{
										 * a.sendKeys(PaymentPageLocators.txtPostalCode,
										 * testData.get("PostalCode")).build().perform(); }
										 */

										tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.PASS,
												"Entered all the payment details");
									} else {
										tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
												"Postal code textbox is not visible");
									}
								} else {
									tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
											"Year expiry dropdown is not visible");
								}
							} else {
								tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
										"Month epiry dropdown is not visible");
							}

						} else {
							tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
									"Card number textbox is not visible");
						}
					} else {
						tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
								"Cardholder last name textbox is not visible");
					}
				} else {
					tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL,
							"Cardholder first name textbox is not visible");
				}
				// fieldDataEnter(PaymentPageLocators.txtPostalCode,
				// testData.get("PostalCode"));
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("PaymentPage", "providePaymentDetails", Status.FAIL, "Getting Error : " + e);
		}
	}

}