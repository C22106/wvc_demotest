package aem.pages;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class SF_LoginPage_IOS extends SF_LoginPage {

	public static final Logger log = Logger.getLogger(SF_LoginPage_IOS.class);
	ReadConfigFile config = new ReadConfigFile();
	SF_LoginPageLocators SF_LoginPageLocators = new SF_LoginPageLocators(tcConfig);
	LandingPageLocators LandingPageLocators = new LandingPageLocators(tcConfig);

	public SF_LoginPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), SF_LoginPageLocators.class);

	}

	public void clearCache() {
		// ClearSafariCache(driver);
		clearCacheSafari();
		// Go to Home Page
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("keySequence", "HOME ");
		((JavascriptExecutor) driver).executeScript("mobile:presskey", param);

		// Launch Settings Menu
		Map<String, Object> params1 = new HashMap<>();
		params1.put("identifier", "com.apple.mobilesafari");
		((JavascriptExecutor) driver).executeScript("mobile:application:open", params1);
		startApp("Safari", driver);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		switchToContext(driver, "WEBVIEW");
	}

	@Override
	public void launchWithoutData(String url) {
		String appUrl = "";
		switch (url) {
		case "QA":
			appUrl = config.get("AEM_URL_QA").trim();
			break;
		case "PROD_WM":
			appUrl = config.get("AEM_URL_PROD_WM").trim();
			break;
		case "PROD_CW":
			appUrl = config.get("AEM_URL_PROD_CW").trim();
			break;
		case "STG_WM":
			appUrl = config.get("AEM_URL_STG_WM").trim();
			break;
		case "STG_CW":
			appUrl = config.get("AEM_URL_STG_CW").trim();
			break;
		}
		// String url=config.get("AEM_URL_QA").trim();
		driver.navigate().to(appUrl);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		// driver.manage().window().maximize();
		// waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.lblHeader, 120);
		enterReservationDetails();
		clickGetStartedBtn();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	@Override
	public void enterReservationDetails() {
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.inputConfirmationNumber, 120);
		// LandingPageLocators.inputConfirmationNumber.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "ConfirmationNumber", testData.get("ConfirmationNumber"));
		/// LandingPageLocators.inputLastName.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		sendKeys((RemoteWebDriver) driver, "LastName", testData.get("LastName"));
		// LandingPageLocators.inputCheckInDate.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeys((RemoteWebDriver) driver, "Check-in Date", testData.get("CheckInDate"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		/*
		 * scrollDownForElementJSWb(LandingPageLocators.btnGetStarted);
		 * waitForSometime(tcConfig.getConfig().get("LowWaits"));
		 */
	}

	public By getStartedBtn = By.xpath("//label/button[@type='submit']");
	public By header = By.xpath("//h1[contains(@class,'wyn-type-title')]");

	@Override
	public void clickGetStartedBtn() {
		driver.findElement(getStartedBtn).submit();
		/*
		 * driver.findElement(header).click();
		 * scrollDownForElementJSWb(SF_LoginPageLocators.btnGetStarted);
		 * driver.findElement(getStartedBtn).click();
		 */
		// tapByCoordinates(getStartedBtn);
		/*
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * scrollDownForElementJSWb(SF_LoginPageLocators.btnGetStarted);
		 * driver.findElement(getStartedBtn).click();
		 * scrollDownForElementJSWb(SF_LoginPageLocators.btnGetStarted);
		 */
		// clickElementJSWithWait(SF_LoginPageLocators.btnGetStarted);
		// clickElementWb(SF_LoginPageLocators.btnGetStarted);
		// SF_LoginPageLocators.btnGetStarted.click();
		// scrollDownForElementJSWb(SF_LoginPageLocators.btnGetStarted);
		// clickIOSElement("GET STARTED", 1, SF_LoginPageLocators.btnGetStarted);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
	}

	@Override
	public void launchUrl(String url){
			
		System.out.println("Launch");
		String appUrl = "";
		switch (url) {
		case "QA_CW":
			appUrl = config.get("AEM_URL_QA_CW").trim();
			break;
		case "QA_WM":
			appUrl = config.get("AEM_URL_QA_WM").trim();
			break;
		case "PROD_WM":
			appUrl = config.get("AEM_URL_PROD_WM").trim();
			break;
		case "PROD_CW":
			appUrl = config.get("AEM_URL_PROD_CW").trim();
			break;
		case "STG_WM":
			appUrl = config.get("AEM_URL_STG_WM").trim();
			break;
		case "STG_CW":
			appUrl = config.get("AEM_URL_STG_CW").trim();
			break;
		case "SAND_WM":
			appUrl = config.get("AEM_URL_SAND_WM").trim();
			break;
		case "SAND_CW":
			appUrl = config.get("AEM_URL_SAND_CW").trim();
			break;
		}
		driver.navigate().to(appUrl);
		// driver.manage().window().maximize();
		waitUntilElementVisibleWeb(driver, SF_LoginPageLocators.lblHeader, 120);
	}
}