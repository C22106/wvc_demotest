package aem.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class ModifyTourConfirmationPage_Android extends ModifyTourConfirmationPage {

	public static final Logger log = Logger.getLogger(HomePage_Android.class);

	public ModifyTourConfirmationPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
