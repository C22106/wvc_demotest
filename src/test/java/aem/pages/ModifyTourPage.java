package aem.pages;

import org.apache.log4j.Logger;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ModifyTourPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public ModifyTourPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), ModifyTourPageLocators.class);

	}

	ModifyTourPageLocators ModifyTourPageLocators = new ModifyTourPageLocators(tcConfig);

	public void verifyModifyTourPageHeader() {
		waitUntilElementVisibleWeb(driver, ModifyTourPageLocators.headerModifyTourPage, 120);
		if (verifyElementDisplayed(ModifyTourPageLocators.headerModifyTourPage)) {
			tcConfig.updateTestReporter("ModifyTourPage", "verifyModifyTourPageHeader", Status.PASS,
					"Modify Tour page header is displayed");
		} else {
			tcConfig.updateTestReporter("ModifyTourPage", "verifyModifyTourPageHeader", Status.FAIL,
					"Modify Tour page header is not displayed");
		}
	}

	public void verifySelectedDateTimeAndSaveButton() {
		if (ModifyTourPageLocators.fieldSelectedDate.getText().contains(testData.get("TourDate"))
				&& !(ModifyTourPageLocators.btnSaveChanges.isEnabled())) {
			tcConfig.updateTestReporter("ModifyTourPage", "verifySelectedDateAndTime", Status.PASS,
					"Previously selected date and time are preselected in modify tour screen and save changes button is disabled");
		} else {
			tcConfig.updateTestReporter("ModifyTourPage", "verifySelectedDateAndTime", Status.FAIL,
					"Either previous selected date and time are not preselected or save changes button is enabled");
		}
	}

	public void verifyCancelChangesButton() {
		waitUntilElementVisibleWeb(driver, ModifyTourPageLocators.btnCancelChanges, 120);
		if (verifyElementDisplayed(ModifyTourPageLocators.btnCancelChanges)) {
			clickElementWb(ModifyTourPageLocators.btnCancelChanges);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(ModifyTourPageLocators.lnkEditTour)) {
				tcConfig.updateTestReporter("ModifyTourPage", "verifyCancelChangesButton", Status.PASS,
						"User is navigated to existing tour page after clicking cancel");
			} else {
				tcConfig.updateTestReporter("ModifyTourPage", "verifyCancelChangesButton", Status.FAIL,
						"User is not navigated to existing tour page after clicking cancel");
			}
		} else {
			tcConfig.updateTestReporter("ModifyTourPage", "verifyCancelChangesButton", Status.FAIL,
					"Cancel changes button is not visible to user");
		}
	}

	public String selectUpdatedTourDate() {
		String strDate = "";
		waitUntilElementVisibleWeb(driver, ModifyTourPageLocators.divEnabledDates.get(0), 120);
		if (ModifyTourPageLocators.divEnabledDates.size() > 0) {
			strDate = ModifyTourPageLocators.divEnabledDates.get(0).getText();
			clickElementWb(ModifyTourPageLocators.divEnabledDates.get(0));
			tcConfig.updateTestReporter("TourSlotPage", "selectTourDate", Status.PASS,
					"TourSlot date is visible and clicked");
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "selectTourDate", Status.FAIL, "TourSlot date is not visible");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		return strDate;
	}

	public String selectUpdatedTourTime() {
		waitUntilElementVisibleWeb(driver, ModifyTourPageLocators.divTourTime.get(0), 120);
		if (ModifyTourPageLocators.divTourTime.get(0).isEnabled()) {
			clickElementWb(ModifyTourPageLocators.divTourTime.get(0));
			tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.PASS,
					"TourSlot time is enabled and clicked");
		} else {
			tcConfig.updateTestReporter("TourSlotPage", "selectTourTime", Status.FAIL, "TourSlot time is not enabled");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		return ModifyTourPageLocators.divTourTime.get(0).getText();
	}

	public void clickSaveChanges() {
		waitUntilElementVisibleWeb(driver, ModifyTourPageLocators.btnSaveChanges, 120);
		clickElementWb(ModifyTourPageLocators.btnSaveChanges);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
}
