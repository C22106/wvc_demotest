package aem.pages;

import org.apache.log4j.Logger;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TourRecordPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(TourRecordPage.class);

	public TourRecordPage(TestConfig tcconfig) {
		super(tcconfig);
		// PageFactory.initElements(tcconfig.getDriver(), TourRecordPageLocators.class);

	}

	TourRecordPageLocators TourRecordPageLocators = new TourRecordPageLocators(tcConfig);

	public void clickBookedTour() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyElementDisplayed(TourRecordPageLocators.lnkBookedTour)) {
			tcConfig.updateTestReporter("TourRecord", "clickBookedTour", Status.PASS,
					"Booked tour record is displayed");
			clickElementWb(TourRecordPageLocators.lnkBookedTour);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("TourRecord", "clickBookedTour", Status.FAIL,
					"Booked tour record is not displayed");
		}
	}

	public void verifyGuestNameInTourRecord(String guestName) {
		waitUntilElementVisibleWeb(driver, TourRecordPageLocators.txtGuestFirstName, 120);
		if (verifyElementDisplayed(TourRecordPageLocators.txtGuestFirstName)) {
			if (verifyElementDisplayed(TourRecordPageLocators.txtGuestLastName)) {
				String fName = TourRecordPageLocators.txtGuestFirstName.getText();
				String lName = TourRecordPageLocators.txtGuestLastName.getText();
				if (guestName.equals(fName + " " + lName)) {
					tcConfig.updateTestReporter("TourRecord", "verifyGuestNameInTourRecord", Status.PASS,
							"Additional guest name is correct");
				} else {
					tcConfig.updateTestReporter("TourRecord", "verifyGuestNameInTourRecord", Status.FAIL,
							"Additional guest name is not correct");
				}
			} else {
				tcConfig.updateTestReporter("TourRecord", "verifyGuestNameInTourRecord", Status.FAIL,
						"Additional guest Last name is not displayed");
			}
		} else {
			tcConfig.updateTestReporter("TourRecord", "verifyGuestNameInTourRecord", Status.FAIL,
					"Additional guest first name is not displayed");
		}
	}

	public void verifySelfServiceBookedTour() {
		try {
			waitUntilElementVisibleWeb(driver, TourRecordPageLocators.lnkViewAllTours, 120);
			scrollDownForElementJSWb(TourRecordPageLocators.btnArrivalDetails);
			scrollUpByPixel(300);
			clickElementJSWithWait(TourRecordPageLocators.lnkViewAllTours);
			waitUntilElementVisibleWeb(driver, TourRecordPageLocators.headerTourRecord, 120);
			if (TourRecordPageLocators.lnkBookedRecord.size() > 0) {
				clickElementWb(TourRecordPageLocators.lnkBookedRecord.get(0));
			} else {
				clickElementWb(TourRecordPageLocators.lnkSortTourRecord);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementWb(TourRecordPageLocators.lnkBookedRecord.get(0));
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			scrollDownForElementJSWb(TourRecordPageLocators.chkSelfServiceFlag
					.get(TourRecordPageLocators.chkSelfServiceFlag.size() - 1));
			scrollUpByPixel(150);
			if (TourRecordPageLocators.chkSelfServiceFlag.size() > 0) {
				if (TourRecordPageLocators.chkSelfServiceFlag.get(TourRecordPageLocators.chkSelfServiceFlag.size() - 1)
						.getAttribute("class").contains("checked")) {
					tcConfig.updateTestReporter("TourRecord", "verifySelfServiceBookedTour", Status.PASS,
							"Self service tour is flowing to journey from AEM");
				} else {
					tcConfig.updateTestReporter("TourRecord", "verifySelfServiceBookedTour", Status.FAIL,
							"Self service tour flag is not checked");
				}
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TourRecord", "verifySelfServiceBookedTour", Status.FAIL, "Getting error:" + e);
		}
	}
}
