package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class TourConfirmationPageLocators {

	public TourConfirmationPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public WebElement lblTourConfHeader;

	@FindBy(xpath = "//span[text()='Schedule']/../../../h5 | //span[text()='TIME']/../../../h5")
	public List<WebElement> fieldScheduleDateAndTime;

	@FindBy(xpath = "//span[text()='Incentives']/../../../h5 | //span[text()='INCENTIVES']/../../../h5")
	public List<WebElement> fieldsIncentives;
}
