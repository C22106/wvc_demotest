package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class OfferPageLocators {

	public OfferPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	// @FindBy(xpath="//h1[contains(@class,'xyz')]")
	public WebElement lblOffersHeader;

	@FindBy(xpath = "//div[contains(@class,'incentives__disclaimer')]/p[1]")
	public WebElement fieldUserMsg;

	@FindBy(xpath = "//p[text()='Choose one']")
	public List<WebElement> lstOfferSections;

	@FindBy(xpath = "(//p[text()='Choose one']/..)[1]/div")
	public List<WebElement> lstNoOfPremiumsUnderEachOffer;

	@FindBy(xpath = "//div[contains(@class,'incentives__group__offer')]//span")
	public List<WebElement> chkBoxOffer;

	@FindBy(xpath = "//div[contains(@class,'incentives__group__offer')]//div[2]//h4")
	public List<WebElement> lblOffersName;

	@FindBy(xpath = "//button[text()='CONTINUE']")
	public WebElement btnContinueOffer;

	@FindBy(xpath = "//a[contains(text(),'Get Details')] | //a[contains(text(),'details')]")
	public WebElement lnkGetDetails;

	@FindBy(xpath = "//a[contains(text(),'please click here')]")
	public WebElement lnkPleaseClickHere;

	@FindBy(xpath = "//a[@title='Close Trip Summary']/../div/div/div[1]/h3")
	public WebElement lblResortName;

	@FindBy(xpath = "//a[@title='Close Trip Summary']/../div/div/div[1]/a")
	public WebElement lnkResortAddress;

	@FindBy(xpath = "//a[@title='Close Trip Summary']/../div/div[1]/div[2]/p")
	public WebElement lblIncentivesSelectedRightRail;

	@FindBy(xpath = "//h2[contains(@class,'title')]/../p[1]")
	public WebElement lblMessageToUser;

	@FindBy(xpath = "//div[contains(@class,'wyn-footer__advertisingdisclaimer')]")
	public WebElement footerOfferPage;

	@FindBy(xpath = "//a[@title='Terms and Conditions']")
	public WebElement lnkTermsAndCondition;

	@FindBy(xpath = "//p[contains(text(),'DEPOSIT')]")
	public WebElement fieldRefundableDeposit;

	@FindBy(xpath = "//p[contains(text(),'MEAL')]")
	public WebElement fieldRefundableMeal;

	@FindBy(xpath = "//h1[contains(@class,'wyn-type-body')]/../..")
	public WebElement fieldPremiumDescription;

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public WebElement lblLandingHeader;

	@FindBy(xpath = "//span[text()='Incentives']/../../p | //div[@class='step-2__section groups']//p")
	public List<WebElement> fieldSelectedIncentives;

	@FindBy(xpath = "//div[@class='wyn-rich-text wyn-l-content']//h1")
	public WebElement termsAndConditionHeader;

	@FindBy(xpath = "//b[text()='Terms of Use']/..")
	public WebElement lnkTermsOfUse;

	@FindBy(xpath = "//b[text()='Privacy Notice']/..")
	public WebElement lnkPrivacyNotice;

	@FindBy(xpath = "//b[text()='Privacy Setting']/..")
	public WebElement lnkPrivacySetting;

	@FindBy(xpath = "//div[@id='main-content']//h1")
	public WebElement headerOtherPages;

	@FindBy(xpath = "//div[@class='grad']/h1")
	public WebElement headerPrivacySetting;

	@FindBy(xpath = "//a[text()='View Trip Summary']")
	public WebElement viewTripSummary;
}
