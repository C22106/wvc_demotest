package aem.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class LandingPageLocators {

	public LandingPageLocators(TestConfig tcconfig) {
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	@FindBy(xpath = "//h1[contains(@class,'title')]")
	public static WebElement lblLandingHeader;

	@FindBy(xpath = "//strong[contains(text(),'verify your vacation information')]")
	public static WebElement lblLandingSubHeader;

	@FindBy(xpath = "//span[text()='Confirmation Number']/../input")
	public static WebElement inputConfirmationNumber;

	@FindBy(xpath = "//span[text()='Last Name']/../input")
	public static WebElement inputLastName;

	@FindBy(xpath = "//img[@class='wyn-footer__image']")
	public static WebElement footerlogo;

	@FindBy(xpath = "//img[@class='logo-swap__logo__image']")
	public static WebElement logo;

	@FindBy(xpath = "//b[text()='NEED HELP BOOKING?']")
	public static WebElement labelContactUs;

	@FindBy(xpath = "//span[text()='Check-In Date']/../input")
	public static WebElement inputCheckInDate;

	@FindBy(xpath = "//button[@type='submit']")
	public static WebElement btnGetStarted;

	@FindBy(xpath = "//div[contains(@class,'validation')][contains(text(),'This is not a')] | //form/div[2]//p")
	public static List<WebElement> lblErrorMsg;

	@FindBy(xpath = "//div[contains(@class,'DayPicker Selectable')]")
	public static WebElement calendarCheckInDate;

	@FindBy(xpath = "//div[contains(@class,'DayPicker-Day')][not(contains(@aria-disabled,'true'))]")
	public static List<WebElement> linkSelectDaysCheckInDate;

	@FindBy(xpath = "//span[text()='chevron_right']")
	public static WebElement calendarArrow;

	@FindBy(xpath = "//div[contains(@class,'wyn-footer__advertisingdisclaimer')]")
	public static WebElement footerLandingPage;

	@FindBy(xpath = "//a[contains(text(),'Terms and Conditions')]")
	public static WebElement lnkTermsAndCondition;

	@FindBy(xpath = "//div[@class='wyn-rich-text wyn-l-content']//h1")
	public static WebElement termsAndConditionHeader;

	@FindBy(xpath = "//b[text()='Terms of Use']/..")
	public static WebElement lnkTermsOfUse;

	@FindBy(xpath = "//b[text()='Privacy Notice']/..")
	public static WebElement lnkPrivacyNotice;

	@FindBy(xpath = "//b[text()='Privacy Setting']/..")
	public static WebElement lnkPrivacySetting;

	@FindBy(xpath = "//div[@id='main-content']//h1")
	public static WebElement headerOtherPages;

	@FindBy(xpath = "//div[@class='grad']/h1")
	public static WebElement headerPrivacySetting;

	@FindBy(xpath = "//span[text()='calendar_today']")
	public static WebElement imgCalendar;
}
