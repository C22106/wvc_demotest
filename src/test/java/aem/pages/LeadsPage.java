package aem.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class LeadsPage extends AEMBasePage {

	public static final Logger log = Logger.getLogger(LeadsPage.class);

	public LeadsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String createdPID;
	public static String createdTourID;

	protected By homeTab = By.xpath("//span[contains(.,'Home')]");
	protected By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	protected By newLeads = By.xpath("//div[@title='New']");
	protected By salutationField = By.xpath("//div[contains(@class,'salutation')]//a");
	protected By firstName = By.xpath("//input[contains(@class,'firstName')]");
	protected By lastName = By.xpath("//input[contains(@class,'lastName')]");
	protected By mobile = By.xpath("//input[@type='tel']");// used as webelement list - first entry
	protected By email = By.xpath("//input[@type='email']");
	protected By leadName = By
			.xpath("//span[contains(.,'Temporary Lead Identifier')]/parent::label/following-sibling::input");
	protected By pidEnter = By.xpath("//span[text()='PID']/parent::label/following-sibling::input");
	protected By streetEnter = By.xpath("//textarea[contains(@class,'street ')]");
	protected By cityEnter = By.xpath("//input[contains(@class,'city')]");
	protected By zipEnter = By.xpath("//input[contains(@class,'postalCode')]");
	// protected By stateEnter = By.xpath("//a[(@class='select')]"); // to be used
	// as web element list-last entry
	protected By stateEnter = By.xpath(
			"//span[@data-aura-class='uiPicklistLabel']//span[text()='State/Province']/../..//a[@class='select']");
	protected By countryEnter = By.xpath("//a[(@class='select')]"); // to be used as web element list- 2nd last entry
	protected By saveButton = By.xpath("//button[@title='Save'] "); // or //span[text()='Save']
	protected By createNewTour = By.xpath("//div[contains(text(),'Create New Tour Record')]");
	protected By acsButton = By.id("tourCheckBTN");
	protected By tourRecords = By.xpath("//span[text()='Tour Records']");
	protected By tourlanguage = By.xpath("//span[contains(text(),'Language')]/parent::span/following-sibling::div//a");
	protected By qualificationStatus = By
			.xpath("//span[contains(text(),'Qualification')]/parent::span/following-sibling::div//a");
	protected By qSelect = By.xpath("//a[@title='Q']");
	protected By qualificationSave = By.xpath("//div[@class='modal-footer slds-modal__footer']//span[text()='Save']");
	protected By tourIdText = By.xpath("//a[@data-refid='recordId']");
	protected By txtDuplicateMsg = By.xpath("//div[contains(text(),'duplicate')]");
	protected By searchAddressLink = By.xpath("//span[text()='Search Address']");
	protected By enterAddressEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'uiInput--default')]");
	protected By addressOption = By.xpath("(//div[@class='option'])[1]");
	protected By globalSearchEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'default')]");
	protected By selectLeadItem = By.xpath("(//span[@class='uiImage']/img[contains(@class,'icon')])[1]");
	protected By commentsEdit = By.xpath("//span[text()='Comments']/../..//input");
	protected By giftEligbleBtn = By.xpath("//button[text()='Incentive Eligible']");
	protected By closePopUpBtn = By.xpath("//button[text()='Close']");
	protected By txtErrorMsg = By
			.xpath("//p[contains(text(),'This lead has an international address and does not require soft scoring')]");
	protected By txtErrorMsgCondition = By
			.xpath("//p[contains(text(),'Please select a Location for the Lead to check Qualification Score')]");
	protected By reqSoftScoreBtn = By.xpath("//button[text()='Request Soft Score']");
	protected By txtQualifiedMsg = By
			.xpath("//p[contains(text(),'This guest received an active score within the last 30 days')]");
	protected By latestTour = By.xpath("//span[text()='Tour Record']//div");

	public void createLead(String addressEnter) {
		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);

			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			// clickElementBy(salutationSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(firstName, testData.get("FirstName"));
			fieldDataEnter(lastName, testData.get("LastName"));
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

//			fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			fieldDataEnter(leadName, testData.get("LeadName"));
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
//				clickElementBy(countryEnter);
				List<WebElement> country = driver.findElements(countryEnter);
				country.get(country.size() - 2).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement countrySelect = driver
						.findElement(By.xpath("//a[text()='" + testData.get("Country").trim() + "']"));
				countrySelect.click();

				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));

//				clickElementBy(stateEnter);
				List<WebElement> State = driver.findElements(countryEnter);
				State.get(State.size() - 1).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State").trim() + "')]"));
				stateSelect.click();

				// clickElementBy(stateSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			clickElementBy(saveButton);
			WebElement SAVE = driver.findElement(saveButton);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", SAVE);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}
			} else {
				waitUntilElementVisibleBy(driver, createNewTour, 120);
				WebElement lblLeadConfirm = driver
						.findElement(By.xpath("//div[contains(@class,'page-header')]/span[contains(text(),'"
								+ testData.get("LeadName") + "')]"));

				if (verifyElementDisplayed(lblLeadConfirm)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"New Lead Created by Name : " + testData.get("LeadName"));
					// + "//with PID : "+createdPID);
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL, "Lead Creation Failed");
				}
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"User is unable to navigate to Leads creation page");
		}

	}

	public void searchForLead() {
		fieldDataEnter(globalSearchEdit, testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(selectLeadItem);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement lblLeadConfirm = driver.findElement(By.xpath(
				"//div[contains(@class,'page-header')]/span[contains(text(),'" + testData.get("LeadName") + "')]"));
		if (verifyElementDisplayed(lblLeadConfirm)) {
			tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.PASS,
					"Lead details is displayed after searching for lead : " + testData.get("LeadName"));
		} else {
			tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.FAIL,
					"Unable to find the Lead details after searching for lead : " + testData.get("LeadName"));
		}
	}

	public void createTourFromLead() {
		clickElementBy(createNewTour);
		/*
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, qualificationStatus, 120);
		 * if(driver.findElement(commentsEdit).getAttribute("value").contains(
		 * "This guest was not soft scored")){ tcConfig.updateTestReporter("LeadsPage",
		 * "createTourFromLead", Status.PASS,
		 * "Error Message is displayed during creation of Tours" ); }else{
		 * tcConfig.updateTestReporter("LeadsPage", "createTourFromLead", Status.FAIL,
		 * "Error Message is NOT displayed during creation of Tours"); }
		 * 
		 * clickElementBy(qualificationStatus);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * clickElementBy(By.xpath("//a[@title='"+testData.get(
		 * "QualificationScore")+"']"));
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(qualificationSave);
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		/*
		 * waitUntilElementVisibleBy(driver, tourRecords, 120);
		 * 
		 * if(verifyObjectDisplayed(tourRecords)) { clickElementBy(tourRecords);
		 * waitForSometime(tcConfig.getConfig().get("MedWait")); List<WebElement>
		 * listtourRecord= driver.findElements(tourIdText);
		 * tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
		 * "Tour Created with No. " +listtourRecord.get(0).getText());
		 * 
		 * }else { tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
		 * "Tour Creation Failed"); }
		 */
	}

	public void checkGiftEligibility() {
		waitUntilObjectVisible(driver, giftEligbleBtn, 120);
		clickElementBy(giftEligbleBtn);
		waitUntilObjectVisible(driver, closePopUpBtn, 120);
		if (verifyObjectDisplayed(txtErrorMsg)) {
			clickElementBy(closePopUpBtn);
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.PASS,
					"Error message is displayed properly");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.FAIL,
					"Error message NOT displayed properly");
			clickElementBy(closePopUpBtn);
		}
	}

	public void checkGiftEligibilityLeads() {
		waitUntilObjectVisible(driver, giftEligbleBtn, 120);
		clickElementBy(giftEligbleBtn);
		waitUntilObjectVisible(driver, reqSoftScoreBtn, 120);
		if (verifyObjectDisplayed(txtErrorMsgCondition)) {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibilityLeads", Status.PASS,
					"User is able to softscore new Lead");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibilityLeads", Status.FAIL,
					"User is unable to softscore");
		}
		clickElementBy(reqSoftScoreBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		clickElementBy(giftEligbleBtn);
		waitUntilObjectVisible(driver, closePopUpBtn, 120);
		if (verifyObjectDisplayed(txtQualifiedMsg)) {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.PASS,
					"User is unable to softscore the Lead as it " + "is already softscored");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkGiftEligibility", Status.FAIL,
					"User is able to softscore the Lead even if it " + "is already softscored");
		}
		clickElementBy(closePopUpBtn);

	}

	public void createLeadAndTour(String addressEnter) {

		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
//			driver.findElement(By.xpath("//span[text()='Next']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);
			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(firstName, testData.get("FirstName"));
			fieldDataEnter(lastName, testData.get("LastName"));
			List<WebElement> Number = driver.findElements(mobile);
			Number.get(0).sendKeys(testData.get("MobileNo"));

//			fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			fieldDataEnter(leadName, testData.get("LeadName"));
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> addList = driver.findElements(stateEnter);
				addList.get(addList.size() - 1).click();
//				clickElementBy(stateEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
				stateSelect.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			clickElementBy(saveButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, createNewTour, 120);
			clickElementBy(createNewTour);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			clickElementBy(qualificationStatus);
			WebElement qualificationStatusSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("QualificationScore") + "']"));
			qualificationStatusSelect.click();
			clickElementBy(tourlanguage);
			WebElement tourlanguageSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
			tourlanguageSelect.click();
			waitUntilObjectVisible(driver, qualificationSave, 120);
			clickElementBy(qualificationSave);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}
			}
		}
	}

	public void getlatestTour() {

		waitUntilElementVisibleBy(driver, latestTour, 120);
		if (verifyObjectDisplayed(latestTour)) {
			createdTourID = driver.findElement(latestTour).getText();
			System.out.println("created TourID: " + createdTourID);
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.PASS, "created TourID: " + createdTourID);
			driver.findElement(latestTour).click();
		} else {
			tcConfig.updateTestReporter("LeadPage", "getlatestTour", Status.FAIL, "Tour not created");
		}

	}

	protected By btnincentive = By.xpath("//button[@id='tourCheckBTN' and contains(.,'Incentive Eligible')]");
	protected By softscore = By.xpath("//button[text()='Request Soft Score']");
	protected By score = By.xpath("//span[text()='Qualification Status']/ancestor::div[@role='listitem']//span[text()='"
			+ testData.get("QualificationScore") + "']");

	public void clickIncentivebuttonandsoftscore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, btnincentive, 120);
		clickElementBy(btnincentive);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, softscore, 120);
		clickElementBy(softscore);
		if (verifyElementDisplayed(driver.findElement(score))) {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Soft score successful");
		} else {
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Failed while Soft Scoring");
		}
	}

	public void validatescore() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, score, 120);
		String Score = driver.findElement(score).getText();
		if (Score.equalsIgnoreCase(testData.get("QualificationScore"))) {
			tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS, "Qualification Score is: " + Score);
		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"Failed while getting Qualification Score");
		}

	}

	protected By crossBtn = By.xpath("//div[@role='combobox']//span/button");
	protected By check = By.xpath("//a[contains(@class,'textUnderline outputLookupLink')]"); // used as Webelement list
																								// - last entry
	String Creditcode = testData.get("QualificationScore");
	protected By creditBandCode = By.xpath("//span[(text()='" + Creditcode.trim() + "')]");
	protected By creditBand = By.xpath("//div//span[text()='Credit Band Code']//..//following-sibling::div");

	@FindBy(xpath = "//div[@class='slds-form-element']//input[@class='slds-input slds-combobox__input']")
	WebElement SearchLocation;

	@FindBy(xpath = "//a[@title='Related']")
	WebElement relatedTab;

	public void checkQualificationScore() throws AWTException {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, giftEligbleBtn, 240);
		clickElementBy(giftEligbleBtn);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, crossBtn, 120);
		clickElementBy(crossBtn);
		SearchLocation.sendKeys(testData.get("AppointmentLoc"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Robot rob;

		rob = new Robot();
		rob.mouseWheel(15);
		List<WebElement> resultList = driver.findElements(By.xpath("//li[@class='slds-listbox__item']"));

		resultList.get(0).click();

		waitUntilObjectVisible(driver, reqSoftScoreBtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(reqSoftScoreBtn);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		relatedTab.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		Robot rob1;

		rob1 = new Robot();
		rob1.mouseWheel(10);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		List<WebElement> Check = driver.findElements(check);

		waitUntilObjectVisible(driver, Check.get(Check.size() - 1), 120);
		// clickElementBy(check);
//		clickElementJS(check);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Check.get(Check.size() - 1).click();
		System.out.println("The code is here");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilObjectVisible(driver, creditBandCode, 120);
		if (verifyElementDisplayed(driver.findElement(creditBand))) {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.PASS,
					"Credit Band Code is: " + (driver.findElement(creditBand).getText()));
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
					"Failed while getting Credit Band Code");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

//	String Creditcode=testData.get("QualificationScore");
//	protected By creditBandCode =	By.xpath("//span[(text()='"+Creditcode.trim()+"')]");
//	protected By checkQualificationScore = By.xpath("//span[text()='Qualification Status']//..//..//span[text()='"+testData.get("QualificationScore")+"']");
	protected By btnMailLetter = By.id("refresh");
	protected By btnPrintLetter = By.id("leadPrint");

	public void scorValidation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, giftEligbleBtn, 120);
		clickElementBy(giftEligbleBtn);
		waitUntilObjectVisible(driver, reqSoftScoreBtn, 120);

		clickElementBy(reqSoftScoreBtn);
		waitUntilObjectVisible(driver, creditBandCode, 120);

		/*
		 * if(driver.findElement(checkQualificationScore).getText().equalsIgnoreCase(
		 * testData.get("QualificationScore"))||
		 * driver.findElement(checkQualificationScore).getText().equalsIgnoreCase(
		 * testData.get("TourNo"))){ tcConfig.updateTestReporter("LeadsPage",
		 * "validatescore", Status.PASS,
		 * "Check Qualification is: "+checkQualificationScore); }else{
		 * tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore",
		 * Status.FAIL, "Failed while getting Qualification"); }
		 */

		if (verifyElementDisplayed(driver.findElement(creditBandCode))) {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.PASS,
					"Check Qualification is: " + testData.get("QualificationScore"));
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
					"Failed while getting Qualification");
		}
	}

	public void printButtonNotEnabled() {

		if (!driver.findElement(btnPrintLetter).isEnabled()) {
			tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS, "Print button is disabled ");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL, "Print button is enabled");
		}
	}

	public void mailLetterButtonNotEnabled() {

		if (!driver.findElement(btnMailLetter).isEnabled()) {
			tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS, "Mail Letter button is disabled ");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore", Status.FAIL,
					"Mail Letter button is enabled");
		}
	}

	public void mailLetterButtonClick() {

		/*
		 * if(!driver.findElement(btnMailLetter).isEnabled()){
		 * tcConfig.updateTestReporter("LeadsPage", "validatescore", Status.PASS,
		 * "Mail Letter button is disabled "); }else{
		 * tcConfig.updateTestReporter("LeadsPage", "checkQualificationScore",
		 * Status.FAIL, "Mail Letter button is enabled");
		 * 
		 * }
		 */
		waitUntilObjectVisible(driver, btnMailLetter, 120);
		clickElementBy(btnMailLetter);
	}
}
