package minivac.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import minivac.pages.BookingConfirmedPage;
import minivac.pages.ConfirmBookingPage;
import minivac.pages.MinvacHomepage;
import minivac.pages.PackageResultPage;
import minivac.pages.PropertyDetailPage;
import minivac.pages.RedeemPackagePage;

public class MinvacScripts_Web extends TestBase {
	public static final Logger log = Logger.getLogger(MinvacScripts_Web.class);

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_001_Minvac_SpecialRequestValidations � Description: Special Request
	 * functionality validation
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch1"})

	public void TC_001_Minvac_SpecialRequestValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPackPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);
		PropertyDetailPage propertyDetail = new PropertyDetailPage(tcconfig);
		ConfirmBookingPage confrmBooking = new ConfirmBookingPage(tcconfig);
		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();
			redeemPackPage.selectDateAndRedeem("NO");
			packageResult.clickFirstResult();
			propertyDetail.propertyDetailCheck();
			confrmBooking.specialRequestCheck();
			cleanUp();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_002_Minvac_FooterValidations � Description:Footer Links Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch1"})

	public void TC_002_Minvac_FooterValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.footerValidations();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_003_Minvac_BrandLogosValidations � Description: Header brand logos
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch1"})

	public void TC_003_Minvac_BrandLogosValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);
		PropertyDetailPage propertyDetail = new PropertyDetailPage(tcconfig);
		ConfirmBookingPage confrmBooking = new ConfirmBookingPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.headerLogos();
			homepage.retrivePackageInfo();

			redeemPage.packageBrandCheck();
			redeemPage.selectDateAndRedeem("No");

			packageResult.packageBrandCheck();
			packageResult.clickFirstResult();

			propertyDetail.propertyDetailCheck();

			confrmBooking.packageBrandCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_004_Minvac_GuestPanelValidations � Description: Guest Panel
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch1"})

	public void TC_004_Minvac_GuestPanelValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();

			redeemPage.guestPanelCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_005_Minvac_WyndhamPreferredListCheck � Description: Wyndham Preffered
	 * List check
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch1"})

	public void TC_005_Minvac_WyndhamPreferredListCheck(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();

			redeemPage.selectDateAndRedeem("No");

			packageResult.wyndhamPreferredCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_006_Minvac_CalendarValidations � Description: Calendar Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch2"})

	public void TC_006_Minvac_CalendarValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();

			redeemPage.calendarValidations();
			redeemPage.redeemPackageClick();

			packageResult.resultListCheck();

			packageResult.checkInDateChange();

			packageResult.resultListCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_007_Minvac_CompleteBooking � Description: Complete Booking Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch2"})

	public void TC_007_Minvac_CompleteBooking(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);
		PropertyDetailPage propertyDetail = new PropertyDetailPage(tcconfig);
		ConfirmBookingPage confirmBookin = new ConfirmBookingPage(tcconfig);
		BookingConfirmedPage confirmed = new BookingConfirmedPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();

			redeemPage.bookingFlowRedeemPage("NO");

			packageResult.bookingFlowResultPage();

			propertyDetail.propertyDetailCheck();

			confirmBookin.bookingFlowConfirmBooking();

			confirmed.detailsCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_008_Minvac_PropertyDetailValidations � Description: Property Details
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch2"})

	public void TC_008_Minvac_PropertyDetailValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);
		PropertyDetailPage propertyDetail = new PropertyDetailPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();

			redeemPage.selectDateAndRedeem("No");

			packageResult.clickFirstResult();

			propertyDetail.propertDetailsValidations();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_009_Minvac_WrongPackageError � Description: Wrong Package Validation
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch2"})

	public void TC_009_Minvac_WrongPackageError(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.wrongPackageInfo();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_010_Minvac_ExpDateValidations� Description: Wrong Package Validation
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch2"})

	public void TC_010_Minvac_ExpDateValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		try {

			homepage.launchAppMinvac();
			homepage.submitButtonValidations();

			redeemPage.expDateValidation();
			redeemPage.guestPanelChild();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_011_Minvac_TermsAndConditionValidation� Description: Terms and
	 * Condition buttons validations Validation
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch3"})

	public void TC_011_Minvac_TermsAndConditionValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);
		PropertyDetailPage propertyDetail = new PropertyDetailPage(tcconfig);
		ConfirmBookingPage confrmBooking = new ConfirmBookingPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();
			redeemPage.selectDateAndRedeem("No");
			packageResult.clickFirstResult();
			propertyDetail.propertyDetailCheck();
			confrmBooking.confirmBookingLanding();
			confrmBooking.phoneBoxFunctionalityCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_012_Minvac_MapFunctionalityValdation� Description: Map Pin
	 * Functionality valdns Validation
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch3"})

	public void TC_012_Minvac_MapFunctionalityValdation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();
			redeemPage.selectDateAndRedeem("No");
			packageResult.mapFunctionalityCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_013_Minvac_WyndhamNotPreferredVal� Description: Check Wynhdam
	 * Preferred not present for not preferred hotels
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch3"})

	public void TC_013_Minvac_WyndhamNotPreferredVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);
		RedeemPackagePage redeemPage = new RedeemPackagePage(tcconfig);
		PackageResultPage packageResult = new PackageResultPage(tcconfig);
		PropertyDetailPage propertyDetail = new PropertyDetailPage(tcconfig);
		ConfirmBookingPage confirmBookin = new ConfirmBookingPage(tcconfig);
		BookingConfirmedPage confirmed = new BookingConfirmedPage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.retrivePackageInfo();
			redeemPage.selectDateAndRedeem("YES");

			packageResult.clickWyndhamNotPreferd();

			propertyDetail.checkWynNotPrefPD();

			confirmBookin.checkNotWynPrefCB();

			confirmed.notWynPrefCheckBC();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date: Mar/2019 � � Version: 1 � function/event:
	 * TC_014_Minvac_DifferentErrorVal� Description: To verify the redeemed
	 * package error and incomplete data error
	 * 
	 */
	@Test(dataProvider = "testData", groups={"minivac","batch3"})

	public void TC_014_Minvac_DifferentErrorVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.incompleteErrorVal();
			homepage.redeemedPkgValidations();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups={"minivac","batch3"})

	public void TC_016_Minvac_Nobel_Chat_Validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		MinvacHomepage homepage = new MinvacHomepage(tcconfig);

		try {

			homepage.launchAppMinvac();
			homepage.validateNobelChatIcon();
			homepage.clickNobelChatIcon();
			homepage.validateNobelChatWindowPopUp(testData.get("OffHour"));
			homepage.retrivePackageInfo();
			homepage.validateNobelChatIcon();
			homepage.clickNobelChatIcon();
			homepage.validateNobelChatWindowPopUp(testData.get("OffHour"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

}