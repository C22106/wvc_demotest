package minivac.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PackageResultPage_Android extends PackageResultPage {

	public static final Logger log = Logger.getLogger(PackageResultPage_Android.class);

	public PackageResultPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void clickFirstResult() {
		resultListCheck();
		try {

			waitUntilElementVisibleBy(driver, resultHeader, 120);
			List<WebElement> resultHeaderList = driver.findElements(resultHeader);
			tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.PASS,
					"First Result of the list is: " + resultHeaderList.get(0).getText());

			// List<WebElement> resultSeeMoreInfoList =
			// driver.findElements(seeMoreInfoButton);

			resultHeaderList.get(0).click();
			waitUntilElementVisibleBy(driver, propertyDetail, 120);
			if (verifyObjectDisplayed(propertyDetail)) {
				tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.PASS,
						"Package Details Window opened");

			} else {
				tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.FAIL,
						"Error in getting property detail window");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.FAIL, "No Results Displayed");
		}
	}

	/*
	 * Method: wyndhamPreferredCheck Description: Wyndham Preffered Check: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamPreferredCheck() {
		waitUntilElementVisibleBy(driver, resultList, 120);
		if (verifyObjectDisplayed(wyndhamPrefferedList)) {
			List<WebElement> prefferedList = driver.findElements(wyndhamPrefferedList);
			tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
					"Wyndham Preferred List Present, Total: " + prefferedList.size());

			String previous = "";
			boolean result;
			for (int i = 0; i < prefferedList.size(); i++) {
				String strName = prefferedList.get(i).getText();
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
						(i + 1) + ". Wyndham Preferred Resort Name: " + strName);

				if (prefferedList.get(i).getText().compareToIgnoreCase(previous) >= 0) {
					result = true;
					previous = prefferedList.get(i).getText();

				} else {
					result = true;
				}

			}

			if (result = true) {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
						"List is sorted alphabetically");
			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.FAIL,
						"List is not sorted alphabetically");

			}

			// if (verifyObjectDisplayed(prefferedList.get(0))) {
			/*
			 * tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck",
			 * Status.PASS, "See More Info Button present"); List<WebElement>
			 * resultSeeMoreInfoList = driver.findElements(seeMoreInfoButton);
			 */

			prefferedList.get(0).click();
			waitUntilElementVisibleBy(driver, propertyDetail, 120);
			if (verifyObjectDisplayed(propertyDetail)) {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
						"Package Details Window opened");

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.FAIL,
						"Error in getting property detail window");
			}

		} /*
			 * else { tcConfig.updateTestReporter("RedeemPackagePage",
			 * "wyndhamPreferredCheck", Status.FAIL, "See More Info Button not present"); }
			 */

		else {
			tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
					"No Wyndham Preffered list present, Hence Cannot be validated");
		}

	}

	public void bookingFlowResultPage() {
		resultListCheck();

		if (verifyObjectDisplayed(viewMapCTA)) {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
					"View Map CTA present");
			clickElementBy(viewMapCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, mapList, 120);
			if (verifyObjectDisplayed(mapList)) {
				tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
						"Map is present in the List view of the result page");
				if (verifyObjectDisplayed(viewListCTA)) {
					clickElementBy(viewListCTA);
				} else {
					tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
							"View List CTA not present");
				}
			} else {
				tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
						"Map not present in list view");
			}

		} else {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
					"View Map CTA not present");
		}

		List<WebElement> resultHeaderList = driver.findElements(resultHeader);
		tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
				"First Result of the list is: " + resultHeaderList.get(0).getText());

		resultHeaderList.get(0).click();
		waitUntilElementVisibleBy(driver, propertyDetail, 120);
		if (verifyObjectDisplayed(propertyDetail)) {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
					"Package Details Window opened");

		} else {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
					"Error in getting property detail window");
		}

	}

}