package minivac.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ConfirmBookingPage extends MinvacBasePage {

	public static final Logger log = Logger.getLogger(ConfirmBookingPage.class);

	public ConfirmBookingPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By selectionHeader = By.xpath("//h1[@class='wyn-lead-requests__heading']");
	private By specialHeaderDrpDwn = By.xpath("//h4[contains(.,'Special Request')]");
	private By requestCheckBox = By.xpath("//div[@role='tabpanel']//label[@for]");
	private By specialRequestContactText = By
			.xpath("//div[@class='wyn-checkout-summary__panel-text']//p[contains(.,'Special requests')]");

	private By backToSearch = By.xpath("//span[contains(.,'BACK TO SEARCH')]");
	private By resultList = By.xpath("//div[@class='wyn-map-list']");

	private By CLUBWYNDHAMlogo = By.xpath("//img[@alt='CLUB WYNDHAM']");
	private By worldMarkLogo = By.xpath("//img[@alt='WorldMark by Wyndham']");

	private By confirmFirstName = By.xpath("//input[@id='first-name']");
	private By confirmLastName = By.xpath("//input[@id='last-name']");

	private By inputPhoneNo = By.xpath("//input[@id='phone']");

	private By agreeSmsTerms = By.xpath("//label[@for='agreeSms']");

	private By termsNConditionHeader = By.xpath("//h4[contains(.,'Terms and Conditions')]");

	private By termsAndConditionContent = By.xpath("//div[@class='wyn-checkout-summary__requirements']");

	private By agreeTermsCondition = By.xpath("//label[@for='agreeTerms']");

	private By completeBooking = By.xpath("//button[contains(.,'COMPLETE BOOKING')]");

	private By completeBookingDisabled = By.xpath("//button[contains(.,'COMPLETE BOOKING') and @disabled]");

	private By summaryImage = By.xpath("//img[@class='wyn-trip-summary__image']");

	private By summaryPreffered = By.xpath("//p[@class='wyn-trip-summary__preferred']");

	private By summaryResortHeading = By.xpath("//h3[@class='wyn-trip-summary__resort-heading']");

	private By summaryResortAddress = By.xpath("//p[@class='wyn-trip-summary__resort-address']");

	private By summaryResortLength = By
			.xpath("//h4[contains(.,'LENGTH')]//following-sibling::h5[@class='wyn-trip-summary__item-text']");

	private By summaryResortCheckIn = By
			.xpath("//h6[contains(.,'CHECK-IN')]//following-sibling::h5[@class='wyn-trip-summary__item-text']");

	private By summaryResortCheckOut = By
			.xpath("//h6[contains(.,'CHECK-OUT')]//following-sibling::h5[@class='wyn-trip-summary__item-text']");

	private By summaryResortGuests = By
			.xpath("//h6[contains(.,'GUESTS')]//following-sibling::h5[@class='wyn-trip-summary__item-text']");

	private By smsError = By.xpath("//span[@name='phoneError']");

	private By notWynPreff = By.xpath("//p[@class='wyn-trip-summary__preferred' and @style]");

	/*
	 * Method: confirmBookingLanding Description: Confirm Booking Landing Page
	 * validation: Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void confirmBookingLanding() {
		waitUntilElementVisibleBy(driver, selectionHeader, 120);
		if (verifyObjectDisplayed(selectionHeader)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "confirmBookingLanding", Status.PASS,
					"Navigated To Confirm Booking page");

		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "confirmBookingLanding", Status.FAIL,
					"Error in navigating to confirm booking page");
		}
	}

	/*
	 * Method: resortDetails Description: resort Details Validations: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void resortDetails() {
		if (verifyObjectDisplayed(confirmFirstName)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"First Name: " + getElementValue(confirmFirstName));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL, "First Name not present");
		}
		if (verifyObjectDisplayed(confirmLastName)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Last Name: " + getElementValue(confirmLastName));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL, "Last Name not present");
		}

		if (verifyObjectDisplayed(summaryImage)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Resort Summary Image Present");
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Resort Summary Image Not present for this particular resort");
		}

		if (verifyObjectDisplayed(summaryPreffered)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"This resort is Wyndham Preferred");
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"This resort is not Wyndham Preferred");
		}

		if (verifyObjectDisplayed(summaryResortHeading)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Resort Name: " + getElementText(summaryResortHeading));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL, "Resort Name not present");
		}

		if (verifyObjectDisplayed(summaryResortAddress)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Resort Address: " + getElementText(summaryResortAddress));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL,
					"Resort Address not present");
		}

		if (verifyObjectDisplayed(summaryResortLength)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Length of stay: " + getElementText(summaryResortLength));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL,
					"Length of stay not present");
		}

		if (verifyObjectDisplayed(summaryResortCheckIn)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Check In Date: " + getElementText(summaryResortCheckIn));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL,
					"Check In date not present");
		}

		if (verifyObjectDisplayed(summaryResortCheckOut)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Check Out Date: " + getElementText(summaryResortCheckOut));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL,
					"Check Out date not present");
		}

		if (verifyObjectDisplayed(summaryResortGuests)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.PASS,
					"Guests: " + getElementText(summaryResortGuests));
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "resortDetails", Status.FAIL, "Guests not present");
		}

	}
	/*
	 * Method: specialRequestCheck Description: Special request Functionality check:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */

	public void specialRequestCheck() {
		waitUntilElementVisibleBy(driver, specialHeaderDrpDwn, 120);
		scrollDownByPixel(150);

		if (verifyObjectDisplayed(specialHeaderDrpDwn)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
					"Special Request field present in the page");

			clickElementBy(specialHeaderDrpDwn);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> requestCheckBoxList = driver.findElements(requestCheckBox);

			for (int i = 0; i < requestCheckBoxList.size(); i++) {

				requestCheckBoxList.get(i).click();
				tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
						"Selected " + requestCheckBoxList.get(i).getText());

				try {

					System.out.println("Checked :" + requestCheckBoxList.get(i).isSelected());
				} catch (Exception e) {

				}

			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			for (int i = 0; i < requestCheckBoxList.size(); i++) {

				requestCheckBoxList.get(i).click();
				tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
						"UnSelected " + requestCheckBoxList.get(i).getText());

				try {

					System.out.println("Checked :" + requestCheckBoxList.get(i).isSelected());
				} catch (Exception e) {

				}

			}

			if (verifyObjectDisplayed(specialRequestContactText)) {
				tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
						"Text present for: " + getElementText(specialRequestContactText));
			} else {
				tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.FAIL,
						"Contact the Customer Service text not present in special request section");
			}

			driver.switchTo().activeElement().sendKeys(Keys.HOME);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(backToSearch)) {
				tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
						"Back to search button present in the page");

				clickElementBy(backToSearch);

				waitUntilElementVisibleBy(driver, resultList, 120);

				if (verifyObjectDisplayed(resultList)) {
					tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
							"Navigated Back to Hotels/Resort List Page");
				} else {
					tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.FAIL,
							"Failed to navigate back to Hotels/Resort List page");
				}

			} else {
				tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.FAIL,
						"Back to search button not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.FAIL,
					"Special Request field is not present in the page");
		}

	}

	/*
	 * Method: termsNdConditionCheck Description: Terms and Condition Validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void termsNdConditionCheck() {
		if (verifyObjectDisplayed(termsNConditionHeader)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "termsNdConditionCheck", Status.PASS,
					"Terms and Condition Header present");
			if (verifyObjectDisplayed(termsAndConditionContent)) {
				tcConfig.updateTestReporter("ConfirmBookingPage", "termsNdConditionCheck", Status.PASS,
						"Terms and Condition Content present");

			} else {
				tcConfig.updateTestReporter("ConfirmBookingPage", "termsNdConditionCheck", Status.FAIL,
						"Terms and Condition content not present");

			}
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "termsNdConditionCheck", Status.FAIL,
					"Terms and Condition Header not present");

		}

	}

	/*
	 * Method: completeBooking Description: Booking Complete click: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void completeBooking() {
		if (verifyObjectDisplayed(agreeTermsCondition)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "completeBooking", Status.PASS,
					"Terms and Condition Checkbox present");
			clickElementBy(agreeTermsCondition);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			try {
				if (verifyObjectDisplayed(completeBooking)) {
					tcConfig.updateTestReporter("ConfirmBookingPage", "completeBooking", Status.PASS,
							"Complete Booking button present");
					clickElementBy(completeBooking);

				} else {
					tcConfig.updateTestReporter("ConfirmBookingPage", "completeBooking", Status.FAIL,
							"Complete Booking button not present");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("ConfirmBookingPage", "completeBooking", Status.FAIL,
						"Complete Booking button not enabled");
			}
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "completeBooking", Status.FAIL,
					"Agree Terms checkbox not present");
		}
	}

	/*
	 * Method: packageBrandCheck Description: Package Brand Check: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void packageBrandCheck() {

		if (verifyObjectDisplayed(CLUBWYNDHAMlogo) && verifyObjectDisplayed(worldMarkLogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.FAIL,
					"Both the brand logo displayed for the package");
		} else if (verifyObjectDisplayed(CLUBWYNDHAMlogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.PASS,
					"The package is under Club Wyndham Brand");

		} else if (verifyObjectDisplayed(worldMarkLogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.PASS,
					"The package is under World Mark By Wyndham Brand");

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.FAIL,
					"No Brand displayed for this package");
		}

	}

	/*
	 * Method: bookingFlowConfirmBooking Description: Confirmed Booking Flow Check:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void bookingFlowConfirmBooking() {
		confirmBookingLanding();
		resortDetails();

		if (verifyObjectDisplayed(inputPhoneNo)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "bookingFlowConfirmBooking", Status.PASS,
					"Phone number box present");
			fieldDataEnter(inputPhoneNo, testData.get("strPhoneNo"));

			if (verifyObjectDisplayed(agreeSmsTerms)) {
				clickElementBy(agreeSmsTerms);
			} else {
				tcConfig.updateTestReporter("ConfirmBookingPage", "bookingFlowConfirmBooking", Status.FAIL,
						"Agree Sms Terms not present");
			}

		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "bookingFlowConfirmBooking", Status.FAIL,
					"Phone number box not present");
		}

		scrollDownForElementJSBy(specialHeaderDrpDwn);
		scrollUpByPixel(100);

		if (verifyObjectDisplayed(specialHeaderDrpDwn)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
					"Special Request field present in the page");

			clickElementBy(specialHeaderDrpDwn);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> requestCheckBoxList = driver.findElements(requestCheckBox);

			for (int i = 0; i < 2; i++) {

				requestCheckBoxList.get(i).click();
				tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.PASS,
						"Selected " + requestCheckBoxList.get(i).getText());

				try {

					System.out.println("Checked :" + requestCheckBoxList.get(i).isSelected());
				} catch (Exception e) {

				}

			}
			clickElementBy(specialHeaderDrpDwn);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "specialRequestCheck", Status.FAIL,
					"Special Request field is not present in the page");
		}

		termsNdConditionCheck();
		completeBooking();

	}

	/*
	 * Method: phoneBoxFunctionalityCheck Description: Phone Box Functionality
	 * Check: Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void phoneBoxFunctionalityCheck() {

		waitUntilElementVisibleBy(driver, completeBooking, 120);

		driver.switchTo().activeElement().sendKeys(Keys.END);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(completeBookingDisabled)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.PASS,
					"Booking Button is disabled when  terms and condition box unchecked");
		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.FAIL,
					"Booking button enabled when T&C box unchecked");
		}

		driver.switchTo().activeElement().sendKeys(Keys.HOME);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(inputPhoneNo)) {

			tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.PASS,
					"Phone Number Box Present");

			fieldDataEnter(inputPhoneNo, testData.get("strPhoneNo"));

			driver.switchTo().activeElement().sendKeys(Keys.END);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			completeBooking();

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(completeBookingDisabled)) {
				tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.FAIL,
						"Booking Button is disabled when  terms and condition box checked");
			} else {
				tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.PASS,
						"Booking button enabled when T&C box checked");
			}

			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(smsError)) {
				tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.PASS,
						"Sms Error present: " + getElementText(smsError));
			} else {

				tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.FAIL,
						"Sms Error not present: ");
			}

			if (verifyObjectDisplayed(agreeSmsTerms)) {
				clickElementBy(agreeSmsTerms);
				tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.PASS,
						"Agree term checkbox clicked, Agree Sms Terms present: " + getElementText(agreeSmsTerms));

			} else {
				tcConfig.updateTestReporter("ConfirmBookingPage", "bookingFlowConfirmBooking", Status.FAIL,
						"Agree Sms Terms not present");
			}

		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "phoneBoxFunctionalityCheck", Status.PASS,
					"Phone Number Box not Present");
		}

	}

	/*
	 * Method: checkNotWynPrefCB Description: Wyndham Not Prefferd validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void checkNotWynPrefCB() {

		/*
		 * if(verifyObjectDisplayed(notWynPreff)) {
		 * tcConfig.updateTestReporter("ConfirmBookingPage", "checkWynNotPrefPD",
		 * Status.PASS, "Wyndham Prefferd label not present"); }else {
		 * tcConfig.updateTestReporter("ConfirmBookingPages", "checkWynNotPrefPD",
		 * Status.FAIL, "Wyndham Preffered label present"); }
		 */

		if (verifyObjectDisplayed(inputPhoneNo)) {
			tcConfig.updateTestReporter("ConfirmBookingPage", "bookingFlowConfirmBooking", Status.PASS,
					"Phone number box present");
			fieldDataEnter(inputPhoneNo, testData.get("strPhoneNo"));

			if (verifyObjectDisplayed(agreeSmsTerms)) {
				clickElementBy(agreeSmsTerms);
			} else {
				tcConfig.updateTestReporter("ConfirmBookingPage", "bookingFlowConfirmBooking", Status.FAIL,
						"Agree Sms Terms not present");
			}

		} else {
			tcConfig.updateTestReporter("ConfirmBookingPage", "bookingFlowConfirmBooking", Status.FAIL,
					"Phone number box not present");
		}

		driver.switchTo().activeElement().sendKeys(Keys.END);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		completeBooking();

	}
}
