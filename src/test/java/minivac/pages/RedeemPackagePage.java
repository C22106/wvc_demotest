package minivac.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class RedeemPackagePage extends MinvacBasePage {

	public static final Logger log = Logger.getLogger(RedeemPackagePage.class);

	public RedeemPackagePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By destinationName = By
			.xpath("//p[contains(.,'DESTINATION')]//following-sibling::h2 | //h2[@data-dest='destination']");
	protected By calendarClick = By.xpath("//div[@class='wyn-search-bar__block-accordion']//button");
	protected By selectMiddleMonthDate = By
			.xpath("//div[@class='ui-datepicker-group ui-datepicker-group-middle']//td/a");
	protected By redeemPackageButton = By.xpath("//div[@class='column wyn-search-bar__block-button ']/button");//// div[@class='column
																												//// wyn-search-bar__block-button
																												//// ']/button
	// button[contains(.,'REDEEM PACKAGE')]
	protected By resultList = By.xpath("//div[@class='wyn-map-list']");
	protected By CLUBWYNDHAMlogo = By.xpath("//img[@alt='CLUB WYNDHAM']");
	protected By worldMarkLogo = By.xpath("//img[@alt='WorldMark by Wyndham']");
	protected By enabledDate = By.xpath("//td[@data-handler='selectDay']/a");
	protected By nextButton = By.xpath("//a[@title='Next']");
	protected By selectedDate = By.xpath(
			"//a[@class='ui-state-default ui-state-active'] | //td[contains(@class,'ui-datepicker-selected-day')]");
	protected By displayedDates = By
			.xpath("//h3[@class='wyn-search-bar__subtitle wyn-search-bar__subtitle-calendar-label']/span");

	protected By guestDrpDwn = By.xpath("//div[@class='wyn-search-bar__block-accordion--guest']");
	protected By guestPanel = By.xpath("//div[@class='wyn-filters__dropdown__panel wyn-search-bar__panel-guest']");
	protected By minusButtonChild = By
			.xpath("//span[@class='wyn-form-guest__link-button js-form-guest-minus-button-children']");
	protected By childNumber = By.xpath("//span[@class='wyn-type-title-2 js-form-guest-children-numb']");
	protected By plusButtonChild = By.xpath(
			"//span[@class='wyn-form-guest__link-button wyn-form-guest__link-button__plus js-form-guest-plus-button-children']");
	protected By minusBtnAdult = By
			.xpath("//span[@class='wyn-form-guest__link-button js-form-guest-minus-button-adults']");
	protected By adultNumber = By.xpath("//span[@class='wyn-type-title-2 js-form-guest-adults-numb']");
	protected By plusBtnAdult = By.xpath(
			"//span[@class='wyn-form-guest__link-button wyn-form-guest__link-button__plus js-form-guest-plus-button-adults']");
	// @style contains=cursor: not-allowed;
	protected By closeGuestPanel = By
			.xpath("//div[@class='wyn-filters__dropdown__panel wyn-search-bar__panel-guest']//a[contains(.,'close')]");

	protected By selectedMMYY = By
			.xpath("//a[@class='ui-state-default ui-state-active']//ancestor::table/parent::div/div/div/span");

	protected By highlightedWeekEndDate = By
			.xpath("//td[@class=' ui-datepicker-week-end ui-state-highlight end-date-range']/a");

	protected By highlightedEndDate = By.xpath("//td[@class=' ui-state-highlight end-date-range']/a");
	protected By cardSummaryTitle = By.xpath("//div[@class='wyn-card-summary__title']//h2");
	protected By cardSummaryContent = By
			.xpath("//div[@class='wyn-card-summary']//div[@class='wyn-card-summary__content'][2]");

	protected By confirmationNoInSummary = By.xpath("//div[@class='wyn-card-summary__label wyn-type-overline']/p");
	protected By expiryDateInSummary = By
			.xpath("//div[@class='wyn-card-summary__label--right wyn-type-overline']/p/span");
	protected By childrenAgeSelect = By.xpath("//select[@id='wyn-form-guest__children-age-select-1']");
	protected By guestPanelInfo = By.xpath("//div[@class='wyn-type-caption wyn-form-guest__bar-info']//p");
	protected By expirationDateCalendar = By
			.xpath("//div[@class='columns wyn-search-bar__expiration-date-message']//h6/span");
	protected By expDateMsg = By.xpath("//div[@class='columns wyn-search-bar__expiration-date-message']//p");

	/*
	 * Method: redeemPackageClick Description: Click on redeem package button: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void redeemPackageClick() {
		waitUntilElementVisibleBy(driver, redeemPackageButton, 120);
		if (verifyObjectDisplayed(redeemPackageButton)) {

			tcConfig.updateTestReporter("RedeemPackagePage", "redeemPackageClick", Status.PASS,
					"Redeem Package Button present");

			clickElementBy(redeemPackageButton);
			waitUntilElementVisibleBy(driver, resultList, 120);
			if (verifyObjectDisplayed(resultList)) {
				tcConfig.updateTestReporter("RedeemPackagePage", "redeemPackageClick", Status.PASS,
						"Navigated To result page");
			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "redeemPackageClick", Status.FAIL,
						"Navigation To result page failed");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "redeemPackageClick", Status.FAIL,
					"Error in getting the destination");
		}

	}

	/*
	 * Method: destinationCheck Description: CCheck the destination displayed: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void destinationCheck() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, destinationName, 120);
		if (verifyObjectDisplayed(destinationName)) {

			String strDestination = getElementText(destinationName);
			String[] strSplit = strDestination.split(",");
			String strCity = strSplit[0];
			String strState = strSplit[strSplit.length - 1];

			tcConfig.updateTestReporter("RedeemPackagePage", "destinationCheck", Status.PASS,
					"Destination City is: " + strCity + " and State is: " + strState);

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "destinationCheck", Status.FAIL,
					"Error in getting the destination");
		}

	}

	/*
	 * Method: checkInDateChange Description: Check in date change: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void checkInDateChange(String selectNext) {

		if (verifyObjectDisplayed(calendarClick)) {
			clickElementBy(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS, "Calendar Opened");
			waitUntilElementVisibleBy(driver, nextButton, 120);
			if (selectNext.equalsIgnoreCase("YES")) {
				for (int i = 0; i < 3; i++) {
					if (driver.findElement(nextButton).getAttribute("class").contains("disabled")) {
						List<WebElement> enabledDateList = driver.findElements(enabledDate);
						enabledDateList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else if (verifyObjectDisplayed(nextButton)) {
						clickElementBy(nextButton);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						List<WebElement> enabledDateList = driver.findElements(enabledDate);
						enabledDateList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
				}

			} else if (selectNext.equalsIgnoreCase("NO")) {
				List<WebElement> enabledDateList = driver.findElements(enabledDate);
				enabledDateList.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			clickElementJSWithWait(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(calendarClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, selectedDate, 120);
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
					"Check In Date Highlighted and Selected as: " + getElementText(selectedDate));

			clickElementJSWithWait(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> displayedDatesList = driver.findElements(displayedDates);
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
					"Check In Date displayed as: " + displayedDatesList.get(0).getText()
							+ " Check Out Date displayed as: " + displayedDatesList.get(1).getText());

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.FAIL,
					"Error in getting calendar field");

		}

	}

	/*
	 * Method: selectDateAndRedeem Description: Select Date and redeem: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void selectDateAndRedeem(String selectNext) {
		destinationCheck();
		checkInDateChange(selectNext);
		redeemPackageClick();
	}

	/*
	 * Method: packageBrandCheck Description: Package Brand Check: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void packageBrandCheck() {

		if (verifyObjectDisplayed(CLUBWYNDHAMlogo) && verifyObjectDisplayed(worldMarkLogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.FAIL,
					"Both the brand logo displayed for the package");
		} else if (verifyObjectDisplayed(CLUBWYNDHAMlogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.PASS,
					"The package is under Club Wyndham Brand");

		} else if (verifyObjectDisplayed(worldMarkLogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.PASS,
					"The package is under World Mark By Wyndham Brand");

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.FAIL,
					"No Brand displayed for this package");
		}

	}

	/*
	 * Method: guestPanelCheck Description: Guest Panel Check: Date Mar/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void guestPanelCheck() {

		waitUntilElementVisibleBy(driver, guestDrpDwn, 120);
		if (verifyObjectDisplayed(guestDrpDwn)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
					"Guest Drop Down Present on the page");

			clickElementBy(guestDrpDwn);
			waitUntilObjectVisible(driver, guestPanel, 120);
			if (verifyObjectDisplayed(guestPanel)) {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS, "Guest Panel Opened");

				if (verifyObjectDisplayed(minusButtonChild)) {
					for (int i = 0; i < 2; i++) {
						clickElementBy(minusButtonChild);
						if (i == 1) {
							String strStyle = driver.findElement(minusButtonChild).getAttribute("style");

							if (strStyle.contains("not-allowed") && getElementText(childNumber).contains("0")) {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
										"Child Decreased to: " + getElementText(childNumber));
							} else {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
										"Error in decreasing child numbers");
							}

						} else {
							System.out.println(" ");
						}

					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
							"Child Minus Button not present");
				}

				if (verifyObjectDisplayed(minusBtnAdult)) {
					for (int i = 0; i < 2; i++) {
						clickElementBy(minusBtnAdult);
						if (i == 1) {
							String strStyle = driver.findElement(minusBtnAdult).getAttribute("style");

							if (strStyle.contains("not-allowed") && getElementText(adultNumber).contains("1")) {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
										"Adult Decreased to: " + getElementText(adultNumber)
												+ " and that's minimum number of adult required");
							} else {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
										"Error in decreasing adult numbers");
							}

						} else {
							System.out.println(" ");
						}

					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
							"Adult Minus Button not present");
				}

				if (verifyObjectDisplayed(closeGuestPanel)) {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
							"Guest Panel close button present");
					clickElementBy(closeGuestPanel);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					try {
						clickElementBy(closeGuestPanel);
					} catch (Exception e) {
						tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
								"Guest Panel closed");
					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
							"Guest Panel close button not present");
				}

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
						"Guest Panel opening error");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
					"Guest Drop Down not Present on the page");
		}

	}

	/*
	 * Method: calendarValidations Description: Calendar Validations : Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void calendarValidations() {
		waitUntilObjectVisible(driver, calendarClick, 120);
		if (verifyObjectDisplayed(calendarClick)) {
			clickElementBy(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.PASS, "Calendar Opened");
			waitUntilElementVisibleBy(driver, nextButton, 120);
			for (int i = 0; i < 3; i++) {
				if (driver.findElement(nextButton).getAttribute("class").contains("disabled")) {
					List<WebElement> enabledDateList = driver.findElements(enabledDate);
					enabledDateList.get(0).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else if (verifyObjectDisplayed(nextButton)) {
					clickElementBy(nextButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					List<WebElement> enabledDateList = driver.findElements(enabledDate);
					enabledDateList.get(0).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
			}

			if (verifyObjectDisplayed(enabledDate)) {
				List<WebElement> enabledDateList = driver.findElements(enabledDate);
				tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.PASS,
						"Total Available Dates to redeem package are: " + enabledDateList.size());

				clickElementBy(calendarClick);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(calendarClick);
				if (verifyObjectDisplayed(selectedDate)) {
					List<WebElement> selectedMonthYear = driver.findElements(selectedMMYY);
					String strMonth = selectedMonthYear.get(0).getText();
					String strYear = selectedMonthYear.get(1).getText();
					tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.PASS,
							"The first availabe date is: " + getElementText(selectedDate) + "/" + strMonth + "/"
									+ strYear);
					if (verifyObjectDisplayed(highlightedEndDate)) {
						String lastSelectedDate = getElementText(highlightedEndDate);
						tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.PASS,
								"Highlighted Dates are between: " + getElementText(selectedDate) + "/" + strMonth + "/"
										+ strYear + "-" + lastSelectedDate + "/" + strMonth + "/" + strYear);
					} else if (verifyObjectDisplayed(highlightedWeekEndDate)) {
						String lastSelectedDate = getElementText(highlightedWeekEndDate);
						tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.PASS,
								"Highlighted Dates are between: " + getElementText(selectedDate) + "/" + strMonth + "/"
										+ strYear + "-" + lastSelectedDate + "/" + strMonth + "/" + strYear);
					} else {
						tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.PASS,
								"Highlighted Date is: " + getElementText(selectedDate) + "/" + strMonth + "/" + strYear
										+ ", End date disabled");
					}

					clickElementBy(calendarClick);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					List<WebElement> displayedDatesList = driver.findElements(displayedDates);
					tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
							"Check In Date displayed as: " + displayedDatesList.get(0).getText()
									+ " Check Out Date displayed as: " + displayedDatesList.get(1).getText());

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.FAIL,
							"Error in selecting a date");
				}

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.FAIL,
						"Error in getting available dates");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "calenderValidations", Status.FAIL,
					"Calender not available");
		}

	}

	/*
	 * Method: cardSummaryCheck Description: Card Summary Chekc: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void cardSummaryCheck() {
		if (verifyObjectDisplayed(cardSummaryTitle)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "bookingFlowRedeemPage", Status.PASS,
					"Summary Title present as: " + getElementText(cardSummaryTitle));
			if (verifyObjectDisplayed(cardSummaryContent)) {
				tcConfig.updateTestReporter("RedeemPackagePage", "bookingFlowRedeemPage", Status.PASS,
						"Summary Content Present");

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "bookingFlowRedeemPage", Status.FAIL,
						"Summary Content not present");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "bookingFlowRedeemPage", Status.FAIL,
					"Summary Title not present");
		}

	}

	/*
	 * Method: bookingFlowRedeemPage Description: Booking Flow redeem page: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void bookingFlowRedeemPage(String selectNext) {
		destinationCheck();
		checkInDateChange(selectNext);
		guestPanelCheck();
		cardSummaryCheck();
		redeemPackageClick();

	}

	/*
	 * Method: expDateValidation Description: Expiry date validation: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void expDateValidation() throws ParseException {

		tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.PASS,
				getElementText(confirmationNoInSummary) + " and Expires: " + getElementText(expiryDateInSummary));

		String strExpDate = getElementText(expiryDateInSummary);

		SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd,yyyy");

		Date d1 = sdf1.parse(strExpDate);

		if (verifyObjectDisplayed(calendarClick)) {
			clickElementBy(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.PASS, "Calendar Opened");
			if (verifyObjectDisplayed(enabledDate)) {
				List<WebElement> enabledDateList = driver.findElements(enabledDate);

				enabledDateList.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(calendarClick);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(calendarClick);
				waitUntilElementVisibleBy(driver, selectedDate, 60);
				tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.PASS,
						"Check In Date Highlighted and Selected as: " + getElementText(selectedDate));

				if (verifyObjectDisplayed(expirationDateCalendar)) {
					tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.PASS,
							"Expiration Date is present in the calendar" + getElementText(expirationDateCalendar));
					String strExpDateCalendar = getElementText(expirationDateCalendar);
					Date d2 = sdf1.parse(strExpDateCalendar);

					if (d1.equals(d2)) {
						tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.PASS,
								"Expiration Date in Calendar and Summary Matches");
					} else {
						tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.FAIL,
								"Expiration Date in Calendar and Summary did not Match");
					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.FAIL,
							"Expiration Date in calendar not visible");
				}

				clickElementBy(calendarClick);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				List<WebElement> displayedDatesList = driver.findElements(displayedDates);
				tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.PASS,
						"Check In Date displayed as: " + displayedDatesList.get(0).getText()
								+ " Check Out Date displayed as: " + displayedDatesList.get(1).getText());
			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.FAIL,
						"No Date is enabled to select");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "expDateValidation", Status.FAIL,
					"Error in getting calendar field");

		}

	}

	/*
	 * Method: guestPanelChild Description: CGuest Pane Check Date Mar/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void guestPanelChild() {
		waitUntilElementVisibleBy(driver, guestDrpDwn, 120);
		if (verifyObjectDisplayed(guestDrpDwn)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.PASS,
					"Guest Drop Down Present on the page");

			clickElementBy(guestDrpDwn);

			if (verifyObjectDisplayed(guestPanel)) {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.PASS, "Guest Panel Opened");

				if (verifyObjectDisplayed(plusButtonChild)) {
					clickElementBy(plusButtonChild);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(guestPanelInfo)) {
						tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.PASS,
								"More Traveller Message Displayed: " + getElementText(guestPanelInfo));
					} else {
						tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
								"Add more guests related message is not dislayed");
					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
							"Add more child option not present");
				}

				if (verifyObjectDisplayed(minusButtonChild)) {
					clickElementBy(minusButtonChild);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(childrenAgeSelect)) {
						new Select(driver.findElement(childrenAgeSelect)).selectByVisibleText("3");
						tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.PASS,
								"Children Age Selected");
						clickElementBy(guestDrpDwn);

					} else {
						tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
								"Unable to select Child age");
					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
							"Add more child option not present");
				}

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
						"Guest Panel opening error");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
					"Guest Drop Down not Present on the page");
		}

	}

}
