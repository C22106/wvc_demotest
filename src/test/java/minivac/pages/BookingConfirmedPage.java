package minivac.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BookingConfirmedPage extends MinvacBasePage {

	public static final Logger log = Logger.getLogger(BookingConfirmedPage.class);

	public BookingConfirmedPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By confirmationPageHeading = By.xpath("//h3[@class='wyn-confirmation-page__text-heading']");
	private By confirmPageResortHeading = By.xpath("//h4[@class='wyn-confirmation-page__resort-heading']");
	private By confirmPageConfNumber = By.xpath("//p[@class='wyn-confirmation-page__confirmation-label']");
	private By confirmPageName = By.xpath("//p[@class='wyn-confirmation-page__reservation-holder']");
	private By specialRequestCheck = By.xpath("//div[@class='wyn-confirmation-page__special-requests']");
	private By reservationDetails = By.xpath("//h5");
	private By confirmationPageMap = By.xpath("//div[@id='mapId']");
	private By timeshareDate = By.xpath("//p[@class='wyn-confirmation-page__timeshare-heading-date']");
	private By emailPresent = By.xpath("//div[@class='wyn-confirmation-page__timeshare-text']/strong");
	private By emailNotPresent = By.xpath("//div[@data-selector='itineraryText']/p");
	private By printConfCTA = By.xpath("//a[contains(.,'print')]");
	private By shareConfCTA = By.xpath("//a[contains(.,'share')]");
	private By wynPreffered = By.xpath("//p[@class='wyn-confirmation-page__preferred']");

	/*
	 * Method: detailsCheck Description: Confirmed Booking Details Check: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void detailsCheck() {

		waitUntilObjectVisible(driver, confirmationPageHeading, 120);
		if (verifyObjectDisplayed(confirmationPageHeading)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					"Booking Confirmed, " + getElementText(confirmationPageHeading));
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Confirmation Header not present");
		}

		if (verifyObjectDisplayed(confirmPageResortHeading)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					"Resort name booked: " + getElementText(confirmPageResortHeading));
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Resort Name not displayed");
		}

		if (verifyObjectDisplayed(confirmPageConfNumber)) {

			if (getElementText(confirmPageConfNumber).contains(testData.get("strConfirmationNo"))) {
				tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
						getElementText(confirmPageConfNumber));

			} else {
				tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
						"Confirmation Number not correctly displayed");
			}

		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Confirmation Number not present");
		}

		if (verifyObjectDisplayed(confirmPageName)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					getElementText(confirmPageName));
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Confirmation Name not present");
		}

		if (verifyObjectDisplayed(specialRequestCheck)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					getElementText(specialRequestCheck));
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS, "No Special Request");
		}

		List<WebElement> detailsList = driver.findElements(reservationDetails);

		if (verifyElementDisplayed(detailsList.get(0))) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					"Length of stay: " + detailsList.get(0).getText());
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Length of stay not present");
		}

		if (verifyElementDisplayed(detailsList.get(1))) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					"Check In Date: " + detailsList.get(1).getText());
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Check In Date not present");
		}

		if (verifyElementDisplayed(detailsList.get(2))) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					"Check Out Date: " + detailsList.get(2).getText());
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Check Out Date not present");
		}

		if (verifyElementDisplayed(detailsList.get(3))) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					"Guests: " + detailsList.get(3).getText());
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL, "Guests not present");
		}

		if (verifyObjectDisplayed(confirmationPageMap)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS, "Map Present");
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL, "Map not present");
		}

		driver.switchTo().activeElement().sendKeys(Keys.END);

		if (verifyObjectDisplayed(timeshareDate)) {
			if (getElementText(timeshareDate).toUpperCase().contains("INVALID")) {
				tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL, "Invalid Date");
			} else {
				tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
						"Timeshare Date: " + getElementText(timeshareDate));
			}

		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Time Share date not present");
		}

		if (verifyObjectDisplayed(emailPresent)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS, "Email Present");
		} else if (verifyObjectDisplayed(emailNotPresent)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS,
					"No Email Present for the package message displayed");
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL,
					"Email related message not present");
		}

		if (verifyObjectDisplayed(printConfCTA)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.PASS, "Print Cta Present");
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck", Status.FAIL, "Print Cta not present");
		}

		/*
		 * if(verifyObjectDisplayed(shareConfCTA)){
		 * tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck",
		 * Status.PASS, "Share cta Present"); }else{
		 * tcConfig.updateTestReporter("BookingConfirmedPage", "detailsCheck",
		 * Status.FAIL, "Share Cta not present"); }
		 */

	}

	/*
	 * Method: notWynPrefCheckBC Description: Wyndham Not Preffered Validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void notWynPrefCheckBC() {

		waitUntilObjectVisible(driver, confirmationPageHeading, 120);
		if (verifyObjectDisplayed(confirmationPageHeading)) {
			tcConfig.updateTestReporter("BookingConfirmedPage", "notWynPrefCheckBC", Status.PASS,
					"Booking Confirmed, " + getElementText(confirmationPageHeading));
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "notWynPrefCheckBC", Status.FAIL,
					"Confirmation Header not present");
		}

		if (!verifyObjectDisplayed(wynPreffered)) {

			tcConfig.updateTestReporter("BookingConfirmedPage", "notWynPrefCheckBC", Status.PASS,
					"Wyndham Prefferd label not present");
		} else {
			tcConfig.updateTestReporter("BookingConfirmedPage", "notWynPrefCheckBC", Status.FAIL,
					"Wyndham Preffered label present");
		}

	}

}