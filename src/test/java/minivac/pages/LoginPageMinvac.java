package minivac.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class LoginPageMinvac extends MinvacBasePage {

	public static final Logger log = Logger.getLogger(LoginPageMinvac.class);

	public LoginPageMinvac(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By minvacLogo = By.xpath("//div[@class='wyn-header-minivac__logo-container']");
	private By termsOfuse = By.xpath("//div[@class='wyn-footer__legal-links']//a[contains(.,'Terms of Use')]");
	// private By termsOfusepage = By.xpath("//div[@id='main-content' and
	// contains(.,'Terms of Use')]");
	private By privacyNotice = By.xpath("//div[@class='wyn-footer__legal-links']//a[contains(.,'Privacy Notice')]");
	private By privacySetting = By.xpath("//div[@class='wyn-footer__legal-links']//a[contains(.,'Privacy Settings')]");
	private By privacySettingpage = By.xpath("//div[@id='header']");
	private By copyRight = By.xpath("//div[@class='wyn-footer__legal']//p");
	private By CLUBWYNDHAMlogo = By.xpath("//img[@alt='CLUB WYNDHAM']");
	private By WMWlogo = By.xpath("//img[@alt='WORLDMARK BY WYNDHAM']");
	private By confirmationTxt = By.xpath("//input[@id='confirmation']");
	private By lastnameTxt = By.xpath("//input[@id='last_name']");
	private By ziptxt = By.xpath("//input[@id='zip_code']");
	private By submitBtn = By.xpath("//button[@class='wyn-button-alternate wyn-lead-form__button']");

	/*
	 * Method: launchAppMinvac Description: launch the Minivac application: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void launchAppMinvac(String strBrowser) {
		this.driver = checkAndInitBrowser(strBrowser);
		driver.navigate().to(testData.get("URL"));
		driver.manage().window().maximize();

		waitUntilObjectVisible(driver, minvacLogo, 120);

		if (verifyObjectDisplayed(minvacLogo)) {

			tcConfig.updateTestReporter("MinvacLoginPage", "launchAppMinvac", Status.PASS,
					"Minvac application launched Successful");
		} else {
			tcConfig.updateTestReporter("MinvacLoginPage", "launchAppMinvac", Status.FAIL,
					"Unable to launch Minvac application");
		}

	}

	/*
	 * Method: footertermsOfuseLink Description: Terms of useLink Validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void footertermsOfuseLink() {

		if (verifyObjectDisplayed(termsOfuse)) {
			tcConfig.updateTestReporter("CareersLoginPage", "navigateTotermsOfuse", Status.PASS,
					"Careers Login page navigated");

			clickElementBy(termsOfuse);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String termsOfusepageURL = driver.getCurrentUrl();
			String URL = testData.get("urlTermOfUsePage");
			if (URL.equalsIgnoreCase(termsOfusepageURL)) {

				tcConfig.updateTestReporter("termsOfusePage", "navigateTotermsOfusepage", Status.PASS,
						"terms Of use page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.FAIL,
						"terms Of use page Navigation Unsuccessful");
			}

			driver.close();
			driver.switchTo().window(tabs.get(0));
		}

	}

	/*
	 * Method: footerPrivacyNoteLink Description: Privacy Notice Link validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void footerPrivacyNoteLink() {

		if (verifyObjectDisplayed(privacyNotice)) {
			tcConfig.updateTestReporter("CareersLoginPage", "navigateToprivacyNotice", Status.PASS,
					"Navigating To privacy Notice page");

			clickElementBy(privacyNotice);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String termsOfusepageURL = driver.getCurrentUrl();
			String URL = testData.get("urlPrivacyNoticePage");
			if (URL.equalsIgnoreCase(termsOfusepageURL)) {

				tcConfig.updateTestReporter("termsOfusePage", "navigateToprivacyNoticepage", Status.PASS,
						"privacy Notice page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("CareersLoginPage", "navigateToprivacyNotice", Status.FAIL,
						"privacy Notice page Navigation Unsuccessful");
			}

			driver.close();
			driver.switchTo().window(tabs.get(0));
		}

	}

	/*
	 * Method: footerPrivacySettingLink Description: Privacy Settings Link
	 * validations: Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void footerPrivacySettingLink() {

		if (verifyObjectDisplayed(privacySetting)) {
			tcConfig.updateTestReporter("CareersLoginPage", "navigateToprivacySetting", Status.PASS,
					"Navigating To privacy Setting page");

			clickElementBy(privacySetting);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			waitUntilObjectVisible(driver, privacySettingpage, 120);
			if (verifyObjectDisplayed(privacySettingpage)) {

				tcConfig.updateTestReporter("privacySettingPage", "navigateprivacySettingpage", Status.PASS,
						"privacy Setting page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.FAIL,
						"privacy Setting page Navigation Unsuccessful");
			}

			driver.close();
			driver.switchTo().window(tabs.get(0));
		}

	}

	/*
	 * Method: footerCopyRight Description: Copyright validation: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void footerCopyRight() {

		if (verifyObjectDisplayed(copyRight)) {
			String copyright = driver.findElement(copyRight).getText();
			tcConfig.updateTestReporter("MinvacLoginPage", "MinvacLoginPageCopyRight", Status.PASS,
					"Navigated to Minvac login page and copyright text is: " + copyright);

		} else {
			tcConfig.updateTestReporter("MinvacLoginPage", "navigateToMinvacLoginPage", Status.FAIL,
					"privacy Setting page Navigation Unsuccessful");
		}
	}

	/*
	 * Method: footerlogos Description: Footer Logos validations: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void footerlogos() {

		if (verifyObjectDisplayed(CLUBWYNDHAMlogo)) {
			tcConfig.updateTestReporter("MinvacLoginPage", "MinvacLoginPageCopyRight", Status.PASS,
					"Navigated to Minvac login page and CLUBWYNDHAMlogo is present");

		} else {
			tcConfig.updateTestReporter("MinvacLoginPage", "navigateToMinvacLoginPage", Status.FAIL,
					"Navigated to Minvac login page and CLUBWYNDHAMlogo is not present");
		}

		if (verifyObjectDisplayed(WMWlogo)) {
			tcConfig.updateTestReporter("MinvacLoginPage", "MinvacLoginPageCopyRight", Status.PASS,
					"Navigated to Minvac login page and WMWlogo is present");

		} else {
			tcConfig.updateTestReporter("MinvacLoginPage", "navigateToMinvacLoginPage", Status.FAIL,
					"Navigated to Minvac login page and WMWlogo is not present");
		}
	}

	/*
	 * Method: retieveConfirmationNumber Description: Confirmation Number
	 * validations: Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void retieveConfirmationNumber() {
		waitUntilObjectVisible(driver, confirmationTxt, 120);
		if (verifyObjectDisplayed(confirmationTxt)) {

			fieldDataEnter(confirmationTxt, testData.get("strConfirmationNumber"));
			fieldDataEnter(lastnameTxt, testData.get("strLastName"));
			fieldDataEnter(ziptxt, testData.get("strZip"));
			clickElementBy(submitBtn);
			tcConfig.updateTestReporter("LoginPageMinvacPage", "retieveConfirmationNumber", Status.PASS,
					"retieveConfirmationNumber is done");
			/*
			 * //waitUntilObjectVisible(driver, CLUBWYNDHAMlogo, 120);
			 * if(verifyObjectDisplayed(CLUBWYNDHAMlogo)) {
			 * 
			 * tcConfig.updateTestReporter("CLUBWYNDHAMlogo", "HeaderCLUBWYNDHAMlogo",
			 * Status.PASS, "CLUBWYNDHAMlogo is present in header");
			 * 
			 * }else { tcConfig.updateTestReporter("CLUBWYNDHAMlogo",
			 * "HeaderCLUBWYNDHAMlogo", Status.PASS,
			 * "CLUBWYNDHAMlogo is not present in header"); }
			 */
		}

	}

}
