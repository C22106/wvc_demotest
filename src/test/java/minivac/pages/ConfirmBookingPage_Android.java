package minivac.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class ConfirmBookingPage_Android extends ConfirmBookingPage {

	public static final Logger log = Logger.getLogger(ConfirmBookingPage_Android.class);

	public ConfirmBookingPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
}