package minivac.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PackageResultPage extends MinvacBasePage {

	public static final Logger log = Logger.getLogger(PackageResultPage.class);

	public PackageResultPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By resultList = By.xpath("//div[@class='wyn-map-list']");
	protected By noOfResult = By.xpath("//h3[contains(.,'Results Found')]/span");
	protected By resultHeader = By
			.xpath("//h3[@class='wyn-map-list__text-card-title wyn-lead-results__open-modal-link']");
	protected By seeMoreInfoButton = By.xpath("//a[contains(.,'MORE INFO')]");
	protected By propertyDetail = By.xpath("//h1[contains(.,'PROPERTY DETAILS')]");
	protected By CLUBWYNDHAMlogo = By.xpath("//img[@alt='CLUB WYNDHAM']");
	protected By worldMarkLogo = By.xpath("//img[@alt='WorldMark by Wyndham']");
	protected By wyndhamPrefferedList = By.xpath("//p[@class='wyn-map-list__text-card-preferred']//ancestor::li//h3");

	protected By calendarClick = By
			.xpath("//div[@class='wyn-search-bar__block-accordion wyn-search-bar__block-accordion--results']//button");
	protected By enabledDate = By.xpath("//td[@data-handler='selectDay']/a");
	protected By selectedDate = By.xpath("//a[@class='ui-state-default ui-state-active']");
	protected By displayedDates = By
			.xpath("//h3[@class='wyn-search-bar__subtitle wyn-search-bar__subtitle-calendar-label']/span");
	protected By selectedMMYY = By
			.xpath("//a[@class='ui-state-default ui-state-active']//ancestor::table/parent::div/div/div/span");
	protected By viewMapCTA = By.xpath("//span[contains(.,'VIEW MAP')]");
	protected By viewListCTA = By.xpath("//span[contains(.,'VIEW LIST')]");
	protected By mapList = By.xpath("//div[@class='wyn-map']//div[@id='mapList']");

	protected By blueMarker = By.xpath("//div[@class='wyn-icon-minivac__marker wyn-icon-minivac__marker--blue']");
	protected By orangeMarker = By.xpath(
			"//div[@class='wyn-icon-minivac__marker wyn-icon-minivac__marker--blue wyn-icon-minivac__marker--orange']");
	protected By activeCard = By.xpath(
			"//li[@class='wyn-map-list__results-card wyn-map-list__results-card-js wyn-map-list__results-card--show-map is-active']");

	protected By resultCards = By.xpath("//li[@class='wyn-map-list__results-card wyn-map-list__results-card-js']");

	/*
	 * Method: resultListCheck Description: Results List Check Validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void resultListCheck() {
		waitUntilElementVisibleBy(driver, resultList, 120);
		if (verifyObjectDisplayed(resultList)) {
			tcConfig.updateTestReporter("PackageResultPage", "resultListCheck", Status.PASS,
					"Result List present with total: " + getElementText(noOfResult));

		} else {
			tcConfig.updateTestReporter("PackageResultPage", "resultListCheck", Status.FAIL,
					"Error in getting the result list");
		}

	}

	/*
	 * Method: clickFirstResult Description: First Result click and validate: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void clickFirstResult() {
		resultListCheck();
		try {

			waitUntilElementVisibleBy(driver, resultHeader, 120);
			List<WebElement> resultHeaderList = driver.findElements(resultHeader);
			tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.PASS,
					"First Result of the list is: " + resultHeaderList.get(0).getText());

			List<WebElement> resultSeeMoreInfoList = driver.findElements(seeMoreInfoButton);

			resultSeeMoreInfoList.get(0).click();
			waitUntilElementVisibleBy(driver, propertyDetail, 120);
			if (verifyObjectDisplayed(propertyDetail)) {
				tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.PASS,
						"Package Details Window opened");

			} else {
				tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.FAIL,
						"Error in getting property detail window");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("PackageResultPage", "clickFirstResult", Status.FAIL, "No Results Displayed");
		}

	}

	/*
	 * Method: packageBrandCheck Description: Check the package brand: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void packageBrandCheck() {

		if (verifyObjectDisplayed(CLUBWYNDHAMlogo) && verifyObjectDisplayed(worldMarkLogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.FAIL,
					"Both the brand logo displayed for the package");
		} else if (verifyObjectDisplayed(CLUBWYNDHAMlogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.PASS,
					"The package is under Club Wyndham Brand");

		} else if (verifyObjectDisplayed(worldMarkLogo)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.PASS,
					"The package is under World Mark By Wyndham Brand");

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "packageBrandCheck", Status.FAIL,
					"No Brand displayed for this package");
		}

	}

	/*
	 * Method: wyndhamPreferredCheck Description: Wyndham Preffered Check: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamPreferredCheck() {
		waitUntilElementVisibleBy(driver, resultList, 120);
		if (verifyObjectDisplayed(wyndhamPrefferedList)) {
			List<WebElement> prefferedList = driver.findElements(wyndhamPrefferedList);
			tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
					"Wyndham Preferred List Present, Total: " + prefferedList.size());

			String previous = "";
			boolean result;
			for (int i = 0; i < prefferedList.size(); i++) {
				String strName = prefferedList.get(i).getText();
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
						(i + 1) + ". Wyndham Preferred Resort Name: " + strName);

				if (prefferedList.get(i).getText().compareToIgnoreCase(previous) >= 0) {
					result = true;
					previous = prefferedList.get(i).getText();

				} else {
					result = true;
				}

			}

			if (result = true) {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
						"List is sorted alphabetically");
			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.FAIL,
						"List is not sorted alphabetically");

			}

			if (verifyObjectDisplayed(seeMoreInfoButton)) {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
						"See More Info Button present");
				List<WebElement> resultSeeMoreInfoList = driver.findElements(seeMoreInfoButton);

				resultSeeMoreInfoList.get(0).click();
				waitUntilElementVisibleBy(driver, propertyDetail, 120);
				if (verifyObjectDisplayed(propertyDetail)) {
					tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
							"Package Details Window opened");

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.FAIL,
							"Error in getting property detail window");
				}

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.FAIL,
						"See More Info Button not present");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "wyndhamPreferredCheck", Status.PASS,
					"No Wyndham Preffered list present, Hence Cannot be validated");
		}

	}

	/*
	 * Method: checkInDateChange Description: Check in Date Change: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void checkInDateChange() {

		if (verifyObjectDisplayed(calendarClick)) {
			clickElementBy(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS, "Calendar Opened");

			List<WebElement> enabledDateList = driver.findElements(enabledDate);
			if (enabledDateList.size() == 1) {
				tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
						"Only single date available, Clicking the same");
				enabledDateList.get(0).click();
			} else if (enabledDateList.size() > 4) {
				enabledDateList.get(4).click();
			} else if (enabledDateList.size() > 1 && enabledDateList.size() <= 4) {
				enabledDateList.get(enabledDateList.size() - 1).click();
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(calendarClick);
			waitUntilElementVisibleBy(driver, selectedDate, 60);
			List<WebElement> selectedMonthYear = driver.findElements(selectedMMYY);
			String strMonth = selectedMonthYear.get(0).getText();
			String strYear = selectedMonthYear.get(1).getText();
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
					"Check In Date Highlighted and Selected as: " + getElementText(selectedDate) + "/" + strMonth + "/"
							+ strYear);

			clickElementBy(calendarClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> displayedDatesList = driver.findElements(displayedDates);
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
					"Check In Date displayed as: " + displayedDatesList.get(0).getText()
							+ " Check Out Date displayed as: " + displayedDatesList.get(1).getText());

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.FAIL,
					"Error in getting calendar field");

		}

	}

	/*
	 * Method: bookingFlowResultPage Description: Booking Flow Results page
	 * validation: Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void bookingFlowResultPage() {
		resultListCheck();

		if (verifyObjectDisplayed(viewMapCTA)) {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
					"View Map CTA present");
			clickElementBy(viewMapCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, mapList, 120);
			if (verifyObjectDisplayed(mapList)) {
				tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
						"Map is present in the List view of the result page");
				if (verifyObjectDisplayed(viewListCTA)) {
					clickElementBy(viewListCTA);
				} else {
					tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
							"View List CTA not present");
				}
			} else {
				tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
						"Map not present in list view");
			}

		} else {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
					"View Map CTA not present");
		}

		List<WebElement> resultHeaderList = driver.findElements(resultHeader);
		tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
				"First Result of the list is: " + resultHeaderList.get(0).getText());

		List<WebElement> resultSeeMoreInfoList = driver.findElements(seeMoreInfoButton);

		resultSeeMoreInfoList.get(0).click();
		waitUntilElementVisibleBy(driver, propertyDetail, 120);
		if (verifyObjectDisplayed(propertyDetail)) {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.PASS,
					"Package Details Window opened");

		} else {
			tcConfig.updateTestReporter("PackageResultPage", "bookingFlowResultPage", Status.FAIL,
					"Error in getting property detail window");
		}

	}

	/*
	 * Method: mapFunctionalityCheck Description: Check the map functionality: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void mapFunctionalityCheck() {
		resultListCheck();
		waitUntilElementVisibleBy(driver, resultHeader, 120);
		if (verifyObjectDisplayed(viewMapCTA)) {
			tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck", Status.PASS,
					"View Map CTA present");
			clickElementBy(viewMapCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, mapList, 120);
			if (verifyObjectDisplayed(mapList)) {
				tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck", Status.PASS,
						"Map is present in the List view of the result page");
				/*
				 * try { List<WebElement> hotelMarkerList = driver.findElements(blueMarker);
				 * tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.PASS, "Total Hotel Marker present in the Map: " +
				 * hotelMarkerList.size()); if (hotelMarkerList.size() > 1) { for (int i = 0; i
				 * < hotelMarkerList.size(); i++) { try {
				 * 
				 * hotelMarkerList.get(i).click();
				 * 
				 * } catch (Exception e) { if (i == (hotelMarkerList.size() - 1)) {
				 * tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.FAIL, "Unable to click hotel marker"); } else {
				 * 
				 * continue;
				 * 
				 * }
				 * 
				 * } tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.PASS, "Pin Clicked #" + (i + 1));
				 * 
				 * if (verifyObjectDisplayed(orangeMarker) && verifyObjectDisplayed(activeCard))
				 * {
				 * 
				 * 
				 * tcConfig.updateTestReporter( "PackageResultPage", "mapFunctionalityCheck",
				 * Status.PASS, "Pin #" + getElementText(orangeMarker) + "and Hotel #" +
				 * getElementText(activeCard) + " both are highlighted ");
				 * 
				 * 
				 * tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.PASS, "Marker is Highlighted, and respective card is highlighted");
				 * 
				 * } else { tcConfig.updateTestReporter("PackageResultPage",
				 * "mapFunctionalityCheck", Status.FAIL, "Marker is not Highlighted");
				 * 
				 * }
				 * 
				 * }
				 * 
				 * } else { hotelMarkerList.get(0).click();
				 * tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.PASS, "Marker Clicked");
				 * 
				 * 
				 * if (getElementText(orangeMarker).trim().equals(
				 * getElementText(activeCard).trim())) {
				 * 
				 * tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.PASS, "Pin #" + getElementText(orangeMarker) + "and Hotel #" +
				 * getElementText(activeCard) + " both are highlighted " ); } else {
				 * tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.FAIL, "Highlighted Pin and hotel Number mismatch ");
				 * 
				 * }
				 * 
				 * 
				 * if (verifyObjectDisplayed(orangeMarker) && verifyObjectDisplayed(activeCard))
				 * {
				 * 
				 * tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck",
				 * Status.PASS, "Marker is Highlighted, and respective card is highlighted");
				 * 
				 * } else { tcConfig.updateTestReporter("PackageResultPage",
				 * "mapFunctionalityCheck", Status.FAIL, "Marker is not Highlighted");
				 * 
				 * }
				 * 
				 * }
				 * 
				 * } catch (Exception e) { tcConfig.updateTestReporter("PackageResultPage",
				 * "mapFunctionalityCheck", Status.FAIL, "no hotel Marker present in the map");
				 * }
				 */

			} else {
				tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck", Status.FAIL,
						"View List CTA not present");
			}
		} else {
			tcConfig.updateTestReporter("PackageResultPage", "mapFunctionalityCheck", Status.FAIL,
					"Map not present in list view");
		}

	}

	/*
	 * Method: clickWyndhamNotPreferd Description: Wyndham Not Preffred check: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void clickWyndhamNotPreferd() {
		try {
			List<WebElement> resultList = driver.findElements(resultCards);

			for (int i = 0; i < resultList.size(); i++) {
				if (resultList.get(i).getText().toUpperCase().contains("WYNDHAM PREFERRED")) {
					System.out.println("Wyndham Preferred");
					if (i == resultList.size() - 1) {
						tcConfig.updateTestReporter("PackageResultPage", "clickWyndhamNotPreferd", Status.PASS,
								"All hotels are wyndham preffered hence, cannot proceed");
						break;
					}

				} else {
					List<WebElement> resultHeaderList = driver.findElements(resultHeader);

					tcConfig.updateTestReporter("PackageResultPage", "clickWyndhamNotPreferd", Status.PASS,
							"Wyndham Not Preffered first header: " + resultHeaderList.get(i).getText());

					List<WebElement> resultSeeMoreInfoList = driver.findElements(seeMoreInfoButton);

					resultSeeMoreInfoList.get(i).click();

					waitUntilElementVisibleBy(driver, propertyDetail, 120);
					if (verifyObjectDisplayed(propertyDetail)) {
						tcConfig.updateTestReporter("PackageResultPage", "clickWyndhamNotPreferd", Status.PASS,
								"Package Details Window opened");

					} else {
						tcConfig.updateTestReporter("PackageResultPage", "clickWyndhamNotPreferd", Status.FAIL,
								"Error in getting property detail window");
					}

					break;
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("PackageResultPage", "clickWyndhamNotPreferd", Status.FAIL,
					"No Results Displayed");
		}

	}

}
