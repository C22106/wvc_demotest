package minivac.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PropertyDetailPage extends MinvacBasePage {

	public static final Logger log = Logger.getLogger(PropertyDetailPage.class);

	public PropertyDetailPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By propertyDetail = By.xpath("//h1[contains(.,'PROPERTY DETAILS')]");
	protected By headerInPropertyDetail = By.xpath(
			"//div[@class='wyn-hotel-details']//h2[@class='wyn-map-list__text-card-title wyn-js-hotel-details__title']");
	protected By selectButton = By
			.xpath("//div[@class='wyn-modal__content wyn-modal__content--html-content']//a[contains(.,'SELECT')]");
	protected By selectionHeader = By.xpath("//h1[contains(.,'Please confirm')]");
	protected By closeWindow = By.xpath("//span[@class='wyn-modal__close']");
	protected By propertyImage = By.xpath("//div[@class='wyn-hero__image wyn-object-fit-ie']");
	protected By nextButton = By.xpath("//button[@class='slick-arrow slick-next']");
	protected By prevButton = By.xpath("//button[@class='slick-arrow slick-prev']");
	protected By prefferedHotel = By.xpath(
			"//div[@class='wyn-hotel-details']//p[@class='wyn-map-list__text-card-preferred wyn-js-hotel-details__preferred-property']");
	protected By hotelAddress = By.xpath(
			"//div[@class='wyn-hotel-details']//p[@class='wyn-hotel-details__text-card-address wyn-js-hotel-details__address wyn-color-grey-04']");
	protected By hotelDescription = By.xpath(
			"//div[@class='wyn-hotel-details']//p[@class='wyn-hotel-details__content-description hotel-details-description wyn-js-hotel-details__description']");
	protected By featuredAmenities = By.xpath("//div[@class='wyn-hotel-details']//div[@class='wyn-type-body-2']");
	protected By resortAmenities = By.xpath(
			"//div[@class='wyn-hotel-details']//div[@class='columns is-multiline is-mobile is-tablet wyn-hotel-details__amenities-details wyn-js-hotel-details__resort-amenities']/div");
	protected By roomAmenities = By.xpath(
			"//div[@class='wyn-hotel-details']//div[@class='columns is-multiline is-mobile is-tablet wyn-hotel-details__amenities-details wyn-js-hotel-details__room-amenities']/div");
	protected By checkInPolicies = By.xpath(
			"//div[@class='wyn-hotel-details']//p[@class='wyn-type-body-2 wyn-js-hotel-details__checkin-description']");
	protected By checkOutPloicies = By.xpath(
			"//div[@class='wyn-hotel-details']//p[@class='wyn-type-body-2 wyn-js-hotel-details__checkout-description']");
	protected By passportPloicies = By.xpath("//div[@class='wyn-hotel-details']//p[contains(.,'passport')]");
	protected By paymentTypesImages = By
			.xpath("//div[@class='wyn-hotel-details']//h3[contains(.,'Payment Types')]//following-sibling::span/img");
	protected By detailsMap = By.xpath("//div[@class='wyn-hotel-details']//div[@class='wyn-map']");
	protected By zoomInMap = By.xpath("//div[@class='wyn-hotel-details']//button[@title='Zoom in']");
	protected By zoomOutMap = By.xpath("//div[@class='wyn-hotel-details']//button[@title='Zoom out']");
	protected By openMapCTA = By
			.xpath("//div[@class='wyn-hotel-details']//div[@data-title='BACK' and contains(.,'OPEN MAP')]");
	protected By hotelMarker = By.xpath("//div[@class='wyn-icon-minivac__marker wyn-icon-minivac__marker--orange']");
	protected By backCTA = By.xpath("//span[@class='wyn-hotel-details__back-map-link']");

	/*
	 * Method: propertyDetailCheck Description: Property Details Check and val: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void propertyDetailCheck() {
		waitUntilElementVisibleBy(driver, propertyDetail, 120);
		if (verifyObjectDisplayed(propertyDetail)) {
			tcConfig.updateTestReporter("PropertyDetailPage", "propertyDetailCheck", Status.PASS,
					"Property detail Window opened with header: " + getElementText(headerInPropertyDetail));

			if (verifyObjectDisplayed(selectButton)) {

				tcConfig.updateTestReporter("PropertyDetailPage", "propertyDetailCheck", Status.PASS,
						"Select button present");

				clickElementBy(selectButton);

				waitUntilElementVisibleBy(driver, selectionHeader, 120);
				if (verifyObjectDisplayed(selectionHeader)) {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertyDetailCheck", Status.PASS,
							"Navigated To booking page");
				} else {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertyDetailCheck", Status.FAIL,
							"Navigation To booking page failed");
				}

			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertyDetailCheck", Status.FAIL,
						"Error in getting the select button");
			}

		} else {
			tcConfig.updateTestReporter("PackageResultPage", "resultListCheck", Status.FAIL,
					"Error in getting the property detail");
		}

	}

	/*
	 * Method: propertDetailsValidations Description: Property details validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void propertDetailsValidations() {

		waitUntilElementVisibleBy(driver, propertyDetail, 120);

		if (verifyObjectDisplayed(propertyDetail)) {
			tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
					"Property detail Window opened with header: " + getElementText(headerInPropertyDetail));

			if (verifyObjectDisplayed(propertyImage)) {
				List<WebElement> imagesList = driver.findElements(propertyImage);
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						imagesList.size() + " Property Image Present");
				if (imagesList.size() > 1) {
					try {
						if (verifyObjectDisplayed(nextButton)) {
							clickElementBy(nextButton);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
									"2nd Property Image Displayed");
							if (verifyObjectDisplayed(prevButton)) {
								clickElementBy(prevButton);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations",
										Status.PASS, "1st Property Image Displayed");

							} else {
								tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations",
										Status.FAIL, "Error in clicking previous button");
							}

						} else {
							tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
									"Error in clicking Next button");
						}

					} catch (Exception e) {
						tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
								"Error in teh carousel functionality");
					}

				} else {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
							"The Only property Image is displayed");
				}
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"No Images for this property");
			}

			if (verifyObjectDisplayed(prefferedHotel)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Wyndham Preffered Label Displayed");
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Not A Wyndham Preffered Resort");
			}

			if (verifyObjectDisplayed(hotelAddress)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Hotel Address: " + getElementText(hotelAddress));
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Hotel Address Not Displayed");
			}

			if (verifyObjectDisplayed(hotelDescription)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Hotel Description Displayed");
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Hotel Description not displayed");
			}

			clickElementBy(headerInPropertyDetail);

			try {

				List<WebElement> featuredAmenitiesList = driver.findElements(featuredAmenities);

				scrollDownForElementJSWb(featuredAmenitiesList.get(0));
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Featured Amenities: " + featuredAmenitiesList.size());
				for (int i = 0; i < featuredAmenitiesList.size(); i++) {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
							(i + 1) + ". " + featuredAmenitiesList.get(i).getText());
				}
			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Featured Amenities displayed");
			}

			try {

				List<WebElement> resortAmenitiesList = driver.findElements(resortAmenities);

				scrollDownForElementJSWb(resortAmenitiesList.get(0));
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Resort Amenities: " + resortAmenitiesList.size());

			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Resort Amenities displayed");
			}

			try {

				List<WebElement> roomAmenitiesList = driver.findElements(roomAmenities);

				scrollDownForElementJSWb(roomAmenitiesList.get(0));
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Room Amenities: " + roomAmenitiesList.size());

			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Room Amenities displayed");
			}
			scrollDownForElementJSBy(checkInPolicies);

			if (verifyObjectDisplayed(checkInPolicies)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Check In Policies: " + getElementText(checkInPolicies));
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Checki In Policies not dsiplayed");
			}

			if (verifyObjectDisplayed(checkOutPloicies)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Check Out Policies: " + getElementText(checkOutPloicies));
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Check Out Policies not dsiplayed");
			}

			if (verifyObjectDisplayed(passportPloicies)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Passport Policies is displayed");
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Passport Policies not dsiplayed");
			}

			try {

				List<WebElement> paymentTypes = driver.findElements(paymentTypesImages);

				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Payment Types: " + paymentTypes.size());

			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Resort Amenities displayed");
			}

			driver.switchTo().activeElement().sendKeys(Keys.END);

			if (verifyObjectDisplayed(detailsMap)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Map displayed in Property Details page");
				/*
				 * try { if (verifyObjectDisplayed(zoomInMap)) { clickElementBy(zoomInMap);
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS, "Zoomed In Map"); } else {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL, "Zoomed In Map failed"); }
				 * 
				 * if (verifyObjectDisplayed(zoomOutMap)) { clickElementBy(zoomOutMap);
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS, "Zoomed Out Map"); } else {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL, "Zoomed Out Map failed"); } } catch
				 * (Exception e) { tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL, "Error in Zooming In and Out"); }
				 */

				if (browserName.equalsIgnoreCase("EDGE")) {
					Actions action = new Actions(driver);
					action.sendKeys(Keys.PAGE_UP).build().perform();
				}
				if (verifyObjectDisplayed(openMapCTA)) {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
							"Open Map CTA present");
					clickElementJSWithWait(openMapCTA);
					waitUntilElementVisibleBy(driver, backCTA, 120);
					if (verifyObjectDisplayed(backCTA) && verifyObjectDisplayed(hotelMarker)) {
						tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
								"Bigger View of Map opened and Marker is present");

						clickElementBy(backCTA);
						waitUntilElementVisibleBy(driver, openMapCTA, 120);
						if (verifyObjectDisplayed(openMapCTA)) {
							tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
									"Navigated Back from Map View");
						} else {
							tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
									"Error in navigating back");
						}

					} else {
						tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
								"Error in opening bigger view of MAP");
					}
				} else {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
							"Open MAP CTA not present");
				}

			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Map not displayed in Property Details page");
			}

			if (verifyObjectDisplayed(closeWindow)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Close Property Details option present");
				clickElementBy(closeWindow);

				if (!verifyObjectDisplayed(selectButton)) {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
							"Property Detail Page Closed");
				} else {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
							"Error In Closing Property Details Page");
				}
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Error in navigating back");
			}

		} else {
			tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
					"Property Detail Window opening error");
		}

	}

	/*
	 * Method: checkWynNotPrefPD Description: Check Wyn not prefferd: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void checkWynNotPrefPD() {

		if (!verifyObjectDisplayed(prefferedHotel)) {
			tcConfig.updateTestReporter("PropertyDetailPage", "checkWynNotPrefPD", Status.PASS,
					"Wyndham Prefferd label not present");
		} else {
			tcConfig.updateTestReporter("PropertyDetailPage", "checkWynNotPrefPD", Status.FAIL,
					"Wyndham Preffered label present");
		}

		if (verifyObjectDisplayed(selectButton)) {

			tcConfig.updateTestReporter("PropertyDetailPage", "checkWynNotPrefPD", Status.PASS,
					"Select button present");

			clickElementBy(selectButton);

			waitUntilElementVisibleBy(driver, selectionHeader, 120);
			if (verifyObjectDisplayed(selectionHeader)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "checkWynNotPrefPD", Status.PASS,
						"Navigated To booking page");
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "checkWynNotPrefPD", Status.FAIL,
						"Navigation To booking page failed");
			}

		} else {
			tcConfig.updateTestReporter("PropertyDetailPage", "checkWynNotPrefPD", Status.FAIL,
					"Error in getting the select button");
		}

	}

}
