package minivac.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class RedeemPackagePage_IOS extends RedeemPackagePage {

	public static final Logger log = Logger.getLogger(RedeemPackagePage_IOS.class);

	public RedeemPackagePage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		guestDrpDwn = By.xpath("//div[@class='wyn-search-bar__block-accordion--guest']//button");
	}

	/*
	 * Method: guestPanelCheck Description: Guest Panel Check: Date Mar/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void guestPanelCheck() {
		waitUntilElementVisibleBy(driver, guestDrpDwn, 120);
		if (verifyObjectDisplayed(guestDrpDwn)) {
			tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
					"Guest Drop Down Present on the page");

			clickElementBy(guestDrpDwn);
			waitUntilObjectVisible(driver, guestPanel, 120);
			if (verifyObjectDisplayed(guestPanel)) {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS, "Guest Panel Opened");

				if (verifyObjectDisplayed(minusButtonChild)) {
					for (int i = 0; i < 2; i++) {
						clickElementBy(minusButtonChild);
						if (i == 1) {
							String strStyle = driver.findElement(minusButtonChild).getAttribute("style");

							if (strStyle.contains("not-allowed") && getElementText(childNumber).contains("0")) {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
										"Child Decreased to: " + getElementText(childNumber));
							} else {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
										"Error in decreasing child numbers");
							}

						} else {
							System.out.println(" ");
						}

					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
							"Child Minus Button not present");
				}

				if (verifyObjectDisplayed(minusBtnAdult)) {
					for (int i = 0; i < 2; i++) {
						clickElementBy(minusBtnAdult);
						if (i == 1) {
							String strStyle = driver.findElement(minusBtnAdult).getAttribute("style");

							if (strStyle.contains("not-allowed") && getElementText(adultNumber).contains("1")) {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
										"Adult Decreased to: " + getElementText(adultNumber)
												+ " and that's minimum number of adult required");
							} else {
								tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
										"Error in decreasing adult numbers");
							}

						} else {
							System.out.println(" ");
						}

					}

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
							"Adult Minus Button not present");
				}

				if (verifyObjectDisplayed(guestDrpDwn)) {
					clickElementBy(guestDrpDwn);
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.PASS,
							"Guest Panel closed");

				} else {
					tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
							"Guest Panel close button not present");
				}

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
						"Guest Panel opening error");
			}

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelCheck", Status.FAIL,
					"Guest Drop Down not Present on the page");
		}

	}
}