package minivac.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class MinvacHomepage_Android extends MinvacHomepage {

	public static final Logger log = Logger.getLogger(MinvacHomepage_Android.class);

	public MinvacHomepage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void footerValidations() {
		footerCopyRight();
		footertermsOfuseLink();
		footerPrivacyNoteLink();
		footerPrivacySettingLink();
	}

	/*
	 * Method: footertermsOfuseLink Description: Terms of use validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void footertermsOfuseLink() {

		if (verifyObjectDisplayed(termsOfuse)) {
			tcConfig.updateTestReporter("MinvacHomepage", "footertermsOfuseLink", Status.PASS,
					"Careers Login page navigated");
			String strURL = driver.getCurrentUrl();
			clickElementBy(termsOfuse);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String termsOfusepageURL = driver.getCurrentUrl();
			String URL = testData.get("urlTermOfUsePage");
			if (termsOfusepageURL.contains(URL)) {

				tcConfig.updateTestReporter("MinvacHomepage", "footertermsOfuseLink", Status.PASS,
						"terms Of use page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "footertermsOfuseLink", Status.FAIL,
						"terms Of use page Navigation Unsuccessful");
			}

			driver.get(strURL);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	public void footerPrivacyNoteLink() {

		if (verifyObjectDisplayed(privacyNotice)) {
			tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacyNoteLink", Status.PASS,
					"Navigating To privacy Notice page");
			String strUrl = driver.getCurrentUrl();
			clickElementBy(privacyNotice);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String privacyNotURL = driver.getCurrentUrl();
			String URL = testData.get("urlPrivacyNoticePage");
			if (privacyNotURL.contains(URL)) {

				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacyNoteLink", Status.PASS,
						"privacy Notice page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacyNoteLink", Status.FAIL,
						"privacy Notice page Navigation Unsuccessful");
			}
			driver.get(strUrl);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	public void footerPrivacySettingLink() {
		if (verifyObjectDisplayed(privacySetting)) {
			tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacySettingLink", Status.PASS,
					"Navigating To privacy Setting page");
			String strUrl = driver.getCurrentUrl();
			clickElementBy(privacySetting);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, privacySettingpage, 120);
			if (verifyObjectDisplayed(privacySettingpage)) {
				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacySettingLink", Status.PASS,
						"privacy Setting page Navigation Successful");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacySettingLink", Status.FAIL,
						"privacy Setting page Navigation Unsuccessful");
			}
			driver.get(strUrl);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	/*
	 * Method: incompleteErrorVal Description: Incomplete package info Validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void incompleteErrorVal() {

		if (verifyObjectDisplayed(confirmationTxt)) {

			tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
					"Confirmation Number Field Present");

			clickElementBy(confirmationTxt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(lastnameTxt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(ziptxt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.switchTo().activeElement().sendKeys(Keys.TAB);

			if (verifyObjectDisplayed(confirmationNoError)) {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
						"Incompleter Confirmation Number Error Displayed");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
						"Incompleter Confirmation Number Error not Displayed");
			}

			if (verifyObjectDisplayed(lastNameError)) {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
						"Incompleter Last Name Error Displayed");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
						"Incompleter Last Name  Error not Displayed");
			}

			driver.navigate().refresh();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(ziptxt);
			driver.findElement(ziptxt).sendKeys(testData.get("incorrectZip"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(zipCodeError)) {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
						"Incompleter zip code Error Displayed");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
						"Incompleter zip code Error not Displayed");
			}

			driver.navigate().refresh();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
					"Confirmation Number Field not Present");
		}

	}

	/*
	 * Method: redeemedPkgValidations Description: Redeemed Package Validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void redeemedPkgValidations() {

		if (verifyObjectDisplayed(confirmationTxt)) {

			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
					"Confirmation Number Field Present");

			String ConfNo = testData.get("strConfirmationNo");

			fieldDataEnter(confirmationTxt, ConfNo);
			fieldDataEnter(lastnameTxt, testData.get("strLastName"));
			fieldDataEnter(ziptxt, testData.get("strZipCode"));

			driver.switchTo().activeElement().sendKeys(Keys.TAB);

			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
					"All the data entered successfully");

			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
						"Submit Button was disabled");

			} else if (verifyObjectDisplayed(submitBtn)) {
				clickElementJSWithWait(submitBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, redeemedPckgError, 120);
				if (verifyObjectDisplayed(redeemPackageButton)) {
					tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
							"Navigated to Package Information page");
				} else {
					tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
							"Redeemed Package Pop Up not displayed");
				}

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
						"Error in clicking submit button");
			}

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
					"Confirmation Number Field not Present");
		}

	}

}