package minivac.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class MinvacHomepage extends MinvacBasePage {

	public static final Logger log = Logger.getLogger(MinvacHomepage.class);

	public MinvacHomepage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By minvacLogo = By.xpath("//div[@class='wyn-header-minivac__logo-container']");
	protected By termsOfuse = By.xpath("//div[@class='wyn-footer__legal-links']//a[contains(.,'Terms of Use')]");
	// protected By termsOfusepage = By.xpath("//div[@id='main-content' and
	// contains(.,'Terms of Use')]");
	protected By privacyNotice = By.xpath("//div[@class='wyn-footer__legal-links']//a[contains(.,'Privacy Notice')]");
	protected By privacySetting = By
			.xpath("//div[@class='wyn-footer__legal-links']//a[contains(.,'Privacy Settings')]");
	protected By privacySettingpage = By.xpath("//h1[contains(.,'Policies')]");
	protected By copyRight = By.xpath("//div[@class='footer']//p[contains(.,'Rights')]");
	protected By CLUBWYNDHAMlogo = By.xpath("//img[@alt='CLUB WYNDHAM']");
	protected By worldMarkLogo = By.xpath("//img[@alt='WorldMark by Wyndham']");
	protected By confirmationTxt = By.xpath("//input[@id='confirmation']");
	protected By lastnameTxt = By.xpath("//input[@id='last_name']");
	protected By ziptxt = By.xpath("//input[@id='zip_code']");
	protected By submitBtn = By.xpath("//button[contains(.,'Submit')]");
	protected By disabledSubmitButton = By.xpath("//button[contains(.,'Submit') and @disabled]");

	protected By redeemPackageText = By.xpath("//h2[contains(.,'REDEEM')]");
	protected By redeemPackageButton = By.xpath("//div[@class='column wyn-search-bar__block-button ']/button");

	protected By wrongInfoError = By.xpath("//p[@class='medium form__error wyn-lead-form__error-message']/p");
	protected By redeemedPckgError = By.xpath("//h1[contains(.,'Assistance Required')]");
	protected By closeRedeemPopUp = By.xpath("//span[@class='wyn-modal__close']");

	protected By confirmationNoError = By.xpath("//p[@id='confirmation']");
	protected By lastNameError = By.xpath("//p[@id='last_name']");
	protected By zipCodeError = By.xpath("//p[@id='zip_code']");
	protected By ieSupportBanner = By.xpath("//div[@class='wyn-ie-alert-message__panel']");
	protected By closeieSupport = By.xpath("//a[@id='alertClose']");

	protected By nobelChatIcon = By.xpath("//div[@id='wynChatContainer']/div[@id='myBtn']/img");
	protected By welcomeMessage = By.xpath("//div[@id='chat-panel']//label[contains(text(),'How can we help you:')]");
	protected By selectOptions = By.xpath("//div[contains(@class,'select-styled')]");
	protected By selectValues = By.xpath("//div[contains(@class,'select')]//ul/li");
	protected By offHourText = By.xpath("//span[@id='myPopup-offhours']/div[@class='off-hours-text']");
	protected By offHourPopUpClose = By.xpath("//span[@id='myPopup-offhours']//span[@class='close']");

	/*
	 * Method: launchAppMinvac Description: Launch the Minivac application: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void launchAppMinvac() {
		driver.navigate().to(testData.get("URL"));
		// driver.manage().window().maximize();

		waitUntilObjectVisible(driver, redeemPackageText, 120);

		if (verifyObjectDisplayed(redeemPackageText)) {

			tcConfig.updateTestReporter("MinvacHomepage", "launchAppMinvac", Status.PASS,
					"Minvac application launched Successful");

			if (verifyObjectDisplayed(ieSupportBanner)) {
				tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.FAIL,
						"IE Support Banner Present");

				clickElementBy(closeieSupport);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
						"IE Support Banner not Present");
			}
		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "launchAppMinvac", Status.FAIL,
					"Unable to launch Minvac application");
		}

	}

	public void validateNobelChatIcon() {
		waitUntilElementVisibleBy(driver, nobelChatIcon, 120);
		if (verifyObjectDisplayed(nobelChatIcon)) {
			tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatIcon", Status.PASS,
					"Nobel Chat Icon is present");
		} else {
			tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatIcon", Status.FAIL,
					"Nobel Chat Icon is not present");
		}
	}

	public void clickNobelChatIcon() {
		clickElementJSWithWait(nobelChatIcon);
		tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatIcon", Status.PASS,
				"Able to Click Nobel Chat Icon");
	}

	public void validateNobelChatWindowPopUp(String OffHours) {
		if (OffHours.equals("N")) {
			String mainWindow = driver.getWindowHandle();
			List<String> allWindowList = new ArrayList<>(driver.getWindowHandles());
			if (allWindowList.size() == 2) {
				driver.switchTo().window(allWindowList.get(1));
				String windowTitle = driver.getTitle();
				System.out.println(windowTitle);
				if (windowTitle.toUpperCase().contains("LIVE CHAT")) {
					tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatWindowPopUp", Status.PASS,
							"Chat window is getting opened");
					validateNobelChatform();
				} else {
					tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatWindowPopUp", Status.FAIL,
							"Chat window is not opened");
				}
				driver.close();
				driver.switchTo().window(mainWindow);

			} else {
				tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatWindowPopUp", Status.FAIL,
						"Chat window is not opened");
			}

		} else {
			waitUntilElementVisibleBy(driver, offHourText, 120);
			System.out.println(getElementText(offHourText));
			System.out.println(testData.get("OffHourText"));
			if (verifyElementDisplayed(driver.findElement(offHourText))
					&& getElementText(offHourText).trim().equals(testData.get("OffHourText").trim())) {
				tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatWindowPopUp", Status.PASS,
						"Off Hour Message is getting Displayed " + getElementText(offHourText));
			} else {
				tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatWindowPopUp", Status.FAIL,
						"Off Hour Message is not Displayed ");
			}
			clickElementJSWithWait(offHourPopUpClose);
		}

	}

	public void validateNobelChatform() {
		String strDropDownValue = "";
		waitUntilElementVisibleBy(driver, welcomeMessage, 120);
		if (verifyElementDisplayed(driver.findElement(welcomeMessage))) {
			tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatform", Status.PASS,
					"Welcome Message is getting Displayed");
			clickElementJSWithWait(selectOptions);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> drpDownList = driver.findElements(selectValues);
			for (int i = 0; i < drpDownList.size(); i++) {
				strDropDownValue = drpDownList.get(i).getText();
				if (strDropDownValue.equals(testData.get("SelectDropDownValue" + (i + 1) + ""))) {
					tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatform", Status.PASS,
							"Drop Down value is displayed as " + strDropDownValue);
				} else {
					tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatform", Status.FAIL,
							"Drop Down value is not displayed");
				}
			}

		} else {
			tcConfig.updateTestReporter("MinivacLoginPage", "validateNobelChatform", Status.FAIL,
					"Welcome Message is not getting Displayed");
		}
	}

	/*
	 * Method: retrivePackageInfo Description: Package Retrieval Validation: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void retrivePackageInfo() {

		if (verifyObjectDisplayed(confirmationTxt)) {

			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
					"Confirmation Number Field Present");

			String ConfNo = testData.get("strConfirmationNo");

			fieldDataEnter(confirmationTxt, ConfNo);
			fieldDataEnter(lastnameTxt, testData.get("strLastName"));
			fieldDataEnter(ziptxt, testData.get("strZipCode"));

			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
					"All the data entered successfully");

			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
						"Submit Button was disabled");

			} else if (verifyObjectDisplayed(submitBtn)) {
				clickElementBy(submitBtn);
				waitUntilElementVisibleBy(driver, redeemPackageButton, 120);

				if (verifyObjectDisplayed(redeemPackageButton)) {
					tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
							"Navigated to Package Information page");
				} else {
					tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
							"Package Information navigation failed");
				}

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
						"Error in clicking submit button");
			}

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
					"Confirmation Number Field not Present");
		}

	}

	/*
	 * Method: footerValidations Description: Footer Validatios: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void footerValidations() {
		footerCopyRight();
		footertermsOfuseLink();
		footerPrivacyNoteLink();
		footerPrivacySettingLink();
	}

	/*
	 * Method: footertermsOfuseLink Description: Terms of use validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void footertermsOfuseLink() {

		if (verifyObjectDisplayed(termsOfuse)) {
			tcConfig.updateTestReporter("MinvacHomepage", "footertermsOfuseLink", Status.PASS,
					"Careers Login page navigated");

			clickElementBy(termsOfuse);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String termsOfusepageURL = driver.getCurrentUrl();
			String URL = testData.get("urlTermOfUsePage");
			if (termsOfusepageURL.contains(URL)) {

				tcConfig.updateTestReporter("MinvacHomepage", "footertermsOfuseLink", Status.PASS,
						"terms Of use page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "footertermsOfuseLink", Status.FAIL,
						"terms Of use page Navigation Unsuccessful");
			}

			driver.close();
			driver.switchTo().window(tabs.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	/*
	 * Method: footerPrivacyNoteLink Description: Privacy Notice Validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void footerPrivacyNoteLink() {

		if (verifyObjectDisplayed(privacyNotice)) {
			tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacyNoteLink", Status.PASS,
					"Navigating To privacy Notice page");
			// String strUrl=driver.getCurrentUrl();
			clickElementBy(privacyNotice);
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String privacyNotURL = driver.getCurrentUrl();
			String URL = testData.get("urlPrivacyNoticePage");
			if (privacyNotURL.contains(URL)) {

				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacyNoteLink", Status.PASS,
						"privacy Notice page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacyNoteLink", Status.FAIL,
						"privacy Notice page Navigation Unsuccessful");
			}
			driver.close();
			driver.switchTo().window(tabs.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	/*
	 * Method: footerPrivacySettingLink Description: Privacy Setings validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void footerPrivacySettingLink() {

		if (verifyObjectDisplayed(privacySetting)) {
			tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacySettingLink", Status.PASS,
					"Navigating To privacy Setting page");

			clickElementBy(privacySetting);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			waitUntilObjectVisible(driver, privacySettingpage, 120);
			if (verifyObjectDisplayed(privacySettingpage)) {

				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacySettingLink", Status.PASS,
						"privacy Setting page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "footerPrivacySettingLink", Status.FAIL,
						"privacy Setting page Navigation Unsuccessful");
			}

			driver.close();
			driver.switchTo().window(tabs.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	/*
	 * Method: footerCopyRight Description: Copyright Validations: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void footerCopyRight() {

		if (verifyObjectDisplayed(copyRight)) {
			String copyright = driver.findElement(copyRight).getText();
			tcConfig.updateTestReporter("MinvacHomepage", "footerCopyRight", Status.PASS,
					"Navigated to Minvac login page and copyright text is: " + copyright);

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "footerCopyRight", Status.FAIL,
					"Copyright Text not available");
		}
	}

	/*
	 * Method: headerLogos Description: Header Logos validations: Date Mar/2019
	 * Author: Unnat Jain Changes By
	 */
	public void headerLogos() {

		if (verifyObjectDisplayed(CLUBWYNDHAMlogo)) {
			tcConfig.updateTestReporter("MinvacHomepage", "headerLogos", Status.PASS,
					"CLUB WYNDHAM brand logo is present on landing page");

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "headerLogos", Status.FAIL,
					"Navigated to Minvac login page and CLUBWYNDHAMlogo is not present");
		}

		if (verifyObjectDisplayed(worldMarkLogo)) {
			tcConfig.updateTestReporter("MinvacHomepage", "headerLogos", Status.PASS,
					"Wyndham World Mark is present on landing page");

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "headerLogos", Status.FAIL,
					"Navigated to Minvac login page and WMWlogo is not present");
		}
	}

	/*
	 * Method: wrongPackageInfo Description: Wrong Package Info validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void wrongPackageInfo() {

		if (verifyObjectDisplayed(confirmationTxt)) {

			tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.PASS,
					"Confirmation Number Field Present");

			String ConfNo = testData.get("strConfirmationNo");

			fieldDataEnter(confirmationTxt, ConfNo);
			fieldDataEnter(lastnameTxt, testData.get("strLastName"));
			fieldDataEnter(ziptxt, testData.get("strZipCode"));

			driver.switchTo().activeElement().sendKeys(Keys.TAB);

			tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.PASS,
					"All the data entered successfully");

			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.FAIL,
						"Submit Button is disabled");

			} else if (verifyObjectDisplayed(submitBtn)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(submitBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (verifyObjectDisplayed(redeemPackageButton)) {
					tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.PASS,
							"Navigated to Package Information page");
				} else if (verifyObjectDisplayed(wrongInfoError)) {
					tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.PASS,
							"Package that you entered is invalid, Errror: " + getElementText(wrongInfoError));
				} else {
					tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.FAIL,
							"Package Retrieval Error, No error msg displayed");
				}

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.FAIL,
						"Error in clicking submit button");
			}

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "wrongPackageInfo", Status.FAIL,
					"Confirmation Number Field not Present");
		}

	}

	/*
	 * Method: submitButtonValidations Description: Submit Button Validations: Date
	 * Mar/2019 Author: Unnat Jain Changes By
	 */
	public void submitButtonValidations() {

		if (verifyObjectDisplayed(confirmationTxt)) {
			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.PASS,
						"Submit Button was disabled, No data entered");

			} else if (verifyObjectDisplayed(submitBtn)) {

				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.FAIL,
						"Submit Button is enabled even when no data entered");
			}
			tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.PASS,
					"Confirmation Number Field Present");

			String ConfNo = testData.get("strConfirmationNo");

			fieldDataEnter(confirmationTxt, ConfNo);

			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.PASS,
						"Submit Button was disabled when only Confirmation Number entered");

			} else if (verifyObjectDisplayed(submitBtn)) {

				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.FAIL,
						"Submit Button is enabled");
			}

			fieldDataEnter(lastnameTxt, testData.get("strLastName"));

			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.PASS,
						"Submit Button was disabled when two data entered");

			} else if (verifyObjectDisplayed(submitBtn)) {

				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.FAIL,
						"Submit Button is enabled");
			}

			fieldDataEnter(ziptxt, testData.get("strZipCode"));

			tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.PASS,
					"All the data entered successfully");

			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.FAIL,
						"Submit Button was disabled");

			} else if (verifyObjectDisplayed(submitBtn)) {
				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.PASS,
						"Submit Button is enabled");

				clickElementBy(submitBtn);
				waitUntilElementVisibleBy(driver, redeemPackageButton, 120);

				if (verifyObjectDisplayed(redeemPackageButton)) {
					tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.PASS,
							"Navigated to Package Information page");
				} else {
					tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.FAIL,
							"Package Information navigation failed");
				}

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.FAIL,
						"Error in clicking submit button");
			}

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "submitButtonValidations", Status.FAIL,
					"Confirmation Number Field not Present");
		}

	}

	/*
	 * Method: redeemedPkgValidations Description: Redeemed Package Validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void redeemedPkgValidations() {

		if (verifyObjectDisplayed(confirmationTxt)) {

			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
					"Confirmation Number Field Present");

			String ConfNo = testData.get("strConfirmationNo");

			fieldDataEnter(confirmationTxt, ConfNo);
			fieldDataEnter(lastnameTxt, testData.get("strLastName"));
			fieldDataEnter(ziptxt, testData.get("strZipCode"));

			driver.switchTo().activeElement().sendKeys(Keys.TAB);

			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
					"All the data entered successfully");

			if (verifyObjectDisplayed(disabledSubmitButton)) {
				tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
						"Submit Button was disabled");

			} else if (verifyObjectDisplayed(submitBtn)) {
				clickElementJSWithWait(submitBtn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.navigate().back();
				waitUntilElementVisibleBy(driver, redeemedPckgError, 120);

				if (verifyObjectDisplayed(redeemPackageButton)) {
					tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
							"Navigated to Package Information page, Not a redeemed Package");
				} else if (verifyObjectDisplayed(redeemedPckgError)) {
					tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.PASS,
							"Already redeemed package, pop Up Displayed");

					if (verifyObjectDisplayed(closeRedeemPopUp)) {
						clickElementBy(closeRedeemPopUp);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
								"Pop Up Close button not present");
					}

				} else {
					tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
							"Redeemed Package Pop Up not displayed");
				}

			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
						"Error in clicking submit button");
			}

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "retrivePackageInfo", Status.FAIL,
					"Confirmation Number Field not Present");
		}

	}

	/*
	 * Method: incompleteErrorVal Description: Incomplete package info Validations:
	 * Date Mar/2019 Author: Unnat Jain Changes By
	 */
	public void incompleteErrorVal() {

		if (verifyObjectDisplayed(confirmationTxt)) {

			tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
					"Confirmation Number Field Present");

			clickElementBy(confirmationTxt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(lastnameTxt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(ziptxt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.switchTo().activeElement().sendKeys(Keys.TAB);

			if (verifyObjectDisplayed(confirmationNoError)) {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
						"Incompleter Confirmation Number Error Displayed");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
						"Incompleter Confirmation Number Error not Displayed");
			}

			if (verifyObjectDisplayed(lastNameError)) {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
						"Incompleter Last Name Error Displayed");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
						"Incompleter Last Name  Error not Displayed");
			}

			if (verifyObjectDisplayed(zipCodeError)) {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
						"Incompleter zip code Error Displayed");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
						"Incompleter zip code Error not Displayed");
			}

			driver.navigate().refresh();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(ziptxt);
			driver.findElement(ziptxt).sendKeys(testData.get("incorrectZip"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(zipCodeError)) {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.PASS,
						"Incompleter zip code Error Displayed");
			} else {
				tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
						"Incompleter zip code Error not Displayed");
			}

			driver.navigate().refresh();
			waitForSometime(tcConfig.getConfig().get("zipCodeError"));

		} else {
			tcConfig.updateTestReporter("MinvacHomepage", "incompleteErrorVal", Status.FAIL,
					"Confirmation Number Field not Present");
		}

	}

}
