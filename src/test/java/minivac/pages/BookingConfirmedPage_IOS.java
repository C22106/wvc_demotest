package minivac.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class BookingConfirmedPage_IOS extends BookingConfirmedPage {

	public static final Logger log = Logger.getLogger(BookingConfirmedPage_IOS.class);

	public BookingConfirmedPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
}