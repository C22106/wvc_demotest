package minivac.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PropertyDetailPage_IOS extends PropertyDetailPage {

	public static final Logger log = Logger.getLogger(PropertyDetailPage_IOS.class);

	public PropertyDetailPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		nextButton = By.xpath("//button[@id='slick-slide-control01']");
		prevButton = By.xpath("//button[@id='slick-slide-control00']");
	}

	public void propertDetailsValidations() {

		waitUntilElementVisibleBy(driver, propertyDetail, 120);

		if (verifyObjectDisplayed(propertyDetail)) {
			tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
					"Property detail Window opened with header: " + getElementText(headerInPropertyDetail));

			if (verifyObjectDisplayed(propertyImage)) {
				List<WebElement> imagesList = driver.findElements(propertyImage);
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						imagesList.size() + " Property Image Present");
				if (imagesList.size() > 1) {
					try {
						if (verifyObjectDisplayed(nextButton)) {
							clickElementBy(nextButton);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
									"2nd Property Image Displayed");
							if (verifyObjectDisplayed(prevButton)) {
								clickElementBy(prevButton);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations",
										Status.PASS, "1st Property Image Displayed");

							} else {
								tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations",
										Status.FAIL, "Error in clicking previous button");
							}

						} else {
							tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
									"Error in clicking Next button");
						}

					} catch (Exception e) {
						tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
								"Error in teh carousel functionality");
					}

				} else {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
							"The Only property Image is displayed");
				}
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"No Images for this property");
			}

			if (verifyObjectDisplayed(prefferedHotel)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Wyndham Preffered Label Displayed");
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Not A Wyndham Preffered Resort");
			}

			if (verifyObjectDisplayed(hotelAddress)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Hotel Address: " + getElementText(hotelAddress));
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Hotel Address Not Displayed");
			}

			if (verifyObjectDisplayed(hotelDescription)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Hotel Description Displayed");
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Hotel Description not displayed");
			}

			clickElementBy(headerInPropertyDetail);

			try {

				List<WebElement> featuredAmenitiesList = driver.findElements(featuredAmenities);

				scrollDownForElementJSWb(featuredAmenitiesList.get(0));
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Featured Amenities: " + featuredAmenitiesList.size());
				for (int i = 0; i < featuredAmenitiesList.size(); i++) {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
							(i + 1) + ". " + featuredAmenitiesList.get(i).getText());
				}
			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Featured Amenities displayed");
			}

			try {

				List<WebElement> resortAmenitiesList = driver.findElements(resortAmenities);

				scrollDownForElementJSWb(resortAmenitiesList.get(0));
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Resort Amenities: " + resortAmenitiesList.size());

			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Resort Amenities displayed");
			}

			try {

				List<WebElement> roomAmenitiesList = driver.findElements(roomAmenities);

				scrollDownForElementJSWb(roomAmenitiesList.get(0));
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Room Amenities: " + roomAmenitiesList.size());

			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Room Amenities displayed");
			}
			scrollDownForElementJSBy(checkInPolicies);

			if (verifyObjectDisplayed(checkInPolicies)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Check In Policies: " + getElementText(checkInPolicies));
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Checki In Policies not dsiplayed");
			}

			if (verifyObjectDisplayed(checkOutPloicies)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Check Out Policies: " + getElementText(checkOutPloicies));
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Check Out Policies not dsiplayed");
			}

			if (verifyObjectDisplayed(passportPloicies)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Passport Policies is displayed");
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Passport Policies not dsiplayed");
			}

			try {

				List<WebElement> paymentTypes = driver.findElements(paymentTypesImages);

				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Total Payment Types: " + paymentTypes.size());

			} catch (Exception e) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"No Resort Amenities displayed");
			}

			driver.switchTo().activeElement().sendKeys(Keys.END);

			if (verifyObjectDisplayed(detailsMap)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Map displayed in Property Details page");
				/*
				 * try { if (verifyObjectDisplayed(zoomInMap)) { clickElementBy(zoomInMap);
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS, "Zoomed In Map"); } else {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS, "Zoomed In Map failed"); }
				 * 
				 * if (verifyObjectDisplayed(zoomOutMap)) { clickElementBy(zoomOutMap);
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS, "Zoomed Out Map"); } else {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL, "Zoomed Out Map failed"); } } catch
				 * (Exception e) { tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL, "Error in Zooming In and Out"); }
				 */

				if (browserName.equalsIgnoreCase("EDGE")) {
					Actions action = new Actions(driver);
					action.sendKeys(Keys.PAGE_UP).build().perform();
				}
				/*
				 * if (verifyObjectDisplayed(openMapCTA)) {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS, "Open Map CTA present");
				 * clickElementJSWithWait(openMapCTA); waitUntilElementVisibleBy(driver,
				 * backCTA, 120); if (verifyObjectDisplayed(backCTA) &&
				 * verifyObjectDisplayed(hotelMarker)) {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS,
				 * "Bigger View of Map opened and Marker is present");
				 * 
				 * clickElementBy(backCTA); waitUntilElementVisibleBy(driver, openMapCTA, 120);
				 * if (verifyObjectDisplayed(openMapCTA)) {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.PASS, "Navigated Back from Map View"); }
				 * else { tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL, "Error in navigating back"); }
				 * 
				 * } else { tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL,
				 * "Error in opening bigger view of MAP"); } } else {
				 * tcConfig.updateTestReporter("PropertyDetailPage",
				 * "propertDetailsValidations", Status.FAIL, "Open MAP CTA not present"); }
				 */

			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Map not displayed in Property Details page");
			}

			if (verifyObjectDisplayed(closeWindow)) {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
						"Close Property Details option present");
				clickElementBy(closeWindow);

				if (!verifyObjectDisplayed(selectButton)) {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.PASS,
							"Property Detail Page Closed");
				} else {
					tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
							"Error In Closing Property Details Page");
				}
			} else {
				tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
						"Error in navigating back");
			}

		} else {
			tcConfig.updateTestReporter("PropertyDetailPage", "propertDetailsValidations", Status.FAIL,
					"Property Detail Window opening error");
		}

	}
}