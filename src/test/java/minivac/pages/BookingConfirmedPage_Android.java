package minivac.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class BookingConfirmedPage_Android extends BookingConfirmedPage {

	public static final Logger log = Logger.getLogger(BookingConfirmedPage_Android.class);

	public BookingConfirmedPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
}