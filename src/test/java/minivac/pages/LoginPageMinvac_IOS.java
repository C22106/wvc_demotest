package minivac.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class LoginPageMinvac_IOS extends LoginPageMinvac {

	public static final Logger log = Logger.getLogger(LoginPageMinvac_IOS.class);

	public LoginPageMinvac_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
}