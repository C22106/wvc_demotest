package atco.pages;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class ATCOBasePage extends FunctionalComponents {

	public ATCOBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	protected By loadingSpinner = By.xpath("//div[@class='loader__tick']");

	/*
	 * *************************Method: checkLoadingSpinner ***
	 * *************Description: Salepoint Loading Spinner**
	 */
	public void checkLoadingSpinner() {
		waitUntilElementInVisible(driver, loadingSpinner, 60);
	}

	public String getRandomStringText(int text) {
		String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < text) { // length of the random string.
			int index = (int) (rnd.nextFloat() * str.length());
			salt.append(str.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
		// System.out.println(saltStr);
		// testData.put("", "");
	}

	public void waitUntilElementVisibleWb(WebDriver driver, WebElement locator, long timeOutInSeconds) {
		for (long i = 0; i < timeOutInSeconds / 2; i++) {

			if (locator.isDisplayed()) {

				waitForSometime("2");
				break;
			} else {

				System.out.println(locator + " Element not visiable");
			}

		}

	}

	public static String adjustDatePattern(String DT) {
		String actaulDate = null;
		String mm = null, dd = null;

		try {

			if (DT.split("/")[0].length() == 1) {
				mm = "0" + DT.split("/")[0];
			} else {
				mm = DT.split("/")[0];
			}
			if (DT.split("/")[1].length() == 1) {
				dd = "0" + DT.split("/")[1];
			} else {
				dd = DT.split("/")[1];
			}
			actaulDate = mm + "/" + dd + "/" + DT.split("/")[2];
		} catch (Exception e) {

		}
		return actaulDate;
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change By
	 * : None
	 */

	public WebDriver checkAndInitBrowser(String browser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(browser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}
	
	

}
