package atco.pages;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class HomePage extends ATCOBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public HomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	
	private By homeTab = By.xpath("//span[contains(.,'Home')]");
	
	private By leadsTab= By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	@FindBy(xpath="//button[@class='slds-button slds-show']") WebElement menuIcon;
	@FindBy(xpath="//input[@class='slds-input' and contains(@placeholder,'Search apps and items')]") WebElement txtSearchBy;
	private By globalSearch = By.xpath("//input[@title='Search Salesforce']");
	
	
	
	public void homePageValidation() {
		waitUntilElementVisibleBy(driver, homeTab, 120);
		
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Homepage Navigated");
		
		if(verifyObjectDisplayed(leadsTab)) {
			clickElementBy(leadsTab);
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Leads Tab clicked");
		}else {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Leads Tab not present");
		}
		
		
	}
	
	public void globalSearch(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearch, 120);
		if(verifyObjectDisplayed(globalSearch)){
			fieldDataEnter(globalSearch, testData.get("MmbershipNumber"));
			driver.findElement(globalSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");
			
		}else{
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
		
	}
	
	public void verifyMemberGlobalSearch(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement mem = driver.findElement(By.xpath("(//a[@title='"+testData.get("MmbershipNumber")+"'])[2]"));
		String member = mem.getText().trim();
		if(member.equalsIgnoreCase(testData.get("MmbershipNumber"))){
				tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");
			
		}else{
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
		
	}
	
	//public By lnkViewAll = By.xpath("//div[@class='slds-size_medium']//button");
	public By txtVocuhers = By.xpath("//span[@title='Vouchers']");
	@FindBy(xpath="//div[@class='slds-size_medium']//button") WebElement lnkViewAll;
	public void navigateToMenu(String Iterator){
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if(verifyObjectDisplayed(txtVocuhers)){
			tcConfig.updateTestReporter("HomePage", "navigateToMenu", Status.PASS, "User is navigated to Voucher page");
		}
		else{
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, homeTab, 120);
				checkLoadingSpinner();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if(verifyElementDisplayed(menuIcon)){
					tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
				}else{
					tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "User is NOT navigated to Homepage");
				}
					menuIcon.click();		
					
				   
					waitUntilElementVisibleBy(driver, txtSearchBy, 20);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					System.out.println("Menu"+Iterator);
					txtSearchBy.sendKeys(testData.get("Menu"+Iterator));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement ele=driver.findElements(By.xpath("//p[@class='slds-truncate']")).get(0);
					waitUntilElementVisibleBy(driver, ele, 60);
					clickElementJSWithWait(ele); 			
	}
	}
	
	
			
		
	
	
	public void navigateToCommunity(){
		
		
			waitForSometime(tcConfig.getConfig().get("LongWait"));
//			waitUntilElementVisibleBy(driver, homeTab, 120);
			checkLoadingSpinner();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if(verifyElementDisplayed(menuIcon)){
				tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
			}else{
				tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "User is NOT navigated to Homepage");
			}
			menuIcon.click();
			waitUntilElementVisibleBy(driver, txtSearchBy, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//a[contains(text(),'Journey Community')]")).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			SwitchtoWindow();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		
	}
	
	public void navigateToUrl(String Iterator) throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("URL" + Iterator));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}
	
	private By arivallist = By.xpath("(//img[@class='wn-left_custom_icon'])[3]"); 
	private By clickadd = By.xpath("(//div[@title='expand'])[2]");
	
	public void arrivallistselect(){
		waitUntilElementVisibleBy(driver, arivallist, 120);
		driver.findElement(arivallist).click();
		waitUntilElementVisibleBy(driver, clickadd, 120);
		driver.findElement(clickadd).click();
	}
	
	private By navigatejourneycomm = By.xpath("//a[contains(.,'Journey Community')]");
	private By Homemarketer=By.xpath("//li//a//img[contains(@src,'Home')]");
	
	public void navigatatojourneycommunity(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, homeTab, 120);
		checkLoadingSpinner();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if(verifyElementDisplayed(menuIcon)){
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "User is navigated to Homepage");
		}else{
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "User is NOT navigated to Homepage");
		}
		menuIcon.click();
		waitUntilElementVisibleBy(driver, navigatejourneycomm, 120);
		driver.findElement(navigatejourneycomm).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		/*ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(1));*/
		
	}
	private By arlst = By.xpath("//b[text()='Arrival List']");
	
	public void selectarrivallist(){
		waitUntilElementVisibleBy(driver, arlst, 120);
		driver.findElement(arlst).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(1));
	}
	private By ressel = By.xpath("//a[text()='Reservations']/ancestor::div[@class='slds-grid slds-grid--vertical slds-m-bottom_small']//a[text()='"+testData.get("ReservationNumber")+"']");
	private By travelchnl = By.xpath("//span[@title='Travel Channel']");
	private By Bookbtn=By.xpath("//button[text()='Book Tour']");
	public void SelRes(){
		waitUntilElementVisibleBy(driver, ressel, 120);
		driver.findElement(ressel).click();
		waitUntilElementVisibleBy(driver, Bookbtn, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(verifyElementDisplayed(driver.findElement(Bookbtn))){
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Clicked on Available Reservation");
		}else{
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Reservation is not found");
		}
	}
	
	private By ownersel = By.xpath("//a[text()='Owners']/ancestor::div[@class='resultsItem slds-col slds-no-flex slds-card']//a[text()='"+testData.get("LeadName")+"']");
	private By ownerimg = By.xpath("//img[@title='Owner']");
	public void SelOwner(){
		waitUntilElementVisibleBy(driver, ownersel, 120);
		driver.findElement(ownersel).click();
		waitUntilElementVisibleBy(driver, ownerimg, 120);
		if(verifyElementDisplayed(driver.findElement(ownerimg))){
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.PASS, "Clicked on Available Owner");
		}else{
			tcConfig.updateTestReporter("HomePage", "SelRes", Status.FAIL, "Owner is not found");
		}
	}
	
	private By txtTourRecords = By.xpath("//input[@class='slds-input input']");
	public void searchTourRecords(){
		waitUntilElementVisibleBy(driver, txtTourRecords, 120);
		driver.findElement(txtTourRecords).click();
	}
	
	
	
	

	private By searchBar =By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Search.svg']");
	private By memberNumber = By.xpath("//input[@class='slds-input']");
	private By btnbookTour = By.xpath("//button[text()='BOOK TOUR']");
	private By btnCreateTour = By.xpath("//button[text()='CREATE TOUR']");
	private By arrivalList = By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Reservation_List.svg']");
	private By txtSearchGuest = By.xpath("//input[@id='text-input-id-1']");
	private By lnkGuestName = By.xpath("//a[text()='"+testData.get("LeadName")+"']");
	private By arrivalIcon = By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Reservation_List.svg'");
	private By imgArrival= By.xpath("//img[contains(@src,'Reservation_List')]");
	                 
	
	public void searchReservaion(){
	
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	    driver.findElement(arrivalList).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(txtSearchGuest).sendKeys(testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(lnkGuestName).click();
		
		
	}	
	public void ownerArrival(){
		
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		 
		if(verifyObjectDisplayed(imgArrival)){
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(imgArrival).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, "Search Result displayed");
			}else{
				tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.FAIL, "Search Result not displayed");
			}
		waitForSometime(tcConfig.getConfig().get("LongWait"));   
	}
		
		
	
	public void globalSearchOwner(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, searchBar, 120);
		if(verifyObjectDisplayed(searchBar)){
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(memberNumber, testData.get("LeadName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(memberNumber).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.PASS, "Search Result displayed");
			
		}else{
			tcConfig.updateTestReporter("HomePage", "globalSearchOwner", Status.FAIL, "Search Result not displayed");
		}
	}
	
	
	public void toursToConfirm(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//WebElement searchBar= driver.findElement(By.xpath("//img[@src='/wvo/resource/1548667560000/Wyn_Custom_Icons/Search.svg']"));
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
		String name = testData.get("LeadName");
		waitUntilElementVisibleBy(driver, searchBar, 120);
		
		driver.findElement(searchBar).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(memberNumber).sendKeys(name);
		driver.findElement(memberNumber).sendKeys(Keys.ENTER);
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnbookTour, 120);
		
		 List<WebElement> button =driver.findElements(btnbookTour);
		 int count=button.size();
		 if(count>0)
		 {
			 button.get(0).click();
			 
		 }
		 else
		 {
			 tcConfig.updateTestReporter("HomePage", "toursToConfirm", Status.FAIL, "Book Tour button not displayed");
		 }
		    
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnCreateTour, 120);
		clickElementBy(btnCreateTour);
	}
	
	
	/*public void mouseoverAndClick(WebElement element){
		try{
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			new Actions(driver).moveToElement(element).click().perform();
		}catch(Exception e){
			tcConfig.updateTestReporter("", "mouseoverAndClick",Status.FAIL, "unable to hover and click "
					+ "element due to "+ e.getMessage());
		}
		
	}
	*/
	
	private By ReservationSearch= By.xpath("//input[@placeholder='Search Reservations and more...']");
	
	public void globalSearchRes(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ReservationSearch, 120);
		if(verifyObjectDisplayed(ReservationSearch)){
			fieldDataEnter(ReservationSearch, testData.get("ReservationNumber"));
			driver.findElement(ReservationSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");
			
		}else{
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
	}
	
	
	public void globalSearch_Owner(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, globalSearch, 120);
		if(verifyObjectDisplayed(globalSearch)){
			fieldDataEnter(globalSearch, testData.get("LeadName"));
			driver.findElement(globalSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed");
			
		}else{
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result not displayed");
		}
	}
	public By FirstName = By.xpath("//span[text()='Primary Guest First Name']//..//..//span[@class='uiOutputText']");
	public By LastName = By.xpath("//span[text()='Primary Guest Last Name']//..//..//span[@class='uiOutputText']");
	public void validateFN_LN(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, FirstName, 120);
		String Fname = driver.findElement(FirstName).getText();
		if (Fname.equalsIgnoreCase(testData.get("FirstName"))) {
			tcConfig.updateTestReporter("HomePage", "validateFN_LN", Status.PASS, "First Name Validated");
			
		}else{
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Error while validating first name");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, LastName, 120);
		String Lname = driver.findElement(LastName).getText();
		if (Lname.equalsIgnoreCase(testData.get("LastName"))) {
			tcConfig.updateTestReporter("HomePage", "validateFN_LN", Status.PASS, "Last Name Validated");
			
		}else{
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Error while validating last name");
		}
	}
	
	
	public void globalSearch(String data){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try{
			waitUntilElementVisibleBy(driver, globalSearch, 120);
			if(verifyObjectDisplayed(globalSearch)){
				fieldDataEnter(globalSearch, data);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> e = driver.findElements(By.xpath("//span[contains(@title,'"+data+"')]/../../../a"));
				if(e.size()>0)
				{
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(e.get(0));
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.PASS, "Search Result displayed and clicked");
				}
				else
				{
					tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Search Result is not displayed");
				}		
			}else{
				tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Global search box is not displayed");
			}
		}
		catch(Exception e){
			tcConfig.updateTestReporter("HomePage", "globalSearch", Status.FAIL, "Getting error:" + e);
			throw e;
		}
		
	}
	
	private By journeyTabHomePage = By.xpath("//div[contains(@class,'wn-banner_home cTMCMAHomePage')]");
	private By lnkArrivalList = By.xpath("//img[contains(@src,'Reservation_List')]/..");
	private By drpDownSalesStore = By.xpath("//input[@name='territoryType']");
	private By lnkSearchGuest = By.xpath("//img[contains(@src,'All_Guests')]");
	private By drpDwnTourOrRes = By.xpath("//select[@name='Dropdown']");
	private By inputSearchTourRes = By.xpath("//input[contains(@placeholder,'Number')]");
	
	public void navigateToArrivalListMobUI()
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, journeyTabHomePage, 120);
		clickElementBy(lnkArrivalList);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
	}
	
	public void navigateToSearchReservationOrTour()
	{
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, journeyTabHomePage, 120);
		clickElementBy(lnkSearchGuest);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, drpDwnTourOrRes, 120);
	}
	
	public void searchReservation(String ReservationNum)
	{
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, inputSearchTourRes, 120);
		new Select(driver.findElement(drpDwnTourOrRes)).selectByIndex(1);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		fieldDataEnter(inputSearchTourRes, ReservationNum);
		driver.findElement(inputSearchTourRes).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, By.xpath("//a[text()='"+ReservationNum+"']"), 120);
		if(verifyObjectDisplayed(By.xpath("//a[text()='"+ReservationNum+"']")))
		{
			tcConfig.updateTestReporter("HomePage", "searchReservation", Status.PASS, "Searched Result is displayed");
			clickElementBy(By.xpath("//a[text()='"+ReservationNum+"']"));
		}
		else
		{
			tcConfig.updateTestReporter("HomePage", "searchReservation", Status.FAIL, "Searched Result is not displayed");
		}
	}
	
	private By drpDwnArrivalList = By.xpath("(//select[@class='slds-select'])[2]");
	private By drpDwnWeek = By.xpath("(//select[@class='slds-select'])[1]");
	private By btnExpand = By.xpath("//div[@title='expand'][contains(@id,'arrivalList')]");
	
	public void openArrivals()
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, drpDownSalesStore, 120);
		
		WebElement option = new Select(driver.findElement(drpDwnArrivalList)).getFirstSelectedOption();
		if(!(option.getText().equals("ARRIVAL LIST")))
		{
			new Select(driver.findElement(drpDwnArrivalList)).selectByIndex(0);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		
		new Select(driver.findElement(drpDwnWeek)).selectByIndex(3);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		while(driver.findElements(btnExpand).size()==0 && driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]")).size()==0)
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		
		List<WebElement> e1 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
		if(e1.size()>0)
		{
			clickElementBy(drpDownSalesStore);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(driver.findElements(By.xpath("//lightning-base-combobox-item")).get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		while(driver.findElements(btnExpand).size()==0 && driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]")).size()==0)
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		List<WebElement> e2 = driver.findElements(By.xpath("//div[contains(text(),'No Records Found')]"));
		if(e2.size()==0)
		{
			tcConfig.updateTestReporter("ArrivalPage", "openArrivals", Status.PASS, "Records are visible");
		}
		else
		{
			tcConfig.updateTestReporter("ArrivalPage", "openArrivals", Status.FAIL, "Records are not available");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	} 
	
	private By lnkReportMobUi = By.xpath("//img[contains(@src,'Report')]/..");
	private By errorMsg = By.xpath("//p[text()='If Reports and Dashboards are not present please refresh.']");
	private By txtBoxSearchReports = By.xpath("//input[contains(@placeholder,'Search recent reports')]");
	
	public void navigateToReportMobUi()
	{
		waitUntilElementVisibleBy(driver, lnkReportMobUi, 120);
		clickElementBy(lnkReportMobUi);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> e = driver.findElements(errorMsg);
		if(e.size()>0)
		{
			driver.navigate().refresh();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, txtBoxSearchReports, 120);
		}
	}
}
