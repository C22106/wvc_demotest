package atco.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class VouchersPage extends ATCOBasePage  {

	public static final Logger log = Logger.getLogger(VouchersPage.class);

	public VouchersPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	static final String SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String getCurrentNoOfPrints;
	public static String getFutureNoOfPrints;
	public static String getFutureModifiedByTimestamp;
	public static String voucherNumber;
	public static String prevVendorID;
	public static String aftrVendorID;
	public static String parentTransactionNo;
	public static String redeemptionExp;
	public static String cardNum;
	public static String productName;
	public static String totalQuantity;
	public static String totalReatilValue;
	public static String totalCost;
	JavascriptExecutor je = (JavascriptExecutor) driver;

	protected By btnEditIssued = By.xpath("(//button[@title='Edit Issued']//span)[1]");
	protected By chkIssued =By.xpath("//input[@name='Issued__c']");
	protected By errorMsgUnIssue = By.xpath("//div[@class='slds-form-element__help']");
	private By txtSearchVoucher = By.xpath("//input[@placeholder='Search Vouchers and more...']");
	private By btnBookTourNative = By.xpath("//button[contains(text(),'Book Tour')]");
	private By clickVoucher = By.xpath("//a[contains(@title,'"+testData.get("VoucherNumber")+"')]");
	private By lnkVoucher = By.xpath("//a[contains(text(),'Vouchers')]");
	protected By btnNew = By.xpath("//div[@title='New']");
	protected By btnNext = By.xpath("//span[contains(text(),'Next')]");
	protected By btnProductInformation = By.xpath("//span[contains(text(),'Product Information')]");			
	protected By txtSalesStorePremium = By.xpath("//span[contains(text(),'Sales Store Premium')]/../..//div//input[@title='Search Sales Store Premiums']");
	protected By clickSalesStorePremium = By.xpath("//div[@title='SP-011621']//mark");
	protected By txtQuantityOnHand = By.xpath("//span[contains(text(),'Quantity of Product')]/../..//input");
	protected By txtSalesStores = By.xpath("//span[contains(text(),'Sales Store')]/../..//div//input[@title='Search Sales Stores']");
	protected By valueSalesStore = By.xpath("//div[@title='Branson']//mark");
	protected By drpTransactionType = By.xpath("//span[contains(text(),'Transaction Type')]/../..//div//a");
	protected By vlaueTransactionType = By.xpath("//div[contains(@class,'select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short ')]");
	protected By valuedrp = By.xpath("//div[contains(@class,'select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short ')]//div//li//a[@title='Ticket Sale']");
	protected By vlaueMain = By.xpath("//a[@title='Ticket Sale']");
	protected By btnCustomerInformation = By.xpath("//span[contains(text(),'Customer Information')]");
	protected By txtCustomerFirstName = By.xpath("//span[contains(text(),'Customer First Name')]/../..//input[@class=' input']");
	protected By txtCustomerLastName = By.xpath("//span[contains(text(),'Customer Last Name')]/../..//input[@class=' input']");
	protected By btnSaveVoucherCreation = By.xpath("(//div[@class='button-container-inner slds-float_right']//span[contains(text(),'Save')])[2]");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtBookingAgent = By.xpath("//span[contains(text(),'Booking Agent')]/../..//div//input");
	protected By clickBookingAgent = By.xpath("(//div[@title='WVO Corporate Super User'])[1]");
	protected By txtQQH = By.xpath("//span[contains(text(),'Quantity of Product')]/../..//input[@class='input  uiInputSmartNumber']");
	protected By vocuherNum = By.xpath("//span[contains(text(),'Voucher Number')]/../..//span//lightning-formatted-text");
	protected By btnEdit = By.xpath("//button[@name='Edit']");
	protected By txtVoucherType = By.xpath("//span[contains(text(),'Voucher Type')]/../../..//div//lightning-formatted-text");
	protected By lnkTransactionNumber = By.xpath("(//span[contains(text(),'Transaction')]/../..//div//a)[2]");
	protected By txtVoucherStatus = By.xpath("//span[contains(text(),'Voucher Status')]");
	protected By btnEditReturned = By.xpath("//button[@title='Edit Returned']");
	protected By chkReturned = By.xpath("//input[@name='Returned__c']");
	protected By drpReturnReason = By.xpath("//label[contains(text(),'Return Reason')]/../../..//div//input");	
	protected By value = By.xpath("//div//lightning-base-combobox-item//span[@title='Distributed in Error']");
	protected By btnSave = By.xpath("//button[@title='Save']");
	protected By chkReturn = By.xpath("(//span[contains(text(),'Returned')]/../..//label//span)[1]");
	protected By lastModifiedBy =By.xpath("//span[contains(text(),'Last Modified By')]/../..//div[@class='slds-form-element__control']"); 
	protected By returnDate = By.xpath("//span[contains(text(),'Return Date')]/../..//div//lightning-formatted-text");
	protected By username= By.xpath("//div[@class='slds-grid']//a[starts-with(@href,'/lightning/r/')]");
	protected  By chkCheckBox = By.xpath("//input[@name='Issued__c']");
	protected By txtHeader = By.xpath("//h2[contains(.,'Edit Voucher')]");
	protected By tabVoucherStatus = By.xpath("(//span[contains(text(),'Voucher Status')])[2]");
	protected By chkReturnUncheck = By.xpath("(//span[contains(.,'Returned')]/../..//input[@type='checkbox'])[2]");
	protected By returnResonDrp =By.xpath("//span[contains(text(),'Return Reason')]/../../..//div//a");
	protected By drpVlaue = By.xpath("//div[contains(@class,'select-options popupTargetContainer ')]//li//a[@title='Distributed in Error']");
	protected By chkRedeemed =By.xpath("//span[contains(text(),'Redeemed')]/../../..//div//label");
	protected By errorMsg = By.xpath("(//div[@class='slds-popover__body'])[2]");
	protected By btnCancel = By.xpath("//button[@title='Cancel']");
	protected By closeErrorMsg = By.xpath("//button[@title='Close error dialog']//lightning-primitive-icon"); 
	protected By quantityOfProduct = By.xpath("//span[contains(text(),'Quantity of Product')]/../.././/div//lightning-formatted-number");
	protected By noOfPrintsBefore = By.xpath("(//span[contains(text(),'Number of Prints')]/../..//div//lightning-formatted-number)[1]");
	protected By btnPrint = By.xpath("(//button[@title='Click this button to Print the Letter.'])");
	protected By pdfPage = By.xpath("//embed[contains(@type,'pdf')]");
	protected By noOfPrintAfter = By.xpath("(//span[contains(text(),'Number of Prints')]/../..//div//lightning-formatted-number)[1]");
	protected By lastModifiedUser = By.xpath("//span[contains(text(),'Last Modified By')]/../..//div[@class='slds-grid']//a");
	protected By txtBarcodes = By.xpath("//span[contains(text(),'Barcodes')]");
	protected By lstModifiedDate = By.xpath("//span[contains(text(),'Last Modified By')]/../..//div//lightning-formatted-text");
	protected By issuedDate = By.xpath("//span[contains(text(),'Issued Date')]/../..//div//lightning-formatted-text");
	protected By txtEditCustomerFirstName = By.xpath("//button[@title='Edit Customer First Name']");
	protected By txtCustomerInformation = By.xpath("//span[contains(text(),'Customer Information')]");
	protected By txtCustFirstName = By.xpath("//span[contains(text(),'Customer First Name')]/../..//input"); 
	protected By txtCreatedBy = By.xpath("//span[contains(text(),'Created By')]/../../..//div//lightning-formatted-text");
	protected By errorMsgVoucher = By.xpath("//div[@class='genericNotifications']");
	protected By errorMsgVoucher_Vendor = By.xpath("//span[@class='genericError uiOutputText']/../..//ul//li");
	protected By btnSaveEditVoucher = By.xpath("//div[contains(@class,'modal-footer slds-modal__footer')]//button[@title='Save']");
	protected By btnCancelEditVoucher = By.xpath("//div[contains(@class,'modal-footer slds-modal__footer')]//button[@title='Cancel']");
	protected By txtCustomerNmBeforeEdit = By.xpath("//span[contains(.,'Customer First Name')]/../..//lightning-formatted-text");
	protected By txtCusLastName = By.xpath("//span[contains(.,'Customer Last Name')]/../..//lightning-formatted-text");
	protected By btnCustoInfo = By.xpath("(//span[contains(text(),'Customer Information')])[2]");
	protected By txtCustLastName = By.xpath("//span[contains(.,'Customer Last Name')]/../..//input");
	public By txtTransactionNumber = By.xpath("(//span[contains(text(),'Transaction Number')]/../../..//div[@class='slds-grid']//a)[1]");
	protected By txtPaymentDetails = By.xpath("//span[contains(text(),'Payment Details')]");
	protected By txtAmountCollected = By.xpath("(//span[contains(text(),'Amount Collected')]/../..//div//lightning-formatted-text)[2]");
	protected By txtPaymentMethod = By.xpath("(//span[contains(text(),'Payment Method')]/../..//div//lightning-formatted-text)[2]");
	protected By txtCardType = By.xpath("(//span[contains(text(),'Card Type')]/../..//div//lightning-formatted-text)[2]");
	protected By txtLast4ofCard = By.xpath("(//span[contains(text(),'Last 4 of Card')]/../..//div//lightning-formatted-text)[2]");
	protected By btnEditTransact = By.xpath("(//button[@name='Edit'])[2]");
	protected By txtPaymentDetails_Transact = By.xpath("(//span[contains(text(),'Payment Details')])[2]");
	protected By txtCardNo_Edit = By.xpath("//span[contains(text(),'Last 4 of Card')]/../..//input[@class=' input']");
	protected By amtcollect = By.xpath("//span[contains(.,'Amount Collected')]/../..//input");
	protected By paymentMethod = By.xpath("//span[contains(.,'Payment Method')]/../..//a");
	protected By container_Payment =By.xpath("//div[@class='select-options']//a[contains(.,'Credit')]");
	protected By cardType = By.xpath("//span[contains(.,'Card Type')]/../..//a");
	protected By container_CardType = By.xpath("//div[@class='select-options']//a[@title='MasterCard']");
	public By del_Icon = By.xpath("(//span[@class='deleteIcon'])[2]");
	protected By unActiveProduct = By.xpath("//li[@class='form-element__help']");
	protected By wyn_calender =By.xpath("(//a[@class='datePicker-openIcon display'])[1]");
	protected By errorMsg_Wyn = By.xpath("//span[@class='genericError uiOutputText']");
	protected By errorMsg_Vendor = By.xpath("//span[@class='genericError uiOutputText']");
	protected By update_Product = By.xpath("(//input[@title='Search Products']/../../..//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body']//mark)[1]");
	protected By Qoh_ErrorMsg = By.xpath("//div[@class='desktop forcePageError']//li");
	protected By btnAddActivity = By.xpath("//button[contains(text(),'Add Activity')]");
	protected By txtSalesStorePremiums = By.xpath("//input[@placeholder='Search Sales Store Premiums...']");
	protected By clickSalesStorePremums = By.xpath("//span[@class='slds-media__body']//lightning-base-combobox-formatted-text");
	protected By btnSaveAddActivity = By.xpath("//button[@name='save']");
	protected By btnRelatedTab = By.xpath("//a[contains(text(),'Related')]");
	protected By txtTransactNoPage = By.xpath("//div[contains(text(),'Voucher Transaction')]/../..//span//a");
	protected By childVoucher = By.xpath("//table[@data-aura-class='uiVirtualDataGrid--default uiVirtualDataGrid']/tbody/tr/td[1]/span");
	protected By clickEditChildVoucher = By.xpath("(//table[@data-aura-class='uiVirtualDataGrid--default uiVirtualDataGrid']/tbody/tr/td[4]//a//lightning-primitive-icon)[1]");
	protected By btnEditTabChild = By.xpath("//a[@title='Edit']");
	protected By transactionNoChild = By.xpath("//span[contains(text(),'Transaction Number')]/../..//span[@class='pillText']");
	protected By btnCancelChildVoucher = By.xpath("//button[@data-aura-class='uiButton forceActionButton']//span[contains(text(),'Cancel')]");
	protected By btnNEwChange = By.xpath("(//div[@class='slds-card__body']//lightning-primitive-icon)[1]");
	public By QOP_AddActivity = By.xpath("//input[@name='Quantity__c']");
	public By txtVocuhers =By.xpath("(//span[@title='Vouchers'])[2]");
	protected By lst_QOP = By.xpath("//span[@class='slds-grid slds-grid--align-spread']//a[@data-aura-class='forceOutputLookup']");
	protected By lnkTransactionNo = By.xpath("(//span[contains(text(),'Transaction')]/../..//a)[2]");
	protected By lnk_TransactionNo = By.xpath("(//span[@class='uiOutputText'])[2]");
	protected By lst_QoP = By.xpath("//span[@class='slds-truncate uiOutputNumber']");
	protected By btnCancelChildTransact = By.xpath("//button[@title='Cancel']//span[contains(text(),'Cancel')]");
	protected By btnReceipt = By.xpath("//button[@title='Click this button to Reciept the Letter.']");
	protected By txtDetailsTab = By.xpath("//a[@id='detailTab__item']");
	protected By txtSalesStorePhone = By.xpath("//span[contains(text(),'Sales Store Phone')]");
	protected By txtRecentlyViewed = By.xpath("(//span[contains(text(),'Recently Viewed')])[1]");
	protected By containerRecentlyViewed = By.xpath("//div[@class='panel-content scrollable']//div[@class='listContent']");
	protected By clickCeasersVoucher = By.xpath("//span[contains(text(),'Caesars Vouchers - Unassigned')]");
	protected By listShowMore = By.xpath("(//a[contains(@class,'rowActionsPlaceHolder slds-button slds-button--icon-x-small slds')]//lightning-primitive-icon)[1]");
	protected By btnEditlst = By.xpath("//div[@class='branding-actions actionMenu']//a[@title='Edit']");
	protected By txtEditVoucher = By.xpath("//h2[contains(text(),'Edit Voucher')]");
	protected By txtSalesStorePhoneVendor = By.xpath("//span[contains(text(),'Sales Store Phone')]/../../..//div//span[@class='uiOutputTextArea']");
	protected By txtCustomerFstNum = By.xpath("//span[contains(text(),'Customer First Name')]/../..//input");
	protected By txtCustomerLstNum = By.xpath("//span[contains(text(),'Customer Last Name')]/../..//input");
	protected By chkIssuedVendor = By.xpath("//span[contains(text(),'Issued')]/../..//input[@type='checkbox']");
	protected By btnSaveVendor = By.xpath("//button[@title='Save']");
	protected By sucessMsgVendor = By.xpath("");
	protected By voucherNum = By.xpath("(//span[@title='Voucher Number']/../../../../../..//tbody//span[@data-aura-class='uiOutputTextArea'])[1]");
	protected By searchVoucher = By.xpath("//input[@title='Search Vouchers and more']");
	protected By txtVoucherSearch = By.xpath("//a[contains(text(),'Vouchers')]");
	protected By lnkVoucherNo = By.xpath("(//span[@title='Voucher Name']/../../../../../..//tbody//a)[1]");
	protected By localVendorSearch = By.xpath("//input[@name='Vouchers__c-search-input']");
	protected By voucher1 = By.xpath("(//slot[@name='primaryField']//lightning-formatted-text)[1]");
	protected By txtSerachVendor = By.xpath("//input[contains(@class,'slds-input default input uiInput uiInputTextForAutocomplete')]");
	protected By lst_Ceasers = By.xpath("//div[@class='listContent']//span[@class=' virtualAutocompleteOptionText']");
	protected By vendorVoucherNum = By.xpath("(//div[contains(.,'Voucher')]/../..//slot[@name='primaryField']//lightning-formatted-text)[1]");
	protected By cesarsVendor = By.xpath("//span[@class='triggerLinkText selectedListView uiOutputText']");
	protected By productDisplay = By.xpath("(//span[@class='slds-truncate uiOutputTextArea'])[1]");
	protected By lst_Product = By.xpath("//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark");
	protected By productName_Vendor  = By.xpath("//span[contains(.,'Product')]/../..//span[@class='pillText']"); 
	protected By productLst = By.xpath("//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark");
	protected By product_Tab = By.xpath("//span[contains(text(),'Product')]/../..//input[@title='Search Products']");
	protected By edit_QOH = By.xpath("(//span[contains(text(),'Quantity of Product')]/../..//input)[2]");
    protected By edit_save = By.xpath("//button[@title='Save']");
    protected By erroMsg_Vendor = By.xpath("//span[@class='genericError uiOutputText']");
	public By txtSearchVoucherTab = By.xpath("//input[@title='Search Salesforce']");
	protected By clickVendorVoucher = By.xpath("(//img[@class='slds-truncate unchecked' and @alt='False'])[1]/../../../..//th//a[@data-aura-class='forceOutputLookup']");
	protected By localSearchVoucher = By.xpath("//input[@name='Vouchers__c-search-input']");
	protected By closeTab = By.xpath("//button[@title='Close']//lightning-primitive-icon");
	protected By lstVocuher = By.xpath("//a[@data-aura-class='forceOutputLookup']");
	protected By radioBtn = By.xpath("//span[@class='slds-radio--faux']");
	protected By txtSampleVoucher = By.xpath("//h2[contains(text(),'New Voucher: Sample Voucher')]");
	protected By sampleVoucherNo =By.xpath("//span[contains(@class,'toastMessage slds-text-heading')]");
	protected By lstSample = By.xpath("//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark");
	protected By lstSaleStore = By.xpath("//div[not(contains(@title,'Indio'))]/following-sibling::div[@title='All Parent Sales Store Operating Hours']");
	protected By lstVendorVoucher = By.xpath("(//a[@class='slds-button slds-button--icon-x-small slds-button--icon-border-filled']//lightning-primitive-icon)[1]");
	protected By btnEditVendor = By.xpath("//div[@class='branding-actions actionMenu']//a[@title='Edit']");
	protected By slaesstoreclick= By.xpath("(//span[@class='deleteIcon'])[2]");
	protected By txtSalesStore = By.xpath("(//span[contains(text(),'Sales Store')]/../../../..//div//ul)[1]");
	protected By bookingAgent = By.xpath("//span[contains(text(),'Booking Agent')]/../..//div//input[@title='Search Agents']");
	protected By deleteIcon = By.xpath("//span[@class='deleteIcon']");
	protected By selectOtherAgent = By.xpath("(//div[@title='WVO In-House Marketer'])[1]");
	protected By errorMsgVendor = By.xpath("//ul[@class='errorsList']//li");
	protected By btnCancelVendor = By.xpath("(//span[contains(text(),'Cancel')])[1]");
	protected By btnSaveEdit = By.xpath("(//button[@title='Save'])[1]");
	protected By btnCanel =By.xpath("(//span[contains(text(),'Cancel')])[2]");
	protected By container = By.xpath("//div[@title='WVO Corporate Super User']");
	protected By container_Update = By.xpath("//div[@title='Yosra Assila']");
	protected By salesStore = By.xpath("(//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark)[1]");
	protected By SSP = By.xpath("//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark[@class='data-match']");
	protected By productTab = By.xpath("(//label[@data-aura-class='uiLabel']//span[contains(text(),'Product')])[1]");
	protected By productTab_Blank = By.xpath("//input[@title='Search Products']");
	protected By productTab_Item = By.xpath("//span[contains(text(),'Product')]/../..//div//span[@class='pillText']");
	protected By btnEditSave = By.xpath("(//div[@class='modal-footer slds-modal__footer']//span[contains(text(),'Save')])[2]");
	protected By btnEditCancel = By.xpath("//div[@class='modal-footer slds-modal__footer']//span[contains(text(),'Cancel')]");
	protected By vendorConfirmation = By.xpath("//span[contains(text(),'Vendor Confirmation')]/../..//input[@class=' input']");
	protected By txtVendorConfirm = By.xpath("//span[contains(text(),'Vendor Confirmation')]/../..//div//lightning-formatted-text");
	protected By clickSalestore_Vendor = By.xpath("//span[contains(text(),'Sales Store')]/../..//div//input[@title='Search Sales Stores']");
	protected By chngSalesStore = By.xpath("//div[@title='Bonnet Creek']");
	protected By txtErrorMsg = By.xpath("//div[@class='genericNotification']/../..//ul//li");
	protected By passCase = By.xpath("(//div[@title='WVO Corporate Super User'])[1]");
	protected By txtSucessMsg = By.xpath("//span[@data-aura-class='forceActionsText']");
	protected By chkPaid = By.xpath("(//span[contains(text(),'Paid')]/../..//input[@type='checkbox'])[2]");
	protected By btnVoucherStatus = By.xpath("(//span[contains(text(),'Voucher Status')])[2]");
	protected By txtInvoiceID = By.xpath("//span[contains(text(),'Invoice')]/../..//input");
	protected By btnEdit_Paid = By.xpath("//button[@title='Edit Paid']");
	protected By chkPaid_Edit = By.xpath("//input[@name='Paid__c']");
	protected By txtInvoiceID_Edit = By.xpath("//input[@name='Invoice__c']");
	protected By chkIssue = By.xpath("//span[contains(text(),'Issued')]/../..//span[@class='slds-checkbox_faux']");
	protected By chk = By.xpath("(//span[@class='slds-checkbox_faux'])[2]");
	protected By IssuedDate = By.xpath("//span[contains(text(),'Issued Date')]/../..//div//lightning-formatted-text");
	protected By chkVoid = By.xpath("//button[@title='Edit Voided']");
	protected By voidcheckbox = By.xpath("//span[contains(text(),'Voided')]/../..//div//span[@class='slds-checkbox_faux']");
	protected By txtProduct = By.xpath("//span[contains(text(),'Product')]/../..//span//a");
	protected By txtQuantity = By.xpath("//span[contains(text(),'Quantity of Product')]/../..//span//lightning-formatted-number");
	protected By ttlRetailValue = By.xpath("//span[contains(text(),'Total Retail Value')]/../..//span//lightning-formatted-text");
	protected By ttlCostValue = By.xpath("//span[contains(text(),'Total Net Cost')]/../..//span//lightning-formatted-text");
	
	protected By lst_return = By.xpath("//div//lightning-base-combobox-item//span[@title='Vendor Cancel']");
	protected By returnReason = By.xpath("(//span[contains(text(),'Return Reason')]/../..//span//lightning-formatted-text)[1]");
	protected By ttlQuantity = By.xpath("//span[contains(text(),'Total Quantity')]/../..//lightning-formatted-number");
	protected By ttlRetail =  By.xpath("//span[contains(text(),'Grand Total Retail')]/../..//lightning-formatted-number");
	protected By ttlCost = By.xpath("//span[contains(text(),'Grand Total Cost')]/../..//lightning-formatted-number");
	protected By tab_Products = By.xpath("//a[@class='slds-context-bar__label-action dndItem']//span[contains(text(),'Products')]");
	protected By searchProduct = By.xpath("//input[@placeholder='Search this list...']");
	protected By lnk_Product = By.xpath("//a[@data-aura-class='forceOutputLookup']");
	protected By btnEdit_Product = By.xpath("//a[@title='Edit']");
	protected By editText = By.xpath("//h2[contains(text(),'Edit Baldknobbers - Family Pass')]");
	protected By edit_redemptionExp = By.xpath("//span[contains(text(),'Redemption Expiration')]/../..//input[@class=' input']");
	protected By txtRedemptionExp = By.xpath("//span[contains(text(),'Redemption Expiration')]/../..//span[@class='uiOutputText']");
	protected By tab_Vouchers = By.xpath("//a[@title='Vouchers']//span[@class='slds-truncate']");
	protected By BookingAgent = By.xpath("//span[contains(text(),'Booking Agent')]/../..//span//a");
	protected By imgIcon = By.xpath("(//div[@data-aura-class='forceEntityIcon']//img[@title='User'])[1]");
	protected By txtLogin = By.xpath("(//a[@class='profile-link-label'])[1]");
	protected By txtRedeemptionExpiration = By.xpath("//span[contains(text(),'Redemption Expiration')]/../..//lightning-formatted-text");
	protected By clear_Agent = By.xpath("(//span[@class='deleteIcon'])[1]");
	protected By txtSearchAgents = By.xpath("//input[@placeholder='Search Agents...']");
	protected By selectAgent = By.xpath("(//div[@class='primaryLabel slds-truncate slds-lookup__result-text']/mark)[1]");
	protected By bulk_Redeem = By.xpath("//span[@class=' virtualAutocompleteOptionText']/mark");
	protected By editRedeem = By.xpath("(//button[@class='slds-button trigger slds-button_icon-border']//span[@class='slds-assistive-text'])[2]");
	protected By lst_Redeem = By.xpath("(//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../../../td[2])");
	protected By redeem_Edit = By.xpath("//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../..//span[@class='slds-grid slds-grid--align-spread forceInlineEditCell']//span[@class='slds-assistive-text']");
	protected By chkBox = By.xpath("//div[@class='slds-cell-wrap slds-popover__body']//input[@type='checkbox']");
	protected By btnApply = By.xpath("//div[@class='slds-text-align_right']//span[contains(text(),'Apply')]");
	protected By btnSave_Redeem = By.xpath("//div[contains(@class,'footer slds-docked-form-footer slds-is-absolute forceListViewManagerGridFooter')]//span[contains(text(),'Save')]");
	protected By cellClick = By.xpath("(//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../../../td[2]/..//td[9]//span[@class='slds-grid slds-grid--align-spread forceInlineEditCell'])[1]");
	protected By pencil_Click = By.xpath("(//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../../../td[2]/..//td[9]//span[@class='slds-grid slds-grid--align-spread forceInlineEditCell']//span[@class='triggerContainer'])[1]");
	protected By sucessMsg_Redeem = By.xpath("//div[@class='slds-align-middle slds-hyphenate']");
	protected By product = By.xpath("(//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../../../td[4])");
	protected By tblRecords = By.xpath("//table[@data-aura-class='uiVirtualDataTable']/tbody/tr/td[1]");
	protected By txtProductInfo = By.xpath("(//span[contains(text(),'Product Information')])[2]");
	protected By hardStopMsg = By.xpath("//li[@class='form-element__help']");
	protected By txtChooseVoucherType = By.xpath("//h2[contains(.,'Choose Voucher Record Type')]");
	protected By nextBtn = By.xpath("//button[@title='Next action']");
	protected By txtAddActivity = By.xpath("//h2[contains(.,'Add Activity')]");
	protected By vendorRadiobtn = By.xpath("//span[@class='slds-radio_faux']");
	protected By lst_select = By.xpath("//button[@title='Clear Selection']//lightning-primitive-icon");
	protected By tabProduct = By.xpath("//input[@placeholder='Search Products...']");
	protected By activityDate = By.xpath("(//input[@name='Activity_Date__c'])[1]");
	protected By quantityOnHand = By.xpath("//input[@name='Quantity__c']");
	protected By txtCustFirstName_Modify = By.xpath("//input[@name='TMCustomerFirstName__c']");
	protected By txtDirection = By.xpath("//textarea[@name='Directions__c']");
	protected By txtCheckIn = By.xpath("(//input[@name='Check_in_Date__c'])[1]");
	protected By select_Product = By.xpath("//span[contains(@class,'slds-listbox__option-text slds-listbox__option-text_entity')]//lightning-base-combobox-formatted-text");
	protected By lstProductName = By.xpath("//span[@class='slds-truncate uiOutputTextArea' and contains(text(),' ')]");
	protected By lstQOP = By.xpath("//span[@class='slds-truncate uiOutputNumber' and contains(text(),'')]");


	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int)(Math.random()*SOURCE.length());
			builder.append(SOURCE.charAt(character));
		}
		return builder.toString();
	}


	

	public void nativeSearchVoucher(){
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, txtSearchVoucher, 120);
			tcConfig.updateTestReporter("VouchersPage", "nativeSearchVoucher", Status.PASS, "Voucher Page is displayed");						
			fieldDataEnter(txtSearchVoucher, testData.get("VoucherNumber").trim());
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(txtSearchVoucher).sendKeys(Keys.ENTER);
			waitUntilElementVisibleBy(driver, lnkVoucher, 120);	
			tcConfig.updateTestReporter("VouchersPage", "nativeSearchVoucher", Status.PASS, "Voucher Record Details is displayed");
			//driver.findElement(clickReservation).click();
			List<WebElement> button =driver.findElements(clickVoucher);
			button.get(button.size()-1).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));					 				
	}


	
		




	public void validateReturnVoucherWyndham(){			
			waitUntilElementVisibleBy(driver, btnEdit, 120);
			if(driver.findElement(txtVoucherType).getText().equals("Wyndham_Vouchers") || driver.findElement(txtVoucherType).getText().equals("Vendor_Vouchers")){
				tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.PASS, "Voucher page is displayed");
				
				clickElementJS(btnEdit);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, txtHeader, 120);
				WebElement element = driver.findElement(tabVoucherStatus);					 								 
									 
				je.executeScript("arguments[0].scrollIntoView(true);",element);
				waitUntilElementVisibleBy(driver, chkReturnUncheck, 120);
				clickElementJS(chkReturnUncheck);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if(driver.findElement(returnResonDrp).isEnabled()){
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(returnResonDrp);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(drpVlaue);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(edit_save);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement elements = driver.findElement(By.xpath("(//span[contains(text(),'Voucher Status')])"));					 								 
					// now execute query which actually will scroll until that element is not appeared on page.					 
					je.executeScript("arguments[0].scrollIntoView(true);",elements);
					if(driver.findElement(chkReturn).getAttribute("className").equals("slds-checkbox_faux")){				        						      				        		
						if(driver.findElement(lastModifiedBy).isDisplayed() && driver.findElement(returnDate).isDisplayed()){
							List<WebElement> button =driver.findElements(username);
							button.get(button.size()-1).getText().trim();								
							System.out.println(button.size()-1); 
							String val = driver.findElement(returnDate).getText().trim();
							System.out.println(val); 
							tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.PASS, "Return checkbox is checked sucessfully and Last modified by : "+button.get(button.size()-1).getText()+" and date is :" +val );
						}
						else{
							tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.FAIL, "Failed while validating: Return checkbox ");				        				        		
						}
					}
					else{
						tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.FAIL, "Failed while validating: Return checkbox and Last Modified date");				        				        		
					}
				}				
			}						
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.FAIL, "Failed while validating voucher details");
			}			
	}

	


	public void validateRedeemedVoucher(){		
			waitUntilElementVisibleBy(driver, btnEdit, 120);
			if(driver.findElement(txtVoucherType).getText().equals("Wyndham_Vouchers") || driver.findElement(txtVoucherType).getText().equals("Vendor_Vouchers")){
				tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.PASS, "Voucher page is displayed");
				JavascriptExecutor je = (JavascriptExecutor) driver;					 									 
				//Identify the WebElement which will appear after scrolling down					 
				WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));					 								 
				// now execute query which actually will scroll until that element is not appeared on page.					 
				je.executeScript("arguments[0].scrollIntoView(true);",element);
				waitUntilElementVisibleBy(driver, chkRedeemed, 120);
				if(driver.findElement(chkRedeemed).getAttribute("innerText").contains("Redeemed")){	
					if(verifyObjectDisplayed(btnEditReturned))
					{
						clickElementJS(btnEditReturned);
						waitForSometime(tcConfig.getConfig().get("MedWait"));												
						clickElementJS(chkReturned);
						waitForSometime(tcConfig.getConfig().get("LowWait"));		
						if(driver.findElement(drpReturnReason).isEnabled()){
							waitForSometime(tcConfig.getConfig().get("LowWait"));				        	
							clickElementJS(drpReturnReason);
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							clickElementJS(value);
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							clickElementJS(btnSave);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							if(verifyObjectDisplayed(errorMsg))
							{
								tcConfig.updateTestReporter("VouchersPage", "validateRedeemedVoucher", Status.PASS, "User should not be allowed to return a redeemed wyndham voucher");
								waitForSometime(tcConfig.getConfig().get("MedWait"));				        						        				        	
								clickElementJS(btnCancel);
								waitForSometime(tcConfig.getConfig().get("LowWait"));

							}
							else{tcConfig.updateTestReporter("VouchersPage", "validateRedeemedVoucher", Status.FAIL, "Failed while validating: error message not shown");}

						}else{tcConfig.updateTestReporter("VouchersPage", "validateRedeemedVoucher", Status.FAIL, "Failed while validating: Return reason dropdown not found ");}

					}else{tcConfig.updateTestReporter("VouchersPage", "validateRedeemedVoucher", Status.FAIL, "Failed while validating: Return edit button not found ");}
				} 
			}else{tcConfig.updateTestReporter("VouchersPage", "validateRedeemedVoucher", Status.FAIL, "Failed while validating: Return edit button not found ");}		
	}

	

	public void validateNotResueReturn(){
	
			waitUntilElementVisibleBy(driver, btnEditIssued, 120);
			if(verifyObjectDisplayed(btnEditIssued)){
				clickElementJS(btnEditIssued);
				waitForSometime(tcConfig.getConfig().get("MedWait"));	
				List<WebElement> eltsChecked = driver.findElements(chkCheckBox);
				Boolean isCheched = eltsChecked.size() > 0;
				if (isCheched) {
					// uncheck the checkbox
					eltsChecked.get(0).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJS(btnSave);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if(verifyObjectDisplayed(errorMsgUnIssue))
					{
						tcConfig.updateTestReporter("VouchersPage", "validateNotResueReturn", Status.PASS, "User should not be allowed to return a reuse wyndham voucher");
						waitForSometime(tcConfig.getConfig().get("MedWait"));				        						        				        	
						clickElementJS(btnCancel);
						waitForSometime(tcConfig.getConfig().get("LowWait"));	        		
					}
					else{System.out.println("Fail 1");
					tcConfig.updateTestReporter("VouchersPage", "validateTransactionNumber", Status.FAIL, "Failed while validating user able to reuse a wyndham voucher");}
				}
				else{
					System.out.println("Failed to uncheck Issued checkbox");
					tcConfig.updateTestReporter("VouchersPage", "validateTransactionNumber", Status.FAIL, "Failed while validating user able uncheck wyndham voucher");
				}						
			}
		
	}

	


	public void validateQuantityOfProduct(){
			waitUntilElementVisibleBy(driver, quantityOfProduct, 120);
			String QOH = driver.findElement(quantityOfProduct).getText().trim();
			int i = Integer.parseInt(QOH);			
			if(i>=1)
			{				
				tcConfig.updateTestReporter("VouchersPage", "validateQuantityOfProduct", Status.PASS, "Quantity on hand should at present :" +i);							
				WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));					 								 
				je.executeScript("arguments[0].scrollIntoView(true);",element);
				waitUntilElementVisibleBy(driver,noOfPrintsBefore, 120);
				if(verifyObjectDisplayed(noOfPrintsBefore)){
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					getCurrentNoOfPrints = driver.findElement(noOfPrintsBefore).getText().trim();
					System.out.println(getCurrentNoOfPrints);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					tcConfig.updateTestReporter("VouchersPage", "validateQuantityOfProduct", Status.PASS, "Number of prints before :" +getCurrentNoOfPrints);	

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement print = driver.findElement(By.xpath("//button[@title='Click this button to Print the Letter.']"));					 								 

					je.executeScript("arguments[0].scrollIntoView(true);",print);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String parentElement = driver.getWindowHandle();
					clickElementJS(btnPrint);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(windowHandles.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
				else{tcConfig.updateTestReporter("VouchersPage", "validateQuantityOfProduct", Status.FAIL, "failed while validating current number of prints");}
			}
			else{tcConfig.updateTestReporter("VouchersPage", "validateQuantityOfProduct", Status.FAIL, "failed while validating Quantity on Hand");}		
	}

	

	public void validateNoOfPrintUpdate(){
	
			waitUntilElementVisibleBy(driver, pdfPage, 240);
			if(verifyObjectDisplayed(pdfPage))
			{
				tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.PASS, "PDF is coming after clicking on Print button");
			}
			else
			{
				tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.FAIL, "PDF is not coming after clicking on Print button");
			}
			driver.close();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(windows.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			JavascriptExecutor je = (JavascriptExecutor) driver;					 									 

			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));					 								 

			je.executeScript("arguments[0].scrollIntoView(true);",element);
			waitUntilElementVisibleBy(driver, noOfPrintAfter, 120);
			getFutureNoOfPrints = driver.findElement(noOfPrintAfter).getText().trim();
			System.out.println(getFutureNoOfPrints);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getFutureModifiedByTimestamp = driver.findElement(lstModifiedDate).getText().trim();
			System.out.println(getFutureModifiedByTimestamp);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if(!getFutureNoOfPrints.equals(getCurrentNoOfPrints)){
				System.out.println("Pass");
				tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.PASS, "validated sucessfully the updating Number of Prints record :-" +getFutureNoOfPrints );
			}
			else{
				System.out.println("Fail");
			}		
	}

	public void validateNoOfPrints(){		
			waitUntilElementVisibleBy(driver, pdfPage, 240);
			if(verifyObjectDisplayed(pdfPage))
			{
				tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.PASS, "PDF is coming after clicking on Print button");
			}
			else
			{
				tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.FAIL, "PDF is not coming after clicking on Print button");
			}
			driver.close();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(windows.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));		
	}

	


	public void validateLastModifiedBy(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String st[]=getFutureModifiedByTimestamp.split("\\s+");
		log.info("the date is :"+st[0]);		
		if(verifyObjectDisplayed(lastModifiedUser))	
		{
			String val = driver.findElement(lastModifiedUser).getText().trim();
			tcConfig.updateTestReporter("VouchersPage", "validateLastModifiedBy", Status.PASS, "Last modified By User is sucessfully validated :-" +val);
			if(verifyObjectDisplayed(lstModifiedDate))	{
				tcConfig.updateTestReporter("VouchersPage", "validateLastModifiedBy", Status.PASS, "Last modified By Date  is sucessfully validated :-" +st[0]);
			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateLastModifiedBy", Status.FAIL, "Failed while validating Date");
			}								
		} 
		else{
			tcConfig.updateTestReporter("VouchersPage", "validateLastModifiedBy", Status.FAIL, "Failed while validating User name");
		}
	}						





	
	

	/*This method was modified after SPrint changes
	 * */
	public void validateRegenratedVoucher() throws ParseException{
	
				waitUntilElementVisibleBy(driver, btnEdit, 120);
				if(driver.findElement(txtVoucherType).getText().equals("Wyndham_Vouchers") || driver.findElement(txtVoucherType).getText().equals("Vendor_Vouchers")){
				tcConfig.updateTestReporter("VouchersPage", "validateRegenratedVoucher", Status.PASS, "Voucher page is displayed");
				JavascriptExecutor je = (JavascriptExecutor) driver;					 									 									 
				WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));					 								 									 
				je.executeScript("arguments[0].scrollIntoView(true);",element);
				waitUntilElementVisibleBy(driver, txtCreatedBy, 120);				
				String dates = driver.findElement(txtCreatedBy).getText().trim();
				String st1[]=dates.split("\\s+");
				log.info("the date is :"+st1[0]);						 
				Date date=new SimpleDateFormat("MM/dd/yyyy").parse(st1[0]);
				System.out.println(dates);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String pattern = "MM/dd/YYYY";
				SimpleDateFormat simpleDateFormat =    new SimpleDateFormat(pattern);			     
				Date date1 = new Date();
				String curdate = simpleDateFormat.format(date1);
				Date date2=new SimpleDateFormat("MM/dd/yyyy").parse(curdate);
				System.out.println(curdate);
				waitForSometime(tcConfig.getConfig().get("LowWait"));				
				long diff = date2.getTime()-date.getTime();
				long i = diff / 1000 / 60 / 60 / 24;
				System.out.println(i);		        		       
				int j = (int) i;
				System.out.println ("Days: " + j);
				if(j>14){
					tcConfig.updateTestReporter("TourRecordsPage", "validateRegenratedVoucher", Status.PASS, "Validated : Voucher Issued date should be more than 14 days than system date and days are :- " +j);
				}
				else{
					tcConfig.updateTestReporter("TourRecordsPage", "validateRegenratedVoucher", Status.FAIL, "Failed while validating Issued date");
				}	
				getCurrentNoOfPrints = driver.findElement(noOfPrintsBefore).getText().trim();
				int print = Integer.parseInt(getCurrentNoOfPrints);
				System.out.println(print);
				if(print>=1){
					waitForSometime(tcConfig.getConfig().get("LowWait"));										
					tcConfig.updateTestReporter("VouchersPage", "validateRegenratedVoucher", Status.PASS, "Number of prints before :" +print);	
				}
				else{
					tcConfig.updateTestReporter("VouchersPage", "validateRegenratedVoucher", Status.FAIL, "Failed while validating number of prints");
				}
				waitUntilElementVisibleBy(driver, txtCustomerNmBeforeEdit, 120);
				String beforeEditFN = driver.findElement(txtCustomerNmBeforeEdit).getText();	
				String beforeEditLN = driver.findElement(txtCusLastName).getText();
				waitUntilElementVisibleBy(driver, btnEdit, 120);
				clickElementJS(btnEdit);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement custInfo = driver.findElement(btnCustoInfo);					 								 				 
				je.executeScript("arguments[0].scrollIntoView(true);",custInfo);
				
				driver.findElement(txtCustFirstName).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				String firstName= getRandomStringText(4);
				driver.findElement(txtCustFirstName).sendKeys(firstName);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(txtCustLastName).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				String lastName= getRandomStringText(4);
				driver.findElement(txtCustLastName).sendKeys(lastName);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(btnSaveEditVoucher);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				if(verifyObjectDisplayed(txtCustomerNmBeforeEdit) && verifyObjectDisplayed(txtCusLastName)) 
				{
					String val = driver.findElement(txtCustomerNmBeforeEdit).getText().trim();	
					String ln = driver.findElement(txtCusLastName).getText().trim();
					tcConfig.updateTestReporter("VouchersPage", "validateRegenratedVoucher", Status.PASS,"Previous First Name given as :-" +beforeEditFN+ 
							" and previous Last Name is :-" +beforeEditLN+ " Updated Customer First Name is:- " +val+ " and Last Name updated is :- " +ln);
					waitForSometime(tcConfig.getConfig().get("LowWait"));				        						        				        						        		
				}
				else{tcConfig.updateTestReporter("VouchersPage", "validateRegenratedVoucher", Status.FAIL, "Failed while updating distributed voucher");}
			}	
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateRegenratedVoucher", Status.FAIL, "Failed : error message not generated");
			}
			((JavascriptExecutor) driver).executeScript("scroll(0, -700);");
			String parentElement = driver.getWindowHandle();
			clickElementJS(btnPrint);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(windowHandles.get(1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, pdfPage, 240);
			if(verifyObjectDisplayed(pdfPage))
			{
				tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.PASS, "PDF is coming after clicking on Print button");
			}
			else
			{
				tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.FAIL, "PDF is not coming after clicking on Print button");
			}
			driver.close();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(windows.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));		
	}	



	
	

	public void validatePaymentDetails(){
		
				waitUntilElementVisibleBy(driver, btnEdit, 120);
				if(driver.findElement(txtVoucherType).getText().equals("Wyndham_Vouchers") || driver.findElement(txtVoucherType).getText().equals("Vendor_Vouchers")){
				tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.PASS, "Voucher page is displayed");
				JavascriptExecutor je = (JavascriptExecutor) driver;					 									 
				//Identify the WebElement which will appear after scrolling down					 
				WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));					 								 
				// now execute query which actually will scroll until that element is not appeared on page.					 
				je.executeScript("arguments[0].scrollIntoView(true);",element);
				waitUntilElementVisibleBy(driver, txtTransactionNumber, 120);
				clickElementJS(txtTransactionNumber);
				waitUntilElementVisibleBy(driver, txtPaymentDetails, 120);
				
				clickElementJS(btnEditTransact);
				waitForSometime(tcConfig.getConfig().get("LowWait"));		
				waitUntilElementVisibleBy(driver, txtPaymentDetails_Transact, 120);
				driver.findElement(amtcollect).clear();
				String amount= getRandomString(3);
				driver.findElement(amtcollect).sendKeys(amount);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(paymentMethod).click();	
				waitForSometime(tcConfig.getConfig().get("LowWait"));			
				driver.findElement(container_Payment).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(cardType).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(container_CardType).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(txtCardNo_Edit).clear();
				String card= getRandomString(4);
				driver.findElement(txtCardNo_Edit).sendKeys(card);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				clickElementJS(btnEditSave);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if(verifyObjectDisplayed(txtAmountCollected) && verifyObjectDisplayed(txtPaymentMethod) && verifyObjectDisplayed(txtCardType) && verifyObjectDisplayed(txtLast4ofCard)) {					
					tcConfig.updateTestReporter("VouchersPage", "validatePaymentDetails", Status.PASS, "Amount displayed is :" +txtAmountCollected+ 
							" and the payment method is : " +txtPaymentMethod+ " and card type is : " +txtCardType+ " and last 4 digit of card :" +txtLast4ofCard);					
				}
				else{tcConfig.updateTestReporter("VouchersPage", "validatePaymentDetails", Status.FAIL, "Failed while validating Amount collected");}
			}	
			else{
				tcConfig.updateTestReporter("VouchersPage", "validatePaymentDetails", Status.FAIL, "Failed : exception caught");
			}
			waitUntilElementVisibleBy(driver, btnEdit, 120);
			cardNum =  driver.findElement(txtLast4ofCard).getText().trim();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnEditTransact);
			waitForSometime(tcConfig.getConfig().get("LowWait"));		
			waitUntilElementVisibleBy(driver, txtPaymentDetails_Transact, 120);
			driver.findElement(txtCardNo_Edit).clear();
			String amount= getRandomString(4);
			driver.findElement(txtCardNo_Edit).sendKeys(amount);				
			driver.switchTo().activeElement().sendKeys(Keys.TAB);		     
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnEditSave);
			waitUntilElementVisibleBy(driver, txtSucessMsg, 120);		
			if(verifyObjectDisplayed(txtSucessMsg)) {
				
				String val = driver.findElement(txtLast4ofCard).getText().trim();
				if(!cardNum.equals(val)){
					tcConfig.updateTestReporter("VouchersPage", "validatePaymentDetails", Status.PASS, "Previous last 4 digit of card is :- " +cardNum+ " and Modified last 4 digit of card is:- " +val);
				}			
			}
			else{tcConfig.updateTestReporter("VouchersPage", "validatePaymentDetails", Status.FAIL, "Failed while updating Amount collected field");}
		
	}



	


	public void createWyndhamVoucher(){
	
			waitUntilElementVisibleBy(driver, btnNew, 120);
			clickElementJS(btnNew);
			waitUntilElementVisibleBy(driver, btnNext, 120);
			clickElementJS(btnNext);				
			waitUntilElementVisibleBy(driver, txtSalesStores, 120);		
			fieldDataEnter(txtSalesStores, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(salesStore);		
			waitForSometime(tcConfig.getConfig().get("LowWait"));			
			waitUntilElementVisibleBy(driver, btnProductInformation, 120);	
			fieldDataEnter(productTab_Blank, testData.get("Product"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));		 
			List<WebElement> edit =driver.findElements(SSP);
			System.out.println(edit.size());
			edit.get(2).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtQuantityOnHand).sendKeys(testData.get("QOH1"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(drpTransactionType);
			waitForSometime(tcConfig.getConfig().get("LowWait"));					
			clickElementJS(vlaueMain);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String vendorConfirm = getRandomString(4);
			driver.findElement(vendorConfirmation).sendKeys(vendorConfirm);
			//driver.findElement(vendorConfirmation).sendKeys("Confirmed");
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Customer Information')]"));					 								 										 
			je.executeScript("arguments[0].scrollIntoView(true);",element);						
			waitUntilElementVisibleBy(driver, txtCustomerFirstName, 120);
			driver.findElement(txtCustomerFirstName).clear();
			String firstName= getRandomStringText(3);
			driver.findElement(txtCustomerFirstName).sendKeys(firstName);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtCustomerLastName, 120);
			driver.findElement(txtCustomerLastName).clear();
			String lastName= getRandomStringText(3);
			driver.findElement(txtCustomerLastName).sendKeys(lastName);		    	
			driver.switchTo().activeElement().sendKeys(Keys.TAB);		     
			waitForSometime(tcConfig.getConfig().get("LowWait"));		      				
			WebElement agent = driver.findElement(By.xpath("(//span[contains(text(),'Information')])[1]"));					 								 										 
			je.executeScript("arguments[0].scrollIntoView(true);",agent);
			waitUntilElementVisibleBy(driver, bookingAgent, 120);
			
			clickElementJS(btnSaveVoucherCreation);					 
			waitUntilElementVisibleBy(driver, sucessMsg, 120);
			if(verifyObjectDisplayed(txtDetailsTab)){
				System.out.println("Pass");
				waitUntilElementVisibleBy(driver, vocuherNum, 120);		
				String voucherNum = driver.findElement(vocuherNum).getText().trim();
				tcConfig.updateTestReporter("VouchersPage", "createWyndhamVoucher", Status.PASS, "Vocuher created sucessfully : " +voucherNum);					       
			}
			else{
				System.out.println("Fail");
				tcConfig.updateTestReporter("VouchersPage", "createWyndhamVoucher", Status.FAIL, "Failed while fetching details");
			}					 
								
	}
	
	public void createWyndhamVoucher_Modified() {
	
			waitUntilElementVisibleBy(driver, btnNew, 120);
			clickElementJS(btnNew);
			waitUntilElementVisibleBy(driver, btnNext, 120);
			clickElementJS(btnNext);				
			waitUntilElementVisibleBy(driver, txtSalesStores, 120);		
			fieldDataEnter(txtSalesStores, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(salesStore);		
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if(verifyProduct_Wyn())
			{
				System.out.println("Pass");
			}
			else
			{
				System.out.println("Fail");
			}
			waitUntilElementVisibleBy(driver, btnProductInformation, 120);
			driver.findElement(del_Icon).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));		
			driver.findElement(productTab_Blank).clear();
			fieldDataEnter(productTab_Blank, testData.get("Product"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));		 
			List<WebElement> edit =driver.findElements(SSP);
			System.out.println(edit.size());
			edit.get(2).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(txtQQH).clear();
			driver.findElement(txtQQH).sendKeys("4");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));		      				
			WebElement agent = driver.findElement(By.xpath("(//span[contains(text(),'Information')])[1]"));					 								 										 
			je.executeScript("arguments[0].scrollIntoView(true);",agent);
			waitUntilElementVisibleBy(driver, bookingAgent, 120);
			if(verifyObjectDisplayed(bookingAgent)){	
				clickElementJS(bookingAgent);
				driver.findElement(bookingAgent).sendKeys("WVO Corporate Super User");
				List<WebElement> saleStore1 =driver.findElements(container);
				System.out.println(saleStore1.size());						
				saleStore1.get(1).click();				
				clickElementJS(btnSaveVoucherCreation);					 
				waitUntilElementVisibleBy(driver, sucessMsg, 120);
				if(verifyObjectDisplayed(sucessMsg)){
					waitUntilElementVisibleBy(driver, txtDetailsTab, 120);
					String voucherNum = driver.findElement(vocuherNum).getText().trim();
					System.out.println("Pass");
					tcConfig.updateTestReporter("VouchersPage", "createWyndhamVoucher_Modified", Status.PASS, "Vocuher created sucessfully :-" +voucherNum);					       
				}
				else{
					System.out.println("Fail");
					tcConfig.updateTestReporter("VouchersPage", "createWyndhamVoucher_Modified", Status.FAIL, "Failed while fetching details");
				}					 

			}					 
			else{					 
				tcConfig.updateTestReporter("VouchersPage", "createWyndhamVoucher_Modified", Status.FAIL, "Failed while fetching details");
			}													
	}

	

	public boolean verifyProduct_Wyn()
	{
		boolean flag=false;
		
			if(verifyObjectDisplayed(btnProductInformation)){
				waitUntilElementVisibleBy(driver, btnProductInformation, 120);					

				if(verifyObjectDisplayed(productTab)){
					driver.findElement(productTab_Blank).sendKeys(testData.get("Product1"));  
					driver.switchTo().activeElement().sendKeys(Keys.TAB);					
					driver.findElement(txtQuantityOnHand).sendKeys(testData.get("QOH1"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementJS(wyn_calender);	
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement ele=driver.findElement(By.xpath("//button[contains(text(),'Today')]")); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Actions act=new Actions(driver);
					act.moveToElement(ele).click().build().perform(); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementJS(drpTransactionType);
					waitForSometime(tcConfig.getConfig().get("LowWait"));					
					clickElementJS(vlaueMain);
					waitForSometime(tcConfig.getConfig().get("LowWait"));	
				
					WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Customer Information')]"));					 								 										 
					je.executeScript("arguments[0].scrollIntoView(true);",element);						
					waitUntilElementVisibleBy(driver, txtCustomerFirstName, 120);
					String firstName= getRandomStringText(3);
					driver.findElement(txtCustomerFirstName).sendKeys(firstName);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, txtCustomerLastName, 120);
					String lastName= getRandomStringText(3);
					driver.findElement(txtCustomerLastName).sendKeys(lastName);		    	
					driver.switchTo().activeElement().sendKeys(Keys.TAB);		     
					waitForSometime(tcConfig.getConfig().get("LowWait"));		      				


					clickElementJS(btnSaveVoucherCreation);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if(verifyObjectDisplayed(errorMsg_Wyn)){
						System.out.println("Pass");
						tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.PASS, "Eror msg displyed sucessfully");					       
					}
					else{
						System.out.println("Fail");
						tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.FAIL, "Failed while fetching details");
					}					 				
					flag=true;
				}
				
				if(verifyObjectDisplayed(productTab)){
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(productTab_Blank).clear();
					driver.findElement(productTab_Blank).sendKeys(testData.get("Product2"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(update_Product).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					driver.findElement(txtQuantityOnHand).clear();
					driver.findElement(txtQuantityOnHand).sendKeys(testData.get("QOH2"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(btnSaveVoucherCreation);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if(verifyObjectDisplayed(Qoh_ErrorMsg)){	
						String val = driver.findElement(Qoh_ErrorMsg).getText().trim();
						System.out.println("Pass");
						tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.PASS, "Eror msg displyed sucessfully" +val );					       
					}
					else{
						System.out.println("Fail");
						tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.FAIL, "Failed while fetching details");
					}
					flag=true;					
				}
				else{
					tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.FAIL, "Failed while changing Data");
				}
			}
		return flag;
	}


	


	public void validateChildTransactionNumber(){
		
				waitUntilElementVisibleBy(driver, QOP_AddActivity, 120);
				driver.findElement(QOP_AddActivity).clear();
				driver.findElement(QOP_AddActivity).sendKeys(testData.get("QOH1"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				WebElement element = driver.findElement(By.xpath("//button[@name='save']"));					 								 										 
				je.executeScript("arguments[0].scrollIntoView(true);",element);
				waitForSometime(tcConfig.getConfig().get("LowWait"));								
				Actions save=new Actions(driver);
				save.moveToElement(element).click().build().perform(); 
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				waitUntilElementVisibleBy(driver, lnkTransactionNo, 120);
				clickElementJS(lnkTransactionNo);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(windowHandles.get(1));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, txtPaymentDetails, 120);
				String actual = driver.findElement(txtTransactNoPage).getText().trim();
				System.out.println(actual);
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
				clickElementJS(txtVocuhers);
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
				List<WebElement> records = driver.findElements(lst_QOP);
				System.out.println("Number of elements:" +records.size());
				for (int i=0; i<=records.size()-1;i++){
					if((records.get(i).getText()) != null){
					tcConfig.updateTestReporter("VouchersPage", "validateChildTransactionNumber", Status.PASS, "Total vouchers created for the related transaction :-"+records.get(i).getText());
				}
				}
				driver.close();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(windows.get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			
	}
	
	
	public void validateTransactionDetails(){
		
			List<WebElement> elements = driver.findElements(lst_QoP);
			System.out.println("Number of elements in Qunatity on Product:" +elements.size());
			for (int i=0; i<=elements.size()-1;i++){
				System.out.println("Qunatity on Product : " + elements.get(i).getText());
				if((elements.get(i).getText()) != null){					
					tcConfig.updateTestReporter("VouchersPage", "validateTransactionDetails", Status.PASS, "Qunatity on Product are displayed "+elements.get(i).getText());
				}		  			  
			}				
}
	

	

	public void validateTransactionReceipt(){
				
			waitUntilElementVisibleBy(driver, btnReceipt, 120);
			if(verifyObjectDisplayed(btnReceipt))
			{
				clickElementJS(btnReceipt);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(windowHandles.get(1));			 		
				waitUntilElementVisibleBy(driver, pdfPage, 240);
				if(verifyObjectDisplayed(pdfPage))
				{
					tcConfig.updateTestReporter("VouchersPage", "validateTransactionReceipt", Status.PASS, "PDF is coming after clicking on Receipt button");
				}
				else
				{
					tcConfig.updateTestReporter("VouchersPage", "validateTransactionReceipt", Status.FAIL, "PDF is not coming after clicking on Receipt button");
				}
				driver.close();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(windows.get(0));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
			else
			{
				tcConfig.updateTestReporter("VouchersPage", "validateTransactionReceipt", Status.FAIL, "Failed while reloading to main page");
			}
		
	}
	
	public void validateTransactionReceipt_IHMarketer(){
					
				waitUntilElementVisibleBy(driver, btnReceipt, 120);
				if(verifyObjectDisplayed(btnReceipt))
				{
					clickElementJS(btnReceipt);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(windowHandles.get(2));			 		
					waitUntilElementVisibleBy(driver, pdfPage, 240);
					if(verifyObjectDisplayed(pdfPage))
					{
						tcConfig.updateTestReporter("VouchersPage", "validateTransactionReceipt", Status.PASS, "PDF is coming after clicking on Receipt button");
					}
					else
					{
						tcConfig.updateTestReporter("VouchersPage", "validateTransactionReceipt", Status.FAIL, "PDF is not coming after clicking on Receipt button");
					}
					driver.close();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().window(windows.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
				else
				{
					tcConfig.updateTestReporter("VouchersPage", "validateTransactionReceipt", Status.FAIL, "Failed while reloading to main page");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("MedWait"));				
			
		}

	
	public void returnToVoucher(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().back();
		waitForSometime(tcConfig.getConfig().get("MedWait"));	
	}
	public void returnToVoucher_IhMarketer(){
		
		driver.close();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windows.get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
	}
	
	
	


	public void validateSalesStorePhone(){
		waitUntilElementVisibleBy(driver, txtSalesStorePhone, 120);

		if(verifyObjectDisplayed(txtSalesStorePhone)){
			tcConfig.updateTestReporter("VouchersPage", "validateSalesStorePhone", Status.PASS, "Sales store Phone text validated sucessfully");
		}
		else{
			tcConfig.updateTestReporter("VouchersPage", "validateSalesStorePhone", Status.FAIL, "Failed while validating Sales Store Phone");
		}
	}

	
	

	public void createVendorVoucher(){
	
			waitUntilElementVisibleBy(driver, txtRecentlyViewed, 120);
			clickElementJS(txtRecentlyViewed);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if(verifyObjectDisplayed(txtSerachVendor)){ 
			//clickElementJS(txtSerachVendor);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(txtSerachVendor, testData.get("VendorVoucher"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			List<WebElement> ceasars =driver.findElements(lst_Ceasers);
			System.out.println(ceasars.size()); 
			for(int i=0; i<=(ceasars.size()-1);i++){
			if(ceasars.get(i).getText().contains("Unassigned")){ 
			ceasars.get(i).click();
			tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.PASS, "Caesars Vouchers - Unassigned clicked sucessfully ");
			break;
			}
			else{ 
			tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.FAIL, " Failed : Caesars Vouchers - Unassigned not clicked");
			}
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, localVendorSearch, 120);
			fieldDataEnter(localVendorSearch, testData.get("VoucherNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(localVendorSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait")); 
			List<WebElement> edit =driver.findElements(listShowMore);
			System.out.println(edit.size());
			edit.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnEditlst);
			waitForSometime(tcConfig.getConfig().get("LowWait")); 
			clickElementJS(drpTransactionType);
			waitForSometime(tcConfig.getConfig().get("LowWait")); 
			clickElementJS(vlaueMain);
			waitForSometime(tcConfig.getConfig().get("LowWait")); 
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Customer Information')]")); 
			je.executeScript("arguments[0].scrollIntoView(true);",element); 
			waitUntilElementVisibleBy(driver, txtCustomerFirstName, 120);
			String firstName= getRandomStringText(3);
			driver.findElement(txtCustomerFirstName).sendKeys(firstName);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtCustomerLastName, 120);
			String lastName= getRandomStringText(3);
			driver.findElement(txtCustomerLastName).sendKeys(lastName);     
			driver.switchTo().activeElement().sendKeys(Keys.TAB);     
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			clickElementJS(btnSaveVendor);   			
			waitUntilElementVisibleBy(driver, sucessMsg, 120);
			if(verifyObjectDisplayed(sucessMsg)){
	
			tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.PASS, " Vendor Voucher created sucessfully");
			}
			else{
	
			tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.FAIL, "Failed while creating vendor Voucher");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, localVendorSearch, 120);
			fieldDataEnter(localVendorSearch, testData.get("VoucherNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(localVendorSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(lnkVoucherNo);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtDetailsTab, 120);
			prevVendorID = driver.findElement(voucher1).getText().trim();
			System.out.println(prevVendorID);        
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String val= driver.findElement(vendorVoucherNum).getText().trim();
			tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.PASS, "Vendor Voucher number is displayed :-" +val);
	
			} 
			else{tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.FAIL, "Failed while fetching details");}
		
}


	
	
	
	
	public void createVendorVoucher_Modified(){
	
				waitUntilElementVisibleBy(driver, txtRecentlyViewed, 120);
				clickElementJS(txtRecentlyViewed);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if(verifyObjectDisplayed(txtSerachVendor)){ 
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(txtSerachVendor, testData.get("VendorVoucher"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				List<WebElement> ceasars =driver.findElements(lst_Ceasers);
				System.out.println(ceasars.size()); 
				for(int i=0; i<=(ceasars.size()-1);i++){
				if(ceasars.get(i).getText().contains("Unassigned")){ 
				ceasars.get(i).click();
				tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.PASS, "Caesars Vouchers - Unassigned clicked sucessfully ");
				break;
				}
				else{ 
				tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher", Status.FAIL, " Failed : Caesars Vouchers - Unassigned not clicked");
				}
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, localVendorSearch, 120);
				fieldDataEnter(localVendorSearch, testData.get("VoucherNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(localVendorSearch).sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
				prevVendorID = driver.findElement(productDisplay).getText().trim();
				System.out.println(prevVendorID);					        
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> edit =driver.findElements(listShowMore);
				System.out.println(edit.size());
				edit.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(btnEditlst);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				productName = driver.findElement(productName_Vendor).getText().trim();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if(verifyProduct_Vendor())
				{
					System.out.println("Pass");

				}
				else
				{
					System.out.println("Fail");

				}
				
				driver.findElement(productTab_Blank).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(product_Tab).sendKeys(productName);			
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> product =driver.findElements(lst_Product);
				System.out.println(product.size());
				product.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(edit_QOH).clear();
				driver.findElement(edit_QOH).sendKeys("5");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(btnSaveVendor);									     
				waitUntilElementVisibleBy(driver, errorMsg_Wyn, 120);
				if(verifyObjectDisplayed(errorMsg_Vendor)){

					tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher_Modified", Status.PASS, " Data not saved validated sucessfully");
				}
				else{

					tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher_Modified", Status.FAIL, "Failed while creating vendor Voucher");
				}


			}	
			else{tcConfig.updateTestReporter("VouchersPage", "createVendorVoucher_Modified", Status.FAIL, "Failed while fetching details");}
		
	}
	public boolean verifyProduct_Vendor()
	{
		boolean flag=false;
		
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				if(verifyObjectDisplayed(btnProductInformation)){
					waitUntilElementVisibleBy(driver, btnProductInformation, 120);					

				if(verifyObjectDisplayed(productTab)){
					if(verifyObjectDisplayed(productTab_Item)){
						List<WebElement> delete =driver.findElements(deleteIcon);
						System.out.println(delete.size());
						delete.get(2).click();
						System.out.println("Pass");
					}
					else{
						System.out.println("Fail");
					}		
					driver.findElement(productTab_Blank).sendKeys(testData.get("Product1"));  
					driver.switchTo().activeElement().sendKeys(Keys.TAB);					
					
					driver.findElement(edit_QOH).clear();
					driver.findElement(edit_QOH).sendKeys(testData.get("QOH1"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
			
					clickElementJS(wyn_calender);	
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement ele=driver.findElement(By.xpath("//button[contains(text(),'Today')]")); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Actions act=new Actions(driver);
					act.moveToElement(ele).click().build().perform(); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementJS(drpTransactionType);
					waitForSometime(tcConfig.getConfig().get("LowWait"));					
					clickElementJS(vlaueMain);
					waitForSometime(tcConfig.getConfig().get("LowWait"));										 
					WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Customer Information')]"));					 								 										 
					je.executeScript("arguments[0].scrollIntoView(true);",element);						
					waitUntilElementVisibleBy(driver, txtCustomerFirstName, 120);
					String firstName= getRandomStringText(3);
					driver.findElement(txtCustomerFirstName).sendKeys(firstName);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, txtCustomerLastName, 120);
					String lastName= getRandomStringText(3);
					driver.findElement(txtCustomerLastName).sendKeys(lastName);				    	
					driver.switchTo().activeElement().sendKeys(Keys.TAB);				     
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					WebElement chk=driver.findElement(By.xpath("//span[contains(text(),'Issued')]/../..//input[@type='checkbox']")); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Actions actChk=new Actions(driver);
					actChk.moveToElement(chk).click().build().perform(); 
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(btnSaveVendor);									     

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if(verifyObjectDisplayed(errorMsg_Vendor)){							
						System.out.println("Pass");
						tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.PASS, "Eror msg displyed sucessfully");					       
					}
					else{
						System.out.println("Fail");
						tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.FAIL, "Failed while fetching details");
					}
					if(verifyObjectDisplayed(productTab)){
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(productTab_Blank).clear();
						driver.findElement(productTab_Blank).sendKeys(testData.get("Product2"));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(edit_QOH).clear();
						driver.findElement(edit_QOH).sendKeys(testData.get("QOH2"));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementJS(edit_save);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if(verifyObjectDisplayed(erroMsg_Vendor)){	
							String val = driver.findElement(erroMsg_Vendor).getText().trim();
							System.out.println("Pass");
							tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.PASS, "Eror msg displyed sucessfully" +val );					       
						}
						else{
							System.out.println("Fail");
							tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.FAIL, "Failed while fetching details");
						}
						flag=true;					
					}
					else{
						tcConfig.updateTestReporter("VouchersPage", "verifyProduct_Wyn", Status.FAIL, "Failed while changing Data");
					}
				}
			}			
		return flag;
	}


	public void vendorReissued(){
		
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtRecentlyViewed, 120);
			clickElementJS(txtRecentlyViewed);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(txtSerachVendor, testData.get("VendorVoucher"));
			
			List<WebElement> ceasars =driver.findElements(lst_Ceasers);
			System.out.println(ceasars.size());					
			for(int i=0; i<=(ceasars.size()-1);i++){
				if(ceasars.get(i).getText().contains("Unassigned")){					 
					ceasars.get(i).click();
					tcConfig.updateTestReporter("VouchersPage", "vendorReissued", Status.PASS, "Caesars Vouchers - Unassigned clicked sucessfully ");
					break;
				}
				else{					 
					tcConfig.updateTestReporter("VouchersPage", "vendorReissued", Status.FAIL, " Failed : Caesars Vouchers - Unassigned not clicked");
				}
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, localVendorSearch, 120);
			fieldDataEnter(localVendorSearch, testData.get("VoucherNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(localVendorSearch).sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> edit =driver.findElements(listShowMore);
			System.out.println(edit.size());
			edit.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnEditlst);
			waitForSometime(tcConfig.getConfig().get("LowWait"));				 
			clickElementJS(drpTransactionType);
			waitForSometime(tcConfig.getConfig().get("LowWait"));					
			clickElementJS(vlaueMain);
			waitForSometime(tcConfig.getConfig().get("LowWait"));										 
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Customer Information')]"));					 								 										 
			je.executeScript("arguments[0].scrollIntoView(true);",element);						
			waitUntilElementVisibleBy(driver, txtCustomerFirstName, 120);
			String firstName= getRandomStringText(3);
			driver.findElement(txtCustomerFirstName).sendKeys(firstName);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtCustomerLastName, 120);
			String lastName= getRandomStringText(3);
			driver.findElement(txtCustomerLastName).sendKeys(lastName);				    	
			driver.switchTo().activeElement().sendKeys(Keys.TAB);				     
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement chk=driver.findElement(By.xpath("//span[contains(text(),'Issued')]/../..//input[@type='checkbox']")); 
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Actions actChk=new Actions(driver);
			actChk.moveToElement(chk).click().build().perform(); 
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnSaveVendor);									     
			waitUntilElementVisibleBy(driver, sucessMsg, 120);
			if(verifyObjectDisplayed(sucessMsg)){

				tcConfig.updateTestReporter("VouchersPage", "vendorReissued", Status.PASS, " Vendor Voucher created sucessfully");
			}
			else{

				tcConfig.updateTestReporter("VouchersPage", "verifyVendorReissued", Status.FAIL, "Failed while creating vendor Voucher");
			}

				 
	}

	

	public boolean validateNewVendorCreated(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().back();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, localVendorSearch, 120);
		fieldDataEnter(localVendorSearch, testData.get("VoucherNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(localVendorSearch).sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnNew, 120);
		List<WebElement> select =driver.findElements(lstVocuher);
		System.out.println(select.size());		
		boolean flag=false;
		for(int i=0; i<=(select.size()-1);i++){
			if(select.get(i).getText().contains("A")){
				flag=true;
				tcConfig.updateTestReporter("VouchersPage", "validateNewVendorCreated", Status.PASS, " Newly Vendor Voucher created sucessfully : -" +select.get(i).getText());
			}
			else{

				flag=false;
				tcConfig.updateTestReporter("VouchersPage", "validateNewVendorCreated", Status.FAIL, "Failed while creating vendor Voucher");
			}
		}
		return flag;		
	}

	
			

	public void createSampleVoucher(){
	
			waitUntilElementVisibleBy(driver, btnNew, 120);
			clickElementJS(btnNew);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> select =driver.findElements(radioBtn);
			System.out.println(select.size());
			select.get(1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnNext, 120);
			clickElementJS(btnNext);
			waitUntilElementVisibleBy(driver, txtSalesStores, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtSalesStores, 120);		
			fieldDataEnter(txtSalesStores, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			//clickElementJS(salesStore);	
			List<WebElement> salestore =driver.findElements(lstSample);
			System.out.println(salestore.size());
			salestore.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			//waitUntilElementVisibleBy(driver, btnProductInformation, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(productTab_Blank, testData.get("Product"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));		 
			List<WebElement> edit =driver.findElements(SSP);
			System.out.println(edit.size());
			edit.get(2).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtQuantityOnHand).sendKeys("4");			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnSaveVendor);	
			//waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, sucessMsg, 120);
			if(verifyObjectDisplayed(sucessMsg)){
				System.out.println("Pass");
				String val = driver.findElement(sampleVoucherNo).getText().trim();
				tcConfig.updateTestReporter("VouchersPage", "createSampleVoucher", Status.PASS, " Sample Voucher created sucessfully " +val);
			}
			else{
				System.out.println("Fail");
				tcConfig.updateTestReporter("VouchersPage", "createSampleVoucher", Status.FAIL, "Failed while creating Sample Voucher");
			}		
	}

	

	public boolean validateParentSalestoreWyndham(){	
			waitUntilElementVisibleBy(driver, btnNew, 120);
			clickElementJS(btnNew);
			waitUntilElementVisibleBy(driver, btnNext, 120);
			clickElementJS(btnNext);
			waitUntilElementVisibleBy(driver, txtSalesStores, 120);
			clickElementJS(txtSalesStores);			
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> saleStore =driver.findElements(lstSaleStore);
			System.out.println(saleStore.size());	
			boolean flag=false;
			for(int i=0; i<=(saleStore.size()-1);i++){
				if(saleStore.get(i).getText().contains("Parent")){					 
					flag=true;
					tcConfig.updateTestReporter("VouchersPage", "validateParentSalestoreWyndham", Status.PASS, "Validated sucessfully Sales Store having Parent ");
				}
				else{					 
					flag=false;
				}
			}
		
		return false;			 			
	}



	


	public void validateParentSalestoreVendor(){
		boolean flag=false;
			waitUntilElementVisibleBy(driver, btnEdit, 120);
			clickElementJS(btnEdit);
			waitUntilElementVisibleBy(driver, txtSalesStore, 120);
			clickElementJS(slaesstoreclick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> saleStore =driver.findElements(lstSaleStore);
			System.out.println(saleStore.size());	
			
			for(int i=0; i<=(saleStore.size()-1);i++){
				if(saleStore.get(i).getText().contains("Parent")){					 
					flag=true;
					tcConfig.updateTestReporter("VouchersPage", "createSampleVoucher", Status.PASS, "Validated sucessfully Sales Store having Parent ");
				}
				else{					 
					flag=false;
				}
			}
		
	}

	

	public void validateBookingAgent_Wyndham(){

	
			waitUntilElementVisibleBy(driver, txtSalesStores, 120);		
			fieldDataEnter(txtSalesStores, testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(salesStore);		
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, btnProductInformation, 120);					
			fieldDataEnter(productTab_Blank, testData.get("Product"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));		 
			List<WebElement> edit =driver.findElements(SSP);
			System.out.println(edit.size());
			edit.get(2).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtQuantityOnHand).sendKeys("4");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(drpTransactionType);
			waitForSometime(tcConfig.getConfig().get("LowWait"));					
			clickElementJS(vlaueMain);
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
			driver.findElement(vendorConfirmation).sendKeys(testData.get("QOH1"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Customer Information')]"));					 								 										 
			je.executeScript("arguments[0].scrollIntoView(true);",element);						
			waitUntilElementVisibleBy(driver, txtCustomerFirstName, 120);
			String firstName= getRandomStringText(3);
			driver.findElement(txtCustomerFirstName).sendKeys(firstName);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtCustomerLastName, 120);
			String lastName= getRandomStringText(3);
			driver.findElement(txtCustomerLastName).sendKeys(lastName);		    	
			//driver.switchTo().activeElement().sendKeys(Keys.TAB);		     
			waitForSometime(tcConfig.getConfig().get("LowWait"));		      				
			WebElement agent = driver.findElement(By.xpath("(//span[contains(text(),'Information')])[1]"));					 								 										 
			je.executeScript("arguments[0].scrollIntoView(true);",agent);
			waitUntilElementVisibleBy(driver, bookingAgent, 120);
			if(verifyObjectDisplayed(bookingAgent)){	
				clickElementJS(bookingAgent);
				driver.findElement(bookingAgent).sendKeys(testData.get("BookingAgent"));
				List<WebElement> saleStore1 =driver.findElements(container);
				System.out.println(saleStore1.size());						
				saleStore1.get(0).click();				
				clickElementJS(btnSaveVoucherCreation);					 
				waitUntilElementVisibleBy(driver, sucessMsg, 120);
				if(verifyObjectDisplayed(sucessMsg)){
					System.out.println("Pass");
					tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent", Status.PASS, "Vocuher created sucessfully");					       
				}
				else{
					System.out.println("Fail");
					tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent", Status.FAIL, "Failed while fetching details");
				}					 

			}					 
			else{					 
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent", Status.FAIL, "Failed while fetching details");
			}					 			 				 				 			 
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnEdit);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> delete =driver.findElements(deleteIcon);
			System.out.println(delete.size());
			delete.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
			driver.findElement(bookingAgent).sendKeys(testData.get("SalesStorePremium"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> saleStore =driver.findElements(container_Update);
			System.out.println(saleStore.size());	
			saleStore.get(0).click();							
			clickElementJS(btnEditSave);		
			waitUntilElementVisibleBy(driver, errorMsgVendor, 120);
			if(verifyObjectDisplayed(errorMsgVendor)){	
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent", Status.PASS, "Error displayed while chnaging slaes store");

			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent", Status.FAIL, "Failed while updating Booking Agent");
			}							 						 					 
			clickElementJS(btnEditCancel);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			 		
	}


    
	
	public void validateVendorConfirmation(){
		WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Activity Details')]"));
		je.executeScript("arguments[0].scrollIntoView(true);",element);
		waitUntilElementVisibleBy(driver, txtVendorConfirm, 120);	
		if(verifyObjectDisplayed(txtVendorConfirm)){
			String val = driver.findElement(txtVendorConfirm).getText().trim();
			tcConfig.updateTestReporter("TourRecordsPage", "validateCompareDates", Status.PASS, "Vendor Confirmation validated sucessfully: " +val);
		}
		else{
			tcConfig.updateTestReporter("TourRecordsPage", "validateCompareDates", Status.FAIL, "Failed while validating Vendor Confirmation" );
		}
	}

	



	public void validateBookingAgent_Vendor(){
	
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(btnEdit);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> delete =driver.findElements(deleteIcon);
			System.out.println(delete.size());
			delete.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));		 
			driver.findElement(bookingAgent).sendKeys(testData.get("Product1"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> bookagt =driver.findElements(container_Update);
			System.out.println(bookagt.size());	
			bookagt.get(0).click();	
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> delStore =driver.findElements(deleteIcon);
			System.out.println(delStore.size());
			delStore.get(1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));		 
			driver.findElement(clickSalestore_Vendor).sendKeys(testData.get("SalesStore"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> saleStore  =driver.findElements(chngSalesStore);
			System.out.println(saleStore.size());	
			saleStore.get(0).click();	
			waitForSometime(tcConfig.getConfig().get("LowWait"));		 
			clickElementJS(btnEditSave);
			waitUntilElementVisibleBy(driver, errorMsgVendor, 120);
			if(verifyObjectDisplayed(errorMsgVendor)){									
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent_Vendor", Status.PASS, "Error message displayed while changing Booking Agent");
				waitUntilElementVisibleBy(driver, txtErrorMsg, 120);
				if(verifyObjectDisplayed(txtErrorMsg)){
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> deleteAgent =driver.findElements(deleteIcon);
					System.out.println(deleteAgent.size());
					deleteAgent.get(0).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(bookingAgent).sendKeys(testData.get("BookingAgent"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> chng =driver.findElements(passCase);
					System.out.println(chng.size());	
					chng.get(0).click();	
				}						
			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent_Vendor", Status.FAIL, "Failed while updating Booking Agent");
			}									
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
			clickElementJS(btnEditSave);
			waitUntilElementVisibleBy(driver, txtSucessMsg, 120);
			if(verifyObjectDisplayed(txtSucessMsg)){	
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent_Vendor", Status.PASS, "Voucher saved sucessfully");
			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent_Vendor", Status.FAIL, "Failed while updating Voucher");
			}		   
	}

	public void validateVendorReturnResue(){
	
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));
			je.executeScript("arguments[0].scrollIntoView(true);",element);
			waitUntilElementVisibleBy(driver, btnEditReturned, 120);
			if(verifyObjectDisplayed(btnEditReturned))
			{
				clickElementJS(btnEditReturned);
				waitForSometime(tcConfig.getConfig().get("LowWait"));										
				clickElementJS(chkReturned);
				waitForSometime(tcConfig.getConfig().get("LowWait"));		
				if(driver.findElement(drpReturnReason).isEnabled()){
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					//waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJS(drpReturnReason);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(value);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(btnSave);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if(driver.findElement(chkReturn).getAttribute("className").equals("slds-checkbox_faux")){				        						      				        		
						if(driver.findElement(lastModifiedBy).isDisplayed() && driver.findElement(returnDate).isDisplayed()){				        								        		
							
						
						tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.PASS, "Return checkbox is checked sucessfully and Last modified is displayed");
					}
					else{
						tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.FAIL, "Faile dwhile validating :Return checkbox is checked sucessfully and Last modified is displayed");			        				        		
					}
				}
				else{tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.FAIL, "Failed while validating Return Reason");}				        						
			}
			else{tcConfig.updateTestReporter("VouchersPage", "validateReturnVoucherWyndham", Status.FAIL, "Failed while validating Return checkbox");}
			}
	}

	



	public void validatePaidStatus(){
		

			waitUntilElementVisibleBy(driver, btnEdit, 120);
			if(verifyObjectDisplayed(btnEdit)){
				clickElementJS(btnEdit);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement element = driver.findElement(By.xpath("(//span[contains(text(),'Voucher Status')])[2]"));
				je.executeScript("arguments[0].scrollIntoView(true);",element);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement chk=driver.findElement(By.xpath("(//span[contains(text(),'Paid')]/../..//input[@type='checkbox'])[2]")); 
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Actions actChk=new Actions(driver);
				actChk.moveToElement(chk).click().build().perform(); 
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, txtInvoiceID, 120);
				String invoiceID= getRandomString(4);
				driver.findElement(txtInvoiceID).sendKeys(invoiceID);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(btnSaveEdit);									     
				waitUntilElementVisibleBy(driver, txtSucessMsg, 120);
				if(verifyObjectDisplayed(txtSucessMsg)){				
					tcConfig.updateTestReporter("VouchersPage", "validatePaidStatus", Status.PASS, "Voucher paid checked sucessfully and invoice generated");
				}
				else{
					tcConfig.updateTestReporter("VouchersPage", "validatePaidStatus", Status.FAIL, "Failed while updating  Voucher");
				}			
			}
			else{			
				tcConfig.updateTestReporter("VouchersPage", "validatePaidStatus", Status.FAIL, "Failed : Edit button not displayed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if(testData.get("Paid").equalsIgnoreCase("Y"))
			{
				clickElementBy(btnEdit_Paid);
				waitForSometime(tcConfig.getConfig().get("LowWait"));		
				((JavascriptExecutor) driver).executeScript("window.scrollBy(0,300)", "");
				List<WebElement> eltsChecked = driver.findElements(chkPaid_Edit);
				boolean isCheched = eltsChecked.size() > 0;
				if (isCheched) {
					// uncheck the checkbox
					eltsChecked.get(0).click();
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(txtInvoiceID_Edit).clear();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(txtInvoiceID_Edit).sendKeys("1234");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(btnSaveEdit);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("VouchersPage", "validatePaidStatus", Status.PASS, "Voucher paid unchecked  and invoice edited successfully");
			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validatePaidStatus", Status.FAIL, "Failed while editing Voucher paid  and invoice ");
			}		
	}	
	



	public void validateIssuedDate() throws ParseException{	
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));
			je.executeScript("arguments[0].scrollIntoView(true);",element);	
			waitForSometime(tcConfig.getConfig().get("LowWait"));			
			if(driver.findElement(chk).getAttribute("contentEditable").contains("inherit")){
				tcConfig.updateTestReporter("VouchersPage", "validateIssuedDate", Status.PASS, "Issued checkbox is checked validated sucessfully");
			}
			else{				
				tcConfig.updateTestReporter("VouchersPage", "validateIssuedDate", Status.FAIL, "Failed while validating Issued checkbox");
			}			
			waitUntilElementVisibleBy(driver, IssuedDate, 120);		
			//input date
			String issueDates = driver.findElement(IssuedDate).getText().trim();
			System.out.println(issueDates);	
			issueDates = adjustDatePattern(issueDates);
			System.out.println(issueDates);	
			//system date
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String currDateString = dateFormat.format(new Date());
			System.out.println(currDateString);
			//comparison of two dates 
			if(issueDates.equals(currDateString)){
				System.out.println("Date1 is equal Date2");
				tcConfig.updateTestReporter("VouchersPage", "validateIssuedDate", Status.PASS, "Issued date is same as system date : validated sucessfully");
			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateIssuedDate", Status.FAIL, "Failed while validating : Issued date same as system date");
			}		

	}

	

	public void validateVoidVoucher(){
	
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
			if(verifyObjectDisplayed(chkVoid)){
				clickElementJS(chkVoid);
				waitForSometime(tcConfig.getConfig().get("LowWait"));		
				((JavascriptExecutor) driver).executeScript("window.scrollBy(0,300)", "");		
				WebElement chk=driver.findElement(By.xpath("//input[@name='Voided__c']")); 
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Actions actChk=new Actions(driver);
				actChk.moveToElement(chk).click().build().perform(); 
				waitForSometime(tcConfig.getConfig().get("LowWait"));			
				clickElementJS(btnSaveEdit);									     
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
				if(driver.findElement(voidcheckbox).getAttribute("contentEditable").contains("inherit")){
					tcConfig.updateTestReporter("VouchersPage", "validateVoidVoucher", Status.PASS, "Voided checkbox is checked sucessfully");
				}
				else{				
					tcConfig.updateTestReporter("VouchersPage", "validateVoidVoucher", Status.FAIL, "Failed while validating Voided checkbox");
				}		
			}
			else{			
				tcConfig.updateTestReporter("VouchersPage", "validateVoidVoucher", Status.FAIL, "Failed : Edit button not displayed");}
		
	}
	

	public void validateProductDetails(){		
		waitForSometime(tcConfig.getConfig().get("LowWait"));			
		if(verifyObjectDisplayed(txtProduct) & verifyObjectDisplayed(txtQuantity) &
				verifyObjectDisplayed(ttlRetailValue) & verifyObjectDisplayed(ttlCostValue) ){
			String product = driver.findElement(txtProduct).getText().trim();
			String QOH = driver.findElement(txtQuantity).getText().trim();
			String retailValue = driver.findElement(ttlRetailValue).getText().trim();
			String costValue = driver.findElement(ttlCostValue).getText().trim();
			//System.out.println(getNumber(costValue));
			
			tcConfig.updateTestReporter("VouchersPage", "validateProductDetails", Status.PASS, "Voucher Product present is : " +product+ 
					"and Quantity on hand present is :"+QOH+ "Total Retail value present is :"+retailValue+ " and Total Cost value is :"+costValue+ "are present");
	}
	}

	
	
	public void validateTotalQuantity(){		

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			totalQuantity = driver.findElement(txtQuantity).getText().trim();
			System.out.println(totalQuantity);	
			totalReatilValue = driver.findElement(ttlRetailValue).getText().trim();
			String s = totalReatilValue;
			String s1 = s.substring(s.indexOf("$")+1);
			s1.trim();
			System.out.println(s1);	
			totalCost = driver.findElement(ttlCostValue).getText().trim();
			String c = totalCost;
			String c1 = c.substring(s.indexOf("$")+1);
			c1.trim();
			System.out.println(c1);				
			waitForSometime(tcConfig.getConfig().get("LowWait"));		
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));					 								 					 
			je.executeScript("arguments[0].scrollIntoView(true);",element);
			waitUntilElementVisibleBy(driver, txtTransactionNumber, 120);
			clickElementJS(txtTransactionNumber);
			waitUntilElementVisibleBy(driver, txtPaymentDetails, 120);
			if(verifyObjectDisplayed(ttlQuantity) & verifyObjectDisplayed(ttlRetail) & verifyObjectDisplayed(ttlCost)){
				String quantity = driver.findElement(ttlQuantity).getText().trim();
				String retail = driver.findElement(ttlRetail).getText().trim();				
				String cost = driver.findElement(ttlCost).getText().trim();			
				if(totalQuantity.equals(quantity) & s1.equals(retail) & c1.equals(cost)){				
					tcConfig.updateTestReporter("VouchersPage", "validateTotalQuantity", Status.PASS, "Total quanity present is : " +quantity+ 
							"and Grand Retail Value present is :"+retail+ " and Grand Total Cost value present is :"+cost+  "are present");
				}
				else{
					tcConfig.updateTestReporter("VouchersPage", "validateTotalQuantity", Status.FAIL, "Failed while validating : Total Quantity , Grand total Retail and Grand Total Cost");	
				}		
			}	
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateTotalQuantity", Status.FAIL, "Failed while validating :Payment details");	
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("LowWait"));				
	}
	
	
	
	public void validateVendorReturnReason(){

			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Voucher Status')]"));
			je.executeScript("arguments[0].scrollIntoView(true);",element);
			waitUntilElementVisibleBy(driver, btnEditReturned, 120);
			if(verifyObjectDisplayed(btnEditReturned))
			{
				clickElementJS(btnEditReturned);
				waitForSometime(tcConfig.getConfig().get("LowWait"));									
				clickElementJS(chkReturned);
				waitForSometime(tcConfig.getConfig().get("LowWait"));		
				if(driver.findElement(drpReturnReason).isEnabled()){
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(drpReturnReason);
					waitForSometime(tcConfig.getConfig().get("LowWait"));		
					clickElementJS(lst_return);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(btnSave);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if(driver.findElement(returnReason).getAttribute("outerText").equals("Vendor Cancel"))
					{				        						      				      						
						tcConfig.updateTestReporter("VouchersPage", "validateVendorReturnReason", Status.PASS, "Return checkbox is checked sucessfully and Return Reason : Vendor Cancel");
					}
					else{
						tcConfig.updateTestReporter("VouchersPage", "validateVendorReturnReason", Status.FAIL, "Failed while validating :Return checkbox is checked and Return Reason");			        				        		
					}
			}
			else{tcConfig.updateTestReporter("VouchersPage", "validateVendorReturnReason", Status.FAIL, "Failed while validating Return Reason");}				        						
		}
		else{tcConfig.updateTestReporter("VouchersPage", "validateVendorReturnReason", Status.FAIL, "Failed while validating Return checkbox");}
	
}


	
	
	public void validateProductTab(){
	
			waitUntilElementVisibleBy(driver, tab_Products, 120);
			if(verifyObjectDisplayed(tab_Products)){
				clickElementJS(tab_Products);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, searchProduct, 120);			
				fieldDataEnter(searchProduct, testData.get("Product"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(searchProduct).sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("LowWait"));		
				clickElementJS(lnk_Product);			
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
				String before = driver.findElement(txtRedemptionExp).getText().trim();
				System.out.println(before);
				waitUntilElementVisibleBy(driver, btnEdit_Product, 120);			
				clickElementJS(btnEdit_Product);
				waitForSometime(tcConfig.getConfig().get("LowWait"));			
				waitUntilElementVisibleBy(driver, edit_redemptionExp, 120);		
				driver.findElement(edit_redemptionExp).clear();
				String redemp= getRandomStringText(4);
				fieldDataEnter(edit_redemptionExp, redemp);		
				//driver.findElement(edit_redemptionExp).sendKeys(testData.get("RedemptionExpiration"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
//				clickElementJS(btnSaveEdit);
				driver.findElement(btnSaveEdit).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, txtSucessMsg, 120);
				if(verifyObjectDisplayed(txtRedemptionExp)){												
					String after = driver.findElement(txtRedemptionExp).getText().trim();
					redeemptionExp = after;
					System.out.println(after);
					if(!before.equals(after)){
						tcConfig.updateTestReporter("VouchersPage", "validateProductTab", Status.PASS, "Previous Redemption Exipration present is : " +before+ "Redemption Exipration updated sucessfully : " +after);
					}
					else{
						tcConfig.updateTestReporter("VouchersPage", "validateProductTab", Status.FAIL, "Failed while updating Redemption Exipration");
					}
				}
				else{
					tcConfig.updateTestReporter("VouchersPage", "validateProductTab", Status.FAIL, "Failed while updating Voucher");
				}				
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(tab_Vouchers);
				waitForSometime(tcConfig.getConfig().get("LowWait"));				
				}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateProductTab", Status.FAIL, "Failed : Product tab not clicked");
			}
		
		}
		
	public void printVoucher(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String parentElement = driver.getWindowHandle();
		clickElementJS(btnPrint);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windowHandles.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, pdfPage, 240);
		if(verifyObjectDisplayed(pdfPage))
		{
			tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.PASS, "PDF is coming after clicking on Print button");
		}
		else
		{
			tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.FAIL, "PDF is not coming after clicking on Print button");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windows.get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	public void printVoucher_IHMarketer(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String parentElement = driver.getWindowHandle();
		clickElementJS(btnPrint);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windowHandles.get(2));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, pdfPage, 240);
		if(verifyObjectDisplayed(pdfPage))
		{
			tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.PASS, "PDF is coming after clicking on Print button");
		}
		else
		{
			tcConfig.updateTestReporter("VouchersPage", "validateNoOfPrintUpdate", Status.FAIL, "PDF is not coming after clicking on Print button");
		}
		driver.close();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windows.get(1));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	public void validateRedeemptionExpiration(){
		waitUntilElementVisibleBy(driver, txtDetailsTab, 120);
		String value = driver.findElement(txtRedeemptionExpiration).getText().trim();
		if(value.equals(redeemptionExp)){
			tcConfig.updateTestReporter("VouchersPage", "validateRedeemptionExpiration", Status.PASS, "Redemption Expiration text is same as in Product page :- " +redeemptionExp);
		}else{
			tcConfig.updateTestReporter("VouchersPage", "validateRedeemptionExpiration", Status.FAIL, "Failed : Redemption Expiration text not same as Product page");
		}
		
	}
	
	public void validateBookingAgent_Wyn()
	{
	 
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, BookingAgent, 120);
			if(verifyObjectDisplayed(BookingAgent)){
				String agent = driver.findElement(BookingAgent).getText().trim();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(imgIcon);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String logOn = driver.findElement(txtLogin).getText().trim();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if(agent.equals(logOn)){
					tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent_Wyn", Status.PASS, "User Login is validated sucessfully :- " +logOn);
				}
				else
				{
					tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent_Wyn", Status.FAIL, "Failed while validating Booking Agent used");
				}
			}
			else
			{
				tcConfig.updateTestReporter("VouchersPage", "validateBookingAgent_Wyn", Status.FAIL, "Failed : Booking agent not visible");
			}
			clickElementJS(txtSearchVoucher);
			waitForSometime(tcConfig.getConfig().get("LowWait"));		
		
	}
	
		
			
	public void validateBookignAgent_Vendor(){
	
			WebElement element = driver.findElement(By.xpath("//a[@id='detailTab__item']"));
			je.executeScript("arguments[0].scrollIntoView(true);",element);			
			waitUntilElementVisibleBy(driver, BookingAgent, 120);
			if(verifyObjectDisplayed(BookingAgent)){
				String agent = driver.findElement(BookingAgent).getText().trim();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(btnEdit);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> button =driver.findElements(clear_Agent);
				 button.get(0).click();
				 waitForSometime(tcConfig.getConfig().get("LowWait"));
				 fieldDataEnter(txtSearchAgents, testData.get("BookingAgent"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(selectAgent).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(btnSaveEdit).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if(verifyObjectDisplayed(BookingAgent)){
						String agentName = driver.findElement(BookingAgent).getText().trim();
						tcConfig.updateTestReporter("VouchersPage", "validateBookignAgent_Vendor", Status.PASS, "Booking agent is updated sucessfully :- " +agentName);
					}
					else{
						tcConfig.updateTestReporter("VouchersPage", "validateBookignAgent_Vendor", Status.FAIL, "Failed while updating Booking agent");
					}				
			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "validateBookignAgent_Vendor", Status.FAIL, "Failed : Booking agent not visible");
			}	
		
}
	
	
	
	public void validateRedeemMultipleVoucher() throws AWTException {
			waitUntilElementVisibleBy(driver, txtRecentlyViewed, 120);
			clickElementJS(txtRecentlyViewed);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if(verifyObjectDisplayed(txtSerachVendor)){														
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(txtSerachVendor, testData.get("VendorVoucher"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(bulk_Redeem).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));			
				try{
					Robot r = new Robot();
					 r.mouseWheel(5);
					 waitForSometime(tcConfig.getConfig().get("LowWait"));
					 //Thread.sleep(5000);
					 r.mouseWheel(5);
					 waitForSometime(tcConfig.getConfig().get("LowWait"));
					 //Thread.sleep(5000);
					 r.mouseWheel(5);
					 waitForSometime(tcConfig.getConfig().get("LowWait"));
					 //Thread.sleep(5000);
				}
				catch(Exception e){
					System.out.println("Scroll 3 times");
				}
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				int count = driver.findElements(By.xpath("(//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../../../td[4])")).size();
				List<WebElement> ceasar =driver.findElements(product);
				System.out.println(ceasar.size());					
				int checked=0;
				for(int p=1; p<count; p++)
				{
				if (driver.findElement(By.xpath("(//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../../../td[4])["+p+"]")).getText().equals("Alii Kayak"))
				{
					continue;
				}
				else
				{
					driver.findElement(By.xpath("(//table/tbody/tr/td[9]//img[@class='slds-truncate unchecked']/../../../../td[4]/../td[2])["+p+"]")).click();
					checked++;
				}
				if(checked==2)
				{
					break;				
				}
				}			
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(cellClick).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(pencil_Click).click();				
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> chk =driver.findElements(chkBox);
				System.out.println(chk.size());	
				int totalcount =0;
				for(int i=0; i<=(chk.size()-1);i++){										
					chk.get(i).click();
					totalcount++;
					if(totalcount==2){
						System.out.println("Pass");						
					break;					
				}
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(btnApply).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(btnSave_Redeem).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("VouchersPage", "validateRedeemMultipleVoucher", Status.PASS, " Checkbox checked");
				//waitUntilElementVisibleBy(driver, sucessMsg_Redeem, 120);	
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
				
	}		
			else{					 
					tcConfig.updateTestReporter("VouchersPage", "validateRedeemMultipleVoucher", Status.FAIL, " Failed : Checkbox not clickable");
				}			
}
	
	
		
	public void validateHardStopMsg(){
	
			waitUntilElementVisibleBy(driver, btnEdit, 120);
			clickElementBy(btnEdit);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtProductInfo, 120);
			clearElementBy(txtQuantityOnHand);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(txtQuantityOnHand).sendKeys(testData.get("QOH1"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(btnSaveVendor);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, hardStopMsg, 120);
			if(verifyObjectDisplayed(hardStopMsg)){
				String val =driver.findElement(hardStopMsg).getText();
				tcConfig.updateTestReporter("VouchersPage", "validateHardStopMsg", Status.PASS, "System displys the following error messgae : -" +val);
			}else{					 
				tcConfig.updateTestReporter("VouchersPage", "validateHardStopMsg", Status.FAIL, " Failed : Record saved sucessfully");
			}
			
	}


	
	
	
	
	public void chooseVoucherType(){

		if(verifyObjectDisplayed(txtChooseVoucherType)){
			waitUntilElementVisibleBy(driver, txtChooseVoucherType, 120);
			clickElementBy(nextBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));			
			tcConfig.updateTestReporter("VouchersPage", "chooseVoucherType", Status.PASS, "Choose Voucher type");
		   }
		   else{tcConfig.updateTestReporter("VouchersPage", "chooseVoucherType", Status.FAIL, "Failed while : Choosing Voucher");}		
		
	}
	
	
	public void chooseVoucherType_Vendor(){
	
			if(verifyObjectDisplayed(txtChooseVoucherType)){
				waitUntilElementVisibleBy(driver, txtChooseVoucherType, 120);
				List<WebElement> select =driver.findElements(vendorRadiobtn);
				System.out.println(select.size());
				select.get(1).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(nextBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("VouchersPage", "chooseVoucherType", Status.PASS, "Choose Voucher type");
				}
			   else{tcConfig.updateTestReporter("VouchersPage", "chooseVoucherType", Status.FAIL, "Failed while : Choosing Voucher");}
						
		}
	
	

	
	
	public void clickAddActivity(){
		waitUntilElementVisibleBy(driver, btnAddActivity, 120);		
		clickElementJS(btnAddActivity);	
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
	
	public void addActivity_Wyndham(){
	
		   waitUntilElementVisibleBy(driver, txtAddActivity, 120);
		   if(verifyObjectDisplayed(txtAddActivity)){
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> product =driver.findElements(lst_select);
			System.out.println(product.size());
			product.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(tabProduct).sendKeys(testData.get("Product"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> chng =driver.findElements(select_Product);
			System.out.println(chng.size());	
			chng.get(0).click();
			waitUntilElementVisibleBy(driver, quantityOnHand, 120);
			clearElementBy(quantityOnHand);
			//String qoh = getRandomString(1);
			driver.findElement(quantityOnHand).sendKeys(testData.get("QOH1"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, activityDate, 120);
			clickElementJS(activityDate);	
			waitForSometime(tcConfig.getConfig().get("LowWait"));		
			
			 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			    Calendar cal = Calendar.getInstance();
			    cal.setTime(new Date());
			    cal.add(Calendar.DATE, 1);
			    String newDate = dateFormat.format(cal.getTime());
			    
			    driver.switchTo().activeElement().sendKeys(Keys.TAB);
			    driver.switchTo().activeElement().sendKeys(Keys.TAB);
			    driver.switchTo().activeElement().sendKeys(Keys.TAB);
			    //clickElementJS(txtDirection);
			    waitUntilElementVisibleBy(driver, txtCustFirstName_Modify, 120);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clearElementBy(txtCustFirstName_Modify);
				String firstName= getRandomStringText(3);
				driver.findElement(txtCustFirstName_Modify).sendKeys(firstName);
				 waitForSometime(tcConfig.getConfig().get("LowWait"));
				
			WebElement element = driver.findElement(By.xpath("//button[@name='save']"));					 								 										 
			je.executeScript("arguments[0].scrollIntoView(true);",element);
			waitForSometime(tcConfig.getConfig().get("LowWait"));								
			Actions save=new Actions(driver);
			save.moveToElement(element).click().build().perform(); 
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("VouchersPage", "addActivity_Wyndham", Status.PASS, "Add Activity to Voucher sucessfully completed");
		   }
		   else{tcConfig.updateTestReporter("VouchersPage", "addActivity_Wyndham", Status.FAIL, "Failed while : Add Activity to Voucher");}
		
	}
	
	String product_Add = testData.get("Product");
	String qOH = testData.get("QOH1");
	
	
	
	
			
	
		
	public void validateChildVoucher(){
	
			waitUntilElementVisibleBy(driver, lnkTransactionNo, 120);
			clickElementJS(lnkTransactionNo);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(windowHandles.get(2));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtPaymentDetails, 120);
			String actual = driver.findElement(txtTransactNoPage).getText().trim();
			System.out.println(actual);
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
			clickElementJS(txtVocuhers);
			waitForSometime(tcConfig.getConfig().get("LowWait"));	
			//comparing Product Name in Transaction screen with Data sheet
			List<WebElement> productName = driver.findElements(lstProductName);
			System.out.println("Number of elements:" +productName.size());					
					String product = productName.get(productName.size()-1).getText().trim();			
					if(product_Add.equalsIgnoreCase(product)){
				tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher", Status.PASS, "The Product Name is sucessfully"
						+ " validated to be same as passed in Add Activity screen and is :- " +product);				
					}	
					else{
						tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher", Status.FAIL, "Failed while validating Product Name");
					}
			//comparing Quantity on Hand in Transaction screen with Data sheet
					List<WebElement> QOH = driver.findElements(lstQOP);
					System.out.println("Number of elements:" +QOH.size());
					String quanity = QOH.get(QOH.size()-1).getText().trim();
					if(qOH.equalsIgnoreCase(quanity)){
						tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher", Status.PASS, "The Quantity of Product is sucessfully"
							+ " validated to be same as passed in Add Activity screen and is :- " +quanity);				
						}
					else{
						tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher", Status.FAIL, "Failed while validating Quantity on Product");
					}
			driver.close();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(windows.get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.close();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> sec = new ArrayList<String>(driver.getWindowHandles());
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(sec.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));			
		
 }
	
	public void validateChildVoucher_VendorAdded(){
		
				waitUntilElementVisibleBy(driver, lnkTransactionNo, 120);
				clickElementJS(lnkTransactionNo);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(windowHandles.get(2));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, txtPaymentDetails, 120);
				String actual = driver.findElement(txtTransactNoPage).getText().trim();
				System.out.println(actual);
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
				clickElementJS(txtVocuhers);
				waitForSometime(tcConfig.getConfig().get("LowWait"));	
				//comparing Product Name in Transaction screen with Data sheet
				List<WebElement> productName = driver.findElements(lstProductName);
				System.out.println("Number of elements:" +productName.size());					
						String product = productName.get(productName.size()-2).getText().trim();			
						if(product_Add.equalsIgnoreCase(product)){
					tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher_VendorAdded", Status.PASS, "The Product Name is sucessfully"
							+ " validated to be same as passed in Add Activity screen and is :- " +product);				
						}	
						else{
							tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher_VendorAdded", Status.FAIL, "Failed while validating Product Name");
						}
				//comparing Quantity on Hand in Transaction screen with Data sheet
						List<WebElement> QOH = driver.findElements(lstQOP);
						System.out.println("Number of elements:" +QOH.size());
						String quanity = QOH.get(QOH.size()-2).getText().trim();
						if(qOH.equalsIgnoreCase(quanity)){
							tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher_VendorAdded", Status.PASS, "The Quantity of Product is sucessfully"
								+ " validated to be same as passed in Add Activity screen and is :- " +quanity);				
							}
						else{
							tcConfig.updateTestReporter("VouchersPage", "validateChildVoucher_VendorAdded", Status.FAIL, "Failed while validating Quantity on Product");
						}
				driver.close();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(windows.get(1));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.close();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> sec = new ArrayList<String>(driver.getWindowHandles());
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().window(sec.get(0));
				waitForSometime(tcConfig.getConfig().get("MedWait"));			
			
	 }

	
	
	public void navigateToIhMarketer(){
		
			waitUntilElementVisibleBy(driver, closeTab, 120);
			if(verifyObjectDisplayed(closeTab)){
			waitForSometime(tcConfig.getConfig().get("LowWait"));		
			clickElementJSWithWait(closeTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("VouchersPage", "navigateToIhMarketer", Status.PASS, "Page is sucessfully navigated to  In-House Marketer");
			}
			else{
				tcConfig.updateTestReporter("VouchersPage", "navigateToIhMarketer", Status.FAIL, "Failed while navigating to In-House Marketer");
		
	}

	}
}
