package atco.pages;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

//import com.relevantcodes.extentreports.DBReporter.Config;
import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class SF_LoginPage extends ATCOBasePage {

	public static final Logger log = Logger.getLogger(SF_LoginPage.class);
	ReadConfigFile config = new ReadConfigFile();

	public SF_LoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By username = By.xpath("//input[@id='username']");
	private By password = By.xpath("//input[@id='password']");
	private By loginButton = By.id("Login");
	private By logOut = By.xpath("//a[text()='Log Out']");
	private By viewProfile = By.xpath("//img[@title='User']");
	private By viewProfileCard = By.xpath("//span[contains(@class,'userProfileCardTrigger')]");
	private By homeIcon = By.xpath("//li[@data-menu-item-id='0']");
	private By profileName = By.xpath("//span[@class=' profileName']");
	private By logOutMarketer = By.xpath("//a[@title='Logout']");
//	@FindBy(xpath="//img[@title='User']") WebElement viewProfile;
	private By imageLogOut = By.xpath("//span[@class='uiImage']//img[@alt='User']"); // webelement list
	protected By customerLookUpTeam = By.xpath("//div[@class = 'cCustomLookupTeam']");
	protected By TeamName = By.xpath("//input[@placeholder= 'Enter Team Name']");
	protected By saleschannnelName = By.xpath("//input[@placeholder= 'Enter Sales Channel Name']");
	protected By nextButton = By.xpath("//button[@title = 'Next']");

	private void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, 20);
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("SF_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	private void setPassword(String strPassword) throws Exception {

		// look for th prob;
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("SF_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	private By testUserIHMarketer = By.xpath("//span[contains(text(),'WVO In-House Marketer')]");
	private By testUserLogOut = By.xpath("//a[@title='Logout']");
	private By btnNewLogin = By.xpath("//input[@name='Login']");

	public void testUserIHMarketerLogout() throws Exception {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, testUserIHMarketer, 120);
			clickElementBy(testUserIHMarketer);
			// driver.findElement(testUserIHMarketer).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(testUserLogOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnNewLogin, 120);
			if (verifyObjectDisplayed(btnNewLogin)) {
				tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.PASS,
						"Log out Successful");
			} else {
				tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.FAIL,
						"Log Off unsuccessful");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}

	}

	public void launchApp() {
		if(verifyObjectDisplayed(viewProfileCard)) {
			salesForceLogoutOnFailure();
		}
		driver.manage().deleteAllCookies();
		String url = testData.get("SF_URL").trim();
		//String url = config.get("SF_URL").trim();
		driver.navigate().to(url);

	}
/*
	public void loginToSalesForce() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("SF_UserID");
		String password = tcConfig.getConfig().get("SF_Password");

		setUserName(userid);
		setPassword(password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");

	}
*/
	public void loginToSalesForceDiffUsers() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String userid = testData.get("SF_UserID");
		String password = testData.get("SF_Password");

		setUserName(userid);
		setPassword(password);
		try {
			driver.findElement(loginButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinner();
			try {
				waitUntilElementVisibleBy(driver, customerLookUpTeam, 60);
				if (verifyObjectDisplayed(customerLookUpTeam)) {
					clickElementJSWithWait(TeamName);
					driver.findElement(TeamName).sendKeys(testData.get("TeamName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("TeamName") + "')]/..")).click();
					clickElementJSWithWait(saleschannnelName);
					driver.findElement(saleschannnelName).sendKeys(testData.get("SalesChannelName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("SalesChannelName") + "')]/.."))
							.click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(nextButton);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			} catch (Exception e) {
				log.info(e);
			}
		} catch (Exception e) {
			log.info(e);
		}
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		/*
		 * if(verifyObjectDisplayed(journeydesktop) || verifyObjectDisplayed(imarkter)
		 * || verifyObjectDisplayed(offers) ) { waitUntilElementVisibleBy(driver,
		 * menuIcon, 120); clickElementJSWithWait(menuIcon);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, lnkViewAll, 120);
		 * clickElementJS(lnkViewAll);
		 * waitForSometime(tcConfig.getConfig().get("MedWait")); List<WebElement>
		 * addList= driver.findElements(txtSearchBy);
		 * addList.get(addList.size()-1).click();
		 * addList.get(addList.size()-1).sendKeys("Vouchers");
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * driver.findElement(By.xpath("//mark[text()='Vouchers']")).click(); } else
		 * if(verifyObjectDisplayed(journeydesktop)){
		 * 
		 * tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
		 * "User is navigated to Homepage No need to switch explicitly");
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * 
		 * 
		 * } else if(verifyObjectDisplayed(imarkter)){
		 * tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
		 * "User is navigated to Homepage No need to switch explicitly");
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else{
		 * tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
		 * "Failed to Login"); waitForSometime(tcConfig.getConfig().get("LowWait")); }
		 */

	}

	public void salesForceLogoutMarketer() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, homeIcon, 120);
			clickElementBy(homeIcon);
			waitUntilElementVisibleBy(driver, profileName, 120);
			clickElementBy(profileName);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, logOutMarketer, 120);
			clickElementBy(logOutMarketer);

			tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.PASS, "Log Off successful");

		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	public void salesForceLogout() throws Exception {

		try {
			/*
			 * waitForSometime(tcConfig.getConfig().get("LongWait")); List<WebElement> img =
			 * driver.findElements(imageLogOut);
			 * 
			 * 
			 * waitForSometime(tcConfig.getConfig().get("lowWait"));
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); img.get(0).click();
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * //waitUntilObjectVisible(driver, viewProfile, 120);
			 * //driver.findElement(viewProfile).click();
			 */
//			mouseoverAndClick(viewProfile);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			clickElementBy(viewProfile);

			WebElement viewpro = driver.findElement(viewProfile);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", viewpro);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, logOut, 120);
			driver.findElement(logOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, username, 120);
			if (verifyObjectDisplayed(username)) {
				tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.PASS, "Log out Successfull");
			} else {
				tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL, "Log Off unsuccessfull");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessfull " + e.getMessage());
		}
	}
	
	public void salesForceLogoutOnFailure() {

		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			WebElement viewpro = driver.findElement(viewProfile);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", viewpro);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, logOut, 120);
			driver.findElement(logOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, username, 120);
			if (verifyObjectDisplayed(username)) {
				log.info("Log out Successful");
				//tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.PASS, "Log out Successfull");
			} else {
				//tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL, "Log Off unsuccessfull");
				log.fatal("Log out unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL,
					"Log out not successful on failure " + e.getMessage());
		}
	}

	private By logout_inhouse = By.xpath("//li[@class='logOut uiMenuItem']");

	@FindBy(xpath = "//img[@title='In-House Admin 2']")
	WebElement profileImg;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrw;

	public void inHouseAdmin2Logout() throws Exception {

		try {

			mouseoverAndClick(profileImg);

			waitUntilElementVisibleBy(driver, logout_inhouse, 120);
			driver.findElement(logout_inhouse).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	@FindBy(xpath = "//img[@title='In-House Marketer 3']")
	WebElement profileImgMarAgent;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrwMarAgent;
	@FindBy(xpath = "//div//div//li//a[@title='Logout']")
	WebElement logOutMarketingAgent3;

	private By lnkViewAll = By.xpath("//div[@class='slds-size_medium']//button");
	@FindBy(xpath = "////button[@class='slds-button slds-show']")
	WebElement menuIcon;
	private By homeTab = By.xpath("//span[contains(.,'Home')]");
	private By txtSearchBy = By.xpath("//input[@class='slds-input']");
	@FindBy(xpath = "//span[@title='Vouchers']")
	private WebElement voucher;

	/*
	 * @FindBy (xpath = "//span[@title = 'Journey Desktop']") private WebElement
	 * journeydesktop;
	 */

	private By journeydesktop = By.xpath("//span[@title = 'Journey Desktop']");
	private By imarkter = By.xpath(
			"//img[@src='/wvo/resource/1561942897000/Wyn_Custom/custom/img/wynd_dst_icon_sq_blk_rgb_150ppi.png']");
	protected By offers = By.xpath("//span[@title='Offers']");

	public void loginToInHouseMarketer() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = testData.get("SF_UserID");
		String password = testData.get("SF_Password");

		setUserName(userid);
		setPassword(password);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToInHouseMarketer", Status.PASS, "Log in button clicked");
		if (verifyElementDisplayed(voucher)) {
			waitUntilElementVisibleWb(driver, menuIcon, 120);
			menuIcon.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lnkViewAll, 120);
			clickElementJS(lnkViewAll);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			List<WebElement> addList = driver.findElements(txtSearchBy);
			addList.get(addList.size() - 1).click();
			addList.get(addList.size() - 1).sendKeys("Journey Desktop");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//mark[text()='Journey Desktop']")).click();
		} else if (verifyObjectDisplayed(journeydesktop)) {
			tcConfig.updateTestReporter("SF_LoginPage", "loginToInHouseMarketer", Status.PASS,
					"User is navigated to Homepage No need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else if (testData.get("SF_UserID").contains("ihmarketer")) {
			// driver.navigate().to("https://uat-clubwyndham-community.cs124.force.com/wvo/s/");

			if (verifyObjectDisplayed(imarkter)) {

				tcConfig.updateTestReporter("SF_LoginPage", "loginToInHouseMarketer", Status.PASS,
						"User is navigated to Homepage No need to switch explicitly");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("SF_LoginPage", "loginToInHouseMarketer", Status.FAIL,
						"User is not navigated to Jouney Mobile  need to switch explicitly");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		}

		else {
			tcConfig.updateTestReporter("SF_LoginPage", "loginToInHouseMarketer", Status.FAIL,
					"User is not navigated to Homepage  need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	protected By plusIcon = By.xpath("//div[@class='help_icon_section']//lightning-primitive-icon");
	protected By btnVoucher = By.xpath("//button[@id='voucher']");
	protected By btnAppLauncher = By.xpath("//div[@class='slds-icon-waffle']");
	protected By tabVouchers = By.xpath("//p[@class='slds-truncate' and contains(.,'Vouchers')]");
	protected By txtOffers = By.xpath("//span[@title='Offers']");
	protected By txtVouchers = By.xpath("//span[@title='Vouchers']");

	public void navigateToVoucherApp() {
		/*
		 * waitUntilElementVisibleBy(driver, plusIcon, 120);
		 * if(verifyObjectDisplayed(plusIcon)){ clickElementJSWithWait(plusIcon);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, btnVoucher, 120);
		 * clickElementJS(btnVoucher);
		 * waitForSometime(tcConfig.getConfig().get("MedWait")); ArrayList<String>
		 * windowHandles = new ArrayList<String>(driver.getWindowHandles());
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * driver.switchTo().window(windowHandles.get(1));
		 * tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp",
		 * Status.PASS,
		 * "User is navigated to Voucher App No need to switch explicitly");
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else{
		 * tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp",
		 * Status.FAIL,
		 * "User is not navigated to Jouney Mobile  need to switch explicitly");
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); }
		 */

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(journeydesktop)) {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp", Status.PASS,
					"User is navigated to Journey Desktop page");
			waitUntilElementVisibleBy(driver, homeTab, 120);
			clickElementJSWithWait(btnAppLauncher);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, tabVouchers, 120);
			clickElementJSWithWait(tabVouchers);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(verifyObjectDisplayed(txtVouchers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp", Status.PASS,
					"User is navigated explicitly to Offers Application Page");
		} else if (verifyObjectDisplayed(txtOffers)) {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp", Status.PASS,
					"User is navigated to Offers page");
			waitUntilElementVisibleBy(driver, txtOffers, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(btnAppLauncher);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, tabVouchers, 120);
			clickElementJSWithWait(tabVouchers);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(verifyObjectDisplayed(txtVouchers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp", Status.PASS,
					"User is navigated explicitly to Offers Application Page");
		} else if (verifyObjectDisplayed(txtOffers)) {
			waitUntilElementVisibleBy(driver, txtVouchers, "ExplicitMedWait");
			Assert.assertTrue(verifyObjectDisplayed(txtVouchers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp", Status.PASS,
					"Navigated successfully to Offers Application Page");
		} else {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToVoucherApp", Status.FAIL,
					"Not navigated to Vouchers Application");
		}

	}

}