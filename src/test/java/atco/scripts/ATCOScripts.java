package atco.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import atco.pages.HomePage;
import atco.pages.SF_LoginPage;
import atco.pages.VouchersPage;
import automation.core.TestBase;

public class ATCOScripts extends TestBase {

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * TC007_JE3451_Return_Voucher_user_return_info_wyndham voucher Description:
	 * Verify that the Voucher Transaction displays all customer payment details
	 * Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch1" })
	public void TC_JRV_TC001_Verify_Voucher_returninfo_WyndhamVoucher(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			vocPage.createWyndhamVoucher();
			vocPage.validateReturnVoucherWyndham();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC008_JE3451_Return_Voucher_user_return_info_vendor voucher"
	 * TC006_JE4738-Voucher_Vendor Voucher Print_Redemption Expiration
	 * displayed_ enter corresponding values in Product tab. Description: Verify
	 * that user and timestamp for a returned vendor voucher Designed By: Jyoti
	 * Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch1" })
	public void TC_JRV_TC002_Verify_Voucher_returninfo_VendorVoucher(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			vocPage.validateProductTab();
			vocPage.createVendorVoucher();
			vocPage.printVoucher();
			vocPage.validateReturnVoucherWyndham();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC009_JE3451_Return_Voucher_redeem_status_wyndham voucher" Description:
	 * Verify that the user cannot return a wyndham voucher with a redeemed
	 * status Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch1" })
	public void TC_JRV_TC003_Verify_ReturnVoucher_redeemstatus_wyndhamvoucher(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			vocPage.nativeSearchVoucher();
			vocPage.validateRedeemedVoucher();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC003_JE2672_print_multiple_vouchers_separate print out_wyndham vouchers"
	 * TC009_JE2693_redistribution_generated_vouchers_track_reprint_wyndham
	 * voucher Description: Verify that the system generates a pdf to send to a
	 * printer with all of the vouchers associated with the transaction and
	 * Multiple quantities of a voucher will print out separate vouchers
	 * Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch1" })
	public void TC_JRV_TC004_Verify_noOfPrints_update_wyndhamvouchers(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.nativeSearchVoucher();
			vocPage.validateQuantityOfProduct();
			vocPage.validateNoOfPrintUpdate();
			vocPage.validateLastModifiedBy();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC002_JE2693_redistribution_generated_vouchers_single_vendor voucher" &
	 * TC004_JE4411_Voucher_Generation_Updates_Change_Booking Agent_Vendor
	 * Voucher & TC005_JE5262-Vendor Vouchers - Print Multiple
	 * Quantities_Quantity of Product > 100_Hardstop Message Description: Verify
	 * that the system re-prints a wyndham voucher with voucher id selection and
	 * prints the most up to date information on the product or vendor Test Data
	 * - For Sales store -Grand Dessert ; booking agent= Subrina Shrriiam/Yosra
	 * Asila (A002117) Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch1" })
	public void TC_JRV_TC005_Verify_noOfPrints_update_vendorvouchers(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.nativeSearchVoucher();
			vocPage.validateQuantityOfProduct();
			vocPage.validateNoOfPrintUpdate();
			vocPage.validateLastModifiedBy();
			vocPage.validateBookignAgent_Vendor();
			vocPage.validateHardStopMsg();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC013_JE2693_redistribution_generated_vouchers_Edit Customer Info_more than 14 days_Wyndham voucher"
	 * TC001_JE5795_Edit Customer Info_return voucher_more than 14 days_Wyndham
	 * voucher Description: Verify that the user is able to edit the Customer
	 * Information of Wyndham Voucher when voucher creation date is more than 14
	 * days and the system prints the original information
	 * 
	 * Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch1" })
	public void TC_JRV_TC006_Verify_EditCustomerInfo_morethan14days_Wyndhamvouchers(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			homePage.navigateToMenu("1");
			vocPage.nativeSearchVoucher();
			vocPage.validateRegenratedVoucher();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC001_JE3920_Voucher_Trans_Pmt_Customer_Payment_Details_Wyndham Voucher"
	 * Description: Verify that the Voucher Transaction displays all customer
	 * payment details Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch2" })
	public void TC_JRV_TC007_Verify_Voucher_Customer_PaymentDetails_WyndhamVoucher(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.nativeSearchVoucher();
			vocPage.validatePaymentDetails();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC002_JE3920_Voucher_Trans_Pmt_Customer_Payment_Details_Vendor Voucher"
	 * Description: Verify that the Voucher Transaction displays all customer
	 * payment details Validate the Total Quantity field in the Voucher
	 * Transaction page Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch2" })
	public void TC_JRV_TC008_Verify_Voucher_Customer_PaymentDetails_VendorVoucher(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			homePage.navigateToMenu("1");
			vocPage.nativeSearchVoucher();
			vocPage.validatePaymentDetails();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC005_JE3919_Create_Voucher_Transaction_Add new voucher_related to transaction_Wyndham Voucher"
	 * TC001_JE3218_Distribute_Voucher_Print_Trans_MV_Wyndham Vouchers
	 * TC001_JE4411_Voucher_Generation_Updates_Default_Booking Agent
	 * display_Wyndham Voucher Description: Verify that the user is able to add
	 * a new Voucher as a related item to the Voucher Transaction Designed By:
	 * Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch2" })
	public void TC_JRV_TC009_Verify_Create_Voucher_Transaction_Wyndhamvouchers(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			vocPage.createWyndhamVoucher();
			vocPage.clickAddActivity();
			vocPage.chooseVoucherType();
			vocPage.validateChildTransactionNumber();
			vocPage.validateBookingAgent_Wyn();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC001_JE3706_Sales Store_Voucher_Phone Number_New Field_Wyndham Voucher"
	 * TC003_JE4410_Voucher - Issued Field Updates_Issue Date_Defaulted to
	 * todays date_Wyndham voucher TC001_JE5062-Vouchers - Accounting &
	 * Permission Updates_Paid_Invoice_Wyndham Voucher TC007_JE4410_Voucher -
	 * Issued Field Updates_Void Voucher_wyndham voucher TC001_JE5000_Voucher -
	 * Wyndham voucher_Create new voucher_single quantity_voucher summary list
	 * TC003_JE4738-Voucher_Wyndham Voucher_Redemption Expiration displayed_
	 * enter corresponding values in Product tab. TC001_JE4998_Voucher -
	 * Transaction Rollup_Voucher Transaction_Total Quantity_Wyndham Voucher
	 * Description: 1. Edit a Product by entering Redemption Expiration. 2.
	 * Verify that if a Wyndham voucher is created with the corresponding
	 * Product, the voucher details display the same Redemption expiration.
	 * Description: Verify that a new field is added to the "Sales Store" object
	 * and displayed for wyndham voucher Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch2" })
	public void TC_JRV_TC010_Verify_SalesStore_Voucher_PhoneNumber_WyndhamVoucher(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			homePage.navigateToMenu("1");
			vocPage.validateProductTab();
			vocPage.createWyndhamVoucher();
			vocPage.printVoucher();
			vocPage.validateRedeemptionExpiration();
			vocPage.validateSalesStorePhone();
			vocPage.validateProductDetails();
			// vocPage.validateTotalQuantity();
			vocPage.validatePaidStatus();
			vocPage.validateIssuedDate();
			// vocPage.validateVoidVoucher();

			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC004_JE3706_Sales Store_Voucher_No Phone Number_vendor Voucher"
	 * TC_JE1943_Access_SaleStore_Parent_vendor_voucher and
	 * TC_JE1943_Access_SaleStore_Parent_vendor_voucher TC004_JE5062-Vouchers -
	 * Accounting & Permission Updates_Edit_Paid_Invoice_vendor Voucher
	 * TC004_JE5000_Voucher - Vendor voucher_Create new voucher_multiple
	 * quantity_voucher summary list Description: Verify that a new field is
	 * added to the "Sales Store" object and displayed for vendor voucher;
	 * checking if the agent while creating a v oucher will see only the parent
	 * sales stores he has access to Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch2" })
	public void TC_JRV_TC011_Verify_SalesStore_Voucher_PhoneNumber_VendorVoucher(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			homePage.navigateToMenu("1");
			vocPage.createVendorVoucher();
			vocPage.validateSalesStorePhone();
			vocPage.validateProductDetails();
			vocPage.validateBookingAgent_Vendor();
			vocPage.validatePaidStatus();

			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC016_JE2693_redistribution_generated_vouchers_Edit Customer Info_more than 14 days_Vendor voucher"
	 * TC_JE1943_Access_SaleStore_Parent_vendor_voucher and
	 * TC_JE1943_Access_SaleStore_Parent_vendor_voucher TC002_JE5795_Edit
	 * Customer Info_return voucher_more than 14 days_Vendor voucher
	 * Description: Verify that the user is able to edit the Customer
	 * Information of Vendor Voucher when voucher creation date is more than 14
	 * days and the system prints the original information Designed By: Jyoti
	 * Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch2" })
	public void TC_JRV_TC012_Verify_EditCustomerInfo_morethan14days_VendorVouchers(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			homePage.navigateToMenu("1");
			vocPage.nativeSearchVoucher();
			vocPage.validateRegenratedVoucher();
			vocPage.validateReturnVoucherWyndham();
			vocPage.validateParentSalestoreVendor();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC002_JE3707_Create_Sample Voucher_existing_Product" Description: Verify
	 * that Verify that the user is able to select an existing product for
	 * creating a "Sample Voucher" Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch3" })
	public void TC_JRV_TC013_Verify_Create_SampleVoucher_existingProduct(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();
			homePage.navigateToMenu("1");
			vocPage.createSampleVoucher();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC_JE1943_Access_SaleStore_Parent_wyndham_voucher" &
	 * TC_JE1943_Access_SaleStore_Parent_vendor_voucher
	 * TC_JE3713_Vendor_confirmation_wyndham_voucher
	 * TC_JE1943_Access_SaleStore_Parent_wyndham_voucher Description: Verify
	 * that the checking if the agent while creating a voucher will see only the
	 * parent sales stores he has access to & adding a confirmation number while
	 * generating a voucher Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch3" })
	public void TC_JRV_TC014_Verify_Access_SaleStore_Parent_wyndhamvoucher(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.validateParentSalestoreWyndham();
			vocPage.validateBookingAgent_Wyndham();
			vocPage.validateVendorConfirmation();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC006_JE3451_Return_single_Voucher_Vendor_reuse" Description: Verify
	 * that the user is able to reuse a single returned vendor voucher Designed
	 * By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch3" })
	public void TC_JRV_TC015_Verify_Create_VendorVoucher_Reuse(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.createVendorVoucher();
			vocPage.validateVendorReturnResue();
			vocPage.vendorReissued();
			vocPage.validateNewVendorCreated();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC_JE1943_Product_not_ative_wyn TC_JE1943_Product_quantity_in_hand_wyn
	 * TC_JE1943_Product_generation_flag_wyn
	 * TC_JE1943_Select_Activity_details-today-wyn TC001_JE5131-Vendor
	 * Cancel_Return Reason_Wyn Voucher Description: Add Product details To
	 * verify he addition of new picklist value to Return Reason for wyndham
	 * voucher Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch3" })
	public void TC_JRV_TC016_Verify_Product_generationflag_QQH_Active_wyn(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		VouchersPage vocPage = new VouchersPage(tcconfig);
		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.createWyndhamVoucher_Modified();
			vocPage.validateVendorReturnReason();

			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * "TC_JE1943_Product_not_ative_vendor" &
	 * TC_JE1943_Product_quantity_in_hand_vendor &
	 * TC_JE1943_Product_generation_flag_vendor Description: Add activity
	 * details Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch3" })
	public void TC_JRV_TC017_Verify_Product_generationflag_QQH_Active_Vendor(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.createVendorVoucher_Modified();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event: TC001_JE4615_Voucher -
	 * Redeem voucher functionality_Multiple voucher selected redeemed
	 * sucessfully_Wyndham Description: Objective : To verify that Multiple
	 * selected voucher is redeemed successfully. Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch4" })
	public void TC_JRV_TC018_Verify_Redeemvoucher_multiple_Wyn(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.validateRedeemMultipleVoucher();

			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event: TC002_JE-1961_Security
	 * Matrix Validation_WVO Tour Reception Description: Validate the below
	 * actions for WVO Tour Reception 1. Voucher 2. Voucher Reprint 3. Voucher
	 * Receipt 4. Voucher Return 5. Sample 6. Transaction 7. Product 8. Product
	 * Price 9. Company 10. Sales Store
	 * 
	 * Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch4" })
	public void TC_JRV_TC019_Verify_SecurityMatrixValidation_TourReception(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.createWyndhamVoucher();
			vocPage.printVoucher();
			vocPage.validatePaymentDetails();
			vocPage.validateTransactionReceipt();
			vocPage.returnToVoucher();
			vocPage.validateReturnVoucherWyndham();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event: TC003_JE-1961_Security
	 * Matrix Validation_WVO Marketing Admin Description: Objective : Validate
	 * the below actions for WVO Marketing Admin 1. Voucher 2. Voucher Reprint
	 * 3. Voucher Receipt 4. Voucher Return 5. Sample 6. Transaction 7. Product
	 * 8. Product Price 9. Company 10. Sales Store
	 * 
	 * Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch4" })
	public void TC_JRV_TC020_Verify_SecurityMatrixValidation_MarketingAdmin(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToVoucherApp();

			vocPage.createWyndhamVoucher();
			vocPage.printVoucher();
			vocPage.validatePaymentDetails();
			vocPage.validateTransactionReceipt();
			vocPage.returnToVoucher();
			vocPage.validateReturnVoucherWyndham();
			vocPage.returnToVoucher();
			vocPage.createSampleVoucher();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * TC001_JE-5586_wyndham_add activity_In house marketer Description: Verify
	 * that the User should be able to add multiple activities to a Voucher and
	 * all these should be displayed under one transaction_wyndham Vouchers
	 * Pre-condition:-having valid credentials -having the role : In house
	 * marketer -Product object = voucher Designed By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch4" })
	public void TC_JRV_TC021_Verify_addactivity_wyndhamVouchers_ihmarketer(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToInHouseMarketer();
			login.navigateToVoucherApp();
			vocPage.nativeSearchVoucher();
			vocPage.clickAddActivity();
			vocPage.chooseVoucherType();
			vocPage.addActivity_Wyndham();
			vocPage.validateChildVoucher();
			vocPage.navigateToIhMarketer();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * TC002_JE-5586_vendor_add activity_add vendor_In house marketer
	 * Description: Verify that the User should be able to add multiple
	 * activities to a Voucher and all these should be displayed under one
	 * transaction_vendor vouchers Pre-condition:-having valid credentials
	 * -having the role : In house marketer -Product object = voucher Designed
	 * By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch4" })
	public void TC_JRV_TC022_Verify_vendorAddActivitywyndham_ihmarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToInHouseMarketer();
			login.navigateToVoucherApp();
			vocPage.nativeSearchVoucher();
			vocPage.clickAddActivity();
			vocPage.chooseVoucherType();
			vocPage.addActivity_Wyndham();
			vocPage.validateChildVoucher();

			vocPage.navigateToIhMarketer();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Journey Voucher Automation Scripts Function/event:
	 * TC003_JE-5586_vendor_add activity_add wyndham_In house marketer
	 * Description: Verify that the User should be able to add multiple
	 * activities to a Voucher and all these should be displayed under one
	 * transaction_vendor vouchers Pre-condition:-having valid credentials
	 * -having the role : In house marketer -Product object = voucher Designed
	 * By: Jyoti Chowdhury
	 */
	@Test(dataProvider = "testData", groups = { "atco", "regression", "batch4" })
	public void TC_JRV_TC023_Verify_vendorAddActivityvendor_ihmarketer(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SF_LoginPage login = new SF_LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		VouchersPage vocPage = new VouchersPage(tcconfig);

		try {

			login.launchApp();
			login.loginToInHouseMarketer();
			login.navigateToVoucherApp();
			vocPage.nativeSearchVoucher();
			vocPage.clickAddActivity();
			vocPage.chooseVoucherType_Vendor();
			vocPage.addActivity_Wyndham();
			vocPage.validateChildVoucher_VendorAdded();
			vocPage.navigateToIhMarketer();
			login.salesForceLogoutMarketer();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}
}
