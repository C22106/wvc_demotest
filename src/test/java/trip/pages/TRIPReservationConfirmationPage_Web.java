package trip.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPReservationConfirmationPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPReservationConfirmationPage_Web.class);

	public TRIPReservationConfirmationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected static HashMap<String, String> textBookingConfirmationNumber = new HashMap<String, String>();
	protected By textConfirmationNumber = By.xpath("//span[@id='bc-confirmationNum']");
	protected By ownerProfileClick = By.xpath("//img[@id = 'ownerProfileID']");
	protected By sectionProfile = By.xpath("//div[@id = 'profile']");
	public static List<String> ratePlanId = new ArrayList<String>();
	public static List<String> checkInCounter = new ArrayList<String>();
	
	/*
	 * Method: verifyReservation Description: verify Reservation Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyReservation() throws Exception {
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, textConfirmationNumber, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textConfirmationNumber), "Booking Failed");
		textBookingConfirmationNumber.put(testData.get("AutomationID"), getElementText(textConfirmationNumber).trim());
		tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "verifyReservation", Status.PASS,
				"Booking Confirmation number is : " + getElementText(textConfirmationNumber));

	}


	/*
	 * Method: navigateProfilePage Description: navigate Profile Page Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void navigateProfilePage() {
		waitUntilElementVisibleBy(driver, ownerProfileClick, "ExplicitLongWait");
		if (verifyObjectDisplayed(ownerProfileClick)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(ownerProfileClick);
			checkLoadingSpinnerTRIP();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(sectionProfile)) {
				tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "navigateProfilePage", Status.PASS,
						"Profile Page is Displayed");
			} else {
				tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "navigateProfilePage", Status.FAIL,
						"Profile page is not displayed");
			}

		} else {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "navigateProfilePage", Status.FAIL,
					"Profile Button  is not clickable");
		}

	}

	/**
	 * Method: checkRatePlanInDB Description: Validate Rate Plan value in
	 * Datebase Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void checkRatePlanInDB() throws Exception {
		List<String> roomBkgId = new ArrayList<String>();

		String queryToGetRoomBkgId = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND rer.res_num in ('"
				+ textBookingConfirmationNumber.get(testData.get("AutomationID")) + "')";
		DBConnectionOnly();
		roomBkgId = ExecuteQueryAndFetchListColumData(queryToGetRoomBkgId, "ROOM_BKG_ID");

		String queryToGetRatePlanID = "select ROOM_RATE_PLAN_ID from RESERVATION.RH_ROOM_BKG_CHNG where ROOM_BKG_ID = '"
				+ roomBkgId.get(0) + "'";
		ratePlanId = ExecuteQueryAndFetchListColumData(queryToGetRatePlanID, "ROOM_RATE_PLAN_ID");

		tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkRatePlanInDB", Status.PASS,
				"Rate Plan before modification is " + ratePlanId.get(0));

		closeDB();
	}

	/**
	 * Method: checkModificationCounterInDB Description: Validate value in
	 * Datebase Date: Aug/2020 Author: Unnat Jain By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void checkModificationCounterInDB() throws Exception {
		List<String> roomBkgId = new ArrayList<String>();
		DBConnectionOnly();
		
		String queryToGetRoomBkgId = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND rer.res_num in ('"
				+ textBookingConfirmationNumber.get(testData.get("AutomationID")) + "')";
		roomBkgId = ExecuteQueryAndFetchListColumData(queryToGetRoomBkgId, "ROOM_BKG_ID");
		
		String queryToGetCounter = "select * from rh_room_bkg b where b.ROOM_BKG_ID = '" + roomBkgId.get(0) + "'";
		checkInCounter = ExecuteQueryAndFetchListColumData(queryToGetCounter, "MDFD_CHKIN_COUNTER");
		
		tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkModificationCounterInDB", Status.PASS,
				"Counter before modification is " + checkInCounter.get(0));
		
		closeDB();
	}

	/**
	 * Method: checkModifyNightsInDB Description: check Modify Nights In DB
	 * Date: Aug/2020 Author: Abhijeet By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void checkModifyNightsInDB() throws Exception {
		String totalNightsModified = Long.toString(TRIPReservationDetailsPage_Web.displayedNight);
		String bookingCheckInDate = convertDateFormat("MM-dd-yyyy", "yyyy-MM-dd", testData.get("CheckInDate"))
				.toUpperCase();
		String bookingCheckOutDate = convertDateFormat("MM-dd-yyyy", "yyyy-MM-dd", testData.get("ModifyCheckOutDate"))
				.toUpperCase();
		String query = "Select * from rh_ent_res r inner join rh_res_item m on m.res_id = r.res_id where r.res_num = '"+TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber.get(testData.get("AutomationID"))+"'";
		DBConnectionOnly();
		String roomBookingID = ExecuteQueryAndFetchColumData(query, "ROOM_BKG_ID").trim();
		String query1 = "select * from rh_room_bkg b where b.room_bkg_id = '" + roomBookingID + "'";
		String checkindate = ExecuteQueryAndFetchColumData(query1, "CHK_IN_LOCAL_DATE").split(" ")[0].trim();
		String checkoutdate = ExecuteQueryAndFetchColumData(query1, "CHK_OUT_LOCAL_DATE").split(" ")[0].trim();
		String lenghtOfStay = ExecuteQueryAndFetchColumData(query1, "LEN_OF_STAY").trim();
		if (checkindate.equals(bookingCheckInDate)) {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkModifyNightsInDB", Status.PASS,
					"DB Check In Date Validate Successfully");
		} else {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkModifyNightsInDB", Status.FAIL,
					"DB Check In Date Validation Failed");
		}

		if (checkoutdate.equals(bookingCheckOutDate)) {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkModifyNightsInDB", Status.PASS,
					"DB Check Out Date Validate Successfully");
		} else {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkModifyNightsInDB", Status.FAIL,
					"DB Check Out Date Validation Failed");
		}

		if (lenghtOfStay.equals(totalNightsModified)) {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkModifyNightsInDB", Status.PASS,
					"DB Length Of Stay Validate Successfully");
		} else {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "checkModifyNightsInDB", Status.FAIL,
					"DB Length of Stay Validation Failed");
		}

		closeDB();

	} 

}
