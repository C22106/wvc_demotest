package trip.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPMembershipPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPMembershipPage_Web.class);

	public TRIPMembershipPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By reasonDrpdwn=By.id("dropdown-reasonForOverride");
	protected By addBlockBtn=By.id("overlay-primary");
	protected By donotbookBlockMsg=By.id("do-not-book-override-lock");
	protected By blockBtn = By.id("addBlock");
	protected By donotbookBlock=By.id("do-not-book");
	protected By membershipHeader=By.xpath("//header//a[contains(.,'MEMBERSHIP')]//img");
	protected By sectionMenuLinks = By.xpath("//div[@class = 'section-menu-links']");
	protected By selectReservationLink = By.xpath("//a[@id= 'section-menu-reservations']");
	protected By reservationsPage = By.xpath("//div[@id = 'reservations']");
	protected By blockPointProtection=By.xpath("//input[@id='block-points-protection']");
	protected By selectReasonOverrideOther=By.xpath("//div[@id='addBlockReasonDropdown']/div[text()='Other']");
	protected By commentTextArea = By.xpath("//textarea[@id='addBlockCommentsId']");
	protected By addBlockModal = By.xpath("//button[text()='ADD BLOCK']");
	protected By pageTitle = By.xpath("//h2[@id='overlay-title']");
	protected By imgPPBlock = By.xpath("//img[@id='block-points-protection-override-lock']");
	protected By ownerProfileClick = By.xpath("//img[@id = 'ownerProfileID']");
	protected By sectionProfile = By.xpath("//div[@id = 'profile']");
	
	
	/*
	 * Method: selectMember Description: Select Member From List
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickBlockBtn() throws Exception {
		Assert.assertTrue(verifyObjectDisplayed(blockBtn), "Block Button not Present");
		getElementInView(blockBtn);
		tcConfig.updateTestReporter("TRIPMembershipPage", "clickBlockBtn", Status.PASS,
				"Block Button is Present");
		clickElementJSWithWait(blockBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerTRIP();

	}
	
	
	/*
	 * Method: selectMember Description: Select Member From List
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void addDoNotBookBlock() throws Exception {
		waitUntilElementVisibleBy(driver, donotbookBlock, "ExplicitLongWait");
		clickElementJSWithWait(donotbookBlock);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkLoadingSpinnerTRIP();
		clickElementJSWithWait(reasonDrpdwn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(selectReasonOverrideOther);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeysBy(commentTextArea, "test");
		clickElementBy(addBlockBtn);
		checkLoadingSpinnerTRIP();
		Assert.assertTrue(verifyObjectDisplayed(donotbookBlockMsg), "Block Msg not Present");
		getElementInView(donotbookBlockMsg);
		tcConfig.updateTestReporter("TRIPMembershipPage", "addDoNotBookBlock", Status.PASS,
				"Do not Book Block added");
		
	}
	
	/*
	 * Method: selectMember Description: Select Member From List
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void profileHeaderClick() throws Exception {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, membershipHeader, "ExplicitLongWait");
		clickElementJSWithWait(membershipHeader);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(sectionMenuLinks),
				"Menu List Not Present, Maybe failed to clcik profile header");

	}
	
	/*
	 * Method: reservationLinkClick Description: reservation Link Click
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void reservationLinkClick() throws Exception {
		waitUntilElementVisibleBy(driver, selectReservationLink, "ExplicitLongWait");
		mouseoverAndClick(getObject(selectReservationLink));
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, reservationsPage, "ExplicitLongWait");
		if (verifyObjectDisplayed(reservationsPage)) {
			tcConfig.updateTestReporter("TRIPMembershipPage", "reservationLinkClick", Status.PASS,
					"Reservations Page is Displayed");
		} else {
			tcConfig.updateTestReporter("TRIPMembershipPage", "reservationLinkClick", Status.FAIL,
					"Reservations page is not displayed");
		}
	}


	/*
	 * Method: clickAddMembershipBlock Description: Click on membership block button
	 * Date: July/2020 Author: Monideep Changes By: NA
	 */
	public void clickAddMembershipBlockPP() {
		if(!verifyObjectDisplayed(imgPPBlock)){
			mouseoverAndClick(getObject(blockBtn));
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilElementVisibleBy(driver, blockPointProtection , "ExplicitLongWait");
			//Assert.assertTrue(verifyObjectDisplayed(blockPointProtection), "Membership block modal popup is not displayed");
			tcConfig.updateTestReporter("TRIPMembershipPage", "clickAddMembershipBlock", Status.PASS,
					"Membership block modal popup is Displayed");
		}else{
			tcConfig.updateTestReporter("TRIPMembershipPage", "clickAddMembershipBlock", Status.INFO,
					"PP already blocked");
		}
		
	}

	/*
	 * Method: setPPBlock Description: Set point protection block
	 * Date: July/2020 Author: Monideep Changes By: NA
	 */
	public void setPPBlock() {
		
		if(!verifyObjectDisplayed(imgPPBlock)){
			mouseoverAndClick(getObject(blockPointProtection));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			mouseoverAndClick(getObject(reasonDrpdwn));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			mouseoverAndClick(getObject(selectReasonOverrideOther));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysBy(commentTextArea, "Test");
			clickElementBy(pageTitle);
			mouseoverAndClick(getObject(addBlockModal));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Assert.assertTrue(verifyObjectDisplayed(imgPPBlock), "PP block image is not displayed");
			tcConfig.updateTestReporter("TRIPMembershipPage", "setPPBlock", Status.PASS,
					"Point protection blocked successfully");
		}else{
			tcConfig.updateTestReporter("TRIPMembershipPage", "setPPBlock", Status.INFO,
					"PP already blocked");
		}	
	}
	
	/*
	 * Method: navigateProfilePage Description: navigate ProfilePage
	 * Date: July/2020 Author: Monideep Changes By: NA
	 */
	public void navigateProfilePage(){
		waitUntilElementVisibleBy(driver, ownerProfileClick, "ExplicitLongWait");
		if (verifyObjectDisplayed(ownerProfileClick)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(ownerProfileClick);
			checkLoadingSpinnerTRIP();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(sectionProfile)) {
				tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "navigateProfilePage", Status.PASS, "Profile Page is Displayed");
			} else {
				tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "navigateProfilePage", Status.FAIL, "Profile page is not displayed");
			}

		} else {
			tcConfig.updateTestReporter("TRIPReservationConfirmationPage", "navigateProfilePage", Status.FAIL, "Profile Button  is not clickable");
		}
		
	}
}
