package trip.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPSearchResultPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPSearchResultPage_Web.class);

	public TRIPSearchResultPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	public static int price;
	protected By totalBookButton = By.xpath("//button[text() = 'BOOK']");
	protected By bookButton = By.xpath("//button[@class='btn resort-room-book']");
	/*
	 * Method: selectRoom Description: Select Room
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectRoom(){
		String roomToBook = testData.get("RoomToBook");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, totalBookButton, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(totalBookButton), "Book Button not present to Book a reservation");
		clickElementJSWithWait(getObject(By.xpath("//span[text() = '"+roomToBook+"']/ancestor::tr[contains(@class,'resort-room')]//button[text() = 'BOOK']")));
		tcConfig.updateTestReporter("TRIPSearchResultPage", "selectRoom", Status.PASS,
				"Click on Book Button for Room: "+roomToBook);
		checkLoadingSpinnerTRIP();
	}
	/*
	 * Method: selectRoom Description: selectFirstAvailableRoom
	 * Date: July/2020 Author: Monideep Roy Changes By: NA
	 */
	public void selectFirstAvailableRoom(){
		waitUntilElementVisibleBy(driver, totalBookButton, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(totalBookButton), "Book Button not present to Book a reservation");
		clickElementJSWithWait(getObject(bookButton));
		tcConfig.updateTestReporter("TRIPSearchResultPage", "selectFirstAvailableRoom", Status.PASS,
				"Click on Book Button for Room");
		checkLoadingSpinnerTRIP();
	}
	
	/*
	 * Method: getDiscountPriceForSpeceficRoom Description: get Discount Price For Specefic Room
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void getDiscountPriceForSpeceficRoom(){
		String discountForRoom = testData.get("DiscountForRoom");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, totalBookButton, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(getObject(By.xpath("//span[text() = '"+discountForRoom+"']/../..//div[@class = 'pointsCount']/span[contains(@class,'afterPts')]"))), "Discount not present for Room: "+discountForRoom);
		price = Integer.parseInt(getElementText(getObject(By.xpath("//span[text() = '"+discountForRoom+"']/../..//div[@class = 'pointsCount']/span[contains(@class,'afterPts')]"))).trim().split(" ")[0].replace(",", ""));
		tcConfig.updateTestReporter("TRIPSearchResultPage", "getDiscountPriceForSpeceficRoom", Status.PASS,
				"Price Displayed is: "+price);
	}
	
	/*
	 * Method: getPriceWIthoutDiscountForSpeceficRoom Description: get Price WIthout Discount For Specefic Room
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void getPriceWithoutDiscountForSpeceficRoom(){
		String discountForRoom = testData.get("DiscountForRoom");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, totalBookButton, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(getObject(By.xpath("//span[text() = '"+discountForRoom+"']/../..//div[@class = 'pointsCount']/span[contains(@class,'room-points')]"))), "Discount not present for Room: "+discountForRoom);
		price = Integer.parseInt(getElementText(getObject(By.xpath("//span[text() = '"+discountForRoom+"']/../..//div[@class = 'pointsCount']/span[contains(@class,'room-points')]"))).trim().split(" ")[0].replace(",", ""));
		tcConfig.updateTestReporter("TRIPSearchResultPage", "getPriceWithoutDiscountForSpeceficRoom", Status.PASS,
				"Price Displayed iss: "+price);
	}
}
