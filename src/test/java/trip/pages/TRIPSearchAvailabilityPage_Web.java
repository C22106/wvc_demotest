package trip.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPSearchAvailabilityPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPSearchAvailabilityPage_Web.class);

	public TRIPSearchAvailabilityPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By fieldLocation = By.xpath("//div[@id='location']");
	protected By checkInDate = By.xpath("//input[@id = 'checkInDate']");
	protected By checkOutDate = By.xpath("//input[@id = 'checkOutDate']");
	protected By searchButton = By.xpath("//button[@id = 'searchAvailability-search']");
	
	/*
	 * Method: selectLocation Description: Select Location
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectLocation() throws Exception {
		String resortName = testData.get("ResortName");
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, fieldLocation, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(fieldLocation), "Field Location to enter not displayed");
		fieldDataEnter(fieldLocation, resortName);
		if (verifyObjectDisplayed(getObject(By.xpath("(//b[contains(text(),'"+resortName+"')])[1]")))) {
			clickElementJSWithWait(getObject(By.xpath("(//b[contains(text(),'"+resortName+"')])[1]")));
			tcConfig.updateTestReporter("SearchAvailibility", "selectResort", Status.PASS,
					"Resort Name selected as : " + resortName);
		}
	}

	/*
	 * Method: selectCheckInDate Description: select CheckIn Date
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCheckInDate() throws InterruptedException{
		String CheckinDate = testData.get("CheckInDate");
		String newCheckinDate = CheckinDate.replace("-", "");
		waitUntilElementVisibleBy(driver, checkInDate, "ExplicitLongWait");
		fieldDataEnter(checkInDate, newCheckinDate);
		tcConfig.updateTestReporter("SearchAvailibility", "SetSearchParam", Status.PASS,
				"Check in Date is set as : " + CheckinDate);
		sendKeyboardKeys(Keys.TAB);
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	/*
	 * Method: selectCheckOutDate Description: select CheckOut Date
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCheckOutDate() throws InterruptedException{
		String CheckoutDate = testData.get("CheckOutDate");
		String newCheckoutDate = CheckoutDate.replace("-", "");
		
		waitUntilElementVisibleBy(driver, checkOutDate, "ExplicitLongWait");
		fieldDataEnter(checkOutDate, newCheckoutDate);
		tcConfig.updateTestReporter("SearchAvailibility", "SetSearchParam", Status.PASS,
				"Check out Date is set as : " + CheckoutDate);
		sendKeyboardKeys(Keys.TAB);
		checkLoadingSpinnerTRIP();
		
	}
	
	
	/*
	 * Method: searchAvailableRoom Description: Search Available Room
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void searchAvailableRoom() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, searchButton, "ExplicitLongWait");
		clickElementBy(searchButton);
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("SearchAvailibility", "searchAvailableRoom", Status.PASS,
				"Search button clicked after entering the parameter");
		try{
			checkLoadingSpinnerTRIP();
		}catch(Exception e){
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}
}
