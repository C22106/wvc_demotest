package trip.pages;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class TRIPBasePage extends FunctionalComponents {
	
	Connection dbConnection;

	public TRIPBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}
	
	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */
	/*
	 * ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	 * ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	 * *************************Method: checkAndSetBrowser ***
	 * ***************Description: Check Driver and Set Browser *********
	 */

	public WebDriver checkAndSetBrowser(String strBrowser) {
		if (tcConfig.getDriver() == null) {

			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser);
				tcConfig.setDriver(driver);
			} catch (Exception e) {

				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	/*
	 * *************************Method: checkAndSetBrowser ***
	 * ***************Description: Check Driver and Set Browser *********
	 */
	public WebDriver checkAndSetBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {

			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {

				log.info("Unable to launch driver : Error -" + e.getMessage());
			}
		}

		return tcConfig.getDriver();

	}
	
	protected By bannerAlertClose = By.xpath("//button[contains(@class,'close-button')]");

	public void closeBannerAlert() {
		try {
			WebElement bannerClose = getList(bannerAlertClose).get(0);
			if (verifyObjectDisplayed(bannerClose)) {
				// clickElementBy(bannerClose);
				clickElementJSWithWait(bannerClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

		} catch (Exception e) {
			log.error("Banner Message Not Present");

		}
	}
	
	protected By loadingSpinnerTRIP = By.xpath("//div[@class='loading-wrap']");
	protected By loadingSpinnerCUI = By.xpath("//div[@class='loading']");

	/*
	 * *************************Method: checkLoadingSpinnerTRIP ***
	 * *************Description: TRIP Loading Spinner**
	 */

	public void checkLoadingSpinnerTRIP() {
		waitUntilElementInVisible(driver, loadingSpinnerTRIP, 120);
	}

	/*
	 * *************************Method: checkLoadingSpinnerCUI ***
	 * *************Description: CUI Loading Spinner**
	 */
	public void checkLoadingSpinnerCUI() {
		waitUntilElementInVisible(driver, loadingSpinnerCUI, 120);
	}
	
	/*
	 * *************************Method: DBConnection ***
	 * *************Description - Database connection
	 */

	public List<String> DBConnection(String query, String columnname) throws UnsupportedEncodingException {
		String hostname = tcConfig.getConfig().get("Hostname");
		int port = Integer.parseInt(tcConfig.getConfig().get("PORT"));
		String servicename = tcConfig.getConfig().get("SERVICE_NAME");
		String user = tcConfig.getConfig().get("USER");
		String password = tcConfig.getConfig().get("PASSWORD");
		String decodedPassword = decodePassword(password);
		List<String> stringList = new ArrayList<String>();
		try {
			// step1 load the driver class
			Class.forName("oracle.jdbc.driver.OracleDriver");

			// step2 create the connection object
			Connection con = DriverManager.getConnection(
					"jdbc:oracle:thin:@//" + hostname + ":" + port + "/" + servicename, user, decodedPassword);
			log.info("Database Connection established");

			// step3 create the statement object
			Statement stmt = con.createStatement();

			// step4 execute query
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				stringList.add(rs.getString(columnname));
			}

			// step5 close the connection object
			con.close();

		} catch (Exception e) {
			log.info(e);
		}
		return stringList;
	}

	/*
	 * *************************Method: DBConnection ***
	 * *************Description - Database connection
	 */

	public Connection DBConnectionOnly() throws UnsupportedEncodingException {
		String hostname = tcConfig.getConfig().get("Hostname");
		int port = Integer.parseInt(tcConfig.getConfig().get("PORT"));
		String servicename = tcConfig.getConfig().get("SERVICE_NAME");
		String user = tcConfig.getConfig().get("USER");
		String password = tcConfig.getConfig().get("PASSWORD");
		String decodedPassword = decodePassword(password);
		try {
			// step1 load the driver class
			Class.forName("oracle.jdbc.driver.OracleDriver");

			// step2 create the connection object
			dbConnection = DriverManager.getConnection(
					"jdbc:oracle:thin:@//" + hostname + ":" + port + "/" + servicename, user, decodedPassword);

		} catch (Exception e) {
			log.info(e);
		}
		return dbConnection;
	}

	/*
	 * *************************Method: ExecuteQueryAndFetchColumData ***
	 * *************Description - Execute DB Query And Fetch Colum Data
	 */
	public String ExecuteQueryAndFetchColumData(String query, String columnname) throws UnsupportedEncodingException {
		String value = null;
		try {
			// step3 create the statement object
			Statement dbStatement = dbConnection.createStatement();

			// step4 execute query
			ResultSet rs = dbStatement.executeQuery(query);
			while (rs.next()) {
				value = (rs.getString(columnname));
			}
		} catch (Exception e) {
			log.info(e);
		}
		return value;
	}

	/*
	 * *************************Method: ExecuteQueryAndFetchColumData ***
	 * *************Description - Execute DB Query And Fetching the list of
	 * Column Data
	 */
	public List<String> ExecuteQueryAndFetchListColumData(String query, String columnname)
			throws UnsupportedEncodingException {
		List<String> stringList = new ArrayList<String>();
		try {
			// step3 create the statement object
			Statement dbStatement = dbConnection.createStatement();

			// step4 execute query
			ResultSet rs = dbStatement.executeQuery(query);
			while (rs.next()) {
				stringList.add(rs.getString(columnname).trim());
			}
		} catch (Exception e) {
			log.info(e);
		}
		return stringList;
	}

	/*
	 * *************************Method: closeDB *** *************Description -
	 * Close Database connection
	 */
	public void closeDB() throws SQLException {
		try {
			dbConnection.close();
		} catch (Exception e) {
			log.info(e);
		}

	}


}
