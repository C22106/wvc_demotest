package trip.pages;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPLoginPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPLoginPage_Web.class);

	public TRIPLoginPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By fieldUsername = By.xpath("//input[@id = 'userName']");
	protected By fieldPassword = By.xpath("//input[@id = 'password']");
	protected By buttonSignIn = By.xpath("//input[@id = 'signIn']");
	protected By fieldMemberSearch = By.xpath("//input[@id = 'quickSearch']");
	protected By sidebarAvatar = By.id("sidebar-avatar");
	protected By buttonSignOut = By.id("signOut");
	protected By advancedButton = By.id("details-button");
	protected By proceedLink = By.id("proceed-link");

	/*
	 * Method: launchApplication Description: launch the TRIP Application Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void launchApplication() {
		getBrowserName();
		navigateToURL(tcConfig.getConfig().get("TRIPURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		proceedToApplication();
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(fieldUsername), "Failed to Launch Application");
		tcConfig.updateTestReporter("TRIPLoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}

	/*
	 * Method: proceedToApplication Description: proceed to application when
	 * connection is not private Date: Dec/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void proceedToApplication() {
		waitUntilElementVisibleBy(driver, advancedButton, "ExplicitLowWait");
		try {
			clickElementBy(advancedButton);
			waitUntilElementVisibleBy(driver, proceedLink, "ExplicitLowWait");
			clickElementBy(proceedLink);
		} catch (Exception e) {
			log.info("Private Connection");
		}

	}

	/*
	 * Method: setUserName Description: Set UserName Date: July/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void setUserName() {
		String userID = tcConfig.getConfig().get("Trip_Username");
		getElementInView(fieldUsername);
		fieldDataEnter(fieldUsername, userID);
		tcConfig.updateTestReporter("TRIPLoginPage", "setUserName", Status.PASS,
				"Username is entered for CUI Login as " + userID);
	}

	/*
	 * Method: setPassword Description: Set Password Date: July/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void setPassword() throws UnsupportedEncodingException {
		String userPassword = tcConfig.getConfig().get("Trip_Password");
		String decodedPassword = decodePassword(userPassword);
		fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("TRIPLoginPage", "setPassword", Status.PASS, "Password Entered Successfully");
	}

	/*
	 * Method: clickButtonLogin Description: Click Login Button Date: July/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void clickButtonLogin() {
		waitUntilElementVisibleBy(driver, buttonSignIn, "ExplicitLongWait");
		if (verifyObjectDisplayed(buttonSignIn)) {
			clickElementJSWithWait(buttonSignIn);
			waitUntilElementVisibleBy(driver, fieldMemberSearch, "ExplicitLongWait");
			if (verifyObjectDisplayed(fieldMemberSearch)) {
				tcConfig.updateTestReporter("TRIPLoginPage", "clickButtonLogin", Status.PASS, "Login Successfull");
			} else {
				tcConfig.updateTestReporter("TRIPLoginPage", "clickButtonLogin", Status.PASS, "Failed to Login");
			}

		} else {
			tcConfig.updateTestReporter("TRIPLoginPage", "clickButtonLogin", Status.PASS, "Login Button not displayed");
		}
	}

	/*
	 * Method: logoutApplication Description: Logout out From TRIP Application
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void logoutApplication() throws Exception {
		try {
			waitUntilElementVisibleBy(driver, sidebarAvatar, "ExplicitLongWait");
			if (verifyObjectDisplayed(sidebarAvatar)) {
				clickElementJSWithWait(sidebarAvatar);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("TRIPLoginPage", "logoutApplication", Status.FAIL,
						"SideBar is not present");

			}
			waitUntilElementVisibleBy(driver, buttonSignOut, "ExplicitLongWait");
			if (verifyObjectDisplayed(buttonSignOut)) {
				tcConfig.updateTestReporter("TRIPLoginPage", "logoutApplication", Status.PASS,
						"SignOut Button is present");
				clickElementJSWithWait(buttonSignOut);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {
					driver.switchTo().alert().accept();
				} catch (Exception e) {
					System.out.println("No Alert");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				try {
					driver.switchTo().alert().accept();
				} catch (Exception e) {
					System.out.println("No Alert");
				}
			} else {
				tcConfig.updateTestReporter("TRIPLoginPage", "logoutApplication", Status.FAIL,
						"SignOut Button is not present");

			}
			waitUntilElementVisibleBy(driver, buttonSignIn, "ExplicitLongWait");
			if (verifyObjectDisplayed(buttonSignIn)) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("TRIPLoginPage", "logoutApplication", Status.PASS, "SignOut Successful");

			} else {
				tcConfig.updateTestReporter("TRIPLoginPage", "logoutApplication", Status.FAIL, "SignOut Failed");

			}

		} catch (Exception ex) {

			tcConfig.updateTestReporter("TRIPLoginPage", "logoutApplication", Status.FAIL,
					"SignOut Failed " + ex.getMessage());

		}

	}
}
