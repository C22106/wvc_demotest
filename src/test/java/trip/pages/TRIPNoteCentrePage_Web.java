package trip.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPNoteCentrePage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPNoteCentrePage_Web.class);

	public TRIPNoteCentrePage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By noteCentreHeader = By.id("nc-header");
	protected By notesBody = By.xpath("//ul[@id='nc-notesList']/li//p[contains(@class,'nc-note')]");
	protected By notesHeader = By.xpath("//ul[@id='nc-notesList']/li//strong[@class='dots__left']");

	/**
	 * Method: validateNotesForNightsModification Description: Validate notes for night
	 * modifications Date: July/2020 Author: Kamalesh Changes By: NA
	 */

	public void validateNotesForNightsModification(String firstIterator, String secondIterator) {
		waitUntilElementVisibleBy(driver, noteCentreHeader, "ExplicitLongWait");
		for (int numberofNotes = 0; numberofNotes < getList(notesBody).size() - 1; numberofNotes++) {
			if (getList(notesHeader).get(numberofNotes).getText().contains("Modification Existing Reservation")
					&& getList(notesBody).get(numberofNotes).getText().contains(testData.get("ReservationNumber"))) {
				
				String losText = "Updated LOS from " + TRIPReservationDetailsPage_Web.nights.get(firstIterator)
						+ " nights to " + TRIPReservationDetailsPage_Web.nights.get(secondIterator) + " nights";
				
				if (getList(notesBody).get(numberofNotes).getText().contains(losText)) {
					tcConfig.updateTestReporter("TRIPNoteCentrePage", "validateNotesForNightsModification", Status.PASS,
							"Night Modification Info Present on the Notes Centre Page");
				}else{
					tcConfig.updateTestReporter("TRIPNoteCentrePage", "validateNotesForNightsModification", Status.FAIL,
							"Night Modification Info not Present on the Notes Centre Page");
				}
				break;
			}
		}
		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		navigateToMainTab(newWindow);
	}
}
