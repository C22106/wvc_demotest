package trip.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPReservationPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPReservationPage_Web.class);

	public TRIPReservationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected String reservationNumber;
	protected By enterReservationNumber = By.xpath("//input[@id = 'reservations-conf-number']");
	protected By searchResrvationButton = By.xpath("//button[@id = 'r-cn-search']");
	protected By totalReservations = By.xpath("//strong[@class='filter-bar__results']");
	protected By reservationDetailsPage = By.xpath("//header[contains(.,'RESERVATION DETAILS')]");
	protected By overlappingMsg = By.xpath("//p[@class='message__text']");
	protected By backToReservationPage = By.xpath("//div[@class='back-to']//span");

	/*
	 * Method: feedReservationNumber Description: feed Reservation Number Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void feedReservationNumber() {

		// reservationNumber = testData.get("ReservationNumber").trim();
		if (reservationNumber == null) {
			reservationNumber = TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
					.get(testData.get("AutomationID"));
		} else {
			try {
				reservationNumber = testData.get("ReservationNumber").trim();
			} catch (Exception e) {
				log.info("Error in getting reservation number");
			}
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, enterReservationNumber, "ExplicitLongWait");
		if (verifyObjectDisplayed(enterReservationNumber)) {
			fieldDataEnter(enterReservationNumber, reservationNumber);
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, searchResrvationButton, "ExplicitLongWait");
			clickElementJSWithWait(searchResrvationButton);
			checkLoadingSpinnerTRIP();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("TRIPReservationPage", "selectExistingReservation", Status.PASS,
					"Reservation Selected is: " + reservationNumber);

		} else {
			tcConfig.updateTestReporter("TRIPReservationPage", "selectExistingReservation", Status.FAIL,
					"Search Not Found");
		}
	}

	/*
	 * Method: reservationSelect Description: reservation Select Date: July/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void reservationSelect() throws Exception {
		waitUntilElementVisibleBy(driver, totalReservations, "ExplicitLongWait");
		if (verifyObjectDisplayed(totalReservations)) {
			tcConfig.updateTestReporter("ReservationsPage", "reservationSelect", Status.PASS,
					"Total Reservations Made: " + getElementText(totalReservations));
			clickElementJSWithWait(By
					.xpath("//table[@id = 'reservations-history']//span[@class = 'reservations-confirmation']/span[text() = '"
							+ reservationNumber + "']"));
			waitUntilElementVisibleBy(driver, reservationDetailsPage, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(reservationDetailsPage), "Failed to Select Reservation");
			tcConfig.updateTestReporter("TRIPReservationPage", "reservationSelect", Status.PASS,
					"Details Page Validated");
		} else if (getElementText(totalReservations).contains("0")) {
			tcConfig.updateTestReporter("ReservationsPage", "reservationSelect", Status.FAIL,
					"No Resrvations To Display");
		}
	}

	/**
	 * Method: verifyOverlappingAlertMsg Description: verify overlapping msg on
	 * reservation page Date: July/2020 Author: Kamalesh Changes By: NA
	 */

	public void verifyOverlappingAlertMsg() {
		clickElementBy(backToReservationPage);
		waitUntilElementVisibleBy(driver, enterReservationNumber, "ExplicitLongWait");
		getElementInView(getList(overlappingMsg).get(0));
		assertTrue(getList(overlappingMsg).size() == 3,
				"Overlapping Messages are not present on all the 3 reservations");
		tcConfig.updateTestReporter("TRIPReservationPage", "verifyOverlappingAlertMsg", Status.PASS,
				"Overlapping Messages are present on all the 3 reservations");
	}

	/**
	 * Method: verifyPPstatus Description: verify PP status on reservation page
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 */

	public void verifyPPstatus() {
		clickElementBy(backToReservationPage);
		waitUntilElementVisibleBy(driver, enterReservationNumber, "ExplicitLongWait");
		feedReservationNumber();
		assertTrue(
				verifyObjectDisplayed(By.xpath(
						"//span[.='" + reservationNumber + "']/ancestor::tr//span[@class='pointsProtection title']")),
				"Point Protection status is not present");
		tcConfig.updateTestReporter("TRIPReservationPage", "verifyOverlappingAlertMsg", Status.PASS,
				"Point Protection status is present on the reservation page for Reservations: " + reservationNumber);
	}
}
