package trip.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPProfileDashboardPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPProfileDashboardPage_Web.class);

	public TRIPProfileDashboardPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected String reservationNumber;
	protected By profileHeaderClick = By.xpath("//header[@id = 'landing-header']");
	protected By sectionMenuLinks = By.xpath("//div[@class = 'section-menu-links']");
	protected By selectReservationLink = By.xpath("//a[@id= 'section-menu-reservations']");
	protected By reservationsPage = By.xpath("//div[@id = 'reservations']");
	protected By listTotalNotesNow = By.xpath("//div[@id = 'recent-notes']/div[1]");
	protected By listNoteTitle = By.xpath("//div[@id = 'recent-notes']/div[1]/p[@class = 'title']");
	protected By listNoteText = By.xpath("//div[@id = 'recent-notes']/div[1]/p[@class = 'text']");
	protected By newVacation = By.xpath("//img[@title='New Vacation']");
	protected By fieldLocation = By.xpath("//div[@id='location']");
	protected By ownerProfile = By.id("ownerProfileID");
	protected By seeAllNotes = By.id("see-all-notes");
	protected By selectPointsLedgerLink= By.xpath("//a[@id='section-menu-point']");
	protected By pointsAdjustmentsBtn= By.xpath("//button[@id='points-adjustments-btn']");
	protected By selectMembershipLink= By.xpath("//a[@id='section-menu-membership']");
	protected By membershipHeader=By.xpath("//header/a[contains(.,'MEMBERSHIP')]");
	protected By houskeepingLabel=By.xpath("//th[text()='HOUSEKEEPING']");
	
	/*
	 * Method: clickNewVacationLink Description: Click New Vacation Link Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickNewVacationLink() {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, newVacation, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(newVacation), "New Vacation List Not Present");
		Actions actions = new Actions(driver);
		actions.moveToElement(getObject(newVacation)).click().perform();
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, fieldLocation, "ExplicitLongWait");
		if (verifyObjectDisplayed(fieldLocation)) {
			tcConfig.updateTestReporter("TRIPProfileDashboardPage", "clickNewVacationLink", Status.PASS,
					"Successfully Clicked on New Vacation Link");
		} else {
			tcConfig.updateTestReporter("TRIPProfileDashboardPage", "clickNewVacationLink", Status.FAIL,
					"Failed to click on New Vacation Link");
		}
	}

	/*
	 * Method: profileHeaderClick Description: profile Header Click Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void profileHeaderClick() throws Exception {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, profileHeaderClick, "ExplicitLongWait");
		getObject(profileHeaderClick).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(sectionMenuLinks),
				"Menu List Not Present, Maybe failed to clcik profile header");

	}
	
	
	/*
	 * Method: membershipClick Description: membershipClick
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void membershipClick() throws Exception {
		waitUntilElementVisibleBy(driver, selectMembershipLink, "ExplicitLongWait");
		mouseoverAndClick(getObject(selectMembershipLink));
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, membershipHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(membershipHeader), "Membership Page is not displayed");
		tcConfig.updateTestReporter("TRIPProfileDashboardPage", "membershipClick", Status.PASS,
				"Membership Page is Displayed");
	}
	
	
	
	/*
	 * Method: reservationLinkClick Description: Reservation Link Click Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void reservationLinkClick() throws Exception {
		waitUntilElementVisibleBy(driver, selectReservationLink, "ExplicitLongWait");
		mouseoverAndClick(getObject(selectReservationLink));
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, reservationsPage, "ExplicitLongWait");
		if (verifyObjectDisplayed(reservationsPage)) {
			tcConfig.updateTestReporter("TRIPProfileDashboardPage", "reservationLinkClick", Status.PASS,
					"Reservations Page is Displayed");
		} else {
			tcConfig.updateTestReporter("TRIPProfileDashboardPage", "reservationLinkClick", Status.FAIL,
					"Reservations page is not displayed");
		}
	}

	/*
	 * Method: checkModifyNigtsRecentNotes Description: check Modify Nigts Recent
	 * Notes Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkModifyNigtsRecentNotes() {
		String totalNightsModified = Long.toString(TRIPReservationDetailsPage_Web.displayedNight);
		//reservationNumber = testData.get("ReservationNumber").trim();
		if (reservationNumber == null) {
			reservationNumber = TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
					.get(testData.get("AutomationID"));
		} else {
			reservationNumber = testData.get("ReservationNumber");
		}
		if (verifyObjectDisplayed(listTotalNotesNow)) {
			String currentDateinFormat = getCurrentDateInSpecificFormat("MMM dd, YYYY").trim();
			if (getElementText(listNoteTitle).trim().contains(currentDateinFormat)) {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkModifyNigtsRecentNotes", Status.PASS,
						"Title Present with date as: " + currentDateinFormat);
			} else {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkModifyNigtsRecentNotes", Status.FAIL,
						"Title failed to match");
			}

			if (getElementText(listNoteText).trim().contains(totalNightsModified)
					&& getElementText(listNoteText).trim().contains(reservationNumber)) {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkModifyNigtsRecentNotes", Status.PASS,
						"Details Matched with the modification Nights Details");

			} else {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkModifyNigtsRecentNotes", Status.FAIL,
						"Modification Details validation failed");
			}

		}
	}

	/**
	 * Method: navigateToSeeAllNotes Description: Navigate to See All Notes Page
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void navigateToSeeAllNotes() {
		clickElementJSWithWait(ownerProfile);
		waitUntilElementVisibleBy(driver, profileHeaderClick, "ExplicitLongWait");
		clickElementBy(seeAllNotes);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(newWindow);
	}
	
	/*
	 * Method: validateUpdatedCheckInDate Description: validateUpdatedCheckInDate
	 * Date: July/2020 Author: MOnideep Changes By: NA
	 */
	public void pointsLedgerClick() throws Exception {
		waitUntilElementVisibleBy(driver, selectPointsLedgerLink, "ExplicitLongWait");
		mouseoverAndClick(getObject(selectPointsLedgerLink));
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, pointsAdjustmentsBtn, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(pointsAdjustmentsBtn), "Points Ledger Page is not displayed");
		tcConfig.updateTestReporter("TRIPProfileDashboardPage", "pointsLedgerClick", Status.PASS,
				"Points Ledger Page is Displayed");
	}
	/**
	 * Method: checkCancelRecentNotes Description: Check Cancel Reservation in Recent Notes
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkCancelRecentNotes() {
		reservationNumber = testData.get("ReservationNumber").trim();
		if (reservationNumber.isEmpty() || reservationNumber == null) {
			reservationNumber = TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
					.get(testData.get("AutomationID"));
		} else {
			reservationNumber = testData.get("ReservationNumber");
		}
		if (verifyObjectDisplayed(listTotalNotesNow)) {
			String currentDateinFormat = getCurrentDateInSpecificFormat("MMM dd, YYYY").trim();
			if (getElementText(listNoteTitle).trim().contains(currentDateinFormat)) {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkCancelRecentNotes", Status.PASS,
						"Title Present with date as: " + currentDateinFormat);
			} else {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkCancelRecentNotes", Status.FAIL,
						"Title failed to match");
			}

			if (getElementText(listNoteText).trim().contains("Cancelled reservation")
					&& getElementText(listNoteText).trim().contains(reservationNumber)) {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkCancelRecentNotes", Status.PASS,
						"Details Matched with the Cancelled Reservation Details");

			} else {
				tcConfig.updateTestReporter("TRIPProfileDashboardPage", "checkCancelRecentNotes", Status.FAIL,
						"Cancelled Reservation Details validation failed");
			}

		}
	}
	/*
	 * Method: validateHousekeeping Description:To validate Housekeeping label 
	 * Date: October/2020 Author: Prattusha Changes By: NA
	 */
	
	public void validateHousekeeping() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, houskeepingLabel, "ExplicitLongWait");
		if (verifyObjectDisplayed(houskeepingLabel)) {
			
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateHousekeeping", Status.PASS,
					"Housekeeping Label is Displayed");
	       
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateHousekeeping", Status.FAIL,
					"Cousekeeping Label is not Displayeds");
		}
	}

}
