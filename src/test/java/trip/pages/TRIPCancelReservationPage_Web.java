package trip.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPCancelReservationPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPCancelReservationPage_Web.class);

	public TRIPCancelReservationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By buttonReservationCancel = By.xpath("//div[contains(@id,'cancel-reservation')]//button[contains(@id,'cancel-reservation')]");
	protected By overlayCancelReservation = By.xpath("//button[contains(@class,'overlay-cancel-reservation')]");
	/*
	 * Method: clickCancelReservation Description: Click Cancel Reservation on Cancel Reservation Page Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickCancelReservation(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, buttonReservationCancel, "ExplicitLongWait");
		clickElementJSWithWait(buttonReservationCancel);
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, overlayCancelReservation, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(overlayCancelReservation), "Cancel Reservation Button not present on Cancel Reservation Page");
		tcConfig.updateTestReporter("TRIPTravelDetailsPage", "clickCancelReservation", Status.PASS,
				"Successfully Clicked Cancel reservation Button on Cancel reservation Page");
	}
	
	/*
	 * Method: clickCancelReservationOverlay Description: Click Cancel Reservation on Cancel Reservation Overlay Page Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickCancelReservationOverlay(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, overlayCancelReservation, "ExplicitLongWait");
		clickElementJSWithWait(overlayCancelReservation);
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	/*
	 * Method: checkCancelDB Description: Verify Cancel in DB Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkCancelDB() throws Exception{
		String bookingCheckInDate = convertDateFormat("MM-dd-yyyy", "yyyy-MM-dd", testData.get("CheckInDate")).toUpperCase();
		String bookingCheckOutDate = convertDateFormat("MM-dd-yyyy", "yyyy-MM-dd", testData.get("ModifyCheckOutDate")).toUpperCase();
		String query = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND gst.stat_name = 'CANCELLED' AND gct.class_name = 'CONFIRM RESERVATION' AND rap.created_by = 'SYSTEM' AND rer.res_num in ('"
				+ TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber.get(testData.get("AutomationID"))
				+ "')";
		DBConnectionOnly();
		String checkindate = ExecuteQueryAndFetchColumData(query, "CHK_IN_LOCAL_DATE").split(" ")[0].trim();
		String checkoutdate = ExecuteQueryAndFetchColumData(query, "CHK_OUT_LOCAL_DATE").split(" ")[0].trim();
		if (checkindate.equals(bookingCheckInDate)) {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.PASS,
					"DB Check In Date Validate Successfully");
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.FAIL,
					"DB Check In Date Validation Failed");
		}
		
		if (checkoutdate.equals(bookingCheckOutDate)) {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.PASS,
					"DB Check Out Date Validate Successfully");
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkCancelDB", Status.FAIL,
					"DB Check Out Date Validate Successfully");
		}
		
		closeDB();
	}
}
