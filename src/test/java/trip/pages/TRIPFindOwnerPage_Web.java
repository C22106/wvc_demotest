package trip.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPFindOwnerPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPFindOwnerPage_Web.class);

	public TRIPFindOwnerPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	protected By fieldMemberSearch = By.xpath("//input[@id = 'quickSearch']");
	protected By buttonMemberSearch = By.xpath("//button[@id='faoSubmit']");
	protected By memberSearchList = By.xpath("//div[@id = 'fao-results-list']");

	/*
	 * Method: searchForMember Description: Search For Member
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void searchForMember(){
		String Member_ID = testData.get("MemberNumber");
		getElementInView(fieldMemberSearch);
		Assert.assertTrue(verifyObjectDisplayed(fieldMemberSearch), "Member Search Field not Present");
		fieldDataEnter(fieldMemberSearch, Member_ID);
		tcConfig.updateTestReporter("TRIPFindOwnerPage", "searchForMember", Status.PASS,
				"Member Number Entered is: " + Member_ID);
	}
	
	/*
	 * Method: clickSearchButton Description: Click Member Search Button
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickSearchButton(){
		if (verifyObjectDisplayed(buttonMemberSearch)) {
			if (getObject(buttonMemberSearch).getAttribute("class").contains("disabledBtn")) {
				tcConfig.updateTestReporter("TRIPFindOwnerPage", "clickSearchButton", Status.FAIL,
						"Member Search Button is Disabled");
			}else{
				clickElementJSWithWait(buttonMemberSearch);
				tcConfig.updateTestReporter("TRIPFindOwnerPage", "clickSearchButton", Status.PASS,
						"Successfully Clicked On Member Search Button");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				checkLoadingSpinnerTRIP();
			}
		}else{
			tcConfig.updateTestReporter("TRIPFindOwnerPage", "clickSearchButton", Status.FAIL,
					"Member Search Button not Displayed");
		}
	}
	/*
	 * Method: selectMember Description: Select Member From List
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectMember() throws Exception {
		try {
			String MemberName = testData.get("MemberName").trim();
			waitUntilElementVisibleBy(driver, memberSearchList, "ExplicitLongWait");
			getElementInView(memberSearchList);
			tcConfig.updateTestReporter("TRIPFindOwnerPage", "selectMember", Status.PASS,
					"Member Search List is Present");
			clickElementJSWithWait(getObject(By.xpath("//p[@class='name' and contains(text(),'" + MemberName + "')]")));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinnerTRIP();
		} catch (Exception e) {
			log.info("Only one member present");
		}

	}
}
