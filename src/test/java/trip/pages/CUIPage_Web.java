package trip.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(CUIPage_Web.class);

	public CUIPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By usernameText = By.xpath("//input[@id='username']//following-sibling::span[contains(.,'Username*')]");
	protected By myAccountDropDown = By
			.xpath("//div[@class='global-navigation__wrapper']//button[contains(@class,'container')]");
	protected By dropDownLogOut = By.xpath(
			"//section[contains(@class,'desktop')]//div[@class='global-account__links']//a[contains(@class,'logout')]//span");
	protected By sectionReservationSummary = By.xpath("//section[@class = 'reservation-details-summary']");
	protected By linknavigateUpcomingVacation = By
			.xpath("//div[@class='accountNavigation']//a[contains(.,'Vacations')]");
	protected By textUpcomingVacations = By.xpath("//h1[text() = 'UPCOMING VACATIONS']");
	//protected By welcomeHeader = By.xpath("//h4[contains(.,'WELCOME')]");
	protected By welcomeHeader = By.xpath("//div[text()='WELCOME TO THE CLUB,']");
	protected By feedbackContainer = By.xpath("//div[@id = 'oo_invitation_prompt']");
	protected By FeedbackNoOption = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By covidAlertClose = By.xpath("//button[contains(@class,'setCookie') and contains(.,'Close')]");
	protected By bannerAlertClose = By.xpath("//button[contains(@class,'close-button')]");
	protected By cookieAccept = By.xpath("//button[contains(.,'Accept')]");
	protected By modificationHistoryList = By.xpath("//div[contains(@class,'holiday modification-history')]");
	protected By listdetailsmodifiedNights = By
			.xpath("//div[contains(@id,'add-nights')]//div[@role = 'tabpanel']//span");
	protected By checkOutDate = By.xpath("// h5[contains(.,'CHECK-OUT')]/following-sibling::p");
	protected By loginUsername = By.xpath("//input[@id = 'username']");
	protected By fieldUsername = By.xpath("//input[@id = 'username']");
	protected By fieldPassword = By.xpath("//input[@id = 'password']");
	protected By loginButton = By.xpath("//button[text()='Login']");
	protected By modalHeader = By
			.xpath("(//h1[contains(.,'GET YOU ON VACATION')] | //div[contains(@id,'loginHeadline')])");

	/*
	 * Method: validateVIPUpgradeInModificationHistory Description:
	 * Date:June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateModifiedNightsDate() {
		String modifyNightsPresence = null;
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
					.size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(
						By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
								+ "]//a[contains(@id,'accordion-label')]/div"));
				if (verifyObjectDisplayed(eachModificationTypeList)) {
					String modificationType = getElementText(eachModificationTypeList);
					if (modificationType.contains("Add Nights")) {
						getElementInView(eachModificationTypeList);
						String modifiedDate = getObject(By.xpath("//div[contains(@id,'opt-in-out')]//a//p/span"))
								.getText().trim();
						clickElementJSWithWait(eachModificationTypeList);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						if (modifiedDate.isEmpty() || modifiedDate == null) {
							tcConfig.updateTestReporter("CUIPage", "validateModifiedNightsInModificationHistory",
									Status.FAIL, "Error Validating Modification History Content for index: "
											+ listmodificationiterator);
						} else {
							tcConfig.updateTestReporter("CUIPage", "validateModifiedNightsInModificationHistory",
									Status.PASS, "" + modificationType + " Modified on: " + modifiedDate);
							modifyNightsPresence = "Y";
							clickElementJSWithWait(eachModificationTypeList);
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							break;
						}
					}

				} else {
					tcConfig.updateTestReporter("CUIPage", "validateModifiedNightsInModificationHistory",
							Status.FAIL, "Modification Type not Present");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUIPage", "validateModifiedNightsInModificationHistory", Status.FAIL,
					"No modification history");
		}
		if (modifyNightsPresence == "Y") {
			tcConfig.updateTestReporter("CUIPage", "validateModifiedNightsInModificationHistory", Status.PASS,
					"Modified Nights present in Modification history Table");
		} else {
			tcConfig.updateTestReporter("CUIPage", "validateModifiedNightsInModificationHistory", Status.FAIL,
					"Failed while Validating Modified Nights Modification Type");
		}
	}

	/*
	 * Method: detailsInModifiedNights Description: details In Modified Nights
	 * Date:June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void detailsInModifiedNights() {
		for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList)
				.size(); listmodificationiterator++) {
			WebElement eachModificationTypeList = getObject(
					By.xpath("//div[contains(@class,'holiday modification-history')][" + listmodificationiterator
							+ "]//a[contains(@id,'accordion-label')]/div"));
			if (verifyObjectDisplayed(eachModificationTypeList)) {
				String modificationType = getElementText(eachModificationTypeList);
				if (modificationType.contains("Add Nights")) {
					getElementInView(eachModificationTypeList);
					clickElementJSWithWait(eachModificationTypeList);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					for (int modifiedDetailsiterator = 0; modifiedDetailsiterator < getList(listdetailsmodifiedNights)
							.size(); modifiedDetailsiterator++) {
						String detailsModified = getElementText(
								getList(listdetailsmodifiedNights).get(modifiedDetailsiterator)).trim();
						if (detailsModified.isEmpty() || detailsModified == null) {
							tcConfig.updateTestReporter("CUIPage", "detailsInModifiedNights", Status.FAIL,
									"Error Validating Modification History Content for index: "
											+ modifiedDetailsiterator);
						} else {
							tcConfig.updateTestReporter("CUIPage", "detailsInModifiedNights", Status.PASS,
									"Details Present as: " + detailsModified);
						}
					}

					break;
				}
			} else {
				tcConfig.updateTestReporter("CUIPage_Web", "detailsInModifiedNights", Status.FAIL,
						"Modification Type not Present");
			}
		}
	}

	/*
	 * Method: validateModifiedCheckOutDate Description:validate Modified Check
	 * Out Date Date:June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateModifiedCheckOutDate() throws Exception {
		String modifiedCheckOutDate = convertDateFormat("MM-dd-yyyy", "MM/dd/yyyy", testData.get("ModifyCheckOutDate"));
		assertTrue(getElementText(checkOutDate).contains(modifiedCheckOutDate), "Check Out Date Not Match");
		tcConfig.updateTestReporter("CUIPage_Web", "validateModifiedCheckOutDate", Status.PASS,
				"Modified date reflected in Reservation Details Page");

	}

	/*
	 * Method: setUserName Description: Set Username with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLongWait");
		getElementInView(modalHeader);
		// Username entry
		String strUserName = testData.get("CUI_username");
		fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUIPage", "setUserName", Status.PASS,
				"Username is entered for CUI Login as " + strUserName);
	}

	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setPassword() throws Exception {
		// password data
		String password = testData.get("CUI_password");
		// decodeed password
		String decodedPassword = decodePassword(password);
		fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("CUIPage_Web", "setPassword", Status.PASS, "Entered password for CUI login ");
	}

	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void launchApplication(){
		getBrowserName();
		navigateToURL(tcConfig.getConfig().get("NewCUIUrl"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(loginUsername));
		tcConfig.updateTestReporter("CUIPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}

	/*
	 * Method: acceptCookie Description: Accept Cookie Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void acceptCookie() {
		waitUntilElementVisibleBy(driver, cookieAccept, "ExplicitMedWait");
		if (verifyObjectDisplayed(cookieAccept)) {
			clickElementBy(cookieAccept);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("CUIPage", "acceptCookie", Status.PASS, "Successfully accepted Cookie");
			closeBannerAlert();
			closeCovidAlert();
		}
	}

	/*
	 * Method: closeCovidAlert Description: Close Covid Date: Apr/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void closeCovidAlert() {
		try {
			WebElement covidClose = getList(covidAlertClose).get(0);
			if (verifyObjectDisplayed(covidClose)) {
				clickElementBy(covidClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CUIPage", "closeAlert", Status.PASS, "Successfully Closed Alert");
			}

		} catch (Exception e) {
			log.error("Covid Message Not Present");

		}
	}

	/*
	 * Method: loginCTAClick Description: Click On login cta Date: Feb/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void loginCTAClick() throws Exception {
		waitUntilElementVisibleBy(driver, loginButton, "ExplicitLongWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			acceptAlert();
			checkFeedbackContainer();
			waitUntilElementVisibleBy(driver, welcomeHeader, "ExplicitLongWait");
			getElementInView(welcomeHeader);

			Assert.assertTrue(verifyObjectDisplayed(welcomeHeader));

			tcConfig.updateTestReporter("CUIPage", "loginCTAClick", Status.PASS, "User Login successful");

		} else {
			tcConfig.updateTestReporter("CUIPage", "loginCTAClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * Method: checkFeedbackContainer Description: Feedbac No Date: Apr/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void checkFeedbackContainer() {
		try {
			waitUntilElementVisibleBy(driver, feedbackContainer, "ExplicitLowWait");
			if (verifyObjectDisplayed(feedbackContainer)) {
				getObject(FeedbackNoOption).click();

			}
		} catch (Exception e) {
			log.error("No feedback Container");
		}
	}

	/*
	 * Method: navigateToUpcomingVacation Description:Navigate to Upcoming
	 * Vacations Page Date field Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void navigateToUpcomingVacation() {
		waitUntilElementVisibleBy(driver, linknavigateUpcomingVacation, "ExplicitLongWait");
		clickElementJSWithWait(linknavigateUpcomingVacation);
		waitUntilElementVisibleBy(driver, textUpcomingVacations, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(textUpcomingVacations));
		tcConfig.updateTestReporter("CUIPage", "navigateToUpcomingVacation", Status.PASS,
				"Successfully Navigated to Upcoming Vacations Page");
	}

	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations
	 * Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadAllReservations() {
		By modifyReservation = By.xpath("//p[text() = '"
				+ TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber.get(testData.get("AutomationID"))
				+ "']");
		if (verifyObjectDisplayed(modifyReservation)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			do {
				sendKeyboardKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} while (!verifyObjectDisplayed(modifyReservation));
		}
	}

	/*
	 * Method: selectReservationDetails Description:Select Reservation Details
	 * Link Date: June/2020 Author: Abhijeet Changes By: NA
	 */
	public void selectReservationDetails() {
		By detailsReservation = By.xpath("//p[text() = '"
				+ TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber.get(testData.get("AutomationID"))
				+ "']/ancestor::div[contains(@class,'reservation-info')]//div[contains(@class,'details-buttons')]/a[contains(text(),'Details')]");
		getElementInView(detailsReservation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(detailsReservation);
		waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(sectionReservationSummary));
		tcConfig.updateTestReporter("CUIPage", "selectReservationDetails", Status.PASS,
				"Successfully Opened Reservation Details Modal");
		waitUntilElementInVisible(driver, loadingSpinnerCUI, "ExplicitLongWait");
	}

	/*
	 * Method: logOutApplicationViaDropDown Description: Account Log out via
	 * dropdown page Validations Date: Mar/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void logOutApplicationViaDropDown() {
		if (verifyObjectDisplayed(myAccountDropDown)) {
			clickElementBy(myAccountDropDown);
			waitUntilElementVisibleBy(driver, dropDownLogOut, "ExplicitLongWait");
			getElementInView(dropDownLogOut);
			clickElementBy(dropDownLogOut);
			waitUntilElementVisibleBy(driver, usernameText, "ExplicitLongWait");
			tcConfig.updateTestReporter("CUIPage", "logOutApplication", Status.PASS, "User Logged Out");
		} else if (verifyObjectDisplayed(loginButton)) {
			tcConfig.updateTestReporter("CUIPage", "logOutApplication", Status.PASS, "No Need to Log Out");
		} else {
			tcConfig.updateTestReporter("CUIPage", "logOutApplication", Status.FAIL, "Logging Out Failed");
		}
	}

}
