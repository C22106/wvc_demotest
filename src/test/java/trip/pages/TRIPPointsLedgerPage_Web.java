package trip.pages;

import static org.testng.Assert.assertTrue;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPPointsLedgerPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPPointsLedgerPage_Web.class);

	public TRIPPointsLedgerPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By pointsAdjustmentsBtn = By.xpath("//button[@id='points-adjustments-btn']");
	protected By resortField = By.xpath("//input[@id = 'resort-name']");
	protected By radioDeduction = By.xpath("//input[@value='pa-deduction']");
	protected By transactiontype = By.xpath("//div[@id='pa-transaction-type']");
	protected By accountCorrection = By.xpath("//div[text()='Account Correction']");
	protected By reasonCode = By.xpath("//div[@id='pa-reason-code']");
	protected By accountAdjustment = By.xpath("//div[text()='Account Adjustment']");
	protected By contractNumber = By.xpath("//div[@id='pa-contract-number']");
	protected By userYearSelection = By.xpath("//div[@id='pa-use-year']");
	protected By firstUserYear = By.xpath("//div[@id='pa-use-year']/div[@style='display: block;']/div");
	protected By pointsAvailable = By.xpath("//fieldset[@id='pa-points']//span/span");
	protected By hkValueAvailable = By.xpath("//fieldset[@id='pa-housekeeping']//span/span");
	protected By rtValueAvailable = By.xpath("//fieldset[@id='pa-reservation-transactions']//span/span");
	protected By pointsInput = By.xpath("//input[@id='pa-points']");
	protected By hkInput = By.xpath("//input[@id='pa-housekeeping']");
	protected By rtInput = By.xpath("//input[@id='pa-reservation-transactions']");
	protected By contentsInput = By.xpath("//textarea[@id='pa-content']");
	protected By reserveTransaction = By.xpath("//input[@id='pa-reservation-transactions']");
	protected By applyBtn = By.xpath("//button[@id='pa-apply-btn']");
	protected By availablePoints = By.xpath("//th[text()='Available']/following-sibling::td");
	protected By pointsLedgerLink = By.id("section-menu-point");
	protected By pointsAdjustmentButton = By.id("points-adjustments-btn");
	protected By transacationTab = By.id("transactions-tab");
	protected By filterOption = By.id("addFilters");
	protected By resortNameField = By.id("resort-name");
	protected By modifyFilter = By.xpath("//label[@for='Resort Reservation-Modify']");
	protected By applyFilterButton = By.xpath("//button[text()='APPLY FILTERS']");
	protected By activepage = By.xpath("//tr[contains(@class,'transactions__row')]");
	protected static HashMap<String, String> nights = new HashMap<String, String>();
	protected static HashMap<String, String> currentUseYear = new HashMap<String, String>();
	protected static HashMap<String, String> nextUseYear = new HashMap<String, String>();
	protected static HashMap<String, String> nextUseYearAfterCancel = new HashMap<String, String>();
	protected By noMoreReservation = By
			.xpath("//p[text() = 'There are no more transactions to display for this Owner']");
	
	/*
	 * Method: navigateToPointsLedgerPage Description: Navigate To Points Ledger
	 * page Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void navigateToPointsLedgerPage() throws Exception {
		waitUntilElementVisibleBy(driver, pointsLedgerLink, "ExplicitLongWait");
		mouseoverAndClick(getObject(pointsLedgerLink));
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, pointsAdjustmentButton, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(pointsAdjustmentButton), "Failed To Navigate To Points Ledger Page");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage_Web", "navigateToPointsLedgerPage", Status.PASS,
				"Points Ledger Page is Displayed");
	}

	/*
	 * Method: clickTransactionTab Description: Click On Transaction Tab page
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clickTransactionTab() {
		clickElementBy(transacationTab);
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, filterOption, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(filterOption), "Failed To Navigate To Transaction Tab");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage_Web", "clickTransactionTab", Status.PASS,
				"Transaction Tab Navigated");
	}

	/*
	 * Method: clickApplyFilter Description: Click On Apply Filter * Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clickApplyFilter() {
		clickElementBy(filterOption);
		waitUntilElementVisibleBy(driver, resortNameField, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(resortNameField), "Failed To Click Filters");

	}

	/*
	 * Method: clickApplyButton Description: Click On Apply Filter Button* Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clickApplyButton() {
		clickElementBy(applyFilterButton);
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: validatePointsRefundBorrowed Description: validate Points Refund
	 * Borrowed* Date: July/2020 Author: Monideep Changes By: NA
	 */
	public void validatePointsRefundBorrowed() {

		Assert.assertTrue(
				verifyObjectDisplayed(
						By.xpath("//tr[contains(.,'Confirmation: " + testData.get("ReservationNumber").trim()
								+ "')]/td[4]/div/strong[not(text()='-') and contains(.,'+')]")),
				"Points refund not present in point ledger");

		String refundedPoints = getElementText(
				By.xpath("//tr[contains(.,'Confirmation: " + testData.get("ReservationNumber").trim()
						+ "')]/td[4][contains(.,'Borrowed')]/div/strong[not(text()='-') and contains(.,'+')]"))
								.replace("+", "").replace(",", "");

		Assert.assertTrue(refundedPoints.equals(testData.get("PointsBorrowed")),
				"Refunded points doesn't match, refund provied : " + refundedPoints);

		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "validatePointsRefundBorrowed", Status.PASS,
				"Points refund reflected successfully in Points ledger");

	}

	/*
	 * Method: applyFilterSelect Description: Click On Apply Some Filter* Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void applyFilterSelect(String iterator) {
		String filterName = testData.get("ApplyDetailsFilter" + iterator).trim();
		By clickFilter = By.xpath("//label[@for='" + filterName + "']");
		clickElementBy(clickFilter);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: checkNoHouseKeeping Description: Check No Housekeeping* Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkNoHouseKeeping() {
		String reservationNumber = TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
				.get(testData.get("AutomationID"));
		By reservationHouseKeeping = By.xpath("// span[contains(.,'Confirmation: " + reservationNumber
				+ "')]/ancestor::tr//strong[contains(@class,'data-t-hk')]");
		Assert.assertTrue(verifyObjectDisplayed(reservationHouseKeeping), "Reservation Not Present");
		getElementInView(getList(reservationHouseKeeping).get(0));
		Assert.assertTrue(getList(reservationHouseKeeping).get(0).getText().equals("-"),
				"Housekeeping Charges Displayed");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "checkNoHouseKeeping", Status.PASS,
				"Housekeeping Charges not Displayed");
	}

	/*
	 * Method: fetchCurrentYearPoints Description: Fetch the Current Use Year
	 * Date: July/2020 Author: Kamalesh Changes By: Unnat Jain
	 */
	public void fetchCurrentYearPoints(String iterator) {
		waitUntilElementVisibleBy(driver, pointsAdjustmentButton, "ExplicitLongWait");
		String getUserYear = testData.get("CurrentUseYear");
		currentUseYear.put(iterator,
				getElementText(By
						.xpath("//th[text()='" + getUserYear + "']/following-sibling::td[contains(@class,'ptsAvail')]"))
								.trim());
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "compareCurrentYearPoints", Status.PASS,
				"Current Use Year Points are displayed as: " + getElementText(By
						.xpath("//th[text()='" + getUserYear + "']/following-sibling::td[contains(@class,'ptsAvail')]"))
								.trim());
	}

	/*
	 * Method: compareCurrentYearPoints Description: compare Current User Year
	 * before and after modifications Date: July/2020 Author: Kamalesh Changes
	 * By: Unnat Jain
	 */
	public void compareCurrentYearPoints(String firstIterator, String secondIterator) {
		String strfirstIterator = nights.get(firstIterator).trim();
		String strsecondIterator = nights.get(secondIterator).trim();
		if (strfirstIterator.contains(",")) {
			strfirstIterator = strfirstIterator.replace(",", "");
		}
		if (strsecondIterator.contains(",")) {
			strsecondIterator = strsecondIterator.replace(",", "");
		}
		int previousPointsValue = Integer.parseInt(strfirstIterator);
		int updatedPointsValue = Integer.parseInt(strsecondIterator);
		Assert.assertTrue(
				previousPointsValue - updatedPointsValue == Integer
						.parseInt(TRIPReservationDetailsPage_Web.strModifyingCost),
				"Curret Use Year Points Not Updated after modifications");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "compareCurrentYearPoints", Status.PASS,
				"Curret Use Year Points are modified successfully");
	}

	/*
	 * Method: clickPointsAdjustment Description: clickPointsAdjustment Date:
	 * July/2020 Author: Monideep Changes By:
	 */
	public void clickPointsAdjustment() {

		clickElementBy(pointsAdjustmentsBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "clickPointsAdjustment", Status.PASS,
				"Points adjustment button has been clicked");
	}

	/*
	 * Method: enterAdjustmentDetails Description: enterAdjustmentDetails Date:
	 * July/2020 Author: Monideep Changes By: Kamalesh
	 */
	public void enterAdjustmentDetails(String adjType, String adjAmount) {

		if (adjType.equalsIgnoreCase("Deduct")) {
			clickElementBy(radioDeduction);
			checkLoadingSpinnerTRIP();

		}

		clickElementBy(transactiontype);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(accountCorrection);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(reasonCode);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(accountAdjustment);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(userYearSelection);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(firstUserYear);

		String pointsAvailableValue = getElementText(pointsAvailable).replace(",", "").trim();
		sendKeysBy(pointsInput, pointsAvailableValue);

		String hkAvailableValue = getElementText(hkValueAvailable).replace(",", "").trim();
		sendKeysBy(hkInput, hkAvailableValue);

		String rtAvailableValue = getElementText(rtValueAvailable).replace(",", "").trim();
		sendKeysBy(rtInput, rtAvailableValue);

		sendKeysBy(contentsInput, "test");
		clickElementBy(reserveTransaction);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementBy(applyBtn);
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String pointsAvailableValueAfterAdjustment = getElementText(availablePoints).replace(",", "").trim();

		if (adjAmount.equalsIgnoreCase("0")) {
			assertTrue(pointsAvailableValueAfterAdjustment.equals("0"), "Point Adjustment failed");
			tcConfig.updateTestReporter("TRIPPointsLedgerPage", "clickPointsAdjustment", Status.PASS,
					"Points adjustment has been successfully");
		}

	}

	/*
	 * Method: getHouskepingPointsforSpeceficUseYear Description: get Houskeping
	 * Points for Specific Use Year Date: July/2020 Author: Abhijeet Changes By:
	 */
	
	public int getHouskepingPointsforSpeceficUseYear(String UseYear) {
		String reservationNumber = testData.get("ReservationNumber").trim();
		if (reservationNumber.isEmpty() || reservationNumber == null) {
			reservationNumber = TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
					.get(testData.get("AutomationID"));
		} else {
			reservationNumber = testData.get("ReservationNumber").trim();
		}
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		if (verifyObjectDisplayed(noMoreReservation)) {
			System.out.println("No more reservation Text Found");
		} else {
			do {
				sendKeyboardKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				checkLoadingSpinnerTRIP();
			} while (!verifyObjectDisplayed(noMoreReservation));
		}
		By housekeepingPoint = By.xpath("//span[contains(.,'Confirmation: " + reservationNumber
				+ "')]/ancestor::tr//td[contains(.,'" + UseYear + "')]/..//strong[contains(@class,'data-t-hk')]");
		waitUntilElementVisibleBy(driver, housekeepingPoint, "ExplicitLongWait");
		getElementInView(housekeepingPoint);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getElementText(housekeepingPoint).trim().contains("-")) {
			int valueHousekeeping = Integer.parseInt(getElementText(housekeepingPoint).trim().split("-")[1]);
			return valueHousekeeping;
		} else {
			int valueHousekeeping = Integer.parseInt(getElementText(housekeepingPoint).trim().split("\\+")[1]);
			return valueHousekeeping;
		}

	}

	/*
	 * Method: housekeepingUsedFromCurrentUseYear Description: housekeeping Used
	 * From Current Use Year Date: July/2020 Author: Abhijeet Changes By:
	 */
	public void housekeepingUsedFromCurrentUseYear(String UseYear) {
		int currentHKUsed = getHouskepingPointsforSpeceficUseYear(UseYear);
		if (TRIPReservationDetailsPage_Web.availableHK == currentHKUsed) {
			tcConfig.updateTestReporter("TRIPPointsLedgerPage", "housekeepingUsedFromCurrentUseYear", Status.PASS,
					"Available Housekeping point while modifying nights and Houskeeping used displayed in Point Ledger Page matched");
		} else {
			tcConfig.updateTestReporter("TRIPPointsLedgerPage", "housekeepingUsedFromCurrentUseYear", Status.FAIL,
					"Available Housekeping point while modifying nights and Houskeeping used displayed in Point Ledger Page did not matched");
		}
	}

	/*
	 * Method: housekeepingBorrowedfromUseYear Description: house keeping
	 * Borrowed from UseYear Date: July/2020 Author: Abhijeet Changes By:
	 */
	public void housekeepingBorrowedfromUseYear(String UseYear) {
		int housekeepingborrowed = Integer.parseInt(testData.get("HKBorrowOnly"));
		int borrowedHK = getHouskepingPointsforSpeceficUseYear(UseYear);
		if (housekeepingborrowed == borrowedHK) {
			tcConfig.updateTestReporter("TRIPPointsLedgerPage", "housekeepingBorrowedfromUseYear", Status.PASS,
					"HouseKeeping Borrowed Matched");
		} else {
			tcConfig.updateTestReporter("TRIPPointsLedgerPage", "housekeepingBorrowedfromUseYear", Status.FAIL,
					"HouseKeeping Borrowed did not Matched");
		}
	}

	/*
	 * Method: selectResortFilter Description: Select Filter For Resort Date:
	 * July/2020 Author: Abhijeet Changes By:
	 */
	public void selectResortFilter() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String Resort = testData.get("ResortFilter").trim();
		int filterResort = 0;
		String resortarr[] = Resort.split(";");
		for (int resortIterator = 0; resortIterator < resortarr.length; resortIterator++) {
			fieldDataEnter(resortField, resortarr[resortIterator]);
			WebElement click = getObject(By.xpath("//span[text() = '" + resortarr[resortIterator] + "']"));
			getElementInView(click);
			clickElementJSWithWait(click);
			tcConfig.updateTestReporter("CUISearchPage", "selectUnitType", Status.PASS,
					"Clicked on Unit Type and selected resort is: " + resortarr[resortIterator]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			filterResort = filterResort + 1;
		}
		validateCheckBoxClicked(filterResort, resortarr, "Resort");
	}

	/*
	 * Method: validateCheckBoxClicked Description: Validate Check Box Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateCheckBoxClicked(int totalClicked, String[] totalSelected, String validationFor) {

		if (totalClicked == totalSelected.length) {
			tcConfig.updateTestReporter("CUISearchPage", "selectExperience", Status.PASS,
					validationFor + " option clicked successfully");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "selectExperience", Status.FAIL,
					"Failed while clicking on " + validationFor + " option");
		}
	}

	
	/*
	 * Method: validateHKReimbersedAfterCancel Description: validate HK Reimbursed After Cancel Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHKReimbersedAfterCancel(String UseYear) {
		int currentHKUsed = getHouskepingPointsforSpeceficUseYear(UseYear);
		if (TRIPReservationDetailsPage_Web.hkUsedValue == currentHKUsed) {
			tcConfig.updateTestReporter("TRIPPointsLedgerPage", "validateHKReimbersedAfterCancel", Status.PASS,
					"Houskeeping used during reservation and reimbersed matched");
		} else {
			tcConfig.updateTestReporter("TRIPPointsLedgerPage", "validateHKReimbersedAfterCancel", Status.FAIL,
					"Houskeeping used during reservation and reimbersed did not matched");
		}
	}

	/*
	 * Method: futureYearPointsAfterCancellation Description: compare Current
	 * User Year before and after cancelation Date: July/2020 Author: Kamalesh
	 * Changes By: Unnat Jain
	 */
	public void futureYearPointsAfterCancellation(String firstIterator, String secondIterator) {
		String strfirstIterator = nextUseYear.get(firstIterator).trim();
		String strsecondIterator = nextUseYear.get(secondIterator).trim();
		if (strfirstIterator.contains(",")) {
			strfirstIterator = strfirstIterator.replace(",", "");
		}
		if (strsecondIterator.contains(",")) {
			strsecondIterator = strsecondIterator.replace(",", "");
		}
		int previousPointsValue = Integer.parseInt(strfirstIterator);
		int updatedPointsValue = Integer.parseInt(strsecondIterator);
		Assert.assertTrue(previousPointsValue + Integer.parseInt(testData.get("PointsBorrow")) == updatedPointsValue,
				"Future Use Year Points Not Updated after modifications");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "compareFutureYearPoints", Status.PASS,
				"Future Use Year Points are modified successfully");
	}

	/*
	 * Method: fetchNextYearPoints Description: Fetch the Next Use Year Date:
	 * July/2020 Author: Kamalesh Changes By: Unnat Jain
	 */
	public void fetchNextYearPoints(String iterator) {
		waitUntilElementVisibleBy(driver, pointsAdjustmentButton, "ExplicitLongWait");
		String getUserYear = testData.get("NextUseYear");
		nextUseYear.put(iterator,
				getElementText(By
						.xpath("//th[text()='" + getUserYear + "']/following-sibling::td[contains(@class,'ptsAvail')]"))
								.trim());
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "fetchNextYearPoints", Status.PASS,
				"Future Use Year Points are displayed as: " + getElementText(By
						.xpath("//th[text()='" + getUserYear + "']/following-sibling::td[contains(@class,'ptsAvail')]"))
								.trim());
	}

	/*
	 * Method: checkPointsDeducted Description: Check Points Deducted* Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkPointsDeducted() {
		// String reservationNumber =
		// TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber;
		String reservationNumber = TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
				.get(testData.get("AutomationID"));
		By reservationPoints = By.xpath("// span[contains(.,'Confirmation: " + reservationNumber
				+ "')]/ancestor::tr//strong[contains(@class,'data-t-pts')]");
		Assert.assertTrue(verifyObjectDisplayed(reservationPoints), "Reservation Not Present");
		getElementInView(getList(reservationPoints).get(0));
		String strTemp = getList(reservationPoints).get(0).getText();
		if (strTemp.contains(",")) {
			strTemp = strTemp.replace(",", "");
		}
		Assert.assertTrue(strTemp.contains(testData.get("PointsBorrow")), "Deducted Points not Displayed");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "checkPointsDeducted", Status.PASS,
				"Deducted Points Displayed");
	}

	/*
	 * Method: applyFilterSelect Description: Click On Apply Some Filter* Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void applyFilterSelect() {
		String filterName = testData.get("ApplyDetailsFilter").trim();
		By clickFilter = By.xpath("//label[@for='" + filterName + "']");
		clickElementBy(clickFilter);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: compareFutureYearPoints Description: compare Current User Year
	 * before and after modifications Date: July/2020 Author: Kamalesh Changes
	 * By: Unnat Jain
	 */
	public void compareFutureYearPoints(String firstIterator, String secondIterator) {
		String strfirstIterator = nextUseYear.get(firstIterator).trim();
		String strsecondIterator = nextUseYear.get(secondIterator).trim();
		if (strfirstIterator.contains(",")) {
			strfirstIterator = strfirstIterator.replace(",", "");
		}
		if (strsecondIterator.contains(",")) {
			strsecondIterator = strsecondIterator.replace(",", "");
		}
		int previousPointsValue = Integer.parseInt(strfirstIterator);
		int updatedPointsValue = Integer.parseInt(strsecondIterator);
		Assert.assertTrue(previousPointsValue - updatedPointsValue == Integer.parseInt(testData.get("PointsBorrow")),
				"Future Use Year Points Not Updated after modifications");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "compareFutureYearPoints", Status.PASS,
				"Future Use Year Points are modified successfully");
	}

	/*
	 * Method: compareinsufficientCurrentYearPoints Description: compare In
	 * SufficientCurrent User Year before and after modifications Date:
	 * July/2020 Author: Kamalesh Changes By: Unnat Jain
	 */
	public void compareInsufficientCurrentYearPoints(String firstIterator, String secondIterator) {
		String strfirstIterator = currentUseYear.get(firstIterator).trim();
		String strsecondIterator = currentUseYear.get(secondIterator).trim();
		if (strfirstIterator.contains(",")) {
			strfirstIterator = strfirstIterator.replace(",", "");
		}
		if (strsecondIterator.contains(",")) {
			strsecondIterator = strsecondIterator.replace(",", "");
		}
		int previousPointsValue = Integer.parseInt(strfirstIterator);
		int updatedPointsValue = Integer.parseInt(strsecondIterator);
		Assert.assertTrue(
				previousPointsValue - updatedPointsValue == Integer
						.parseInt(TRIPReservationDetailsPage_Web.strAvailablePoints),
				"Curret Use Year Points Not Updated after modifications");
		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "compareCurrentYearPoints", Status.PASS,
				"Curret Use Year Points are modified successfully");
	}
	
	/*
	 * Method: validateHKRefund Description: validate HK Refund
	 * Borrowed* Date: July/2020 Author: Monideep Changes By: NA
	 */
	
	public void validateHKRefund() {

		Assert.assertTrue(
				verifyObjectDisplayed(By.xpath("//tr[contains(.,'Confirmation: "
						+ testData.get("ReservationNumber").trim() + "')]/td[5]/div/strong[not(text()='-')]")),
				"House keeping refund not present in point ledger");

		String refundedPoints = getElementText(
				By.xpath("//tr[contains(.,'Confirmation: " + testData.get("ReservationNumber").trim()
						+ "')]/td[5][contains(.,'Borrowed')]/div/strong[not(text()='-')]")).replace("+", "");

		Assert.assertTrue(refundedPoints.equals(testData.get("BorrowedHKPoints")),
				"Refunded points doesn't match, refund provied : " + refundedPoints);

		tcConfig.updateTestReporter("TRIPPointsLedgerPage", "validateHKRefund", Status.PASS,
				"House keeping refund reflected successfully in Points ledger");

	}

	

}
