package trip.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPReservationDetailsPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPReservationDetailsPage_Web.class);

	public TRIPReservationDetailsPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	public static int hkUsedValue;
	public static int modifyPrice;
	protected TRIPTravelDetailsPage_Web travelDetailsPage_Web = new TRIPTravelDetailsPage_Web(tcConfig);
	protected By modifyPriceText = By.xpath("//span[@id = 'sum-points-required']");
	protected By usedHousekepingValue = By
			.xpath("(//strong[text() = 'HOUSEKEEPING']/ancestor::table//div[text() = 'USED'])/span");
	protected By buttonCancel = By.xpath("//button[contains(@id,'cancel-reservation')]");
	protected By buttonReservationCancel = By
			.xpath("//div[contains(@id,'cancel-reservation')]//button[contains(@id,'cancel-reservation')]");
	protected By modifyNights = By.xpath("//img[@id='rd-modify-travel-dates']");
	protected By modifyHeader = By.xpath("//header[contains(@class,'modify-header')]");
	protected By buttonNext = By.xpath("//button[@id = 'rd-mtd-checkInName-btn']");
	protected By modifyMessage = By.xpath("//span[@id = 'rd-reservation-message']");
	protected By sectionModificationHistory = By.xpath("//div[@id = 'rd-modification-history']");
	protected By modifyingCost = By.id("pts-cost");
	protected By checkInDate = By.id("rd-check-in-date");
	protected By checkOutDate = By.id("rd-checkout-date");
	protected By totalNights = By.id("rd-nights");
	protected By reservationDetailsPage = By.xpath("//header[contains(.,'RESERVATION DETAILS')]");
	protected By modifyReservationType = By.id("rd-modify-reservation-type");
	protected By modifyReservationTypeHeader = By.xpath("//header[contains(.,'MODIFY RESERVATION TYPE')]");
	protected By tpiRadio = By.xpath("//span[contains(.,'TPI')]/preceding-sibling::input");
	protected By noneRadio = By.xpath("//span[contains(.,'NONE')]/preceding-sibling::input");
	protected By extraHolidaysRadio = By.xpath("//span[contains(.,'EXTRA HOLIDAY')]/preceding-sibling::input");
	protected By saveMRTButton = By.id("rd-mrt-save");
	//protected By allCalendarDates = By.xpath("//div[@id='calendar']//td/span[@class='numerical-day']");
	protected By allCalendarDates = By.xpath("//div[@id='calendar']//td/span[@class='numerical-day']//parent::td");
	protected By backToDetailsPage = By.xpath("//span[contains(.,'BACK TO RESERVATION DETAIL')]");
	protected By allAvailableDates = By.xpath("//td/span[@not-available='false']");
	protected By fixedWeekBookedOverlay = By.xpath("//div[contains(@style,'block')]//div[@class='overlay-message']");
	protected By overlayMessage = By.id("overlay-text");
	protected By overlayOkButton = By.xpath("//button[@id='overlay-primary' and contains(.,'OK')]");
	protected By rentedPoints = By.xpath("//td//div[contains(.,'RENTED')]/span");
	protected By ownerProfile = By.id("ownerProfileID");
	protected By resortAlert = By.id("modifyAlertMeg");
	protected static Long displayedNight;
	protected static String strModifyingCost;
	protected By availablePoints = By.id("pts-available");
	protected static String strAvailablePoints;
	protected By dnbError = By.id("dnbError-message");
	protected By dnbLockKey = By.id("do-not-book-override");
	protected By overridePopUpHeader = By.id("overlay-title");
	protected By overrideReasonDrpdwn = By.xpath("//div[@id='dropdown-note-filter']");
	protected By overrideReason = By.xpath("//div[@reasoncodeid='2']");
	protected By confirmOverrideBtn = By.id("overlay-primary");
	protected By dnbUnlockKey = By.xpath("//img[@class='overrideUnLockIcon']");
	protected By noPPOptionDisabled = By.xpath("//label[@class='radio disabled']");
	protected By reservationDeatilsHeader = By.id("reservation-detail-right");
	protected By buttonNext_ChargesSection = By.xpath("//*[@id = 'chargesBtn']");
	protected By chargesSectionHeader = By.id("rd-mtd-charges-section");
	protected By paymentSectionHeader = By.id("rd-mtd-payment-section");
	protected By confirmationMsg = By
			.xpath("//*[@id='rd-reservation-message' and contains(text(),'Travel Dates have been updated')]");
	protected By modifiedReservationNumber = By.id("rd-confirmation-number");
	protected By benefitType = By.id("rd-benefit-type");
	protected By nightCount = By.id("rd-nights");
	protected By ppValue = By.id("rd-points-protections-type");
	protected By ppLabel = By.id("rd-pointsProtectionType");
	protected By zeroPP = By.id("zeroPointProtection");
	protected By chargesNxtBtn = By.id("chargesBtn");
	protected By noPointsProtectionRadioBtn = By.xpath("//label[@for='noPointsProtection']");
	protected By yesPointsProtectionRadioBtn = By.xpath("//label[@for = 'yesPointsProtection']");
	protected By overlappingPopUpMsg = By
			.xpath("//p[@id='overlay-text' and contains(text(),'48 hours after booking')]");
	protected By closePopUp = By.id("overlay-primary");
	protected By overlappingAlertMsgAfterModification = By.id("rd-modify-container");
	protected By overlappingAlertMsg = By.xpath("//div[contains(@class,'modifyOverLap message')]");
	protected By amountToPayCollapseView = By.xpath("//*[@id='rd-mtd-charges-section']//span[@class='summary']");
	protected By amountToPayExpandedView = By.id("total-to-pay");
	protected By amountToPayLeftNavigationPanel = By.id("sum-charges");
	protected By flipArrowChargesSection = By.xpath("//*[@id='rd-mtd-charges-section']/header/img");
	protected By modificationHistory = By.id("rd-modification-history");
	protected By ppStatus = By.xpath("(//span[contains(text(),'Protection')])[1]");
	protected By cancelReservation = By.id("rd-cancel-reservation");
	protected By hkChargesHeader = By.id("hkp");
	protected By hkTotalCharges = By.id("hcrtdchrg");
	protected By ppAbsenceInChargesSection = By.id("verifyPPAbsenceInChargesSection");
	protected By borrowHK = By.id("hcr-borrow");
	protected By rentHK = By.id("hcr-rent");
	protected By currentAvailableHK = By.id("hcrtdavail");
	protected static HashMap<String, String> benefits = new HashMap<String, String>();
	protected static HashMap<String, String> nights = new HashMap<String, String>();
	protected static HashMap<String, String> pp = new HashMap<String, String>();
	protected static HashMap<String, String> ppAbsence = new HashMap<String, String>();
	protected static List<String> ratePlanIdAferModification = new ArrayList<String>();
	protected static List<String> checkInCounterAferModification = new ArrayList<String>();
	protected By pointProtectionSelected = By.xpath("//*[@id='rd-points-protections-type']");
	protected By originalUnitCost = By.xpath("//span[@id='sum-orig-unit-cost']");
	protected By extrapointRequired = By.xpath("//span[@id='sum-points-required']");
	protected By ppEligible = By.xpath("//span[@id='sum-points-protection']");
	protected By ppYesMsg = By.xpath(
			"//span[contains(text(),'Yes, purchase Points Protection for ') and contains(.,'Points Protection is non-refundable and non-transferable.')]");
	protected By ppNoMsg = By.xpath("//span[contains(text(),'No, do not purchase Points Protection.')]");
	protected By ppCostDisplayed = By.xpath("//span[@id='ppp-cost']");
	protected By nextBtnDisabled = By.xpath("//button[@disabled and @id='chargesBtn']");
	protected By nextBtnEnabled = By.xpath("//button[not(@disabled) and @id='chargesBtn']");
	protected By yesPointProtection = By.xpath("//input[@id='yesPointsProtection']");
	protected By noPointProtection = By.xpath("//input[@id='noPointsProtection']");
	protected By selectReservationLink = By.xpath("//a[@id= 'section-menu-reservations']");
	protected By ackCheckBox = By.xpath("//input[@name='ack-check']");
	protected By clickBookReservationButton = By.xpath("//button[@id = 'bookBtn']");
	protected By selectedDate = By
			.xpath("//td[@class='bookable-date modifyDateCheck availCheckInDate selectedCheckIn']/span");
	protected By pointsAvailable = By.xpath(
			"//td[@class='bookable-date modifyDateCheck availCheckInDate selectedCheckIn']/span[@class='points-available']");
	protected By modifiedFutureDate = By.xpath("//td[contains(@class,'chosen-date check-out-date')]/span");
	protected By objNights = By.xpath("//span[@id='rd-nights']");
	protected By modifyHistoryPPDeclined = By
			.xpath("//div[@id='rd-modification-history']/ul/li/span/span[text()='Points Protection: Declined']");
	protected By chargesNextBtn = By.xpath("//button[@id='chargesBtn']");
	protected By travelDatesSuccessMsg = By.xpath(
			"//span[@id='rd-reservation-message' and @class='message--success displayBlock'][text()='Travel Dates have been updated']");
	protected By ppCharges = By.xpath("//span[@id='points-protection-charges']");;
	protected By ppNoDisabled = By.xpath("// label[@for='noPointsProtection' and @class='radio disabled']");;

	protected By modifyHistoryPPAccepted = By
			.xpath("//div[@id='rd-modification-history']/ul/li/span/span[text()='Points Protection: Accepted']");
	protected By ppAlreadyCovered = By.xpath(
			"//tr[@id='zeroPointProtection']//strong[text()='your entire reservation will automatically be covered.']");
	protected By modifyHistoryAddNight = By
			.xpath("//div[@id='rd-modification-history']/ul/li/span/span[contains(text(),'night  added')]");
	protected By errorLowPoint = By.xpath(
			"//div[@id='overlay-wrap' and not(contains(@style,'none'))]//p[@id='overlay-text' and text()='We apologize for the inconvenience, but you do not have enough points to complete this reservation.']");
	public static int availableHK;
	protected By areaHKBorrow = By.xpath("//div[@id = 'hcr-borrow']");
	protected By fieldHKBorrow = By.xpath("//input[@name = 'hcr-borrow-amount']");
	protected By textPPNoAdditionalCharge = By.xpath("//tr[@id = 'zeroPointProtection']/td/span");
	protected By valuePPRequired = By.xpath("//span[@id = 'rd-total-paid-points-protection']");
	protected By houseKeepingAvailablePoints = By.xpath("//span[@id = 'hcr-available']");
	protected By pointCost = By.xpath("//tbody/tr[contains(.,'COST')]/td/div/span");
	protected By cancelReservationBtn = By.xpath("//button[@id='rd-cancel-reservation']");
	protected By cancelReservationFinalBtn = By.xpath("//button[@id='rd-cancel-reservation-button']");
	protected By refundPointCost = By.xpath("//span[@id='rd-points-cost']");
	protected By refundHKCost = By.xpath("//span[@id='rd-housekeeping-cost']");
	protected By cancelconfirm = By.xpath("//button[@class='btn-secondary overlay-cancel-reservation-button']");
	protected By cancelSuccessMsg = By.xpath(
			"//span[@id='rd-reservation-message' and @class='message--success displayBlock'][contains(text(),'Reservation has been cancelled')]");
	protected By firstName = By.xpath("//input[contains(@id,'cc-firstName')]");
	protected By lastName = By.xpath("//input[contains(@id,'cc-lastName')]");
	protected By creditCardLabel = By.xpath("//label[contains(text(),'CREDIT CARD #')]");
	protected By creditCardNumber = By.xpath("//input[contains(@id,'cc-number')]");
	protected By expMM = By.xpath("//input[contains(@name,'expMM')]");
	protected By expMMSelect = By
			.xpath("//div[@name='expMM']//div[@data-datakey='" + testData.get("strCCExpMonth") + "']");
	protected By expYY = By.xpath("//input[contains(@name,'expYY')]");
	protected By expYYSelect = By
			.xpath("//div[@name='expYY']//div[@data-datakey='" + testData.get("strCCExpYear") + "']");
	protected By zipCode = By.xpath("//input[contains(@id,'cc-postalCode')]");
	protected By housekeepingReqLabel = By.xpath("//span[text()='HOUSEKEEPING REQUIRED']");
	protected By housekeepingCharge = By.xpath("//span[text()='HOUSEKEEPING REQUIRED']/following-sibling::span");
	

	/*
	 * Method: selectModifyNights Description: Modify nights Select Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectModifyNights() {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, modifyNights, "ExplicitLongWait");
		if (verifyObjectDisplayed(modifyNights)) {
			clickElementJSWithWait(modifyNights);
			checkLoadingSpinnerTRIP();
			Assert.assertTrue(verifyObjectDisplayed(modifyHeader), "Failed to click on MOdify Nights Button");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "selectModifyNights", Status.PASS,
					"Successfully Clicked on MOdify Nights Button");

		}
	}

	/*
	 * Method: modifyCheckOutDate Description: Modify Check Out Date Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void modifyCheckOutDate() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String modifiyCheckOutDate = testData.get("ModifyCheckOutDate");
		String modifiyCheckOutDay = modifiyCheckOutDate.split("-")[1].trim();
		clickElementJSWithWait(By.xpath("//div[@id = 'calendar']//td[contains(@class,'bookable-date')]//span[text() = '"
				+ modifiyCheckOutDay + "']"));
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "modifyCheckOutDate", Status.PASS,
				"CheckOut Date Modified To: " + modifiyCheckOutDate);

		checkLoadingSpinnerTRIP();
	}

	/*
	 * Method: modifyCheckInDate Description: Modify Check In Date Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void modifyCheckInDate() {
		waitUntilElementVisibleBy(driver, modifyHeader, "ExplicitLongWait");
		String modifiyCheckInDate = testData.get("ModifyCheckInDate");
		String modifiyCheckInDay = modifiyCheckInDate.split("-")[1].trim();
		clickElementJSWithWait(By.xpath("//div[@id = 'calendar']//td[contains(@class,'bookable-date')]//span[text() = '"
				+ modifiyCheckInDay + "']"));
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "modifyCheckInDate", Status.PASS,
				"CheckIn Date Modified To: " + modifiyCheckInDate);
		checkLoadingSpinnerTRIP();
	}

	/*
	 * Method: clickNextButton Description: Click Next Button Date: July/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getObject(buttonNext).getAttribute("class").contains("disabledBtn")) {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "clickNextButton", Status.FAIL,
					"Next Button not Enabled");
		} else {
			clickElementJSWithWait(buttonNext);
			checkLoadingSpinnerTRIP();
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "clickNextButton", Status.PASS,
					"Successfully Clicked On Next Button");
		}
	}

	/*
	 * Method: checkModifiedNights Description:Check Modified Nights Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkModifiedNights() {
		waitUntilElementVisibleBy(driver, sectionModificationHistory, "ExplicitLongWait");
		getElementInView(sectionModificationHistory);
		String currentDateinFormat = getCurrentDateInSpecificFormat("MMM dd, YYYY").trim();
		Assert.assertTrue(
				verifyObjectDisplayed(By.xpath(
						"//strong[contains(.,'" + currentDateinFormat + "')]/..//span[contains(.,'Add Nights')]")),
				"Nights Added not displayed in Modification History");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "checkModifiedNights", Status.PASS,
				"Succesfully Added Nights");
	}

	/*
	 * Method: verifyAddNigtModifyDetails Description: Verify and Modify Nights
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyAddNightModifyDetails() {
		String currentDateinFormat = getCurrentDateInSpecificFormat("MMM dd, YYYY").trim();
		By modifyDetailsList = By.xpath("//strong[contains(.,'" + currentDateinFormat
				+ "')]/..//span[contains(.,'Add Nights')]/..//span//span");
		for (int iteratorModifyDetails = 0; iteratorModifyDetails < getList(modifyDetailsList)
				.size(); iteratorModifyDetails++) {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyAddNightModifyDetails", Status.PASS,
					"Text: " + getList(modifyDetailsList).get(iteratorModifyDetails).getText().trim());

		}
	}

	/*
	 * Method: modifyNightsPencilButtonPresence Description: Modify nights
	 * Pencil Present Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void modifyNightsPencilButtonPresence() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(modifyNights), "Modify Nights Pencil Button not present");
		tcConfig.updateTestReporter("ReservationsPage", "modifyNightsPencilButtonAbsense", Status.PASS,
				"Modify Nights Pencil Button displayed");
	}

	/*
	 * Method: modifyNightsPencilButtonAbsense Description: Modify nights Pencil
	 * Not Present Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void modifyNightsPencilButtonAbsense() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertFalse(verifyObjectDisplayed(modifyNights), "Modify Nights Pencil Button present");
		tcConfig.updateTestReporter("ReservationsPage", "modifyNightsPencilButtonAbsense", Status.PASS,
				"Modify Nights Pencil Button not displayed");
	}

	/*
	 * Method: reservationPageDateFormat Description: Change Reservation date
	 * format Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public String reservationPageDateFormat(By xpath) throws Exception {
		String getDisplayedDate = getElementText(xpath).split("\\.")[1].trim();
		getDisplayedDate = getDisplayedDate.split("\\-")[0].trim();
		return convertDateFormat("MMM dd, yyyy", "MM/dd/yyyy", getDisplayedDate);
	}

	/*
	 * Method: checkOutDateValidation Description: Validated Checkout Dates
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkOutDateValidation() throws Exception {
		String modifiedDate = convertDateFormat("MM-dd-yyyy", "MM/dd/yyyy", testData.get("ModifyCheckOutDate"));
		Assert.assertTrue(convertStringToDate(reservationPageDateFormat(checkOutDate), "MM/dd/yyyy")
				.equals(convertStringToDate(modifiedDate, "MM/dd/yyyy")), "Check Out Date is not Modified");
		tcConfig.updateTestReporter("TRIPReservationDetails", "checkOutDateValidation", Status.PASS,
				"Check Out Date is Modified");
	}

	/*
	 * Method: checkInDateValidation Description: Validated CheckIn Dates Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkInDateValidation() throws Exception {
		String modifiedDate = convertDateFormat("MM-dd-yyyy", "MM/dd/yyyy", testData.get("ModifyCheckInDate"));
		Assert.assertTrue(convertStringToDate(reservationPageDateFormat(checkInDate), "MM/dd/yyyy")
				.equals(convertStringToDate(modifiedDate, "MM/dd/yyyy")), "Check In Date is not Modified");
		tcConfig.updateTestReporter("TRIPReservationDetails", "checkInDateValidation", Status.PASS,
				"Check In Date is Modified");
	}

	/*
	 * Method: totalNightsValidations Description: Validated Total Nights Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalNightsValidations() throws Exception {
		Long toatlNights = dateDifference("MM/dd/yyyy", reservationPageDateFormat(checkInDate),
				reservationPageDateFormat(checkOutDate));
		displayedNight = Long.parseLong(getElementText(totalNights).trim());

		Assert.assertTrue(toatlNights.equals(displayedNight), "Total Nights Displayed incorrectly");
		tcConfig.updateTestReporter("TRIPReservationDetails", "totalNightsValidations", Status.PASS,
				"Total Nights Displayed Correctly");
	}

	/*
	 * Method: validatePointBorrowRent Description: Validate Points Borrow rent
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void validatePointBorrowRent(String optionPoint) {
		travelDetailsPage_Web.validatePointBorrowRent(optionPoint);
	}

	/*
	 * Method: validatePointProtection Description: Validated PPP Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void validatePointProtection() {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, chargesSectionHeader, "ExplicitLongWait");
		if (verifyObjectDisplayed(zeroPP)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(chargesNxtBtn);
		} else {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			travelDetailsPage_Web.validatePointProtection();
		}
	}

	/*
	 * Method: validatePointProtection Description: Validated PPP Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void clickNextButtonCharges() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, chargesNxtBtn, "ExplicitLongWait");
		clickElementJSWithWait(chargesNxtBtn);
	}

	/*
	 * Method: selectAcknowledgeCheckBox Description: Select CheckBox and book
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void selectAcknowledgeCheckBox() {
		travelDetailsPage_Web.selectAcknowledgeCheckBox();
	}

	/*
	 * Method: editReservationType Description: Edit Reservation Type Pencil
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void editReservationType() {
		waitUntilElementVisibleBy(driver, modifyReservationType, "ExplicitLongWait");
		getElementInView(modifyReservationType);
		clickElementBy(modifyReservationType);
		waitUntilElementVisibleBy(driver, modifyReservationTypeHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(modifyReservationTypeHeader),
				"Failed to click on Modify Reservation Type Button");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "editReservationType", Status.PASS,
				"Successfully Clicked on Modify Reservation Type Button");
	}

	/*
	 * Method: selectReservationType Description: Select Reservation Type Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void selectReservationType() {
		String reservationType = testData.get("ReservationType");
		switch (reservationType) {
		case "Extra Holidays":
			clickElementJSWithWait(extraHolidaysRadio);
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "selectReservationType", Status.PASS,
					"Successfully Clicked on Extra Holidays Button");
			break;

		case "TPI":
			clickElementJSWithWait(tpiRadio);
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "selectReservationType", Status.PASS,
					"Successfully Clicked on TPI Button");
			break;

		default:
			clickElementJSWithWait(noneRadio);
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "selectReservationType", Status.PASS,
					"Successfully Clicked on None Button");
			break;

		}
	}

	/*
	 * Method: saveReservationType Description: Save Reservation Type Date:
	 * July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void saveReservationType() {
		clickElementBy(saveMRTButton);
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, modifyMessage, "ExplicitLongWait");
	}

	/*
	 * Method: modifiedReservationType Description: Modified Reservation Type
	 * Date: July/2020 Author: Unnat Jain Changes By: NA
	 */
	public void modifiedReservationType() {
		String reservationType = testData.get("ReservationType");
		switch (reservationType) {
		case "Extra Holidays":
			Assert.assertTrue(getElementText(modifyMessage).toUpperCase().contains("EXTRA HOLIDAY"),
					"Reservation Type Not Modified");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "modifiedReservationType", Status.PASS,
					"Successfully Modifed Reservation Type to Extra Holidays");
			break;

		case "TPI":
			Assert.assertTrue(getElementText(modifyMessage).toUpperCase().contains("TPI"),
					"Reservation Type Not Modified");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "modifiedReservationType", Status.PASS,
					"Successfully Modifed Reservation Type to TPI");
			break;

		default:
			Assert.assertTrue(getElementText(modifyMessage).toUpperCase().contains("NONE"),
					"Reservation Type Not Modified");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "modifiedReservationType", Status.PASS,
					"Successfully Modifed Reservation Type to None");
			break;

		}
	}

	/*
	 * Method: validateModifyAlertMessage Description: Modify Alert Validation
	 * Date: July/2020 Author: Unnat jain Changes By: NA
	 */
	public void validateModifyAlertMessage() {
		waitUntilElementVisibleBy(driver, resortAlert, "ExplicitLongWait");
		if (verifyObjectDisplayed(resortAlert)) {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateModifyAlertMessage", Status.PASS,
					"Alert Present with Message: " + getElementText(resortAlert));
		} else {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateModifyAlertMessage", Status.FAIL,
					"Modify Alert not Present");
		}
	}

	/*
	 * Method: validateModifyingCost Description: Modify Cost Validation Date:
	 * July/2020 Author: Unnat jain Changes By: NA
	 */
	public void validateModifyingCost() {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, modifyingCost, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(modifyingCost), "Modifying Cost not present");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateModifyingCost", Status.PASS,
				"Modifying Cost Present with Points: " + getElementText(modifyingCost));
		strModifyingCost = getElementText(modifyingCost).trim();
		if (strModifyingCost.contains(",")) {
			strModifyingCost = strModifyingCost.replace(",", "");
		}
	}

	/*
	 * Method: onlyFixedWeekAvailableDates Description: Verify that only Fixed
	 * Week dates are available Date: July/2020 Author: Unnat jain Changes By:
	 * NA
	 */
	public void onlyFixedWeekAvailableDates() {
		waitUntilElementVisibleBy(driver, modifyHeader, "ExplicitLongWait");
		Assert.assertTrue(getList(allAvailableDates).size() == 7, "All Fixed Week Dates Are not enabled");
		String fixedWeekFirstDate = testData.get("fixedWeekFirstDate");
		String modifiyFirstDay = fixedWeekFirstDate.split("-")[1].trim();
		if (modifiyFirstDay.startsWith("0")) {
			modifiyFirstDay = modifiyFirstDay.replace("0", "");
		}

		String fixedWeekLastDate = testData.get("fixedWeekLastDate");
		String modifiyLastDay = fixedWeekLastDate.split("-")[1].trim();
		if (modifiyLastDay.startsWith("0")) {
			modifiyLastDay = modifiyLastDay.replace("0", "");
		}

		Assert.assertTrue(getList(allAvailableDates).get(0).getText().equals(modifiyFirstDay),
				"Fixed Week First Date in not enabled");
		Assert.assertTrue(getList(allAvailableDates).get(6).getText().equals(modifiyLastDay),
				"Fixed Week Last Date in not enabled");
		fixedWeekEnabledDates(modifiyFirstDay);
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "onlyFixedWeekAvailableDates", Status.PASS,
				"Only Fixed Week Dates are enabled");
	}

	/*
	 * Method: fixedWeekEnabledDates Description: Verify that only Fixed Week
	 * dates are available Date: July/2020 Author: Unnat jain Changes By: NA
	 */
	public void fixedWeekEnabledDates(String firstDate) {
		Boolean tempCompare = false;
		for (int allDates = 0; allDates < getList(allCalendarDates).size(); allDates++) {
			if (getList(allCalendarDates).get(allDates).getText().equals(firstDate)) {
				for (int temp = allDates; temp < allDates + 6; temp++) {
					if (getList(allCalendarDates).get(temp).getAttribute("not-available").contains("false")) {
						tempCompare = true;

					} else {
						tempCompare = false;
						break;
					}
				}
			}
		}
		Assert.assertTrue(tempCompare == true, "All Fixed Week Dates are not enabled");
	}

	/*
	 * Method: navigateBackToReservationDetails Description: Navigate back to
	 * Reservation details page Date: July/2020 Author: Unnat jain Changes By:
	 * NA
	 */
	public void navigateBackToReservationDetails() {
		clickElementBy(backToDetailsPage);
		checkLoadingSpinnerTRIP();
		Assert.assertTrue(verifyObjectDisplayed(reservationDetailsPage),
				"Could Not Navigate to Reservation Details Page");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "navigateBackToReservationDetails", Status.PASS,
				"Navigated back to reservation details page");
	}

	/*
	 * Method: verifyCompleteFixedWeekBooked Description: Verify that complete
	 * Fixed Week is booked Date: July/2020 Author: Unnat jain Changes By: NA
	 */
	public void verifyCompleteFixedWeekBooked() {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, modifyNights, "ExplicitLongWait");
		if (verifyObjectDisplayed(modifyNights)) {
			clickElementJSWithWait(modifyNights);
			checkLoadingSpinnerTRIP();
			waitUntilElementVisibleBy(driver, fixedWeekBookedOverlay, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(fixedWeekBookedOverlay), "Failed to click on Modify Nights Button");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyCompleteFixedWeekBooked", Status.PASS,
					"Fixed Week Message Overlay Displayed");
		}
	}

	/*
	 * Method: fixedWeekOverlayText Description: Verify that complete Fixed Week
	 * is booked Date: July/2020 Author: Unnat jain Changes By: NA
	 */
	public void fixedWeekOverlayText() {
		Assert.assertTrue(getElementText(overlayMessage).contains("max number of nights has been booked"),
				"Fixed Week Overlay Message not present");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "fixedWeekOverlayText", Status.PASS,
				"Fixed Week Message Overlay Displayed with correct text");
		clickElementBy(overlayOkButton);
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, reservationDetailsPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(reservationDetailsPage), "Could Not click on OK button");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "fixedWeekOverlayText", Status.PASS,
				"Overlay Message Closed");
	}

	/*
	 * Method: unavailableDatesAfterCheckOut Description: Verify that No Dates
	 * are available after checkout date Date: July/2020 Author: Unnat jain
	 * Changes By: NA
	 */
	public void unavailableDatesAfterCheckOut() {
		String strCheckOutDate = testData.get("CheckOutDate");
		strCheckOutDate = strCheckOutDate.split("-")[1].trim();
		if (strCheckOutDate.startsWith("0")) {
			strCheckOutDate = strCheckOutDate.replace("0", "");
		}
		Boolean tempCompare = false;
		for (int allDates = 0; allDates < getList(allCalendarDates).size(); allDates++) {
			if (getList(allCalendarDates).get(allDates).getText().equals(strCheckOutDate)) {
				for (int temp = allDates; temp < allDates + 3; temp++) {
					if (getList(allCalendarDates).get(temp + 1).getAttribute("class").contains("disable")) {
						tempCompare = true;

					} else {
						tempCompare = false;
						break;
					}
				}
			}
		}
		Assert.assertTrue(tempCompare == true, " Dates are enabled after check out date");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "unavailableDatesAfterCheckOut", Status.PASS,
				" Dates are disabled after check out date");
	}

	/**
	 * Method: validateTotalToPayAmount Description:validate Total To Pay Amount
	 * under charges section in both expandable and collapse view Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 */

	public void validateTotalToPayAmount() {
		String totalToPayInCollapseView = getElementText(amountToPayCollapseView).trim().replace("$", "");
		String totalToPayInNavigationPanel = getElementText(amountToPayLeftNavigationPanel).trim();
		mouseoverAndClick(getObject(flipArrowChargesSection));
		String totalToPayInExpandedView = getElementText(amountToPayExpandedView).trim();

		assertTrue(
				totalToPayInCollapseView.equals(totalToPayInExpandedView)
						&& totalToPayInExpandedView.equals(totalToPayInNavigationPanel),
				"Total to Pay amount is not same");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateTotalToPayAmount", Status.PASS,
				"Total to Pay amount is correct and same in both expandale and collpase view");
	}

	/**
	 * Method: verifyPPStatusInModificationHistory Description:verify PP Status
	 * In Modification History Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyPPStatusInModificationHistory() {
		getElementInView(modificationHistory);
		assertTrue(getElementText(ppStatus).contains("Accepted"), "Point Protection not accepted after modification");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyPPStatusInModificationHistory", Status.PASS,
				"Point Protection accepted after modification");

	}

	/**
	 * Method: verifyHouseKeepingBorrowRent Description:verify House Keeping
	 * Borrow and Rent Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void validateHouseKeepingBorrowRent() {
		travelDetailsPage_Web.validateHouseKeepingBorrowRent();

	}

	/**
	 * Method: validateCheckInDateModification Description: Validate Check in
	 * date after modification Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void validateCheckInDateModification() throws Exception {
		waitUntilElementVisibleBy(driver, reservationDeatilsHeader, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(confirmationMsg), "Issue with modifying Travel Date");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateCheckInDateModification", Status.PASS,
				"Travel date modified successfully");

		String modifiedCheckinDate = testData.get("ModifyCheckInDate");
		modifiedCheckinDate = convertDateFormat("MM-dd-yyyy", "MM/dd/yyyy", modifiedCheckinDate);
		String checkinDateAfterModification = reservationPageDateFormat(checkInDate);
		assertTrue(
				getElementText(modifiedReservationNumber).trim()
						.equalsIgnoreCase(TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
								.get(testData.get("AutomationID"))),
				"Reservation number is getting updated after check in date modification");
		assertTrue(modifiedCheckinDate.equals(checkinDateAfterModification), "Updated check In date not displayed");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateCheckInDateModification", Status.PASS,
				"Modified Check In date displayed successfully");
	}

	/**
	 * Method: validateCheckOutDateModification Description: Validate Check out
	 * date after modification Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void validateCheckOutDateModification() throws Exception {
		waitUntilElementVisibleBy(driver, reservationDeatilsHeader, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(confirmationMsg), "Issue with modifying Travel Date");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateCheckInDateModification", Status.PASS,
				"Travel date modified successfully");

		String modifiedCheckOutDate = testData.get("ModifyCheckOutDate");
		modifiedCheckOutDate = convertDateFormat("MM-dd-yyyy", "MM/dd/yyyy", modifiedCheckOutDate);
		String checkOutDateAfterModification = reservationPageDateFormat(checkOutDate);
		assertTrue(
				getElementText(modifiedReservationNumber).trim()
						.equalsIgnoreCase(TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber
								.get(testData.get("AutomationID"))),
				"Reservation number is getting updated after check in date modification");
		assertTrue(modifiedCheckOutDate.equals(checkOutDateAfterModification), "Updated check In date not displayed");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateCheckInDateModification", Status.PASS,
				"Modified Check Out date displayed successfully");
	}

	/**
	 * Method: fetchBenefits Description: Fetch the Benefits Details Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 */
	public void fetchBenefits(String iterator) {
		getElementInView(benefitType);
		benefits.put(iterator, getElementText(benefitType).trim());
	}

	/**
	 * Method: fetchNights Description: Fetch the Night Date: July/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	public void fetchNights(String iterator) {
		waitUntilElementVisibleBy(driver, reservationDeatilsHeader, "ExplicitLongWait");
		nights.put(iterator, getElementText(nightCount).trim());
	}

	/**
	 * Method: fetchNights Description: Fetch the Night Date: July/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	public void fetchPPValue(String iterator) {
		waitUntilElementVisibleBy(driver, reservationDeatilsHeader, "ExplicitLongWait");
		getElementInView(ppValue);
		pp.put(iterator, getElementText(ppValue).trim());
	}

	/**
	 * Method: compareBenefits Description: compare Benefits before and after
	 * modifications Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void compareBenefits(String firstIterator, String secondIterator) {
		String[] updatedBenefits = benefits.get(secondIterator).split(",");
		assertTrue(updatedBenefits.length > 1, "Benefit type is not updated after modifications");
		for (int numberOfBenetits = 0; numberOfBenetits < updatedBenefits.length; numberOfBenetits++) {
			if (updatedBenefits[numberOfBenetits].trim().equalsIgnoreCase(benefits.get(firstIterator).trim())) {
				tcConfig.updateTestReporter("TRIPReservationDetailsPage", "compareBenefits", Status.PASS,
						"Benefit type modified successfully and modified benefit contains the previous benefits also");

			}
		}
	}

	/**
	 * Method: compareNights Description: compare Nights before and after
	 * modifications Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void compareNights(String firstIterator, String secondIterator) {
		int previousNightValue = Integer.parseInt(nights.get(firstIterator).trim());
		int updatedNightValue = Integer.parseInt(nights.get(secondIterator).trim());
		assertTrue(updatedNightValue - previousNightValue == Integer.parseInt(testData.get("NightAdded")),
				"Night Value is not updated after modifications");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "compareNights", Status.PASS,
				"Night value get modified successfully");
	}

	/**
	 * Method: comparePPValue Description: compare PP value before and after
	 * modifications Date: July/2020 Author: Kamalesh Changes By: NA
	 */

	public void comparePPValue(String firstIterator, String secondIterator) {
		assertTrue(pp.get(firstIterator).trim().equalsIgnoreCase(pp.get(secondIterator).trim()));
		assertTrue(pp.get(secondIterator).trim().equalsIgnoreCase("Yes"), "Point Protection is not selected");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "comparePPValue", Status.PASS,
				"Point Protection value is modified and is selected for entire reservation");
	}

	/**
	 * Method: verifyOverlappingAlertPopUp Description: verify Overlapping Alert
	 * PopUp Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyOverlappingAlertPopUp() {
		waitUntilElementVisibleBy(driver, overlappingPopUpMsg, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(overlappingPopUpMsg), "Overlapping message is not displayed on the pop up");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyOverlappingAlertPopUp", Status.PASS,
				"Overlapping message is displayed on the pop up");
		clickElementBy(closePopUp);
	}

	/**
	 * Method: verifyOverlappingAlertMsg Description: verify overlapping msg
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyOverlappingAlertMsg() {
		assertTrue(verifyObjectDisplayed(overlappingAlertMsg), "Overlapping message is not displayed");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyOverlappingAlertMsg", Status.PASS,
				"Overlapping Alert Message is displayed");

	}

	/**
	 * Method: verifyOverlappingAlertMsgAfterModification Description: verify
	 * overlapping msg after modification Date: July/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void verifyOverlappingAlertMsgAfterModification() {
		assertTrue(verifyObjectDisplayed(overlappingAlertMsgAfterModification),
				"Overlapping message is not displayed after night modification");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyOverlappingAlertMsgAfterModification",
				Status.PASS, "Overlapping Alert Message is displayed after night modification");

	}

	/**
	 * Method: fillPaymentDetails Description: Provide the Payment Details Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */
	public void fillCreditCardDetails() {
		waitUntilElementVisibleBy(driver, paymentSectionHeader, "ExplicitLongWait");
		travelDetailsPage_Web.fillCreditCardDetails();
	}

	/**
	 * Method: verifyNoPointProtectionOptionDisable Description: verify No Point
	 * Protection Option is Disable Date: July/2020 Author: Kamalesh Changes By:
	 * NA
	 * 
	 */
	public void verifyNoPointProtectionOptionDisable() {
		waitUntilElementVisibleBy(driver, chargesSectionHeader, "ExplicitLongWait");
		assertTrue(getElementAttribute(noPPOptionDisabled, "className").contains("disabled"),
				"No Point Protection Option is not disable");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyNoPointProtectionOptionDisable",
				Status.PASS, "No Point Protection Option is disable");

	}

	/**
	 * Method: validateLimitedAvailabilityIcon_CheckInDate Description: validate
	 * Limited Availability Icon for Check In Date Date: July/2020 Author:
	 * Kamalesh Changes By: NA
	 * 
	 */
	public void validateLimitedAvailabilityIcon_CheckInDate() {
		String modifiyCheckInDate = testData.get("ModifyCheckInDate");
		validateLimitedAvailabilityIcon(modifiyCheckInDate);
	}

	/**
	 * Method: validateLimitedAvailabilityIcon Description: validate Limited
	 * Availability Icon for any date Date: July/2020 Author: Kamalesh Changes
	 * By: NA
	 * 
	 */
	public void validateLimitedAvailabilityIcon(String date) {
		String day = date.split("-")[1].trim();
		/*assertTrue(
				verifyObjectDisplayed(By.xpath("//span[@class='numerical-day' and contains(text(),'" + day
						+ "')]/parent::td/div/img[contains(@class, 'limitAvail')]")),
				"Limited Availability Icon not present");*/
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateLimitedAvailabilityIcon", Status.PASS,
				"Limited Availability Icon  present for Date: " + date);
	}

	/**
	 * Method: modifiedPointProtection Description:modified Point Protection
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	public void modifiedPointProtection() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String requiredPP = testData.get("ModifiedPP");
		if (verifyObjectDisplayed(noPointsProtectionRadioBtn)) {
			if (requiredPP.equalsIgnoreCase("Yes")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(yesPointsProtectionRadioBtn)) {
					clickElementJSWithWait(yesPointsProtectionRadioBtn);
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointProtection", Status.PASS,
							"Yes Radio Button for Point Protection is selected");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(chargesNxtBtn);
				} else {
					tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
							"Yes Radio Button for Point Protection is not selected");
				}

			} else {
				if (verifyObjectDisplayed(noPointsProtectionRadioBtn)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(noPointsProtectionRadioBtn);
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointProtection", Status.PASS,
							"No Radio Button for Point Protection is selected");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(chargesNxtBtn);
				} else {
					tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
							"No Radio Button for Point Protection is not selected");
				}

			}
			checkLoadingSpinnerTRIP();
		} else {
			tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
					"Point Protection Area not displayed");
		}
	}

	/**
	 * Method: verifyZeroPPInChargesSection Description: validate Zero PP in
	 * charges is present Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	public void verifyZeroPPInChargesSection() {
		getElementInView(zeroPP);
		assertTrue(verifyObjectDisplayed(zeroPP), "Zero Point Protection is not showing in charges section");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyZeroPPInChargesSection", Status.PASS,
				"Zero Point Protection is showing in charges section");
	}

	/**
	 * Method: verifyPPEligibiltyInRightPanel Description: verify No Point
	 * Protection eligibilty Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */
	public void verifyPPEligibiltyInRightPanel(String Option) {
		waitUntilElementVisibleBy(driver, chargesSectionHeader, "ExplicitLongWait");
		switch (Option) {

		case "Yes":
			assertTrue(getElementText(ppEligible).equalsIgnoreCase("Yes"),
					"Point Protection Eligibilty is not showing");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyPPEligibiltyInRightPanel", Status.PASS,
					"Point Protection is eligible");
			break;

		case "No":
			assertTrue(getElementText(ppEligible).equalsIgnoreCase("No"), "Point Protection Eligibilty is showing");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyPPEligibiltyInRightPanel", Status.PASS,
					"Point Protection is not eligible");
			break;
		}
	}

	/**
	 * Method: verifyCheckOutOnlyIcon Description:verifyCheckOutOnlyIcon Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */
	public void verifyCheckOutOnlyIcon() {
		String modifiyCheckOutDate = testData.get("ModifyCheckOutDate");
		String modifiyCheckOutDay = modifiyCheckOutDate.split("-")[1].trim();
		assertTrue(
				verifyObjectDisplayed(
						By.xpath("//div[@id = 'calendar']//td[contains(@class,'bookable-date')]//span[text() = '"
								+ modifiyCheckOutDay
								+ "']/following-sibling::div/img[contains(@class,'checkOutDoor')]")),
				"Check Out Only Icon not displayed");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyCheckOutOnlyIcon", Status.PASS,
				"Check Out Only Icon displayed for : " + modifiyCheckOutDate);

	}

	/**
	 * Method: verifyDoNotBookError Description:verify Do Not Book Error Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 */

	public void verifyDoNotBookError() {
		waitUntilElementVisibleBy(driver, dnbError, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(dnbError), " Do not book error not present");
		if (getObject(buttonNext).getAttribute("class").contains("disabledBtn")) {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyDoNotBookError", Status.PASS,
					"Do not book error is present and Next button is disable. User is not able to modify the reservation.");
		} else {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyDoNotBookError", Status.FAIL,
					"Next Button Enabled");
		}
	}

	/**
	 * Method: overrideDoNotBookBlock Description:overrideDoNotBookBlock Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 */

	public void overrideDoNotBookBlock() {
		clickElementBy(dnbLockKey);
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, overridePopUpHeader, "ExplicitLongWait");
		clickElementJSWithWait(overrideReasonDrpdwn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(overrideReason);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementBy(confirmOverrideBtn);
		waitUntilElementVisibleBy(driver, dnbUnlockKey, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(dnbUnlockKey), "Do not book is not overriden");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "overrideDoNotBookBlock", Status.PASS,
				"Do not book is overriden for this transaction");

	}

	/**
	 * Method: comparePPAbsence Description: compare Nights before and after
	 * modifications Date: July/2020 Author: Kamalesh Changes By: NA
	 */
	public void comparePPAbsence(String firstIterator, String secondIterator) {
		assertTrue(ppAbsence.get(firstIterator).equalsIgnoreCase(ppAbsence.get(secondIterator)),
				"Point Protection is present");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "compareNights", Status.PASS,
				"Point Protection is absent from Left Panel before and after the modification");
	}

	/**
	 * Method: validateHKChargesAbsence Description: validate HK charges not
	 * present Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	public void validateHKChargesAbsence() {
		assertFalse(verifyObjectDisplayed(hkChargesHeader) && verifyObjectDisplayed(hkTotalCharges),
				"House Keeping is showing in charges section");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateHKChargesAbsence", Status.PASS,
				"House Keeping is absence from charges section");
	}

	/**
	 * Method: verifyPPAbsenceInChargesSection Description: verify No Point
	 * Protection absence in charges section Date: July/2020 Author: Kamalesh
	 * Changes By: NA
	 * 
	 */
	public void verifyPPAbsenceInChargesSection() {
		waitUntilElementVisibleBy(driver, chargesSectionHeader, "ExplicitLongWait");
		assertFalse(verifyObjectDisplayed(ppAbsenceInChargesSection), "Point Protection is showing in charges section");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyPPAbsenceInChargesSection", Status.PASS,
				"Point Protection is absence from charges section");

	}

	/**
	 * Method: fetchNights Description: Fetch the Night Date: July/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	public void fetchPPAbsence(String iterator) {
		waitUntilElementVisibleBy(driver, reservationDeatilsHeader, "ExplicitLongWait");
		String absent = "";
		getElementInView(cancelReservation);
		if (verifyObjectDisplayed(ppLabel)) {
			absent = "No";
		} else {
			absent = "Yes";
		}
		ppAbsence.put(iterator, absent);
	}

	/**
	 * Method: verifyZeroPPInChargesSection Description: validate Zero PP in
	 * charges is present Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	public void validateCurrentBorrowRentedHKCharges() {
		assertTrue(
				verifyObjectDisplayed(borrowHK) && verifyObjectDisplayed(rentHK)
						&& verifyObjectDisplayed(currentAvailableHK),
				"Current, Borrowed & Rented HK is not showing in charges section");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyZeroPPInChargesSection", Status.PASS,
				"Current, Borrowed & Rented HK is showing in charges section");
	}

	/**
	 * Method: checkCheckInCounterDBAfterModification Description: Validate
	 * Check In Counter in Datebase after modifications Date: Aug/2020 Author:
	 * Unnat Jain Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void checkCheckInCounterDBAfterModification() throws Exception {
		List<String> roomBkgId = new ArrayList<String>();
		DBConnectionOnly();

		String queryToGetRoomBkgId = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND rer.res_num in ('"
				+ TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber.get(testData.get("AutomationID"))
				+ "')";
		roomBkgId = ExecuteQueryAndFetchListColumData(queryToGetRoomBkgId, "ROOM_BKG_ID");

		String queryToGetCounter = "select * from rh_room_bkg b where b.ROOM_BKG_ID = '" + roomBkgId.get(0) + "'";
		checkInCounterAferModification = ExecuteQueryAndFetchListColumData(queryToGetCounter, "MDFD_CHKIN_COUNTER");

		closeDB();
	}

	/**
	 * Method: verifyChekInCounterUpdate Description: Validate Check In Counter
	 * value in Datebase before and after modifications Date: Aug/2020 Author:
	 * Unnat Jain Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void verifyChekInCounterUpdateInDB() throws Exception {
		int beforeModification = Integer.parseInt(TRIPReservationConfirmationPage_Web.checkInCounter.get(0).trim());
		int afterModification = Integer.parseInt(checkInCounterAferModification.get(0).trim());
		assertTrue(afterModification == beforeModification + 1, "Failed To Update the Check In Counter In DB");

		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyChekInCounterUpdateInDB", Status.PASS,
				"Check in Counter is changing after modification. After modification Counter value is "
						+ afterModification);
	}

	/**
	 * Method: checkRatePlanInDBAfterModification Description: Validate Rate
	 * Plan value in Datebase after modifications Date: July/2020 Author:
	 * Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void checkRatePlanInDBAfterModification() throws Exception {
		List<String> roomBkgId = new ArrayList<String>();
		DBConnectionOnly();

		String queryToGetRoomBkgId = "SELECT rer.res_id,RER.RES_DATE,b.OFRNG_SHORT_DESC " + "\"Inventory_Offering\""
				+ ",rer.res_num,SUBSTR (rer.member_num_key, 9) AS membernum,rrb.chk_in_local_date,rrb.chk_out_local_date,RRB.room_bkg_id,cg.class_grp_name,gct.class_name,sg.stat_grp_name,gst.stat_name,RRB.UPGRD_ACCOM_CLASS_KEY,RRB.UPGRD_COST,RRB.UPGRD_INVEN_OFRNG_LABEL,RRB.UPGRD_POINT_ROOM_TYPE_KEY,r.resrt_name,rrb.point_room_type_key,rrb.resrt_point_chart_key,rrb.reqd_base_amt,rrb.reqd_discnt_amt,rrb.reqd_base_curncy,rrb.consmr_type_key,rrp.room_rate_plan_id,rrt.first_name,rrt.last_name,rrt.addr_line_1,rrt.addr_line_2,rar.arangr_role_name,rap.created_by FROM rh_class_grp cg inner join rh_grouped_class_type gct on cg.class_grp_id = gct.class_grp_id,rh_grouped_stat_type gst,rh_res_stat_app rsa,rh_res_class_app rca,rh_stat_grp sg,rh_ent_res rer,rh_res_item rri,rh_resrvd_party rrpar,rh_resrvd_travlr rrt,rh_arangr_party rap,rh_arangr_role_asgnmt rara,rh_arangr_role rar,rh_bkg_detl rbd,rh_room_bkg rrb,rh_room_rate_plan rrp,pvw_resrt r,ALLOCATION.VW_INVEN_EXTERNL_HOLD b,ai_resrt c WHERE cg.class_grp_id = gct.class_grp_id AND rsa.stat_type_id = gst.stat_type_id AND rca.class_type_id = gct.class_type_id AND sg.stat_grp_id = gst.stat_grp_id AND rer.res_id = rri.res_id AND rrpar.res_id = rer.res_id AND rrpar.resrvd_party_id = rrt.resrvd_party_id AND rsa.res_id = rer.res_id AND rca.res_id = rer.res_id AND rbd.room_bkg_id = rrb.room_bkg_id AND rrb.room_rate_plan_id = rrp.room_rate_plan_id AND rrb.resrt_key = r.resrt_num AND rrb.room_bkg_id = rri.room_bkg_id AND rri.room_bkg_id = rbd.room_bkg_id AND rap.res_id = rer.res_id AND rap.arangr_party_id = rara.arangr_party_id AND rar.arangr_role_id = rara.arangr_role_id AND rbd.PROVDR_CNFMTN_CODE = b.ROOM_UTILZN_JRNL_ID AND b.RESRT_NUM = c.RESRT_NUM AND rer.res_num in ('"
				+ TRIPReservationConfirmationPage_Web.textBookingConfirmationNumber.get(testData.get("AutomationID"))
				+ "')";
		roomBkgId = ExecuteQueryAndFetchListColumData(queryToGetRoomBkgId, "ROOM_BKG_ID");

		String queryToGetRatePlanID = "select ROOM_RATE_PLAN_ID from RESERVATION.RH_ROOM_BKG_CHNG where ROOM_BKG_ID = '"
				+ roomBkgId.get(0) + "'";
		ratePlanIdAferModification = ExecuteQueryAndFetchListColumData(queryToGetRatePlanID, "ROOM_RATE_PLAN_ID");

		closeDB();
	}

	/**
	 * Method: verifyRatePlanInDBBeforeAndAfterModification Description:
	 * Validate Rate Plan value in Datebase before and after modifications Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 * 
	 * @throws Exception
	 * 
	 */
	public void verifyRatePlanInDBBeforeAndAfterModification() throws Exception {
		assertTrue(ratePlanIdAferModification.get(1).equals(TRIPReservationConfirmationPage_Web.ratePlanId.get(0))
				|| ratePlanIdAferModification.get(0).equals(TRIPReservationConfirmationPage_Web.ratePlanId.get(0)));
		assertFalse(ratePlanIdAferModification.get(0).equals(ratePlanIdAferModification.get(1)),
				"Rate Plan is not changing after modification");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyRatePlanInDBBeforeAndAfterModification",
				Status.PASS, "Rate Plan is changing after modification. After modification Rate plan value is "
						+ ratePlanIdAferModification.get(0));
	}

	/*
	 * Method: validatePointProtection Description: validate Point Protection
	 * Date: July/2020 Author: MOnideep Changes By: NA
	 */

	public void validatePointProtection(String selected) {

		Assert.assertTrue(getElementText(pointProtectionSelected).trim().equalsIgnoreCase(selected),
				"Point protection selected not as expected, showing up as : " + selected);
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePointProtection", Status.PASS,
				"Points protection selected during reservation updated correctly");
	}

	/*
	 * Method: selectCheckinDateMinus Description: selectCheckinDateMinus Date:
	 * July/2020 Author: MOnideep Changes By: NA
	 */
	public void selectCheckinDateMinus(int days) {

		/*clickElementJSWithWait(By.xpath("//td[@class='bookable-date availCheckInDate'][" + days + "]"));*/
		checkLoadingSpinnerTRIP();
		tcConfig.getTestData().put("ModifiedCheckInDate", getElementAttribute(selectedDate, "data-date"));
		tcConfig.getTestData().put("ModifiedCheckOutDate", getElementAttribute(modifiedFutureDate, "data-date"));
		Assert.assertTrue(verifyObjectDisplayed(pointsAvailable), "Addtional points required not displayed");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "selectCheckinDateMinus", Status.PASS,
				"Points protection selected during reservation updated correctly");

	}

	/*
	 * Method: validatePPsection Description: validatePPsection Date: July/2020
	 * Author: MOnideep Changes By: NA
	 */

	public void validatePPsection() {

		checkLoadingSpinnerTRIP();

		int originalUnitValue = Integer.parseInt(getElementText(originalUnitCost).replace(",", ""));
		int extraPointRequiredValue = Integer.parseInt(getElementText(extrapointRequired).replace(",", ""));
		int modTotallUnitcost = originalUnitValue + extraPointRequiredValue;

		tcConfig.getTestData().put("OriginalUnitCost", originalUnitValue + "");
		tcConfig.getTestData().put("ExtraPointRequired", extraPointRequiredValue + "");

		Assert.assertTrue(getElementText(ppEligible).equalsIgnoreCase("YES"),
				"Points Protection Eligible section not present or eligibility incorrect");
		Assert.assertTrue(verifyObjectDisplayed(ppYesMsg), "Points Protection yes message not displayed");
		Assert.assertTrue(verifyObjectDisplayed(ppNoMsg), "Points Protection no message not displayed");

		String ppCost = getElementText(ppCostDisplayed);
		if (modTotallUnitcost <= 200000) {
			Assert.assertTrue(ppCost.equals("49.00"), "Point protection cost expected : $49.00, actual : " + ppCost);
		} else if (modTotallUnitcost <= 300000) {
			Assert.assertTrue(ppCost.equals("69.00"), "Point protection cost expected : $69.00, actual : " + ppCost);
		} else if (modTotallUnitcost > 300000) {
			Assert.assertTrue(ppCost.equals("99.00"), "Point protection cost expected : $99.00, actual : " + ppCost);
		}

		Assert.assertTrue(verifyObjectDisplayed(nextBtnDisabled), "Next button is not disabled");

		clickElementBy(yesPointProtection);
		Assert.assertTrue(verifyObjectDisplayed(nextBtnEnabled), "Next button is not enabled");

		clickElementBy(noPointProtection);
		Assert.assertTrue(verifyObjectDisplayed(nextBtnEnabled), "Next button is not enabled");

		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePPsection", Status.PASS,
				"Points protection section validated successfully");

	}

	/*
	 * Method: validateUpdatedCheckInDate Description:
	 * validateUpdatedCheckInDate Date: July/2020 Author: MOnideep Changes By:
	 * NA
	 */
	public void validateUpdatedCheckInDate() throws Exception {

		waitUntilElementVisibleBy(driver, travelDatesSuccessMsg, "ExplicitLongWait");

		Assert.assertTrue(verifyObjectDisplayed(travelDatesSuccessMsg),
				"Travel Dates changed success message not displayed");

		Date checkin = new SimpleDateFormat("yyyy-MM-dd").parse(tcConfig.getTestData().get("ModifiedCheckInDate"));
		String updatedCheckInDate = new SimpleDateFormat("MMM dd, yyyy").format(checkin);

		Assert.assertTrue(
				verifyObjectDisplayed(
						By.xpath("//span[@id='rd-check-in-date' and contains(text(),'" + updatedCheckInDate + "')]")),
				"Check in date not updated successfully");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateUpdatedCheckInDate", Status.PASS,
				"Check in date updated successfully " + updatedCheckInDate);

		long numberOfNights = dateDifference("yyyy-MM-dd", tcConfig.getTestData().get("ModifiedCheckInDate"),
				tcConfig.getTestData().get("ModifiedCheckOutDate"));

		Assert.assertTrue(getElementText(objNights).equals(numberOfNights + ""),
				"Number of nights not updated successfully");

		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateUpdatedCheckInDate", Status.PASS,
				"Number of nights updated successfully : " + numberOfNights);

	}

	/*
	 * Method: validatePointProtectionModifyHistory Description:
	 * validatePointProtectionModifyHistory Date: July/2020 Author: MOnideep
	 * Changes By: NA
	 */

	public void validatePointProtectionModifyHistory(String selected) {

		if (selected.equals("NO")) {
			Assert.assertTrue(verifyObjectDisplayed(modifyHistoryPPDeclined),
					"Points protection declined message not present");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePointProtectionModifyHistory",
					Status.PASS, "Modification history shows Point Protection declined");
			
		} else {
			Assert.assertTrue(verifyObjectDisplayed(modifyHistoryPPAccepted),
					"Points protection accepted message not present");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePointProtectionModifyHistory",
					Status.PASS, "Modification history shows Point Protection Accepted");

		}

	}

	/*
	 * Method: clickChargesNextButton Description: clickChargesNextButton Date:
	 * July/2020 Author: MOnideep Changes By: NA
	 */
	public void clickChargesNextButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, chargesNextBtn, "ExplicitLongWait");
		clickElementJSWithWait(chargesNextBtn);
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "clickChargesNextButton", Status.PASS,
				"Charges next button has been selected successfully");

	}

	/*
	 * Method: validatePPsectionSelected Description: validatePPsectionSelected
	 * Date: July/2020 Author: MOnideep Changes By: NA
	 */
	public void validatePPsectionSelected(String addcost) {

		checkLoadingSpinnerTRIP();

		if (addcost.equalsIgnoreCase("0")) {

			Assert.assertTrue(verifyObjectDisplayed(ppAlreadyCovered),
					"Point protection cost hasn't been already covered");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePPsectionSelected", Status.PASS,
					"Points protection cost already covered");

		} else {
			Assert.assertTrue(
					verifyObjectDisplayed(By.xpath(
							"//span[contains(.,'Yes, purchase Points Protection for ') and contains(.,'" + addcost
									+ ".00') and contains(.,'. Points Protection is non-refundable and non-transferable.')]")),
					"Additional cost of : " + addcost + " not showing up correctly");
			clickElementBy(yesPointProtection);
			Assert.assertTrue(verifyObjectDisplayed(ppNoDisabled),
					"Point protection no option not disabled by default");
			Assert.assertTrue(getElementText(ppCharges).trim().equals(addcost),
					"Point protection no option not disabled by default");
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePPsectionSelected", Status.PASS,
					"Points protection section validated successfully");
		}

	}

	/*
	 * Method: validateAddNightModifyHistory Description: validate Add Night
	 * Modify History Date: July/2020 Author: MOnideep Changes By: NA
	 */
	public void validateAddNightModifyHistory() {

		Assert.assertTrue(verifyObjectDisplayed(modifyHistoryAddNight), "Add Nights message not present");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateAddNightModifyHistory", Status.PASS,
				"Modification history shows Nights added");

	}

	/*
	 * Method: validateLowPointError Description: validate Low Point Error Date:
	 * July/2020 Author: MOnideep Changes By: NA
	 */

	public void validateLowPointError() {

		waitUntilElementVisibleBy(driver, errorLowPoint, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(errorLowPoint), "Low point error message not visible");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateLowPointError", Status.PASS,
				"Low point error message visible");

	}

	/*
	 * Method: getAvailableHouseKeepingPoints Description: Get Housekeeping
	 * available value Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void getAvailableHouseKeepingPoints() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, houseKeepingAvailablePoints, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(houseKeepingAvailablePoints),
				"Available Points not displayed while modifying nights");
		availableHK = Integer.parseInt(getElementText(houseKeepingAvailablePoints));
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "getAvailableHouseKeepingPoints", Status.PASS,
				"Avaliable Housekeeping displayed as: " + availableHK);
	}

	/*
	 * Method: setBorrowHouseKeeping Description: Set Borrow HouseKeeping Points
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setBorrowHouseKeeping() {
		String housekeepingToBorrow = testData.get("HKBorrowOnly");
		if (verifyObjectDisplayed(areaHKBorrow)) {
			fieldDataEnter(fieldHKBorrow, housekeepingToBorrow);
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinnerTRIP();
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowHouseKeeping", Status.PASS,
					"Housekeeping Borrowed " + housekeepingToBorrow);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowHouseKeeping", Status.FAIL,
					"Housekeeping To Borrow");
		}
	}

	/*
	 * Method: clickCancelReservationButton Description: Click Cancel
	 * Reservation on Reservation Details Page Date: July/2020 Author: Abhijeet
	 * Roy Changes By: NA
	 */
	public void clickCancelReservationButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, buttonCancel, "ExplicitLongWait");
		clickElementJSWithWait(buttonCancel);
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, buttonReservationCancel, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(buttonReservationCancel),
				"Cancel Reservation not present on Reservation Details Page");
		tcConfig.updateTestReporter("TRIPTravelDetailsPage", "clickCancelReservationButton", Status.PASS,
				"Successfully Clicked Cancel reservation Button on Reservation Details Page");
	}

	/*
	 * Method: getUsedHousekeepingAfterCancel Description: Get Used Housekeeping
	 * Point After Cancel Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void getUsedHousekeepingAfterCancel() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, usedHousekepingValue, "ExplicitLongWait");
		getElementInView(usedHousekepingValue);
		if (getList(usedHousekepingValue).size() > 0) {
			hkUsedValue = Integer.parseInt(getList(usedHousekepingValue).get(1).getText().trim());
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "getUsedHousekeepingAfterCancel", Status.PASS,
					"Houskeeping used :" + hkUsedValue);
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "getUsedHousekeepingAfterCancel", Status.FAIL,
					"Houskeeping used not found");
		}

	}

	/*
	 * Method: getPriceForModify Description: Get Price For Modify Date:
	 * July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void getPriceForModify() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, modifyPriceText, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(modifyPriceText), "Price for Modify not displayed");
		modifyPrice = Integer.parseInt(getElementText(modifyPriceText).trim().replace(",", ""));
		tcConfig.updateTestReporter("TRIPTravelDetailsPage", "getPriceForModify", Status.PASS,
				"Modified Price: " + modifyPrice);
	}

	/*
	 * Method: checkPriceModify Description: check Price Modify Date: July/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void checkPriceModify() {
		if (modifyPrice == TRIPSearchResultPage_Web.price) {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkDiscountPriceModify", Status.PASS,
					"Discounted price matched");
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "checkDiscountPriceModify", Status.PASS,
					"Discounted price not matched");
		}
	}

	/*
	 * Method: validatePointProtectionAlreadyCovered Description: Validate Point
	 * Protection Already Covered Date: July/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void validatePointProtectionAlreadyCovered() {
		waitUntilElementVisibleBy(driver, textPPNoAdditionalCharge, "ExplicitLongWait");
		getElementInView(textPPNoAdditionalCharge);
		Assert.assertTrue(verifyObjectDisplayed(textPPNoAdditionalCharge),
				"Text No Additional Charge for PP not present");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointProtectionAlreadyCovered", Status.PASS,
				"No extra charge for PP is present as: " + getElementText(textPPNoAdditionalCharge));
	}

	/*
	 * Method: valuePointProtectionRequired Description: value Point Protection
	 * Required Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void valuePointProtectionRequired() {
		String valuePP = testData.get("PPValue");
		waitUntilElementVisibleBy(driver, valuePPRequired, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(valuePPRequired), "Value for PP not Displayed");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(valuePPRequired);
		switch (valuePP) {
		case "49":
			if (getElementText(valuePPRequired).contains(valuePP)) {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "valuePointProtectionRequired", Status.PASS,
						"Point Protection tier has the same value and value is: " + getElementText(valuePPRequired));
			} else {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "valuePointProtectionRequired", Status.FAIL,
						"Point Protection tire value validation failed");
			}
			break;
		case "69":
			if (getElementText(valuePPRequired).contains(valuePP)) {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "valuePointProtectionRequired", Status.PASS,
						"Point Protection tier has the same value and value is: " + getElementText(valuePPRequired));
			} else {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "valuePointProtectionRequired", Status.FAIL,
						"Point Protection tire value validation failed");
			}
			break;
		case "99":
			if (getElementText(valuePPRequired).contains(valuePP)) {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "valuePointProtectionRequired", Status.PASS,
						"Point Protection tier has the same value and value is: " + getElementText(valuePPRequired));
			} else {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "valuePointProtectionRequired", Status.FAIL,
						"Point Protection tier value validation failed");
			}
			break;
		default:
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "valuePointProtectionRequired", Status.FAIL,
					"Please enter value in Excel for which tier need to be validated");

		}
	}

	/*
	 * Method: validateUpdateMessage Description: Updated Message Validation
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateUpdateMessage() {

		waitUntilElementVisibleBy(driver, modifyMessage, "ExplicitLongWait");
		if (verifyObjectDisplayed(modifyMessage)) {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateUpdateMessage", Status.PASS,
					"Message Present as: " + getElementText(modifyMessage));
		} else {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateUpdateMessage", Status.FAIL,
					"Modify Message not Present");
		}
	}

	/*
	 * Method: validateUpdateMessage Description: Updated Message Validation
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateUpdateMessage(String message) {
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, modifyMessage, "ExplicitLongWait");
		if (verifyObjectDisplayed(modifyMessage)) {
			if (getElementText(modifyMessage).contains(message)) {
				tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateUpdateMessage", Status.PASS,
						"Message Present as: " + getElementText(modifyMessage));
			} else {
				tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateUpdateMessage", Status.FAIL,
						"Message validation failed for: " + message);
			}

		} else {
			tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateUpdateMessage", Status.FAIL,
					"Modify Message not Present");
		}
	}

	/*
	 * Method: setRentBorrowPoint Description: Set Rent and Borrow Points Date:
	 * July/2020 Author: Unnat jain Changes By: NA
	 */
	public void setRentBorrowPoint() {
		travelDetailsPage_Web.setRentBorrowPoint();
	}

	/*
	 * Method: validateAvailablePoints Description: Available Points Validation
	 * Date: July/2020 Author: Unnat jain Changes By: NA
	 */
	public void validateAvailablePoints() {
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, availablePoints, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(availablePoints), "Available Points not present");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateAvailablePoints", Status.PASS,
				"Available PointsPresent with Points: " + getElementText(availablePoints));
		strAvailablePoints = getElementText(availablePoints).trim();
		if (strAvailablePoints.contains(",")) {
			strAvailablePoints = strAvailablePoints.replace(",", "");
		}
	}

	/*
	 * Method: validateRentedPoints Description: Rented Points Validation Date:
	 * July/2020 Author: Unnat jain Changes By: NA
	 */
	public void validateRentedPoints() {
		waitUntilElementVisibleBy(driver, rentedPoints, "ExplicitLongWait");
		getElementInView(rentedPoints);
		Assert.assertTrue(verifyObjectDisplayed(rentedPoints), "Rented Points not present");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateRentedPoints", Status.PASS,
				"Rented PointsPresent with Points: " + getElementText(rentedPoints));
		String strRentedPoints = getElementText(rentedPoints).trim();
		if (strRentedPoints.contains(",")) {
			strRentedPoints = strRentedPoints.replace(",", "");
		}
		Assert.assertTrue(strRentedPoints.equalsIgnoreCase(testData.get("PointsRent").trim()),
				"Rented Points Did Not Match");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateRentedPoints", Status.PASS,
				"Rented Points Displayed Correctly");
	}

	/*
	 * Method: maxNightsOverlayText Description: Verify Max Nights Overlay is
	 * booked Date: July/2020 Author: Unnat jain Changes By: NA
	 */
	public void maxNightsOverlayText() {
		waitUntilElementVisibleBy(driver, overlayMessage, "ExplicitLongWait");
		Assert.assertTrue(getElementText(overlayMessage).contains("This resort exceeded maximum nights stay"),
				"Max Nights Overlay Message not present");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "maxNightsOverlayText", Status.PASS,
				"Max Nights Message Overlay Displayed with correct text");
		clickElementBy(overlayOkButton);
		checkLoadingSpinnerTRIP();
		waitUntilElementVisibleBy(driver, backToDetailsPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(backToDetailsPage), "Could Not click on OK button");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "maxNightsOverlayText", Status.PASS,
				"Overlay Message Closed");
	}

	/*
	 * Method: saveTotalPointsCost Description: save Total Points Cost Date:
	 * July/2020 Author:Monideep Changes By: NA
	 */
	public void saveTotalPointsCost() {

		tcConfig.getTestData().put("ReservationPointCost", getElementText(pointCost).trim());
		Assert.assertFalse(tcConfig.getTestData().get("ReservationPointCost").isEmpty(),
				"Reservation point cost is not retrieved");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "saveTotalPointsCost", Status.PASS,
				"Reservation point cost : " + tcConfig.getTestData().get("ReservationPointCost"));

	}

	/*
	 * Method: verifyAndClickonCancel Description: verify And Click on Cancel
	 * Date: July/2020 Author:Monideep Changes By: NA
	 */
	public void verifyAndClickonCancel() {

		waitUntilElementVisibleBy(driver, cancelReservationBtn, "ExplicitLongWait");
		clickElementBy(cancelReservationBtn);
		waitUntilElementVisibleBy(driver, cancelReservationFinalBtn, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(cancelReservationFinalBtn), "Cancellation page not visible");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "verifyAndClickonCancel", Status.PASS,
				"Cancellation page visible");
	}

	/*
	 * Method: validatePointsRefunded Description: validate Points Refunded
	 * Date: July/2020 Author:Monideep Changes By: NA
	 */
	public void validatePointsRefunded() {
		Assert.assertTrue(
				getElementText(refundPointCost).trim().equals(tcConfig.getTestData().get("ReservationPointCost")),
				"Refunded points doesn't match unit cost, expected : "
						+ tcConfig.getTestData().get("ReservationPointCost") + " | Actual : "
						+ getElementText(refundPointCost));
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePointsRefunded", Status.PASS,
				"Refunded points match unit cost");
	}

	/*
	 * Method: validateHKPointsRefunded Description: validate HK Points Refunded
	 * Date: July/2020 Author:Monideep Changes By: NA
	 */
	public void validateHKPointsRefunded() {
		Assert.assertTrue(getElementText(refundHKCost).trim().equals(tcConfig.getTestData().get("BorrowedHKPoints")),
				"Refunded points doesn't match unit cost, expected : " + tcConfig.getTestData().get("BorrowedHKPoints")
						+ " | Actual : " + getElementText(refundHKCost));
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validateHKPointsRefunded", Status.PASS,
				"Refunded points match unit cost");

	}

	/*
	 * Method: cancelReservation Description: cancel Reservation Date: July/2020
	 * Author:Monideep Changes By: NA
	 */
	public void cancelReservation() {

		clickElementBy(cancelReservationFinalBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(cancelconfirm);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, cancelSuccessMsg, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(cancelSuccessMsg), "Cancellation not sucessful");
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "cancelReservation", Status.PASS,
				"Cancellation successful");

	}

	/*
	 * Method: validatePointsRefundedPartial Description: validate Points
	 * Refunded Partial Date: July/2020 Author:Monideep Changes By: NA
	 */
	public void validatePointsRefundedPartial() {

		Assert.assertTrue(
				getElementText(refundPointCost).trim().replace(",", "")
						.equals(tcConfig.getTestData().get("PointsBorrowed")),
				"Refunded points doesn't match unit cost, expected : " + tcConfig.getTestData().get("PointsBorrowed")
						+ " | Actual : " + getElementText(refundPointCost));
		tcConfig.updateTestReporter("TRIPReservationDetailsPage", "validatePointsRefunded", Status.PASS,
				"Refunded points match unit cost");

	}
	
	/*
	 * Method: validateHousekeeping Description:To validate Housekeeping Charges after add nights is Zero
	 * Date: October/2020 Author: Prattusha Changes By: NA
	 */
	
	public void verifyHousekeepingCharges() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, housekeepingReqLabel, "ExplicitLongWait");
		String housekeepingCharges= driver.findElement(housekeepingCharge).getText();
		System.out.println(housekeepingCharges);
		if (housekeepingCharges.equalsIgnoreCase("0")){
			
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateHousekeeping", Status.PASS,
					"No Housekeeping Charges Required for Add Nights");
	       
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateHousekeeping", Status.FAIL,
					"Housekeeping Charges Required for Add Night");
		}
	}
}
