package trip.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TRIPTravelDetailsPage_Web extends TRIPBasePage {

	public static final Logger log = Logger.getLogger(TRIPTravelDetailsPage_Web.class);

	public TRIPTravelDetailsPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By ackCheckBox = By.xpath("//input[@name='ack-check']");
	protected By clickBookReservationButton = By.xpath("//button[@id = 'bookBtn']");
	protected By areaHKBorrow = By.xpath("//div[@id = 'hcr-borrow']");
	protected By areaHKRent = By.xpath("//div[@id = 'hcr-rent']");
	protected By fieldHKBorrow = By.xpath("//input[@name = 'hcr-borrow-amount']");
	protected By fieldHKRent = By.xpath("//input[@name = 'hcr-rent-amount']");
	protected By ageCheckbox = By.xpath("//label[@for = 'cin-age']");
    protected By bookingSummary = By.xpath("//aside[@id='summary']");
    protected By donotVipUpgradeRadio = By.xpath("//span[text()='DO NOT UPGRADE']");
	protected By vipNextBtn = By.id("vipNextBtn");
	protected By specialReqNextBtn = By.xpath("//button[@id = 'specialReqBtn']");
	protected By customerAcknowldgChkBx1 = By.xpath("//section[@id='checkInNameSec']/div/form/div/label/input");
	protected By customerAcknowldgChkBx = By.xpath("//section[@id='checkInNameSec']/div/form/div/label/input");
	protected By customerAgeCheck = By.xpath("//input[@type='checkbox' and @id='cin-age']");
	protected By checkInNameNxtBtn = By.id("checkInNameBtn");
	protected By noPointsProtectionRadioBtn = By.xpath("//label[@for='noPointsProtection']");
	protected By chargesNxtBtn = By.id("chargesBtn");
	protected By pointsTxt = By.id("pts-cost");
	protected By ackCheckbox = By.name("ack-check");
	protected By upgradeSection = By.id("vipUpgradesSec");
	protected By firstName = By.xpath("//input[contains(@id,'cc-firstName')]");
	protected By lastName = By.xpath("//input[contains(@id,'cc-lastName')]");
	protected By creditCardLabel = By.xpath("//label[contains(text(),'CREDIT CARD #')]");
	protected By creditCardNumber = By.xpath("//input[contains(@id,'cc-number')]");
	protected By expMM = By.xpath("//input[contains(@name,'expMM')]");
	protected By expMMSelect = By.xpath("//div[@name='expMM']//div[@data-datakey='"+testData.get("strCCExpMonth")+"']");
	protected By expYY = By.xpath("//input[contains(@name,'expYY')]");
	protected By expYYSelect = By.xpath("//div[@name='expYY']//div[@data-datakey='"+testData.get("strCCExpYear")+"']");
	protected By zipCode = By.xpath("//input[contains(@id,'cc-postalCode')]");
	protected By yesPointsProtectionRadioBtn = By.xpath("//label[@for = 'yesPointsProtection']");
	protected By areaPointBorrow = By.xpath("//div[@id = 'pts-borrow']");
	protected By areaPointRent = By.xpath("//div[@id = 'pts-rent']");
	protected By fieldPointBorrow = By.xpath("//input[@name = 'pts-borrow-amount']");
	protected By fieldPointRent = By.xpath("//input[@name = 'pts-rent-amount']");
	protected By pointsNeed = By.xpath("//span[@id = 'pts-needed']");
	protected By hrcNeed = By.xpath("//span[@id = 'hcr-needed']");
	protected By totalUpgradeUnit = By.xpath("//div[@id = 'upgradeUnits']//div[@class = 'unit']");
	protected By ppChargesDetails = By.xpath("//span[@id='points-protection-charges']");
	protected By pointsNeeded = By.xpath("//span[@id='pts-needed']");
	
	/*
	 * Method: navigateTravelDetailsPage Description: navigate Travel Details Page
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void navigateTravelDetailsPage(){
		checkLoadingSpinnerTRIP();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, bookingSummary, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(bookingSummary), "Failed to Navigate to travel details page");
		tcConfig.updateTestReporter("TRIPFindOwnerPage", "selectMember", Status.PASS,
				"Successfully navigated to Travel Details Page");
	}
	
	/*
	 * Method: validateUpgradeSection Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateUpgradeSection() {
		String upgradeRequired = testData.get("RequiredUpgrade");
		String upgradeTo = testData.get("UpgradeTo");
		if (verifyObjectDisplayed(upgradeSection)) {
			if (upgradeRequired.equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(totalUpgradeUnit)) {
					clickElementJSWithWait(getObject(By.xpath("//span[text() = '" + upgradeTo + "']/..")));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateUpgradeSection", Status.PASS,
							"Upgraded To: "+upgradeTo);
				} else {
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateUpgradeSection", Status.FAIL,
							"Not able to select Upgrade Option");
				}

			} else {
				if (verifyObjectDisplayed(donotVipUpgradeRadio)) {
					clickElementJSWithWait(donotVipUpgradeRadio);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateUpgradeSection", Status.PASS,
							"Clicked on No radio button for Upgrade");
				} else {
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateUpgradeSection", Status.FAIL,
							"Failed to select No Upgrade Option");
				}

			}
			clickElementJSWithWait(vipNextBtn);
			checkLoadingSpinnerTRIP();
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateUpgradeSection", Status.FAIL,
					"Upgrade Section Not Present");
		}
	}
	
	/*
	 * Method: validateSpecialRequest Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateSpecialRequest() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(specialReqNextBtn)) {
			clickElementJSWithWait(specialReqNextBtn);
			tcConfig.updateTestReporter("TravelDetailsPage", "validateSpecialRequest", Status.PASS,
					"Special Request Next button is clicked");
			checkLoadingSpinnerTRIP();
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateSpecialRequest", Status.FAIL,
					"Special Request Section Not Present");
		}
	}
	
	/*
	
	/*
	 * Method: validateCheckInName Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	
	public void validateCheckInName() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ageCheckbox, "ExplicitLongWait");
		if (verifyObjectDisplayed(ageCheckbox)) {
			sendKeysToElement(ageCheckbox, Keys.SPACE);
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateCheckInName", Status.PASS,
					"Customer Acknowledge Age Checkbox is selected");
	        waitUntilElementVisibleBy(driver, checkInNameNxtBtn, "ExplicitLongWait");
	        clickElementJSWithWait(checkInNameNxtBtn);
	        waitForSometime(tcConfig.getConfig().get("LowWait"));
	        checkLoadingSpinnerTRIP();
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validateCheckInName", Status.FAIL,
					"Customer Age Checkbox is not displayed");
		}
	}
	
	
	 
	/*
	 * Method: validatePointBorrowRent Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	
	public void validatePointBorrowRent(String optionPoint){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//String optionPoint = testData.get("OptionPoint");
		if (optionPoint.equalsIgnoreCase("Borrow")) {
			setBorrowPoint();
			checkLoadingSpinnerTRIP();
		}else if (optionPoint.equalsIgnoreCase("Rent")) {
			setRentPoint();
			checkLoadingSpinnerTRIP();
		}else if (optionPoint.equalsIgnoreCase("Both")) {
			setRentBorrowPoint();
			checkLoadingSpinnerTRIP();
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointBorrowRent", Status.FAIL,
					"Please Select option what need to be done for point i.e Borrow,Rent or Both");
		}
	}
	
	/*
	 * Method: setBorrowPoint Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setBorrowPoint(){
		if (verifyObjectDisplayed(areaPointBorrow)) {
			String pointsNeeded = driver.findElement(By.xpath("//span[@id = 'pts-needed']")).getText().trim()
					.replace(",", "");
			if (pointsNeeded.isEmpty() || pointsNeeded == null) {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowPoint", Status.FAIL,
						"Point Needed to Continue with Reservation not Displayed");
			} else {
				fieldDataEnter(fieldPointBorrow, pointsNeeded);
				sendKeyboardKeys(Keys.TAB);
				checkLoadingSpinnerTRIP();
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowPoint", Status.PASS,
						"Points for Borrow " + pointsNeeded);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (testData.containsKey("PointsBorrowed")) {
					int pointsBorrowed = Integer.parseInt(testData.get("PointsBorrowed"))
							+ Integer.parseInt(pointsNeeded);
					testData.put("PointsBorrowed", pointsBorrowed + "");
				} else {
					testData.put("PointsBorrowed", pointsNeeded + "");
				}
			}
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowPoint", Status.FAIL,
					"Point To Borrow Area not Displayed");
		}
	}
	
	/*
	 * Method: setRentPoint Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setRentPoint(){
		if (verifyObjectDisplayed(areaPointRent)) {
			String pointsNeeded = driver.findElement(By.xpath("//span[@id = 'pts-needed']")).getText().trim().replace(",", "");
			if (pointsNeeded.isEmpty() || pointsNeeded == null) {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentPoint", Status.FAIL,
						"Point Needed to Continue with Reservation not Displayed");
			}else{
			fieldDataEnter(fieldPointRent, pointsNeeded);
            sendKeyboardKeys(Keys.TAB);
            checkLoadingSpinnerTRIP();
            tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentPoint", Status.PASS,
    				"Points for Rent "+pointsNeeded);
            waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentPoint", Status.FAIL,
					"Point To Rent Area not Displayed");
		}
	}
	
	
	/*
	 * Method: setRentBorrowPoint Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setRentBorrowPoint(){
		String pointsToRent = testData.get("PointsRent");
		if (verifyObjectDisplayed(areaPointBorrow) && verifyObjectDisplayed(areaPointRent)) {
			fieldDataEnter(fieldPointRent, pointsToRent);
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkLoadingSpinnerTRIP();
			int pointsNeededValue = Integer.parseInt(getElementText(pointsNeeded).trim().replace(",", ""));

			if (testData.containsKey("PointsRented")) {
				int pointsRented = Integer.parseInt(testData.get("PointsRented")) + Integer.parseInt(pointsToRent);
				testData.put("PointsRented", pointsRented + "");
			} else {
				testData.put("PointsRented", pointsToRent);
			}

			if (testData.containsKey("PointsBorrowed")) {
				int pointsBorrowed = Integer.parseInt(testData.get("PointsBorrowed"))
						+ (pointsNeededValue - Integer.parseInt(pointsToRent));
				testData.put("PointsBorrowed", pointsBorrowed + "");
			} else {
				testData.put("PointsBorrowed", (pointsNeededValue - Integer.parseInt(pointsToRent)) + "");
			}
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentBorrowPoint", Status.PASS,
					"Points Borrowed " + (pointsNeededValue - Integer.parseInt(pointsToRent)) + " and Points Rented: "
							+ pointsToRent);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentBorrowPoint", Status.FAIL,
					"Point To Borrow and Rent Area not Displayed");
		}
	}
	
	/*
	 * Method: validateHouseKeepingBorrowRent Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateHouseKeepingBorrowRent(){
		String optionHK = testData.get("OptionHK");
		if (optionHK.equalsIgnoreCase("Borrow")) {
			setBorrowHouseKeeping();
			checkLoadingSpinnerTRIP();
		}else if (optionHK.equalsIgnoreCase("Rent")) {
			setRentHousekeeping();
			checkLoadingSpinnerTRIP();
		}else if (optionHK.equalsIgnoreCase("Both")) {
			setRentBorrowHouseKeeping();
			checkLoadingSpinnerTRIP();
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointBorrowRent", Status.FAIL,
					"Please Select option what need to be done for HouseKeeping i.e Borrow,Rent or Both");
		}
	}
	
	/*
	 * Method: setBorrowHouseKeeping Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setBorrowHouseKeeping(){
		checkLoadingSpinnerTRIP();
		if (verifyObjectDisplayed(areaHKBorrow)) {
			String hkNeeded = driver.findElement(By.xpath("//span[@id = 'hcr-needed']")).getText().trim().replace(",",
					"");
			if (hkNeeded.isEmpty() || hkNeeded == null) {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowHouseKeeping", Status.FAIL,
						"HouseKeeping Needed to Continue with Reservation not Displayed");
			} else {
				if (tcConfig.getTestData().containsKey("BorrowedHKPoints")) {
					int totalPointsBorrowed = Integer.parseInt(tcConfig.getTestData().get("BorrowedHKPoints"))
							+ Integer.parseInt(hkNeeded);
					tcConfig.getTestData().put("BorrowedHKPoints", totalPointsBorrowed + "");
				} else {
					tcConfig.getTestData().put("BorrowedHKPoints", hkNeeded + "");
				}
				fieldDataEnter(fieldHKBorrow, hkNeeded);
				sendKeyboardKeys(Keys.TAB);
				checkLoadingSpinnerTRIP();
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowHouseKeeping", Status.PASS,
						"HouseKeeping for Borrow " + hkNeeded);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		} else {
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setBorrowHouseKeeping", Status.FAIL,
					"HouseKeeping To Borrow Area not Displayed");
		}
	}
	
	/*
	 * Method: setRentHousekeeping Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setRentHousekeeping(){
		if (verifyObjectDisplayed(areaHKRent)) {
			String hkNeeded = driver.findElement(By.xpath("//span[@id = 'hcr-needed']")).getText().trim().replace(",", "");
			if (hkNeeded.isEmpty() || hkNeeded == null) {
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentHousekeeping", Status.FAIL,
						"Point Needed to Continue with Reservation not Displayed");
			}else{
				fieldDataEnter(fieldHKRent, hkNeeded);
	            sendKeyboardKeys(Keys.TAB);
	            checkLoadingSpinnerTRIP();
	            tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentHousekeeping", Status.PASS,
	    				"Housekeeping for Rent "+hkNeeded);
	            waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}else{
			tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentHousekeeping", Status.FAIL,
					"Point To Rent Area not Displayed");
		}
	}
	
	/*
	 * Method: setRentBorrowHouseKeeping Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void setRentBorrowHouseKeeping(){
		String housekeepingToRent = testData.get("HKRent");
		String housekeepingToBorrow = testData.get("HKBorrow");
		if (verifyObjectDisplayed(areaPointBorrow) && verifyObjectDisplayed(areaPointRent)) {
			fieldDataEnter(fieldHKBorrow, housekeepingToBorrow);
            sendKeyboardKeys(Keys.TAB);
            waitForSometime(tcConfig.getConfig().get("MedWait"));
            checkLoadingSpinnerTRIP();
            fieldDataEnter(fieldHKRent, housekeepingToRent);
            sendKeyboardKeys(Keys.TAB);
            checkLoadingSpinnerTRIP();
            tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentBorrowHouseKeeping", Status.PASS,
    				"Housekeeping Borrowed "+housekeepingToBorrow+" and housekeeping Rented: "+housekeepingToRent);
            waitForSometime(tcConfig.getConfig().get("MedWait"));
			}else{
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "setRentBorrowHouseKeeping", Status.FAIL,
					"Housekeeping To Borrow and Rent Area not Displayed");
		}
	}
	
	/*
	 * Method: validatePointProtection Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validatePointProtection() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String requiredPP = testData.get("PPRequired");
		if (verifyObjectDisplayed(noPointsProtectionRadioBtn)) {
			if (requiredPP.equalsIgnoreCase("Yes")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(yesPointsProtectionRadioBtn)) {
					clickElementJSWithWait(yesPointsProtectionRadioBtn);
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointProtection", Status.PASS,
							"Yes Radio Button for Point Protection is selected");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(chargesNxtBtn);
				} else {
					tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
							"Yes Radio Button for Point Protection is not selected");
				}

			} else {
				if (verifyObjectDisplayed(noPointsProtectionRadioBtn)) {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(noPointsProtectionRadioBtn);
					tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointProtection", Status.PASS,
							"No Radio Button for Point Protection is selected");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(chargesNxtBtn);
				} else {
					tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
							"No Radio Button for Point Protection is not selected");
				}

			}
			checkLoadingSpinnerTRIP();
		}else{
			tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
					"Point Protection Area not displayed");
		}
	}
	
	/*
	 * Method: clickNextOnly Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickNextOnly(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(chargesNxtBtn);
		checkLoadingSpinnerTRIP();
	}
	
	/*
	 * Method: fillCreditCardDetails Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void fillCreditCardDetails() {
		waitForSometime(tcConfig.getConfig().get("MedWait")); 
		try {
			waitUntilElementVisibleBy(driver, firstName, "ExplicitLongWait");
			if (verifyObjectDisplayed(firstName)) {
					fieldDataEnter(firstName, testData.get("strFirstName"));
					fieldDataEnter(lastName, testData.get("strLastName"));
					fieldDataEnter(creditCardNumber, testData.get("strCCNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJSWithWait(expMM);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJSWithWait(expMMSelect);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJSWithWait(expYY);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJSWithWait(expYYSelect);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					fieldDataEnter(zipCode, testData.get("strPostalCode"));
				tcConfig.updateTestReporter("TravelDetailsPage", "fillTravelDetails", Status.PASS,
						"Credit Card Details has been entered successfully");

			} else {
				tcConfig.updateTestReporter("TravelDetailsPage", "fillCreditCardDetails", Status.FAIL, "Credit Card Details to Enter Fields not present");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("TravelDetailsPage", "fillCreditCardDetails", Status.FAIL, "Failed in Method"+e.getMessage());
		}
	}
	
	/*
	 * Method: selectAcknowledgeCheckBox Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectAcknowledgeCheckBox(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(ackCheckBox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(clickBookReservationButton)) {
			mouseoverAndClick(getObject(clickBookReservationButton));
			tcConfig.updateTestReporter("TravelDetailsPage", "selectAcknowledgeCheckBox", Status.PASS,
					"Book Now button is enabled and Clicked");
		} else {
			tcConfig.updateTestReporter("TravelDetailsPage", "selectAcknowledgeCheckBox", Status.FAIL,
					"Book Now button is not enabled");
		}
		checkLoadingSpinnerTRIP();
	}

	/*
	 * Method: validatePointProtectionTier Description: 
	 * Date: July/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validatePointProtectionTier(String tier) {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(noPointsProtectionRadioBtn)) {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(yesPointsProtectionRadioBtn)) {
				clickElementJSWithWait(yesPointsProtectionRadioBtn);
				tcConfig.updateTestReporter("TRIPTravelDetailsPage", "validatePointProtection", Status.PASS,
						"Yes Radio Button for Point Protection is selected");
				waitUntilElementVisibleBy(driver, ppChargesDetails, "ExplicitLongWait");
				Assert.assertTrue(getElementText(ppChargesDetails).equals(tier),
						"Point Protection charges doesn't match the expected tier which should be : " + tier);
				clickElementJSWithWait(chargesNxtBtn);
			} else {
				tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
						"Yes Radio Button for Point Protection is not selected");
			}

			checkLoadingSpinnerTRIP();
		} else {
			tcConfig.updateTestReporter("TravelDetailsPage", "validatePointProtection", Status.FAIL,
					"Points Protection Area not displayed");
		}

	}
}
