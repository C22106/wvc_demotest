package trip.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import trip.pages.CUIPage_Web;
import trip.pages.TRIPCancelReservationPage_Web;
import trip.pages.TRIPFindOwnerPage_Web;
import trip.pages.TRIPLoginPage_Web;
import trip.pages.TRIPMembershipPage_Web;
import trip.pages.TRIPNoteCentrePage_Web;
import trip.pages.TRIPPointsLedgerPage_Web;
import trip.pages.TRIPProfileDashboardPage_Web;
import trip.pages.TRIPReservationConfirmationPage_Web;
import trip.pages.TRIPReservationDetailsPage_Web;
import trip.pages.TRIPReservationPage_Web;
import trip.pages.TRIPSearchAvailabilityPage_Web;
import trip.pages.TRIPSearchResultPage_Web;
import trip.pages.TRIPTravelDetailsPage_Web;

public class TRIPScripts_Web extends TestBase {

	public static final Logger log = Logger.getLogger(TRIPScripts_Web.class);

	/*
	 * Method: tc_001_ModifiedReservationTo14Nights Description: Verify Modifed
	 * Reservation to 14 Nights Date: Jul/2020 Author: Abhijeet Roy Changes By:
	 * Unnat Jain
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch1" })

	public void tc_001_ModifiedReservationTo14Nights(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);

		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			//House keeping in charges
			travelDetailsPage.validatePointProtection();
			//travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			//reservationDetailsPage.clickNextButtonCharges();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.clickTransactionTab();
			pointsLedgerPage.clickApplyFilter();
			pointsLedgerPage.applyFilterSelect("");
			pointsLedgerPage.clickApplyButton();
			pointsLedgerPage.checkNoHouseKeeping();

			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.modifyNightsPencilButtonAbsense();

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_002_ModifiedReservationTypeExtraHolidays Description: Modify
	 * reservation To Extra Holidays Validations Date: jul/2020 Author: Unnat
	 * Jain Changes By:
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch1" })

	public void tc_002_ModifiedReservationTypeExtraHolidays(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.modifyNightsPencilButtonPresence();
			reservationDetailsPage.editReservationType();
			reservationDetailsPage.selectReservationType();
			reservationDetailsPage.saveReservationType();
			reservationDetailsPage.modifiedReservationType();
			reservationDetailsPage.modifyNightsPencilButtonAbsense();

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_003_ModifiedReservationResortAlert Description: Verify Modifed
	 * Reservation Alert Validation Date: Jul/2020 Author: Unnat Jain Changes
	 * By: Unnat Jain
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch1" })

	public void tc_003_ModifiedReservationResortAlert(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			 travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			//travelDetailsPage.validatePointProtection();
			travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			//reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.clickChargesNextButton();
			reservationDetailsPage.validateModifyAlertMessage();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();

			loginPage.logoutApplication();


		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_004_ModifiedReservationCurrentYearPoints Description: Verify
	 * Modifed Reservation (Next Use Year check in) deducts points from Current
	 * use Year Date: Jul/2020 Author: Unnat Jain Changes By: Unnat Jain
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch1" })

	public void tc_004_ModifiedReservationCurrentYearPoints(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchCurrentYearPoints("BeforeModification");
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validateModifyingCost();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkInDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchCurrentYearPoints("AfterModification");
			pointsLedgerPage.compareCurrentYearPoints("BeforeModification", "AfterModification");

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_005_FixedWeekDatesAvailability Description: Verify that based
	 * on the no of nights booked use is anle to/not able to modify Fixed Week
	 * Dates Date: Jul/2020 Author: Unnat Jain Changes By: Na
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch1" })

	public void tc_005_FixedWeekDatesAvailability(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			if (testData.get("FixedWeekBooked").equalsIgnoreCase("No")) {
				reservationDetailsPage.selectModifyNights();
				reservationDetailsPage.onlyFixedWeekAvailableDates();
				reservationDetailsPage.navigateBackToReservationDetails();
			} else if (testData.get("FixedWeekBooked").equalsIgnoreCase("Yes")) {
				reservationDetailsPage.verifyCompleteFixedWeekBooked();
				reservationDetailsPage.fixedWeekOverlayText();
			}
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_006_VerifyAddNightsForChekoutOutsideARP Description: Verify
	 * that when Chekout Date is outside ARP window and Check in Date in ARP
	 * user is able to edit only the check In date Date: Jul/2020 Author: Unnat
	 * Jain Changes By: Na
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch1" })

	public void tc_006_VerifyAddNightsForChekoutOutsideARP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			//Method Updated by Prattusha	
			reservationDetailsPage.unavailableDatesAfterCheckOut();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage();
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			
			reservationDetailsPage.checkInDateValidation();
			reservationDetailsPage.totalNightsValidations();

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_007_VerifyAddNightsForDiscoveryCheckOutinARP Description:
	 * Verify that when Checkout Date is in ARP window and Check in Date in
	 * Standard user is able to edit only the check In date Date: Jul/2020
	 * Author: Unnat Jain Changes By: Na
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch1" })

	public void tc_007_VerifyAddNightsForDiscoveryCheckOutinARP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateCheckInName();
			reservationDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.maxNightsOverlayText();
			reservationDetailsPage.navigateBackToReservationDetails();

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_008_ModifiedCheckOutBorrowAndRent Description: Verify Modifed
	 * Reservation (Next Use Year check in) deducts points from Current use Year
	 * Date: Jul/2020 Author: Unnat Jain Changes By: Unnat Jain
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch2" })

	public void tc_008_ModifyCheckOutDateBorrowAndRent(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchCurrentYearPoints("BeforeModification");
			pointsLedgerPage.fetchNextYearPoints("BeforeModification");
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.verifyHousekeepingCharges();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validateAvailablePoints();

			reservationDetailsPage.setRentBorrowPoint();
			reservationDetailsPage.validatePointProtection();

			reservationDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage();

			reservationDetailsPage.validateRentedPoints();
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchCurrentYearPoints("AfterModification");
			pointsLedgerPage.fetchNextYearPoints("AfterModification");
			pointsLedgerPage.compareInsufficientCurrentYearPoints("BeforeModification", "AfterModification");
			pointsLedgerPage.compareFutureYearPoints("BeforeModification", "AfterModification");

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_009_ModifyCheckInDateBorrowAndRent Description: Verify Modifed
	 * Reservation (Next Use Year check in) deducts points from Current use Year
	 * Date: Jul/2020 Author: Unnat Jain Changes By: Unnat Jain
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch2" })

	public void tc_009_ModifyCheckInDateBorrowAndRent(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchCurrentYearPoints("BeforeModification");
			pointsLedgerPage.fetchNextYearPoints("BeforeModification");
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.verifyHousekeepingCharges();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validateAvailablePoints();
			reservationDetailsPage.setRentBorrowPoint();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage();
			reservationDetailsPage.validateRentedPoints();
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkInDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchCurrentYearPoints("AfterModification");
			pointsLedgerPage.fetchNextYearPoints("AfterModification");
			pointsLedgerPage.compareInsufficientCurrentYearPoints("BeforeModification", "AfterModification");
			pointsLedgerPage.compareFutureYearPoints("BeforeModification", "AfterModification");

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_010_PointsRefundValidationAfterModifyAndCancel Description:
	 * Verify Modifed Reservation (Next Use Year check in) deducts points from
	 * Current use Year Date: Jul/2020 Author: Unnat Jain Changes By: Unnat Jain
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch2" })

	public void tc_010_PointsRefundValidationAfterModifyAndCancel(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		TRIPCancelReservationPage_Web cancelReservationPage = new TRIPCancelReservationPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();

			travelDetailsPage.validateCheckInName();
			travelDetailsPage.clickNextOnly();
			// travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchNextYearPoints("BeforeModification");
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.verifyHousekeepingCharges();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validateAvailablePoints();
			reservationDetailsPage.setRentBorrowPoint();
			travelDetailsPage.clickNextOnly();
			// reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage();
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchNextYearPoints("AfterModification");
			pointsLedgerPage.compareFutureYearPoints("BeforeModification", "AfterModification");
			pointsLedgerPage.clickTransactionTab();
			pointsLedgerPage.clickApplyFilter();
			pointsLedgerPage.applyFilterSelect();
			pointsLedgerPage.clickApplyButton();
			pointsLedgerPage.checkPointsDeducted();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.clickCancelReservationButton();
			cancelReservationPage.clickCancelReservation();
			cancelReservationPage.clickCancelReservationOverlay();
			reservationDetailsPage.validateUpdateMessage("Reservation has been cancelled");
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkCancelRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.fetchNextYearPoints("AfterCancellation");
			pointsLedgerPage.futureYearPointsAfterCancellation("AfterModification", "AfterCancellation");
			loginPage.logoutApplication();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_011_ModificationCounterValidationinDatabase Description:
	 * Verify Modified Counter is Updated in DB Date: Aug/2020 Author: Unnat
	 * Jain Changes By: Unnat Jain
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch2" })

	public void tc_011_ModificationCounterValidationinDatabase(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.checkModificationCounterInDB();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.verifyHousekeepingCharges();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckInDateModification();
			reservationDetailsPage.checkCheckInCounterDBAfterModification();
			reservationDetailsPage.verifyChekInCounterUpdateInDB();

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_012_ModifiedReservation_Checkout_CheckCUI Description: Verify
	 * Modifed Reservation & validate in CUI Date: Jul/2020 Author: Abhijeet
	 * Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch2" })

	public void tc_012_ModifiedReservation_Checkout_CheckCUI(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		CUIPage_Web cuiPage = new CUIPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.verifyHousekeepingCharges();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			loginPage.logoutApplication();
			cuiPage.launchApplication();

			cuiPage.acceptCookie();
			cuiPage.setUserName();
			cuiPage.setPassword();
			cuiPage.loginCTAClick();
			cuiPage.navigateToUpcomingVacation();
			cuiPage.loadAllReservations();
			cuiPage.selectReservationDetails();
			cuiPage.validateModifiedCheckOutDate();
			cuiPage.validateModifiedNightsDate();
			cuiPage.detailsInModifiedNights();
			cuiPage.logOutApplicationViaDropDown();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_013_ModifiedReservationCheckInDate_WithRentBorrowHK
	 * Description: Modified Reservation Check Out Date with Rent & Borrow HK
	 * Date: Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch2" })

	public void tc_013_ModifiedReservationCheckInDate_WithRentBorrowHK(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.verifyHousekeepingCharges();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.getAvailableHouseKeepingPoints();
			reservationDetailsPage.setBorrowHouseKeeping();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkInDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.clickTransactionTab();
			pointsLedgerPage.clickApplyFilter();
			pointsLedgerPage.applyFilterSelect("");
			pointsLedgerPage.clickApplyButton();
			pointsLedgerPage.housekeepingUsedFromCurrentUseYear("Oct 01, 2019 - Sep 30, 2020");
			pointsLedgerPage.housekeepingBorrowedfromUseYear("Oct 01, 2020 - Sep 30, 2021");
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method:
	 * tc_014_ModifiedReservationCheckInDateCancelReservation_WithRentBorrowHK
	 * Description: Modified Reservation Check Out Date with Rent & Borrow HK
	 * Date: Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch2" })

	public void tc_014_ModifiedReservationCheckInDateCancelReservation_WithRentBorrowHK(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		TRIPCancelReservationPage_Web cancelReservationPage = new TRIPCancelReservationPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.getAvailableHouseKeepingPoints();
			reservationDetailsPage.setBorrowHouseKeeping();
			reservationDetailsPage.clickNextButtonCharges();
			reservationDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkInDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			profileDashboardPage.profileHeaderClick();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.clickTransactionTab();
			pointsLedgerPage.clickApplyFilter();
			pointsLedgerPage.applyFilterSelect("");
			pointsLedgerPage.clickApplyButton();
			pointsLedgerPage.housekeepingUsedFromCurrentUseYear("Oct 01, 2019 - Sep 30, 2020");
			pointsLedgerPage.housekeepingBorrowedfromUseYear("Oct 01, 2020 - Sep 30, 2021");
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.clickCancelReservationButton();
			cancelReservationPage.clickCancelReservation();
			cancelReservationPage.clickCancelReservationOverlay();
			reservationDetailsPage.validateUpdateMessage("Reservation has been cancelled");
			reservationDetailsPage.getUsedHousekeepingAfterCancel();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.checkCancelRecentNotes();
			pointsLedgerPage.navigateToPointsLedgerPage();
			pointsLedgerPage.clickTransactionTab();
			pointsLedgerPage.clickApplyFilter();
			pointsLedgerPage.applyFilterSelect("1");
			pointsLedgerPage.selectResortFilter();
			pointsLedgerPage.clickApplyButton();
			pointsLedgerPage.validateHKReimbersedAfterCancel("Oct 01, 2019 - Sep 30, 2020");
			pointsLedgerPage.housekeepingBorrowedfromUseYear("Oct 01, 2020 - Sep 30, 2021");
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_015_ModifiedReservationCheckOutDate_MembershipDiscount
	 * Description: Modified Reservation CheckOut Date Membership Discount Date:
	 * Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch3" })

	public void tc_015_ModifiedReservationCheckOutDate_MembershipDiscount(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.validateHousekeeping();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.getDiscountPriceForSpeceficRoom();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.getPriceForModify();
			reservationDetailsPage.checkPriceModify();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.clickNextButtonCharges();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_016_ModifiedReservationCheckOutDate_WithoutDiscount
	 * Description: Modified Reservation CheckOut Date Without Discount Date:
	 * Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch3" })

	public void tc_016_ModifiedReservationCheckOutDate_WithoutDiscount(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.getPriceWithoutDiscountForSpeceficRoom();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.getPriceForModify();
			reservationDetailsPage.checkPriceModify();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_017_ModifiedReservationCheckOutDate_InventoryDiscount
	 * Description: Modified Reservation CheckOut Date Inventory Discount Date:
	 * Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch3" })

	public void tc_017_ModifiedReservationCheckOutDate_InventoryDiscount(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.getDiscountPriceForSpeceficRoom();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.getPriceForModify();
			reservationDetailsPage.checkPriceModify();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.clickNextButtonCharges();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_018_ModifiedReservationCheckOutDate_ValidatePPSameTier
	 * Description: Modified Reservation CheckOut Date Validate PP Same Tier
	 * Date: Jul/2020 Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch3" })

	public void tc_018_ModifiedReservationCheckOutDate_ValidatePPSameTier(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtectionAlreadyCovered();
			reservationDetailsPage.clickNextButtonCharges();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.valuePointProtectionRequired();
			reservationDetailsPage.checkModifiedNights();
			reservationDetailsPage.verifyAddNightModifyDetails();
			reservationDetailsPage.checkOutDateValidation();
			reservationDetailsPage.totalNightsValidations();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.checkModifyNigtsRecentNotes();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_19_CreateReservationModifyCancelAndValidateDB Description:
	 * Create Reservation, Modify & Cancel and Validate DB Date: Jul/2020
	 * Author: Abhijeet Changes By:
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch3" })

	public void tc_19_CreateReservationModifyCancelAndValidateDB(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPCancelReservationPage_Web cancelReservationPage = new TRIPCancelReservationPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
			reservationDetailsPage.clickCancelReservationButton();
			cancelReservationPage.clickCancelReservation();
			cancelReservationPage.clickCancelReservationOverlay();
			reservationDetailsPage.validateUpdateMessage("Reservation has been cancelled");
			cancelReservationPage.checkCancelDB();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_020_ModifiedReservationCheckDB Description: Modified
	 * Reservation and Check DB Date: Jul/2020 Author: Abhijeet Roy Changes By:
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch3" })

	public void tc_020_ModifiedReservationCheckDB(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
            loginPage.setPassword();
            loginPage.clickButtonLogin();
            findOwnerPage.searchForMember();
            findOwnerPage.clickSearchButton();
            findOwnerPage.selectMember();
            profileDashboardPage.clickNewVacationLink();
            searchAvailabilityPage.selectLocation();
            searchAvailabilityPage.selectCheckInDate();
            searchAvailabilityPage.selectCheckOutDate();
            searchAvailabilityPage.searchAvailableRoom();
            searchResultPage.selectRoom();
            travelDetailsPage.navigateTravelDetailsPage();
            travelDetailsPage.validateUpgradeSection();
            travelDetailsPage.validateSpecialRequest();
            travelDetailsPage.validateCheckInName();
            //travelDetailsPage.validatePointProtection();
            travelDetailsPage.clickNextOnly();
            travelDetailsPage.selectAcknowledgeCheckBox();
            reservationConfirmPage.verifyReservation();
            reservationConfirmPage.navigateProfilePage();
            profileDashboardPage.profileHeaderClick();
            profileDashboardPage.reservationLinkClick();
            reservationPage.feedReservationNumber();
            reservationPage.reservationSelect();
            reservationDetailsPage.selectModifyNights();
            reservationDetailsPage.modifyCheckOutDate();
            reservationDetailsPage.clickNextButton();
            //reservationDetailsPage.validatePointProtection();
            reservationDetailsPage.clickChargesNextButton();
            reservationDetailsPage.selectAcknowledgeCheckBox();
            reservationDetailsPage.validateUpdateMessage("Travel Dates have been updated");
            reservationDetailsPage.checkModifiedNights();
            reservationDetailsPage.verifyAddNightModifyDetails();
            reservationDetailsPage.checkOutDateValidation();
            reservationDetailsPage.totalNightsValidations();
            reservationConfirmPage.navigateProfilePage();
            profileDashboardPage.checkModifyNigtsRecentNotes();
            reservationConfirmPage.checkModifyNightsInDB();
            loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_21_AddNights_PP_Opt_Out_Exp_Booking Description: Trip Add
	 * Nights scenarios Test Case :PP Opt Out Exp Booking Date: July/2020
	 * Author: Monideep Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch3" })

	public void tc_21_AddNights_PP_Opt_Out_Exp_Booking(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectFirstAvailableRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();

			travelDetailsPage.validatePointProtection();

			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.validatePointProtection("NO");
			reservationDetailsPage.selectCheckinDateMinus(2);
			reservationDetailsPage.clickNextButton();

			reservationDetailsPage.validatePPsection();
			reservationDetailsPage.clickChargesNextButton();

			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdatedCheckInDate();
			reservationDetailsPage.validatePointProtection("NO");
			reservationDetailsPage.validatePointProtectionModifyHistory("NO");

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_22_AddNights_PP_Change_49_99_ORC_Member Description: Trip Add
	 * Nights scenarios Test Case : TC026_HCWMP-131_ORC member can modify
	 * check-in date with point protection when modification changes the point
	 * protection tier from $49 to $99_ standard window Date: July/2020 Author:
	 * Monideep Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch4" })

	public void tc_22_AddNights_PP_Change_49_99_ORC_Member(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();

			travelDetailsPage.validatePointProtectionTier("49");
			travelDetailsPage.fillCreditCardDetails();

			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.selectCheckinDateMinus(2);
			reservationDetailsPage.clickNextButton();

			reservationDetailsPage.validatePPsectionSelected("20");
			reservationDetailsPage.clickChargesNextButton();

			travelDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdatedCheckInDate();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.validatePointProtectionModifyHistory("YES");

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_23_AddNights_PP_NoChange_69_Tier_CWA_Member Description: Trip
	 * Add Nights scenarios Test Case : TC021_HCWMP-131_CWA member able to
	 * modify check-in date with point protection when modification does not
	 * change $69 tier_standard window Date: July/2020 Author: Monideep Changes
	 * By: NA
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch4" })

	public void tc_23_AddNights_PP_NoChange_69_Tier_CWA_Member(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();

			travelDetailsPage.validatePointProtectionTier("69");
			travelDetailsPage.fillCreditCardDetails();

			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.selectCheckinDateMinus(1);
			reservationDetailsPage.clickNextButton();

			reservationDetailsPage.validatePPsectionSelected("0");
			reservationDetailsPage.clickChargesNextButton();

			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdatedCheckInDate();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.validatePointProtectionModifyHistory("YES");
			reservationDetailsPage.validateAddNightModifyHistory();

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_24_AddNights_Modify_Cannot_Afford_error Description: Trip Add
	 * Nights scenarios Test Case : Add Nights Modify Cannot Afford Error Date:
	 * July/2020 Author: Monideep Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch4" })

	public void tc_24_AddNights_Modify_Cannot_Afford_error(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.pointsLedgerClick();

			pointsLedgerPage.clickPointsAdjustment();
			pointsLedgerPage.enterAdjustmentDetails("Deduct", "0");

			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.selectCheckinDateMinus(1);
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validateLowPointError();

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_25_PP_Change_With_Point_Protection_Blocked Description:
	 * TC010_HCWMP-131_CWP member unable to view point protection details during
	 * check-out date modification when modification changes PPP tier and user
	 * has point protection block_beyond 15 days Date: July/2020 Author:
	 * Monideep Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch4" })

	public void tc_25_PP_Change_With_Point_Protection_Blocked(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPMembershipPage_Web membershipPage = new TRIPMembershipPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.membershipClick();
			membershipPage.clickAddMembershipBlockPP();
			membershipPage.setPPBlock();
			membershipPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();

			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.selectCheckinDateMinus(2);
			reservationDetailsPage.clickNextButton();

			reservationDetailsPage.validatePPsectionSelected("20");
			reservationDetailsPage.clickChargesNextButton();

			travelDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdatedCheckInDate();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.validatePointProtectionModifyHistory("YES");

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_26_Modify_Cancel_HK_Borrow_Points_NonVIP Description: Non VIP
	 * members able to view refunded points after cancelling check-in date
	 * modified reservation(more than 8 days) using current HK
	 * points+borrow_within 15 days and with Point Protection Date: July/2020
	 * Author: Monideep Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch4" })

	public void tc_26_Modify_Cancel_HK_Borrow_Points_NonVIP(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();

			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validateHouseKeepingBorrowRent();
			travelDetailsPage.validatePointProtectionTier("49");
			travelDetailsPage.fillCreditCardDetails();

			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.selectCheckinDateMinus(1);
			reservationDetailsPage.clickNextButton();

			reservationDetailsPage.validateHouseKeepingBorrowRent();
			reservationDetailsPage.validatePPsectionSelected("0");
			reservationDetailsPage.clickChargesNextButton();

			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdatedCheckInDate();
			reservationDetailsPage.saveTotalPointsCost();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.validatePointProtectionModifyHistory("YES");

			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.verifyAndClickonCancel();
			reservationDetailsPage.validatePointsRefunded();
			reservationDetailsPage.validateHKPointsRefunded();
			reservationDetailsPage.cancelReservation();

			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.pointsLedgerClick();

			pointLedgerPage.clickTransactionTab();
			pointLedgerPage.clickApplyFilter();
			pointLedgerPage.applyFilterSelect("Resort Reservation - Cancel");
			pointLedgerPage.clickApplyButton();
			pointLedgerPage.validateHKRefund();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_27_Modify_Cancel_Rent_Borrow_Points_MGVC_Member Description:
	 * MGVC member able to view refunded points after cancelling check-in date
	 * modified reservation using current points+borrow+rent_beyond 15 days
	 * Date: July/2020 Author: Monideep Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch4" })

	public void tc_27_Modify_Cancel_Rent_Borrow_Points_MGVC_Member(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();
			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();
			profileDashboardPage.clickNewVacationLink();
			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();
			searchResultPage.selectRoom();
			travelDetailsPage.navigateTravelDetailsPage();

			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointBorrowRent("Both");
			travelDetailsPage.validatePointProtectionTier("49");
			travelDetailsPage.fillCreditCardDetails();

			travelDetailsPage.selectAcknowledgeCheckBox();
			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.selectCheckinDateMinus(1);
			reservationDetailsPage.clickNextButton();

			reservationDetailsPage.validatePointBorrowRent("Borrow");
			reservationDetailsPage.validatePPsectionSelected("0");
			reservationDetailsPage.clickChargesNextButton();

			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateUpdatedCheckInDate();
			reservationDetailsPage.saveTotalPointsCost();
			reservationDetailsPage.validatePointProtection("YES");
			reservationDetailsPage.validatePointProtectionModifyHistory("YES");

			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();
			reservationDetailsPage.verifyAndClickonCancel();
			reservationDetailsPage.validatePointsRefundedPartial();
			reservationDetailsPage.cancelReservation();

			reservationConfirmPage.navigateProfilePage();
			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.pointsLedgerClick();

			pointLedgerPage.clickTransactionTab();
			pointLedgerPage.clickApplyFilter();
			pointLedgerPage.applyFilterSelect("Resort Reservation - Cancel");
			pointLedgerPage.clickApplyButton();
			pointLedgerPage.validatePointsRefundBorrowed();
			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/**
	 * Method:
	 * tc_028_UnableToModifyNight_CheckInDateWithin48HoursForExistingReservation_ORCMember
	 * Description: ORC member unable to modify night when check-in date of
	 * existing reservation is within 48 hours window from current date Date:
	 * July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch4" })
	public void tc_028_UnableToModifyNight_CheckInDateWithin48HoursForExistingReservation_ORCMember(
			Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.modifyNightsPencilButtonAbsense();

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/**
	 * Method: tc_029_AbleToModifyCheckInDateWithLimitedAvailability_CWAMember
	 * Description: CWA member able to modify check-in date with limited
	 * availability in calendar view Date: July/2020 Author: Kamalesh Changes
	 * By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_029_AbleToModifyCheckInDateWithLimitedAvailability_CWAMember(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			// travelDetailsPage.validateUpgradeSection();
			// travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.fetchNights("BeforeModification");
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.validateLimitedAvailabilityIcon_CheckInDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.clickNextButtonCharges();
			reservationDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckInDateModification();
			reservationDetailsPage.fetchNights("AfterModification");
			reservationDetailsPage.compareNights("BeforeModification", "AfterModification");

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/**
	 * Method: tc_030_ModifyBenefitType Description: Verify that If CWP standard
	 * member has an existing reservation with Benefit type and user change the
	 * existing resort with 3 nights at the begining of check-in date then
	 * benefit type must change in to other benefit types. Date: July/2020
	 * Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_030_ModifyBenefitType(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPNoteCentrePage_Web notesCentrePage = new TRIPNoteCentrePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.fetchNights("BeforeModification");
			reservationDetailsPage.fetchBenefits("BeforeModification");
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckInDateModification();
			reservationDetailsPage.fetchNights("AfterModification");
			reservationDetailsPage.fetchBenefits("AfterModification");
			reservationDetailsPage.compareNights("BeforeModification", "AfterModification");
			reservationDetailsPage.compareBenefits("BeforeModification", "AfterModification");

			profileDashboardPage.navigateToSeeAllNotes();

			notesCentrePage.validateNotesForNightsModification("BeforeModification", "AfterModification");

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/**
	 * Method: tc_031_VerifyOverlappingMessage_ThirdReservation_PRMember
	 * Description: Verify that PR member having two existing reservation with
	 * exact same check-in, check-out date and unit type and modifying check-in
	 * date for a 3rd reservation makes all the 3 reservations having same
	 * check-in check-out date, unit type and check-in name, an overlapping
	 * warning message is getting displayed when modify check-in date upto 3
	 * nights at the beginning of the existing check-in date Date: July/2020
	 * Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_031_VerifyOverlappingMessage_ThirdReservation_PRMember(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.fetchNights("BeforeModification");
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.verifyOverlappingAlertPopUp();
			reservationDetailsPage.verifyOverlappingAlertMsg();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.clickNextButtonCharges();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.verifyOverlappingAlertMsgAfterModification();
			reservationDetailsPage.validateCheckInDateModification();
			reservationDetailsPage.fetchNights("AfterModification");
			reservationDetailsPage.compareNights("BeforeModification", "AfterModification");

			reservationPage.verifyOverlappingAlertMsg();

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/**
	 * Method:
	 * tc_032_VerifyTotalToPayAmountInExpandedAndCollpasedView_NonVipCWPMember
	 * Description: Verify non VIP CWP member able to view valid "Total to Pay"
	 * amount in both expanded and collapsed view when user adds up to 3 nights
	 * after existing check-out date with rented points, rented housekeeping
	 * charges and modification changes the original point protection tier of
	 * the existing reservation_total reservation duration 8+ days and express
	 * booking window Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_032_VerifyTotalToPayAmountInExpandedAndCollpasedView_NonVipCWPMember(Map<String, String> testData)
			throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			// travelDetailsPage.validateUpgradeSection();
			// travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.pointsLedgerClick();

			pointsLedgerPage.clickPointsAdjustment();
			pointsLedgerPage.enterAdjustmentDetails("Deduct", "0");

			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointBorrowRent("Rent");
			reservationDetailsPage.validateHouseKeepingBorrowRent();
			reservationDetailsPage.verifyNoPointProtectionOptionDisable();
			reservationDetailsPage.modifiedPointProtection();
			reservationDetailsPage.validateTotalToPayAmount();

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * Method:
	 * tc_033_VerifyPPSelection_NoPPInOriginalReservation_MGVCMember_StandardWindow
	 * Description: MGVC member able to select point protection for entire
	 * reservation for check-out date modification when original reservation
	 * does not have point protection_standard booking window outside 15 days
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_033_VerifyPPSelection_NoPPInOriginalReservation_MGVCMember_StandardWindow(
			Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			// travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();
			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.fetchNights("BeforeModification");
			reservationDetailsPage.fetchPPValue("BeforeModification");
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.modifiedPointProtection();
			reservationDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.fetchNights("AfterModification");
			reservationDetailsPage.fetchPPValue("AfterModification");
			reservationDetailsPage.validateCheckOutDateModification();
			reservationDetailsPage.compareNights("BeforeModification", "AfterModification");
			reservationDetailsPage.comparePPValue("BeforeModification", "AfterModification");
			reservationDetailsPage.verifyPPStatusInModificationHistory();

			reservationPage.verifyPPstatus();

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/**
	 * Method: tc_034_UnableToViewPointProtectionAndHK Description: Verify
	 * Limited Edition member is unable to view point protection section under
	 * "Charges" and unable to complete modification with point protection when
	 * up to 3 days are added before existing check-out date_beyond 15 days
	 * Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_034_UnableToViewPointProtectionAndHK(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.clickNextOnly();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.pointsLedgerClick();

			pointsLedgerPage.clickPointsAdjustment();
			pointsLedgerPage.enterAdjustmentDetails("Deduct", "0");

			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.fetchPPAbsence("BeforeModification");
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.verifyPPEligibiltyInRightPanel("No");
			reservationDetailsPage.verifyPPAbsenceInChargesSection();
			reservationDetailsPage.validateHKChargesAbsence();
			reservationDetailsPage.clickChargesNextButton();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckOutDateModification();
			reservationDetailsPage.fetchPPAbsence("AfterModification");
			reservationDetailsPage.comparePPAbsence("BeforeModification", "AfterModification");

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/**
	 * Method: tc_035_UnableToModify_DoNotBookApplied Description: Verify that
	 * CWA Member is not able to modify an existing reservationâ€™s number of
	 * nights of the reservation when Do Not Book flag is on. Date: July/2020
	 * Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_035_UnableToModify_DoNotBookApplied(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPMembershipPage_Web membershipPage = new TRIPMembershipPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.membershipClick();

			membershipPage.clickBlockBtn();
			membershipPage.addDoNotBookBlock();
			membershipPage.profileHeaderClick();
			membershipPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.verifyDoNotBookError();
			reservationDetailsPage.overrideDoNotBookBlock();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckOutDateModification();

			loginPage.logoutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * Method: tc_036_VerifyAddNightModificationWithCheckOutOnlyIcon
	 * Description: Verify CWA member is able to modify check-out date with up
	 * to 3 nights after existing check-out date with check-out only icon in
	 * calendar view Date: July/2020 Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_036_VerifyAddNightModificationWithCheckOutOnlyIcon(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateSpecialRequest();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.verifyCheckOutOnlyIcon();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckOutDateModification();

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/**
	 * Method: tc_037_AbleToViewCurrentBorrowedRentedHKPoints Description:
	 * Verify ORC Non VIP member is able to view current+borrow+rented HK points
	 * in Reservation Details page when check-in date modification (Total
	 * reservation duration more than 8 days) does not change $99 tier_Express
	 * window_with Point Protection Date: July/2020 Author: Kamalesh Changes By:
	 * NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_037_AbleToViewCurrentBorrowedRentedHKPoints(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);
		TRIPPointsLedgerPage_Web pointsLedgerPage = new TRIPPointsLedgerPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.fillCreditCardDetails();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.pointsLedgerClick();

			pointsLedgerPage.clickPointsAdjustment();
			pointsLedgerPage.enterAdjustmentDetails("Deduct", "0");

			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.fetchPPValue("BeforeModification");
			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckOutDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.verifyPPEligibiltyInRightPanel("Yes");
			reservationDetailsPage.verifyZeroPPInChargesSection();
			reservationDetailsPage.validateCurrentBorrowRentedHKCharges();
			reservationDetailsPage.validatePointBorrowRent("Rent");
			reservationDetailsPage.validateHouseKeepingBorrowRent();
			reservationDetailsPage.clickChargesNextButton();
			reservationDetailsPage.fillCreditCardDetails();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckOutDateModification();
			reservationDetailsPage.fetchPPValue("AfterModification");
			reservationDetailsPage.comparePPValue("BeforeModification", "AfterModification");
			reservationDetailsPage.validatePointProtectionModifyHistory("Yes");

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/**
	 * Method: tc_038_VerifyRatePlanInDatabase_NightModification Description:
	 * Verify that if CWP Standard member modifies check in date for a
	 * reservation having existing reservation's check-in date on first day of
	 * standard booking window, rate plan will be chnaged Date: July/2020
	 * Author: Kamalesh Changes By: NA
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "trip", "batch5" })
	public void tc_038_VerifyRatePlanInDatabase_NightModification(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		TRIPLoginPage_Web loginPage = new TRIPLoginPage_Web(tcconfig);
		TRIPFindOwnerPage_Web findOwnerPage = new TRIPFindOwnerPage_Web(tcconfig);
		TRIPProfileDashboardPage_Web profileDashboardPage = new TRIPProfileDashboardPage_Web(tcconfig);
		TRIPSearchAvailabilityPage_Web searchAvailabilityPage = new TRIPSearchAvailabilityPage_Web(tcconfig);
		TRIPSearchResultPage_Web searchResultPage = new TRIPSearchResultPage_Web(tcconfig);
		TRIPTravelDetailsPage_Web travelDetailsPage = new TRIPTravelDetailsPage_Web(tcconfig);
		TRIPReservationConfirmationPage_Web reservationConfirmPage = new TRIPReservationConfirmationPage_Web(tcconfig);
		TRIPReservationPage_Web reservationPage = new TRIPReservationPage_Web(tcconfig);
		TRIPReservationDetailsPage_Web reservationDetailsPage = new TRIPReservationDetailsPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.clickButtonLogin();

			findOwnerPage.searchForMember();
			findOwnerPage.clickSearchButton();
			findOwnerPage.selectMember();

			profileDashboardPage.clickNewVacationLink();

			searchAvailabilityPage.selectLocation();
			searchAvailabilityPage.selectCheckInDate();
			searchAvailabilityPage.selectCheckOutDate();
			searchAvailabilityPage.searchAvailableRoom();

			searchResultPage.selectRoom();

			travelDetailsPage.navigateTravelDetailsPage();
			// travelDetailsPage.validateUpgradeSection();
			travelDetailsPage.validateCheckInName();
			travelDetailsPage.validatePointProtection();
			travelDetailsPage.selectAcknowledgeCheckBox();

			reservationConfirmPage.verifyReservation();
			reservationConfirmPage.checkRatePlanInDB();
			reservationConfirmPage.navigateProfilePage();

			profileDashboardPage.profileHeaderClick();
			profileDashboardPage.reservationLinkClick();

			reservationPage.feedReservationNumber();
			reservationPage.reservationSelect();

			reservationDetailsPage.selectModifyNights();
			reservationDetailsPage.modifyCheckInDate();
			reservationDetailsPage.clickNextButton();
			reservationDetailsPage.validatePointProtection();
			reservationDetailsPage.selectAcknowledgeCheckBox();
			reservationDetailsPage.validateCheckInDateModification();
			reservationDetailsPage.checkRatePlanInDBAfterModification();
			reservationDetailsPage.verifyRatePlanInDBBeforeAndAfterModification();

			loginPage.logoutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

}