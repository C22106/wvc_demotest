package careers.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class RecruitingEventsPage extends CareersBasePage {

	public static final Logger log = Logger.getLogger(RecruitingEventsPage.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public RecruitingEventsPage(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By recruitingEventsTab = By.xpath("//div[@class='careerheader']//a[contains(.,'Recruiting Events')]");
	private By recruitingBanner = By.xpath("//div[@class='wyn-banner']");
	private By recruitingHeader = By.xpath("//div[@id='main-content']//span[contains(.,'Recruiting Events')]");
	private By recruitingBreadCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Recruiting Events')]");
	private By sortSelect = By.xpath("//select[@aria-label='Sort By']");
	// Date (oldest first)
	// Date (newest first)
	private By recruitingArticleDate = By.xpath("//article/div[@class='wyn-content__date-box']/span");
	private By recruitingArticleHeader = By.xpath("//article/h3/a");
	private By recruitingArticleDuration = By.xpath("//article[1]/div/span[1]/p");
	private By recruitingArticleLocation = By.xpath("//article[1]/div/span[2]/p");
	private By viewEventCTA = By.xpath("//a[contains(.,'View Event')]");
	private By readMoreCTA = By.xpath("//a[contains(.,'Read More')]");

	public void recruitingEventsValidations() throws ParseException {

		waitUntilElementVisibleBy(driver, recruitingEventsTab, 120);

		if (verifyObjectDisplayed(recruitingEventsTab)) {
			tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
					"Recruiting Events Tab present on the homepage");

			clickElementBy(recruitingEventsTab);

			waitUntilElementVisibleBy(driver, recruitingHeader, 120);
			if (verifyObjectDisplayed(recruitingHeader)) {
				tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
						"Recruiting Events Navigation Successful");
				if (verifyObjectDisplayed(recruitingBanner)) {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
							"Banner Present");
				} else {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
							"Banner not Present on the page");
				}

				if (verifyObjectDisplayed(recruitingBreadCrumb)) {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
							"Breadcrumb Present");
				} else {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
							"Breadcrumb not Present on the page");
				}

				try {
					List<WebElement> viewEvent = driver.findElements(viewEventCTA);

					if (verifyElementDisplayed(viewEvent.get(0))) {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
								"View Event CTA present on the page, total:" + viewEvent.size());
					} else {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
								"View Event CTA not present");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
							"View Event CTA not present");
				}

				try {
					List<WebElement> readMore = driver.findElements(readMoreCTA);

					if (verifyElementDisplayed(readMore.get(0))) {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
								"Read More CTA present on the page, total:" + readMore.size());
					} else {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
								"Read More CTA not present");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
							"Read More CTA not present");
				}

				List<WebElement> dateSortCheck = driver.findElements(recruitingArticleDate);

				if (dateSortCheck.size() > 1) {

					SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd,yyyy");
					Date d3 = null;
					Boolean b6 = null;
					for (int i = 0; i < dateSortCheck.size(); i++) {

						Date d4 = sdf1.parse(dateSortCheck.get(i).getText());

						if (i >= 1) {
							Boolean b4 = d4.after(d3);
							Boolean b5 = d4.equals(d3);
							if (b4 == true || b5 == true) {
								b6 = true;
								d3 = d4;
							} else {
								b6 = false;
								break;
							}
						} else {
							d3 = d4;
						}

					}

					if (b6 == true) {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
								"Date is sorted in Ascending order by default");
					} else {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
								"Date is not sorted in Ascending order");
					}

					if (verifyObjectDisplayed(sortSelect)) {

						new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (oldest first)");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						List<WebElement> dateSortCheck1 = driver.findElements(recruitingArticleDate);

						SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy");
						Date d2 = null;
						Boolean b3 = null;
						for (int i = 0; i < dateSortCheck1.size(); i++) {

							Date d1 = sdf.parse(dateSortCheck1.get(i).getText());

							if (i >= 1) {
								Boolean b1 = d1.before(d2);
								Boolean b2 = d1.equals(d2);
								if (b1 == true || b2 == true) {
									b3 = true;
									d2 = d1;
								} else {
									b3 = false;
									break;
								}
							} else {
								d2 = d1;
							}

						}

						if (b3 == true) {
							tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations",
									Status.PASS, "Date is sorted Oldest to Newest manner");
						} else {
							tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations",
									Status.FAIL, "Date is  sorted in Newest to oldest manner");
						}

					} else {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
								"Sorting Option Not Available");
					}

				} else {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
							"Only one Article, hence no Sorting chechked");
				}

				try {
					List<WebElement> viewEvent = driver.findElements(viewEventCTA);

					if (verifyElementDisplayed(viewEvent.get(0))) {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
								"View Event CTA present on the page, total:" + viewEvent.size());
					} else {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
								"View Event CTA not present");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
							"View Event CTA not present");
				}

				try {
					List<WebElement> readMore = driver.findElements(readMoreCTA);

					if (verifyElementDisplayed(readMore.get(0))) {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
								"Read More CTA present on the page, total:" + readMore.size());
					} else {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
								"Read More CTA not present");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
							"Read More CTA not present");
				}

				new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (newest first)");

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				List<WebElement> numberOfArticles = driver.findElements(recruitingArticleHeader);
				String strfirstNewsHeader = numberOfArticles.get(0).getText().toUpperCase();
				if (verifyElementDisplayed(numberOfArticles.get(0))) {

					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
							"Articles present in Recruiting Events Page, Header of first Article: "
									+ numberOfArticles.get(0).getText());

					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
							"Article Duration: " + driver.findElement(recruitingArticleDuration).getText()
									+ " Article Location: " + driver.findElement(recruitingArticleLocation).getText());

					numberOfArticles.get(0).click();

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (driver.getTitle().toUpperCase().contains(strfirstNewsHeader)) {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.PASS,
								"First Article Associated page opened");

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.switchTo().window(tabs2.get(0));

						if (verifyObjectDisplayed(recruitingArticleHeader)) {
							tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations",
									Status.PASS, "Navigated Back to Recruiting Event Page");
						} else {
							tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations",
									Status.FAIL, "Navigation Back to Recruiting Event Page failed");
						}

					} else {
						tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
								"First Article Associated page  could not be opened");
					}

				} else {
					tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
							"Sorting Option Not Available");
				}

			} else {
				tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
						"Recruiting Events navigation unsuccessful");
			}

		} else {
			tcConfig.updateTestReporter("RecruitingEventsPage", "recruitingEventsValidations", Status.FAIL,
					"Recruiting Events Tab not present on the homepage");
		}

	}

}
