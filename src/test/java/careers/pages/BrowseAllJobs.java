package careers.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BrowseAllJobs extends CareersBasePage {

	public static final Logger log = Logger.getLogger(BrowseAllJobs.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public BrowseAllJobs(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(id = "keywords")
	WebElement ieKeyword;

	@FindBy(xpath = "//button[contains(.,'Go')]")
	WebElement ieFindJobsBtn;

	@FindBy(xpath = "//h1[contains(.,'Your Career Destination')] ")
	WebElement ieBannerTitle;

	private By keywordField = By.id("keywords");
	private By locationField = By.id("location");
	private By homepageHeader = By.xpath("//h1[contains(.,'Adventure Together')]");
	private By findJobsButtonHp = By.xpath("//button[contains(.,'Go')]");
	private By findButtonInternal = By.xpath("//button[contains(.,'Find')]");
	private By browseAllJobsTab = By.xpath("//div[@class='careerheader']//a[contains(.,'Browse')]");
	private By browseAllJobsBanner = By.xpath("//div[@class=’wyn-hero-forms’]");
	private By locationSuggestion = By.xpath("//div[@class='wyn-location-suggestions']//li");
	private By resultsText = By.xpath("//div[@class='column wyn-l-flex--vertical-center']/span");
	private By internalKeywordField = By.xpath("//input[@aria-label='Search']");
	private By internalLocationField = By.xpath("//input[@placeholder='Location']");
	private By intermalPageJobId = By.id("job-id");
	private By internalPageJobDesc = By.xpath("//h3[contains(.,'Job Function')]/following-sibling::div[1]/span");

	// private By pagination = By.xpath("//ul[@class='pagination']/li/a");

	private By articleHeader = By.xpath("//article/h3/a");

	private By articleDetails = By.xpath("//article/p[1]");

	private By articleLocation = By.xpath("//article/p[2]");

	private By jobsId = By.xpath("//article/p[3]");

	// private By articleDate =
	// By.xpath("//div[@class='wyn-content__date-box']/span");

	private By filterTypes = By.xpath("//button[@class='wyn-filters__dropdown__trigger']");
	private By countryFilterTab = By
			.xpath("//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Country')]");

	private By clearAllFilters = By.xpath("//button[@class='wyn-button-primary' and contains(.,'Clear All')]");

	private By jobOrKeywordFields = By.xpath("//input[@aria-label='Job or Keywords']");
	private By searchButton = By.xpath("//button[@type='submit' and contains(.,'Search')]");

	private By jobFunctionFilter = By.xpath("//input[@aria-controls='job-function']");
	private By countryFilter = By.xpath("//input[@aria-controls='country']");
	private By stateFilter = By.xpath("//input[@aria-controls='state']");
	private By cityFilter = By.xpath("//input[@aria-controls='city']");

	private By filterOptions = By.xpath("//div[@class='wyn-filters__dropdown__panel']");

	private By filterLabelName = By.xpath("//label[@role='option']/span");
	private By filterLabelCheckbox = By.xpath("//label[@role='option']/div/span");

	// div[@class='wyn-l-margin-xsmall--bottom'][3]//label/span
	private By locationCountryCheck = By.xpath("//div[@class='wyn-l-margin-xsmall--bottom'][3]//label/span");
	private By locationStateCheck = By.xpath("//div[@class='wyn-l-margin-xsmall--bottom'][4]//label/span");
	private By locationCityCheck = By.xpath("//div[@class='wyn-l-margin-xsmall--bottom'][5]//label/span");
	private By selectedCityCheck = By.xpath("//div[@class='wyn-l-margin-xsmall--bottom'][5]//label/div/input");
	private By searchPage = By.xpath("//span[contains(.,'Search Results')]");
	private By noResults = By
			.xpath("//span[@class='wyn-type-title-1' and contains(.,'Currently there is not a match')]");
	private By searchResultsTitle = By.xpath("//span[@class='wyn-type-title-1' and contains(.,'result')]");
	private By searchResultsKeyword = By.xpath("//span[@class='wyn-type-title-1' and contains(.,'for')]");
	private By resultsName = By.xpath("//a[@class='wyn-content-article-excerpt__title']");
	private By paginationTab = By.xpath("//ul[@class='pagination']/li/a");
	private By sortSelect = By.xpath("//select[@aria-label='Sort by']"); // text
																			// //Date
																			// (oldest
																			// first)
	private By dateCheck = By.xpath("//span[@class='wyn-type-body-2 wyn-l-margin-small--bottom']");
	private By dateColor = By.xpath("//div[@class='wyn-content__date-box wyn-content__date-box-white']/span");
	private By viewJobCTA = By.xpath("//a[contains(.,'View Job')]");
	private By resultClickHeader = By.xpath("//div[@class='columns']//h1");
	private By selectedState = By.xpath("//div[@class='wyn-l-margin-xsmall--bottom'][4]//label/span");
	private By resultPageDate = By
			.xpath("//h3[contains(.,'Posting Date')]//parent::div//span[@class='wyn-type-subheading-2']");
	private By resultPageSchedule = By
			.xpath("//h3[contains(.,'Schedule')]//parent::div//span[@class='wyn-type-subheading-2']");
	private By resultPageShare = By
			.xpath("//h3[contains(.,'Share this job')]//parent::div//div[@style='display: inline-block;']");
	private By resultPageJobFunction = By.xpath("//h3[contains(.,'Job Function')]/following-sibling::div[1]/span");
	private By resultPageJobDetails = By.xpath("//h3[contains(.,'Job Details')]/following-sibling::div[1]/span");
	private By resultPageLeftApplyBtn = By
			.xpath("//div[@class='column is-3 is-offset-1 wyn-type--left']/div[1]/a[contains(.,'Apply For Job')]");
	private By resultPageCenterApplyBtn = By.xpath("//div[@align='center']//a[contains(.,'Apply')]");
	private By resultPageDescription = By.xpath("//div[@class='wyn-l-content-wrapper wyn-l-content']");
	private By resultPageDescriptionText = By.xpath("//div[@class='wyn-l-content-wrapper wyn-l-content']//p");
	private By searchResultsBreadcrumb = By.xpath("//a[contains(.,'Search Results')]");

	@FindBy(xpath = "//a[contains(.,'Search Results')]")
	WebElement searchBreadCrumb;
	// Welcome - Apply Process

	private By stateFiltered = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'State')]//following-sibling::div//label");
	private By cityFiltered = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'City')]//following-sibling::div//label/span");

	String filterParent = "//div[@class='wyn-l-margin-xsmall--bottom'][";
	String filterMid = "]//label/span[contains(.,'";
	String filterLast = "')]";

	String countryParent = "//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Country')]//following-sibling::div//label/span[contains(.,'";
	String stateParent = "//button[@class='wyn-filters__dropdown__trigger' and contains(.,'State')]//following-sibling::div//label/span[contains(.,'";
	String cityParent = "//button[@class='wyn-filters__dropdown__trigger' and contains(.,'City')]//following-sibling::div//label/span[contains(.,'";
	String brandParent = "//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Brand')]//following-sibling::div//label/span[contains(.,'";

	String paginationParent = "//div[@id='main-content']/div[2]//ul[@class='pagination']/li/a[text()='";
	String paginationEnd = "']";

	private By topPaginations = By.xpath("//div[@id='main-content']/div[2]//ul[@class='pagination']/li/a");

	private By jobFunctionFilterTab = By
			.xpath("//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Function')]");

	private By stateFilterTab = By.xpath("//button[@class='wyn-filters__dropdown__trigger' and contains(.,'State')]");

	private By cityFilterTab = By.xpath("//button[@class='wyn-filters__dropdown__trigger' and contains(.,'City')]");

	private By brandFilterTab = By.xpath("//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Brand')]");

	private By scheduleFilterTab = By
			.xpath("//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Schedule')]");

	private By jobLevelFilterTab = By
			.xpath("//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Level')]");

	private By allCountryFilters = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Country')]//following-sibling::div//label/span");
	private By allStateFilters = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'State')]//following-sibling::div//label/span");
	private By allCityFilters = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'City')]//following-sibling::div//label/span");
	private By allBrandFilters = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Brand')]//following-sibling::div//label/span");
	private By allScheduleFilters = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Schedule')]//following-sibling::div//label/span");
	private By allJobLevelFilters = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Level')]//following-sibling::div//label/span");
	private By allJobFunctionFilters = By.xpath(
			"//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Function')]//following-sibling::div//label/span");

	public void browseAllJobsNav() {
		if (verifyObjectDisplayed(browseAllJobsTab)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "browseAllJobsNav", Status.PASS,
					"Browse ALl Jobs Tab present on the homepage");
			clickElementBy(browseAllJobsTab);
			waitUntilElementVisibleBy(driver, searchPage, 120);
			if (verifyObjectDisplayed(searchPage)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "browseAllJobsNav", Status.PASS,
						"Navigated to Browse All Jobs page");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "browseAllJobsNav", Status.FAIL, "Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "browseAllJobsNav", Status.FAIL,
					"Browse ALl Jobs Tab not present on the homepage");
		}
	}

	public void browseAllJobsValidations() throws ParseException {

		if (verifyObjectDisplayed(browseAllJobsTab)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
					"Browse ALl Jobs Tab present on the homepage");
			clickElementBy(browseAllJobsTab);

			waitUntilObjectVisible(driver, jobOrKeywordFields, 120);
			if (verifyObjectDisplayed(internalKeywordField)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
						"Banner present ");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.FAIL,
						"Banner not present");
			}

			scrollDownByPixel(250);
			waitUntilObjectVisible(driver, searchPage, 120);

			if (verifyObjectDisplayed(searchPage)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
						"Navigated to search results page");

				if (verifyObjectDisplayed(searchResultsTitle)) {
					String strTotalResult = driver.findElement(searchResultsTitle).getText();
					String[] strResult = strTotalResult.split("of");
					strTotalResult = strResult[strResult.length - 1];
					tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
							"Number of Results: " + strTotalResult);

					if (verifyObjectDisplayed(paginationTab)) {

						List<WebElement> numberOfArticles = driver.findElements(articleHeader);
						List<WebElement> paginationsTab = driver.findElements(paginationTab);
						tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
								"Pagination Available, Number of articles on the first page: "
										+ numberOfArticles.size());

						clickElementWb(paginationsTab.get(1));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						String strTotalResult2 = driver.findElement(searchResultsTitle).getText();
						if (strTotalResult2.contains("11") && verifyObjectDisplayed(resultsName)) {
							List<WebElement> numberOfArticles2 = driver.findElements(articleHeader);
							tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
									"Navigated to 2nd page and Results are present, Total: "
											+ numberOfArticles2.size());

						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
									"2nd page navigation failed");
						}

						clickElementWb(paginationsTab.get(0));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						String strTotalResult1 = driver.findElement(searchResultsTitle).getText();
						if (strTotalResult1.contains("10") && verifyObjectDisplayed(resultsName)) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
									"Navigated back to 1st Page");

						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
									"1st page navigation failed");
						}

					} else {

						tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
								"The keyword you searched does not contain more than 10 results hence pagination not available");
					}

					if (verifyObjectDisplayed(sortSelect)) {

						new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (oldest first)");
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						List<WebElement> dateSortCheck1 = driver.findElements(dateCheck);

						SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd,yyyy");
						Date d3 = null;
						Boolean b6 = null;
						for (int i = 0; i <= 5; i++) {

							Date d4 = sdf1.parse(dateSortCheck1.get(i).getText());

							if (i >= 1) {
								Boolean b4 = d4.after(d3);
								Boolean b5 = d4.equals(d3);
								if (b4 == true || b5 == true) {
									b6 = true;
									d3 = d4;
								} else {
									b6 = false;
									break;
								}
							} else {
								d3 = d4;
							}

						}

						if (b6 == true) {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.PASS,
									"Date is sorted in Ascending order");
						} else {
							tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
									"Date is not sorted in Ascending order");
						}

					} else {
						tcConfig.updateTestReporter("NewsAndMediaPage", "pressReleaseValidations", Status.FAIL,
								"Sorting Option Not Available");
					}

					new Select(driver.findElement(sortSelect)).selectByVisibleText("Date (newest first)");

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					;

					List<WebElement> strResultHeader = driver.findElements(resultsName);
					List<WebElement> strResultLocation = driver.findElements(articleLocation);
					List<WebElement> strResultDetails = driver.findElements(articleDetails);
					List<WebElement> strResultJobId = driver.findElements(jobsId);
					String strfirstHeader = strResultHeader.get(0).getText();
					String strfirstLocation = strResultLocation.get(0).getText();
					String strfirstDetails = strResultDetails.get(0).getText();
					String strfirstJobId = strResultJobId.get(0).getText();
					tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
							"First Element Header Title is: " + strfirstHeader + " , Locations is " + strfirstLocation
									+ ", Details are: " + strfirstDetails + ", and Job ID: " + strfirstJobId);

					clickElementWb(strResultHeader.get(0));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String newsHeader = driver.findElement(resultClickHeader).getText();
					if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
						if ((strfirstJobId).contains(getElementText(intermalPageJobId).trim())) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
									"Successfully Navigated to associated page with header: " + newsHeader + " and ID: "
											+ getElementText(intermalPageJobId) + " with Job Function: "
											+ getElementText(internalPageJobDesc));
						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.FAIL,
									"Job Id is not matching");
						}
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.FAIL,
								"Could not navigate to associated page");
					}

				} else if (verifyObjectDisplayed(noResults)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
							driver.findElement(noResults).getText() + " " + testData.get("strSearch")
									+ " ,Please search with other keyword");
				}

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.FAIL,
						"Search Results not displayed");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.FAIL,
					"Browse all jobs tab no present");
		}

	}

	public void onlyKeywordSearch() {

		if (verifyObjectDisplayed(keywordField)) {

			if (verifyObjectDisplayed(homepageHeader)) {
				tcConfig.updateTestReporter("CareersHomePage", "onlyKeywordSearch", Status.PASS,
						"Careers HomePage Displayed, Hero Banner Present with Title: "
								+ driver.findElement(homepageHeader).getText());

			} else {
				tcConfig.updateTestReporter("CareersHomePage", "onlyKeywordSearch", Status.FAIL,
						"Careers HomePage Displayed, Hero Banner title is not displayed as Lets Adventure Together");
			}

			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.PASS,
					"Keyword Field Present on the homepage");
			fieldDataEnter(keywordField, testData.get("strKeyword"));

			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.PASS, "Keyword Entered");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			if (browserName.equalsIgnoreCase("CHROME")) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(findJobsButtonHp);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (browserName.equalsIgnoreCase("IE")) {
				String strURL = testData.get("strKeywordSearchURL") + testData.get("strKeyword");

				driver.navigate().to(strURL);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(searchResultsTitle)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String strTotalResult = driver.findElement(searchResultsTitle).getText();
				try {
					String strResultKey = driver.findElement(searchResultsKeyword).getText();
					String[] strResult = strTotalResult.split("of");
					strTotalResult = strResult[strResult.length - 1];

					if (strResultKey.toUpperCase().contains(testData.get("strKeyword").toUpperCase())) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.PASS,
								"Number of Results: " + strTotalResult + " " + strResultKey);
					} else {

						tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
								"Keyword Entered did not match with Search result");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
							"Seacrh results are not generated for: " + testData.get("strKeyword").toUpperCase());
				}
			} else if (verifyObjectDisplayed(noResults)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
						"No Results for searched keyword");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
						"Search results are not displayed");
			}

			if (verifyObjectDisplayed(clearAllFilters)) {
				scrollDownForElementJSBy(clearAllFilters);
				scrollUpByPixel(150);

				clickElementBy(clearAllFilters);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				System.out.println("Clear All Filter option not present");
			}

			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
					"Keyword field not present");
		}

	}

	public void onlyLocationSearch() {

		if (verifyObjectDisplayed(internalLocationField)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
					"Location Field Present on the page");
			fieldDataEnter(internalLocationField, testData.get("strLocation1"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (browserName.equalsIgnoreCase("CHROME")) {
				if (verifyObjectDisplayed(locationSuggestion)) {
					List<WebElement> suggestionList = driver.findElements(locationSuggestion);
					suggestionList.get(0).click();
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
							"Location Entered");
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
							"Entered location is incorrect, No suggestions");
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(findButtonInternal);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (browserName.equalsIgnoreCase("IE")) {
				String strURL = testData.get("strLocationSearchURL") + testData.get("strLocation1");

				driver.navigate().to(strURL);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(searchResultsTitle)) {

				scrollDownForElementJSBy(locationCityCheck);
				scrollUpByPixel(200);

				if (getElementText(locationCityCheck).toUpperCase()
						.contains(testData.get("strLocation1").toUpperCase())) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
							"The location entered matches with the city filter that is "
									+ getElementText(locationCityCheck));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strTotalResult = driver.findElement(searchResultsTitle).getText();

					String[] strResult = strTotalResult.split("of");
					strTotalResult = strResult[strResult.length - 1];
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
							"Number of Results: " + strTotalResult + " for entered location ");

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.FAIL,
							"The location entered does not match with the city filter that is "
									+ getElementText(locationCityCheck));
				}

				if (verifyObjectDisplayed(clearAllFilters)) {
					scrollDownForElementJSBy(clearAllFilters);
					scrollUpByPixel(150);

					clickElementBy(clearAllFilters);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					System.out.println("Clear All Filter option not present");
				}

				driver.switchTo().activeElement().sendKeys(Keys.HOME);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.FAIL,
						"Location field not present");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.FAIL,
					"Location field not present");
		}

	}

	public void keywordAndLocationSearch() {

		if (verifyObjectDisplayed(internalKeywordField) && verifyObjectDisplayed(internalLocationField)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.PASS,
					"Keyword and Location Field Present on the homepage");
			fieldDataEnter(internalKeywordField, testData.get("strKeyword"));

			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.PASS, "Keyword Entered");

			fieldDataEnter(internalLocationField, testData.get("strLocation"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (browserName.equalsIgnoreCase("CHROME")) {
				if (verifyObjectDisplayed(locationSuggestion)) {
					List<WebElement> suggestionList = driver.findElements(locationSuggestion);
					suggestionList.get(0).click();
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
							"Location Entered");
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
							"Entered location is incorrect, No suggestions");
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(findButtonInternal);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (browserName.equalsIgnoreCase("IE")) {
				String strURL = testData.get("strLocationSearchURL") + testData.get("strLocation")
						+ testData.get("strBothSearchURL") + testData.get("strKeyword");

				driver.navigate().to(strURL);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchResultsTitle)) {

				try {
					String strResultKey = driver.findElement(searchResultsKeyword).getText();

					if (strResultKey.toUpperCase().contains(testData.get("strKeyword").toUpperCase())) {
						String strTotalResult = driver.findElement(searchResultsTitle).getText();

						String[] strResult = strTotalResult.split("of");
						strTotalResult = strResult[strResult.length - 1];
						tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.PASS,
								"Number of Results: " + strTotalResult + " " + strResultKey);

						scrollDownForElementJSBy(locationCityCheck);
						scrollUpByPixel(200);

						List<WebElement> selectedList = driver.findElements(locationCityCheck);
						List<WebElement> selectedCity = driver.findElements(selectedCityCheck);

						for (int i = 0; i < selectedList.size(); i++) {

							if (selectedList.get(i).getText().toUpperCase()
									.contains(testData.get("strLocation").toUpperCase())) {
								if (selectedCity.get(i).isSelected()) {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.PASS,
											"The location entered matches with the selected city filter, that is "
													+ getElementText(locationCityCheck));
									break;

								} else {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyLocationSearch", Status.FAIL,
											"The location entered does not match with the city filter, that is "
													+ getElementText(locationCityCheck));
								}

							}

						}

					} else {

						tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
								"Keyword Entered did not match with Search result");
					}
				} catch (Exception e) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
							"Seacrh results are not generated for: " + testData.get("strKeyword").toUpperCase());
				}
			} else if (verifyObjectDisplayed(noResults)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
						"No Results for searched keyword");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
						"Search results are not displayed");
			}

			if (verifyObjectDisplayed(clearAllFilters)) {
				scrollDownForElementJSBy(clearAllFilters);
				scrollUpByPixel(150);

				clickElementBy(clearAllFilters);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				System.out.println("Clear All Filter option not present");
			}

			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
					"Keyword or Location field not present");
		}

	}

	public void filterSearchFieldValdns() {

		if (verifyObjectDisplayed(browseAllJobsTab)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.PASS,
					"Browse ALl Jobs Tab present on the homepage");
			clickElementBy(browseAllJobsTab);
			waitUntilElementVisibleBy(driver, jobOrKeywordFields, 120);
			if (verifyObjectDisplayed(searchPage)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.PASS,
						"Navigated to Browse All Jobs page");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.FAIL,
						"Navigation Failed");
			}

			if (verifyObjectDisplayed(jobOrKeywordFields)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.PASS,
						"Job or Keyword Field present on the Browse all Job Page");

				fieldDataEnter(jobOrKeywordFields, testData.get("strKeyword"));

				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.PASS,
						"Keyword Entered");
				if (browserName.equalsIgnoreCase("CHROME")) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(searchButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else if (browserName.equalsIgnoreCase("IE")) {
					String strURL = testData.get("strKeywordSearchURL") + testData.get("strKeyword");

					driver.navigate().to(strURL);
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(searchResultsTitle)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strTotalResult = driver.findElement(searchResultsTitle).getText();
					try {
						String strResultKey = driver.findElement(searchResultsKeyword).getText();
						String[] strResult = strTotalResult.split("of");
						strTotalResult = strResult[strResult.length - 1];

						if (strResultKey.toUpperCase().contains(testData.get("strKeyword").toUpperCase())) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.PASS,
									"Number of Results: " + strTotalResult + " " + strResultKey);
							List<WebElement> strResultHeader = driver.findElements(resultsName);
							List<WebElement> strResultLocation = driver.findElements(articleLocation);
							List<WebElement> strResultDetails = driver.findElements(articleDetails);
							List<WebElement> strResultJobId = driver.findElements(jobsId);
							String strfirstHeader = strResultHeader.get(0).getText();
							String strfirstLocation = strResultLocation.get(0).getText();
							String strfirstDetails = strResultDetails.get(0).getText();
							String strfirstJobId = strResultJobId.get(0).getText();
							tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.PASS,
									"First Element Header Title is: " + strfirstHeader + " , Locations is "
											+ strfirstLocation + ", Details are: " + strfirstDetails + ", and Job ID: "
											+ strfirstJobId);

							clickElementWb(strResultHeader.get(0));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							String newsHeader = driver.findElement(resultClickHeader).getText();
							if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
								if ((strfirstJobId).contains(getElementText(intermalPageJobId).trim())) {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations",
											Status.PASS,
											"Successfully Navigated to associated page with header: " + newsHeader
													+ " and ID: " + getElementText(intermalPageJobId)
													+ " with Job Function: " + getElementText(internalPageJobDesc));
								} else {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations",
											Status.FAIL, "Job Id is not matching");
								}
							} else {
								tcConfig.updateTestReporter("BrowseAllJobsPage", "brwseAllJobsVAldiations", Status.FAIL,
										"Could not navigate to associated page");
							}

						} else {

							tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.FAIL,
									"Keyword Entered did not match with Search result");
						}

					} catch (Exception e) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "onlyKeywordSearch", Status.FAIL,
								"Seacrh results are not generated for: " + testData.get("strKeyword").toUpperCase());
					}
				} else if (verifyObjectDisplayed(noResults)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.FAIL,
							"No Results for searched keyword");
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.FAIL,
							"Search results are not displayed");
				}

				if (verifyObjectDisplayed(clearAllFilters)) {
					scrollDownForElementJSBy(clearAllFilters);
					scrollUpByPixel(150);

					clickElementBy(clearAllFilters);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					System.out.println("Clear All Filter option not present");
				}

				driver.switchTo().activeElement().sendKeys(Keys.HOME);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.FAIL,
						"Filter Search Option not present");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.FAIL,
					"Browse All Jobs not present on homepage");
		}
	}

	public void allFilterValidations() {
		if (verifyObjectDisplayed(browseAllJobsTab)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
					"Browse ALl Jobs Tab present on the homepage");
			clickElementBy(browseAllJobsTab);
			waitUntilElementVisibleBy(driver, searchPage, 120);
			if (verifyObjectDisplayed(searchPage)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
						"Navigated to Browse All Jobs page");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.FAIL,
						"Navigation Failed");
			}

			List<WebElement> filtersList = driver.findElements(filterTypes);

			for (int i = 0; i < filtersList.size(); i++) {

				scrollDownForElementJSWb(filtersList.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (i != 2 && i != 3 && i != 4) {
					clickElementWb(filtersList.get(i));
				} else if (i == 3) {
					i = 4;
					clickElementWb(filtersList.get(4));
				} else if (i == 4) {
					i = 3;
				}
				try {

					String strFilterName = null;
					switch (i) {
					case 0:
						strFilterName = testData.get("strJobFn");
						break;
					case 1:
						strFilterName = testData.get("strCountry");
						break;
					case 2:
						strFilterName = testData.get("strState");
						break;
					case 3:

						strFilterName = testData.get("strCity");
						break;
					case 4:

						strFilterName = testData.get("strBrand");
						break;

					case 5:
						strFilterName = testData.get("strSchedule");
						break;
					case 6:
						strFilterName = testData.get("strJobLevel");
						break;
					default:
						System.out.println("Error");
						break;
					}

					driver.findElement(By.xpath(filterParent + (i + 2) + filterMid + strFilterName + filterLast))
							.click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
							getElementText(By.xpath(filterParent + (i + 2) + filterMid + strFilterName + filterLast))
									+ " is present in " + getElementText(filterTypes));

					String strSelectedFilter = getElementText(
							By.xpath(filterParent + (i + 2) + filterMid + strFilterName + filterLast));
					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					String strTotalResult = driver.findElement(searchResultsTitle).getText();
					String[] strResult1 = strTotalResult.split("of");
					strTotalResult = strResult1[strResult1.length - 1];
					tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
							"Number of Results: " + strTotalResult);
					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
						break;
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.FAIL,
							"Error in Filter Validation, Please check the data Entered");
					break;
				}

			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "filterSearchFieldValdns", Status.FAIL,
					"Browse All Jobs not present on homepage");
		}

	}

	public void jobIdSearch() {

		if (verifyObjectDisplayed(keywordField)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
					"Keyword Field Present on the homepage");
			fieldDataEnter(keywordField, testData.get("jobID"));

			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS, "Keyword Entered");

			if (browserName.equalsIgnoreCase("CHROME")) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(findJobsButtonHp);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (browserName.equalsIgnoreCase("IE")) {
				String strURL = testData.get("strKeywordSearchURL") + testData.get("jobID");

				driver.navigate().to(strURL);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchResultsTitle)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String strTotalResult = driver.findElement(searchResultsTitle).getText();

				try {
					String strResultKey = driver.findElement(searchResultsKeyword).getText();
					String[] strResult = strTotalResult.split("of");
					strTotalResult = strResult[strResult.length - 1];

					if (strTotalResult.contains("1")) {
						if (strResultKey.toUpperCase().contains(testData.get("jobID").toUpperCase())) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
									"Number of Results: " + strTotalResult + " " + strResultKey);

							if (verifyObjectDisplayed(jobsId)
									&& getElementText(jobsId).contains(testData.get("jobID"))) {
								tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
										"Searched JobsId and Result Jobs ID matched as: " + getElementText(jobsId));
								if (verifyObjectDisplayed(articleHeader)) {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
											"Header: " + getElementText(articleHeader));

								} else {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
											"Header not present ");
								}

								if (verifyObjectDisplayed(articleLocation)) {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
											"Location: " + getElementText(articleLocation));

								} else {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
											"Location not present ");
								}
								if (verifyObjectDisplayed(articleDetails)) {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
											"Details: " + getElementText(articleDetails));

								} else {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
											"Details not present ");
								}

								if (verifyObjectDisplayed(dateCheck)) {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
											"Date: " + getElementText(dateCheck));

								} else {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
											"Date not present ");
								}

							} else {
								tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
										"Searched Job ID and Result Job Id did not macth");
							}

						} else {

							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
									"Job ID Entered did not match with Search result");
						}

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
								"More than one results displayed for searched Job ID");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
							"Seacrh results are not generated for: " + testData.get("strKeyword").toUpperCase());
				}

			} else if (verifyObjectDisplayed(noResults)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
						"No Results for searched Job ID");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
						"Search results are not displayed");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL, "Keyword field not present");
		}
	}

	public void jobDescriptionValdns() {
		waitUntilElementVisibleBy(driver, resultsName, 120);
		if (verifyObjectDisplayed(resultsName)) {
			String strfirstHeader = null;
			// String strfirstLocation = null;
			// String strfirstDetails = null;
			String strfirstJobId = null;
			try {
				List<WebElement> strResultHeader = driver.findElements(resultsName);
				// List<WebElement> strResultLocation =
				// driver.findElements(articleLocation);
				// List<WebElement> strResultDetails =
				// driver.findElements(articleDetails);
				List<WebElement> strResultJobId = driver.findElements(jobsId);
				strfirstHeader = strResultHeader.get(0).getText();
				// strfirstLocation = strResultLocation.get(0).getText();
				// strfirstDetails = strResultDetails.get(0).getText();
				strfirstJobId = strResultJobId.get(0).getText();
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
						"Clicking First Element on the page, with Header: " + strfirstHeader);

				clickElementWb(strResultHeader.get(0));

			} catch (Exception e) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
						"Error in getting all the details for the first job in the ");
			}

			try {
				waitUntilElementVisibleBy(driver, resultClickHeader, 120);
				String newsHeader = driver.findElement(resultClickHeader).getText();
				if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Successfully Navigated to associated page with header: " + newsHeader);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Could not navigate to associated page");
				}

				if ((strfirstJobId).contains(getElementText(intermalPageJobId).trim())) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Job ID Present and Matched: " + strfirstJobId);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Job Id is not matching or is not present");
				}

				if (verifyObjectDisplayed(resultPageDescription)) {

					List<WebElement> paraList = driver.findElements(resultPageDescriptionText);
					if (paraList.size() > 1) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Job Description with brief content present");
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Content not present");
					}

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Job Description not present");
				}

				if (verifyObjectDisplayed(resultPageLeftApplyBtn)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Apply for Job button present in the top left rail");

					clickElementBy(resultPageLeftApplyBtn);

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (driver.getTitle().contains("Welcome - Apply Process")) {

						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Navigated to Apply For Job Page");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						driver.navigate().back();

						waitUntilElementVisibleBy(driver, resultPageLeftApplyBtn, 120);

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Navigated to Apply For Job Page failed");
					}

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Apply for job button not present in the left");
				}

				if (verifyObjectDisplayed(resultPageJobFunction)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Job Function present as: " + getElementText(resultPageJobFunction));
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Job Function not present");
				}

				if (verifyObjectDisplayed(resultPageJobDetails)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Job Details present as: " + getElementText(resultPageJobDetails));
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Job Details not present");
				}

				if (verifyObjectDisplayed(resultPageDate)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Posting Date present as: " + getElementText(resultPageDate));
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Posting Date not present");
				}

				if (verifyObjectDisplayed(resultPageSchedule)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Schedule present as: " + getElementText(resultPageSchedule));
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Schedule not present");
				}

				scrollDownByPixel(200);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(resultPageShare)) {
					List<WebElement> shareList = driver.findElements(resultPageShare);

					for (int i = 0; i < shareList.size(); i++) {

						String strShare = shareList.get(i).getAttribute("data-network");

						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Sharing Option present at: " + strShare);

					}

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Sharing Option not present not present");
				}

				scrollDownForElementJSBy(resultPageCenterApplyBtn);
				scrollUpByPixel(200);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(resultPageCenterApplyBtn)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Apply for Job button present in the bootom center of the page");

					clickElementBy(resultPageCenterApplyBtn);

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (driver.getTitle().contains("Welcome - Apply Process")) {

						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Navigated to Apply For Job Page");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						driver.navigate().back();

						waitUntilElementVisibleBy(driver, resultPageCenterApplyBtn, 120);

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Navigated back to job details page");

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Navigation to Apply For Job Page failed");
					}

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Apply for job button not present in the bottom of the page");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
						"Error in Validating Job Description page");
			}

		} else {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
					"Results not displayed");

		}

	}

	public void jobPageValidations() {

		waitUntilElementVisible(driver, ieFindJobsBtn, 120);

		if (verifyElementDisplayed(ieFindJobsBtn)) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.PASS,
					"Find Jobs Button present on the homepage");

			ieFindJobsBtn.click();

			waitUntilObjectVisible(driver, searchPage, 120);

			if (verifyObjectDisplayed(searchPage)) {

				if (driver.getTitle().toUpperCase().contains("BROWSE ALL JOBS")) {

					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.PASS,
							"Navigated to search results page with title: " + driver.getTitle());
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.FAIL,
							"Page Title different from Browse All Jobs");
				}
				if (verifyObjectDisplayed(searchResultsTitle)) {
					String strTotalResult = driver.findElement(searchResultsTitle).getText();
					String[] strResult = strTotalResult.split("of");
					strTotalResult = strResult[strResult.length - 1];
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.PASS,
							"Number of Results: " + strTotalResult);
					try {
						List<WebElement> dateList = driver.findElements(dateColor);

						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.PASS,
								"Dat present on the page with White Background, 1st Date " + dateList.get(0).getText());

					} catch (Exception e) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.FAIL,
								"Date is not as expected");
					}

					List<WebElement> strResultHeader = driver.findElements(resultsName);
					List<WebElement> strResultLocation = driver.findElements(articleLocation);
					List<WebElement> strResultDetails = driver.findElements(articleDetails);
					List<WebElement> strViewJobBtn = driver.findElements(viewJobCTA);
					List<WebElement> strResultJobId = driver.findElements(jobsId);
					String strfirstHeader = strResultHeader.get(0).getText();
					String strfirstLocation = strResultLocation.get(0).getText();
					String strfirstDetails = strResultDetails.get(0).getText();
					String strfirstJobId = strResultJobId.get(0).getText();
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.PASS,
							"First Element Header Title is: " + strfirstHeader + " , Locations is " + strfirstLocation
									+ ", Details are: " + strfirstDetails + ", and Job ID: " + strfirstJobId);

					clickElementWb(strViewJobBtn.get(0));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					String newsHeader = driver.findElement(resultClickHeader).getText();
					if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
						if ((strfirstJobId).contains(getElementText(intermalPageJobId).trim())) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.PASS,
									"Successfully Navigated to associated page with header: " + newsHeader + " and ID: "
											+ getElementText(intermalPageJobId) + " with Job Function: "
											+ getElementText(internalPageJobDesc));
						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.FAIL,
									"Job Id is not matching");
						}
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.FAIL,
								"Could not navigate to associated page");
					}

					driver.navigate().back();

					waitUntilObjectVisible(driver, searchPage, 120);

					if (verifyObjectDisplayed(searchPage)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.PASS,
								"Navigated Back to seacrh result page");

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.FAIL,
								"Failed to navigate back to search result page");
					}

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.FAIL,
							"Search Results title is not displayed");
				}
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobPageValidations", Status.FAIL,
						"Search Results not displayed");
			}

		} else {

		}

	}

	public void jobFilterSameCityValdn() {

		String strFilterCountry = testData.get("strCountry");
		String strFilterState = testData.get("strState");
		String strFilterCity = testData.get("strCity");

		if (verifyObjectDisplayed(countryFilterTab)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
					"Country Filter Tab present on the browse all job page");
			scrollDownForElementJSBy(countryFilterTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(countryFilterTab);

			try {

				scrollDownForElementJSBy(By.xpath(countryParent + strFilterCountry + filterLast));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(By.xpath(countryParent + strFilterCountry + filterLast)).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
						getElementText(By.xpath(countryParent + strFilterCountry + filterLast))
								+ " is present in country");

				/*
				 * scrollDownForElementJSBy(By.xpath(stateParent + strFilterState +
				 * filterLast)); waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * scrollUpByPixel(150); waitForSometime(tcConfig.getConfig().get("LowWait"));
				 */

				/*
				 * driver.findElement(By.xpath(stateParent + strFilterState +
				 * filterLast)).click(); waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn",
				 * Status.PASS, getElementText(By.xpath(stateParent + strFilterState +
				 * filterLast)) + " is present in state");
				 */

				scrollDownForElementJSBy(By.xpath(cityParent + strFilterCity + filterLast));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(By.xpath(cityParent + strFilterCity + filterLast)).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
						getElementText(By.xpath(cityParent + strFilterCity + filterLast)) + " is present in city ");

			} catch (Exception e) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.FAIL,
						"Error in selecting Filter");
			}

			if (driver.getTitle().toUpperCase().contains("AVAILABLE JOBS")) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
						"Page title has changed to: " + driver.getTitle());
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.FAIL,
						"Page title  has not  changed to Available Jobs at Wyndham Destinations ");
			}

			List<WebElement> stateSelectedList = driver.findElements(stateFiltered);
			scrollDownForElementJSWb(stateSelectedList.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String strFirstState = stateSelectedList.get(0).getText();
			String[] fisrtCountryName = strFirstState.split("\\(");
			strFirstState = fisrtCountryName[0];
			strFirstState = strFirstState.trim();
			String strSecondState = null;
			if (stateSelectedList.size() > 1) {
				strSecondState = stateSelectedList.get(1).getText();
				String[] secondCountryName = strSecondState.split("\\(");
				strSecondState = secondCountryName[0];
				strSecondState = strSecondState.trim();

			}
			if (stateSelectedList.size() > 1) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
						stateSelectedList.size() + " state are displayed with same city as: " + strFilterCity);
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.FAIL,
						"State with same city are not displayed");
			}

			String strSelectedFilter = getElementText((selectedState));
			String[] strResult = strSelectedFilter.split(" ");

			strSelectedFilter = strResult[strResult.length - 1];
			strSelectedFilter = strSelectedFilter.replace(")", "").trim();
			strSelectedFilter = strSelectedFilter.replace("(", "").trim();

			String strTotalResult = driver.findElement(searchResultsTitle).getText();
			String[] strResult1 = strTotalResult.split("of");
			strTotalResult = strResult1[strResult1.length - 1];
			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
					"Number of Results: " + strTotalResult);
			/*
			 * if (strTotalResult.contains(strSelectedFilter)) {
			 * driver.switchTo().activeElement().sendKeys(Keys.HOME);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn",
			 * Status.PASS, "The filter Result and Search result is equal that is: " +
			 * strTotalResult); } else { tcConfig.updateTestReporter("BrowseAllJobsPage",
			 * "jobFilterSameCityValdn", Status.FAIL,
			 * "The filter number and the result displayed did not match");
			 * 
			 * }
			 */

			// paginationCheck(strTotalResult);

			if (stateSelectedList.size() > 1) {

				scrollDownForElementJSWb(stateSelectedList.get(stateSelectedList.size() - 1));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				stateSelectedList.get(stateSelectedList.size() - 1).click();

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				List<WebElement> stateSelectedList2 = driver.findElements(stateFiltered);
				if (stateSelectedList2.get(0).getText().contains(strSecondState)) {

					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
							"Last State selected");

					String strSelectedFilter2 = stateSelectedList2.get(0).getText();
					String[] strResult2 = strSelectedFilter2.split(" ");

					strSelectedFilter2 = strResult2[strResult2.length - 1];
					strSelectedFilter2 = strSelectedFilter2.replace(")", "").trim();
					strSelectedFilter2 = strSelectedFilter2.replace("(", "").trim();

					String strTotalResult2 = driver.findElement(searchResultsTitle).getText();
					String[] strResult3 = strTotalResult2.split("of");
					strTotalResult2 = strResult3[strResult3.length - 1];
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
							"Number of Results: " + strTotalResult2);
					if (strTotalResult2.contains(strSelectedFilter2)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult2 + " for "
										+ strSecondState);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.FAIL,
								"The filter number and the result displayed did not match");

					}

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.FAIL,
							"States Were selected automatically");
				}

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.FAIL,
						"State with same city are not displayed");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFilterSameCityValdn", Status.FAIL,
					"Country Tab not present");
		}

	}

	public void filterDiffPageNavValdns() {

		if (verifyObjectDisplayed(browseAllJobsTab)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
					"Browse ALl Jobs Tab present on the homepage");
			clickElementBy(browseAllJobsTab);

			String strFilterCountry = testData.get("strCountry");
			String strFilterState = testData.get("strState");
			// String strFilterCity = testData.get("strCity");
			waitUntilObjectVisible(driver, searchPage, 120);
			if (verifyObjectDisplayed(searchPage)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
						"Navigated to search results page");

				if (verifyObjectDisplayed(countryFilterTab)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
							"Country Filter Tab present on the browse all job page");
					clickElementBy(countryFilterTab);

					try {
						scrollDownForElementJSBy(By.xpath(countryParent + strFilterCountry + filterLast));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(150);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						driver.findElement(By.xpath(countryParent + strFilterCountry + filterLast)).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
								getElementText(By.xpath(countryParent + strFilterCountry + filterLast))
										+ " is present in country");

						scrollDownForElementJSBy(By.xpath(stateParent + strFilterState + filterLast));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						scrollUpByPixel(150);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						driver.findElement(By.xpath(stateParent + strFilterState + filterLast)).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
								getElementText(By.xpath(stateParent + strFilterState + filterLast))
										+ " is present in state");

						String strSelectedFilter = getElementText(
								(By.xpath(stateParent + strFilterState + filterLast)));
						String[] strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();

						String strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];
						tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
								"Number of Results: " + strTotalResult);
						if (strTotalResult.contains(strSelectedFilter)) {
							driver.switchTo().activeElement().sendKeys(Keys.HOME);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
									"The filter Result and Search result is equal that is: " + strTotalResult);
						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
									"The filter number and the result displayed did not match");

						}

						try {
							List<WebElement> paginationsList = driver.findElements(paginationTab);
							int intPagination = paginationsList.size() / 2;

							if (intPagination >= 6) {
								for (int i = 0; i < 5; i++) {
									paginationsList.get(i + 1).click();
									waitForSometime(tcConfig.getConfig().get("LowWait"));

									String strTotalResult1 = driver.findElement(searchResultsTitle).getText();
									String[] strResult2 = strTotalResult1.split("of");
									strTotalResult1 = strResult2[strResult2.length - 1];

									if (strTotalResult1.contains(strSelectedFilter)) {
										// driver.switchTo().activeElement().sendKeys(Keys.HOME);
										waitForSometime(tcConfig.getConfig().get("LowWait"));
										tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
												Status.PASS, "The filter Result and Search result is equal that is: "
														+ strTotalResult1 + " on " + (i + 1) + " page");
									} else {
										tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
												Status.FAIL, "The filter number and the result displayed did not match"
														+ " on " + (i + 1) + " page");

									}

								}

								paginationsList.get(0).click();
								waitForSometime(tcConfig.getConfig().get("MedWait"));

							} else if (intPagination < 6) {
								for (int i = 0; i < intPagination; i++) {
									paginationsList.get(i + 1).click();
									waitForSometime(tcConfig.getConfig().get("LowWait"));

									String strTotalResult1 = driver.findElement(searchResultsTitle).getText();
									String[] strResult2 = strTotalResult1.split("of");
									strTotalResult1 = strResult2[strResult2.length - 1];

									if (strTotalResult1.contains(strSelectedFilter)) {
										driver.switchTo().activeElement().sendKeys(Keys.HOME);
										waitForSometime(tcConfig.getConfig().get("LowWait"));
										tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
												Status.PASS, "The filter Result and Search result is equal that is: "
														+ strTotalResult1 + " on " + (i + 1) + " page");
									} else {
										tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
												Status.FAIL, "The filter number and the result displayed did not match"
														+ " on " + (i + 1) + " page");

									}

								}

								paginationsList.get(0).click();
								waitForSometime(tcConfig.getConfig().get("MedWait"));

							}

						} catch (Exception e) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
									"Pagination Not Available");
						}

						try {
							/*
							 * 
							 * List<WebElement> strResultHeader = driver.findElements(resultsName); String
							 * strfirstHeader = strResultHeader.get(0).getText();
							 * 
							 * tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
							 * Status.PASS, "Clicking First Element with Header: " + strfirstHeader);
							 * 
							 * 
							 * 
							 * clickElementWb(strResultHeader.get(0)); waitUntilElementVisibleBy(driver,
							 * resultClickHeader, 120);
							 * 
							 * String newsHeader = driver.findElement(resultClickHeader).getText(); if
							 * (newsHeader.equalsIgnoreCase(strfirstHeader)) {
							 * tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
							 * Status.PASS, "Successfully Navigated to associated page with header: " +
							 * newsHeader);
							 * 
							 * if (verifyObjectDisplayed(searchResultsBreadcrumb)) {
							 * tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
							 * Status.PASS, "Search Results present in the breadcrumb");
							 * waitForSometime(tcConfig.getConfig().get( "MedWait"));
							 * clickElementBy(searchResultsBreadcrumb); //driver.navigate().back();
							 * //clickElementJSWithWait(searchBreadCrumb);
							 * 
							 * Actions action= new Actions(driver); action.moveToElement(searchBreadCrumb);
							 * action.click();
							 * 
							 * Action ac= action.build(); ac.perform();
							 * 
							 * 
							 * waitUntilElementVisibleBy(driver, searchResultsTitle, 120);
							 * 
							 * String strTotalResult2 = driver.findElement(searchResultsTitle).getText();
							 * String[] strResult2 = strTotalResult2.split("of"); strTotalResult2 =
							 * strResult2[strResult2.length - 1];
							 * tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
							 * Status.PASS, "Number of Results: " + strTotalResult2); if
							 * (strTotalResult2.contains(strSelectedFilter)) {
							 * driver.switchTo().activeElement().sendKeys(Keys. HOME);
							 * waitForSometime(tcConfig.getConfig().get( "LowWait"));
							 * tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
							 * Status.PASS, "The filter Result and Search result is equal that is: " +
							 * strTotalResult2 + " after nav back from details page"); } else {
							 * tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn",
							 * Status.FAIL,
							 * "The filter number and the result displayed did not match after navigating back from details page"
							 * );
							 * 
							 * }
							 * 
							 * } else { tcConfig.updateTestReporter("BrowseAllJobsPage",
							 * "filterDiffPageNavValdn", Status.PASS,
							 * "Search Results not present in the breadcrumb"); }
							 * 
							 * } else { tcConfig.updateTestReporter("BrowseAllJobsPage",
							 * "filterDiffPageNavValdn", Status.FAIL,
							 * "Could not navigate to associated page"); }
							 * 
							 */} catch (Exception e) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
									"Error in getting first element header");
						}
					} catch (Exception e) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
								"Error in selecting Filter");
					}

				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
							"Country Tab not present");
				}

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
						"Search Results not displayed");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
					"Browse all jobs tab no present");
		}

	}

	public void filterResultsVerification(By filterCheck) {
		String strSelectedFilter = getElementText(filterCheck);
		String[] strResult = strSelectedFilter.split(" ");
		strSelectedFilter = strResult[strResult.length - 1];
		strSelectedFilter = strSelectedFilter.replace(")", "").trim();
		strSelectedFilter = strSelectedFilter.replace("(", "").trim();

		String strTotalResult = driver.findElement(searchResultsTitle).getText();
		String[] strResult1 = strTotalResult.split("of");
		strTotalResult = strResult1[strResult1.length - 1];
		tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
				"Number of Results: " + strTotalResult);
		if (strTotalResult.contains(strSelectedFilter)) {
			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
					"The filter Result and Search result is equal that is: " + strTotalResult);
		} else {
			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.FAIL,
					"The filter number and the result displayed did not match");

		}

	}

	public void cityFilterResultsVerification(By filterCheck) {
		List<WebElement> stateList = driver.findElements(filterCheck);
		String strSelectedFilter = null;
		int temp = 0;
		int prev;
		for (int i = 0; i < stateList.size(); i++) {
			strSelectedFilter = stateList.get(i).getText();
			String[] strResult = strSelectedFilter.split(" ");
			strSelectedFilter = strResult[strResult.length - 1];
			strSelectedFilter = strSelectedFilter.replace(")", "").trim();
			strSelectedFilter = strSelectedFilter.replace("(", "").trim();
			prev = Integer.parseInt(strSelectedFilter);
			temp = temp + prev;
		}

		String strFilterNo = String.valueOf(temp);
		String strTotalResult = driver.findElement(searchResultsTitle).getText();
		String[] strResult1 = strTotalResult.split("of");
		strTotalResult = strResult1[strResult1.length - 1];
		tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
				"Number of Results: " + strTotalResult);
		if (strTotalResult.contains(strFilterNo)) {
			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
					"The filter Result and Search result is equal that is: " + strTotalResult);
		} else {
			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.FAIL,
					"The filter number and the result displayed did not match");

		}

	}

	public void clearAllFilter() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(clearAllFilters)) {
			scrollDownForElementJSBy(clearAllFilters);
			scrollUpByPixel(150);

			clickElementBy(clearAllFilters);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			System.out.println("Clear All Filter option not present");
		}

		driver.switchTo().activeElement().sendKeys(Keys.HOME);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void aspenValidations() {

		// waitUntilElementVisibleBy(driver, cityFilterTab, 120);
		String strFilterCity = testData.get("strCity");
		String strFilterBrand = testData.get("strBrand");

		singleCityVal(strFilterCity);

		singleBrandVal(strFilterBrand);

		clearAllFilter();

	}

	public void resortQuestBritishColVal() {
		// brand = resort quest by Wyn Vac Rentals
		// state= british coloumbia
		// country canada

		String strFilterBrand = testData.get("strBrand2");
		String strFilterState = testData.get("strState2");
		String strFilterCountry = testData.get("strCountry2");

		singleBrandVal(strFilterBrand);

		singleStateVal(strFilterState);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> countryList = driver.findElements(allCountryFilters);

		if (countryList.size() == 1) {
			String countrySelected = getElementText(By.xpath(countryParent + strFilterCountry + filterLast))
					.toUpperCase();
			if (countrySelected.contains("CANADA")) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "resortQuestBritishColVal", Status.PASS,
						"Canada is selected automatically");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "resortQuestBritishColVal", Status.FAIL,
						"Canada is not selected");
			}
		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "resortQuestBritishColVal", Status.FAIL,
					"There are more than one country present");
		}

		/*
		 * driver.switchTo().activeElement().sendKeys(Keys.HOME);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 */
		driver.findElement(By.xpath(stateParent + strFilterState + filterLast)).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		filterResultsVerification((By.xpath(brandParent + strFilterBrand + filterLast)));

		clearAllFilter();
	}

	public void myrtleBeachAndBrandsVal() {
		String strFilterCity = testData.get("strCity3");
		String strFilterBrand = testData.get("strBrand3");
		String strFilterBrand2 = testData.get("strBrand3_1");

		singleCityVal(strFilterCity);

		singleBrandVal(strFilterBrand);
		scrollDownForElementJSBy(By.xpath(brandParent + strFilterBrand + filterLast));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(By.xpath(brandParent + strFilterBrand + filterLast)).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		/*
		 * scrollDownForElementJSBy(By.xpath(brandParent + strFilterBrand2 +
		 * filterLast)); waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * scrollUpByPixel(150); waitForSometime(tcConfig.getConfig().get("MedWait"));
		 */

		String brandParentText = "//button[@class='wyn-filters__dropdown__trigger' and contains(.,'Brand')]//following-sibling::div//label/span[text()='";
		String textLast = "']";

		driver.findElement(By.xpath(brandParentText + strFilterBrand2 + textLast)).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("BrowseAllJobsPage", "singleBrandVal", Status.PASS,
				getElementText(By.xpath(brandParentText + strFilterBrand2 + textLast)) + " is present in brand");

		filterResultsVerification((By.xpath(brandParentText + strFilterBrand2 + textLast)));

		clearAllFilter();

	}

	public void anaheimAndBrandVal() {
		String strFilterCity = testData.get("strCity4");
		String strFilterBrand = testData.get("strBrand4");
		singleCityVal(strFilterCity);

		singleBrandVal(strFilterBrand);

		scrollDownForElementJSBy(By.xpath(cityParent + strFilterCity + filterLast));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.findElement(By.xpath(cityParent + strFilterCity + filterLast)).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> countryList = driver.findElements(allCountryFilters);
		List<WebElement> stateList = driver.findElements(allStateFilters);
		List<WebElement> cityList = driver.findElements(allCityFilters);

		if (countryList.size() > 1) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "anaheimAndBrandVal", Status.PASS,
					"No Countries Selected");

		} else if (countryList.size() == 1) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "anaheimAndBrandVal", Status.FAIL,
					countryList.get(0).getText() + " is selected");
		}

		if (stateList.size() > 1) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "anaheimAndBrandVal", Status.PASS, "No State is Selected");

		} else if (stateList.size() == 1) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "anaheimAndBrandVal", Status.FAIL,
					stateList.get(0).getText() + " is selected");
		}

		if (cityList.size() > 1) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "anaheimAndBrandVal", Status.PASS, "No City is Selected");

		} else if (cityList.size() == 1) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "anaheimAndBrandVal", Status.FAIL,
					cityList.get(0).getText() + " is selected");
		}

		filterResultsVerification((By.xpath(brandParent + strFilterBrand + filterLast)));

		clearAllFilter();

	}

	public void bendAndOregonVal() {
		String strFilterCity = testData.get("strCity");

		singleCityVal(strFilterCity);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> countryList = driver.findElements(allCountryFilters);
		List<WebElement> stateList = driver.findElements(allStateFilters);
		String stateSelected = stateList.get(0).getText();

		if (stateList.size() > 1) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "bendAndOregonVal", Status.FAIL,
					"More than One state displayed");

		} else if (stateList.size() == 1) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "bendAndOregonVal", Status.PASS,
					stateList.get(0).getText() + " is selected");
		}

		String strSelectedFilter = getElementText(selectedState);
		String[] strResult = strSelectedFilter.split(" ");
		strSelectedFilter = strResult[strResult.length - 1];
		strSelectedFilter = strSelectedFilter.replace(")", "").trim();
		strSelectedFilter = strSelectedFilter.replace("(", "").trim();

		jobValdandBack(strSelectedFilter);
		List<WebElement> stateList2 = driver.findElements(allStateFilters);
		if (stateList2.size() > 1) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "bendAndOregonVal", Status.FAIL,
					"More than One state displayed");

		} else if (stateList2.size() == 1 && stateList2.get(0).getText().equals(stateSelected)) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "bendAndOregonVal", Status.PASS,
					stateSelected + " is selected");
		}

		clearAllFilter();

	}

	public void wanakaRotoruaVal() {

		waitUntilObjectVisible(driver, searchResultsTitle, 120);

		String strFilterCity = testData.get("strCity1");
		String strFilterCity2 = testData.get("strCity2");
		String strFilterCountry = testData.get("strCountry2");

		waitUntilObjectVisible(driver, searchResultsTitle, 120);

		clickElementBy(cityFilterTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(By.xpath(cityParent + strFilterCity + filterLast))) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "wanakaRotoruaVal", Status.PASS,
					strFilterCity + " Present in the City Filter");
		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "wanakaRotoruaVal", Status.FAIL,
					strFilterCity + " not Present in the City Filter");

		}

		if (verifyObjectDisplayed(By.xpath(cityParent + strFilterCity2 + filterLast))) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "wanakaRotoruaVal", Status.PASS,
					strFilterCity2 + " Present in the City Filter");
		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "wanakaRotoruaVal", Status.WARNING,
					strFilterCity2 + " not Present in the City Filter");

		}

		singleCountryVal(strFilterCountry);

		if (verifyObjectDisplayed(By.xpath(cityParent + strFilterCity + filterLast))) {

			scrollDownForElementJSBy(By.xpath(cityParent + strFilterCity + filterLast));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(By.xpath(cityParent + strFilterCity + filterLast)).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCityVal", Status.PASS,
					getElementText(By.xpath(cityParent + strFilterCity + filterLast)) + " is present in city");

			filterResultsVerification((By.xpath(cityParent + strFilterCity + filterLast)));

		} else if (verifyObjectDisplayed(By.xpath(cityParent + strFilterCity2 + filterLast))) {

			scrollDownForElementJSBy(By.xpath(cityParent + strFilterCity2 + filterLast));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(By.xpath(cityParent + strFilterCity2 + filterLast)).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCityVal", Status.PASS,
					getElementText(By.xpath(cityParent + strFilterCity2 + filterLast)) + " is present in city");

			filterResultsVerification((By.xpath(cityParent + strFilterCity2 + filterLast)));

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCityVal", Status.FAIL,
					"Searched City Filter not present");
		}

		clearAllFilter();

	}

	public void countryStateVal() {

		String strFilterCountry = testData.get("strCountry2");
		String strFilterState = testData.get("strState2");

		singleCountryVal(strFilterCountry);

		if (verifyObjectDisplayed(By.xpath(stateParent + strFilterState + filterLast))) {

			scrollDownForElementJSBy(By.xpath(stateParent + strFilterState + filterLast));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(By.xpath(stateParent + strFilterState + filterLast)).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.PASS,
					getElementText(By.xpath(stateParent + strFilterState + filterLast)) + " is present in state");

			filterResultsVerification((By.xpath(stateParent + strFilterState + filterLast)));

			String stateSelected = driver.findElement(By.xpath(stateParent + strFilterState + filterLast)).getText();

			String strSelectedFilter = getElementText(By.xpath(stateParent + strFilterState + filterLast));
			String[] strResult = strSelectedFilter.split(" ");
			strSelectedFilter = strResult[strResult.length - 1];
			strSelectedFilter = strSelectedFilter.replace(")", "").trim();
			strSelectedFilter = strSelectedFilter.replace("(", "").trim();

			jobValdandBack(strSelectedFilter);
			List<WebElement> stateList2 = driver.findElements(allStateFilters);
			if (stateList2.size() > 1) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.FAIL,
						"More than One state displayed");

			} else if (stateList2.size() == 1 && stateList2.get(0).getText().equals(stateSelected)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.PASS,
						stateSelected + " is selected");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.FAIL,
					"Entered State Not Present");
		}

		clearAllFilter();

	}

	public void stateCityVal() {

		String strFilterState = testData.get("strState2");
		String strFilterCity = testData.get("strCity1");

		singleStateVal(strFilterState);

		if (verifyObjectDisplayed(By.xpath(cityParent + strFilterCity + filterLast))) {

			scrollDownForElementJSBy(By.xpath(cityParent + strFilterCity + filterLast));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.findElement(By.xpath(cityParent + strFilterCity + filterLast)).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCityVal", Status.PASS,
					getElementText(By.xpath(cityParent + strFilterCity + filterLast)) + " is present in city");

			filterResultsVerification((By.xpath(cityParent + strFilterCity + filterLast)));

			String strSelectedFilter = getElementText(By.xpath(cityParent + strFilterCity + filterLast));
			String[] strResult = strSelectedFilter.split(" ");
			strSelectedFilter = strResult[strResult.length - 1];
			strSelectedFilter = strSelectedFilter.replace(")", "").trim();
			strSelectedFilter = strSelectedFilter.replace("(", "").trim();

			jobValdandBack(strSelectedFilter);
			List<WebElement> cityList2 = driver.findElements(allStateFilters);
			if (cityList2.size() > 1) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.FAIL,
						"More than One city displayed");

			} else if (cityList2.size() == 1 && cityList2.get(0).getText().equals(strSelectedFilter)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.PASS,
						strSelectedFilter + " is selected");
			}

		} else {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "stateCityVal", Status.FAIL,
					"Searched City Filter not present");

		}

		clearAllFilter();

	}

	public void avonStateVal() {

		String strFilterCity = testData.get("strCity3");

		singleCityVal(strFilterCity);

		List<WebElement> stateList = driver.findElements(allStateFilters);

		if (stateList.size() == 2) {

			for (int i = 0; i < stateList.size(); i++) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "avonStateVal", Status.PASS,
						(i + 1) + " Staete is " + stateList.get(i).getText());

			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "avonStateVal", Status.FAIL,
					"State List size is not equal to 2 for Avon");
		}

		String strFilterBrand = testData.get("strBrand3");

		singleBrandVal(strFilterBrand);

		waitUntilElementVisibleBy(driver, searchResultsTitle, 120);

		/*
		 * scrollDownForElementJSBy(allStateFilters);
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); scrollUpByPixel(150);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 */

		scrollDownByPixel(200);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		List<WebElement> stateList2 = driver.findElements(allStateFilters);

		if (stateList2.size() > 1) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "avonStateVal", Status.FAIL,
					"More than One state displayed");

		} else if (stateList2.size() == 1) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "avonStateVal", Status.PASS,
					stateList2.get(0).getText() + " is selected");

			String strSelectedFilter = getElementText(By.xpath(stateParent + stateList2.get(0).getText() + filterLast));
			String[] strResult = strSelectedFilter.split(" ");
			strSelectedFilter = strResult[strResult.length - 1];
			strSelectedFilter = strSelectedFilter.replace(")", "").trim();
			strSelectedFilter = strSelectedFilter.replace("(", "").trim();

			jobValdandBack(strSelectedFilter);
			List<WebElement> stateList3 = driver.findElements(allStateFilters);
			if (stateList3.size() > 1) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.FAIL,
						"More than One state displayed");

			} else if (stateList3.size() == 1 && stateList3.get(0).getText().equals(strSelectedFilter)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryStateVal", Status.PASS,
						stateList2.get(0).getText() + " is selected");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "avonStateVal", Status.FAIL, "No State Selected");
		}

		clearAllFilter();
	}

	public void singleCityVal(String strFilterCity) {
		waitUntilElementVisibleBy(driver, cityFilterTab, 120);
		if (verifyObjectDisplayed(cityFilterTab)) {

			clickElementBy(cityFilterTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(By.xpath(cityParent + strFilterCity + filterLast))) {

				scrollDownForElementJSBy(By.xpath(cityParent + strFilterCity + filterLast));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(By.xpath(cityParent + strFilterCity + filterLast)).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCityVal", Status.PASS,
						getElementText(By.xpath(cityParent + strFilterCity + filterLast)) + " is present in city");

				cityFilterResultsVerification((selectedState));

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCityVal", Status.FAIL,
						"Searched City Filter not present");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCityVal", Status.FAIL,
					"City filter tab not present in the page");
		}
	}

	public void singleStateVal(String strFilterState) {
		waitUntilElementVisibleBy(driver, stateFilterTab, 120);
		if (verifyObjectDisplayed(stateFilterTab)) {

			clickElementBy(stateFilterTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(By.xpath(stateParent + strFilterState + filterLast))) {

				scrollDownForElementJSBy(By.xpath(stateParent + strFilterState + filterLast));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(By.xpath(stateParent + strFilterState + filterLast)).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleStateVal", Status.PASS,
						getElementText(By.xpath(stateParent + strFilterState + filterLast)) + " is present in state");

				filterResultsVerification((By.xpath(stateParent + strFilterState + filterLast)));

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleStateVal", Status.FAIL,
						"Entered State Not Present");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleStateVal", Status.FAIL,
					"State filter tab not present in the page");
		}

	}

	public void singleBrandVal(String strFilterBrand) {
		waitUntilElementVisibleBy(driver, brandFilterTab, 120);
		if (verifyObjectDisplayed(brandFilterTab)) {

			clickElementBy(brandFilterTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(By.xpath(brandParent + strFilterBrand + filterLast))) {

				scrollDownForElementJSBy(By.xpath(brandParent + strFilterBrand + filterLast));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(By.xpath(brandParent + strFilterBrand + filterLast)).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleBrandVal", Status.PASS,
						getElementText(By.xpath(brandParent + strFilterBrand + filterLast)) + " is present in brand");

				filterResultsVerification((By.xpath(brandParent + strFilterBrand + filterLast)));

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleBrandVal", Status.FAIL,
						"Searched Brand not present");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleBrandVal", Status.FAIL,
					"Brand filter tab not present in the page");
		}

	}

	public void singleCountryVal(String strFilterCountry) {

		waitUntilElementVisibleBy(driver, countryFilterTab, 120);
		if (verifyObjectDisplayed(countryFilterTab)) {

			clickElementBy(countryFilterTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(By.xpath(countryParent + strFilterCountry + filterLast))) {
				scrollDownForElementJSBy(By.xpath(countryParent + strFilterCountry + filterLast));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(By.xpath(countryParent + strFilterCountry + filterLast)).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCountryVal", Status.PASS,
						getElementText(By.xpath(countryParent + strFilterCountry + filterLast))
								+ " is present in country");

				filterResultsVerification((By.xpath(countryParent + strFilterCountry + filterLast)));

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCountryVal", Status.FAIL,
						"Entered Country Not Present");

			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "singleCountryVal", Status.FAIL,
					"Country filter tab not present in the page");
		}

	}

	public void allFilterNumbersCheckForCountry() {

		waitUntilElementVisibleBy(driver, countryFilterTab, 120);

		if (verifyObjectDisplayed(countryFilterTab)) {

			clickElementBy(countryFilterTab);

			List<WebElement> countryList = driver.findElements(allCountryFilters);

			if (countryList.size() > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterNumbersCheckForCountry", Status.PASS,
						"Country Filter present, Total: " + countryList.size());

				for (int i = 0; i < countryList.size(); i++) {

					if (i > 0) {
						clickElementBy(countryFilterTab);
					}

					List<WebElement> countryList2 = driver.findElements(allCountryFilters);

					scrollDownForElementJSWb(countryList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementWb(countryList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String strSelectedFilter = countryList2.get(i).getText();

					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strTotalResult = driver.findElement(searchResultsTitle).getText();
					String[] strResult1 = strTotalResult.split("of");
					strTotalResult = strResult1[strResult1.length - 1];

					tcConfig.updateTestReporter("BrowseAllJobsPage", "filterNumbersCheckForCountry", Status.PASS,
							"Clicked on " + countryList2.get(i).getText() + " with total jobs " + strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					int totalJob = Integer.parseInt(strSelectedFilter);

					jobFunctionNumbersCheck(totalJob);
					stateFilterNoCheck(totalJob);
					cityFilterNoCheck(totalJob);
					brandFilterNoCheck(totalJob);
					scheduleFilterNoCheck(totalJob);
					jobLevelFilterNoCheck(totalJob);

					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);

						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (countryList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterNumbersCheckForCountry", Status.FAIL,
						"No Country Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterNumbersCheckForCountry", Status.FAIL,
						"Error in getting country filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "filterNumbersCheckForCountry", Status.FAIL,
					"Country Filter Tab not present");
		}

	}

	public void jobFunctionNumbersCheck(int intResult) {
		waitUntilElementVisibleBy(driver, jobFunctionFilterTab, 120);

		scrollDownForElementJSBy(jobFunctionFilterTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(jobFunctionFilterTab)) {

			clickElementBy(jobFunctionFilterTab);

			List<WebElement> jobFunctionList = driver.findElements(allJobFunctionFilters);

			if (jobFunctionList.size() > 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFunctionNumbersCheck", Status.PASS,
						"Job Function Filter present, Total: " + jobFunctionList.size());
				int totalJob = 0;
				List<Integer> addList = new ArrayList<>();
				for (int i = 0; i < jobFunctionList.size(); i++) {

					String strSelectedFilter = jobFunctionList.get(i).getText();

					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					totalJob = Integer.parseInt(strSelectedFilter);

					addList.add(totalJob);
				}

				int totalofAll = 0;
				for (int j : addList) {
					totalofAll += j;

				}

				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFunctionNumbersCheck", Status.PASS,
						"Jub Function All Filter Total: " + totalofAll);

				if (totalofAll == intResult) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFunctionNumbersCheck", Status.PASS,
							"All Job Function Filter numbers matches with the main filter applied that is: "
									+ totalofAll);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFunctionNumbersCheck", Status.FAIL,
							"All Job Function Filter does not match with the main filter applied");
				}

			} else if (jobFunctionList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFunctionNumbersCheck", Status.FAIL,
						"Job Functiony Filter not Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobFunctionNumbersCheck", Status.FAIL,
						"Error in getting job function filter");
			}

		}

	}

	public void stateFilterNoCheck(int intResult) {

		waitUntilElementVisibleBy(driver, stateFilterTab, 120);

		scrollDownForElementJSBy(stateFilterTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(stateFilterTab)) {

			// clickElementBy(stateFilterTab);

			List<WebElement> stateList = driver.findElements(allStateFilters);

			if (stateList.size() > 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "stateNumbersCheck", Status.PASS,
						"State Filter present, Total: " + stateList.size());
				int totalJob = 0;
				List<Integer> addList = new ArrayList<>();
				for (int i = 0; i < stateList.size(); i++) {

					String strSelectedFilter = stateList.get(i).getText();

					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					totalJob = Integer.parseInt(strSelectedFilter);

					addList.add(totalJob);
				}

				int totalofAll = 0;
				for (int j : addList) {
					totalofAll += j;

				}

				tcConfig.updateTestReporter("BrowseAllJobsPage", "stateNumbersCheck", Status.PASS,
						"State All Filter Total: " + totalofAll);

				if (totalofAll == intResult) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "stateNumbersCheck", Status.PASS,
							"All State Filter numbers matches with the main filter applied that is: " + totalofAll);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "stateNumbersCheck", Status.FAIL,
							"All State Filter does not match with the main filter applied");
				}

			} else if (stateList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "stateNumbersCheck", Status.FAIL,
						"State Filter not Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "stateNumbersCheck", Status.FAIL,
						"Error in getting State filter");
			}

		}

	}

	public void cityFilterNoCheck(int intResult) {

		waitUntilElementVisibleBy(driver, cityFilterTab, 120);

		scrollDownForElementJSBy(cityFilterTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(cityFilterTab)) {

			// clickElementBy(cityFilterTab);

			List<WebElement> cityList = driver.findElements(allCityFilters);

			if (cityList.size() > 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "cityNumbersCheck", Status.PASS,
						"City Filter present, Total: " + cityList.size());
				int totalJob = 0;
				List<Integer> addList = new ArrayList<>();
				for (int i = 0; i < cityList.size(); i++) {

					String strSelectedFilter = cityList.get(i).getText();

					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					totalJob = Integer.parseInt(strSelectedFilter);

					addList.add(totalJob);
				}

				int totalofAll = 0;
				for (int j : addList) {
					totalofAll += j;
					totalofAll = totalofAll + j;

				}

				tcConfig.updateTestReporter("BrowseAllJobsPage", "cityNumbersCheck", Status.PASS,
						"City All Filter Total: " + totalofAll);

				if (totalofAll == intResult) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "cityNumbersCheck", Status.PASS,
							"All City Filter numbers matches with the main filter applied that is: " + totalofAll);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "cityNumbersCheck", Status.PASS,
							"All City Filter does not match with the main filter applied that is: " + totalofAll);
				}

			} else if (cityList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "cityNumbersCheck", Status.FAIL,
						"City Filter not Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "cityNumbersCheck", Status.FAIL,
						"Error in getting City filter");
			}

		}

	}

	public void brandFilterNoCheck(int intResult) {

		waitUntilElementVisibleBy(driver, brandFilterTab, 120);

		scrollDownForElementJSBy(brandFilterTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(brandFilterTab)) {

			clickElementBy(brandFilterTab);

			List<WebElement> brandList = driver.findElements(allBrandFilters);

			if (brandList.size() > 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brandNumbersCheck", Status.PASS,
						"Brand Filter present, Total: " + brandList.size());
				int totalJob = 0;
				List<Integer> addList = new ArrayList<>();
				for (int i = 0; i < brandList.size(); i++) {

					String strSelectedFilter = brandList.get(i).getText();

					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					totalJob = Integer.parseInt(strSelectedFilter);

					addList.add(totalJob);
				}

				int totalofAll = 0;
				for (int j : addList) {
					totalofAll += j;

				}

				tcConfig.updateTestReporter("BrowseAllJobsPage", "brandNumbersCheck", Status.PASS,
						"Brand All Filter Total: " + totalofAll);

				if (totalofAll == intResult) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "brandNumbersCheck", Status.PASS,
							"All Brand Filter numbers matches with the main filter applied that is: " + totalofAll);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "brandNumbersCheck", Status.FAIL,
							"All Brand Filter does not match with the main filter applied");
				}

			} else if (brandList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brandNumbersCheck", Status.FAIL,
						"Brand Filter not Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brandNumbersCheck", Status.FAIL,
						"Error in getting Brand filter");
			}

		}

	}

	public void scheduleFilterNoCheck(int intResult) {

		waitUntilElementVisibleBy(driver, scheduleFilterTab, 120);

		scrollDownForElementJSBy(scheduleFilterTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(scheduleFilterTab)) {

			clickElementBy(scheduleFilterTab);

			List<WebElement> scheduleList = driver.findElements(allScheduleFilters);

			if (scheduleList.size() > 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleNumbersCheck", Status.PASS,
						"Schedule Filter present, Total: " + scheduleList.size());
				int totalJob = 0;
				List<Integer> addList = new ArrayList<>();
				for (int i = 0; i < scheduleList.size(); i++) {

					String strSelectedFilter = scheduleList.get(i).getText();

					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					totalJob = Integer.parseInt(strSelectedFilter);

					addList.add(totalJob);
				}

				int totalofAll = 0;
				for (int j : addList) {
					totalofAll += j;

				}

				tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleNumbersCheck", Status.PASS,
						"Schedule All Filter Total: " + totalofAll);

				if (totalofAll == intResult) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleNumbersCheck", Status.PASS,
							"All Schedule Filter numbers matches with the main filter applied that is: " + totalofAll);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleNumbersCheck", Status.FAIL,
							"All Schedule Filter does not match with the main filter applied");
				}

			} else if (scheduleList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleNumbersCheck", Status.FAIL,
						"Schedule Filter not Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleNumbersCheck", Status.FAIL,
						"Error in getting Schedule filter");
			}

		}

	}

	public void jobLevelFilterNoCheck(int intResult) {

		waitUntilElementVisibleBy(driver, jobLevelFilterTab, 120);

		scrollDownForElementJSBy(jobLevelFilterTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(jobLevelFilterTab)) {

			clickElementBy(jobLevelFilterTab);

			List<WebElement> jobLevelList = driver.findElements(allJobLevelFilters);

			if (jobLevelList.size() > 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelNumbersCheck", Status.PASS,
						"Job Level Filter present, Total: " + jobLevelList.size());
				int totalJob = 0;
				List<Integer> addList = new ArrayList<>();
				for (int i = 0; i < jobLevelList.size(); i++) {

					String strSelectedFilter = jobLevelList.get(i).getText();

					String[] strResult = strSelectedFilter.split(" ");

					strSelectedFilter = strResult[strResult.length - 1];
					strSelectedFilter = strSelectedFilter.replace(")", "").trim();
					strSelectedFilter = strSelectedFilter.replace("(", "").trim();

					totalJob = Integer.parseInt(strSelectedFilter);

					addList.add(totalJob);
				}

				int totalofAll = 0;
				for (int j : addList) {
					totalofAll += j;

				}

				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelNumbersCheck", Status.PASS,
						"Job Level All Filter Total: " + totalofAll);

				if (totalofAll == intResult) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelNumbersCheck", Status.PASS,
							"All Job Level Filter numbers matches with the main filter applied that is: " + totalofAll);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelNumbersCheck", Status.FAIL,
							"All Job Level Filter does not match with the main filter applied");
				}

			} else if (jobLevelList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelNumbersCheck", Status.FAIL,
						"Job Level Filter not Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelNumbersCheck", Status.FAIL,
						"Error in getting Job Level filter");
			}

		}

	}

	public void paginationResultCountCheck(String resultCount) {
		if (verifyObjectDisplayed(searchResultsTitle)) {

			// driver.switchTo().activeElement().sendKeys(Keys.HOME);

			int totalResult = Integer.parseInt(resultCount);

			tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.PASS,
					"Total Results for applied Filter: " + totalResult);

			List<WebElement> topPagList = driver.findElements(topPaginations);

			if (topPagList.size() > 1) {
				int jobsCountPerPage = 0;
				for (int i = 0; i < topPagList.size(); i++) {

					if (i > 0) {
						topPagList.get(i).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

					String strTotalResult = driver.findElement(searchResultsTitle).getText();
					String[] strResult1 = strTotalResult.split("of");
					strTotalResult = strResult1[strResult1.length - 1];
					strTotalResult = strTotalResult.replaceAll("results", "");
					strTotalResult = strTotalResult.trim();

					int headResult = Integer.parseInt(strTotalResult);

					if (totalResult == headResult) {

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.PASS,
								"The Filter is intact on " + (i + 1) + " page");
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.FAIL,
								"The Filter was not intact on " + (i + 1) + " page");
					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> jobList = driver.findElements(resultsName);

					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.PASS,
							"Total Count of Jobs in " + (i + 1) + " page is: " + jobList.size());

					int tempCount = 0;
					tempCount = jobList.size();

					jobsCountPerPage += tempCount;

				}

				if (totalResult == jobsCountPerPage) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.PASS,
							"The total of jobs in all the pages equals the total job in the filter that is: "
									+ jobsCountPerPage);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.FAIL,
							"The total of jobs in all the pages " + jobsCountPerPage
									+ " does not equals the total job in the filter that is: " + totalResult);
				}

			} else if (topPagList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.FAIL,
						"No Pagination Available");
			} else if (topPagList.size() == 1) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.PASS,
						"Only Single Page available");

				String strTotalResult = driver.findElement(searchResultsTitle).getText();
				String[] strResult1 = strTotalResult.split("of");
				strTotalResult = strResult1[strResult1.length - 1];
				if (strTotalResult.contains("results")) {
					strTotalResult = strTotalResult.replaceAll("results", "");
				} else {
					strTotalResult = strTotalResult.replaceAll("result", "");
				}
				strTotalResult = strTotalResult.trim();

				int headResult = Integer.parseInt(strTotalResult);

				if (totalResult == headResult) {

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.PASS,
							"The Filter is intact on the page ");
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.FAIL,
							"The Filter was not intact on the page");
				}

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.FAIL,
						"Pagination Error");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationResultCountCheck", Status.FAIL,
					"Search Results not displayed");
		}
	}

	public void countryFilterValidations() {

		waitUntilElementVisibleBy(driver, countryFilterTab, 120);

		if (verifyObjectDisplayed(countryFilterTab)) {

			clickElementBy(countryFilterTab);

			List<WebElement> countryList = driver.findElements(allCountryFilters);
			int intTotalCountries = countryList.size();

			if (intTotalCountries > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryFilterValidations", Status.PASS,
						"Country Filter present, Total: " + intTotalCountries);

				String startValdnFrom = testData.get("ValdnStartFrom");
				int startingFrom = Integer.parseInt(startValdnFrom);

				startingFrom = startingFrom - 1;

				String endValdnAt = testData.get("ValdnEndAt");
				int endingAt;
				if (endValdnAt.trim().equalsIgnoreCase("END")) {

					endingAt = intTotalCountries;

				} else {
					endingAt = Integer.parseInt(endValdnAt);

					if (endingAt >= intTotalCountries) {
						endingAt = intTotalCountries;
					} else {
						// endingAt=endingAt-1;
					}
				}

				for (int i = startingFrom; i < endingAt; i++) {

					if (i > startingFrom) {
						clickElementBy(countryFilterTab);
					}

					List<WebElement> countryList2 = driver.findElements(allCountryFilters);

					scrollDownForElementJSWb(countryList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String slectedFilterName = countryList2.get(i).getText();

					clickElementWb(countryList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strSelectedFilter = null;
					String[] strResult = null;
					try {
						strSelectedFilter = countryList2.get(i).getText();

						strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();
					} catch (Exception e) {

					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					try {
						waitUntilElementVisibleBy(driver, searchResultsTitle, 10);
					} catch (Exception e) {

					}
					String strTotalResult = null;
					if (verifyObjectDisplayed(searchResultsTitle)) {
						strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
								"No Results for the filter " + slectedFilterName);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyObjectDisplayed(clearAllFilters)) {
							scrollDownForElementJSBy(clearAllFilters);
							scrollUpByPixel(150);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							clickElementBy(clearAllFilters);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("Clear All Filter option not present");
						}

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						continue;
					}

					tcConfig.updateTestReporter("BrowseAllJobsPage", "countryFilterValidations", Status.PASS,
							"Clicked on " + countryList2.get(i).getText() + " with total jobs " + strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "countryFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "countryFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					// int totalJob = Integer.parseInt(strSelectedFilter);

					paginationCheck(strSelectedFilter);

					jobDescriptionForFilters(strSelectedFilter);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);

						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (countryList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryFilterValidations", Status.FAIL,
						"No Country Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "countryFilterValidations", Status.FAIL,
						"Error in getting country filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "countryFilterValidations", Status.FAIL,
					"Country Filter Tab not present");
		}

	}

	public void stateFilterValidations() {

		waitUntilElementVisibleBy(driver, stateFilterTab, 120);

		if (verifyObjectDisplayed(stateFilterTab)) {

			clickElementBy(stateFilterTab);

			List<WebElement> stateList = driver.findElements(allStateFilters);
			int intTotalStates = stateList.size();

			if (intTotalStates > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "stateFilterValidations", Status.PASS,
						"State Filter present, Total: " + intTotalStates);
				String startValdnFrom = testData.get("ValdnStartFrom");
				int startingFrom = Integer.parseInt(startValdnFrom);

				startingFrom = startingFrom - 1;

				String endValdnAt = testData.get("ValdnEndAt");
				int endingAt;
				if (endValdnAt.trim().equalsIgnoreCase("END")) {

					endingAt = intTotalStates;

				} else {
					endingAt = Integer.parseInt(endValdnAt);

					if (endingAt >= intTotalStates) {
						endingAt = intTotalStates;
					} else {
						// endingAt=endingAt-1;
					}
				}

				for (int i = startingFrom; i < endingAt; i++) {

					if (i > startingFrom) {
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(stateFilterTab);
					}

					List<WebElement> stateList2 = driver.findElements(allStateFilters);

					scrollDownForElementJSWb(stateList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// String selectedFilterName= stateList2.get(i).getText();

					clickElementWb(stateList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String strSelectedFilter = null;
					String[] strResult = null;
					try {
						strSelectedFilter = stateList2.get(i).getText();

						strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();
					} catch (Exception e) {

					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					try {
						waitUntilElementVisibleBy(driver, searchResultsTitle, 10);
					} catch (Exception e) {

					}
					String strTotalResult = null;
					if (verifyObjectDisplayed(searchResultsTitle)) {
						strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
								"No Results for the filter");

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyObjectDisplayed(clearAllFilters)) {
							scrollDownForElementJSBy(clearAllFilters);
							scrollUpByPixel(150);

							clickElementBy(clearAllFilters);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("Clear All Filter option not present");
						}

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						continue;
					}

					tcConfig.updateTestReporter("BrowseAllJobsPage", "stateFilterValidations", Status.PASS,
							"Clicked on " + stateList2.get(i).getText() + " with total jobs " + strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "stateFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "stateFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					// int totalJob = Integer.parseInt(strSelectedFilter);

					paginationCheck(strSelectedFilter);

					jobDescriptionForFilters(strSelectedFilter);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (stateList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "stateFilterValidations", Status.FAIL,
						"No State Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "stateFilterValidations", Status.FAIL,
						"Error in getting state filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "stateFilterValidations", Status.FAIL,
					"State Filter Tab not present");
		}

	}

	public void cityFilterValidations() {

		waitUntilElementVisibleBy(driver, cityFilterTab, 120);

		if (verifyObjectDisplayed(cityFilterTab)) {

			clickElementBy(cityFilterTab);

			List<WebElement> cityList = driver.findElements(allCityFilters);
			int intTotalCity = cityList.size();

			if (intTotalCity > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.PASS,
						"City Filter present, Total: " + intTotalCity);

				String startValdnFrom = testData.get("ValdnStartFrom");
				int startingFrom = Integer.parseInt(startValdnFrom);

				startingFrom = startingFrom - 1;

				String endValdnAt = testData.get("ValdnEndAt");
				int endingAt;
				if (endValdnAt.trim().equalsIgnoreCase("END")) {

					endingAt = intTotalCity;

				} else {
					endingAt = Integer.parseInt(endValdnAt);

					if (endingAt >= intTotalCity) {
						endingAt = intTotalCity;
					} else {
						// endingAt=endingAt-1;
					}
				}

				for (int i = startingFrom; i < endingAt; i++) {

					if (i > startingFrom) {
						clickElementBy(cityFilterTab);
					}

					List<WebElement> cityList2 = driver.findElements(allCityFilters);

					scrollDownForElementJSWb(cityList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementWb(cityList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String strSelectedFilter = null;
					String[] strResult = null;
					try {
						strSelectedFilter = cityList2.get(i).getText();

						strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();
					} catch (Exception e) {

					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					try {
						waitUntilElementVisibleBy(driver, searchResultsTitle, 10);
					} catch (Exception e) {

					}
					String strTotalResult = null;
					if (verifyObjectDisplayed(searchResultsTitle)) {
						strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
								"No Results for: " + cityList2.get(i).getText());

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyObjectDisplayed(clearAllFilters)) {
							scrollDownForElementJSBy(clearAllFilters);
							scrollUpByPixel(150);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							clickElementBy(clearAllFilters);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("Clear All Filter option not present");
						}

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						continue;
					}

					tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.PASS,
							"Clicked on " + cityList2.get(i).getText() + " with total jobs " + strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					// int totalJob = Integer.parseInt(strSelectedFilter);

					paginationCheck(strSelectedFilter);

					jobDescriptionForFilters(strSelectedFilter);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);

						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (cityList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
						"No City Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
						"Error in getting city filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
					"City Filter Tab not present");
		}

	}

	public void jobFunctionFilterValidations() {

		waitUntilElementVisibleBy(driver, jobFunctionFilterTab, 120);

		if (verifyObjectDisplayed(jobFunctionFilterTab)) {

			clickElementBy(jobFunctionFilterTab);

			List<WebElement> JobFunctionList = driver.findElements(allJobFunctionFilters);
			int intTotalJobFunction = JobFunctionList.size();

			if (intTotalJobFunction > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.PASS,
						"Job Function Filter present, Total: " + intTotalJobFunction);

				String startValdnFrom = testData.get("ValdnStartFrom");
				int startingFrom = Integer.parseInt(startValdnFrom);

				startingFrom = startingFrom - 1;

				String endValdnAt = testData.get("ValdnEndAt");
				int endingAt;
				if (endValdnAt.trim().equalsIgnoreCase("END")) {

					endingAt = intTotalJobFunction;

				} else {
					endingAt = Integer.parseInt(endValdnAt);

					if (endingAt >= intTotalJobFunction) {
						endingAt = intTotalJobFunction;
					} else {

					}
				}

				for (int i = startingFrom; i < endingAt; i++) {

					if (i > startingFrom) {
						clickElementBy(jobFunctionFilterTab);
					}

					List<WebElement> JobFunctionList2 = driver.findElements(allJobFunctionFilters);

					scrollDownForElementJSWb(JobFunctionList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String selectedFilterName = JobFunctionList2.get(i).getText();

					clickElementWb(JobFunctionList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String strSelectedFilter = null;
					String[] strResult = null;
					try {
						strSelectedFilter = JobFunctionList2.get(i).getText();

						strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();
					} catch (Exception e) {

					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					try {
						waitUntilElementVisibleBy(driver, searchResultsTitle, 10);
					} catch (Exception e) {

					}
					String strTotalResult = null;
					if (verifyObjectDisplayed(searchResultsTitle)) {
						strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.FAIL,
								"No Results for: " + selectedFilterName);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyObjectDisplayed(clearAllFilters)) {
							scrollDownForElementJSBy(clearAllFilters);
							scrollUpByPixel(150);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							clickElementBy(clearAllFilters);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("Clear All Filter option not present");
						}

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						continue;
					}

					tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.PASS,
							"Clicked on " + JobFunctionList2.get(i).getText() + " with total jobs "
									+ strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					// int totalJob = Integer.parseInt(strSelectedFilter);

					paginationCheck(strSelectedFilter);

					jobDescriptionForFilters(strSelectedFilter);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);

						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (JobFunctionList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.FAIL,
						"No Job Function Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.FAIL,
						"Error in getting JobFunction filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "JobFunctionFilterValidations", Status.FAIL,
					"Job Function Filter Tab not present");
		}

	}

	public void brandFilterValidations() {

		waitUntilElementVisibleBy(driver, brandFilterTab, 120);

		if (verifyObjectDisplayed(brandFilterTab)) {

			clickElementBy(brandFilterTab);

			List<WebElement> brandList = driver.findElements(allBrandFilters);
			int intTotalBrand = brandList.size();

			if (intTotalBrand > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.PASS,
						"Brand Filter present, Total: " + intTotalBrand);

				String startValdnFrom = testData.get("ValdnStartFrom");
				int startingFrom = Integer.parseInt(startValdnFrom);

				startingFrom = startingFrom - 1;

				String endValdnAt = testData.get("ValdnEndAt");
				int endingAt;
				if (endValdnAt.trim().equalsIgnoreCase("END")) {

					endingAt = intTotalBrand;

				} else {
					endingAt = Integer.parseInt(endValdnAt);

					if (endingAt >= intTotalBrand) {
						endingAt = intTotalBrand;
					} else {
						// endingAt=endingAt-1;
					}
				}

				for (int i = startingFrom; i < endingAt; i++) {

					if (i > startingFrom) {
						clickElementBy(brandFilterTab);
					}

					List<WebElement> brandList2 = driver.findElements(allBrandFilters);

					scrollDownForElementJSWb(brandList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String slectedFilterName = brandList2.get(i).getText();

					clickElementWb(brandList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String strSelectedFilter = null;
					String[] strResult = null;
					try {
						strSelectedFilter = brandList2.get(i).getText();

						strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();
					} catch (Exception e) {

					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					try {
						waitUntilElementVisibleBy(driver, searchResultsTitle, 10);
					} catch (Exception e) {

					}
					String strTotalResult = null;
					if (verifyObjectDisplayed(searchResultsTitle)) {
						strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.FAIL,
								"No Results for: " + slectedFilterName);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyObjectDisplayed(clearAllFilters)) {
							scrollDownForElementJSBy(clearAllFilters);
							scrollDownByPixel(150);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							clickElementBy(clearAllFilters);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("Clear All Filter option not present");
						}

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						continue;
					}

					tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.PASS,
							"Clicked on " + brandList2.get(i).getText() + " with total jobs " + strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					// int totalJob = Integer.parseInt(strSelectedFilter);

					paginationCheck(strSelectedFilter);

					jobDescriptionForFilters(strSelectedFilter);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);

						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (brandList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.FAIL,
						"No Brand Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.FAIL,
						"Error in getting brand filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "brandFilterValidations", Status.FAIL,
					"Brand Filter Tab not present");
		}

	}

	public void scheduleFilterValidations() {

		waitUntilElementVisibleBy(driver, scheduleFilterTab, 120);

		if (verifyObjectDisplayed(scheduleFilterTab)) {

			clickElementBy(scheduleFilterTab);

			List<WebElement> scheduleList = driver.findElements(allScheduleFilters);
			int intTotalCountries = scheduleList.size();

			if (intTotalCountries > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleFilterValidations", Status.PASS,
						"Schedule Filter present, Total: " + intTotalCountries);

				String startValdnFrom = testData.get("ValdnStartFrom");
				int startingFrom = Integer.parseInt(startValdnFrom);

				startingFrom = startingFrom - 1;

				String endValdnAt = testData.get("ValdnEndAt");
				int endingAt;
				if (endValdnAt.trim().equalsIgnoreCase("END")) {

					endingAt = intTotalCountries;

				} else {
					endingAt = Integer.parseInt(endValdnAt);

					if (endingAt >= intTotalCountries) {
						endingAt = intTotalCountries;
					} else {
						// endingAt=endingAt-1;
					}
				}

				for (int i = startingFrom; i < endingAt; i++) {

					if (i > startingFrom) {
						clickElementBy(scheduleFilterTab);
					}

					List<WebElement> scheduleList2 = driver.findElements(allScheduleFilters);

					scrollDownForElementJSWb(scheduleList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String slectedFilterName = scheduleList2.get(i).getText();

					clickElementWb(scheduleList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strSelectedFilter = null;
					String[] strResult = null;
					try {
						strSelectedFilter = scheduleList2.get(i).getText();

						strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();
					} catch (Exception e) {

					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					try {
						waitUntilElementVisibleBy(driver, searchResultsTitle, 10);
					} catch (Exception e) {

					}
					String strTotalResult = null;
					if (verifyObjectDisplayed(searchResultsTitle)) {
						strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
								"No Results for the filter " + slectedFilterName);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyObjectDisplayed(clearAllFilters)) {
							scrollDownForElementJSBy(clearAllFilters);
							scrollUpByPixel(150);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							clickElementBy(clearAllFilters);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("Clear All Filter option not present");
						}

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						continue;
					}

					tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleFilterValidations", Status.PASS,
							"Clicked on " + scheduleList2.get(i).getText() + " with total jobs " + strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					// int totalJob = Integer.parseInt(strSelectedFilter);

					paginationCheck(strSelectedFilter);

					jobDescriptionForFilters(strSelectedFilter);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);

						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (scheduleList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleFilterValidations", Status.FAIL,
						"No Schedule Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleFilterValidations", Status.FAIL,
						"Error in getting schedule filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "scheduleFilterValidations", Status.FAIL,
					"Schedule Filter Tab not present");
		}

	}

	public void jobLevelFilterValidations() {

		waitUntilElementVisibleBy(driver, jobLevelFilterTab, 120);

		if (verifyObjectDisplayed(jobLevelFilterTab)) {

			clickElementBy(jobLevelFilterTab);

			List<WebElement> jobLevelList = driver.findElements(allJobLevelFilters);
			int intTotalCountries = jobLevelList.size();

			if (intTotalCountries > 0) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelFilterValidations", Status.PASS,
						"Job Level Filter present, Total: " + intTotalCountries);

				String startValdnFrom = testData.get("ValdnStartFrom");
				int startingFrom = Integer.parseInt(startValdnFrom);

				startingFrom = startingFrom - 1;

				String endValdnAt = testData.get("ValdnEndAt");
				int endingAt;
				if (endValdnAt.trim().equalsIgnoreCase("END")) {

					endingAt = intTotalCountries;

				} else {
					endingAt = Integer.parseInt(endValdnAt);

					if (endingAt >= intTotalCountries) {
						endingAt = intTotalCountries;
					} else {
						// endingAt=endingAt-1;
					}
				}

				for (int i = startingFrom; i < endingAt; i++) {

					if (i > startingFrom) {
						clickElementBy(jobLevelFilterTab);
					}

					List<WebElement> jobLevelList2 = driver.findElements(allJobLevelFilters);

					scrollDownForElementJSWb(jobLevelList2.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String slectedFilterName = jobLevelList2.get(i).getText();

					clickElementWb(jobLevelList2.get(i));

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strSelectedFilter = null;
					String[] strResult = null;
					try {
						strSelectedFilter = jobLevelList2.get(i).getText();

						strResult = strSelectedFilter.split(" ");

						strSelectedFilter = strResult[strResult.length - 1];
						strSelectedFilter = strSelectedFilter.replace(")", "").trim();
						strSelectedFilter = strSelectedFilter.replace("(", "").trim();
					} catch (Exception e) {

					}

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					try {
						waitUntilElementVisibleBy(driver, searchResultsTitle, 10);
					} catch (Exception e) {

					}
					String strTotalResult = null;
					if (verifyObjectDisplayed(searchResultsTitle)) {
						strTotalResult = driver.findElement(searchResultsTitle).getText();
						String[] strResult1 = strTotalResult.split("of");
						strTotalResult = strResult1[strResult1.length - 1];

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "cityFilterValidations", Status.FAIL,
								"No Results for the filter " + slectedFilterName);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						if (verifyObjectDisplayed(clearAllFilters)) {
							scrollDownForElementJSBy(clearAllFilters);
							scrollUpByPixel(150);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							clickElementBy(clearAllFilters);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("Clear All Filter option not present");
						}

						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						continue;
					}

					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelFilterValidations", Status.PASS,
							"Clicked on " + jobLevelList2.get(i).getText() + " with total jobs " + strSelectedFilter);

					if (strTotalResult.contains(strSelectedFilter)) {
						driver.switchTo().activeElement().sendKeys(Keys.HOME);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelFilterValidations", Status.PASS,
								"The filter Result and Search result is equal that is: " + strTotalResult);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelFilterValidations", Status.FAIL,
								"The filter number and the result displayed did not match");
					}

					// int totalJob = Integer.parseInt(strSelectedFilter);

					paginationCheck(strSelectedFilter);

					jobDescriptionForFilters(strSelectedFilter);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(clearAllFilters)) {
						scrollDownForElementJSBy(clearAllFilters);
						scrollUpByPixel(150);

						clickElementBy(clearAllFilters);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						System.out.println("Clear All Filter option not present");
					}

					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else if (jobLevelList.size() == 0) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelFilterValidations", Status.FAIL,
						"No Job Level Filter Present");
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelFilterValidations", Status.FAIL,
						"Error in getting jobLevel filter");
			}

		} else {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobLevelFilterValidations", Status.FAIL,
					"Job Level Filter Tab not present");
		}

	}

	public void paginationCheck(String resultCount) {

		try {

			int totalResult = Integer.parseInt(resultCount);

			if (verifyObjectDisplayed(paginationTab) && totalResult > 10) {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.PASS,
						"Pagination Present: with total results: " + totalResult);

				List<WebElement> topPageList = driver.findElements(topPaginations);

				String strLastPage = topPageList.get(topPageList.size() - 1).getText();
				strLastPage = strLastPage.trim();

				int lastNo = Integer.parseInt(strLastPage);

				int jobsCountPerPage = 0;
				for (int i = 1; i <= lastNo; i++) {

					if (i > 1) {
						clickElementBy(By.xpath(paginationParent + i + paginationEnd));
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

					String strTotalResult = driver.findElement(searchResultsTitle).getText();
					String[] strResult1 = strTotalResult.split("of");
					strTotalResult = strResult1[strResult1.length - 1];
					if (strTotalResult.contains("results")) {
						strTotalResult = strTotalResult.replaceAll("results", "");
					} else {
						strTotalResult = strTotalResult.replaceAll("result", "");
					}
					strTotalResult = strTotalResult.trim();

					int headResult = Integer.parseInt(strTotalResult);

					if (totalResult == headResult) {

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.PASS,
								"The Filter is intact on " + (i) + " page");
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.FAIL,
								"The Filter was not intact on " + (i) + " page");
					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> jobList = driver.findElements(viewJobCTA); // resultName

					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.PASS,
							"Total Count of Jobs in " + (i) + " page is: " + jobList.size());

					int tempCount = 0;
					tempCount = jobList.size();

					jobsCountPerPage += tempCount;

				}

				if (totalResult == jobsCountPerPage) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.PASS,
							"The total of jobs in all the pages equals the total job in the filter that is: "
									+ jobsCountPerPage);
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.FAIL,
							"The total of jobs in all the pages " + jobsCountPerPage
									+ " does not equals the total job in the filter that is: " + totalResult);
				}

			} else if (totalResult <= 10) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.PASS,
						"Only Single Page available");

				String strTotalResult = driver.findElement(searchResultsTitle).getText();
				String[] strResult1 = strTotalResult.split("of");
				strTotalResult = strResult1[strResult1.length - 1];
				if (strTotalResult.contains("results")) {
					strTotalResult = strTotalResult.replaceAll("results", "");
				} else {
					strTotalResult = strTotalResult.replaceAll("result", "");
				}
				strTotalResult = strTotalResult.trim();

				int headResult = Integer.parseInt(strTotalResult);

				if (totalResult == headResult) {

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.PASS,
							"The Filter is intact on the page ");
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.FAIL,
							"The Filter was not intact on the page");
				}
			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.FAIL,
						"Error in Pagination Validation");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.FAIL,
					"Error in Pagination Validation");
		}
	}

	public void jobValdandBack(String strTotalresult) {

		driver.switchTo().activeElement().sendKeys(Keys.HOME);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, resultsName, 120);
		if (verifyObjectDisplayed(resultsName)) {
			String strfirstHeader = null;
			// String strfirstLocation = null;
			// String strfirstDetails = null;
			String strfirstJobId = null;
			try {
				List<WebElement> strResultHeader = driver.findElements(resultsName);
				// List<WebElement> strResultLocation =
				// driver.findElements(articleLocation);
				// List<WebElement> strResultDetails =
				// driver.findElements(articleDetails);
				List<WebElement> strResultJobId = driver.findElements(jobsId);
				strfirstHeader = strResultHeader.get(0).getText();
				// strfirstLocation = strResultLocation.get(0).getText();
				// strfirstDetails = strResultDetails.get(0).getText();
				strfirstJobId = strResultJobId.get(0).getText();
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
						"Clicking First Element on the page, with Header: " + strfirstHeader);

				clickElementWb(strResultHeader.get(0));

			} catch (Exception e) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
						"Error in getting all the details for the first job in the ");
			}

			waitUntilElementVisibleBy(driver, resultClickHeader, 120);
			String newsHeader = driver.findElement(resultClickHeader).getText();
			if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
						"Successfully Navigated to associated page with header: " + newsHeader);

				backNavigationCheck(strTotalresult);

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
						"Could not navigate to associated page");
			}

		} else {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
					"Results not displayed");

		}

	}

	public void jobDescriptionForFilters(String strTotalresult) {
		try {
			waitUntilElementVisibleBy(driver, resultsName, 120);
			if (verifyObjectDisplayed(resultsName)) {
				String strfirstHeader = null;
				// String strfirstLocation = null;
				// String strfirstDetails = null;
				String strfirstJobId = null;
				try {
					List<WebElement> strResultHeader = driver.findElements(resultsName);
					// List<WebElement> strResultLocation =
					// driver.findElements(articleLocation);
					// List<WebElement> strResultDetails =
					// driver.findElements(articleDetails);
					List<WebElement> strResultJobId = driver.findElements(jobsId);
					strfirstHeader = strResultHeader.get(0).getText();
					// strfirstLocation = strResultLocation.get(0).getText();
					// strfirstDetails = strResultDetails.get(0).getText();
					strfirstJobId = strResultJobId.get(0).getText();
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
							"Clicking First Element on the page, with Header: " + strfirstHeader);

					clickElementWb(strResultHeader.get(0));

				} catch (Exception e) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Error in getting all the details for the first job in the ");
				}

				try {
					waitUntilElementVisibleBy(driver, resultClickHeader, 120);
					String newsHeader = driver.findElement(resultClickHeader).getText();
					if (newsHeader.equalsIgnoreCase(strfirstHeader)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Successfully Navigated to associated page with header: " + newsHeader);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Could not navigate to associated page");
					}

					if ((strfirstJobId).contains(getElementText(intermalPageJobId).trim())) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Job ID Present and Matched: " + strfirstJobId);
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Job Id is not matching or is not present");
					}

					if (verifyObjectDisplayed(resultPageDescription)) {

						List<WebElement> paraList = driver.findElements(resultPageDescriptionText);
						if (paraList.size() > 1) {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
									"Job Description with brief content present");
						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
									"Content not present");
						}

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Job Description not present");
					}

					if (verifyObjectDisplayed(resultPageLeftApplyBtn)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Apply for Job button present in the top left rail");

						clickElementBy(resultPageLeftApplyBtn);

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (driver.getTitle().contains("Welcome - Apply Process")) {

							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
									"Navigated to Apply For Job Page");
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							driver.navigate().back();

							waitUntilElementVisibleBy(driver, resultPageLeftApplyBtn, 120);

						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
									"Navigated to Apply For Job Page failed");
						}

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Apply for job button not present in the left");
					}

					if (verifyObjectDisplayed(resultPageJobFunction)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Job Function present as: " + getElementText(resultPageJobFunction));
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Job Function not present");
					}

					if (verifyObjectDisplayed(resultPageJobDetails)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Job Details present as: " + getElementText(resultPageJobDetails));
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Job Details not present");
					}

					if (verifyObjectDisplayed(resultPageDate)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Posting Date present as: " + getElementText(resultPageDate));
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Posting Date not present");
					}

					if (verifyObjectDisplayed(resultPageSchedule)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Schedule present as: " + getElementText(resultPageSchedule));
					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Schedule not present");
					}

					scrollDownByPixel(200);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(resultPageShare)) {
						List<WebElement> shareList = driver.findElements(resultPageShare);

						for (int i = 0; i < shareList.size(); i++) {

							String strShare = shareList.get(i).getAttribute("data-network");

							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
									"Sharing Option present at: " + strShare);

						}

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Sharing Option not present not present");
					}

					scrollDownForElementJSBy(resultPageCenterApplyBtn);
					scrollUpByPixel(200);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(resultPageCenterApplyBtn)) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
								"Apply for Job button present in the bootom center of the page");

						clickElementBy(resultPageCenterApplyBtn);

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (driver.getTitle().contains("Welcome - Apply Process")) {

							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
									"Navigated to Apply For Job Page");
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							driver.navigate().back();

							waitUntilElementVisibleBy(driver, resultPageCenterApplyBtn, 120);

							driver.switchTo().activeElement().sendKeys(Keys.HOME);
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.PASS,
									"Navigated back to job details page");

						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
									"Navigation to Apply For Job Page failed");
						}

					} else {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
								"Apply for job button not present in the bottom of the page");
					}

					backNavigationCheck(strTotalresult);

				} catch (Exception e) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
							"Error in Validating Job Description page");

					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} else {

				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobDescriptionsValidatns", Status.FAIL,
						"Results not displayed");

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("BrowseAllJobsPage", "paginationCheck", Status.FAIL,
					"Error in Job Description Validation");
		}

	}

	public void backNavigationCheck(String strSelectedFilter) {
		try {

			if (verifyObjectDisplayed(searchResultsBreadcrumb)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
						"Search Results present in the breadcrumb");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// clickElementBy(searchResultsBreadcrumb);
				driver.navigate().back();

				waitUntilElementVisibleBy(driver, searchResultsTitle, 120);

				String strTotalResult2 = driver.findElement(searchResultsTitle).getText();
				String[] strResult2 = strTotalResult2.split("of");
				strTotalResult2 = strResult2[strResult2.length - 1];
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
						"Number of Results: " + strTotalResult2);
				if (strTotalResult2.contains(strSelectedFilter)) {

					tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
							"The filter Result and Search result is equal that is: " + strTotalResult2
									+ " after nav back from details page");
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
							"The filter number and the result displayed did not match after navigating back from details page");

				}

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.PASS,
						"Search Results not present in the breadcrumb");
			}
		} catch (Exception e) {

			tcConfig.updateTestReporter("BrowseAllJobsPage", "filterDiffPageNavValdn", Status.FAIL,
					"Error in validating the filter");

			driver.navigate().back();
		}
	}

	public void multipleJobIdSearch() {
		String tempNo = testData.get("jobID");
		int tempInt = Integer.parseInt(tempNo);

		for (int k = 0; k < tempInt; k++) {
			if (verifyObjectDisplayed(keywordField)) {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
						"Keyword Field Present on the homepage");
				fieldDataEnter(keywordField, testData.get("jobId" + (k + 1)));

				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS, "Keyword Entered");

				if (browserName.equalsIgnoreCase("CHROME")) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(findJobsButtonHp);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else if (browserName.equalsIgnoreCase("IE")) {
					String strURL = testData.get("strKeywordSearchURL") + testData.get("jobId" + (k + 1));

					driver.navigate().to(strURL);
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(searchResultsTitle)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strTotalResult = driver.findElement(searchResultsTitle).getText();

					try {
						String strResultKey = driver.findElement(searchResultsKeyword).getText();
						String[] strResult = strTotalResult.split("of");
						strTotalResult = strResult[strResult.length - 1];

						if (strTotalResult.contains("1")) {
							if (strResultKey.toUpperCase().contains(testData.get("jobId" + (k + 1)).toUpperCase())) {
								tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
										"Number of Results: " + strTotalResult + " " + strResultKey);

								if (verifyObjectDisplayed(jobsId)
										&& getElementText(jobsId).contains(testData.get("jobId" + (k + 1)))) {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
											"Searched JobsId and Result Jobs ID matched as: " + getElementText(jobsId));
									if (verifyObjectDisplayed(articleHeader)) {
										tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.PASS,
												"Header: " + getElementText(articleHeader));
										CareerLoginPage loginPage = new CareerLoginPage(tcConfig);
										loginPage.navigateToHomepage("logo");

									} else {
										tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
												"Header not present ");
									}

								} else {
									tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
											"Searched Job ID and Result Job Id did not macth");
								}

							} else {

								tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
										"Job ID Entered did not match with Search result");
							}

						} else {
							tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
									"More than one results displayed for searched Job ID");
						}

					} catch (Exception e) {
						tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
								"Seacrh results are not generated for: "
										+ testData.get("jobId" + (k + 1)).toUpperCase());
					}

				} else if (verifyObjectDisplayed(noResults)) {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
							"No Results for searched Job ID");
				} else {
					tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
							"Search results are not displayed");
				}

			} else {
				tcConfig.updateTestReporter("BrowseAllJobsPage", "jobIdSearch", Status.FAIL,
						"Keyword field not present");
			}

		}
	}

}
