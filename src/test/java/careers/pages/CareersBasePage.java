package careers.pages;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CareersBasePage extends FunctionalComponents {

	public CareersBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change By
	 * : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	/*
	 * Method : launchApplication Parameters :None Description : launch application
	 * in browser Author : CTS Date : 2020 Change By : None
	 */

	public void launchApplication() {
		String url = testData.get("URL");
		driver.manage().deleteAllCookies();
		driver.navigate().to(url);
		driver.navigate().refresh();
		driver.manage().window().maximize();
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Application is Launched Successfully , URL : " + url);

	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser Author : CTS Date : 2019 Change By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

}
