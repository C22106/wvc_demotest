package careers.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class StudentsNGradsPage extends CareersBasePage {

	public static final Logger log = Logger.getLogger(StudentsNGradsPage.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public StudentsNGradsPage(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By headerTitle = By.xpath("//div[@class='column is-4']//h2[contains(.,'Students')]");
	private By videoIndex = By.xpath("//div[@data-slick-index='0']");
	private By videoClick = By.xpath("//div[@class='wyn-video-player__icon']");
	private By applyForInternship = By
			.xpath("//div[@class='wyn-content-block__cta']//a[contains(.,'Apply for Internship')]");
	private By browseAllJobsHeader = By.xpath("//div[@class='wyn-hero-forms']");
	private By joinOurTeamHeader = By.xpath("//span[contains(.,'JOIN')]/parent::div/parent::h2");
	private By internShipEligibility = By.xpath("//span[contains(.,'INTERNSHIP')]/parent::div/parent::h2");
	private By iFrameVideoVal = By.xpath("//iframe[@class='wyn-video-player__iframe']");
	private By openVideo = By.xpath("//div[@class='ytp-cued-thumbnail-overlay-image']");
	private By closeClick = By.xpath("//span[@class='wyn-modal__close']");
	private By faqList = By.xpath("//div[@class='columns wyn-accordion__container']//button");
	private By faqHeaderList = By.xpath("//span[@class='wyn-accordion__trigger-label ']");
	private By faqContentList = By.xpath("//div[@class='wyn-accordion__content__text']");
	private By breadCrumbHome = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	private By careerHeroImage = By.xpath("//div[@class='wyn-hero-forms']");
	private By studentsGradsTab = By.xpath("//a[@title='Students & Grads']");

	public void studentsNGradsPage() {

		if (verifyObjectDisplayed(studentsGradsTab)) {
			tcConfig.updateTestReporter("AssociateStoriesPage", "associateStoriesPage", Status.PASS,
					"Students & Grads Tab Present");

			clickElementBy(studentsGradsTab);

			waitUntilElementVisibleBy(driver, headerTitle, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(headerTitle)) {
				tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
						"Students and Grads Header Present on the page");

				if (verifyObjectDisplayed(applyForInternship)) {

					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
							"Apply For Internship Button present in the page");

					clickElementBy(applyForInternship);

					waitUntilObjectVisible(driver, browseAllJobsHeader, 120);
					if (driver.getTitle().toUpperCase().contains("JOBS")) {
						tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
								"Browse All Jobs page navigated");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
								"Browse All Jobs page could not be navigated");
					}

				} else {
					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
							"Apply for internship button not present");
				}

				clickElementBy(studentsGradsTab);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, headerTitle, 120);
				if (verifyObjectDisplayed(joinOurTeamHeader)) {

					scrollDownByPixel(200);

					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
							driver.findElement(joinOurTeamHeader).getText() + " section present");

					// content updated, no video

					/*
					 * if (verifyObjectDisplayed(videoClick)) {
					 * 
					 * tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage",
					 * Status.PASS, "Video Card Present on the S&G page");
					 * 
					 * waitForSometime(tcConfig.getConfig().get("MedWait")); List<WebElement>
					 * videoClick1 = driver.findElements(videoClick);
					 * clickElementWb(videoClick1.get(0));
					 * waitForSometime(tcConfig.getConfig().get("MedWait"));
					 * 
					 * driver.switchTo().frame(driver.findElement(iFrameVideoVal));
					 * waitUntilElementVisibleBy(driver, openVideo, 30);
					 * 
					 * if (verifyObjectDisplayed(openVideo)) {
					 * tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage",
					 * Status.PASS, "Video Overlay opened");
					 * 
					 * driver.switchTo().defaultContent();
					 * 
					 * clickElementBy(closeClick);
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * 
					 * } else { driver.switchTo().defaultContent();
					 * tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage",
					 * Status.FAIL, "Video Opening Error"); }
					 * 
					 * } else {
					 * 
					 * tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage",
					 * Status.FAIL, "Video Index Error"); }
					 */

				} else {
					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
							"Join Our Team section not present");
				}

				if (verifyObjectDisplayed(internShipEligibility)) {

					scrollDownForElementJSBy(internShipEligibility);
					scrollUpByPixel(200);

					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
							driver.findElement(internShipEligibility).getText() + " section present");
				} else {
					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
							"Internship Eligibility section not present");
				}

				List<WebElement> faqList1 = driver.findElements(faqList);
				List<WebElement> faqHeaderList1 = driver.findElements(faqHeaderList);
				List<WebElement> faqContentList1 = driver.findElements(faqContentList);

				scrollDownForElementJSWb(faqList1.get(0));
				scrollUpByPixel(200);

				if (verifyElementDisplayed(faqList1.get(0))) {

					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
							"FAQ Present on the S&G page");
					clickElementWb(faqList1.get(1));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyElementDisplayed(faqHeaderList1.get(1))
							&& verifyElementDisplayed(faqContentList1.get(1))) {
						tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
								"Faq Header and content Present");
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
								"Faq Functionality validation Error");
					}

				} else {
					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
							"FAQ List Error");
				}

				driver.switchTo().activeElement().sendKeys(Keys.HOME);
				if (verifyObjectDisplayed(breadCrumbHome)) {
					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.PASS,
							"Breadcrumb present and Home navigation present");

					clickElementBy(breadCrumbHome);
					waitUntilElementVisibleBy(driver, careerHeroImage, 120);

					if (verifyObjectDisplayed(careerHeroImage)) {

						tcConfig.updateTestReporter("CareersLoginPage", "launchApplication", Status.PASS,
								"Wyndham Careers Navigation Successful");
					} else {
						tcConfig.updateTestReporter("CareersLoginPage", "launchApplication", Status.PASS,
								"Error in navigating to homepage");
					}

				} else {
					tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
							"breadcrumb Error");
				}

			} else {
				tcConfig.updateTestReporter("StudentsNGradsPage", "studentsNGradsPage", Status.FAIL,
						"Students and Grads Header Error");
			}
		} else {
			tcConfig.updateTestReporter("AssociateStoriesPage", "associateStoriesPage", Status.FAIL,
					"Students & Grads Tab Error");
		}

	}

}
