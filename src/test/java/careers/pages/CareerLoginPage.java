package careers.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CareerLoginPage extends CareersBasePage {

	public static final Logger log = Logger.getLogger(CareerLoginPage.class);

	public CareerLoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By careerHeroImage = By.xpath("//div[@class='wyn-hero-forms']");
	private By acceptButton = By.xpath("//button[contains(.,'Accept')]");
	private By wyndhamLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']//a[2]");
	private By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	private By jobName = By.id("wyn-job-title");
	private By jobID = By.id("job-id");
	private By errorName = By.xpath("//h1[contains(.,'Error 404')]");

	public void launchApplication(String strBrowser) {
		this.driver = checkAndInitBrowser(strBrowser);
		driver.navigate().to(testData.get("URL"));
		String urlCheck = testData.get("URL");
		// driver.manage().window().maximize();

		waitUntilObjectVisible(driver, careerHeroImage, 120);

		if (verifyObjectDisplayed(careerHeroImage)) {
			if (urlCheck.toUpperCase().contains("QA")) {
				System.out.println("Testing in QA link");
			} else if (urlCheck.toUpperCase().contains("PROD")) {
				System.out.println("Testing in PROD link");
			}
			tcConfig.updateTestReporter("CareersLoginPage", "launchApplication", Status.PASS,
					"Wyndham Careers Navigation Successful");
			try {

				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("CareersLoginPage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementBy(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No Button");
			}

		} else {
			tcConfig.updateTestReporter("CareersLoginPage", "launchApplication", Status.FAIL,
					"Wyndham Careers Navigation Unsuccessful");
		}

	}

	public void navigateToHomepage(String strHome) {

		if (strHome.equalsIgnoreCase("logo")) {
			tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage");

			clickElementBy(wyndhamLogo);

			waitUntilObjectVisible(driver, careerHeroImage, 120);

			if (verifyObjectDisplayed(careerHeroImage)) {

				tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.PASS,
						"Wyndham Careers Navigation Successful");

			} else {
				tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.FAIL,
						"Wyndham Careers Navigation Unsuccessful");
			}

		} else if (strHome.equalsIgnoreCase("breadcrumb")) {
			tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage");

			clickElementBy(homeBreadcrumb);

			waitUntilObjectVisible(driver, careerHeroImage, 120);

			if (verifyObjectDisplayed(careerHeroImage)) {

				tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.PASS,
						"Wyndham Careers Navigation Successful");

			} else {
				tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.FAIL,
						"Wyndham Careers Navigation Unsuccessful");
			}
		} else {
			tcConfig.updateTestReporter("CareersLoginPage", "navigateToHomepage", Status.FAIL,
					"Unknown keyword entered, Failed to navigate to homepage");

		}

	}

	public void launchTaleoURL() {

		
		String urlCheck = testData.get("URL");

		for (int i = 0; i < 3; i++) {
			String taleoLink = urlCheck.concat(testData.get("taleoId" + (i + 1)));
			driver.navigate().to(taleoLink);

			driver.manage().window().maximize();

			waitUntilElementVisibleBy(driver, jobName, 20);

			if (verifyObjectDisplayed(jobName)) {
				tcConfig.updateTestReporter("CareersLoginPage", "launchTaleoURL", Status.PASS,
						"Navigated To Job Details Page");

				String jobIdDisplayed = driver.findElement(jobID).getText();
				String jobNameDisplayed = driver.findElement(jobName).getText().toUpperCase();

				String actualJobId = testData.get("jobId" + (i + 1));
				String actualJobName = testData.get("JobName" + (i + 1)).toUpperCase();

				if (jobIdDisplayed.trim().contains(actualJobId.trim())) {
					tcConfig.updateTestReporter("CareersLoginPage", "launchTaleoURL", Status.PASS,
							"Job ID with Respective Taleo Id Displayed");
				} else {
					tcConfig.updateTestReporter("CareersLoginPage", "launchTaleoURL", Status.FAIL,
							"Job ID with Respective Taleo Id not Displayed");
				}

				if (jobNameDisplayed.trim().contains(actualJobName.trim())) {
					tcConfig.updateTestReporter("CareersLoginPage", "launchTaleoURL", Status.PASS,
							"Job name with Respective Taleo Id Displayed");
				} else {
					tcConfig.updateTestReporter("CareersLoginPage", "launchTaleoURL", Status.FAIL,
							"Job name with Respective Taleo Id not Displayed");
				}

			} else if (verifyObjectDisplayed(errorName)) {
				tcConfig.updateTestReporter("CareersLoginPage", "launchTaleoURL", Status.FAIL,
						"The Taleo ID enetered is not valid, hence error message displayed");
			} else {
				tcConfig.updateTestReporter("CareersLoginPage", "launchTaleoURL", Status.FAIL,
						"Error in Taleo Validation");
			}

		}

	}

}
