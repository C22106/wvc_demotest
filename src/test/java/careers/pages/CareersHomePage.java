package careers.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CareersHomePage extends CareersBasePage {

	public static final Logger log = Logger.getLogger(CareersHomePage.class.getName());

	/**
	 * Constructor , page factory initialization
	 * 
	 * @param driver
	 */
	public CareersHomePage(TestConfig tcconfig) {
		// this.driver = driver;
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By careersHeroImage = By.xpath("//div[@class='wyn-hero-forms__image']");
	private By homepageHeader = By.xpath("//h1[@class='wyn-type-display-2 wyn-hero-forms__title']");
	private By slidingTabContainer = By.xpath("//div[@class='wyn-sliding-tabs__container']");
	private By individualTab2 = By.xpath("//a[@id='tab-2']");
	private By tab2Header = By.xpath("//div[@id='panel-2']//h2[@class='wyn-type-display-3']");
	private By tab2Content = By.xpath("//div[@id='panel-2']//div[@class='column wyn-tabs__copy wyn-l-content']");
	private By associateStoriesHeader = By.xpath("//h3/span[contains(.,'Associate')]");
	private By associatesList = By.xpath("//div[@class='wyn-card-content__container']");
	private By associateNameList = By.xpath(
			"//div[@class='wyn-card-content__container']//div[@class='wyn-type-subheading-1 wyn-l-margin-small--bottom']");
	private By associateImage = By.xpath("//div[@class='wyn-card-content__container']//img");
	private By associateCard = By.xpath("//div[@class='wyn-js-expand-card wyn-expand-card is-active']");
	private By carouselCardContent = By.xpath("//div[@class='wyn-js-expand-card wyn-expand-card is-active']//p");
	private By closeCard = By.xpath("//div[@class='wyn-js-expand-card wyn-expand-card is-active']/a");
	private By learnMore = By.xpath(
			"//div[@class='wyn-js-expand-card wyn-expand-card is-active']//a[@class='wyn-button-primary wyn-button-primary--dark']");
	private By wyndhamLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']//a[2]");

	private By joinOurTeamHeader = By.xpath("//h3/span[contains(.,'Join')]");
	private By searchResultBrdCrmbHeader = By.xpath("//span[contains(.,'Search')]");
	private By careerOpHeader = By.xpath("//h3/span[contains(.,'Career')]");
	private By careerOpportunityHeader = By.xpath("//h3/following-sibling::h2[contains(.,'Career')]");
	private By beyondOrdinaryHeader = By.xpath("//h3/following-sibling::h2[contains(.,'Go Beyond')]");
	private By careerOpportunityCards = By.xpath("//div[@class='wyn-card-list__card']/h3/a");// 3=students
																								// and
																								// grads
	private By ourCulturSection = By.xpath("//h2//span[contains(.,'OUR CULTURE')]");
	private By contributeHeader = By.xpath("//span[contains(.,'OUR CULTURE')]/ancestor::h2");
	private By learnMoreButton = By.xpath("//a[@class='wyn-button-primary']");// Our
																				// culture
	private By recruitingEventsHeader = By.xpath("//span[contains(.,'RECRUITING')]");
	private By recruitingEventsArticle = By.xpath("//article/h3/a");
	private By studentsNGradsheader = By.xpath("//div[@class='column is-4']//h2[contains(.,'Students')]");

	private By brandItemsFooter = By.xpath("//div[@class='column wyn-footer__brand-item']");
	private By contactUsFooter = By.xpath("//a[@class='wyn-footer__bold' and contains(.,'Contact Us')]");
	private By contactUsHeader = By.xpath("//div[@class='columns']//h1");
	private By wynCardsContact = By.xpath("//div[@class='wyn-vcard']");

	private By hotelResortLearnMoreCta = By
			.xpath("//a[contains(.,'Resort Operations')]/ancestor::div[@class='wyn-card-list__card']//div/a");
	private By hotelresortFilter = By
			.xpath("//div[@data-category='job-function']//label/span[contains(.,'Hotel/Resort Operations')]");
	private By searchResultsTitle = By.xpath("//span[@class='wyn-type-title-1' and contains(.,'result')]");

	private By talentNwTab = By.xpath("//a[@title='Join Our Talent Network']");
	private By iframeWidget = By.xpath("//div[@class='wyn-fly-out__container']/iframe");
	private By widgetHeader = By.xpath("//h2[contains(.,'Join')]");
	private By firstNameField = By.id("fname");
	private By lastNameField = By.id("lname");
	private By emailField = By.id("email");
	private By countryCodeSelect = By.xpath("//div[@class='iti-arrow']");
	private By indiaSelect = By.xpath("//li[@data-country-code='in']");
	private By mobileField = By.id("mobile");
	private By countrySelect = By.xpath("//div[@id='dk0-combobox']");
	private By categorySelect = By.xpath("//div[@id='dk1-combobox']");
	private By SubCategorySelect = By.xpath("//select[@id='job_speciality']");
	private By zipCodeField = By.id("postal_code");
	// private By subCategoryText = By.xpath("//label[contains(.,'Choose')]");
	private By tcCheckBox = By.id("PID10");
	private By agreeButton = By.id("formContinue");
	private By thankqNote = By.xpath("//p[@id='submission_result']//span");

	public void homePageValidations() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, careersHeroImage, 20);
		if (verifyObjectDisplayed(careersHeroImage)) {
			tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
					"Careers HomePage Displayed, Hero Banner Present with Title: "
							+ driver.findElement(homepageHeader).getText());
			scrollDownByPixel(250);

			if (verifyObjectDisplayed(joinOurTeamHeader) || verifyObjectDisplayed(careerOpHeader)) {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
						"Career Opportunities section present in the Homepage");

				if (verifyObjectDisplayed(careerOpportunityHeader) || verifyObjectDisplayed(beyondOrdinaryHeader)) {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
							"career Op Main Header present in the Homepage");

					List<WebElement> opportunitiesCardList = driver.findElements(careerOpportunityCards);
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
							"Career Opportunity cards present on the homepage Total: " + opportunitiesCardList.size());

					for (int i = 0; i < opportunitiesCardList.size(); i++) {

						tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
								getElementText(opportunitiesCardList.get(i)) + " card present in the homepage");

						if (i == 2) {
							clickElementJS(opportunitiesCardList.get(i));
							// update according to 10/10 release
							/*
							 * waitUntilObjectVisible(driver, studentsNGradsheader, 120);
							 * 
							 * if (verifyObjectDisplayed(studentsNGradsheader)) {
							 * tcConfig.updateTestReporter("CareersHomePage", "homePageValidations",
							 * Status.PASS, "Third card related page opened");
							 * 
							 * driver.navigate().back();
							 * 
							 * } else { tcConfig.updateTestReporter("CareersHomePage",
							 * "homePageValidations", Status.FAIL,
							 * "Students and Grads related page openeing error" ); }
							 */

							waitUntilElementVisibleBy(driver, searchResultBrdCrmbHeader, 120);
							if (verifyObjectDisplayed(searchResultBrdCrmbHeader)) {
								tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
										"Third card related page opened");

								driver.navigate().to(testData.get("URL"));

							} else {
								tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
										"Third Card related page openeing error");
							}
						}

					}

					waitUntilElementVisibleBy(driver, beyondOrdinaryHeader, 120);
					if (verifyObjectDisplayed(hotelResortLearnMoreCta)) {
						tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
								"Hotel resort Learn More CTA present on the homepage");

						clickElementBy(hotelResortLearnMoreCta);

						waitUntilElementVisibleBy(driver, searchResultsTitle, 120);

						if (verifyObjectDisplayed(hotelresortFilter)) {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
									"Hotel Resort Job Function Filter Present");

							filterResultsVerification(hotelresortFilter);
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						} else {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
									"Hotel Resort Job Function Filter not Present");

						}

					} else {
						tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
								"Hotel resort Learn More CTA not present on the homepage");
					}

				} else {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
							"Main header not present in homepage");
				}

			} else {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
						"Career Opportunities section not present in the Homepage");
			}

			clickElementBy(wyndhamLogo);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.navigate().to(testData.get("URL"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilObjectVisible(driver, beyondOrdinaryHeader, 120);
			scrollDownForElementJSBy(contributeHeader);
			scrollUpByPixel(150);

			if (verifyObjectDisplayed(ourCulturSection)) {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
						getElementText(ourCulturSection) + " section present in the Homepage");

				if (verifyObjectDisplayed(contributeHeader)) {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
							getElementText(contributeHeader) + " header present in the Homepage");

					if (verifyObjectDisplayed(learnMoreButton)) {
						tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
								"Learn More button present");
						clickElementBy(learnMoreButton);

						waitForSometime(tcConfig.getConfig().get("MedWait"));
						if (driver.getTitle().toUpperCase().contains("COMPANY")
								|| driver.findElement(By.xpath("//div[@id='main-content']//h1")).getText().toUpperCase()
										.contains("COMPANY")) {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
									"Our Culture page opened");
							driver.navigate().back();

						} else {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
									"Our Culture page not opened");
						}

					} else {
						tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
								"Learn More button not present");
					}

				} else {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
							"Career opportunity header not present in homepage");
				}

			} else {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
						"Join Our Team section not present in the Homepage");
			}

			waitUntilObjectVisible(driver, careersHeroImage, 120);

			scrollDownByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(individualTab2)) {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
						"Sliding Tab Container is present on homepage");

				clickElementBy(individualTab2);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
						"3rd Tab Opened Title: " + driver.findElement(individualTab2).getText());

				if (verifyObjectDisplayed(tab2Content)) {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
							"Sliding Tab Content and Title Present as :" + driver.findElement(tab2Header).getText());
				} else {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
							"Sliding Tab Error in content");
				}

			} else {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL, "Sliding Tab Error");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollDownForElementJSBy(associateStoriesHeader);
			scrollUpByPixel(200);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> associateList1 = driver.findElements(associatesList);
			List<WebElement> associateName1 = driver.findElements(associateNameList);
			List<WebElement> associateImg1 = driver.findElements(associateImage);

			if (verifyElementDisplayed(associateList1.get(0))) {
				if (verifyElementDisplayed(associateImg1.get(0))) {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
							"1st Associate Story is present with image and Name as: "
									+ associateName1.get(0).getText());

					associateList1.get(0).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(associateCard)) {
						if (verifyObjectDisplayed(carouselCardContent)) {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
									"1st Associate Card opened and content present");

							if (verifyObjectDisplayed(closeCard)) {
								scrollUpByPixel(150);
								clickElementBy(closeCard);

							} else {
								tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
										"Error in closing card");
							}

						} else {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
									"Associate Card content Error");
						}

					} else {
						tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
								"Associate Card Error");
					}

				} else {
					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
							"Associate Content Error");
				}
			} else {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
						"Associate List Error");
			}

			if (verifyObjectDisplayed(recruitingEventsHeader)) {

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollDownForElementJSBy(recruitingEventsHeader);
				scrollUpByPixel(200);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
						getElementText(recruitingEventsHeader) + " header present in the Homepage");

				List<WebElement> articleList = driver.findElements(recruitingEventsArticle);
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
						"Career Opportunity cards present on the homepage Total: " + articleList.size());

				for (int i = 0; i < articleList.size(); i++) {

					tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
							getElementText(articleList.get(i)) + " card present in the homepage");

					if (i == 2) {

						String thirdarticleHeader = getElementText(articleList.get(i)).toUpperCase();

						clickElementJS(articleList.get(i));

						if (driver.getTitle().toUpperCase().contains(thirdarticleHeader)) {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.PASS,
									"Third article related page opened");

						} else {
							tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
									"Thirdt article related related page openeing error");
						}
					}

				}

			} else {
				tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
						"Recruiting Events section in homepage");
			}

		} else {
			tcConfig.updateTestReporter("CareersHomePage", "homePageValidations", Status.FAIL,
					"Career homepage navigation error");
		}

	}

	public void footerContactUs() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, careersHeroImage, 20);
		if (verifyObjectDisplayed(careersHeroImage)) {
			tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.PASS,
					"Careers HomePage Displayed, Hero Banner Present with Title: "
							+ driver.findElement(homepageHeader).getText());

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			scrollDownForElementJSBy(brandItemsFooter);
			// scrollUpByPixel(200);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> brandList = driver.findElements(brandItemsFooter);
			if (verifyElementDisplayed(brandList.get(0))) {
				tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.PASS,
						"Brand Items Present; Total: " + brandList.size());

			} else {
				tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.FAIL, "Brand Items error");
			}

			if (verifyObjectDisplayed(contactUsFooter)) {
				tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.PASS,
						"Contact Us Present in footer");
				clickElementBy(contactUsFooter);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, contactUsHeader, 20);
				List<WebElement> wynCard = driver.findElements(wynCardsContact);
				if (verifyObjectDisplayed(contactUsHeader)) {
					tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.PASS,
							"Contact Us Header present");
					if (verifyElementDisplayed(wynCard.get(0))) {
						tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.PASS,
								"Contact Cards Present");
					} else {
						tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.FAIL,
								"Contact Cards not Present");
					}

				} else {
					tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.FAIL,
							"Contact Us Header not present");
				}

			} else {
				tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.FAIL, "Brand Items error");
			}

		} else {
			tcConfig.updateTestReporter("CareersHomePage", "footerContactUs", Status.FAIL,
					"Career homepage navigation error");
		}

	}

	public void widgetValidation() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, careersHeroImage, 20);
		if (verifyObjectDisplayed(careersHeroImage)) {
			tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.PASS,
					"Careers HomePage Displayed, Hero Banner Present with Title: "
							+ driver.findElement(homepageHeader).getText());
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(talentNwTab)) {

				tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.PASS,
						"Join Our Talent Network Widget Present in the homepage");

				clickElementBy(talentNwTab);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement eleIFrameWidget = driver.findElement(iframeWidget);
				driver.switchTo().frame(eleIFrameWidget);

				if (verifyObjectDisplayed(agreeButton)) {
					tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.PASS,
							"Widget Box Opened");

					try {
						if (verifyObjectDisplayed(firstNameField)) {
							driver.findElement(firstNameField).click();
							driver.findElement(firstNameField).sendKeys(testData.get("strFirstName"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}

						if (verifyObjectDisplayed(lastNameField)) {
							driver.findElement(lastNameField).click();
							driver.findElement(lastNameField).sendKeys(testData.get("strLastName"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}

						if (verifyObjectDisplayed(emailField)) {
							driver.findElement(emailField).click();
							driver.findElement(emailField).sendKeys(testData.get("strEmail"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
						if (browserName.equalsIgnoreCase("CHROME")) {
							if (verifyObjectDisplayed(mobileField)) {

								if (testData.get("strCountryCode").contains("IND")) {
									clickElementJSWithWait(countryCodeSelect);
									waitForSometime(tcConfig.getConfig().get("LowWait"));
									driver.switchTo().activeElement().sendKeys(testData.get("strCountryCode"));

									// clickElementBy(indiaSelect);
									driver.switchTo().activeElement().sendKeys(Keys.ENTER);
									waitForSometime(tcConfig.getConfig().get("LowWait"));
								} else if (testData.get("strCountryCode").contains("US")) {
									waitForSometime(tcConfig.getConfig().get("LowWait"));
								}
								driver.findElement(mobileField).click();
								driver.findElement(mobileField).sendKeys(testData.get("strMobileNo"));
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.switchTo().activeElement().sendKeys(Keys.TAB);
							}

							clickElementJSWithWait(countrySelect);
							driver.switchTo().activeElement().sendKeys(testData.get("strCountry"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							/*
							 * Select drpCountry = new Select(driver.findElement(countrySelect));
							 * drpCountry.selectByValue(testData.get( "strCountry"));
							 * waitForSometime(tcConfig.getConfig().get( "LowWait"));
							 */

							if (verifyObjectDisplayed(zipCodeField)) {
								driver.findElement(zipCodeField).click();
								driver.findElement(zipCodeField).sendKeys(testData.get("strZip"));
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.switchTo().activeElement().sendKeys(Keys.TAB);
							}

							// driver.switchTo().activeElement().sendKeys(Keys.TAB);
							clickElementJSWithWait(categorySelect);
							driver.switchTo().activeElement().sendKeys(testData.get("strCategory"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().activeElement().sendKeys(Keys.ENTER);
							/*
							 * Select drpCategory = new Select(driver.findElement(categorySelect));
							 * drpCategory.selectByValue(testData.get( "strCategory") );
							 * waitForSometime(tcConfig.getConfig().get( "LowWait"));
							 */

							// clickElementJSWithWait(categorySelect);

						} else if (browserName.equalsIgnoreCase("IE")) {
							driver.findElement(mobileField).click();
							driver.findElement(mobileField).sendKeys(testData.get("strUSMobileNo"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().activeElement().sendKeys(Keys.TAB);

							clickElementJSWithWait(countrySelect);
							// driver.switchTo().activeElement().sendKeys(testData.get("strCountry"));
							clickElementJSWithWait(
									By.xpath("//li[@class='dk-option ' and contains(.,'United States')]"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().activeElement().sendKeys(Keys.TAB);

							if (verifyObjectDisplayed(zipCodeField)) {
								driver.findElement(zipCodeField).click();
								driver.findElement(zipCodeField).sendKeys(testData.get("strUSZip"));
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.switchTo().activeElement().sendKeys(Keys.TAB);
							}

							clickElementJSWithWait(categorySelect);
							// driver.switchTo().activeElement().sendKeys(testData.get("strCategory"));

							clickElementJSWithWait(By.xpath("//li[@class='dk-option ' and contains(.,'Admin')]"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().activeElement().sendKeys(Keys.TAB);

							// clickElementJSWithWait(categorySelect);
							tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.PASS,
									"All Data Entered Successfully");
						}

						if (verifyObjectDisplayed(SubCategorySelect)) {

							Select drpSbCategory = new Select(driver.findElement(SubCategorySelect));
							drpSbCategory.selectByValue(testData.get("SubCategory"));
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						}

						tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.PASS,
								"All Data Entered Successfully");

						if (verifyObjectDisplayed(tcCheckBox)) {
							clickElementBy(tcCheckBox);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							System.out.println("No Check Box");
						}

						if (verifyObjectDisplayed(agreeButton)) {
							// clickElementBy(agreeButton);

							tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.PASS,
									"Submit Button Present");
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							// captcha introduced, can't be automated
							/*
							 * try { waitUntilElementVisibleBy(driver, thankqNote, 120); }catch(Exception e)
							 * {
							 * 
							 * } if (verifyObjectDisplayed(thankqNote)) {
							 * 
							 * tcConfig.updateTestReporter("CareersHomePage", "widgetValidation",
							 * Status.PASS, "Form Submission was successful");
							 * 
							 * } else { tcConfig.updateTestReporter("CareersHomePage", "widgetValidation",
							 * Status.FAIL, "Form Submission Error, Please Recheck Your Form" ); }
							 */

						} else {
							tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.FAIL,
									"Submit Button not Present");
						}

					} catch (Exception e) {
						tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.FAIL,
								"Error in Form Submission");
					}

				} else {
					tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.FAIL,
							"Widget Opening Error");
				}

			} else {

				tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.FAIL, "Talent Tab Error");
			}

		} else {
			tcConfig.updateTestReporter("CareersHomePage", "widgetValidation", Status.FAIL,
					"Career homepage navigation error");
		}
	}

	public void filterResultsVerification(By filterCheck) {
		String strSelectedFilter = getElementText(filterCheck);
		String[] strResult = strSelectedFilter.split(" ");
		strSelectedFilter = strResult[strResult.length - 1];
		strSelectedFilter = strSelectedFilter.replace(")", "").trim();
		strSelectedFilter = strSelectedFilter.replace("(", "").trim();

		String strTotalResult = driver.findElement(searchResultsTitle).getText();
		String[] strResult1 = strTotalResult.split("of");
		strTotalResult = strResult1[strResult1.length - 1];
		tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
				"Number of Results: " + strTotalResult);
		if (strTotalResult.contains(strSelectedFilter)) {
			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.PASS,
					"The filter Result and Search result is equal that is: " + strTotalResult);
		} else {
			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BrowseAllJobsPage", "allFilterValidations", Status.FAIL,
					"The filter number and the result displayed did not match");

		}

	}

}
