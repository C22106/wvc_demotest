package careers.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import careers.pages.BrowseAllJobs;
import careers.pages.CareerLoginPage;
import careers.pages.CareersHomePage;
import careers.pages.RecruitingEventsPage;
import careers.pages.StudentsNGradsPage;

public class CareersScripts extends TestBase {

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_001_WC_RecruitingEvents � Description: Recruiting Events Validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch1" })

	public void TC_001_WC_RecruitingEvents(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		RecruitingEventsPage recruiting = new RecruitingEventsPage(tcconfig);

		try {

			loginPage.launchApplication();

			recruiting.recruitingEventsValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_002_WC_JoinOurTalentNetwork � Description: Join our talent network
	 * Validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch1" })

	public void TC_002_WC_JoinOurTalentNetwork(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		CareersHomePage homePage = new CareersHomePage(tcconfig);

		try {

			loginPage.launchApplication();

			homePage.widgetValidation();

			// loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_003_WC_StudentsAndGradsVal � Description: Students and Grads
	 * Validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch1" })

	public void TC_003_WC_StudentsAndGradsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		StudentsNGradsPage studentsNGrads = new StudentsNGradsPage(tcconfig);

		try {

			loginPage.launchApplication();

			studentsNGrads.studentsNGradsPage();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_004_WC_HomepageValidations � Description: Homepage content and
	 * functionalities validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch1" })

	public void TC_004_WC_HomepageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		CareersHomePage homePage = new CareersHomePage(tcconfig);

		try {

			loginPage.launchApplication();

			homePage.homePageValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_005_WC_BrowseAllJobsValidations � Description: Browse All Jobs content
	 * and functionalities validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch1" })

	public void TC_005_WC_BrowseAllJobsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_006_WC_DifferentSearchValidations � Description: Search
	 * functionalities validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch1" })

	public void TC_006_WC_DifferentSearchValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.onlyKeywordSearch();
			jobsPage.onlyLocationSearch();
			jobsPage.keywordAndLocationSearch();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_007_WC_filterSearchFieldValdns � Description: Search functionalities
	 * validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch2" })

	public void TC_007_WC_filterSearchFieldValdns(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.filterSearchFieldValdns();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_008_WC_AllFilterValidations � Description: Filter functionalities
	 * validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch2" })

	public void TC_008_WC_AllFilterValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.allFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_009_WC_JobDetailsPageVerfications � Description: Job description page
	 * validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch2" })

	public void TC_009_WC_JobDetailsPageVerfications(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.jobIdSearch();
			jobsPage.jobDescriptionValdns();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_010_WC_InternshipJobApplyValidations � Description: Internship Job
	 * Apply Validations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch2" })

	public void TC_010_WC_InternshipJobApplyValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.jobIdSearch();
			jobsPage.jobDescriptionValdns();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_011_WC_JobsPageValidations � Description: Different Validations on
	 * Jobs page
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch2" })

	public void TC_011_WC_JobsPageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.jobPageValidations();
			jobsPage.jobFilterSameCityValdn();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_012_WC_DifferentPageNavFilterVald � Description: Filter validation
	 * after validating to details page and paginations
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch2" })

	public void TC_012_WC_DifferentPageNavFilterVald(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.filterDiffPageNavValdns();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_013_WC_AllCountryFilterNumbersVal � Description: Filter
	 * functionalities validations for country numbers
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch3" })

	public void TC_013_WC_AllCountryFilterNumbersVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.allFilterNumbersCheckForCountry();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	// � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// * TC_014_WC_CountryFiltersValidation � Description: Filter //
	// functionalities // * validations for country filters

	@Test(dataProvider = "testData", groups = { "careers", "batch3" })

	public void TC_014_WC_CountryFiltersValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.countryFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	// � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// TC_015_WC_StateFiltersValidation � Description: Filter
	// functionalities // validations for state filters //

	@Test(dataProvider = "testData", groups = { "careers", "batch3" })

	public void TC_015_WC_StateFiltersValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.stateFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion(); cleanUp(); }
		}
	}

	// // � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// // TC_016_WC_CityFiltersValidation � Description: Filter
	// // functionalities // validations for city filters
	//
	@Test(dataProvider = "testData", groups = { "careers", "batch3" })

	public void TC_016_WC_CityFiltersValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.cityFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion(); cleanUp(); }
		}
	}

	// // � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// // TC_017_WC_JobFunctionFiltersValidation � Description: Filter // *
	// // functionalities validations for job function filters //
	//
	@Test(dataProvider = "testData", groups = { "careers", "batch3" })

	public void TC_017_WC_JobFunctionFiltersValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.jobFunctionFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion(); cleanUp(); }
		}
	}

	// // � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// // TC_018_WC_BrandFiltersValidation � Description: Filter
	// // functionalities // validations for brand filters //
	//
	@Test(dataProvider = "testData", groups = { "careers", "batch3" })

	public void TC_018_WC_BrandFiltersValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.brandFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion(); cleanUp(); }
		}
	}

	// // � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// // TC_019_WC_ScheduleFiltersValidation � Description: Filter //
	// // functionalities // validations for brand filters //
	//
	@Test(dataProvider = "testData", groups = { "careers", "batch4" })

	public void TC_019_WC_ScheduleFiltersValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.scheduleFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion(); cleanUp(); }
		}
	}

	// // � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// // TC_020_WC_JobLevelFiltersValidation � Description: Filter //
	// // functionalities // validations for brand filters //
	//
	@Test(dataProvider = "testData", groups = { "careers", "batch4" })
	//
	public void TC_020_WC_JobLevelFiltersValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.jobLevelFilterValidations();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) { // TODO Auto-generated catch block //
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion(); cleanUp(); }
		}
	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_021_WC_AspenFilterValidations � Description: Filter functionalities
	 * validations for different scenarios Aspen
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch4" })

	public void TC_021_WC_SingleFilterValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.aspenValidations();
			jobsPage.resortQuestBritishColVal();
			jobsPage.myrtleBeachAndBrandsVal();
			jobsPage.anaheimAndBrandVal();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	 * TC_022_WC_FilterCombinations � Description: Filter functionalities
	 * validations for different scenarios Aspen
	 */

	@Test(dataProvider = "testData", groups = { "careers", "batch4" })

	public void TC_022_WC_FilterCombinations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.browseAllJobsNav();
			jobsPage.bendAndOregonVal();
			jobsPage.wanakaRotoruaVal();
			jobsPage.countryStateVal();
			jobsPage.stateCityVal();
			jobsPage.avonStateVal();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	// � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// * TC_023_WC_TaleoCheck � Description: Taleo
	// functionalities
	// * validations
	//
	@Test(dataProvider = "testData", groups = { "careers", "batch4" })
	public void TC_023_WC_TaleoCheck(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);

		try {

			loginPage.launchTaleoURL();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			cleanUp();
		}

	}

	// � Name:Unnat � Date of change: 11/27/18 � Version: 1 � function/event:
	// * TC_024_WC_JobsPresenceVal � Description: Jobs presence
	// functionalities
	// * validations
	//
	@Test(dataProvider = "testData", groups = { "careers", "batch4" })
	public void TC_024_WC_JobsPresenceVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		CareerLoginPage loginPage = new CareerLoginPage(tcconfig);
		BrowseAllJobs jobsPage = new BrowseAllJobs(tcconfig);

		try {

			loginPage.launchApplication();

			jobsPage.multipleJobIdSearch();

			loginPage.navigateToHomepage("logo");

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			cleanUp();
		}

	}

}