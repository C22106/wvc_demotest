package bbm.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import bbm.pages.AccommodationsPage;
import bbm.pages.BBMHomepage;
import bbm.pages.BBMHomepage_IOS;
import bbm.pages.BeginSearchPage;
import bbm.pages.BeginSearchPage_IOS;
import bbm.pages.GuestInfoPage;
import bbm.pages.GuestInfoPage_IOS;
import bbm.pages.PackageDetailsPage;
import bbm.pages.PaymentPage;
//import bbm.pages.PropertyDetailPage;
import bbm.pages.ReviewandBookPage;

public class BBMScripts_TabletIOS extends TestBase {

	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_001_BBM_footerValidations_CW � Description: Footer Validations Footer
	 * validation
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch1" })

	public void tc_001_BBM_footerValidations_CW(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage_IOS(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidation();
			homepage.footerCopyRight();
			homepage.footerLogoCWVal();
			homepage.footerAdvertisingMaterial();
			homepage.footertermsOfuseLink();
			homepage.footerPrivacyNoteLink();
			homepage.footerPrivacySettingLink();
			homepage.footerClubWyndhamLink();
			homepage.termsNdConditionCheck();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	/*
	 * � Name:Syba Malllick � � Version: 1 �
	 * function/tc_002_BBM_HeaderValidations � Description: Header validation
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch1" })
	public void tc_002_BBM_HeaderValidations_CW(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.headerLogoCW();
			homepage.labelContactNoValidation();
			// homepage.anchorHeroBannerValidation();
			homepage.selectDestination();
			homepage.anchorHeroBannerViewOfferValidation();
			homepage.linkViewPackageNavigation();
			homepage.linkBookNowNavigation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_003_BBM_FooterLinkNavigationValidations � Description: Special Request
	 * functionality validation
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch1" })
	public void tc_003_BBM_FooterLinkNavigationValidations_CW(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			// homepage.anchorHeroBannerValidation();
			/*
			 * homepage.footertermsOfuseLink();
			 * homepage.footerPrivacyNoteLink();
			 * homepage.footerPrivacySettingLink();
			 */
			// homepage.footerClubWyndhamLink();
			// homepage.footerWorldMarkByWyndhamLink();
			homepage.selectDestination();
			homepage.linkBookNowNavigation();
			// homepage.radioButtonIncentiveSelection();
			// beginPage.labelProgressBarValidation();
			/*
			 * beginPage.checkInMonthYearValidation();
			 * beginPage.guestPanelChild(); beginPage.calendarValidations();
			 * accommodationPage.popupModal();
			 */

			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();

			accommodationPage.resortDetailsValidation();
			accommodationPage.logoLinktoDestinationPageValidation();
			homepage.selectDestination();

			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * beginPage.checkInMonthYearValidation();
			 * beginPage.guestPanelChild(); beginPage.calendarValidations();
			 */

			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();

			accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			accommodationPage.popupModal();
			guestPage.pageGuestInformationNavigation();
			accommodationPage.logoLinktoDestinationPageValidation();
			homepage.selectDestination();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}
	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_004_BBM_DestinationContentPackageValidation � Description:Destination
	 * Content validation for Packages
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch1" })
	public void tc_004_BBM_DestinationContentPackageValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.destinationContentPackagesValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	// tc_005_BBM_PackageContentValidation
	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_005_BBM_HotelContentCardValidation � Description:Destination Content
	 * validation for Packages
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch1" })
	public void tc_005_BBM_HotelContentCardValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();

			homepage.selectDestination();
			homepage.CookiesValidationWOFail();
			homepage.packageSortValidation();
			homepage.linkViewPackageNavigation();
			homepage.linkBookNowNavigation();
			// homepage.radioButtonIncentiveSelection();
			packagePage.incentiveSelect();
			beginPage.freeCancellationPolicyDisplayValidation();
			// beginPage.checkInMonthYearValidation();
			packagePage.checkInMonthYearVal();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_006_BBM_PackageContentValidation � Description:Destination Content
	 * validation for Packages
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch2" })
	public void tc_006_BBM_PackageContentValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.packageContentValidationBBM();
			homepage.linkBookNowNavigation();

			/*
			 * homepage.radioButtonIncentiveSelection();
			 * beginPage.labelProgressBarValidation();
			 * beginPage.guestPanelChild(); //beginPage.checkInDateChange();
			 * beginPage.calendarValidations();
			 */

			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();

			// accommodationPage.popupModal();
			// accommodationPage.labelWyndhamPreferredValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_007_BBM_TripSummaryValidation � Description:Destination Trip Summary
	 * VValidation for Packages
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch2" })
	public void tc_007_BBM_TripSummaryValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage_IOS(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			// packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_008_BBM_GuestInfoErrorMessageValidation � Description:Destination Trip
	 * Summary VValidation for Packages
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch2" })
	public void tc_008_BBM_GuestInfoErrorMessageValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			// homepage.packageContentValidationBBM();
			homepage.selectDestination();
			homepage.linkBookNowNavigation();

			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.checkInDateChange();
			 */

			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();

			accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.labelProgressBarValidation();
			guestPage.pageGuestInformationMandatoryFieldErrorValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_009_BBM_PymnyPageErrorMessageValidation
	 * 
	 * � Description:Destination Trip Summary VValidation for Packages
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch2" })
	public void tc_009_BBM_PymnyPageErrorMessageValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.linkBookNowNavigation();

			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.checkInDateChange();
			 */

			// ****Updated Change***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//
			homepage.labelContactNoValidation();
			// BeginSearchPage.labelProgressBarValidation();
			accommodationPage.resortDetailsValidation();
			accommodationPage.linkBookNowNavigation();
			accommodationPage.labelProgressBarValidation();
			guestPage.pageGuestInformationNavigation();
			guestPage.labelProgressBarValidation();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.CardDetailsValidation();
			pymntPage.enterPaymentDetailsErrorvalidation();
			pymntPage.labelProgressBarValidation();
			pymntPage.cardTripSummaryPaymentPageValidation();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	// tc_010_BBM_CancellationPolicyValidationThroughoutBookingFlow
	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_010_BBM_CancellationPolicyValidationThroughoutBookingFlow �
	 * Description:To validate cancellation policy throughout booking flow
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch2" })
	public void tc_010_BBM_CancellationPolicyValidationThroughoutBookingFlow(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.linkBookNowNavigation();

			// homepage.radioButtonIncentiveSelection();
			packagePage.incentiveSelect();

			beginPage.freeCancellationPolicyDisplayValidation();
			// beginPage.checkInDateChange();
			packagePage.checkInDateChangeVal();
			accommodationPage.freeCancellationPolicyDisplayValidation();
			accommodationPage.linkBookNowNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.freeCancellationPolicyDisplayValidation();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetailsErrorvalidation();
			pymntPage.labelProgressBarValidation();
			pymntPage.freeCancellationPolicyDisplayValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	// tc_011_BBM_PaymentFailValidationSoftScoreOff
	/*
	 * � Name:Syba Malllick � � Version: 1 � function/event:
	 * tc_010_BBM_CancellationPolicyValidationThroughoutBookingFlow �
	 * Description:To validate cancellation policy throughout booking flow
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_011_BBM_PaymentFailValidationSoftScoreOff(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.linkBookNowNavigation();

			/*
			 * homepage.radioButtonIncentiveSelection();
			 * beginPage.checkInDateChange();
			 */

			packagePage.incentiveSelect();
			packagePage.checkInDateChangeVal();

			accommodationPage.linkBookNowNavigation();
			guestPage.pageGuestInformationNavigation();

			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.PaymentSoftScoreOffValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	/*
	 * � Name:Ankush � � Version: 1 � function/event:
	 * tc_012_BBM_PackagesValidation � Description: Package details Validations
	 */

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_012_BBM_PackagesValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		BeginSearchPage beginPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination_Complete();
			homepage.Separator_Validation();
			homepage.linkBookNowNavigation();
			homepage.back();
			homepage.linkViewPackageNavigation_book();
			packagePage.incentiveSelect();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	/*
	 * � Name:Anlkush � � Version: 1 � function/event:
	 * tc_013_BBM_TripSummaryCardTroughoutBookingFlow � Description: Trip
	 * summary Validations
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_013_BBM_TripSummaryCardTroughoutBookingFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();

			// homepage.Trip_Summary_View_details();
			// BeginSearchPage.addaChild_search();
			homepage.linkBookNowNavigation();
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();

			BeginSearchPage.Trip_Summary_View_details_BeginSearch();
			guestPage.Trip_Summary_View_details_GuestInfo();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.Trip_Summary_View_details_Payment();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();
			cnfrmPage.Video_Banner_validation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_014_BBM_New_Location_combined_incentive_validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();

			// homepage.radioButtonIncentiveSelection();

			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();

			// BeginSearchPage.checkInMonthYearValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_015_BBM_TripSummaryValidation_AMEX1(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			// packagePage.checkInMonthYearVal();
			// packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_016_BBM_TripSummaryValidation_AMEX2(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_017_BBM_TripSummaryValidation_VISA1(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch3" })
	public void tc_018_BBM_TripSummaryValidation_VISA2(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch1" })
	public void tc_019_BBM_TripSummaryValidation_DISCOVER1(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch4" })
	public void tc_020_BBM_TripSummaryValidation_DISCOVER2(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch4" })
	public void tc_021_BBM_TripSummaryValidation_MASTERCARD1(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch4" })
	public void tc_022_BBM_TripSummaryValidation_MASTERCARD2(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			packagePage.checkInMonthYearVal();
			packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.enterPaymentDetails();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch4" })
	public void tc_023_BBM_Email_form_Validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage_IOS(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			BeginSearchPage.Email_form_Validation();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			// homepage.radioButtonIncentiveSelection();
			packagePage.incentiveSelect();
			// packagePage.checkInMonthYearVal();
			// packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			BeginSearchPage.Email_form_Validation();
			homepage.labelContactNoValidation();
			accommodationPage.modifySearchValidation();
			accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			/*
			 * BeginSearchPage.Email_form_Validation();
			 * guestPage.pageGuestInformationNavigation();
			 * guestPage.popupModal();
			 * guestPage.pageGuestInformationMandatoryFieldValidation();
			 * BeginSearchPage.Email_form_Validation();
			 */
			// pymntPage.enterPaymentDetails();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch4" })
	public void tc_024_BBM_Trip_Summary_Retail_Validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			// homepage.radioButtonIncentiveSelection();
			packagePage.incentiveSelect();
			// packagePage.checkInMonthYearVal();
			// packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();

			BeginSearchPage.Trip_Summary_View_details_Validation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch4" })
	public void tc_025_BBM_New_Destinations_Dropdown_Validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.Destination_Drpdown();
			/*
			 * homepage.labelContactNoValidation();
			 * homepage.linkBookNowNavigation();
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.Trip_Summary_View_details_Validation();
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

	@Test(dataProvider = "testData", groups = { "bbm", "tablet", "ios", "batch4" })
	public void tc_026_BBM_PaypalPaymentValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		BBMHomepage homepage = new BBMHomepage(tcconfig);
		BeginSearchPage BeginSearchPage = new BeginSearchPage(tcconfig);
		AccommodationsPage accommodationPage = new AccommodationsPage(tcconfig);
		GuestInfoPage guestPage = new GuestInfoPage(tcconfig);
		PaymentPage pymntPage = new PaymentPage(tcconfig);
		ReviewandBookPage cnfrmPage = new ReviewandBookPage(tcconfig);
		PackageDetailsPage packagePage = new PackageDetailsPage(tcconfig);

		try {

			homepage.launchAppBBM();
			homepage.CookiesValidationWOFail();
			homepage.selectDestination();
			homepage.labelContactNoValidation();
			homepage.linkBookNowNavigation();
			/*
			 * homepage.radioButtonIncentiveSelection();
			 * BeginSearchPage.labelProgressBarValidation();
			 * BeginSearchPage.checkInMonthYearValidation();
			 * BeginSearchPage.guestPanelChild();
			 * BeginSearchPage.calendarValidations();
			 */

			// //****Updated Change Jan 2020***//
			packagePage.incentiveSelect();
			// packagePage.checkInMonthYearVal();
			// packagePage.guestPanelChildSel();
			packagePage.checkInDateChangeVal();
			//

			// BeginSearchPage.popupModal();
			homepage.labelContactNoValidation();
			// ****Updated Change Jan 2020***//
			// accommodationPage.modifySearchVal();

			// accommodationPage.resortDetailsValidation();
			accommodationPage.linkViewPropertyDetailsPageNavigation();
			guestPage.pageGuestInformationNavigation();
			guestPage.popupModal();
			guestPage.pageGuestInformationMandatoryFieldValidation();
			pymntPage.clickpaypalradio();
			pymntPage.paymentdetailspaypal();
			cnfrmPage.completeBookFlow();
			cnfrmPage.confirmationPageMapValidation();
			cnfrmPage.labelContactNoValidation();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			cleanUp();
		}
	}

}