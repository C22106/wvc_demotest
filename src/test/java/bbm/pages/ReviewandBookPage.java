package bbm.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ReviewandBookPage extends BBMBasePage {

	public static final Logger log = Logger.getLogger(ReviewandBookPage.class);

	public ReviewandBookPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By termsNConditionLink = By.xpath("//a[contains(.,'Terms and Conditions')]");
	protected By confirmBoking = By.xpath("//span[contains(.,'Thank you')]");
	protected By labelContactNo = By.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By map = By.xpath("//div[@id='map']");
	protected By orderSummury = By.xpath("//h1[@class='subtitle3 mb-half']");
	protected By linkViewDrivingDirection = By.xpath("//a[contains(.,'View Driving Directions')]");
	protected By labelTotalforPackage = By.xpath("//p[contains(.,'Total')]");
	protected By lengthofStay = By.xpath("//h1[@class='subtitle3 mb-half']");
	protected By labelTimesharePresentation = By.xpath("//h2[text()='TIMESHARE SALES PRESENTATION']");
	protected By clubwyndhamLogoHeader = By.xpath("//header//img[@alt='CLUB WYNDHAM']");
	protected By confirmationNo = By.xpath("//h2[text()='CONFIRMATION #']");
	public By fineprint = By.xpath("//div[@class='p-1 bg-indigo']");
	public By fineprint_content = By.xpath("//div[@class='bg-gray5 p-1.5']");
	/*
	 * Method Name: completeBookFlow Description: Date: Author:
	 */

	protected By confirmationTxt = By.xpath("(//div[contains(@class,'confirmation__user')]//p)[1]");

	public void completeBookFlow() {
		waitUntilObjectVisible(driver, confirmBoking, 180);
		if (verifyObjectDisplayed(confirmBoking)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"Confirmation Message is present in Confirmation Page. Booking is done successfully.");
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Confirmation Message is not present in Confirmation Page");
		}
		if (verifyObjectDisplayed(clubwyndhamLogoHeader)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"Header with brand logo for Package is present in Confirmation Page");
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Header with brand logo for Package is not present in Confirmation Page");
		}

		if (verifyObjectDisplayed(confirmationTxt)) {
			String strConfirmationNumber = driver.findElement(confirmationTxt).getText();
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"Confirmation number is displayed as " + strConfirmationNumber);
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Confirmation number is not displayed");
		}

		if (verifyObjectDisplayed(fineprint)) {
			String fireprint = driver.findElement(fineprint).getText();
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"The fine print Header is dispalyed with text : " + fireprint);
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"The fine print Header is not dispalyed");
		}

		if (verifyObjectDisplayed(fineprint_content)) {
			String fireprint_cntnt = driver.findElement(fineprint_content).getText();
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"The fine print content is dispalyed with text : " + fireprint_cntnt);
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"The fine print content is not dispalyed");
		}

		/*
		 * if (verifyObjectDisplayed(confirmationNo)) { String CnfrmNo =
		 * driver.findElement(confirmationNo).getText();
		 * tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow",
		 * Status.PASS, "Cnfrm No  for Package is present in Confirmation Page"
		 * +CnfrmNo); } else { tcConfig.updateTestReporter("ReviewandBookPage",
		 * "completeBookFlow", Status.FAIL,
		 * "Header with brand logo for Package is not present in Confirmation Page"); }
		 */
		if (verifyObjectDisplayed(labelTotalforPackage)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"Label Total for Package is present in Confirmation Page. Booking is done successfully.");
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Label Total for Package is not present in Confirmation Page");
		}
		if (verifyObjectDisplayed(orderSummury)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"Order Summary for Package is present in Confirmation Page.Booking is done successfully.");
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Order Summary is not present in Confirmation Page");
		}

		if (verifyObjectDisplayed(lengthofStay)) {
			String lengthOfStay = driver.findElement(lengthofStay).getText();
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"Length of Stay for Package is present in Confirmation Page." + lengthOfStay);
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Length of Stay for Package is not present in Confirmation Page");
		}
		scrollDownForElementJSBy(labelTimesharePresentation);
		if (verifyObjectDisplayed(labelTimesharePresentation)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.PASS,
					"In Payment Details page Timeshare Presentation label is validated"
							+ getElementText(labelTimesharePresentation));
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"In Payment Details page TimesharePresentation label is not validated");

		}

	}

	/*
	 * * Method Name: confirmationPageMapValidation Description:This method
	 * validates the presence of map and link view driving directions in
	 * Confirmation page Date: 31st July 2019 Author:Syba Mallick
	 */
	public void labelContactNoValidation() {

		waitUntilObjectVisible(driver, confirmBoking, 120);

		if (verifyObjectDisplayed(labelContactNo)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "labelContactNoValidation", Status.PASS,
					"Contact No is present in Confirmation Page");

		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Contact No is not present in Confirmation Page");
		}

	}

	public By video_banner = By.xpath("//div[contains(@class,'wyn-js-hero--video')]");
	public By video_play_icon = By.xpath("//div[contains(@class,'wyn-video-player__icon')]");

	public void Video_Banner_validation() {

		scrollDownForElementJSBy(video_banner);
		waitUntilObjectVisible(driver, video_banner, 120);

		if (verifyObjectDisplayed(video_banner)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "labelContactNoValidation", Status.PASS,
					"Video banner is present in Confirmation Page");

		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Video banner is not present in Confirmation Page");
		}

		if (verifyObjectDisplayed(video_play_icon)) {

			tcConfig.updateTestReporter("ReviewandBookPage", "labelContactNoValidation", Status.PASS,
					"Video Play icon is present in Confirmation Page");

		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "completeBookFlow", Status.FAIL,
					"Video Play icon is not present in Confirmation Page");
		}

	}

	/*
	 * * Method Name: confirmationPageMapValidation Description:This method
	 * validates the presence of map and link view driving directions in
	 * Confirmation page Date: 31st July 2019 Author:Syba Mallick
	 */
	public void confirmationPageMapValidation() {

		waitUntilObjectVisible(driver, map, 180);

		if (verifyObjectDisplayed(linkViewDrivingDirection)) {

			clickElementJSWithWait(linkViewDrivingDirection);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("ReviewandBookPage", "confirmationPageMapValidation", Status.PASS,
					"Link Open in Map is present in Accommodation Page");

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String mapURL = driver.getCurrentUrl();
			String URL = testData.get("urlMap");

			if (mapURL.contains(URL)) {

				tcConfig.updateTestReporter("ReviewandBookPage", "confirmationPageMapValidation", Status.PASS,
						"Clicking on maps is navigating to Google maps");
				driver.close();
				driver.switchTo().window(tabs2.get(0));
			} else {
				tcConfig.updateTestReporter("ReviewandBookPage", "confirmationPageMapValidation", Status.FAIL,
						"Clicking on maps is not navigating to Google maps");
			}

		}
	}

	/*
	 * * Method Name: termsNdConditionCheck Description:This method validates the
	 * presence of terms and Condition link in home page Date: 31st July 2019
	 * Author:Syba Mallick
	 */
	public void termsNdConditionCheck() {
		waitUntilObjectVisible(driver, termsNConditionLink, 120);
		if (verifyObjectDisplayed(termsNConditionLink)) {
			tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.PASS,
					"Terms and Condition link is present");
			clickElementBy(termsNConditionLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String termsNConditionLink = driver.getTitle();

			if (termsNConditionLink.contains("Terms")) {

				tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.PASS,
						"Terms and Condition page Navigation Successful.The title is : " + termsNConditionLink);

			}

			else {
				tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.FAIL,
						"Terms and Condition page Navigation  Unsuccessful");
			}
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.FAIL,
					"Terms and Condition Link not present");

		}

	}

	protected By totalPackageLabelTxt = By.xpath("//p[text()='Total for Package']");

	public void packagePriceValidation() {
		try {
			waitUntilElementVisibleBy(driver, totalPackageLabelTxt, 120);
			String strPckagePrice = driver.findElement(By.xpath("//p[text()='" + GuestInfoPage.postPackageAmt + "']"))
					.getText();
			if (strPckagePrice.equals(GuestInfoPage.postPackageAmt)) {
				tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.PASS,
						"Package Price is displayed after Appying the promo code");
			} else {
				tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.FAIL,
						"Package Price is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.FAIL,
					"Package Price is not displayed");
		}
	}

}