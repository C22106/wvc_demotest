package bbm.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ReviewandBookPage_Android extends ReviewandBookPage {

	public static final Logger log = Logger.getLogger(ReviewandBookPage_Android.class);

	public ReviewandBookPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void confirmationPageMapValidation() {

		waitUntilObjectVisible(driver, map, 180);

		if (verifyObjectDisplayed(linkViewDrivingDirection)) {
			String strURL = driver.getCurrentUrl();
			clickElementJSWithWait(linkViewDrivingDirection);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("ReviewandBookPage", "confirmationPageMapValidation", Status.PASS,
					"Link Open in Map is present in Accommodation Page");

			/*
			 * ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			 * driver.switchTo().window(tabs2.get(1));
			 * 
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */
			String mapURL = driver.getCurrentUrl();
			String URL = testData.get("urlMap");

			if (mapURL.contains(URL)) {

				tcConfig.updateTestReporter("ReviewandBookPage", "confirmationPageMapValidation", Status.PASS,
						"Clicking on maps is navigating to Google maps");
				/*
				 * driver.close(); driver.switchTo().window(tabs2.get(0));
				 */
				driver.get(strURL);
			} else {
				tcConfig.updateTestReporter("ReviewandBookPage", "confirmationPageMapValidation", Status.FAIL,
						"Clicking on maps is not navigating to Google maps");
			}

		}
	}

}
