package bbm.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PaymentPage_Android extends PaymentPage {

	public static final Logger log = Logger.getLogger(PaymentPage_Android.class);

	public PaymentPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void labelProgressBarValidation() {
		waitUntilObjectVisible(driver, labelProgressBar, 120);

		if (verifyObjectDisplayed(labelProgressBar)) {

			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"Progress Bar label is displayed");

		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
					"Progress Bar label not present");
		}
	}

	public void freeCancellationPolicyDisplayValidation() {

		scrollDownForElementJSBy(testCancel);

		List<WebElement> testlist = driver.findElements(testCancel);
		// testlist.get(testlist.size()-1).click();
		clickElementJSWithWait(testlist.get(testlist.size() - 1));

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: "
							+ contentCPolicy);
			// clickElementJSWithWait(linkLearnMoreCancellationPolicy);

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(frameCancellationPolicy)) {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.PASS,
						"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: "
								+ contentCPolicy);
			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.FAIL, "Cancellation Policy frame is not present");
			}
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}

}
