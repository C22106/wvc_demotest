package bbm.pages;

import java.awt.AWTException;
import java.awt.Robot;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BeginSearchPage_IOS extends BeginSearchPage {

	public static final Logger log = Logger.getLogger(BeginSearchPage_IOS.class);

	public BeginSearchPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public By closeTripSummarylnk = By.xpath("//a[@title='Close Trip Summary']");

	public void closeTripSummary() {
		waitUntilObjectVisible(driver, closeTripSummarylnk, 120);
		clickElementBy(closeTripSummarylnk);
		tcConfig.updateTestReporter("BeginSearchPage", "CloseTripSummary", Status.PASS,
				"Trip Summary is closed by cliking 'X' ");
	}

	
	@Override
	public void Email_form_Validation() throws AWTException {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(email_form);
		waitUntilObjectVisible(driver, email_form, 120);

		if (verifyObjectDisplayed(email_form)) {

			tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
					"The Email Form is present and the message is : " + driver.findElement(email_form).getText());

			waitUntilObjectVisible(driver, email_field, 120);
			if (verifyObjectDisplayed(email_field)) {

				if (driver.findElement(email_field_txt).getText().contains("Email Address")) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The Email field contains the text 'Email Address'");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL,
							"The Email field not contains the text 'Email Adress'");
				}

				if (driver.findElement(state_provinance).getText().contains("Your State/Province")) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The State field contains the text 'Your State/Province'");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL,
							"The State field not contains the text 'Your State/Province'");
				}

				fieldDataEnter(email_field, "XYZ");
				//driver.findElement(email_field).sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(email_form_submit);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(failure_msg)) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The Failure Message is populating as expected");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The Failure Message is not populating as expected");
				}

				driver.navigate().refresh();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.findElement(email_field).click();
				sendKeys((RemoteWebDriver) driver, "Email Address", "Joy.Watson@wyn.com");
				// fieldDataEnter(email_field, "Joy.Watson@wyn.com");
				// driver.findElement(email_field).sendKeys(Keys.TAB);
				Select sel = new Select(driver.findElement(state_select));
				sel.selectByValue("FL");
				//driver.findElement(state_select).sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(email_form_submit);

				waitUntilObjectVisible(driver, success_msg, 120);
				if (verifyObjectDisplayed(success_msg)) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The Success Message is populating as expected");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL,
							"The Success Message is NOT populating as expected");
				}

				scrollUpByPixel(3000);

			}

		} else {
			tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL, "The Email Form is not present");
		}

	}
}
