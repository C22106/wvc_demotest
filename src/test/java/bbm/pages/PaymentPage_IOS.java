package bbm.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PaymentPage_IOS extends PaymentPage {

	public static final Logger log = Logger.getLogger(PaymentPage_IOS.class);

	public PaymentPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void labelProgressBarValidation() {
		waitUntilObjectVisible(driver, labelProgressBar, 120);

		if (verifyObjectDisplayed(labelProgressBar)) {

			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"Progress Bar label is displayed");

		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
					"Progress Bar label not present");
		}
	}

	public void enterPaymentDetails() {
		waitUntilObjectVisible(driver, inputCreditCardNo, 120);
		if (verifyObjectDisplayed(headingPaymentInformation)) {

			tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
					"Payment Details header displayed,Payments details page navigated sucessfully ");

			if (verifyObjectDisplayed(labelContactNo)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.PASS,
						"Contact No is present in Payment Details Page");

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
						"Contact No not getting displayed in Payment Details page");
			}
			if (verifyObjectDisplayed(contentReservationandCancellationPolicy)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "contentReservationandCancellationPolicy",
						Status.PASS, "Reservation and Cancellation Policy is present in Payment Details Page");

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "contentReservationandCancellationPolicy",
						Status.FAIL, "Reservation and Cancellation Policy is not present in Payment Details page");
			}

			if (verifyObjectDisplayed(inputCreditCardNo)) {
				driver.findElement(inputCreditCardNo).click();
				sendKeys2((RemoteWebDriver) driver, "Credit Card Number", testData.get("CreditCardNo"));
				// fieldDataEnter(inputCreditCardNo, testData.get("CreditCardNo"));
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
						"In Payment details page input Credit card field is present and details are filled from data sheet.");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment details page input Credit card field is not present");

			}

			if (verifyObjectDisplayed(dropdwnExpirationdateMonth)) {
				Select drpCategoryMonth = new Select(driver.findElement(dropdwnExpirationdateMonth));
				drpCategoryMonth.selectByValue(testData.get("ExpiryMonth"));
				// selectByVisibletext(testData.get("ExpiryYear"));
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
						"In Payment Details page dropdown Expiry Month is present and data is set.");
			} else {

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page dropdown Expiry Month is not present");

			}
			if (verifyObjectDisplayed(drpdownExpirationYear)) {
				Select drpCategoryYear = new Select(driver.findElement(drpdownExpirationYear));
				drpCategoryYear.selectByValue(testData.get("ExpiryYear"));

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
						"In Payment Details page dropdown Expiry Year is present and data is set.");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page dropdown Expiry Year is not present");

			}
			scrollDownForElementJSBy(buttonReviewandBook);
			driver.findElement(buttonReviewandBook).click();
			// clickElementJSWithWait(buttonReviewandBook);
			tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
					"In Payment Details page Book Now Button is clicked successfully");
			waitForSometime(tcConfig.getConfig().get("LongWait"));
		}

		else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "launchenterPaymentDetailsAppBBM", Status.FAIL,
					"Payment Details header not displayed,Payments details page navigated fail");
		}

	}

	public void freeCancellationPolicyDisplayValidation() {

		/*
		 * try { waitUntilObjectVisible(driver, linkLearnMoreCancellationPolicy, 120); }
		 * catch (Exception e) { // TODO Auto-generated catch block e.printStackTrace();
		 * }
		 */
		// scrollDownByPixel(1200);
		/*
		 * scrollDownForElementJSWb(driver.findElements(testCancel).get(1));
		 * List<WebElement> testlist=driver.findElements(testCancel);
		 * testlist.get(testlist.size()-1).click();
		 */
		scrollDownForElementJSBy(testCancel);
		scrollUpByPixel(100);
		clickIOSElement("Learn More", 1, testCancel);

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: "
							+ contentCPolicy);
//			clickElementJSWithWait(linkLearnMoreCancellationPolicy);

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(frameCancellationPolicy)) {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.PASS,
						"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: "
								+ contentCPolicy);
			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.FAIL, "Cancellation Policy frame is not present");
			}
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}
}
