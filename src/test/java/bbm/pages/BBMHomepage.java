package bbm.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BBMHomepage extends BBMBasePage {

	public static final Logger log = Logger.getLogger(BBMHomepage.class);

	public BBMHomepage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public List<String> elements;

	@FindBy(xpath = "//span[text()='(844) 296-4004']")
	WebElement lblPhoneNo;
	public static String stringPkg1;
	public static String stringPkg2;
	public static String initial_pkg_amt;
	protected By linkViewDestinations = By.xpath("//a[contains(.,'View All Destinations')]");
	protected By linkViewOffersHB = By.xpath("//a[contains(.,'View Offers')]");

	protected By packagePricesSort = By.xpath(/* "//h3[contains(.,'$')]" */
			"//span[contains(.,'$')]/../../p[contains(@class,'giant text-black')]");
	protected By labelContactNo = By.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By termsNConditionLink = By.xpath("//a[text()='Terms and Conditions']");
	protected By advertisingMaterial = By.xpath(
			"//b[contains(.,'THIS ADVERTISING MATERIAL IS BEING USED FOR THE PURPOSE OF SOLICITING SALES OF TIMESHARE INTERESTS.')]");
	protected By worldMarkLogoHeader = By.xpath("//header//img[@alt='WorldMark by Wyndham']");
	protected By worldMarkLogoFooter = By.xpath("//footer//img[@alt='WorldMark by Wyndham']");
	protected By clubwyndhamLogoFooter = By.xpath("//footer//img[@alt='CLUB WYNDHAM']");
	protected By clubwyndhamLogoHeader = By.xpath("//header//img[@alt='CLUB WYNDHAM']");
	protected By linkTermsOfuse = By.xpath("//a[contains(.,'Terms of Use')]");
	protected By linkCopyright = By.xpath("//p[contains(.,'Rights')]");
	protected By linkPrivacyNotice = By.xpath("//a[contains(.,'Privacy Notice')]");
	protected By linkPrivacySetting = By.xpath("//a[contains(.,'Privacy Setting')]");
	protected By tripsummaryBookNow = By.xpath("//section//span[contains(.,'Book Now')]");
	protected By tripsummaryAvailabilty = By.xpath("//a[contains(.,'Search Availability')]");
	protected By buttonBookNow = By.xpath("(//span[@data-fragment='global.package.detail.book.now'])[1]");
	protected By buttonBookNow2 = By.xpath("(//span[@data-fragment='global.package.detail.book.now'])[2]");
	protected By linkBacktoVacationPackages = By.xpath("//a[contains(.,'Back to Vacation Packages')]");

	protected By linkViewPackages = By.xpath("(//a[contains(.,'View Details')])[2]");
	protected By linkPrivacySettingpage = By.xpath(/*"//h1['Interest Based Advertising Policies']"*/
			"(//div[@class='title-1' and contains(text(),'PRIVACY SETTINGS')])[2]");
	protected By diffPackages = By.xpath("//div[@class='wyn-bbm-package flex flex-col items-start h-full']");
	protected By lengthOfStay = By.xpath("//h4[contains(.,'Hotel Accommodations')]");
	protected By packageExtras = By.xpath("//h4[contains(.,'Package Extras')]");
	protected By discountSaving = By.xpath("//div[@class='icon-bed w-full icon-savings']");

	protected By listpackage = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-title']//h3");
	protected By startingUpto = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-l-content wyn-card__copy']");
	protected By viewoffer = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-card__cta']");
	protected By pkgDetailTripSummrry = By.xpath("//div[@class='trip-summary__footer p-1 bg-gray3 ']");
	protected By PackageDetailsTripSummary = By.xpath("//h2[@class='font-heading text-xl font-medium mb-half']");
	protected By popupCookies = By.xpath("//div[@id='cookies']");
	protected By btnAccept = By.xpath("//span[contains(.,'Accept')]");

	protected By radiobtnIncentive = By.xpath("//label[contains(text(),'Wyndham Reward')]/../input");
	// protected By radiobtnIncentive = By.xpath("//label[contains(text(),'Wyndham
	// Rewards Points')]/../input");
	protected By btnSelectNcontinue = By.xpath("//span[contains(text(),'Select & Continue')]");
	protected By btnselctncontinue = By.xpath("//span[@id='selectIncentive']");

	protected By linkLearnMoreCancellationPolicy = By.xpath("//a[@data-fragment='global.cancellation.LearnMore']");
	protected By contentCancellationPolicy = By.xpath("//p[contains(.,'Cancel within')]");
	protected By frameCancellationPolicy = By.xpath("//div[@id='popupCancellation']");
	protected By frameCancellationPolicycClose = By.xpath("//*[@id='popupCancellation']/div");

	/*
	 * Method Name: freeCancellationPolicyDisplayValidation Description:This method
	 * validates free Cancellation Policy Display throughout the booking flow Date:
	 * 25th Oct 2019 Author:Syba Mallick
	 */

	public void freeCancellationPolicyDisplayValidation() {

		waitUntilObjectVisible(driver, linkLearnMoreCancellationPolicy, 120);

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: "
							+ contentCPolicy);
			clickElementJSWithWait(linkLearnMoreCancellationPolicy);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			verifyObjectDisplayed(frameCancellationPolicy);
			tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: "
							+ contentCPolicy);
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
					"Cancellation Policy frame is not present");
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}

	/*
	 * Method Name: CookiesValidation Description:This method validates Cookies in
	 * theHome Page of destination landing Date: 18th Sept 2019 Author:Syba Mallick
	 */

	public void CookiesValidation() {

		try {
			waitUntilObjectVisible(driver, btnAccept, 120);

			if (verifyObjectDisplayed(btnAccept)) {
				String cookies = driver.findElement(popupCookies).getText();

				tcConfig.updateTestReporter("BBMHomepage", "CookiesValidation", Status.PASS,
						"Cookies is present in Destination Package Page with the content as: " + cookies);
				clickElementJSWithWait(btnAccept);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerValidation", Status.FAIL,
						"Cookies not present");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Accept button not present");
		}

	}

	/*
	 * * Method Name: termsNdConditionCheck Description:This method validates the
	 * presence of terms and Condition link in home page Date: 31st July 2019
	 * Author:Syba Mallick
	 */

	protected By termsTxt = By.xpath("//h1[text()='Terms and Conditions']");

	public void termsNdConditionCheck() {
		waitUntilObjectVisible(driver, termsNConditionLink, 120);
		if (verifyObjectDisplayed(termsNConditionLink)) {
			tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.PASS,
					"Terms and Condition link is present");
			clickElementBy(termsNConditionLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * String termsNConditionLink = driver.getTitle();
			 * System.out.println(termsNConditionLink);
			 */
			/*
			 * ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
			 * driver.switchTo().window(tab.get(1));
			 */
			waitUntilObjectVisible(driver, termsTxt, 120);
			if (verifyObjectDisplayed(termsTxt)) {

				tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.PASS,
						"Terms and Condition page Navigation Successful.The title is : " + termsNConditionLink);

			}

			else {
				tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.FAIL,
						"Terms and Condition page Navigation  Unsuccessful");
			}
		} else {
			tcConfig.updateTestReporter("ReviewandBookPage", "termsNdConditionCheck", Status.FAIL,
					"Terms and Condition Link not present");

		}

	}

	public void CookiesValidationWOFail() {
		try {
			waitUntilObjectVisible(driver, btnAccept, 30);
		} catch (Exception e) {
			System.out.println("Accept Not Presnt");
		}

		if (verifyObjectDisplayed(btnAccept)) {
			String cookies = driver.findElement(popupCookies).getText();

			tcConfig.updateTestReporter("BBMHomepage", "CookiesValidation", Status.PASS,
					"Cookies is present in Destination Package Page with the content as: " + cookies);
			clickElementJSWithWait(btnAccept);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerValidation", Status.PASS,
					"Cookies not Displayed");
		}

	}

	/*
	 * Method Name: anchorHeroBannerValidation Description:This method validates
	 * Hero Banner in the Package Page of destination Date: 04th October 2019
	 * Author:Syba Mallick
	 */

	public void anchorHeroBannerViewOfferValidation() {

		waitUntilObjectVisible(driver, linkViewOffersHB, 120);

		if (verifyObjectDisplayed(linkViewOffersHB)) {

			tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerViewOfferValidation", Status.PASS,
					"HeroBanner is present in Destination Package Page");
			List<WebElement> ViewOfferList = driver.findElements(linkViewOffersHB);
			tcConfig.updateTestReporter("BeginSearchPage", "calenderValidations", Status.PASS,
					"View Offer is present in the Hero Banner of Package Card page");
			ViewOfferList.get(0).click();
			// clickElementJSWithWait(linkViewOffersHB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(listpackage)) {
				tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerViewOfferValidation", Status.PASS,
						"List of packages visible in Destination Page");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerViewOfferValidation", Status.FAIL,
						"List of packages not visible in Destination Page");
			}

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerViewOfferValidation", Status.FAIL,
					"HeroBanner is not present in Destination Page");
		}

	}

	/*
	 * Method Name: anchorHeroBannerValidation Description:This method validates
	 * Hero Banner in theHome Page of destination landing Date: 11th July 2019
	 * Author:Syba Mallick
	 */

	public void anchorHeroBannerValidation() {

		waitUntilObjectVisible(driver, linkViewDestinations, 120);

		if (verifyObjectDisplayed(linkViewDestinations)) {

			tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerValidation", Status.PASS,
					"HeroBanner is present in Destination Package Page");
			clickElementJSWithWait(linkViewDestinations);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(listpackage)) {
				tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerValidation", Status.PASS,
						"List of packages visible in Destination Page");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerValidation", Status.FAIL,
						"List of packages not visible in Destination Page");
			}

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "anchorHeroBannerValidation", Status.FAIL,
					"HeroBanner is not present in Destination Page");
		}

	}

	/*
	 * Method Name: packageSortValidation Description:This method validates the
	 * sorting of packages. Date: 11th July 2019 Author:Syba Mallick
	 */

	public void packageSortValidation() {

		waitUntilObjectVisible(driver, packagePricesSort, 120);
		tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
				"Destination Landing Pages: Package information shown as cards");

		List<WebElement> ViewPackagetList = driver.findElements(packagePricesSort);

		int i;

		for (i = 0; i < ViewPackagetList.size(); i++) {

			String strPackagePricing = ViewPackagetList.get(i).getText();
			strPackagePricing = strPackagePricing.replace("$", "").trim();
			if (i == 0) {

				stringPkg1 = strPackagePricing;
			} else if (i == 1) {
				stringPkg2 = strPackagePricing;
			}
		}
		if (Integer.parseInt(stringPkg1) <= Integer.parseInt(stringPkg2)) {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages: Lowest price and lowest package id number is displayed in order left to right");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL, "Packages are not sorted");
		}
		if (verifyObjectDisplayed(packageExtras)) {
			scrollDownForElementJSBy(packageExtras);
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages:In Package Card Package Card Package Extras are present");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL,
					"Destination Landing Pages:In Package Card Package Card Package Extras are not present");
		}
		if (verifyObjectDisplayed(lengthOfStay)) {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages:In Package Card Lenght of Stay are present.");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL,
					"Destination Landing Pages:In Package Card Lenght of Stay are not present.");
		}
		if (verifyObjectDisplayed(discountSaving)) {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages:In Package Card Discount Savings are present.");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL,
					"Destination Landing Pages:In Package Card Discount Savings are not present.");
		}

	}

	/*
	 * Method Name: packageSortValidation Description:This method validates the
	 * occurrence of the separator in the incentive choices of packages. Date: 15th
	 * Nov 2019 Author:Ankush
	 */

	protected By PackageExtra = By.xpath("//div//h4[text()='Package Extras']");

	// public By PackageExtra = By.xpath("//div//h4[text()='Package Incentives']");
	public void Separator_Validation() {

		waitUntilObjectVisible(driver, PackageExtra, 120);
		tcConfig.updateTestReporter("BBMHomepage", "PackageExtra", Status.PASS, "Packages Page is present");

		String script = "return window.getComputedStyle(document.querySelector('ul.incentive-choices li:not(:first-of-type)'),':after').getPropertyValue('content')";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String content = (String) js.executeScript(script);
		// System.out.println(content);

		if (content.toUpperCase().contains("OR")) {
			tcConfig.updateTestReporter("BBMHomepage", "PackageExtra", Status.PASS,
					"The Incentive choices separator is present");
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "PackageExtra", Status.FAIL,
					"The Incentive choices separator is not present");
		}
	}

	/*
	 * Method Name: selectDestination Description:This method validates the presence
	 * of destinations and selecting destination from test data sheet. Date: 11th
	 * July Author:Syba Mallick
	 */

	public void selectDestination() {

		waitUntilObjectVisible(driver, listpackage, 120);
		tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.PASS,
				"Destination Packages are present in the application as from Home page");
		List<WebElement> ViewPropertyList = driver.findElements(listpackage);
		Boolean tempVal = true;

		for (int i = 0; i < ViewPropertyList.size(); i++) {

			String strDestination = ViewPropertyList.get(i).getText();
			// ViewPropertyList.get(0).click();

			if (strDestination.equalsIgnoreCase(testData.get("Destination"))) {

				ViewPropertyList.get(i).click();

				tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.PASS,
						"Destination is selected as " + (strDestination) + " from Home page");

				break;
			} else {
				tempVal = false;
			}

		}

		if (tempVal = false) {
			tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.FAIL,
					"The destination value does not exist in application");
		} else {

		}

	}

	/*
	 * Method Name: Destination_Drpdown Description:This method validates the
	 * presence of destinations Dropdown on home page . Date: 21th Jan 2020
	 * Author:Ankush
	 */

	protected By dest_drop = By.xpath("//a[contains(@class,'show-dropdown')]");
	protected By dest_table = By.xpath("//div[@class='top-nav__dropdown']");
	protected By all_Destination_list = By.xpath("//div[@class='top-nav__dropdown']//ul//li//a[contains(text(),',')]");

	public void Destination_Drpdown() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, dest_drop, 120);
		if (verifyObjectDisplayed(dest_drop)) {
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.PASS,
					"Destination Dropdown is present");
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.FAIL,
					"Destination Dropdown is NOT present");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(dest_drop);
		/*
		 * Actions actions = new Actions(driver); WebElement menuOption =
		 * driver.findElement(dest_drop); actions.moveToElement(menuOption).perform();
		 */
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(dest_table)) {
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.PASS, "Destinations are present");
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.FAIL,
					"Destinations are NOT present");
		}

		List<WebElement> dest_list = driver.findElements(all_Destination_list);
		int dest_ele = dest_list.size();

		for (int i = 0; i <= dest_ele - 1; i++) {
			System.out.println(dest_list.get(i).getText());
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.PASS,
					"visible destination no-" + (i + 1) + " is :" + dest_list.get(i).getText());

		}
		/*
		 * boolean isSorted = Ordering.natural().isOrdered(elements);
		 * System.out.println(isSorted);
		 */

		// waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, dest_drop, 120);
		clickElementJSWithWait(dest_drop);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (!verifyObjectDisplayed(dest_table)) {
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.PASS,
					"Destination dropdown is closed on clicking Destinations link");
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.FAIL,
					"Destination dropdown is NOT closed on clicking Destinations link");
		}

	}

	/*
	 * Method Name: selectDestination_Complete Description:This method validates the
	 * presence of destinations, starting from and Upto offers and selecting
	 * destination from test data sheet. Date: 11th July Author:Ankush
	 */
	protected By homepageBanner = By.xpath("//div[@class='banner bbm-banner']");

	public void selectDestination_Complete() {

		waitUntilObjectVisible(driver, listpackage, 120);
		if (verifyObjectDisplayed(homepageBanner)) {
			tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.PASS, "Homepage Banner is present");
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.FAIL,
					"Homepage Banner is not present ");
		}

		tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.PASS,
				"Destination Packags are present in the application as from Home page");
		List<WebElement> ViewPropertyList = driver.findElements(listpackage);
		// List<WebElement> ViewStartiingupto= driver.findElements(startingUpto);
		List<WebElement> ViewOffers = driver.findElements(viewoffer);
		Boolean tempVal = true;

		for (int i = 0; i < ViewPropertyList.size(); i++) {

			String strDestination = ViewPropertyList.get(i).getText();
			// ViewPropertyList.get(0).click();
			// String strstaringupto = ViewStartiingupto.get(i).getText();

			String strviewoffers = ViewOffers.get(i).getText();

			if (strDestination.equalsIgnoreCase(testData.get("Destination"))) {

				tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.PASS,
						"Destination is selected as " + (strDestination) + " from Home page");
				// tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.PASS,
				// "The Offerings are "+strstaringupto);
				tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.PASS,
						"The " + strviewoffers + " Link is present");

				ViewPropertyList.get(i).click();

				break;
			} else {
				tempVal = false;
			}

		}

		if (tempVal = false) {
			tcConfig.updateTestReporter("BBMHomepage", "selectDestination", Status.FAIL,
					"The destination value does not exist in application");
		} else {

		}

	}

	/*
	 * Method Name: launchAppBBM Description:This method launches the application
	 * Date:3rd May Author:Syba Mallick
	 */

	public void launchAppBBM() {
		driver.navigate().to(testData.get("URL"));
		// driver.manage().window().maximize();

		waitUntilObjectVisible(driver, advertisingMaterial, 120);
		if (verifyObjectDisplayed(advertisingMaterial)) {

			tcConfig.updateTestReporter("BBMHomepage", "launchAppBBM", Status.PASS,
					"BBM application launched Successfully");
			scrollDownForElementJSBy(listpackage);
			scrollUpByPixel(170);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "launchAppBBM", Status.FAIL, "Unable to launch BBM application");
		}

	}

	/*
	 * Method Name: packageContentValidationBBM Description:This method validates
	 * package Content Date: 11th April Author:Syba mallick
	 */

	public void packageContentValidationBBM() {
		waitUntilObjectVisible(driver, diffPackages, 120);
		scrollDownForElementJSBy(diffPackages);
		List<WebElement> packagesList = driver.findElements(diffPackages);
		int Packages = packagesList.size();

		tcConfig.updateTestReporter("BBMHomepage", "packageContentValidationBBM", Status.PASS,

				"There are total: " + Packages + " Packages available for" + driver.getTitle() + "location");
		List<WebElement> lengthOfStayList = driver.findElements(diffPackages);
		int stay = lengthOfStayList.size();
		verifyObjectDisplayed(lengthOfStay);
		verifyObjectDisplayed(discountSaving);
		if (Packages == stay) {
			tcConfig.updateTestReporter("BBMHomepage", "packageContentValidationBBM", Status.PASS,
					"In BBM application: Brands,:lengthOfStay,:Price,Saving upto Value :Description of Premium are available per packages ");
		} else

		{
			tcConfig.updateTestReporter("BBMHomepage", "packageContentValidationBBM", Status.FAIL,
					"In BBM application: Brands,:lengthOfStay,:Price,Saving upto Value :Description of Premium are not available per packages ");
		}

		/*
		 * if (verifyObjectDisplayed(packageExtras) &&
		 * verifyObjectDisplayed(packagesIncentives) &&
		 * verifyObjectDisplayed(discountSaving)) {
		 * 
		 * 
		 * tcConfig.updateTestReporter("BBMHomepage", "packageContentValidationBBM",
		 * Status.PASS,
		 * "In BBM application: Brands,:length Of Stay,Packages Incentives and discount Savings are available as per packages "
		 * ); } else { tcConfig.updateTestReporter("BBMHomepage",
		 * "packageContentValidationBBM", Status.FAIL,
		 * "In BBM application: Brands,:length Of Stay,Packages Incentives and discount Savings are not available as per packages"
		 * ); }
		 */

	}

	/*
	 * Method Name: footerValidations Description:This method validates all the
	 * elements and link present in the in the footer section Date: 31st July 2019
	 * Author:Syba Mallick
	 */

	public void footerValidations() {
		footerCopyRight();
		footertermsOfuseLink();
		footerPrivacyNoteLink();
		footerPrivacySettingLink();
	}

	/*
	 * Method Name: footertermsOfuseLink Description:This method validates
	 * Advertising Material in the footer section Date: 31st July Author:Syba
	 * Mallick
	 */

	public void footertermsOfuseLink() {

		if (verifyObjectDisplayed(linkTermsOfuse)) {
			tcConfig.updateTestReporter("BBMHomepage", "footertermsOfuseLink", Status.PASS, "BBM Home page navigated");

			clickElementBy(linkTermsOfuse);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String termsOfusepageURL = driver.getCurrentUrl();
			System.out.println(termsOfusepageURL);
			String URL = testData.get("urlTermOfUsePage");
			if (termsOfusepageURL.contains(URL)) {

				tcConfig.updateTestReporter("BBMHomepage", "footertermsOfuseLink", Status.PASS,
						"terms Of use page Navigation Successful.The url is : " + termsOfusepageURL);

			}

			else {
				tcConfig.updateTestReporter("BBMHomepage", "footertermsOfuseLink", Status.FAIL,
						"terms Of use page Navigation Unsuccessful");
			}

			driver.close();
			driver.switchTo().window(tabs.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	/*
	 * Method Name: destinationContentPackagesValidation Description:This method
	 * validates destination Content of Packages Date: 31st July Author:Syba Mallick
	 */
	public void destinationContentPackagesValidation() {

		if (verifyObjectDisplayed(advertisingMaterial)) {
			tcConfig.updateTestReporter("BBMHomepage", "destinationContentPackagesValidation", Status.PASS,
					"BBM Home page navigated");

			String pageTitle = driver.getTitle();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Title = testData.get("titleOfPage");
			if (Title.contains(pageTitle)) {
				System.out.println(Title);
				tcConfig.updateTestReporter("BBMHomepage", "destinationContentPackagesValidation", Status.PASS,
						"Destination Content for the Package validation Successful.The destination is" + pageTitle);

			}

			else {
				tcConfig.updateTestReporter("BBMHomepage", "footertermsOfuseLink", Status.FAIL,
						"Destination Content for the Package validation Unsuccessful" + pageTitle);
			}

		}

	}

	/*
	 * Method Name: footerClubWyndhamLink Description:This method validates
	 * Advertising Material in the footer section Date: 31st July Author:Syba
	 * Mallick
	 */

	public void footerClubWyndhamLink() {

		if (verifyObjectDisplayed(clubwyndhamLogoFooter)) {
			tcConfig.updateTestReporter("BBMHomepage", "clubwyndhamLogoFooter", Status.PASS,
					"Navigating To clubWyndham page");

			clickElementBy(clubwyndhamLogoFooter);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String clubwyndhamLogoFooterURL = driver.getCurrentUrl();
			String URL = testData.get("urlCW");

			if (clubwyndhamLogoFooterURL.contains(URL)) {

				tcConfig.updateTestReporter("BBMHomepage", "footerClubWyndhamLink", Status.PASS,
						"Club Wyndham  page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "footerClubWyndhamLink", Status.FAIL,
						"Club Wyndham  page Navigation Unsuccessful");
			}
		}
	}

	/*
	 * Method Name: footerWorldMarkByWyndhamLink Description:This method validates
	 * logo World Mark By Wyndham as a Link in the footer section Date: 31st July
	 * Author:Syba Mallick
	 */
	public void footerWorldMarkByWyndhamLink() {
		waitUntilObjectVisible(driver, worldMarkLogoFooter, 120);
		if (verifyObjectDisplayed(worldMarkLogoFooter)) {
			tcConfig.updateTestReporter("BBMHomepage", "worldMarkLogoFooter", Status.PASS,
					"Navigating To World Mark By Wyndham page");

			clickElementBy(worldMarkLogoFooter);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String worldMarkLogoFooterURL = driver.getCurrentUrl();
			String URL = testData.get("urlWBW");
			if (worldMarkLogoFooterURL.contains(URL)) {

				tcConfig.updateTestReporter("BBMHomepage", "linkWorldMarkLogoFooter", Status.PASS,
						"WorldMarkWyndham  page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "worldMarkLogoFooter", Status.FAIL,
						"WorldMarkWyndham  page Navigation Unsuccessful");
			}

			/* driver.close(); */

		}

	}

	/*
	 * Method Name: footerPrivacyNoteLink Description:This method validates Privacy
	 * Note as a Link in the footer section Date: 31st July Author:Syba Mallick
	 */
	public void footerPrivacyNoteLink() {

		if (verifyObjectDisplayed(linkPrivacyNotice)) {
			tcConfig.updateTestReporter("BBMHomepage", "footerPrivacyNoteLink", Status.PASS,
					"Navigating To privacy Notice page");

			clickElementBy(
					linkPrivacyNotice);/*
										 * waitForSometime(tcConfig.getConfig().get("LowWait")); ArrayList<String> tabs
										 * = new ArrayList<String>(driver.getWindowHandles());
										 * driver.switchTo().window(tabs.get(1));
										 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String privacyNoteLinkURL = driver.getCurrentUrl();
			String URL = testData.get("urlPrivacyNoticePage");
			if (privacyNoteLinkURL.contains(URL)) {

				tcConfig.updateTestReporter("BBMcHomepage", "footerPrivacyNoteLink", Status.PASS,
						"privacy Notice page Navigation Successful.The URL is:" + privacyNoteLinkURL);

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "footerPrivacyNoteLink", Status.FAIL,
						"privacy Notice page Navigation Unsuccessful");
			}

			/*
			 * driver.close(); driver.switchTo().window(tabs.get(0));
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().back();
		}

	}

	/*
	 * Method Name: footerPrivacySettingLink Description:This method validates
	 * Privacy Setting as a Link in the footer section Date: 31st june 2019
	 * Author:Syba Mallick
	 */
	public void footerPrivacySettingLink() {

		if (verifyObjectDisplayed(linkPrivacySetting)) {
			tcConfig.updateTestReporter("BBMHomepage", "footerPrivacySettingLink", Status.PASS,
					"Navigating To privacy Setting page");

			clickElementBy(linkPrivacySetting);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			/*
			 * waitUntilObjectVisible(driver, linkPrivacySettingpage, 120); if
			 * (verifyObjectDisplayed(linkPrivacySettingpage)) {
			 * 
			 * tcConfig.updateTestReporter("BBMHomepage", "footerPrivacySettingLink",
			 * Status.PASS, "privacy Setting page Navigation Successful");
			 * 
			 * } else { tcConfig.updateTestReporter("BBMHomepage",
			 * "footerPrivacySettingLink", Status.FAIL,
			 * "privacy Setting page Navigation Unsuccessful"); }
			 */

			String privacySettingURL = driver.getCurrentUrl();
			String URL = testData.get("urlPrivacySettingPage");
			if (privacySettingURL.contains(URL)) {

				tcConfig.updateTestReporter("BBMcHomepage", "footerPrivacyNoteLink", Status.PASS,
						"privacy Setting page Navigation Successful.The URL is:" + privacySettingURL);

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "footerPrivacyNoteLink", Status.FAIL,
						"privacy Setting page Navigation Unsuccessful");
			}

			driver.close();
			driver.switchTo().window(tabs.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	/*
	 * Method Name: footerAdvertisingMaterial Description:This method validates
	 * Advertising Material in the footer section Date: 28th June Author:Syba
	 * Mallick
	 */
	public void footerAdvertisingMaterial() {

		if (verifyObjectDisplayed(advertisingMaterial)) {
			String advertising = driver.findElement(advertisingMaterial).getText();
			tcConfig.updateTestReporter("BBMHomepage", "footerAdvertisingMaterial", Status.PASS,
					"Navigated to BBM home page and advertising material text is: " + advertising);

		} else {
			tcConfig.updateTestReporter("BBMcHomepage", "footerCopyRight", Status.FAIL,
					"Advertising material Text not available");
		}
	}

	/*
	 * Method Name: footerCopyRight Description:This method validates CopyRight in
	 * the footer section Date: 28th June Author:Syba Mallick
	 */
	public void footerCopyRight() {

		if (verifyObjectDisplayed(linkCopyright)) {
			String copyright = driver.findElement(linkCopyright).getText();
			tcConfig.updateTestReporter("BBMHomepage", "footerCopyRight", Status.PASS,
					"Navigated to BBM login page and copyright text is: " + copyright);

		} else {
			tcConfig.updateTestReporter("BBMcHomepage", "footerCopyRight", Status.FAIL, "Copyright Text not available");
		}
	}

	/*
	 * Method Name: footerLogoWBWVal Description:This method validates logo orldmark
	 * By Wyndham Logo in the footer section Date: 28th June Author:Syba Mallick
	 */
	public void footerLogoWBWVal() {

		waitUntilObjectVisible(driver, worldMarkLogoFooter, 120);
		if (verifyObjectDisplayed(worldMarkLogoFooter)) {
			tcConfig.updateTestReporter("MinvacHomepage", "headerLogos", Status.PASS,
					"Wyndham World Mark is present in footer on home page");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "headerLogos", Status.FAIL,
					"Navigated to BBM Home page and WMWlogo is not present");
		}
	}

	/*
	 * Method Name: footerLogoCWVal Description:This method validates logo Club
	 * wyndham in the footer section Date:24th June Author:Syba Mallick
	 */
	public void footerLogoCWVal() {
		waitUntilObjectVisible(driver, clubwyndhamLogoFooter, 120);
		scrollDownForElementJSBy(clubwyndhamLogoFooter);
		scrollUpByPixel(100);
		if (verifyObjectDisplayed(clubwyndhamLogoFooter)) {
			tcConfig.updateTestReporter("BBMHomepage", "footerLogos", Status.PASS,
					"CLUB WYNDHAM brand logo is present in footer on home page");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "headerLogos", Status.FAIL,
					"Navigated to BBM home page and CLUBWYNDHAMlogo is not present");
		}

	}

	/*
	 * Method Name: headerLogoCW Description:This method validates logo Club wyndham
	 * in the header section Date: 24th June Author:Syba Mallick
	 */
	public void headerLogoCW() {
		waitUntilObjectVisible(driver, clubwyndhamLogoHeader, 120);
		if (verifyObjectDisplayed(clubwyndhamLogoHeader)) {
			tcConfig.updateTestReporter("BBMHomepage", "headerLogos", Status.PASS,
					"CLUB WYNDHAM brand logo is present in header on Home page");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "headerLogos", Status.FAIL,
					"Navigated to BBM login page and CLUBWYNDHAMlogo is not present");
		}

	}

	/*
	 * Method Name: headerLogoWBW Description:This method validates Worldmark By
	 * Wyndham Logo in home page in header section Date: 27th June Author:Syba
	 * Mallick
	 */
	public void headerLogoWBW() {
		waitUntilObjectVisible(driver, worldMarkLogoHeader, 120);

		if (verifyObjectDisplayed(worldMarkLogoHeader)) {
			tcConfig.updateTestReporter("BBMHomepage", "headerLogoWBW", Status.PASS,
					"Wyndham World Mark logo is present in header on Home page");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "headerLogoWBW", Status.FAIL,
					"Navigated to BBM login page and WMWlogo is not present");
		}
	}

	/*
	 * Method Name: linkViewPackageNavigation Description:This method validates the
	 * presence of link View All packages on the destination page clicking and then
	 * booking that package. Date: 28th June 2019 Author:Syba Mallick
	 */

	public void linkViewPackageNavigation() {

		waitUntilObjectVisible(driver, buttonBookNow, 120);
		scrollDownForElementJSBy(buttonBookNow);
		scrollUpByPixel(100);
		if (verifyObjectDisplayed(linkViewPackages)) {
			scrollDownForElementJSBy(linkViewPackages);
			scrollUpByPixel(50);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
					"link View Details is present.");

			List<WebElement> listViewPackages = driver.findElements(linkViewPackages);
			listViewPackages.get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

					" Navigating To ViewPackages page");
			waitUntilElementVisibleBy(driver, linkBacktoVacationPackages, 120);
			if (verifyObjectDisplayed(linkBacktoVacationPackages)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

						"Link BacktoVacationPackages is present in View Packages Details page");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Link BacktoVacationPackages is present in View Packages Details page");
			}
			if (verifyObjectDisplayed(tripsummaryAvailabilty)) {
				// scrollDownForElementJSBy(buttonBookNow);
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.PASS,
						"View Details Page:Search Availabilty Button is present in Book This package card");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.FAIL,
						"View Details Page:Search Availabilty Button is not present in Book This package card");
			}
			/*
			 * if (verifyObjectDisplayed(pkgDetailTripSummrry)) { String viewPkgTripSummary
			 * = driver.findElement(pkgDetailTripSummrry).getText();
			 * tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation",
			 * Status.PASS,
			 * "View Details Page:viewPkgTripSummary is present in Trip Summary card"
			 * +viewPkgTripSummary);
			 * 
			 * } else { tcConfig.updateTestReporter("BBMHomepage",
			 * "linkViewPackageNavigation", Status.FAIL,
			 * "View Details Page:Book Now Button is not present in Trip Summary card"); }
			 * if (verifyObjectDisplayed(PackageDetailsTripSummary)) { String
			 * PackageDetailsTripSummary =
			 * driver.findElement(pkgDetailTripSummrry).getText();
			 * tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation",
			 * Status.PASS,
			 * "View Details Page:viewPkgTripSummary is present in Trip Summary card"
			 * +PackageDetailsTripSummary);
			 * 
			 * } else { tcConfig.updateTestReporter("BBMHomepage",
			 * "linkViewPackageNavigation", Status.FAIL,
			 * "View Details Page:Book Now Button is not present in Trip Summary card"); }
			 */

			clickElementJSWithWait(linkBacktoVacationPackages);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, buttonBookNow, 120);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

					"Link Back to Vacation Packages navigation successful");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
					"Link Back to Vacation Packages navigation unsuccessful");
		}

	}

	public void back() {
		driver.navigate().back();
	}

	/*
	 * Method Name: linkViewPackageNavigation_book Description: This method
	 * validates the presence of Book Now button on Destnation landing page and
	 * click the 1st Book Now button Date: 17th Nov 2019 Author:Ankush
	 */
	public By package_Details = By.xpath("//div[@class='w-full icon-gift mb-1']");
	public By Book_Now_Btn = By.xpath("//span[text()='Book Now' and @class='button -small w-full text-center']");
	public By Calander_Object = By.xpath("//div[@class='calendar-container hasDatepicker']");
	public By incentive_dtls = By.xpath("//div[@class='long-incentive bg-gray3 p-1.5 my-1 block']");

	public void linkViewPackageNavigation_book() {

		waitUntilObjectVisible(driver, buttonBookNow, 120);
		scrollDownForElementJSBy(buttonBookNow);
		scrollUpByPixel(100);
		if (verifyObjectDisplayed(linkViewPackages)) {
			scrollDownForElementJSBy(linkViewPackages);
			scrollUpByPixel(50);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
					"link View Details is present.");

//			List<WebElement> listViewPackages = driver.findElements(linkViewPackages);
//			listViewPackages.get(0).click();
			clickElementJSWithWait(linkViewPackages);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, linkBacktoVacationPackages, 120);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

					" Navigating To ViewPackages page");

			if (verifyObjectDisplayed(package_Details)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Package Incentive details section is present.");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Package Incentive details section is not present.");
			}

			List<WebElement> Incentives = driver.findElements(By.xpath("//a[text()='View Incentive Details']"));
			for (int i = 0; i < Incentives.size(); i = i + 2) {
//					Incentives.get(i).click();
				clickElementJSWithWait(Incentives.get(i));
				waitUntilElementVisibleBy(driver, incentive_dtls, 120);
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"The incentive Information is : " + driver.findElement(incentive_dtls).getText());
			}

//				clickElementJSWithWait(Book_Now_Btn);

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
					"link View Details is not present.");
		}
	}

	public By Trip_Summary_Card = By.xpath("//div[@class='p-1 bg-indigo']//h1[text()='Trip Summary']");
	public By Trip_summary_Pkg_Dtls = By.xpath("//div//h2[text()='PACKAGE DETAILS']");
	public By Trip_Destination = By.xpath("//div//h2[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Includes = By.xpath("//div//p//strong[text()='Includes:']//parent::p");
	public By Trip_Retail = By.xpath("(//div[@class='flex justify-between text-xs'])[1]");
	public By Trip_Saving = By.xpath("(//div[@class='flex justify-between text-xs'])[2]");
	public By Trip_Total = By.xpath("(//div[@class='flex justify-between'])");
	public By Trip_Tax_details = By.xpath("//p[@data-fragment='minivac.paymentDetails.disclaimer']");

	public void Trip_Summary_View_details() {

		waitUntilObjectVisible(driver, buttonBookNow, 120);
		scrollDownForElementJSBy(buttonBookNow);
		scrollUpByPixel(100);
		if (verifyObjectDisplayed(linkViewPackages)) {
			scrollDownForElementJSBy(linkViewPackages);
			scrollUpByPixel(50);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
					"link View Details is present.");

//			List<WebElement> listViewPackages = driver.findElements(linkViewPackages);
//			listViewPackages.get(0).click();
			clickElementJSWithWait(linkViewPackages);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, linkBacktoVacationPackages, 120);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

					" Navigating To ViewPackages page");

			if (verifyObjectDisplayed(package_Details)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Package Incentive details section is present.");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Package Incentive details section is not present.");
			}

			if (verifyObjectDisplayed(Trip_Summary_Card)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary Title with Blue Backgound color is present.");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary Title with Blue Backgound color is not present.");
			}

			if (verifyObjectDisplayed(PackageDetailsTripSummary)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary package details header is present.");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary package details header is not present.");
			}

			if (verifyObjectDisplayed(Trip_Destination)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary destination is present as : " + driver.findElement(Trip_Destination).getText());
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary destination is not present.");
			}

			if (verifyObjectDisplayed(Trip_Includes)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary Includes is present as : " + driver.findElement(Trip_Includes).getText());
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary Includes is not present.");
			}

			if (verifyObjectDisplayed(Trip_Retail)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary Retail is present as : " + driver.findElement(Trip_Retail).getText());
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary Retail is not present.");
			}

			if (verifyObjectDisplayed(Trip_Saving)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary Saving is present as : " + driver.findElement(Trip_Saving).getText());
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary Saving is not present.");
			}

			if (verifyObjectDisplayed(Trip_Total)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary Total is present as : " + driver.findElement(Trip_Total).getText());
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary Total is not present.");
			}

			if (verifyObjectDisplayed(Trip_Tax_details)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
						"Trip summary Tax Detail is present as : " + driver.findElement(Trip_Tax_details).getText());
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"Trip summary Tax Detail is not present.");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(Book_Now_Btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(radiobtnIncentive)) {
				waitUntilObjectVisible(driver, radiobtnIncentive, 120);
				clickElementJSWithWait(radiobtnIncentive);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(btnSelectNcontinue);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			waitUntilObjectVisible(driver, Calander_Object, 120);
			if (verifyObjectDisplayed(Calander_Object)) {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS, "The Calander is present.");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
						"The Calander is not present.");
			}

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
					"link View Details is not present.");
		}
	}

	/*
	 * Method Name: linkBookNowNavigation Description: This method validates the
	 * presence of Book Now button on Destnation landing page and click the 1st Book
	 * Now button Date: 26th May 2019 Author:Syba Mallick
	 */

	public By package_amt = By.xpath("(//p[@class='giant text-black pr-1 border-r border-gray3 mr-1'])[1]");
	public By package_amt2 = By.xpath("(//p[@class='giant text-black pr-1 border-r border-gray3 mr-1'])[2]");

	public void linkBookNowNavigation() {

		waitUntilObjectVisible(driver, buttonBookNow, 120);
		initial_pkg_amt = driver.findElement(package_amt).getText();
		System.out.println(initial_pkg_amt);

		scrollDownForElementJSBy(buttonBookNow);
		scrollUpByPixel(200);
		if (verifyObjectDisplayed(buttonBookNow)) {

//			List<WebElement> listBookNow = driver.findElements(buttonBookNow);
//			listBookNow.get(0).click();
			clickElementJSWithWait(buttonBookNow);
			tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.PASS,
					"Button Book Now is present and clicked Successfully");
			verifyObjectDisplayed(btnSelectNcontinue);

			String selctncontinue = driver.findElement(btnselctncontinue).getAttribute("class");
			if (selctncontinue.contains("disabled")) {
				tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.PASS,

						"Button Select and continue is not activated until radio button is selected");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.FAIL,

						"Button Select and continue is activated before radio button is selected");
			}

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.FAIL,
					"Book Now page navigation unsucessful");
		}

	}

	public void linkBookNowNavigation_Orlando_Promocode() {

		waitUntilObjectVisible(driver, buttonBookNow2, 120);
		initial_pkg_amt = driver.findElement(package_amt2).getText();
		System.out.println(initial_pkg_amt);

		scrollDownForElementJSBy(buttonBookNow2);
		scrollUpByPixel(200);
		if (verifyObjectDisplayed(buttonBookNow2)) {

//			List<WebElement> listBookNow = driver.findElements(buttonBookNow);
//			listBookNow.get(0).click();
			clickElementJSWithWait(buttonBookNow2);
			tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.PASS,
					"Button Book Now is present and clicked Successfully");
			verifyObjectDisplayed(btnSelectNcontinue);

			String selctncontinue = driver.findElement(btnselctncontinue).getAttribute("class");
			if (selctncontinue.contains("disabled")) {
				tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.PASS,

						"Button Select and continue is not activated until radio button is selected");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.FAIL,

						"Button Select and continue is activated before radio button is selected");
			}

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.FAIL,
					"Book Now page navigation unsucessful");
		}

	}

	/*
	 * Method Name: radioButtonIncentiveSelection Description: This method validates
	 * the presence of incentives for particular package and select Incentive of
	 * choice * Date: 26th May 2019 Author:Syba Mallick
	 */

	public By modal_personalization = By.xpath("//span[text()='Personalize Your Package']");

	public void radioButtonIncentiveSelection() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(radiobtnIncentive)) {

			waitUntilObjectVisible(driver, radiobtnIncentive, 120);
			// scrollDownForElementJSBy(buttonBookNow);
			if (verifyObjectDisplayed(modal_personalization)) {
				tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.PASS,
						"Personalization modal is present");
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.PASS,
						"Personalization modal is not present");
			}
			if (verifyObjectDisplayed(btnSelectNcontinue)) {

				clickElementJSWithWait(radiobtnIncentive);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(btnSelectNcontinue);
				tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.PASS,
						"Package incentive of choice is selected Successfully");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.PASS,

						"Package incentive of choice is not selected");
			}
		}

	}

	public By cancel_popup = By.xpath("//div[@id='popupIncentive']//div[@class='popup__close']");
	public By choose_heading_txt = By.xpath("//h2[text()='Choose Your Incentive!']");

	public void radioButtonIncentiveSelectionCancel() {

		waitUntilObjectVisible(driver, radiobtnIncentive, 120);
		if (verifyObjectDisplayed(modal_personalization)) {
			tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.PASS,
					"Personalization modal is present");
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.FAIL,
					"Personalization modal is not present");
		}

		if (verifyObjectDisplayed(choose_heading_txt)) {
			tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.PASS,
					"Choose your incentive! title is present");
		} else {
			tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.PASS,
					"Choose your incentive! title is not present");
		}

		if (verifyObjectDisplayed(btnSelectNcontinue)) {

			clickElementJSWithWait(radiobtnIncentive);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(cancel_popup);
			tcConfig.updateTestReporter("BBMHomepage", "PackageIncentive Selection", Status.PASS,
					"Package incentive is closed Successfully");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.FAIL,
					"Book Now page navigation unsucessful");
		}

	}

	/*
	 * Method Name: labelContactNoValidation Description:This method validates
	 * contact no label display on home page Date: 28th July 2019 Author:Syba
	 * Mallick
	 */
	public void labelContactNoValidation() {

		waitUntilObjectVisible(driver, labelContactNo, 120);

		if (verifyObjectDisplayed(labelContactNo)) {

			tcConfig.updateTestReporter("BBMHomepage", "labelContactNoValidation", Status.PASS,
					"Contact No is present in Destination Package Page");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "labelContactNoValidation", Status.FAIL,
					"Contact No is not present in Destination Page");
		}

		// WebElement phoneNo = driver.findElement(By.xpath("//p[@class='text-xxs
		// tabletp:text-xs text-white leading-tight']"));

		/*
		 * String strphoneNo = lblPhoneNo.getAttribute("onsubmit");
		 * 
		 * if (strphoneNo.contains("null")) { tcConfig.updateTestReporter("BBMHomepage",
		 * "labelContactNoValidation", Status.PASS,
		 * "Contact No is not displayed as link" + strphoneNo);
		 * 
		 * 
		 * 
		 * } else { tcConfig.updateTestReporter("BrowseAllJobsPage",
		 * "labelContactNoValidation", Status.FAIL, "Contact No is displayed as link" +
		 * strphoneNo); }
		 * 
		 * 
		 * 
		 */
	}

	public void expandViewPackageHighlights() {
		// TODO Auto-generated method stub

	}

	public void clickHamBurger() {
		// TODO Auto-generated method stub
		
	}

}
