package bbm.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class GuestInfoPage_IOS extends GuestInfoPage {

	public static final Logger log = Logger.getLogger(GuestInfoPage_IOS.class);

	public GuestInfoPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void pageGuestInformationMandatoryFieldValidation()

	{
		scrollDownForElementJSBy(By.xpath("//h1[@class='heading6 mb-half']"));
		/* driver.navigate().refresh(); */

		if (verifyObjectDisplayed(headingGuesInformation)) {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"Guest Information page navigation sucessful");
			verifyObjectDisplayed(mandatoryastricks);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"In Guest Information page mandatory astricks is present");

		}

		if (verifyObjectDisplayed(guest1stname)) {

			// driver.findElement(guest1stname).click();
			/*
			 * String inputText = testData.get("GuestFirstName"); WebElement myElement =
			 * driver.findElement(By.id("guest-first-name")); String js =
			 * "arguments[0].setAttribute('value','"+inputText+"')"; ((JavascriptExecutor)
			 * driver).executeScript(js, myElement);
			 */

			/*
			 * ((RemoteWebDriver)
			 * driver).findElementByXPath("(//input[@id=\"guest-first-name\"])[1]")
			 * .sendKeys(" " + testData.get("GuestFirstName"));
			 */

			sendKeys2((RemoteWebDriver) driver, "First Name", testData.get("GuestFirstName"));
			// fieldDataEnter(guest1stname, testData.get("GuestFirstName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page first name fiels is present and value is : "
							+ testData.get("GuestFirstName"));

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page first name field is not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(guestlastName)) {

			// driver.findElement(guestlastName).click();
			/*
			 * String inputText = testData.get("GuestLastName"); WebElement myElement =
			 * driver.findElement(By.id("guest-last-name")); String js =
			 * "arguments[0].setAttribute('value','"+inputText+"')"; ((JavascriptExecutor)
			 * driver).executeScript(js, myElement);
			 */
			/*
			 * ((RemoteWebDriver)
			 * driver).findElementByXPath("(//input[@id=\"guest-last-name\"])[1]")
			 * .sendKeys(testData.get("GuestLastName"));
			 */

			sendKeys2((RemoteWebDriver) driver, "Last Name", testData.get("GuestLastName"));
			// fieldDataEnter(guestlastName, testData.get("GuestLastName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page last name field is  present and value is : "
							+ testData.get("GuestLastName"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page last name field is not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(email)) {
			// driver.findElement(email).click();

			/*
			 * String inputText = testData.get("Email"); WebElement myElement =
			 * driver.findElement(By.id("guest-email")); String js =
			 * "arguments[0].setAttribute('value','"+inputText+"')"; ((JavascriptExecutor)
			 * driver).executeScript(js, myElement);
			 */

			/*
			 * ((RemoteWebDriver)
			 * driver).findElementByXPath("(//input[@id=\"guest-email\"])[1]")
			 * .sendKeys(testData.get("Email"));
			 */

			sendKeys2((RemoteWebDriver) driver, "Email", testData.get("Email"));
			// fieldDataEnter(email, testData.get("Email"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page email field is present and value is : " + testData.get("Email"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page email field is not present");

		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(phone)) {
			// driver.findElement(phone).click();
			/*
			 * ((RemoteWebDriver)
			 * driver).findElementByXPath("(//input[@id=\"guest-phone\"])[1]")
			 * .sendKeys(testData.get("PhoneNo"));
			 */

			sendKeys2((RemoteWebDriver) driver, "Phone Number", testData.get("PhoneNo"));
			// fieldDataEnter(phone, testData.get("PhoneNo"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page phone no. field is present and value is : " + testData.get("PhoneNo"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page phone no. field is not present");

		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(address1)) {
			// driver.findElement(address1).click();
			/*
			 * ((RemoteWebDriver)
			 * driver).findElementByXPath("(//input[@id=\"guest-address1\"])[1]")
			 * .sendKeys(testData.get("GuestAddress1"));
			 */

			sendKeys2((RemoteWebDriver) driver, "Street Address", testData.get("GuestAddress1"));
			// fieldDataEnter(address1, testData.get("GuestAddress1"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page address1 field is present and value is : "
							+ testData.get("GuestAddress1"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page address1 field is not present");
		}
		/*
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); if
		 * (verifyObjectDisplayed(address2)) { driver.findElement(address2).click();
		 * sendKeys((RemoteWebDriver) driver, "Apt",testData.get("GuestAddress2"));
		 * //fieldDataEnter(address2, testData.get("GuestAddress2"));
		 * tcConfig.updateTestReporter("GuestInformationPage",
		 * "pageGuestInformationMandatoryFieldValidation", Status.PASS,
		 * "In Guest Information page address1 field is present and value is : " +
		 * testData.get("GuestAddress2")); } else {
		 * tcConfig.updateTestReporter("GuestInformationPage",
		 * "pageGuestInformationMandatoryFieldValidation", Status.FAIL,
		 * "In Guest Information page address2 field is not present");
		 * 
		 * }
		 */

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(city)) {
			// driver.findElement(city).click();
			/*
			 * ((RemoteWebDriver)
			 * driver).findElementByXPath("(//input[@id=\"guest-city\"])[1]")
			 * .sendKeys(testData.get("City"));
			 */

			sendKeys2((RemoteWebDriver) driver, "City", testData.get("City"));
			// fieldDataEnter(city, testData.get("City"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page City field is present and value is : " + testData.get("City"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page City field is not present");

		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(zip)) {
			// driver.findElement(zip).click();
			/*
			 * ((RemoteWebDriver)
			 * driver).findElementByXPath("(//input[@id=\"guest-zip\"])[1]")
			 * .sendKeys(testData.get("ZipCode"));
			 */

			sendKeys2((RemoteWebDriver) driver, "Zip Code", testData.get("ZipCode"));
			// fieldDataEnter(zip, testData.get("ZipCode"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page Postal Code field is present and value is : " + testData.get("ZipCode"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page Postal Code field is not present");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(drpdownState)) {
			Select drpSbCategory = new Select(driver.findElement(drpdownState));
			drpSbCategory.selectByVisibleText(testData.get("DropdownState"));
			// selectByVisibletext(testData.get("DropdownState"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page dropdown State is present and value is : "
							+ testData.get("DropdownState"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page dropdown State is not present");

		}

		scrollDownByPixel(150);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(SMS_Notification_txt)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(SMS_Notification_txt).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(mobile_chkbox).click();
			// clickElementJSWithWait(mobile_chkbox);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// clickElementJSWithWait(SMS_Notification_txt);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "SMS Notification text is displayed");
			waitUntilObjectVisible(driver, mobile_sms_number, 120);
			driver.findElement(mobile_sms_number).click();
			if (verifyObjectDisplayed(mobile_sms_number)) {
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is present");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(mobile_sms_number).click();
				/*
				 * ((RemoteWebDriver)
				 * driver).findElementByXPath("(//input[@id=\"guest-sms\"])[1]")
				 * .sendKeys(testData.get("Mobile_Num_SMS"));
				 */

				sendKeys2((RemoteWebDriver) driver, "Mobile Phone Number", testData.get("Mobile_Num_SMS"));

				// fieldDataEnter(mobile_sms_number,
				// testData.get("Mobile_Num_SMS"));

			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is not present");
			}

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "SMS Notification text is not displayed");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(webCheckbox);
		driver.findElement(By.xpath("//div[@id='guest-requirements']")).click();
		scrollScopeLimiter(By.xpath("//div[@id='guest-requirements']"),
				By.xpath("//div[@id='guest-requirements']//p//b[contains(.,'West')]"));
		/*
		 * scrollDownForElementJSBy(webCheckbox); scrollUpByPixel(150); if
		 * (verifyObjectDisplayed(termsCondition)) { clickElementBy(termsCondition);
		 * 
		 * driver.switchTo().activeElement().sendKeys(Keys.END);
		 * 
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); scrollDownByPixel(200);
		 * }
		 */
		verifyObjectDisplayed(webCheckbox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(webCheckbox);
		clickElementBy(buttonSubmit);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void freeCancellationPolicyDisplayValidation() {

		/*
		 * try { waitUntilObjectVisible(driver, linkLearnMoreCancellationPolicy, 120); }
		 * catch (Exception e) { // TODO Auto-generated catch block e.printStackTrace();
		 * }
		 */
		// scrollDownByPixel(1200);
		scrollDownForElementJSBy(testCancel);
		/*
		 * scrollDownForElementJSWb(testCancel); List<WebElement>
		 * testlist=driver.findElements(testCancel);
		 * testlist.get(testlist.size()-1).click();
		 */
		clickIOSElement("Learn More", 1, testCancel);

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: "
							+ contentCPolicy);
//			clickElementJSWithWait(linkLearnMoreCancellationPolicy);

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(frameCancellationPolicy)) {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.PASS,
						"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: "
								+ contentCPolicy);
			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.FAIL, "Cancellation Policy frame is not present");
			}
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}
}
