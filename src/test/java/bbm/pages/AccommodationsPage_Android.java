package bbm.pages;

import java.awt.AWTException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class AccommodationsPage_Android extends AccommodationsPage {

	public static final Logger log = Logger.getLogger(AccommodationsPage_Android.class);
	BeginSearchPage_Android BeginSearchPage=new BeginSearchPage_Android(tcConfig);
	public AccommodationsPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		buttonBookNow = By.xpath(/*"//a[@class='button mb-1 w-full' and contains(.,'Book Now')]"*/
				"//a[@class='button mb-1 w-full' and contains(.,'Stay Here')]");
		linkViewPropertyDetails = By
				.xpath("//a[@class='text-xs link-arrow block ' and contains(.,'View Details')]");
		labelProgressBar = By.xpath("//p[@class='subtitle3 text-white']");
		linkLearnMoreCancellationPolicy = By.xpath("//div[@id='hotels-list']//a[@data-fragment='global.cancellation.LearnMore']");
		contentCancellationPolicy = By.xpath("//div[@id='hotels-list']//p[contains(.,'Cancel within')]");
	}
	@Override
	public void linkViewPropertyDetailsPageNavigation() throws AWTException {

		waitUntilObjectVisible(driver, headingAvailableAccommodation, 120);

				
		if (verifyObjectDisplayed(headingAvailableAccommodation)) {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
					"Available Accommodation  page navigated");

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(labelContactNo)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Contact No is present in Accommodation Page");

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
						"Contact No not getting displayed in Accommodation Page");
			}

			waitUntilObjectVisible(driver, linkViewPropertyDetails, 120);
			if (verifyObjectDisplayed(linkViewPropertyDetails)) {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Options to view property details is available");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.FAIL, "Options to view property details is not available");
			}

			if (verifyObjectDisplayed(buttonBookNow)) {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Book Now button is available");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.FAIL, "Book Now button is not available");
			}

			List<WebElement> linkViewPropertyList = driver.findElements(linkViewPropertyDetails);
			linkViewPropertyList.get(0).click();

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			///
			if (verifyObjectDisplayed(linkViewMoreAminities)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Link View More Aminities is present in Accommodation Page");
				scrollDownForElementJSBy(linkViewMoreAminities); //
				scrollUpByPixel(100);
				clickElementJSWithWait(linkViewMoreAminities);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, aminitiesDetails, 120);
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Aminity Details are displayed in Accommodation Page");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
						"Aminity Details not getting displayed in Accommodation Page");
			}

			if (verifyObjectDisplayed(pymntCards)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Payment Cards option present in Accommodation Page");

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
						"Payment Cards option not getting displayed in Accommodation Page");
			}

			/*if (verifyObjectDisplayed(linkOpeninMaps)) {

				if (verifyObjectDisplayed(buttonZoom)) {
					scrollDownForElementJSBy(buttonZoom);
					scrollUpByPixel(250);
					clickElementBy(buttonZoom);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(buttonZoomOut);
					tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
							Status.PASS, "Map zoom in and Zoom Out is present and working in Accommodation Page");

				} else {
					tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
							Status.FAIL, "Map zoom in and Zoom Out not getting displayed in Accommodation Page");
				}

				clickElementJSWithWait(linkOpeninMaps);

				tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.PASS,
						"Link Open in Map is present in Accommodation Page");

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String mapURL = driver.getCurrentUrl();
				String URL = testData.get("urlMap");

				if (mapURL.contains(URL)) {

					tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.PASS,
							"Clicking on maps is navigating to Google maps");
					driver.navigate().back();

				} else {
					tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.FAIL,
							"Clicking on maps is not navigating to Google maps");
				}

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.FAIL,
						"Link Open in Map is not getting displayed in Accommodation Page");
			}*/

			waitUntilObjectVisible(driver, buttonSelectAccommodationtemp, 120);

			if (verifyObjectDisplayed(buttonSelectAccommodationtemp)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Property Details page Navigation Successful"); //
				getElementInView(buttonSelectAccommodationtemp);
				// driver.findElement(buttonSelectAccommodationtemp).click();
				// clickElementBy(buttonSelectAccommodationtemp);
//				BeginSearchPage.Email_form_Validation(); //comment in case the email form not present in stage
				clickElementJSWithWait(buttonSelectAccommodationtemp);
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Button Select Accommodation clicked successfully");
			}

			else

			{
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.FAIL, "Button Select Accommodation not clickable");
			}

		} else

		{
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
					"Available Accommodation  page navigation unsucessful");
		}
	}
	
	public void labelProgressBarValidation() {
		waitUntilObjectVisible(driver, labelProgressBar, 120);

		if (verifyObjectDisplayed(labelProgressBar)) {
			

			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"Progress Bar label is displayed");
			
					
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
						"Progress Bar label not present");
			}
	}
	
	public void clickViewDetailsLnk() {
		waitUntilObjectVisible(driver, linkViewPropertyDetails, 120);

		if (verifyObjectDisplayed(linkViewPropertyDetails)) {
			
			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"View Details Link is displayed");
			
			driver.findElements(linkViewPropertyDetails).get(0).click();
					
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
						"Progress Bar label not present");
			}
	}
	
public By backToAccommodationsLnk=By.xpath("//a[@class='link-back font-semibold' and contains(.,'Back to All Accommodations')]");
	
	public void clickbackToAccommodationsLnk() {
		waitUntilObjectVisible(driver, backToAccommodationsLnk, 120);

		if (verifyObjectDisplayed(backToAccommodationsLnk)) {
			
			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"Back to Accommodations Link is displayed");
			
			driver.findElement(backToAccommodationsLnk).click();
					
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
						"Back to Accommodations Link not present");
			}
	}
	
	@Override
	public void freeCancellationPolicyDisplayValidation() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		waitUntilElementVisibleBy(driver, linkLearnMoreCancellationPolicy, 120);

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: " +contentCPolicy);
			clickElementJSWithWait(linkLearnMoreCancellationPolicy);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			verifyObjectDisplayed(frameCancellationPolicy);
			tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: " +contentCPolicy);
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
						"Cancellation Policy frame is not present");
			}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}

}
