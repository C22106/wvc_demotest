package bbm.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class GuestInfoPage_Android extends GuestInfoPage {

	public static final Logger log = Logger.getLogger(GuestInfoPage_Android.class);

	public GuestInfoPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void pageGuestInformationMandatoryFieldValidation()

	{

		if (verifyObjectDisplayed(headingGuesInformation)) {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"Guest Information page navigation sucessful");
			verifyObjectDisplayed(mandatoryastricks);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"In Guest Information page mandatory astricks is present");

		}

		if (verifyObjectDisplayed(guest1stname)) {

			fieldDataEnter(guest1stname, testData.get("GuestFirstName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page first name fiels is present and value is : "
							+ testData.get("GuestFirstName"));

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page first name field is not present");
		}

		if (verifyObjectDisplayed(guestlastName)) {

			fieldDataEnter(guestlastName, testData.get("GuestLastName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page last name field is  present and value is : "
							+ testData.get("GuestLastName"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page last name field is not present");
		}

		if (verifyObjectDisplayed(address1)) {

			fieldDataEnter(address1, testData.get("GuestAddress1"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page address1 field is present and value is : "
							+ testData.get("GuestAddress1"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page address1 field is not present");
		}

		if (verifyObjectDisplayed(address2)) {

			fieldDataEnter(address2, testData.get("GuestAddress2"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page address1 field is present and value is : "
							+ testData.get("GuestAddress2"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page address2 field is not present");

		}

		if (verifyObjectDisplayed(email)) {

			fieldDataEnter(email, testData.get("Email"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page email field is present and value is : " + testData.get("Email"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page email field is not present");

		}
		if (verifyObjectDisplayed(phone)) {

			fieldDataEnter(phone, testData.get("PhoneNo"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page phone no. field is present and value is : " + testData.get("PhoneNo"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page phone no. field is not present");

		}
		if (verifyObjectDisplayed(city)) {

			fieldDataEnter(city, testData.get("City"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page City field is present and value is : " + testData.get("City"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page City field is not present");

		}
		if (verifyObjectDisplayed(zip)) {

			fieldDataEnter(zip, testData.get("ZipCode"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page Postal Code field is present and value is : " + testData.get("ZipCode"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page Postal Code field is not present");

		}

		if (verifyObjectDisplayed(drpdownState)) {
			Select drpSbCategory = new Select(driver.findElement(drpdownState));
			drpSbCategory.selectByVisibleText(testData.get("DropdownState"));
			// selectByVisibletext(testData.get("DropdownState"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page dropdown State is present and value is : "
							+ testData.get("DropdownState"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page dropdown State is not present");

		}

		if (verifyObjectDisplayed(SMS_Notification_txt)) {

			clickElementJSWithWait(SMS_Notification_txt);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "SMS Notification text is displayed");
			waitUntilObjectVisible(driver, mobile_sms_number, 120);
			if (verifyObjectDisplayed(mobile_sms_number)) {
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is present");

				fieldDataEnter(mobile_sms_number, testData.get("Mobile_Num_SMS"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(mobile_chkbox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is not present");
			}

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "SMS Notification text is not displayed");

		}

		scrollDownForElementJSBy(webCheckbox);
		scrollScopeLimiter(By.xpath("//div[@id='guest-requirements']"),
				By.xpath("//div[@id='guest-requirements']//p//b[contains(.,'West')]"));
		/*
		 * scrollDownForElementJSBy(webCheckbox); scrollUpByPixel(150); if
		 * (verifyObjectDisplayed(termsCondition)) { clickElementBy(termsCondition);
		 * 
		 * driver.switchTo().activeElement().sendKeys(Keys.END);
		 * 
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); scrollDownByPixel(200);
		 * }
		 */
		verifyObjectDisplayed(webCheckbox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(webCheckbox);
		clickElementBy(buttonSubmit);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void freeCancellationPolicyDisplayValidation() {

		scrollDownForElementJSBy(testCancel);

		List<WebElement> testlist = driver.findElements(testCancel);
		// testlist.get(testlist.size()-1).click();
		clickElementJSWithWait(testlist.get(testlist.size() - 1));

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: "
							+ contentCPolicy);
//			clickElementJSWithWait(linkLearnMoreCancellationPolicy);

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(frameCancellationPolicy)) {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.PASS,
						"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: "
								+ contentCPolicy);
			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation",
						Status.FAIL, "Cancellation Policy frame is not present");
			}
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}

}
