package bbm.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PaymentPage extends BBMBasePage {

	public static final Logger log = Logger.getLogger(PaymentPage.class);

	public PaymentPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By linkBacktoAllAcommodation = By.xpath("//a[@class='link-back font-semibold']");
	protected By inputCreditCardNo = By.xpath("//input[@id='cc-number']");
	protected By CreditCardNoErr = By.xpath("//p[@id='cc-error']");
	protected By expiryDateErr = By.xpath("//p[@id='cc-dates-error']");
	protected By dropdwnExpirationdateMonth = By.xpath("//select[@id='cc-exp-month']");
	protected By drpdownExpirationYear = By.xpath("//select[@id='cc-exp-year']");
	protected By headingPaymentInformation = By.xpath("//h1[text()='4. Payment Details']");
	protected By buttonReviewandBook = By.xpath(/* "//button[text()='Book Now']" */
			"//button[contains(text(),'Book Now')]");
	protected By labelContactNo = By.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By contentReservationandCancellationPolicy = By
			.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By labelTimesharePresentation = By.xpath("//h2[text()='TIMESHARE SALES PRESENTATION']");
	protected By reservationCancellationPolicy = By
			.xpath("//h2[contains(.,'Reservation Details & Cancellation Policy')]");

	protected By headerPaymentMethod = By.xpath("//h2[text()='PAYMENT METHOD']");
	protected By imageMasterCard = By.xpath("//div[@class='icon-card icon-visa mr-half']");
	protected By imageVisaCard = By.xpath("//div[@class='icon-card icon-mastercard mr-half']");
	protected By imageAmericanExpressCard = By.xpath("//div[@class='icon-card icon-american-express mr-half']");
	protected By imageDiscoverCard = By.xpath("//div[@class='icon-card icon-discover mr-half']");
	protected By imageJcbCard = By.xpath("//div[@class='icon-card icon-Jcb mr-half']");
	protected By popupContinueBooking = By.xpath("//a[@id='popupContinue']");
	protected By labelProgressBar = By.xpath("//div[@class='steps-desktop hidden tabletl:flex text-white']");
	protected By contentRetailValue = By.xpath("//p[contains(.,'Retail Value')]");
	protected By contentYourSavings = By.xpath("//p[contains(.,'Your Savings')]");
	protected By contentTotalForPackage = By.xpath(/* "//p[contains(.,'Total for Package')]" */
			"//p[contains(.,'Total Package Price')]");
	// p[@class='font-heading text-xl font-medium mb-half']
	protected By textSaving = By.xpath("//p[contains(.,'up to $')]");
	protected By contentAccommodationDetails = By.xpath("//h2[contains(.,'Accommodations')]");
	protected By linkFreeCancellationLernMore = By.xpath("//a[contains(.,'Learn More')]");
	protected By linkViewPropertyDetails = By.xpath("//a[contains(.,'View Property Details')]");
	protected By accommodation = By.xpath("//p[@class='font-heading text-xl font-medium mb-half']");
	protected By paymentErrorMessage = By.xpath("//div//h3[contains(text(),'Unfortunately')]");
	protected By paymentMandatoryMessage = By.xpath("//*[@id='paymentDetails']/p[1]/text()");

	public By testCancel = By.xpath("//div[contains(@class,'cancellation-box')]//a[contains(text(),'Learn More')]");
	protected By linkLearnMoreCancellationPolicy = By
			.xpath("//section[@id='trip-summary']//div/a[contains(.,'Learn')]");
	protected By contentCancellationPolicy = By.xpath("//p[contains(.,'Cancel within')]");
	protected By frameCancellationPolicy = By.xpath("//div[@id='popupCancellation']");
	protected By frameCancellationPolicycClose = By.xpath("//*[@id='popupCancellation']/div");
	protected By mandatoryastricks = By.xpath("//p[contains(.,'*Indicates required field')]");

	// p[contains(.,'*Indicates required field')]

	/*
	 * Method Name: PaymentSoftScoreOffValidation Description:This method validates
	 * the Extra Holidays Images when score is Off Date: April 2020 Author:Priya Das
	 */

	protected By extraHolidaysImg = By.xpath("//p/img[contains(@src,'extra-holidays')]");

	public void PaymentSoftScoreOffValidation() {

		waitUntilObjectVisible(driver, paymentErrorMessage, 120);

		if (verifyObjectDisplayed(paymentErrorMessage)) {
			String PaymentError = driver.findElement(paymentErrorMessage).getText();

			tcConfig.updateTestReporter("PaymentDetailsPage", "PaymentErrorMesageValidation", Status.PASS,
					"Payment error message is displayed as: " + PaymentError + "When Soft Score is ON");

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
					"Payment error message is not displayed");
		}

		scrollDownForElementJSBy(extraHolidaysImg);
		if (verifyObjectDisplayed(extraHolidaysImg)) {

			tcConfig.updateTestReporter("PaymentDetailsPage", "ExtraHolidaysImage", Status.PASS,
					"Extra Holidays Image is displayed");

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
					"Extra Holidays Image is not displayed");
		}

	}

	public void PaymentErrorMesageValidation() {

		waitUntilObjectVisible(driver, mandatoryastricks, 120);
		verifyObjectDisplayed(paymentMandatoryMessage);
		tcConfig.updateTestReporter("PaymentDetailsPage", "PaymentErrorMesageValidation", Status.PASS,
				"In Payment page mandatory astrics and Payment message is available ");
		if (verifyObjectDisplayed(paymentErrorMessage)) {
			String PaymentError = driver.findElement(paymentErrorMessage).getText();

			tcConfig.updateTestReporter("PaymentDetailsPage", "PaymentErrorMesageValidation", Status.PASS,
					"Payment error message is displayed as: " + PaymentError + "When Soft Score is ON");

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
					"Payment error message is not displayed");
		}
	}

	public By PackageDetailsTripSummary = By.xpath("//h2[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Summary_Card = By.xpath("//div[@class='p-1 bg-indigo']//h1[text()='Trip Summary']");
	public By Trip_summary_Pkg_Dtls = By.xpath("//div//h2[text()='PACKAGE DETAILS']");
	public By Trip_Destination = By.xpath("//div//h2[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Includes = By.xpath("//div//p//strong[text()='Includes:']//parent::p");
	public By Trip_Retail = By.xpath("(//div[@class='flex justify-between text-xs'])[1]");
	public By Trip_Saving = By.xpath("(//div[@class='flex justify-between text-xs'])[2]");
	public By Trip_Total = By.xpath("(//div[@class='flex justify-between'])");
	public By Trip_Tax_details = By.xpath("//p[@data-fragment='minivac.paymentDetails.disclaimer']");
	public By package_Details = By.xpath("//div//p[text()='PACKAGE INCENTIVE DETAILS']");
	public By Book_Now_Btn = By.xpath("//span[text()='Book Now' and @class='button -small w-full text-center']");
	public By Calander_Object = By.xpath("//div[@class='calendar-container hasDatepicker']");
	public By Trip_dates = By.xpath("//p[@class='text-sm mb-half']");
	public By Trip_for_details = By.xpath("//p[@class='text-sm mb-1']");
	public By Trip_Modify_Link = By.xpath("//a[text()='Modify Search']");
	public By Trip_Book_Begin_Search = By.xpath("(//a[text()='Book Now'])[1]");
	public By Trip_Guest_page = By.xpath("//h1[contains(text(),'Guest Information')]");
	public By Trip_Accomodatios = By.xpath("//h2[text()='Accommodations']");
	public By Trip_resort_Name = By.xpath("//p[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Verify_Property = By.xpath("//a[text()='View Property Details']");

	public void Trip_Summary_View_details_Payment() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, Trip_Summary_Card, 120);

		if (verifyObjectDisplayed(Trip_Summary_Card)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary Title with Blue Backgound color is present.");
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary Title with Blue Backgound color is not present.");
		}

		if (verifyObjectDisplayed(PackageDetailsTripSummary)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary package details header is present.");
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary package details header is not present.");
		}

		if (verifyObjectDisplayed(Trip_Destination)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary destination is present as : " + driver.findElement(Trip_Destination).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary destination is not present.");
		}

		if (verifyObjectDisplayed(Trip_Includes)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary Includes is present as : " + driver.findElement(Trip_Includes).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary Includes is not present.");
		}

		if (verifyObjectDisplayed(Trip_dates)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary Dates are present as : " + driver.findElement(Trip_dates).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary Dates are not present.");
		}

		if (verifyObjectDisplayed(Trip_for_details)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip For details are present as : " + driver.findElement(Trip_for_details).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip For details are not present.");
		}

		if (verifyObjectDisplayed(Trip_Accomodatios)) {
			tcConfig.updateTestReporter("Guest Info page", "linkViewPackages", Status.PASS,
					"Trip accomodations header is present");
			if (verifyObjectDisplayed(Trip_resort_Name)) {
				tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
						"Trip Resort Name is " + driver.findElement(Trip_resort_Name).getText());
			} else {
				tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
						"Trip Resort Name is not present.");
			}

			if (verifyObjectDisplayed(Trip_Verify_Property)) {
				tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
						"View property Details link is present.");
			} else {
				tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
						"View property Details link is not present.");
			}

		}

		if (verifyObjectDisplayed(Trip_Retail)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary Retail is present as : " + driver.findElement(Trip_Retail).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary Retail is not present.");
		}

		if (verifyObjectDisplayed(Trip_Saving)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary Saving is present as : " + driver.findElement(Trip_Saving).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary Saving is not present.");
		}

		if (verifyObjectDisplayed(Trip_Total)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary Total is present as : " + driver.findElement(Trip_Total).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary Total is not present.");
		}

		if (verifyObjectDisplayed(Trip_Tax_details)) {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.PASS,
					"Trip summary Tax Detail is present as : " + driver.findElement(Trip_Tax_details).getText());
		} else {
			tcConfig.updateTestReporter("PaymentPage", "linkViewPackages", Status.FAIL,
					"Trip summary Tax Detail is not present.");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	/*
	 *
	 * Method Name: freeCancellationPolicyDisplayValidation Description:This method
	 * validates free Cancellation Policy Display throughout the booking flow Date:
	 * 25th Oct 2019 Author:Syba Mallick
	 */
	public void freeCancellationPolicyDisplayValidation() {

		waitUntilObjectVisible(driver, linkLearnMoreCancellationPolicy, 120);

		List<WebElement> testlist = driver.findElements(testCancel);
		testlist.get(testlist.size() - 1).click();

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: "
							+ contentCPolicy);
//			clickElementJSWithWait(linkLearnMoreCancellationPolicy);

			tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(frameCancellationPolicy)) {
				tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation",
						Status.PASS,
						"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: "
								+ contentCPolicy);
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation",
						Status.FAIL, "Cancellation Policy frame is not present");
			}
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("PaymentDetailsPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}

	/*
	 * Method Name:enterPaymentDetails Description:this method is used to verify
	 * card details and fill card details and click on continue button Date:26th May
	 * 2019 Author:Syba Mallick
	 */
	public By ccradio = By.xpath("//input[@id='paymentMethodCreditCard']");
	public By ppradio = By.xpath("//input[@id='paymentMethodPaypal']");

	public void clickpaypalradio() {

		waitUntilObjectVisible(driver, ppradio, 120);
		if (verifyObjectDisplayed(ppradio)) {

			tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.PASS,
					"Paypal radio button is present in Payment Details Page");

			clickElementBy(ppradio);

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
					"Paypal radio button not getting displayed in Payment Details page");
		}

	}

	public void clickccradio() {

		waitUntilObjectVisible(driver, ccradio, 120);
		if (verifyObjectDisplayed(ccradio)) {

			tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.PASS,
					"credit card radio button is present in Payment Details Page");

			clickElementBy(ccradio);

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
					"credit card radio button not getting displayed in Payment Details page");
		}

	}

	public By paypalbutton = By.xpath(/* "//div//div//a//img[@alt='Check out with PayPal']" */
			"//div[@id='paymentChoices']//a//img[@alt='Check out with PayPal']");
	public By emailPaypal = By.xpath("//input[@id='email']");
	public By nextbtn = By.xpath("//button[@id='btnNext']");
	public By passwordPaypal = By.xpath("//input[@id='password']");
	protected By loginPaypal = By.xpath("//button[@id='btnLogin']");
	public By submitPaypal = By.xpath("//button[@id='payment-submit-btn']");
	protected By payPalBox = By.xpath("//div[contains(text(),'PayPal Account Verified')]");

	public void paymentdetailspaypal() {

		waitUntilObjectVisible(driver, paypalbutton, 120);

		if (verifyObjectDisplayed(paypalbutton)) {

			tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.PASS,
					"Paypal button is present in Payment Details Page");

			clickElementBy(paypalbutton);

			waitUntilObjectVisible(driver, emailPaypal, 120);
			if (verifyObjectDisplayed(emailPaypal)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "PaypalDetails", Status.PASS,
						"Email field present on paypal portal");

				fieldDataEnter(emailPaypal, testData.get("Paypal_email"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
						"Email field not present on paypal portal");
			}

			waitUntilObjectVisible(driver, nextbtn, 120);
			if (verifyObjectDisplayed(nextbtn)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "PaypalDetails", Status.PASS,
						"Next button present on paypal portal");

				clickElementBy(nextbtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
						"Next button not present on paypal portal");
			}

			waitUntilObjectVisible(driver, passwordPaypal, 120);
			if (verifyObjectDisplayed(passwordPaypal)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "PaypalDetails", Status.PASS,
						"Password field present on paypal portal");

				fieldDataEnter(passwordPaypal, testData.get("Paypal_Pass"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
						"Password field not present on paypal portal");
			}

			getElementInView(loginPaypal);
			if (verifyObjectDisplayed(loginPaypal)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "PaypalDetails", Status.PASS,
						"Login button present on paypal portal");

				clickElementJSWithWait(loginPaypal);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
						"Login button not present on paypal portal");
			}

			getElementInView(submitPaypal);
			if (verifyObjectDisplayed(submitPaypal)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "PaypalDetails", Status.PASS,
						"Continue Payment button present on paypal portal");

				clickElementJSWithWait(submitPaypal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
						"Continue Payment button not present on paypal portal");
			}
			waitUntilObjectVisible(driver, payPalBox, 120);
//			driver.switchTo().window(wins.get(0));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			scrollDownForElementJSBy(buttonReviewandBook);
			clickElementJSWithWait(buttonReviewandBook);
			tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
					"In Payment Details page Book Now Button is clicked successfully");
			waitForSometime(tcConfig.getConfig().get("LongWait"));

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
					"Paypal button not getting displayed in Payment Details page");
		}

	}
	protected By confirmBoking = By.xpath("//span[contains(.,'Thank you')]");

	public void enterPaymentDetails() {

		waitUntilObjectVisible(driver, inputCreditCardNo, 120);
		if (verifyObjectDisplayed(headingPaymentInformation)) {

			tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
					"Payment Details header displayed,Payments details page navigated sucessfully ");

			if (verifyObjectDisplayed(labelContactNo)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.PASS,
						"Contact No is present in Payment Details Page");

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "labelContactNoValidation", Status.FAIL,
						"Contact No not getting displayed in Payment Details page");
			}
			if (verifyObjectDisplayed(contentReservationandCancellationPolicy)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "contentReservationandCancellationPolicy",
						Status.PASS, "Reservation and Cancellation Policy is present in Payment Details Page");

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "contentReservationandCancellationPolicy",
						Status.FAIL, "Reservation and Cancellation Policy is not present in Payment Details page");
			}

			if (verifyObjectDisplayed(inputCreditCardNo)) {
				fieldDataEnter(inputCreditCardNo, testData.get("CreditCardNo"));
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
						"In Payment details page input Credit card field is present and details are filled from data sheet.");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment details page input Credit card field is not present");

			}

			if (verifyObjectDisplayed(dropdwnExpirationdateMonth)) {
				Select drpCategoryMonth = new Select(driver.findElement(dropdwnExpirationdateMonth));
				drpCategoryMonth.selectByValue(testData.get("ExpiryMonth"));
				// selectByVisibletext(testData.get("ExpiryYear"));
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
						"In Payment Details page dropdown Expiry Month is present and data is set.");
			} else {

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page dropdown Expiry Month is not present");

			}
			if (verifyObjectDisplayed(drpdownExpirationYear)) {
				Select drpCategoryYear = new Select(driver.findElement(drpdownExpirationYear));
				drpCategoryYear.selectByValue(testData.get("ExpiryYear"));

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
						"In Payment Details page dropdown Expiry Year is present and data is set.");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page dropdown Expiry Year is not present");

			}
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			scrollDownForElementJSBy(buttonReviewandBook);
			clickElementJSWithWait(buttonReviewandBook);
			tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
					"In Payment Details page Book Now Button is clicked successfully");
			pageCheck();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, confirmBoking, 180);
		}

		else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "launchenterPaymentDetailsAppBBM", Status.FAIL,
					"Payment Details header not displayed,Payments details page navigated fail");
		}

	}
	/*
	 * Method Name: labelProgressBarValidation Description:This method validates the
	 * label Progress Bar in the Begin Search Page Date: 18th Sept 2019 Author:Syba
	 * Mallick
	 */

	public void labelProgressBarValidation() {

		waitUntilObjectVisible(driver, labelProgressBar, 120);

		if (verifyObjectDisplayed(labelProgressBar)) {
			String progress = driver.findElement(labelProgressBar).getText();

			tcConfig.updateTestReporter("PaymentDetailsPage", "labelProgressBarValidation", Status.PASS,
					"Progress Bar label is displayed in Payment Page with the content as: " + progress);

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "labelProgressBarValidation", Status.FAIL,
					"Progress Bar label is displayed in Payment Page ");
		}

	}
	/*
	 * Method Name: cardTripSummaryPaymentPageValidation Description:This method
	 * validates the label Progress Bar in the Begin Search Page Date: 14th October
	 * 2019 Author:Syba Mallick
	 */

	public void cardTripSummaryPaymentPageValidation() {

		waitUntilObjectVisible(driver, linkViewPropertyDetails, 120);

		if (verifyObjectDisplayed(linkViewPropertyDetails)) {
			String selectedAccommodation = driver.findElement(accommodation).getText();
			tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.PASS,
					"Accommodation Name is present in Trip summarry card and accommodation name is : "
							+ selectedAccommodation);

			if (verifyObjectDisplayed(contentYourSavings)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.PASS,
						"Trip Summary card is present in Payment Page with proper Your Savings Upto content");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.FAIL,
						"Trip Summary card is not present in Payment Page with proper Your Savings Upto content");

			}

			if (verifyObjectDisplayed(contentRetailValue)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.PASS,
						"Trip Summary card is present in Payment Page with proper Retail Value content");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.FAIL,
						"Trip Summary card is not present in Payment Page with proper Retail Value content");

			}

			if (verifyObjectDisplayed(contentTotalForPackage)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.PASS,
						"Trip Summary card is present in Payment Page with proper Total For Package content");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.FAIL,
						"Trip Summary card is not present in Payment Page with proper Total For Package content");

			}

			if (verifyObjectDisplayed(contentAccommodationDetails)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.PASS,
						"Trip Summary card is  present in Payment Page with proper Accommodation content");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.FAIL,
						"Trip Summary card is not present in Payment Page with proper Accommodation content");

			}

			if (verifyObjectDisplayed(textSaving)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.PASS,
						"Trip Summary card is  present in Payment Page with proper Savings Upto content");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.FAIL,
						"Trip Summary card is not present in Payment Page with proper Savings Upto content");

			}

			/*
			 * if (verifyObjectDisplayed(textRetail) ) {
			 * 
			 * tcConfig.updateTestReporter("PaymentDetailsPage",
			 * "cardTripSummaryPaymentPageValidation", Status.PASS,
			 * "Trip Summary card is  present in Payment Page with proper Retail Value text content"
			 * + getElementText(reservationCancellationPolicy)); } else {
			 * tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails",
			 * Status.FAIL,
			 * "Trip Summary card is not present in Payment Page with proper Retail text content"
			 * );
			 * 
			 * }
			 */
			clickElementJSWithWait(linkViewPropertyDetails);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifyObjectDisplayed(linkBacktoAllAcommodation);
			tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.PASS,
					"Link View Property Details is displayed in Payment Page and clicking on the link navigates to Property Details Page.");

		}

		else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "cardTripSummaryPaymentPageValidation", Status.FAIL,
					"Trip Summary card is not present in Payment Page with proper content");
		}

	}
	/*
	 * Method Name: enterPaymentDetailsErrorvalidation Description:This method is
	 * used to validate error message of payment details page Date: 26th May
	 * Author:Syba Mallick
	 */

	public void enterPaymentDetailsErrorvalidation() {

		try {
			waitUntilObjectVisible(driver, buttonReviewandBook, 120);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (verifyObjectDisplayed(buttonReviewandBook)) {

			tcConfig.updateTestReporter("BBMHomepage", "enterPaymentDetails", Status.PASS,
					"Payment Details header displayed,Payments details page navigated sucessfully ");

			if (verifyObjectDisplayed(reservationCancellationPolicy)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetailsErrorvalidation", Status.PASS,
						"In Payment Details page Reservation and Cancellation Policy is validated"
								+ getElementText(reservationCancellationPolicy));
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page Reservation and Cancellation Policy is not validated");

			}
			if (verifyObjectDisplayed(labelTimesharePresentation)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetailsErrorvalidation", Status.PASS,
						"In Payment Details page Timeshare Presentation label is validated "
								+ getElementText(labelTimesharePresentation));
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page TimesharePresentation label is not validated");

			}
			scrollDownForElementJSBy(buttonReviewandBook);
			clickElementJSWithWait(buttonReviewandBook);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			/*
			 * if (verifyObjectDisplayed(CreditCardNoErr)) {
			 * 
			 * tcConfig.updateTestReporter("PaymentDetailsPage",
			 * "enterPaymentDetailsErrorvalidation", Status.PASS,
			 * "In Payment Details page input Credit card field is mandatory and error message is validated"
			 * + getElementText(CreditCardNoErr)); } else {
			 * tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails",
			 * Status.FAIL,
			 * "In Payment Details page  Credit card field error message is not validated");
			 * 
			 * } // p[@id='cc-dates-error'] if (verifyObjectDisplayed(expiryDateErr)) {
			 * 
			 * tcConfig.updateTestReporter("PaymentDetailsPage",
			 * "enterPaymentDetailsErrorvalidation", Status.PASS,
			 * "In Payment Details page input Expiry date dropdown is mandatory and error message is validated"
			 * + getElementText(expiryDateErr)); } else {
			 * tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails",
			 * Status.FAIL,
			 * "In Payment Details page  Credit card input Expiry date dropdown error message is not validated"
			 * );
			 * 
			 * }
			 */
			// System.out.println(driver.findElement(buttonReviewandBook).getAttribute("disabled"));
			if (driver.findElement(buttonReviewandBook).getAttribute("disabled").equals("true")) {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetailsErrorvalidation", Status.PASS,
						"Book now button is disabled");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetailsErrorvalidation", Status.FAIL,
						"Book now button is not disabled");
			}

		}

		else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
					"Payment Details header not displayed,Payments details page navigated fail");
		}

	}

	/*
	 * Method Name: CardDetailsValidation Description:This method is used to
	 * validate Card details validation on the payment details page Date: 26th May
	 * Author:Syba Mallick
	 */
	public void CardDetailsValidation() {
		waitUntilObjectVisible(driver, headerPaymentMethod, 120);
		if (verifyObjectDisplayed(headerPaymentMethod)) {

			tcConfig.updateTestReporter("PaymentDetailsPage", "CardDetailsValidation", Status.PASS,
					"Payment Details header Payment Method is present");

			if (verifyObjectDisplayed(imageVisaCard)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "CardDetailsValidation", Status.PASS,
						"In Payment Details page, Visa Card is present as payment option");

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "CardDetailsValidation", Status.FAIL,
						"In Payment Details page, Visa Card is not present as payment option");
			}
			if (verifyObjectDisplayed(imageMasterCard)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "CardDetailsValidation", Status.PASS,
						"n Payment Details page, Master Card is present as payment option");

			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "CardDetailsValidation", Status.FAIL,
						"In Payment Details page, Master Card is not present as payment option");
			}

			if (verifyObjectDisplayed(imageAmericanExpressCard)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "CardDetailsValidation", Status.PASS,
						"In Payment Details page, American Express Card is  present as payment option");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page, American Express Card is not present as payment option");

			}

			if (verifyObjectDisplayed(imageDiscoverCard)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "CardDetailsValidation", Status.PASS,
						"In Payment Details page, Discover Card is  present as payment option");
			} else {
				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page, Discover Card is not present as payment option");

			}

			if (verifyObjectDisplayed(imageJcbCard)) {

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.FAIL,
						"In Payment Details page, JCB Card is not present as payment option");
			} else {

				tcConfig.updateTestReporter("PaymentDetailsPage", "enterPaymentDetails", Status.PASS,
						"In Payment Details page,JCB Card is not present as payment option");

			}

		} else {
			tcConfig.updateTestReporter("PaymentDetailsPage", "launchenterPaymentDetailsAppBBM", Status.FAIL,
					"Payment Details header Payment Method is not present");
		}

	}

	/*
	 * * Method Name: popupModal Description:This method validates Pop UP modal in
	 * Begin search page Date: 21st Sept 2019 Author:Syba Mallick
	 */

	public void popupModal() {

		waitUntilObjectVisible(driver, popupContinueBooking, 120);
		if (verifyObjectDisplayed(popupContinueBooking)) {
			tcConfig.updateTestReporter("PaymentPage", "popupModal", Status.PASS,
					"Pop Up Are you sure you want to leave? is present in BeginSearch Page");
			clickElementJSWithWait(popupContinueBooking);
		} else {
			tcConfig.updateTestReporter("PaymentPage", "labelContactNoValidation", Status.FAIL,
					"Pop Up Are you sure you want to leave? is not present in BeginSearch Page");
		}
	}
}
