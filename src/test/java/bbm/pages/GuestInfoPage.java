package bbm.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class GuestInfoPage extends BBMBasePage {

	public static final Logger log = Logger.getLogger(GuestInfoPage.class);

	public GuestInfoPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String postPackageAmt;

	protected By guest1stname = By.xpath("//input[@id='guest-first-name']");
	protected By guestlastName = By.xpath("//input[@id='guest-last-name']");
	protected By email = By.xpath("//input[@id='guest-email']");
	protected By phone = By.xpath("//input[@id='guest-phone']");
	protected By address1 = By.xpath("//input[@for='guest-address1']");
	protected By address2 = By.xpath("//input[@id='guest-address2']");
	protected By city = By.xpath("//input[@id='guest-city']");
	protected By drpdownState = By.xpath("//select[@id='guest-state']");
	protected By zip = By.xpath("//input[@id='guest-zip']");
	protected By eligiblityRequirement = By.xpath("//div[@class='must-read text-overflow mb-1.5 w-full']");
	protected By termsCondition = By.xpath("//div[@id='guest-requirements']//p[1]");
	 protected By webCheckbox =By.xpath("//label[@for='guest-agree']");
	// div//input[@id='guest-agree']/following-sibling::label
	// By.xpath("//input[@id='guest-agree']/parent::div");
	protected By buttonSubmit = By.xpath("//button[text()='Continue to Payment']");
	protected By headingGuesInformation = By.xpath("//h1[@class='heading6 mb-half']");
	protected By firstNameErrorMsg = By.xpath("//p[@id='first-name-error']");
	protected By lastNameErrorMsg = By.xpath("//p[@id='last-name-error']");
	protected By phoneError = By.xpath("//p[@id='phone-error']");
	protected By addressError = By.xpath("//p[@id='address1-error']");
	protected By zipError = By.xpath("//p[@id='zip-error']");
	protected By cityError = By.xpath("//p[@id='city-error']");
	protected By emailError = By.xpath("//p[@id='email-error']");
	protected By labelContactNo= By.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By linkViewPropertyDetails= By.xpath("//a[@class='text-sm']");
	protected By popupContinueBooking = By.xpath("//a[@id='popupContinue']");
	protected By labelNeedHelp = By.xpath("//p//strong[text()='Need Help Booking?']");
	protected By labelProgressBar = By.xpath("//div[@class='steps-desktop hidden tabletl:flex text-white']");
	protected By mandatoryastricks = By.xpath("//p[contains(.,'*Indicates required field')]");
	
//	protected By linkLearnMoreCancellationPolicy =	By.xpath("(//div/a[contains(text(),'Learn More')])[2]");
	protected By linkLearnMoreCancellationPolicy =	By.xpath("//section[@id='trip-summary']//div/a[contains(.,'Learn')]");
		//	By.xpath("//section[@id='trip-summary']//a[@data-fragment='global.cancellation.LearnMore']");
	public By testCancel=By.xpath("//div[contains(@class,'cancellation-box')]//a[contains(text(),'Learn More')]");
	
	protected By contentCancellationPolicy = By.xpath("//p[contains(.,'Cancel within')]");	
	protected By frameCancellationPolicy = By.xpath("//div[@id='popupCancellation']");
	protected By frameCancellationPolicycClose = By.xpath("//*[@id='popupCancellation']/div");
	
	/*
	 * Method Name: freeCancellationPolicyDisplayValidation
	 * Description:This method validates free Cancellation Policy Display  throughout the booking flow
	 * Date: 25th Oct 2019
	 * Author:Syba Mallick
	 */

	public void freeCancellationPolicyDisplayValidation() {

		try {
			waitUntilObjectVisible(driver, linkLearnMoreCancellationPolicy, 120);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<WebElement> testlist=driver.findElements(testCancel);
		testlist.get(testlist.size()-1).click();
		
		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();
			
			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: " +contentCPolicy);
//			clickElementJSWithWait(linkLearnMoreCancellationPolicy);
			
			

			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(frameCancellationPolicy)){
			tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: " +contentCPolicy);
			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
						"Cancellation Policy frame is not present");
			}
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("GuestInformationPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}
	/* * Method Name: popupModal
	 * Description:This method validates Pop UP modal in Accommodation search page
	 * Date: 21st Sept 2019
	 * Author:Syba Mallick*/
	
	public void popupModal() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(verifyObjectDisplayed(popupContinueBooking))
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Actions action =new Actions(driver);
			action.moveToElement(driver.findElement(labelNeedHelp)).build().perform();
			waitUntilObjectVisible(driver, popupContinueBooking, 120);
			if (verifyObjectDisplayed(popupContinueBooking)) {
				tcConfig.updateTestReporter("GuestInformation Page", "popupModal", Status.PASS,
						"Pop Up Are you sure you want to leave? is present in GuestInformationPage");
				clickElementJSWithWait(popupContinueBooking);
			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "labelContactNoValidation", Status.FAIL,
						"Pop Up Are you sure you want to leave? is not present in GuestInformationPage");
				}
			}
	}
	
	public By PackageDetailsTripSummary = By.xpath("//h2[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Summary_Card=By.xpath("//div[@class='p-1 bg-indigo']//h1[text()='Trip Summary']");
	public By Trip_summary_Pkg_Dtls=By.xpath("//div//h2[text()='PACKAGE DETAILS']");
	public By Trip_Destination=By.xpath("//div//h2[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Includes=By.xpath("//div//p//strong[text()='Includes:']//parent::p");
	public By Trip_Retail=By.xpath("(//div[@class='flex justify-between text-xs'])[1]");
	public By Trip_Saving=By.xpath("(//div[@class='flex justify-between text-xs'])[2]");
	public By Trip_Total=By.xpath("(//div[@class='flex justify-between'])");
	public By Trip_Tax_details=By.xpath("//p[@data-fragment='minivac.paymentDetails.disclaimer']");
	public By package_Details=By.xpath("//div//p[text()='PACKAGE INCENTIVE DETAILS']");
	public By Book_Now_Btn=By.xpath("//span[text()='Book Now' and @class='button -small w-full text-center']");
	public By Calander_Object=By.xpath("//div[@class='calendar-container hasDatepicker']");
	public By Trip_dates=By.xpath("//p[@class='text-sm mb-half']");
	public By Trip_for_details=By.xpath("//p[@class='text-sm mb-1']");
	public By Trip_Modify_Link=By.xpath("//a[text()='Modify Search']");
	public By Trip_Book_Begin_Search=By.xpath("(//a[text()='Book Now'])[1]");
	public By Trip_Guest_page=By.xpath("//h1[contains(text(),'Guest Information')]");
	public By Trip_Accomodatios=By.xpath("//h2[text()='Accommodations']");
	public By Trip_resort_Name=By.xpath("//p[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Verify_Property=By.xpath("//a[text()='View Property Details']");
	
	
	public void Trip_Summary_View_details_GuestInfo() {

		waitUntilObjectVisible(driver, Trip_Summary_Card, 120);
				
				if(verifyObjectDisplayed(Trip_Summary_Card)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary Title with Blue Backgound color is present.");
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary Title with Blue Backgound color is not present.");
				}
				
				if(verifyObjectDisplayed(PackageDetailsTripSummary)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary package details header is present.");
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary package details header is not present.");
				}
				
				if(verifyObjectDisplayed(Trip_Destination)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary destination is present as : "+driver.findElement(Trip_Destination).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary destination is not present.");
				}
				
				if(verifyObjectDisplayed(Trip_Includes)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary Includes is present as : "+driver.findElement(Trip_Includes).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary Includes is not present.");
				}
				
				if(verifyObjectDisplayed(Trip_dates)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary Dates are present as : "+driver.findElement(Trip_dates).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary Dates are not present.");
				}
				

				if(verifyObjectDisplayed(Trip_for_details)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip For details are present as : "+driver.findElement(Trip_for_details).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip For details are not present.");
				}
				
				if(verifyObjectDisplayed(Trip_Accomodatios)){
					tcConfig.updateTestReporter("Guest Info page", "linkViewPackages", Status.PASS,
							"Trip accomodations header is present");
					if(verifyObjectDisplayed(Trip_resort_Name)){
						tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
								"Trip Resort Name is "+driver.findElement(Trip_resort_Name).getText());
					}else{
						tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
								"Trip Resort Name is not present.");
					}
					
					if(verifyObjectDisplayed(Trip_Verify_Property)){
						tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
								"View property Details link is present.");
					}else{
						tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
								"View property Details link is not present.");
					}
					
				}
				
				
				
				if(verifyObjectDisplayed(Trip_Retail)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary Retail is present as : "+driver.findElement(Trip_Retail).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary Retail is not present.");
				}
				
				if(verifyObjectDisplayed(Trip_Saving)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary Saving is present as : "+driver.findElement(Trip_Saving).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary Saving is not present.");
				}
				
				if(verifyObjectDisplayed(Trip_Total)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary Total is present as : "+driver.findElement(Trip_Total).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary Total is not present.");
				}
				

				if(verifyObjectDisplayed(Trip_Tax_details)){
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.PASS,
							"Trip summary Tax Detail is present as : "+driver.findElement(Trip_Tax_details).getText());
				}else{
					tcConfig.updateTestReporter("GuestInfoPage", "linkViewPackages", Status.FAIL,
							"Trip summary Tax Detail is not present.");
				}
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				
				
			
			
	}
	
	

	/*
	 * Method Name: 
	 * Description:
	 * Date: 
	 * Author:
	 */
	public void pageGuestInformationNavigation() {
		waitUntilObjectVisible(driver, headingGuesInformation, 120);
		if (verifyObjectDisplayed(headingGuesInformation)) {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"Guest Information page navigation sucessful");
		
		
		if (verifyObjectDisplayed(labelContactNo)) {			
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"Contact No is present in Guest Information Page");	

		} else
		{
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.FAIL,
					"Contact No not getting displayed in Guest Information page");

		}
		}
		else

		{
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.FAIL,
					"Guest Information page navigation unsucessful");
		}

	}

	
	/*
	 * Method Name: 
	 * Description:
	 * Date: 
	 * Author:
	 */
	public By SMS_Notification_txt=By.xpath("//a[text()='SMS Notification Preferences (Optional)']");
	public By mobile_sms_number=By.xpath("//input[@id='guest-sms']");
	public By mobile_chkbox=By.xpath("//label[@for='reservation-confirmation']");
	
	
	
	public void pageGuestInformationMandatoryFieldValidation()

	{

		if (verifyObjectDisplayed(headingGuesInformation)) {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"Guest Information page navigation sucessful");
			verifyObjectDisplayed(mandatoryastricks);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"In Guest Information page mandatory astricks is present");
			
		}

	
		if (verifyObjectDisplayed(guest1stname)) {
			
			fieldDataEnter(guest1stname, testData.get("GuestFirstName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page first name fiels is present and value is : "
							+ testData.get("GuestFirstName"));

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page first name field is not present");
		}

		if (verifyObjectDisplayed(guestlastName)) {
			
			fieldDataEnter(guestlastName, testData.get("GuestLastName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page last name field is  present and value is : "
							+ testData.get("GuestLastName"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page last name field is not present");
		}

		if (verifyObjectDisplayed(address1)) {
			
			fieldDataEnter(address1, testData.get("GuestAddress1"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page address1 field is present and value is : "
							+ testData.get("GuestAddress1"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page address1 field is not present");
		}

		if (verifyObjectDisplayed(address2)) {
			
			fieldDataEnter(address2, testData.get("GuestAddress2"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page address1 field is present and value is : "
							+ testData.get("GuestAddress2"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page address2 field is not present");

		}

		if (verifyObjectDisplayed(email)) {
			
			fieldDataEnter(email, testData.get("Email"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page email field is present and value is : " + testData.get("Email"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page email field is not present");

		}
		if (verifyObjectDisplayed(phone)) {
		
			fieldDataEnter(phone, testData.get("PhoneNo"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page phone no. field is present and value is : " + testData.get("PhoneNo"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page phone no. field is not present");

		}
		if (verifyObjectDisplayed(city)) {
			
			fieldDataEnter(city, testData.get("City"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page City field is present and value is : " + testData.get("City"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page City field is not present");

		}
		if (verifyObjectDisplayed(zip)) {
		
			fieldDataEnter(zip, testData.get("ZipCode"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page Postal Code field is present and value is : " + testData.get("ZipCode"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page Postal Code field is not present");

		}
		
		if (verifyObjectDisplayed(drpdownState)) {
			Select drpSbCategory = new Select(driver.findElement(drpdownState));
			drpSbCategory.selectByVisibleText(testData.get("DropdownState"));
			// selectByVisibletext(testData.get("DropdownState"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page dropdown State is present and value is : "
							+ testData.get("DropdownState"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page dropdown State is not present");

		}
		
		
		if (verifyObjectDisplayed(SMS_Notification_txt)) {
			
			clickElementJSWithWait(SMS_Notification_txt);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"SMS Notification text is displayed");
			waitUntilObjectVisible(driver, mobile_sms_number, 120);
			if(verifyObjectDisplayed(mobile_sms_number)){
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is present");
				
				fieldDataEnter(mobile_sms_number, testData.get("Mobile_Num_SMS"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(mobile_chkbox);
		        waitForSometime(tcConfig.getConfig().get("MedWait"));
				
			}else{
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is not present");
			}
			
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "SMS Notification text is not displayed");

		}
		

		scrollDownForElementJSBy(webCheckbox);
		scrollUpByPixel(150);
		if (verifyObjectDisplayed(termsCondition)) {
			clickElementBy(termsCondition);
			
			driver.switchTo().activeElement().sendKeys(Keys.END);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollDownByPixel(200);
		}
		verifyObjectDisplayed(webCheckbox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(webCheckbox);
		clickElementBy(buttonSubmit);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

		
	/*
	 * Method Name: 
	 * Description:
	 * Date: 
	 * Author:
	 */
	public void pageGuestInformationMandatoryFieldErrorValidation()

	{
		waitUntilElementVisibleBy(driver, buttonSubmit, 120);
		scrollDownForElementJSBy(buttonSubmit);
		scrollUpByPixel(100);
		//verifyObjectDisplayed(webCheckbox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//clickElementBy(webCheckbox);
		clickElementBy(buttonSubmit);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(headingGuesInformation)) {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"Guest Information page navigation sucessful");
		}

		if (verifyObjectDisplayed(firstNameErrorMsg)) {

			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.PASS,
					"In Guest Information page first name fiels is mandatory and we are getting error message on leaving fiels as blank as :"
							+ getElementText(firstNameErrorMsg));

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page first name field is not present");
		}

		if (verifyObjectDisplayed(lastNameErrorMsg)) {

			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.PASS,
					"In Guest Information page last name field is mandatory and we are getting error message on leaving fiels as blank as :"
							+ getElementText(lastNameErrorMsg));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page last name field is not present");
		}

		if (verifyObjectDisplayed(addressError)) {

			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.PASS,
					"In Guest Information page address1 field is mandatory and we are getting error message on leaving fiels as blank as :"
							+ getElementText(addressError));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.FAIL, "In Guest Information page address1 field is not present");
		}

		if (verifyObjectDisplayed(emailError)) {

			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.PASS,
					"In Guest Information page email field is present and we are getting error message on leaving fiels as blank as :"
							+ getElementText(emailError));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.FAIL, "In Guest Information page email field is not present");

		}
		if (verifyObjectDisplayed(phoneError)) {

			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.PASS,
					"In Guest Information page phone no. field is present and we are getting error message on leaving fiels as blank as :"
							+ getElementText(phoneError));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.FAIL, "In Guest Information page phone no. field is not present");

		}
		if (verifyObjectDisplayed(cityError)) {

			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.PASS,
					"In Guest Information page City field is present and and we are getting error message on leaving fiels as blank as :"
							+ getElementText(cityError));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.FAIL, "In Guest Information page City field is not present");

		}
		if (verifyObjectDisplayed(zipError)) {

			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.PASS,
					"In Guest Information page Postal Code field is present and we are getting error message on leaving fiels as blank as :"
							+ getElementText(zipError));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldErrorValidation",
					Status.FAIL, "In Guest Information page Postal Code field is not present");

		}

	}
	/*
	 * Method Name: labelProgressBarValidation
	 * Description:This method validates the label Progress Bar in the Begin Search Page 
	 * Date: 18th Sept 2019
	 * Author:Syba Mallick
	 */

	public void labelProgressBarValidation() {

		/*waitUntilObjectVisible(driver, labelProgressBar, 120);

		if (verifyObjectDisplayed(labelProgressBar)) {
			String progress = driver.findElement(labelProgressBar).getText();

			tcConfig.updateTestReporter("GuestInfoPage", "labelProgressBarValidation", Status.PASS,
					"Progress Bar label is displayed in Begin search Page with the content as: " +progress);
			
					
			} else {
				tcConfig.updateTestReporter("GuestInfoPage", "labelProgressBarValidation", Status.FAIL,
						"Cookies not present");
			}*/

	}
	
	/*
	 * Method Name: validatePromocodeErrorMsg
	 * Description:This method validates the Promo codes Error Messages by giving different types of Promo codes 
	 * Date: May 2020
	 * Author:Priya Das
	 */
	
	protected By txtPromoCode = By.xpath("//input[contains(@id,'promo-code')]");
	protected By applyBtn = By.xpath("//button[contains(text(),'Apply')]");
	protected By promoExpiryError = By.xpath("//p[contains(text(),'Promo Code Has Expired')]");
	protected By promoInvalidError = By.xpath("//p[contains(text(),'Invalid Promo Code')]");
	protected By promoInvalidPkgError = By.xpath("//p[contains(text(),'Promo code is not valid for this package')]");
	
	public void validatePromocodeErrorMsg()
	{
		ArrayList<String> promocodes = new ArrayList<String>();
		promocodes.add(testData.get("InvalidPromoCode"));
		promocodes.add(testData.get("ExpiredPromoCode"));
		promocodes.add(testData.get("NotValidPCPkg"));
		try{
			waitUntilElementVisibleBy(driver, txtPromoCode, 120);
			for(int i=0;i<3;i++)
			{
				driver.findElement(txtPromoCode).clear();
				driver.findElement(txtPromoCode).sendKeys(promocodes.get(i));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(applyBtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if(promocodes.get(i).equals(testData.get("ExpiredPromoCode")))
				{
					if(verifyObjectDisplayed(promoExpiryError))
					{
						tcConfig.updateTestReporter("GuestInformationPage", "promocodeErrorValidation",
								Status.PASS,
								"User is getting the error message after giving the Expired Promo Code"
										+ getElementText(promoExpiryError));
					}else{
						tcConfig.updateTestReporter("GuestInformationPage", "promocodeErrorValidation",
								Status.FAIL,
								"User is not getting the error message after giving the Expired Promo Code");
					}
				}else if(promocodes.get(i).equals(testData.get("InvalidPromoCode"))){
					if(verifyObjectDisplayed(promoInvalidError))
					{
						tcConfig.updateTestReporter("GuestInformationPage", "promocodeErrorValidation",
								Status.PASS,
								"User is getting the error message after giving the Invalid Promo Code"
										+ getElementText(promoInvalidError));
					}else{
						tcConfig.updateTestReporter("GuestInformationPage", "promocodeErrorValidation",
								Status.FAIL,
								"User is not getting the error message after giving the Invalid Promo Code");
					}
				}else if(promocodes.get(i).equals(testData.get("NotValidPCPkg")))
				{
					if(verifyObjectDisplayed(promoInvalidPkgError))
					{
						tcConfig.updateTestReporter("GuestInformationPage", "promocodeErrorValidation",
								Status.PASS,
								"User is getting the error message after giving the Invalid for Package Promo Code"
										+ getElementText(promoInvalidPkgError));
					}else{
						tcConfig.updateTestReporter("GuestInformationPage", "promocodeErrorValidation",
								Status.FAIL,
								"User is not getting the error message after giving the Invalid for Package Promo Code");
					}
				}
			}
		}catch(Exception e)
		{
			tcConfig.updateTestReporterWithoutScreenshot("GuestInformationPage", "promocodeErrorValidation",
					Status.FAIL,
					"Failed in Method"+e.getMessage());
		}
		
	}
	
	/*
	 * Method Name: validPromoCodeforPackage
	 * Description:This method validates the Promo codes applied on a Package 
	 * Date: May 2020
	 * Author:Priya Das
	 */
	
	protected By prePackageAmt = By.xpath(/*"(//p[text()='Total for Package']//following::p)[1]"*/
			"(//p[text()='Total Package Price']//following::p)[1]");
	protected By validPromoMessage = By.xpath("//span[contains(text(),'Promo code has been applied')]");

	public void validPromoCodeforPackage() {
		String strPrePackageAmt = "";

		try {
			strPrePackageAmt = driver.findElement(prePackageAmt).getText();
			waitUntilElementVisibleBy(driver, txtPromoCode, 120);
			driver.findElement(txtPromoCode).clear();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(txtPromoCode).sendKeys(testData.get("Promocodes"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(applyBtn).click();
			waitUntilElementVisibleBy(driver, validPromoMessage, 120);
			if (verifyElementDisplayed(driver.findElement(validPromoMessage))) {
				postPackageAmt = driver.findElement(prePackageAmt).getText();
				if (!postPackageAmt.equals(strPrePackageAmt)) {
					tcConfig.updateTestReporter("GuestInformationPage", "validPromoCodeforPackage", Status.PASS,
							"Promo Code has been Applied Successfully");
				} else {
					tcConfig.updateTestReporter("GuestInformationPage", "validPromoCodeforPackage", Status.FAIL,
							"Promo Code has not been Applied");
				}

			}else{
				tcConfig.updateTestReporter("GuestInformationPage", "validPromoCodeforPackage", Status.FAIL,
						"Promo Code message is not displayed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporterWithoutScreenshot("GuestInformationPage", "validPromoCodeforPackage", Status.FAIL,
					"Failed in Method"+e.getMessage());
		}
	}
	
	public void pageGuestInformationMandatoryFieldValidation_PromoCode()

	{

		if (verifyObjectDisplayed(headingGuesInformation)) {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"Guest Information page navigation sucessful");
			verifyObjectDisplayed(mandatoryastricks);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationValidation", Status.PASS,
					"In Guest Information page mandatory astricks is present");
			
		}

	
		if (verifyObjectDisplayed(guest1stname)) {
			
			fieldDataEnter(guest1stname, testData.get("GuestFirstName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page first name fiels is present and value is : "
							+ testData.get("GuestFirstName"));

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page first name field is not present");
		}

		if (verifyObjectDisplayed(guestlastName)) {
			
			fieldDataEnter(guestlastName, testData.get("GuestLastName"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page last name field is  present and value is : "
							+ testData.get("GuestLastName"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page last name field is not present");
		}

		if (verifyObjectDisplayed(address1)) {
			
			fieldDataEnter(address1, testData.get("GuestAddress1"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page address1 field is present and value is : "
							+ testData.get("GuestAddress1"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page address1 field is not present");
		}

		if (verifyObjectDisplayed(address2)) {
			
			fieldDataEnter(address2, testData.get("GuestAddress2"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page address1 field is present and value is : "
							+ testData.get("GuestAddress2"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page address2 field is not present");

		}

		if (verifyObjectDisplayed(email)) {
			
			fieldDataEnter(email, testData.get("Email"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page email field is present and value is : " + testData.get("Email"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page email field is not present");

		}
		if (verifyObjectDisplayed(phone)) {
		
			fieldDataEnter(phone, testData.get("PhoneNo"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page phone no. field is present and value is : " + testData.get("PhoneNo"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page phone no. field is not present");

		}
		if (verifyObjectDisplayed(city)) {
			
			fieldDataEnter(city, testData.get("City"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page City field is present and value is : " + testData.get("City"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page City field is not present");

		}
		if (verifyObjectDisplayed(zip)) {
		
			fieldDataEnter(zip, testData.get("ZipCode"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS,
					"In Guest Information page Postal Code field is present and value is : " + testData.get("ZipCode"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page Postal Code field is not present");

		}
		
		if (verifyObjectDisplayed(drpdownState)) {
			Select drpSbCategory = new Select(driver.findElement(drpdownState));
			drpSbCategory.selectByVisibleText(testData.get("DropdownState"));
			// selectByVisibletext(testData.get("DropdownState"));
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "In Guest Information page dropdown State is present and value is : "
							+ testData.get("DropdownState"));
		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "In Guest Information page dropdown State is not present");

		}
		
		
		
		
	}
	
	public void submitGuestInformation() {

		if (verifyObjectDisplayed(SMS_Notification_txt)) {

			clickElementJSWithWait(SMS_Notification_txt);
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.PASS, "SMS Notification text is displayed");
			waitUntilObjectVisible(driver, mobile_sms_number, 120);
			if (verifyObjectDisplayed(mobile_sms_number)) {
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is present");

				fieldDataEnter(mobile_sms_number, testData.get("Mobile_Num_SMS"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(mobile_chkbox);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
						Status.PASS, " Mobile number Field is not present");
			}

		} else {
			tcConfig.updateTestReporter("GuestInformationPage", "pageGuestInformationMandatoryFieldValidation",
					Status.FAIL, "SMS Notification text is not displayed");

		}
		scrollDownForElementJSBy(webCheckbox);
		scrollUpByPixel(150);
		if (verifyObjectDisplayed(termsCondition)) {
			clickElementBy(termsCondition);

			driver.switchTo().activeElement().sendKeys(Keys.END);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollDownByPixel(200);
		}
		verifyObjectDisplayed(webCheckbox);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(webCheckbox);
		clickElementBy(buttonSubmit);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

}
