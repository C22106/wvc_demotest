package bbm.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BeginSearchPage extends BBMBasePage {

	public static final Logger log = Logger.getLogger(BeginSearchPage.class);
	public static String Retail_amt;
	public static String Saving_amt;
	public static String Total_amt;

	public BeginSearchPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By currentMonth = By.xpath("//div[contains(@class,'group-first')]//span[@class='ui-datepicker-month']");
	protected By currentYear = By.xpath("//div[contains(@class,'group-first')]//span[@class='ui-datepicker-year']");
	protected By consecutiveMonth = By
			.xpath("//div[contains(@class,'group-last')]//span[@class='ui-datepicker-month']");
	protected By enabledDate = By.xpath("//td[@data-handler='selectDay']/a");
	protected By buttonSearchAvailability = By.xpath("//a[text()='Search Availability']");

	protected By noOfChild = By.xpath("//span[@class='increment__number']");
	protected By noOfAdult = By.xpath("//span[@class='increment__number']");
	protected By maxGuest = By.xpath("//p[@class='text-gray2 disclaimer pl-2 xl:text-xxs']");
	protected By adultPlusButton = By.xpath("//a[@title='Add one Adult']");
	protected By plusButtonChild = By.xpath("//a[@title='Add one Child']");
	protected By guestPanel = By.xpath("//a[@title='Add one Child']");
	protected By minusButtonChild = By.xpath("//a[@title='Subtract one Child']");
	protected By labelContactNo = By.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By labelNeedHelp = By.xpath("//p//strong[text()='Need Help Booking?']");
	protected By buttonNext = By.xpath("//a[@data-handler='next']");
	protected By popupContinueBooking = By.xpath("//a[@id='popupContinue']");
	protected By labelProgressBar = By.xpath("//div[@class='steps-desktop hidden tabletl:flex text-white']");

	protected By linkLearnMoreCancellationPolicy = By.xpath("//a[@data-fragment='global.cancellation.LearnMore']");
	protected By contentCancellationPolicy = By.xpath("//p[contains(.,'Cancel within')]");
	protected By frameCancellationPolicy = By.xpath("//div[@id='popupCancellation']");
	protected By frameCancellationPolicycClose = By.xpath("//*[@id='popupCancellation']/div");
	protected By linkViewDestinations = By.xpath("//a[contains(.,'View All Destinations')]");
	protected By linkViewOffersHB = By.xpath("//a[contains(.,'View Offers')]");

	protected By packagePricesSort = By.xpath("//h3[contains(.,'$')]");
	protected By termsNConditionLink = By.xpath("//a[contains(.,'Terms and Conditions')]");
	protected By advertisingMaterial = By.xpath(
			"//b[contains(.,'THIS ADVERTISING MATERIAL IS BEING USED FOR THE PURPOSE OF SOLICITING SALES OF TIMESHARE INTERESTS.')]");
	protected By worldMarkLogoHeader = By.xpath("//header//img[@alt='WorldMark by Wyndham']");
	protected By worldMarkLogoFooter = By.xpath("//footer//img[@alt='WorldMark by Wyndham']");
	protected By clubwyndhamLogoFooter = By.xpath("//footer//img[@alt='CLUB WYNDHAM']");
	protected By clubwyndhamLogoHeader = By.xpath("//header//img[@alt='CLUB WYNDHAM']");
	protected By linkTermsOfuse = By.xpath("//a[contains(.,'Terms of Use')]");
	protected By linkCopyright = By.xpath("//p[contains(.,'Rights')]");
	protected By linkPrivacyNotice = By.xpath("//a[contains(.,'Privacy Notice')]");
	protected By linkPrivacySetting = By.xpath("//a[contains(.,'Privacy Setting')]");
	protected By tripsummaryBookNow = By.xpath("//section//span[contains(.,'Book Now')]");
	protected By buttonBookNow = By.xpath("(//span[@data-fragment='global.package.detail.book.now'])[1]");
	protected By linkBacktoVacationPackages = By.xpath("//a[contains(.,'Back to Vacation Packages')]");

	protected By linkViewPackages = By.xpath("(//a[contains(.,'View Details')])[1]");
	protected By linkPrivacySettingpage = By.xpath("//h1['Interest Based Advertising Policies']");
	protected By diffPackages = By.xpath("//div[@class='wyn-bbm-package flex flex-col items-start h-full']");
	protected By lengthOfStay = By.xpath("//h4[contains(.,'Hotel Accommodations')]");
	protected By packageExtras = By.xpath("//h4[contains(.,'Package Extras')]");
	protected By discountSaving = By.xpath("//div[@class='icon-bed w-full icon-savings']");

	protected By listpackage = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-title']//h3");
	protected By startingUpto = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-l-content wyn-card__copy']");
	protected By viewoffer = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-card__cta']");
	protected By pkgDetailTripSummrry = By.xpath("//div[@class='trip-summary__footer p-1 bg-gray3 ']");
	protected By PackageDetailsTripSummary = By.xpath("//h2[@class='font-heading text-xl font-medium mb-half']");
	protected By popupCookies = By.xpath("//div[@id='cookies']");
	protected By btnAccept = By.xpath("//span[contains(.,'Accept')]");

	protected By radiobtnIncentive = By.xpath("//label[contains(text(),'Wyndham Reward')]/../input");
	// protected By radiobtnIncentive = By.xpath("//label[contains(text(),'Wyndham
	// Rewards Points')]/../input");
	protected By btnSelectNcontinue = By.xpath("//span[contains(text(),'Select & Continue')]");
	protected By btnselctncontinue = By.xpath("//span[@id='selectIncentive']");

	/*
	 * Method Name: freeCancellationPolicyDisplayValidation Description:This method
	 * validates free Cancellation Policy Display throughout the booking flow Date:
	 * 25th Oct 2019 Author:Syba Mallick
	 */

	public void freeCancellationPolicyDisplayValidation() {

		waitUntilObjectVisible(driver, linkLearnMoreCancellationPolicy, 120);

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: "
							+ contentCPolicy);
			clickElementJSWithWait(linkLearnMoreCancellationPolicy);

			tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			verifyObjectDisplayed(frameCancellationPolicy);
			tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: "
							+ contentCPolicy);
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
					"Cancellation Policy frame is not present");
		}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}

	/*
	 * * Method Name: calendarValidations Description:This method validates the
	 * presence of calendar in Begin search page Date: 31st July 2019 Author:Syba
	 * Mallick
	 * 
	 */

	public void Increase_selection_Month() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, buttonNext, 120);

		clickElementBy(buttonNext);
		clickElementBy(buttonNext);
		clickElementBy(buttonNext);

	}

	public By email_form = By
			.xpath("//div[@id='emailCapture']//div[@class='mb-1 tabletl:mb-0 tabletl:pr-1 tabletl:w-2/5']");
	public By email_field_txt = By.xpath("//span[@data-fragment='global.email.Address']");
	public By email_field = By.xpath("//input[@id='emailCapture']");
	public By state_provinance = By.xpath("//span[@data-fragment='global.state.Province']");
	public By failure_msg = By.xpath("//input[@id='emailCapture']//following-sibling::p");
	public By state_select = By.xpath("//select[@id='emailCaptureState']");
	public By email_form_submit = By.xpath("//button[text()='Submit']");
	public By success_msg = By.xpath("//div[@class='text-center tabletl:text-left tabletl:ml-1']");

	public void Email_form_Validation() throws AWTException {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(email_form);
		waitUntilObjectVisible(driver, email_form, 120);

		if (verifyObjectDisplayed(email_form)) {

			tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
					"The Email Form is present and the message is : " + driver.findElement(email_form).getText());

			waitUntilObjectVisible(driver, email_field, 120);
			if (verifyObjectDisplayed(email_field)) {

				if (driver.findElement(email_field_txt).getText().contains("Email Address")) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The Email field contains the text 'Email Address'");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL,
							"The Email field not contains the text 'Email Adress'");
				}

				if (driver.findElement(state_provinance).getText().contains("Your State/Province")) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The State field contains the text 'Your State/Province'");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL,
							"The State field not contains the text 'Your State/Province'");
				}

				fieldDataEnter(email_field, "XYZ");
				driver.findElement(email_field).sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(failure_msg)) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The Failure Message is populating as expected");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL,
							"The Failure Message is not populating as expected");
				}
				String Parent = driver.getWindowHandle();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.navigate().refresh();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.switchTo().window(Parent);
				fieldDataEnter(email_field, testData.get("Email"));
				driver.findElement(email_field).sendKeys(Keys.TAB);
				Select sel = new Select(driver.findElement(state_select));
				sel.selectByVisibleText(testData.get("DropdownState"));
				driver.findElement(state_select).sendKeys(Keys.TAB);
				clickElementJSWithWait(email_form_submit);

				waitUntilObjectVisible(driver, success_msg, 120);
				if (verifyObjectDisplayed(success_msg)) {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.PASS,
							"The Success Message is populating as expected");
				} else {
					tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL,
							"The Success Message is NOT populating as expected");
				}

				Actions act = new Actions(driver);
				act.sendKeys(Keys.PAGE_UP);
				act.sendKeys(Keys.PAGE_UP);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				Robot rob = new Robot();
				rob.mouseWheel(-1500);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

		} else {
			tcConfig.updateTestReporter("BBM", "Email Form Validation", Status.FAIL, "The Email Form is not present");
		}

	}

	public void calendarValidations() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilObjectVisible(driver, enabledDate, 120);

		if (verifyObjectDisplayed(enabledDate)) {
			clickElementBy(buttonNext);
			clickElementBy(buttonNext);
			/*
			 * clickElementBy(buttonNext); clickElementBy(buttonNext);
			 * clickElementBy(buttonNext);
			 */
			List<WebElement> enabledDateList = driver.findElements(enabledDate);
			tcConfig.updateTestReporter("BeginSearchPage", "calenderValidations", Status.PASS,
					"Total Available Dates to check in are: " + enabledDateList.size());

			enabledDateList.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(buttonSearchAvailability);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("BeginSearchPage", "calenderValidations", Status.PASS,
					"Check-in date is selected");

		}

	}

	/*
	 * * Method Name: checkInDateChange Description:This method validates check In
	 * and Check-out date change in Begin search page Date: 31st July 2019
	 * Author:Syba Mallick
	 */
	public void checkInDateChange() {

		waitUntilObjectVisible(driver, enabledDate, 120);
		if (verifyObjectDisplayed(labelContactNo)) {
			tcConfig.updateTestReporter("BeginSearchPage", "labelContactNoValidation", Status.PASS,
					"Contact No is present in BeginSearch Page");

		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "labelContactNoValidation", Status.FAIL,
					"Contact No not getting displayed in BeginSearch page");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(enabledDate)) {
			List<WebElement> enabledDateList = driver.findElements(enabledDate);
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChange", Status.PASS,
					"Total Available Dates to check in are: " + enabledDateList.size());

			enabledDateList.get(0).click();
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChange", Status.PASS,
					"Check-in date is selected");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> enabledDateReselectList = driver.findElements(enabledDate);
			enabledDateReselectList.get(5).click();
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChange", Status.PASS,
					"Check-in date is re-selected");
			clickElementBy(buttonSearchAvailability);
			tcConfig.updateTestReporter("BeginSearchPage", "calenderValidations", Status.PASS,
					"Check-in date is selected");

		} else

		{
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChange", Status.FAIL,
					"Check-in date is not enabled and can not be selected");
		}

	}

	/*
	 * * Method Name: checkInMonthYearValidation Description:This method validates
	 * Pop UP modal in Begin search page Date: 21st Sept 2019 Author:Syba Mallick
	 */

	public void popupModal() {

		if (driver.findElements(popupContinueBooking).size() > 0) {
			Actions action = new Actions(driver);
			action.moveToElement(driver.findElement(labelNeedHelp)).build().perform();

			waitUntilObjectVisible(driver, popupContinueBooking, 120);
			if (verifyObjectDisplayed(popupContinueBooking)) {
				tcConfig.updateTestReporter("BeginSearchPage", "popupModal", Status.PASS,
						"Pop Up Are you sure you want to leave? is present in BeginSearch Page");
				clickElementJSWithWait(popupContinueBooking);
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "labelContactNoValidation", Status.FAIL,
						"Pop Up Are you sure you want to leave? is not present in BeginSearch Page");
			}
		}
	}
	/*
	 * Method Name: labelProgressBarValidation Description:This method validates the
	 * label Progress Bar in the Begin Search Page Date: 18th Sept 2019 Author:Syba
	 * Mallick
	 */

	public void labelProgressBarValidation() {

		waitUntilObjectVisible(driver, labelProgressBar, 120);

		if (verifyObjectDisplayed(labelProgressBar)) {
			String progress = driver.findElement(labelProgressBar).getText();

			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"Progress Bar label is displayed in Begin search Page with the content as: " + progress);

		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
					"Cookies not present");
		}

	}

	public By add_child = By.xpath("//a[@title='Add one Child']");
	public By remove_adult = By.xpath("//a[@title='Subtract one Adult']");
	public By Search_Btn = By.xpath("//a[text()='Search Availability']");

	public void addaChild_search() {

		waitUntilObjectVisible(driver, add_child, 120);
		if (verifyObjectDisplayed(add_child)) {
			if (verifyObjectDisplayed(enabledDate)) {
				List<WebElement> enabledDateList = driver.findElements(enabledDate);
				tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChange", Status.PASS,
						"Total Available Dates to check in are: " + enabledDateList.size());

				enabledDateList.get(0).click();
				tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChange", Status.PASS,
						"Check-in date is selected");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> enabledDateReselectList = driver.findElements(enabledDate);
				enabledDateReselectList.get(5).click();
				tcConfig.updateTestReporter("BeginSearchPage", "calenderValidations", Status.PASS,
						"Check-in date is selected");

			} else

			{
				tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChange", Status.FAIL,
						"Check-in date is not enabled and can not be selected");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(add_child);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(remove_adult);
			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"Number of Adults and Children are updated");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(Search_Btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
					"Number of Adults and Children are not updated");
		}

	}

	public By Trip_Summary_Card = By.xpath("//div[@class='p-1 bg-indigo']//h1[text()='Trip Summary']");
	public By Trip_summary_Pkg_Dtls = By.xpath("//div//h2[text()='PACKAGE DETAILS']");
	public By Trip_Destination = By.xpath("//div//h2[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Includes = By.xpath("//div//p//strong[text()='Includes:']//parent::p");
	public By Trip_Retail = By.xpath("(//div[@class='flex justify-between text-xs'])[1]");
	public By Trip_Saving = By.xpath("(//div[@class='flex justify-between text-xs'])[2]");
	public By Trip_Total = By.xpath("(//div[@class='flex justify-between'])/p[2]");
	public By Trip_Tax_details = By.xpath("//p[@data-fragment='minivac.paymentDetails.disclaimer']");
	public By package_Details = By.xpath("//div//p[text()='PACKAGE INCENTIVE DETAILS']");
	public By Book_Now_Btn = By.xpath("//span[text()='Book Now' and @class='button -small w-full text-center']");
	public By Calander_Object = By.xpath("//div[@class='calendar-container hasDatepicker']");
	public By Trip_dates = By.xpath("//p[@class='text-sm mb-half']");
	public By Trip_for_details = By.xpath("//p[@class='text-sm mb-1']");
	public By Trip_Modify_Link = By.xpath("//a[text()='Modify Search']");
	public By Trip_Book_Begin_Search = By.xpath("(//a[text()='Book Now'])[1]");
	public By Trip_Guest_page = By.xpath("//h1[contains(text(),'Guest Information')]");
	public By Trip_Accomodatios = By.xpath("//h2[text()='Accommodations']");
	public By Trip_resort_Name = By.xpath("//p[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Verify_Property = By.xpath("//a[text()='View Property Details']");
	protected By selectProperty = By.xpath("//div[@id='hotels-list']//p[@class='mb-quarter']/a[contains(text(),'"
			+ testData.get("accomodationName") + "')]/../..//a[contains(.,'Stay Here')]");

	public void Trip_Summary_View_details_BeginSearch() {

		waitUntilObjectVisible(driver, Trip_Summary_Card, 120);

		if (verifyObjectDisplayed(Trip_Summary_Card)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Title with Blue Backgound color is present.");
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Title with Blue Backgound color is not present.");
		}

		if (verifyObjectDisplayed(PackageDetailsTripSummary)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary package details header is present.");
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary package details header is not present.");
		}

		if (verifyObjectDisplayed(Trip_Destination)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary destination is present as : " + driver.findElement(Trip_Destination).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary destination is not present.");
		}

		if (verifyObjectDisplayed(Trip_Includes)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Includes is present as : " + driver.findElement(Trip_Includes).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Includes is not present.");
		}

		if (verifyObjectDisplayed(Trip_dates)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Dates are present as : " + driver.findElement(Trip_dates).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Dates are not present.");
		}

		if (verifyObjectDisplayed(Trip_for_details)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip For details are present as : " + driver.findElement(Trip_for_details).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip For details are not present.");
		}

		if (verifyObjectDisplayed(Trip_Accomodatios)) {
			tcConfig.updateTestReporter("Guest Info page", "linkViewPackages", Status.PASS,
					"Trip accomodations header is present");
			if (verifyObjectDisplayed(Trip_resort_Name)) {
				tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
						"Trip Resort Name is " + driver.findElement(Trip_resort_Name).getText());
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
						"Trip Resort Name is not present.");
			}

			if (verifyObjectDisplayed(Trip_Verify_Property)) {
				tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
						"View property Details link is present.");
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
						"View property Details link is not present.");
			}

		}

		if (verifyObjectDisplayed(Trip_Modify_Link)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Modify Link is present");
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Modify Link is not present.");
		}

		if (verifyObjectDisplayed(Trip_Retail)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Retail is present as : " + driver.findElement(Trip_Retail).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Retail is not present.");
		}

		if (verifyObjectDisplayed(Trip_Saving)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Saving is present as : " + driver.findElement(Trip_Saving).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Saving is not present.");
		}

		if (verifyObjectDisplayed(Trip_Total)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Total is present as : " + driver.findElement(Trip_Total).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Total is not present.");
		}

		if (verifyObjectDisplayed(Trip_Tax_details)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Tax Detail is present as : " + driver.findElement(Trip_Tax_details).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Tax Detail is not present.");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(selectProperty);
		/*
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * if(verifyObjectDisplayed(radiobtnIncentive)){ waitUntilObjectVisible(driver,
		 * radiobtnIncentive, 120); clickElementJSWithWait(radiobtnIncentive);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * clickElementJSWithWait(btnSelectNcontinue);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));}
		 */
		waitUntilObjectVisible(driver, Trip_Guest_page, 120);
		if (verifyObjectDisplayed(Trip_Guest_page)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"The Guest information Page is present.");
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"The Guest information Page is not present.");
		}

	}

	public void Trip_Summary_View_details_Validation() {

		waitUntilObjectVisible(driver, Trip_Summary_Card, 120);

		if (verifyObjectDisplayed(Trip_Summary_Card)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Title with Blue Backgound color is present.");
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Title with Blue Backgound color is not present.");
		}

		if (verifyObjectDisplayed(PackageDetailsTripSummary)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary package details header is present.");
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary package details header is not present.");
		}

		if (verifyObjectDisplayed(Trip_Destination)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary destination is present as : " + driver.findElement(Trip_Destination).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary destination is not present.");
		}

		if (verifyObjectDisplayed(Trip_Includes)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Includes is present as : " + driver.findElement(Trip_Includes).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Includes is not present.");
		}

		if (verifyObjectDisplayed(Trip_Retail)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Retail is present as : " + driver.findElement(Trip_Retail).getText());
			Retail_amt = driver.findElement(Trip_Retail).getText();
			System.out.println(Retail_amt);
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Retail is not present.");
		}

		if (verifyObjectDisplayed(Trip_Saving)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Saving is present as : " + driver.findElement(Trip_Saving).getText());
			Saving_amt = driver.findElement(Trip_Saving).getText();
			System.out.println(Saving_amt);
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Saving is not present.");
		}

		if (verifyObjectDisplayed(Trip_Total)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Total is present as : " + driver.findElement(Trip_Total).getText());
			Total_amt = driver.findElement(Trip_Total).getText();
			System.out.println(Total_amt);
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Total is not present.");
		}

		if (verifyObjectDisplayed(Trip_Tax_details)) {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary Tax Detail is present as : " + driver.findElement(Trip_Tax_details).getText());
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.FAIL,
					"Trip summary Tax Detail is not present.");
		}

		if (Total_amt.contains(BBMHomepage.initial_pkg_amt)) {

			tcConfig.updateTestReporter("BeginSearchPage", "linkViewPackages", Status.PASS,
					"Trip summary card Total amount and Package amount is same as : " + BBMHomepage.initial_pkg_amt);

			int a = Integer.parseInt(Retail_amt.replaceAll("[\\D]", ""));
			System.out.println(a);

			int b = Integer.parseInt(Saving_amt.replaceAll("[\\D]", ""));
			System.out.println(b);

			int c = Integer.parseInt(Total_amt.substring(1,Total_amt.indexOf('.')));
			System.out.println(c);

			if (c == a - b) {
				tcConfig.updateTestReporter("BBM", "Trip_Summary_View_details_Validation", Status.PASS,
						"The Calculations regarding the Total amount are correct");
			} else {
				tcConfig.updateTestReporter("BBM", "Trip_Summary_View_details_Validation", Status.FAIL,
						"The Calculations regarding the Total amount are NOT correct");
			}
		}

	}

	/*
	 * * Method Name: checkInMonthYearValidation Description:This method validates
	 * check In and Checkk-out Month and Year in Begin search page Date: 31st July
	 * 2019 Author:Syba Mallick
	 */
	public void checkInMonthYearValidation() throws AWTException {

		waitUntilObjectVisible(driver, enabledDate, 120);

		/*
		 * Actions action = new Actions(driver);
		 * action.moveToElement(driver.findElement(whyClubWyndhamHeader)).build().
		 * perform();
		 */
		/*
		 * popupModal();
		 */

		/*
		 * Robot rob=new Robot(); rob.mouseWheel(-30);
		 */
		/*
		 * ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		 * driver.switchTo().window(tabs.get(1)); waitUntilObjectVisible(driver,
		 * popupContinueBooking, 120); if (verifyObjectDisplayed(popupContinueBooking))
		 * { tcConfig.updateTestReporter("BeginSearchPage", "popupModal", Status.PASS,
		 * "Pop Up Are you sure you want to leave? is present in BeginSearch Page");
		 * clickElementJSWithWait(popupContinueBooking); } else {
		 * tcConfig.updateTestReporter("BeginSearchPage", "labelContactNoValidation",
		 * Status.FAIL,
		 * "Pop Up Are you sure you want to leave? is not present in BeginSearch Page");
		 * } driver.navigate().back();
		 */
		// waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyObjectDisplayed(enabledDate)) {

			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
					"Calendar is getting displayed with Check In dates enabled");
		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL,
					"Error in getting calendar field");

		}
		if (verifyObjectDisplayed(currentMonth)) {
			String CurrentMonth = driver.findElement(currentMonth).getText();
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
					"Two Months are getting displayed at a time .The Current Month is :" + CurrentMonth);
		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL, "Error in displaying month");

		}

		if (verifyObjectDisplayed(consecutiveMonth)) {
			String ConsecutiveMonth = driver.findElement(consecutiveMonth).getText();
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
					"Two Months are getting displayed at a time .The Consecutive Month is :" + ConsecutiveMonth);
		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL, "Error in displaying month");

		}
		if (verifyObjectDisplayed(currentYear)) {
			String ConsecutiveMonth = driver.findElement(currentYear).getText();
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
					"Year field isd also getting displayed in calendar.The  Year is :" + ConsecutiveMonth);
		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL, "Error in displaying month");
		}

	}

	/*
	 * Method Name: guestPanelChild Description:This method validates guest Panel
	 * for child in Begin search page Date: 31st July 2019 Author:Syba Mallick
	 */
	public void guestPanelChild() {
		waitUntilElementVisibleBy(driver, guestPanel, 120);
		if (verifyObjectDisplayed(guestPanel)) {
			tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.PASS,
					"Guest Panel is Present on the page");

			if (verifyObjectDisplayed(plusButtonChild)) {
				clickElementBy(plusButtonChild);
				clickElementBy(plusButtonChild);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.PASS,
						"Add more child option is Present on the page");

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
						"Add more child option not present");
			}

			if (verifyObjectDisplayed(minusButtonChild)) {
				clickElementBy(minusButtonChild);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.PASS,
						"Substract child as guest option present");

			} else {
				tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.FAIL,
						"Substract child as guest option not present");
			}

		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.FAIL, "Guest Panel not present");
		}

	}

	/*
	 * Method Name: guestPanelCheck Description:This method validates guest Panel
	 * from Begin search page Date: 31st July 2019 Author:Syba Mallick
	 */
	public void guestPanelCheck() {

		if (verifyObjectDisplayed(maxGuest)) {
			tcConfig.updateTestReporter("beginSearchPage", "guestPanelCheck", Status.PASS,
					"Select your Guest section is present on the page");

			if (verifyObjectDisplayed(plusButtonChild)) {
				for (int i = 0; i < 3; i++) {
					clickElementBy(plusButtonChild);
					String childCount = driver.findElement(noOfChild).getText().trim();
					int noOfChildCount = Integer.parseInt(childCount);
					if (noOfChildCount == 2) {

						tcConfig.updateTestReporter("beginSearchPage", "guestPanelCheck", Status.PASS,
								"Child incresed to: " + getElementText(noOfChild));

						break;
					}
					try {
						clickElementBy(plusButtonChild);
						System.out.println("Checked :" + getElementText(noOfChild));
					} catch (Exception e) {

					}

				}

			}

		}

		if (verifyObjectDisplayed(adultPlusButton)) {
			for (int i = 0; i <= 1; i++) {
				clickElementBy(adultPlusButton);
				String adultCount = driver.findElement(noOfAdult).getText().trim();
				int noOfAdultCount = Integer.parseInt(adultCount);
				if (noOfAdultCount == 2) {

					tcConfig.updateTestReporter("beginSearchPage", "guestPanelCheck", Status.PASS,
							"Adult incresed to: " + getElementText(noOfAdult));

					break;
				}
				try {
					clickElementBy(plusButtonChild);
					System.out.println("Checked :" + getElementText(noOfAdult));
				} catch (Exception e) {

				}

			}

		}
		tcConfig.updateTestReporter("BeginSearchPage", "guestPanelCheck", Status.FAIL,
				"Guest Panel close button not present");

	}

	public void closeTripSummary() {
		// TODO Auto-generated method stub

	}

}
