package bbm.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BBMHomepage_IOS extends BBMHomepage {

	public static final Logger log = Logger.getLogger(BBMHomepage_IOS.class);

	public BBMHomepage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		tripsummaryAvailabilty = By.xpath("//span[contains(.,'Check Availability')]");
		package_amt=By.xpath("(//p[@class='giant text-black pr-1 border-r border-gray3 mr-1'])[1]");
		buttonBookNow = By.xpath("(//span[@data-fragment='global.package.detail.book.now'])[1]");
		dest_table=By.xpath("//div[contains(@class,'mobile-menu')]");
		all_Destination_list=By.xpath("//div[contains(@class,'mobile-menu')]//ul//li//a[contains(text(),',')]");
	}

	public void footertermsOfuseLink() {

		if (verifyObjectDisplayed(linkTermsOfuse)) {
			tcConfig.updateTestReporter("BBMHomepage", "footertermsOfuseLink", Status.PASS,
					"BBM Home page navigated");
			String strURL=driver.getCurrentUrl();
			clickElementBy(linkTermsOfuse);
			clickIOSElement("Terms of Use", 1, linkTermsOfuse);
			/*waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));*/
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String termsOfusepageURL = driver.getCurrentUrl();
			String URL = testData.get("urlTermOfUsePage");
			if (termsOfusepageURL.contains(URL)) {

				tcConfig.updateTestReporter("BBMHomepage", "footertermsOfuseLink", Status.PASS,
						"terms Of use page Navigation Successful.The url is : " + termsOfusepageURL);

			}

			else {
				tcConfig.updateTestReporter("BBMHomepage", "footertermsOfuseLink", Status.FAIL,
						"terms Of use page Navigation Unsuccessful");
			}

			/*driver.close();
			driver.switchTo().window(tabs.get(0));*/
			driver.get(strURL);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}
	public void footerPrivacyNoteLink() {

		if (verifyObjectDisplayed(linkPrivacyNotice)) {
			tcConfig.updateTestReporter("BBMHomepage", "footerPrivacyNoteLink", Status.PASS,
					"Navigating To privacy Notice page");
			String StrUrl=driver.getCurrentUrl();
			clickElementBy(linkPrivacyNotice);
			clickIOSElement("Privacy Notice", 1, linkPrivacyNotice);
			/*waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));*/
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String privacyNoteLinkURL = driver.getCurrentUrl();
			String URL = testData.get("urlPrivacyNoticePage");
			if (privacyNoteLinkURL.contains(URL)) {

				tcConfig.updateTestReporter("BBMcHomepage", "footerPrivacyNoteLink", Status.PASS,
						"privacy Notice page Navigation Successful.The URL is:" + privacyNoteLinkURL);

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "footerPrivacyNoteLink", Status.FAIL,
						"privacy Notice page Navigation Unsuccessful");
			}

			driver.get(StrUrl);
			/*driver.close();
			driver.switchTo().window(tabs.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));*/
		}

	}

	
	/*
	 * Method Name: footerPrivacySettingLink
	 * Description:This method validates Privacy Setting as a Link in the footer section
	 * Date: 31st june 2019
	 * Author:Syba Mallick
	 */
	public void footerPrivacySettingLink() {

		if (verifyObjectDisplayed(linkPrivacySetting)) {
			tcConfig.updateTestReporter("BBMHomepage", "footerPrivacySettingLink", Status.PASS,
					"Navigating To privacy Setting page");
			String StrUrl=driver.getCurrentUrl();
			clickElementBy(linkPrivacySetting);
			clickIOSElement("Privacy Setting", 1, linkPrivacySetting);
			
			/*waitForSometime(tcConfig.getConfig().get("LowWait"));
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));*/
			waitUntilObjectVisible(driver, linkPrivacySettingpage, 120);
			if (verifyObjectDisplayed(linkPrivacySettingpage)) {

				tcConfig.updateTestReporter("BBMHomepage", "footerPrivacySettingLink", Status.PASS,
						"privacy Setting page Navigation Successful");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "footerPrivacySettingLink", Status.FAIL,
						"privacy Setting page Navigation Unsuccessful");
			}

			driver.get(StrUrl);
			/*driver.close();
			driver.switchTo().window(tabs.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));*/
		}

	}
	
	public By viewHighlight=By.xpath("(//a[contains(.,'View Package Highlights')])[1]");
	public void linkViewPackageNavigation() {

		waitUntilObjectVisible(driver, buttonBookNow, 120);
		scrollDownForElementJSBy(buttonBookNow);
		scrollUpByPixel(100);
		clickElementBy(viewHighlight);
		if (verifyObjectDisplayed(linkViewPackages)) {
			scrollDownForElementJSBy(linkViewPackages);
			scrollUpByPixel(50);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,
					"link View Details is present.");

			List<WebElement> listViewPackages = driver.findElements(linkViewPackages);
			listViewPackages.get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

					" Navigating To ViewPackages page");
			waitUntilElementVisibleBy(driver, linkBacktoVacationPackages, 120);
			if (verifyObjectDisplayed(linkBacktoVacationPackages)) {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

					"Link BacktoVacationPackages is present in View Packages Details page");

			} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
					"Link BacktoVacationPackages is present in View Packages Details page");
			}
				if (verifyObjectDisplayed(tripsummaryAvailabilty)) {
				//scrollDownForElementJSBy(buttonBookNow);
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.PASS,
						"View Details Page:Search Availabilty Button is present in Book This package card");

			} else {
				tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.FAIL,
						"View Details Page:Search Availabilty Button is not present in Book This package card");
			}
		/*if (verifyObjectDisplayed(pkgDetailTripSummrry)) {
			String viewPkgTripSummary = driver.findElement(pkgDetailTripSummrry).getText();
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.PASS,
					"View Details Page:viewPkgTripSummary is present in Trip Summary card"+viewPkgTripSummary);

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.FAIL,
					"View Details Page:Book Now Button is not present in Trip Summary card");
		}
		if (verifyObjectDisplayed(PackageDetailsTripSummary)) {
			String PackageDetailsTripSummary = driver.findElement(pkgDetailTripSummrry).getText();
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.PASS,
					"View Details Page:viewPkgTripSummary is present in Trip Summary card"+PackageDetailsTripSummary);

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackageNavigation", Status.FAIL,
					"View Details Page:Book Now Button is not present in Trip Summary card");
		}*/
		
			clickElementJSWithWait(linkBacktoVacationPackages);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, buttonBookNow, 120);
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.PASS,

					"Link Back to Vacation Packages navigation successful");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkViewPackages", Status.FAIL,
					"Link Back to Vacation Packages navigation unsuccessful");
		}

	}
	
	public void packageSortValidation() {

		waitUntilObjectVisible(driver, packagePricesSort, 120);
		tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
				"Destination Landing Pages: Package information shown as cards");

		List<WebElement> ViewPackagetList = driver.findElements(packagePricesSort);

		int i;

		for (i = 0; i < ViewPackagetList.size(); i++) {

			String strPackagePricing = ViewPackagetList.get(i).getText();
			strPackagePricing = strPackagePricing.replace("$", "").trim();
			if (i == 0) {

				stringPkg1 = strPackagePricing;
			} else if (i == 1) {
				stringPkg2 = strPackagePricing;
			}
		}
		if (Integer.parseInt(stringPkg1) <= Integer.parseInt(stringPkg2)) {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages: Lowest price and lowest package id number is displayed in order left to right");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL,
					"Packages are not sorted");
		}
		
		clickElementBy(viewHighlight);
		if (verifyObjectDisplayed(packageExtras)) {
			scrollDownForElementJSBy(packageExtras);
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages:In Package Card Package Card Package Extras are present");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL,
					"Destination Landing Pages:In Package Card Package Card Package Extras are not present");
		}
		if  (verifyObjectDisplayed(lengthOfStay)){
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages:In Package Card Lenght of Stay are present.");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL,
					"Destination Landing Pages:In Package Card Lenght of Stay are not present.");
		}
		if (verifyObjectDisplayed(discountSaving)) {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.PASS,
					"Destination Landing Pages:In Package Card Discount Savings are present.");

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "packageSortValidation", Status.FAIL,
					"Destination Landing Pages:In Package Card Discount Savings are not present.");
		}
		clickElementBy(viewHighlight);
	}
	
	public void expandViewPackageHighlights(){
		waitUntilObjectVisible(driver, viewHighlight, 120);
		scrollDownForElementJSBy(viewHighlight);
		scrollUpByPixel(100);
		if (verifyObjectDisplayed(viewHighlight)) {
			
		clickElementBy(viewHighlight);
		tcConfig.updateTestReporter("BBMHomepage", "expandViewPackageHighlights", Status.PASS,
				"View package highlight expanded");
		}
	}
	
	@Override
	public void linkBookNowNavigation() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		//waitUntilObjectVisible(driver, buttonBookNow, 120);
		/*initial_pkg_amt=driver.findElement(package_amt).getText();
		System.out.println(initial_pkg_amt);*/
		if (verifyObjectDisplayed(buttonBookNow)) {

//			List<WebElement> listBookNow = driver.findElements(buttonBookNow);
//			listBookNow.get(0).click();
			clickElementBy(buttonBookNow);
			clickIOSElement("Book Now", 1, buttonBookNow);
			tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.PASS,
					"Button Book Now is present and clicked Successfully");
			verifyObjectDisplayed(btnSelectNcontinue);
			
			String selctncontinue = driver.findElement(btnselctncontinue).getAttribute("class");
			if(selctncontinue.contains("disabled")){
				tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.PASS,

						"Button Select and continue is not activated until radio button is selected");
					
			}else{
				tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.FAIL,

						"Button Select and continue is activated before radio button is selected");
			}
			
			

		} else {
			tcConfig.updateTestReporter("BBMHomepage", "linkBookNowNavigation", Status.FAIL,
					"Book Now page navigation unsucessful");
		}

	}
	
	protected By hamburgermenuicon = By.xpath("//div[contains(@class,'top-nav__hamburger')]");
	protected By closeMobileIcon = By.xpath("//div[@id='closeMobileMenu']");
	
	@Override
	public void clickHamBurger()
	{
		waitUntilElementVisibleBy(driver, hamburgermenuicon, 120);
		clickElementBy(hamburgermenuicon);
	}
	
	@Override
	public void Destination_Drpdown(){
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(verifyObjectDisplayed(dest_table)){
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.PASS,
					"Destinations are present");
		}else{
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.FAIL,
					"Destinations are NOT present");
		}
		
		List<WebElement> dest_list=driver.findElements(all_Destination_list);
		int dest_ele=dest_list.size();
		
		for(int i=0;i<=dest_ele-1;i++){
		System.out.println(dest_list.get(i).getText());
		tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.PASS,
				"visible destination no-"+(i+1)+" is :"+ dest_list.get(i).getText());
		
		}
		waitUntilObjectVisible(driver, closeMobileIcon, 120);
		clickElementBy(closeMobileIcon);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if(!verifyObjectDisplayed(dest_table)){
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.PASS,
					"Destination dropdown is closed on clicking Destinations link");
		}else{
			tcConfig.updateTestReporter("BBMHomepage", "Destination_Drpdown", Status.FAIL,
					"Destination dropdown is NOT closed on clicking Destinations link");
			}
		
		
	}
}
