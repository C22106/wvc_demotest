package bbm.pages;

import java.awt.AWTException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PackageDetailsPage extends BBMBasePage {

	public static final Logger log = Logger.getLogger(PackageDetailsPage.class);
	public static String Retail_amt;
	public static String Saving_amt;
	public static String Total_amt;

	public PackageDetailsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By currentMonth = By.xpath("//span[@class='ui-datepicker-month']");
	protected By currentYear = By.xpath("//span[@class='ui-datepicker-year']");
	protected By consecutiveMonth = By.xpath("//div[contains(@class,'group-last')]//span[@class='ui-datepicker-month']");
	protected By enabledDate = By.xpath("//td[@data-handler='selectDay']/a");
	protected By nextmonth = By.xpath("//a[@class='ui-datepicker-next ui-corner-all']");
	protected By buttonSearchAvailability = By.xpath("//a[text()='Search Availability']");

	protected By noOfChild = By.xpath("//span[@class='increment__number']");
	protected By noOfAdult = By.xpath("//span[@class='increment__number']");
	protected By maxGuest = By.xpath("//p[@class='text-gray2 disclaimer pl-2 xl:text-xxs']");
	protected By adultPlusButton = By.xpath("//a[@title='Add one Adult']");
	protected By plusButtonChild = By.xpath("//a[@title='Add one Child']");
	protected By guestPanel = By.xpath("//a[@title='Add one Child']");
	protected By minusButtonChild = By.xpath("//a[@title='Subtract one Child']");
	protected By labelContactNo = By.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By labelNeedHelp = By.xpath("//p//strong[text()='Need Help Booking?']");
	protected By buttonNext = By.xpath("//a[@data-handler='next']");
	protected By popupContinueBooking = By.xpath("//a[@id='popupContinue']");
	protected By labelProgressBar = By.xpath("//div[@class='steps-desktop hidden tabletl:flex text-white']");

	protected By linkLearnMoreCancellationPolicy = By.xpath("//a[@data-fragment='global.cancellation.LearnMore']");
	protected By contentCancellationPolicy = By.xpath("//p[contains(.,'Cancel within')]");
	protected By frameCancellationPolicy = By.xpath("//div[@id='popupCancellation']");
	protected By frameCancellationPolicycClose = By.xpath("//*[@id='popupCancellation']/div");
	protected By linkViewDestinations = By.xpath("//a[contains(.,'View All Destinations')]");
	protected By linkViewOffersHB = By.xpath("//a[contains(.,'View Offers')]");

	protected By packagePricesSort = By.xpath("//h3[contains(.,'$')]");
	protected By termsNConditionLink = By.xpath("//a[contains(.,'Terms and Conditions')]");
	protected By advertisingMaterial = By.xpath(
			"//b[contains(.,'THIS ADVERTISING MATERIAL IS BEING USED FOR THE PURPOSE OF SOLICITING SALES OF TIMESHARE INTERESTS.')]");
	protected By worldMarkLogoHeader = By.xpath("//header//img[@alt='WorldMark by Wyndham']");
	protected By worldMarkLogoFooter = By.xpath("//footer//img[@alt='WorldMark by Wyndham']");
	protected By clubwyndhamLogoFooter = By.xpath("//footer//img[@alt='CLUB WYNDHAM']");
	protected By clubwyndhamLogoHeader = By.xpath("//header//img[@alt='CLUB WYNDHAM']");
	protected By linkTermsOfuse = By.xpath("//a[contains(.,'Terms of Use')]");
	protected By linkCopyright = By.xpath("//p[contains(.,'Rights')]");
	protected By linkPrivacyNotice = By.xpath("//a[contains(.,'Privacy Notice')]");
	protected By linkPrivacySetting = By.xpath("//a[contains(.,'Privacy Setting')]");
	protected By tripsummaryBookNow = By.xpath("//section//span[contains(.,'Book Now')]");
	protected By buttonBookNow = By.xpath("(//span[@data-fragment='global.package.detail.book.now'])[1]");
	protected By linkBacktoVacationPackages = By.xpath("//a[contains(.,'Back to Vacation Packages')]");

	protected By linkViewPackages = By.xpath("(//a[contains(.,'View Details')])[1]");
	protected By linkPrivacySettingpage = By.xpath("//h1['Interest Based Advertising Policies']");
	protected By diffPackages = By.xpath("//div[@class='wyn-bbm-package flex flex-col items-start h-full']");
	protected By lengthOfStay = By.xpath("//h4[contains(.,'Hotel Accommodations')]");
	protected By packageExtras = By.xpath("//h4[contains(.,'Package Extras')]");
	protected By discountSaving = By.xpath("//div[@class='icon-bed w-full icon-savings']");

	protected By listpackage = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-title']//h3");
	protected By startingUpto = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-l-content wyn-card__copy']");
	protected By viewoffer = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-card__cta']");
	protected By pkgDetailTripSummrry = By.xpath("//div[@class='trip-summary__footer p-1 bg-gray3 ']");
	protected By PackageDetailsTripSummary = By.xpath("//h2[@class='font-heading text-xl font-medium mb-half']");
	protected By popupCookies = By.xpath("//div[@id='cookies']");
	protected By btnAccept = By.xpath("//span[contains(.,'Accept')]");

	protected By radiobtnIncentive = By.xpath("//label[contains(text(),'Wyndham Reward')]/../input");
	// protected By radiobtnIncentive = By.xpath("//label[contains(text(),'Wyndham
	// Rewards Points')]/../input");
	protected By btnSelectNcontinue = By.xpath("//span[contains(text(),'Select & Continue')]");
	protected By btnselctncontinue = By.xpath("//span[@id='selectIncentive']");

	protected By checkInDate = By.xpath("//p[contains(.,'Check-In Date')]");
	protected By calendarDropDown = By.xpath("//div[@class='activate wyn-calendar-container']");
	protected By guestDropDown = By.xpath("//div[@class='activate guestsData']");

	protected By guestDrpDwnActive = By.xpath("//div[@class='activate guestsData -active']");
	protected By calendarDrpDwnActive = By.xpath("//div[@class='activate wyn-calendar-container -active']");
	public By email_form = By
			.xpath("//div[@id='emailCapture']//div[@class='mb-1 tabletl:mb-0 tabletl:pr-1 tabletl:w-2/5']");
	public By email_field_txt = By.xpath("//span[@data-fragment='global.email.Address']");
	public By email_field = By.xpath("//input[@id='emailCapture']");
	public By state_provinance = By.xpath("//span[@data-fragment='global.state.Province']");
	public By failure_msg = By.xpath("//input[@id='emailCapture']//following-sibling::p");
	public By state_select = By.xpath("//select[@id='emailCaptureState']");
	public By email_form_submit = By.xpath("//button[text()='Submit']");
	public By success_msg = By.xpath("//div[@class='text-center tabletl:text-left tabletl:ml-1']");
	public By add_child = By.xpath("//a[@title='Add one Child']");
	public By remove_adult = By.xpath("//a[@title='Subtract one Adult']");
	public By Search_Btn = By.xpath("//a[text()='Search Availability']");
	public By Trip_Summary_Card = By.xpath("//div[@class='p-1 bg-indigo']//h1[text()='Trip Summary']");
	public By Trip_summary_Pkg_Dtls = By.xpath("//div//h2[text()='PACKAGE DETAILS']");
	public By Trip_Destination = By.xpath("//div//h2[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Includes = By.xpath("//div//p//strong[text()='Includes:']//parent::p");
	public By Trip_Retail = By.xpath("(//div[@class='flex justify-between text-xs'])[1]");
	public By Trip_Saving = By.xpath("(//div[@class='flex justify-between text-xs'])[2]");
	public By Trip_Total = By.xpath("(//div[@class='flex justify-between'])");
	public By Trip_Tax_details = By.xpath("//p[@data-fragment='minivac.paymentDetails.disclaimer']");
	public By package_Details = By.xpath("//div//p[text()='PACKAGE INCENTIVE DETAILS']");
	public By Book_Now_Btn = By.xpath("//span[text()='Book Now' and @class='button -small w-full text-center']");
	public By Calander_Object = By.xpath("//div[@class='calendar-container hasDatepicker']");
	public By Trip_dates = By.xpath("//p[@class='text-sm mb-half']");
	public By Trip_for_details = By.xpath("//p[@class='text-sm mb-1']");
	public By Trip_Modify_Link = By.xpath("//a[text()='Modify Search']");
	public By Trip_Book_Begin_Search = By.xpath("(//a[text()='Book Now'])[1]");
	public By Trip_Guest_page = By.xpath("//h1[contains(text(),'Guest Information')]");
	public By Trip_Accomodatios = By.xpath("//h2[text()='Accommodations']");
	public By Trip_resort_Name = By.xpath("//p[@class='font-heading text-xl font-medium mb-half']");
	public By Trip_Verify_Property = By.xpath("//a[text()='View Property Details']");

	/**************** Updated Calendar and Guest Info Methods ***********/
	protected By incentiveSel = By.xpath("//div[@id='incentiveChoices']//label");
	protected By incentiveHeader = By.xpath("//section//h2[contains(.,'Choose Your Incentive')]");

	/*
	 * * Method Name: incentiveSelect Description:Select Incentive Date:Jan 2020
	 * Author:Unnat
	 */
	public void incentiveSelect() {

		waitUntilElementVisibleBy(driver, incentiveHeader, 120);

		if (verifyObjectDisplayed(incentiveHeader)) {
			tcConfig.updateTestReporter("BeginSearchPage", "incentiveSelect", Status.PASS,
					"Incentive Section Present");

			List<WebElement> incentiveList = driver.findElements(incentiveSel);
			getElementInView(incentiveList.get(0));

			incentiveList.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("BeginSearchPage", "incentiveSelect", Status.PASS, "Incentive Selected");
		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "incentiveSelect", Status.FAIL,
					"Incentive Section not Present");
		}

	}

	/*
	 * * Method Name: calendarDateSelect Description:Select Calendar date
	 * Date:Jan 2020 Author:Unnat
	 */
	public void calendarDateSelect() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilObjectVisible(driver, checkInDate, 120);

		if (verifyObjectDisplayed(calendarDropDown)) {
			clickElementBy(calendarDropDown);
			waitUntilObjectVisible(driver, calendarDrpDwnActive, 20);
		}

		if (verifyObjectDisplayed(calendarDrpDwnActive)) {

			waitUntilObjectVisible(driver, enabledDate, 20);

			if (verifyObjectDisplayed(enabledDate)) {
				tcConfig.updateTestReporter("BeginSearchPage", "calendarDateSelect", Status.PASS,
						"Calendar Visible");
				getElementInView(buttonNext);
				for (int i = 0; i < 2; i++) {

					clickElementBy(buttonNext);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

				/*
				 * clickElementBy(buttonNext); clickElementBy(buttonNext);
				 * clickElementBy(buttonNext);
				 */
				List<WebElement> enabledDateList = driver.findElements(enabledDate);
				tcConfig.updateTestReporter("BeginSearchPage", "calendarDateSelect", Status.PASS,
						"Total Available Dates to check in are: " + enabledDateList.size());
				getElementInView(enabledDateList.get(0));
				enabledDateList.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(buttonSearchAvailability);
				clickElementBy(buttonSearchAvailability);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("BeginSearchPage", "calendarDateSelect", Status.PASS,
						"Check-in date is selected");
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "calendarDateSelect", Status.FAIL,
						"Calendar dates not Visible");

			}

		}

	}

	/*
	 * * Method Name: checkInMonthYearVal Description:Validate Calendar dates
	 * Date:Jan 2020 Author:Unnat
	 */
	
	protected By checkinDate = By.xpath("//span[contains(@class,'checkInDate')]");
	protected By checkOutDate = By.xpath("//span[contains(@class,'checkOutDate')]");
	
	public void selectCheckInDate() {
		String checkindate = testData.get("Checkindate").trim();
		// To store day from date
		String day = (checkindate.split(" ")[0]).trim();
		// To store month from date
		String month = (checkindate.split(" ")[1]).trim();
		// To store year from date
		String year = (checkindate.split(" ")[2]).trim();
		String monthyear = month.concat(" ").concat(year);

		if (verifyObjectDisplayed(calendarDropDown)) {
			clickElementJSWithWait(calendarDropDown);
			waitUntilObjectVisible(driver, calendarDrpDwnActive, 120);
			tcConfig.updateTestReporter("GuestInfoPage", "selectCheckInDate", Status.PASS, "Calendar Opened");

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.FAIL,
					"Calendar not Opened");
		}
		selectDate(month, day, monthyear, "Checkin", checkindate);
		tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
				"Check In Date displayed as: " + driver.findElement(checkinDate).getText() + " Check Out Date displayed as: "
						+ driver.findElement(checkOutDate).getText());

	}
	
	public void selectModifiedCheckInDate() {
		String checkindate = testData.get("ModifiedCheckindate").trim();
		// To store day from date
		String day = (checkindate.split(" ")[0]).trim();
		// To store month from date
		String month = (checkindate.split(" ")[1]).trim();
		// To store year from date
		String year = (checkindate.split(" ")[2]).trim();
		String monthyear = month.concat(" ").concat(year);

		if (verifyObjectDisplayed(calendarDropDown)) {
			clickElementJSWithWait(calendarDropDown);
			waitUntilObjectVisible(driver, calendarDrpDwnActive, 120);
			tcConfig.updateTestReporter("GuestInfoPage", "selectCheckInDate", Status.PASS, "Calendar Opened");

		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.FAIL,
					"Calendar not Opened");
		}
		selectDate(month, day, monthyear, "Checkin", checkindate);
		tcConfig.updateTestReporter("RedeemPackagePage", "checkInDateChange", Status.PASS,
				"Check In Date displayed as: " + driver.findElement(checkinDate).getText() + " Check Out Date displayed as: "
						+ driver.findElement(checkOutDate).getText());

	}
	
	protected By listCalendarMonthName = By.xpath("//div[@class='ui-datepicker-title']");
	protected By nextButton = By.xpath("//a[@title='Next']");
	
	public void selectDate(String month, String day, String monthyear, String whichDate, String selectDate) {
		String monthName = null;
		int monthPresent = 0;
		do {
			for (int monthIncrease = 0; monthIncrease <= getList(listCalendarMonthName).size() - 1; monthIncrease++) {
				// To get month name from screen 1 by 1
				monthName = getList(listCalendarMonthName).get(monthIncrease).getText().trim();
				if (monthyear.equals(monthName)) {
					getObject(By
							.xpath("//div[@class='ui-datepicker-title']/span[contains(@class,'datepicker-month') and contains(text(),'"
									+ month + "')]/../../../table//td/a[text()='" + day + "']")).click();
					monthPresent = monthPresent + 1;
					break;
				}
			}
			if (monthPresent == 0) {
				clickElementBy(nextButton);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} while (!monthyear.equals(monthName));
		if (monthPresent == 1) {
			tcConfig.updateTestReporter("RedeemPackagePage", "selectDate()", Status.PASS,
					"Selected" + whichDate + "date is: " + selectDate);
		} else {
			tcConfig.updateTestReporter("RedeemPackagePage", "selectDate()", Status.FAIL,
					whichDate + "date selection failed");
		}
	}
	
	
	public void checkInMonthYearVal() throws AWTException {

		waitUntilObjectVisible(driver, checkInDate, 120);

		if (verifyObjectDisplayed(calendarDropDown)) {
			clickElementBy(calendarDropDown);
			waitUntilObjectVisible(driver, calendarDrpDwnActive, 120);
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> enabledDateList1 = driver.findElements(enabledDate);
		
		if(enabledDateList1.size()==0){
			clickElementBy(nextmonth);
		}
		if (verifyObjectDisplayed(enabledDate)) {

			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
					"Calendar is getting displayed with Check In dates enabled");
		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL,
					"Error in getting calendar field");

		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(currentMonth)) {
			String CurrentMonth = driver.findElement(currentMonth).getText();
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
					"Single Month is getting displayed at a time .The Current Month is :" + CurrentMonth);
		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL,
					"Error in displaying month");

		}

//		if (verifyObjectDisplayed(consecutiveMonth)) {
//			String ConsecutiveMonth = driver.findElement(consecutiveMonth).getText();
//			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
//					"Two Months are getting displayed at a time .The Consecutive Month is :" + ConsecutiveMonth);
//		} else {
//			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL,
//					"Error in displaying month");
//
//		}
		
		
		if (verifyObjectDisplayed(currentYear)) {
			String currentYearDis = driver.findElement(currentYear).getText();
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.PASS,
					"Year field isd also getting displayed in calendar.The  Year is :" + currentYearDis);
		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "checkInDateChange", Status.FAIL,
					"Error in displaying year");
		}

	}

	/*
	 * * Method Name: guestPanelChildSel Description:Select Guest Date:Jan 2020
	 * Author:Unnat
	 */
	public void guestPanelChildSel() {

		waitUntilObjectVisible(driver, checkInDate, 120);

		if (verifyObjectDisplayed(guestDropDown)) {
			getElementInView(guestDropDown);
			clickElementBy(guestDropDown);
			waitUntilObjectVisible(driver, guestDrpDwnActive, 20);
		}

		waitUntilElementVisibleBy(driver, guestPanel, 20);
		if (verifyObjectDisplayed(guestPanel)) {
			tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.PASS,
					"Guest Panel is Present on the page");

			if (verifyObjectDisplayed(plusButtonChild)) {
				getElementInView(plusButtonChild);
				for (int i = 0; i < 2; i++) {

					clickElementBy(plusButtonChild);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.PASS,
						"Add more child option is Present on the page");

			} else {
				tcConfig.updateTestReporter("RedeemPackagePage", "guestPanelChild", Status.FAIL,
						"Add more child option not present");
			}

			if (verifyObjectDisplayed(minusButtonChild)) {
				getElementInView(minusButtonChild);
				clickElementBy(minusButtonChild);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.PASS,
						"Substract child as guest option present");

			} else {
				tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.FAIL,
						"Substract child as guest option not present");
			}

		} else {
			tcConfig.updateTestReporter("GuestInfoPage", "guestPanelChild", Status.FAIL, "Guest Panel not present");
		}

	}

	/*
	 * * Method Name: checkInDateChangeVal Description:This method validates
	 * check In and Check-out date change in Begin search page Date: Jan 2020
	 * Author: Unnat Jain
	 */
	public void checkInDateChangeVal() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(calendarDropDown);
		waitUntilObjectVisible(driver, checkInDate, 120);

		if (verifyObjectDisplayed(calendarDropDown)) {
			clickElementBy(calendarDropDown);
			waitUntilObjectVisible(driver, calendarDrpDwnActive, 120);
		}

		if (verifyObjectDisplayed(labelContactNo)) {
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChangeVal", Status.PASS,
					"Contact No is present in BeginSearch Page");

		} else {
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChangeVal", Status.FAIL,
					"Contact No not getting displayed in BeginSearch page");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> enabledDateList1 = driver.findElements(enabledDate);
				
				if(enabledDateList1.size()==0){
					clickElementBy(nextmonth);
				}
		if (verifyObjectDisplayed(enabledDate)) {
			List<WebElement> enabledDateList = driver.findElements(enabledDate);
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChangeVal", Status.PASS,
					"Total Available Dates to check in are: " + enabledDateList.size());
			getElementInView(enabledDateList.get(0));
			enabledDateList.get(0).click();
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChangeVal", Status.PASS,
					"Check-in date is selected");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> enabledDateReselectList = driver.findElements(enabledDate);
			enabledDateReselectList.get(0).click();
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChangeVal", Status.PASS,
					"Check-in date is re-selected");
			getElementInView(buttonSearchAvailability);
			clickElementBy(buttonSearchAvailability);
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChangeVal", Status.PASS,
					"Check-in date is selected");

		} else

		{
			tcConfig.updateTestReporter("BeginSearchPage", "checkInDateChangeVal", Status.FAIL,
					"Check-in date is not enabled and can not be selected");
		}

	}
	
	public void freeCancellationPolicyDisplayValidation() {

		waitUntilObjectVisible(driver, linkLearnMoreCancellationPolicy, 120);

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: " +contentCPolicy);
			clickElementJSWithWait(linkLearnMoreCancellationPolicy);

			tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is clicked sucessfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			verifyObjectDisplayed(frameCancellationPolicy);
			tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: " +contentCPolicy);
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
						"Cancellation Policy frame is not present");
			}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("BeginSearchPage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}
	
	public void searchAvailabilityButton()
	{
		getElementInView(buttonSearchAvailability);
		clickElementBy(buttonSearchAvailability);
		tcConfig.updateTestReporter("BeginSearchPage", "searchAvailabilityButton", Status.PASS,
				"Search Availability button is clicked");
	}

	public void clickTripSummary() {
		// TODO Auto-generated method stub
		
	}

}
