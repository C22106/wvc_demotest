package bbm.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PackageDetailsPage_IOS extends PackageDetailsPage {

	public static final Logger log = Logger.getLogger(PackageDetailsPage_IOS.class);

	public PackageDetailsPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		incentiveHeader = By.xpath("//*[@id='viewTripSummary']");
	}

	public void clickTripSummary() {
		waitUntilElementVisibleBy(driver, incentiveHeader, 120);
		clickElementBy(incentiveHeader);
		tcConfig.updateTestReporter("PackageDetailsPage", "incentiveSelect", Status.PASS,
				"Navigated to Incentive Section");
	}

}
