package bbm.pages;

import java.awt.AWTException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class AccommodationsPage extends BBMBasePage {

	public static final Logger log = Logger.getLogger(AccommodationsPage.class);
	BeginSearchPage BeginSearchPage=new BeginSearchPage(tcConfig);
	PackageDetailsPage pckDetPage = new PackageDetailsPage(tcConfig);
	public AccommodationsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	//p[contains(.,'Photo Coming Soon')]
	protected By labelWyndhamPreferred = By.xpath("//p[contains(.,'Wyndham Preferred')]");
	protected By labelContactNo = By.xpath("//p[@class='text-xxs tabletp:text-xs text-white leading-tight']");
	protected By pinMap = By.xpath("//img[@src='https://maps.gstatic.com/mapfiles/transparent.png']");
	protected By linkOpeninMaps = By.xpath("//a[contains(.,'Open In Maps')]");
	protected By buttonZoom = By.xpath("//button[@title='Zoom in']");
	protected By buttonZoomOut = By.xpath("//button[@title='Zoom out']");
	protected By headingAccommodationDetails = By.xpath("//h2[contains(.,'ACCOMMODATION DETAILS')]");
	protected By pymntCards = By.xpath("//div[@class='flex mb-1.5']");
	protected By aminitiesDetails = By.xpath("//div[@id='header-trigger']");
	protected By linkViewMoreAminities = By.xpath("//a[@class='text-xs font-semibold link-arrow -down']");
	protected By clubwyndhamLogoFooter = By.xpath("//footer//img[@alt='CLUB WYNDHAM']");
	protected By clubwyndhamLogoHeader = By.xpath("//header//img[@alt='CLUB WYNDHAM']");
	protected By listpackage = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-title']//h3");
	protected By linkBacktoAllAcommodation = By.xpath("//a[@class='link-back font-semibold' and contains(.,'Acc')]");
	protected By linkViewPropertyDetails = By
			.xpath("//a[contains(.,'View Details')]");
	//protected By buttonBookNow = By.xpath("//a[text()='  Book Now']");
	protected By buttonBookNow = By.xpath("//a[@class='button -xsmall mr-1.5' and ( contains(.,'Book Now')  or contains (.,'Stay Here')) ]");
	protected By linkModifySearch = By.xpath("//a[contains(.,'Modify Search')]");
	protected By updateCalendar = By.xpath("//div[@class='activate wyn-calendar-container']");
	protected By checkINDate_Drpdwn = By.xpath("//div[@class='activate wyn-calendar-container']/a");
	protected By nextMonth=By.xpath("//a[@title='Next']");
	protected By secondDate=By.xpath("//a[text()='2']");
	protected By doneBtn=By.xpath("//a[text()='Done']");
	protected By checkInDate=By.xpath("//div[@class='activate wyn-calendar-container']//span[contains(@class,'checkInDate')]");
	protected By guest=By.xpath("//span[contains(@class,'guestPlaceholder')]");
	protected By close=By.xpath("//a[text()='Close']");
	protected By updateSearch=By.xpath("//a[text()='Update Search']");
	
	protected By labelProgressBar = By.xpath("//div[@class='steps-desktop hidden tabletl:flex text-white']");
	
	protected By headingAvailableAccommodation = By.xpath("//h1[@class='heading6 mb-2']");
	protected By buttonSelectAccommodation = By.xpath("(//a[text()='Select Accommodations'])[2]");
	protected By labelNeedHelp = By.xpath("//p//strong[text()='Need Help Booking?']");
	protected By imageHotel = By.xpath("//a[@class='block w-full h-full']");
 protected By imageHotelphotoComingSoon = By.xpath("//p[text()='Photo Coming Soon']");
 protected By popupContinueBooking = By.xpath("//a[@id='popupContinue']");
	// protected By imageTripSummary = By.xpath("//div[@class='trip-summary__image
	// hidden tabletl:flex bg-cover bg-center items-center content-center
	// justify-center']");
	protected By availableProperty = By
			.xpath("//div[@class='hotel-details pb-1 border-b border-std tabletp:shadow mb-1 tabletl:pb-0']");
	protected By selectProperty = By.xpath("//div[@id='hotels-list']//p[@class='mb-quarter']/a[contains(text(),'"+testData.get("accomodationName")+"')]/../..//a[contains(.,'View Details')]");

	// div[@class='hotel-details shadow mb-1']
	protected By nameOfProperty = By.xpath(/*"//p[@class='mb-quarter']"*/
			"//div[@id='hotels-list']//p[@class='mb-quarter']/a");
	protected By descriptionOfProperty = By
			.xpath(/*"//p[@class='text-xs leading-tight mb-half text-gray2 hidden tabletp:block']"*/
					"//div[@id='hotels-list']//p[@class='mb-quarter']/a/../../p[4]");
	protected By addOfProperty = By.xpath(/*"//p[@class='text-sm mb-quarter']"*/
			"//div[@id='hotels-list']//p[@class='mb-quarter']/a/../../p[@class='text-sm mb-quarter']");
	protected By buttonSelectAccommodationtemp = By.xpath("//a[@class='button -fixedMobile tabletl:mb-1.5']");

	protected By linkLearnMoreCancellationPolicy = By.xpath("//section[@id='trip-summary']//a[@data-fragment='global.cancellation.LearnMore']");
	protected By contentCancellationPolicy = By.xpath("//section[@id='trip-summary']//p[contains(.,'Cancel within')]");	
	protected By frameCancellationPolicy = By.xpath("//div[@id='popupCancellation']");
	protected By frameCancellationPolicycClose = By.xpath("//*[@id='popupCancellation']/div");
	
	

	/*
	 * Method Name: freeCancellationPolicyDisplayValidation
	 * Description:This method validates free Cancellation Policy Display  throughout the booking flow
	 * Date: 25th Oct 2019
	 * Author:Syba Mallick
	 */

	public void freeCancellationPolicyDisplayValidation() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		
		waitUntilElementVisibleBy(driver, linkLearnMoreCancellationPolicy, 240);

		if (verifyObjectDisplayed(contentCancellationPolicy)) {
			String contentCPolicy = driver.findElement(contentCancellationPolicy).getText();

			tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Link Learn More is present in Cancellation Policy in Package Details Page with the content as: " +contentCPolicy);
			clickElementJSWithWait(linkLearnMoreCancellationPolicy);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifyObjectDisplayed(frameCancellationPolicy);
			tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
					"Cancellation Policy frame is displayed on cliking on in Package Details Page with the content as: " +contentCPolicy);
			} else {
				tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.FAIL,
						"Cancellation Policy frame is not present");
			}
		clickElementJSWithWait(frameCancellationPolicycClose);
		tcConfig.updateTestReporter("BBMHomepage", "freeCancellationPolicyDisplayValidation", Status.PASS,
				"Cancellation Policy frame is closed by cliking 'X' ");
	}
	
	
	
	/* * Method Name: popupModal
	 * Description:This method validates Pop UP modal in Accommodation search page
	 * Date: 21st Sept 2019
	 * Author:Syba Mallick*/
	
	public void popupModal() {
		if(verifyObjectDisplayed(popupContinueBooking))
		{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Actions action =new Actions(driver);
			action.moveToElement(driver.findElement(labelNeedHelp)).build().perform();
			//waitUntilObjectVisible(driver, popupContinueBooking, 120);
			if (verifyObjectDisplayed(popupContinueBooking)) {
				tcConfig.updateTestReporter("Accommodation Page", "popupModal", Status.PASS,
						"Pop Up Are you sure you want to leave? is present in Accommodation Page");
				clickElementJSWithWait(popupContinueBooking);
			} else {
				tcConfig.updateTestReporter("Accommodation Page", "labelContactNoValidation", Status.FAIL,
						"Pop Up Are you sure you want to leave? is not present in Accommodation Page");
				}
			}
	}

	/* * Method Name: modifySearchValidation
	 * Description:This method validates link modify search Trip Summary Card of Accommodation page and update the search
	 * Date: 29st Oct 2019
	 * Author:Kamalesh Gupta*/
	
	public void modifySearchValidation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, linkModifySearch, 120);
		/*List <WebElement> we=driver.findElements(linkModifySearch);
		clickElementJSWithWait(we.get(we.size()-1));*/
		clickElementJS(linkModifySearch);
		clickElementJS(checkINDate_Drpdwn);
		clickElementJS(nextMonth);
		clickElementJSWithWait(secondDate);
		clickElementJSWithWait(doneBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String cinDate=driver.findElement(checkInDate).getText();	
		String[] s=cinDate.split("/");
		System.out.println("first "+s[0]);
		System.out.println("second "+s[1]);
		System.out.println("third "+s[2]);
		if(Integer.parseInt(s[1])==2){
			tcConfig.updateTestReporter("Accommodation Page", "modifySearchValidation", Status.PASS,
					"Check In Date updated from Modify search link from Trip Summary Card");
		}else{
			tcConfig.updateTestReporter("Accommodation Page", "modifySearchValidation", Status.FAIL,
					"Error while updating Check in date");
		}
		
		clickElementJS(guest);
		clickElementJS(close);
		String guestCount=driver.findElement(guest).getText();
		tcConfig.updateTestReporter("Accommodation Page", "modifySearchValidation", Status.PASS,
				"Guest count are: "+guestCount);
		clickElementJS(updateSearch);
	}
	
	/*****************Updated Code******************/
	
	//protected By checkInDate = By.xpath("//p[contains(.,'Check-In Date')]");
	protected By calendarDropDown = By.xpath("//div[@class='activate wyn-calendar-container']");
	protected By guestDropDown = By.xpath("//div[@class='activate guestsData']");
	protected By guestDrpDwnActive = By.xpath("//div[@class='activate guestsData -active']");
	protected By calendarDrpDwnActive = By.xpath("//div[@class='activate wyn-calendar-container -active']");
	/* * Method Name: modifySearchVal
	 * Description:This method validates link modify search Trip Summary Card of Accommodation page and update the search
	 * Date: jan 2020
	 * Author:Unnat*/
	
	public void modifySearchVal() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, linkModifySearch, 240);
		/*List <WebElement> we=driver.findElements(linkModifySearch);
		clickElementJSWithWait(we.get(we.size()-1));*/
		getElementInView(linkModifySearch); 
		clickElementJS(linkModifySearch);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getElementInView(calendarDropDown); 
		/*if (verifyObjectDisplayed(calendarDropDown)) {
			clickElementBy(calendarDropDown);
			waitUntilObjectVisible(driver, calendarDrpDwnActive, 20);
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		//getElementInView(nextMonth); 
		clickElementJS(nextMonth);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getElementInView(secondDate); 
		clickElementJSWithWait(secondDate);*/
		pckDetPage.selectModifiedCheckInDate();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getElementInView(doneBtn); 
		clickElementJSWithWait(doneBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String cinDate=driver.findElement(checkInDate).getText();	
		String[] s=cinDate.split("/");
		System.out.println("first "+s[0]);
		System.out.println("second "+s[1]);
		System.out.println("third "+s[2]);
		String checkindate = testData.get("ModifiedCheckindate").trim();
		// To store day from date
		String day = (checkindate.split(" ")[0]).trim();
		if(Integer.parseInt(s[1])==Integer.parseInt(day)){
			tcConfig.updateTestReporter("Accommodation Page", "modifySearchValidation", Status.PASS,
					"Check In Date updated from Modify search link from Trip Summary Card");
		}else{
			tcConfig.updateTestReporter("Accommodation Page", "modifySearchValidation", Status.FAIL,
					"Error while updating Check in date");
		}
		getElementInView(guest); 
		clickElementJS(guest);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getElementInView(close); 
		clickElementJS(close);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String guestCount=driver.findElement(guest).getText();
		tcConfig.updateTestReporter("Accommodation Page", "modifySearchValidation", Status.PASS,
				"Guest count are: "+guestCount);
		getElementInView(updateSearch); 
		clickElementJS(updateSearch);
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}
	
	
	
	/*
	 * Method Name: linkViewPropertyDetailsPageNavigation
	 * Description:This method validates link View Property Details Page Navigation from Accommodation page
	 * Date: 31st July 2019
	 * Author:Syba Mallick
	 */
	public void linkViewPropertyDetailsPageNavigation() throws AWTException {
		waitUntilElementVisibleBy(driver, headingAvailableAccommodation, 120);

				
		if (verifyObjectDisplayed(headingAvailableAccommodation)) {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
					"Available Accommodation  page navigated");

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(labelContactNo)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Contact No is present in Accommodation Page");

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
						"Contact No not getting displayed in Accommodation Page");
			}

			waitUntilObjectVisible(driver, linkViewPropertyDetails, 120);
			if (verifyObjectDisplayed(linkViewPropertyDetails)) {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Options to view property details is available");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.FAIL, "Options to view property details is not available");
			}

			if (verifyObjectDisplayed(buttonBookNow)) {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Book Now button is available");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.FAIL, "Book Now button is not available");
			}

			/*List<WebElement> linkViewPropertyList = driver.findElements(linkViewPropertyDetails);
			linkViewPropertyList.get(0).click();*/
			
			clickElementBy(selectProperty);

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			///
			if (verifyObjectDisplayed(linkViewMoreAminities)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Link View More Aminities is present in Accommodation Page");
				scrollDownForElementJSBy(linkViewMoreAminities); //
				scrollUpByPixel(100);
				clickElementJSWithWait(linkViewMoreAminities);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, aminitiesDetails, 120);
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Aminity Details are displayed in Accommodation Page");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
						"Aminity Details not getting displayed in Accommodation Page");
			}

			if (verifyObjectDisplayed(pymntCards)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
						"Payment Cards option present in Accommodation Page");

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
						"Payment Cards option not getting displayed in Accommodation Page");
			}

			/*if (verifyObjectDisplayed(linkOpeninMaps)) {

				if (verifyObjectDisplayed(buttonZoom)) {
					scrollDownForElementJSBy(buttonZoom);
					scrollUpByPixel(250);
					clickElementBy(buttonZoom);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(buttonZoomOut);
					tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
							Status.PASS, "Map zoom in and Zoom Out is present and working in Accommodation Page");

				} else {
					tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
							Status.FAIL, "Map zoom in and Zoom Out not getting displayed in Accommodation Page");
				}

				clickElementJSWithWait(linkOpeninMaps);

				tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.PASS,
						"Link Open in Map is present in Accommodation Page");

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String mapURL = driver.getCurrentUrl();
				String URL = testData.get("urlMap");

				if (mapURL.contains(URL)) {

					tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.PASS,
							"Clicking on maps is navigating to Google maps");
					driver.navigate().back();

				} else {
					tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.FAIL,
							"Clicking on maps is not navigating to Google maps");
				}

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkOpeninMaps", Status.FAIL,
						"Link Open in Map is not getting displayed in Accommodation Page");
			}*/

			waitUntilObjectVisible(driver, buttonSelectAccommodationtemp, 120);

			if (verifyObjectDisplayed(buttonSelectAccommodationtemp)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Property Details page Navigation Successful"); //
				getElementInView(buttonSelectAccommodationtemp);
				// driver.findElement(buttonSelectAccommodationtemp).click();
				// clickElementBy(buttonSelectAccommodationtemp);
//				BeginSearchPage.Email_form_Validation(); //comment in case the email form not present in stage
				clickElementJSWithWait(buttonSelectAccommodationtemp);
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.PASS, "Button Select Accommodation clicked successfully");
			}

			else

			{
				tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation",
						Status.FAIL, "Button Select Accommodation not clickable");
			}

		} else

		{
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
					"Available Accommodation  page navigation unsucessful");
		}

	}

	
	/*
	 * Method Name: linkBookNowNavigation
	 * Description:This method validates link Book Now Navigation 
	 * Date: 31st July 2019
	 * Author:Syba Mallick
	 */
	public void linkBookNowNavigation() {
		waitUntilObjectVisible(driver, headingAvailableAccommodation, 120);

		if (verifyObjectDisplayed(headingAvailableAccommodation)) {
			tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation", Status.PASS,
					"Available Accommodation  page navigated");

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(labelContactNo)) {

				tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation", Status.PASS,
						"Contact No is present in Accommodation Page");

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation", Status.FAIL,
						"Contact No not getting displayed in Accommodation Page");
			}

			waitUntilObjectVisible(driver, linkViewPropertyDetails, 120);
			if (verifyObjectDisplayed(linkViewPropertyDetails)) {
				tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation",
						Status.PASS, "Options to view property details is available");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation",
						Status.FAIL, "Options to view property details is not available");
			}
			

			if (verifyObjectDisplayed(buttonBookNow)) {
				tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation",
						Status.PASS, "Book Now button is available");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation",
						Status.FAIL, "Book Now button is not available");
			}

			List<WebElement> listBookNow = driver.findElements(buttonBookNow);
			listBookNow.get(0).click();

			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else

		{
			tcConfig.updateTestReporter("AccommodationPage", "linkBookNowNavigation", Status.FAIL,
					"Available Accommodation  page navigation unsucessful");
		}

	}

	
	/*
	 * Method Name: labelWyndhamPreferredValidation
	 * Description:This method validates resort or hotel details as label Wyndham Preferred 
	 * Date: 31st July 2019
	 * Author:Syba Mallick
	 */
	public void labelWyndhamPreferredValidation() {

		waitUntilObjectVisible(driver, labelWyndhamPreferred, 120);
		if (verifyObjectDisplayed(labelWyndhamPreferred)) {
			tcConfig.updateTestReporter("PropertyDetailPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
					"Wyndham Preffered Label Displayed");
		} else {
			tcConfig.updateTestReporter("PropertyDetailPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
					"Not A Wyndham Preffered Resort");
		}

	}

	
	/*
	 * Method Name: resortDetailsValidation
	 * Description:This method validates resort or hotel details
	 * Date: 31st July 2019
	 * Author:Syba Mallick
	 */
	public void resortDetailsValidation() {
		waitUntilObjectVisible(driver, headingAvailableAccommodation, 120);
		if (verifyObjectDisplayed(headingAvailableAccommodation)) {
			tcConfig.updateTestReporter("AccommodationPage", "resortDetailsValidation", Status.PASS,
					"Available Accommodation  page navigated");

			waitUntilObjectVisible(driver, availableProperty, 120);

			tcConfig.updateTestReporter("AccommodationPage", "selectDestination", Status.PASS,
					"Destination Packages are present in the application as from Home page");

			List<WebElement> PropertyList = driver.findElements(availableProperty);
			List<WebElement> NamePropertyList = driver.findElements(nameOfProperty);
			List<WebElement> addPropertyList = driver.findElements(addOfProperty);
			List<WebElement> descriptionPropertyList = driver.findElements(descriptionOfProperty);
			for (int i = 0; i < PropertyList.size(); i++) {

				String StrPropertyDescription = descriptionPropertyList.get(i).getText();

				tcConfig.updateTestReporter("AccommodationPage", "resortDetailsValidation", Status.PASS,
						"Resort/Hotel name  is available as  " + (StrPropertyDescription)
								+ " from Available Accommodation page");

				String PropertyName = NamePropertyList.get(i).getText();

				tcConfig.updateTestReporter("AccommodationPage", "resortDetailsValidation", Status.PASS,
						"Resort/Hotel name  is available as" + (PropertyName) + "from Available Accommodation page");

				String PropertyAdd = addPropertyList.get(i).getText();

				tcConfig.updateTestReporter("AccommodationPage", "resortDetailsValidation", Status.PASS,
						"Resort/Hotel Address  is available as" + (PropertyAdd) + " from Available Accommodation page");
			}

			if (verifyObjectDisplayed(imageHotel) || (verifyObjectDisplayed(imageHotelphotoComingSoon)))
			{
				clickElementJSWithWait(imageHotel);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("AccommodationPage", "resortDetailsValidation", Status.PASS,
						"Resort is Image Present and clicking on thumbnail is navigating to Resort Details page");
			} else {
				tcConfig.updateTestReporter("AccommodationPage", "resortDetailsValidation", Status.PASS,
						"Resort Image is not Present");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			if (verifyObjectDisplayed(linkBacktoAllAcommodation)) {

				clickElementJSWithWait(linkBacktoAllAcommodation);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("AccommodationPage", "resortDetails", Status.PASS,
						"Link Back to all Accommodation is Present and clicking on link is navigating to Available Accommodation page");

			} else {
				tcConfig.updateTestReporter("AccommodationPage", "resortDetails", Status.PASS,
						"Resort Image is not Present");
			}
		}

	}

	
	/*
	 * Method Name: 
	 * Description:
	 * Date: 
	 * Author:
	 */
	public void labelContactNoValidation() {

		waitUntilObjectVisible(driver, labelContactNo, 120);

		if (verifyObjectDisplayed(labelContactNo)) {

			tcConfig.updateTestReporter("AccommodationPage", "labelContactNoValidation", Status.PASS,
					"Contact No is present in AccomodationPage");

		} else {
			tcConfig.updateTestReporter("AccommodationPage", "labelContactNoValidation", Status.FAIL,
					"Contact No is not present in Accomodation Page");
		}

	}

	
	/*
	 * Method Name: 
	 * Description:
	 * Date: 
	 * Author:
	 */
	public void logoLinktoDestinationPageValidation() {

		waitUntilObjectVisible(driver, clubwyndhamLogoHeader, 120);

		if (verifyObjectDisplayed(clubwyndhamLogoHeader)) {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
					"Club Wyndham logo is present on header in Accommodation Page");
		} else {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
					"Club Wyndham logo is not present on header in Accommodation Page");
		}

		if (verifyObjectDisplayed(clubwyndhamLogoFooter)) {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
					"Club Wyndham logo is present in footer in Accommodation Page");
		} else {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
					"Club Wyndham logo is not present on footer in Accommodation Page");
		}

		clickElementBy(clubwyndhamLogoHeader);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, listpackage, 120);

		if (verifyObjectDisplayed(listpackage)) {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.PASS,
					"Clicking on Club Wyndham logo is navigating to Destination Page");
		} else {
			tcConfig.updateTestReporter("AccommodationPage", "linkViewPropertyDetailsPageNavigation", Status.FAIL,
					"Clicking on Club Wyndham logo is not navigating to Destination Page");
		}

	}
	
	/*
	 * Method Name: labelProgressBarValidation
	 * Description:This method validates the label Progress Bar in the Begin Search Page 
	 * Date: 18th Sept 2019
	 * Author:Syba Mallick
	 */

	public void labelProgressBarValidation() {

		waitUntilObjectVisible(driver, labelProgressBar, 120);

		if (verifyObjectDisplayed(labelProgressBar)) {
			String progress = driver.findElement(labelProgressBar).getText();

			tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.PASS,
					"Progress Bar label is displayed in Begin search Page with the content as: " +progress);
			
					
			} else {
				tcConfig.updateTestReporter("BeginSearchPage", "labelProgressBarValidation", Status.FAIL,
						"Cookies not present");
			}

	}



	public void clickViewDetailsLnk() {
		// TODO Auto-generated method stub
		
	}



	public void clickbackToAccommodationsLnk() {
		// TODO Auto-generated method stub
		
	}


}
