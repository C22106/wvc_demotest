package wbw.scripts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import panoramaco.scripts.PanoramacoScripts_IOS;
import wbw.pages.WBWHomePage;
import wbw.pages.WBWHomePage_Android;
import wbw.pages.WBWHomePage_IOS;

public class WBWScripts_IOS extends TestBase {
	public static final Logger log = Logger.getLogger(WBWScripts_IOS.class);
	public String environment;
	
	@Test(dataProvider = "testData")

	public void tc01_WBW_Footer_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			WBWHomePage home = new WBWHomePage_IOS(tcconfig);
			home.launchApplication();
		
			home.scrollToFooter();
			home.selectSecondLevel();
			home.dynamicSentence();
			//home.footerSocialIcons();
			home.validateSocialMediaImages();
			home.validateLogo();
			home.validateLegalPages();
			home.validateSiteMap();
			//home.brokenLinkValidation();
}
		catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}
	
	@Test(dataProvider = "testData")
	public void TC_002_WBW_DropDownMenu_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			WBWHomePage home = new WBWHomePage_IOS(tcconfig);
			home.launchApplication();
			home.checkMobToggleMenu();
			home.validateHeaderMenu();
			
			/*Validate sub menu items and submenu links*/
			ArrayList<String> WhyWorldMarkSubmenuList = new ArrayList<String>(Arrays.asList("Using My Ownership","Pride of Ownership","Tour the club","Owner Testimonials","Buzzworthy News"));
			ArrayList<String> ResortsSubmenuList = new ArrayList<String>(Arrays.asList("Explore Resorts","Destinations","News","Vacations Unpacked", "Local Discounts"));
			ArrayList<String> OwnerGuideSubmenuList = new ArrayList<String>(Arrays.asList("Get Started", "Get Informed", "Get Ready","Get More"));
			ArrayList<String> DealsnOffersSubmenuList = new ArrayList<String>(Arrays.asList("Travel Deals", "Owner Exclusives", "Partner Offers"));
			ArrayList<String> helpSubmenuList = new ArrayList<String>(Arrays.asList("Wyndham Cares", "FAQs"));
			home.validateMenuHoverSubMenu("Why WorldMark", WhyWorldMarkSubmenuList, 5, true);
			home.validateMenuHoverSubMenu("Resorts", ResortsSubmenuList, 5, true);
			home.validateMenuHoverSubMenu("Owner Guide", OwnerGuideSubmenuList, 4, true);
			home.validateMenuHoverSubMenu("Deals & Offers", DealsnOffersSubmenuList, 3, true);
			home.validateMenuHoverSubMenu("Help", helpSubmenuList, 2, true);
			}
		catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			}
	}
	@Test(dataProvider = "testData")
	public void TC_003_WBW_Mobile_Menu(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
				WBWHomePage home = new WBWHomePage_IOS(tcconfig);
				home.launchApplication();
				home.headerMenuVal();
				home.verifyLogoandClick();
				//home.scrollDownNav();
				//home.verifySearchAction("Wyndham");
				//home.verifySearchAction("Register");
				home.verifyItemVisible("Sign-In");
				
				
		}
		catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			}
		}
	
	
}
