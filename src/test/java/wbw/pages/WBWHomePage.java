package wbw.pages;


import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWHomePage extends FunctionalComponents{

	protected String url;

	public WBWHomePage(TestConfig tcconfig) {
		super(tcconfig);
	}
	//Footer
	protected By WW =By.xpath("//a[contains(@class,'level-1 gaChecker') and contains(.,'Why WorldMark')]");
	protected By dynamicText= By.xpath("//div[contains(@class,'disclaimer')][1]");
	protected By facebookIcon =By.xpath("//img[contains(@src,'facebook')][1]");
	protected By twitterIcon =By.xpath("//img[contains(@src,'twitter')][1]");
	protected By youtubeIcon =By.xpath("//img[contains(@src,'youtube')][1]");
	protected By instagramIcon =By.xpath("//img[contains(@src,'instagram')][1]");
	protected By linkedinIcon =By.xpath("//img[contains(@src,'linkedin')][1]");
	protected By worldmarkLogo = By.xpath("//div[contains(@class,'cell')]//img[contains(@alt,'Wyndham Logo')]");
	protected By footerSocialIcons = By.xpath("//div[contains(@class,'middle-footer')]//ul[contains(@class,'menu')]");
	protected By worldmark2021 = By.xpath("(//div[contains(@class,'footerFooter')]//div[contains(@class,'copyright')])[1]");
	protected By termsofuseFooter =  By.xpath("(//ul[contains(@class,'menu')]//li//a[contains(@data-eventlabel,'Terms of Use')])[1]");
	protected By privacyNoticeFooter =By.xpath("(//ul[contains(@class,'menu')]//li//a[contains(@data-eventlabel,'Privacy Notice')])[1]");
	protected By privacySettingsFooter = By.xpath("(//ul[contains(@class,'menu')]//li//a[contains(@data-eventlabel,'Privacy Settings')])[1]");
	protected By sitemapFooter = By.xpath("(//ul[contains(@class,'menu')]//li//a[contains(@data-eventlabel,'Site Map')])[1]");
	protected By donotSellInfoFooter =By.xpath("(//ul[contains(@class,'menu')]//li//a[contains(@data-eventlabel,'Sitemap')])[1]");
	protected By breadcrumbLink = By.xpath("//ul/li[last()]/a[@data-eventname='breadCrumb']");
	
	protected By secondLevelItem =By.xpath("//a[contains(@class,'gaChecker') and contains(.,'Pride of Ownership')]");
	//header
	protected By header = By.xpath("//div[contains(@class,'footer-holiday')][1]");
	protected By headerMenu_benefits = By.xpath("(//li[contains(@class,'-item is-dropdown-submenu-parent opens-right') and @role='menuitem']/a[text()='Benefits'])[1]");
	protected By headerMenu_help = By.xpath("(//li[contains(@class,'-item is-dropdown-submenu-parent opens-right') and @role='menuitem']/a[text()='Help'])[1]");
	protected By headerSubmenu_benefits = By.xpath("(//ul/li/div/ul[@class='nested menu']/li/a[text()='Using My Ownership'])[1]");
	protected By headerMenu_Partial = By.xpath("//li[@role='menuitem']/a[text()='Benefits']");
	protected By globalHeaderLogo = By.xpath("//li[@class='global-header-logo']");
	protected By registerBtn = By.xpath("//a/button[contains(@class,'account-info--register button')]");
	protected By menuWhyWorldmark = By.xpath("//li[@aria-label='Why WorldMark']/a[text()='Why WorldMark']");
	protected By menuResorts = By.xpath("//li[@aria-label='Resorts']/a[text()='Resorts']");
	protected By menuOwnerGuide = By.xpath("//li[@aria-label='Owner Guide']/a[text()='Owner Guide']");
	protected By menuDealsnOffer = By.xpath("//li[@aria-label='Deals & Offers']/a[text()='Deals & Offers']");
	protected By menuHelp = By.xpath("//li[@aria-label='Help']/a[text()='Help']");
	
	
	public void launchApplication() {

		url = tcConfig.getTestData().get("URL");
		driver.get(url);
		pageCheck();

		tcConfig.updateTestReporter("WBWHomePage", "launchApplication", Status.PASS,
				"WBW URL was launched successfully");
	}
	
	public void mouseHover(){
//		try {
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			hoverOnElement(headerMenu_benefits);
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//		}
//		catch (Exception e) {
//			tcConfig.updateTestReporter("WBWHomePage", "mouseHover", Status.FAIL, "Error in Mouse Hover to Header Menu" + e.getMessage());
//		}
//	}
	
	
//	public void mouseHoverOnMenu(String menu, ArrayList<String> subMenuList){
//		try {
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			hoverOnElement(headerMenu_benefits);
//			waitForSometime(tcConfig.getConfig().get("LowWait"));
//		}
//		catch (Exception e) {
//			tcConfig.updateTestReporter("WBWHomePage", "mouseHover", Status.FAIL, "Error in Mouse Hover to Header Menu" + e.getMessage());
//		}	
	}
	

	/*public void validateMenuHoverSubMenu(String headerMenuItem, ArrayList<String> subMenuList, int count, boolean validateSubLink) {
		System.out.println("Inside validateMenuHoverSubMenu.........for "+ headerMenuItem);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		hoverOnElement(By.xpath("(//li[@role='menuitem']/a[contains(text(),'" + headerMenuItem + "')])[1]"));

		assertTrue(getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size() == count,
				"List size doesn't match, expected count=" + count + " | actual count="
						+ getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size());

		tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
				headerMenuItem + " menu item's sub menu count is/are matching");
		
		int submenuCount = getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size();
		//int submenuCount = subMenuList.size();
		for(int i=0; i<submenuCount;i++ )
		{
			String strSubmenu = subMenuList.get(i);
			System.out.println("submenu item is "+ strSubmenu +" for i= "+i);
			assertTrue(
					verifyObjectDisplayed(
							By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + strSubmenu + "']")),
					headerMenuItem + " menu item's sub menu item " + strSubmenu + " is not present");			
			tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
			headerMenuItem + " menu item's sub menu items " + strSubmenu + " is present");
			System.out.println("submenu item "+ strSubmenu +" is present");
//			if(validateSubLink){				
//				validateSubMenuLinks(strSubmenu);
//			}
		} 
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		hoverOnElement(registerBtn);
		
	}*/
	
	public void validateSubMenuLinks(String subMenu)
	{			
		clickElementJSWithWait(
				By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + subMenu + "']"));
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			switch (subMenu) {
			/*Benefits sub menu items*/
			case "Using My Ownership":
//				waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
//				Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
//				tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
//						"Using My Ownership submenu navigation is Successful");
				break;
			case "Pride of Ownership":

				break;
			case "Tour the club":

				break;
			case "Using my Ownership":

				break;
			case "Owner Testimonials":
				
				break;
			case "Buzzworthy News":
				
				break;

			/*Resorts sub menu items*/
			case "Explore Resorts":

				break;
			case "Destinations":

				break;
			case "News":

				break;
			case "Vacations Unpacked":
				
				break;
			case "Local Discounts":
				
				break;
				
			/*Owner Guide Sub menu*/
			case "Get Started":

				break;
			case "Get Informed":

				break;
			case "Get Ready":
				
				break;
			case "Get More":
				
				break;
				
			/*Deals n Offers Sub menu*/
			case "Travel Deals":

				break;
			case "Owner Exclusives":

				break;
			case "Partner Offers":
				
				break;
				
			/*Helps Sub menu*/
			case "Wyndham Cares":

				break;
			case "Contact Us":

				break;
			case "FAQs":
				
				break;				
			}
			navigateToMainTab(allTabs);
			pageCheck();

		} else {
			switch (subMenu) {
			/*Benefits sub menu items*/
			case "Using My Ownership":
//				waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
//				Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
//				tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
//						"Using My Ownership submenu navigation is Successful");
				break;
			case "Pride of Ownership":

				break;
			case "Tour the club":

				break;
			case "Using my Ownership":

				break;
			case "Owner Testimonials":
				
				break;
			case "Buzzworthy News":
				
				break;

			/*Resorts sub menu items*/
			case "Explore Resorts":

				break;
			case "Destinations":

				break;
			case "News":

				break;
			case "Vacations Unpacked":
				
				break;
			case "Local Discounts":
				
				break;
				
			/*Owner Guide Sub menu*/
			case "Get Started":

				break;
			case "Get Informed":

				break;
			case "Get Ready":
				
				break;
			case "Get More":
				
				break;
				
			/*Deals n Offers Sub menu*/
			case "Travel Deals":

				break;
			case "Owner Exclusives":

				break;
			case "Partner Offers":
				
				break;
				
			/*Helps Sub menu*/
			case "Wyndham Cares":

				break;
			case "Contact Us":

				break;
			case "FAQs":
				
				break;				
			}
			navigateBack();
			pageCheck();
		}	
		
	}
	
	
	public void clickSubmenuItem(){
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			hoverOnElement(headerMenu_benefits);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			hoverOnElement(headerSubmenu_benefits);
			clickElementBy(headerSubmenu_benefits);
			String currentUrl = driver.getCurrentUrl();
			Assert.assertTrue(currentUrl.contains(tcConfig.getTestData().get("BenefitsMenuLink")), "Username Field Not Present");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		catch (Exception e) {
			tcConfig.updateTestReporter("WBWHomePage", "clickSubmenuItem", Status.FAIL, "Error in Header Submenu navigation" + e.getMessage());
		}	
	}
	
	
	public void clicHelpmenuItem(){
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(headerMenu_help);
			String currentUrl = driver.getCurrentUrl();
			Assert.assertTrue(currentUrl.contains(tcConfig.getTestData().get("HelpMenuLink")), "Help page not opened");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		catch (Exception e) {
			tcConfig.updateTestReporter("WBWHomePage", "clicHelpmenuItem", Status.FAIL, "Error in Header Helpmenu navigation" + e.getMessage());
		}	
	}
					
			
	/*public void validateHeaderMenu() {

		assertTrue(verifyObjectDisplayed(menuBenefits), "Benefits menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Benefits menu item is present");
		//navigateValidateAndReturn(menuBenefits);

		assertTrue(verifyObjectDisplayed(menuResorts), "Resorts menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Resorts menu item is present");
		///navigateValidateAndReturn(menuResorts);

		assertTrue(verifyObjectDisplayed(menuOwnerGuide), "Owner Guide menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Owner Guide menu item is present");
		//navigateValidateAndReturn(menuOwnerGuide);
		
		assertTrue(verifyObjectDisplayed(menuDealsnOffer), "Deals & Offer menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Deals & Offer menu item is present");
		//navigateValidateAndReturn(menuDealsnOffer);
		
		assertTrue(verifyObjectDisplayed(menuHelp), "Help menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Help menu item is present");
		navigateValidateAndReturn(menuHelp);
	}*/
	
	/*public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href").trim();
		clickElementBy(link);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() > 1) {
			newTabNavigations(allTabs);
			String title = driver.getCurrentUrl().trim();
			//assertTrue(title.equalsIgnoreCase(href),
			assertTrue(title.contains(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			pageCheck();
		} else {
			String title = driver.getCurrentUrl().trim();
			//assertTrue(title.equalsIgnoreCase(href),
			assertTrue(title.contains(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}*/

	
	public void scrollToFooter(){
		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			scrollDownForElementJSBy(WW);
			navigateValidateAndReturn(WW);
		}
		catch (Exception e) {
			tcConfig.updateTestReporter("WBWHomePage", "scrollToFooter", Status.FAIL, "Error in Scrolling ot footer" + e.getMessage());
		}
	}

	public void selectSecondLevel(){
		assertTrue(verifyObjectDisplayed(secondLevelItem), "Second Level item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "selectSecondLevel", Status.PASS,
				"Second level item is displayed");
		navigateValidateAndReturntoPage(secondLevelItem, "worldmarktheclub");
		
	}
	
	public void navigateValidateAndReturntoPage(By link, String menu) {

		String href = getElementAttribute(link, "href").trim();
		clickElementJSWithWait(link);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.contains(menu),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			pageCheck();
		} else {
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.contains(menu),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}
	
		/*assertTrue(verifyObjectDisplayed(CB), "Wyndham Destinations menu item is not present");
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
				"Wyndham Destinations menu item is present");
		navigateValidateAndReturn(CB);*/
	
	/*public void navigateValidateAndReturn(By link) {
		String title = "";
		String mainWindow = driver.getWindowHandle();
		String href = getElementAttribute(link, "href");
		clickElementJSWithWait(link);
		waitForSometime("5");
		ArrayList<String> tabList = new ArrayList<String>(driver.getWindowHandles());
		if (tabList.size() == 2) {
			
			 * newTabNavigations(tabList); pageCheck(); title = driver.getTitle();
			 * assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
			 * "Link Navigation not correct, expected : " + href + " | Actual : " +
			 * driver.getCurrentUrl().trim());
			 * tcConfig.updateTestReporter("PanoramacoHomePage",
			 * "navigateValidateAndReturn", Status.PASS, "Page Title : " + title +
			 * " navigating to link : " + href + " successful"); navigateToMainTab(tabList);
			 * waitForSometime("5"); pageCheck();
			 
			for (String windowHandle : driver.getWindowHandles()) {
				driver.switchTo().window(windowHandle);
				if (driver.getCurrentUrl().trim().equalsIgnoreCase(href)) {
					tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
							"Page Title : " + title + " navigating to link : " + href + " successful");
					break;
				}
			}
			driver.close();
			driver.switchTo().window(mainWindow);

		} else {
			pageCheck();
			waitForSometime("5");
			title = driver.getTitle();
			assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + driver.getCurrentUrl().trim());
			tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page Title : " + title + " navigating to link : " + href + " successful");

			navigateBack();
			waitForSometime("5");
			pageCheck();
		}

	}*/
	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("li"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("#")) {
					System.out.println("URL contians #");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it."); report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}
	public void dynamicSentence(){
		

			/*assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
			tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
					"Correct banner is displayed");*/

			String timeshareSentence = getElementText(dynamicText);
			assertTrue(!timeshareSentence.trim().equals(""), "Timeshare Sentence absent");
			tcConfig.updateTestReporter("WBWHomePage", "dynamicSentence", Status.PASS, "Timeshare Sentence is displayed");
			/*getElementInView(bannerHeader);
			String bannerHeading = getElementText(bannerHeader).trim();
			assertTrue(bannerHeading.length() > 0, "Banner Header is not present");
			String bannerSubHeading = getElementText(bannerSubDescription).trim();
			assertTrue(bannerSubHeading.length() > 0, "Banner Sub Description is not present");
			tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
					"Banner Header and Sub Description is displayed");*/
		
	}
	public void validateSocialMediaImages()

	{

		getElementInView(facebookIcon);
		assertTrue(getElementAttribute(facebookIcon, "src").contains(".svg"), "facebook Image source not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
				"facebook Image source present");
		assertTrue(getElementAttribute(twitterIcon, "src").contains(".svg"), "twitter Image source not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
				"Twitter Image source present");
		assertTrue(getElementAttribute(youtubeIcon, "src").contains(".svg"),
				"youtube Image source not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
				"youtube Image source present");
		assertTrue(getElementAttribute(instagramIcon, "src").contains(".svg"),
				"instagram Image source not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
				"instagram Image source present");
		assertTrue(getElementAttribute(linkedinIcon, "src").contains(".svg"), "linkedIn Image source not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
				"linkedIn Image source present");
		

	}
	
	public void footerSocialIcons() {

		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());

			for (int i = 0; i < socialIconsList.size(); i++) {
				String strSiteName = null;

				switch (i) {
				case 0:
					strSiteName = "facebook";
					break;
				case 1:
					strSiteName = "twitter";
					break;
				case 2:
					strSiteName = "youtube";
					break;
				case 3:
					strSiteName = "instagram";
					break;
				case 4:
					strSiteName = "linkedin";
					break;

				default:
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}

				clickElementJS(socialIconsList.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getTitle().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}

	}
	
	public void validateLogo() {
		url = tcConfig.getTestData().get("URL");
		assertTrue(verifyObjectDisplayed(worldmarkLogo), "Worldmark logo is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateLogo", Status.PASS,
				"Worldmark logo is displayed");
		
		String homepageUrl = driver.getCurrentUrl();
		clickElementBy(worldmarkLogo);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(driver.getCurrentUrl().contains(homepageUrl), "Home page not opened after WBW logo clicked");	
	}
	
	public void validateLegalPages(){
		getElementInView(worldmark2021);
		String worldmarkText = getElementText(worldmark2021);
		assertTrue(!worldmarkText.trim().equals(""), "WorldMark 2021 is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
				"WorldMark 2021 is present");
		
		/*String allrightreservedText = getElementText(allrightText);
		assertTrue(!allrightreservedText.trim().equals(""), "All Rights Reserved text is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
				"All right reserved text is present");*/
		
		assertTrue(verifyObjectDisplayed(termsofuseFooter), "Terms of Use is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
				"Terms of Use is displayed");
		//navigateValidateAndReturn(termsofuseFooter);
		
		assertTrue(verifyObjectDisplayed(privacyNoticeFooter), "Privacy Notice is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
				"Privacy Notice is displayed");
		//navigateValidateAndReturn(privacyNoticeFooter);
		
		assertTrue(verifyObjectDisplayed(privacySettingsFooter), "Privacy Settings is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
				"Privacy Settings is displayed");
		//navigateValidateAndReturn(privacySettingsFooter);
		
		assertTrue(verifyObjectDisplayed(donotSellInfoFooter), "Do Not Sell My Personal Information is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
				"Do Not Sell My Personal Information is displayed");
		//navigateValidateAndReturn(donotSellInfoFooter);
		
	}
	
	public void validateSiteMap(){
		assertTrue(verifyObjectDisplayed(sitemapFooter), "Site Map is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateSiteMap", Status.PASS,
				"Site Map is displayed");
		
	}
	//TC004
	
		protected By headerLogo = By.xpath("//li[@class='global-header-logo']");
		protected By signIn = By.xpath("//a[contains(@class,'account-info--sign-in button') and text()='Sign In']");
		protected By searchIcon = By.xpath("//button[contains(@class,'button siteSearch')]");
		protected By searchTextField = By.xpath("//input[contains(@class,'input searchQuery open')]");
		
		public void verifyLogoandClick()
		{
			try{
				String homepageUrl = driver.getCurrentUrl();
				clickElementBy(headerLogo);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				Assert.assertTrue(driver.getCurrentUrl().contains(homepageUrl), "Home page not opened afetr WBW logo clicked");		
			}
			catch (Exception e) {
				tcConfig.updateTestReporter("WBWHomePage", "verifyLogoandClick", Status.FAIL, "Error in WBW Logo click" + e.getMessage());
			}
		
		}
		
		public void verifyItemVisible(String strItemname)
		{
			try{
				waitUntilObjectVisible(driver, registerBtn, 5);
				
				switch (strItemname) {
					case "Register":			
						Assert.assertTrue(verifyObjectDisplayed(registerBtn), "Register button is not displayed");
						tcConfig.updateTestReporter("WBWHomePage", "verifyItemVisible", Status.PASS,
								"Register button is displayed");
					break;
					
					case "Sign-In":
						Assert.assertTrue(verifyObjectDisplayed(signIn), "Sign-in button is not displayed");
						tcConfig.updateTestReporter("WBWHomePage", "verifyItemVisible", Status.PASS,
								"Sign-in button is displayed");
					break;			
				}
			}
			catch (Exception e) {
				
			}
		}
		
		public void verifySearchAction()
		{
			try{
				waitUntilObjectVisible(driver, registerBtn, 5);
				
				Assert.assertTrue(verifyObjectDisplayed(searchIcon), "Search icon is not displayed");
				tcConfig.updateTestReporter("WBWHomePage", "verifySearchAction", Status.PASS,
						"Search icon is displayed");
				clickElementBy(searchIcon);
				Assert.assertTrue(verifyObjectDisplayed(searchTextField), "Search text field is not displayed");
				tcConfig.updateTestReporter("WBWHomePage", "verifySearchAction", Status.PASS,
						"Search text field is displayed");
				//Add search text and search result page is opening
				//driver.findElement(searchTextField).sendKeys("WorldMark");
				driver.findElement(searchTextField).click();
				driver.findElement(searchTextField).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				//waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(searchIcon);
			}
			catch (Exception e) {
				
			}
		}
		
		
		public void validateMenuHoverSubMenu(String headerMenuItem, ArrayList<String> subMenuList, int count, boolean validateSubLink) {
			System.out.println("Inside validateMenuHoverSubMenu.........for "+ headerMenuItem);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			hoverOnElement(By.xpath("(//li[@role='menuitem']/a[contains(text(),'" + headerMenuItem + "')])[1]"));

			assertTrue(getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size());

			tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu count is/are matching");
			
			int submenuCount = getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size();
			//int submenuCount = subMenuList.size();
			for(int i=0; i<submenuCount;i++ )
			{
				String strSubmenu = subMenuList.get(i);
				System.out.println("submenu item is "+ strSubmenu +" for i= "+i);
				assertTrue(
						verifyObjectDisplayed(
								By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + strSubmenu + "']")),
						headerMenuItem + " menu item's sub menu item " + strSubmenu + " is not present");			
				tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
				headerMenuItem + " menu item's sub menu items " + strSubmenu + " is present");
				System.out.println("submenu item "+ strSubmenu +" is present");
//				if(validateSubLink){				
//					//validateSubMenuLinks(strSubmenu); /*we may discard this method, instead will use below "navigateValidateAndReturn()"*/
//					navigateValidateAndReturn(By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + strSubmenu + "']"));
//					waitForSometime(tcConfig.getConfig().get("LowWait"));
//					hoverOnElement(By.xpath("(//li[@role='menuitem']/a[contains(text(),'" + headerMenuItem + "')])[1]"));
//				}
			} 
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			hoverOnElement(registerBtn);
			
		}

	//	
//		public void validateSubMenuLinks(String subMenu)
//		{			
//			clickElementJSWithWait(
//					By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + subMenu + "']"));
//			pageCheck();
//			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
//			if (allTabs.size() == 2) {
//				newTabNavigations(allTabs);
//				switch (subMenu) {
//				/*Benefits sub menu items*/
//				case "Using My Ownership":
////					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
////					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
////					tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
////							"Using My Ownership submenu navigation is Successful");
//					break;
//				case "Pride of Ownership":
	//
//					break;
//				case "Tour the club":
	//
//					break;
//				case "Using my Ownership":
	//
//					break;
//				case "Owner Testimonials":
//					
//					break;
//				case "Buzzworthy News":
//					
//					break;
	//
//				/*Resorts sub menu items*/
//				case "Explore Resorts":
	//
//					break;
//				case "Destinations":
	//
//					break;
//				case "News":
	//
//					break;
//				case "Vacations Unpacked":
//					
//					break;
//				case "Local Discounts":
//					
//					break;
//					
//				/*Owner Guide Sub menu*/
//				case "Get Started":
	//
//					break;
//				case "Get Informed":
	//
//					break;
//				case "Get Ready":
//					
//					break;
//				case "Get More":
//					
//					break;
//					
//				/*Deals n Offers Sub menu*/
//				case "Travel Deals":
	//
//					break;
//				case "Owner Exclusives":
	//
//					break;
//				case "Partner Offers":
//					
//					break;
//					
//				/*Helps Sub menu*/
//				case "Wyndham Cares":
	//
//					break;
//				case "Contact Us":
	//
//					break;
//				case "FAQs":
//					
//					break;				
//				}
//				navigateToMainTab(allTabs);
//				pageCheck();
	//
//			} else {
//				switch (subMenu) {
//				/*Benefits sub menu items*/
//				case "Using My Ownership":
////					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
////					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
////					tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
////							"Using My Ownership submenu navigation is Successful");
//					break;
//				case "Pride of Ownership":
	//
//					break;
//				case "Tour the club":
	//
//					break;
//				case "Using my Ownership":
	//
//					break;
//				case "Owner Testimonials":
//					
//					break;
//				case "Buzzworthy News":
//					
//					break;
	//
//				/*Resorts sub menu items*/
//				case "Explore Resorts":
	//
//					break;
//				case "Destinations":
	//
//					break;
//				case "News":
	//
//					break;
//				case "Vacations Unpacked":
//					
//					break;
//				case "Local Discounts":
//					
//					break;
//					
//				/*Owner Guide Sub menu*/
//				case "Get Started":
	//
//					break;
//				case "Get Informed":
	//
//					break;
//				case "Get Ready":
//					
//					break;
//				case "Get More":
//					
//					break;
//					
//				/*Deals n Offers Sub menu*/
//				case "Travel Deals":
	//
//					break;
//				case "Owner Exclusives":
	//
//					break;
//				case "Partner Offers":
//					
//					break;
//					
//				/*Helps Sub menu*/
//				case "Wyndham Cares":
	//
//					break;
//				case "Contact Us":
	//
//					break;
//				case "FAQs":
//					
//					break;				
//				}
//				navigateBack();
//				pageCheck();
//			}	
//			
//		}
					
				
		public void validateHeaderMenu() {

			assertTrue(verifyObjectDisplayed(menuWhyWorldmark), "Why WorldMark menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Why WorldMark menu item is present");
			navigateValidateAndReturn(menuWhyWorldmark);

			assertTrue(verifyObjectDisplayed(menuResorts), "Resorts menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Resorts menu item is present");
			//navigateValidateAndReturn(menuResorts);

			assertTrue(verifyObjectDisplayed(menuOwnerGuide), "Owner Guide menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Owner Guide menu item is present");
			navigateValidateAndReturn(menuOwnerGuide);
			
			assertTrue(verifyObjectDisplayed(menuDealsnOffer), "Deals & Offer menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Deals & Offer menu item is present");
			navigateValidateAndReturn(menuDealsnOffer);
			
			assertTrue(verifyObjectDisplayed(menuHelp), "Help menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Help menu item is present");
			navigateValidateAndReturn(menuHelp);
		}
		
		public void navigateValidateAndReturn(By link) {

			String href = getElementAttribute(link, "href");
			String title = getElementAttribute(link, "data-eventlabel");
			clickElementJSWithWait(link);
			pageCheck();
			assertTrue(getElementAttribute(breadcrumbLink, "href").trim().equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : "
							+ getElementAttribute(breadcrumbLink, "href").trim());
			tcConfig.updateTestReporter("TimeshareHomePage", "navigateValidateAndReturn", Status.PASS,
					"Latest News Title : " + title + " navigating to link : " + href + " successful");

			navigateBack();
			pageCheck();

		}
		
		/*public void navigateValidateAndReturn(By link) {
			String title = "";
			String mainWindow = driver.getWindowHandle();
			String href = getElementAttribute(link, "href");
			clickElementJSWithWait(link);
			waitForSometime("5");
			ArrayList<String> tabList = new ArrayList<String>(driver.getWindowHandles());
			if (tabList.size() == 2) {
				
				 * newTabNavigations(tabList); pageCheck(); title = driver.getTitle();
				 * assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
				 * "Link Navigation not correct, expected : " + href + " | Actual : " +
				 * driver.getCurrentUrl().trim());
				 * tcConfig.updateTestReporter("PanoramacoHomePage",
				 * "navigateValidateAndReturn", Status.PASS, "Page Title : " + title +
				 * " navigating to link : " + href + " successful"); navigateToMainTab(tabList);
				 * waitForSometime("5"); pageCheck();
				 
				for (String windowHandle : driver.getWindowHandles()) {
					driver.switchTo().window(windowHandle);
					if (driver.getCurrentUrl().trim().equalsIgnoreCase(href)) {
						tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
								"Page Title : " + title + " navigating to link : " + href + " successful");
						break;
					}
				}
				driver.close();
				driver.switchTo().window(mainWindow);

			} else {
				pageCheck();
				waitForSometime("5");
				title = driver.getTitle();
				assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
						"Link Navigation not correct, expected : " + href + " | Actual : " + driver.getCurrentUrl().trim());
				tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page Title : " + title + " navigating to link : " + href + " successful");

				navigateBack();
				waitForSometime("5");
				pageCheck();
			}

		}*/
		/*public void navigateValidateAndReturn(By link) {

			String href = getElementAttribute(link, "href").trim();
			clickElementBy(link);
			pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() > 1) {
				newTabNavigations(allTabs);
				String title = driver.getCurrentUrl().trim();
				//assertTrue(title.equalsIgnoreCase(href),
				assertTrue(title.contains(href),
						"Link Navigation not correct, expected : " + href + " | Actual : " + title);
				tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page: " + title + " navigating to link : " + href + " successful");
				navigateToMainTab(allTabs);
				pageCheck();
			} else {
				String title = driver.getCurrentUrl().trim();
				//assertTrue(title.equalsIgnoreCase(href),
				assertTrue(title.contains(href),
						"Link Navigation not correct, expected : " + href + " | Actual : " + title);
				tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page: " + title + " navigating to link : " + href + " successful");
				navigateBack();
				pageCheck();
			}

		}*/
	public void checkMobToggleMenu() {
		// is used for mobile devices, function body declared in child class. 
		
	}
	public void headerMenuVal() {
		// is used for mobile devices, function body declared in child class. 
		
	}
	public void scrollDownNav() {
		// is used for mobile devices, function body declared in child class. 
		
	}
	
	public void openSubmenuItem(String strMenu, String strSubmenu)
	{
		try{
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			hoverOnElement(By.xpath("(//li[@role='menuitem']/a[contains(text(),'" + strMenu + "')])[1]"));

			assertTrue(
					verifyObjectDisplayed(
							By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + strSubmenu + "']")),
					strMenu + " menu item's sub menu item " + strSubmenu + " is not present");			
			tcConfig.updateTestReporter("WBWHomePage", "openSubmenuItem", Status.PASS,
					strMenu + " menu item's sub menu items " + strSubmenu + " is present");
			
			clickElementBy(By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + strSubmenu + "']"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		catch (Exception e) {
			tcConfig.updateTestReporter("WBWHomePage", "openSubmenuItem", Status.FAIL, "Error in WBW Logo click" + e.getMessage());
		}
	
	}
	
	public void verifySearchAction(String strSearchKey)
	{
		try{
			waitUntilObjectVisible(driver, registerBtn, 5);
			
			Assert.assertTrue(verifyObjectDisplayed(searchIcon), "Search icon is not displayed");
			tcConfig.updateTestReporter("WBWHomePage", "verifySearchAction", Status.PASS,
					"Search icon is displayed");
			clickElementBy(searchIcon);
			Assert.assertTrue(verifyObjectDisplayed(searchTextField), "Search text field is not displayed");
			tcConfig.updateTestReporter("WBWHomePage", "verifySearchAction", Status.PASS,
					"Search text field is displayed");
			//Add search text and search result page is opening
			driver.findElement(searchTextField).sendKeys(strSearchKey);
			clickElementBy(searchIcon);
		}
		catch (Exception e) {
			
		}
	}
	
}
