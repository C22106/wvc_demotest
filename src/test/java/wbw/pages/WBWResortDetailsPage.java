package wbw.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWResortDetailsPage extends FunctionalComponents{

	protected String url;
	public static final Logger log = Logger.getLogger(WBWResortDetailsPage.class.getName());
	
	public WBWResortDetailsPage(TestConfig tcconfig) {
		super(tcconfig);
	}

	protected By breadcrums = By.xpath("//ul[@class='breadcrumbs']/li/a[text()='Home']");
	protected By resortType = By.xpath("//div[contains(@class,'resort-type')]");
	protected By resortName = By.xpath("//div[contains(@class,'resort-name')]");
	protected By resortAddress = By.xpath("//div[contains(@class,'resort-address')]");
	protected By ratingAndReview = By.xpath("//div[contains(@class,'resort-address')]//following-sibling::iframe[@title='Reviews of this location']");
	protected By accesibleFeature = By.xpath("//ul/li/a[@data-eventlabel='Accessible Features']/div[text()='Accessible Features']");
	protected By accessibleFeatureList = By.xpath("//a[@data-eventlabel='Accessible Features']//following-sibling::div/section/div/div/ul/ul/li");
	
	public void checkResortDetails()
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(breadcrums);
		
		Assert.assertTrue(verifyObjectDisplayed(resortType), "Resort type is not displayed");
		tcConfig.updateTestReporter("WBWResortDetailsPage", "checkResortDetails", Status.PASS,
				"Resort type is displayed");
		
		Assert.assertTrue(verifyObjectDisplayed(resortName), "Resort name is not displayed");
		tcConfig.updateTestReporter("WBWResortDetailsPage", "checkResortDetails", Status.PASS,
				"Resort name is displayed");
		
		Assert.assertTrue(verifyObjectDisplayed(resortAddress), "Resort address is not displayed");
		tcConfig.updateTestReporter("WBWResortDetailsPage", "checkResortDetails", Status.PASS,
				"Resort address is displayed");
		
		Assert.assertTrue(verifyObjectDisplayed(ratingAndReview), "Resort type is not displayed");
		tcConfig.updateTestReporter("WBWResortDetailsPage", "checkResortDetails", Status.PASS,
				"Resort rating and review is displayed");
	}
	
	public void clickAccessibleFeature()
	{
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		scrollDownForElementJSBy(breadcrums);
		
		Assert.assertTrue(verifyObjectDisplayed(accesibleFeature), "Accessible feature is not displayed");
		tcConfig.updateTestReporter("WBWResortDetailsPage", "clickAccessibleFeature", Status.PASS,
				"Accessible feature is displayed");
		clickElementBy(accesibleFeature);
	}
	
	public void checkGuaranteedAccessibleFeature()
	{
		List<WebElement> accessibleList = getList(accessibleFeatureList);
		boolean guaranteedAccessibleFeature = false;
		for (int textCount = 0; textCount < accessibleList.size(); textCount++) {
			if ((accessibleList).get(textCount).getText().toLowerCase()
					.contains(testData.get("GuaranteedAccessiblity").toLowerCase())) {
				tcConfig.updateTestReporter("WBWResortDetailsPage", "checkGuaranteedAccessibleFeature",
						Status.PASS,
						"Accessible featue displayed : " + (accessibleList).get(textCount).getText()
								+ " as guaranteed feature ");
				guaranteedAccessibleFeature = true;
			}
		}
		if(!guaranteedAccessibleFeature)
		{
			tcConfig.updateTestReporter("WBWResortDetailsPage", "checkGuaranteedAccessibleFeature",
					Status.FAIL, "Guaranteed accessible featue not present");
		}
	}
	
	public void checkNonGuaranteedAccessibleFeature()
	{
		List<WebElement> accessibleList = getList(accessibleFeatureList);
		boolean nonGuaranteedAccessibleFeature = false;
		for (int textCount = 0; textCount < accessibleList.size(); textCount++) {
			if ((accessibleList).get(textCount).getText().toLowerCase()
					.contains(testData.get("NonGuaranteedAccessiblity").toLowerCase())) {
				tcConfig.updateTestReporter("WBWResortDetailsPage", "checkGuaranteedAccessibleFeature",
						Status.PASS,
						"Accessible featue displayed : " + (accessibleList).get(textCount).getText()
								+ " as non-guaranteed feature ");
				nonGuaranteedAccessibleFeature = true;
			}
		}
		if(!nonGuaranteedAccessibleFeature)
		{
			tcConfig.updateTestReporter("WBWResortDetailsPage", "checkGuaranteedAccessibleFeature",
					Status.FAIL, "Non-guaranteed accessible featue not present");
		}
	}
	
}
