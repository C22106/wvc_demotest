package wbw.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import org.testng.Assert;



public class WBWHomePage_Android extends WBWHomePage {

	
	
	public WBWHomePage_Android(TestConfig tcconfig) {
		super(tcconfig);
		menuWhyWorldmark = By.xpath("//a[@class='mobile-navigation__list-item--title' and text()='Why WorldMark']");
		menuResorts = By.xpath("//a[@class='mobile-navigation__list-item--title' and text()='Resorts']");
		menuOwnerGuide = By.xpath("//a[@class='mobile-navigation__list-item--title' and text()='Owner Guide']");
		menuDealsnOffer = By.xpath("//a[@class='mobile-navigation__list-item--title' and text()='Deals & Offers']");
		menuHelp = By.xpath("//a[@class='mobile-navigation__list-item--title' and text()='Help']");
		
		headerSubmenu_benefits = By.xpath("//ul[contains(@class,'submenu is-accordion')]/li/a[text()='Using My Ownership']");	
		
		
	}
	protected By hamburgerMenu = By.xpath("(//button[contains(@class,'hamburger-menu')])[1]");
	protected By whyWorldMarkDropdown = By.xpath("(//li[contains(@class,'mobile-navigation__list')]//button[contains(@class,'submenu-toggle')])[1]");
	protected By resortsToggleMenu = By.xpath("//li/a[contains(text(),'Resorts')]/..//button");
	protected By ownerGuideToggleMenu = By.xpath("//li/a[contains(text(),'Owner Guide')]/..//button");
	protected By dealsnOffersToggleMenu = By.xpath("//li/a[contains(text(),'Deals & Offers')]/..//button");
	protected By helpToggleMenu = By.xpath("//li/a[contains(text(),'Help')]/..//button");
	protected By wbwLogo = By.xpath("//div[contains(@class,'mobile-logo')][1]");
	protected By searchMenu =By.xpath("//input[contains(@class,'mobile-search')][1]");
	protected By signInCTA= By.xpath("//a[@class='button sign-in' and contains(.,'Sign In')]");
	protected By publicationsLink=By.xpath("//li//a[@class='gaChecker top-links' and contains(.,'Publications')]/../a");
	protected By searchIcon = By.xpath("//button[contains(@class,'icon siteSearch')]");
	protected By searchTextField =By.xpath("//input[contains(@class,'mobile-search__input searchQuery')]");
	protected By headerLogo = By.xpath("//div[@class='dam-icon mobile-logo'][1]");
	protected By signIn = By.xpath("//a[contains(@class,'button sign-in') and text()='Sign In'][1]");
	protected By headerSubmenu_benefits = By.xpath("(//ul/li/a[@class='level-2' and text()='Using My Ownership'])[1]");
	protected By secondLevelItem = By.xpath("//li[contains(@class,'is-submenu-item') and contains(.,'Pride of Ownership')][1]");
	
	private void openCloseHamburgerMenu(){
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	clickElementBy(hamburgerMenu);
}
		
		@Override
		public void scrollToFooter(){
			try {
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				scrollDownForElementJSBy(WW);
				navigateValidateAndReturn(WW);
			}
			catch (Exception e) {
				tcConfig.updateTestReporter("WBWHomePage", "scrollToFooter", Status.FAIL, "Error in Scrolling ot footer" + e.getMessage());
			}
		}
		@Override
		public void selectSecondLevel(){
			openCloseHamburgerMenu();
			clickElementBy(whyWorldMarkDropdown);
			assertTrue(verifyObjectDisplayed(secondLevelItem), "Second Level item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "selectSecondLevel", Status.PASS,
					"Second level item is displayed");
			openCloseHamburgerMenu();
			//navigateValidateAndReturn(secondLevelItem);
			
		}
			/*assertTrue(verifyObjectDisplayed(CB), "Wyndham Destinations menu item is not present");
			tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterMenu", Status.PASS,
					"Wyndham Destinations menu item is present");
			navigateValidateAndReturn(CB);*/
		
		
		/*@Override
		public void navigateValidateAndReturn(By link) {
			String title = "";
			String mainWindow = driver.getWindowHandle();
			String href = getElementAttribute(link, "href");
			clickElementJSWithWait(link);
			waitForSometime("5");
			ArrayList<String> tabList = new ArrayList<String>(driver.getWindowHandles());
			if (tabList.size() == 2) {
				
				 * newTabNavigations(tabList); pageCheck(); title = driver.getTitle();
				 * assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
				 * "Link Navigation not correct, expected : " + href + " | Actual : " +
				 * driver.getCurrentUrl().trim());
				 * tcConfig.updateTestReporter("PanoramacoHomePage",
				 * "navigateValidateAndReturn", Status.PASS, "Page Title : " + title +
				 * " navigating to link : " + href + " successful"); navigateToMainTab(tabList);
				 * waitForSometime("5"); pageCheck();
				 
				for (String windowHandle : driver.getWindowHandles()) {
					driver.switchTo().window(windowHandle);
					if (driver.getCurrentUrl().trim().equalsIgnoreCase(href)) {
						tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
								"Page Title : " + title + " navigating to link : " + href + " successful");
						break;
					}
				}
				driver.close();
				driver.switchTo().window(mainWindow);

			} else {
				pageCheck();
				waitForSometime("5");
				title = driver.getTitle();
				assertTrue(driver.getCurrentUrl().trim().equalsIgnoreCase(href),
						"Link Navigation not correct, expected : " + href + " | Actual : " + driver.getCurrentUrl().trim());
				tcConfig.updateTestReporter("PanoramacoHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page Title : " + title + " navigating to link : " + href + " successful");

				navigateBack();
				waitForSometime("5");
				pageCheck();
			}

		}*/
		
		@Override
		public void brokenLinkValidation() throws InterruptedException {

			String url = "";
			// String xpath = "";
			String linkText = "";
			int numberOfLinksTested = 0;
			int numberOfLinksBroken = 0;
			int numberOfLinksWithServerSideErrors = 0;
			int numberOfLinksWithClientSideErrors = 0;
			int numberOfLinksRedirected = 0;
			int numberOfMalformedURL = 0;
			HttpURLConnection con = null;
			int respCode = 200;
			String respMessage = "";
			List<WebElement> links = driver.findElements(By.tagName("li"));
			int totalNumberOfLinksPresentInThisPage = links.size();
			Iterator<WebElement> it = links.iterator();
			boolean validLinkReportingToggleEnabled = true; // will report valid
															// links when this
															// toggle is set to true

			tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
					"Navigated to Associated page", false);

			log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
			while (it.hasNext()) {
				try {
					WebElement linkElement = it.next();
					linkText = linkElement.getText();
					url = linkElement.getAttribute("href");

					if (url == null || url.isEmpty()) {
						System.out.println("URL is either not configured for anchor tag or it is empty");
						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
								"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
						continue;
					}

					if (url.contains("#")) {
						System.out.println("URL contians #");
						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
								"URL:" + url + ": contains HTML", false);
						continue;
					}

					/*
					 * if(!url.startsWith(homePage)){ System.out.println(
					 * "URL belongs to another domain, skipping it."); report.info("URL:"+url +
					 * ": URL belongs to another domain, skipping it."); continue; }
					 */

					con = (HttpURLConnection) (new URL(url).openConnection());
					con.setRequestMethod("HEAD");
					con.connect();

					respCode = con.getResponseCode();
					respMessage = con.getResponseMessage();

					if (respCode == 400 || respCode == 404) {
						System.out.println("Validating URL:" + url);
						log.info("Validating URL:" + url);

						if (linkText.isEmpty()) {
							log.info("Link text :" + "[No text is associated with this link]");
						} else {
							log.info("Link text :" + linkText);
						}

						System.out.println(url + " is a broken link");
						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
								"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
										+ "]: URL is a broken link. ",
								false);
						numberOfLinksBroken++;
					}
					if (respCode >= 500) {
						System.out.println("Validating URL:" + url);
						log.info("Validating URL:" + url);

						if (linkText.isEmpty()) {
							log.info("Link text :" + "[No text is associated with this link]");
						} else {
							log.info("Link text :" + linkText);
						}

						System.out.println(url + " is a link with server side error");

						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
								"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
										+ "]: URL is a link having server side error. ",
								false);
						numberOfLinksWithServerSideErrors++;
					} else if (respCode > 400 && respCode != 404 && respCode < 500) {
						System.out.println("Validating URL:" + url);
						log.info("Validating URL:" + url);

						if (linkText.isEmpty()) {
							log.info("Link text :" + "[No text is associated with this link]");
						} else {
							log.info("Link text :" + linkText);
						}

						System.out.println(url + " is a potentially broken link");

						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
								"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
										+ "]: URL is a potentially broken link, generating runtime error. ",
								false);
						numberOfLinksWithClientSideErrors++;
					} else if (respCode >= 300 && respCode < 400) {
						System.out.println("Validating URL:" + url);
						log.info("Validating URL:" + url);

						if (linkText.isEmpty()) {
							log.info("Link text :" + "[No text is associated with this link]");
						} else {
							log.info("Link text :" + linkText);
						}

						System.out.println(url + " is a redirect link");
						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
								"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
										+ "]: URL is a redirect link. ",
								false);
						numberOfLinksRedirected++;
					} // Valid link reporting is disabled by default
					else if (respCode < 300 && validLinkReportingToggleEnabled) {

						System.out.println("Validating URL:" + url);
						log.info("Validating URL:" + url);

						if (linkText.isEmpty()) {
							log.info("Link text :" + "[No text is associated with this link]");
						} else {
							log.info("Link text :" + linkText);
						}

						System.out.println(url + " is a valid link");

						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
								"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
										+ "]: URL is a valid link. ",
								false);
					}
				} catch (Exception e) {

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"Exception while validating URL:" + url + ":" + e, false);
					if (e.toString().contains("MalformedURLException")) {
						numberOfMalformedURL++;
					}
				}
				numberOfLinksTested++;
				// Thread.sleep(1000);
			}

			log.info("Number of Links Tested: " + numberOfLinksTested);
			log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
			log.info("Number of links with Server side runtime errors, http response code [>=500]: "
					+ numberOfLinksWithServerSideErrors);
			log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
					+ numberOfLinksWithClientSideErrors);
			log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
			log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
			log.info("Completed scanning page: " + driver.getCurrentUrl());

		}
		
		@Override
		public void dynamicSentence(){
			

				/*assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
				tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
						"Correct banner is displayed");*/
			openCloseHamburgerMenu();
				String timeshareSentence = getElementText(dynamicText);
				assertTrue(!timeshareSentence.trim().equals(""), "Timeshare Sentence absent");
				tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS, "Timeshare Sentence is displayed");
				/*getElementInView(bannerHeader);
				String bannerHeading = getElementText(bannerHeader).trim();
				assertTrue(bannerHeading.length() > 0, "Banner Header is not present");
				String bannerSubHeading = getElementText(bannerSubDescription).trim();
				assertTrue(bannerSubHeading.length() > 0, "Banner Sub Description is not present");
				tcConfig.updateTestReporter("PanoramacoHomePage", "validateBanners", Status.PASS,
						"Banner Header and Sub Description is displayed");*/
			
		}
		
		@Override
		public void validateSocialMediaImages()

		{

			getElementInView(facebookIcon);
			assertTrue(getElementAttribute(facebookIcon, "src").contains(".svg"), "facebook Image source not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
					"facebook Image source present");
			assertTrue(getElementAttribute(twitterIcon, "src").contains(".svg"), "twitter Image source not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
					"Twitter Image source present");
			assertTrue(getElementAttribute(youtubeIcon, "src").contains(".svg"),
					"youtube Image source not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
					"youtube Image source present");
			assertTrue(getElementAttribute(instagramIcon, "src").contains(".svg"),
					"instagram Image source not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
					"instagram Image source present");
			assertTrue(getElementAttribute(linkedinIcon, "src").contains(".svg"), "linkedIn Image source not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateSocialMediaImages", Status.PASS,
					"linkedIn Image source present");
			

		}
		
		@Override
		public void footerSocialIcons() {

			if (verifyObjectDisplayed(footerSocialIcons)) {

				List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
				tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
						"Social Icons Present in the Footer,Total: " + socialIconsList.size());

				for (int i = 0; i < socialIconsList.size(); i++) {
					String strSiteName = null;

					switch (i) {
					case 0:
						strSiteName = "facebook";
						break;
					case 1:
						strSiteName = "twitter";
						break;
					case 2:
						strSiteName = "youtube";
						break;
					case 3:
						strSiteName = "instagram";
						break;
					case 4:
						strSiteName = "linkedin";
						break;

					default:
						tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
								"Invalid Social Site");
						break;
					}

					clickElementJS(socialIconsList.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (driver.getTitle().toUpperCase().contains(strSiteName)) {
						tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
								"Wyndham " + strSiteName + " page is displayed");

					} else {
						tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
								"Wyndham " + strSiteName + " page is not displayed");
					}

					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

				}

			} else {
				tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
						"Social Icons are not present in the Footer");
			}

		}
		
		@Override
		/*public void validateLogo() {
			assertTrue(verifyObjectDisplayed(worldmarkLogo), "Worldmark logo is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLogo", Status.PASS,
					"Worldmark logo is displayed");
			String href=getElementAttribute(worldmarkLogo, "href");
			clickElementBy(worldmarkLogo);
			pageCheck();

			assertTrue(href.contains(driver.getCurrentUrl()),
					"Worldmark logo url not correct");
			driver.navigate().back();
			pageCheck();
			tcConfig.updateTestReporter("WBWHomePage", "validateLogo", Status.PASS,
					"Worldmark logo is present");
		}*/
		public void validateLogo() {
			url = tcConfig.getTestData().get("URL");
			assertTrue(verifyObjectDisplayed(worldmarkLogo), "Worldmark logo is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLogo", Status.PASS,
					"Worldmark logo is displayed");
			
			String homepageUrl = driver.getCurrentUrl();
			clickElementBy(worldmarkLogo);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Assert.assertTrue(driver.getCurrentUrl().contains(homepageUrl), "Home page not opened after WBW logo clicked");	
		}
		@Override
		public void headerMenuVal() {
			assertTrue(verifyObjectDisplayed(wbwLogo), "Worldmark logo is not present");
			tcConfig.updateTestReporter("WBWHomePage", "headerMenuVal", Status.PASS,
					"Worldmark logo is displayed");
			
			assertTrue(verifyObjectDisplayed(hamburgerMenu), "Hamburger Menu is not present");
			tcConfig.updateTestReporter("WBWHomePage", "headerMenuVal", Status.PASS,
					"Hamburger Menu is displayed");
			openCloseHamburgerMenu();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			assertTrue(verifyObjectDisplayed(searchMenu), "Search Menu is not present");
			tcConfig.updateTestReporter("WBWHomePage", "headerMenuVal", Status.PASS,
					"Search Menu is displayed");
			
			assertTrue(verifyObjectDisplayed(signInCTA), "SignIn CTA is not present");
			tcConfig.updateTestReporter("WBWHomePage", "headerMenuVal", Status.PASS,
					"SignIn CTA is displayed");			
		}
		@Override
		public void verifyLogoandClick()
		{
			try{
				String homepageUrl = driver.getCurrentUrl();
				clickElementBy(headerLogo);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				Assert.assertTrue(driver.getCurrentUrl().contains(homepageUrl), "Home page not opened afetr WBW logo clicked");		
			}
			catch (Exception e) {
				tcConfig.updateTestReporter("WBWHomePage", "verifyLogoandClick", Status.FAIL, "Error in WBW Logo click" + e.getMessage());
			}
		
		}
		
		@Override
		public void verifyItemVisible(String strItemname){
			openCloseHamburgerMenu();
			try{
				waitUntilObjectVisible(driver, registerBtn, 5);
				
				switch (strItemname) {
					case "Register":			
						Assert.assertTrue(verifyObjectDisplayed(registerBtn), "Register button is not displayed");
						tcConfig.updateTestReporter("WBWHomePage", "verifyItemVisible", Status.PASS,
								"Register button is displayed");
					break;
					
					case "Sign-In":
						Assert.assertTrue(verifyObjectDisplayed(signIn), "Sign-in button is not displayed");
						tcConfig.updateTestReporter("WBWHomePage", "verifyItemVisible", Status.PASS,
								"Sign-in button is displayed");
					break;			
				}
			}
			catch (Exception e) {
				
			}
		}
		
		@Override
		public void verifySearchAction(){
			
			try{
				openCloseHamburgerMenu();
				waitUntilObjectVisible(driver, registerBtn, 5);
				
				Assert.assertTrue(verifyObjectDisplayed(searchIcon), "Search icon is not displayed");
				tcConfig.updateTestReporter("WBWHomePage", "verifySearchAction", Status.PASS,
						"Search icon is displayed");
				clickElementBy(searchIcon);
				Assert.assertTrue(verifyObjectDisplayed(searchTextField), "Search text field is not displayed");
				tcConfig.updateTestReporter("WBWHomePage", "verifySearchAction", Status.PASS,
						"Search text field is displayed");
				//Add search text and search result page is opening
				driver.findElement(searchTextField).click();
				driver.findElement(searchTextField).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				//waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(searchIcon);
			}
			catch (Exception e) {
				
			}
		}
		
		@Override
		public void validateLegalPages(){
			String worldmarkText = getElementText(worldmark2021);
			assertTrue(!worldmarkText.trim().equals(""), "WorldMark 2021 is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
					"WorldMark 2021 is present");
			
			/*String allrightreservedText = getElementText(allrightText);
			assertTrue(!allrightreservedText.trim().equals(""), "All Rights Reserved text is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
					"All right reserved text is present");*/
			
			assertTrue(verifyObjectDisplayed(termsofuseFooter), "Terms of Use is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
					"Terms of Use is displayed");
			//navigateValidateAndReturn(termsofuseFooter);
			
			assertTrue(verifyObjectDisplayed(privacyNoticeFooter), "Privacy Notice is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
					"Privacy Notice is displayed");
			//navigateValidateAndReturn(privacyNoticeFooter);
			
			assertTrue(verifyObjectDisplayed(privacySettingsFooter), "Privacy Settings is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
					"Privacy Settings is displayed");
			//navigateValidateAndReturn(privacySettingsFooter);
			
			assertTrue(verifyObjectDisplayed(donotSellInfoFooter), "Do Not Sell My Personal Information is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateLegalPages", Status.PASS,
					"Do Not Sell My Personal Information is displayed");
			
		}
		@Override
		public void navigateValidateAndReturn(By link) {

			String href = getElementAttribute(link, "href");
			String title = getElementAttribute(link, "data-eventlabel");
			clickElementJSWithWait(link);
			pageCheck();
			assertTrue(getElementAttribute(breadcrumbLink, "href").trim().equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : "
							+ getElementAttribute(breadcrumbLink, "href").trim());
			tcConfig.updateTestReporter("TimeshareHomePage", "navigateValidateAndReturn", Status.PASS,
					"Latest News Title : " + title + " navigating to link : " + href + " successful");

			navigateBack();
			pageCheck();

		}
		
		@Override
		public void validateSiteMap(){
			assertTrue(verifyObjectDisplayed(sitemapFooter), "Site Map is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateSiteMap", Status.PASS,
					"Site Map is displayed");
			
		}
		
		public void checkMobToggleMenu(){
			openCloseHamburgerMenu();
			clickElementBy(whyWorldMarkDropdown);
			assertTrue(verifyObjectDisplayed(headerSubmenu_benefits), "Mobile Toggle Dropdown is not Opening");
			tcConfig.updateTestReporter("WBWHomePage", "checkMobToggleMenu", Status.PASS,
					"Mobile Toggle Dropdown is Opened");
			clickElementBy(whyWorldMarkDropdown);
			assertFalse(verifyObjectDisplayed(headerSubmenu_benefits), "Mobile Toggle Dropdown is not Closing");
			tcConfig.updateTestReporter("WBWHomePage", "checkMobToggleMenu", Status.PASS,
					"Mobile Toggle Dropdown is Closed");
			openCloseHamburgerMenu();
		
		}
				
		@Override		
		public void validateHeaderMenu() {
			openCloseHamburgerMenu();
			assertTrue(verifyObjectDisplayed(menuWhyWorldmark), "Benefits menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Benefits menu item is present");
			//navigateValidateAndReturn(menuBenefits);
			
			//openCloseHamburgerMenu();
			assertTrue(verifyObjectDisplayed(menuResorts), "Resorts menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Resorts menu item is present");
			///navigateValidateAndReturn(menuResorts);

			//openCloseHamburgerMenu();
			assertTrue(verifyObjectDisplayed(menuOwnerGuide), "Owner Guide menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Owner Guide menu item is present");
			//navigateValidateAndReturn(menuOwnerGuide);
			
			//openCloseHamburgerMenu();
			assertTrue(verifyObjectDisplayed(menuDealsnOffer), "Deals & Offer menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Deals & Offer menu item is present");
			//navigateValidateAndReturn(menuDealsnOffer);
			
			//openCloseHamburgerMenu();
			assertTrue(verifyObjectDisplayed(menuHelp), "Help menu item is not present");
			tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
					"Help menu item is present");
			//navigateValidateAndReturn(menuHelp);
			openCloseHamburgerMenu();
		}
		
//		@Override
//		public void navigateValidateAndReturn(By link) {
//			String href = getElementAttribute(link, "href");
//			// String title = getElementAttribute(link, "data-eventlabel");
//			String title = getObject(link).findElement(By.xpath("./span")).getText();
//			clickElementBy(link);
//			pageCheck();
//			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href),
//					"Link Navigation not correct, expected : " + href + " | Actual : " + getCurrentURL().trim());
	//
//			/*assertTrue(getElementText(bannertitle).trim().equalsIgnoreCase(title),
//					"Link Navigation not correct, expected Title: " + title + " | Actual Title: "
//							+ getElementText(bannertitle).trim());*/
	//
//			tcConfig.updateTestReporter("WBWHomePage", "navigateValidateAndReturn", Status.PASS,
//					"Title : " + title + " navigating to link : " + href + " successful");
	//
//			navigateBack();
//			pageCheck();
	//
//		}
		
		@Override
		public void validateMenuHoverSubMenu(String headerMenuItem, ArrayList<String> subMenuList, int count, boolean validateSubLink) {
			System.out.println("Inside validateMenuHoverSubMenu.........for "+ headerMenuItem);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			openCloseHamburgerMenu();
			/*Click on toggle menu to open sub menu items list*/
			clickElementBy(By.xpath("//li/a[contains(text(),'"+ headerMenuItem +"')]/..//button"));
			assertTrue(getList(By.xpath("//ul/li/a[contains(text(),'"+ headerMenuItem +"')]//..//ul[contains(@class,'submenu is-accordion-')]/li")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By.xpath("//ul/li/a[contains(text(),'"+ headerMenuItem +"')]//..//ul[contains(@class,'submenu is-accordion-')]/li")).size());

			tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu count is/are matching");
			
			int submenuCount = getList(By.xpath("//ul/li/a[contains(text(),'"+ headerMenuItem +"')]//..//ul[contains(@class,'submenu is-accordion-')]/li")).size();
			//int submenuCount = subMenuList.size();
			for(int i=0; i<submenuCount;i++ )
			{
				String strSubmenu = subMenuList.get(i);
				System.out.println("submenu item is "+ strSubmenu +" for i= "+i);
				assertTrue(
						verifyObjectDisplayed(
								By.xpath("//ul[contains(@class,'submenu is-accordion')]/li/a[text()='"+ strSubmenu +"']")),
						headerMenuItem + " menu item's sub menu item " + strSubmenu + " is not present");			
				tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
				headerMenuItem + " menu item's sub menu items " + strSubmenu + " is present");
				System.out.println("submenu item "+ strSubmenu +" is present");
				if(validateSubLink){				
					//validateSubMenuLinks(strSubmenu);
				}
			} 
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(By.xpath("//li/a[contains(text(),'"+ headerMenuItem +"')]/..//button"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			openCloseHamburgerMenu();
		}
		
		public void validateSubMenuLinks(String subMenu)
		{			
			clickElementJSWithWait(
					By.xpath("//ul[contains(@class,'submenu is-accordion')]/li/a[text()='"+ subMenu +"']"));
			pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				switch (subMenu) {
				/*Benefits sub menu items*/
				case "Using My Ownership":
//					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
//					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
//					tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
//							"Using My Ownership submenu navigation is Successful");
					break;
				case "Pride of Ownership":

					break;
				case "Tour the club":

					break;
				case "Using my Ownership":

					break;
				case "Owner Testimonials":
					
					break;
				case "Buzzworthy News":
					
					break;

				/*Resorts sub menu items*/
				case "Explore Resorts":

					break;
				case "Destinations":

					break;
				case "News":

					break;
				case "Vacations Unpacked":
					
					break;
				case "Local Discounts":
					
					break;
					
				/*Owner Guide Sub menu*/
				case "Get Started":

					break;
				case "Get Informed":

					break;
				case "Get Ready":
					
					break;
				case "Get More":
					
					break;
					
				/*Deals n Offers Sub menu*/
				case "Travel Deals":

					break;
				case "Owner Exclusives":

					break;
				case "Partner Offers":
					
					break;
					
				/*Helps Sub menu*/
				case "Wyndham Cares":

					break;
				case "Contact Us":

					break;
				case "FAQs":
					
					break;				
				}
				navigateToMainTab(allTabs);
				pageCheck();

			} else {
				switch (subMenu) {
				/*Benefits sub menu items*/
				case "Using My Ownership":
//					waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
//					Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
//					tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
//							"Using My Ownership submenu navigation is Successful");
					break;
				case "Pride of Ownership":

					break;
				case "Tour the club":

					break;
				case "Using my Ownership":

					break;
				case "Owner Testimonials":
					
					break;
				case "Buzzworthy News":
					
					break;

				/*Resorts sub menu items*/
				case "Explore Resorts":

					break;
				case "Destinations":

					break;
				case "News":

					break;
				case "Vacations Unpacked":
					
					break;
				case "Local Discounts":
					
					break;
					
				/*Owner Guide Sub menu*/
				case "Get Started":

					break;
				case "Get Informed":

					break;
				case "Get Ready":
					
					break;
				case "Get More":
					
					break;
					
				/*Deals n Offers Sub menu*/
				case "Travel Deals":

					break;
				case "Owner Exclusives":

					break;
				case "Partner Offers":
					
					break;
					
				/*Helps Sub menu*/
				case "Wyndham Cares":

					break;
				case "Contact Us":

					break;
				case "FAQs":
					
					break;				
				}
				navigateBack();
				pageCheck();
			}	
			
		}
		
		@Override
		public void scrollDownNav(){
			try {
				openCloseHamburgerMenu();
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				scrollDownForElementJSBy(publicationsLink);
			}
			catch (Exception e) {
				tcConfig.updateTestReporter("WBWHomePage", "scrollDown", Status.FAIL, "Error in Scrolling to footer" + e.getMessage());
			}
		}

		
	}
	

	


