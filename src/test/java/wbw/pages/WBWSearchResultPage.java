package wbw.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWSearchResultPage extends FunctionalComponents {

	protected String url;
	
	public WBWSearchResultPage(TestConfig tcconfig) {
		super(tcconfig);
		
	}
	
	protected By headline = By.xpath("//div[contains(@class,'search-title-text-default') and contains(.,'Search')]");
	protected By searchBar = By.xpath("//input[contains(@class,'searchComponentInput')]");
	protected By searchCTA = By.xpath("//button[contains(@class,'searchComponentButton')]");
	protected By resultCount = By.xpath("//div[contains(@class,'resultNumber')]");
	protected By searchTitle = By.xpath("//div[contains(@class,'search-title-text-default')]");
	protected By searchPageSearchTextField = By.xpath("//input[contains(@class,'input-group-field searchComponentInput')]");
	protected By searchComponentBtn = By.xpath("//button[contains(@class,'searchComponentButton')]");
	protected By searchResultNumber = By.xpath("//div[contains(@class,'resultNumber')]");
	protected By searchRsltPageNumber = By.xpath("//ul[@class='pagination text-center']//li[2]");
	
	public void validateSearchHeader()
	{
		waitUntilObjectVisible(driver, searchTitle, 5);	
		Assert.assertTrue(verifyObjectDisplayed(searchTitle), "Search header is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchHeader", Status.PASS,
				"Search header is displayed");
	}
	
	public void validateSearchTextField()
	{
		Assert.assertTrue(verifyObjectDisplayed(searchPageSearchTextField), "Search bar is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchTextField", Status.PASS,
				"Search bar is displayed");
	}
	
	public void validateSearchKeywordContent()
	{
		String strSearchBarContent = driver.findElement(searchPageSearchTextField).getAttribute("value");
		if(strSearchBarContent.equals(testData.get("SearchKeyword")))
		{
			tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchKeywordContent", Status.PASS,
					"Search bar content is matching with search keyword");
		}
		else
		{
			tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchKeywordContent", Status.FAIL,
					"Search bar content is not matching with search keyword");
		}
	}
	
	public void validateSearchTextEditDelete()
	{
		String strSearchBarContent = driver.findElement(searchPageSearchTextField).getAttribute("value");
		clickElementBy(searchPageSearchTextField);
		driver.findElement(searchPageSearchTextField).sendKeys(Keys.SPACE);
		driver.findElement(searchPageSearchTextField).sendKeys("resort");
		String strSearchBarContent2 = driver.findElement(searchPageSearchTextField).getAttribute("value");
		if(strSearchBarContent2.equals(strSearchBarContent + " " + "resort"))
		{
			tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchTextEditDelete", Status.PASS,
					"User can update search bar content");
		} 
		else
		{
			tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchTextEditDelete", Status.FAIL,
					"User can not update search bar content");
		}
		
		clearElementBy(searchPageSearchTextField);
		String strSearchBarContentBlank = driver.findElement(searchPageSearchTextField).getAttribute("value");
		if(strSearchBarContentBlank.equals(""))
		{
			tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchTextEditDelete", Status.PASS,
					"User can delete search bar content");
		}
		else
		{
			tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchTextEditDelete", Status.PASS,
					"User can not delete search bar content");
		}	
	}
	
	public void validateSearchBtnAvailable()
	{		
		Assert.assertTrue(verifyObjectDisplayed(searchComponentBtn), "Search button is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchBtnAvailable", Status.PASS,
				"Search button is displayed");
	}
	
	public void searchInSearchPage(String searchKeyword)
	{
		clearElementBy(searchPageSearchTextField);
		driver.findElement(searchPageSearchTextField).sendKeys(searchKeyword);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(searchComponentBtn);		
	}
	
	public void validateSearchResultCountAvailable()
	{	
		searchInSearchPage(testData.get("SearchKeyword"));
		Assert.assertTrue(verifyObjectDisplayed(searchResultNumber), "Search result count is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchResultCountAvailable", Status.PASS,
				"Search result count is displayed");
	}
	
	public void validateSearchRsltPageNumber()
	{	
		Assert.assertTrue(verifyObjectDisplayed(searchRsltPageNumber), "Search result page number is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchRsltPageNumber", Status.PASS,
				"Search result page number is displayed");
	}
	
	public void validateNoSearchRsltPageNumber()
	{	
		boolean isElmntDslpd = verifyObjectDisplayed(searchRsltPageNumber);
		Assert.assertFalse(isElmntDslpd, "Search result page number is displayed");		
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchRsltPageNumber", Status.PASS,
				"Search result page number is not displayed as expected");
	}
	
	
	public void validateHeadline(){
		waitUntilElementVisibleBy(driver,headline,120);
		String searchHeadline = getElementText(headline);
		assertTrue(!searchHeadline.trim().equals(""), "Search Headline is absent");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateHeader", Status.PASS, "Search headline is displayed");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	public void validateSearch(){
		Assert.assertTrue(verifyObjectDisplayed(searchBar), "Search bar is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearch", Status.PASS,
				"Search bar along with input is displayed");
		
		}
	
	public void validateSearchCTA(){
		Assert.assertTrue(verifyObjectDisplayed(searchCTA), "Search CTA is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchCTA", Status.PASS,
				"Search CTA is displayed");
		clickElementJSWithWait(searchCTA);
		String strResult = getElementText(resultCount);
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterBrandlogo", Status.PASS,
				"Result count is " + strResult);
		
	}
	
	
}
