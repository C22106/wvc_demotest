package wbw.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWResortExplorePage extends FunctionalComponents{

	protected String url;
	public static final Logger log = Logger.getLogger(WBWResortExplorePage.class.getName());
	
	public WBWResortExplorePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	protected By searchTextField = By.xpath("//input[@placeholder='Enter a location']");
	protected By searchResults = By.xpath("//div[contains(@class,'resort-card__name')]");

	public void searchResort(String searchKeyword)
	{
		waitUntilObjectVisible(driver, searchTextField, 5);	
		clickElementBy(searchTextField);
		driver.findElement(searchTextField).sendKeys(searchKeyword);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.findElement(searchTextField).sendKeys(Keys.ENTER);
	}
	
	public void openFirstResort()
	{
		waitUntilObjectVisible(driver, searchTextField, 5);	
		Assert.assertTrue(verifyObjectDisplayed(searchTextField), "Search header is not displayed");
		tcConfig.updateTestReporter("WBWResortExplorePage", "openFirstResort", Status.PASS,
				"Search text field is displayed");

		List<WebElement> listOfResults = driver.findElements(searchResults);
		tcConfig.updateTestReporter("WBWResortExplorePage", "openFirstResort", Status.PASS,
				"No of results on resort explore page : " + listOfResults.size());

		String firstTitle = listOfResults.get(0).getText();
		tcConfig.updateTestReporter("WBWResortExplorePage", "openFirstResort", Status.PASS,
				"First Resort title is : " + firstTitle);
		
		if (listOfResults.size() >= 1) 
		{
			listOfResults.get(0).click();
		} else {
			System.out.println("No result found in resort explore page");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		
	}
	

	
}
