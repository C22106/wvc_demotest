package wbw.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWHomePage_WebSafari extends FunctionalComponents{

	protected String url;
	public static final Logger log = Logger.getLogger(WBWHomePage_WebSafari.class.getName());
	
	public WBWHomePage_WebSafari(TestConfig tcconfig) {
		super(tcconfig);
	}
	protected By footer = By.xpath("//div[contains(@class,'footer-holiday')][1]");
	protected By header = By.xpath("//div[contains(@class,'footer-holiday')][1]");
	protected By headerMenu_benefits = By.xpath("(//li[contains(@class,'-item is-dropdown-submenu-parent opens-right') and @role='menuitem']/a[text()='Benefits'])[1]");
	protected By headerMenu_help = By.xpath("(//li[contains(@class,'-item is-dropdown-submenu-parent opens-right') and @role='menuitem']/a[text()='Help'])[1]");
	protected By headerSubmenu_benefits = By.xpath("(//ul/li/div/ul[@class='nested menu']/li/a[text()='Using My Ownership'])[1]");
	
	
	protected By headerMenu_Partial = By.xpath("//li[@role='menuitem']/a[text()='Benefits']");
	protected By globalHeaderLogo = By.xpath("//li[@class='global-header-logo']");
	protected By registerBtn = By.xpath("//a/button[contains(@class,'account-info--register button')]");

	protected By menuBenefits = By.xpath("//li[@aria-label='Benefits']/a[text()='Benefits']");
	protected By menuResorts = By.xpath("//li[@aria-label='Resorts']/a[text()='Resorts']");
	protected By menuOwnerGuide = By.xpath("//li[@aria-label='Owner Guide']/a[text()='Owner Guide']");
	protected By menuDealsnOffer = By.xpath("//li[@aria-label='Deals & Offers']/a[text()='Deals & Offers']");
	protected By menuHelp = By.xpath("//li[@aria-label='Help']/a[text()='Help']");
	
	public void launchApplication() {

		url = tcConfig.getTestData().get("URL");
		driver.get(url);
		pageCheck();

		tcConfig.updateTestReporter("WBWHomePage", "launchApplication", Status.PASS,
				"WBW URL was launched successfully");
	}
	public void scrollToFooter(){
		try {
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			scrollDownForElementJSBy(footer);
		}
		catch (Exception e) {
			tcConfig.updateTestReporter("WBWHomePage", "scrollToFooter", Status.FAIL, "Error in Scrolling ot footer" + e.getMessage());
		}
	}
//	public void mouseHover(){
//		try {
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			hoverOnElement(headerMenu_benefits);
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//		}
//		catch (Exception e) {
//			tcConfig.updateTestReporter("WBWHomePage", "mouseHover", Status.FAIL, "Error in Mouse Hover to Header Menu" + e.getMessage());
//		}
//	}
	
	
//	public void mouseHoverOnMenu(String menu, ArrayList<String> subMenuList){
//		try {
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			hoverOnElement(headerMenu_benefits);
//			waitForSometime(tcConfig.getConfig().get("LowWait"));
//		}
//		catch (Exception e) {
//			tcConfig.updateTestReporter("WBWHomePage", "mouseHover", Status.FAIL, "Error in Mouse Hover to Header Menu" + e.getMessage());
//		}	
//	}
	

	public void validateMenuHoverSubMenu(String headerMenuItem, ArrayList<String> subMenuList, int count, boolean validateSubLink) {
		System.out.println("Inside validateMenuHoverSubMenu.........for "+ headerMenuItem);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		hoverOnElement(By.xpath("(//li[@role='menuitem']/a[contains(text(),'" + headerMenuItem + "')])[1]"));

		assertTrue(getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size() == count,
				"List size doesn't match, expected count=" + count + " | actual count="
						+ getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size());

		tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
				headerMenuItem + " menu item's sub menu count is/are matching");
		
		int submenuCount = getList(By.xpath("//li/a[contains(text(),'" + headerMenuItem + "')]//../ul/li/div/ul/li")).size();
		//int submenuCount = subMenuList.size();
		for(int i=0; i<submenuCount;i++ )
		{
			String strSubmenu = subMenuList.get(i);
			System.out.println("submenu item is "+ strSubmenu +" for i= "+i);
			assertTrue(
					verifyObjectDisplayed(
							By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + strSubmenu + "']")),
					headerMenuItem + " menu item's sub menu item " + strSubmenu + " is not present");			
			tcConfig.updateTestReporter("WBWHomePage", "validateMenuHoverSubMenu", Status.PASS,
			headerMenuItem + " menu item's sub menu items " + strSubmenu + " is present");
			System.out.println("submenu item "+ strSubmenu +" is present");
//			if(validateSubLink){				
//				validateSubMenuLinks(strSubmenu);
//			}
		} 
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		hoverOnElement(registerBtn);
		
	}
	
	public void validateSubMenuLinks(String subMenu)
	{			
		clickElementJSWithWait(
				By.xpath("//ul/li/div/ul[@class='nested menu']/li/a[text()='" + subMenu + "']"));
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			switch (subMenu) {
			/*Benefits sub menu items*/
			case "Using My Ownership":
//				waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
//				Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
//				tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
//						"Using My Ownership submenu navigation is Successful");
				break;
			case "Pride of Ownership":

				break;
			case "Tour the club":

				break;
			case "Using my Ownership":

				break;
			case "Owner Testimonials":
				
				break;
			case "Buzzworthy News":
				
				break;

			/*Resorts sub menu items*/
			case "Explore Resorts":

				break;
			case "Destinations":

				break;
			case "News":

				break;
			case "Vacations Unpacked":
				
				break;
			case "Local Discounts":
				
				break;
				
			/*Owner Guide Sub menu*/
			case "Get Started":

				break;
			case "Get Informed":

				break;
			case "Get Ready":
				
				break;
			case "Get More":
				
				break;
				
			/*Deals n Offers Sub menu*/
			case "Travel Deals":

				break;
			case "Owner Exclusives":

				break;
			case "Partner Offers":
				
				break;
				
			/*Helps Sub menu*/
			case "Wyndham Cares":

				break;
			case "Contact Us":

				break;
			case "FAQs":
				
				break;				
			}
			navigateToMainTab(allTabs);
			pageCheck();

		} else {
			switch (subMenu) {
			/*Benefits sub menu items*/
			case "Using My Ownership":
//				waitUntilObjectVisible(driver, clubWyndhamLogo, 240);
//				Assert.assertTrue(verifyObjectDisplayed(clubWyndhamLogo), "Club Wyndham Logo is not Displayed");
//				tcConfig.updateTestReporter("WBWHomePage", "validateSubMenuLinks", Status.PASS,
//						"Using My Ownership submenu navigation is Successful");
				break;
			case "Pride of Ownership":

				break;
			case "Tour the club":

				break;
			case "Using my Ownership":

				break;
			case "Owner Testimonials":
				
				break;
			case "Buzzworthy News":
				
				break;

			/*Resorts sub menu items*/
			case "Explore Resorts":

				break;
			case "Destinations":

				break;
			case "News":

				break;
			case "Vacations Unpacked":
				
				break;
			case "Local Discounts":
				
				break;
				
			/*Owner Guide Sub menu*/
			case "Get Started":

				break;
			case "Get Informed":

				break;
			case "Get Ready":
				
				break;
			case "Get More":
				
				break;
				
			/*Deals n Offers Sub menu*/
			case "Travel Deals":

				break;
			case "Owner Exclusives":

				break;
			case "Partner Offers":
				
				break;
				
			/*Helps Sub menu*/
			case "Wyndham Cares":

				break;
			case "Contact Us":

				break;
			case "FAQs":
				
				break;				
			}
			navigateBack();
			pageCheck();
		}	
		
	}
	
	
//	public void clickSubmenuItem(){
//		try {
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			hoverOnElement(headerMenu_benefits);
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			hoverOnElement(headerSubmenu_benefits);
//			clickElementBy(headerSubmenu_benefits);
//			String currentUrl = driver.getCurrentUrl();
//			Assert.assertTrue(currentUrl.contains(tcConfig.getTestData().get("BenefitsMenuLink")), "Username Field Not Present");
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//		}
//		catch (Exception e) {
//			tcConfig.updateTestReporter("WBWHomePage", "clickSubmenuItem", Status.FAIL, "Error in Header Submenu navigation" + e.getMessage());
//		}	
//	}
	
	
//	public void clicMenuItem(){
//		try {
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			clickElementBy(headerMenu_help);
//			String currentUrl = driver.getCurrentUrl();
//			Assert.assertTrue(currentUrl.contains(tcConfig.getTestData().get("HelpMenuLink")), "Help page not opened");
//			waitForSometime(tcConfig.getConfig().get("MedWait"));
//		}
//		catch (Exception e) {
//			tcConfig.updateTestReporter("WBWHomePage", "clicHelpmenuItem", Status.FAIL, "Error in Header Helpmenu navigation" + e.getMessage());
//		}	
//	}
					
			
	public void validateHeaderMenu() {

		assertTrue(verifyObjectDisplayed(menuBenefits), "Benefits menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Benefits menu item is present");
		//navigateValidateAndReturn(menuBenefits);

		assertTrue(verifyObjectDisplayed(menuResorts), "Resorts menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Resorts menu item is present");
		///navigateValidateAndReturn(menuResorts);

		assertTrue(verifyObjectDisplayed(menuOwnerGuide), "Owner Guide menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Owner Guide menu item is present");
		//navigateValidateAndReturn(menuOwnerGuide);
		
		assertTrue(verifyObjectDisplayed(menuDealsnOffer), "Deals & Offer menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Deals & Offer menu item is present");
		//navigateValidateAndReturn(menuDealsnOffer);
		
		assertTrue(verifyObjectDisplayed(menuHelp), "Help menu item is not present");
		tcConfig.updateTestReporter("WBWHomePage", "validateHeaderMenu", Status.PASS,
				"Help menu item is present");
		navigateValidateAndReturn(menuHelp);
	}
	

	public void navigateValidateAndReturn(By link) {
		String href = getElementAttribute(link, "href").trim();
		clickElementBy(link);
		//pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() > 1) {
			newTabNavigations(allTabs);
			String title = driver.getCurrentUrl().trim();
			//assertTrue(title.equalsIgnoreCase(href),
			assertTrue(title.contains(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			//pageCheck();
		} else {
			String title = driver.getCurrentUrl().trim();
			//assertTrue(title.equalsIgnoreCase(href),
			assertTrue(title.contains(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateBack();
			//pageCheck();
		}

	}
	
	
	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("html")) {
					System.out.println("URL contians html");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it."); report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}

	
}
