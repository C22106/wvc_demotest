package wbw.pages;


import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWLoggedOutPage_Web extends FunctionalComponents{

	protected String url;
	public WBWLoggedOutPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		
	}
	public void launchApplication() {

		url = tcConfig.getTestData().get("URL");
		driver.get(url);
		pageCheck();

		tcConfig.updateTestReporter("WBWHomePage", "launchApplication", Status.PASS,
				"WBW URL was launched successfully");
	}
}