package wbw.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import org.testng.Assert;


public class WBWSearchResultPage_Android extends WBWSearchResultPage{
	public WBWSearchResultPage_Android(TestConfig tcconfig) {
		super(tcconfig);

		
}
	@Override
	public void validateHeadline(){
		waitUntilElementVisibleBy(driver,headline,120);
		String searchHeadline = getElementText(headline);
		assertTrue(!searchHeadline.trim().equals(""), "Search Headline is absent");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateHeader", Status.PASS, "Search headline is displayed");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
	
	@Override
	public void validateSearch(){
		Assert.assertTrue(verifyObjectDisplayed(searchBar), "Search bar is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearch", Status.PASS,
				"Search bar along with input is displayed");
		
		}
	@Override
	public void validateSearchCTA(){
		Assert.assertTrue(verifyObjectDisplayed(searchCTA), "Search CTA is not displayed");
		tcConfig.updateTestReporter("WBWSearchResultPage", "validateSearchCTA", Status.PASS,
				"Search CTA is displayed");
		clickElementJSWithWait(searchCTA);
		String strResult = getElementText(resultCount);
		tcConfig.updateTestReporter("PanoramaHomePage", "validateFooterBrandlogo", Status.PASS,
				"Result count is " + strResult);
		
	}
	
}
