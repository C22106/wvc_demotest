package genesis.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import automation.core.TestBase;
import genesis.pages.OFSLLCustDetailsPage_Web;
import genesis.pages.OFSLLCustServicePage_Web;
import genesis.pages.OFSLLHomePage_Web;
import genesis.pages.OFSLLLoginPage_Web;
import genesis.pages.OFSLLPaymentsPage_Web;
import genesis.pages.OFSLLSummaryPage_Web;

public class OFSLLScripts extends TestBase {
	public static final Logger log = Logger.getLogger(OFSLLScripts.class);

	/*
	 * Method: tc_001_SubmitPromise Description: Verify agent can enter promise
	 * in the system Validations Date: June/2020 Author: Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch1" })

	public void tc_001_SubmitPromise(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.submitPromise();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_002_SubmitBankruptcy Description: Verify agent is able to
	 * submit account for bankruptcy review Validations Date: June/2020 Author:
	 * Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch1" })

	public void tc_002_SubmitBankruptcy(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLSummaryPage_Web summaryPage = new OFSLLSummaryPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.submitBankruptcy();
			summaryPage.navigateToSummary();
			summaryPage.conditionCheck();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_003_SubmitHardship Description: Verify agent can submit
	 * Hardship condition Validations Date: June/2020 Author: Harsh Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch1" })

	public void tc_003_SubmitHardship(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLSummaryPage_Web summaryPage = new OFSLLSummaryPage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.submitHardship();
			summaryPage.navigateToSummary();
			summaryPage.conditionCheck();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_004_UpdateEmailId Description: Verify agent can change email
	 * id Validations Date: May/2020 Author: Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch1" })

	public void tc_004_UpdateEmailId(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLCustDetailsPage_Web custDeatilsPage = new OFSLLCustDetailsPage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custDeatilsPage.updateEmail();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_005_UpdateAdress Description: Verify agent can update the
	 * address details in the system Validations Date: May/2020 Author: Ankush
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch1" })

	public void tc_005_UpdateAddress(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLCustDetailsPage_Web custDeatilsPage = new OFSLLCustDetailsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custDeatilsPage.updateAddress();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_006_AddAddress Description: Verify agent can add the address
	 * details in the system Validations Date: May/2020 Author: Ankush Changes
	 * By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch1" })

	public void tc_006_AddAddress(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLCustDetailsPage_Web custDeatilsPage = new OFSLLCustDetailsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custDeatilsPage.addAddress();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_007_SubmitResortCancellation_Ref Description: Verify agent is
	 * able to submit account for Resort Cancellation with Refund Validations
	 * Date: June/2020 Author: Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch1" })

	public void tc_007_SubmitResortCancellation_Ref(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLSummaryPage_Web summaryPage = new OFSLLSummaryPage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.submitResortCancel();
			summaryPage.navigateToSummary();
			summaryPage.conditionCheck();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_008_ChangeInterestRate_Add Description: Verify agent can
	 * update the interest in the system Validations Date: May/2020 Author:
	 * Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch2" })

	public void tc_008_ChangeInterestRate_Add(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getinterestamount_balancetab();
			custServicePage.addInterest("ADD");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_009_ChangeInterestRate_Sub Description: Verify agent can
	 * update the interest in the system Validations Date: May/2020 Author:
	 * Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch2" })

	public void tc_009_ChangeInterestRate_Sub(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getinterestamount_balancetab();
			custServicePage.addInterest("SUB");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_010_Principle_Add Description: Verify agent can update the
	 * Principle amount in the system Validations Date: June/2020 Author: Ankush
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch2" })

	public void tc_010_Principal_Add(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getprincipalamount_balancetab();
			custServicePage.updatePrincipal("ADD");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_011_Principal_Subtract Description: Verify agent can update
	 * the Principle amount in the system Validations Date: June/2020 Author:
	 * Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch2" })

	public void tc_011_Principal_Subtract(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getprincipalamount_balancetab();
			custServicePage.updatePrincipal("SUB");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_012_Principal_Paidaccnt_Validation Description: Verify
	 * Principal amount update for Paid account in the system Validations Date:
	 * June/2020 Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch2" })

	public void tc_012_Principal_Paidaccnt_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {
			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			// custServicePage.getprincipalamount_balancetab();
			if (custServicePage.AccStats.equals("PAID OFF")) {
				custServicePage.add_Principal_paidAccnt_Transaction();
			}
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_013_SCRA_Validation_onDuty Description: Verify SCRA
	 * transaction update for account in the system Validations Date: July/2020
	 * Author: Ankush Changes By: Harsh (related to applying
	 * Foreclosure,Bankruptcy,Reposession to the account
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch2" })

	public void tc_013_SCRA_Validation_onDuty(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLSummaryPage_Web OFSLLSummaryPage = new OFSLLSummaryPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.updateSCRA("ONDuty");
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			OFSLLSummaryPage.validatecallActivity_summary("ONDuty");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_014_SCRA_Validation_offDuty Description: Verify SCRA
	 * transaction update for account in the system Validations Date: June/2020
	 * Author: Ankush Changes By: Harsh (related to Add Call Activity->Condition
	 * Code
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch2" })

	public void tc_014_SCRA_Validation_offDuty(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLSummaryPage_Web OFSLLSummaryPage = new OFSLLSummaryPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.updateSCRA("OFFDuty");
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			OFSLLSummaryPage.validatecallActivity_summary("OFFDuty");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_015_MaintenanceFee_Add Description: Verify agent can update
	 * the MaintenanceFee in the system Validations Date: June/2020 Author:
	 * Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch3" })

	public void tc_015_MaintenanceFee_Add(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("ADD");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_016_MaintenanceFee_Sub Description: Verify agent can update
	 * the MaintenanceFee in the system Validations Date: June/2020 Author:
	 * Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch3" })

	public void tc_016_MaintenanceFee_Sub(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		// OFSLLSummaryPage_Web OFSLLSummaryPage= new
		// OFSLLSummaryPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("SUB");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_017_LateFee_Add_Master Description: Verify agent can update
	 * the LateFee for master account in the system Validations Date: June/2020
	 * Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch3" })

	public void tc_017_LateFee_Add_Master(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);

		try {
			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("ADD");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_018_LateFee_Sub_Master Description: Verify agent can update
	 * the LateFee for master account in the system Validations Date: June/2020
	 * Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch3" })

	public void tc_018_LateFee_Sub_Master(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		// OFSLLSummaryPage_Web OFSLLSummaryPage= new
		// OFSLLSummaryPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("SUB");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_018_LateFee_Add_Loan Description: Verify agent can update the
	 * LateFee for loan account in the system Validations Date: June/2020
	 * Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch3" })

	public void tc_019_LateFee_Add_Loan(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		// OFSLLSummaryPage_Web OFSLLSummaryPage= new
		// OFSLLSummaryPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("ADD");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_020_LateFee_Sub_Loan Description: Verify agent can update the
	 * LateFee for loan account in the system Validations Date: June/2020
	 * Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch3" })

	public void tc_020_LateFee_Sub_Loan(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		// OFSLLSummaryPage_Web OFSLLSummaryPage= new
		// OFSLLSummaryPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();
			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("SUB");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_021_LateFee_Paid_Off_Validation Description: Verify amount
	 * update for Paid account in the system Validations Date: June/2020 Author:
	 * Ankush Changes By: Harsh/Ankush: Add PAID OFF check
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch3" })

	public void tc_021_LateFee_Paid_Off_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			if (custServicePage.AccStats.equals("PAID OFF")) {
				custServicePage.Update_paidAccnt_Transaction();
			} else {
				tcconfig.updateTestReporterWithoutScreenshot("OFSLLScripts", "Update_paidAccnt_Transaction",
						Status.PASS, "The Account is not in paid off status");
			}
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_007_SubmitResortCancellation_Ref Description: Verify agent is
	 * able to submit account for Resort Cancellation with Refund Validations
	 * Date: June/2020 Author: Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch4" })

	public void tc_022_SubmitResortCancellation_NoRef(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLSummaryPage_Web summaryPage = new OFSLLSummaryPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.submitResortCancel();
			summaryPage.navigateToSummary();
			summaryPage.conditionCheck();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_023_Due_interest_Add Description: Verify agent can update the
	 * Dues Interest in the system Validations Date: Aug/2020 Author: Ankush
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch4" })

	public void tc_023_Due_interest_Add(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("ADD");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_024_Due_interest_SUB Description: Verify agent can update the
	 * Dues Interest in the system Validations Date: Aug/2020 Author: Ankush
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch4" })

	public void tc_024_Due_interest_SUB(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("SUB");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_025_Down_Payment_Add Description: Verify agent can update the
	 * Down Payment in the system Validations Date: Aug/2020 Author: Ankush
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch4" })

	public void tc_025_Down_Payment_Add(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("ADD");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_026_Down_Payment_Sub Description: Verify agent can update the
	 * Down Payment in the system Validations Date: Aug/2020 Author: Ankush
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch4" })

	public void tc_026_Down_Payment_Sub(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.getamount_balancetab();
			custServicePage.UpdateTranscation("SUB");

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: tc_027_Down_Payment_Paid_Off_Validation Description: Verify agent
	 * cannot post the Down Payment on Paid Loan accounts in the system
	 * Validations Date: Aug/2020 Author: Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch4" })

	public void tc_027_Down_Payment_Paid_Off_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			if (custServicePage.AccStats.equals("PAID OFF")) {
				custServicePage.add_Principal_paidAccnt_Transaction();
			}

			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_028_Payoff_Amount Description: Verify agent can update the
	 * Payoff Amount in the system Validations Date: Aug/2020 Author: Ankush
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch4" })

	public void tc_028_Payoff_Amount_FWAssociateAcct(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLPaymentsPage_Web paymentPage = new OFSLLPaymentsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToPayments();
			paymentPage.Add_Batch_Payment();
			paymentPage.Add_Payment_Associates();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_029_Payoff_Amount_Paid_Accnt Description: Verify agent can
	 * update the Payoff Amount in the system Validations Date: Aug/2020 Author:
	 * Ankush/Harsh Changes By: Harsh for the test data
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch5" })

	public void tc_029_Payoff_Amount_Paid_Accnt(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		OFSLLPaymentsPage_Web paymentPage = new OFSLLPaymentsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			if (custServicePage.AccStats.equals("PAID OFF")) {
				homePage.clickToPayments();
				paymentPage.Add_Batch_Payment();
				paymentPage.Add_Payment_Paid();
			} else {
				tcconfig.updateTestReporter("OFSLLScripts", "tc_029_Payoff_Amount_Paid_Accnt", Status.PASS,
						"It is not a paid account, please change the test data and run it again ");

			}
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_030_Payoff_Amount_Master_Accnt_club Description: Verify agent
	 * can update the Payoff Amount in the system Validations Date: Aug/2020
	 * Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch5" })

	public void tc_030_Payoff_Amount_Master_Accnt_club(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLPaymentsPage_Web paymentPage = new OFSLLPaymentsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToPayments();
			paymentPage.Add_Batch_Payment();
			paymentPage.Add_Payment_Master_club();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_031_Payoff_Amount_Associate_loan_active Description: Verify
	 * agent can update the Payoff Amount in the system Validations Date:
	 * Aug/2020 Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch5" })

	public void tc_031_Payoff_Amount_Associate_loan_active(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLPaymentsPage_Web paymentPage = new OFSLLPaymentsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToPayments();
			paymentPage.Add_Batch_Payment();
			paymentPage.Add_Payment_Associates();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_032_Payoff_Amount_Master_Accnt_FW Description: Verify agent
	 * can update the Payoff Amount in the system Validations Date:
	 * September/2020 Author: Ankush Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch5" })

	public void tc_032_Payoff_Amount_Master_Accnt_FW(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLPaymentsPage_Web paymentPage = new OFSLLPaymentsPage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();

			loginPage.loginToOFSLL();
			homePage.clickToPayments();
			paymentPage.Add_Batch_Payment();
			paymentPage.Add_Payment_Master_FW();
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_033_SCRA_Validation_onDuty_LoanAcct Description: Verify On
	 * Active Military Duty to Loan Account Validations Date: September/2020
	 * Author: Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch5" })

	public void tc_033_SCRA_Validation_onDuty_LoanAcct(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);

		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);
		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();
			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.addSCRA_loan("ONDuty");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_034_SCRA_Validation_offDuty_LoanAcct Description: Verify Off
	 * Military Duty to Loan Account Validations Date: September/2020 Author:
	 * Harsh Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ofsll", "genesis", "batch5" })

	public void tc_034_SCRA_Validation_offDuty_LoanAcct(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		OFSLLLoginPage_Web loginPage = new OFSLLLoginPage_Web(tcconfig);
		OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcconfig);
		OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcconfig);

		try {

			loginPage.launchApplication();

			loginPage.loginToOKTA();
			loginPage.loginToOFSLL();
			homePage.clickToCustomerService();
			custServicePage.searchAcctNo();
			custServicePage.addSCRA_loan("OFFDuty");
			loginPage.logOutOFSLL();
			loginPage.switchToTab();
			loginPage.logOutOKTA();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}