package genesis.pages;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class OFSLLBasePage extends FunctionalComponents {

	public OFSLLBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}
	
	/**
	 * Check If any extra tabs Present
	 * 
	 * @param applicationURl
	 */
	public void navigateToAppURL(String applicationURl) {
		driver.navigate().to(applicationURl);
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info("No Alert Present");
		}
	}

	
	/*
	 * *************************Method: selectByText ***
	 * *************Description: Drop Down Select By Text**
	 */
	public void selectByText(By by, String strData) {
		try {
			new Select(getObject(by)).selectByVisibleText(strData);
		} catch (Exception e) {
			log.info("The Drop Down Text is not present");
		}
	}

	/*
	 * *************************Method: selectByText ***
	 * *************Description: Drop Down Select By Text**
	 */
	public void selectByText(WebElement by, String strData) {
		try {
			new Select((by)).selectByVisibleText(strData);
		} catch (Exception e) {
			log.info("The Drop Down Text is not present");
		}
	}

	/*
	 * *************************Method: selectByValue ***
	 * *************Description: Drop Down Select By Value**
	 */

	public void selectByValue(By by, String strData) {
		try {
			new Select(getObject(by)).selectByValue(strData);
		} catch (Exception e) {
			log.info("The Drop Down Value is not present");
		}
	}

	/*
	 * *************************Method: selectByValue ***
	 * *************Description: Drop Down Select By Value**
	 */

	public void selectByValue(WebElement by, String strData) {
		try {
			new Select((by)).selectByValue(strData);
		} catch (Exception e) {
			log.info("The Drop Down Value is not present");
		}
	}
	
	/*
	 * *************************Method: isElementPresent ***
	 * *************Description: Check wether element is present **
	 */

	public static boolean isElementPresent(WebElement element) {
		boolean flag = false;
		try {
			if (element.isDisplayed() || element.isEnabled())
				flag = true;
		} catch (NoSuchElementException e) {
			flag = false;
		} catch (StaleElementReferenceException e) {
			flag = false;
		}
		return flag;
	}
	
	/*
	 * *************************Method: mouseHoverJScript ***
	 * *************Description: hover mouse and click by JS **
	 */
	public void mouseHoverJScript(WebElement HoverElement) {
		try {
			if (isElementPresent(HoverElement)) {

				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
				((JavascriptExecutor) driver).executeScript(mouseOverScript, HoverElement);

			} else {
				System.out.println("Element was not visible to hover " + "\n");

			}
		} catch (StaleElementReferenceException e) {
			System.out.println(
					"Element with " + HoverElement + "is not attached to the page document" + e.getStackTrace());
		} catch (NoSuchElementException e) {
			System.out.println("Element " + HoverElement + " was not found in DOM" + e.getStackTrace());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error occurred while hovering" + e.getStackTrace());
		}
	}



}
