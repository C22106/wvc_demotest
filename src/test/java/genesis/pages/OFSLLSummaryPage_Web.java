package genesis.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OFSLLSummaryPage_Web extends OFSLLBasePage {

	public static final Logger log = Logger.getLogger(OFSLLSummaryPage_Web.class);

	public OFSLLSummaryPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	public int Check;
	public String todays_payoff_amount, future_payoff_amount, Other_amount, LC_due_amount, Total_sum_amount,
			Total_sum_amount_Inc, Interest_Rate;
	public static String Payment_interest;

	// Bankruptcy

	protected By summaryTab = By.xpath("//div[@class='x13t']/a[text()='Summary']");
	protected By condList = By.xpath(
			"(//div[@aria-label='Conditions']//div[2]//table//tr[@_afrrk='0'])[1]//td[@role='presentation']//td//span");
	protected By condTable = By.xpath("(//div[@aria-label='Conditions']//div[2]//table[@class='x14q x15f'])[1]");
	protected By condCodeList = By
			.xpath("(//div[@aria-label='Conditions']//div[2]//table//tr//td[@role='presentation']//td[1]//span)");

	// SCRA val

	protected By Activeduty_indicator = By.xpath(
			"//td[@class='xu x4z']//label[text()='Active Military Duty']//parent::td//following-sibling::td//span");
	protected By Militaryduty_indicator = By
			.xpath("//td[@class='x15 x4z']//label[text()='Military Duty']//parent::td//following-sibling::td");
	protected By Account_Rel = By.xpath("(//span[@class='x1z']//select[@class='x2h'])[1]");
	protected By condDropdown = By.xpath("//label[text()='Condition']/../../td[@class='xvo']/select[@class='x2h']");
	protected By actionDropdown = By.xpath("//label[text()='Action']/../../td[@class='xvo']/select[@class='x2h']");
	protected By resultDropdown = By.xpath("//label[text()='Result']/../../td[@class='xvo']/select[@class='x2h']");
	protected By postCall = By.xpath("//span[text()='Post Call ']");
	protected By condList_SCRA = By
			.xpath("//label[text()='Condition']//parent::td//following-sibling::td//select//option");
	protected By erromsg = By.xpath("//div[contains(text(),'not allowed while account is ON ACTIVE MILITARY DUTY')]");
	protected By ok_btn = By.xpath("//td[@class='p_AFResizable x1dx']//span[@class='xfv'and text()='OK']");

	// Get Summary Amount
	protected By todays_payoff_amt = By
			.xpath("//td[@class='x15 x4z']//label[text()='Todays Payoff']//parent::td//following-sibling::td");
	protected By Future_payoff_amt = By
			.xpath("//td[@class='x15 x4z']//label[text()='Future Payoff']//parent::td//following-sibling::td");
	protected By other_amt = By
			.xpath("//td[@class='x15 x4z']//label[text()='Other Due']//parent::td//following-sibling::td");
	protected By Total_sum_amt = By
			.xpath("//td[@class='x15 x4z']//label[text()='Total Due']//parent::td//following-sibling::td");
	protected By Total_sum_amt_inc = By.xpath(
			"//td[@class='x15 x4z']//label[text()='Total Due(incl current due)']//parent::td//following-sibling::td");
	protected By LC_amt = By.xpath("//td[@class='x15 x4z']//label[text()='LC Due']//parent::td//following-sibling::td");
	protected By interest_Rate = By.xpath(
			"//div[contains(@id,'pt1:t1') and @role='grid']//div[@class='x14p']//td[contains(@id,'t1:0:c13')]//span");
	protected By Payment_int = By.xpath(
			"((//div[contains(@id,'pt1:o3') and @class='x6v' ])[1]//following-sibling::div[1])[2]//tr[@class='xta']//td//label[text()='Current Pmt']//parent::td//following-sibling::td");

	/*
	 * Method: navigateToSummary Description: Navigating to Summary Tab" Screen
	 * Date: May/2020 Author: Harsh Changes By: NA
	 */

	public void navigateToSummary() throws Exception {

		try {

			getElementInView(summaryTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(summaryTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS, "Navigate to Summary Tab");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: conditionCheck Description: Verifying conditions are getting
	 * added successfully on Summary screen" Screen Date: May/2020 Author: Harsh
	 * Changes By: NA
	 */

	public void conditionCheck() throws Exception {

		String expCode = testData.get("summary_condcode");
		WebElement table = getObject(condTable);
		boolean flag = false;
		try {

			String row_Count = table.getAttribute("_rowcount");
			int total_rows = Integer.parseInt(row_Count);
			// log.info("total rows"+ row_Count);
			List<WebElement> Concodes = driver.findElements(condCodeList);
			for (int i = 0; i < total_rows; i++) {

				getElementInView(Concodes.get(i));
				// log.info("The condition code number "+(i+1)+" is : "+
				// getElementText(Concodes.get(i)));

				if (expCode.equals(getElementText(Concodes.get(i)))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Condition code added successfully on Summary screen");
					flag = true;
					break;
				}

			}

			if (!flag) {

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Condition code not added on Summary screen");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: val_Transcation_summary Description: Validate transaction form
	 * summary tab Date: June/2020 Author: Ankush Changes By: NA
	 */
	public void val_Transcation_summary(String flag) throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			switch (testData.get("FieldName")) {
			case "FEE MAINTENANCE":

				if (flag.equalsIgnoreCase("ADD")) {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}

					/*
					 * Harsh: This is commented as part of defect GPI-7927
					 * 
					 * /*getElementInView(other_amt); String
					 * other_amt_aftTxn=getElementText(other_amt);
					 * if(Double.valueOf(other_amt_aftTxn.replaceAll(",", ""))==
					 * Double.valueOf(Other_amount.replaceAll(",",
					 * ""))+Double.valueOf(testData.get("TransactionAmount"))){
					 * 
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.PASS,
					 * " Other amount validated successfully"); }else{
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.FAIL,
					 * " Other amount validation failed"); }
					 * 
					 * getElementInView(Total_sum_amt); String
					 * total_amt_aftTxn=getElementText(Total_sum_amt);
					 * if(Double.valueOf(total_amt_aftTxn.replaceAll(",", ""))==
					 * Double.valueOf(Total_sum_amount.replaceAll(",",
					 * ""))+Double.valueOf(testData.get("TransactionAmount"))){
					 * 
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.PASS,
					 * " Total amount validated successfully"); }else{
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.FAIL,
					 * " Total amount validation failed"); }
					 * 
					 * getElementInView(Total_sum_amt_inc); String
					 * total_amt_inc_aftTxn=getElementText(Total_sum_amt_inc);
					 * if(Double.valueOf(total_amt_inc_aftTxn.replaceAll(",",
					 * ""))==
					 * Double.valueOf(Total_sum_amount_Inc.replaceAll(",",
					 * ""))+Double.valueOf(testData.get("TransactionAmount"))){
					 * 
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.PASS,
					 * " Total amount (incl current due) validated successfully"
					 * ); }else{ tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.FAIL,
					 * " Total amount (incl current due) validation failed"); }
					 */

				} else {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}
					/*
					 * Harsh: This is commented as part of defect GPI-7927
					 * 
					 * /*getElementInView(other_amt); String
					 * other_amt_aftTxn=getElementText(other_amt);
					 * if(Double.valueOf(other_amt_aftTxn.replaceAll(",", ""))==
					 * Double.valueOf(Other_amount.replaceAll(",",
					 * ""))-Double.valueOf(testData.get("TransactionAmount"))){
					 * 
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.PASS,
					 * " Other amount validated successfully"); }else{
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.FAIL,
					 * " Other amount validation failed"); }
					 * 
					 * getElementInView(Total_sum_amt); String
					 * total_amt_aftTxn=getElementText(Total_sum_amt);
					 * if(Double.valueOf(total_amt_aftTxn.replaceAll(",", ""))==
					 * Double.valueOf(Total_sum_amount.replaceAll(",",
					 * ""))-Double.valueOf(testData.get("TransactionAmount"))){
					 * 
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.PASS,
					 * " Total amount validated successfully"); }else{
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.FAIL,
					 * " Total amount validation failed"); }
					 * 
					 * getElementInView(Total_sum_amt_inc); String
					 * total_amt_inc_aftTxn=getElementText(Total_sum_amt_inc);
					 * if(Double.valueOf(total_amt_inc_aftTxn.replaceAll(",",
					 * ""))==
					 * Double.valueOf(Total_sum_amount_Inc.replaceAll(",",
					 * ""))-Double.valueOf(testData.get("TransactionAmount"))){
					 * 
					 * tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.PASS,
					 * " Total amount (incl current due) validated successfully"
					 * ); }else{ tcConfig.updateTestReporter("CustomerSummary",
					 * "val_Transcation_summary", Status.FAIL,
					 * " Total amount (incl current due) validation failed"); }
					 */

				}
				break;
			case "FEE LATE CHARGE":

				if (flag.equalsIgnoreCase("ADD")) {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}

					getElementInView(Total_sum_amt);
					String total_amt_aftTxn = getElementText(Total_sum_amt);
					if (Double.valueOf(total_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(
							Total_sum_amount.replaceAll(",", "")) + Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount validation failed");
					}

					getElementInView(Total_sum_amt_inc);
					String total_amt_inc_aftTxn = getElementText(Total_sum_amt_inc);
					if (Double.valueOf(total_amt_inc_aftTxn.replaceAll(",", "")) == Double
							.valueOf(Total_sum_amount_Inc.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount (incl current due) validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount (incl current due) validation failed");
					}
					getElementInView(LC_amt);
					String LC_amt_aftTxn = getElementText(LC_amt);
					if (Double.valueOf(
							LC_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(LC_due_amount.replaceAll(",", ""))
									+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Late Charge amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								"  Late Charge amount validation failed");
					}

				} else {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}

					getElementInView(Total_sum_amt);
					String total_amt_aftTxn = getElementText(Total_sum_amt);
					if (Double.valueOf(total_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(
							Total_sum_amount.replaceAll(",", "")) - Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount validation failed");
					}

					getElementInView(Total_sum_amt_inc);
					String total_amt_inc_aftTxn = getElementText(Total_sum_amt_inc);
					if (Double.valueOf(total_amt_inc_aftTxn.replaceAll(",", "")) == Double
							.valueOf(Total_sum_amount_Inc.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount (incl current due) validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount (incl current due) validation failed");
					}

					getElementInView(LC_amt);
					String LC_amt_aftTxn = getElementText(LC_amt);
					if (Double.valueOf(
							LC_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(LC_due_amount.replaceAll(",", ""))
									- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Late Charge amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								"  Late Charge amount validation failed");
					}

				}
				break;

			case "DUES INTEREST":

				if (flag.equalsIgnoreCase("ADD")) {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}

					getElementInView(other_amt);
					String other_amt_aftTxn = getElementText(other_amt);
					if (Double.valueOf(
							other_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(Other_amount.replaceAll(",", ""))
									+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Other amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Other amount validation failed");
					}

					getElementInView(Total_sum_amt);
					String total_amt_aftTxn = getElementText(Total_sum_amt);
					if (Double.valueOf(total_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(
							Total_sum_amount.replaceAll(",", "")) + Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount validation failed");
					}

					getElementInView(Total_sum_amt_inc);
					String total_amt_inc_aftTxn = getElementText(Total_sum_amt_inc);
					if (Double.valueOf(total_amt_inc_aftTxn.replaceAll(",", "")) == Double
							.valueOf(Total_sum_amount_Inc.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount (incl current due) validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount (incl current due) validation failed");
					}

				} else {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}

					getElementInView(other_amt);
					String other_amt_aftTxn = getElementText(other_amt);
					if (Double.valueOf(
							other_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(Other_amount.replaceAll(",", ""))
									- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Other amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Other amount validation failed");
					}

					getElementInView(Total_sum_amt);
					String total_amt_aftTxn = getElementText(Total_sum_amt);
					if (Double.valueOf(total_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(
							Total_sum_amount.replaceAll(",", "")) - Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount validation failed");
					}

					getElementInView(Total_sum_amt_inc);
					String total_amt_inc_aftTxn = getElementText(Total_sum_amt_inc);
					if (Double.valueOf(total_amt_inc_aftTxn.replaceAll(",", "")) == Double
							.valueOf(Total_sum_amount_Inc.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount (incl current due) validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount (incl current due) validation failed");
					}

				}
				break;

			case "DOWN PAYMENT":

				if (flag.equalsIgnoreCase("ADD")) {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}

					getElementInView(other_amt);
					String other_amt_aftTxn = getElementText(other_amt);
					if (Double.valueOf(
							other_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(Other_amount.replaceAll(",", ""))
									+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Other amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Other amount validation failed");
					}

					getElementInView(Total_sum_amt);
					String total_amt_aftTxn = getElementText(Total_sum_amt);
					if (Double.valueOf(total_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(
							Total_sum_amount.replaceAll(",", "")) + Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount validation failed");
					}

					getElementInView(Total_sum_amt_inc);
					String total_amt_inc_aftTxn = getElementText(Total_sum_amt_inc);
					if (Double.valueOf(total_amt_inc_aftTxn.replaceAll(",", "")) == Double
							.valueOf(Total_sum_amount_Inc.replaceAll(",", ""))
							+ Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount (incl current due) validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount (incl current due) validation failed");
					}

				} else {

					getElementInView(todays_payoff_amt);
					String today_payoff_aftTxn = getElementText(todays_payoff_amt);
					if (Double.valueOf(today_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(todays_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Todays payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Todays payoff amount validation failed");
					}

					getElementInView(Future_payoff_amt);
					String future_payoff_aftTxn = getElementText(Future_payoff_amt);
					if (Double.valueOf(future_payoff_aftTxn.replaceAll(",", "")) == Double
							.valueOf(future_payoff_amount.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Future payoff amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Future payoff amount validation failed");
					}

					getElementInView(other_amt);
					String other_amt_aftTxn = getElementText(other_amt);
					if (Double.valueOf(
							other_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(Other_amount.replaceAll(",", ""))
									- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Other amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Other amount validation failed");
					}

					getElementInView(Total_sum_amt);
					String total_amt_aftTxn = getElementText(Total_sum_amt);
					if (Double.valueOf(total_amt_aftTxn.replaceAll(",", "")) == Double.valueOf(
							Total_sum_amount.replaceAll(",", "")) - Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount validation failed");
					}

					getElementInView(Total_sum_amt_inc);
					String total_amt_inc_aftTxn = getElementText(Total_sum_amt_inc);
					if (Double.valueOf(total_amt_inc_aftTxn.replaceAll(",", "")) == Double
							.valueOf(Total_sum_amount_Inc.replaceAll(",", ""))
							- Double.valueOf(testData.get("TransactionAmount"))) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Total amount (incl current due) validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Total amount (incl current due) validation failed");
					}

				}
			case "SCRA":
				OFSLLCustServicePage_Web cs = new OFSLLCustServicePage_Web(tcConfig);

				if (flag.equalsIgnoreCase("OFFDuty")) {

					getElementInView(interest_Rate);
					String New_interest = getElementText(interest_Rate);
					if (New_interest.equals(testData.get("TransactionAmount"))) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Interest Rate validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								"Interest Rate validation failed");
					}

					if (Double.valueOf(cs.New_amount.replaceAll(",", "")) > Double
							.valueOf(Payment_interest.replaceAll(",", ""))) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Payment validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Payment validation failed");
					}

				} else {

					getElementInView(interest_Rate);
					String New_interest = getElementText(interest_Rate);
					if (New_interest.equals(testData.get("TransactionAmount"))) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Interest Rate validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								"Interest Rate validation failed");
					}

					if (Double.valueOf(cs.New_amount.replaceAll(",", "")) < Double
							.valueOf(Payment_interest.replaceAll(",", ""))) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Payment validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								" Payment validation failed");
					}

				}

			default:
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: val_SCRA_summary Description: Validate SCRA form summary tab
	 * Date: June/2020 Author: Ankush Changes By: NA
	 */
	public void val_SCRA_summary(String flag) throws Exception {

		try {
			if (flag.equalsIgnoreCase("ONDuty")) {
				WebElement table = getObject(condTable);
				String row_Count = table.getAttribute("_rowcount");
				int total_rows = Integer.parseInt(row_Count);
				// log.info("total rows"+ row_Count);
				List<WebElement> Concodes = driver.findElements(condCodeList);
				int Check = 0;
				int i = 0, j = 0;

				for (i = 0; i < total_rows; i++) {

					getElementInView(Concodes.get(i));

					String[] codes = testData.get("summary_condcode").split(",");
					for (j = 0; j < codes.length; j++) {
						if (codes[j].equals(getElementText(Concodes.get(i)))) {
							tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
									" Condition code " + codes[j] + " is populating");
							Check = Check + 1;
							break;
						} else if (codes[j + 1].equals(getElementText(Concodes.get(i)))) {
							tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
									" Condition code " + codes[j + 1] + " is populating");
							Check = Check + 1;
							break;
						} else {
							break;
						}
					}
				}
				if (Check == 2) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Condition codes are validated successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Condition codes validation failed");
				}
				getElementInView(Activeduty_indicator);
				if (getElementText(Activeduty_indicator).equals("Y")) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Active Duty Indicator validated successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Active Duty Indicator validation failed");
				}

				getElementInView(Militaryduty_indicator);
				if (getElementText(Militaryduty_indicator).equals("Y")) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Military Duty Indicator validated successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Military Duty Indicator validation failed");
				}

			} else {

				WebElement table = getObject(condTable);
				String row_Count = table.getAttribute("_rowcount");
				int total_rows = Integer.parseInt(row_Count);
				log.info("total rows" + row_Count);
				List<WebElement> Concodes = driver.findElements(condCodeList);

				String[] codes = testData.get("summary_condcode").split(",");
				int Check = 0;
				int i = 0, j = 0;
				boolean check_cond = false;
				for (i = 0; i < total_rows; i++) {

					getElementInView(Concodes.get(i));

					for (j = 0; j < codes.length; j++) {
						log.info(getElementText(Concodes.get(i)));
						log.info(getElementText(Concodes.get(i + 1)));
						if (!codes[j].equals(getElementText(Concodes.get(i)))
								&& !codes[j + 1].equals(getElementText(Concodes.get(i)))) {

							// log.info(getElementText(Concodes.get(i)));
							// log.info(getElementText(Concodes.get(i+1)));
							Check = Check + 1;
							check_cond = true;
							break;
						} else {
							break;
						}
					}
				}
				if (Check == 0 || check_cond) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Condition code " + codes[j] + " is not populating");
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Condition code " + codes[j + 1] + " is not populating");

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Condition codes are validated successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Condition codes validation failed");
				}
				getElementInView(Activeduty_indicator);
				if (getElementText(Activeduty_indicator).equals("N")) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Active Duty Indicator validated successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Active Duty Indicator validation failed");
				}

				getElementInView(Militaryduty_indicator);
				if (getElementText(Militaryduty_indicator).equals("N")) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Military Duty Indicator validated successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Military Duty Indicator validation failed");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateSCRA_summary amount Description: Validates SCRA
	 * transaction from summary Screen Date: June/2020 Author: Ankush Changes
	 * By: NA
	 */

	public void validateSCRA_summary(String flag) throws Exception {

		try {

			navigateToSummary();
			val_SCRA_summary(flag);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"SCRA Summary validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validatecallActivity_summary amount Description: Validates call
	 * Activity from summary Screen Date: July/2020 Author: Harsh Changes By: NA
	 */

	public void validatecallActivity_summary(String flag) throws Exception {

		try {

			List<WebElement> Concodes = driver.findElements(condList_SCRA);
			getElementInView(condDropdown);
			clickElementBy(condDropdown);

			String[] codes = testData.get("summary_condcode").split(",");
			int total_codes = Concodes.size();
			int count = 0;

			if (flag.equals("ONDuty")) {
				for (int i = 0; i < total_codes; i++) {

					for (int j = 0; j < codes.length; j++) {
						if (codes[j].equals(getElementText(Concodes.get(i)))) {
							tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
									" Condition code " + codes[j]
											+ " is  populating in the Condition drop down of Add Call Activity");
							count++;
						}

					}
					log.info(count);

				}

				if (count < 2) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Condition codes are not populating in the condition drop down of Add Call Activity");
				}
			}

			else if (flag.equals("OFFDuty")) {

				for (int i = 0; i < total_codes; i++) {

					for (int j = 0; j < codes.length; j++) {
						if (codes[j].equals(getElementText(Concodes.get(i)))) {
							tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
									" Condition code " + codes[j]
											+ " is  populating in the Condition drop down of Add Call Activity");
							count++;
						}

					}
					log.info(count);

				}

				if (count == 0) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Condition codes are not populating in the condition drop down of Add Call Activity");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateconditioncodes Description: Validates condition codes
	 * from summary Screen Date: August/2020 Author: Harsh Changes By: NA
	 */

	public void validateconditioncodes(String action, String result) throws Exception {

		try {

			String[] action_code = action.split(",");
			String[] result_code = result.split(",");
			log.info(action_code.length);
			for (int i = 0; i < action_code.length; i++) {

				applyCondition(action_code[i], result_code[i]);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: applyCondition Description: Apply condition code in Add Call
	 * Activity Date: August/2020 Author: Harsh Changes By: NA
	 */

	public void applyCondition(String action, String result) throws Exception {

		try {

			getElementInView(actionDropdown);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(actionDropdown, action);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(resultDropdown, result);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(condDropdown, "NONE");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCall);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(erromsg)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"System is not allowing condition code" + action + "," + result);
				clickElementBy(ok_btn);
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"System is allowing condition code" + action + "," + result);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validate_Transction_summary amount Description: Validates
	 * transaction from summary Screen Date: June/2020 Author: Ankush Changes
	 * By: NA
	 */

	public void validate_Transction_summary(String flag) throws Exception {

		try {
			navigateToSummary();
			val_Transcation_summary(flag);
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Tranaction Summary validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * Method: get_amount_summary amount Description: Get transaction from
	 * summary Screen Date: June/2020 Author: Ankush Changes By: NA
	 */

	public void get_amount_summary() throws Exception {

		try {

			getElementInView(todays_payoff_amt);
			todays_payoff_amount = getElementText(todays_payoff_amt);

			getElementInView(Future_payoff_amt);
			future_payoff_amount = getElementText(Future_payoff_amt);

			getElementInView(other_amt);
			Other_amount = getElementText(other_amt);

			getElementInView(LC_amt);
			LC_due_amount = getElementText(LC_amt);

			getElementInView(Total_sum_amt);
			Total_sum_amount = getElementText(Total_sum_amt);

			getElementInView(Total_sum_amt_inc);
			Total_sum_amount_Inc = getElementText(Total_sum_amt_inc);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Tranaction Summary amount fetched successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * Method: get_interest_summary Description: Get transaction from summary
	 * Screen Date: September/2020 Author: Ankush Changes By: NA
	 */

	public void get_interest_summary() throws Exception {

		try {

			/*
			 * getElementInView(interest_Rate); Interest_Rate=
			 * getElementText(interest_Rate);
			 */

			getElementInView(Payment_int);
			Payment_interest = getElementText(Payment_int);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Payment amount fetched successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
