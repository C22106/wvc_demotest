package genesis.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OFSLLCustDetailsPage_Web extends OFSLLBasePage {

	public static final Logger log = Logger.getLogger(OFSLLCustDetailsPage_Web.class);

	public OFSLLCustDetailsPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		// TODO Auto-generated constructor stub
	}

	// Update E-mail

	protected By updatedEmailId = By.xpath("//span[@class='x25' and text()='" + testData.get("email") + "']");
	protected By custDetailsTab = By.xpath("//div[@class='x13t']/a[text()='Customer Details']");
	protected By viewButtonCustInfo = By.xpath("//span[@class='xfy' and text()='V']");
	protected By emailField = By.xpath("//label[text()='Email']/../../td[@class='xvo']/input");
	protected By saveandReturn = By.xpath("//span[@class='xfy' and text()='S']");
	protected By emailErrormsg = By.xpath("//div[text()='Not a valid email address']");
	protected By editButton = By.xpath("//span[@class='xfv']/span[text()='E']");

	// EditAddress

	protected By editbtnAddress = By.xpath("//a[@class='xfn']//span[text()='Edit']");
	protected By editzipDropdown = By.xpath("//input[contains(@name,'adrZip')]");
	protected By editState = By.xpath("//label[text()='State']//parent::td//following-sibling::td//select");
	protected By editPhoneNumber = By.xpath("//label[contains(text(),'Phone (Required)')]//parent::span//input");
	protected By MadatoryFiledError = By
			.xpath("//td[@class='x1at' and contains(text(),'Error: A value is required.') ]");
	protected By InvalidNumberError = By.xpath("//td[@class='x1at' and contains(text(),'Error: Invalid Number') ]");
	protected By Permission_chkbox = By.xpath("//input[@class='xu4'  and contains(@name,'sbc99')]");
	protected By Permission_chkbox_chhecked = By
			.xpath("//input[@class='xu4'  and contains(@name,'sbc99') and contains(@checked,'')]");
	protected By viewBtnList = By.xpath("//a[@class='xfn']//span[@class='xfv' and contains(text(),'View')]");
	protected By viewZip = By.xpath("//td//span[contains(@id,'adrZip')]");
	protected By viewPhone = By.xpath("//span[@class='x1u p_AFReadOnly']//span");

	// Add Address

	protected By addAddress = By.xpath("//a[@class='xfn']//span[text()='Add']");
	protected By MadatoryFiledError_Add = By
			.xpath("//div[contains(text(),'Messages for this page are listed below.')]");
	protected By InvalidFiledError_Add = By
			.xpath("//div[@class='x15p'  and contains(text(),'The number entered is invalid')]");
	protected By Type_drpdwn_list = By.xpath("//label[text()='Type']//parent::td//parent::tr//select");
	protected By Country_drpdwn = By.xpath("//label[text()='Country']//parent::td//parent::tr//select");
	protected By viewType = By.xpath("//label[text()='Type']//parent::td//parent::tr//td//span[@class='x2h']");
	protected By viewContry = By.xpath("//label[text()='Country']//parent::td//parent::tr//td//span[@class='x2h']");
	protected By work_adrs_list = By
			.xpath("//span[text()='WORK']//parent::td//following-sibling::td[1]//span[text()='Y']");
	protected By home_adrs_list = By
			.xpath("//span[text()='HOME']//parent::td//following-sibling::td[1]//span[text()='Y']");

	/*
	 * Method: updateEmail Description: Update email id on customer service page
	 * Screen Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void updateEmail() throws Exception {

		String emailId = testData.get("email");

		try {

			invalidEmailCheck();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearField(emailField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(emailField, emailId);
			clickElementBy(saveandReturn);
			validateEmail();

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: invalidEmailCheck Description: invalidEmailCheck on customer
	 * service page Screen Date: May/2020 Author: Harsh Changes By: NA
	 */

	public void invalidEmailCheck() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(custDetailsTab);
			clickElementBy(custDetailsTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(editButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(emailField);
			clearField(emailField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// sendKeysBy(emailField,emailId);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(emailField, "A@B");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(emailErrormsg)) {
				// log.info("Invalid email id check validated");
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Invalid email error Message validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Invalid email error Message validation failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: edit Address error Description: Verify address update is getting
	 * error with wrong data Date: May/2020 Author: Ankush Changes By: NA
	 */

	public void editAddressErrorCheck() throws Exception {

		try {
			// waitUntilElementVisibleBy(driver, actionDropdown, 10);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(custDetailsTab);
			clickElementBy(custDetailsTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(editbtnAddress);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * getElementInView(editzipDropdown);
			 * 
			 * clearField(editzipDropdown);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * sendKeysBy(editzipDropdown,testData.get("ZipCode"));
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */

			getElementInView(editPhoneNumber);

			clearField(editPhoneNumber);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(MadatoryFiledError)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Maditory field error Message validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Maditory field error Message validation failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(editPhoneNumber, "ASD234$@");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(InvalidNumberError)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Invalid Number error Message validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Invalid Number error Message validation failed");
			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Address edit error messages validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateUpdatedAddress Description: Verify updated address is
	 * getting reflected Date: May/2020 Author: Ankush Changes By: NA
	 */

	public void validateUpdatedAddress() throws Exception {

		try {
			// waitUntilElementVisibleBy(driver, actionDropdown, 10);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * getElementInView(custDetailsTab); clickElementBy(custDetailsTab);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */
			List<WebElement> ViewBtn = driver.findElements(viewBtnList);
			ViewBtn.get(ViewBtn.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(viewZip);
			String Zipcode = getElementText(viewZip);
			if (Zipcode.contains(testData.get("ZipCode"))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Updated ZIP validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Updated ZIP validation failed");
			}

			getElementInView(viewPhone);
			String viewphonestr = getElementText(viewPhone);
			// log.info(viewphonestr);
			if (viewphonestr.contains(testData.get("PhoneNumber"))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Updated Phone validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Updated Phone validation failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// clickElementBy(saveandReturn);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Updated Information validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: updateAddress Description: Update Address of customer Screen
	 * Date: May/2020 Author: Ankush Changes By: NA
	 */

	public void updateAddress() throws Exception {

		try {
			editAddressErrorCheck();
			editAddress();
			validateUpdatedAddress();

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Address Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add Address Description: Add Address on customer Screen Date:
	 * May/2020 Author: Ankush Changes By: NA
	 */

	public void addAddress() throws Exception {

		try {
			addAddressErrorCheck();
			addAddress_new();
			validateaddeddAddress();
			currentAddressValidation();

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS, "Address Added successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: add Address error Description: Verify address update is getting
	 * error with wrong data Date: May/2020 Author: Ankush Changes By: NA
	 */

	public void addAddressErrorCheck() throws Exception {

		try {
			// waitUntilElementVisibleBy(driver, actionDropdown, 10);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(custDetailsTab);
			clickElementBy(custDetailsTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(addAddress);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * getElementInView(editzipDropdown);
			 * 
			 * clearField(editzipDropdown);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * sendKeysBy(editzipDropdown,testData.get("ZipCode"));
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */

			getElementInView(editPhoneNumber);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(MadatoryFiledError_Add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Maditory field error Message validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Maditory field error Message validation failed");
			}
			/*
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * sendKeysBy(editPhoneNumber,"ASD234$@");
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * clickElementBy(saveandReturn);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * if(verifyObjectDisplayed(InvalidFiledError_Add)){
			 * tcConfig.updateTestReporter("CustomerService",
			 * "addAddressErrorCheck", Status.PASS,
			 * "Invalid Number error Message validated successfully"); }else{
			 * tcConfig.updateTestReporter("CustomerService",
			 * "addAddressErrorCheck", Status.FAIL,
			 * "Invalid Number error Message validation failed"); }
			 */

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Address edit error messages validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: currentAddressValidation Description: Verify current address is
	 * getting updated Date: May/2020 Author: Ankush Changes By: Harsh/Ankush
	 * changes related to only one Work and Home address
	 */

	public void currentAddressValidation() throws Exception {

		try {
			// waitUntilElementVisibleBy(driver, actionDropdown, 10);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> work = driver.findElements(work_adrs_list);
			// getElementInView(work.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// String work_addrs=getElementText(work.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> home = driver.findElements(home_adrs_list);
			// getElementInView(home.get(0));
			// String home_addrs=getElementText(home.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (work.size() == 1) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Current work address validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Current work address validation failed");
			}
			if (home.size() == 1) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Current home address validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Current home address validation failed");
			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Current Address validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: addAddress_new Description: Adding address of type work Date:
	 * May/2020 Author: Ankush Changes By: NA
	 */

	public void addAddress_new() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().refresh();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getElementInView(editzipDropdown);

			Select type = new Select(driver.findElements(Type_drpdwn_list).get(0));
			type.selectByVisibleText("WORK");

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			Select country = new Select(driver.findElement(Country_drpdwn));
			country.selectByVisibleText("UNITED STATES");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(editzipDropdown);

			clearField(editzipDropdown);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(editzipDropdown, testData.get("ZipCode"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			getElementInView(editPhoneNumber);

			clearField(editPhoneNumber);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(editPhoneNumber, testData.get("PhoneNumber"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// Added for SaaS env
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectByText(editState, testData.get("State"));

			clickElementJSWithWait(Permission_chkbox);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveandReturn);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS, "Address added successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateaddedAddress Description: Verify added address is getting
	 * reflected Date: May/2020 Author: Ankush Changes By: NA
	 */

	public void validateaddeddAddress() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> ViewBtn = driver.findElements(viewBtnList);
			ViewBtn.get(ViewBtn.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(viewType);
			String Type_view = getElementText(viewType);
			if (Type_view.contains("WORK")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Updated Type validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Updated Type validation Failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Country_view = getElementText(viewContry);
			if (Country_view.contains("UNITED STATES")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Updated Country validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Updated Country validation Failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			getElementInView(viewZip);
			String Zipcode = getElementText(viewZip);
			if (Zipcode.contains(testData.get("ZipCode"))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Updated ZIP validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Updated ZIP validation failed");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(viewPhone);
			String viewphonestr = getElementText(viewPhone);
			// log.info(viewphonestr);
			if (viewphonestr.contains(testData.get("PhoneNumber"))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Updated Phone validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Updated Phone validation failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// clickElementBy(saveandReturn);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Updated Information validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: edit Address Description: Verify address is getting edited Date:
	 * May/2020 Author: Ankush Changes By: NA
	 */

	public void editAddress() throws Exception {

		try {
			// waitUntilElementVisibleBy(driver, actionDropdown, "ExplicitLowWait");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * getElementInView(custDetailsTab); clickElementBy(custDetailsTab);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * clickElementBy(editbtnAddress);
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(editzipDropdown);

			clearField(editzipDropdown);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(editzipDropdown, testData.get("ZipCode"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(editPhoneNumber);

			clearField(editPhoneNumber);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(editPhoneNumber, testData.get("PhoneNumber"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectByText(editState, testData.get("State"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				if (verifyObjectDisplayed(Permission_chkbox_chhecked)) {
					// log.info("Pass ");
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"CheckBox already Checked");
				} else {
					clickElementJSWithWait(Permission_chkbox);
				}
			} catch (Exception e) {
				e.printStackTrace();
				clickElementJSWithWait(Permission_chkbox);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveandReturn);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS, "Address edited successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateEmail Description: Validate email id is updated in
	 * Customer Information screen: May/2020 Author: Harsh Changes By: NA
	 */
	public void validateEmail() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(viewButtonCustInfo);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			try {

				if (verifyObjectDisplayed(updatedEmailId)) {
					getElementInView(updatedEmailId);
					// log.info("Email id updated");
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS, "Email id is updated");
				}

				else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Email id is not updated");
				}
			} catch (Exception e) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL, "Email id is not updated");
				e.printStackTrace();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
