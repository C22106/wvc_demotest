package genesis.pages;

import java.awt.Robot;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OFSLLPaymentsPage_Web extends OFSLLBasePage {

	public static final Logger log = Logger.getLogger(OFSLLPaymentsPage_Web.class);
	public static String Adjust_min, Adjust_max, Total_amt_val, Date_pymt;
	public String payoff_amt_bfr_txn, TXN_Statement, AccStats;
	public int num;

	public OFSLLPaymentsPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	OFSLLSummaryPage_Web OFSLLSummaryPage = new OFSLLSummaryPage_Web(tcConfig);
	OFSLLHomePage_Web homePage = new OFSLLHomePage_Web(tcConfig);
	OFSLLCustServicePage_Web custServicePage = new OFSLLCustServicePage_Web(tcConfig);

	protected By addbutton_batch = By.xpath("//a[@role='button']//span[text()='Add']");
	protected By company = By.xpath("//select[contains(@id,'soc1')]");
	protected By Batch_Type = By.xpath("//select[contains(@id,'soc5')]");
	protected By Amount = By.xpath("//input[contains(@id,'it9')]");
	protected By Branch = By.xpath("//select[contains(@id,'soc2')]");
	protected By Total_num = By.xpath("//input[contains(@id,'it7')]");
	protected By Date_pay = By.xpath("//input[contains(@id,'it4')]");
	protected By Date_payment = By.xpath("//input[contains(@id,'it1')]");
	protected By SaveAndReturn = By.xpath("//span[(text()='S')]");
	protected By edit_payment = By.xpath("(//span[(text()='Edit')])[2]");
	protected By Populate_account = By.xpath("//a//span[text()='Populate Accounts']//parent::a");
	protected By enabled_populate = By.xpath(
			"//a//span[text()='Populate Accounts']//parent::a//parent::div[@class='p_AFHoverTarget xfl p_AFLeading']//a");
	protected By addbutton_payment = By.xpath("(//a[@role='button']//span[text()='Add'])[2]");
	protected By Add_multiple_Btn = By.xpath("//a[@accesskey='M']");
	protected By multi_acc_check = By.xpath("//input[contains(@id,'sbc35')]");
	protected By MasterAccount = By.xpath("//input[contains(@id,'iclov1')]");
	protected By AccountNumber = By.xpath("//input[contains(@id,'dPmtAcc')]");
	protected By YES_button = By.xpath("//span[(text()='Yes')]");
	protected By pmt_amt = By.xpath("//input[contains(@id,'it3')]");
	protected By mode = By.xpath("//select[contains(@id,'soc5')]");
	protected By reason = By.xpath("//select[contains(@id,'soc6')]");
	protected By reference = By.xpath("//input[contains(@id,'it7')]");
	protected By ErrorMsg = By.xpath("//td//div[contains(text(),'Group control Totals not matching')]");
	protected By Paid_error = By.xpath(
			"(//div[@aria-label='Payment Txns']//table[1]//table[1])//tr//td[contains(text(),'Transaction PAYMENT  not allowed for status PAID')]");
	protected By Process_dt_error = By.xpath(
			"(//div[@aria-label='Payment Txns']//table[1]//table[1])//tr//td[contains(text(),'greater than Process Dt')]");
	protected By Error_box = By
			.xpath("(//div[@aria-label='Payment Txns']//table[1]//table[1])//tr//td[contains(@id,'c11')]");
	protected By Ok_button = By.xpath("(//a//span[text()='OK'])[1]");
	protected By Edit_pay = By.xpath("(//a//span[text()='Edit'])[2]");
	protected By Post_button = By.xpath("//a//span//span[text()='P']");
	protected By show_all = By.xpath("(//input[@type='radio' and contains(@name,'sor1')])[2]");
	protected By Batch_status = By.xpath("(//div[@aria-label='Batch']//table[1]//table[1])[11]//tr//td[6]");
	protected By refresh_batch = By.xpath("(//div[contains(@title,'Click to refresh')])[1]");
	protected By Batch_num = By.xpath(
			"(//div[@aria-label='Batch']//table[1]//tr[contains(@class,'p_AFSelected')]//table[1])//tr//td[3]//span");
	// "(//div[@aria-label='Batch']//table[1]//table[1])[11]//tr//td[3]//span");
	protected By Paymnt_dt = By.xpath("(//div[@aria-label='Payments']//table[1]//table[1])//tr//td[8]//span");
	protected By ref_num = By.xpath("(//div[@aria-label='Payments']//table[1]//table[1])//tr//td[14]//span");
	protected By account_num = By
			.xpath("(//div[@aria-label='Payment Txns']//table[1]//tr[@role='row']//table[1])//td[1]//span");
	protected By acc_amount = By
			.xpath("(//div[@aria-label='Payment Txns']//table[1]//tr[@role='row']//table[1])//td[5]//span");
	protected By Payment_tab_open = By.xpath("(//a[@role='tab'  and (text()='Payments')])[1]");

	/*
	 * Method: Add_Batch_Payment Description: Verify Subtract interest Date:
	 * August/2020 Author: Ankush Changes By: NA
	 */
	public void Add_Batch_Payment() throws Exception {

		try {

			waitUntilElementVisibleBy(driver, addbutton_batch, "ExplicitLongWait");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(show_all);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(addbutton_batch);
			clickElementBy(addbutton_batch);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(company, testData.get("company"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(Batch_Type, "PMT MANUAL");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Amount, testData.get("TransactionAmount"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(Branch, "ALL");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Total_num, testData.get("Parameters"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Date_pymt = getCurrentDateInSpecificFormat("MM/dd/yyyy");
			fieldDataEnter(Date_pay, Date_pymt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(SaveAndReturn);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" Batch is added successfully! ");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add_Payment_Associates Description: Add Payments: August/2020
	 * Author: Ankush Changes By: Harsh/Ankush: Random number in the reference
	 * field, process date related changes
	 */
	public void Add_Payment_Associates() throws Exception {

		try {

			waitUntilElementVisibleBy(driver, addbutton_payment, "ExplicitLongWait");
			String Batch_Number = getElementText(Batch_num);
			getElementInView(addbutton_payment);
			clickElementBy(addbutton_payment);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(AccountNumber, testData.get("acctno"));
			//
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			int Payment = Integer.parseInt(testData.get("TransactionAmount"));
			String pay = Integer.toString(Payment);
			fieldDataEnter(pmt_amt, pay);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(mode, testData.get("FieldName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(reason, testData.get("FieldName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Random ran = new Random();
			int num = ran.nextInt(10000);
			fieldDataEnter(reference, Integer.toString(num));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(SaveAndReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Post_button);
			String Batch = "(//div[@aria-label='Batch']//table[1]//table[1])//tr//td[3]//span[text()='" + Batch_Number
					+ "']";
			String latestStatus = Batch + "//parent::td//parent::tr//td[6]";

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(Ok_button)) {
				clickElementBy(Ok_button);
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(show_all);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			for (int i = 0; i <= 6; i++) {
				getElementInView(By.xpath(latestStatus));
				if (getElementText(By.xpath(latestStatus)).equals("ERROR")) {
					break;
				} else if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
					break;
				} else {

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(refresh_batch);
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(By.xpath(Batch));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			scrollDownForElementJSBy(Process_dt_error);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getElementInView(Process_dt_error);
			if (verifyObjectDisplayed(Process_dt_error)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Error exist for wrong Process Date! ");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(Edit_pay);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				int Payment_re = Integer.parseInt(testData.get("TransactionAmount")) + 5;
				String pay_re = Integer.toString(Payment_re);
				fieldDataEnter(pmt_amt, pay_re);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String[] Process_date = getElementText(Process_dt_error).split(" ");
				log.info(Process_date[(Process_date.length) - 1]);
				fieldDataEnter(Date_payment, Process_date[(Process_date.length) - 1]);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(SaveAndReturn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(Post_button);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Process Date and Current date are same!");
			}

			if (verifyObjectDisplayed(ErrorMsg)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Error exist for wrong amount! ");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(Ok_button);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// String
				// Batch1="(//div[@aria-label='Batch']//table[1]//table[1])//tr//td[3]//span[text()='"+Batch_Number+"']";
				for (int i = 0; i <= 6; i++) {
					getElementInView(By.xpath(latestStatus));
					if (getElementText(By.xpath(latestStatus)).equals("ERROR")) {
						break;
					} else {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementBy(refresh_batch);
					}
				}
				clickElementBy(By.xpath(Batch));

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(Edit_pay);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(pmt_amt, testData.get("TransactionAmount"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(SaveAndReturn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(Post_button);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(show_all);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				getElementInView(By.xpath(latestStatus));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				log.info(getElementText(By.xpath(latestStatus)));
				for (int i = 0; i <= 6; i++) {
					getElementInView(By.xpath(latestStatus));
					if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
						break;
					} else {
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementBy(refresh_batch);
					}
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String msg = getElementText(Error_box);
				log.info("'Test is :" + msg);
				if (getElementText(By.xpath(latestStatus)).equals("POSTED") && getElementText(Error_box).equals(" ")) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Payment is added successfully! ");

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					getElementInView(Paymnt_dt);
					String Pay_dt = getElementText(Paymnt_dt);

					homePage.clickToCustomerService();
					custServicePage.searchAcctNo();
					String Ref_Num = Integer.toString(num);
					custServicePage.validate_Transctions_Payments(Ref_Num, Pay_dt);

				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Payment is not added! Validation not required.");
				}
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Error not exist for wrong amount!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add_Payment_Master_club Description: Verify Payment Transaction
	 * Date: Aug/2020 Author: Ankush Changes By: NA
	 */
	public void Add_Payment_Master_club() throws Exception {

		try {

			waitUntilElementVisibleBy(driver, addbutton_payment, "ExplicitLongWait");
			String Batch_Number = getElementText(Batch_num);
			getElementInView(addbutton_payment);
			clickElementBy(addbutton_payment);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(multi_acc_check);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(YES_button);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			fieldDataEnter(MasterAccount, testData.get("acctno"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			int Payment = Integer.parseInt(testData.get("TransactionAmount"));
			String pay = Integer.toString(Payment);
			fieldDataEnter(pmt_amt, pay);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(mode, testData.get("FieldName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(reason, testData.get("FieldName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Random ran = new Random();
			int num = ran.nextInt(10000);
			fieldDataEnter(reference, Integer.toString(num));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			getElementInView(Populate_account);

			do {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Robot r = new Robot();
				r.mouseMove(10, 10);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Populate_account);
				Point p = driver.findElement(Populate_account).getLocation();
				int x = p.getX();
				int y = p.getY();
				Dimension d = driver.findElement(Populate_account).getSize();
				int h = d.getHeight();
				int w = d.getWidth();
				r.mouseMove(x + (w / 2), y + (h / 2) + 115);
				r.delay(1500);
				getElementInView(Populate_account);
				mouseHoverJScript(driver.findElement(Populate_account));
				clickElementJSWithWait(Populate_account);
			} while (!verifyObjectDisplayed(SaveAndReturn));

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Post_button);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(show_all);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Batch = "(//div[@aria-label='Batch']//table[1]//table[1])//tr//td[3]//span[text()='" + Batch_Number
					+ "']";

			clickElementBy(By.xpath(Batch));

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			String latestStatus = Batch + "//parent::td//parent::tr//td[6]";
			getElementInView(By.xpath(latestStatus));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info(getElementText(By.xpath(latestStatus)));
			for (int i = 0; i <= 6; i++) {
				getElementInView(By.xpath(latestStatus));
				if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
					break;
				} else if (verifyObjectDisplayed(Process_dt_error)
						|| getElementText(By.xpath(latestStatus)).equals("ERROR")) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Error exist for wrong Process Date! ");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					scrollDownForElementJSBy(Process_dt_error);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					getElementInView(Process_dt_error);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(Edit_pay);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String[] Process_date = getElementText(Process_dt_error).split(" ");
					log.info(Process_date[(Process_date.length) - 1]);
					Date_pymt = Process_date[(Process_date.length) - 1];
					fieldDataEnter(Date_payment, Process_date[(Process_date.length) - 1]);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(SaveAndReturn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(Post_button);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					for (int j = 0; j <= 4; j++) {
						getElementInView(By.xpath(Batch));
						clickElementBy(By.xpath(Batch));
						if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
							break;
						} else {

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							clickElementBy(refresh_batch);
						}
					}

				} else {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(refresh_batch);
				}
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Payment is added successfully! ");

				homePage.clickToCustomerService();
				custServicePage.searchAcctNo();
				String Ref_Num = Integer.toString(num);
				custServicePage.validate_Transctions_Payments(Ref_Num, Date_pymt);

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Payment is not added! Validation not required.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add_Payment_Master_FW Description: Verify Payment Transaction
	 * Date: Sep/2020 Author: Ankush Changes By: Harsh/Ankush for picking the
	 * account nos from Payment Screen
	 */
	public void Add_Payment_Master_FW() throws Exception {

		try {

			waitUntilElementVisibleBy(driver, Add_multiple_Btn, "ExplicitLongWait");
			String Batch_Number = getElementText(Batch_num);
			log.info(Batch_Number);
			getElementInView(Add_multiple_Btn);
			clickElementBy(Add_multiple_Btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			for (int i = 0; i < Integer.parseInt(testData.get("Parameters")); i++) {
				List<WebElement> multi_check = driver.findElements(multi_acc_check);
				clickElementBy(multi_check.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(YES_button);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> Master_Acc = driver.findElements(MasterAccount);
				fieldDataEnter(Master_Acc.get(i), testData.get("acctno"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				sendKeyboardKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				int Payment = Integer.parseInt(testData.get("TransactionAmount"));
				String pay = Integer.toString(Payment / 2);
				List<WebElement> Pmt_Amt = driver.findElements(pmt_amt);
				fieldDataEnter(Pmt_Amt.get(i), pay);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> Mode = driver.findElements(mode);
				selectByText(Mode.get(i), testData.get("FieldName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> Reason = driver.findElements(reason);
				selectByText(Reason.get(i), testData.get("FieldName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Random ran = new Random();
				num = ran.nextInt(10000);
				List<WebElement> Reference = driver.findElements(reference);
				fieldDataEnter(Reference.get(i), Integer.toString(num));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				sendKeyboardKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				getElementInView(Populate_account);

				do {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					Robot r = new Robot();
					r.mouseMove(10, 10);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					getElementInView(Populate_account);
					Point p = driver.findElements(Populate_account).get(i).getLocation();
					int x = p.getX();
					int y = p.getY();
					Dimension d = driver.findElements(Populate_account).get(i).getSize();
					int h = d.getHeight();
					int w = d.getWidth();
					r.mouseMove(x + (w / 2), y + (h / 2) + 115);
					r.delay(1500);
					getElementInView(Populate_account);
					mouseHoverJScript(driver.findElements(Populate_account).get(i));
					clickElementJSWithWait(driver.findElements(Populate_account).get(i));
				} while (!verifyObjectDisplayed(SaveAndReturn));

				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (i == 0) {
					clickElementBy(edit_payment);
				}

			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Post_button);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(show_all);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Batch = "(//div[@aria-label='Batch']//table[1]//table[1])//tr//td[3]//span[text()='" + Batch_Number
					+ "']";

			clickElementBy(By.xpath(Batch));

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			String latestStatus = Batch + "//parent::td//parent::tr//td[6]";
			getElementInView(By.xpath(latestStatus));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info(getElementText(By.xpath(latestStatus)));
			for (int i = 0; i <= 6; i++) {
				getElementInView(By.xpath(Batch));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(By.xpath(Batch));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
					break;
				} else if (getElementText(By.xpath(latestStatus)).equals("ERROR")) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Error exist for wrong Process Date! ");
					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					clickElementBy(By.xpath(Batch));
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					scrollDownForElementJSBy(Process_dt_error);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					getElementInView(Process_dt_error);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(Edit_pay);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String[] Process_date = getElementText(Process_dt_error).split(" ");
					log.info(Process_date[(Process_date.length) - 1]);
					List<WebElement> Date_pay = driver.findElements(Date_payment);
					fieldDataEnter(Date_pay.get(0), Process_date[(Process_date.length) - 1]);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					fieldDataEnter(Date_pay.get(1), Process_date[(Process_date.length) - 1]);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(SaveAndReturn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(Post_button);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					for (int j = 0; j <= 4; j++) {
						getElementInView(By.xpath(Batch));
						clickElementBy(By.xpath(Batch));
						if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
							break;
						} else {

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							clickElementBy(refresh_batch);
						}
					}

				} else {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(refresh_batch);
				}
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getElementText(By.xpath(latestStatus)).equals("POSTED")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Payment is added successfully! ");
				for (int k = 0; k < Integer.parseInt(testData.get("Parameters")); k++) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					getElementInView(Paymnt_dt);
					List<WebElement> Paydate = driver.findElements(Paymnt_dt);
					clickElementBy(Paydate.get(k));
					String Pay_dt = getElementText(Paydate.get(k));
					getElementInView(ref_num);
					List<WebElement> Ref_number = driver.findElements(ref_num);
					String reference = getElementText(Ref_number.get(k));
					getElementInView(account_num);
					String ACC_Number = getElementText(account_num);
					String ACC_amount = getElementText(acc_amount);

					homePage.clickToCustomerService();
					custServicePage.searchAcctNo(ACC_Number);
					custServicePage.validate_Transctions_Payments(reference, Pay_dt, ACC_amount);

					if (k == 0) {
						clickElementBy(Payment_tab_open);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
				}

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" Payment is not added! Validation not required.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add_Payment_Paid Description: Verify Subtract interest Date:
	 * August/2020 Author: Ankush Changes By: Harsh: Random number in the
	 * reference field, process date change, error message change
	 */
	public void Add_Payment_Paid() throws Exception {

		try {

			waitUntilElementVisibleBy(driver, addbutton_payment, "ExplicitLongWait");
			String Batch_Number = getElementText(Batch_num);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(addbutton_payment);
			clickElementBy(addbutton_payment);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(AccountNumber, testData.get("acctno"));
			//
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			int Payment = Integer.parseInt(testData.get("TransactionAmount"));
			String pay = Integer.toString(Payment);
			fieldDataEnter(pmt_amt, pay);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(mode, testData.get("FieldName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(reason, testData.get("FieldName"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Random ran = new Random();
			int num = ran.nextInt(10000);
			fieldDataEnter(reference, Integer.toString(num));// random string
																// generator
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(SaveAndReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Post_button);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Batch = "(//div[@aria-label='Batch']//table[1]//table[1])//tr//td[3]//span[text()='" + Batch_Number
					+ "']";
			String latestStatus = Batch + "//parent::td//parent::tr//td[6]";

			clickElementBy(show_all);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(By.xpath(Batch));

			// log.info(getElementText(Batch_status));
			for (int i = 0; i <= 4; i++) {
				getElementInView(By.xpath(latestStatus));
				if (getElementText(By.xpath(latestStatus)).equals("ERROR"))
					break;
				if (getElementText(Batch_status).equals("POSTED")) {
					break;
				} else {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(refresh_batch);
				}
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(By.xpath(Batch));
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			// scrollDownForElementJSBy(Process_dt_error);
			/*
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * getElementInView(Process_dt_error);
			 */
			if (verifyObjectDisplayed(Process_dt_error)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Error exist for wrong Process Date! ");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(Edit_pay);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				String[] Process_date = getElementText(Process_dt_error).split(" ");
				log.info(Process_date[(Process_date.length) - 1]);
				fieldDataEnter(Date_payment, Process_date[(Process_date.length) - 1]);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(SaveAndReturn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(Post_button);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Process Date and Current date are same!");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			for (int i = 0; i <= 4; i++) {
				getElementInView(By.xpath(latestStatus));
				if (getElementText(By.xpath(latestStatus)).equals("ERROR"))
					break;
				if (getElementText(Batch_status).equals("POSTED")) {
					break;
				} else {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementBy(refresh_batch);
				}
			}
			if (getElementText(Batch_status).equals("POSTED")) {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				getElementInView(Paid_error);
				if (verifyObjectDisplayed(Paid_error)) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" Error exist for Paid account");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Error not exist for wrong amount!");
				}
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Payment is not posted No need to validate!");
			}

			waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
