package genesis.pages;

import java.util.ArrayList;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OFSLLLoginPage_Web extends OFSLLBasePage {

	public static final Logger log = Logger.getLogger(OFSLLLoginPage_Web.class);

	public OFSLLLoginPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	// Login XPath
	protected By fieldUsername = By.id("okta-signin-username");
	protected By fieldPassword = By.id("okta-signin-password");
	protected By loginButton = By.xpath("//*[@id='okta-signin-submit']");
	protected By ofslllink = By.xpath("(//img[contains(@alt,'Graphic Link Oracle OFSLL - PWT')])");
	protected By ofslllinkSaas = By.xpath("(//img[contains(@alt,'OFSLL UAT SaaS')])[1]");

	protected By signOut = By.xpath("//span[text()='Sign ']");
	protected By OktaLogOutMenu = By.xpath("(//span[@class='icon-dm'])[2]");
	protected By signOut_OKTA = By.xpath("//p[text()='Sign out']");
	protected By warning_msg = By.xpath("//*[contains(@class,'message::')]");
	protected By ok_button = By.xpath("//span[text()='OK']");
	protected By homeLogo = By.xpath("//img[contains(@src,'/logo/logo.gif')]");
	protected By oktaApplications = By.xpath("//p[contains(@class,'app-button-name')]");

	/*
	 * Method: loginToOKTA Description: Login to OKTA application Date: May/2020
	 * Author: Harsh Changes By: NA
	 */
	public void loginToOKTA() throws Exception {

		try {
			if (getList(oktaApplications).size() > 0) {
				log.info("Already Logged In");
			} else {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				setUserName();
				driver.switchTo().activeElement().sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				setPassword();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(loginButton);

				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			// log.info("Login to OKTA Successful");

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "User Logged in");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/*
	 * Method : launchApplication Parameters :None Description : launch
	 * application in browser Author : CTS Date : 2020 Change By : None
	 */

	public void launchApplication(){
		String url = testData.get("URL");
		driver.manage().deleteAllCookies();
		navigateToAppURL(url);

		try {
			driver.manage().window().maximize();
		} catch (Exception e) {
			log.info("Browser is Maximized");
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Application is Launched Successfully , URL : " + url);

	}

	/*
	 * Method: loginToOFSLL Description: Login to OFSLL application Date:
	 * May/2020 Author: Harsh Changes By: NA
	 */
	public void loginToOFSLL() throws Exception {

		try {
			if (tcConfig.getConfig().get("ENVIRONMENT").equalsIgnoreCase("PWT")) {
				waitUntilElementVisibleBy(driver, ofslllink, "ExplicitLongWait");
				if (verifyElementDisplayed(driver.findElement(ofslllink))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
							"Found OFSLL Chicklet on OKTA");
					clickElementBy(ofslllink);
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
							"Launched OFSLL PWT environment");

				}
			} else {
				waitUntilElementVisibleBy(driver, ofslllinkSaas, "ExplicitLongWait");
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(driver.findElement(ofslllinkSaas))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
							"Found OFSLL Chicklet on OKTA");
					clickElementBy(ofslllinkSaas);
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
							"Launched OFSLL SaaS environment");
				}
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String parentWinHandle = driver.getWindowHandle();
			Set<String> winHandles = driver.getWindowHandles();
			// Loop through all handles
			for (String handle : winHandles) {
				if (!handle.equals(parentWinHandle)) {
					driver.switchTo().window(handle);

				}
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(homeLogo))
				log.info("Login to OFSLL Successful");
			else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "OFSLL login failed");
			}

			if (verifyObjectDisplayed(warning_msg)) {

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(ok_button);

			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Clicked on OFSLL chiclet and User is successfully logged in");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	/*
	 * Method: launchApplication Description: launch the application Date:
	 * May/2020 Author: Harsh Changes By: NA
	 */

	public void launchApplication(String strBrowserInUse, String strBrowserName, String strModel,
			String strPlatformName, String strPlatformVR, String strBrowserVersion) {
		// this.driver = checkAndSetBrowser(strBrowserInUse, strBrowserName,
		// strModel, strPlatformName, strPlatformVR,
		// strBrowserVersion);
		// getBrowserName(strBrowserInUse); // String url =
		// testData.get("OKTA_URL");
		if (tcConfig.getConfig().get("ENVIRONMENT").equalsIgnoreCase("PWT")) {
			String url = testData.get("URL");
			navigateToURL(url);
		} else {
			String url = testData.get("URL");
			navigateToURL(url);
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: setUserName Description: Set Username with correct data Date:
	 * May/2020 Author: Harsh Changes By:
	 */
	public void setUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLowWait");

		String strUserName = testData.get("username");

		try {
			waitUntilElementVisibleBy(driver, fieldUsername, "ExplicitLowWait");
			driver.findElement(fieldUsername).sendKeys(strUserName);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Username is entered for Login");
		} catch (Exception e) {
			log.info("Couldn't Set username");
		}
	}

	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * May/2020 Author: Harsh Changes By: NA
	 */
	public void setPassword() throws Exception {
		// password data
		try {
			String password = testData.get("password");
			// decoded password
			String decodedPassword = decodePassword(password);
			fieldDataEnter(fieldPassword, decodedPassword);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Password is entered for OKTA login");
		} catch (Exception e) {
			log.info("Couldn't Set Password");
		}
	}

	/*
	 * Method: logOutOFSLL Description: Logging off from OFSLL May/2020 Author:
	 * Harsh Changes By: NA
	 */

	public void logOutOFSLL() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(signOut);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log Off Successful");

		} catch (Exception e) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Log Off Unsuccessfull" + e.getMessage());

		}
	}

	/*
	 * Method: logOutOKTA Description: Logging off from OKTA May/2020 Author:
	 * Harsh Changes By: NA
	 */

	public void logOutOKTA() {
		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(OktaLogOutMenu);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(signOut_OKTA);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log Off from Okta Successful");

		} catch (Exception e) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	/*
	 * Method: switchToTab Description: Switching to parent window May/2020
	 * Author: Harsh Changes By: NA
	 */

	public void switchToTab() {
		try {
			ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tab.get(1));
			driver.close();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().window(tab.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
