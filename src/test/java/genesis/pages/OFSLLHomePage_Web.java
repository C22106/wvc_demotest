package genesis.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import automation.core.TestConfig;

public class OFSLLHomePage_Web extends OFSLLBasePage {

	public static final Logger log = Logger.getLogger(OFSLLHomePage_Web.class);

	public OFSLLHomePage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By servicing = By.xpath("//span[text()='Servicing']");
	protected By cust_service = By.linkText("Customer Service");
	protected By payments = By.linkText("Payments");

	/*
	 * Method: clickToCustomerService Description: Click on Servicing->Customer
	 * Service page Date: May/2020 Author: Harsh Changes By:
	 */

	public void clickToCustomerService() throws Exception {

		try {
			waitUntilElementVisibleBy(driver, servicing, "ExplicitLongWait");
			if (verifyElementDisplayed(driver.findElement(servicing))) {
				clickElementBy(servicing);

			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(cust_service);

			/*
			 * tcConfig.updateTestReporter("OFSLLHomePage",
			 * "clickToCustomerService", Status.PASS,
			 * "Clicked on Custmoer service successfully");
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: clickToPayments Description: Click on Servicing->Payments Service
	 * page Date: August/2020 Author: Harsh Changes By:
	 */

	public void clickToPayments() throws Exception {

		try {
			waitUntilElementVisibleBy(driver, servicing, "ExplicitLongWait");
			if (verifyElementDisplayed(driver.findElement(servicing))) {
				clickElementBy(servicing);

			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(payments);

			/*
			 * tcConfig.updateTestReporter("OFSLLHomePage",
			 * "clickToCustomerService", Status.PASS,
			 * "Clicked on Custmoer service successfully");
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
