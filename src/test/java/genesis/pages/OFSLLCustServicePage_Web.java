package genesis.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OFSLLCustServicePage_Web extends OFSLLBasePage {

	public static final Logger log = Logger.getLogger(OFSLLCustServicePage_Web.class);
	public static String Adjust_min, Adjust_max, Total_amt_val, New_amount;;
	public String payoff_amt_bfr_txn, TXN_Statement, AccStats;

	public OFSLLCustServicePage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	OFSLLSummaryPage_Web OFSLLSummaryPage = new OFSLLSummaryPage_Web(tcConfig);

	// Search Account Number

	protected By acctNumber = By.xpath("//label[text()='Acc #']/../..//input[@class='x25']");
	protected By subButton = By.xpath("//span[text()='Sub']");
	protected By Acctstatus = By.xpath("//div[@aria-label='Account(s)']//div//table//tr[1]//td[14]//span");

	// Submit Bankruptcy

	protected By bankruptcyTab = By.xpath("//div[@class='x13t']/a[text()='Bankruptcy']");
	protected By actionDropdown = By.xpath("//label[text()='Action']/../../td[@class='xvo']/select[@class='x2h']");
	protected By resultDropdown = By.xpath("//label[text()='Result']/../../td[@class='xvo']/select[@class='x2h']");
	protected By mandFieldCheckAddCallActivity = By.xpath("//td[@class='x1dm']/div[text()='Error']");
	protected By ok_errorWindow = By
			.xpath("//td[@class='p_AFResizable x1dx']/div/a[@class='xfn']/span[@class='xfv' and text()='OK']");
	protected By postCall = By.xpath("//span[text()='Post Call ']");
	protected By summaryConditionCode = By.xpath(
			"(//div[@aria-label='Conditions']//div[2]//table//tr[@_afrrk='0'])[1]//td[@role='presentation']//td//span");

	// Submit Promise To Pay

	protected By multiplePromises = By.xpath("//span[text()='Create Multiple Promises']");
	protected By followUpDate = By.xpath("//label[text()='Followup Dt']/../../td[@class='xvo']/input[@class='x2b']");

	protected By promiseDate = By.xpath("//label[text()='Promise St Dt']/../../td[@class='xvo']/input[@class='x2b']");
	protected By frequency = By.xpath("//label[text()='Frequency']/../../td[@class='xvo xtk']/span/select");
	protected By noOfPromises = By.xpath("//label[text()='No. of Promises']/../../td[@class='xvo']/input");
	protected By createButton = By.xpath("//span[@class='xfv' and text()='Create']");
	protected By saveButton = By.xpath("//span[@class='xfv']/span[text()='S']");

	protected By cond = By.xpath("//label[text()='Condition']/../../td[@class='xvo']/select[@class='x2h']");
	protected By amtField = By.xpath("(//label[text()='Promise Amt']/../../td[@class='xvo']/input[@class='x25'])[1]");
	protected By pOverride = By.xpath("//div[text()='Override']");
	protected By promiseYes = By.xpath("//a[@class='xfn']/span[text()='Yes']");
	protected By promiseGrid = By
			.xpath("//table[@role='presentation']//tr[@role='row']/td[contains(@class,'p_AFFocused')]");
	protected By promiseTab = By.xpath("//div[@class='x13t']/a[@role='tab' and contains(.,'Promises')] ");
	protected By promiseDateTable = By.xpath("//div[@aria-label='Promises']//div[@class='x14p']//tr//table//td[4]");
	protected By promiseAmt = By.xpath("//div[@aria-label='Promises']//div[@class='x14p']//tr//table//td[1]");

	protected By error_msg = By.xpath("//div[@class='x1e5' and text()='Error']");

	// Submit Hardship

	protected By callActivitiesTab = By.xpath("//div[@class='x13t']/a[text()='Call Activities'])");
	protected By addButton = By.xpath("//a[@class='xfn']/span[text()='Add']");
	protected By custServiceTab = By
			.xpath("//div[@class='x13t']/a[contains(@class,'x13v') and text()='Customer Service']");
	protected By callDetailsList = By.xpath("//span[@class='x1z']//select");
	protected By apptCheckbox = By.xpath("//label[contains(text(),'Appn')]//parent::span//input");
	protected By saveReturn = By.xpath("//span[@class='xfy' and text()='S']");
	protected By errorMsg = By.xpath("//div[@class='x1e5' and text()='Error']");
	protected By callActivitiesTabDt = By.xpath(
			"//div[@aria-label='Call Activities']//div//table [@class='x14q x15f']//tr[@_afrrk='0']//td[2]//td[1]//span");
	protected By callActivitiesTabActionCode = By.xpath(
			"//div[@aria-label='Call Activities']//div//table [@class='x14q x15f']//tr[@_afrrk='0']//td[2]//td[2]//span");
	protected By callActivitiesTabResultCode = By.xpath(
			"//div[@aria-label='Call Activities']//div//table [@class='x14q x15f']//tr[@_afrrk='0']//td[2]//td[4]//span");
	protected By callActivitiesTabAppt = By.xpath(
			"//div[@aria-label='Call Activities']//div//table [@class='x14q x15f']//tr[@_afrrk='0']//td[2]//td[12]//span");

	// Validate Transaction Payments
	protected By trans_date = By.xpath("(//div[@aria-label='Transactions']//table[1]//table[1])[1]//tr//td[2]//span");
	protected By Payment_amt = By.xpath("(//div[@aria-label='Transactions']//table[1]//table[1])[1]//tr//td[5]//span");
	protected By reference = By.xpath("(//div[@aria-label='Transactions']//table[1]//table[1])[1]//tr//td[11]//span");

	// Add adjustment
	protected By cust_service_tab = By.xpath("//div[@class='x13t']//a[text()='Customer Service' and @class='x13v']");
	protected By maintanance_tab = By.xpath("//div[@class='x13t']//a[text()='Maintenance' and @class='x13v']");
	protected By Add_Tractions_btn = By.xpath("//a[@class='xfn']//span[text()='Add']");
	protected By Transactions_field = By
			.xpath("//label[text()='Transaction']//parent::td//following-sibling::td//span//input");
	protected By LoadParameter_btn = By.xpath("//div[@class='xfl p_AFLeading']//a[@role='button' and @accesskey='L' ]");
	protected By TXN_dt_field = By.xpath("(//span[@class='af_column_data-container']//span[@class='x1q']//input)[1]");
	protected By TXN_Amt_field = By.xpath("//span[@class='af_column_data-container']//span[@class='x1u']//input");
	protected By Post_btn = By.xpath("//div[@class='xfl p_AFLeading']//a[@class='xfn' and @accesskey='P']");
	protected By Result_screen = By
			.xpath("//div[@aria-label='Result']//div//table [@class='x14q']//tr[1]//td[2]//td[1]//span");
	protected By Result_screen_Open1 = By
			.xpath("//div[@aria-label='Result']//div//table [@class='x14q']//tr[@_afrrk='2']//td[2]//td[1]//span");
	protected By Result_screen_Open2 = By
			.xpath("//div[@aria-label='Result']//div//table [@class='x14q']//tr[@_afrrk='3']//td[2]//td[1]//span");
	protected By Return_btn = By.xpath("//div[@class='xfl p_AFLeading']//a[@accesskey='R']");
	protected By get_rowcount_TXNtable = By
			.xpath("//div[@aria-label='Transaction Batch Information']//div//table[@class='x14q x15f']");
	protected By selectMonth = By.xpath("//span[@class='xor' ]//select [contains(@name,'mSel')]");
	protected By selectDate = By.xpath("(//td[text()='1'])[1]");
	protected By Transaction_History_tab = By
			.xpath("//div[@class='x13t']//a[text()='Transaction History' and @class='x13v']");
	protected By Balance_grp = By.xpath("//h1[text()='Balance Group']");
	protected By transactions = By.xpath("//div[@class='x13t']//a[text()='Transactions' and @class='x13v']");
	protected By Adjust_sub = By.xpath("//span[text()='INTEREST']//parent::td//following-sibling::td[6]//span");
	protected By Adjust_add = By.xpath("//span[text()='INTEREST']//parent::td//following-sibling::td[7]//span");
	protected By saveandReturn = By.xpath("//span[@class='xfy' and text()='S']");
	protected By Total_amt = By
			.xpath("//label[text()='Current Balance Total']//parent::td//following-sibling::td//span");
	protected By Bal_table = By.xpath("//div[@aria-label='Balance Group']");
	protected By Detach_close_btn = By.xpath("(//a[contains(@id,'_afrDetachDialogId::close')])");
	protected By Detach_btn = By.xpath("(//a[@class='xfm']//span[text()='Detach'])[2]");
	protected By calander_btn = By.xpath("//span[@class='x1q']//a[@title='Select Date']");
	protected By Edit_btn_intrst = By.xpath("//a[@class='xfn']//span[text()='Edit']");
	protected By post_btn_list = By.xpath("//a[@class='xfn']//span[text()='P']");
	protected By get_status_latest = By
			.xpath("(//div[@aria-label='Transaction Batch Information']//div//table//tr[1]//td[4]//span)[1]");
	protected By get_date_latest = By
			.xpath("(//div[@aria-label='Transaction Batch Information']//div//table//tr[1]//td[2]//span)[1]");
	protected By get_monetary_latest = By
			.xpath("(//div[@aria-label='Transaction Batch Information']//div//table//tr[1]//td[2]//span)[2]");
	protected By get_transaction_latest = By
			.xpath("(//div[@aria-label='Transaction Batch Information']//div//table//tr[1]//td[3]//span)[1]");
	protected By void_btn_intrst = By.xpath("//div[@class='xfl p_AFLeading']//a[@class='xfn']//span[text()='V']");
	protected By payoff_amt = By.xpath("//div[@aria-label='Account(s)']//div//table//tr[1]//td[12]//span");
	protected By ok_btn = By.xpath("(//a[@class='xfn']//span[text()='OK'])[1]");

	// Principle amount
	protected By Adjust_sub_Principal = By
			.xpath("//span[text()='ADVANCE / PRINCIPAL']//parent::td//following-sibling::td[6]//span");
	protected By Adjust_add_Principal = By
			.xpath("//span[text()='ADVANCE / PRINCIPAL']//parent::td//following-sibling::td[7]//span");

	// Paid Account validation

	protected By Error_msg_paid = By.xpath("//td[@class='af|message::detail-cell']//div");

	// SCRA
	protected By TXN_Rate = By.xpath("(//span[@class='af_column_data-container']//span[@class='x1u']//input)[2]");
	protected By Override_dropdwn_list = By
			.xpath("(//span[@class='af_column_data-container']//span[@class='x1z']//select)");
	protected By Summary_tab = By.xpath("//div[@class='x13t']//a[text()='Summary' and @class='x13v']");
	protected By Alert_tab = By.xpath("//h2[text()='Alerts']");
	protected By condTable = By.xpath("(//div[@aria-label='Conditions']//div[2]//table[@class='x14q x15f'])[1]");
	protected By condCodeList = By
			.xpath("(//div[@aria-label='Conditions']//div[2]//table//tr//td[@role='presentation']//td[1]//span)");
	protected By Activeduty_indicator = By.xpath(
			"//td[@class='xu x4z']//label[text()='Active Military Duty']//parent::td//following-sibling::td//span");
	protected By Militaryduty_indicator = By
			.xpath("//td[@class='x15 x4z']//label[text()='Military Duty']//parent::td//following-sibling::td");
	protected By Account_Rel = By.xpath("(//span[@class='x1z']//select[@class='x2h'])[1]");
	protected By Calc_Method = By.xpath("(//span[@class='x1z']//select[@class='x2h'])[2]");
	protected By callActivityRefreshButton = By.xpath("//div[@class='xg5']//img[@alt='Click to refresh data']");
	protected By condListCallAct = By.xpath(
			"//div[@aria-label='Call Activities']//div//table [@class='x14q x15f']//tr[@_afrrk='0']//td[2]//td[8]//span//select//option");
	protected By condCallAct = By.xpath(
			"//div[@aria-label='Call Activities']//div//table [@class='x14q x15f']//tr[@_afrrk='0']//td[2]//td[8]//span//select");

	/*
	 * Method: searchAcctNo Description: Search Acct No on Customer Service
	 * Screen Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void searchAcctNo() throws Exception {

		String acct_no = testData.get("acctno");
		try {
			waitUntilElementVisibleBy(driver, acctNumber, "ExplicitLowWait");
			sendKeysBy(acctNumber, acct_no);
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Account number is entered on Customer Service Search Screen");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitUntilElementVisibleBy(driver, subButton, "ExplicitLowWait");
			clickElementBy(subButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			getElementInView(Acctstatus);
			AccStats = getElementText(Acctstatus);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Clicked on search button on Customer Service Screen");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: searchAcctNo Description: Search Acct No on Customer Service
	 * Screen Date: September/2020 Author: Harsh Changes By: NA
	 */
	public void searchAcctNo(String Account_num) throws Exception {

		try {
			waitUntilElementVisibleBy(driver, acctNumber, "ExplicitLowWait");
			sendKeysBy(acctNumber, Account_num);
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Account number is entered on Customer Service Search Screen");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitUntilElementVisibleBy(driver, subButton, "ExplicitLowWait");
			clickElementBy(subButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			getElementInView(Acctstatus);
			AccStats = getElementText(Acctstatus);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Clicked on search button on Customer Service Screen");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: submitPromise Description:Save Promise through Customer Service
	 * ->PromisesScreen Date: June/2020 Author: Harsh Changes By: NA
	 */

	public void submitPromise() throws Exception {

		try {
			addmultiplePromise();
			mandatoryFieldCheckPromise();
			postandValidatePromise();

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Promise to Pay added successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: submitBankruptcy Description: Routing the account for bankruptcy
	 * review Screen Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void submitBankruptcy() throws Exception {

		try {

			selectConditionCode();
			mandatoryFieldCheck();
			postAndValidateBankruptcy();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: submitHardship Description: Routing the account for Potential
	 * Hardship Screen Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void submitHardship() throws Exception {

		try {

			navigateToCustomerService();
			addConditionCode();
			saveAndValidateHardship();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: submitResortCancel Description: Add "Resort Cancellation"
	 * condition code on an account Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void submitResortCancel() throws Exception {

		try {

			selectConditionCode();
			mandatoryFieldCheck();
			postAndValidateCancellation();

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Condition code added and validated successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: navigateToCustomerService Description: Navigating to Customer
	 * Service tab" Screen Date: May/2020 Author: Harsh Changes By: NA
	 */

	public void navigateToCustomerService() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(custServiceTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Navigate to Customer Service->Call Activities tab");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: navigateToPromisesTab Description: Navigating to Customer
	 * Service->Promises tab" Screen Date: June/2020 Author: Harsh Changes By:
	 * NA
	 */

	public void navigateToPromisesTab() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(custServiceTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(promiseTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Navigate to Promises tab successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: addmultiplePromise Description: Select Action and Result drop
	 * down fields" Screen Date: June/2020 Author: Harsh Changes By: NA
	 */
	public void addmultiplePromise() throws Exception {

		String actionValue = testData.get("action_code");
		String resultValue = testData.get("result_code");

		try {

			navigateToPromisesTab();
			clickElementBy(multiplePromises);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(actionDropdown);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(actionDropdown, actionValue);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(resultDropdown, resultValue);

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: mandatoryFieldCheckPromise Description: Mandatory field
	 * Validation checks Screen Date: June/2020 Author: Harsh Changes By: NA
	 */
	public void mandatoryFieldCheckPromise() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(createButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(errorMsg)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(ok_errorWindow);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Mandatory Field Check validated successfully");
			}

			else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Mandatory Field Check validation failed");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().refresh();
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: postandValidatePromise Description: Post and Validate Promise
	 * Screen Date: June/2020 Author: Harsh Changes By: NA
	 */
	public void postandValidatePromise() throws Exception {

		String pDate = addDateInSpecificFormat("MM/dd/yyyy", 4);
		String pamt = testData.get("promise_amt");
		String freq = testData.get("frequency");
		String countPromise = testData.get("total_promise");

		try {

			getElementInView(promiseDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(promiseDate, pDate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(frequency, freq);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysBy(noOfPromises, countPromise);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(amtField, pamt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(noOfPromises).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(createButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveButton);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(error_msg)) {
				waitUntilElementVisibleBy(driver, ok_errorWindow, "ExplicitLongWait");
				clickElementBy(ok_errorWindow);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Total no of promises cannot exceed more than 5");
			} else if (!(verifyObjectDisplayed(pOverride))) {

				validatePromises();

			}
			if (verifyObjectDisplayed(pOverride)) {

				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(promiseYes);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(error_msg)) {
					waitUntilElementVisibleBy(driver, ok_errorWindow, "ExplicitLongWait");
					// clickElementBy(ok_errorWindow);

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Total no of promises cannot exceed more than 5");
				} else {

					validatePromises();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validatePromises Description: Validating Promises in the table"
	 * Screen Date: June/2020 Author: Harsh Changes By: NA
	 */

	public void validatePromises() throws Exception {

		String todayDate = getCurrentDateInSpecificFormat("MM/dd/yyyy");
		String pamt = testData.get("promise_amt");
		String countPromise = testData.get("total_promise");

		try {

			getElementInView(promiseDateTable);
			List<WebElement> elems_Date = getList(promiseDateTable);
			List<WebElement> elems_Amt = getList(promiseAmt);

			int total_date = 0;
			boolean flag_amt = false;

			for (int i = 0; i < elems_Date.size(); i++) {

				if (todayDate.equals(getElementText(elems_Date.get(i)))) {

					total_date++;
					if (Double.parseDouble(pamt) == Double.parseDouble(getElementText(elems_Amt.get(i)))) {
						flag_amt = true;
					}

				}

			}
			// log.info("total promise"+total_date);
			if (total_date >= Integer.parseInt(countPromise) && flag_amt) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Promise added successfully " + countPromise + " number of times with promise amount as "
								+ pamt);
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Promise validation failed");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: addConditionCode Description: Selecting condition code in
	 * "Add Call Activity tab" Screen Date: May/2020 Author: Harsh Changes By:
	 * NA
	 */
	public void addConditionCode() throws Exception {

		String actionValue = testData.get("action_code");
		String resultValue = testData.get("result_code");

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(addButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> callList = driver.findElements(callDetailsList);
			selectByText(callList.get(0), actionValue);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(saveReturn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			mandatoryFieldCheck_CallActivities();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> callListRefresh = driver.findElements(callDetailsList);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// selectByValue(callListRefresh.get(1), "2");
			selectByText(callListRefresh.get(1), resultValue);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(callListRefresh.get(2), "BROTHER");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(callListRefresh.get(4));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectByValue(callListRefresh.get(4), "0");
			getElementInView(callListRefresh.get(5));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByValue(callListRefresh.get(5), "1");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			getElementInView(apptCheckbox);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(apptCheckbox);

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: selectConditionCode Description: Selecting condition code in
	 * "Add Call Activity tab" Screen Date: May/2020 Author: Harsh Changes By:
	 * NA
	 */
	public void selectConditionCode() throws Exception {

		String actionValue = testData.get("action_code");
		String resultValue = testData.get("result_code");

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(actionDropdown);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(actionDropdown, actionValue);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(resultDropdown, resultValue);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Action code and Result code added succesfully");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
					"Could not select Action and result codes");

			e.printStackTrace();
		}
	}

	/*
	 * Method: mandatoryFieldCheck Description: Mandatory field Validation
	 * checks Screen Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void mandatoryFieldCheck() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCall);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(mandFieldCheckAddCallActivity)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(ok_errorWindow);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Mandatory Field Check validated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Mandatory Field Check validation failed");
			}

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: mandatoryFieldCheck_CallActivities Description: Mandatory field
	 * Validation check in CustomerService-> Call Activities tab Screen Date:
	 * May/2020 Author: Harsh Changes By: NA
	 */

	public void mandatoryFieldCheck_CallActivities() throws Exception {

		try {

			if (verifyObjectDisplayed(errorMsg)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(ok_errorWindow);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Mandatory Field Check validated successfully");
			}

			else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Mandatory Field Check validation failed");
			}

			// sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.navigate().refresh();
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: postAndValidateBankruptcy Description: Posting and Validating
	 * Bankruptcy" Screen Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void postAndValidateBankruptcy() throws Exception {

		String expActionCode = testData.get("action_code");
		String expResultCode = testData.get("result_code");

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(cond, "NONE");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCall);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			navigateToCustomerService();

			String expDate = getCurrentDateInSpecificFormat("MM/dd/YYYY");
			String actualDate = getElementText(callActivitiesTabDt);

			String actualActionCode = getElementText(callActivitiesTabActionCode);
			String actualResultCode = getElementText(callActivitiesTabResultCode);

			if (expDate.equals(actualDate)) {
				if (expActionCode.contains(actualActionCode)) {
					if (expResultCode.contains(actualResultCode)) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								"Action code, Result code, Date added and validated successfully in Call Activities");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								"Action code, Result code, Date validation failed in Call Activities");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: postAndValidateCancellation Description: Posting and Validating
	 * Resort Cancellation" Screen Date: June/2020 Author: Harsh Changes By: NA
	 */
	public void postAndValidateCancellation() throws Exception {

		String expActionCode = testData.get("action_code");
		String expResultCode = testData.get("result_code");

		try {

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(cond, "NONE");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCall);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			navigateToCustomerService();

			String expDate = getCurrentDateInSpecificFormat("MM/dd/YYYY");
			String actualDate = getElementText(callActivitiesTabDt);

			String actualActionCode = getElementText(callActivitiesTabActionCode);
			String actualResultCode = getElementText(callActivitiesTabResultCode);

			if (expDate.equals(actualDate)) {
				if (expActionCode.contains(actualActionCode)) {
					if (expResultCode.contains(actualResultCode)) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								"Resort Cancellation added and validated successfully");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								"Resort Cancellation Validation failed");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: saveAndValidateHardship Description: Posting and Validating
	 * Hardship" Screen Date: May/2020 Author: Harsh Changes By: NA
	 */
	public void saveAndValidateHardship() throws Exception {

		String actionValue = testData.get("action_code");

		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(saveReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String expDate = getCurrentDateInSpecificFormat("MM/dd/yyyy");
			String actualDate = getElementText(callActivitiesTabDt);

			String actualCode = getElementText(callActivitiesTabActionCode);
			getElementInView(callActivitiesTabAppt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String actualAppt = getElementText(callActivitiesTabAppt);

			log.info(actualCode);
			log.info(actualAppt);

			if (expDate.equals(actualDate)) {
				if (actionValue.contains(actualCode)) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Potential Hardship added and validated successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Potential Hardship Validation failed");
				}
			}

			if (actualAppt.equals("Y")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Appointment agreed and validated");
			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Appointment validation failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: get interest amount Description: Get interest amount from
	 * customer Screen Date: May/2020 Author: Ankush Changes By: NA
	 */

	public void getinterestamount_balancetab() throws Exception {

		try {
			navigate_to_Balance_tab();
			get_interest_amount();

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Interest checked successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: get Tranaction amount Description: Get amount from customer
	 * Screen Date: June/2020 Author: Ankush Changes By: NA
	 */

	public void getamount_balancetab() throws Exception {

		try {

			navigate_to_Balance_tab();

			get_amount();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			OFSLLSummaryPage.navigateToSummary();
			OFSLLSummaryPage.get_amount_summary();

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Amount checked successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: get Principal amount Description: Get principle amount from
	 * customer Screen Date: June/2020 Author: Ankush Changes By: NA
	 */

	public void getprincipalamount_balancetab() throws Exception {

		try {
			navigate_to_Balance_tab();
			get_Principal_amount();

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Principle amount checked successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Validate interest amount Description: Validates interest amount
	 * from customer Screen Date: May/2020 Author: Ankush Changes By: NA
	 */

	public void validateinterestamount_balancetab(String flag) throws Exception {

		try {

			navigate_to_Balance_tab();
			val_interest_amount(flag);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Interest validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateTransctionamount_balancetab Description: Validates amount
	 * from customer Screen Date: June/2020 Author: Ankush Changes By: NA
	 */

	public void validateTransctionamount_balancetab(String flag) throws Exception {

		try {

			navigate_to_Balance_tab();
			val_Transaction_amount(flag);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Transaction validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateTransctionamount_balancetab Description: Validates amount
	 * from customer Screen Date: August/2020 Author: Ankush Changes By: NA
	 */

	public void validate_Transctions_Payments(String flag, String Date, String Amount) throws Exception {

		try {

			navigate_to_transaction_tab();
			val_Transaction_pay(flag, Date, Amount);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Transaction validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validateTransctionamount_balancetab Description: Validates amount
	 * from customer Screen Date: August/2020 Author: Ankush Changes By: NA
	 */

	public void validate_Transctions_Payments(String flag, String Date) throws Exception {

		try {

			navigate_to_transaction_tab();
			val_Transaction_pay(flag, Date);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Transaction validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Validate Principal amount Description: Validates Principal amount
	 * from customer Screen Date: June/2020 Author: Ankush Changes By: NA
	 */

	public void validatePrincipalamount_balancetab(String flag) throws Exception {

		try {

			navigate_to_Balance_tab();
			val_principal_amount(flag);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Principal amount validated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: addInterest Description: Verify that the
	 * transaction(ADD/Subtract) is getting updated to the account Date:
	 * May/2020 Author: Ankush Changes By: NA
	 */
	public void addInterest(String flag) throws Exception {

		try {
			navigate_to_maintanance_tab();
			add_adjustment_transaction_madatory_Check();
			add_interest_transaction();
			validate_transaction_completed(flag);
			String status = getElementText(get_status_latest);
			if (status.equals("OPEN")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction is in OPEN status, no need to verify amount");
			} else {
				validateinterestamount_balancetab(flag);
			}
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS, "Interest Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: UpdateTranscation Description: Verify that the
	 * transaction(ADD/Subtract) is getting updated to the account Date:
	 * June/2020 Author: Ankush Changes By: NA
	 */
	public void UpdateTranscation(String flag) throws Exception {

		try {
			navigate_to_maintanance_tab();
			add_transaction();
			validate_transaction_completed(flag);
			String status = getElementText(get_status_latest);
			if (status.equals("OPEN")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction is in OPEN status, no need to verify amount");
			} else if (status.equals("POSTED")) {
				validateTransctionamount_balancetab(flag);
				OFSLLSummaryPage.validate_Transction_summary(flag);
			}
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Transaction " + testData.get("FieldName") + " Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: updatePrincipal Description: Verify that the
	 * transaction(ADD/Subtract principal) is getting updated to the account
	 * Date: June/2020 Author: Ankush Changes By: NA
	 */
	public void updatePrincipal(String flag) throws Exception {

		try {
			navigate_to_maintanance_tab();
			add_adjustment_transaction_madatory_Check();
			add_Principal_transaction();
			validate_transaction_completed(flag);
			String status = getElementText(get_status_latest);
			if (status.equals("OPEN")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction is in OPEN status, no need to verify amount");
			} else {
				validatePrincipalamount_balancetab(flag);
			}
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Transaction Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: navigation to maintanance tab Description: Verify navigation to
	 * maintanance Date: 11/May/2020 Author: Ankush Changes By: NA
	 */
	public void navigate_to_maintanance_tab() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(cust_service_tab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(maintanance_tab);

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Maintanance tab navigated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: navigation to BALANCES tab Description: Verify navigation to
	 * BALANCES Date: May/2020 Author: Ankush Changes By: NA
	 */
	public void navigate_to_Balance_tab() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Transaction_History_tab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(maintanance_tab);
			if (verifyObjectDisplayed(Balance_grp)) {

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Balances tab navigated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"Balances tab NOT navigated successfully");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: navigation to Transactions tab Description: Verify navigation to
	 * Transactions Date: August/2020 Author: Ankush Changes By: NA
	 */
	public void navigate_to_transaction_tab() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Transaction_History_tab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// clickElementBy(maintanance_tab);
			if (verifyObjectDisplayed(transactions)) {
				clickElementBy(transactions);
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"transactions tab navigated successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"transactions tab NOT navigated successfully");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Get interest amount Description: Verify interest amount form
	 * balance tab Date: May/2020 Author: Ankush Changes By: NA
	 */
	public void get_interest_amount() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(Detach_btn);

			/*
			 * ((JavascriptExecutor) driver).executeScript(
			 * "arguments[0].scrollTop=arguments[1].offsetTop",
			 * driver.findElement(Bal_table), driver.findElement(By.xpath(
			 * "//span[text()='//span[text()='ADVANCE / PRINCIPAL']']")));
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * getElementInView(By.xpath(
			 * "//span[text()='//span[text()='ADVANCE / PRINCIPAL']']"));
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Adjust_min = getElementText(Adjust_sub);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Adjust_max = getElementText(Adjust_add);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Total_amt_val = getElementText(Total_amt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * log.info(Adjust_min); log.info(Adjust_max);
			 * log.info(Total_amt_val);
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> cls_btn = driver.findElements(Detach_close_btn);
			clickElementBy(cls_btn.get(cls_btn.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Interest Amount is fetched from Transaction history ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Get amount Description: Verify amount form balance tab Date:
	 * June/2020 Author: Ankush Changes By: NA
	 */
	public void get_amount() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(Detach_btn);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Adjust_min = getElementText(By.xpath(
					"//span[text()='" + testData.get("FieldName") + "']//parent::td//following-sibling::td[6]//span"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Adjust_max = getElementText(By.xpath(
					"//span[text()='" + testData.get("FieldName") + "']//parent::td//following-sibling::td[7]//span"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Total_amt_val = getElementText(Total_amt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info(Adjust_min);
			log.info(Adjust_max);
			log.info(Total_amt_val);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> cls_btn = driver.findElements(Detach_close_btn);
			clickElementBy(cls_btn.get(cls_btn.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" Amount is fetched from Transaction history ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Get Principle amount Description: Verify Principle amount form
	 * balance tab Date: June/2020 Author: Ankush Changes By: NA
	 */
	public void get_Principal_amount() throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(Detach_btn);

			/*
			 * ((JavascriptExecutor) driver).executeScript(
			 * "arguments[0].scrollTop=arguments[1].offsetTop",
			 * driver.findElement(Bal_table), driver.findElement(By.xpath(
			 * "//span[text()='//span[text()='ADVANCE / PRINCIPAL']']")));
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * getElementInView(By.xpath(
			 * "//span[text()='//span[text()='ADVANCE / PRINCIPAL']']"));
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			Adjust_min = getElementText(Adjust_sub_Principal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Adjust_max = getElementText(Adjust_add_Principal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Total_amt_val = getElementText(Total_amt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info(Adjust_min);
			log.info(Adjust_max);
			log.info(Total_amt_val);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> cls_btn = driver.findElements(Detach_close_btn);
			clickElementBy(cls_btn.get(cls_btn.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Principle Amount is fetched from Transaction history ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: validate transaction completed Description: Verify transaction
	 * completed successfully tab Date: May/2020 Author: Ankush Changes By:
	 * Harsh/Ankush: Monetary check validation and pay off amount check
	 * validation
	 */
	public void validate_transaction_completed(String flag) throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			String OFSLLDate = getElementText(get_date_latest);
			// log.info(OFSLLDate);
			String Actualdate = getCurrentDateInSpecificFormat("MM/dd/yyyy");
			// log.info(Actualdate);
			if (Actualdate.equals(OFSLLDate)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction date validated successfully ");
			}
			String Monetary = getElementText(get_monetary_latest);
			log.info(Monetary);
			if (Monetary.equals("Y")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Monetary check validated successfully ");
			}
			String transaction = getElementText(get_transaction_latest);
			// log.info(transaction);
			if (transaction.equals(testData.get("Transction"))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction check validated successfully ");
			}
			String status = getElementText(get_status_latest);
			log.info(status);
			if (status.equals("OPEN")) {

				log.info(TXN_Statement);
				String[] Datelist = TXN_Statement.split(" ");
				String Processdate = Datelist[Datelist.length - 1];
				log.info("Process date is" + Processdate);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				getElementInView(Edit_btn_intrst);
				clickElementBy(Edit_btn_intrst);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String year_Current = getCurrentDateInSpecificFormat("yyyy");
				if (Processdate.endsWith(year_Current)) {

					clearElementBy(TXN_dt_field);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (verifyObjectDisplayed(TXN_Amt_field))
						driver.findElement(TXN_Amt_field).click();

					if (verifyObjectDisplayed(Calc_Method))
						driver.findElement(Calc_Method).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					sendKeysBy(TXN_dt_field, Processdate);
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				List<WebElement> post = driver.findElements(post_btn_list);
				post.get(post.size() - 1).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				// if(payoff_amt_bfr_txn.equals("0.00"))
				if (AccStats.equalsIgnoreCase("PAID OFF")) {
					if (verifyObjectDisplayed(Error_msg_paid)) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								"Error Message for Paid account exist with text :" + getElementText(Error_msg_paid));
						clickElementBy(ok_btn);
						clickElementBy(Return_btn);
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
								"Error Message for Paid account does not exist ");
					}
				} else {

					String new_status = getElementText(get_status_latest);
					if (new_status.equals("POSTED")) {
						getElementInView(payoff_amt);
						String new_payoff_amt = getElementText(payoff_amt);
						// log.info("New payoff amount is:
						// "+new_payoff_amt);
						String transaction_amount = testData.get("TransactionAmount");
						if (flag.equals("ADD"))

						{

							// if(Double.valueOf(new_payoff_amt)==(Double.valueOf(payoff_amt_bfr_txn)+Double.valueOf(transaction_amount))){
							if (Double.parseDouble(new_payoff_amt.replaceAll(",",
									"")) == (Double.parseDouble(payoff_amt_bfr_txn.replaceAll(",", ""))
											+ Double.valueOf(transaction_amount))) {
								tcConfig.updateTestReporter("CustomerService", "ValidateTransactionComplete",
										Status.PASS, "Payoff amount is validated successfully ");
							} else {
								tcConfig.updateTestReporter("CustomerService", "ValidateTransactionComplete",
										Status.FAIL, "Payoff amount validatation failed ");
							}
						} else if (flag.equals("SUB")) {
							if (Double.parseDouble(new_payoff_amt.replaceAll(",",
									"")) == (Double.parseDouble(payoff_amt_bfr_txn.replaceAll(",", ""))
											- Double.valueOf(transaction_amount))) {
								tcConfig.updateTestReporter("CustomerService", "ValidateTransactionComplete",
										Status.PASS, "Payoff amount is validated successfully ");
							} else {
								tcConfig.updateTestReporter("CustomerService", "ValidateTransactionComplete",
										Status.FAIL, "Payoff amount validatation failed ");
							}
						}
					} else {
						if (verifyObjectDisplayed(ok_btn)) {
							clickElementBy(ok_btn);
						}
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						getElementInView(Result_screen);
						TXN_Statement = getElementText(Result_screen);

						String[] New_Pay_amount = TXN_Statement.split("=");
						New_amount = New_Pay_amount[1].replace(" $", "");
						log.info(New_amount);
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Transaction is added with statement -" + TXN_Statement);

					}

				}
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"transaction completion validated ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Val interest amount Description: Validate interest amount form
	 * balance tab Date: May/2020 Author: Ankush Changes By: NA
	 */
	public void val_interest_amount(String flag) throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(Detach_btn);

			/*
			 * ((JavascriptExecutor) driver).executeScript(
			 * "arguments[0].scrollTop=arguments[1].offsetTop",
			 * driver.findElement(Bal_table), driver.findElement(By.xpath(
			 * "//span[text()='//span[text()='ADVANCE / PRINCIPAL']']")));
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * getElementInView(By.xpath(
			 * "//span[text()='//span[text()='ADVANCE / PRINCIPAL']']"));
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Adjust_min1 = getElementText(Adjust_sub);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Adjust_max1 = getElementText(Adjust_add);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Total_amount1 = getElementText(Total_amt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info(Adjust_min1);
			log.info(Adjust_max1);
			log.info(Total_amount1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> cls_btn = driver.findElements(Detach_close_btn);
			clickElementBy(cls_btn.get(cls_btn.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Interest Amount is fetched from Transaction history ");
			if (flag.equalsIgnoreCase("ADD")) {
				if (Double.valueOf(Adjust_max1) == Double.valueOf(Adjust_max)
						+ Double.valueOf(testData.get("TransactionAmount"))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"ADD Interest Amount is validated successfully after tranaction  " + "\n"
									+ "The total amount after transaction is :" + Total_amount1
									+ " and Before transction was :" + Total_amt_val);
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"ADD Interest Amount is validatation is failed after tranaction  ");
				}
			} else {
				if (Double.valueOf(Adjust_min1) == Double.valueOf(Adjust_min)
						+ Double.valueOf(testData.get("TransactionAmount"))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Subtract Interest Amount is validated successfully after tranaction" + "\n"
									+ "The subtrsct amount is : " + testData.get("TransactionAmount") + "\n"
									+ "The total amount after transaction is :" + Total_amount1
									+ " and Before transction was :" + Total_amt_val);
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Subtract Interest Amount is validatation is failed after tranaction  ");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Val Transaction amount Description: Validate interest amount form
	 * balance tab Date: June/2020 Author: Ankush Changes By: NA
	 */
	public void val_Transaction_amount(String flag) throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(Detach_btn);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Adjust_min1 = getElementText(By.xpath(
					"//span[text()='" + testData.get("FieldName") + "']//parent::td//following-sibling::td[6]//span"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Adjust_max1 = getElementText(By.xpath(
					"//span[text()='" + testData.get("FieldName") + "']//parent::td//following-sibling::td[7]//span"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Total_amount1 = getElementText(Total_amt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info(Adjust_min1);
			log.info(Adjust_max1);
			log.info(Total_amount1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> cls_btn = driver.findElements(Detach_close_btn);
			clickElementBy(cls_btn.get(cls_btn.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" Amount is fetched from Transaction history ");
			if (flag.equalsIgnoreCase("ADD")) {
				if (Double.valueOf(Adjust_max1) == Double.valueOf(Adjust_max)
						+ Double.valueOf(testData.get("TransactionAmount"))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"ADD  Amount is validated successfully after tranaction  " + "\n"
									+ "The total amount after transaction is :" + Total_amount1
									+ " and Before transction was :" + Total_amt_val);
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"ADD  Amount is validatation is failed after tranaction  ");
				}
			} else {
				if (Double.valueOf(Adjust_min1) == Double.valueOf(Adjust_min)
						+ Double.valueOf(testData.get("TransactionAmount"))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Subtract  Amount is validated successfully after tranaction" + "\n"
									+ "The subtrsct amount is : " + testData.get("TransactionAmount") + "\n"
									+ "The total amount after transaction is :" + Total_amount1
									+ " and Before transction was :" + Total_amt_val);
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Subtract  Amount is validatation is failed after tranaction  ");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: val_Transaction_pay Description: Validate interest amount form
	 * balance tab Date: Aug/2020 Author: Ankush Changes By: Harsh/Ankush:
	 * Related to Reference No.
	 */
	public void val_Transaction_pay(String flag, String Date, String Amount) throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getElementInView(reference);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Reference = "(//div[@aria-label='Transactions']//table[1]//table[1])//tr//td[11]//span[text()='"
					+ flag + "']";
			if (verifyObjectDisplayed(By.xpath(Reference))) {
				String Trans_Date = Reference + "//parent::td//parent::tr//td[2]//span";
				getElementInView(trans_date);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				log.info(getElementText(By.xpath(Trans_Date)));
				if (getElementText(By.xpath(Trans_Date)).equals(Date)) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Transaction date is validated successfully ");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Transaction date is not correct");
				}
				String Payment_amount = Reference + "//parent::td//parent::tr//td[5]//span";
				getElementInView(Payment_amt);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				log.info(getElementText(By.xpath(Payment_amount)));
				if (getElementText(By.xpath(Payment_amount)).contains(Amount)) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Transaction Amount is validated successfully ");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Transaction Amount is not correct");
				}
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"PayOff amount is not posted successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: val_Transaction_pay Description: Validate interest amount form
	 * balance tab Date: Aug/2020 Author: Ankush Changes By: Harsh/Ankush:
	 * Related to Reference No.
	 */
	public void val_Transaction_pay(String flag, String Date) throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			getElementInView(reference);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Reference = "(//div[@aria-label='Transactions']//table[1]//table[1])//tr//td[11]//span[text()='"
					+ flag + "']";
			if (verifyObjectDisplayed(By.xpath(Reference))) {
				String Trans_Date = Reference + "//parent::td//parent::tr//td[2]//span";
				getElementInView(trans_date);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				log.info(getElementText(By.xpath(Trans_Date)));
				if (getElementText(By.xpath(Trans_Date)).equals(Date)) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Transaction date is validated successfully ");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Transaction date is not correct");
				}
				String Payment_amount = Reference + "//parent::td//parent::tr//td[5]//span";
				getElementInView(Payment_amt);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				log.info(getElementText(By.xpath(Payment_amount)));
				if (getElementText(By.xpath(Payment_amount)).contains(testData.get("TransactionAmount"))) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Transaction Amount is validated successfully ");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Transaction Amount is not correct");
				}
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"PayOff amount is not posted successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Val principal amount Description: Validate principal amount form
	 * balance tab Date: June/2020 Author: Ankush Changes By: Harsh/Ankush:
	 * Proper variable names
	 */
	public void val_principal_amount(String flag) throws Exception {

		try {
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(Detach_btn);

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String Adjust_min_After = getElementText(Adjust_sub_Principal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Adjust_max_After = getElementText(Adjust_add_Principal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String Total_amount_After = getElementText(Total_amt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			log.info(Adjust_min_After);
			log.info(Adjust_max_After);
			log.info(Total_amount_After);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			List<WebElement> cls_btn = driver.findElements(Detach_close_btn);
			clickElementBy(cls_btn.get(cls_btn.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Principal Amount is fetched from Transaction history ");
			if (flag.equalsIgnoreCase("ADD")) {
				if (Double.valueOf(Adjust_max_After) == Double.valueOf(Adjust_max)
						+ Double.valueOf(testData.get("TransactionAmount"))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"ADD Principal Amount is validated successfully after tranaction  " + "\n"
									+ "The total amount after transaction is :" + Total_amount_After
									+ " and Before transction was :" + Total_amt_val);
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"ADD Principal Amount is validatation is failed after tranaction  ");
				}
			} else {
				if (Double.valueOf(Adjust_min_After) == Double.valueOf(Adjust_min)
						+ Double.valueOf(testData.get("TransactionAmount"))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Subtract Principal Amount is validated successfully after tranaction" + "\n"
									+ "The subtrsct amount is : " + testData.get("TransactionAmount") + "\n"
									+ "The total amount after transaction is :" + Total_amount_After
									+ " and Before transction was :" + Total_amt_val);
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Subtract Principal Amount is validatation is failed after tranaction  ");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add adjustment mandatory field check tab Description: Verify
	 * mandatory fields Date: 11/May/2020 Author: Ankush Changes By: NA
	 */
	public void add_adjustment_transaction_madatory_Check() throws Exception {

		try {

			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Return_btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for blank parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for blank parameters ");
			}

			/*
			 * tcConfig.updateTestReporter("CustomerService",
			 * "navigate_to_maintanance_tab", Status.PASS,
			 * "Maintanance tab navigated successfully");
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String LatestStatus = getElementText(get_status_latest);
			if (LatestStatus.equals("OPEN")) {

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Edit_btn_intrst);
				clickElementBy(Edit_btn_intrst);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(void_btn_intrst);
				clickElementBy(void_btn_intrst);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String Status_updated = getElementText(get_status_latest);
				if (Status_updated.equals("VOID")) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" The transaction is voided successfully");

				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" The transaction is not voided successfully");
				}
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" The transaction already in posted status");

			}

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add interest Description: Verify add interest Date: 11/May/2020
	 * Author: Ankush Changes By: Harsh/Ankush: Code related to Calendar Pick up
	 * and Process Date Fetch
	 */
	public void add_interest_transaction() throws Exception {

		try {
			payoff_amt_bfr_txn = getElementText(payoff_amt);
			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(TXN_Amt_field).clear();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			String strDate = formatter.format(date);
			log.info(strDate);

			sendKeysBy(TXN_dt_field, strDate);
			/*
			 * clickElementBy(calander_btn);
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * clickElementBy(selectDate);
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysBy(TXN_Amt_field, testData.get("TransactionAmount"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for given parameters ");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(Edit_btn_intrst);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> post = driver.findElements(post_btn_list);
			post.get(post.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (isAlertPresent()) {
				// tcConfig.updateTestReporter("CustomerService", "Add
				// Interest", Status.PASS,
				// " Follow Up date alert is present");

				getElementInView(Result_screen_Open1);
				String Statement = getElementText(Result_screen_Open1);
				String Statement2 = getElementText(Result_screen_Open2);

				String StatementFin = Statement + " " + Statement2;

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Interest transaction is added with statement -" + StatementFin);

			} else {

				// tcConfig.updateTestReporter("CustomerService", "Add
				// Interest", Status.PASS,
				// " Follow Up date alert is not present");
				clickElementBy(ok_btn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Result_screen);
				TXN_Statement = getElementText(Result_screen);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Interest transaction is added with statement -" + TXN_Statement);

			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" Interest is added successfully! ");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: add_transaction Description: Verify add interest Date: June/2020
	 * Author: Ankush Changes By: Harsh/Ankush: Code related to Calendar Pick up
	 * and Process Date Fetch
	 */
	public void add_transaction() throws Exception {

		try {
			getElementInView(payoff_amt);
			payoff_amt_bfr_txn = getElementText(payoff_amt);
			// log.info("Payoff amount "+payoff_amt_bfr_txn);
			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(TXN_Amt_field).clear();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			String strDate = formatter.format(date);
			log.info(strDate);

			sendKeysBy(TXN_dt_field, strDate);
			/*
			 * clickElementBy(calander_btn);
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 * clickElementBy(selectDate);
			 */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysBy(TXN_Amt_field, testData.get("TransactionAmount"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for given parameters ");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(Edit_btn_intrst);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> post = driver.findElements(post_btn_list);
			post.get(post.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (isAlertPresent()) {

				getElementInView(Result_screen_Open1);
				String Statement = getElementText(Result_screen_Open1);
				String Statement2 = getElementText(Result_screen_Open2);

				String StatementFin = Statement + " " + Statement2;

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Transaction is added with statement -" + StatementFin);

			} else {

				if (verifyObjectDisplayed(ok_btn)) {
					clickElementBy(ok_btn);
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Result_screen);
				TXN_Statement = getElementText(Result_screen);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Transaction is added with statement -" + TXN_Statement);

			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" Transction is added successfully! ");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add Principal Description: Verify add principal Date: June/2020
	 * Author: Ankush Changes By: NA
	 */
	public void add_Principal_transaction() throws Exception {

		try {
			getElementInView(payoff_amt);
			payoff_amt_bfr_txn = getElementText(payoff_amt);
			log.info("The payoff amount before transaction is :" + payoff_amt_bfr_txn);
			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(TXN_Amt_field).clear();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			String strDate = formatter.format(date);
			// log.info(strDate);

			sendKeysBy(TXN_dt_field, strDate);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			sendKeysBy(TXN_Amt_field, testData.get("TransactionAmount"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for given parameters ");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(Edit_btn_intrst);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> post = driver.findElements(post_btn_list);
			post.get(post.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (isAlertPresent()) {
				// tcConfig.updateTestReporter("CustomerService", "Add
				// Interest", Status.PASS,
				// " Follow Up date alert is present");

				getElementInView(Result_screen_Open1);
				String Statement = getElementText(Result_screen_Open1);
				String Statement2 = getElementText(Result_screen_Open2);

				String StatementFin = Statement + " " + Statement2;

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Principal transaction is added with statement -" + StatementFin);

			} else {

				// tcConfig.updateTestReporter("CustomerService", "Add
				// Interest", Status.PASS,
				// " Follow Up date alert is not present");
				clickElementBy(ok_btn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Result_screen);
				TXN_Statement = getElementText(Result_screen);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Principal transaction is added with statement -" + TXN_Statement);

			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" Principal is added successfully! ");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: SCRA_transaction Description: Verify add principal Date:
	 * June/2020 Author: Ankush Changes By: NA
	 */
	public void SCRA_transaction(String flag) throws Exception {

		try {
			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (flag.equals("ONDuty")) {
				// driver.findElement(TXN_Rate).clear();

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				Date date = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				String strDate = formatter.format(date);
				// log.info(strDate);

				sendKeysBy(TXN_dt_field, strDate);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				/*
				 * clickElementBy(calander_btn);
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * clickElementBy(selectDate);
				 */

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				sendKeysBy(TXN_Rate, testData.get("TransactionAmount"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> Override = driver.findElements(Override_dropdwn_list);
				selectByText(Override.get(2), "YES");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (flag.equals("OFFDuty")) {

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				selectByText(Account_Rel, "PRIMARY");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				selectByText(Calc_Method, "CALCULATE NEW PAYMENT");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				Date date = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				String strDate = formatter.format(date);
				// log.info(strDate);
				// clickElementBy(TXN_dt_field);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				sendKeysBy(TXN_dt_field, strDate);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				/*
				 * clickElementBy(calander_btn);
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * clickElementBy(selectDate);
				 */

				waitForSometime(tcConfig.getConfig().get("MedWait"));

			}

			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for given parameters ");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(Edit_btn_intrst);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> post = driver.findElements(post_btn_list);
			post.get(post.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (isAlertPresent()) {

				getElementInView(Result_screen_Open1);
				String Statement = getElementText(Result_screen_Open1);
				String Statement2 = getElementText(Result_screen_Open2);

				String StatementFin = Statement + " " + Statement2;

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" SCRA transaction is added with statement -" + StatementFin);

			} else {

				clickElementBy(ok_btn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Result_screen);
				TXN_Statement = getElementText(Result_screen);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" SCRA transaction is added with statement -" + TXN_Statement);

			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" SCRA Transaction is added successfully! ");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: SCRA_transaction Description: Verify add principal Date:
	 * June/2020 Author: Ankush Changes By: NA
	 */
	public void SCRA_transaction_loan(String flag) throws Exception {

		try {
			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			String strDate = formatter.format(date);

			sendKeysBy(TXN_dt_field, strDate);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			List<WebElement> post = driver.findElements(post_btn_list);
			post.get(post.size() - 1).click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for given parameters ");
			}

			if (isAlertPresent()) {

				getElementInView(Result_screen_Open1);
				String Statement = getElementText(Result_screen_Open1);
				String Statement2 = getElementText(Result_screen_Open2);

				String StatementFin = Statement + " " + Statement2;

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" SCRA transaction is added with statement -" + StatementFin);

			} else {

				// clickElementBy(ok_btn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Result_screen);
				TXN_Statement = getElementText(Result_screen);
				String[] New_Pay_amount = TXN_Statement.split("=");
				New_amount = New_Pay_amount[1].replace(" $", "");
				// log.info(New_amount);
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Transaction is added with statement -" + TXN_Statement);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" SCRA transaction is added with statement -" + TXN_Statement);

			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" SCRA Transaction is added successfully! ");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Add Principal paid account Description: Verify add principal for
	 * paid account Date: June/2020 Author: Ankush Changes By: NA
	 */
	public void add_Principal_paidAccnt_Transaction() throws Exception {

		try {

			navigate_to_maintanance_tab();
			String[] transactions = testData.get("Transction").split(",");
			for (int i = 0; i < transactions.length; i++) {

				getElementInView(payoff_amt);
				payoff_amt_bfr_txn = getElementText(payoff_amt);
				// log.info("The payoff amount before transaction is
				// :"+payoff_amt_bfr_txn);
				String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(Add_Tractions_btn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Transactions_field);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(Transactions_field, transactions[i]);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(Transactions_field).sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(LoadParameter_btn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(TXN_Amt_field).clear();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				Date date = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				String strDate = formatter.format(date);
				// log.info(strDate);

				sendKeysBy(TXN_dt_field, strDate);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				sendKeysBy(TXN_Amt_field, testData.get("TransactionAmount"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(saveandReturn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable)
						.getAttribute("_rowcount");
				if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"No New row getting added for parameters ");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" New row getting added for given parameters ");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(Edit_btn_intrst);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> post = driver.findElements(post_btn_list);
				post.get(post.size() - 1).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				String status = getElementText(get_status_latest);
				if (status.equals("OPEN")) {

					if (verifyObjectDisplayed(error_msg)) {

						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								" Error message is displayed for paid off account");
						clickElementBy(ok_btn);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementBy(saveandReturn);
					}
					/*
					 * else { tcConfig.updateTestReporter("CustomerService",
					 * " AddPrincipal", Status.FAIL,
					 * " Error message is not displayed for paid off account");
					 * 
					 * }
					 */
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Result_screen);
				TXN_Statement = getElementText(Result_screen);

				if (TXN_Statement.contains("Successful")) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" Transaction is posted to paid account");
				}

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Transaction is added with statement -" + TXN_Statement);

				if (i == 0) {

					validate_transaction_completed("ADD");
				} else if (i == 1) {
					validate_transaction_completed("SUB");
				}

				// waitForSometime(tcConfig.getConfig().get("LongWait"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: update_transaction_madatory_Check Description: Verify mandatory
	 * fields Date: 11/May/2020 Author: Ankush Changes By: NA
	 */
	public void update_transaction_madatory_Check() throws Exception {

		try {

			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Return_btn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for blank parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for blank parameters ");
			}

			/*
			 * tcConfig.updateTestReporter("CustomerService",
			 * "navigate_to_maintanance_tab", Status.PASS,
			 * "Maintanance tab navigated successfully");
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String LatestStatus = getElementText(get_status_latest);
			if (LatestStatus.equals("OPEN")) {

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Edit_btn_intrst);
				clickElementBy(Edit_btn_intrst);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(void_btn_intrst);
				clickElementBy(void_btn_intrst);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String Status_updated = getElementText(get_status_latest);
				if (Status_updated.equals("VOID")) {

					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" The transaction is voided successfully");

				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							" The transaction is not voided successfully");
				}
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						" The transaction already in posted status");

			}

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Update paid account Description: Verify updates for paid account
	 * Date: June/2020 Author: Ankush Changes By: NA
	 */
	public void Update_paidAccnt_Transaction() throws Exception {

		try {

			navigate_to_maintanance_tab();
			String[] transactions = testData.get("Transction").split(",");
			for (int i = 0; i < transactions.length; i++) {

				getElementInView(payoff_amt);
				payoff_amt_bfr_txn = getElementText(payoff_amt);
				// log.info("The payoff amount before transaction is
				// :"+payoff_amt_bfr_txn);
				String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(Add_Tractions_btn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(Transactions_field);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(Transactions_field, transactions[i]);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(Transactions_field).sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(LoadParameter_btn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(TXN_Amt_field).clear();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				// waitForSometime(tcConfig.getConfig().get("MedWait"));
				// driver.findElement(TXN_Amt_field).clear();
				// waitForSometime(tcConfig.getConfig().get("MedWait"));

				Date date = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				String strDate = formatter.format(date);
				// log.info(strDate);

				sendKeysBy(TXN_dt_field, strDate);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				sendKeysBy(TXN_Amt_field, testData.get("TransactionAmount"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(saveandReturn);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable)
						.getAttribute("_rowcount");
				if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"No New row getting added for parameters ");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							" New row getting added for given parameters ");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(Edit_btn_intrst);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> post = driver.findElements(post_btn_list);
				post.get(post.size() - 1).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (verifyObjectDisplayed(ok_btn)) {
					clickElementBy(ok_btn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}
				getElementInView(Result_screen);
				TXN_Statement = getElementText(Result_screen);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						testData.get("FieldName") + " transaction is added with statement -" + TXN_Statement);
				// log.info(TXN_Statement);
				if (i == 0) {

					if (TXN_Statement.endsWith("2020")) {
						validate_transaction_completed("ADD");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								testData.get("FieldName")
										+ " transaction for Add is Validated and result statement is -"
										+ TXN_Statement);
					}
				} else if (i == 1) {
					if (TXN_Statement.endsWith("2020")) {
						validate_transaction_completed("SUB");
					} else {
						tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
								testData.get("FieldName")
										+ " transaction for Subtract is Validated and result statement is -"
										+ TXN_Statement);
					}
				}

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction is added successfully! ");

				// waitForSometime(tcConfig.getConfig().get("LongWait"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: updateSCRA Description: Verify that the transaction(SCRA) is
	 * getting updated to the account Date: June/2020 Author: Ankush Changes By:
	 * NA
	 */
	public void updateSCRA(String flag) throws Exception {

		try {
			navigate_to_maintanance_tab();
			update_transaction_madatory_Check();
			SCRA_transaction(flag);
			validate_transaction_completed(flag);
			String status = getElementText(get_status_latest);
			String actionCodes = testData.get("action_code");
			String resultCodes = testData.get("result_code");
			if (status.equals("OPEN")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction is in OPEN status, no need to verify");
			} else {

				OFSLLSummaryPage.validateSCRA_summary(flag);

				if (flag.equals("ONDuty")) {

					OFSLLSummaryPage.validateconditioncodes(actionCodes, resultCodes);
				}
				// validatecallActivity(flag);
			}
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Transaction Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: addSCRA_loan Description: Verify that the transaction(SCRA) is
	 * getting added to the loan account Date: Sep/2020 Author: Harsh Changes
	 * By: NA
	 */
	public void addSCRA_loan(String flag) throws Exception {

		try {
			navigate_to_maintanance_tab();
			SCRA_transaction_loan(flag);
			validate_transaction_completed(flag);
			String status = getElementText(get_status_latest);
			if (status.equals("OPEN")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						"Transaction is in OPEN status, no need to verify");
			} else {

				OFSLLSummaryPage.validate_Transction_summary(flag);
			}
			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					"Transaction Updated successfully");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void validatecallActivity(String flag) throws Exception {

		try {

			// getElementInView(callActivitiesTab);
			navigateToCustomerService();
			/*
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * clickElementBy(callActivitiesTab);
			 */
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(callActivityRefreshButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementBy(addButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> Concodes = driver.findElements(condListCallAct);
			getElementInView(condCallAct);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String[] codes = testData.get("summary_condcode").split(",");
			int total_codes = Concodes.size();
			int count = 0;

			if (flag.equals("ONDuty")) {
				for (int i = 0; i < total_codes; i++) {

					for (int j = 0; j < codes.length; j++) {
						if (codes[j].equals(getElementText(Concodes.get(i)))) {
							tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
									" Condition code " + codes[j]
											+ " is  populating in the Condition drop down of Add Call Activity");
							count++;
						}

					}
					// log.info(count);

				}

				if (count < 2) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
							"Condition codes are not populating in the condition drop down of Add Call Activity");
				}

			}

			else if (flag.equals("OFFDuty")) {

				for (int i = 0; i < total_codes; i++) {

					for (int j = 0; j < codes.length; j++) {
						if (codes[j].equals(getElementText(Concodes.get(i)))) {
							tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
									" Condition code " + codes[j]
											+ " is  populating in the Condition drop down of Add Call Activity");
							count++;
						}

					}
					// log.info(count);

				}

				if (count == 0) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
							"Condition codes are not populating in the condition drop down of Add Call Activity");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method: Subtract interest Description: Verify Subtract interest Date:
	 * May/2020 Author: Ankush Changes By: NA
	 */
	public void Sub_interest_transaction() throws Exception {

		try {

			String total_transactions_bfr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(Add_Tractions_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			getElementInView(Transactions_field);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			fieldDataEnter(Transactions_field, testData.get("Transction2"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(Transactions_field).sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(LoadParameter_btn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(TXN_dt_field, testData.get("TransactionDate"));
			// driver.findElement(TXN_dt_field).click();
			// driver.findElement(TXN_dt_field).sendKeys(Keys.DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(TXN_Amt_field).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(TXN_Amt_field, testData.get("TransactionAmount"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(saveandReturn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String total_transactions_aftr_add = driver.findElement(get_rowcount_TXNtable).getAttribute("_rowcount");
			if (total_transactions_aftr_add.equals(total_transactions_bfr_add)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.FAIL,
						"No New row getting added for parameters ");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" New row getting added for given parameters ");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (isAlertPresent()) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Follow Up date alert is present");

				getElementInView(Result_screen_Open1);
				String Statement1 = getElementText(Result_screen_Open1);
				String Statement2 = getElementText(Result_screen_Open2);

				String Statement = Statement1 + " " + Statement2;

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Interest transaction is added with statement -" + Statement);

			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Follow Up date alert is not present");

				getElementInView(Result_screen);
				String Statement1 = getElementText(Result_screen);

				tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
						" Interest transaction is added with statement -" + Statement1);

			}

			tcConfig.updateTestReporter(getClassName(), getMethodName(),Status.PASS,
					" Interest is added successfully! ");

			// waitForSometime(tcConfig.getConfig().get("LongWait"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
