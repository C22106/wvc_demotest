package travelPlusLeisure.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public abstract class TravelPlusLeisureHomePage extends FunctionalComponents {

	protected By cookieAlert = By.xpath("//button[contains(@class,'setCookie') and contains(text(),'Got it')]");
	protected By travelPlusLeisureLogo = By
			.xpath("(//a[img[contains(@class,'logo')] and parent::div[contains(@class,'cell')]])[1]");
	protected By brandLogoComponent = By
			.xpath("//div[contains(@class,'tlFooter')]//div[contains(@class,'logo-footer')]/a[img]");

	protected By ourCompany = By.xpath("//div[contains(@class,'mainMenu')]//a/span[contains(.,'Our Company')]/..");
	protected By ourBrands = By.xpath("//div[contains(@class,'mainMenu')]//a/span[contains(.,'Our Brands')]/..");
	protected By careers = By.xpath("//div[contains(@class,'mainMenu')]//a/span[contains(.,'Careers')]/..");
	protected By investorRelations = By
			.xpath("//div[contains(@class,'mainMenu')]//a/span[contains(.,'Investor Relations')]/..");
	protected By socialResponsibilities = By
			.xpath("//div[contains(@class,'mainMenu')]//a/span[contains(.,'Social Responsibility')]/..");
	protected By news = By.xpath("//div[contains(@class,'mainMenu')]//a/span[contains(.,'News')]/..");
	protected By slickDots = By.xpath("//ul[@class='slick-dots']/li");
	protected By bannerImgSrc = By.xpath("//div[@class='image-quote-banner']//img");
	protected By bannerTitleObj = By.xpath("//div[@class='image-quote-banner']//div[contains(@class,'title-1')]");
	protected By bannerSubTitleObj = By.xpath("//div[@class='image-quote-banner']//div[contains(@class,'body-1')]");
	protected By learnMoreBtn = By.xpath("//div[@class='image-quote-banner']//a[text()='Learn More']");
	protected By breadcrumbLink = By.xpath("//ul/li[last()]/a[@data-eventname='breadCrumb']");
	protected By exploringTimeshareHeader = By.xpath("//div[text()='Exploring Timeshare Ownership']");
	protected By cardComponent = By.xpath("//div[@class='cardComponent']//div[@class='cell']/div/div[a]");

	protected By promoSustainableTravelImage = By.xpath(
			"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[@class='cardBanner']//img)[1]");

	protected By promoSustainableTravelTitle = By.xpath(
			"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[1]");
	protected By promoSustainableTravelBody = By.xpath(
			"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[1]");
	protected By btnLearnMorePromoSustainableTravel = By.xpath(
			"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[1]");

	protected By panoramaBrandImage = By.xpath(
			"(//div[contains(.,'Panorama')]/div[@gtm_component]//div[contains(@class,'blockBannerRight')]//img)[1]");
	protected By panoramaTitle = By
			.xpath("//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Panorama')]");
	protected By panoramaBody = By.xpath(
			"(//div[contains(.,'Panorama')]/div[@gtm_component]//div[contains(@class,'blockBannerRight')]//div[contains(@class,'body')])[1]");
	protected By btnLearnMorepanorama = By.xpath(
			"(//div[contains(.,'Panorama')]/div[@gtm_component]//div[contains(@class,'blockBannerRight')]//a)[1]");

	protected By wyndhamDestinationsBrandImage = By.xpath(
			"(//div[contains(.,'Wyndham Destinations')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//img)[1]");
	protected By wyndhamDestinationsTitle = By.xpath(
			"//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Wyndham Destinations')]");
	protected By wyndhamDestinationsBody = By.xpath(
			"(//div[contains(.,'Wyndham Destinations')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//div[contains(@class,'body')])[1]");
	protected By btnLearnMoreWyndhamDestinations = By.xpath(
			"(//div[contains(.,'Wyndham Destinations')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//a)[1]");

	protected By travelPlusLeisureBrandImage = By.xpath(
			"(//div[contains(.,'Travel + Leisure Group')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//img)[2]");
	protected By travelPlusLeisureTitle = By.xpath(
			"//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Travel + Leisure Group')]");
	protected By travelPlusLeisureBody = By.xpath(
			"(//div[contains(.,'Travel + Leisure Group')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//div[contains(@class,'body')])[2]");
	protected By btnLearnMoreTravelPlusLeisure = By.xpath(
			"(//div[contains(.,'Travel + Leisure Group')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//a)[2]");

	protected By greatVacationHeader = By.xpath(
			"//div[contains(.,'Keys To A Great Timeshare Vacation') and contains(@class,'contentSlice')]//div[contains(@class,'caption')]");
	protected By keystoVacationComponent = By.xpath(
			"//div[contains(.,'Keys To A Great Timeshare Vacation')]/div[@class='cardComponent']//div[@class='cell']/div/div[a]");
	protected By latestNewsHeader = By.xpath("//div[@class='contentSlice' and contains(.,'Latest News')]");
	protected By latestNewsComponent = By.xpath(
			"//div[contains(.,'Latest News')]/div[@class='indexCardComponent']//div[@class='cell']/div/div[a and @data-ishidden='false']");
	protected By careerFooter = By.xpath("(//div[contains(@class,'tlFooter')]//a[span[text()='Careers']])[1]");
	protected By contactUsFooter = By.xpath("(//div[contains(@class,'tlFooter')]//a[span[text()='Contact Us']])[1]");
	protected By investorsFooter = By.xpath("(//div[contains(@class,'tlFooter')]//a[span[text()='Investors']])[1]");
	protected By newsFooter = By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='News']]");
	protected By socialResponsibilityFooter = By
			.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='Social Responsibility']]");
	protected By ourBrandsFooter = By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='Our Brands']]");
	protected By OurCompanyFooter = By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='Our Company']]");

	protected By facebookPageObj = By.xpath("(//a[contains(@href,'www.facebook.com/travelleisureco')])[1]");

	protected By twitterIcon = By
			.xpath("(//div[contains(@class,'tlFooter')]//li/a[contains(@href,'https://twitter.com/tnlconews')])[1]");
	protected By twitterPageObj = By.xpath("(//span[text()='Travel + Leisure Co.'])[1]");
	protected By linkedInIcon = By.xpath(
			"(//div[contains(@class,'tlFooter')]//li/a[contains(@href,'https://www.linkedin.com/company/travelleisureco')])[1]");
	protected By facebookIcon = By.xpath(
			"(//div[contains(@class,'tlFooter')]//li/a[contains(@href,'https://www.facebook.com/travelleisureco')])[1]");

	protected By instaIcon = By.xpath("//li/a[contains(@href,'https://www.instagram.com/timeshare_com/')]");
	protected By instaPageObj = By.xpath("//a[@page_id='profilePage' and text()='www.timeshare.com']");

	protected By youtubeIcon = By
			.xpath("//li/a[contains(@href,'https://www.youtube.com/channel/UCwy9UWOWSLAypQmetVS95Cw')]");
	protected By youtubePageObj = By.xpath("//div[@id='text-container' and contains(.,'Timeshare Info')]");
	protected By footerWyndhamLogo = By
			.xpath("(//div[contains(@class,'footerFooter')]//a[img[@alt='wyndham destinations logo']])[1]");
	protected By privacyNotice = By
			.xpath("(//ul[@class='quickLinks menu float-right']//a[@data-eventlabel='Privacy Notice'])[1]");
	protected By termsOfUse = By
			.xpath("(//ul[@class='quickLinks menu float-right']//a[@data-eventlabel='Terms of Use'])[1]");
	protected By privacySettings = By
			.xpath("(//ul[@class='quickLinks menu float-right']//a[@data-eventlabel='Privacy Settings'])[1]");
	protected By supportTimeshare = By.xpath(
			"(//ul[@class='quickLinks menu float-right']//a[@data-eventlabel='Proudly Supports Timeshare.com'])[1]");

	protected By footerCopyRight = By.xpath(
			"(//div[contains(@class,'footerCopyrightLarge')]/ul/li[contains(text(),'© Travel + Leisure Co. 2021. All Rights Reserved.')])[1]| (//div[contains(@class,'footerCopyrightLarge')]/ul/li[contains(text(),'© Travel + Leisure Co. 2021. <i>All Rights Reserved.</i>')])[1]");
	protected By doNotsellInfo = By.xpath(
			"(//div[@class='align-self-middle cell']//a[contains(text(),'Do Not Sell My Personal Information')])[1]");
	protected By optOutForm = By.xpath("//div[text()='Opt Out Request Form']");

	protected String url;

	public TravelPlusLeisureHomePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	public void launchApplication() {

		url = tcConfig.getTestData().get("URL");
		driver.get(url);
		pageCheck();
		if (verifyObjectDisplayed(cookieAlert)) {
			clickElementBy(cookieAlert);
		}

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "launchApplication", Status.PASS,
				"TravelPlusLeisure URL was launched successfully");
	}

	public void validateLogo() {
		assertTrue(verifyObjectDisplayed(travelPlusLeisureLogo), "Header TravelPlusLeisure logo is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLogo", Status.PASS,
				"Header TravelPlusLeisure logo is displayed");

		assertTrue(getElementAttribute(travelPlusLeisureLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Header TravelPlusLeisure logo url not correct");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLogo", Status.PASS,
				"Header TravelPlusLeisure logo is present");
	}

	public void validateBrandLogo() {
		getElementInView(brandLogoComponent);
		assertTrue(verifyObjectDisplayed(brandLogoComponent), "Brand logos are not Displayed");
		List<WebElement> brandLogos = getList(brandLogoComponent);
		for (int i = 1; i < brandLogos.size(); i++) {
			brandLogos = getList(brandLogoComponent);
			assertTrue(brandLogos.get(i).isDisplayed(), "Brand Logo is not Displayed");
			String href = brandLogos.get(i).getAttribute("href").trim();
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBrandLogo", Status.PASS,
					"Brand logo is present for " + href);
			if (i == 1) {
				href = "https://www.wyndhamdestinations.com/";
			} else if (i == 2) {
				href = "https://www.panoramaco.com/";
			} else if (i == 3) {
				href = url + "us/en/our-brands/travel-leisure-group";
			}

			clickElementBy(brandLogos.get(i));
			pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				String getCurrentURL = driver.getCurrentUrl().trim();
				assertTrue(href.contains(getCurrentURL), "Link Navigation is not Correct");
				tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBrandLogo", Status.PASS,
						"Link navigated to correct page" + getCurrentURL);
				navigateToMainTab(allTabs);
				pageCheck();
			} else {
				String getCurrentURL = driver.getCurrentUrl().trim();
				assertTrue(href.contains(getCurrentURL), "Link Navigation is not Correct");
				tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBrandLogo", Status.PASS,
						"Link navigated to correct page" + getCurrentURL);
				navigateBack();
				pageCheck();
			}

		}

	}

	public void validateHomeHeaderMenu() {

		assertTrue(verifyObjectDisplayed(ourCompany), "Our Company menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Our Company menu item is present");
		navigateValidateAndReturn(ourCompany);

		assertTrue(verifyObjectDisplayed(ourBrands), "OurBrands menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"OurBrands menu item is present");
		navigateValidateAndReturn(ourBrands);

		assertTrue(verifyObjectDisplayed(careers), "Careers menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Careers menu item is present");
		navigateValidateAndReturntoPage(careers, "careers");

		assertTrue(verifyObjectDisplayed(investorRelations), "Investors Relations menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Investors Relations menu item is present");
		navigateValidateAndReturntoPage(investorRelations, "travelandleisureco");

		assertTrue(verifyObjectDisplayed(socialResponsibilities), "Social Responsibility menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Social Responsibility menu item is present");
		navigateValidateAndReturn(socialResponsibilities);

		assertTrue(verifyObjectDisplayed(news), "News menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"News menu item is present");
		navigateValidateAndReturn(news);
	}

	public void validateHomeHeaderSubMenu(String headerMenuItem, String subMenu, int count) {

		if (count == 0) {
			clickElementBy(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem + "']]]"));
			pageCheck();

			assertTrue(verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();

		} else {
			waitForSometime("5");
			hoverOnElement(By
					.xpath("//div[contains(@class,'genHeaderParent')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
							+ headerMenuItem + "')]"
			/*
			 * "//*[contains(@class,'submenu-item')][a[span[text()='"+
			 * headerMenuItem+"']]]"
			 */));
			waitForSometime("8");

			assertTrue(
					getList(By
							.xpath("//div[contains(@class,'genHeaderParent')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
									+ headerMenuItem + "')]/../..//ul/li")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By
									.xpath("//div[contains(@class,'genHeaderParent')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
											+ headerMenuItem + "')]/../..//ul/li")).size());

			assertTrue(
					verifyObjectDisplayed(
							By.xpath("//ul[contains(@class,'dropdown-submenu')]//a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			clickElementJSWithWait(
					By.xpath("//ul[contains(@class,'dropdown-submenu')]//a[contains(.,'" + subMenu + "')]"));

			pageCheck();
			if (subMenu.equalsIgnoreCase("Board of Directors")) {
				subMenu = "travelandleisureco";
			} else if (subMenu.equalsIgnoreCase("Wyndham Destinations")) {
				subMenu = "wyndhamdestinations";
			} else if (subMenu.equalsIgnoreCase("Panorama")) {
				subMenu = "panoramaco";
			} else if (subMenu.equalsIgnoreCase("Mission & Values")) {
				subMenu = "Mission and Values";
			}
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				waitForSometime("3");
				String getUrl = driver.getCurrentUrl().trim();
				assertTrue(getUrl.contains(subMenu), "Page is not navigated Successfully");
				tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						subMenu + " navigations successful");
				navigateToMainTab(allTabs);
				pageCheck();

			} else {
				getElementInView(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']"));
				assertTrue(verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
						subMenu + " navigations not successful");

				tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						subMenu + " navigations successful");

				navigateBack();
				pageCheck();
			}

		}

	}

	public void validateBanners() {

		assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
		String bannerTitle = getElementText(bannerTitleObj);
		String bannerSubTitle = getElementText(bannerSubTitleObj);
		assertTrue(!bannerTitle.trim().equals(""), "Banner title absent");
		assertTrue(!bannerSubTitle.trim().equals(""), "Banner Sub title absent");
		assertTrue(verifyObjectDisplayed(learnMoreBtn), "Learn more button is absent");
		String linkdetails = getElementAttribute(learnMoreBtn, "href");
		clickElementJSWithWait(learnMoreBtn);
		pageCheck();
		String getUrl = driver.getCurrentUrl().trim();
		assertTrue(getUrl.equalsIgnoreCase(linkdetails), "Navigation to learn more link not correct");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBanners", Status.PASS,
				"Banner Title : " + bannerTitle + " navigating to link : " + linkdetails + " successful");
		navigateBack();
		pageCheck();

	}

	protected By stockTicker = By.xpath("//div[@id='ticker']//div[@class='stockTicker']");
	protected By stockDate = By.xpath("//div[@id='ticker']//div[@class='stockDate']");
	protected By stockPrice = By.xpath("//div[@id='ticker']//div[@class='stockPrice']");
	protected By stockGains = By.xpath("//div[@id='ticker']//div[contains(@class,'stockGains')]");
	protected By stockCTA = By.xpath("//div[@id='ticker']//div[contains(@class,'stockCta')]//a");

	public void validateTicker() {
		String strStockTicker = getElementText(stockTicker);
		String strStockDate = getElementText(stockTicker);
		String strStockPrice = getElementText(stockTicker);
		String strStockGains = getElementText(stockTicker);
		assertTrue(!strStockTicker.trim().equals(""), "Stock Ticker absent");
		assertTrue(!strStockDate.trim().equals(""), "Stock Date absent");
		assertTrue(!strStockPrice.trim().equals(""), "Stock Price absent");
		assertTrue(!strStockGains.trim().equals(""), "Stock Gains absent");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBanners", Status.PASS,
				"Stock Ticker" + ":" + " " + strStockTicker + " " + "Stock Date" + ":" + " " + strStockDate + " "
						+ "Stock Price" + ":" + " " + strStockPrice + " " + "Stock Gains" + ":" + " " + strStockGains
						+ " " + "is Displayed");

	}

	public void validateCardComponent() {

		List<WebElement> cardList = getList(cardComponent);
		for (int i = 0; i < cardList.size(); i++) {
			getElementInView(cardComponent);

			cardList = getList(cardComponent);

			assertTrue(cardList.get(i).findElement(By.xpath("./a")).isDisplayed(), "cards link not present");
			String hrefCard = cardList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(cardList.get(i).findElement(By.xpath("./div/a")).isDisplayed(), "pictures link not present");
			String hrefPicture = cardList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Card section link not present");
			String hrefCardSection = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Cards");

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.isDisplayed(), "card section title not present");
			String cardTitle = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateCardComponent", Status.PASS,
					"All links are in sync within the Card, which is : " + hrefCard);

			WebElement eleLearnMore = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Learn More']"));
			clickElementJSWithWait(eleLearnMore);
			pageCheck();

			if (cardTitle.equalsIgnoreCase("Our Investors")) {
				cardTitle = "investor.travelandleisureco";
			}

			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				String title = driver.getCurrentUrl().trim();
				assertTrue(title.toLowerCase().contains(cardTitle.toLowerCase()),
						"Link Navigation not correct, expected : " + title + " | Actual : " + title);
				tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page: " + cardTitle + " navigating to link : " + title + " successful");
				navigateToMainTab(allTabs);
				pageCheck();
			} else {
				String title = driver.getCurrentUrl().trim();
				assertTrue(title.toLowerCase().contains(cardTitle.toLowerCase()),
						"Link Navigation not correct, expected : " + title + " | Actual : " + title);
				tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page: " + cardTitle + " navigating to link : " + title + " successful");
				navigateBack();
				pageCheck();
			}

		}
	}

	public void validatePromotingSustainableTravel() {

		getElementInView(promoSustainableTravelTitle);

		assertTrue(verifyObjectDisplayed(promoSustainableTravelImage), "Promoting Sustainable Travel image is absent");
		assertTrue(verifyObjectDisplayed(promoSustainableTravelTitle), "Promoting Sustainable Travel title is absent");
		assertTrue(getElementText(promoSustainableTravelTitle).trim().length() > 0,
				"Promoting Sustainable Travel title is blank");
		String title = getElementText(promoSustainableTravelTitle).trim();

		assertTrue(verifyObjectDisplayed(promoSustainableTravelBody), "Promoting Sustainable Travel body is absent");
		assertTrue(getElementText(promoSustainableTravelBody).trim().length() > 0,
				"Promoting Sustainable Travel body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorePromoSustainableTravel),
				"Promoting Sustainable Travel Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePromotingSustainableTravel", Status.PASS,
				"Promoting Sustainable Travel content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorePromoSustainableTravel, "href");
		clickElementBy(btnLearnMorePromoSustainableTravel);
		pageCheck();
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePromotingSustainableTravel", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	public void validateWyndhamDestinationsBrandContent() {

		getElementInView(wyndhamDestinationsBrandImage);

		assertTrue(verifyObjectDisplayed(wyndhamDestinationsBrandImage), "Wyndham Destinations image is absent");
		assertTrue(verifyObjectDisplayed(wyndhamDestinationsTitle), "Wyndham Destinations title is absent");
		assertTrue(getElementText(wyndhamDestinationsTitle).trim().length() > 0, "Wyndham Destinations title is blank");
		String title = getElementText(wyndhamDestinationsTitle).trim();

		assertTrue(verifyObjectDisplayed(wyndhamDestinationsBody), "Wyndham Destinations body is absent");
		assertTrue(getElementText(wyndhamDestinationsBody).trim().length() > 0, "Wyndham Destinations body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreWyndhamDestinations),
				"Wyndham Destinations Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateWyndhamDestinationsBrandContent", Status.PASS,
				"Wyndham Destinations content is displayed Successfully");

		String href = getElementAttribute(btnLearnMoreWyndhamDestinations, "href").trim();
		String browserTitle = getElementAttribute(btnLearnMoreWyndhamDestinations, "data-eventlabel").trim()
				.toLowerCase().replace(" ", "");
		clickElementBy(btnLearnMoreWyndhamDestinations);
		pageCheck();

		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			String getURL = driver.getCurrentUrl().trim().toLowerCase();
			assertTrue(getURL.equalsIgnoreCase("https://www.wyndhamdestinations.com/"),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateWyndhamDestinationsBrandContent",
					Status.PASS, "Content Title : " + title + " navigating to link : " + href + " successfull");
			navigateToMainTab(allTabs);
			pageCheck();
		}
	}

	public void validatePanoramacoBrandContent() {

		getElementInView(panoramaTitle);

		assertTrue(verifyObjectDisplayed(panoramaBrandImage), "Panoramaco image is absent");
		assertTrue(verifyObjectDisplayed(panoramaTitle), "Panoramaco title is absent");
		assertTrue(getElementText(panoramaTitle).trim().length() > 0, "Panoramaco title is blank");
		String title = getElementText(panoramaTitle).trim();

		assertTrue(verifyObjectDisplayed(panoramaBody), "Panoramaco body is absent");
		assertTrue(getElementText(panoramaBody).trim().length() > 0, "Panoramaco body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorepanorama), "Panoramaco Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePanoramacoBrandContent", Status.PASS,
				"Panoramaco content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorepanorama, "href").trim();
		String browserTitle = getElementAttribute(btnLearnMorepanorama, "data-eventlabel").trim().toLowerCase();
		clickElementBy(btnLearnMorepanorama);
		pageCheck();

		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			String getURL = driver.getCurrentUrl().trim().toLowerCase();
			assertTrue(getURL.equalsIgnoreCase("https://www.panoramaco.com/"),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePanoramacoBrandContent", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successfull");
			navigateToMainTab(allTabs);
			pageCheck();
		}
	}

	public void validateTravelPlusLeisureBrandContent() {

		getElementInView(travelPlusLeisureTitle);

		assertTrue(verifyObjectDisplayed(travelPlusLeisureBrandImage), "Travel Plus Leisure Group image is absent");
		assertTrue(verifyObjectDisplayed(travelPlusLeisureTitle), "Travel Plus Leisure Group title is absent");
		assertTrue(getElementText(travelPlusLeisureTitle).trim().length() > 0,
				"Travel Plus Leisure Group title is blank");
		String title = getElementText(travelPlusLeisureTitle).trim();

		assertTrue(verifyObjectDisplayed(travelPlusLeisureBody), "Travel Plus Leisure Group body is absent");
		assertTrue(getElementText(travelPlusLeisureBody).trim().length() > 0,
				"Travel Plus Leisure Group body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreTravelPlusLeisure),
				"Travel Plus Leisure Group Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTravelPlusLeisureBrandContent", Status.PASS,
				"Travel Plus Leisure Group content is displayed Successfully");

		String href = getElementAttribute(btnLearnMoreTravelPlusLeisure, "href").trim();
		clickElementBy(btnLearnMoreTravelPlusLeisure);
		pageCheck();

		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTravelPlusLeisureBrandContent", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();

	}

	public void validateLatestNews() {

		assertTrue(verifyObjectDisplayed(latestNewsHeader), "Latest News header is absent");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLatestNews", Status.PASS,
				"Latest News header is present");

		List<WebElement> newsList = getList(latestNewsComponent);

		for (int i = 0; i < newsList.size(); i++) {

			getElementInView(latestNewsComponent);
			newsList = getList(latestNewsComponent);

			assertTrue(newsList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Latest News cards link not present");
			String hrefCard = newsList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(newsList.get(i).findElement(By.xpath("./div/a")).isDisplayed(),
					"Latest News pictures link not present");
			String hrefPicture = newsList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Latest News card section link not present");
			String hrefCardSection = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Latest News card");

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.isDisplayed(), "Latest News card section title not present");
			String cardTitle = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateLatestNews", Status.PASS,
					"All links are in sync within the Latest News Card, which is : " + hrefCard);

			WebElement eleReadMore = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Read More']"));
			clickElementBy(eleReadMore);

			pageCheck();
			getElementInView(breadcrumbLink);
			assertTrue(
					/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(hrefCard),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLatestNews", Status.PASS,
					"Latest News Title : " + cardTitle + " navigating to link : " + hrefCard + " successful");
			navigateBack();
			pageCheck();

		}
	}

	public void validateFooterMenu() {

		waitUntilElementVisibleBy(driver, OurCompanyFooter, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(OurCompanyFooter), "Our Company menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Our Company menu item is present");
		navigateValidateAndReturn(OurCompanyFooter);

		waitUntilElementVisibleBy(driver, ourBrandsFooter, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(ourBrandsFooter), "Our Brand menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Our Brand menu item is present");
		navigateValidateAndReturn(ourBrandsFooter);

		waitUntilElementVisibleBy(driver, socialResponsibilityFooter, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(socialResponsibilityFooter), "Social Responsibility menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Social Responsibility menu item is present");
		navigateValidateAndReturn(socialResponsibilityFooter);

		waitUntilElementVisibleBy(driver, newsFooter, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(newsFooter), "News menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"News menu item is present");
		navigateValidateAndReturn(newsFooter);

		waitUntilElementVisibleBy(driver, investorsFooter, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(investorsFooter), "Investors menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Investors menu item is present");
		navigateValidateAndReturntoPage(investorsFooter, "travelandleisureco");

		waitUntilElementVisibleBy(driver, careerFooter, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(careerFooter), "Career menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Career menu item is present");
		navigateValidateAndReturntoPage(careerFooter, "careers");

		waitUntilElementVisibleBy(driver, contactUsFooter, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(contactUsFooter), "Contact US menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Contact US menu item is present");
		navigateValidateAndReturn(contactUsFooter);

	}

	public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href");
		String title = getElementAttribute(link, "data-eventlabel");
		clickElementJSWithWait(link);
		pageCheck();
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
				"Link Navigation not correct, expected : " + href + " | Actual : "
						+ /*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim());
		tcConfig.updateTestReporter("TimeshareHomePage", "navigateValidateAndReturn", Status.PASS,
				"Latest News Title : " + title + " navigating to link : " + href + " successful");

		navigateBack();
		pageCheck();

	}

	public void navigateValidateAndReturntoPage(By link, String menu) {

		String href = getElementAttribute(link, "href").trim();
		clickElementJSWithWait(link);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.contains(menu),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			pageCheck();
		} else {
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.contains(menu),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}

	public void validateHomeFooterSubMenu(String headerMenuItem, String subMenu, int count) {

		if (count == 0) {
			navigateValidateAndReturn(
					By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='" + headerMenuItem + "']]"));

		} else {

			assertTrue(
					getList(By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='" + headerMenuItem
							+ "']]/../following-sibling::li/a")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='" + headerMenuItem
									+ "']]/../following-sibling::li/a")).size());

			assertTrue(
					verifyObjectDisplayed(By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='"
							+ headerMenuItem + "']]/../following-sibling::li/a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeFooterSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			clickElementJSWithWait(By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='" + headerMenuItem
					+ "']]/../following-sibling::li/a[contains(.,'" + subMenu + "')]"));
			pageCheck();

			if (subMenu.equalsIgnoreCase("Board of Directors")) {
				subMenu = "travelandleisureco";
			} else if (subMenu.equalsIgnoreCase("Wyndham Destinations")) {
				subMenu = "wyndhamdestinations";
			} else if (subMenu.equalsIgnoreCase("Panorama")) {
				subMenu = "panoramaco";
			} else if (subMenu.equalsIgnoreCase("Mission & Values")) {
				subMenu = "Mission and Values";
			} else if (subMenu.equalsIgnoreCase("Travel + Leisure Co. Overview")) {
				subMenu = "news-and-media/wynd-company-overview-080720.pdf";
			}
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				String getUrl = driver.getCurrentUrl().trim();
				assertTrue(getUrl.contains(subMenu), "Page is not navigated Successfully");
				tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						subMenu + " navigations successful");
				navigateToMainTab(allTabs);
				pageCheck();

			} else {
				getElementInView(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']"));
				assertTrue(verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
						subMenu + " navigations not successful");

				tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						subMenu + " navigations successful");

				navigateBack();
				pageCheck();
			}

		}

	}

	public void validateTwitterLink() {

		getElementInView(twitterIcon);
		assertTrue(verifyObjectDisplayed(twitterIcon), "Twitter icon is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTwitterLink", Status.PASS,
				"Twitter icon is present");

		clickElementBy(twitterIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		assertTrue(verifyObjectDisplayed(twitterPageObj), "Twitter link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateTwitterLink", Status.PASS,
				"Twitter link navigation successful");
		navigateToMainTab(tabs2);
		// navigateBack();
		pageCheck();

	}

	public void validateLinkedInLink() {

		getElementInView(linkedInIcon);
		assertTrue(verifyObjectDisplayed(linkedInIcon), "LinkedIn icon is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLinkedInLink", Status.PASS,
				"LinkedIn icon is present");
		String href = getElementAttribute(linkedInIcon, "href");
		clickElementBy(linkedInIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		String url = driver.getCurrentUrl().trim();
		assertTrue(url.contains("https://www.linkedin.com"), "LinkedIn link navigation not successful");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLinkedInLink", Status.PASS,
				"LinkedIn link navigation successful");
		navigateToMainTab(tabs2);
		// navigateBack();
		pageCheck();

	}

	public void validateFacebookLink() {

		getElementInView(facebookIcon);
		assertTrue(verifyObjectDisplayed(facebookIcon), "Facebook icon is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFacebookLink", Status.PASS,
				"LinkedIn icon is present");
		String href = getElementAttribute(facebookIcon, "href");
		clickElementBy(facebookIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		String url = driver.getCurrentUrl().trim();
		assertTrue(href.equalsIgnoreCase(url), "Facebook link navigation not successful");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFacebookLink", Status.PASS,
				"LinkedIn link navigation successful");
		navigateToMainTab(tabs2);
		// navigateBack();
		pageCheck();

	}

	public void validatefooterlogo() {

		String url = testData.get("URL");
		assertTrue(verifyObjectDisplayed(footerWyndhamLogo), "Footer Wyndham logo is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatefooterlogo", Status.PASS,
				"Footer Wyndham logo is present");

		assertTrue(getElementAttribute(footerWyndhamLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Footer url not correct");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatefooterlogo", Status.PASS,
				"Footer Wyndham logo is present");

	}

	public void validateTermsofUse() {

		assertTrue(verifyObjectDisplayed(termsOfUse), "Terms of Use link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTermsofUse", Status.PASS,
				"Terms of Use link is present");
		navigateValidateAndReturn(termsOfUse);

	}

	public void validatePrivacyNotice() {

		assertTrue(verifyObjectDisplayed(privacyNotice), "Privacy notice link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePrivacyNotice", Status.PASS,
				"Privacy notice link is present");
		navigateValidateAndReturn(privacyNotice);

	}

	public void validatePrivacySettings() {

		assertTrue(verifyObjectDisplayed(privacySettings), "Privacy settings link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePrivacySettings", Status.PASS,
				"Privacy settings link is present");
		navigateValidateAndReturntoPage(privacySettings, "info.evidon");

	}

	public void validateSupportTimeshare() {

		assertTrue(verifyObjectDisplayed(supportTimeshare), "Support Timeshare link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateSupportTimeshare", Status.PASS,
				"Support Timeshare link is present");
		navigateValidateAndReturntoPage(supportTimeshare, "timeshare");

	}

	public void validatefooterCopyRight() {

		assertTrue(verifyObjectDisplayed(footerCopyRight), "Footer CopyRight Label is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatefooterCopyRight", Status.PASS,
				"Footer CopyRight Label is present" + " " + getElementText(footerCopyRight));

	}

	public void validateDoNotSellInfo() {
		assertTrue(verifyObjectDisplayed(doNotsellInfo), "Do not sell info link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateDoNotSellInfo", Status.PASS,
				"Do not sell info link is present");
		clickElementBy(doNotsellInfo);
		pageCheck();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if (tabs.size() == 2) {
			newTabNavigations(tabs);
			getElementInView(optOutForm);
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateToMainTab(tabs);
			pageCheck();
		} else {
			getElementInView(optOutForm);
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateBack();
			pageCheck();
		}

	}

	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("html")) {
					System.out.println("URL contians html");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it.");
				 * report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components",
							"brokenLinkValidation", Status.FAIL, "[Response Code: " + respCode + "] [Response message: "
									+ respMessage + "] [URL:" + url + "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}

	public void launchApplicationforBrokenLinkValidation() {
		driver.get(tcConfig.getTestData().get("URL"));

	}

	protected By searchContainer = By.xpath("//div[@id='searchInputBox']//input[contains(@class,'searchQuery')]");
	protected By searchIcon = By.xpath("//a[contains(@class,'tnlSearchButton')]//img");
	protected By searchButton = By.xpath("//div[@id='searchInputBox']//input[contains(@class,'siteSearch')]");
	protected By searchTitle = By.xpath("//div[contains(@class,'search-title')]");
	protected By resultNumber = By.xpath("//div[contains(@class,'resultNumber')]");
	protected By resultList = By.xpath("//ul[contains(@class,'searchResults')]/li//div[@class='searchItemTitle']");
	protected By paginationTab = By.xpath("//ul[contains(@class,'pagination')]");
	protected By paginationNext = By.xpath("//li[@class='pagination-next']");
	protected By firstPageList = By.xpath(
			"//ul[contains(@class,'searchResults')]/li[@class='searchItem page-1']//div[@class='searchItemTitle']");
	protected By searchListLink = By.xpath(
			"//ul[contains(@class,'searchResults')]/li[@class='searchItem page-1']//div[@class='searchItemTitle']/..");

	public void validateSearchFunctionality() {
		String searchValue = testData.get("strSearch");
		assertTrue(verifyObjectDisplayed(searchIcon), "Search Icon is not Displayed");
		clickElementBy(searchIcon);
		waitUntilElementVisibleBy(driver, searchContainer, "ExplicitLongWait");
		fieldDataEnter(searchContainer, searchValue);
		clickElementBy(searchButton);
		pageCheck();
		assertTrue(verifyObjectDisplayed(searchTitle), "Search Page is not Loaded");

	}

	public void searchResultsValidatins() {

		waitUntilElementVisibleBy(driver, searchTitle, 120);

		if (verifyObjectDisplayed(searchTitle)) {
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");

			String strResultsFor = getElementText(resultNumber);

			String strResults = strResultsFor.split(" ")[0];

			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchFunctionality", Status.PASS,
					"Total Results: " + strResults + " for" + strResultsFor);

			List<WebElement> listOfTotalResults = driver.findElements(resultList);
			List<WebElement> listFirstResults = driver.findElements(firstPageList);
			List<WebElement> listCTA = driver.findElements(searchListLink);
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"No Of Results on first page " + listFirstResults.size());

			if (listOfTotalResults.size() >= 10) {
				if (verifyObjectDisplayed(paginationTab)) {
					tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
							"Pagination Available on the page");
				}
			} else {
				System.out.println("Less than 10 results hence no pagination");
			}

			String firstTitle = listFirstResults.get(0).getText();
			String href = getElementAttribute(listCTA.get(0), "href").trim().replace(".html", "");

			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"First Result title " + firstTitle);

			clickElementBy(listFirstResults.get(0));
			pageCheck();
			assertTrue(getCurrentURL().trim().contains(href), "Navigations not successfull");

			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to first Search related page " + firstTitle);

			driver.navigate().back();

			pageCheck();

		} else {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

}
