package travelPlusLeisure.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TravelPlusLeisureHomePage_Android extends TravelPlusLeisureHomePage_Web {

	// protected String url;

	public TravelPlusLeisureHomePage_Android(TestConfig tcconfig) {
		super(tcconfig);

		promoSustainableTravelImage = By.xpath(
				"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[@class='cardBanner']//img)[4]");
		promoSustainableTravelTitle = By.xpath(
				"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[4]");
		promoSustainableTravelBody = By.xpath(
				"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[4]");
		btnLearnMorePromoSustainableTravel = By.xpath(
				"(//div[contains(.,'Promoting Sustainable Travel')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[4]");
		careerFooter = By.xpath("(//div[contains(@class,'wdFooter')]//a[text()='Careers'])[2]");
		contactUsFooter = By.xpath("(//div[contains(@class,'wdFooter')]//a[text()='Contact Us'])[2]");
		investorsFooter = By.xpath("(//div[contains(@class,'wdFooter')]//a[text()='Investors'])[2]");

		twitterIcon = By.xpath(
				"(//div[contains(@class,'socialMedia')]//li/a[contains(@href,'https://twitter.com/tnlconews')])[4]");
		linkedInIcon = By.xpath(
				"(//div[contains(@class,'socialMedia')]//li/a[contains(@href,'https://www.linkedin.com/company/travelleisureco')])[4]");
		footerWyndhamLogo = By
				.xpath("(//div[contains(@class,'footerFooter')]//a[img[contains(@class,'footerLogoSmall')]])[2]");
		doNotsellInfo = By.xpath(
				"(//div[@class='wdFooter']//a[contains(@class,'legal') and contains(text(),'Do Not Sell My Personal Information')])[2]|(//div[@class='tlFooter']//a[contains(@class,'legal') and contains(text(),'Do Not Sell')])[1]");
		wyndhamDestinationsBrandImage = By
				.xpath("(//div[contains(text(),'Wyndham Destinations')])[2]/../../../..//img");
		wyndhamDestinationsTitle = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Wyndham Destinations')]");
		wyndhamDestinationsBody = By
				.xpath("(//div[contains(text(),'Wyndham Destinations')])[2]/../../../..//div[contains(@class,'body')]");
		btnLearnMoreWyndhamDestinations = By
				.xpath("(//div[contains(text(),'Wyndham Destinations')])[2]/../../../..//a");

		panoramaBrandImage = By.xpath("(//div[contains(text(),'Panorama')])[2]/../../../..//img");
		panoramaTitle = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Panorama')]");
		panoramaBody = By.xpath("(//div[contains(text(),'Panorama')])[2]/../../../..//div[contains(@class,'body')]");
		btnLearnMorepanorama = By.xpath("(//div[contains(text(),'Panorama')])[2]/../../../..//a");

		travelPlusLeisureBrandImage = By
				.xpath("(//div[contains(text(),'Travel + Leisure Group')])[2]/../../../..//img");
		travelPlusLeisureTitle = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Travel + Leisure Group')]");
		travelPlusLeisureBody = By
				.xpath("(//div[contains(text(),'Travel + Leisure')])[2]/../../../..//div[contains(@class,'body')]");
		btnLearnMoreTravelPlusLeisure = By.xpath("(//div[contains(text(),'Travel + Leisure')])[2]/../../../..//a");

		footerCopyRight = By.xpath(
				"//div[contains(@class,'footerCopyright')]//div[contains(text(),'© Travel + Leisure Co. 2021. All Rights Reserved.')]|//div[contains(@class,'footerCopyright')]//div[contains(text(),'© Travel + Leisure Co. 2021. <i>All Rights Reserved.')]");
		termsOfUse = By.xpath("//ul[@class='menu align-center']//a[@data-eventlabel='Terms of Use']");
		privacySettings = By.xpath("//ul[@class='menu align-center']//a[@data-eventlabel='Privacy Settings']");
		supportTimeshare = By
				.xpath("//ul[@class='menu align-center']//a[@data-eventlabel='Proudly Supports Timeshare.com']");
		privacyNotice = By.xpath("//ul[@class='menu align-center']//a[@data-eventlabel='Privacy Notice']");

		ourCompany = By.xpath("(//li[contains(@class,'submenu-parent')]//a/span[contains(.,'Our Company')])[2]/..");
		ourBrands = By.xpath("(//li[contains(@class,'submenu-parent')]//a/span[contains(.,'Our Brand')])[2]/..");
		careers = By.xpath("//li[contains(@class,'-border')]//a/span[contains(.,'Careers')]/parent::a");
		investorRelations = By.xpath("//li[contains(@class,'-border')]//a/span[contains(.,'Investor')]/parent::a");
		socialResponsibilities = By
				.xpath("(//li[contains(@class,'submenu-parent')]//a/span[contains(.,'Social Res')])[2]/..");
		news = By.xpath("//li[contains(@class,'-border')]//a/span[contains(.,'News')]/..");
		travelPlusLeisureLogo = By
				.xpath("(//a[img[contains(@class,'logo')] and parent::div[contains(@class,'cell')]])[2]");
		careerFooter = By.xpath("//ul[contains(@class,'emptyAccordion')]/li/a[contains(.,'Careers')]");
		contactUsFooter = By.xpath("//ul[contains(@class,'emptyAccordion')]/li/a[contains(.,'Contact Us')]");
		investorsFooter = By.xpath("//ul[contains(@class,'emptyAccordion')]/li/a[contains(.,'Investors')]");
		twitterIcon = By.xpath(
				"(//ul[contains(@class,'menu align-center')]//li/a[contains(@href,'https://twitter.com/tnlconews')])[2]");
		linkedInIcon = By.xpath(
				"(//ul[contains(@class,'menu align-center')]//li/a[contains(@href,'https://www.linkedin.com/company/travelleisureco')])[2]");
		footerWyndhamLogo = By
				.xpath("//div[contains(@class,'footerFooter')]//a//img[contains(@class,'float-center')]/..");
		doNotsellInfo = By.xpath(
				"//div[contains(@class,'align-center text-center')]//a[contains(text(),'Do Not Sell My Personal Information')]");
	}

	protected By linkedInLabel = By.xpath("//h1[@class='top-card-layout__title' and contains(.,'Travel')]");
	protected By hamburgerCTA = By
			.xpath("(//div[contains(@class,'cell small-6 ')]//div//..//div[contains(@id,'nav')]/span)[1]");
	protected By closeHamburgerCTA = By
			.xpath("(//div[contains(@class,'cell small-6 ')]//div//..//div[contains(@id,'nav')]/span)[1]");

	@Override
	public void validateLogo() {
		assertTrue(verifyObjectDisplayed(travelPlusLeisureLogo), "Header TravelPlusLeisure logo is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLogo", Status.PASS,
				"Header TravelPlusLeisure logo is displayed");

		assertTrue(getElementAttribute(travelPlusLeisureLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Header TravelPlusLeisure logo url not correct");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLogo", Status.PASS,
				"Header TravelPlusLeisure logo is present");
	}

	@Override
	public void validateHomeHeaderSubMenu(String headerMenuItem, String subMenu, int count) {

		if (count == 0) {
			clickElementBy(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem + "']]]"));
			pageCheck();

			assertTrue(
					verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[@data-eventlabel='" + subMenu + "']")),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();

		} else {
			clickElementBy(hamburgerCTA);
			waitForSometime("1");
			clickElementBy(By
					.xpath("//li[contains(@role,'treeitem')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
							+ headerMenuItem + "')]/parent::a/following-sibling::button"));
			waitForSometime("1");

			assertTrue(
					getList(By
							.xpath("//div[contains(@class,'genHeaderParent')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
									+ headerMenuItem + "')]/../..//ul/li")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By
									.xpath("//div[contains(@class,'genHeaderParent')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
											+ headerMenuItem + "')]/../..//ul/li")).size());

			assertTrue(
					verifyObjectDisplayed(
							By.xpath("//li[contains(@role,'treeitem')]//a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			clickElementJSWithWait(By.xpath("//li[contains(@role,'treeitem')]//a[contains(.,'" + subMenu + "')]"));

			// pageCheck();
			if (subMenu.equalsIgnoreCase("Board of Directors")) {
				subMenu = "travelandleisureco";
			} else if (subMenu.equalsIgnoreCase("Wyndham Destinations")) {
				subMenu = "wyndhamdestinations";
			} else if (subMenu.equalsIgnoreCase("Panorama")) {
				subMenu = "panoramaco";
			} else if (subMenu.equalsIgnoreCase("Mission & Values")) {
				subMenu = "Mission & Values";
			}
			String title = driver.getTitle();
			if (driver.getCurrentUrl().trim().contains("travelandleisureco.com/us/en")) {
				if (title.contains(subMenu))
					tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
							subMenu + " navigations successful");

				navigateBack();
				// pageCheck();

			} else {

				String getUrl = driver.getCurrentUrl().trim();
				assertTrue(getUrl.contains(subMenu), "Page is not navigated Successfully");
				tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						subMenu + " navigations successful");
				navigateToURL(url);
				driver.get(url);
				// pageCheck();
			}

		}

	}

	@Override
	public void validateHomeHeaderMenu() {
		clickElementBy(hamburgerCTA);

		assertTrue(verifyObjectDisplayed(ourCompany), "Our Company menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Our Company menu item is present");
		navigateValidateAndReturn(ourCompany);
		clickElementBy(hamburgerCTA);

		assertTrue(verifyObjectDisplayed(ourBrands), "OurBrands menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"OurBrands menu item is present");
		navigateValidateAndReturn(ourBrands);
		clickElementBy(hamburgerCTA);

		assertTrue(verifyObjectDisplayed(careers), "Careers menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Careers menu item is present");
		navigateValidateAndReturntoPage(careers, "careers");
		clickElementBy(hamburgerCTA);

		assertTrue(verifyObjectDisplayed(investorRelations), "Investors Relations menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Investors Relations menu item is present");
		navigateValidateAndReturntoPage(investorRelations, "travelandleisureco");
		clickElementBy(hamburgerCTA);

		assertTrue(verifyObjectDisplayed(socialResponsibilities), "Social Responsibility menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Social Responsibility menu item is present");
		navigateValidateAndReturn(socialResponsibilities);
		clickElementBy(hamburgerCTA);

		assertTrue(verifyObjectDisplayed(news), "News menu item is not present");
		tcConfig.updateTestReporter("OurCompanyHomePage", "validateHomeHeaderMenu", Status.PASS,
				"News menu item is present");
		navigateValidateAndReturn(news);
	}

	@Override
	public void validateLatestNews() {

		assertTrue(verifyObjectDisplayed(latestNewsHeader), "Latest News header is absent");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLatestNews", Status.PASS,
				"Latest News header is present");

		List<WebElement> newsList = getList(latestNewsComponent);

		for (int i = 0; i < newsList.size(); i++) {

			getElementInView(latestNewsComponent);
			newsList = getList(latestNewsComponent);

			assertTrue(newsList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Latest News cards link not present");
			String hrefCard = newsList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(newsList.get(i).findElement(By.xpath("./div/a")).isDisplayed(),
					"Latest News pictures link not present");
			String hrefPicture = newsList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Latest News card section link not present");
			String hrefCardSection = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Latest News card");

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.isDisplayed(), "Latest News card section title not present");
			String cardTitle = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateLatestNews", Status.PASS,
					"All links are in sync within the Latest News Card, which is : " + hrefCard);

			WebElement eleReadMore = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Read More']"));
			clickElementBy(eleReadMore);

			pageCheck();
			// getElementInView(breadcrumbLink);
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(hrefCard), "Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLatestNews", Status.PASS,
					"Latest News Title : " + cardTitle + " navigating to link : " + hrefCard + " successful");
			navigateBack();
			pageCheck();

		}
	}

	@Override
	public void validateCardComponent() {

		List<WebElement> cardList = getList(cardComponent);
		for (int i = 0; i < cardList.size(); i++) {
			getElementInView(cardComponent);

			cardList = getList(cardComponent);

			assertTrue(cardList.get(i).findElement(By.xpath("./a")).isDisplayed(), "cards link not present");
			String hrefCard = cardList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(cardList.get(i).findElement(By.xpath("./div/a")).isDisplayed(), "pictures link not present");
			String hrefPicture = cardList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Card section link not present");
			String hrefCardSection = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Cards");

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.isDisplayed(), "card section title not present");
			String cardTitle = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateCardComponent", Status.PASS,
					"All links are in sync within the Card, which is : " + hrefCard);

			WebElement eleLearnMore = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Learn More']"));
			clickElementJSWithWait(eleLearnMore);
			pageCheck();

			if (cardTitle.equalsIgnoreCase("Our Investors")) {
				cardTitle = "investor.travelandleisureco";
			} else if (cardTitle.equalsIgnoreCase("Careers")) {
				cardTitle = "careers.wyndhamdestinations.com";
			}

			String title = driver.getCurrentUrl().trim();
			assertTrue(title.toLowerCase().contains(cardTitle.toLowerCase()),
					"Link Navigation not correct, expected : " + title + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + cardTitle + " navigating to link : " + title + " successful");
			navigateToURL(url);
			driver.get(url);
			pageCheck();

		}
	}

	@Override
	public void validatePromotingSustainableTravel() {

		getElementInView(promoSustainableTravelTitle);

		assertTrue(verifyObjectDisplayed(promoSustainableTravelImage), "Promoting Sustainable Travel image is absent");
		assertTrue(verifyObjectDisplayed(promoSustainableTravelTitle), "Promoting Sustainable Travel title is absent");
		assertTrue(getElementText(promoSustainableTravelTitle).trim().length() > 0,
				"Promoting Sustainable Travel title is blank");
		String title = getElementText(promoSustainableTravelTitle).trim();

		assertTrue(verifyObjectDisplayed(promoSustainableTravelBody), "Promoting Sustainable Travel body is absent");
		assertTrue(getElementText(promoSustainableTravelBody).trim().length() > 0,
				"Promoting Sustainable Travel body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorePromoSustainableTravel),
				"Promoting Sustainable Travel Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePromotingSustainableTravel", Status.PASS,
				"Promoting Sustainable Travel content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorePromoSustainableTravel, "href");
		clickElementBy(btnLearnMorePromoSustainableTravel);
		pageCheck();
		assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Navigation to learn more link not correct");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePromotingSustainableTravel", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	@Override
	public void validateFooterMenu() {

		getElementInView(investorsFooter);
		assertTrue(verifyObjectDisplayed(investorsFooter), "Investors menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Investors menu item is present");
		navigateValidateAndReturntoPage(investorsFooter, "travelandleisureco");
		getElementInView(careerFooter);
		assertTrue(verifyObjectDisplayed(careerFooter), "Career menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Career menu item is present");
		navigateValidateAndReturntoPage(careerFooter, "careers");
		getElementInView(contactUsFooter);
		assertTrue(verifyObjectDisplayed(contactUsFooter), "Contact US menu item is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateFooterMenu", Status.PASS,
				"Contact US menu item is present");
		navigateValidateAndReturn(contactUsFooter);

	}

	@Override
	public void navigateValidateAndReturntoPage(By link, String menu) {

		String href = getElementAttribute(link, "href").trim();
		clickElementBy(link);
		// pageCheck();
		waitForSometime("5");
		String title = driver.getCurrentUrl().trim();
		assertTrue(title.contains(menu), "Link Navigation not correct, expected : " + href + " | Actual : " + title);
		tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
				"Page: " + title + " navigating to link : " + href + " successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href");
		String title = getElementAttribute(link, "data-eventlabel");
		clickElementBy(link);
		pageCheck();
		assertTrue(getCurrentURL().trim().equalsIgnoreCase(href),
				"Link Navigation not correct, expected : " + href + " | Actual : " + getCurrentURL().trim());
		tcConfig.updateTestReporter("TimeshareHomePage", "navigateValidateAndReturn", Status.PASS,
				"Latest News Title : " + title + " navigating to link : " + href + " successful");

		navigateBack();
		pageCheck();

	}

	@Override
	public void validateTwitterLink() {

		getElementInView(twitterIcon);
		assertTrue(verifyObjectDisplayed(twitterIcon), "Twitter icon is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTwitterLink", Status.PASS,
				"Twitter icon is present");

		clickElementBy(twitterIcon);
		waitForSometime("3");
		assertTrue(getCurrentURL().trim().equalsIgnoreCase("https://mobile.twitter.com/tnlconews"),
				"Twitter link navigation not successful");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateTwitterLink", Status.PASS,
				"Twitter link navigation successful");
		// navigateToMainTab(tabs2);
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validateLinkedInLink() {

		getElementInView(linkedInIcon);
		assertTrue(verifyObjectDisplayed(linkedInIcon), "LinkedIn icon is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLinkedInLink", Status.PASS,
				"LinkedIn icon is present");
		clickElementBy(linkedInIcon);
		waitForSometime("3");
		System.out.println(getCurrentURL());
		assertTrue(getCurrentURL().trim().contains("https://www.linkedin.com"),
				"LinkedIn link navigation not successful");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLinkedInLink", Status.PASS,
				"LinkedIn link navigation successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validatefooterlogo() {

		String url = testData.get("URL");
		getElementInView(footerWyndhamLogo);
		assertTrue(verifyObjectDisplayed(footerWyndhamLogo), "Footer Wyndham logo is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatefooterlogo", Status.PASS,
				"Footer Wyndham logo is present");

		assertTrue(getElementAttribute(footerWyndhamLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Footer url not correct");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatefooterlogo", Status.PASS,
				"Footer Wyndham logo is present");

	}

	@Override
	public void validateDoNotSellInfo() {
		getElementInView(doNotsellInfo);
		assertTrue(verifyObjectDisplayed(doNotsellInfo), "Do not sell info link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateDoNotSellInfo", Status.PASS,
				"Do not sell info link is present");
		clickElementBy(doNotsellInfo);
		waitForSometime("3");
		getElementInView(optOutForm);
		assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateDoNotSellInfo", Status.PASS,
				"Opt out form is displayed");
		navigateBack();
		pageCheck();

	}

	@Override
	public void validatefooterCopyRight() {

		assertTrue(verifyObjectDisplayed(footerCopyRight), "Footer CopyRight Label is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatefooterCopyRight", Status.PASS,
				"Footer CopyRight Label is present" + " " + getElementText(footerCopyRight));

	}

	@Override
	public void validateTermsofUse() {

		assertTrue(verifyObjectDisplayed(termsOfUse), "Terms of Use link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTermsofUse", Status.PASS,
				"Terms of Use link is present");
		navigateValidateAndReturn(termsOfUse);

	}

	@Override
	public void validatePrivacyNotice() {

		assertTrue(verifyObjectDisplayed(privacyNotice), "Privacy notice link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePrivacyNotice", Status.PASS,
				"Privacy notice link is present");
		navigateValidateAndReturn(privacyNotice);

	}

	@Override
	public void validatePrivacySettings() {

		assertTrue(verifyObjectDisplayed(privacySettings), "Privacy settings link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePrivacySettings", Status.PASS,
				"Privacy settings link is present");
		navigateValidateAndReturntoPage(privacySettings, "info.evidon");

	}

	@Override
	public void validateSupportTimeshare() {

		assertTrue(verifyObjectDisplayed(supportTimeshare), "Support Timeshare link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateSupportTimeshare", Status.PASS,
				"Support Timeshare link is present");
		navigateValidateAndReturntoPage(supportTimeshare, "timeshare");

	}

	@Override
	public void validateWyndhamDestinationsBrandContent() {

		getElementInView(wyndhamDestinationsBrandImage);

		assertTrue(verifyObjectDisplayed(wyndhamDestinationsBrandImage), "Wyndham Destinations image is absent");
		assertTrue(verifyObjectDisplayed(wyndhamDestinationsTitle), "Wyndham Destinations title is absent");
		assertTrue(getElementText(wyndhamDestinationsTitle).trim().length() > 0, "Wyndham Destinations title is blank");
		String title = getElementText(wyndhamDestinationsTitle).trim();

		assertTrue(verifyObjectDisplayed(wyndhamDestinationsBody), "Wyndham Destinations body is absent");
		assertTrue(getElementText(wyndhamDestinationsBody).trim().length() > 0, "Wyndham Destinations body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreWyndhamDestinations),
				"Wyndham Destinations Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateWyndhamDestinationsBrandContent", Status.PASS,
				"Wyndham Destinations content is displayed Successfully");

		String href = getElementAttribute(btnLearnMoreWyndhamDestinations, "href").trim();
		String browserTitle = getElementAttribute(btnLearnMoreWyndhamDestinations, "data-eventlabel").trim()
				.toLowerCase().replace(" ", "");
		clickElementBy(btnLearnMoreWyndhamDestinations);
		// pageCheck();
		waitForSometime("3");
		String getURL = driver.getCurrentUrl().trim().toLowerCase();
		assertTrue(getURL.equalsIgnoreCase("https://www.wyndhamdestinations.com/"),
				"Navigation to learn more link not correct");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateWyndhamDestinationsBrandContent", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successfull");
		navigateToURL(url);
		driver.get(url);
		pageCheck();
	}

	@Override
	public void validatePanoramacoBrandContent() {

		getElementInView(panoramaTitle);

		assertTrue(verifyObjectDisplayed(panoramaBrandImage), "Panoramaco image is absent");
		assertTrue(verifyObjectDisplayed(panoramaTitle), "Panoramaco title is absent");
		assertTrue(getElementText(panoramaTitle).trim().length() > 0, "Panoramaco title is blank");
		String title = getElementText(panoramaTitle).trim();

		assertTrue(verifyObjectDisplayed(panoramaBody), "Panoramaco body is absent");
		assertTrue(getElementText(panoramaBody).trim().length() > 0, "Panoramaco body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorepanorama), "Panoramaco Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePanoramacoBrandContent", Status.PASS,
				"Panoramaco content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorepanorama, "href").trim();
		String browserTitle = getElementAttribute(btnLearnMorepanorama, "data-eventlabel").trim().toLowerCase();
		clickElementBy(btnLearnMorepanorama);
		// pageCheck();
		waitForSometime("3");
		String getURL = driver.getCurrentUrl().trim().toLowerCase();
		assertTrue(getURL.equalsIgnoreCase("https://www.panoramaco.com/"), "Navigation to learn more link not correct");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatePanoramacoBrandContent", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successfull");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validateTravelPlusLeisureBrandContent() {

		getElementInView(travelPlusLeisureTitle);

		assertTrue(verifyObjectDisplayed(travelPlusLeisureBrandImage), "Travel Plus Leisure Group image is absent");
		assertTrue(verifyObjectDisplayed(travelPlusLeisureTitle), "Travel Plus Leisure Group title is absent");
		assertTrue(getElementText(travelPlusLeisureTitle).trim().length() > 0,
				"Travel Plus Leisure Group title is blank");
		String title = getElementText(travelPlusLeisureTitle).trim();

		assertTrue(verifyObjectDisplayed(travelPlusLeisureBody), "Travel Plus Leisure Group body is absent");
		assertTrue(getElementText(travelPlusLeisureBody).trim().length() > 0,
				"Travel Plus Leisure Group body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreTravelPlusLeisure),
				"Travel Plus Leisure Group Learn more button is absent");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTravelPlusLeisureBrandContent", Status.PASS,
				"Travel Plus Leisure Group content is displayed Successfully");

		String href = getElementAttribute(btnLearnMoreTravelPlusLeisure, "href").trim();
		clickElementBy(btnLearnMoreTravelPlusLeisure);
		// pageCheck();
		waitForSometime("3");
		assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Navigation to learn more link not correct");

		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateTravelPlusLeisureBrandContent", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();

	}

}
