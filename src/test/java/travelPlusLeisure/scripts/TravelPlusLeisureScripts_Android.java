package travelPlusLeisure.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import travelPlusLeisure.pages.TravelPlusLeisureHomePage;
import travelPlusLeisure.pages.TravelPlusLeisureHomePage_Android;
import travelPlusLeisure.pages.TravelPlusLeisureHomePage_Web;

public class TravelPlusLeisureScripts_Android extends TestBase {
	public static final Logger log = Logger.getLogger(TravelPlusLeisureScripts_Android.class);

	@Test(dataProvider = "testData", groups = { "header", "regression", "" })

	public void tc01_TravelPlusLeisure_Header_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			 TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Android(tcconfig);
	            homePage.launchApplication();
	            homePage.validateLogo();
	            homePage.validateHomeHeaderMenu();	 

	            homePage.validateHomeHeaderSubMenu("Our Company", "Mission & Values", 4);
	            homePage.validateHomeHeaderSubMenu("Our Company", "Board of Directors", 4);
	            homePage.validateHomeHeaderSubMenu("Our Company", "Senior Leadership", 4);
	            homePage.validateHomeHeaderSubMenu("Our Company", "Suppliers", 4);	 

	            homePage.validateHomeHeaderSubMenu("Our Brands", "Wyndham Destinations", 3);
	            homePage.validateHomeHeaderSubMenu("Our Brands", "Panorama", 3);
	            homePage.validateHomeHeaderSubMenu("Our Brands", "Travel + Leisure Group", 3);	 

	            homePage.validateHomeHeaderSubMenu("Social Responsibility", "Overview and Performance", 5);
	            homePage.validateHomeHeaderSubMenu("Social Responsibility", "Inclusion and Diversity", 5);
	            homePage.validateHomeHeaderSubMenu("Social Responsibility", "Environmental Sustainability", 5);
	            homePage.validateHomeHeaderSubMenu("Social Responsibility", "Philanthropy", 5);
	            homePage.validateHomeHeaderSubMenu("Social Responsibility", "Ethics and Human Rights", 5);	 

	            homePage.validateHomeHeaderSubMenu("News", "Press Releases", 3);
	            homePage.validateHomeHeaderSubMenu("News", "Awards", 3);
	            homePage.validateHomeHeaderSubMenu("News", "Media Contacts", 3);	 

	            homePage.validateBanners();
	            homePage.validateTicker();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "CareerInvestor", "regression", "demo" })

	public void tc02_TravelPlusLeisure_Career_Investor_validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateCardComponent();
			homePage.validatePromotingSustainableTravel();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "latestNews", "regression", "" })

	public void tc03_TravelPlusLeisure_Latest_News(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateLatestNews();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "footer", "regression", "" })

	public void tc04_TravelPlusLeisure_Validate_Footer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Android(tcconfig);
			homePage.launchApplication();
			//homePage.validateFooterMenu();
			homePage.validateTwitterLink();
			homePage.validateLinkedInLink();
			homePage.validatefooterlogo();
			homePage.validateDoNotSellInfo();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "Brand", "regression", "" })

	public void tc06_TravelPlusLeisure_Brand_Content_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validateWyndhamDestinationsBrandContent();
			homePage.validatePanoramacoBrandContent();
			homePage.validateTravelPlusLeisureBrandContent();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "LegalLinks", "regression", "" })

	public void tc07_TravelPlusLeisure_LegalLinks_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Android(tcconfig);
			homePage.launchApplication();
			homePage.validatefooterCopyRight();
			homePage.validateTermsofUse();
			homePage.validatePrivacyNotice();
			homePage.validatePrivacySettings();
			homePage.validateSupportTimeshare();
			

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}