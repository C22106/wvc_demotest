package travelPlusLeisure.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import travelPlusLeisure.pages.TravelPlusLeisureHomePage;
import travelPlusLeisure.pages.TravelPlusLeisureHomePage_Web;

public class TravelPlusLeisureScripts extends TestBase {
	public static final Logger log = Logger.getLogger(TravelPlusLeisureScripts.class);

	@Test(dataProvider = "testData", groups = { "header", "regression", "" })

	public void tc01_TravelPlusLeisure_Header_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateLogo();
			homePage.validateHomeHeaderMenu();

			homePage.validateHomeHeaderSubMenu("Our Company", "Mission & Values", 4);
			homePage.validateHomeHeaderSubMenu("Our Company", "Board of Directors", 4);
			homePage.validateHomeHeaderSubMenu("Our Company", "Senior Leadership Team", 4);
			homePage.validateHomeHeaderSubMenu("Our Company", "Suppliers", 4);

			homePage.validateHomeHeaderSubMenu("Our Brands", "Wyndham Destinations", 3);
			homePage.validateHomeHeaderSubMenu("Our Brands", "Panorama", 3);
			homePage.validateHomeHeaderSubMenu("Our Brands", "Travel + Leisure Group", 3);

			homePage.validateHomeHeaderSubMenu("Social Responsibility", "Overview and Performance", 5);
			homePage.validateHomeHeaderSubMenu("Social Responsibility", "Inclusion and Diversity", 5);
			homePage.validateHomeHeaderSubMenu("Social Responsibility", "Environmental Sustainability", 5);
			homePage.validateHomeHeaderSubMenu("Social Responsibility", "Philanthropy", 5);
			homePage.validateHomeHeaderSubMenu("Social Responsibility", "Ethics and Human Rights", 5);

			homePage.validateHomeHeaderSubMenu("News", "Press Releases", 3);
			homePage.validateHomeHeaderSubMenu("News", "Our Awards", 3);
			homePage.validateHomeHeaderSubMenu("News", "Media Contacts", 3);

			homePage.validateBanners();
			homePage.validateTicker();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "CareerInvestor", "regression", "CB" })

	public void tc02_TravelPlusLeisure_Career_Investor_validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateCardComponent();
			homePage.validatePromotingSustainableTravel();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "latestNews", "regression", "CB" })

	public void tc03_TravelPlusLeisure_Latest_News(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateLatestNews();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "footer", "regression", "wip" })

	public void tc04_TravelPlusLeisure_Validate_Footer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateFooterMenu();

			homePage.validateHomeFooterSubMenu("Our Company", "Mission & Values", 4);
			homePage.validateHomeFooterSubMenu("Our Company", "Board of Directors", 4);
			homePage.validateHomeFooterSubMenu("Our Company", "Senior Leadership Team", 4);
			homePage.validateHomeFooterSubMenu("Our Company", "Suppliers", 4);

			homePage.validateHomeFooterSubMenu("Our Brands", "Wyndham Destinations", 3);
			homePage.validateHomeFooterSubMenu("Our Brands", "Panorama", 3);
			homePage.validateHomeFooterSubMenu("Our Brands", "Travel + Leisure Group", 3);

			homePage.validateHomeFooterSubMenu("Social Responsibility", "Overview and Performance", 5);
			homePage.validateHomeFooterSubMenu("Social Responsibility", "Inclusion and Diversity", 5);
			homePage.validateHomeFooterSubMenu("Social Responsibility", "Environmental Sustainability", 5);
			homePage.validateHomeFooterSubMenu("Social Responsibility", "Philanthropy", 5);
			homePage.validateHomeFooterSubMenu("Social Responsibility", "Ethics and Human Rights", 5);

			homePage.validateHomeFooterSubMenu("News", "Press Releases", 4);
			homePage.validateHomeFooterSubMenu("News", "Our Awards", 4);
			homePage.validateHomeFooterSubMenu("News", "Media Contacts", 4);
			homePage.validateHomeFooterSubMenu("News", "Travel + Leisure Co. Overview", 4);

			homePage.validateTwitterLink();
			homePage.validateLinkedInLink();
			homePage.validateFacebookLink();

			homePage.validatefooterlogo();
			homePage.validateDoNotSellInfo();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "demo", "regression", "CB" })

	public void tc05_TravelPlusLeisure_Search_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateSearchFunctionality();
			homePage.searchResultsValidatins();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "Brand", "regression", "CB" })

	public void tc06_TravelPlusLeisure_Brand_Content_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateWyndhamDestinationsBrandContent();
			homePage.validatePanoramacoBrandContent();
			homePage.validateTravelPlusLeisureBrandContent();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "LegalLinks", "regression", "CB" })

	public void tc07_TravelPlusLeisure_LegalLinks_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			/*homePage.validateTermsofUse();
			homePage.validatePrivacyNotice();*/
			homePage.validatePrivacySettings();
			homePage.validateSupportTimeshare();
			homePage.validatefooterCopyRight();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "FooterBrandLogo", "regression", "CB" })

	public void tc08_TravelPlusLeisure_BrandLogo_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateBrandLogo();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData", groups = { "brokenlink", "brokenlink" })

	public void tc09_TravelPlusLeisure_Broken_Link_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			TravelPlusLeisureHomePage homePage = new TravelPlusLeisureHomePage_Web(tcconfig);
			homePage.launchApplicationforBrokenLinkValidation();
			homePage.brokenLinkValidation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}