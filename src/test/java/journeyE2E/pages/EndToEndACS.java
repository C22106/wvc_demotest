package journeyE2E.pages;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class EndToEndACS extends EndToEndBasePage {

	public static final Logger log = Logger.getLogger(EndToEndACS.class);
	ReadConfigFile config = new ReadConfigFile();

	public EndToEndACS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String scoreBand;
	public static String scoreType;
	public static String contractNum;
	private By username = By.xpath("//input[@id='fldUsername']");
	private By password = By.xpath("//input[@id='fldPassword']");
	private By loginButton = By.id("login-btn");
	private By logOut = By.xpath("//a[text()='Logout']");
	private By logoutConf = By.xpath("//div[@class='msg inform' and contains(text(),'You have been logged off. ')]");
	private By roleSelectionTab = By.xpath("//li[contains(text(),'Role Selection')]");
	private By serviceEntityDrpDD = By.xpath("//select[@name='serviceEntity']");
	private By nextBtn = By.xpath("//button[@id='next-btn']");
	private By tourIdTxt = By.xpath("//input[@name='id']");
	private By retCustBtn = By.xpath("//button[@id='getcustomer-btn']");
	private By reviewAndScore = By.xpath("(//td[@class='center'])[1]");
	private By ssn1 = By.xpath("//input[@name='ssn1']");
	private By ssn2 = By.xpath("//input[@name='ssn2']");
	private By ssn3 = By.xpath("//input[@name='ssn3']");
	private By fName = By.xpath("//input[@name='firstname']");
	private By lName = By.xpath("//input[@name='lastname']");
	private By dobMonth = By.xpath("//select[@name='dob_month']");
	private By dobDay = By.xpath("//select[@name='dob_day']");
	private By dobYear = By.xpath("//select[@name='dob_year']");
	private By gender = By.xpath("//select[@name='gender']");
	private By streetnum = By.xpath("//input[@name='address_no']");
	private By streettype = By.xpath("//select[@name='address_type']");
	private By streetname = By.xpath("//input[@name='address_name']");
	private By city = By.xpath("//input[@name='address_city']");
	private By state = By.xpath("//select[@name='address_state']");
	private By postcode = By.xpath("//input[@name='address_postal_code']");
	private By country = By.xpath("//select[@name='address_country']");
	private By homephone = By.xpath("//input[@name='home_phone']");
	private By annualIncome = By.xpath("//input[@name='totalIncome']");
	private By rentRequired = By.xpath("//input[@name='rent']");
	private By getScoreBtn = By.xpath("//button[@name='scoreBtn']");
	private By scoreband = By.xpath("//div[@id='credit_band']");
	private By scoretype = By.xpath("//div//strong[text()='Score Type']//..");

	private void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, 10);
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("ACS_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	private void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("ACS_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	public void launchACS(String strBrowser) {

		// this.driver = checkAndInitBrowser(strBrowser);
		String url = config.get("ACS_URL").trim();
		driver.navigate().to(url);
		driver.manage().window().maximize();

	}

	public void loginToACS() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("ACS_UserID");
		String password = tcConfig.getConfig().get("ACS_Password");
		setUserName(userid);
		setPassword(password);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("ACS_LoginPage", "loginToACS", Status.PASS, "Log in button clicked");

	}

	public void logoutFromACS() throws Exception {

		try {

			waitUntilElementVisibleBy(driver, logOut, 120);
			driver.findElement(logOut).click();

			waitUntilElementVisibleBy(driver, logoutConf, 10);
			if (verifyObjectDisplayed(logoutConf)) {
				tcConfig.updateTestReporter("ACSLoginPage", "ACSLogout", Status.PASS, "Log out Successful");
			} else {
				tcConfig.updateTestReporter("ACSLoginPage", "ACSLogout", Status.FAIL, "Log Off unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("ACSLoginPage", "ACSLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	public void homePageValidationACSapp() {
		waitUntilElementVisibleBy(driver, roleSelectionTab, 120);

		tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Homepage Navigated");

		if (verifyObjectDisplayed(roleSelectionTab)) {
			clickElementBy(roleSelectionTab);
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Leads Tab clicked");
		} else {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Leads Tab not present");
		}

	}

	public void ScoringFromACS() {
		// waitUntilElementVisible(driver, homeTab, 120);
		clickElementBy(serviceEntityDrpDD);
		new Select(driver.findElement(serviceEntityDrpDD)).selectByVisibleText(testData.get("ServiceEntityname"));
		clickElementBy(nextBtn);
		waitUntilElementVisibleBy(driver, tourIdTxt, 120);
		// String createdTourid= "55011048";
		String createdTourid = EndToEndSalesForce.TourID;
		sendKeysBy(tourIdTxt, createdTourid);
		clickElementBy(retCustBtn);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(reviewAndScore)) {
			tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
					"User is navigated to Role Selection Page");
			clickElementBy(reviewAndScore);

			tcConfig.updateTestReporter("RoleSelectionPage", "RoleSelection", Status.PASS,
					"review And Score button clicked");

		} else {
			tcConfig.updateTestReporter("RoleSelectionPage", "Role Selection", Status.PASS,
					"User is navigated to Role Selection Page");
		}

		waitUntilElementVisibleBy(driver, getScoreBtn, 120);
		fieldDataEnter(ssn1, testData.get("SSSN1"));
		fieldDataEnter(ssn2, testData.get("SSSN2"));
		fieldDataEnter(ssn3, testData.get("SSSN3"));
		fieldDataEnter(fName, testData.get("FirstName"));
		fieldDataEnter(lName, testData.get("LastName"));
		clickElementBy(dobMonth);
		new Select(driver.findElement(dobMonth)).selectByVisibleText(testData.get("Month"));
		clickElementBy(dobDay);
		new Select(driver.findElement(dobDay)).selectByVisibleText(testData.get("Day"));
		clickElementBy(dobYear);
		new Select(driver.findElement(dobYear)).selectByVisibleText(testData.get("Year"));
		clickElementBy(gender);
		new Select(driver.findElement(gender)).selectByVisibleText(testData.get("Gender"));
		fieldDataEnter(streetnum, testData.get("StreetNum"));
		fieldDataEnter(streetname, testData.get("StreetName"));
		clickElementBy(streettype);
		new Select(driver.findElement(streettype)).selectByVisibleText(testData.get("StreetType"));
		fieldDataEnter(city, testData.get("City"));
		clickElementBy(state);
		new Select(driver.findElement(state)).selectByVisibleText(testData.get("State").toUpperCase());
		fieldDataEnter(postcode, testData.get("ZIP"));
		clickElementBy(country);
		new Select(driver.findElement(country)).selectByVisibleText(testData.get("Country"));
		fieldDataEnter(homephone, testData.get("HomePhone"));
		fieldDataEnter(annualIncome, testData.get("AnnualIncome"));
		fieldDataEnter(rentRequired, testData.get("RentRequired"));

		// waitForSometime(tcConfig.getConfig().get("MedWait"));

		String winHandleBefore = driver.getWindowHandle();

		clickElementBy(getScoreBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			// Switch to new window opened
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			driver.findElement(By.xpath("//input[@id='reviewBtn']")).click();
			driver.switchTo().window(winHandleBefore);

		} catch (Exception e) {
			log.info(e);
		}

		scoreBand = driver.findElement(scoreband).getText().trim();
		scoreType = driver.findElement(scoretype).getText().trim();
		tcConfig.updateTestReporter("ScoreCreationPage", "ScoreCreation", Status.PASS, "Score Generated" + scoreBand);
	}

}
