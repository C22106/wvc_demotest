package journeyE2E.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class EndToEndSalePoint extends EndToEndBasePage {

	public static String createdPitchID;
	public static String customerLastName;
	public String selectedCustName;

	public static final Logger log = Logger.getLogger(EndToEndSalePoint.class);
	ReadConfigFile config = new ReadConfigFile();

	public EndToEndSalePoint(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(name = "j_username")
	WebElement textUsername;
	@FindBy(xpath = "//form[@name = 'contractsListOwners']")
	WebElement chcekOwner;

	/*
	 * private void setUserNameSP(String strUserName) throws Exception {
	 * waitUntilElementVisible(driver, textUsername, 120);
	 * textUsername.sendKeys(strUserName);
	 * tcConfig.updateTestReporter("SalePointLogin", "setUserName", Status.PASS,
	 * "Username is entered for SalePoint Login"); }
	 */

	@FindBy(name = "j_password")
	WebElement textPassword;

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	/*
	 * private void setPasswordSP(String strPassword) throws Exception {
	 * 
	 * byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
	 * String dString = new String(decodedString, "UTF-8");
	 * textPassword.sendKeys(dString); tcConfig.updateTestReporter("SalePointLogin",
	 * "setPassword", Status.PASS, "Entered password for SalePoint login"); }
	 */

	@FindBy(xpath = "//input[@name='logon']")
	WebElement buttonLogIn;
	// @FindBy(xpath = "//table[@class='client_table']/tbody/tr[2]/td")
	// WebElement buildVersion;
	/*
	 * public void loginToSP(String strBrowser) throws Exception {
	 * 
	 * this.driver=checkAndInitBrowser(strBrowser);
	 * 
	 * driver.navigate().to(testData.get("URL_SALE"));
	 * driver.manage().window().maximize();
	 * waitUntilElementVisible(driver,buildVersion,120); if
	 * (verifyElementDisplayed(buildVersion)){
	 * tcConfig.updateTestReporter("SalePointLogin", "loginToSP", Status.PASS,
	 * "Salepoint launched-Build version:" + buildVersion.getText().substring(53) );
	 * }else { tcConfig.updateTestReporter("SalePointLogin", "loginToSP",
	 * Status.FAIL, "Salepoint buildversion text not displayed"); }
	 * waitUntilElementVisible(driver, textUsername, 120);
	 * setUserNameSP(testData.get("SP_UserID"));
	 * setPasswordSP(testData.get("SP_Password"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); JavascriptExecutor
	 * executor = (JavascriptExecutor) driver;
	 * executor.executeScript("arguments[0].click();", buttonLogIn);
	 * 
	 * // buttonLogIn.click(); tcConfig.updateTestReporter("SalePointLogin",
	 * "loginToSP", Status.PASS, "Log in button clicked"); //
	 * waitUntilElementVisible(tcConfig.getDriver(), linkMangInv, 10000);
	 * 
	 * }
	 */
	private By buildVersion = By.xpath("//table[@class='client_table']");
	private By username = By.xpath("//input[@id='fldUsername']");
	private By password = By.xpath("//input[@id='fldPassword']");
	private By loginButton = By.name("logon");
	private By logOut = By.xpath("//a[text()='Logout']");
	private By logoutConf = By.xpath("//div[@class='msg inform' and contains(text(),'You have been logged off. ')]");

	// ***************************
	protected By printOption = By.xpath("//select[@id = 'justification']");
	private By locationSelect = By.xpath("//select[@name='location']");// name
																		// location
	private By saveButton = By.xpath("//input[@value='Save']");// input value
																// Save
	private By mainMenButton = By.xpath("//input[@value='Main Menu']");// input
																		// value
																		// Main
																		// Menu
	private By serviceEntityChange = By.xpath("//select[@name='serviceEntity']");// select
																					// name
																					// serviceEntity
																					// valuew
																					// 000
																					// 001

	// 002

	By companySelect = By.xpath("//SELECT[@id='companyList']");
	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;
	// socialSecurityNumber
	@FindBy(xpath = "//input[@name='salesman']")
	WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(xpath = "//input[@value='Save and Print']")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	WebElement conNo;
	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	String strContractNo = "";
	String strMemberNo = "";

	@FindBy(id = "customerNumber")
	WebElement customerNo;

	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	@FindBy(xpath = "//td[@class = 'button_area']/button[text() = 'NEXT']")
	WebElement nextBtn1;

	@FindBy(xpath = "//input[@value='Update']")
	WebElement updateVal;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	WebElement saleType;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	WebElement udiSelect;
	@FindBy(xpath = "//input[@name='sales_types' and @value='Discovery']")
	WebElement DiscoverySelect;

	@FindBy(id = "Print")
	WebElement print;

	@FindBy(id = "newPoints")
	WebElement newPoints;

	@FindBy(xpath = "//input[@value='Calculate']")
	WebElement calculate;

	By nextClick = By.xpath("//input[@value='Next']");

	By customerNoInput = By.id("customerNumber");

	By saleTypeSelect = By.xpath("//input[@name='fvl_saleType' and @value='M']");

	By deleteCheck = By.xpath("//a[contains(.,'Delete')]");

	By udiClick = By.xpath("//input[@name='sales_types' and @value='UDI']");

	By newPointsCheck = By.id("newPoints");

	By calculateClick = By.xpath("//input[@value='Calculate']");

	By salePersonSelect = By.xpath("//input[@name='salesman']");

	By saveClick = By.xpath("//input[@value='Save and Print']");

	By printClick = By.id("Print");

	By memberNoCheck = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");

	By ssnSelect = By.xpath("//input[@name='ssn']");

	By genderSelect = By.xpath("//input[@name='sex' and @value='M']");

	By cashSelect = By.xpath("//td[contains(.,'Cash Sale')]/input[@type='radio']");
	By ccSelect = By.xpath("//td[contains(.,'CC Auto')]/input[@type='radio']");

	By achSelect = By.xpath("//td[contains(.,'ACH Auto Pay')]/input[@type='radio']");

	By headerContractProcessOwner = By.xpath("//div[contains(text(),'Contract Processing - Owners')]");

	By headerCustInfo = By.xpath("//div[contains(text(),'Contract Processing - Customer Information')]");

	By editPrimaryOwner = By.xpath("//div[contains(text(),'Edit Primary Owner')]");
	By editPOwner = By.xpath("//input[@id='primaryOwner']");
	By fairshare = By.xpath("//input[@id='fairshare']");

	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By StatusOwner = By.xpath("//a[contains(.,'Status')]");

	By MonthFromDrpDwn = By.xpath("//select[@name='dobmonth']");
	By DateFromDrpDwn = By.xpath("//select[@name='dobday']");
	By YearFromDrpDwn = By.xpath("//select[@name='dobyear']");

	By primaryOwnerEdit = By.xpath("//input[@class='button' and @id='primaryOwner']");

	By PaymentEdit = By.xpath("//input[@class='button' and @id='loan']");

	By updateButton = By.xpath("//input[@class='button' and @value='Update']");

	By home_Phone = By.xpath("//input[@class='textBox' and @name='home_phone']");

	@FindBy(xpath = "//input[@class='textBox' and @name='home_phone']")
	WebElement home_Phone_fld;

	By inventory_Site = By.xpath("//select[@id='inventorySite']");
	By inventory_phase = By.xpath("//select[@id='inventoryPhase']");

	@FindBy(xpath = "//input[contains(@name,'cc_payment_member_name')]")
	WebElement ccName;

	@FindBy(xpath = "//input[contains(@name,'cc_payment_account_number')]")
	WebElement ccNumber;

	By ccType = By.xpath("//select[contains(@name,'cc_payment_type')]");

	By ccMonth = By.xpath("//select[contains(@name,'cc_payment_exp_month')]");

	By ccYear = By.xpath("//select[contains(@name,'cc_payment_exp_year')]");

	By deleteListOfOwner = By.xpath("(//a[contains(.,'Delete')])[2]");
	By deleteFirstOwner = By.xpath("(//a[contains(.,'Delete')])[1]");

	By deleteonesecOwner = By.xpath("(//a[contains(.,'Delete')])");

	By statusListOfOwner = By.xpath("(//a[contains(.,'Status')])[1]");

	By relationshipStatus = By.xpath("//select[@name='relationship']");

	By inputMemberNum = By.xpath("//input[contains(@name,'memberNumber')]");

	@FindBy(xpath = "//input[@name='ssn']")
	WebElement ssnField;

	@FindBy(xpath = "//input[@name='ficoBand']")
	WebElement fico;

	// @FindBy(xpath="//td[contains(.,'Primary
	// Owner')]/./tr[@class='evenRow']/td[1][contains(.,*)]")
	// WebElement pName;

	@FindBy(xpath = "(//td[contains(.,'Primary Owner')]//tr[@class='evenRow'])[1]")
	WebElement pName;

	// td[contains(.,'Primary Owner')]//tr[@class='evenRow']")[0]

	By discoveryMember = By.xpath("//input[@name='member_number']");
	By searchBtn = By.xpath("//input[@name='search']");
	By upgradeButton = By.xpath("//input[@value='Process Upgrade']");

	By tradeContract = By.xpath("//input[@name='contract_number']");
	By viewtradeContracts = By.xpath("//input[@value='View Traded Contracts']");
	By processTrade = By.xpath("//input[@value='Process Trades']");

	@FindBy(xpath = "//input[@value='Experience']")
	WebElement experienceClick;

	@FindBy(xpath = "//input[@value='WorldMark']")
	WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	WebElement finalizeButton;

	@FindBy(xpath = "//input[@value='Void']")
	WebElement voidButton;

	@FindBy(xpath = "//a[contains(.,'ALL BY DATE')]")
	WebElement selectAllByDate;

	@FindBy(xpath = "//select[@name='startmonth']")
	WebElement startmonth;

	@FindBy(xpath = "//select[@name='startday']")
	WebElement startday;

	@FindBy(xpath = "//input[@name='getData']")
	WebElement getData;

	@FindBy(xpath = "//input[@value='Change Status']")
	WebElement changeStatus;

	@FindBy(xpath = "//input[@value='Transmit']")
	WebElement Transmit;

	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	WebElement returnBack;

	@FindBy(xpath = "//tr/td[contains(.,'T')]")
	WebElement validateStatus;

	By searchFilter = By.xpath("//a[contains(.,'ALL BY DATE')]");

	By statusChangeClick = By.xpath("//input[@value='Change Status']");

	By transmitClick = By.xpath("//input[@value='Transmit']");

	By reportBack = By.xpath("//a[contains(.,'Return to Batch Report')]");

	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	WebElement ReturnReport;

	By StatusVali = By.xpath("//tr/td[contains(.,'T')]");

	By StatusVoid = By.xpath("//tr/td[contains(.,'V')]");

	@FindBy(xpath = "//input[@value='Void']")
	WebElement Void;

	By voidClick = By.xpath("//input[@value='Void']");

	By VoidElementWVR = By.xpath("//td[contains(.,'V')]");

	By experienceSelect = By.xpath("//input[@value='Experience']");

	By summaryPage = By.xpath("//tr[@class='page_title']//div");

	By worldMarkSelect = By.xpath("//input[@value='WorldMark']");

	By nextValue = By.xpath("//input[@value='Next']");

	By MonthFromDrpDwnWBW = By.xpath("//select[@name='beginDateMonth']");
	By DateFromDrpDwnWBW = By.xpath("//select[@name='beginDateDay']");
	By YearFromDrpDwnWBW = By.xpath("//select[@name='beginDateYear']");

	By VoidElement = By.xpath("//td[contains(.,'Void')]");

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtnwbw;

	@FindBy(name = "tourID")
	WebElement tourIDwbw;

	@FindBy(name = "tourTime")
	WebElement tourTimewbw;

	@FindBy(xpath = "//input[@name='next']")
	WebElement nextOwner;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next2;

	By tourIdEnter = By.name("tourID");

	By paymentSelect = By.name("paymentType");

	By salePersonId = By.id("wbwcommissions_salesMan");

	By saveContractClick = By.id("saveContract");

	@FindBy(xpath = "//input[@id='saveContract']")
	WebElement savecontractWBW;

	By editMemberWBW = By.xpath("//tr/td[contains(text(),'Owner Information')]/../td/input[@type='button']");

	By editPaymentWBW = By.xpath("//tr/td[contains(text(),'Finance Information')]/../td/input[@type='button']");

	@FindBy(xpath = "//tr/td[contains(text(),'Finance Information')]/../td/input[@type='button']")
	WebElement editPaymentWBWelmnt;

	@FindBy(xpath = "///tr/td[contains(text(),'Owner Information')]/../td/input[@type='button']")
	WebElement editMemberWBWelmnt;

	By homePhoneSelect = By.xpath("//input[@name='homePhone']");

	@FindBy(xpath = "//input[@name='homePhone']")
	WebElement homePhoneWBW;
	@FindBy(xpath = "//input[@name='home_phone']")
	WebElement homePhoneWVR;

	@FindBy(xpath = "//input[@name='emailAddress']")
	WebElement emailPownerWBW;

	By primaryOwnerPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]");
	By financePage = By.xpath("//tr[@class='page_title']//div[contains(.,'Finance Information')]");
	By wbwPaymentOption = By.xpath("//tr[@class='page_title']//div[contains(.,'WorldMark - Payment Options Screen')]");

	By ownerDetailsPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Owners')]");

	By ssnWBW = By.xpath("//input[@name='socialSecurityNumber']");

	@FindBy(xpath = "//input[@name='socialSecurityNumber']")
	WebElement ssnWBWSelect;
	@FindBy(xpath = "//input[@name='ssn']")
	WebElement ssnWVRSelect;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	WebElement pNameWBW;
	By editPOwnerWBW = By.xpath("//a[contains(.,'Edit')][1]");
	By deleteOwnerWBW = By.xpath("//a[contains(.,'Delete')]");
	By deleteFirstOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[1]");
	By deleteListOfOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[2]");

	By MakePrimaryWBW = By.xpath("//a[contains(.,'Make Primary')]");

	@FindBy(xpath = "//select[contains(@id,'loanPACId')]")
	WebElement contractPayMethod;

	@FindBy(xpath = "//input[contains(@name,'ccPacNameOnCard')]")
	WebElement ccNameWBW;

	@FindBy(xpath = "//input[contains(@name,'ccPacCardNum')]")
	WebElement ccNumberWBW;

	By ccTypeWBW = By.xpath("//select[contains(@name,'ccPacCardType')]");

	By ccMonthWBW = By.xpath("//select[contains(@name,'ccPacCardExpiresMonth')]");

	By ccYearWBW = By.xpath("//select[contains(@name,'ccPacCardExpiresYear')]");

	@FindBy(xpath = "//input[@value='Next']")
	WebElement financeNext;

	@FindBy(id = "member_number")
	WebElement txtownerNum;
	@FindBy(name = "next")
	WebElement nextBtn;
	@FindBy(xpath = "//select[@id='duesPACId']")
	WebElement duePayMethod;
	@FindBy(id = "duesCCPACInfo.cardMemberName")
	WebElement dueccNameWBW;
	@FindBy(id = "useSameAutoPayId")
	WebElement duecccheckboxWBW;

	@FindBy(id = "duesCCPACInfo.cardNumber")
	WebElement dueccNumberWBW;

	By dueccTypeWBW = By.xpath("//select[contains(@name,'duesCCPACInfo.cardType')]");

	By dueccMonthWBW = By.xpath("//select[contains(@name,'duesCCPACInfo.expireMonth')]");

	By dueccYearWBW = By.xpath("//select[contains(@name,'duesCCPACInfo.expireYear')]");

	@FindBy(xpath = "//input[@name='memberNo']")
	WebElement txtmemberNo;

	@FindBy(xpath = "//input[@name='search']")
	WebElement Btnsearch;
	@FindBy(xpath = "//input[@name='tsc']")
	WebElement txtTsc;
	@FindBy(xpath = "//select[contains(@name,'contract.duesPACId')]")
	WebElement duePaymethod;

	@FindBy(xpath = "//span[contains(.,'Loan')] | //a[contains(.,'Loan')]")
	WebElement loanTab;
	@FindBy(xpath = "//select[contains(@name,'contract.PACId')]")
	WebElement loanPaymethod;
	@FindBy(xpath = "//input[@id='pacInfo.accountHolderName']")
	WebElement loanccNameWBW;
	@FindBy(id = "pacInfo.address.city")
	WebElement loanccCity;
	@FindBy(id = "pacInfo.address.postalCode")
	WebElement loanccPostalCode;

	@FindBy(id = "pacInfo.accountNumber")
	WebElement loanacnumberWBW;
	@FindBy(id = "pacInfo.name")
	WebElement loanbankNameWBW;
	@FindBy(id = "pacInfo.address.state.abbreviation")
	WebElement loanstateWBW;

	@FindBy(id = "pacInfo.routingNumber")
	WebElement loansRoutinWBW;

	@FindBy(id = "pacInfo.token")
	WebElement loansTockenWBW;
	@FindBy(xpath = "//select[contains(@name,'contract.duesPACId')]")
	WebElement duepaymentMethod;

	@FindBy(id = "ccPACInfo.cardMemberName")
	WebElement loanacNameWBW;
	@FindBy(id = "ccPACInfo.cardNumber")
	WebElement loanccNumber;
	@FindBy(id = "ccPACInfo.expireMonth")
	WebElement loanccExMonth;
	@FindBy(id = "ccPACInfo.cardType")
	WebElement loanccCardType;
	@FindBy(id = "ccPACInfo.expireYear")
	WebElement loanccExYear;

	@FindBy(xpath = "//input[@id='duesPACInfo.accountHolderName']")
	WebElement duesccNameWBW;
	@FindBy(id = "duesPACInfo.address.city")
	WebElement duesccCity;
	@FindBy(id = "duesPACInfo.address.postalCode")
	WebElement duesccPostalCode;

	@FindBy(id = "duesPACInfo.accountNumber")
	WebElement duesacnumberWBW;
	@FindBy(id = "duesPACInfo.name")
	WebElement duesbankNameWBW;
	@FindBy(id = "duesPACInfo.address.state.abbreviation")
	WebElement duesstateWBW;

	@FindBy(id = "duesPACInfo.routingNumber")
	WebElement duessRoutinWBW;
	@FindBy(xpath = "//table[@class='client_table']//tr[8]//td[3]//input")
	WebElement SellingRep;

	@FindBy(xpath = "//img[@src='/webapp/ccis/images/log_off.gif']")
	WebElement LogoutBtnSP;

	@FindBy(xpath = "//input[@id='loan' and @value='Edit']")
	WebElement loan_edit;
	@FindBy(xpath = "//input[@name='cc_payment_member_name' and @class='textBox']")
	WebElement loan_cardname;
	@FindBy(xpath = "//input[@id='cc_payment_account_number']")
	WebElement loan_cardaccount;

	@FindBy(xpath = "//select[@name='signatureDate']")
	WebElement signatureDate;

	By editprimaryOwnerPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Edit Primary Owner ')]");
	By editprimaryOwner = By.xpath("//input[@id='primaryOwner']");
	@FindBy(xpath = "//td[text()='Primary Owner']/../../tr[@class='evenRow']/td[2]")
	WebElement editPowner;
	By maritalStatus = By.xpath("//select[@name='marital_status']");

	private void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, 10);
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("SalePoint_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	private void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("SalePoint_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	public void launchSalepoint(String strBrowser) {

		// this.driver = checkAndInitBrowser(strBrowser);
		String url = config.get("SP_URL").trim();
		driver.navigate().to(url);
		driver.manage().window().maximize();
		waitUntilElementVisibleBy(driver, buildVersion, 120);
		WebElement buildversion = driver.findElement(buildVersion);
		if (verifyObjectDisplayed(buildVersion)) {
			tcConfig.updateTestReporter("SalePointLoginPage", "loginToApp", Status.PASS,
					"Salepoint launched-Build version:" + buildversion.getText().substring(53));
		} else {
			tcConfig.updateTestReporter("SalePointLoginPage", "loginToApp", Status.FAIL,
					"Salepoint buildversion text not displayed");
		}

	}

	public void loginToSalePoint() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String SP_userid = tcConfig.getConfig().get("SP_UserID");
		String SP_password = tcConfig.getConfig().get("SP_Password");

		setUserName(SP_userid);
		setPassword(SP_password);

		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SalePointLoginPage", "loginToApp", Status.PASS, "Log in button clicked");

	}

	public void changeProfileLocation() {

		driver.navigate().to(testData.get("myProfileURL"));

		waitUntilObjectVisible(driver, locationSelect, 120);

		if (verifyObjectDisplayed(serviceEntityChange)) {
			tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.PASS,
					"serviceEntityChange Field present");

			String strCompany = testData.get("ServiceEntity");

			if (strCompany.equalsIgnoreCase("WBW")) {
				new Select(driver.findElement(serviceEntityChange)).selectByVisibleText("Worldmark By Wyndham");
			} else if (strCompany.equalsIgnoreCase("WVR")) {
				new Select(driver.findElement(serviceEntityChange)).selectByVisibleText("Wyndham Vacation Resorts");
			} else if (strCompany.equalsIgnoreCase("WVRAP")) {
				new Select(driver.findElement(serviceEntityChange))
						.selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
			} else {
				tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.FAIL,
						"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
			}

		}
		waitForSometime(tcConfig.getConfig().get("LongWait"));

		if (verifyObjectDisplayed(locationSelect)) {
			tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.PASS,
					"Location Field present");

			String strLocation = testData.get("strLocation");

			new Select(driver.findElement(locationSelect)).selectByVisibleText(strLocation);

			tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.PASS, "Location Selected");

			clickElementJS(saveButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilObjectVisible(driver, mainMenButton, 120);

		}

		else {
			tcConfig.updateTestReporter("SalePointHomePage", "changeProfileLocation", Status.FAIL,
					"Location Field not present");
		}

	}

	public void companySelectSalePoint() throws Exception {

		waitUntilElementVisibleIE(driver, companySelect, 120);

		String strCompany = testData.get("ServiceEntity");

		if (strCompany.equalsIgnoreCase("WBW")) {

			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Worldmark By Wyndham");

		} else if (strCompany.equalsIgnoreCase("WVR")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts");
		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.switchTo().activeElement().sendKeys(Keys.TAB);

		tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.PASS,
				"Company Selected as: " + strCompany);

		clickElementJS(okBtn);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Actions act = new Actions(driver);

		if (strCompany.equalsIgnoreCase("WVRAP")) {
			System.out.println("No Alert Present");

		} else {

			try {
				new WebDriverWait(driver, 120).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				System.out.println("No Alert Present after Login");
			}
		}
		tcConfig.updateTestReporter("SalePointHomePage", "companySelectSalePoint", Status.PASS,
				"HomePage Navigation Successful");
	}

	public void SalePoint_conversion_Flow() throws Exception {
		int noOfSecOwnWBW = 0;
		List<WebElement> secOwnlistWBW = null;

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, txtownerNum, 120);

		if (verifyElementDisplayed(txtownerNum)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_conversion_Flow", Status.PASS,
					"Contract processing- conversion Page Navigation Succesfull");

			txtownerNum.sendKeys(testData.get("memberNumber"));
			nextBtn.click();

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_conversion_Flow", Status.FAIL,
					"Contract processing- conversion Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
						"Automation Pop Up message is displayed");

			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, saveContractClick, 120);

			// Edit the Owner

			if (verifyObjectDisplayed(editMemberWBW)) {

				driver.findElement(editMemberWBW).click();

				if (verifyObjectDisplayed(primaryOwnerPage)) {

					// ssn Field

					if (ssnWBWSelect.isEnabled() == false) {

						System.out.println("SSN Not Required User Can Proceed");

					} else {
						if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
							ssnWBWSelect.clear();
							ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
						}
					}

					if (homePhoneWBW.getAttribute("value").equals("")) {
						homePhoneWBW.sendKeys(testData.get("homeNumber"));

					}
					System.out.println("Primary Owner Edit page");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleIE(driver, nextOwner, 120);
					clickElementJS(nextOwner);

				} else if (verifyElementDisplayed(driver.findElement(ownerDetailsPage))) {
					System.out.println("OwnerDetails Page Directly Opened");

					// check the secondary owner count with delete buttons

					if (verifyObjectDisplayed(deleteOwnerWBW)) {
						secOwnlistWBW = driver.findElements(deleteOwnerWBW);

						noOfSecOwnWBW = secOwnlistWBW.size();

					}

					// check primary owner

					if (verifyElementDisplayed(pNameWBW)) {

						System.out.println("Primary Owner Exists");

						// delete all secondary owner
						for (int i = 0; i < noOfSecOwnWBW; i++) {

							clickElementJSWithWait(deleteFirstOwnerWBW);
							waitForSometime(tcConfig.getConfig().get("MedWait"));

							if (ExpectedConditions.alertIsPresent() != null) {
								try {

									driver.switchTo().alert().accept();

								} catch (Exception e) {
									System.out.println(e);
								}
							}
							waitForSometime(tcConfig.getConfig().get("MedWait"));

						}
					} else {
						System.out.println("Primary Owner Does not exist");

						// delete all secondary owner except one

						for (int i = 0; i < (noOfSecOwnWBW - 1); i++) {

							// clickElementJSWithWait(secOwnlist.get(0));
							clickElementJSWithWait(deleteListOfOwnerWBW);

							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						}

						// make one secondary to primary

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (verifyObjectDisplayed(MakePrimaryWBW)) {
							clickElementJSWithWait(MakePrimaryWBW);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}

					}

					if (verifyObjectDisplayed(editPOwnerWBW)) {

						clickElementJSWithWait(editPOwnerWBW);

						if (verifyObjectDisplayed(primaryOwnerPage)) {
							if (ssnWBWSelect.isEnabled() == false) {

								System.out.println("SSN Not Required User Can Proceed");

							} else {
								if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
									ssnWBWSelect.clear();
									ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
								}
							}

							if (homePhoneWBW.getAttribute("value").equals("")) {
								homePhoneWBW.sendKeys(testData.get("homeNumber"));

							}

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							// waitUntilElementVisibleIE(driver, nextOwner,
							// 120);

							clickElementJS(nextOwner);
							// nextOwner.click();

						}

					}

				} else {
					System.out.println("No Primary owner edit button");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, nextOwner, 120);

				clickElementJS(nextOwner);

				waitUntilElementVisibleIE(driver, editPaymentWBW, 120);

				WebElement ele = driver.findElement(editPaymentWBW);
				clickElementJS(ele);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				//

				// waitUntilElementVisibleIE(driver, financePage, 120); Ajay
				// waitUntilElementVisibleIE(driver, wbwPaymentOption, 120);
				waitUntilElementVisible(driver, duePayMethod, 120);

				if (verifyElementDisplayed(duePayMethod)) {

					duePayMethod.click();
					String payMethod = testData.get("PayMethod");

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(
							By.xpath("//select[contains(@id,'duesPACId')]/option[contains(.,'" + payMethod + "')]"))
							.click();

				}
				// Ajay1
				waitUntilElementVisible(driver, dueccNameWBW, 120);

				dueccNameWBW.click();
				dueccNameWBW.clear();
				dueccNameWBW.sendKeys(testData.get("CCName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(dueccTypeWBW).click();
				String cardType = testData.get("CCType");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath(
						"//select[contains(@name,'duesCCPACInfo.cardType')]/option[contains(.,'" + cardType + "')]"))
						.click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				dueccNumberWBW.click();
				dueccNumberWBW.clear();
				dueccNumberWBW.sendKeys(testData.get("CCNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(dueccMonthWBW).click();
				String expMnth = testData.get("CCExpMnth");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath(
						"//select[contains(@name,'duesCCPACInfo.expireMonth')]/option[contains(.,'" + expMnth + "')]"))
						.click();

				driver.findElement(dueccYearWBW).click();
				String expYr = testData.get("CCExpYear");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath(
						"//select[contains(@name,'duesCCPACInfo.expireYear')]/option[contains(.,'" + expYr + "')]"))
						.click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// Checkbox
				clickElementJS(duecccheckboxWBW);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}

				waitUntilElementVisibleIE(driver, financeNext, 120);
				clickElementJS(financeNext);

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("Alert not coming");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, saveContractClick, 120);

				if (verifyElementDisplayed(savecontractWBW)) {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Review Page Displayed and Save Button Clicked");

					clickElementJS(savecontractWBW);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("");
					}
				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Contract Save Page Navigation Error");

				}
				/*
				 * waitUntilElementVisibleIE(driver, savecontractWBW, 120);
				 * 
				 * if (verifyElementDisplayed(savecontractWBW)) {
				 * tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow",
				 * Status.PASS, "Contract Review Page Displayed and Save Button Clicked");
				 * 
				 * clickElementJS(savecontractWBW);
				 * waitForSometime(tcConfig.getConfig().get("LowWait")); try {
				 * 
				 * driver.switchTo().alert().accept();
				 * 
				 * } catch (Exception e) { System.out.println(""); } } else {
				 * tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow",
				 * Status.FAIL, "Contract Save Page Navigation Error");
				 * 
				 * }
				 */

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, generate, 120);

				if (verifyElementDisplayed(generate)) {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Review Page Displayed and Generate Button Clicked");
					// clickElementJSWithWait(generate);
					clickElementJS(generate);

				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Generate contract Page Navigation Error");
				}

				// waitUntilElementVisibleIE(driver,
				// By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"),
				// 120);
				waitUntilElementVisibleIE(driver, conNo, 120);

				if (verifyElementDisplayed(conNo)) {

					// testData.put("strContract", conNo.getText());
					strContractNo = conNo.getText();

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Created with number: " + conNo.getText());
				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Contract Creation failed ");
				}

				if (verifyElementDisplayed(memberNo)) {

					strMemberNo = memberNo.getText().split("\\:")[1].trim();
					System.out.println("MemberNo " + strMemberNo);

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Member No: " + strMemberNo);

				} else {

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Member number could not be retrieved");
				}

			}

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Automation Pop Up Not Available");

		}

	}

	public void contractProcessingConversion() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, txtownerNum, 120);

		if (verifyElementDisplayed(txtownerNum)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_conversion_Flow", Status.PASS,
					"Contract processing- conversion Page Navigation Succesfull");

			txtownerNum.sendKeys(testData.get("memberNumber"));
			nextBtn.click();

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_conversion_Flow", Status.FAIL,
					"Contract processing- conversion Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
						"Automation Pop Up message is displayed");

			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}

	}

	public void WorldMarkOwners() throws Exception {
		int noOfSecOwnWBW = 0;
		List<WebElement> secOwnlistWBW = null;

		waitUntilElementVisibleIE(driver, saveContractClick, 120);

		// Edit the Owner

		if (verifyObjectDisplayed(editMemberWBW)) {

			driver.findElement(editMemberWBW).click();

			if (verifyObjectDisplayed(primaryOwnerPage)) {

				// ssn Field

				if (ssnWBWSelect.isEnabled() == false) {

					System.out.println("SSN Not Required User Can Proceed");

				} else {
					if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
						ssnWBWSelect.clear();
						ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
					}
				}

				if (homePhoneWBW.getAttribute("value").equals("")) {
					homePhoneWBW.sendKeys(testData.get("homeNumber"));

				}
				System.out.println("Primary Owner Edit page");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleIE(driver, nextOwner, 120);
				clickElementJS(nextOwner);

			} else if (verifyElementDisplayed(driver.findElement(ownerDetailsPage))) {
				System.out.println("OwnerDetails Page Directly Opened");

				// check the secondary owner count with delete buttons

				if (verifyObjectDisplayed(deleteOwnerWBW)) {
					secOwnlistWBW = driver.findElements(deleteOwnerWBW);

					noOfSecOwnWBW = secOwnlistWBW.size();

				}

				// check primary owner

				if (verifyElementDisplayed(pNameWBW)) {

					System.out.println("Primary Owner Exists");

					// delete all secondary owner
					for (int i = 0; i < noOfSecOwnWBW; i++) {

						clickElementJSWithWait(deleteFirstOwnerWBW);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (ExpectedConditions.alertIsPresent() != null) {
							try {

								driver.switchTo().alert().accept();

							} catch (Exception e) {
								System.out.println(e);
							}
						}
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}
				} else {
					System.out.println("Primary Owner Does not exist");

					// delete all secondary owner except one

					for (int i = 0; i < (noOfSecOwnWBW - 1); i++) {

						// clickElementJSWithWait(secOwnlist.get(0));
						clickElementJSWithWait(deleteListOfOwnerWBW);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					}

					// make one secondary to primary

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(MakePrimaryWBW)) {
						clickElementJSWithWait(MakePrimaryWBW);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

				}

				if (verifyObjectDisplayed(editPOwnerWBW)) {

					clickElementJSWithWait(editPOwnerWBW);

					if (verifyObjectDisplayed(primaryOwnerPage)) {
						if (ssnWBWSelect.isEnabled() == false) {

							System.out.println("SSN Not Required User Can Proceed");

						} else {
							if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
								ssnWBWSelect.clear();
								ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
							}
						}

						if (homePhoneWBW.getAttribute("value").equals("")) {
							homePhoneWBW.sendKeys(testData.get("homeNumber"));

						}

						waitForSometime(tcConfig.getConfig().get("LongWait"));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						// waitUntilElementVisibleIE(driver, nextOwner,
						// 120);

						clickElementJS(nextOwner);
						// nextOwner.click();

					}

				}

			} else {
				System.out.println("No Primary owner edit button");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, nextOwner, 120);

			clickElementJS(nextOwner);
		}

	}

	public void WorldMarkPaymentOptionsScreen() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, editPaymentWBW, 120);

		// clickElementJS(driver.findElement(editPaymentWBW));

		WebElement ele = driver.findElement(editPaymentWBW);
		clickElementJS(ele);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisible(driver, duePayMethod, 120);

		if (verifyElementDisplayed(duePayMethod)) {

			duePayMethod.click();
			String payMethod = testData.get("PayMethod");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select[contains(@id,'duesPACId')]/option[contains(.,'" + payMethod + "')]"))
					.click();

		}
		// Ajay1
		waitUntilElementVisible(driver, dueccNameWBW, 120);

		dueccNameWBW.click();
		dueccNameWBW.clear();
		dueccNameWBW.sendKeys(testData.get("CCName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(dueccTypeWBW).click();
		String cardType = testData.get("CCType");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(
				By.xpath("//select[contains(@name,'duesCCPACInfo.cardType')]/option[contains(.,'" + cardType + "')]"))
				.click();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		dueccNumberWBW.click();
		dueccNumberWBW.clear();
		dueccNumberWBW.sendKeys(testData.get("CCNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(dueccMonthWBW).click();
		String expMnth = testData.get("CCExpMnth");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(
				By.xpath("//select[contains(@name,'duesCCPACInfo.expireMonth')]/option[contains(.,'" + expMnth + "')]"))
				.click();

		driver.findElement(dueccYearWBW).click();
		String expYr = testData.get("CCExpYear");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(
				By.xpath("//select[contains(@name,'duesCCPACInfo.expireYear')]/option[contains(.,'" + expYr + "')]"))
				.click();

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Checkbox
		clickElementJS(duecccheckboxWBW);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {

			driver.switchTo().alert().accept();

		} catch (Exception e) {
			System.out.println("");
		}

		waitUntilElementVisibleIE(driver, financeNext, 120);
		clickElementJS(financeNext);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {

			driver.switchTo().alert().accept();

		} catch (Exception e) {
			System.out.println("Alert not coming");
		}

	}

	public void contractSaveAndPrint() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, saveContractClick, 120);

		if (verifyElementDisplayed(savecontractWBW)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontractWBW);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, generate, 120);

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			// clickElementJSWithWait(generate);
			clickElementJS(generate);

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		// waitUntilElementVisibleIE(driver,
		// By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"),
		// 120);
		waitUntilElementVisibleIE(driver, conNo, 120);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	public void sp_New_ContractFlow_Automation() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, customerNoInput, 120);

		int noOfSecOwn = 0;
		List<WebElement> secOwnlist = null;
		// List<WebElement> secStatus = null;

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			String createdTourid = EndToEndSalesForce.TourID;
			customerNo.sendKeys(createdTourid);

			// String createdTouriD = "12452404";
			// tourID.sendKeys(createdTouriD);

			clickElementJS(next1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.FAIL,
					"Page Navigation Error");

		}

		// try{

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Automation Pop Up message");

			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}

			waitUntilElementVisibleIE(driver, saveClick, 120);

			if (verifyObjectDisplayed(primaryOwnerEdit)) {

				driver.findElement(primaryOwnerEdit).click();

				// Owner Page - Primary Owner and Secondary Owner

				if (verifyObjectDisplayed(headerContractProcessOwner)) {

					// check the secondary owner count with delete buttons

					if (verifyObjectDisplayed(deleteOwner)) {
						secOwnlist = driver.findElements(deleteOwner);

						noOfSecOwn = secOwnlist.size();

					}

					// check primary owner

					if (verifyElementDisplayed(pName)) {

						System.out.println("Primary Owner Exists");

						// delete all secondary owner
						for (int i = 0; i < noOfSecOwn; i++) {

							clickElementJSWithWait(deleteFirstOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						}

					} else {

						System.out.println("No Primary Owner Exists");

						// delete all secondary owner except one

						for (int i = 0; i < (noOfSecOwn - 1); i++) {

							// clickElementJSWithWait(secOwnlist.get(0));
							clickElementJSWithWait(deleteListOfOwner);

							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						}

						// make one secondary to primary

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (verifyObjectDisplayed(statusListOfOwner)) {
							clickElementJSWithWait(statusListOfOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}

					}

				}

				if (verifyObjectDisplayed(editPrimaryOwner)) {

					try {

						// select the relationship

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						new Select(driver.findElement(relationshipStatus)).selectByIndex(2);

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						// clickElementJS(driver.findElement(genderSelect));
						WebElement ele = driver.findElement(genderSelect);
						clickElementJS(ele);

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						// select sale type

						if (verifyElementDisplayed(saleType)) {
							tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_Automation",
									Status.PASS, "Customer Info page Displayed");

							// JavascriptExecutor executor =
							// (JavascriptExecutor) driver;
							clickElementJS(saleType);

							driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						} else {
							System.out.println("SaleType is not visible ");

						}

						// ssn field

						if (ssnField.isEnabled() == false) {

							System.out.println("SSN Not Required User Can Proceed");

						} else {
							if (ssnField.getAttribute("value").isEmpty() == true) {
								ssnField.clear();
								ssnField.sendKeys(testData.get("ssnNumber"));
							}
						}

						// DateOf Birth

						String dob = testData.get("DOB");
						String dobmonth = "", dobDay = "";
						String arr[] = dob.split("/");

						// MonthFormation
						if (arr[0].length() == 1) {
							dobmonth = "0" + arr[0];
						} else {
							dobmonth = arr[0];
						}
						// DayFormation
						if (arr[1].length() == 1) {
							dobDay = "0" + arr[1];
						} else {
							dobDay = arr[1];
						}
						// Year

						String dobYear = arr[2];

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (driver.findElement(MonthFromDrpDwn).getAttribute("value").equals("")) {
							// new
							// Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);
							new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(dobmonth);

						}

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (driver.findElement(DateFromDrpDwn).getAttribute("value").equals("")) {
							// new
							// Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);
							new Select(driver.findElement(DateFromDrpDwn)).selectByValue(dobDay);

						}

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (driver.findElement(YearFromDrpDwn).getAttribute("value").equals("")) {
							// new
							// Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);
							new Select(driver.findElement(YearFromDrpDwn)).selectByValue(dobYear);

						}

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (home_Phone_fld.getAttribute("value").equals("")) {
							home_Phone_fld.sendKeys(testData.get("homeNumber"));

						}

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						clickElementJS(updateVal);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} catch (Exception e) {
						System.out.println("SSN Cannot Be Entered");
					}

				}

				if (verifyObjectDisplayed(headerContractProcessOwner)) {

					// check the secondary owner count with delete buttons

					if (verifyObjectDisplayed(deleteOwner)) {
						secOwnlist = driver.findElements(deleteOwner);

						noOfSecOwn = secOwnlist.size();

					}

					// check primary owner

					// if (verifyElementDisplayed(pName)){

					// System.out.println("Primary Owner Exists");

					// delete all secondary owner
					for (int i = 0; i < noOfSecOwn; i++) {

						clickElementJSWithWait(deleteonesecOwner);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (ExpectedConditions.alertIsPresent() != null) {
							try {

								driver.switchTo().alert().accept();

							} catch (Exception e) {
								System.out.println(e);
							}
						}
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						// }

					}

					if (verifyElementDisplayed(nextVal)) {
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						clickElementJS(nextVal);
					}
				}

			} else {
				System.out.println("No Primary owner edit button");
			}

			waitUntilElementVisibleIE(driver, saveClick, 120);

			if (verifyObjectDisplayed(PaymentEdit)) {

				driver.findElement(PaymentEdit).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no alert is displayed");
					}
				}

				waitUntilElementVisibleIE(driver, ccSelect, 120);

				WebElement ele = driver.findElement(ccSelect);
				clickElementJS(ele);
				// clickElementJS(driver.findElement(ccSelect));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no Alert is displayed");
					}

				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				ccName.clear();
				ccName.click();
				ccName.sendKeys(testData.get("CCName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(ccType).click();
				String cardType = testData.get("CCType");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'" + cardType + "')]")).click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				ccNumber.clear();
				ccNumber.click();
				ccNumber.sendKeys(testData.get("CCNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(ccMonth).click();
				String expMnth = testData.get("CCExpMnth");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'" + expMnth + "')]")).click();

				driver.findElement(ccYear).click();
				String expYr = testData.get("CCExpYear");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'" + expYr + "')]")).click();

				// new
				// Select(driver.findElement(ccYear)).selectByValue(testData.get("CCExpYear"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisibleIE(driver, nextVal, 120);
				clickElementJS(nextVal);

				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no Alert is displayed");
					}

				}

				waitUntilElementVisibleIE(driver, calculateClick, 120);

				if (verifyElementDisplayed(calculate)) {
					tcConfig.updateTestReporter("SalePointCalculationPage", "sp_New_ContractFlow_Automation",
							Status.PASS, "Money Screen present");

					clickElementJS(calculate);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementJS(nextVal);
				}

				if (ExpectedConditions.alertIsPresent() != null) {
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("no Alert is displayed");
					}

				}

			}

			waitUntilElementVisibleIE(driver, savecontract, 120);

			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("ContractSavePage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("ContractSavePage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Contract Save Page Navigation Error");

			}

			// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, printClick, 120);

			if (verifyElementDisplayed(print)) {
				clickElementJSWithWait(print);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			waitUntilElementVisibleIE(driver, memberNoCheck, 120);

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText().trim();

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
						"Member number could not be retrieved");
			}

		} else {

			tcConfig.updateTestReporter("SalePointContractFinalPage", "sp_New_ContractFlow_Automation", Status.FAIL,
					"Automation Pop Up is not available");

		}

	}

	// @FindBy(xpath = "//a[contains(.,'Status')]")
	// WebElement statusSec;

	public void navigateToMenuURL() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void navigateToMenu(String Iterator) throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL" + Iterator));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void searchMemDiscoveryUpgrdae() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(discoveryMember).click();
		driver.findElement(discoveryMember).sendKeys(testData.get("memberNumber"));
		driver.findElement(searchBtn).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(upgradeButton).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void contractProcessinglocateCustomer() throws Exception {

		waitUntilElementVisibleIE(driver, customerNo, 120);

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractProcessinglocateCustomer", Status.PASS,
					"contract Processing locate Customer Page Navigation Successful");
			customerNo.click();
			String createdTourid = EndToEndSalesForce.TourID;
			// S String createdTourid = "57520006";
			customerNo.sendKeys(createdTourid);
			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractProcessinglocateCustomer", Status.FAIL,
					"contract Processing locate Customer Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	public void contractProcessingOwners() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		checkPrimaryandSecondaryOwner();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, headerCustInfo, 120);
		if (verifyObjectDisplayed(headerCustInfo)) {

			try {
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				new Select(driver.findElement(relationshipStatus)).selectByIndex(0);
				new Select(driver.findElement(maritalStatus)).selectByIndex(0);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (testData.get("MemberRequired").contains("N")) {

					System.out.println("MemberNum Not Required User Can Proceed");

				} else {

					driver.findElement(inputMemberNum).clear();
					driver.findElement(inputMemberNum).sendKeys(testData.get("memberNumber"));

				}
				if (ssnField.isEnabled() == false) {

					System.out.println("SSN Not Required User Can Proceed");

				} else {

					ssnField.clear();
					ssnField.sendKeys(testData.get("ssnNumber"));

				}
				if (home_Phone_fld.isEnabled() == false) {

					System.out.println("SSN Not Required User Can Proceed");

				} else {

					home_Phone_fld.clear();
					home_Phone_fld.sendKeys(testData.get("HomePhone"));

				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(MonthFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(DateFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(YearFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);

				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement ele = driver.findElement(genderSelect);
				clickElementJS(ele);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(nextVal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractProcessingCustomerInformation",
						Status.PASS, "Customer Info page saved succesfully");

			} catch (Exception e) {
				System.out.println("Error in filling Contract Processing - Customer Information ");
			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

	public void checkPrimaryandSecondaryOwner() {
		if (verifyElementDisplayed(chcekOwner)) {
			try {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisible(driver, chcekOwner, 60);
				List<WebElement> secOwnlist = driver.findElements(deleteOwner);
				int noOfSecOwn = secOwnlist.size();
				if (verifyElementDisplayed(chcekOwner)) {
					String Pname = pName.getText().trim();
					if (Pname == null || Pname.isEmpty()) {
						if (noOfSecOwn == 1) {
							clickElementJSWithWait(statusListOfOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						} else {
							for (int i = 0; i < (noOfSecOwn - 1); i++) {
								clickElementJSWithWait(deleteFirstOwner);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.switchTo().alert().accept();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
							clickElementJSWithWait(statusListOfOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
					} else {
						for (int i = 0; i < (noOfSecOwn); i++) {
							clickElementJSWithWait(deleteFirstOwner);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}
					}
				}

			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	public void WVRcontractProcessingOwners() throws Exception {

		int noOfSecOwn = 0;
		List<WebElement> secOwnlist = null;
		// List<WebElement> secStatus = null;

		// Owner Page - Primary Owner and Secondary Owner
		if (verifyObjectDisplayed(headerContractProcessOwner)) {

			// check the secondary owner count with delete buttons

			if (verifyObjectDisplayed(deleteOwner)) {
				secOwnlist = driver.findElements(deleteOwner);

				noOfSecOwn = secOwnlist.size();

			}

			// if (verifyObjectDisplayed(StatusOwner)) {
			// secStatus = driver.findElements(StatusOwner);
			//
			// }

			// check primary owner
			String Pname = pName.getText();
			// verifyElementDisplayed(pName)
			if (Pname.equalsIgnoreCase(null)) {

				System.out.println("No Primary Owner Exists");

				// delete all secondary owner except one

				for (int i = 0; i < (noOfSecOwn - 1); i++) {

					// clickElementJSWithWait(secOwnlist.get(0));
					clickElementJSWithWait(deleteListOfOwner);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				}

				// make one secondary to primary

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(statusListOfOwner)) {
					clickElementJSWithWait(statusListOfOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} else {

				System.out.println("Primary Owner Exists");

				// delete all secondary owner
				for (int i = 0; i < noOfSecOwn; i++) {

					clickElementJSWithWait(deleteFirstOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				}

			}

		}
		clickElementJS(nextVal);
	}

	public void contractProcessingCustomerInformation() throws Exception {

		waitUntilElementVisibleIE(driver, headerCustInfo, 120);
		if (verifyObjectDisplayed(headerCustInfo)) {

			try {
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				new Select(driver.findElement(relationshipStatus)).selectByIndex(2);
				new Select(driver.findElement(maritalStatus)).selectByIndex(2);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (ssnField.isEnabled() == false) {

					System.out.println("SSN Not Required User Can Proceed");

				} else {

					ssnField.clear();
					ssnField.sendKeys(testData.get("ssnNumber"));

				}
				if (home_Phone_fld.isEnabled() == false) {

					System.out.println("SSN Not Required User Can Proceed");

				} else {

					home_Phone_fld.clear();
					home_Phone_fld.sendKeys(testData.get("HomePhone"));

				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(MonthFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(DateFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(YearFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);

				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement ele = driver.findElement(genderSelect);
				clickElementJS(ele);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(saleType);

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(nextVal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				System.out.println("Error in filling Contract Processing - Customer Information ");
			}

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractProcessingCustomerInformation", Status.PASS,
					"Customer Info page saved succesfully");
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractProcessingCustomerInformation", Status.PASS,
					"Customer Info page not saved succesfully");
		}

	}

	public void contractProcessingOwner() throws Exception {

		waitUntilElementVisibleIE(driver, headerContractProcessOwner, 120);

		if (verifyObjectDisplayed(headerContractProcessOwner)) {
			tcConfig.updateTestReporter("contractProcessingOwner", "contractProcessingOwner", Status.PASS,
					"contractProcessingOwner page is displayed");
			if (verifyElementDisplayed(nextVal)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(nextVal);
			}

		} else {
			tcConfig.updateTestReporter("contractProcessingOwner", "contractProcessingOwner", Status.FAIL,
					"contractProcessingOwner page is not displayed");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void contractProcessingDataEntrySelection() throws Exception {

		// waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, udiClick, 120);

		if (verifyElementDisplayed(udiSelect)) {

			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void SelectDiscoveryContractProcessing() throws Exception {

		// waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, udiClick, 120);

		if (verifyElementDisplayed(DiscoverySelect)) {

			clickElementJSWithWait(DiscoverySelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void contractProcessingInventrySelectionPhase() throws Exception {

		waitUntilElementVisibleIE(driver, newPointsCheck, 120);

		if (verifyElementDisplayed(newPoints)) {

			// select inventory

			// new
			// Select(driver.findElement(inventory_Site)).selectByValue(testData.get("invSite"));

			String invName = testData.get("invSite");
			driver.findElement(inventory_Site).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			driver.findElement(By.xpath("//select/option[contains(.,'" + invName + "')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String invPhase = testData.get("invPhase");
			driver.findElement(inventory_phase).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			driver.findElement(By.xpath("//select/option[contains(.,'" + invPhase + "')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("totalNewPoint"));

		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void contractProcessingInventrySelection() throws Exception {

		waitUntilElementVisibleIE(driver, nextClick, 120);

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, 120);

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, 120);

		if (verifyObjectDisplayed(cashSelect)) {

			WebElement ele = driver.findElement(cashSelect);
			clickElementJS(ele);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			WebElement ele2 = driver.findElement(cashSelect);
			clickElementJS(ele2);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, 120);

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, 120);

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, 120);

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, 120);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, printClick, 120);
		if (verifyObjectDisplayed(printClick)) {
			clickElementJSWithWait(printClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(printOption)) {
				selectByIndex(printOption, 2);
				tcConfig.updateTestReporter("SalepointContractPrintPage", "contractClickPrintButton", Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(printClick);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("SalepointContractPrintPage", "contractProcessingContractSummary", Status.FAIL,
					"Failed to click Print PDF button");
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, 120);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	public void contractProcessingMoneyScreen() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, 120);

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Money Screen not present");
		}

		/*
		 * waitUntilElementVisibleIE(driver, salePersonSelect, 120);
		 * 
		 * saleper.click(); saleper.sendKeys(testData.get("strSalePer"));
		 * driver.switchTo().activeElement().sendKeys(Keys.TAB);
		 * clickElementJS(nextVal); waitUntilElementVisibleIE(driver, nextClick, 120);
		 * 
		 * clickElementJS(nextVal);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 */
		waitUntilElementVisibleIE(driver, savecontract, 120);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, 120);
		if (verifyObjectDisplayed(printClick)) {
			clickElementJSWithWait(printClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(printOption)) {
				selectByIndex(printOption, 2);
				tcConfig.updateTestReporter("SalepointContractPrintPage", "contractClickPrintButton", Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(printClick);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("SalepointContractPrintPage", "contractProcessingContractSummary", Status.FAIL,
					"Failed to click Print PDF button");
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, 120);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	public void sp_New_ContractFlow_NOpopUp() throws Exception {

		int noOfSecOwn = 0;
		List<WebElement> secOwnlist = null;
		// List<WebElement> secStatus = null;

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, customerNo, 120);

		if (verifyElementDisplayed(customerNo)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			// customerNo.sendKeys(testData.get("strTourId"));
			// String createdTourid = EndToEndSalesForce.TourID;
			// tourID.sendKeys(createdTourid);

			String createdTouriD = "45933345";
			customerNo.sendKeys(createdTouriD);

			clickElementJS(next1);

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		// if worksheet is from different site, pop up

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				System.out.println(e);
			}
		}

		// Owner Page - Primary Owner and Secondary Owner

		if (verifyObjectDisplayed(headerContractProcessOwner)) {

			// check the secondary owner count with delete buttons

			if (verifyObjectDisplayed(deleteOwner)) {
				secOwnlist = driver.findElements(deleteOwner);

				noOfSecOwn = secOwnlist.size();

			}

			// if (verifyObjectDisplayed(StatusOwner)) {
			// secStatus = driver.findElements(StatusOwner);
			//
			// }

			// check primary owner

			if (verifyElementDisplayed(pName)) {

				System.out.println("Primary Owner Exists");

				// delete all secondary owner
				for (int i = 0; i < noOfSecOwn; i++) {

					clickElementJSWithWait(deleteFirstOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				}

			} else {

				System.out.println("No Primary Owner Exists");

				// delete all secondary owner except one

				for (int i = 0; i < (noOfSecOwn - 1); i++) {

					// clickElementJSWithWait(secOwnlist.get(0));
					clickElementJSWithWait(deleteListOfOwner);

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				}

				// make one secondary to primary

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(statusListOfOwner)) {
					clickElementJSWithWait(statusListOfOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			}

		}

		if (verifyObjectDisplayed(headerCustInfo)) {

			try {

				// select the relationship

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				new Select(driver.findElement(relationshipStatus)).selectByIndex(2);

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*
				 * driver.findElement(ssnSelect).click(); driver.findElement(ssnSelect).clear();
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * driver.findElement(ssnSelect).sendKeys(testData.get( "ssnNumber"));
				 */
				// waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement ele = driver.findElement(genderSelect);
				clickElementJS(ele);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				// select sale type

				if (verifyElementDisplayed(saleType)) {
					tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
							"Customer Info page Displayed");

					// JavascriptExecutor executor = (JavascriptExecutor)
					// driver;
					clickElementJS(saleType);

					driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					System.out.println("MainLine  Not Present User Can Proceed");

				}

				// ssn field

				if (ssnField.isEnabled() == false) {

					System.out.println("SSN Not Required User Can Proceed");

				} else {

					ssnField.clear();
					ssnField.sendKeys(testData.get("ssnNumber"));

				}

				// DateOf Birth

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(MonthFromDrpDwn).getText() == "") {
					new Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);
					;

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(DateFromDrpDwn).getText() == "") {
					new Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);
					;

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(YearFromDrpDwn).getText() == "") {
					new Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);
					;

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (home_Phone_fld.getText() == "") {
					home_Phone_fld.sendKeys(testData.get("homeNumber"));

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				clickElementJS(nextVal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				System.out.println("SSN Cannot Be Entered");
			}
			// waitForSometime(tcConfig.getConfig().get("MedWait"));

			// clickElementJS(nextVal);

		}

		if (verifyObjectDisplayed(headerContractProcessOwner)) {

			if (verifyElementDisplayed(nextVal)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(nextVal);
			}
		}

		// waitForSometime(tcConfig.getConfig().get("LongWait"));

		waitUntilElementVisibleIE(driver, udiClick, 120);

		if (verifyElementDisplayed(udiSelect)) {
			clickElementJSWithWait(udiSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextVal);
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"UDI not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, newPointsCheck, 120);

		if (verifyElementDisplayed(newPoints)) {

			// select inventory

			// new
			// Select(driver.findElement(inventory_Site)).selectByValue(testData.get("invSite"));

			String invName = testData.get("invSite");
			driver.findElement(inventory_Site).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			driver.findElement(By.xpath("//select/option[contains(.,'" + invName + "')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"NewPoints not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, nextClick, 120);

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, 120);

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, nextClick, 120);

		if (verifyObjectDisplayed(cashSelect)) {
			WebElement ele = driver.findElement(cashSelect);
			clickElementJS(ele);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(ele);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, calculateClick, 120);

		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Money Screen present");

			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Money Screen not present");
		}

		waitUntilElementVisibleIE(driver, salePersonSelect, 120);

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementJS(nextVal);
		waitUntilElementVisibleIE(driver, nextClick, 120);

		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, saveClick, 120);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, 120);
		waitUntilElementVisibleIE(driver, print, 120);

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		waitUntilElementVisibleIE(driver, memberNoCheck, 120);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_ContractFlow_NOpopUp", Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	public void sp_New_contractSearch_and_VOID() throws Exception {

		List<WebElement> voidListWVR = null;

		int voidCountBefore = 0;

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, searchFilter, 120);

		if (verifyElementDisplayed(selectAllByDate)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.PASS,
					"Contract in batch report Page Displayed");

			clickElementJS(selectAllByDate);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// LocalDate today= LocalDate.now();
			Calendar cal = Calendar.getInstance();
			// DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			// Date today = cal.getTime();
			// cal.MONTH;

			int monthVal = (cal.get(Calendar.MONTH));

			String strMonth = String.valueOf(monthVal).trim();
			String strDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH)).trim();

			waitUntilElementVisibleIE(driver, By.name("startmonth"), 120);

			new Select(driver.findElement(By.name("startmonth"))).selectByValue(strMonth);

			new Select(driver.findElement(By.name("startday"))).selectByValue(strDate);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(getData);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"), 120);

			if (verifyObjectDisplayed(VoidElementWVR)) {

				voidListWVR = driver.findElements(VoidElementWVR);

				voidCountBefore = voidListWVR.size();

			}

			if (verifyElementDisplayed(
					driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointContractSeacrh", "sp_New_contractSearch_and_VOID", Status.PASS,
						"Contract No Verified and is: " + strContractNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"));

				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointContractSeacrh", "sp_New_contractSearch_and_VOID", Status.FAIL,
						"Contract No Not Verified");

			}

			waitUntilElementVisibleIE(driver, statusChangeClick, 120);

			if (verifyElementDisplayed(changeStatus)) {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				clickElementJSWithWait(changeStatus);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisibleIE(driver, voidClick, 120);

				if (verifyElementDisplayed(Void)) {

					clickElementJSWithWait(Void);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();

					waitUntilElementVisibleIE(driver, ReturnReport, 120);

					if (verifyElementDisplayed(ReturnReport)) {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID",
								Status.PASS, "Status Change Successful");
						ReturnReport.click();
						// clickElementJSWithWait(ReturnReport);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID",
								Status.FAIL, "Status Change UnSuccessful");
					}

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					waitUntilElementVisibleIE(driver, StatusVoid, 120);

					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'V')]"));

					voidListWVR.clear();
					voidListWVR = driver.findElements(VoidElementWVR);

					int voidCountAfter = voidListWVR.size();

					if ((list2.get(0).getText().contains("V")) && (voidCountAfter > voidCountBefore)) {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID",
								Status.PASS, "Contract Void is completed");
					} else {
						tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID",
								Status.FAIL, "Contract Void is not completed");

					}

					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			} else {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWVRMenuPage", "sp_New_contractSearch_and_VOID", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

	public void sp_Ex_contractSearchAndVoid() throws Exception {

		String fromDate = addDateInSpecificFormat("MM/dd/yyyy", -1);
		String[] arrDate = fromDate.split("/");
		String strMonthFromDrpDwn = arrDate[0];
		String strDateFromDrpDwn = arrDate[1];
		String strYearFromDrpDwn = arrDate[2];
		List<WebElement> voidList = null;

		int voidCountBefore = 0;

		if (strMonthFromDrpDwn.startsWith("0")) {
			strMonthFromDrpDwn = strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")) {
			strDateFromDrpDwn = strDateFromDrpDwn.replace("0", "");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, experienceSelect, 120);

		if (verifyElementDisplayed(experienceClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
					"Contract Management Page Displayed");

			new Select(driver.findElement(MonthFromDrpDwnWBW)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwnWBW)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwnWBW)).selectByValue(strYearFromDrpDwn);

			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(experienceClick);

			// clickElementJS(worldmarkClick);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), 120);

			if (verifyObjectDisplayed(VoidElement)) {

				voidList = driver.findElements(VoidElement);

				voidCountBefore = voidList.size();

			}

			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryPage, 120);

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(voidButton)) {

					clickElementJSWithWait(voidButton);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().alert().accept();

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Void')]"), 120);
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Void')]"));
					// List<WebElement> list2 =
					// driver.findElements(By.xpath("//tr/td[contains(.,'" +
					// strMemberNo + "')]"));

					voidList.clear();
					voidList = driver.findElements(VoidElement);

					int voidCountAfter = voidList.size();

					if ((list2.get(0).getText().contains("Void")) && (voidCountAfter > voidCountBefore)) {

						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
								"Contract Void");
					} else {

						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
								"Contract was not Void");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

	public void WorldMarkTourInformation() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourID, 120);

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();

			String createdTourid = EndToEndSalesForce.TourID;
			tourID.sendKeys(createdTourid);

			// String createdTouriD = "12452404";
			// tourID.sendKeys(createdTouriD);

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	public void SalePoint_ownership_Automation_Flow() throws Exception {

		int noOfSecOwnWBW = 0;
		List<WebElement> secOwnlistWBW = null;

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourID, 120);

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();

			String createdTourid = EndToEndSalesForce.TourID;
			tourID.sendKeys(createdTourid);

			// String createdTouriD = "55008266";
			// tourID.sendKeys(createdTouriD);

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
						"Automation Pop Up message is displayed");

			} catch (Exception e) {
				System.out.println("Automation Pop Up message is not displayed");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, saveContractClick, 120);

			// Edit the Owner

			if (verifyObjectDisplayed(editMemberWBW)) {

				driver.findElement(editMemberWBW).click();

				if (verifyObjectDisplayed(primaryOwnerPage)) {

					// ssn Field

					if (ssnWBWSelect.isEnabled() == false) {

						System.out.println("SSN Not Required User Can Proceed");

					} else {
						if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
							ssnWBWSelect.clear();
							ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
						}
					}

					if (homePhoneWBW.getAttribute("value").equals("")) {
						homePhoneWBW.sendKeys(testData.get("homeNumber"));

					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementJS(nextOwner);

				} else if (verifyElementDisplayed(driver.findElement(ownerDetailsPage))) {
					System.out.println("OwnerDetails Page Directly Opened");

					// check the secondary owner count with delete buttons

					if (verifyObjectDisplayed(deleteOwnerWBW)) {
						secOwnlistWBW = driver.findElements(deleteOwnerWBW);

						noOfSecOwnWBW = secOwnlistWBW.size();

					}

					// check primary owner

					if (verifyElementDisplayed(pNameWBW)) {

						// System.out.println("Primary Owner Exists");

						// delete all secondary owner
						for (int i = 0; i < noOfSecOwnWBW; i++) {

							clickElementJSWithWait(deleteFirstOwnerWBW);
							waitForSometime(tcConfig.getConfig().get("MedWait"));

							if (ExpectedConditions.alertIsPresent() != null) {
								try {

									driver.switchTo().alert().accept();

								} catch (Exception e) {
									System.out.println(e);
								}
							}
							waitForSometime(tcConfig.getConfig().get("MedWait"));

						}
					} else {
						System.out.println("Primary Owner Does not exist");

						// delete all secondary owner except one

						for (int i = 0; i < (noOfSecOwnWBW - 1); i++) {

							// clickElementJSWithWait(secOwnlist.get(0));
							clickElementJSWithWait(deleteListOfOwnerWBW);

							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						}

						// make one secondary to primary

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (verifyObjectDisplayed(MakePrimaryWBW)) {
							clickElementJSWithWait(MakePrimaryWBW);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}

					}

					if (verifyObjectDisplayed(editPOwnerWBW)) {

						clickElementJSWithWait(editPOwnerWBW);

						if (verifyObjectDisplayed(primaryOwnerPage)) {
							if (ssnWBWSelect.isEnabled() == false) {

								System.out.println("SSN Not Required User Can Proceed");

							} else {
								if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
									ssnWBWSelect.clear();
									ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
								}
							}

							if (homePhoneWBW.getAttribute("value").equals("")) {
								homePhoneWBW.sendKeys(testData.get("homeNumber"));

							}
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							clickElementJS(nextOwner);

						}

					}

				} else {
					System.out.println("No Primary owner edit button");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, nextOwner, 120);

				clickElementJS(nextOwner);

				waitUntilElementVisibleIE(driver, editPaymentWBW, 120);

				WebElement ele = driver.findElement(editPaymentWBW);
				clickElementJS(ele);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				//

				// waitUntilElementVisibleIE(driver, financePage, 120); Ajay
				// waitUntilElementVisibleIE(driver, wbwPaymentOption, 120);
				waitUntilElementVisible(driver, duepaymentMethod, 120);

				if (verifyElementDisplayed(duepaymentMethod)) {
					String payMethod = testData.get("PayMethod");
					duepaymentMethod.click();

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					driver.findElement(
							By.xpath("//select[contains(@id,'duesPACId')]/option[text()='" + payMethod + "']")).click();

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisible(driver, duesccNameWBW, 120);

					duesccNameWBW.click();
					duesccNameWBW.clear();
					duesccNameWBW.sendKeys(testData.get("CCName"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					duesccCity.click();
					duesccCity.clear();
					duesccCity.sendKeys(testData.get("City"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					duesccPostalCode.click();
					duesccPostalCode.clear();
					duesccPostalCode.sendKeys(testData.get("ZIP"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					duesacnumberWBW.click();
					duesacnumberWBW.clear();
					duesacnumberWBW.sendKeys(testData.get("CCNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					duesbankNameWBW.click();
					duesbankNameWBW.clear();
					duesbankNameWBW.sendKeys(testData.get("BankName"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					duessRoutinWBW.click();
					duessRoutinWBW.clear();
					duessRoutinWBW.sendKeys(testData.get("Routing"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				}

				/*
				 * // Ajay1 waitUntilElementVisible(driver, dueccNameWBW, 120);
				 * 
				 * dueccNameWBW.clear(); dueccNameWBW.click();
				 * dueccNameWBW.sendKeys(testData.get("CCName"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * driver.findElement(dueccTypeWBW).click(); String cardType =
				 * testData.get("CCType"); waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * driver.findElement(By.xpath(
				 * "//select[contains(@name,'duesCCPACInfo.cardType')]/option[contains(.,'" +
				 * cardType + "')]")) .click();
				 * 
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * dueccNumberWBW.clear(); dueccNumberWBW.click();
				 * dueccNumberWBW.sendKeys(testData.get("CCNumber"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * driver.findElement(dueccMonthWBW).click(); String expMnth =
				 * testData.get("CCExpMnth");
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * driver.findElement(By.xpath(
				 * "//select[contains(@name,'duesCCPACInfo.expireMonth')]/option[contains(.,'" +
				 * expMnth + "')]")) .click();
				 * 
				 * driver.findElement(dueccYearWBW).click(); String expYr =
				 * testData.get("CCExpYear");
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * driver.findElement(By.xpath(
				 * "//select[contains(@name,'duesCCPACInfo.expireYear')]/option[contains(.,'" +
				 * expYr + "')]")) .click();
				 * 
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * loanTab.click(); // waitUntilElementVisible(driver, loanPaymethod, 120);
				 * String payMethod = testData.get("PayMethod");
				 * 
				 * if (verifyElementDisplayed(loanPaymethod)) {
				 * 
				 * // loanPaymethod.click();
				 * 
				 * waitForSometime(tcConfig.getConfig().get("LowWait")); if
				 * (payMethod.equalsIgnoreCase("Auto Pay")) { loanPaymethod.click();
				 * driver.findElement(By.xpath(
				 * "(//select[contains(@id,'loanPACId')]/option[contains(.,'" + payMethod +
				 * "')])[2]")) .click();
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * waitUntilElementVisible(driver, loanccNameWBW, 120);
				 * 
				 * loanccNameWBW.clear(); loanccNameWBW.click();
				 * loanccNameWBW.sendKeys(testData.get("CCName"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * loanccCity.click(); loanccCity.click();
				 * loanccCity.sendKeys(testData.get("City"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * loanccPostalCode.click(); loanccPostalCode.click();
				 * loanccPostalCode.sendKeys(testData.get("ZIP"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * loanacnumberWBW.clear(); loanacnumberWBW.click();
				 * loanacnumberWBW.sendKeys(testData.get("CCNumber"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 * 
				 * loanbankNameWBW.clear(); loanbankNameWBW.click();
				 * loanbankNameWBW.sendKeys(testData.get("BankName"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait")); loansRoutinWBW.clear();
				 * loansRoutinWBW.click(); loansRoutinWBW.sendKeys(testData.get("Routing"));
				 * waitForSometime(tcConfig.getConfig().get("LowWait"));
				 */
				// Checkbox
				clickElementJS(duecccheckboxWBW);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}

				waitUntilElementVisibleIE(driver, financeNext, 120);
				clickElementJS(financeNext);

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("Alert not coming");
				}

				waitUntilElementVisibleIE(driver, saveContractClick, 120);

				if (verifyElementDisplayed(savecontractWBW)) {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Review Page Displayed and Save Button Clicked");

					clickElementJS(savecontractWBW);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("");
					}
				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Contract Save Page Navigation Error");

				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, generate, 120);

				if (verifyElementDisplayed(generate)) {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Review Page Displayed and Generate Button Clicked");
					// clickElementJSWithWait(generate);
					clickElementJS(generate);

				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Generate contract Page Navigation Error");
				}

				// waitUntilElementVisibleIE(driver,
				// By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"),
				// 120);
				waitUntilElementVisibleIE(driver, conNo, 120);

				if (verifyElementDisplayed(conNo)) {

					// testData.put("strContract", conNo.getText());
					strContractNo = conNo.getText();

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Created with number: " + conNo.getText());
				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Contract Creation failed ");
				}

				if (verifyElementDisplayed(memberNo)) {

					strMemberNo = memberNo.getText().split("\\:")[1].trim();
					System.out.println("MemberNo " + strMemberNo);

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Member No: " + strMemberNo);

				} else {

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Member number could not be retrieved");
				}

			}

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Automation Pop Up Not Available");

		}
	}

	public void SalePoint_Exp_Automation_Flow() throws Exception {

		int noOfSecOwnWBW = 0;
		List<WebElement> secOwnlistWBW = null;

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourID, 120);

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();

			String createdTourid = EndToEndSalesForce.TourID;
			// String createdTourid = "55006293";
			tourID.sendKeys(createdTourid);

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.alertIsPresent());

		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "SalePoint_Exp_Automation_Flow", Status.PASS,
						"Automation Pop Up message is displayed");

			} catch (Exception e) {
				System.out.println("Automation Pop Up message is not displayed");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, saveContractClick, 120);

			// Edit the Owner

			if (verifyObjectDisplayed(editMemberWBW)) {

				driver.findElement(editMemberWBW).click();

				if (verifyObjectDisplayed(primaryOwnerPage)) {

					// ssn Field

					if (ssnWBWSelect.isEnabled() == false) {

						System.out.println("SSN Not Required User Can Proceed");

					} else {
						if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
							ssnWBWSelect.clear();
							ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
						}
					}

					if (homePhoneWBW.getAttribute("value").equals("")) {
						homePhoneWBW.sendKeys(testData.get("homeNumber"));

					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					clickElementJS(nextOwner);

				} else if (verifyElementDisplayed(driver.findElement(ownerDetailsPage))) {
					System.out.println("OwnerDetails Page Directly Opened");

					// check the secondary owner count with delete buttons

					if (verifyObjectDisplayed(deleteOwnerWBW)) {
						secOwnlistWBW = driver.findElements(deleteOwnerWBW);

						noOfSecOwnWBW = secOwnlistWBW.size();

					}

					// check primary owner

					if (verifyElementDisplayed(pNameWBW)) {

						// System.out.println("Primary Owner Exists");

						// delete all secondary owner
						for (int i = 0; i < noOfSecOwnWBW; i++) {

							clickElementJSWithWait(deleteFirstOwnerWBW);
							waitForSometime(tcConfig.getConfig().get("MedWait"));

							if (ExpectedConditions.alertIsPresent() != null) {
								try {

									driver.switchTo().alert().accept();

								} catch (Exception e) {
									System.out.println(e);
								}
							}
							waitForSometime(tcConfig.getConfig().get("MedWait"));

						}
					} else {
						System.out.println("Primary Owner Does not exist");

						// delete all secondary owner except one

						for (int i = 0; i < (noOfSecOwnWBW - 1); i++) {

							// clickElementJSWithWait(secOwnlist.get(0));
							clickElementJSWithWait(deleteListOfOwnerWBW);

							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						}

						// make one secondary to primary

						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (verifyObjectDisplayed(MakePrimaryWBW)) {
							clickElementJSWithWait(MakePrimaryWBW);
							waitForSometime(tcConfig.getConfig().get("LowWait"));
							driver.switchTo().alert().accept();
							waitForSometime(tcConfig.getConfig().get("LowWait"));
						}

					}

					if (verifyObjectDisplayed(editPOwnerWBW)) {

						clickElementJSWithWait(editPOwnerWBW);

						if (verifyObjectDisplayed(primaryOwnerPage)) {
							if (ssnWBWSelect.isEnabled() == false) {

								System.out.println("SSN Not Required User Can Proceed");

							} else {
								if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
									ssnWBWSelect.clear();
									ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
								}
							}

							if (homePhoneWBW.getAttribute("value").equals("")) {
								homePhoneWBW.sendKeys(testData.get("homeNumber"));

							}
							waitForSometime(tcConfig.getConfig().get("LowWait"));

							clickElementJS(nextOwner);

						}

					}

				} else {
					System.out.println("No Primary owner edit button");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, nextOwner, 120);

				clickElementJS(nextOwner);

				waitUntilElementVisibleIE(driver, editPaymentWBW, 120);

				WebElement ele = driver.findElement(editPaymentWBW);
				clickElementJS(ele);

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				//

				// waitUntilElementVisibleIE(driver, financePage, 120); Ajay
				// waitUntilElementVisibleIE(driver, wbwPaymentOption, 120);
				waitUntilElementVisible(driver, contractPayMethod, 120);

				if (verifyElementDisplayed(contractPayMethod)) {

					contractPayMethod.click();
					String payMethod = testData.get("PayMethod");

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(
							By.xpath("//select[contains(@id,'loanPACId')]/option[contains(.,'" + payMethod + "')]"))
							.click();

				}

				waitUntilElementVisible(driver, ccNameWBW, 120);

				ccNameWBW.clear();
				ccNameWBW.click();
				ccNameWBW.sendKeys(testData.get("CCName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(ccTypeWBW).click();
				String cardType = testData.get("CCType");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(
						By.xpath("//select[contains(@name,'ccPacCardType')]/option[contains(.,'" + cardType + "')]"))
						.click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				ccNumberWBW.clear();
				ccNumberWBW.click();
				ccNumberWBW.sendKeys(testData.get("CCNumber"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.findElement(ccMonthWBW).click();
				String expMnth = testData.get("CCExpMnth");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By.xpath(
						"//select[contains(@name,'ccPacCardExpiresMonth')]/option[contains(.,'" + expMnth + "')]"))
						.click();

				driver.findElement(ccYearWBW).click();
				String expYr = testData.get("CCExpYear");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.findElement(By
						.xpath("//select[contains(@name,'ccPacCardExpiresYear')]/option[contains(.,'" + expYr + "')]"))
						.click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisibleIE(driver, financeNext, 120);
				clickElementJS(financeNext);

				waitUntilElementVisibleIE(driver, saveContractClick, 120);

				if (verifyElementDisplayed(savecontractWBW)) {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Review Page Displayed and Save Button Clicked");

					clickElementJS(savecontractWBW);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					try {

						driver.switchTo().alert().accept();

					} catch (Exception e) {
						System.out.println("");
					}
				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Contract Save Page Navigation Error");

				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, generate, 120);

				if (verifyElementDisplayed(generate)) {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Review Page Displayed and Generate Button Clicked");
					// clickElementJSWithWait(generate);
					clickElementJS(generate);

				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Generate contract Page Navigation Error");
				}

				// waitUntilElementVisibleIE(driver,
				// By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"),
				// 120);
				waitUntilElementVisibleIE(driver, conNo, 120);

				if (verifyElementDisplayed(conNo)) {

					// testData.put("strContract", conNo.getText());
					strContractNo = conNo.getText();

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Contract Created with number: " + conNo.getText());
				} else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Contract Creation failed ");
				}

				if (verifyElementDisplayed(memberNo)) {

					strMemberNo = memberNo.getText().split("\\:")[1].trim();
					System.out.println("MemberNo " + strMemberNo);

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
							"Member No: " + strMemberNo);

				} else {

					tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
							"Member number could not be retrieved");
				}

			}

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Automation Pop Up Not Available");

		}

	}

	public void contractSearchandFinalize() throws Exception {

		String fromDate = addDateInSpecificFormat("MM/dd/yyyy", -1);
		String[] arrDate = fromDate.split("/");
		String strMonthFromDrpDwn = arrDate[0];
		String strDateFromDrpDwn = arrDate[1];
		String strYearFromDrpDwn = arrDate[2];
		List<WebElement> voidList = null;

		int voidCountBefore = 0;

		if (strMonthFromDrpDwn.startsWith("0")) {
			strMonthFromDrpDwn = strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")) {
			strDateFromDrpDwn = strDateFromDrpDwn.replace("0", "");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, experienceSelect, 120);

		if (verifyElementDisplayed(experienceClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
					"Contract Management Page Displayed");

			new Select(driver.findElement(MonthFromDrpDwnWBW)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwnWBW)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwnWBW)).selectByValue(strYearFromDrpDwn);

			/* changes By Utsav */

			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			// Changed by ajay
			// clickElementJS(experienceClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), 20);
			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryPage, 120);

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(signatureDate)) {
					String pattern = "MM/dd/yyyy";
					String dateInString = new SimpleDateFormat(pattern).format(new Date());
					clickElementJS(signatureDate);

					driver.findElement(By.xpath(
							"//select[contains(@name,'signatureDate')]/option[contains(.,'" + dateInString + "')]"))
							.click();
				}

				if (verifyElementDisplayed(finalizeButton)) {

					clickElementJSWithWait(finalizeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					// driver.switchTo().activeElement().sendKeys(Keys.ENTER);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Finalize')]"), 120);
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));

					if (list2.get(0).getText().contains("Finalize")) {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
								"Contract Finalized");
					} else {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
								"Contract did notr finalize");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

	public void contractSearchandTransmit() throws Exception {

		List<WebElement> voidListWVR = null;

		int voidCountBefore = 0;

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("urlContractInBatchReport"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, selectAllByDate, 120);

		if (verifyElementDisplayed(selectAllByDate)) {

			tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractSearch_and_Transmit", Status.PASS,
					"Contract in batch report Page Displayed");

			clickElementJS(selectAllByDate);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// LocalDate today= LocalDate.now();
			Calendar cal = Calendar.getInstance();
			DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

			Date today = cal.getTime();
			// cal.MONTH;

			int monthVal = (cal.get(Calendar.MONTH));

			String strMonth = String.valueOf(monthVal).trim();
			String strDate = String.valueOf(cal.get(Calendar.DAY_OF_MONTH)).trim();

			waitUntilElementVisibleIE(driver, By.name("startmonth"), 120);

			new Select(driver.findElement(By.name("startmonth"))).selectByValue(strMonth);

			new Select(driver.findElement(By.name("startday"))).selectByValue(strDate);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(getData);

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"), 120);

			if (verifyObjectDisplayed(VoidElementWVR)) {

				voidListWVR = driver.findElements(VoidElementWVR);

				voidCountBefore = voidListWVR.size();

			}

			if (verifyElementDisplayed(
					driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointContractSeacrh", "contractSearch_and_Transmit", Status.PASS,
						"Contract No Verified and is: " + strContractNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strContractNo + "')]"));

				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointContractSeacrh", "contractSearch_and_Transmit", Status.FAIL,
						"Contract No Not Verified");

			}

			waitUntilElementVisibleIE(driver, statusChangeClick, 120);

			if (verifyElementDisplayed(changeStatus)) {
				tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractSearch_and_Transmit", Status.PASS,
						"Summary Page Navigation Successful and Verified");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(changeStatus);

				waitUntilElementVisibleIE(driver, Transmit, 120);

				if (verifyElementDisplayed(Transmit)) {

					clickElementJSWithWait(Transmit);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("SalePointWVRMenuPage", "contractSearch_and_Transmit", Status.PASS,
							"Contract transmitted successfully");
				}

			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "contractSearch_and_Transmit", Status.FAIL,
					"Contract not transmitted successfully");
		}

	}

	public void manualUpgradeWBW() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, txtmemberNo, 120);

		if (verifyElementDisplayed(txtmemberNo)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.PASS,
					"Upgrade Page Navigation Succesfull");
			clickElementJS(txtmemberNo);

			txtmemberNo.sendKeys(testData.get("memberNumber"));
			clickElementJS(Btnsearch);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.PASS,
							"Deliquency Pop Up message is displayed");

				} catch (Exception e) {
					System.out.println("Deliquency Pop Up message is not displayed");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.FAIL,
						"Deliquency Pop Up message is not displayed");

			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			String createdTourid = "57522538";
			// String createdTourid = EndToEndSalesForce.TourID;
			clickElementJSWithWait(txtTsc);
			txtTsc.sendKeys(createdTourid);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(next1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			checkPrimaryandSecondaryOwner();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(nextBtn1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(next1);
			waitUntilElementVisible(driver, loanTab, 120);
			clickElementJSWithWait(loanTab);
			String payMethod = testData.get("PayMethod");

			if (verifyElementDisplayed(loanPaymethod)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (payMethod.equalsIgnoreCase("Auto Pay")) {
					loanPaymethod.click();
					driver.findElement(By
							.xpath("(//select[contains(@id,'loanPACId')]/option[contains(.,'" + payMethod + "')])[2]"))
							.click();

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisible(driver, loanccNameWBW, 120);

					loanccNameWBW.clear();
					loanccNameWBW.click();
					loanccNameWBW.sendKeys(testData.get("CCName"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					loanccCity.click();
					loanccCity.click();
					loanccCity.sendKeys(testData.get("City"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					loanccPostalCode.click();
					loanccPostalCode.click();
					loanccPostalCode.sendKeys(testData.get("ZIP"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					loanacnumberWBW.clear();
					loanacnumberWBW.click();
					loanacnumberWBW.sendKeys(testData.get("CCNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					loanbankNameWBW.clear();
					loanbankNameWBW.click();
					loanbankNameWBW.sendKeys(testData.get("BankName"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					loansRoutinWBW.clear();
					loansRoutinWBW.click();
					loansRoutinWBW.sendKeys(testData.get("Routing"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else if (payMethod.equalsIgnoreCase("CC Auto Pay")) {

					loanPaymethod.click();
					driver.findElement(By.xpath(
							"//select[contains(@name,'contract.PACId')]/option[contains(.,'" + payMethod + "')]"))
							.click();

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisible(driver, loanacNameWBW, 120);

					loanacNameWBW.clear();
					loanacNameWBW.click();
					loanacNameWBW.sendKeys(testData.get("CCName"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					loanccNumber.click();
					loanccNumber.clear();
					loanccNumber.sendKeys(testData.get("CCNumber"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					String cardType = testData.get("CCType");
					loanccCardType.click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath("//select[contains(@name,'loanCCPACInfo.cardType')]/option[contains(.,'"
							+ cardType + "')]")).click();

					String expMnth = testData.get("CCExpMnth");
					loanccExMonth.click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(
							By.xpath("//select[contains(@name,'loanCCPACInfo.expireMonth')]/option[contains(.,'"
									+ expMnth + "')]"))
							.click();

					String expYr = testData.get("CCExpYear");
					loanccExYear.click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(By.xpath(
							"//select[contains(@name,'loanCCPACInfo.expireYear')]/option[contains(.,'" + expYr + "')]"))
							.click();

					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}

			}

			clickElementJSWithWait(nextVal);

			if (ExpectedConditions.alertIsPresent() != null) {
				try {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.switchTo().alert().accept();

				} catch (Exception e) {
					log.info(e);
				}

			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, SellingRep, 120);
			SellingRep.clear();
			SellingRep.click();
			SellingRep.sendKeys(testData.get("SalesAssociate"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(nextVal);
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			waitUntilElementVisibleIE(driver, savecontractWBW, 120);

			if (verifyElementDisplayed(savecontractWBW)) {
				clickElementJSWithWait(savecontractWBW);
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println("");
				}
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.FAIL,
						"Contract Save Page Navigation Error");

			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, generate, 120);

			if (verifyElementDisplayed(generate)) {
				clickElementJS(generate);
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.PASS,
						"Contract Review Page Displayed and Generate Button Clicked");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(printOption)) {
					selectByIndex(printOption, 2);
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.PASS,
							"Selected Print PDF Option");
					clickElementJS(generate);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.FAIL,
						"Generate contract Page Navigation Error");
			}
			waitUntilElementVisibleIE(driver, conNo, 120);

			if (verifyElementDisplayed(conNo)) {

				strContractNo = conNo.getText();

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				System.out.println("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "manualUpgradeWBW", Status.FAIL,
						"Member number could not be retrieved");
			}

		}
	}

	public void viewTradeContract() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tradeContract, 120);

		driver.findElement(tradeContract).click();
		driver.findElement(tradeContract).sendKeys(testData.get("ContractNum"));
		driver.findElement(nextValue).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}
		}
	}

	public void viewTradedContract() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, viewtradeContracts, 120);

		driver.findElement(viewtradeContracts).click();

	}

	public void processTradeContract() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, processTrade, 120);

		driver.findElement(processTrade).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (ExpectedConditions.alertIsPresent() != null) {
			try {

				driver.switchTo().alert().dismiss();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				System.out.println("no Alert is displayed");
			}
		}
	}

	public void editPrimaryOwnerfromsummary() throws Exception {

		waitUntilElementVisibleIE(driver, savecontract, 120);

		tcConfig.updateTestReporter("EndToEndSalePoint", "editPrimaryOwnerfromsummary", Status.PASS,
				"Sucessfully navigated to Save Contract page as automation popup appeared");

		if (verifyObjectDisplayed(editprimaryOwner)) {

			driver.findElement(editprimaryOwner).click();

			if (verifyObjectDisplayed(editprimaryOwnerPage)) {

				new Select(driver.findElement(relationshipStatus)).selectByIndex(2);
				new Select(driver.findElement(maritalStatus)).selectByIndex(2);

				// ssn Field

				if (ssnWVRSelect.isEnabled() == false) {

					System.out.println("SSN Not Required User Can Proceed");

				} else {
					if (ssnWVRSelect.getAttribute("value").isEmpty() == true) {
						ssnWVRSelect.clear();
						ssnWVRSelect.sendKeys(testData.get("ssnNumber"));
					}
				}

				if (homePhoneWVR.getAttribute("value").equals("")) {
					homePhoneWVR.sendKeys(testData.get("homeNumber"));

				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// DateOf Birth

				if (driver.findElement(MonthFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(MonthFromDrpDwn)).selectByIndex(2);

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(DateFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(DateFromDrpDwn)).selectByIndex(2);

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.findElement(YearFromDrpDwn).isEnabled()) {
					new Select(driver.findElement(YearFromDrpDwn)).selectByIndex(40);

				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				WebElement ele = driver.findElement(genderSelect);
				clickElementJS(ele);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				clickElementJS(updateButton);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			checkPrimaryandSecondaryOwner();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

	public void LoanAutoPay() throws Exception {
		try {
			waitUntilElementVisibleIE(driver, loan_edit, 120);

			if (verifyElementDisplayed(loan_edit)) {
				// loan_edit.click();
				clickElementJSWithWait(loan_edit);

				tcConfig.updateTestReporter("SalePointLoginPage", "loan_edit", Status.PASS,
						"Clicked on Loan_Autopay edit button");
			} else {
				tcConfig.updateTestReporter("SalePointLoginPage", "loan_edit", Status.PASS,
						"Failed while clicking on Loan_Autopay edit button");
			}
			/*
			 * if (verifyElementDisplayed(loan_cardname)) { loan_cardname.click();
			 * loan_cardname.sendKeys(testData.get("CCName")); loan_cardaccount.click();
			 * loan_cardaccount.sendKeys(testData.get("CCNumber")); String expMnth =
			 * testData.get("CCExpMnth"); driver.findElement(ccMonth).click();
			 * waitForSometime(tcConfig.getConfig().get("LowWait")); driver.findElement(
			 * By.xpath("//select[@name='cc_payment_exp_month']/option[contains(.,'" +
			 * expMnth + "')]")) .click();
			 * 
			 * driver.findElement(ccYear).click(); String expYr = testData.get("CCExpYear");
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * driver.findElement(
			 * By.xpath("//select[@name='cc_payment_exp_year']/option[contains(.,'" + expYr
			 * + "')]")).click();
			 * 
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 * 
			 * }
			 */
			clickElementJS(nextVal);
			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					System.out.println(e);
				}
			}
		} catch (Exception e) {

		}
	}

	public void LogoutSP() throws Exception {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", LogoutBtnSP);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(buttonLogIn)) {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL, "Log Out Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL, "Log Out Failed" + e.getMessage());
		}
		// driver.close();
		// driver.quit();
	}
}
