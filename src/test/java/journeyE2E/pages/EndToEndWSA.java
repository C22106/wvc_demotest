package journeyE2E.pages;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

//import com.relevantcodes.extentreports.DBReporter.Config;
import com.aventstack.extentreports.Status;
import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class EndToEndWSA extends EndToEndBasePage {

	public static final Logger log = Logger.getLogger(EndToEndWSA.class);
	ReadConfigFile config = new ReadConfigFile();
	public String selectedCustName;
	public static String createdPitchID;
	public static String customerLastName;
	public static String contractNum;

	public EndToEndWSA(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@id='fldUsername']")
	WebElement textUsername;

	/*
	 * enter user name
	 */
	private void setUserName(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsername, 10);
		textUsername.sendKeys(strUserName);
		tcConfig.updateTestReporter("WSALoginPage", "setUserName", Status.PASS, "Username is entered for WSA Login");
	}

	@FindBy(xpath = "//input[@id='fldPassword']")
	WebElement textPassword;

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	private void setPassword(String strPassword) throws Exception {
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		textPassword.sendKeys(dString);
		tcConfig.updateTestReporter("WSALoginPage", "setPassword", Status.PASS, "Password is entered for WSA Login");
	}

	@FindBy(xpath = "//button[@name='cn-submit']")
	WebElement buttonLogIn;
	// @FindBy(xpath="//label[text()='Manage Inventory Availability']")
	// WebElement linkMangInv;

	/*
	 * Launch WSA application
	 */

	public void launchApp(String strBrowser) {

		// this.driver = checkAndInitBrowser(strBrowser);
		String url = tcConfig.getConfig().get("WSA_URL").trim();
		driver.navigate().to(url);
		driver.manage().window().maximize();
	}

	@FindBy(xpath = "//img[@title='Change Sales Site']")
	WebElement changeSiteicon;
	@FindBy(xpath = "//div[@id='page-top-utility-item-container']//strong[2]")
	WebElement siteEntityID;
	@FindBy(xpath = "//input[@id='selectedServiceEntity001']")
	WebElement entity001;
	@FindBy(xpath = "//button[@id='submit-btn']")
	WebElement saveBtn;
	@FindBy(xpath = "//div[@id='errorPopup']//img")
	WebElement closePopup;

	/*
	 * Login WSA application
	 */
	public void loginToWSAApp(String strBrowser) throws Exception {

		launchApp(strBrowser);
		String userid = tcConfig.getConfig().get("WSA_UserID");
		String password = tcConfig.getConfig().get("WSA_Password");
		setUserName(userid);
		setPassword(password);
		buttonLogIn.click();
		waitForSometime(tcConfig.getConfig().get("LongWait"));
	}

	@FindBy(xpath = "//a[text()='Logout']")
	WebElement LogoutBtn;
	@FindBy(xpath = "//div[@class='msg inform']")
	WebElement logoutMsg;
	@FindBy(xpath = "//img[@title='Home']")
	WebElement homeIcon;

	public void logoutfromWSAapp() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		// waitUntilObjectVisible(driver, LogoutBtn, 120);
		try {
			LogoutBtn.click();
			waitUntilElementVisible(driver, textUsername, 120);
			tcConfig.updateTestReporter("WSALoginPage", "logOut", Status.PASS, "Log Out Succesful");
		} catch (Exception e) {
			tcConfig.updateTestReporter("WSALoginPage", "logOut", Status.FAIL, "Log Out Failed" + e.getMessage());
		}
	}

	// @FindBy(xpath = "//img[@title='Change Sales Site']")WebElement
	// changeSiteicon;
	@FindBy(xpath = "//button[@name='cs-submit']")
	WebElement confirmOKBtn;
	// @FindBy(xpath = "//button[@id='submit-btn']") WebElement saveBtn;
	@FindBy(xpath = "//p[text()='Your site was updated successfully.']")
	WebElement txtSuccessMsg;

	public final By radioBtnWVRServiceEntity = By.xpath("(//input[@name='selectedServiceEntity'])[1]");
	public final By radioBtnWBWServiceEntity = By.xpath("(//input[@name='selectedServiceEntity'])[2]");
	public final By radioBtnWVRAPServiceEntity = By.xpath("(//input[@name='selectedServiceEntity'])[3]");
	public final By siteDropDown = By.xpath("//select[@id='selectedSite']");
	public final By closePopupIcon = By.xpath("(//img[@alt='Close this window'])[2]");
	public final By PopupClose = By.xpath("(//img[@alt='Close this window'])[6]");

	/*
	 * function name: selectEntity() Purpose : Set Service entity from homepage
	 */

	public void selectEntity() throws Exception {
		waitUntilObjectVisible(driver, changeSiteicon, 120);
		if (verifyElementDisplayed(changeSiteicon)) {
			changeSiteicon.click();
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity()", Status.PASS, "Sales Site Icon Clicked");
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity()", Status.FAIL, "Sales Site Icon Not Displayed");
		}

		if (verifyElementDisplayed(confirmOKBtn)) {
			confirmOKBtn.click();
		}
		waitUntilObjectVisible(driver, radioBtnWVRServiceEntity, 120);
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")) {
			driver.findElement(radioBtnWVRServiceEntity).click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			driver.findElement(radioBtnWBWServiceEntity).click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVRAP")) {
			driver.findElement(radioBtnWVRAPServiceEntity).click();
		}
		new Select(driver.findElement(siteDropDown)).selectByVisibleText(testData.get("SiteName"));
		saveBtn.click();
		waitUntilObjectVisible(driver, closePopupIcon, 120);
		if (verifyElementDisplayed(txtSuccessMsg)) {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity()", Status.PASS,
					"Site changed to " + testData.get("ServiceEntity") + " success message is diplayed properly");
			driver.findElement(closePopupIcon).click();
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity()", Status.FAIL,
					"Site changed to " + testData.get("ServiceEntity") + " success message is Not Displayed");
		}
	}

	/*
	 * function name: pitchTypeSelection() Purpose : Navigate to pitchtype -->
	 * subPitchType e.g New Propsal ---> UDI etc.
	 */

	public void pitchTypeSelection() throws Exception {

		final WebElement pitchType = driver.findElement(By.xpath("//a[text()='" + testData.get("PitchType") + "']"));
		pitchType.click();
		// Actions action=new Actions(driver);
		// action.moveToElement(pitchType).click().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		final WebElement subPitchType = driver.findElement(By.xpath(" //a[text()='" + testData.get("PitchType")
				+ "']/following-sibling::ul/descendant::a[contains(text(),'" + testData.get("SubPitchType") + "')]"));
		// subPitchType.click();
		if (verifyElementDisplayed(subPitchType)) {
			subPitchType.click();
			tcConfig.updateTestReporter("WSAHomePage", "navigateToPitchCreation()", Status.PASS,
					testData.get("SubPitchType") + " selected from " + testData.get("PitchType") + " menu");
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "navigateToPitchCreation()", Status.FAIL,
					testData.get("SubPitchType") + " NOT selected from " + testData.get("PitchType") + " menu");
		}
	}

	public void clickonTrade() throws Exception {

		final WebElement pitchType = driver.findElement(By.xpath("//a[text()='" + testData.get("PitchType") + "']"));
		pitchType.click();

	}

	@FindBy(name = "tourNumber")
	WebElement txtTourNumber;
	@FindBy(xpath = "//button[@name='cn-submit']")
	WebElement submitBtn;
	@FindBy(xpath = "//table[@id='membersTable']/tbody/tr/td[7]/a[1]")
	WebElement searchBtn;
	@FindBy(xpath = "(//table[@id='memberContractsTable']/tbody/tr/td[10]/a)[1]")
	WebElement addTocontractbtn;

	@FindBy(xpath = "//button[@name='btn-continue']")
	WebElement continueBtn;
	@FindBy(xpath = "//button[@name='btn-existingContinue']")
	WebElement existingContinueBtn;
	@FindBy(xpath = "//button[@name='btn-newOwnerContinue']")
	WebElement newOwnerContinueBtn;
	@FindBy(xpath = "//button[@name='btn-discoveryContinue']")
	WebElement discontinueBtn;
	@FindBy(xpath = "//input[@name='memberNumber']")
	WebElement txtMemberNumber;
	@FindBy(xpath = "//h1[contains(text(),'Co-owners Selection')]")
	WebElement coOwnerSelectionPageHeader;
	@FindBy(xpath = "//h1[contains(text(),'Add New Owners')]")
	WebElement addOwnersHeader;

	public final By customerNametxt = By.xpath("(//form[@id='resultsForm']//p)[1]");
	public final By selectCustRadio = By.xpath("(//form[@id='resultsForm']//input[@type='radio'])[1]");

	/*
	 * function name: tourLookUp() Purpose : Search by tour number and navigate to
	 * pitch creation page
	 */

	public void tourLookUp() throws Exception {
		waitUntilElementVisible(driver, txtTourNumber, 120);
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("UDI")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				String createdTourid = EndToEndSalesForce.TourID;
				// String createdTourid= "57519875";
				txtTourNumber.sendKeys(createdTourid);
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);
			selectedCustName = driver.findElement(customerNametxt).getText();
			driver.findElement(selectCustRadio).click();
			continueBtn.click();

			waitUntilElementVisible(driver, txtMemberNumber, 120);
			continueBtn.click();
			waitUntilElementVisible(driver, coOwnerSelectionPageHeader, 120);
			existingContinueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, 120);
			newOwnerContinueBtn.click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			if (verifyElementDisplayed(txtTourNumber)) {

				String createdTourid = EndToEndSalesForce.TourID;
				// String createdTourid= "57519875";
				txtTourNumber.sendKeys(createdTourid);

				// String createdTourID= "55008266";
				// txtTourNumber.sendKeys(createdTourID);

				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);
			selectedCustName = driver.findElement(customerNametxt).getText();
			driver.findElement(selectCustRadio).click();
			continueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, 120);
			newOwnerContinueBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				String createdTourid = EndToEndSalesForce.TourID;
				// String createdTourid= "12444513";
				txtTourNumber.sendKeys(createdTourid);
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);
			driver.findElement(selectCustRadio).click();
			continueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, 120);
			newOwnerContinueBtn.click();
		}
	}

	public void selectEntityWSA() throws Exception {
		if (verifyElementDisplayed(changeSiteicon)) {
			changeSiteicon.click();
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA()", Status.PASS, "Sales Site Icon Clicked");
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA()", Status.FAIL,
					"Sales Site Icon Not Displayed");
		}

		if (verifyElementDisplayed(confirmOKBtn)) {
			confirmOKBtn.click();
		}
		waitUntilObjectVisible(driver, radioBtnWVRServiceEntity, 120);
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")) {
			driver.findElement(radioBtnWVRServiceEntity).click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			driver.findElement(radioBtnWBWServiceEntity).click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVRAP")) {
			driver.findElement(radioBtnWVRAPServiceEntity).click();
		}
		new Select(driver.findElement(siteDropDown)).selectByVisibleText(testData.get("SiteName"));
		saveBtn.click();
		waitUntilObjectVisible(driver, closePopupIcon, 120);
		if (verifyElementDisplayed(txtSuccessMsg)) {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA()", Status.PASS,
					"Site changed to " + testData.get("ServiceEntity") + " success message is diplayed properly");
			driver.findElement(closePopupIcon).click();
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntityWSA()", Status.FAIL,
					"Site changed to " + testData.get("ServiceEntity") + " success message is Not Displayed");
		}
	}

	@FindBy(name = "memberNumber")
	WebElement txtMemmberNumber;
	@FindBy(name = "searchType")
	WebElement dropsearchType;
	protected By customerList = By.xpath("//form[@id = 'resultsFormNew']//div[contains(@id,'container')]");
	@FindBy(xpath = "(//input[@name = 'customerIndex'])[1]")
	WebElement firstCustomer;
	@FindBy(xpath = "//button[@id = 'btn-continue']")
	WebElement customerContinue;

	public void tourLookupUpgradeconvertFlow() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilElementVisible(driver, txtTourNumber, 120);
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("UDI")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				String createdTourid = EndToEndSalesForce.TourID;
				// String createdTourid= "57519919";
				txtTourNumber.sendKeys(createdTourid);
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookupUpgradeconvertFlow", Status.PASS,
						"Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookupUpgradeconvertFlow", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();

			// waitUntilElementVisible(driver, txtMemmberNumber, 120);
			// txtMemmberNumber.sendKeys(testData.get("memberNumber"));
			// submitBtn.click();

			waitUntilElementVisible(driver, continueBtn, 120);
			selectedCustName = driver.findElement(customerNametxt).getText();
			driver.findElement(selectCustRadio).click();
			continueBtn.click();

			waitUntilElementVisible(driver, txtMemberNumber, 120);
			txtMemmberNumber.sendKeys(testData.get("memberNumber"));
			submitBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(PopupClose)) {
				tcConfig.updateTestReporter("WSAHomePage", "tourLookupUpgradeconvertFlow", Status.PASS,
						"Site changed to " + testData.get("ServiceEntity") + " success message is diplayed properly");
				driver.findElement(PopupClose).click();
			} else {
				tcConfig.updateTestReporter("WSAHomePage", "tourLookupUpgradeconvertFlow", Status.FAIL,
						"Site changed to " + testData.get("ServiceEntity") + " success message is Not Displayed");
			}

			searchBtn.click();
			addTocontractbtn.click();
			continueBtn.click();
			waitUntilElementVisible(driver, discontinueBtn, 120);
			discontinueBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);
			continueBtn.click();
			waitUntilElementVisible(driver, newOwnerContinueBtn, 120);
			newOwnerContinueBtn.click();
//			waitUntilElementVisible(driver, coOwnerSelectionPageHeader, 120);
//			existingContinueBtn.click();
//			waitUntilElementVisible(driver, addOwnersHeader, 120);
//			newOwnerContinueBtn.click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			// System.out.println("Convert");

			WebElement convertTab = driver.findElement(By.xpath("//a[text()='" + testData.get("PitchType") + "']"));
			convertTab.click();
			if (verifyElementDisplayed(txtTourNumber)) {

				String createdTourid = EndToEndSalesForce.TourID;
				// String createdTourid= "57519919";
				txtTourNumber.sendKeys(createdTourid);

				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();

			waitUntilElementVisible(driver, txtMemmberNumber, 120);
			txtMemmberNumber.sendKeys(testData.get("memberNumber"));
			submitBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			try {
				if (driver.findElements(customerList).size() > 0) {
					firstCustomer.click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					customerContinue.click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			} catch (Exception e) {
				log.info(e);
			}

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				String createdTourid = EndToEndSalesForce.TourID;
				// String createdTourid= "12444513";
				txtTourNumber.sendKeys(createdTourid);
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "TourLookUp()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);
			driver.findElement(selectCustRadio).click();
			continueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, 120);
			newOwnerContinueBtn.click();
		}
	}

	@FindBy(xpath = "//a[text()='Commissions']")
	WebElement commisionsLink;
	@FindBy(xpath = "//input[@id='salesAssociate']")
	WebElement salesAssociatetxt;
	@FindBy(xpath = "//input[@id='sellingRep']")
	WebElement salesRepresentativetxt;
	@FindBy(xpath = "//button[@name='commission-done']")
	WebElement comissionDoneBtn;
	@FindBy(xpath = "//a[text()='Select Inventory']")
	WebElement linkSelectInv;
	@FindBy(xpath = "//input[@id='newPoints1']")
	WebElement newPointstxt;
	@FindBy(xpath = "//button[@name='inventory-done']")
	WebElement inventoryDoneBtn;
	@FindBy(xpath = "(//a[contains(text(),'Click to adjust')])[1]")
	WebElement adjustLink;
	@FindBy(xpath = "//button[@id='band-submit1']")
	WebElement paymentDoneBtn;
	@FindBy(xpath = "(//button[@id='band-submit'])[6]")
	WebElement paymentSubmitBtn;

	@FindBy(xpath = "//div[@id='requiredDownPayment1']")
	WebElement txtRequiredAmt;
	@FindBy(xpath = "//div[@id='requiredToQualify1']")
	WebElement txtRequiredQlfyAmt;

	@FindBy(xpath = "//input[@id='paymentTypeForm_offer1_downPaymentList_0__dpAmount']")
	WebElement txtAmountLine1;

	@FindBy(xpath = "//input[@id='paymentTypeForm_offer1_downPaymentList_1__dpAmount']")
	WebElement txtAmountLine2;
	@FindBy(xpath = "//button[@name='final-offer']")
	WebElement finalBtn;
	@FindBy(xpath = "//button[@name='finalizeSummary']")
	WebElement finalBtnWBW;
	@FindBy(xpath = "//input[@type='radio' and @value='ACHAutoPay']")
	WebElement ACHautoPayRadio;

	public final By titleDrpDwn = By.xpath("//select[@name='titleOption']");
	public final By depositTypeDropDown = By
			.xpath("//select[@id='paymentTypeForm_offer1_downPaymentList_1__paymentTypeName']");
	public final By selectFlagRadioBtn = By.xpath("(//input[@name='flag'])[1]");
	public final By selectFlagRadioBtnWBW = By.xpath("(//input[@name='flagForWorksheet1'])[1]");
	public final By CustNameTxt = By.xpath("//h2[contains(text(),'Name:')]");
	public final By pitchNumberHeader = By.xpath("//div[@id='page-body-container']//h1");
	public final By invPhase1 = By.xpath("//select[@id='inventoryId1']");

	/*
	 * function name: createNewPitch() Purpose : Fill up all the details and create
	 * a new pitch for various types of entity
	 */

	public void createNewPitch() throws Exception {
		double amountone, amounttwo;

		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("UDI")) {
			System.out.println("Entered create New Pitc");
			waitUntilElementVisible(driver, commisionsLink, 120);
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Sales Associate number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyElementDisplayed(linkSelectInv)) {
				linkSelectInv.click();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Select inventory link is displayed");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Select inventory link is NOT displayed");
			}

			if (testData.get("Trade").equalsIgnoreCase("Y")) {
				driver.findElement(invPhase1).click();
				WebElement invPhaseDrpDwn = driver
						.findElement(By.xpath("(//select[@id='inventoryId1']/option[contains(text(),'"
								+ testData.get("invPhaseWSA") + "')])[1]"));
				invPhaseDrpDwn.click();
			}

			newPointstxt.clear();
			newPointstxt.sendKeys(testData.get("Points"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Points entered");
			inventoryDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(titleDrpDwn)) {
				// new
				// Select(driver.findElement(titleDrpDwn)).selectByVisibleText("Single
				// Man");
				new Select(driver.findElement(titleDrpDwn)).selectByIndex(1);

				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Title Displayed");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Title NOT displayed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Adjust link not displayed");
			}

			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitUntilObjectVisible(driver, pitchNumberHeader, 120);
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails = pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails = newheaderdetails.replace("-1 Selected", "");
				createdPitchID = headerdetails.trim();
				String CustName = driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Pitch # " + headerdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"No pitch number is generated");
			}

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			waitUntilElementVisible(driver, commisionsLink, 120);
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesRepresentativetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Sales Representative number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();

			// ajay
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement paymentMethodBtn = driver.findElement(
					By.xpath("//input[@type='radio' and @value='" + testData.get("wsaPaymentMethod") + "']"));
			paymentMethodBtn.click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredQlfyAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);

			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Adjust link not displayed");
			}

			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSubmitBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Payment done button clicked");

			waitUntilObjectVisible(driver, selectFlagRadioBtnWBW, 120);

			driver.findElement(selectFlagRadioBtnWBW).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtnWBW.click();
			waitUntilObjectVisible(driver, pitchNumberHeader, 120);
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails = pageheaderdetails.replace("Final Summary for Reference # ", "");
				createdPitchID = newheaderdetails.trim();
				String CustName = driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Pitch # " + newheaderdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"No pitch number is generated");
			}
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			waitUntilElementVisible(driver, commisionsLink, 120);
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Sales Associate number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Adjust link not displayed");
			}
			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			waitUntilObjectVisible(driver, pitchNumberHeader, 240);
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails = pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails = newheaderdetails.replace("-1 Selected", "");
				createdPitchID = headerdetails.trim();
				String CustName = driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Pitch # " + headerdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"No pitch number is generated");
			}
		}
	}

	// ***********************************************
	@FindBy(xpath = "//input[@id='fldUsername']")
	WebElement textUsernameWSA;

	/*
	 * enter user name
	 */
	private void setUserNameWSA(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsernameWSA, 120);
		textUsernameWSA.sendKeys(strUserName);
		tcConfig.updateTestReporter("WSALoginPage", "setUserName", Status.PASS, "Username is entered for WSA Login");
	}

	@FindBy(xpath = "//input[@id='fldPassword']")
	WebElement textPasswordWSA;

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	private void setPasswordWSA(String strPassword) throws Exception {
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");

		textPasswordWSA.sendKeys(dString);
		tcConfig.updateTestReporter("WSALoginPage", "setPassword", Status.PASS, "Password is entered for WSA Login");
	}

	@FindBy(xpath = "//button[@name='cn-submit']")
	WebElement buttonLogInWSA;
	// @FindBy(xpath="//label[text()='Manage Inventory Availability']") WebElement
	// linkMangInv;

	/*
	 * Launch WSA application
	 */

	public void launchAppWSA(String strBrowser) {

		// this.driver=checkAndInitBrowser(strBrowser);
		String url = testData.get("URL");
		driver.navigate().to(url);
		driver.manage().window().maximize();
	}

	@FindBy(xpath = "//img[@title='Change Sales Site']")
	WebElement changeSiteiconWSA;

	/*
	 * Login WSA application
	 */
	public void loginToWSA(String strBrowser) throws Exception {

		// testData.put("testTry", "Monideep");
		launchAppWSA(strBrowser);
		String userid = testData.get("WSA_UserID");
		String password = testData.get("WSA_Password");
		setUserNameWSA(userid);
		setPasswordWSA(password);
		buttonLogInWSA.click();

		if (verifyElementDisplayed(changeSiteiconWSA)) {
			tcConfig.updateTestReporter("WSALoginPage", "setPassword", Status.PASS, "Log in button clicked");
		} else {
			launchAppWSA(strBrowser);

			setUserNameWSA(userid);
			setPasswordWSA(password);
			buttonLogIn.click();
			if (verifyElementDisplayed(changeSiteicon)) {
				tcConfig.updateTestReporter("WSALoginPage", "setPassword", Status.PASS, "Log in button clicked");
			} else {
				tcConfig.updateTestReporter("WSALoginPage", "setPassword", Status.FAIL,
						"Log in button clicked : But application not opened");
			}
		}
	}

	/*
	 * @FindBy(xpath="//img[@title='Change Sales Site']") WebElement changeSiteicon;
	 * 
	 * @FindBy(xpath="//button[@name='cs-submit']") WebElement confirmOKBtn;
	 * 
	 * @FindBy(xpath="//button[@id='submit-btn']") WebElement saveBtn;
	 * 
	 * @FindBy(xpath="//p[text()='Your site was updated successfully.']") WebElement
	 * txtSuccessMsg;
	 * 
	 * public final By radioBtnWVRServiceEntity=By.xpath(
	 * "(//input[@name='selectedServiceEntity'])[1]"); public final By
	 * radioBtnWBWServiceEntity=By.xpath(
	 * "(//input[@name='selectedServiceEntity'])[2]"); public final By
	 * radioBtnWVRAPServiceEntity=By.xpath(
	 * "(//input[@name='selectedServiceEntity'])[3]"); public final By
	 * siteDropDown=By.xpath("//select[@id='selectedSite']"); public final By
	 * closePopupIcon=By.xpath("(//img[@alt='Close this window'])[2]");
	 */

	/*
	 * function name: selectEntity() Purpose : Set Service entity from homepage
	 */

	/*
	 * function name: pitchTypeSelection() Purpose : Navigate to pitchtype -->
	 * subPitchType e.g New Propsal ---> UDI etc.
	 */

	public void pitchTypeSelectionWSA() throws Exception {

		final WebElement pitchType = driver.findElement(By.xpath("//a[text()='" + testData.get("PitchType") + "']"));
		pitchType.click();
		// Actions action=new Actions(driver);
		// action.moveToElement(pitchType).click().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		final WebElement subPitchType = driver.findElement(By.xpath(" //a[text()='" + testData.get("PitchType")
				+ "']/following-sibling::ul/descendant::a[contains(text(),'" + testData.get("SubPitchType") + "')]"));
		// subPitchType.click();
		if (verifyElementDisplayed(subPitchType)) {
			subPitchType.click();
			tcConfig.updateTestReporter("WSAHomePage", "pitchTypeSelectionWSA()", Status.PASS,
					testData.get("SubPitchType") + " selected from " + testData.get("PitchType") + " menu");
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "pitchTypeSelectionWSA()", Status.FAIL,
					testData.get("SubPitchType") + " NOT selected from " + testData.get("PitchType") + " menu");
		}
	}

	/*
	 * @FindBy(name="tourNumber") WebElement txtTourNumber;
	 * 
	 * @FindBy(xpath="//button[@name='cn-submit']") WebElement submitBtn;
	 * 
	 * @FindBy(xpath="//button[@name='btn-continue']") WebElement continueBtn;
	 * 
	 * @FindBy(xpath="//button[@name='btn-existingContinue']") WebElement
	 * existingContinueBtn;
	 * 
	 * @FindBy(xpath="//button[@name='btn-newOwnerContinue']") WebElement
	 * newOwnerContinueBtn;
	 * 
	 * @FindBy(xpath="//input[@name='memberNumber']") WebElement txtMemberNumber;
	 * 
	 * @FindBy(xpath="//h1[contains(text(),'Co-owners Selection')]") WebElement
	 * coOwnerSelectionPageHeader;
	 * 
	 * @FindBy(xpath="//h1[contains(text(),'Add New Owners')]") WebElement
	 * addOwnersHeader;
	 * 
	 * 
	 * public final By
	 * customerNametxt=By.xpath("(//form[@id='resultsForm']//p)[1]"); public final
	 * By selectCustRadio=By.xpath(
	 * "(//form[@id='resultsForm']//input[@type='radio'])[1]");
	 */
	// public String selectedCustName;
	/*
	 * function name: tourLookUpWSA() Purpose : Search by tour number and navigate
	 * to pitch creation page
	 */

	public void tourLookUpWSA() throws Exception {
		waitUntilElementVisible(driver, txtTourNumber, 120);
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("UDI")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);

			// select the

			String MemberName = testData.get("selectUser");

			// p[contains(.,'"+MemberName+"')]

			// selectedCustName=driver.findElement(customerNametxt).getText();
			// driver.findElement(selectCustRadio).click();

			selectedCustName = MemberName;
			List<WebElement> listogelem = driver
					.findElements(By.xpath("//p[contains(.,'" + MemberName + "')]/../../..//input[@type='radio']"));
			listogelem.get(0).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			continueBtn.click();

			waitUntilElementVisible(driver, txtMemberNumber, 120);
			continueBtn.click();
			waitUntilElementVisible(driver, coOwnerSelectionPageHeader, 120);
			existingContinueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, 120);
			newOwnerContinueBtn.click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);
			selectedCustName = driver.findElement(customerNametxt).getText();
			driver.findElement(selectCustRadio).click();
			continueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, 120);
			newOwnerContinueBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			if (verifyElementDisplayed(txtTourNumber)) {
				txtTourNumber.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()", Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter("TourLookUpPage", "tourLookUpWSA()", Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtn.click();
			waitUntilElementVisible(driver, continueBtn, 120);
			driver.findElement(selectCustRadio).click();
			continueBtn.click();
			waitUntilElementVisible(driver, addOwnersHeader, 120);
			newOwnerContinueBtn.click();
		}
	}

	/*
	 * 
	 * 
	 * @FindBy(xpath="//a[text()='Commissions']") WebElement commisionsLink;
	 * 
	 * @FindBy(xpath="//input[@id='salesAssociate']") WebElement salesAssociatetxt;
	 * 
	 * @FindBy(xpath="//input[@id='sellingRep']") WebElement salesRepresentativetxt;
	 * 
	 * @FindBy(xpath="//button[@name='commission-done']") WebElement
	 * comissionDoneBtn;
	 * 
	 * @FindBy(xpath="//a[text()='Select Inventory']") WebElement linkSelectInv;
	 * 
	 * @FindBy(xpath="//input[@id='newPoints1']") WebElement newPointstxt;
	 * 
	 * @FindBy(xpath="//button[@name='inventory-done']") WebElement
	 * inventoryDoneBtn;
	 * 
	 * @FindBy(xpath="(//a[contains(text(),'Click to adjust')])[1]") WebElement
	 * adjustLink;
	 * 
	 * @FindBy(xpath="//button[@id='band-submit1']") WebElement paymentDoneBtn;
	 * 
	 * @FindBy(xpath="(//button[@id='band-submit'])[6]") WebElement
	 * paymentSubmitBtn;
	 * 
	 * @FindBy(xpath="//div[@id='requiredDownPayment1']") WebElement txtRequiredAmt;
	 * 
	 * @FindBy(xpath="//div[@id='requiredToQualify1']") WebElement
	 * txtRequiredQlfyAmt;
	 * 
	 * @FindBy(xpath=
	 * "//input[@id='paymentTypeForm_offer1_downPaymentList_0__dpAmount']")
	 * WebElement txtAmountLine1;
	 * 
	 * @FindBy(xpath=
	 * "//input[@id='paymentTypeForm_offer1_downPaymentList_1__dpAmount']")
	 * WebElement txtAmountLine2;
	 * 
	 * @FindBy(xpath="//button[@name='final-offer']") WebElement finalBtn;
	 * 
	 * @FindBy(xpath="//button[@name='finalizeSummary']") WebElement finalBtnWBW;
	 * 
	 * 
	 * public final By titleDrpDwn=By.xpath("//select[@name='titleOption']"); public
	 * final By depositTypeDropDown=By.xpath(
	 * "//select[@id='paymentTypeForm_offer1_downPaymentList_1__paymentTypeName']");
	 * public final By selectFlagRadioBtn=By.xpath("(//input[@name='flag'])[1]");
	 * public final By
	 * selectFlagRadioBtnWBW=By.xpath("(//input[@name='flagForWorksheet1'])[1]");
	 * public final By CustNameTxt=By.xpath("//h2[contains(text(),'Name:')]");
	 * public final By
	 * pitchNumberHeader=By.xpath("//div[@id='page-body-container']//h1");
	 */
	// public static String createdPitchID;
	// public static String customerLastName;
	/*
	 * function name: createNewPitchWSA() Purpose : Fill up all the details and
	 * create a new pitch for various types of entity
	 */

	public void createNewPitchWSA() throws Exception {
		double amountone, amounttwo;

		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("UDI")) {
			System.out.println("Entered create New Pitc");
			waitUntilElementVisible(driver, commisionsLink, 120);
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Sales Associate number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyElementDisplayed(linkSelectInv)) {
				linkSelectInv.click();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Select inventory link is displayed");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Select inventory link is NOT displayed");
			}

			newPointstxt.clear();
			newPointstxt.sendKeys(testData.get("Points"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Points entered");
			inventoryDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
//			if(verifyObjectDisplayed(titleDrpDwn)){
//				//new Select(driver.findElement(titleDrpDwn)).selectByVisibleText("Single Man");
//				new Select(driver.findElement(titleDrpDwn)).selectByIndex(1);
//
//				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS, "Title Displayed");
//			}else {
//				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL, "Title NOT displayed");
//			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Adjust link not displayed");
			}

			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Payment done button clicked");

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(titleDrpDwn)) {
				// new Select(driver.findElement(titleDrpDwn)).selectByVisibleText("Single
				// Man");
				new Select(driver.findElement(titleDrpDwn)).selectByIndex(1);

				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Title Displayed");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Title NOT displayed");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitUntilObjectVisible(driver, pitchNumberHeader, 120);
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails = pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails = newheaderdetails.replace("-1 Selected", "");
				createdPitchID = headerdetails.trim();
				String CustName = driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Pitch # " + headerdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"No pitch number is generated");
			}

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			waitUntilElementVisible(driver, commisionsLink, 120);
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesRepresentativetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Sales Representative number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredQlfyAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);

			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Adjust link not displayed");
			}

			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSubmitBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtnWBW).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtnWBW.click();
			waitUntilObjectVisible(driver, pitchNumberHeader, 120);
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails = pageheaderdetails.replace("Final Summary for Reference # ", "");
				createdPitchID = newheaderdetails.trim();
				String CustName = driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Pitch # " + newheaderdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"No pitch number is generated");
			}
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			waitUntilElementVisible(driver, commisionsLink, 120);
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Sales Associate number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"Adjust link not displayed");
			}
			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(driver.findElement(depositTypeDropDown)).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
					"Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(selectFlagRadioBtn).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitUntilObjectVisible(driver, pitchNumberHeader, 240);
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = driver.findElement(pitchNumberHeader).getText();
				String newheaderdetails = pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails = newheaderdetails.replace("-1 Selected", "");
				createdPitchID = headerdetails.trim();
				String CustName = driver.findElement(CustNameTxt).getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.PASS,
						"Pitch # " + headerdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter("NewProposalOptionsPage", "createNewPitch()", Status.FAIL,
						"No pitch number is generated");
			}
		}
	}

	/*
	 * @FindBy(xpath="//a[text()='Logout']") WebElement LogoutBtn;
	 * 
	 * @FindBy(xpath="//div[@class='msg inform']") WebElement logoutMsg;
	 * 
	 * @FindBy(xpath="//img[@title='Home']") WebElement homeIcon;
	 */

	public void WSALogoutApp() throws Exception {
		try {
			LogoutBtn.click();
			waitUntilElementVisible(driver, textUsername, 120);
			tcConfig.updateTestReporter("WSALoginPage", "logOut", Status.PASS, "Log Out Succesful");
		} catch (Exception e) {
			tcConfig.updateTestReporter("WSALoginPage", "logOut", Status.FAIL, "Log Out Failed" + e.getMessage());
		}
	}

	@FindBy(xpath = "//a[text()='Retrieve']")
	WebElement retriveLink;
	@FindBy(xpath = "//input[@name='startDate']")
	WebElement txtStartDate;
	@FindBy(xpath = "//input[@name='endDate']")
	WebElement txtEndDate;
	@FindBy(xpath = "//input[@name='siteNumber']")
	WebElement txtSiteNumber;
	@FindBy(xpath = "//button[@name='lu-submit']")
	WebElement submitBtnRtrv;
	@FindBy(name = "tourNumber")
	WebElement txtTourNumberRetrv;
	@FindBy(xpath = "//input[@name='controlNumber']")
	WebElement txtReferenceNumber;

	public final By lookUpDrpDwn = By.xpath("//select[@id='pitch-lookup-type']");
	By NoOfResDrpdwn = By.xpath("//select[contains(@title,'Number of results to display per page')]");

	public void navigatetoretrievePitch() throws Exception {
		if (verifyElementDisplayed(retriveLink)) {
			retriveLink.click();
			tcConfig.updateTestReporter("RetrieveTourPage", "navigatetoretrievePitch()", Status.PASS,
					"Retrieve link clicked successfully");
		} else {
			tcConfig.updateTestReporter("RetrieveTourPage", "navigatetoretrievePitch()", Status.FAIL,
					"Retrieve link not displayed");
		}
		waitUntilObjectVisible(driver, lookUpDrpDwn, 120);
	}

	/*
	 * public void retrievePitch() throws Exception { // LocalDate
	 * today=LocalDate.now(); String CheckinDate = "", strSiteNumber = null;
	 * 
	 * CheckinDate = addDateInSpecificFormat("MM/dd/yyyy", 0);
	 * 
	 * new
	 * Select(driver.findElement(lookUpDrpDwn)).selectByVisibleText(testData.get(
	 * "LookUpBy")); waitForSometime(tcConfig.getConfig().get("LowWait")); if
	 * (testData.get("LookUpBy").equalsIgnoreCase("Date Range")) { if
	 * (verifyElementDisplayed(txtStartDate)) { txtStartDate.click();
	 * txtStartDate.clear(); txtStartDate.sendKeys(CheckinDate);
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.PASS, "Start date entered as : " + CheckinDate); } else {
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.FAIL, "Start date txtbox is not displayed"); } txtEndDate.click();
	 * txtEndDate.clear(); txtEndDate.sendKeys(CheckinDate); //
	 * if(testData.get("SiteName").contains("WYNDHAM BONNET CREEK // (00064)")) { //
	 * strSiteNumber="00064"; // } strSiteNumber =
	 * testData.get("SiteName").substring(testData.get("SiteName").indexOf("(") + 1,
	 * testData.get("SiteName").indexOf(")")); txtSiteNumber.click();
	 * txtSiteNumber.clear(); txtSiteNumber.sendKeys(strSiteNumber);
	 * txtSiteNumber.sendKeys(Keys.TAB); submitBtnRtrv.click();
	 * waitUntilObjectVisible(driver, NoOfResDrpdwn,120); new
	 * Select(driver.findElement(NoOfResDrpdwn)).selectByValue("50");
	 * 
	 * waitUntilObjectVisible(driver, By.xpath("//td[text()='" +
	 * WsaSalepointFlow.createdPitchID + "']"), 25); if
	 * (verifyObjectDisplayed(By.xpath("//td[text()='" +
	 * WsaSalepointFlow.createdPitchID + "']"))) {
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.PASS, "Pitch retrieve successfully"); } else {
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.FAIL, "Unable to retrieve the pitch"); } } else if
	 * (testData.get("LookUpBy").equalsIgnoreCase("Tour Number")) { if
	 * (verifyElementDisplayed(txtTourNumberRetrv)) {
	 * txtTourNumberRetrv.sendKeys(testData.get("TourNumber"));
	 * tcConfig.updateTestReporter("TourLookUpPage", "retrievePitch()", Status.PASS,
	 * "Tour Number entered"); } else {
	 * tcConfig.updateTestReporter("TourLookUpPage", "retrievePitch()", Status.FAIL,
	 * "Tour Number field is not displayed"); } submitBtnRtrv.click();
	 * waitUntilObjectVisible(driver, By.xpath("//td[text()='" +
	 * WsaSalepointFlow.createdPitchID + "']"), 120); if
	 * (verifyObjectDisplayed(By.xpath("//td[text()='" +
	 * WsaSalepointFlow.createdPitchID + "']"))) {
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.PASS, "Pitch #" + WsaSalepointFlow.createdPitchID +
	 * "retrieved successfully"); } else {
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.FAIL, "Unable to retrieve the pitch #" +
	 * WsaSalepointFlow.createdPitchID); } }else if
	 * (testData.get("LookUpBy").equalsIgnoreCase("Reference Number")) { if
	 * (verifyElementDisplayed(txtReferenceNumber)) {
	 * txtReferenceNumber.sendKeys(WsaSalepointFlow.createdPitchID.trim());
	 * tcConfig.updateTestReporter("TourLookUpPage", "retrievePitch()", Status.PASS,
	 * "Reference number entered");
	 * 
	 * submitBtnRtrv.click(); waitUntilObjectVisible(driver,
	 * By.xpath("//td[text()='" + WsaSalepointFlow.createdPitchID + "']"), 120); if
	 * (verifyObjectDisplayed(By.xpath("//td[text()='" +
	 * WsaSalepointFlow.createdPitchID + "']"))) {
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.PASS, "Pitch #" + WsaSalepointFlow.createdPitchID +
	 * "retrieved successfully"); } else {
	 * tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.FAIL, "Unable to retrieve the pitch #" +
	 * WsaSalepointFlow.createdPitchID); }
	 * 
	 * }else { tcConfig.updateTestReporter("RetrieveTourPage", "retrievePitch()",
	 * Status.FAIL, "Unable to retrieve the pitch #" +
	 * WsaSalepointFlow.createdPitchID); } }
	 * 
	 * }
	 */

}
