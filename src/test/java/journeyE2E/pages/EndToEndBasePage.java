package journeyE2E.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class EndToEndBasePage extends FunctionalComponents{

	public EndToEndBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}
	
	// @FindBy(xpath="//div[@class='loading-wrap']") WebElement loadingSpinner;
	By loadingSpinner = By.xpath("//div[@class='loader__tick']");

	public void checkLoadingSpinner() {
		waitUntilElementInVisible(driver, loadingSpinner, 60);
	}
	
	public void waitUntilElementVisibleIE(WebDriver driver, WebElement locator, long timeOutInSeconds) {
		for (long i = 0; i < timeOutInSeconds / 2; i++) {

			if (locator.isDisplayed()) {

				waitForSometime("2");
				break;
			} else {

				System.out.println(locator + " Element not visiable");
			}

		}

	}
	
	public void waitUntilElementVisibleIE(WebDriver driver, By by, long timeOutInSeconds) {
		boolean flag = false;

		for (long i = 0; i < timeOutInSeconds / 2; i++) {
			try {

				if (driver.findElement(by).isDisplayed()) {
					flag = true;
					waitForSometime("2");
					break;
				} else {

					System.out.println(by + " Element not visiable");
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				System.out.println(by + " Element not found");
				waitForSometime("2");
			}

		}
		if (!flag) {

			System.out.println("I am catched here!!!");
			driver.navigate().refresh();

			try {
				driver.switchTo().alert();
				waitForSometime("10");
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}
	

	public void clickElementJS(By by) {
		//driver.findElement(by).click();

		
		  JavascriptExecutor executor = (JavascriptExecutor) driver;
		  executor.executeScript("arguments[0].click();",
		  driver.findElement(by));
		 
	}

	public void clickElementJS(WebElement wbel) {
		
		 JavascriptExecutor executor = (JavascriptExecutor) driver;
		 executor.executeScript("arguments[0].click();", wbel);
		 
		//wbel.click();
	}

}
