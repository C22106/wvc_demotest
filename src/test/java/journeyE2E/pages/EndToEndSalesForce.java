package journeyE2E.pages;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

//import com.relevantcodes.extentreports.DBReporter.Config;
import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class EndToEndSalesForce extends EndToEndBasePage {

	public static final Logger log = Logger.getLogger(EndToEndSalesForce.class);
	ReadConfigFile config = new ReadConfigFile();
	public static String createdPID;
	public static String TourID;

	public EndToEndSalesForce(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By username = By.xpath("//input[@id='username']");
	private By password = By.xpath("//input[@id='password']");
	private By loginButton = By.id("Login");
	private By logOut = By.xpath("//a[text()='Log Out']");
	private By viewProfile = By.xpath("//img[@title='User']");

	public void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, 10);
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("EndToEndSalesForce", "setUserName", Status.PASS, "Username is entered for Login");
	}

	public void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("EndToEndSalesForce", "setPassword", Status.PASS, "Entered password for login");
	}

	public void launchSalesForce(String strBrowser) {

		//this.driver = checkAndInitBrowser(strBrowser);
		String url = config.get("SF_URL").trim();
		driver.navigate().to(url);
		driver.manage().window().maximize();

	}

	public void loginToSalesForce() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("SF_UserID");
		String password = tcConfig.getConfig().get("SF_Password");

		setUserName(userid);
		setPassword(password);
		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");

	}

	public void salesForceLogout() throws Exception {

		try {

			mouseoverAndClick(driver.findElement(viewProfile));
			waitUntilElementVisibleBy(driver, logOut, 120);
			driver.findElement(logOut).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, username, 120);
			if (verifyObjectDisplayed(username)) {
				tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.PASS,
						"Log outl Successful");
			} else {
				tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
						"Log Off unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	private By homeTab = By.xpath("//span[contains(.,'Home')]");
	private By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	@FindBy(xpath = "//button[@class='slds-button slds-show']")
	WebElement menuIcon;
	@FindBy(xpath = "//input[@class='slds-input' and @placeholder='Search apps and items...']")
	WebElement txtSearchBy;

	public void homePageValidation() {
		waitUntilElementVisibleBy(driver, homeTab, 120);

		tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Homepage Navigated");

		if (verifyObjectDisplayed(leadsTab)) {
			clickElementBy(leadsTab);
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS, "Leads Tab clicked");
		} else {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Leads Tab not present");
		}

	}

	public void navigateToMenu(String Iterator) {
		checkLoadingSpinner();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(menuIcon)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage");
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
					"User is NOT navigated to Homepage");
		}
		menuIcon.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisible(driver, txtSearchBy, 120);
		txtSearchBy.sendKeys(testData.get("Menu" + Iterator));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//b[text()='" + testData.get("Menu" + Iterator) + "']")).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public By createdTourLink = By.xpath("(//table[contains(@class,'uiVirtualDataTable')]/tbody/tr/th//a)[1]");
	public By newBtn = By.xpath("//div[text()='New']");
	// public By
	// ownerTourRecordRadioBtn=By.xpath("//div[@class='changeRecordTypeOptionRightColumn']/span[text()='Owner
	// VOI Tour Record']");
	public By nextBtn = By.xpath("//button/span[text()='Next']");
	public By brand = By.xpath("//span[contains(text(),'Brand')]/../..//a");
	public By txtSalesStore = By.xpath("//input[contains(@title,'Sales Stores')]");
	public By itemStoreOption = By.xpath("(//li[contains(@class,'default uiAutocompleteOption')]/a)[2]");
	public By txtAppointmentLoc = By.xpath("//input[contains(@title,'Locations')]");
	public By txtLeads = By.xpath("//input[contains(@title,'Leads')]");
	public By txtMarkettingAgent = By.xpath("//span[text()='Marketing Agent']/../../../../div//input");
	public By lblTourID = By.xpath("//span[text()='Tour Record Number']/../div//span");
	public By saveBtn = By.xpath("//button[@title='Save']");
	public By bookOptionSelect = By.xpath("//span[text()='Book Appointment']");
	// public By getAppointmentsBtn=By.xpath("//button[text()='Get
	// Appointments']");
	public By drpDwnMarketingStrtgy = By.xpath("(//select[@name='marketingStrategy'])[1]");
	public By drpDwnMarketingSrcType = By.xpath("(//select[@name='marketingSourceType'])[1]");
	public By lblAvailableSlot = By.xpath("(//span[text()='Available Slots'])[2]");
	public By yesBtn = By.xpath("//button[text()='YES']");
	public By txtAppointmentConfirm = By
			.xpath("//h2[text()='Appointment Confirmed']/../../div//span/div[contains(.,'Success')]");
	public By okBtn = By.xpath("//button[text()='OK']");
	public By qScoreDropdwn = By.xpath("//span[text()='Qualification Status']/../..//a[@class='select']");
	@FindBy(xpath = "//button[text()='Get Appointments']")
	WebElement getAppointmentsBtn;

	public By removeSalesStoreIcon = By.xpath("(//button[contains(@title,'Remove')])[1]");
	public By removeAppointmentLocIcon = By.xpath("(//button[contains(@title,'Remove')])[2]");

	public By reSalesStoretxt = By.xpath("//div[contains(@class,'SalesStoreSelect')]//input");
	public By reBookLocation = By.xpath("//div[contains(@class,'BookLocationSelect')]//input");

	public void verifyTourRecord() {
		waitUntilElementVisibleBy(driver, createdTourLink, 120);
		String strTourNumber = driver.findElement(createdTourLink).getAttribute("title");
		// clickElementBy(createdTourLink);
		tcConfig.updateTestReporter("TourRecordsPage", "verifyTourRecord", Status.PASS,
				"New tour created ; Tour ID : " + strTourNumber);

	}

	public void selectTourType() {
		waitUntilElementVisibleBy(driver, newBtn, 120);
		if (verifyObjectDisplayed(newBtn)) {
			tcConfig.updateTestReporter("TourRecordsPage", "selectTourType", Status.PASS,
					"User is navigated to Tour Records Page");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "selectTourType", Status.FAIL,
					"User is Not navigated to Tour Records Page");
		}
		clickElementBy(newBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement ownerTourRecordRadioBtn = driver.findElement(By.xpath(
				"//div[@class='changeRecordTypeOptionRightColumn']/span[text()='" + testData.get("TourType") + "']"));
		ownerTourRecordRadioBtn.click();
		clickElementBy(nextBtn);
	}

	public void createNewTour() {

		/*
		 * waitUntilElementVisibleBy(driver, txtSalesStore, 120);
		 * fieldDataEnter(txtSalesStore, testData.get("SalesStore"));
		 * clickElementBy(By.xpath("(//div[contains(@title,'" +
		 * testData.get("SalesStore") + "')])[1]"));
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(brand);
		WebElement Brand = driver.findElement(
				By.xpath("//div[@class='select-options']/ul/li/a[@title='" + testData.get("Brand") + "']"));
		Brand.click();
		fieldDataEnter(txtAppointmentLoc, testData.get("AppointmentLoc"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement appLocOption = driver
				.findElement(By.xpath("(//div[contains(@title,'" + testData.get("AppointmentLoc") + "')])[1]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		appLocOption.click();
		clickElementBy(qScoreDropdwn);
		WebElement qScoreValue = driver
				.findElement(By.xpath("//a[@title='" + testData.get("QualificationScore") + "']"));
		qScoreValue.click();
		fieldDataEnter(txtLeads, testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement txtLeadsOption = driver.findElement(By.xpath("//div[@title='" + testData.get("LeadName") + "']"));
		txtLeadsOption.click();
		fieldDataEnter(txtMarkettingAgent, testData.get("AgentName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement marketingAgentOption = driver
				.findElement(By.xpath("//div[@title='" + testData.get("AgentName") + "']"));
		marketingAgentOption.click();
		clickElementBy(saveBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, lblTourID, 120);

		TourID = driver.findElement(lblTourID).getText().trim(); // Ajay

		System.out.println("Tour ID :" + driver.findElement(lblTourID).getText().trim());
		if (verifyObjectDisplayed(lblTourID)) {
			tcConfig.updateTestReporter("TourRecordsPage", "createNewTour", Status.PASS,
					"Tour ID : " + driver.findElement(lblTourID).getText().trim() + " has been created");
		} else {
			tcConfig.updateTestReporter("TourRecordsPage", "createNewTour", Status.FAIL, "No tour has been created");
		}

	}

	public By latestTour = By.xpath("//span[text()='Tour Record']//div");

	public void getlatestTour() {

		waitUntilElementVisibleBy(driver, latestTour, 120);
		if (verifyObjectDisplayed(latestTour)){
			TourID = driver.findElement(latestTour).getText();
			System.out.println("created TourID: " + TourID);
			tcConfig.updateTestReporter("LeadPage", "getlatestTour",
					Status.PASS, "created TourID: " + TourID);
			driver.findElement(latestTour).click();
		}else{
			tcConfig.updateTestReporter("LeadPage", "getlatestTour",
					Status.FAIL, "Tour not created");
		}
		
	}
//	public void getlatestTour() {
//
//		waitUntilElementVisibleBy(driver, latestTour, 120);
//		TourID = driver.findElement(latestTour).getText();
//		System.out.println(TourID);
//		driver.findElement(latestTour).click();
//	}

	public By drpmarketingsubsource = By.xpath("(//select[@name='marketingSubSource'])[1]");
	public By drpMarketingcamp = By.xpath(
			"((//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select'])[2])[1]");
	public By drpBrand = By.xpath(
			"((//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select'])[1])[1]");

	public void bookAppointment() {
		waitUntilObjectVisible(driver, bookOptionSelect, 120);
		clickElementBy(bookOptionSelect);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("NoSalesStore").equalsIgnoreCase("Y")) {
			clickElementBy(removeSalesStoreIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*
			 * clickElementBy(removeSalesStoreIcon);
			 * waitForSometime(tcConfig.getConfig().get("LowWait"));
			 */
			try {
				if (getAppointmentsBtn.isEnabled() == false || getAppointmentsBtn.getAttribute("disabled") == "true") {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
							"User is unable to book appointment if Sales store is blank");
				} else {
					tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
							"User is able to book appointment if Sales store is blank");
				}
			} catch (NoSuchElementException e) {
				System.out.println("catched");
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is able to book appointment if Sales store is blank");
			}
		} else {
			clickElementBy(removeSalesStoreIcon);
			fieldDataEnter(reSalesStoretxt, testData.get("SalesStoreSalesForce"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//span[contains(text(),'" + testData.get("SalesStoreSalesForce") + "')][1]"))
					.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementBy(removeAppointmentLocIcon);
			fieldDataEnter(reBookLocation, testData.get("AppointmentLoc"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//span[contains(text(),'"+testData.get("AppointmentLoc")+"')][1]")).click();
			//driver.findElement(By.xpath("//span[@class='slds-media__body singleRow']")).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			new Select(driver.findElement(drpBrand)).selectByVisibleText(testData.get("Brand"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (driver.findElement(drpmarketingsubsource).isEnabled() == true) {
				new Select(driver.findElement(drpmarketingsubsource)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				new Select(driver.findElement(drpMarketingcamp)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			mouseoverAndClick(getAppointmentsBtn);
			// clickElementBy(getAppointmentsBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, lblAvailableSlot, 120);
			clickElementBy(lblAvailableSlot);
			waitUntilElementVisibleBy(driver, yesBtn, 120);
			clickElementBy(yesBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, txtAppointmentConfirm, 120);
			if (verifyObjectDisplayed(txtAppointmentConfirm)) {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.PASS,
						"New appointment has been created");
			} else {
				tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment", Status.FAIL,
						"User is unable to book an appointment");
			}
			waitUntilElementVisibleBy(driver, okBtn, 120);
			clickElementBy(okBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

	}

	/*
	 * public void bookAppointment() { waitUntilElementVisibleBy(driver,
	 * bookOptionSelect, 120); clickElementBy(bookOptionSelect);
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
	 * (testData.get("NoSalesStore").equalsIgnoreCase("Y")) {
	 * clickElementBy(removeSalesStoreIcon);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * clickElementBy(removeSalesStoreIcon);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * try { if (getAppointmentsBtn.isEnabled() == false ||
	 * getAppointmentsBtn.getAttribute("disabled") == "true") {
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.PASS,
	 * "User is unable to book appointment if Sales store is blank"); } else {
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL,
	 * "User is able to book appointment if Sales store is blank"); } } catch
	 * (NoSuchElementException e) { System.out.println("catched");
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL,
	 * "User is able to book appointment if Sales store is blank"); } } else {
	 * clickElementBy(removeAppointmentLocIcon); fieldDataEnter(reBookLocation,
	 * testData.get("AppointmentLoc"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * driver.findElement(By.xpath("//span[contains(text(),'" +
	 * testData.get("AppointmentLoc") + "')]")).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * new Select(driver.findElement(drpDwnMarketingStrtgy)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * 
	 * new Select(driver.findElement(drpDwnMarketingSrcType)).selectByIndex(1);
	 * waitForSometime(tcConfig.getConfig().get("MedWait"));
	 * 
	 * mouseoverAndClick(getAppointmentsBtn); //
	 * clickElementBy(getAppointmentsBtn); waitUntilElementVisibleBy(driver,
	 * lblAvailableSlot, 120); clickElementBy(lblAvailableSlot);
	 * waitUntilElementVisibleBy(driver, yesBtn, 120); clickElementBy(yesBtn);
	 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
	 * (verifyObjectDisplayed(txtAppointmentConfirm)) {
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.PASS, "New appointment has been created"); } else {
	 * tcConfig.updateTestReporter("TourRecordsPage", "bookAppointment",
	 * Status.FAIL, "User is unable to book an appointment"); }
	 * clickElementBy(okBtn);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); }
	 * 
	 * }
	 */
	// private By homeTab = By.xpath("//span[contains(.,'Home')]");
	// private By leadsTab= By.xpath("//span[@class='slds-truncate' and
	// contains(.,'Leads')]");
	private By newLeads = By.xpath("//div[@title='New']");

	private By salutationField = By.xpath("//div[contains(@class,'salutation')]//a");

	private By firstName = By.xpath("//input[@class='firstName compoundBorderBottom form-element__row input']");
	private By lastName = By.xpath("//input[@class='lastName compoundBorderBottom form-element__row input']");
	private By mobile = By.xpath("//span[contains(.,'Mobile')]/parent::label/following-sibling::input[@type='tel']");
	private By email = By.xpath("//input[@inputmode='email']");
	private By leadName = By
			.xpath("//span[contains(.,'Temporary Lead Identifier')]/parent::label/following-sibling::input");
	// private By
	// pidEnter=By.xpath("//span[text()='PID']/parent::label/following-sibling::input");
	private By streetEnter = By.xpath("//span[text()='Street']/parent::label/following-sibling::textarea");
	private By cityEnter = By.xpath("//span[text()='City']/parent::label/following-sibling::input");
	private By zipEnter = By.xpath("//span[contains(text(),'Zip')]/parent::label/following-sibling::input");
	private By stateEnter = By.xpath("//span[contains(text(),'State')]/parent::span/following-sibling::div//a");

	private By saveButton = By.xpath("//button[@title='Save']//span[text()='Save']");

	private By createNewTour = By.xpath("//button[contains(text(),'Create New Tour Record')]");
	// private By acsButton=By.id("tourCheckBTN");
	// private By tourRecords =By.xpath("//span[text()='Tour Records']");
	//
	private By qualificationStatus = By
			.xpath("//span[contains(text(),'Qualification')]/parent::span/following-sibling::div//a");
	private By qSelect = By.xpath("//a[@title='Q']");
	private By tourlanguage = By.xpath("//span[contains(text(),'Language')]/parent::span/following-sibling::div//a");
	private By qualificationSave = By.xpath("//div[@class='modal-footer slds-modal__footer']//span[text()='Save']");
	private By tourIdText = By.xpath("(//a[@data-refid='recordId'])[1]");
	private By txtDuplicateMsg = By.xpath("//div[contains(text(),'duplicate')]");
	private By searchAddressLink = By.xpath("//span[text()='Search Address']");
	private By enterAddressEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'uiInput--default')]");
	private By addressOption = By.xpath("(//div[@class='option'])[1]");

	private By globalSearchEdit = By
			.xpath("//div[contains(@class,'uiAutocomplete')]/input[contains(@class,'default')]");
	private By selectLeadItem = By.xpath("(//span[@class='uiImage']/img[contains(@class,'icon')])[1]");

	public void createLead(String addressEnter) {

		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);

			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			// clickElementBy(salutationSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(firstName, testData.get("FirstName"));
			fieldDataEnter(lastName, testData.get("LastName"));
			fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			fieldDataEnter(leadName, testData.get("LeadName"));
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));
				clickElementBy(stateEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
				stateSelect.click();
				// clickElementBy(stateSelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			clickElementBy(saveButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}
			} else {
				waitUntilElementVisibleBy(driver, createNewTour, 120);
				WebElement lblLeadConfirm = driver
						.findElement(By.xpath("//div[contains(@class,'page-header')]/span[contains(text(),'"
								+ testData.get("LeadName") + "')]"));

				if (verifyElementDisplayed(lblLeadConfirm)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"New Lead Created by Name : " + testData.get("LeadName") + " with PID : " + createdPID);
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL, "Lead Creation Failed");
				}
			}

		} else {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
					"User is unable to navigate to Leads creation page");
		}

	}

	public void searchForLead() {
		fieldDataEnter(globalSearchEdit, testData.get("LeadName"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(selectLeadItem);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		WebElement lblLeadConfirm = driver.findElement(By.xpath(
				"//div[contains(@class,'page-header')]/span[contains(text(),'" + testData.get("LeadName") + "')]"));
		if (verifyElementDisplayed(lblLeadConfirm)) {
			tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.PASS,
					"Lead details is displayed after searching for lead : " + testData.get("LeadName"));
		} else {
			tcConfig.updateTestReporter("LeadsPage", "searchForLead", Status.FAIL,
					"Unable to find the Lead details after searching for lead : " + testData.get("LeadName"));
		}
	}

	public void createLeadAndTour(String addressEnter) {

		waitUntilElementVisibleBy(driver, newLeads, 120);

		if (verifyObjectDisplayed(newLeads)) {
			tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
					"User is navigated to Leads creation page");
			clickElementBy(newLeads);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, firstName, 120);

			clickElementBy(salutationField);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WebElement salutationSelect = driver
					.findElement(By.xpath("//a[@title='" + testData.get("Salutation").trim() + "']"));
			System.out.println(salutationSelect.isDisplayed());
			salutationSelect.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			fieldDataEnter(firstName, testData.get("FirstName"));
			fieldDataEnter(lastName, testData.get("LastName"));
			fieldDataEnter(mobile, testData.get("MobileNo"));
			fieldDataEnter(email, testData.get("Email"));
			fieldDataEnter(leadName, testData.get("LeadName"));
			// createdPID=getRandomString(8);
			// fieldDataEnter(pidEnter, createdPID);
			if (addressEnter.equalsIgnoreCase("ManualAddress")) {
				fieldDataEnter(streetEnter, testData.get("StreetName"));
				fieldDataEnter(cityEnter, testData.get("City"));
				fieldDataEnter(zipEnter, testData.get("ZIP"));
				clickElementBy(stateEnter);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				WebElement stateSelect = driver
						.findElement(By.xpath("//a[contains(.,'" + testData.get("State") + "')]"));
				stateSelect.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else if (addressEnter.equalsIgnoreCase("GoogleAddress")) {
				String streetName = testData.get("StreetName");
				String streetNum = streetName.split(" ")[0];
				clickElementBy(searchAddressLink);
				waitUntilObjectVisible(driver, enterAddressEdit, 120);
				fieldDataEnter(enterAddressEdit, streetNum + " " + testData.get("City") + " " + testData.get("State")
						+ " " + testData.get("ZIP"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(addressOption)) {
					clickElementBy(addressOption);
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Address identified by Google Address help");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Address Not identified by Google Address help");
				}

			}

			clickElementBy(saveButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(createNewTour);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// clickElementBy(qualificationStatus);
			// WebElement qualificationStatusSelect = driver.findElement(By.xpath("//a[@title='" + testData.get("QualificationScore") + "']"));
			// qualificationStatusSelect.click();
			clickElementBy(tourlanguage);
			WebElement tourlanguageSelect = driver.findElement(By.xpath("//a[@title='" + testData.get("tourlanguage") + "']"));
			tourlanguageSelect.click();

			clickElementBy(qualificationSave);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// navigateToMenu("1");
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			//
			// waitUntilElementVisibleBy(driver, tourIdText, 120);
			// List<WebElement> listtourRecord =
			// driver.findElements(tourIdText);
			// tcConfig.updateTestReporter("LeadsPage", "createLead",
			// Status.PASS,
			// "Tour Created with No. " + listtourRecord.get(0).getText());
			// TourID = listtourRecord.get(0).getText();
			// System.out.println("Tour ID :" + TourID);

			if (testData.get("verifyDuplicateLead").equalsIgnoreCase("Yes")) {
				if (verifyObjectDisplayed(txtDuplicateMsg)) {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.PASS,
							"Duplicate message is displayed if same data is used during Lead Creation");
				} else {
					tcConfig.updateTestReporter("LeadsPage", "createLead", Status.FAIL,
							"Duplicate message is NOT displayed if same data is used during Lead Creation");
				}

			}
		}
	}
}
