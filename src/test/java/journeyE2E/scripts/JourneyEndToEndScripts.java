package journeyE2E.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import journeyE2E.pages.EndToEndACS;
import journeyE2E.pages.EndToEndSalePoint;
import journeyE2E.pages.EndToEndSalesForce;
import journeyE2E.pages.EndToEndWSA;
import automation.core.TestBase;

public class JourneyEndToEndScripts extends TestBase {

	public static final Logger log = Logger.getLogger(JourneyEndToEndScripts.class);

	@Test(dataProvider = "testData")
	public void tc_01_ETM_SP_WBW_Ownership_contractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);
			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();
			wsaE2E.pitchTypeSelection();
			wsaE2E.tourLookUp();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

			System.out.println("SalePoint starting");

			driver.quit();
			tcconfig.setDriver(null);
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);

			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.SalePoint_ownership_Automation_Flow();
			SalePointE2E.contractSearchandFinalize();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_02_ETM_SP_WBW_Experience_ContractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);
			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();
			wsaE2E.pitchTypeSelection();
			wsaE2E.tourLookUp();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);

			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.SalePoint_Exp_Automation_Flow();
			SalePointE2E.contractSearchandFinalize();

			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData")
	public void tc_03_WBW_UpgradeContract_ManualFlow_ACHautoPay(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			System.out.println("SalePoint starting");
			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);

			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.manualUpgradeWBW();
			SalePointE2E.contractSearchandFinalize();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_04_WBW_UpgradeContract_ManualFlow_CCautoPay(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			System.out.println("SalePoint starting");
			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);

			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.manualUpgradeWBW();
			SalePointE2E.contractSearchandFinalize();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_05_WBW_ConvertContract(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);
			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();

			wsaE2E.tourLookupUpgradeconvertFlow();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			System.out.println("SalePoint starting");
			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);

			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.navigateToMenu("1");
			SalePointE2E.contractProcessingConversion();
			SalePointE2E.WorldMarkOwners();
			SalePointE2E.WorldMarkPaymentOptionsScreen();
			SalePointE2E.contractSaveAndPrint();
			// SalePointE2E.SalePoint_conversion_Flow();
			SalePointE2E.contractSearchandFinalize();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_06_WVR_Manual_DiscoveryUpgradeContractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			System.out.println("SalePoint starting");
			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);

			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.navigateToMenuURL();
			SalePointE2E.searchMemDiscoveryUpgrdae();
			SalePointE2E.contractProcessinglocateCustomer();
			SalePointE2E.contractProcessingOwners();
			SalePointE2E.contractProcessingDataEntrySelection();
			SalePointE2E.contractProcessingInventrySelectionPhase();
			SalePointE2E.contractProcessingInventrySelection();
			SalePointE2E.contractSearchandTransmit();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_07_WVR_Trade_Manual_ContractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();
			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			System.out.println("SalePoint starting");
			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);
			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.viewTradeContract();
			SalePointE2E.viewTradedContract();
			SalePointE2E.processTradeContract();
			SalePointE2E.contractProcessinglocateCustomer();
			SalePointE2E.contractProcessingOwners();
			SalePointE2E.contractProcessingDataEntrySelection();
			SalePointE2E.contractProcessingInventrySelectionPhase();
			SalePointE2E.contractProcessingInventrySelection();
			SalePointE2E.contractSearchandTransmit();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_08_WVR_Trade_Automation_ContractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);

			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();
			wsaE2E.clickonTrade();
			wsaE2E.tourLookupUpgradeconvertFlow();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			System.out.println("SalePoint starting");

			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);
			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.viewTradeContract();
			SalePointE2E.viewTradedContract();
			SalePointE2E.processTradeContract();
			SalePointE2E.contractProcessinglocateCustomer();
			SalePointE2E.editPrimaryOwnerfromsummary();
			// SalePointE2E.contractProcessingOwners();
			SalePointE2E.LoanAutoPay();
			SalePointE2E.contractProcessingMoneyScreen();
			SalePointE2E.contractSearchandTransmit();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_09_WBW_Ownership_FinalizePitch(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);
			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();
			wsaE2E.pitchTypeSelection();
			wsaE2E.tourLookUp();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_10_WBW_Experience_FinalizePitch(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);
			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();
			wsaE2E.pitchTypeSelection();
			wsaE2E.tourLookUp();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData")
	public void tc_11_WBW_UpgradeContract_bandB1(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);
			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();
			wsaE2E.tourLookupUpgradeconvertFlow();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_12_WBW_UpgradeContract_bandC(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			System.out.println("WSA starting");
			EndToEndWSA wsaE2E = new EndToEndWSA(tcconfig);
			wsaE2E.loginToWSAApp(strBrowserInUse);
			wsaE2E.selectEntity();
			wsaE2E.tourLookupUpgradeconvertFlow();
			wsaE2E.createNewPitch();
			wsaE2E.logoutfromWSAapp();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	@Test(dataProvider = "testData")
	public void tc_13_WVR_Manual_UDI_ContractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {

			System.out.println("SalesForce starting");

			EndToEndSalesForce salesforceE2E = new EndToEndSalesForce(tcconfig);
			salesforceE2E.launchSalesForce("strBrowserInUse");
			salesforceE2E.loginToSalesForce();
			salesforceE2E.navigateToMenu("1");
			salesforceE2E.createLeadAndTour("ManualAddress");
			salesforceE2E.getlatestTour();
			salesforceE2E.bookAppointment();
			salesforceE2E.salesForceLogout();

			System.out.println("ACS starting");
			EndToEndACS acsE2E = new EndToEndACS(tcconfig);
			acsE2E.launchACS(strBrowserInUse);
			acsE2E.loginToACS();
			acsE2E.ScoringFromACS();
			acsE2E.logoutFromACS();

			driver.quit();
			tcconfig.setDriver(null);

			System.out.println("SalePoint starting");
			this.driver = checkAndInitBrowser("IE","","","","","","");
			this.tcconfig.setDriver(this.driver);

			System.out.println("SalePoint starting");
			EndToEndSalePoint SalePointE2E = new EndToEndSalePoint(tcconfig);

			SalePointE2E.launchSalepoint("IE");
			SalePointE2E.loginToSalePoint();
			SalePointE2E.companySelectSalePoint();
			SalePointE2E.changeProfileLocation();
			SalePointE2E.navigateToMenu("1");
			SalePointE2E.contractProcessinglocateCustomer();
			SalePointE2E.contractProcessingOwners();
			SalePointE2E.contractProcessingDataEntrySelection();
			SalePointE2E.contractProcessingInventrySelectionPhase();
			SalePointE2E.contractProcessingInventrySelection();
			SalePointE2E.contractSearchandTransmit();
			SalePointE2E.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

}