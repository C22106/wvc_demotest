package wsa.pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class SalePoint_ResalePage extends WSABasePage {

	public static final Logger log = Logger.getLogger(SalePoint_ResalePage.class);

	public SalePoint_ResalePage(TestConfig tcconfig) {
		super(tcconfig);

	}

	public static String point;
	public static String strContractNo;
	public static String strMemberNo;
	protected By okBtn = By.xpath("//input[@name='ok']");
	protected By tourID = By.name("tourID");
	protected By tourTime = By.name("tourTime");
	protected By nextBtn = By.xpath("//input[@name='next']");
	protected By searchBtn = By.xpath("//input[@name='search']");
	protected By tscField = By.xpath("//input[@name='tsc']");
	protected By saleper = By.name("salesMan");
	protected By deleteSec = By.xpath("//a[contains(.,'Delete')]");
	protected By savecontract = By.id("saveContract");
	protected By generate = By.xpath("//input[@name='print']");
	protected By conNo = By.xpath("//td[contains(.,'The Contract Number for this contract is:')]/b");
	protected By memberNo = By.xpath("//td[contains(.,'The Primary Owner Member Number is')]");
	protected By contracts = By.xpath("//span[contains(text(), 'Contracts')]");
	protected By tourIdEnter = By.xpath("tourID");
	protected By deleteOwner = By.xpath("//a[contains(.,'Delete')]");
	protected By paymentSelect = By.name("paymentType");
	protected By nextClick = By.xpath("//button[@name='Next']");
	protected By salePersonId = By.id("wbwcommissions_salesMan");
	protected By saveContractClick = By.id("saveContract");
	protected By printClick = By.xpath("//input[@name='print']");
	protected By memberNoCheck = By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]");
	protected By ssnSelect = By.xpath("//input[@name='socialSecurityNumber']");
	protected By homePhoneSelect = By.xpath("//input[@name='homePhone']");
	protected By primaryOwnerPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]");
	protected By ownerDetailsPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Owners')]");
	protected By MonthFromDrpDwn = By.xpath("//select[@name='beginDateMonth']");
	protected By DateFromDrpDwn = By.xpath("//select[@name='beginDateDay']");
	protected By YearFromDrpDwn = By.xpath("//select[@name='beginDateYear']");
	protected By ssnWBWSelect = By.xpath("//input[@name='socialSecurityNumber']");
	protected By pNameWBW = By.xpath("//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]");
	protected By editPOwnerWBW = By.xpath("//a[contains(.,'Edit')][1]");
	protected By deleteOwnerWBW = By.xpath("//a[contains(.,'Delete')]");
	protected By deleteFirstOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[1]");
	protected By deleteListOfOwnerWBW = By.xpath("(//a[contains(.,'Delete')])[2]");
	protected By MakePrimaryWBW = By.xpath("//a[contains(.,'Make Primary')]");
	protected By holdBtn = By.xpath("//input[@id='holdBtn']");
	protected By proceedBtn = By.xpath("//input[@id='nextBtn']");
	protected By memberNumber = By.name("memberNo");
	public By sellingSiteDrwpdwn = By.id("sellingSite");
	public By financedCheckBox = By.xpath("//input[@name='paymentType' and @value='finance']");
	public By cashCheckBox = By.xpath("//input[@name='paymentType' and @value='cash']");
	public By procFeeCollected = By.name("procFeeCollected");
	protected By ownerInfoScreen = By.xpath("//div[contains(text(),'Owner Info Screen')]");
	protected By deleteCheck = By.xpath("//a[contains(.,'Delete')]");
	protected By duesPaymentMethods = By.id("duesPACId");
	protected By cardtype = By.id("duesCCPACInfo.cardType");
	protected By cardMemberName = By.id("duesCCPACInfo.cardMemberName");
	protected By cardNumber = By.id("duesCCPACInfo.cardNumber");
	protected By cardExpireMonth = By.id("duesCCPACInfo.expireMonth");
	protected By cardExpireYear = By.id("duesCCPACInfo.expireYear");
	protected By sameAutoPay = By.id("useSameAutoPayId");
	protected By nextBtnPaymentPage = By.xpath("//input[@value='Next']");
	protected By printOption = By.id("justification");
	protected By upgradeRadioBtn = By.xpath("//input[@name='contractTypes' and @value='Upgrade']");
	protected By finalizeBtn = By.id("T");
	protected By voidBtn = By.id("V");
	protected By signatureDrpdwn = By.name("signatureDate");
	protected By docStampFeeValue = By.xpath("//td[contains(.,'Doc Stamp Fee')]/following-sibling::td[1]");
	protected By grossPurchasePrice = By.xpath("//td[contains(.,'Gross Purchase Price')]/following-sibling::td");
	protected By serviceFee = By.xpath("(//td[contains(.,'Service Fee')]/following-sibling::td)[1]");
	public By firstAmtTxtBx = By.id("pymtAmt0");
	public By secondAmtTxtBx = By.id("pymtAmt1");
	public By secondTypeDrpdwn = By.id("pymtTypeId1");
	protected By splitIndicator = By.xpath("//td[contains(.,'Split Indicator')]/following-sibling::td");
	protected By thirdPartyIndicator = By.xpath("//td[contains(.,'Third Party Indicator')]/following-sibling::td");
	protected By netPurchasePriceValue = By.xpath("//td[contains(.,'Net Purchase Price')]/following-sibling::td[1]");
	protected By contractDownPymt = By.name("contractDownPymt");

	public By bandSecondaryOwner = By.xpath("//a[contains(@href,'SecondaryOwner')]/following-sibling::b");
	public By chngePrimaryOwnerLink = By.xpath("//a[contains(text(),'Change Primary Owner')]");
	protected By save = By.xpath("//input[@name='Submit3']");
	protected By editUserInformationTitle = By
			.xpath("//tr[@class='page_title']//div[contains(.,'Edit User Information')]");
	protected By locationDrpdw = By.xpath("//SELECT[@name='location']");
	protected By companyListDrpdwn = By.xpath("//SELECT[@id='companyList']");
	protected By logoutBtn=By.xpath("//img[@src='/webapp/ccis/images/log_off.gif']");
	protected By loginUsername = By.xpath("//input[@id = 'fldUsername']");
	protected By loginPassword = By.xpath("//input[@id = 'fldPassword']");
	protected By buildVersion = By.xpath("//table[@class='client_table']/tbody/tr[2]/td");
	protected By buttonLogIn = By.xpath("//input[@name = 'logon']");
	
	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void launchApplicationAndLogin(String strBrowser) {
		// to get the current browser
		
		getBrowserName();
		navigateToURL(testData.get("SalepointURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(loginUsername), "Username Field Not Present");
		tcConfig.updateTestReporter("SalePointLoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
	}
	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void launchApplicationAndLogin() throws Exception {
		navigateToURL(testData.get("SalepointURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(loginUsername), "Username Field Not Present");
		tcConfig.updateTestReporter("SalePointLoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
		
		waitUntilElementVisibleBy(driver, loginUsername, "ExplicitWaitLongerTime");
		setUserName(testData.get("SP_UserID"));
		setPassword(testData.get("SP_Password"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(buttonLogIn);
		tcConfig.updateTestReporter("SalePointLoginPage", "loginToApp", Status.PASS, "Log in button clicked");
	}
	
	/*
	 * Method: setUserName Description: set User Name Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	protected void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, loginUsername, "ExplicitLowWait");
		sendKeysBy(loginUsername, strUserName);
		tcConfig.updateTestReporter("SalePointLoginPage", "setUserName", Status.PASS,
				"Username is entered for SalePoint Login");
	}

	/*
	 * Method: setPassword Description: set Password Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	protected void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		sendKeysBy(loginPassword, dString);
		//textPassword.sendKeys(dString);
		tcConfig.updateTestReporter("SalePointLoginPage", "setPassword", Status.PASS,
				"Entered password for SalePoint login");
	}


	/*
	 * Method: logOutApplication Description: logOut Application Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void logOutApplication() {
		try {
			clickElementJSWithWait(logoutBtn);			
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(buttonLogIn)) {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL, "Log Out Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalePointLoginPage", "Logout", Status.FAIL,
					"Log Out Failed" + e.getMessage());
		}
		
	}
	

	/*
	 * Method: companySelect Description: company Select Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	public void companySelect() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, companyListDrpdwn, "ExplicitWaitLongerTime");
		String strCompany = testData.get("strCompany");

		if (strCompany.equalsIgnoreCase("WBW")) {
			selectByText(companyListDrpdwn, "Worldmark By Wyndham");

		} else if (strCompany.equalsIgnoreCase("WVR")) {
			selectByText(companyListDrpdwn, "Wyndham Vacation Resorts");

		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			selectByText(companyListDrpdwn, "Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter("SalePointHomePage", "companySelect", Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		tcConfig.updateTestReporter("SalePointHomePage", "companySelect", Status.PASS,
				"Company Selected as: " + strCompany);

		clickElementJSWithWait(okBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (strCompany.equalsIgnoreCase("WVRAP")) {
			System.out.println("No Alert Present");

		} else {

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				System.out.println("No Alert Present after Login");
			}
		}
		tcConfig.updateTestReporter("SalePointHomePage", "companySelect", Status.PASS,
				"HomePage Navigation Successful");
	}

	/*
	 * Method: changeSite Description: change Site Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	public void changeSite() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("ChangeProfileURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(editUserInformationTitle)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			selectByText(locationDrpdw, testData.get("Location"));
			clickElementJSWithWait(save);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("SalePointHomePage", "changeSite", Status.PASS,
					"Location Change Successfully");
		} else {
			tcConfig.updateTestReporter("SalePointHomePage", "changeSite", Status.FAIL, "Location not changed");
		}
	}
	
	/*
	 * Method: resaleContractSelection Description: resale Contract Selection
	 * Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void resaleContractSelection() {
		String wsaCorrspContractNumber=WSA_ResaleSelectionPage.corrspContractNumber;
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
			
		point = driver.findElement(By.xpath("//td[contains(text(),'" + wsaCorrspContractNumber + "')]/../td[6]")).getText()
				.trim();

		if (driver.findElement(By.xpath("//td[contains(text(),'" + wsaCorrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		driver.findElement(By
				.xpath("//td[contains(text(),'" + wsaCorrspContractNumber + "')]/..//input[@id='resaleContractIdRadio']"))
				.click();
		tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
				"Radio Button selected for the Contract status Hold");

		clickElementBy(proceedBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(memberNumber)) {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.PASS,
					"Clicked on Proceed button and User navigated to Upgrade-Purchase Information Page");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in navigating to Upgrade-Purchase Information Page");
		}

	}
	
	/*
	 * Method: acceptOrDeclineAutomationFlow Description:Accept Or Decline Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */

	public void acceptOrDeclineAutomationFlow(Boolean automation) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.alertIsPresent());

			if (ExpectedConditions.alertIsPresent() != null) {

				if (automation == true) {
					driver.switchTo().alert().accept();
				} else {
					driver.switchTo().alert().dismiss();
				}
				tcConfig.updateTestReporter("SalePointResalePage", getMethodName(), Status.PASS,
						"Automation Pop Up message displayed");
			} else {

				tcConfig.updateTestReporter("SalePointResalePage", getMethodName(), Status.INFO,
						"Automation Pop Up is not available");
			}

		} catch (Exception e) {
			log.info("no Alert is displayed");
		}
	}
	
	/*
	 * Method: enterMemberNumberAndTourId Description: resale
	 * enterMemberNumberAndTourId Date: Nov/2020 Author: Kamalesh Gupta Changes
	 * By: NA
	 */

	public void enterMemberNumberAndTourId(String type) {

		sendKeysBy(memberNumber, testData.get("MemberNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(searchBtn);
		acceptOrDeclineAutomationFlow(false);
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			log.info("No Alert Present");
		}

		waitUntilElementVisibleBy(driver, tscField, "ExplicitWaitLongerTime");
		Assert.assertTrue(verifyObjectDisplayed(tscField),
				"Something wrong with member number. Please enter correct member number");
		try {
			if (getElementText(bandSecondaryOwner).trim().contains("(A1)")
					|| getElementText(bandSecondaryOwner).trim().contains("(B1)")
					|| getElementText(bandSecondaryOwner).trim().contains("(A)")
					|| getElementText(bandSecondaryOwner).trim().contains("(B)")) {
				clickElementJSWithWait(chngePrimaryOwnerLink);
			}
		} catch (Exception e) {
			log.info("No Secondary Owner with band A1, B1, A or B");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		sendKeysBy(tscField, testData.get("strTourId"));

		selectByText(sellingSiteDrwpdwn, testData.get("SellingSite"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (type.equalsIgnoreCase("CASH")) {
			clickElementBy(cashCheckBox);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			String value = getElementText(netPurchasePriceValue).trim().replace("$", "").replace(",", "");
			// value=String.valueOf(Integer.parseInt(value)/100);
			clickElementBy(contractDownPymt);
			for (int i = 0; i < 10; i++) {
				sendKeysBy(contractDownPymt, Keys.BACK_SPACE);
				sendKeysBy(contractDownPymt, Keys.DELETE);
			}
			driver.findElement(contractDownPymt).sendKeys(value);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearElementBy(procFeeCollected);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				log.info("No Alert Present");
			}
			sendKeysBy(procFeeCollected, testData.get("ProcessingFeeCollected"));

		} else if (type.equalsIgnoreCase("Financed")) {
			clickElementBy(financedCheckBox);
		}
		clickElementBy(nextBtn);

	}

	/*
	 * Method: enterMemberNumberAndTourId Description: resale
	 * enterMemberNumberAndTourId Date: Nov/2020 Author: Kamalesh Gupta Changes
	 * By: NA
	 */

	public void removeAdditionalSecondaryOwner() {
		waitUntilElementVisibleBy(driver, nextClick, "ExplicitWaitLongerTime");
		assertTrue(verifyObjectDisplayed(ownerInfoScreen), "Owner info Screen not Displayed");
		if (verifyObjectDisplayed(deleteSec)) {
			recDeleteSecondaryOwner();
		} else {
			System.out.println("No Secondary owner good to go");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(nextClick);

	}

	/*
	 * Method: recDeleteSecondaryOwner Description: recursive method to delete
	 * secondary owner Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void recDeleteSecondaryOwner() {
		List<WebElement> e = driver.findElements(deleteCheck);
		if (e.size() > 0) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(e.get(0));
			try {
				new WebDriverWait(driver, 30).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception exc) {
				log.info("No Alert Present after Login");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			recDeleteSecondaryOwner();
		}
	}
	/*
	 * Method: worldmakePayment Description: make payment on payment page Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void worldmakePayment(String type) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (type.equalsIgnoreCase("CASH")) {
			clickElementBy(cashCheckBox);
			clearElementBy(procFeeCollected);
			sendKeysBy(procFeeCollected, testData.get("ProcessingFeeCollected"));

		}
		clickElementBy(nextBtn);
		waitUntilElementVisibleBy(driver, duesPaymentMethods, "ExplicitWaitLongerTime");

		String paymentType = testData.get("PaymentMethod");

		selectByText(duesPaymentMethods, paymentType);
		if (paymentType.equalsIgnoreCase("CC Auto Pay")) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByText(cardtype, testData.get("CardType"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clearElementBy(cardMemberName);
			sendKeysBy(cardMemberName, testData.get("CreditCardHolder"));
			clearElementBy(cardNumber);
			sendKeysBy(cardNumber, testData.get("CCNumber"));
			selectByText(cardExpireMonth, testData.get("CCMonth"));
			selectByText(cardExpireYear, testData.get("CCYear"));
		} else if (paymentType.equalsIgnoreCase("No Auto Pay")) {

		}
		if (verifyObjectDisplayed(sameAutoPay)) {
			clickElementBy(sameAutoPay);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		clickElementBy(nextBtnPaymentPage);
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception exc) {
			log.info("No Alert Present");
		}

	}

	/*
	 * Method: contractConfirmation Description: final step for contract
	 * creation Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void contractConfirmation(String type) {
		waitUntilElementVisibleBy(driver, saleper, "ExplicitWaitLongerTime");
		sendKeysBy(saleper, testData.get("strSalePer"));
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		clickElementBy(nextBtn);

		waitUntilElementVisibleBy(driver, savecontract, "ExplicitWaitLongerTime");
		if (testData.get("validateDocStampFee").equalsIgnoreCase("Yes")) {
			checkDocStampFee(type);
		}
		if (testData.get("validateServiceFee").equalsIgnoreCase("Yes")) {
			checkServiceFee();
		}
		if (verifyObjectDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointResalePage", "contractConfirmation", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");
			clickElementBy(savecontract);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				log.info("No Alert Present");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		waitUntilElementVisibleBy(driver, printClick, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(printClick)) {
			clickElementJSWithWait(printClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(printOption)) {
				new Select(driver.findElement(printOption)).selectByIndex(2);
				tcConfig.updateTestReporter("SalePointResalePage", "contractClickPrintButton", Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(printClick);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		}

		waitUntilElementVisibleBy(driver, memberNoCheck, "ExplicitWaitLongerTime");
		if (verifyObjectDisplayed(conNo)) {
			strContractNo = getElementText(conNo).trim();
			System.out.println("Contract no: " + strContractNo);

			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.PASS,
					"Contract Created with number: " + getElementText(conNo));
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyObjectDisplayed(memberNo)) {

			strMemberNo = getElementText(memberNo).split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointResalePage", "contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

		log.info("Member Number is " + strMemberNo + " and corresponding Contract Number is " + strContractNo);
	}

	/*
	 * Method: finalizeContract Description:finalize Contract Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void finalizeContract() {

		driver.navigate().to(testData.get("ContractManagement"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementBy(upgradeRadioBtn);
		clickElementBy(nextBtn);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementBy(By.xpath("//a[contains(text(),'" + strMemberNo + "')]"));
		waitUntilElementVisibleBy(driver, finalizeBtn, "ExplicitWaitLongerTime");
		selectByIndex(signatureDrpdwn, 1);
		clickElementBy(finalizeBtn);
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			log.info("No Alert Present");
		}

		waitUntilElementVisibleBy(driver,
				By.xpath("//a[contains(text(),'" + strMemberNo + "')]/../following-sibling::td[4]"),
				"ExplicitWaitLongerTime");
		if (getElementText(By.xpath("//a[contains(text(),'" + strMemberNo + "')]/../following-sibling::td[4]"))
				.equalsIgnoreCase("Finalize")) {
			tcConfig.updateTestReporter("SalePointResalePage", "finalizeContract", Status.PASS,
					"Contract Finalize successfully");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "finalizeContract", Status.FAIL,
					"Contract not Finalize ");
		}

	}
	
	/*
	 * Method: checkDocStampFee Description:check Doc Stamp Fee Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */
	public void checkDocStampFee(String type) {
		if (type.equalsIgnoreCase("CASH")) {
			if (getElementText(docStampFeeValue).contains("$0.00")) {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.PASS,
						"Doc Stamp Fee for Cash type contract is $0.00");
			} else {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.FAIL,
						"Doc Stamp Fee for Cash type contract is not $0.00");
			}

		} else if (type.equalsIgnoreCase("Financed")) {
			String value = getElementText(grossPurchasePrice).trim().replaceAll("\\D", "");
			if (Integer.parseInt(getElementText(docStampFeeValue).replaceAll("\\D", "")) == 0.35
					* Integer.parseInt(value) / 100) {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.PASS,
						"Doc Stamp Fee for Financed type contract is " + 0.35 * Integer.parseInt(value) / 10000);
			} else {
				tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.FAIL,
						"Doc Stamp Fee for Financed type contract is incorrect");
			}
		}
	}

	/*
	 * Method: checkServiceFee Description:check service Fee Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void checkServiceFee() {
		String value = getElementText(serviceFee).trim().replaceAll("\\D", "");
		if (Integer.parseInt(value) / 100 == 80) {
			tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.PASS,
					"Service Fee for Financed type contract is $80");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "checkDocStampFee", Status.FAIL,
					"Service Fee for Financed type contract is not $80.00");
		}
	}

	
}
