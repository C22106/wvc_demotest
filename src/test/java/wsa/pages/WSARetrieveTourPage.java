package wsa.pages;

//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WSARetrieveTourPage extends WSABasePage {
	// public static String createdPitchID;
	public static final Logger log = Logger.getLogger(WSARetrieveTourPage.class);

	public WSARetrieveTourPage(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By drpdwnLookUpBy = By.xpath("//select[@id='pitch-lookup-type']");
	protected By txtReferenceNumber = By.xpath("//input[@name='controlNumber']");
	protected By btnSubmit = By.xpath("//button[@name='lu-submit']");
	protected By retrieveLnk = By.xpath("//a[text()='Retrieve']");

	/*
	 * Method : navigateRetrievePage Parameters :None Description : retrieve
	 * Pitch details using Reference Number Author : CTS Date : 2020 Change By :
	 * None
	 */
	public void navigateRetrievePage() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementClickableBy(driver, retrieveLnk, "ExplicitMedWait");
		getObject(retrieveLnk).click();
		tcConfig.updateTestReporter("WSARetrieveTourPage", getMethodName(), Status.PASS, "Navigated to Retrieve Page");
	}

	/*
	 * Method : retrievePitchDetails Parameters :None Description : retrieve
	 * Pitch details using Reference Number Author : CTS Date : 2020 Change By :
	 * None
	 */

	public void retrievePitchDetails() {

		String strReferenceNumber = WSANewProposalOptionsPage.strPitchNumber;
		waitUntilElementClickableBy(driver, drpdwnLookUpBy, "ExplicitMedWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementClickableBy(driver, txtReferenceNumber, "ExplicitLowWait");
		fieldDataEnter(txtReferenceNumber, strReferenceNumber);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		getObject(btnSubmit).click();
		tcConfig.updateTestReporter("WSARetrieveTourPage", getMethodName(), Status.PASS,
				"Reference Number is entered toretreive Pitch");

		waitUntilElementClickableBy(driver, By.xpath("//td[text()='" + strReferenceNumber + "']"),
				"ExplicitWaitLongerTime");
		tcConfig.updateTestReporter("WSARetrieveTourPage", getMethodName(), Status.PASS,
				"Pitch #" + strReferenceNumber + "is retrieved successfully");

	}

	protected By selectedPrintButton = By.xpath("//img[@title='Click to Print the Selected Proposal']");
	protected By PrintPageHeader = By.xpath("//h1[contains(.,'Print Summary')]");
	/*
	 * Method : printPitchDetails Parameters :None Description : Print Pitch
	 * details using Reference Number Author : CTS Date : 2020 Change By : None
	 */

	public void printPitchDetails() {
		getObject(selectedPrintButton).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(PrintPageHeader)) {
			tcConfig.updateTestReporter("WSARetrieveTourPage", getMethodName(), Status.PASS, "Print page is displayed");
		} else {
			tcConfig.updateTestReporter("WSARetrieveTourPage", getMethodName(), Status.FAIL,
					"Print page is not displayed");
		}

		if (getElementText(PrintPageHeader).contains(WSANewProposalOptionsPage.strPitchNumber)) {
			tcConfig.updateTestReporter("WSARetrieveTourPage", getMethodName(), Status.PASS,
					"Print page contains the correct pitch number");
		} else {
			tcConfig.updateTestReporter("WSARetrieveTourPage", getMethodName(), Status.FAIL,
					"Print page contains the incorrect pitch number");
		}
	}

}
