package wsa.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WSA_ResaleSelectionPage extends WSABasePage {

	public static final Logger log = Logger.getLogger(WSA_ResaleSelectionPage.class);
	public String selectedCustName;

	public WSA_ResaleSelectionPage(TestConfig tcconfig) {
		super(tcconfig);

	}

	public static String corrspContractNumber;
	public static String point;
	protected By expirationDate = By.xpath("//input[@name='selectedResaleContractId']/../following-sibling::td[3]");
	protected By holdBtn = By.id("HoldBtn");
	protected By removeHoldBtn = By.id("RemoveHoldBtn");
	protected By proceedBtn = By.id("ProceedBtn");
	protected By headerUpgradeOptions = By.xpath("//h1[contains(text(),'Upgrade Options')]");

	/*
	 * Method: resaleContractSelectionAndRelease Description: resale Contract
	 * Selection and then releaseing the same Date: Nov/2020 Author: Kamalesh
	 * Gupta Changes By: NA
	 */

	public void resaleContractSelectionAndRelease() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver.findElements(
				By.xpath("(//td[contains(text(),'Available')])/..//input[@name='selectedResaleContractId']"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {
				a.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver
							.findElement(By.xpath("(//td[contains(text(),'Available')])[" + (i + 1) + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		clickElementBy(holdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		clickElementBy(removeHoldBtn);

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().equalsIgnoreCase("Available")) {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
					"Clicked on Remove Hold button and Status of the selected Contract changed to Available");
		} else {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[12]")).getText()
				.equals("")) {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
					"Hold User Column is Empty");
		} else {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.FAIL,
					"Hold User Column is not Empty");
		}

	}

	/*
	 * Method: resaleContractSelection Description: resale Contract Selection
	 * and then releaseing the same Date: Nov/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */
	public void resaleContractSelection() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver.findElements(
				By.xpath("(//td[contains(text(),'Available')])/..//input[@name='selectedResaleContractId']"));

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {
				a.get(i).click();
				if (driver.findElement(holdBtn).isEnabled()) {
					System.out.println("Hold button enabled for index: " + i);
					tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
							"Radio Button selected for the first Available Contract and where hold is enabled");
					corrspContractNumber = driver
							.findElement(By.xpath("(//td[contains(text(),'Available')])[" + (i + 1) + "]/../td[2]"))
							.getText().trim();
					break;
				}

			}
		}
		clickElementBy(holdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		point = driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[6]")).getText()
				.trim();

		if (driver.findElement(By.xpath("//td[contains(text(),'" + corrspContractNumber + "')]/../td[11]")).getText()
				.trim().contains("Hold")) {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
					"Clicked on Hold button and Status of the selected Contract changed to Hold");
		} else {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.FAIL,
					"Error in clicking button");
		}

		clickElementBy(proceedBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(headerUpgradeOptions)) {
			tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "resaleContractSelection", Status.PASS,
					"Clicked on Proceed button and User navigated to Upgrade Options Page");
		} else {
			tcConfig.updateTestReporter("SalePointResalePage", "resaleContractSelection", Status.FAIL,
					"Error in navigating to Upgrade Options Page");
		}

	}

	/*
	 * Method: resaleContractSelection_MultiContract Description: resale
	 * Contract Selection having multiple contact as Yes Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	public void validateRowColor_ResaleContract() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "validateRowColor_ResaleContract", Status.PASS,
				"User navigated to Resale Contract Selection Page");
		List<WebElement> a = driver.findElements(expirationDate);

		if (a.size() > 0) {
			for (int i = 0; i < a.size(); i++) {

				corrspContractNumber = driver
						.findElement(By.xpath(
								"(//*[@name='selectedResaleContractId'])[" + (i + 1) + "]/../following-sibling::td[1]"))
						.getText().trim();

				String ContractDate = getElementText(a.get(i)).trim();
				float daysBetween = 0;
				SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yy");
				Date date = new Date(System.currentTimeMillis());
				try {
					Date UIDate = myFormat.parse(ContractDate);
					Date currentDate = myFormat.parse(myFormat.format(date).toString());
					long difference = UIDate.getTime() - currentDate.getTime();
					daysBetween = (difference / (1000 * 60 * 60 * 24));
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (daysBetween <= 5) {
					if (Color.fromString(a.get(i).getCssValue("background-color")).asHex()
							.equalsIgnoreCase("#FF0000")) {
						tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "validateRowColor_ResaleContract",
								Status.PASS, "Row is Red for contract number- " + corrspContractNumber);
					} else {
						tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "validateRowColor_ResaleContract",
								Status.FAIL, "Row is not Red for contract number- " + corrspContractNumber);
					}

				} else if (daysBetween > 5 && daysBetween <= 15) {

					if (Color.fromString(a.get(i).getCssValue("background-color")).asHex()
							.equalsIgnoreCase("#FFFF00")) {
						tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "validateRowColor_ResaleContract",
								Status.PASS, "Row is Yellow for contract number- " + corrspContractNumber);
					} else {
						tcConfig.updateTestReporter("WSA_ResaleSelectionPage", "validateRowColor_ResaleContract",
								Status.FAIL, "Row is not Yellow for contract number- " + corrspContractNumber);
					}

				}

			}
		}

	}
}
