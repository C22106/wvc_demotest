package wsa.pages;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WSA_LoginPage extends WSABasePage {

	public static final Logger log = Logger.getLogger(WSA_LoginPage.class);

	public WSA_LoginPage(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By logoutBtn=By.xpath("//a[text()='Logout']");
	protected By loginUsername = By.xpath("//input[@id = 'fldUsername']");
	protected By loginPassword = By.xpath("//input[@id = 'fldPassword']");
	protected By buildVersion = By.xpath("//table[@class='client_table']/tbody/tr[2]/td");
	protected By buttonLogIn = By.xpath("//button[@type='submit' and contains(@name,'submit')]");
	

	/*
	 * Method: launchApplication Description: launch the application Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void launchApplicationAndLogin() throws Exception {
		navigateToURL(testData.get("WSAURL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(loginUsername), "Username Field Not Present");
		tcConfig.updateTestReporter("WSALoginPage", "launchApplication", Status.PASS,
				"Application Launched Successfully");
		
		waitUntilElementVisibleBy(driver, loginUsername, "ExplicitWaitLongerTime");
		setUserName(testData.get("WSA_UserID"));
		setPassword(testData.get("WSA_Password"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(buttonLogIn);
		tcConfig.updateTestReporter("WSALoginPage", "loginToApp", Status.PASS, "Log in button clicked");
	}

	/*
	 * Method: setUserName Description: set User Name Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	protected void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, loginUsername, "ExplicitLowWait");
		sendKeysBy(loginUsername, strUserName);
		tcConfig.updateTestReporter("SalePointLoginPage", "setUserName", Status.PASS,
				"Username is entered");
	}

	/*
	 * Method: setPassword Description: set Password Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */
	private void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		sendKeysBy(loginPassword, dString);
		tcConfig.updateTestReporter("WSALoginPage", "setPassword", Status.PASS,
				"Password entered");
	}


	/*
	 * Method: logOutApplication Description: logOut Application Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void logOutApplication() {
		try {
			clickElementJSWithWait(logoutBtn);			
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(buttonLogIn)) {
				tcConfig.updateTestReporter("WSALoginPage", "Logout", Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter("WSALoginPage", "Logout", Status.FAIL, "Log Out Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("WSALoginPage", "Logout", Status.FAIL,
					"Log Out Failed" + e.getMessage());
		}
		
	}

}
