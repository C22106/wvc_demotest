package wsa.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WSA_HomePage extends WSABasePage {

	public static final Logger log = Logger.getLogger(WSA_HomePage.class);

	public WSA_HomePage(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By iconChangeSite = By.xpath("//img[@id='siteChanger']");
	protected By modalChangeSite = By.xpath("//div[@class='basicModalContent']");
	protected By btnConfirmOK = By.xpath("//button[@name='cs-submit']");
	protected By btnSave = By.xpath("//button[@id='submit-btn']");
	protected By upgradeLink = By.xpath("//a[text()='Upgrade/Conversion']");
	protected By radioBtnWVRServiceEntity = By.xpath("//input[@id='selectedServiceEntity000']");
	protected By radioBtnWBWServiceEntity = By.xpath("//input[@id='selectedServiceEntity001']");
	protected By radioBtnWVRAPServiceEntity = By.xpath("//input[@id='selectedServiceEntity002']");

	protected By dropdownSite = By.xpath("//select[@id='selectedSite']");

	protected By iconClosePopup = By.xpath("(//img[@class='modalClose'])[2]");
	protected By txtSuccessMsg = By.xpath("//p[contains(.,'site was updated successfully')]");

	// Header Links
	protected By linkOwnerLookUp = By.xpath("//a[text()='Owner Lookup']");
	protected By linkRetrieveBy = By.xpath("//a[text()='Retrieve']");

	// LogOut Link

	protected By btnLogOut = By.xpath("//a[text()='Logout']");

	/*
	 * Method: selectEntity Description: select Entity Date: Nov/2020 Author:
	 * Kamalesh Gupta Changes By: NA
	 */

	public void selectEntity() {

		String strEntityName = testData.get("ServiceEntity");
		String strSiteName = testData.get("SiteName");

		Assert.assertTrue(verifyObjectDisplayed(iconChangeSite), "Change Site Icon is not getting displayed");
		getObject(iconChangeSite).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(modalChangeSite)) {
			getObject(btnConfirmOK).click();
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity", Status.FAIL,
					"Site Change Confirmation pop up is not getting displayed");
		}
		waitUntilElementVisibleBy(driver, btnSave, "ExplicitRandomWait");

		if (strEntityName.equalsIgnoreCase("WVR")) {
			getObject(radioBtnWVRServiceEntity).click();

		} else if (strEntityName.equalsIgnoreCase("WBW")) {
			getObject(radioBtnWBWServiceEntity).click();

		} else if (strEntityName.equalsIgnoreCase("WVRAP")) {
			getObject(radioBtnWVRAPServiceEntity).click();
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity", Status.INFO,
					"default Service Entity is selected");

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(dropdownSite, strSiteName);

		tcConfig.updateTestReporter("WSAHomePage", "selectEntity", Status.INFO, "Service Entity is selected as "
				+ strEntityName + "  Sales Site  Entity is selected as " + strSiteName);

		getObject(btnSave).click();
		waitUntilElementVisibleBy(driver, iconClosePopup, "ExplicitRandomWait");

		if (verifyObjectDisplayed(txtSuccessMsg)) {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity", Status.PASS,
					"Sales Site update is successful");
			getObject(iconClosePopup).click();
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "selectEntity", Status.FAIL,
					"Sales Site Change is not successful");
		}

	}

	/*
	 * Method: clickOnOwnerLookUp Description: click On OwnerLookUp Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void clickOnOwnerLookUp() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(linkOwnerLookUp), "Owner LookUp link is not getting displayed");
		getObject(linkOwnerLookUp).click();
		tcConfig.updateTestReporter("WSAHomePage", "clickOnOwnerLookUp", Status.PASS,
				"Owner LookUp link is clicked");

	}

	/*
	 * Method: clickOnRetrieveBy Description: click On RetrieveBy Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void clickOnRetrieveBy() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(linkRetrieveBy), "Retrieve By link is not getting displayed");
		getObject(linkRetrieveBy).click();
		tcConfig.updateTestReporter("WSAHomePage", "clickOnRetrieveBy", Status.PASS, "Retrieve By Link is clicked");

	}

	/*
	 * Method: pitchTypeSelection Description: pitch Type Selection Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void pitchTypeSelection() {

		String strPitchType = testData.get("PitchType");
		String strSubPitchType = testData.get("SubPitchType");

		WebElement pitchType = driver.findElement(By.xpath("//a[text()='" + strPitchType + "']"));
		pitchType.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		WebElement subPitchType = driver.findElement(By.xpath(" //a[text()='" + strPitchType
				+ "']/following-sibling::ul/descendant::a[contains(text(),'" + strSubPitchType + "')]"));
		if (verifyObjectDisplayed(subPitchType)) {
			subPitchType.click();

			tcConfig.updateTestReporter("WSAHomePage", "pitchTypeSelection", Status.PASS,
					"Pitch Type is selected as " + strPitchType + " > " + strSubPitchType);
		} else {
			tcConfig.updateTestReporter("WSAHomePage", "pitchTypeSelection", Status.FAIL,
					"Pitch Type is not selected properly");
		}
	}

	/*
	 * Method: navigateToUpgradeConversionPage Description: navigate To UpgradeConversion Page Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void navigateToUpgradeConversionPage() {
		waitUntilElementVisibleBy(driver, upgradeLink, "ExplicitRandomWait");
		clickElementBy(upgradeLink);
	}

}
