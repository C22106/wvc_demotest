package wsa.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WSA_TourLookUpPage extends WSABasePage {

	public static final Logger log = Logger.getLogger(WSA_TourLookUpPage.class);
	public String selectedCustName;

	public WSA_TourLookUpPage(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By txtTourNumber = By.xpath("//input[@id='tourNumber']");
	protected By btnTourSubmit = By.xpath("//button[@name='cn-submit']");
	protected By txtCustomerName = By.xpath("(//form[@id='resultsForm']//p)[1]");
	protected By btnContinue = By.xpath("//button[@id='btn-continue']");
	protected By btnExistingContinue = By.xpath("//button[@name='btn-existingContinue']");
	protected By txtFxdWeekMember = By.xpath("//input[@name='memberNumber']");
	protected By labelExistingember = By.xpath("//label[contains(.,'Existing Member')]");
	protected By btnNewOwnerContinue = By.xpath("//button[@name='btn-newOwnerContinue']");
	protected By memberNumbeTxtBx = By.id("memberNumber");
	protected By ownerListHeader = By.xpath("//h1[contains(.,'Owner List - Upgrade')]");

	/*
	 * Method: tourLookUpNewProposal Description: tour Look Up New Proposal
	 * Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void tourLookUpNewProposal() {

		String strTourNumber = testData.get("strTourId");
		String strCustomerName = testData.get("SelectUser");
		String strMemberNumber = testData.get("MemberNumber");
		Assert.assertTrue(verifyObjectDisplayed(txtTourNumber), "Tour Look Up option is not displayed");
		fieldDataEnter(txtTourNumber, strTourNumber);
		getObject(btnTourSubmit).click();
		waitUntilElementVisibleBy(driver, memberNumbeTxtBx, "ExplicitRandomWait");
		fieldDataEnter(memberNumbeTxtBx, strMemberNumber);
		getObject(btnTourSubmit).click();
		waitUntilElementVisibleBy(driver, ownerListHeader, "ExplicitRandomWait");
		List<WebElement> listCustomer = driver
				.findElements(By.xpath("//p[contains(.,'" + strCustomerName + "')]/../../..//input[@type='radio']"));
		listCustomer.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("WSATourLookupPage", "tourLookUpNewProposal", Status.INFO,
				"Tour Number entered : " + strTourNumber + ", Member Number entered " + strMemberNumber
						+ " and Customer is selected as " + strCustomerName);

		getObject(btnContinue).click();

	}

	/*
	 * Method: navigateToFinalizePage Description: navigate To Finalize Page
	 * Date: Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	public void navigateToFinalizePage() {

		String strEntityName = testData.get("ServiceEntity");

		if (strEntityName.equalsIgnoreCase("WVR")) {
			waitUntilElementVisibleBy(driver, txtFxdWeekMember, "ExplicitMedWait");
			getObject(btnContinue).click();
			tcConfig.updateTestReporter("WSATourLookupPage", "navigateToFinalizePage", Status.PASS,
					"Fixed Week Conversion Selection Page is displayed");
			waitUntilElementVisibleBy(driver, labelExistingember, "ExplicitMedWait");
			getObject(btnExistingContinue).click();
			tcConfig.updateTestReporter("WSATourLookupPage", "navigateToFinalizePage", Status.PASS,
					"Existing Member/CoOwner Selection Page is displayed");

		} else {
			tcConfig.updateTestReporter("WSATourLookupPage", "navigateToFinalizePage", Status.INFO,
					"WBW Pitch Creation flow ");

		}

		waitUntilElementVisibleBy(driver, btnNewOwnerContinue, "ExplicitMedWait");
		getObject(btnNewOwnerContinue).click();
		tcConfig.updateTestReporter("WSATourLookupPage", "navigateToFinalizePage", Status.PASS,
				"Add New Member Page is displayed");

	}
}
