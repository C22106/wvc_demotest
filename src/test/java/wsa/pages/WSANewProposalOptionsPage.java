package wsa.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WSANewProposalOptionsPage extends WSABasePage {
	public static String strPitchNumber;
	public static final Logger log = Logger.getLogger(WSANewProposalOptionsPage.class);
	public String selectedCustName;

	public WSANewProposalOptionsPage(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By docStampFeeValue = By.xpath("//p[contains(.,'Doc Stamp Fee')]");
	protected By grossPurchasePrice = By.xpath("//p[contains(.,'Gross Price')]");
	protected By serviceFee = By.xpath("//p[contains(.,'Service Fee')]");
	protected By linkCommissons = By.xpath("//a[text()='Commissions']");
	protected By textSalesAssociate = By.xpath("//input[@id='salesAssociate' or @id='sellingRep']");
	protected By labelSalesAssociate = By.xpath("//span[@id='salesAssociateName' or @id='sellingRepName']");
	protected By percentDownTxtBox = By.id("percentDown1Other");
	protected By btnCmsnSubmit = By.xpath("//button[@name='commission-done']");
	protected By linkSelectInv = By.xpath("//a[text()='Select Inventory']");
	protected By textInvPoints = By.xpath("//input[@id='newPoints1']");
	protected By btnInvDone = By.xpath("//button[@name='inventory-done']");
	protected By editButton = By.xpath("//img[@title='Click to Finalize Proposal']");
	protected By percentageDownPaymentValue = By.xpath("//p[contains(.,'% Down - Interest Rate')]");
	protected By dropdwnTitle = By.xpath("//select[@id='titleOption']");
	protected By linkAdjustment = By.xpath("(//a[contains(text(),'Click to adjust')])[1]");
	protected By labelRequiredAmt = By.xpath("//div[@id='requiredDownPayment1' or @id = 'requiredToQualify1']");
	protected By textDownAmountLine1 = By.xpath("//input[@id='paymentTypeForm_offer1_downPaymentList_0__dpAmount']");
	protected By textDownAmountLine2 = By.xpath("//input[@id='paymentTypeForm_offer1_downPaymentList_1__dpAmount']");
	protected By drpdwnDownTypeLine1 = By
			.xpath("//select[@id='paymentTypeForm_offer1_downPaymentList_0__paymentTypeName']");
	protected By drpdwnDownTypeLine2 = By
			.xpath("//select[@id='paymentTypeForm_offer1_downPaymentList_1__paymentTypeName']");
	protected By btnPaymentDoneWBW = By.xpath("(//button[@id='band-submit'])[6]");
	protected By btnPaymentDoneWVR = By.xpath("(//button[@id ='band-submit1'])[1]");
	protected By btnSave = By.xpath("//button[@name='finalSummary']");
	protected By radioBtnSelect = By.xpath("(//input[@name='flag' or @name='flagForWorksheet1'])[1]");
	protected By btnFinal = By.xpath("//button[@name='final-offer' or @name='finalizeSummary']");
	protected By labelPitchNumber = By.xpath("//div[@id='page-body-container']//h1[contains(.,'Reference')]");
	protected By labelBand = By.xpath("//span[@id='scoreBand']");
	protected By btnDeleteOwner = By.xpath("(//td/a/img[@title='Delete this co-owner'])");
	protected By btnDelConfirm = By.xpath("//div[@id='deleteOwnerConfirmation']//button[@id='confirm']");
	protected By sellingSite = By.xpath("//select[@id='selectedSite']");
	protected By percentageDownPayment = By.xpath("//select[@id='percentDown1']");

	/*
	 * Method : removeSecondaryOwner Parameters :None Description : remove the
	 * Secondary Owner from the list Author : CTS Date : NOV-2020 Change By :
	 * None
	 */

	public void removeSecondaryOwner() {
		waitUntilElementClickableBy(driver, labelBand, "ExplicitRandomWait");
		int countSecondOwner = 0;

		if (verifyObjectDisplayed(btnDeleteOwner)) {
			countSecondOwner = (driver.findElements(btnDeleteOwner)).size();
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "removeSecondaryOwner", Status.INFO,
					"Co owner List is displayed");
			for (int i = 0; i < countSecondOwner; i++) {
				waitUntilElementClickableBy(driver, labelBand, "LongWait");
				clickElementJSWithWait(driver.findElements(btnDeleteOwner).get(0));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementClickableBy(driver, btnDelConfirm, "LongWait");
				clickElementJSWithWait(btnDelConfirm);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "removeSecondaryOwner", Status.INFO,
					"All Co owners are removed");

		} else {

			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "removeSecondaryOwner", Status.INFO,
					"Co owner Listis not displayed");

		}

	}

	/*
	 * Method : selectSalesRepWSA Parameters :None Description : Select the
	 * Sales Associate in WSA Author : CTS Date : "ExplicitRandomWait"19 Change
	 * By : None
	 */

	public void selectSalesRepWSA() {

		String strSalesAssociateID = testData.get("SalesAssociate");
		waitUntilElementClickableBy(driver, linkCommissons, "ExplicitRandomWait");
		getObject(linkCommissons).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementClickableBy(driver, textSalesAssociate, "ExplicitRandomWait");
		fieldDataEnter(textSalesAssociate, strSalesAssociateID);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String strSalesName = getObject(labelSalesAssociate).getText();

		if ((strSalesName.isEmpty()) || (strSalesName == null)) {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "selectSalesRepWSA", Status.FAIL,
					"Sales Associate name is not populated");
		} else {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "selectSalesRepWSA", Status.PASS,
					"Sales Associate name is selected as " + strSalesName);
		}
		getObject(btnCmsnSubmit).click();

	}

	/*
	 * Method : selectInventoryWSA Parameters :None Description : Select the
	 * Sales Associate in WSA Author : CTS Date : "ExplicitRandomWait"19 Change
	 * By : None
	 */

	public void selectInventoryWSA() {

		String strEntityName = testData.get("ServiceEntity");
		String strInvPoints = testData.get("Points");

		if (strEntityName.equalsIgnoreCase("WVR")) {

			Assert.assertTrue(verifyObjectDisplayed(linkSelectInv), "Select Inventory Link is not displayed");
			getObject(linkSelectInv).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementClickableBy(driver, textInvPoints, "ExplicitRandomWait");
			fieldDataEnter(textInvPoints, strInvPoints);
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "selectSalesRepWSA", Status.PASS,
					"Inventory Detailsis entered");
			getObject(btnInvDone).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementClickableBy(driver, dropdwnTitle, "ExplicitRandomWait");
			new Select(driver.findElement(dropdwnTitle)).selectByIndex(1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "selectSalesRepWSA", Status.PASS,
					"Title is selected");

		} else if (strEntityName.equalsIgnoreCase("WBW")) {
			selectByText(sellingSite, testData.get("WSASellingSite"));
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "selectSalesRepWSA", Status.PASS,
					"Selling Site is selected");

		}

	}

	/*
	 * Method : adjustPaymentWSA Parameters :None Description : Select the Sales
	 * Associate in WSA Author : CTS Date : "ExplicitRandomWait"19 Change By :
	 * None
	 */

	public void adjustPaymentWSA(String operation) {
		String strEntityName = testData.get("ServiceEntity");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (testData.get("CashDeal").equalsIgnoreCase("Yes")) {
			selectByText(percentageDownPayment, "Other");
			waitUntilElementVisibleBy(driver, percentDownTxtBox, "ExplicitRandomWait");
			clickElementBy(percentDownTxtBox);
			for (int i = 0; i < 7; i++) {
				sendKeysBy(percentDownTxtBox, Keys.BACK_SPACE);
				sendKeysBy(percentDownTxtBox, Keys.DELETE);

			}
			driver.findElement(percentDownTxtBox).sendKeys("100.00");
		}

		String strTotalAmt = null;
		double dblAmount = 0.00;
		String strFirstAmount = null;
		String strSecondAmount = null;

		String strDownAmountLine1 = testData.get("DownPayment_Line_1");
		String strDownAmountLine2 = testData.get("DownPayment_Line_2");

		Assert.assertTrue(verifyObjectDisplayed(linkAdjustment), "1st Adjustment Link is not displayed");
		getObject(linkAdjustment).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementClickableBy(driver, labelRequiredAmt, "ExplicitRandomWait");
		tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.PASS,
				"DownPayment Window is displayed");

		if (testData.get("SplitPayment").equalsIgnoreCase("Yes")) {
			strTotalAmt = getObject(labelRequiredAmt).getText().trim();
			dblAmount = Double.parseDouble(strTotalAmt);
			strFirstAmount = String.format("%.2f", (dblAmount % 100));
			strSecondAmount = String.format("%.2f", (dblAmount - dblAmount % 100));

			selectByText(drpdwnDownTypeLine1, strDownAmountLine1);
			fieldDataEnter(textDownAmountLine1, strFirstAmount);

			selectByText(drpdwnDownTypeLine2, strDownAmountLine2);
			fieldDataEnter(textDownAmountLine2, strSecondAmount);
			driver.switchTo().activeElement().sendKeys(Keys.TAB);

			tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.PASS,
					"DownPayment Details - " + strDownAmountLine1 + " : " + strFirstAmount + " , " + strDownAmountLine2
							+ " : " + strSecondAmount);

		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (strEntityName.equalsIgnoreCase("WVR")) {
			getObject(btnPaymentDoneWVR).click();
		} else if (strEntityName.equalsIgnoreCase("WBW")) {
			getObject(btnPaymentDoneWBW).click();
		} else {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.FAIL,
					"Enter the Entity Data in Datasheet");

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getObject(radioBtnSelect).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.PASS, "Form is filled Up");
		if (operation.equalsIgnoreCase("Final")) {
			getObject(btnFinal).click();
		} else if (operation.equalsIgnoreCase("Save")) {
			getObject(btnSave).click();

		}

	}

	/*
	 * Method : validatePitchNumber Parameters :None Description : Validate the
	 * Generated Pitch Number Author : CTS Date : "ExplicitRandomWait"19 Change
	 * By : None
	 */

	public void validatePitchNumber() {

		waitUntilElementClickableBy(driver, labelPitchNumber, "ExplicitRandomWait");
		String strPicthHeader = driver.findElement(labelPitchNumber).getText();
		strPitchNumber = ((strPicthHeader.split("#")[1]).split("-1")[0]).trim();
		tcConfig.updateTestReporter("WSANewProposalOptionsPage", "validatePitchNumber", Status.PASS,
				"Pitch is created successfully, Pitch Number " + strPitchNumber);

	}

	/*
	 * Method: validateDocStampFee Description:check Doc Stamp Fee Date:
	 * Nov/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void validateDocStampFee() {
		if (testData.get("CashDeal").equalsIgnoreCase("Yes")) {
			if (getElementText(docStampFeeValue).contains("$0.00")) {
				tcConfig.updateTestReporter("WSANewProposalOptionsPage", "checkDocStampFee", Status.PASS,
						"Doc Stamp Fee is $0.00");
			} else {
				tcConfig.updateTestReporter("WSANewProposalOptionsPage", "checkDocStampFee", Status.FAIL,
						"Doc Stamp Fee is not $0.00");
			}

		} else {
			String value = getElementText(grossPurchasePrice).trim().replaceAll("\\D", "");
			if (Integer.parseInt(getElementText(docStampFeeValue).replaceAll("\\D", "")) == 0.35
					* Integer.parseInt(value) / 100) {
				tcConfig.updateTestReporter("WSANewProposalOptionsPage", "checkDocStampFee", Status.PASS,
						"Doc Stamp Fee is " + 0.35 * Integer.parseInt(value) / 10000);
			} else {
				tcConfig.updateTestReporter("WSANewProposalOptionsPage", "checkDocStampFee", Status.FAIL,
						"Doc Stamp Fee is incorrect");
			}
		}
	}

	/*
	 * Method: checkServiceFee Description:check service Fee Date: Nov/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	public void checkServiceFee() {
		String value = getElementText(serviceFee).trim().replaceAll("\\D", "");
		if (Integer.parseInt(value) / 100 == 80) {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "checkDocStampFee", Status.PASS,
					"Service Fee is $80");
		} else {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", "checkDocStampFee", Status.FAIL,
					"Service Fee is not $80.00");
		}
	}

	public void editPitch() {
		String strEntityName = testData.get("ServiceEntity");
		getObject(editButton).click();

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		selectByText(percentageDownPayment, "Other");
		waitUntilElementVisibleBy(driver, percentDownTxtBox, "ExplicitRandomWait");
		clickElementBy(percentDownTxtBox);
		for (int i = 0; i < 7; i++) {
			sendKeysBy(percentDownTxtBox, Keys.BACK_SPACE);
			sendKeysBy(percentDownTxtBox, Keys.DELETE);

		}
		driver.findElement(percentDownTxtBox).sendKeys("90.00");

		Assert.assertTrue(verifyObjectDisplayed(linkAdjustment), "1st Adjustment Link is not displayed");
		getObject(linkAdjustment).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementClickableBy(driver, labelRequiredAmt, "ExplicitRandomWait");
		tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.PASS,
				"DownPayment Window is displayed");

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (strEntityName.equalsIgnoreCase("WVR")) {
			getObject(btnPaymentDoneWVR).click();
		} else if (strEntityName.equalsIgnoreCase("WBW")) {
			getObject(btnPaymentDoneWBW).click();
		} else {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.FAIL,
					"Enter the Entity Data in Datasheet");

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getObject(btnFinal).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String value = getElementText(percentageDownPaymentValue).trim().replaceAll("\\D", "");
		if (value.contains("9000")) {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.PASS,
					"Pitch was edited and finalize successfuuly.");
		} else {
			tcConfig.updateTestReporter("WSANewProposalOptionsPage", getMethodName(), Status.FAIL,
					"Issue in editing the pitch");
		}

	}

}
