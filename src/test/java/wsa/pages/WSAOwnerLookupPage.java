package wsa.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WSAOwnerLookupPage extends WSABasePage {

	public static final Logger log = Logger.getLogger(WSAOwnerLookupPage.class);
	public String selectedCustName;

	public WSAOwnerLookupPage(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By dropdownlookUp = By.xpath("//select[@id='cust-lookup-type']");
	protected By btnsubmit = By.xpath("//button[@name='cn-submit']");
	protected By txtLastName = By.xpath("//input[@name='lastName']");
	protected By txtPhoneNumber = By.xpath("//input[@name='phoneNumber']");
	protected By txtSiteNumber = By.xpath("//input[@name='siteNumber']");
	protected By tableResult = By.xpath("//table[@id='lookupResultsTable']");

	/*
	 * Method : selectLookUpByValue Parameters :None Description : Select Look
	 * Up By Values for search Author : CTS Date : 2020 Change By : None
	 */

	public void selectLookUpByValue() {

		String strLookUPBy = testData.get("LookUpBy");
		String strLastName = testData.get("LastName");
		String strPhNumber = testData.get("PhoneNumber");
		String strSiteNumber = testData.get("SiteNumber");

		Assert.assertTrue(verifyObjectDisplayed(dropdownlookUp), "Look Up By Value Page is loaded");

		selectByText(dropdownlookUp, strLookUPBy);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (strLookUPBy.equalsIgnoreCase("Last Name & Site")) {
			fieldDataEnter(txtLastName, strLastName);
			fieldDataEnter(txtSiteNumber, strSiteNumber);

		} else if (strLookUPBy.equalsIgnoreCase("Last Name & Home Phone Number")) {
			fieldDataEnter(txtLastName, strLastName);
			fieldDataEnter(txtPhoneNumber, strPhNumber);
		} else {

			tcConfig.updateTestReporter("WSAOwnerLookupPage", "selectLookUpByValue", Status.FAIL,
					"Please check the Look Up By Value entered ");
		}

		tcConfig.updateTestReporter("WSAOwnerLookupPage", "selectLookUpByValue", Status.INFO,
				"Look Up Values are Selected as " + strLookUPBy);

		getObject(btnsubmit).click();

	}

	/*
	 * Method : validateLookUpResultLastName Parameters :None Description :
	 * Validate the Search Result by Look Up Values Last Name Author : CTS Date
	 * : 2020 Change By : None
	 */

	public void validateLookUpResultLastName() {
		String strLastName = testData.get("LastName");
		waitUntilElementVisibleBy(driver, tableResult, "ExplicitMedWait");
		List<WebElement> customerLists = driver.findElements(By.xpath("//td[contains(text(),'" + strLastName + "')]"));
		if (customerLists.size() >= 1) {
			tcConfig.updateTestReporter("WSAOwnerLookupPage", "validateLookUpResultLastName", Status.PASS,
					"Result Table with Last Name Details is displayed");
		} else {
			tcConfig.updateTestReporter("WSAOwnerLookupPage", "validateLookUpResultLastName", Status.FAIL,
					"Result Table is not populated with Last name date");
		}

	}

}
