package wsa.scripts;

import java.util.Map;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import wsa.pages.SalePoint_ResalePage;
import wsa.pages.WSANewProposalOptionsPage;
import wsa.pages.WSARetrieveTourPage;
import wsa.pages.WSA_HomePage;
import wsa.pages.WSA_LoginPage;
import wsa.pages.WSA_ResaleSelectionPage;
import wsa.pages.WSA_TourLookUpPage;
import automation.core.TestBase;

public class WSATestScripts extends TestBase {
	public static final Logger log = Logger.getLogger(WSATestScripts.class);

	/*
	 * Method: tc_01_WSAResale_validateRowColor_ResaleContract
	 * Description:Verify that System highlighted the contracts in yellow and
	 * red if If expiration date is within 15 days, the entire row will be
	 * highlighted in Yellow Date: DEC/2020 Author: Kamalesh Gupta Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "wsa", "batch1", "resale" })

	public void tc_01_WSAResale_validateRowColor_ResaleContract(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		WSA_LoginPage loginPage = new WSA_LoginPage(tcconfig);
		WSA_HomePage homepage = new WSA_HomePage(tcconfig);
		WSA_TourLookUpPage tourLookup = new WSA_TourLookUpPage(tcconfig);
		WSA_ResaleSelectionPage resalecontract = new WSA_ResaleSelectionPage(tcconfig);
		try {

			loginPage.launchApplicationAndLogin();
			homepage.selectEntity();
			homepage.navigateToUpgradeConversionPage();
			tourLookup.tourLookUpNewProposal();
			resalecontract.validateRowColor_ResaleContract();
			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_02_WSAResale_validateHoldButtonFunctionality
	 * Description:Verify that System highlighted the contracts in yellow and
	 * red if If expiration date is within 15 days, the entire row will be
	 * highlighted in Yellow Date: DEC/2020 Author: Kamalesh Gupta Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "wsa", "batch1", "resale" })

	public void tc_02_WSAResale_validateHoldButtonFunctionality(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		WSA_LoginPage loginPage = new WSA_LoginPage(tcconfig);
		WSA_HomePage homepage = new WSA_HomePage(tcconfig);
		WSA_TourLookUpPage tourLookup = new WSA_TourLookUpPage(tcconfig);
		WSA_ResaleSelectionPage resalecontract = new WSA_ResaleSelectionPage(tcconfig);
		try {

			loginPage.launchApplicationAndLogin();
			homepage.selectEntity();
			homepage.navigateToUpgradeConversionPage();
			tourLookup.tourLookUpNewProposal();
			resalecontract.resaleContractSelectionAndRelease();
			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_03_WSAResale_WBW_Pitch_Creation Description:Verify user is
	 * able to create buyer proposal for a seller contract_New Member_Front Line
	 * Date: DEC/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "wsa", "batch1", "resale"})

	public void tc_03_WSAResale_WBW_Pitch_Creation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		WSA_LoginPage loginPage = new WSA_LoginPage(tcconfig);
		WSA_HomePage homepage = new WSA_HomePage(tcconfig);
		WSA_TourLookUpPage tourLookup = new WSA_TourLookUpPage(tcconfig);
		WSA_ResaleSelectionPage resalecontract = new WSA_ResaleSelectionPage(tcconfig);
		WSANewProposalOptionsPage wsanewProposal = new WSANewProposalOptionsPage(tcconfig);
		WSARetrieveTourPage wsaretrieve = new WSARetrieveTourPage(tcconfig);
		try {

			loginPage.launchApplicationAndLogin();
			homepage.selectEntity();
			homepage.navigateToUpgradeConversionPage();
			tourLookup.tourLookUpNewProposal();
			resalecontract.resaleContractSelection();
			wsanewProposal.selectSalesRepWSA();
			wsanewProposal.selectInventoryWSA();
			wsanewProposal.adjustPaymentWSA("Final");
			wsanewProposal.validatePitchNumber();
			wsanewProposal.validateDocStampFee();
			wsaretrieve.retrievePitchDetails();
			wsaretrieve.printPitchDetails();
			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_04_WSAResale_WBW_SaveEditAndFinalizePitch Description:Verify
	 * user is able to edit the % Down Interest Rate by editing the proposal
	 * Date: DEC/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "wsa", "batch1", "resale" })

	public void tc_04_WSAResale_WBW_SaveEditAndFinalizePitch(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		WSA_LoginPage loginPage = new WSA_LoginPage(tcconfig);
		WSA_HomePage homepage = new WSA_HomePage(tcconfig);
		WSA_TourLookUpPage tourLookup = new WSA_TourLookUpPage(tcconfig);
		WSA_ResaleSelectionPage resalecontract = new WSA_ResaleSelectionPage(tcconfig);
		WSANewProposalOptionsPage wsanewProposal = new WSANewProposalOptionsPage(tcconfig);
		WSARetrieveTourPage wsaretrieve = new WSARetrieveTourPage(tcconfig);
		try {

			loginPage.launchApplicationAndLogin();
			homepage.selectEntity();
			homepage.navigateToUpgradeConversionPage();
			tourLookup.tourLookUpNewProposal();
			resalecontract.resaleContractSelection();
			wsanewProposal.selectSalesRepWSA();
			wsanewProposal.selectInventoryWSA();
			wsanewProposal.adjustPaymentWSA("Save");
			wsanewProposal.validatePitchNumber();
			wsanewProposal.checkServiceFee();
			wsaretrieve.navigateRetrievePage();
			wsaretrieve.retrievePitchDetails();
			wsanewProposal.editPitch();
			loginPage.logOutApplication();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Method: tc_05_PitchCreationAndContractUpgrade_EndToEndFlow_WSAToSalePoint
	 * Description:Verify user is able to create buyer proposal for a seller
	 * contract_New Member_Front Line Date: DEC/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "wsa", "batch1" , "resale" })

	public void tc_05_PitchCreationAndContractUpgrade_EndToEndFlow_WSAToSalePoint(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		WSA_LoginPage loginPage = new WSA_LoginPage(tcconfig);
		WSA_HomePage homepage = new WSA_HomePage(tcconfig);
		WSA_TourLookUpPage tourLookup = new WSA_TourLookUpPage(tcconfig);
		WSA_ResaleSelectionPage resalecontract = new WSA_ResaleSelectionPage(tcconfig);
		WSANewProposalOptionsPage wsanewProposal = new WSANewProposalOptionsPage(tcconfig);
		SalePoint_ResalePage spResale = new SalePoint_ResalePage(tcconfig);

		try {

			loginPage.launchApplicationAndLogin();
			homepage.selectEntity();
			homepage.navigateToUpgradeConversionPage();
			tourLookup.tourLookUpNewProposal();
			resalecontract.resaleContractSelection();
			wsanewProposal.selectSalesRepWSA();
			wsanewProposal.selectInventoryWSA();
			wsanewProposal.adjustPaymentWSA("Final");
			wsanewProposal.validatePitchNumber();
			loginPage.logOutApplication();

			spResale.launchApplicationAndLogin();
			spResale.companySelect();
			spResale.changeSite();
			spResale.resaleContractSelection();
			spResale.enterMemberNumberAndTourId("Financed");
			spResale.removeAdditionalSecondaryOwner();
			spResale.worldmakePayment("Financed");
			spResale.contractConfirmation("Financed");
			spResale.finalizeContract();
			spResale.logOutApplication();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

}