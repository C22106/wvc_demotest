package tgs.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import tgs.pages.BusinessRulesPage;
import tgs.pages.DestinationsPage;
import tgs.pages.ExtensionsPage;
import tgs.pages.HomePage;
import tgs.pages.LoginPage;
import tgs.pages.OfferAgentsPage;
import tgs.pages.OfferFeesPage;
import tgs.pages.OfferPage;
import tgs.pages.OfferTransactionsPage;
import tgs.pages.PaymentRecordsPage;
import tgs.pages.PromoCodePage;
import tgs.pages.SalesChannelPage;
import tgs.pages.TeamsPage;
import tgs.pages.TermsAndConditionsPage;


import tgs.pages.SalesChannelPage;
import tgs.pages.TeamsPage;
import tgs.pages.TermsAndConditionsPage;

public class TGSScripts extends TestBase {

	public static final Logger log = Logger.getLogger(TGSScripts.class);

	/*
	 * Function/event: TC03_CRS-205_Configure Offer Disclosures Edit_Sales
	 * Channel TC20_CRS-259_Restrict assignments to master sales channel_Edit
	 * Offer Disclosure_Warranty Admin Description: To validate that user is
	 * able to edit existing offer disclosure for Sales Channel Designed By:
	 * Jyoti_Chowdhury Pre-Requisites: Test Data: 1. Login with Corporate Super
	 * User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC001_Configure_Offer_Disclosures_Edit_Sales_Channel(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();

			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.validateEditOfferDisclosure();
			salesPage.verifyFieldUpdated();
			salesPage.assignMSUToOfferDisclosure();
			salesPage.clickCancel();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC01_CRS-214_Assign Sales Team to Sales Channel_Add a New
	 * Sales Team to Sales Channel TC002_CRS61_APSC_new Description: To validate
	 * that the user is able to add a New Sales Team to Sales Channel ; Assign a
	 * new premiums to a sales channel Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: Test Data: 1. Login with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC002_Assign_SalesTeam_to_SalesChannel(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.createNewSalesChannelTeams();
			salesPage.clickNewRecord();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.createSalesChannelPremiumAssign();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Function/event: TC005_CRS62_Ext_new TC04_CRS-1348_Fee type selected as
	 * Spev-Referral Fee_Enter value in fee field_New Offer Fees form_enter
	 * other mandatory fields_CSU Description: create a new record of extensions
	 * Designed By: Jyoti_Chowdhury Pre-Requisites: Test Data: 1. Login with
	 * Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC003_Create_new_Extension(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		OfferFeesPage offFeePage = new OfferFeesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffersDiffLogin();
			offerPage.navigateToMenu("1");
			extnPage.clickNewbtn();
			extnPage.createExtensionRecord();
			offerPage.navigateToMenu("2");
			offFeePage.clickNewbtn();
			offFeePage.createOfferFees();
			offFeePage.validateSucessMsg();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC05_CRS-18_CRS-206_Configure Change & Cancel
	 * Fees_Effective and End Date validation_CSU TC06_CRS-86_Configure Change &
	 * Cancel Fees_Error Validation_Fee Type = Tour No Show and Fee Unit Type =
	 * Standard_CSU Description: To validate that the Effective and End Date for
	 * New Offer creation Designed By: Jyoti_Chowdhury Pre-Requisites: Test
	 * Data: 1. Login with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC004_Configure_Effective_and_EndDatevalidation_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		OfferFeesPage offFeePage = new OfferFeesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToOffersTab();
			offerPage.clickNew();
			offerPage.selectOfferType_BringAFrnd();
			offerPage.createOffer();
			offerPage.validateErrorMsg();
			salesPage.clickCancel();
			offerPage.navigateToMenu("1");
			offFeePage.clickNewbtn();
			offFeePage.createOfferFees();
			offFeePage.validateErrorMsg();
			salesPage.clickCancel();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_CRS63_ExtensionRestriction_new
	 * TC007_CRS62_Ext_delete Description: To validate that the Effective and
	 * End Date for New Offer creation Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: Test Data: 1. Login with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC005_Add_ExtensionRestriction_new_CRMuser(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffersDiffLogin();
			offerPage.navigateToOffersTab();
			offerPage.searchOffers();
			offerPage.createExtensionRestriction();
			offerPage.clickNewRecord();
			offerPage.getExtensionRecord();
			offerPage.navigateToMenu("1");
			extnPage.searchOfferExtension(OfferPage.extensionRecord);
			extnPage.validateDeleteRecord();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC05_CRS-219_Duplicate Premiums on the Sales Channel_Set
	 * Up_Same Product_Overlapping Effective Dates TC06_CRS-215_Duplicate Sales
	 * Team on the Sales Channel_Set Up_Same Team_Overlapping Effective Dates
	 * Description: To validate that the Effective and End Date for New Offer
	 * creation Designed By: Jyoti_Chowdhury Pre-Requisites: Test Data: 1. Login
	 * with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC006_Duplicate_Premium_SalesChannel_Overlapping_EffectiveDates_CSU(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.verifySaleChnlPremiumAssignOverlap();
			salesPage.validateErrorMsg();
			salesPage.clickCancel();
			salesPage.verifySaleChnlTeamOverlap();
			salesPage.validateErrorMsg();
			salesPage.clickCancel();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC01_CRS-223_Configure Car and Cruise prices_Offer Sales
	 * Channel record_Car Prices_Auto Populates from Sales Channel Business
	 * Rules Description: To validate that the values of the
	 * "Car Minimum Net price" and Car "Maximum Net price" fields auto Populates
	 * from Sales Channel Business Rules on the Offer Sales Channel record
	 * Designed By: Jyoti_Chowdhury Pre-Requisites:Sales Channel Business Rules
	 * with values in "Car Minimum Net price" and Car "Maximum Net price" fields
	 * Test Data: 1. Login with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC007_Configure_CarCruise_prices_populates_from_BusinessRules_CSU(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.clickNew();
			businessRulePage.createSalesChannelBusinessRules();
			businessRulePage.verifyCarPrices();
			businessRulePage.clickOfferTab();
			offerPage.createOfferSalesChannel();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC005_CRS11_MSC_new TC002_CRS12_SC_new_FTF
	 * Description:create a new record of master sales channel Designed
	 * By:Jyoti_Chowdhury Pre-Requisites: Test Data: 1. Login with Corporate
	 * Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC008_create_new_Master_SaleChannel_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			salesPage.selectMasterSalesChannel();
			salesPage.createMasterSalesChannel();
			offerPage.navigateToMenu("1");
			salesPage.selectSalesChannel();
			salesPage.createSalesChannel();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_CRS46_PremRest_new TC002_CRS6_ATSL_new
	 * TC004_CRS6_ATSL_delete Description: Add new Premium Restriction; Add new
	 * ATSL; Delete a ATSL record Designed By: Jyoti_Chowdhury Pre-Requisites:
	 * Test Data: 1. Login with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC009_Add_Premium_restriction_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.clickNew();
			businessRulePage.selectPremiumRestriction();
			businessRulePage.createPremiumRestriction();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.clickNew();
			businessRulePage.selectATSLRule();
			businessRulePage.createATSLRule();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.searchATSLBusinessRule();
			businessRulePage.deleteATSLRule();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC002_CRS14_ABRSC_new TC03_CRS-259_Restrict assignments
	 * to master sales channel_Offers_CSU TC11_CRS-259_Restrict assignments to
	 * master sales channel_Create Extension Restrictions_Offers Level_CSU
	 * Description: Assign a new business rules to a sales channel Designed By:
	 * Jyoti_Chowdhury Pre-Requisites: Test Data: 1. Login with Corporate Super
	 * User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC010_Create_SalesChannel_BusinessRules_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.clickNew();
			businessRulePage.createSalesChannelBusinessRules();
			offerPage.navigateToOffersTab();
			offerPage.searchOffers();
			offerPage.validateMSCnotPresent();
			salesPage.clickCancel();
			offerPage.assignMSCExtensionRestriction();
			salesPage.clickCancelOnNewExt();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC32_CRS-352_Offer Set Up Workflow_Bring a Friend
	 * Offer_Assign Extension Restriction_CSU Description: To validate that the
	 * user is able to Assign Extension Restriction Designed By: Jyoti_Chowdhury
	 * Pre-Requisites: Test Data: 1. Login with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC011_Assign_Extn_Restriction_to_BAF_offer_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToOffersTab();
			offerPage.clickNew();
			offerPage.selectOfferType_BringAFrnd();
			offerPage.createBAFOffer();
			offerPage.validateOfferStatus();
			offerPage.createExtensionRestrictionCSU();
			offerPage.createExtensionRestrictionCSU();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC01_CRS-259_Restrict assignments to master sales
	 * channel_CreateTeams_CRM TC05_CRS-259_Restrict assignments to master
	 * saleschannel_Create Sales Store_CRM TC06_CRS-259_Restrict assignments to
	 * master sales channel_Create Premiums_Warranty Admin
	 * 
	 * Description: To validate that system restricts assignment of Teams to
	 * Master Sales Channel Date: July/2020 Author: Jyoti_Chowdhury Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC015_Restrict_assignments_MSC_CreateTeams_CRM(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.assignMSCtoSalesChannelTeams();
			salesPage.clickCancelUpdated();
			salesPage.assignMSCToSaleChannelStoreAssigned();
			salesPage.clickCancelUpdated();
			salesPage.assignMSCToSaleChannelPremiumAssigned();
			salesPage.clickCancelUpdated();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC001_CRS40_Destination_and_related_assignments Description: Add,
	 * Edit Destinations, Assign Sales Store, Accommodation, Set Accommodation
	 * Net Max Price, new premium to destination Date: July/2020 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC012_CRS40_Destination_and_related_assignments(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		DestinationsPage destinationsPage = new DestinationsPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			destinationsPage.createDestinations();
			destinationsPage.navigateToDestinationRelated();
			destinationsPage.assignSalesStoreToDestination();
			destinationsPage.addAccommodation();
			destinationsPage.assignPremiumtoDestination();
			// OfferPage.addBusRuleSalesChannel();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC02_ CRS-473 _Duplicate Destination on the Offer_Set Up_Same
	 * Product_Overlapping Effective Dates. TC01_CRS-75_Offer Expiration Months
	 * and Dates_Set multiple Expiration Months_Regular Offer_CSU Description:
	 * Set multiple expiration month for offer Date: July/2020 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC013_Regular_Offer_Multiple_Exp_Month_Destination_overlapping_Dates(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			OfferPage.chooseOfferType();
			OfferPage.chooseRegularOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.enterMultipleExpMonth();
			OfferPage.saveOffer();
			OfferPage.addDestinationToOffer();
			OfferPage.addDestinationOverlapDates();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC12_CRS-15_Configure Face to Face Offer_Create new Face to Face
	 * Offer_Error Validation_Saving without mandatory field. TC12_CRS-352_Offer
	 * Set Up Workflow_Face to Face Offer_Offer Status_Mark Status as
	 * Complete_Without mandatory assignments_CSU Description: Create Face to
	 * Face offer mandatory fields and mandatory assignments validation Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC014_Face_To_Face_Mark_Complete_without_mandatory_fields_assignments(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			OfferPage.chooseOfferType();
			OfferPage.chooseFTFOffer();
			OfferPage.verifyMandatoryFieldDetails();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.markCompleteWithoutAssignments();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC76_CRS-352_Offer Set Up Workflow_Special Event Offer_Assign
	 * Offer Accommodation(s)_CRM Description: To validate that the user is able
	 * to Assign Offer Accommodation(s) Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC016_Create_Special_Event_Offer_Assign_Multiple_Accommodation(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			OfferPage.chooseOfferType();
			OfferPage.chooseSplEventOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.assignAccommodationToOffer();
			OfferPage.assignMultiAccommToOffer();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC004_CRS8_new_WVR_T&C Description: create a new record of terms
	 * and conditions Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC008_Validate_Offer_Transactions_Terms_And_Conditions(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		TermsAndConditionsPage termsAndConditions = new TermsAndConditionsPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			termsAndConditions.chooseTermsAndCondType();
			termsAndConditions.chooseOfferTermsAndCond();
			termsAndConditions.enterTermsAndCondData();
			login.navigateToOffers();
			transaction.selectOwnerRecord();
			transaction.validateOfferTermsandConditions();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC54_CRS-352_Offer Set Up Workflow_Regular Offer_Assign
	 * Premium(s)_CSU Description: To validate that the user is able to Assign
	 * Premium(s) Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch2" })
	public void TC018_Assign_Multiple_Premium_to_Offers(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			OfferPage.chooseOfferType();
			OfferPage.chooseRegularOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.assignPremiumToOffer();
			OfferPage.assignMultiPremiumToOffer();
			OfferPage.editPremiumToOffer();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC15_CRS-259_Restrict assignments to master sales channel_Edit
	 * Business Rules_CRM Description: To validate that system restricts
	 * assignment of Business Rules to Master Sales Channel while editing a
	 * sales channel business rule Date: July/2020 Author: Jyoti_Chowdhury
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC019_Restrict_assignments_MSC_Edit_BusinessRules_CRM(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.searchBusinessRule();
			businessRulePage.editSalesChannel();
			salesPage.clickCancel();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC03_CRS-228_Configure Reservation Fee fields_Offer Sales Channel
	 * record_Override Reservation Fee % Description: To validate that the
	 * system allows the user to overwrite the value of "Reservation Fee %"
	 * field Date: August/2020 Author: Jyoti_Chowdhury Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC020_Configure_OfferSalesChannel_Override_ReservationFee_CSU(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.clickNew();
			businessRulePage.createSalesChannelBusinessRules();
			businessRulePage.getReservationFee();
			businessRulePage.navigateToSaleChannelPage();
			salesPage.editReservationFee();
			businessRulePage.displayUpdatedReservationFee();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC002_CRS35_ASSSC_new
	 * TC005_CRS38_Teams_Mandatory_fields_Validation Description: Assign a new
	 * sales stores to a sales channel Date: August/2020 Author: Jyoti_Chowdhury
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC021_Assign_Salestore_To_SaleChannel_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.assignSaleStoreToSaleChannel();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.validateSalesChannelTeams();
			salesPage.clickCancel();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Function/event: TC004_CRS16_AOSC_edit TC002_CRS36_AASC_new
	 * TC002_CRS37_ATSC_new Description: To edit an assigned offer to sales
	 * channel Designed By: Jyoti_Chowdhury Pre-Requisites: Test Data: 1. Login
	 * with Corporate Super User
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC022_Assign_Offer_To_saleschannel_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		OfferAgentsPage agentsPage = new OfferAgentsPage(tcconfig);
		TeamsPage teamsPage = new TeamsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.validateOffers();
			salesPage.editOffers();
			offerPage.navigateToMenu("2");
			agentsPage.clickNew();
			agentsPage.createOfferAgent();
			offerPage.navigateToMenu("3");
			teamsPage.clickNew();
			teamsPage.createNewTeams();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC57_CRS-352_Offer Set Up Workflow_Face to Face Offer_Assign
	 * Offer Attributes_Warranty Admin. TC016_CRS-430_Offer Attribute Set-up_CRM
	 * Description: Assign Offer Attributes Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC023_Face_To_Face_Assign_Offer_Attributes(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			OfferPage.chooseOfferType();
			OfferPage.chooseFTFOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.assignOfferAttributes();
			OfferPage.setupOfferAttribute();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC61_CRS-352_Offer Set Up Workflow_Bring a Friend Offer_Offer
	 * Status = Review_Error Validation_Legal Registration Program
	 * Number_Warranty Admin Description: validate that the user is required to
	 * add Legal Registration Program Number before marking the Review status as
	 * complete Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC024_Bring_A_Friend_Offer_Review_Error_Validation_Legal_Registration_Program(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			OfferPage.chooseOfferType();
			OfferPage.chooseBAFOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.selectReferralCheckBoxes();
			OfferPage.saveOffer();
			OfferPage.addDestinationToOffer();
			OfferPage.addSalesChannelToOffer();
			OfferPage.addDisclosureToOffer();
			OfferPage.markCompleteWithoutLegalRegNo();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC18_CRS-352_Offer Set Up Workflow_Regular Offer_Offer Status =
	 * Validate_Error Validation_Saving Validation Flag_Same User_CSU
	 * Description: Displays an error on trying to validate the offer by saving
	 * the Validation Flag with the same user who created the offer Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC025_Validate_Offer_SameUser_updates_IsValidated_Flag(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			OfferPage.chooseOfferType();
			OfferPage.chooseRegularOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.addDestinationToOffer();
			OfferPage.addSalesChannelToOffer();
			OfferPage.addDisclosureToOffer();
			OfferPage.enterLegalRegProgramNo();
			OfferPage.validateOfferSameUser();
			login.salesForceLogout();
			login.launchApp();
			login.loginOfferValidationUser();
			OfferPage.selectOfferRecentlyViewed();
			OfferPage.saveValidationDetail();
			login.salesForceLogout();
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			OfferPage.selectOfferRecentlyViewed();
			OfferPage.changeValidatedUser();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC23_CRS-352_Offer Set Up Workflow_Regular Offer_Offer Status =
	 * Complete_Mark Status as Complete_CSU Description: To validate that the
	 * User is able to mark the Offer as Complete after offer validation Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC026_Regular_Offer_Mark_Status_As_Complete(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			OfferPage.chooseOfferType();
			OfferPage.chooseRegularOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.markCompleteWithoutAssignments();
			OfferPage.addDestinationToOffer();
			OfferPage.addSalesChannelToOffer();
			OfferPage.addDisclosureToOffer();
			OfferPage.assignOfferAttributes();
			OfferPage.enterLegalRegProgramNo();
			login.salesForceLogout();
			login.launchApp();
			login.loginOfferValidationUser();
			login.navigateToOffers();
			OfferPage.selectOfferRecentlyViewed();
			OfferPage.validateOffer();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC29_CRS-500_Configure_Accommodation Net Price per Night
	 * field_Error Validation_Overlapping dates_CRM Description: To validate the
	 * system does not save any 2 or more records with overlapping date ranges
	 * Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC027_CRS500_Accommodation_Net_Price_per_Night_field_Error_Validation_Overlapping_dates(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		DestinationsPage destinationsPage = new DestinationsPage(tcconfig);
		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			destinationsPage.createDestinations();
			destinationsPage.editDestinations();
			destinationsPage.navigateToDestinationRelated();
			destinationsPage.SetAccommNetMaxPriceOverlapDates();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC23_CRS-352_Offer Set Up Workflow_Face to Face Offer_Offer
	 * Status = Complete_Mark Status as Complete_CSU Description: Displays an
	 * error on trying to validate the offer by saving the Validation Flag with
	 * the same user who created the offer Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC028_Face_To_Face_Offer_Mark_Status_As_Complete(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			OfferPage.chooseOfferType();
			OfferPage.chooseFTFOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.addDestinationToOffer();
			OfferPage.addSalesChannelToOffer();
			OfferPage.addDisclosureToOffer();
			OfferPage.assignAccommodationToOffer();
			// OfferPage.createOfferExtensionRestriction();
			OfferPage.assignOfferAttributes();
			OfferPage.enterLegalRegProgramNo();
			login.salesForceLogout();
			login.launchApp();
			login.loginOfferValidationUser();
			login.navigateToOffers();
			OfferPage.selectOfferRecentlyViewed();
			OfferPage.validateOffer();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC23_CRS-352_Offer Set Up Workflow_Special Event Offer_Offer
	 * Status = Complete_Mark Status as Complete Description: Validate Special
	 * event offer Date:July/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC029_Special_Event_Offer_Mark_Status_As_Complete(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			OfferPage.chooseOfferType();
			OfferPage.chooseSplEventOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.addDestinationToOffer();
			OfferPage.addSalesChannelToOffer();
			OfferPage.addDisclosureToOffer();
			OfferPage.assignAccommodationToOffer();
			// OfferPage.createOfferExtensionRestriction();
			OfferPage.assignOfferAttributes();
			OfferPage.enterLegalRegProgramNo();
			login.salesForceLogout();
			login.launchApp();
			login.loginOfferValidationUser();
			login.navigateToOffers();
			OfferPage.selectOfferRecentlyViewed();
			OfferPage.validateOffer();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC23_CRS-352_Offer Set Up Workflow_Bring a Friend Offer_Offer
	 * Status = Complete_Mark Status as Complete Description: Validate Bring a
	 * Friend event offer Date:July/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC030_Bring_A_Friend_Offer_Mark_Status_As_Complete(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			OfferPage.chooseOfferType();
			OfferPage.chooseBAFOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.selectReferralCheckBoxes();
			OfferPage.saveOffer();
			OfferPage.addDestinationToOffer();
			OfferPage.addSalesChannelToOffer();
			OfferPage.addDisclosureToOffer();
			OfferPage.enterLegalRegProgramNo();
			login.salesForceLogout();
			login.launchApp();
			login.loginOfferValidationUser();
			login.navigateToOffers();
			OfferPage.selectOfferRecentlyViewed();
			OfferPage.validateOffer();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC06_CRS-74_Multiple Offer Prices per offer_Fee Type = Price,
	 * Points and Price & Points_Face to Face Offer_CSU Description:To validate
	 * that the user is able to select multiple prices for Face To Face Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC031_Face_To_Face_Offer_Offer_Price(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			OfferPage.chooseOfferType();
			OfferPage.chooseFTFOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.addSalesChannelToOffer();
			OfferPage.clickOSClink();
			OfferPage.addPriceToSalesChannel("Price & Points");
			OfferPage.addPriceToSalesChannel("Price");
			OfferPage.addPriceToSalesChannel("Points");
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC07_CRS-259_Restrict assignments to master sales channel_Create
	 * Accommodation_CRM TC04_CRS-259_Restrict assignments to master sales
	 * channel_Create BusinessRules_Warranty Admin
	 * 
	 * Description: To validate that system restricts assignment of Teams to
	 * Master Sales Channel Date: July/2020 Author: Jyoti_Chowdhury Changes By :
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TC032_Restrict_assignments_MSC_BusinessRules_CRM(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		BusinessRulesPage businessRulePage = new BusinessRulesPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			salesPage.searchSalesChannel();
			salesPage.clickRelatedTab();
			salesPage.assignMSCToAccomodationRestricted();
			salesPage.clickCancelUpdated();
			businessRulePage.navigateToBusinessRulesTab();
			businessRulePage.clickNew();
			businessRulePage.validateSaleChannelRestrict();

			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/* TGS-Offer sale UI Scripting */

	/*
	 * Method: TC001_CRS-199_Transfer Offer Button_Payment pending_Owner_CSU
	 * Description: To validate that the system displays an error message on
	 * clicking Transfer Offer button if the offer transaction has Payment
	 * pending Date: Sept/2020 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC033_Transfer_OfferButton_PaymentPending_Lead_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Pending Payments");
			offerTranPage.validateErrorMessage();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC014_CRS-247_Offer Transaction of Owner transferred to a Lead
	 * _Reservation Agent Description: To verify that Offer Transaction record
	 * of a Owner can be transferred to a Lead Pre Condition : 1.Login with
	 * Reservation Agent 2.Active Offer Transaction should be available(Offer
	 * should not be of past date/expired) for Owner 3.Active Offer Transaction
	 * should be available(Offer should not be of past date/expired) for Lead
	 * Date: Sept/2020 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC034_OfferTransaction_Owner_transfer_to_Lead_ReservationAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.searchByDifferentFields();
			offerTranPage.validateLeadName();
			offerTranPage.validatePayemntRecords();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC011_CRS-188_Total extension length = Global Extension
	 * Restriction_Owner_CSU Description: To validate the error message on
	 * clicking the "Add Extension" button when Total extension length > =
	 * Global Extension Restriction Pre Condition : 1. Active Offer Transaction
	 * record 2. Extension already added to Offer Transaction 3. Extension
	 * Restriction is NOT present with any combination of Offer or Destination
	 * or Sales Channel 4. Extension is already added to the Offer Transaction
	 * 5. The Extension Length = Extension Length of Global Extension
	 * Restriction Date: Sept/2020 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC035_Total_extensionlength_Same_GlobalExtnRestriction_Owner_CSU(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.validateExtensionRestrictionMsg();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC011_CRS-247_Offer Transaction of Lead transferred to an Owner
	 * _Reservation Supervisor/Manager Description: To verify that Offer
	 * Transaction record of a Lead can be transferred to an Owner Pre Condition
	 * :1.Login with Reservation Supervisor/Manager 2.Active Offer Transaction
	 * should be available(Offer should not be of past date/expired) for Owner
	 * 3.Active Offer Transaction should be available(Offer should not be of
	 * past date/expired) for Lead Date: Sept/2020 Author: Jyoti_Chowdhury
	 * Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC036_OfferTransaction_Lead_transfer_to_Owner_ReservationManager(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.searchByDifferentFields();
			offerTranPage.validateOwnerName();
			offerTranPage.validatePayemntRecords();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC009_CRS-189_Add new extension_Override Price_Customer Price >
	 * Extension Max Price_Code Matches_Lead_Corporate Super User Description:
	 * To validate the system allows the user to override the extension price
	 * when Customer price entered is more than Extension Maximum Price Pre
	 * Condition :1. Active Offer Transaction record 2. Offer Transaction having
	 * existing Extension with Extension Length < Global extension restriction
	 * 3. Extension Restriction is NOT present with any combination of Offer or
	 * Destination or Sales Channel Date: Oct/2020 Author: Jyoti_Chowdhury
	 * Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC037_Add_extensionOverride_Code_LeadOT_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.validateAddExtension();
			offerTranPage.verifyOverrideCode();
			offerTranPage.validateExtensionFields();
			offerTranPage.navigateOfferTransaction();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC010_CRS-225_Money Transfer_Offer Transaction details payment
	 * record_Owner_Corporate Super User Description: To validate the scenario
	 * when Money Transfer is done for Offer Transaction details payment record
	 * Pre Condition : 1. Offer Transaction is Active 2. Offer Transaction has
	 * not Expired 3. Offer Transaction has Transaction Type = Payment Date:
	 * Oct/2020 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC038_MoneyTransfer_OfferTransaction_paymentrecord_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.selectOwnerRecord();
			offerTranPage.createOfferTransactionFirst();
			offerTranPage.chooseCreditCardPaymentNew();
			offerPage.navigateToMenu("1");
			offerTranPage.selectOwnerRecord();
			offerTranPage.createOfferTransactionSecond();
			offerTranPage.noPaymentScenario();
			offerTranPage.validateMoneyTransfer();
			offerTranPage.compareBalanceDue();
			offerTranPage.validateOldandNewPaymentFields();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC003_CRS-225_Transfer Amount > Balance Due_Lead_Corporate Super
	 * user Description: To validate the scenario when Transferred amount is
	 * more than Balance Due on Offer Transaction Pre Condition : 1. Offer
	 * Transaction is Active 2. Offer Transaction has not Expired 3. Offer
	 * Transaction has Transaction Type = Payment Date: Oct/2020 Author:
	 * Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC039_TransferAmount_greater_BalanceDue_Lead_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerTranPage.selectLeadRecord();
			offerTranPage.createOfferTransactionFirst();
			offerTranPage.chooseCreditCardPaymentNew();
			offerPage.navigateToMenu("1");
			offerTranPage.selectLeadRecord();
			offerTranPage.createOfferTransactionSecond();
			offerTranPage.noPaymentScenario();
			offerTranPage.validateMoneyTransfer();
			offerTranPage.compareTransferBalance();
			offerTranPage.validateOldandNewPaymentFields();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC005_CRS-225_Transfer Amount < Balance Due_Owner_Corporate Super
	 * User Description: To validate the scenario when Transferred amount is
	 * less than Balance Due on Offer Transaction Pre Condition : 1. Offer
	 * Transaction is Active 2. Offer Transaction has not Expired 3. Offer
	 * Transaction has Transaction Type = Payment Date: Oct/2020 Author:
	 * Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch4" })
	public void TC040_TransferAmount_Lesser_BalanceDue_Lead_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerTranPage.selectLeadRecord();
			offerTranPage.createOfferTransactionFirst();
			offerTranPage.chooseCreditCardPaymentNew();
			offerPage.navigateToMenu("2");
			offerTranPage.selectOwnerRecord();
			offerTranPage.createOfferTransactionSecond();
			offerTranPage.noPaymentScenario();
			offerTranPage.validateMoneyTransfer();
			offerTranPage.compareTransferBalance();
			offerTranPage.validateOldandNewPaymentFields();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC009_CRS-182_Successful Cancel OT_Credit Card Paid
	 * Offer_Lead_Regular Offer_Reservation Agent Description:To validate that
	 * the user is navigated to the Payment page to process a Refund on
	 * selecting the cancel reason if the customer paid for the Offer Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC041_Successful_Cancel_OT_Credit_Card_Paid_Lead_Regular_Offer_ReservationAgent(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.selectLeadRecord();
			transaction.createOfferTransaction();
			transaction.chooseCreditCardPayment();
			transaction.checkTransactionStatus();
			transaction.cancelOfferTransaction();
			transaction.refundToAlternateCard();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC005_CRS-182_Successful Cancel OT_Manually Paid
	 * Offer_Onwer_Bring a Friend Offer_CSU Description: To validate that the
	 * user is navigated to the Payment page to process a Refund on selecting
	 * the cancel reason if the customer paid for the Offer Date: September/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC042_Successful_Cancel_OT_Manually_Paid_Owner_BAF_Offer_CSU(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.selectOwnerRecord();
			transaction.createOfferTransaction();
			transaction.chooseManualPayment();
			transaction.cancelOfferTransaction();
			transaction.refundToAlternateCard();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			;
		}
	}

	/*
	 * Method: TC006_CRS-196_Change Premiums Button_Active OT_Existing premium
	 * on Offer_Owner_Corporate Super user Description: To validate that the
	 * system displays list of Premiums if the Offer on the Transaction is set
	 * up for Offer Specific Premiums Date: September/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC043_CRS196_ChangePremiums_Button_Active_OT_Existing_premium_on_Offer_Owner(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.selectOwnerRecord();
			transaction.createOfferTransaction();
			transaction.chooseCreditCardPayment();
			transaction.changeBasePremium();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC012_CRS-196_Add new Premium_No premium on
	 * Offer_Lead_Reservation Supervisor/Manager Description: To validate that
	 * the user is able to save new Premiums if the Offer on the offer
	 * transaction is not set up with Specific Premiums Date: September/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC044_CRS196_Add_New_Premium_No_Premium_on_Offer_Lead_Reservation_Supervisor(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.selectLeadRecord();
			transaction.createOfferTransaction();
			transaction.chooseCreditCardPayment();
			transaction.checkTransactionStatus();
			transaction.changeBasePremium();
			transaction.validateOfferTransaction();

			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC042_CRS-195_Premium Exists on New destination_Upgrade
	 * Available_Previous Destination Change Exists_Upgrade Cost New > Upgrade
	 * Cost Old_Lead_Corporate Super User Description: To validate the
	 * Destination change on Offer transaction based on below conditions: 1.
	 * Premiums available on New Destination 2. Upgrade charge exists for New
	 * Destination 3. Old Destination change record present for offer
	 * transaction 4. New Upgrade Cost > Old Upgrade Cost Date: September/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC045_CRS195_Change_Destination_Upgrade_Cost_New_Greater_than_Old(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.selectLeadRecord();
			transaction.createOfferTransaction();
			transaction.chooseCreditCardPayment();
			transaction.checkTransactionStatus();
			transaction.changeDestination();
			transaction.validateOfferTransaction();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC040_CRS-195_Premium Exists on New destination_Upgrade
	 * Available_Previous Destination Change Exists_Upgrade Cost New = Upgrade
	 * Cost Old_Lead_Reservation Supervisor/Manager Description: To validate the
	 * Destination change on Offer transaction based on below conditions: 1.
	 * Premiums available on New Destination 2. Upgrade charge exists for New
	 * Destination 3. Old Destination change record present for offer
	 * transaction 4. New Upgrade Cost = Old Upgrade Cost Date: September/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC046_CRS195_Change_Destination_Upgrade_Cost_New_Equals_Old(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.selectLeadRecord();
			transaction.createOfferTransaction();
			transaction.chooseCreditCardPayment();
			transaction.checkTransactionStatus();
			transaction.changeDestination();
			transaction.validateOfferTransaction();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC037_CRS-195_Premium Exists on New destination_Upgrade
	 * Available_Previous Destination Change Exists_Upgrade Cost New < Upgrade
	 * Cost Old_Lead_Corporate Super User Description: To validate the
	 * Destination change on Offer transaction based on below conditions: 1.
	 * Premiums available on New Destination 2. Upgrade charge exists for New
	 * Destination 3. Old Destination change record present for offer
	 * transaction 4. New Upgrade Cost < Old Upgrade Cost Date: September/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC047_CRS195_Change_Destination_Upgrade_Cost_New_Less_than_Old(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.selectLeadRecord();
			transaction.createOfferTransaction();
			transaction.chooseCreditCardPayment();
			transaction.checkTransactionStatus();
			transaction.changeDestination();
			transaction.validateOfferTransaction();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC007_CRS-200_OT Detail_Balance Due_Transferred_Lead_CSU
	 * Description: To validate that the Balance Due displayed on the Offer
	 * Transaction Details screen for a Transferred transaction Date:
	 * December/2020 Author: Jyoti Chowdhury Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC048_Verify_BalanceDue_Chargeback_Lead_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.validateChargebackBalanceDue();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC014_CRS-247_Offer Transaction of Owner transferred to a Lead
	 * _Reservation Agent Description: To verify that Offer Transaction record
	 * of a Owner can be transferred to a Lead Pre Condition : 1.Login with
	 * Reservation Agent 2.Active Offer Transaction should be available(Offer
	 * should not be of past date/expired) for Owner 3.Active Offer Transaction
	 * should be available(Offer should not be of past date/expired) for Lead
	 * Date: Sept/2020 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC049_Verify_BalanceDue_Lead_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.searchByDifferentFields();
			offerTranPage.validateLeadName();
			offerTranPage.validateBalanceDue();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			;
		}
	}

	/* TGS-Regression E2E */

	/*
	 * Method: TC016_CRS-38, CRS-2228_To configure Teams Description: To
	 * validate that the user is able to configure Teams Pre Condition :Login
	 * with Corporate Super User Date: January/2021 Author: Jyoti_Chowdhury
	 * Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC001_E2E_Configure_Teams_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		TeamsPage teamsPage = new TeamsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			teamsPage.clickNew();
			teamsPage.createNewTeam();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC001_CRS-119_To Search for Offer Transaction by Customer Name
	 * TC002_CRS-120_To Search for Offer Transaction by Customer Phone
	 * TC003_CRS-124_To Search for Offer Transaction by Customer Email
	 * TC004_117_To Search for Offer Transaction by Offer Transaction Number
	 * Description: To validate the system returns the related offer transaction
	 * number when searched by Customer Name Pre Condition :Login with Corporate
	 * Super User Date: January/2021 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC002_E2E_Search_Offer_Transaction_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchByCustomerName();
			offerPage.navigateToMenu("1");
			offerTranPage.searchByCustomerPhone();
			offerPage.navigateToMenu("1");
			offerTranPage.searchByCustomerEmail();
			offerPage.navigateToMenu("1");
			offerTranPage.searchByOfferTransactionNumber();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC005_CRS-118_To Search for Offer Transaction by Payment
	 * information Description: To validate the system returns the related offer
	 * transaction number when searched by Payment information Pre Condition
	 * :Login with Corporate Super User Date: January/2021 Author:
	 * Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC003_E2E_Search_using_Payments_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		PaymentRecordsPage paymentPage = new PaymentRecordsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			paymentPage.searchByPaymentRecords();
			// .getPaymentRecord();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC006_CRS-116_To Search for Offer Transaction by Call Recording
	 * Number Description: To validate the system returns the related offer
	 * transaction number when searched by Call Recording Number Pre Condition
	 * :1. Offer Transaction should have values in Call Recording Number 1' and
	 * 'Call Recording Number 2 fields (Call Recording Number fields are present
	 * in Verifier Information page while creating Offer Transaction) Date:
	 * January/2021 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC004_E2E_Search_using_Call_Recording_Number_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.searchCallRecordNumber();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC005_To validate Book Tour process Description: To validate that
	 * the user is able to Book Tour Pre Condition :Offer Transaction Record
	 * should have reservation booked Date: January/2021 Author: Jyoti_Chowdhury
	 * Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC005_E2E_validate_BookTour_process_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.compareDates();
			offerTranPage.validateBookTour();

			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC003_To validate the Offer Reservation Booking Description: To
	 * validate that the user is able to create offer reservation booking Pre
	 * Condition :1. Offer Transaction Record should be created 2. No Balance
	 * Due on Offer Transaction record Date: January/2021 Author:
	 * Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC006_E2E_validate_OfferReservation_Booking_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.storeNoOfNight();
			offerTranPage.validateBookReservation();
			offerTranPage.validateIntregrationStatus("Res Book Success");
			offerTranPage.displayOfferReservation();
			// offerTranPage.validateBookTour();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC013_To validate the Offer Reservation Modification Description:
	 * To validate that the user is able to do Offer Reservation Modification
	 * Pre Condition :1. Offer Transaction Record should have reservation booked
	 * Date: January/2021 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC007_E2E_validate_OfferReservation_Modification_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.validateOfferReservationPresent();
			offerTranPage.validateOfferBookModification();
			offerTranPage.validateIntregrationStatus("Res ReBook Success");
			offerTranPage.validateActiveLineItemsUpdated();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC004_To validate the Offer Reservation Cancellation Description:
	 * To validate that the user is able to cancel the offer reservation booking
	 * Pre Condition :1. Offer Transaction Record should have reservation booked
	 * Date: January/2021 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC008_E2E_validate_OfferReservation_Cancellation_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.validateOfferReservationPresent();
			offerTranPage.cancelReservation();
			offerTranPage.validateIntregrationStatus("Res Cancel Success");
			offerTranPage.validateBalnceDueandCreditAvailiable();
			offerTranPage.cancellationLineItems();
			offerTranPage.processRefundOnBooking();
			offerTranPage.validateRefundStatusInPaymentTab();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC008 CRS-455 Validate Get Appointment action for Offer with No
	 * Accommodation Description: Verify the appointment for Primary Guest for
	 * an Offer transaction with No accommodations Pre Condition :Offer
	 * Transactions created from Offer with No accommodation should be
	 * available. Date: Febuary/2021 Author: Jyoti_Chowdhury Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC009_E2E_Get_Appointment_with_NoAccommodation_ReservationAgent(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.bookTourSelectDates();
			offerTranPage.getSalesStoreAndAppointment();
			offerTranPage.switchToParent();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: TC011_Book_Extra_Room Description:To validate Extra room Booking
	 * and Promocode discounts and taxes Date: February/2021 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC011_Book_Extra_Room_Promocode(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			transaction.navigateToOfferTransaction();
			transaction.bookExtraRoom();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC004_CRS959_Apply_Adjustment_Credit_to_Payment_Adjustment
	 * Description:To validate credit adjustment Date: February/2021 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC004_CRS959_Apply_Adjustment_Credit_to_Payment_Adjustment(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		LoginPage login = new LoginPage(tcconfig);
		OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");	
			///transaction.navigateToOfferTransaction();
			transaction.searchOfferTransaction();
			transaction.verifyOfferTransactionStatus("Active");
			transaction.processAdjustment(); 
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	   Method:
	   TC006_CRS76_CRS78_CRS79_CRS_254_Validate_Offer_Availability_functionality
	   Description: TC006_CRS76_CRS78_CRS79_CRS-254 Validate Offer Availability functionality
	   Reservation Booking with NO Referral Identified Date: February/2021
	   Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC006_CRS76_CRS78_CRS79_CRS_254_Validate_Offer_Availability_functionality(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToOffersTab();
			offerPage.clickOffersAvailablity();
			offerPage.clickNextOnOffersAvailablity();
			offerPage.clickNextOnOffersAvailablityWithMandatoryFields();
			offerTranPage.slectOfferOnOfferSelection();
			offerTranPage.slectNextButtonOnOfferDeatail();
			offerTranPage.selectDestinationAndTapNext();
			offerTranPage.tapNextOnAvailableAccomodation();
			offerTranPage.validateFieldsOnDisplayPrem();
			login.salesForceLogout();

		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			;
		}
	} 

	/*
    Method:
      TC027_CRS_5145_tour_in_booked_status_is_cancelled_if_the_tour_book_date_does_not_falls_between_
	  checkin_and_checkout_date_while_offer_reservation_modification
      Description: TC027 CRS-5145 tour in booked status is cancelled if the tour book date does not falls 
	  between checkin and checkout date while offer reservation modification  Identified Date: February/2021
      Author: Shubhendra Vaishya By: NA
	 */


	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC027_CRS_5145_tour_in_booked_status_is_cancelled_if_the_tour_book_date_does_not_falls_between_checkin_and_checkout_date_while_offer_reservation_modification(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);		
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();			
			offerPage.navigateToMenu("1");	
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.validateOfferReservationPresent();
			offerTranPage.validateOfferBookModificationToCheckBookedStatusIsCancelled();
			offerTranPage.validateIntregrationStatus("Res ReBook Success");
			offerTranPage.validateTourDetailsNotPresentInOfferTransactionOnceReservationDatesModified();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}
	/*
    Method:
     TC014_To_validate_the_Extra_Room_Modification
     Description: TC014_To validate the Extra Room Modification
     Identified Date: February/2021
     Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC014_To_validate_the_Extra_Room_Modification(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);		
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();			
			offerPage.navigateToMenu("1");	
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.validateOfferReservationPresent();
			offerTranPage.extraRoomModification();
			offerTranPage.validateIntregrationStatus("Extra ReBook Success");
			//offerTranPage.validateActiveLineItemsUpdated();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
    Method:
     TC008_To_validate_the_cancellation_of_Extra_Room_Reservation
     Description: TC008_To validate the cancellation of Extra Room Reservation
     Identified Date: February/2021
     Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC008_To_validate_the_cancellation_of_Extra_Room_Reservation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);		
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();			
			offerPage.navigateToMenu("1");	
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.validateOfferReservationPresent();
			offerTranPage.cancelExtraRoomReservation();
			offerTranPage.validateIntregrationStatus("Extra Cancel Success");
			offerTranPage.validateBalnceDueandCreditAvailiable();
			offerTranPage.cancellationLineItems();
			offerTranPage.processRefundOnBookingtoOrignalCard();
			offerTranPage.validateRefundStatusInPaymentTab();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
    Method:
     TC001_CRS_1379_CRS_487_Add_Multiple_Destination_and_Offers_to_Promo_Code_and_check_error_message_on_
	 Apply_PromoCode_page_when_no_Upgrade_available_during_reservation
     Description: TC001 CRS 1379 Add Multiple Destination and Offers to Promo Code 
     Identified Date: February/2021
     Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC001_CRS_1379_CRS_487_Add_Multiple_Destination_and_Offers_to_Promo_Code_and_check_error_message_on_Apply_PromoCode_page_when_no_Upgrade_available_during_reservation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);		
		OfferPage offerPage = new OfferPage(tcconfig);
		PromoCodePage promoCodePage = new PromoCodePage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			promoCodePage.clickNewOnPromoCode();
			promoCodePage.createNewPromoCode();
			promoCodePage.clickRelatedTabofPromocodeDetail();
			promoCodePage.AddMultipleDesinationAndOffer1(); 
			promoCodePage.AddMultipleDesinationAndOffer2(); 
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}
	/*
    Method:
	  TC005_CRS_448_and_CRS_524_Check_Accommodation_Availability_with_No_
	  Accommodations_on_offer_and_Sales_Channel_has_Accommodation_Restrictions
      Description: TC005 CRS-448 & CRS-524 Check Accommodation Availability with NO 
	  Accommodations on Offer and Sales Channel has Accommodation Restrictions
      Identified Date: February/2021
      Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC005_CRS_448_and_CRS_524_Check_Accommodation_Availability_with_No_Accommodations_on_offer_and_Sales_Channel_has_Accommodation_Restrictions(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);		
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();			
			offerPage.navigateToMenu("1");	
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.validateBookReservationWithNoAccomodationOnOfferAndSaleChannelhasAccomodationRestrictions();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
    Method:
	  TC003_CRS_463_Cancel_Special_Event_Reservation_which_has_No_Tour_and_No_Referral_
	  and_Cancel_Date_less_than_15_days_from_OT_Create_Date_With_Cancel_Fee
      Description: TC003 CRS-463 Cancel Special Event Reservation which has No Tour & No 
	  Referral and Cancel Date < 15 days from OT Create Date With Cancel Fee
      Identified Date: February/2021
      Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC003_CRS_463_Cancel_Special_Event_Reservation_which_has_No_Tour_and_No_Referral_and_Cancel_Date_less_than_15_days_from_OT_Create_Date_With_Cancel_Fee(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);		
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();			
			offerPage.navigateToMenu("1");	
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.validateOfferReservationPresent();
			offerTranPage.cancelReservationforOverRiddenCode();
			offerTranPage.validateIntregrationStatus("Res Cancel Success");
			offerTranPage.validateBalnceDueandCreditAvailiable();
			offerTranPage.cancellationLineItems();
			//offerTranPage.processRefundOnBooking();
			offerTranPage.processRefundOnBookingtoOrignalCard();
			offerTranPage.validateRefundStatusInPaymentTab();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}
	/*
    Method:
	  TC012_To_Create_Extension_and_apply_to_Offer_Transaction
      Description: TC012_To Create Extension and apply to Offer Transaction
	  TC003_Create_new_Extension
	  TC035_Total_extensionlength_Same_GlobalExtnRestriction_Owner_CSU
      TC037_Add_extensionOverride_Code_LeadOT_CSU
     Identified Date: February/2021
     Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch1" })
	public void TC012_To_Create_Extension_and_apply_to_Offer_Transaction(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		HomePage homePage = new HomePage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		SalesChannelPage salesPage = new SalesChannelPage(tcconfig);
		ExtensionsPage extnPage = new ExtensionsPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffersDiffLogin();
			offerPage.navigateToMenu("1");
			extnPage.clickNewbtn();
			extnPage.createExtensionRecordUpdated();
			offerPage.navigateToMenu("2");
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.validateAddExtension();
			offerTranPage.verifyOverrideCode();
			offerTranPage.validateExtensionFields();
			//extnPage.deleteExtension();
			//extnPage.searchOfferExtensionNew(""); 
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
    Method:
	 TC015_To_validate_the_change_destination_process_before_and_during_offer_reservation_booking
     Description: TC015_To validate the change destination process before and during offer reservation booking
     Identified Date: February/2021
     Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC015_To_validate_the_change_destination_process_before_and_during_offer_reservation_booking(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();			
			offerPage.navigateToMenu("1");	
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Active");
			offerTranPage.changeDestinationHavingUpgradeFees();
			offerTranPage.validateActiveLineItemsforDistnationChange();
			offerTranPage.validateBookingNotAllowedOnBalanceDueDuringReservation();
			offerTranPage.processPaymentForBalanceDue();
			offerTranPage.validateBookReservationForDestChange();
			offerTranPage.validateIntregrationStatus("Res Book Success");
			offerTranPage.displayOfferReservation();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}
	/*
      Method:
	  TC005_CRS_966_CRS_5212_Validate_the_list_of_Add_on_Premium_and_Delete_Premium_on_OT_Deletes_Promised_Premiums_on_Tour
      Description: TC005 CRS-966, CRS-5212 Validate the list of Add-on Premium and Delete Premium on 
	  OT Deletes  Promised Premiums on Tour  Identified Date: February/2021  Author: Shubhendra Vaishya By: NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC005_CRS_966_CRS_5212_Validate_the_list_of_Add_on_Premium_and_Delete_Premium_on_OT_Deletes_Promised_Premiums_on_Tour(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();			
			offerPage.navigateToMenu("1");	
			offerTranPage.searchOfferTransaction();
			offerTranPage.verifyOfferTransactionStatus("Reservation Booked");
			offerTranPage.validateOpeningAndClosingofAddOnPrimiumWindow();
			offerTranPage.validateActiveLineItemsBasePremiumOnRelated();
			offerTranPage.validatePromisedItemOnIncetiveTabOfTourDetail();
			offerTranPage.changeBasePremium();
			offerTranPage.validateOfferTransaction();
			login.salesForceLogout();
		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method:
	 * TC007_CRS5142_Validate_error_message_BAF_Offer_Reservation_Booking_with_NO_Referral
	 * Description: TC007 CRS-5142 Validate error message for BAF Offer
	 * Reservation Booking with NO Referral Identified Date: January/2021
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TC007_CRS5142_Validate_error_message_BAF_Offer_Reservation_Booking_with_NO_Referral(Map<String, String> testData) throws Exception {setupTestData(testData);
	LoginPage login = new LoginPage(tcconfig);
	OfferTransactionsPage transaction = new OfferTransactionsPage(tcconfig);

	try {
		login.launchApp();
		login.loginToSalesForceDiffUsers();
		login.navigateToOffers();
		transaction.selectLeadRecord();
		transaction.createOfferTransaction();
		transaction.chooseCreditCardPayment();
		transaction.checkTransactionStatus();
		transaction.validateBAFReferral();
		login.salesForceLogout();
	} catch (Exception e) {
		login.salesForceLogout();
		Assert.assertTrue(false,
				getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
	}
	}

	//Prod Val Scenario For Demo

	/*
	 * Method: TGS_Prod_Val_TC026_Regular_Offer_Mark_Status_As_Complete
	 * Complete_CSU Description: To validate that the
	 * User is able to mark the Offer as Complete after offer validation Date:
	 * March/2021 Author: Shubhendra Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "tgs", "batch3" })
	public void TGS_Prod_Val_TC026_Regular_Offer_Mark_Status_As_Complete(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage OfferPage = new OfferPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			OfferPage.chooseOfferType();
			OfferPage.chooseRegularOffer();
			OfferPage.getOfferNameRandom();
			OfferPage.enterOfferData();
			OfferPage.saveOffer();
			OfferPage.addDestinationToOffer();
			OfferPage.addSalesChannelToOffer();
			login.navigateToOffers();
			OfferPage.selectOfferRecentlyCreated();
			login.salesForceLogout();

		} catch (Exception e) {
			login.salesForceLogout();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Method: TC004_117_To Search for Offer Transaction by Offer Transaction Number
	 * Description: To validate the system returns the related offer transaction
	 * number when searched by offer transaction number Pre Condition :Login with Corporate
	 * Super User Date: Mar/2021 Author: Shubhendra Changes By : NA
	 */

	@Test(dataProvider = "testData", groups = { "tgs", "batch5" })
	public void TGS_Prod_Val_TC002_E2E_Search_Offer_Transaction_CSU(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		LoginPage login = new LoginPage(tcconfig);
		OfferPage offerPage = new OfferPage(tcconfig);
		OfferTransactionsPage offerTranPage = new OfferTransactionsPage(tcconfig);

		try {
			login.launchApp();
			login.loginToSalesForceDiffUsers();
			login.navigateToOffers();
			offerPage.navigateToMenu("1");
			offerTranPage.searchByOfferTransactionNumber();
			login.salesForceLogout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}




}
