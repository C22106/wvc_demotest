
package tgs.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class DestinationsPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(DestinationsPage.class);

	public DestinationsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	/* <Xpath by Krishnaraj> */
	/* Destinations Main Page elements */
	protected static String Destination1, Accommodation1;
	protected By destinationsTab = By.xpath("//span[@class='slds-truncate' and text()='Destinations']");
	protected By destinationNameLink = By.xpath("//a[@title='" + testData.get("Destination") + "']");
	protected By newDestinationstitle = By.xpath("//h2[text()='New Destination']");
	protected By newButton = By.xpath("//div[@title='New']");

	/* New Destination form elements */
	protected By destinationsField = By.xpath("//input[@name='Name']");
	/// protected By welcomCenterField = By.xpath("//label[text()= 'Welcome
	/// Center Name']/..//input");
	protected By welcomCenterField = By.xpath("//input[@name='Welcome_Center_Name__c']");
	// protected By cityField = By.xpath("//label[text()= 'City']/..//input");
	protected By cityField = By.xpath("//input[@name='City__c']");
	/// protected By stateField = By.xpath("//label[text()='State']/..//input");
	protected By stateField = By.xpath(
			"//label[text()='State']/following::div[@class='slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click']");
	protected By stateSelect = By.xpath(
			"//div[@class='slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds-dropdown_length-with-icon-7']/lightning-base-combobox-item[7]");
	protected By upgradeDestFlagChkbox = By.xpath("//span[@class='slds-checkbox slds-checkbox_standalone']//input[@name='Upgrade_Destination_flag__c']");
	protected By upgradeChargeperNightField = By.xpath("//label[text()= 'Upgrade Charge per Night']/..//input");
	protected By disclouresChkbox = By.xpath("//label[text()='Disclosures']/..//textarea");
	protected By saveButton = By.xpath("//button[@name='SaveEdit']");
	protected By saveButtonOnAddAssign = By.xpath("//button[@title='Save']");
	protected By cancelButton = By.xpath("//button[@title='Cancel']");
	protected By saveSnagError = By.xpath("//h2[text()='We hit a snag.']");

	/* Selected Destination form elements */
	protected By nextMonth = By.xpath("//a[@title='Go to next month']");
	protected By startDate = By.xpath("//td[@class='uiDayInMonthCell']//span[text()='3']");
	protected By endDate = By.xpath("//td[@class='uiDayInMonthCell']//span[text()='7']");
	protected By endDate2 = By.xpath("//td[@class='uiDayInMonthCell']//span[text()='10']");
	protected By toastErrorMessage = By.xpath(
			"//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage' and @data-key = 'error']");
	protected By errorMessage = By.xpath("//ul[@class='errorsList']");
	protected By relatedSectionLink = By.xpath("//li[@class='slds-tabs_default__item' and @title='Related']");
	protected By editButton = By.xpath("//button[@name='Edit']");
	protected By addPremiumButton = By.xpath("//button[text()='Add Premium Assigned']");
	protected By destinationName = By
			.xpath("//div/span[text()='Destinations Name']/../../../..//lightning-formatted-text");
	protected By AccommRetailPriceMinValue = By
			.xpath("//span[text()='Accommodation Retail Price Minimum']/../..//slot/lightning-formatted-text");
	protected By AccommRetailPriceMaxValue = By
			.xpath("//span[text()='Accommodation Retail Price Maximum']/../..//slot/lightning-formatted-text");
	protected By AccommTaxMinValue = By
			.xpath("//span[text()='Accommodation Tax Minimum']/../..//slot/lightning-formatted-text");
	protected By AccommTaxMaxValue = By
			.xpath("//span[text()='Accommodation Tax Maximum']/../..//slot/lightning-formatted-text");
	protected By AccommFeeMinValue = By
			.xpath("//span[text()='Accommodation Fee Minimum']/../..//slot/lightning-formatted-text");
	protected By AccommFeeMaxValue = By
			.xpath("//span[text()='Accommodation Fee Maximum']/../..//slot/lightning-formatted-text");
	protected By AccommParkingFeeMinValue = By
			.xpath("//span[text()='Accommodation Parking Fee Minimum']/../..//slotlightning-formatted-text");
	protected By AccommParkingFeeMaxValue = By
			.xpath("//span[text()='Accommodation Parking Fee Maximum']/../..//slot/lightning-formatted-text");

	/* Edit Destination form elements */
	protected By editDestinationTitle = (By.xpath("//h2[contains(text(),'Edit " + testData.get("Destination") + "')]"));
	protected By successMessageTitle = By
			.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By successNewMessageTitle = By
			.xpath("//span[@data-aura-class='forceActionsText' and text()='\"" + testData.get("Destination") + "\"']");

	/* Destination Related Tab elements */
	protected By relatedSection = By
			.xpath("//flexipage-tab2[@class='slds-tabs_default__content slds-show' and @id='tab-3']");
	protected By accommodationNewButton = By.xpath("//span[@title='Accommodations']//ancestor::article//button[@name='New']");
	protected By accommNetMaxPriceNewButton = By
			.xpath("//span[@title='Accommodation Net Max Price Per Night']/following::a[1]/div[@title='New']");
	protected By salesStoreNewButton = By
			.xpath("//span[@title='Destination Stores Assigned']//ancestor::article//button[@name='New']");
	protected By Destinationlinktotest = By.xpath("//a[@title='DestRNVZIBP']");

	/* Accommodation new form elements */
	protected By newAccommodationTitle = By.xpath("//h2[text()='New Accommodation']");
	protected By accommodationNameField = By.xpath("//input[@name='Name']");
	protected By accommodationRetailPriceMinField = By.xpath("//input[@name='Accommodation_Retail_Price_Minimum__c']");
	protected By accommodationRetailPriceMaxField = By.xpath("//input[@name='Accommodation_Retail_Price_Maximum__c']");
	// protected By resortFeesIncludedChkBox = By.xpath("//div[label[contains(.,
	// 'Resort Fees Included?')]]/input");
	protected By resortFeesIncludedChkBox = By.xpath("//input[@name='Resort_Fees_Included__c']");
	protected By accommodationResortFeeField = By.xpath("//input[@name='Accommodation_Resort_Fee__c']");;
	protected By taxesIncludedChkBox = By.xpath("//input[@name='Taxes_Included__c']");
	protected By accommodationTaxField = By.xpath("//input[@name='Accommodation_Tax__c']");
	protected By accommodationParkingFeeMinField = By.xpath("//input[@name='Accommodation_Parking_Fee_Minimum__c']");
	protected By accommodationParkingFeeMaxField = By.xpath("//input[@name='Accommodation_Parking_Fee_Maximum__c']");
	protected By effStartDate = By.xpath("//input[@name='Effective_Start_Date__c']");
	protected By effDateAcc = By.xpath("//input[@name='Effective_Date__c']");
	protected By effEndDate = By.xpath("//input[@name='Effective_End_Date__c']");
	protected By streetField = By.xpath("//label[text()='Street Address1']/..//input");
	protected By postalCodeField = By.xpath("//input[@name='Postal_Code__c']");
	/*
	 * protected By effStartDateCal = By.xpath(
	 * "//span[text()='Effective Start Date']/following::a[1]"); protected By
	 * effEndDateCal = By.xpath(
	 * "//span[text()='Effective End Date']/following::a[1]");
	 */
	protected By dateLink = By.xpath("//button[text()='Today']");

	/* Accommodation Net Max Price Per Night form elements */
	protected By netMaxPricePerNightTitle = By.xpath("//h2[text()='New Accommodation Net Max Price Per Night']");
	protected By netMaxPricePerNightField = By.xpath("//input[@name='Net_Max_Price_Per_Night__c']");
	protected By nextMonthAccNetMax = By.xpath("//button[@title='Next Month']");
	protected By startDateAccNetMax = By.xpath("//span[text()='3']");
	protected By endDateAccNetMax = By.xpath("//span[text()='7']");
	protected By endDate2AccNetMax = By.xpath("//span[text()='10']");
	protected By getSaveSnagError = By.xpath("//div[@class='pageLevelErrors']");
	

	/* Assign Sales Store New form elements */
	protected By saleStoreTitle = By.xpath("//h2[text()='New Destination Stores Assigned']");
	protected By salesStoreLookUpBox = By.xpath("//input[@placeholder='Search Sales Stores...']");
	protected By salesStoreSelect = By
			.xpath("//lightning-base-combobox-formatted-text[@title='" + testData.get("SalesStore") + "']");

	/* Add Premium Assigned New form elements */
	protected By addPremiumTitle = By.xpath("//h2[text()='Add Premium Assigned']");
	protected By productLookUpBox = By.xpath("//input[@placeholder='search..']");
	protected By productSelect = By.xpath(
			"//ul[@class='slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-lookup__menu slds']//li[1]");
	protected By startDateCal = By.xpath("//input[@name='startDate']");
	protected By endDateCal = By.xpath("//input[@name='endDate']");
	protected By currentDateCal = By.xpath("//span[@class='slds-is-today']");
	protected By successPremiumTitle = By.xpath("//span[@class='toastMessage forceActionsText']");

	/*
	 * Method: createDestinations Description: Create new destination Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void createDestinations() {
		clickElementJSWithWait(destinationsTab);
		clickElementBy(newButton);
		if (verifyTitleDisplayed(newDestinationstitle, "DestinationsPage", "createDestinations")) {
			// fieldEnterData(destinationsField, testData.get("Destination"));
			Destination1 = "DestinationQA " + getRandomStringText(5);
			fieldEnterData(destinationsField, Destination1);
			fieldEnterData(welcomCenterField, "Welcome to automated " + getRandomStringText(7));
			fieldEnterData(cityField, testData.get("City"));
			clickElementBy(stateField);
			selectElement("Create Destinations", stateSelect, "State");
			fieldEnterData(disclouresChkbox, "Automated " + getRandomStringText(7));
			tcConfig.updateTestReporter("DestinationsPage", "createDestinations", Status.PASS,
					"Able to enter data in the Destinations form");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "createDestinations", Status.PASS,
					"Unable to enter data in the Destinations form");
		}
		verifySave("DestinationsPage", "createDestinations", saveButton, successMessageTitle, saveSnagError,
				cancelButton);
		// verifySuccessDisplayed(successMessageTitle, "DestinationsPage",
		// "createDestinations");
		// String DestinationName = getElementValue(destinationName);
	}

	/*
	 * Method: editDestinations Description: Edit the destination Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void editDestinations() {
		if (verifyObjectDisplayed(editButton)) {
			clickElementBy(editButton);
			fieldEnterData(welcomCenterField, "Automation edited");
			selectCheckBox(upgradeDestFlagChkbox);
			String upgradeCharge = getRandomString(2);
			fieldEnterData(upgradeChargeperNightField, upgradeCharge);
			tcConfig.updateTestReporter("DestinationsPage", "editDestinations", Status.PASS,
					"Able to edit data in the Destinations form");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "editDestinations", Status.FAIL,
					"Unable to edit data in the Destinations form");
		}
		selectElement("DestinationsPage", saveButton, "Save button");
		verifySuccessDisplayed(successMessageTitle, "DestinationsPage", "editDestinations");
	}

	/*
	 * Method: destinationRelated Description: Navigate to destination related
	 * section Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	public void navigateToDestinationRelated() {
		clickElementBy(relatedSectionLink);
		waitUntilElementVisibleBy(driver, relatedSection, "ExplicitLongWait");
		if (verifyObjectEnabled(relatedSection)) {
			tcConfig.updateTestReporter("DestinationsPage", "destinationRelated", Status.PASS,
					"Successfully navigated to Destination related tab section");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "destinationRelated", Status.FAIL,
					"Error while navigating to Destination related tab section");
		}
	}

	/*
	 * Method: addAccommodation Description: Add new accommodation to
	 * destination Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA Checkbox and textbox are mutually exclusive in this page
	 */
	public void addAccommodation() {
		clickElementJSWithWait(accommodationNewButton);
		if (verifyTitleDisplayed(newAccommodationTitle, "DestinationsPage", "addAccommodation")) {
			Accommodation1 = "Accom" + getRandomStringText(7);
			fieldEnterData(accommodationNameField, Accommodation1);
			fieldEnterData(accommodationRetailPriceMinField, testData.get("AccommRetailPriceMin"));
			fieldEnterData(accommodationRetailPriceMaxField, testData.get("AccommRetailPriceMax"));
			fieldEnterData(accommodationResortFeeField, testData.get("AccommResortFee"));
			fieldEnterData(accommodationTaxField, testData.get("AccommTax"));
			fieldEnterData(accommodationParkingFeeMinField, testData.get("AccommResortFeeMin"));
			fieldEnterData(accommodationParkingFeeMaxField, testData.get("AccommResortFeeMax"));
			scrollDownForElementJSBy(effDateAcc);
			clickElementBy(effDateAcc);
			clickElementBy(dateLink);
			fieldEnterData(streetField, "6277 " + getRandomStringText(7));
			fieldEnterData(cityField, testData.get("City"));
			fieldEnterData(postalCodeField, getRandomString(5));
		    waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(stateField);
			clickElementBy(stateSelect);
			tcConfig.updateTestReporter("DestinationsPage", "addAccommodation", Status.PASS,
					"Able to enter data in the Accommodation form");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "addAccommodation", Status.FAIL,
					"Unable to enter data in the Accommodation form");
		}
		verifySave("DestinationsPage", "addAccommodation", saveButton, successMessageTitle, saveSnagError,
				cancelButton);
		/*
		 * clickElementBy(saveButton);
		 * verifySuccessDisplayed(successMessageTitle, "DestinationsPage",
		 * "addAccommodation");
		 */
	}

	/*
	 * Method: SetAccommNetMaxPricePerNight Description: Set up Accommodation
	 * Net Max Price Per Night for a destination Val Date: July/2020 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void setAccommNetMaxPricePerNight() {
		scrollDownViaKeyboard(5);
		clickElementJSWithWait(accommNetMaxPriceNewButton);
		if (verifyTitleDisplayed(netMaxPricePerNightTitle, "DestinationsPage", "setAccommNetMaxPricePerNight")) {
			String NetMaxPrice = getRandomString(2);
			fieldEnterData(netMaxPricePerNightField, NetMaxPrice);
			clickElementBy(effStartDate);
			clickElementBy(dateLink);
		    chooseCalDate(effEndDate, nextMonthAccNetMax, endDateAccNetMax);
			tcConfig.updateTestReporter("DestinationsPage", "setAccommNetMaxPricePerNight", Status.PASS,
					"Able to enter data in the Accommodation Net Max Price Per Night form");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "setAccommNetMaxPricePerNight", Status.FAIL,
					"Unable to enter data in the Accommodation Net Max Price Per Night form");
		}
		verifySave("DestinationsPage", "setAccommNetMaxPricePerNight", saveButton, successMessageTitle, saveSnagError,
				cancelButton);
		/*
		 * clickElementBy(saveButton);
		 * verifySuccessDisplayed(successMessageTitle, "DestinationsPage",
		 * "setAccommNetMaxPricePerNight");
		 */
	}

	/*
	 * Method: SetAccommNetMaxPriceOverlapDates Description: Set up
	 * Accommodation Net Max Price Per Night for a destination with overlapping
	 * dates Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void SetAccommNetMaxPriceOverlapDates() {
		setAccommNetMaxPricePerNight();
		scrollDownForElementJSBy(accommNetMaxPriceNewButton);
		clickElementJSWithWait(accommNetMaxPriceNewButton);
		if (verifyTitleDisplayed(netMaxPricePerNightTitle, "DestinationsPage", "setAccommNetMaxPricePerNight")) {
			String NetMaxPrice2 = getRandomString(2);
			fieldEnterData(netMaxPricePerNightField, NetMaxPrice2);
			chooseCalDate(effStartDate, nextMonthAccNetMax, startDateAccNetMax);
			chooseCalDate(effEndDate, nextMonthAccNetMax, endDate2AccNetMax);
			tcConfig.updateTestReporter("DestinationsPage", "SetAccommNetMaxPriceOverlapDates", Status.PASS,
					"Overlapping dates entered in the Accommodation Net Max Price Per Night form");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "SetAccommNetMaxPriceOverlapDates", Status.FAIL,
					"Unable to enter Overlapping dates in the Accommodation Net Max Price Per Night form");
		}
		clickElementBy(saveButton);
		verifyPageTitleDisplayed(getSaveSnagError, "Unable to add Accommodation Net Max Price with overlapping dates",
				"DestinationsPage", "SetAccommNetMaxPriceOverlapDates");
		clickElementBy(cancelButton);
	}

	/*
	 * Method: assignSalesStoreToDestination Description: Assign new Sales Store
	 * to destination Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void assignSalesStoreToDestination() {
		clickElementJSWithWait(salesStoreNewButton);
		if (verifyTitleDisplayed(saleStoreTitle, "DestinationsPage", "assignSalesStoreToDestination")) {
			fieldEnterData(salesStoreLookUpBox, testData.get("SalesStore"));
			clickElementBy(salesStoreSelect);
			clickElementBy(effStartDate);
			clickElementBy(dateLink);
			clickElementBy(effEndDate);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("DestinationsPage", "assignSalesStoreToDestination", Status.PASS,
					"Able to enter data in the Sales Store form");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "assignSalesStoreToDestination", Status.FAIL,
					"Unable to enter data in the Sales Store form");
		}
		verifySave("DestinationsPage", "assignSalesStoreToDestination", saveButton, successMessageTitle, saveSnagError,
				cancelButton);
		/*
		 * clickElementBy(saveButton);
		 * verifySuccessDisplayed(successMessageTitle, "DestinationsPage",
		 * "assignSalesStoreToDestination");
		 */
	}

	/*
	 * Method: assignPremiumtoDestination Description: Assign new premium to
	 * destination Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void assignPremiumtoDestination() {
		// clickElementBy(destinationNameLink);
		clickElementBy(addPremiumButton);
		if (verifyTitleDisplayed(addPremiumTitle, "DestinationsPage", "assignPremiumtoDestination")) {
			clickElementBy(productLookUpBox);
			clickElementBy(productSelect);
			clickElementBy(startDateCal);
			clickElementBy(dateLink);
			clickElementBy(endDateCal);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("DestinationsPage", "assignPremiumtoDestination", Status.PASS,
					"Able to enter data in the Add Premium form");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "assignPremiumtoDestination", Status.FAIL,
					"Unable to enter data in the Add Premium form");
		}
		verifySave("DestinationsPage", "assignPremiumtoDestination", saveButtonOnAddAssign, successPremiumTitle, saveSnagError,
				cancelButton);
		   
	}

	/*
	 * Method: verifyAccommodationFee Description: Verfiy Accommodation Fee
	 * section Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	public void verifyAccommodationFee() {

		waitUntilElementVisibleBy(driver, relatedSection, "ExplicitLongWait");

		List<String> accom1Fee = new ArrayList<String>();
		List<String> accom2Fee = new ArrayList<String>();

		accom1Fee.add(getElementValue(AccommRetailPriceMinValue));
		accom1Fee.add(getElementValue(AccommRetailPriceMaxValue));
		accom1Fee.add(getElementValue(AccommTaxMinValue));
		accom1Fee.add(getElementValue(AccommTaxMaxValue));
		accom1Fee.add(getElementValue(AccommFeeMinValue));
		accom1Fee.add(getElementValue(AccommFeeMaxValue));
		accom1Fee.add(getElementValue(AccommParkingFeeMinValue));
		accom1Fee.add(getElementValue(AccommParkingFeeMaxValue));

		accom2Fee.add(getElementValue(AccommRetailPriceMinValue));
		accom2Fee.add(getElementValue(AccommRetailPriceMaxValue));
		accom2Fee.add(getElementValue(AccommTaxMinValue));
		accom2Fee.add(getElementValue(AccommTaxMaxValue));
		accom2Fee.add(getElementValue(AccommFeeMinValue));
		accom2Fee.add(getElementValue(AccommFeeMaxValue));
		accom2Fee.add(getElementValue(AccommParkingFeeMinValue));
		accom2Fee.add(getElementValue(AccommParkingFeeMaxValue));

		if (verifyObjectEnabled(relatedSection)) {
			tcConfig.updateTestReporter("DestinationsPage", "destinationRelated", Status.PASS,
					"Successfully navigated to Destination related tab section");
		} else {
			tcConfig.updateTestReporter("DestinationsPage", "destinationRelated", Status.FAIL,
					"Error while navigating to Destination related tab section");
		}
	}

}
