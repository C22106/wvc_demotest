package tgs.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import automation.utilities.ReadConfigFile;

public class LoginPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(LoginPage.class);
	ReadConfigFile config = new ReadConfigFile();

	public LoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	private By username = By.xpath("//input[@id='username']");
	private By password = By.xpath("//input[@id='password']");
	private By loginButton = By.id("Login");
	private By logOut = By.xpath("//a[text()='Log Out']");
	private By viewProfile = By.xpath("//img[@title='User']");
	private By homeIcon = By.xpath("//li[@data-menu-item-id='0']");
	private By profileName = By.xpath("//span[@class=' profileName']");
	private By logOutMarketer = By.xpath("//a[@title='Logout']");
	private By imageLogOut = By.xpath("//span[@class='uiImage']//img[@alt='User']");

	/* <Offers Scritping Xpath> */

	protected By btnAppLauncher = By.xpath("//div[@class='slds-icon-waffle']");
	protected By tabOffers = By.xpath("(//p[@class='slds-truncate' and contains(.,'Offers')])[1]");
	protected By txtOffers = By.xpath("//span[@title='Offers']");
	protected By txtSearchBox = By.xpath("//input[@class='slds-input']");

	private void setUserName(String strUserName) throws Exception {
		waitUntilElementVisibleBy(driver, username, 10);
		driver.findElement(username).sendKeys(strUserName);
		tcConfig.updateTestReporter("SF_LoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	public void launchApp() {

		String url = config.get("SF_URL").trim();
		driver.navigate().to(url);
	
		driver.manage().window().maximize();

	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	private void setPassword(String strPassword) throws Exception {

		// look for th prob;
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(password).sendKeys(dString);
		tcConfig.updateTestReporter("SF_LoginPage", "setPassword", Status.PASS, "Entered password for login");
	}
	
	/*
	 * Handling Alert PopUp
	 * Created by Shubhednra Date:March 2021
	 */
	
	public void allowNotificationPopUp() {
		


	}

	private By testUserIHMarketer = By.xpath("//span[contains(text(),'Test user Inhousemarketer')]");
	private By testUserLogOut = By.xpath("//a[@title='Logout']");
	private By btnNewLogin = By.xpath("//input[@name='Login']");

	public void testUserIHMarketerLogout() throws Exception {
		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, testUserIHMarketer, 120);
			clickElementBy(testUserIHMarketer);
			// driver.findElement(testUserIHMarketer).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(testUserLogOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnNewLogin, 120);
			if (verifyObjectDisplayed(btnNewLogin)) {
				tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.PASS,
						"Log out Successful");
			} else {
				tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.FAIL,
						"Log Off unsuccessful");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "testUserIHMarketerLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}

	}

	public void loginToSalesForce() throws Exception {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("SF_UserID");
		String password = tcConfig.getConfig().get("SF_Password");

		setUserName(userid);
		setPassword(password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");

	}

	public void loginToSalesForceAdmin() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String userid = tcConfig.getConfig().get("SF_UserID");
		String password = tcConfig.getConfig().get("SF_Password");

		setUserName(userid);
		setPassword(password);

		// waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.findElement(loginButton).click();
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS, "Log in button clicked");
	}

	protected By lnkViewAll = By.xpath("//div[@class='slds-size_medium']//button");
	@FindBy(xpath = "//button[@class='slds-button']")
	WebElement menuIcon;
	protected By homeTab = By.xpath("//span[contains(.,'Home')]");
	protected By txtSearchBy = By.xpath("//input[@class='slds-input']");
	@FindBy(xpath = "//p[@class='slds-truncate' and contains(.,'Offers')]")
	protected WebElement Offers;
	protected By journeydesktop = By.xpath("//span[@title = 'Journey Desktop']");
	protected By txtVouchers = By.xpath("//span[@title='Vouchers']");
	protected By imarkter = By.xpath(
			"//img[@src='/wvo/resource/1561942897000/Wyn_Custom/custom/img/wynd_dst_icon_sq_blk_rgb_150ppi.png']");

	protected By teamName = By.xpath("//input[@placeholder= 'Enter Team Name']");
	protected By salesTeamValue = By
			.xpath("//div/label[text()='Sales Team']/following::li[@class='slds-listbox__item eachItem']");
	protected By saleschannnelName = By.xpath("//input[@placeholder='Enter Sales Channel Name']");
	protected By salesChannelValue = By
			.xpath("//div/label[text()='Sales Channel']/following::li[@class='slds-listbox__item eachItem']");
	protected By nextButton = By.xpath("//button[@title = 'Next']");
	protected By customerLookUpTeam = By.xpath("//div[@class = 'cCustomLookupTeam']");

	// new method from Abhijeet

	public void loginToSalesForceDiffUsers() throws Exception {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String userid = testData.get("SF_UserID");
		String password = testData.get("SF_Password");

		setUserName(userid);
		setPassword(password);
		try {
			driver.findElement(loginButton).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		//	checkLoadingSpinner();
			try {
			//	waitUntilElementVisibleBy(driver, customerLookUpTeam, 30);
				if (verifyObjectDisplayed(customerLookUpTeam)) {
					clickElementJSWithWait(teamName);
					driver.findElement(teamName).sendKeys(testData.get("TeamName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("TeamName") + "')]/..")).click();
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(saleschannnelName);
					driver.findElement(saleschannnelName).sendKeys(testData.get("SalesChannelName"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//span[contains(.,'" + testData.get("SalesChannelName") + "')]/.."))
							.click();
					// waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
							"Successfully selected Team and SalesChannel");
					clickElementJSWithWait(nextButton);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			} catch (Exception e) {
				log.info(e);
			}
		} catch (Exception e) {
			log.info(e);
		}
	//	checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS,
				"Next button in the Team, SalesChannel window is clicked");
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		/*
		 * if (!verifyObjectDisplayed(journeydesktop) &&
		 * !verifyObjectDisplayed(imarkter))
		 * 
		 * { // waitUntilElementVisibleWb(driver, menuIcon, 120);
		 * clickElementJSWithWait(menuIcon);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * waitUntilElementVisibleBy(driver, lnkViewAll, 120);
		 * clickElementJS(lnkViewAll);
		 * waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * List<WebElement> addList = driver.findElements(txtSearchBy);
		 * addList.get(addList.size() - 1).click(); addList.get(addList.size() -
		 * 1).sendKeys("Journey Desktop");
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * driver.findElement(By.xpath("//mark[text()='Journey Desktop']"
		 * )).click(); }
		 */
		/*if (verifyObjectDisplayed(txtOffers)) {
			waitUntilElementVisibleBy(driver, txtOffers, "ExplicitLowWait");
			Assert.assertTrue(verifyObjectDisplayed(txtOffers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"Navigated successfully to Offers Application Page");
		}

		else if (verifyObjectDisplayed(journeydesktop)) {

			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage No need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else if (verifyObjectDisplayed(imarkter)) {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
					"User is navigated to Homepage No need to switch explicitly");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL, "Failed to Login");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}*/

	}

	public void navigateToOffers() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(journeydesktop)) {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated to Journey Desktop page");
			waitUntilElementVisibleBy(driver, homeTab, 120);
			clickElementJSWithWait(btnAppLauncher);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, tabOffers, 120);
			clickElementJSWithWait(tabOffers);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(verifyObjectDisplayed(txtOffers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated explicitly to Offers Application Page");
		} else if (verifyObjectDisplayed(txtOffers)) {
			waitUntilElementVisibleBy(driver, txtOffers, "ExplicitMedWait");
			Assert.assertTrue(verifyObjectDisplayed(txtOffers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"Navigated successfully to Offers Application Page");
		}	else if (verifyObjectDisplayed(txtVouchers)) {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated to Vouchers page");
			waitUntilElementVisibleBy(driver, txtVouchers, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(btnAppLauncher);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, tabOffers, 120);
			clickElementJSWithWait(tabOffers);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(verifyObjectDisplayed(txtOffers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated explicitly to Offers Application Page");
		} else {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.FAIL,
					"Not navigated to Offers Application");
		}
	}

	public void navigateToOffersDiffLogin() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(journeydesktop)) {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated to Journey Desktop page");
			waitUntilElementVisibleBy(driver, homeTab, 120);
			clickElementJSWithWait(btnAppLauncher);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtSearchBox, 120);
			String strOffer = testData.get("Menu3");
			fieldDataEnter_modified(txtSearchBox, strOffer);
			clickElementJSWithWait(tabOffers);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(verifyObjectDisplayed(txtOffers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated explicitly to Offers Application Page");
		} else if (verifyObjectDisplayed(txtVouchers)) {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated to Vouchers page");
			waitUntilElementVisibleBy(driver, txtVouchers, 120);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(btnAppLauncher);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, tabOffers, 120);
			clickElementJSWithWait(tabOffers);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(verifyObjectDisplayed(txtOffers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"User is navigated explicitly to Offers Application Page");
		} else if (verifyObjectDisplayed(txtOffers)) {
			waitUntilElementVisibleBy(driver, txtOffers, "ExplicitMedWait");
			Assert.assertTrue(verifyObjectDisplayed(txtOffers));
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.PASS,
					"Navigated successfully to Offers Application Page");
		} else {
			tcConfig.updateTestReporter("SF_LoginPage", "navigateToOffers", Status.FAIL,
					"Not navigated to Offers Application");
		}
	}

	/*
	 * Method-To logout form Sales force Designed By-JYoti
	 */
	public void salesForceLogout() throws Exception {

		try {

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			WebElement viewpro = driver.findElement(viewProfile);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", viewpro);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, logOut, 120);
			driver.findElement(logOut).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, username, 120);
			if (verifyObjectDisplayed(username)) {
				tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.PASS, "Log out Successful");
			} else {
				tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL, "Log Off unsuccessful");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("SF_LoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	public By logout_inhouse = By.xpath("//li[@class='logOut uiMenuItem']");

	@FindBy(xpath = "//img[@title='In-House Admin 2']")
	WebElement profileImg;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrw;

	public void inHouseAdmin2Logout() throws Exception {
		try {

			mouseoverAndClick(profileImg);

			waitUntilElementVisibleBy(driver, logout_inhouse, 120);
			driver.findElement(logout_inhouse).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("SalesForceLoginPage", "salesForceLogout", Status.FAIL,
					"Log Off unsuccessful " + e.getMessage());
		}
	}

	@FindBy(xpath = "//img[@title='In-House Marketer 3']")
	WebElement profileImgMarAgent;
	@FindBy(xpath = "//span[@class='triggerDownArrow down-arrow']")
	WebElement triggerDwnArrwMarAgent;
	@FindBy(xpath = "//div//div//li//a[@title='Logout']")
	WebElement logOutMarketingAgent3;

	public void loginOfferValidationUser() throws Exception {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String userid = testData.get("SF_Validation_UserID");
		String password = testData.get("SF_Validation_Password");
		setUserName(userid);
		setPassword(password);
		clickElementBy(loginButton);
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginOfferValidationUser", Status.PASS,
				"Logged in as Offer Validation user: " + userid);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, btnAppLauncher, "ExplicitWaitLongerTime");
		checkLoadingSpinner();
		try {
			waitUntilElementVisibleBy(driver, customerLookUpTeam, 60);
			if (verifyObjectDisplayed(customerLookUpTeam)) {
				clickElementJSWithWait(teamName);
				driver.findElement(teamName).sendKeys(testData.get("TeamName_ValUser"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("//span[contains(.,'" + testData.get("TeamName_ValUser") + "')]/.."))
						.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(saleschannnelName);
				driver.findElement(saleschannnelName).sendKeys(testData.get("SalesChannelName_ValUser"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(
						By.xpath("//span[contains(.,'" + testData.get("SalesChannelName_ValUser") + "')]/..")).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("SF_LoginPage", "loginOfferValidationUser", Status.PASS,
						"Successfully selected Team and SalesChannel");
				clickElementJSWithWait(nextButton);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		} catch (Exception e) {
			log.info(e);
		}
		checkLoadingSpinner();
		tcConfig.updateTestReporter("SF_LoginPage", "loginToSalesForce", Status.PASS,
				"Next button in the Team, SalesChannel window is clicked");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

}