package tgs.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class ExtensionsPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(ExtensionsPage.class);

	public ExtensionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/* <Added By Jyoti> */
	protected By txtExtension = By
			.xpath("//span[contains(.,'Extensions') and @data-aura-class='uiOutputText forceBreadCrumbItem']");
	protected By btnNew = By.xpath("//div[@title='New']");
	protected By txtExtensionName = By.xpath("//span[contains(.,'Extensions Name')]/../..//input[@name='Name']");
	protected By drpExtnLength = By.xpath(
			"//span[contains(.,'Extension Length')]/following::div//input[@class='slds-input slds-combobox__input']");
	protected By drpValue = By.xpath(
			"((//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown')])[2]//span[@class='slds-media__body'])[2]");
	protected By txtMinExtnPrice = By
			.xpath("//span[contains(.,'Minimum Extension Price')]/../..//input[@name='Minimum_Extension_Price__c']");
	protected By txtMaxExtnPrice = By
			.xpath("//span[contains(.,'Maximum Extension Price')]/../..//input[@name='Maximum_Extension_Price__c']");
	protected By txtEffStartDate = By
			.xpath("(//span[contains(.,'Effective Start Date')]/../..//input[@name='Effective_Start_Date__c'])");
	protected By txtEndDate = By
			.xpath("//label[contains(.,'Effective End Date')]/following::input[@name='Effective_End_Date__c']");
	protected By btnSave = By.xpath(" (//button[@title='Save'])[1]");
	protected By btnSaveOnCreateExt = By.xpath("(//button[@name='SaveEdit'])[1]");
	protected By txtNewExtn = By.xpath("//h2[contains(.,'New Extensions')]");
	protected By lnkToday = By.xpath("//button[contains(text(),'Today')]");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtGlobalSearch = By.xpath("//input[@placeholder='Search Extensions and more...']");
	protected By imgClick = By.xpath("//img[@class='icon ' and contains(@src,'custom/custom43_120.png')]");
	protected By tabDetails = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[3]");
	protected By btnEdit = By.xpath("(//button[@name='Edit'])[3]");
	protected By txtEffEndDt = By.xpath("(//span[contains(.,'Effective End Date')]/../..//input[@class=' input'])[1]");
	protected By txtEditExtnForm = By
			.xpath("//h2[contains(@class,'inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p')]");
	protected By txtNewExtnData = By.xpath("(//div[@class='slds-media__body']//lightning-formatted-text)[1]");
	protected By btnDelete = By.xpath("//button[@name='Delete']");
	protected By txtDeleteExtension = By.xpath("//h2[@class='title slds-text-heading--medium slds-hyphenate']");
	protected By btnClickDelete = By.xpath("//button[@title='Delete']");
	protected By deleteMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By extnPage = By.xpath("(//div[@class='slds-media__body']//lightning-formatted-text)[3]");
	protected By delBtn = By.xpath("(//button[@name='Delete'])[2]");

	/*
	 * Description: Click on New button on Extension Page Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void clickNewbtn() {
		waitUntilElementVisibleBy(driver, txtExtension, "ExplicitMedWait");
		verifyPageDisplayed(txtExtension, "Page Title", "OfferFeesPage", "clickNewbtn");
		clickElementJSWithWait(btnNew);
		verifyPageDisplayed(txtNewExtn, "Page Title", "OfferFeesPage", "clickNewbtn");
	}

	/*
	 * Description: Create a new Extension Record Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void createExtensionRecord() {
		waitUntilElementVisibleBy(driver, txtNewExtn, "ExplicitMedWait");
		String strExtnNm = getRandomStringText(4);
		String strData = "AutomationDataExtn_" + strExtnNm;
		sendKeysBy(txtExtensionName, strData);
		clickElementBy(drpExtnLength);
		clickElementBy(drpValue);
		waitUntilElementVisibleBy(driver, txtMinExtnPrice, "ExplicitLowWait");
		String strMinPrice = getRandomString(2);
		sendKeysBy(txtMinExtnPrice, strMinPrice);
		waitUntilElementVisibleBy(driver, txtMaxExtnPrice, "ExplicitLowWait");
		String strMaxPrice = getRandomString(3);
		sendKeysBy(txtMaxExtnPrice, strMaxPrice);
		clickElementBy(txtEffStartDate);
		selectSystemDate(lnkToday);
		clickElementJSWithWait(txtEndDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveOnCreateExt);
		verifyPageDisplayed(sucessMsg, "Page Title", "OfferFeesPage", "clickNewbtn");
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("ExtensionsPage",
		 * "createExtensionRecord", Status.PASS, "Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
	}

	/*
	 * Description: Global Search for an existing Extension Record Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void searchOfferExtension(String extension) throws Exception {
		verifyPageDisplayed(txtExtension, "Page Title", "ExtensionsPage", "searchOfferExtension");
		waitUntilElementVisibleBy(driver, txtGlobalSearch, "ExplicitMedWait");
		fieldDataEnter_modified(txtGlobalSearch, extension);
		waitUntilElementVisibleBy(driver, By.xpath("//mark[contains(.,'" + extension + "')]"), 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(imgClick);
		waitUntilElementVisibleBy(driver, tabDetails, 120);
		Assert.assertTrue(verifyObjectDisplayed(tabDetails));
		tcConfig.updateTestReporter("ExtensionsPage", "searchOfferExtension", Status.PASS,
				"User navigated to Offer record details page Successfully");
	}

	/*
	 * Description: Delete an existing Extension Record Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void validateDeleteRecord() {
		waitUntilElementVisibleBy(driver, extnPage, "ExplicitMedWait");
		verifyPageDisplayed(extnPage, "Page Title", "ExtensionsPage", "validateDeleteRecord");
		clickElementJSWithWait(delBtn);
		verifyPageDisplayed(txtDeleteExtension, "Page Title", "ExtensionsPage", "deleteExtension");
		clickElementJSWithWait(btnClickDelete);
		waitUntilElementVisibleBy(driver, deleteMsg, 120);
		// Assert.assertTrue(verifyObjectDisplayed(deleteMsg));
		tcConfig.updateTestReporter("ExtensionsPage", "deleteExtension", Status.PASS,
				"Newly created Extension record is deleted Successfully");
	}

	/*
	 * Description: Delete the newly created Extension Record Date:
	 * December/2020 Author: Jyoti Changes By: NA
	 */
	public void deleteExtension() {
		waitUntilElementVisibleBy(driver, txtNewExtnData, "ExplicitMedWait");
		verifyPageDisplayed(txtNewExtnData, "Page Title", "ExtensionsPage", "deleteExtension");
		clickElementJSWithWait(btnDelete);
		verifyPageDisplayed(txtDeleteExtension, "Page Title", "ExtensionsPage", "deleteExtension");
		clickElementJSWithWait(btnClickDelete);
		waitUntilElementVisibleBy(driver, deleteMsg, 120);
		// Assert.assertTrue(verifyObjectDisplayed(deleteMsg));
		tcConfig.updateTestReporter("ExtensionsPage", "deleteExtension", Status.PASS,
				"Newly created Extension record is deleted Successfully");
	}
	
	/*
	 * Description: Create a new Extension Record Date: Feb/2020 Author: Shubhendra
	 * Changes By: NA
	 */
	public void createExtensionRecordUpdated() {
		waitUntilElementVisibleBy(driver, txtNewExtn, "ExplicitMedWait");
		String strExtnNm = getRandomStringText(4);
		String strData = "AutomationDataExtn_" + strExtnNm;
		sendKeysBy(txtExtensionName, strData);
		clickElementBy(drpExtnLength);
		clickElementBy(drpValue);
		waitUntilElementVisibleBy(driver, txtMinExtnPrice, "ExplicitLowWait");
		String strMinPrice = getRandomString(2);
		sendKeysBy(txtMinExtnPrice, strMinPrice);
		waitUntilElementVisibleBy(driver, txtMaxExtnPrice, "ExplicitLowWait");
		String strMaxPrice = getRandomString(3);
		sendKeysBy(txtMaxExtnPrice, strMaxPrice);
		clickElementBy(txtEffStartDate);
		selectSystemDate(lnkToday);
		clickElementJSWithWait(txtEndDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveOnCreateExt);
		verifyPageDisplayed(sucessMsg, "Page Title", "OfferFeesPage", "clickNewbtn");
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("ExtensionsPage",
		 * "createExtensionRecord", Status.PASS, "Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
	}
	

	
	public void searchOfferExtensionNew(String extension) throws Exception {
		verifyPageDisplayed(txtExtension, "Page Title", "ExtensionsPage", "searchOfferExtension");
		waitUntilElementVisibleBy(driver, txtGlobalSearch, "ExplicitMedWait");
		fieldDataEnter_modified(txtGlobalSearch, extension);
		waitUntilElementVisibleBy(driver, By.xpath("//mark[contains(.,'" + extension + "')]"), 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(imgClick);
		waitUntilElementVisibleBy(driver, tabDetails, 120);
		Assert.assertTrue(verifyObjectDisplayed(tabDetails));
		tcConfig.updateTestReporter("ExtensionsPage", "searchOfferExtension", Status.PASS,
				"User navigated to Offer record details page Successfully");
	}

}