package tgs.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class TGSBasePage extends FunctionalComponents {

	public TGSBasePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	protected By loadingSpinner = By.xpath("//div[@class='loader__tick']");

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change
	 * By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	/*
	 * *************************Method: checkLoadingSpinner ***
	 * *************Description: Salepoint Loading Spinner**
	 */
	public void checkLoadingSpinner() {
		waitUntilElementInVisible(driver, loadingSpinner, 60);
	}

	/*
	 * Method: verifyPageDisplayed Description: Validate the Page title
	 * displayed
	 */
	public void verifyPageDisplayed(By by, String type, String pageName, String functionName) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String pageTitle = getObject(by).getText();

		if (getObject(by).isDisplayed()) {
			tcConfig.updateTestReporter(pageName, functionName, Status.PASS,
					"Navigated to Page with " + type + " : " + pageTitle);
		} else {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL, "Unable to navigate to " + pageTitle);
		}
	}

	public String getRandomStringText(int text) {
		String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < text) { // length of the random string.
			int index = (int) (rnd.nextFloat() * str.length());
			salt.append(str.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
		// System.out.println(saltStr);
		// testData.put("", "");
	}

	/*
	 * *************************Method: deviceInfo *** *************Description
	 * - Fetch System date from Calender*
	 */

	public void selectSystemDate(By by) {
		WebElement wbEndDate = driver.findElement(by);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions actEndDate = new Actions(driver);
		actEndDate.moveToElement(wbEndDate).click().build().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void fieldDataEnter_modified(By by, String strData) {

		try {
			if (verifyObjectDisplayed(by)) {
				clickElementBy(by);
				waitForSometime("1");
				driver.findElement(by).clear();
				driver.findElement(by).sendKeys(strData);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("", "", Status.FAIL,
						"Editbox not displayed where " + "'" + strData + "'" + " can be entered");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("", "", Status.FAIL,
					"Editbox not Found with the object property where " + "'" + strData + "'" + " can be entered");
			e.printStackTrace();
		}

	}

	/*
	 * Method: verifyPageTitleDisplayed Description: Validate the Page title
	 * displayed
	 */
	public void verifyPageTitleDisplayed(By by, String message, String pageName, String functionName) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String pageTitle = getObject(by).getText();

		try {
			if (getObject(by).isDisplayed()) {
				tcConfig.updateTestReporter(pageName, functionName, Status.PASS, message + " : " + pageTitle);
			} else {
				tcConfig.updateTestReporter(pageName, functionName, Status.PASS, pageTitle + "is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL, "Unable to navigate to " + pageTitle);
		}

	}

	/*
	 * Method: verifyTitleDisplayed Description: Validate the Page title
	 * displayed
	 */
	public boolean verifyTitleDisplayed(By by, String pageName, String functionName) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String pageTitle = getObject(by).getText();
		if (getObject(by).isDisplayed()) {
			tcConfig.updateTestReporter(pageName, functionName, Status.PASS, "Navigated to Page : " + pageTitle);
			return true;
		} else {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL, "Unable to navigate to " + pageTitle);
			return false;
		}
	}

	/*
	 * *************************Method: fieldDataEnter ***
	 * *************Description: Enter Data in field after clearing**
	 */

	public void fieldEnterData(By by, String strData) {

		try {
			clickElementBy(by);
			clearElementBy(by);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			sendKeysBy(by, strData);
		} catch (Exception e) {
			tcConfig.updateTestReporter("Send Data to Object", "fieldDataEnter", Status.FAIL,
					"Could Not Enter the data in the field");
		}

	}

	/*
	 * Method: verifySuccessDisplayed Description: Validate the success message
	 * is displayed or not
	 */
	public void verifySuccessDisplayed(By by, String pageName, String functionName) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String createMessage = getObject(by).getText();

		if (getObject(by).isDisplayed()) {
			tcConfig.updateTestReporter(pageName, functionName, Status.PASS,
					functionName + " completed Successfully: " + createMessage);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL,
					"Success message not displayed: Unable to " + functionName);
		}
	}

	/*
	 * Method: WebObject_Click Description: Choose dates from Calendar Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void selectElement(String pageName, By by, String objName) {
		try {
			if (verifyObjectDisplayed(by)) {
				getObject(by).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter(pageName, "selectElement", Status.PASS, objName + " is clicked");
			} else {
				tcConfig.updateTestReporter(pageName, "selectElement", Status.FAIL, objName + " is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter(pageName, "selectElement", Status.FAIL, objName + " is not Clicked");
		}

	}

	/*
	 * Method: chooseCalDate Description: Choose dates from Calendar Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void chooseCalDate(By eff, By month, By date) {
		clickElementJSWithWait(eff);
		clickElementJSWithWait(month);
		clickElementBy(date);
	}

	/*
	 * Method: verifyProgressStatus Description: Validate the Poffer progress
	 * status displayed
	 */
	public void verifyProgressStatus(By by, String pageName, String functionName) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String barTitle = getObject(by).getText();
		if (getObject(by).isDisplayed()) {
			tcConfig.updateTestReporter(pageName, functionName, Status.PASS,
					"Progress bar " + barTitle + " highlighted successfully");
		} else {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL, "Unable to navigate to " + barTitle);
		}
	}

	/*
	 * Method: assertExpectedValue Description: Validate the success message
	 * matches the expected value
	 */
	public void assertExpectedValue(By by, Object expectedValue, String message, String pageName, String functionName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			String actualValue = getObject(by).getText();
			Assert.assertEquals(actualValue, expectedValue);
			tcConfig.updateTestReporter(pageName, functionName, Status.PASS, message + actualValue);
		} catch (Exception e) {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL, "Issue while comparing expected values");
		}
	}

	/*
	 * Method: verifyElementSelected Description: Validate whether element is
	 * selected displayed
	 */
	public void verifyElementSelected(By by, String pageName, String ObjName) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		try {
			if (getObject(by).isDisplayed()) {
				tcConfig.updateTestReporter(pageName, "verifyElementSelected", Status.PASS,
						ObjName + " Element selected successfully");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter(pageName, "verifyElementSelected", Status.FAIL,
					"Unable to select Element " + by);
		}

	}

	public void scrollDownJavaScriptExecutor(By by) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = getObject(by);
		((JavascriptExecutor) driver).executeScript("scroll(0,250);");

	}

	/*
	 * Method: verifySuccessDisplayed Description: Validate the success message
	 * is displayed or not
	 */
	public void verifySave(String pageName, String functionName, By saveButton, By successMessageTitle,
			By saveSnagError, By cancelButton) {

		try {
			clickElementBy(saveButton);
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(successMessageTitle));
			String createMessage = getObject(successMessageTitle).getText();

			if (getObject(successMessageTitle).isDisplayed()) {
				tcConfig.updateTestReporter(pageName, functionName, Status.PASS, "Save successful " + createMessage);
				// waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL, "Failed due to : " + e.getMessage());
			clickElementBy(cancelButton);
		}
	}

	/*
	 * *************************Method: waitUntilElementVisibleBy ***
	 * *************Description: Wait Until Element Visible**
	 */

	public void waitUntilElementVisibleBy(WebDriver driver, WebElement wb, String timeOutInSeconds) {
		boolean flag = false;
		String strValue = tcConfig.getConfig().get(timeOutInSeconds);

		Long timeOut = Long.parseLong(strValue);
		for (long i = 0; i < timeOut / 10; i++) {
			try {

				if (wb.isDisplayed()) {
					flag = true;
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					break;
				}

			} catch (Exception e) {
				log.info("Could Not Get the Element ");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

		}
		if (!flag) {

			try {
				driver.switchTo().alert();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {
				log.info("Element Found");
			}
		}

	}

	/*
	 * Method: validateMessage Description: CLick the link and verify the
	 * success status message displayed
	 */
	public void validateMessage(String pageName, String functionName, By by, String message) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String pageTitle = getObject(by).getText();

		try {
			Assert.assertTrue(verifyObjectDisplayed(by));
			tcConfig.updateTestReporter(pageName, functionName, Status.PASS, message + " : " + pageTitle);

		} catch (Exception e) {
			tcConfig.updateTestReporter(pageName, functionName, Status.FAIL,
					"Unable to navigate to " + pageTitle + e.getMessage());
		}

	}

	public void verifyAddedOfferAssignments(String recordDisplayed, String recordType, String recordAdded) {

		Assert.assertTrue(recordAdded.equalsIgnoreCase(recordDisplayed));
		tcConfig.updateTestReporter("OffersPage", "verifyAddedOfferAssignments", Status.PASS,
				"Newly added " + recordType + " displayed: " + recordAdded);
	}

	/*
	 * Method: navigateToOfferAssignment Description: Navigate to the Offer
	 * assignment after saving the record Date:December/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void navigateToOfferAssignment(By tabName, By linkName, By loadingSpinner) {
		clickElementBy(tabName);
		scrollDownForElementJSBy(linkName);
		clickElementBy(linkName);
		waitUntilElementInVisible(driver, loadingSpinner, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: verifySavedOfferAssignments Description: Verify the saved offer
	 * record Date:December/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void verifySavedOfferAssignments(By addedRecord, String recordType, String datasheetColumnname) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(addedRecord));
		String expRecordName = testData.get(datasheetColumnname);
		String actualRecordName = getElementText(addedRecord);
		Assert.assertTrue(actualRecordName.equalsIgnoreCase(expRecordName));
		// Assert.assertEquals(actualRecordName, expRecordName);
		tcConfig.updateTestReporter("OffersPage", "addSalesChannelToOffer", Status.PASS,
				"Newly added " + recordType + "displayed: " + actualRecordName);
	}

	/*
	 * *************************Method: getList *** *************Description Get
	 * Click on last element List with values**
	 */

	public void getListElement(By by) {
		List<WebElement> allElement = driver.findElements(by);
		int count = allElement.size();
		allElement.get(count - 1).click();
	}
	
	
	public void clickElementJSWithWait(By by) {
		try {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("var elem=arguments[0];setTimeout(function(){elem.click();},120)", getObject(by));
		} catch (Exception e) {
		tcConfig.updateTestReporter("ClickElement", "clickElementJSWithWait", Status.FAIL,
		"Could Not Click the object");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	
	public void clickElementBy(By by) {
		try {
		waitForSometime(tcConfig.getConfig().get("LowWait"));	
		waitUntilElementVisibleBy(driver, by, "ExplicitWaitLongerTime");
		getObject(by).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
		log.error("Could Not Click The Element");
		}
		}
	public void selectCheckBox(By by) {
        try {
        waitForSometime(tcConfig.getConfig().get("LowWait")); 
        if (getObject(by).isEnabled()) {
              getObject(by).click();  
        }           
        } catch (Exception e) {
        log.error("Could Not Click The Element");
        }
        }
	
	/*
	 * *************************Method: verifyObjectEnabled ***
	 * *************Description: Verify Object Selected**
	 */

	public boolean verifyObjectSelected(By by) {

		try {
			if (getObject(by).isSelected()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}
}


