
package tgs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TermsAndConditionsPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(TermsAndConditionsPage.class);

	public TermsAndConditionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	/* <Xpath by Krishnaraj> */
	/* Terms & Conditions Main Page elements */
	protected By termAndConditionsTab = By.xpath("//span[@class='slds-truncate' and text()='Terms & Conditions']");
	protected By newButton = By.xpath("//div[@title='New']");
	protected By recordSelectTitle = By.xpath("//h2[text()='New Terms & Conditions']");
	protected By tcSelectRadioButton = By.xpath("//span[text()='Offer Terms & Conditions']");
	protected By nextButton = By.xpath("//span[text()='Next']");

	/* Terms & Conditions form elements */
	protected By tcFormTitle = By.xpath("//h2[text()='New Terms & Conditions: Offer Terms & Conditions']");
	protected By tcLabelTextBox = By.xpath("//input[@name='Terms_Conditions_Label__c']");
	protected By brandField = By.xpath("//label[text()='Brand']//following::input[1]");
	protected By brandDropdwn = By
			.xpath("//lightning-base-combobox-item//span[@title='" + testData.get("Brand") + "']");
	protected By guestField = By.xpath("//label[text()='Guest Type']//following::input[1]");
	protected By guestOption = By.xpath("//lightning-base-combobox-item//span[@title='" + testData.get("Guest") + "']");
	protected By salesChannelLookup = By
			.xpath("//label[text()='Sales Channel']//following::input[@placeholder='Search Sales Channels...']");
	protected By destinationLookup = By
			.xpath("//label[text()='Destination']//following::input[@placeholder='Search Destinations...']");
	protected By salesChannelOption1 = By.xpath("//ul[@class='lookup__list  visible']/li[1]");
	protected By stateListBox = By.xpath("//li/div//span[@class='slds-truncate' and text()='" + testData.get("State") + "']");
	protected By stateAddToButton = By
			.xpath("//div[text()='State of Residence']/following::button[1][@title='Move selection to Chosen']");
	protected By effStartDatePicker = By.xpath("//label[text()='Effective Start Date']//following::input[1]");
	protected By effEndDatePicker = By.xpath("//label[text()='Effective End Date']//following::input[1]");
	protected By dateLink = By.xpath("//button[text()='Today']");
	protected By StartCurrentDate = By.xpath("//td[@class='slds-is-today slds-is-selected uiDayInMonthCell']");
	protected By legalTermsCondTextBox = By
			.xpath("//label[text()='Legal Terms and Conditions']//following::textarea[1]");
	protected By businessTermsCondTextBox = By
			.xpath("//label[text()='Business Terms and Conditions']//following::textarea[1]");
	protected By saveButton = By.xpath("//button[text()='Save']");
	protected By successMessageTitle = By
			.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By comboListItem1 = By
			.xpath("(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'])[2]");
	protected By selectDestination = By.xpath("//lightning-base-combobox-formatted-text[@title='" + testData.get("Destination") + "']");
	protected By saveSnagError = By.xpath("//div[@class='fieldLevelErrors']");
	protected By cancelButton = By.xpath("//button[@title='Cancel']");
	protected By scrollModal = By.xpath("//div[@class='modal-container slds-modal__container']");
	
	
	/*
	 * Method: chooseTermsAndCondType Description: Select the Terms and
	 * Conditions record type Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void chooseTermsAndCondType() {
		clickElementJSWithWait(termAndConditionsTab);
		clickElementBy(newButton);
		verifyPageTitleDisplayed(recordSelectTitle, "Navigated to Select record type page with Title",
				"TermsAndConditionsPage", "chooseTermsAndCondType");
	}

	/*
	 * Method: chooseOfferTermsAndCond Description: Choose Regular Offer Val
	 * Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseOfferTermsAndCond() {
		clickElementBy(tcSelectRadioButton);
		clickElementBy(nextButton);
		verifyPageTitleDisplayed(tcFormTitle, "Navigated to Offer Terms and conditions page with Title",
				"TermsAndConditionsPage", "chooseOfferTermsAndCond");
	}

	/*
	 * Method: enterTermsAndCondData Description: Enter Terms And Conditions
	 * Data Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	
	String legalTnc, businessTnC;
	
	public void enterTermsAndCondData() {
		if (verifyObjectDisplayed(tcFormTitle)) {
			fieldEnterData(tcLabelTextBox, "TandC " + getRandomStringText(7));
			clickElementJSWithWait(brandField);
			clickElementBy(brandDropdwn);
			clickElementJSWithWait(guestField);
			clickElementBy(guestOption);
			fieldEnterData(salesChannelLookup, testData.get("SalesChannel"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(comboListItem1);
			fieldEnterData(destinationLookup, testData.get("Destination"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(selectDestination);
			tcConfig.updateTestReporter("TermsAndConditionsPage", "enterTermsAndCondData", Status.PASS,
					"Brand, Guest Type and Sales Channel details entered");
			scrollDownJavaScriptExecutor(scrollModal);
			clickElementJSWithWait(stateListBox);
			clickElementJSWithWait(stateAddToButton);
			clickElementBy(effStartDatePicker);
			clickElementBy(dateLink);
			clickElementBy(effEndDatePicker);
			clickElementBy(dateLink);
			legalTnc = "Testing Legal TandC";
			businessTnC = "Testing General Or business TandC";	
			fieldEnterData(legalTermsCondTextBox, legalTnc);
			fieldEnterData(businessTermsCondTextBox, businessTnC );
			tcConfig.updateTestReporter("TermsAndConditionsPage", "enterTermsAndCondData", Status.PASS,
					"Able to enter data in the Offer Terms and conditions page");
		} else {
			tcConfig.updateTestReporter("TermsAndConditionsPage", "enterOfferData", Status.FAIL,
					"Unable to enter data in the Offer Terms and conditions page");
		}
		verifySave("TermsAndConditionsPage", "saveOffer", saveButton, successMessageTitle, saveSnagError, cancelButton);
	}

	/*
	 * Method: saveTermsAndConditions Description: Save Terms And Conditions and
	 * verify the success message Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void saveTermsAndConditions() {
		if (verifyObjectDisplayed(saveButton)) {
			clickElementBy(saveButton);
			verifyPageTitleDisplayed(successMessageTitle, "Offer Terms and conditions created Successfully",
					"TermsAndConditionsPage", "saveTermsAndConditions");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("TermsAndConditionsPage", "saveOffer", Status.FAIL,
					"Unable to create Offer Terms and conditions");
		}

	}

}
