package tgs.pages;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;


import automation.core.TestConfig;

public class PaymentRecordsPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(OfferTransactionsPage.class);

	public PaymentRecordsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	
	
	protected  By pgPaymentRecord = By.xpath("//span[contains(.,'Payment Records') and contains(@class,'slds-var-p-right_x-small')]");
	protected By txtSearchPayment = By.xpath("//input[@title='Search Payment Records and more']");
	protected By imgPaymentClick = By.xpath("(//img[@class='icon ' and contains(@src,'custom/custom40_120.png')])[1]");
	protected By pagePaymentResult = By.xpath("//a[contains(@class,'slds-page-header__title slds-text-color--default slds')]");
	protected  By paymentPage = By.xpath("//div[@class='entityNameTitle slds-line-height--reset']");
	protected By tblPayment = By.xpath("(//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds')]//a[starts-with(.,'P-')])[1]");
	
	
	/*
	 * Description: To search using Payment ID : Last 2 digit of Card, Amount and Date
	 *  Date: February/2021 Author: Jyoti Changes By: NA
	 */
	public void searchByPaymentRecords(){
		waitUntilElementVisibleBy(driver, pgPaymentRecord, "ExplicitLowWait");
		if(verifyObjectDisplayed(pgPaymentRecord)){		
		verifyPageDisplayed(pgPaymentRecord, "Page Title", "Payment Record Page", "searchByPaymentRecords");
		fieldDataEnter(txtSearchPayment, testData.get("PaymentId"));
		clickElementJSWithWait(imgPaymentClick);
		verifyPageDisplayed(paymentPage, "Page Title", "Payment Record Page", "searchByPaymentRecords");
		tcConfig.updateTestReporter("PaymentRecordsPage", "searchByPaymentRecords", Status.PASS,
				"User navigated to Payment page Successfully");
		}
		else{
			tcConfig.updateTestReporter("PaymentRecordsPage", "searchByPaymentRecords", Status.FAIL,
					"Failed while navigating to payment page");
		}
		
	}
	
	
	
	

}
