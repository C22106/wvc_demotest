package tgs.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OfferPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(OfferPage.class);

	public OfferPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	Select select;
	public static String extensionRecord;
	
	protected static String slaeschannel;
	protected By txtOffers = By.xpath("//span[@title='Offers']");
	protected By txtOfferPage = By.xpath("//span[@class='slds-var-p-right_x-small uiOutputText forceBreadCrumbItem']");
	protected By btnAppLauncher = By.xpath("//div[@class='slds-icon-waffle']");
	protected By tabOffers = By.xpath("//p[@class='slds-truncate' and contains(.,'Offers')]");
	@FindBy(xpath = "//input[contains(@placeholder,'Search apps or items')]")
	WebElement txtSearchBy;
	protected By btnAdd = By.xpath("//footer[@class='slds-modal__footer']//button[@title='Add']");
	protected By searchby = By.xpath("//input[contains(@placeholder,'Search apps or items')]");
	protected By btnViewAll = By.xpath("//button[contains(text(),'View All')]");
	protected By noItems = By.xpath("//a[@title='No Items']//span");
	protected By joruneyDesktop = By.xpath("//p[contains(.,'Journey Desktop')]");
	protected By btnNew = By.xpath("(//div[@title='New'])[1]");
	protected By txtNewOffer = By.xpath("//h2[contains(.,'New Offer')]");
	protected By radioBtn = By.xpath("//span[@class='slds-radio--faux']");
	protected By btnNext = By.xpath("//span[contains(.,'Next')]");
	protected By hypOffers = By.xpath(
			"(//a[@class='slds-context-bar__label-action dndItem']//span[@class='slds-truncate' and contains(.,'Offers')])[1]");
	protected By txtOfferType = By
			.xpath("//h2[contains(@class,'slds-modal__title slds-hyphenate slds-text-heading--medium')]");
	protected By txtOfferName = By.xpath("//label[contains(.,'Offer Name')]/following::input[@name='Name']");
	protected By txtOfferDescription = By
			.xpath("//label[contains(.,'Offer Description')]/following::textarea[@class='slds-textarea']");
	protected By drpBrand = By.xpath("//label[contains(.,'Brand')]/following::input[@class='slds-input slds-combobox__input']");
	//protected By drpValue = By.xpath("//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left ')]//lightning-base-combobox-item//span[@title='" + testData.get("Brand").trim() + "']");	
	protected By drpValue = 
	By.xpath("//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left ')]//lightning-base-combobox-item//span[@title='Wyndham Vacation Resorts']");
	protected By txtNoOfAdults = By
			.xpath("//label[contains(.,'Number of Adults')]/../..//input[@name='Number_of_Adults__c']");
	protected By txtNoOfChild = By
			.xpath("//label[contains(.,'Number of Children')]/../..//input[@name='Number_of_Children__c']");
	protected By tblChosenLst = By.xpath("//button[@title='Move selection to Chosen']");
	protected By txtAccom = By.xpath("//span[contains(.,'Provide Accommodation?')]");
	protected By tabReferals = By.xpath("//span[contains(.,'Referrals')]");
	protected By chkRefReqd = By.xpath("//span[contains(.,'Referral Required?')]/../..//input[@type='checkbox']");
	protected By chkRefRqdTour = By
			.xpath("//span[contains(.,'Referral Required to Tour')]/../..//input[@type='checkbox']");
	//protected By btnSave = By.xpath("//button[@title='Save']");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By tabDetails = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[2]");
	protected By tabDetail = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[1]");
	protected By txtEffDate = By.xpath("//label[contains(.,'Effective Start Date')]/../..//input[@name='Effective_Start_Date__c']");
	protected By txtEndDate = By.xpath("//label[contains(.,'Effective End Date')]/../..//input[@name='End_Date__c']");
	protected By errorMsg = By.xpath("//h2[@class='slds-truncate slds-text-heading_medium']");
	/*protected By lnkToday = By
			.xpath("//button[contains(text(),'Today') and contains(@class,'slds-button slds-align_absolute-center slds-text-link')]"); */
	protected By txtGlobalSearch = By.xpath("//input[@placeholder='Search Offers and more...']");
	protected By imgClick = By.xpath("//img[@class='icon ' and contains(@src,'custom/custom47_120.png')]");
	protected By tabExtensnRestriction = By
			.xpath("//a[@class='slds-tabs_default__link' and contains(.,'Extension Restriction')]");
	protected By lnkExtnsnRestn = By.xpath("//span[@title='Extension Restrictions']");
	protected By btnNewExtnRes = By
			.xpath("//button[normalize-space()='New']");
	protected By txtExtnRest = By.xpath("//label[contains(.,'Extensions')]/../..//input[@class='slds-input slds-combobox__input']");
	protected By addExtn = By.xpath("//span[@title='New Extensions']");
	protected By txtExtnsnName = By.xpath("//span[contains(.,'Extensions Name')]/../..//input[@class=' input']");
	protected By drpExtnLngth = By.xpath("//span[contains(.,'Extension Length')]/../..//a[@class='select']");
	protected By btnSaveExtnRest = By.xpath("(//button[@title='Save'])[2]");
	protected By txtExtnRestEffSt  =By.xpath("(//label[contains(.,'Effective Start Date')]/../..//input[@name='Effective_Date__c'])");
	protected By txtEffStartDate = By
			.xpath("(//span[contains(.,'Effective Start Date')]/../..//input[@class=' input'])[1]");
	protected By txtEfftEndDate = By.xpath("(//span[contains(.,'Effective End Date')]/../..//input[@class=' input'])");
	/*protected By txtEffStDate = By
			.xpath("//label[contains(.,'Effective Start Date')]/following::input[@name='Effective_Date__c']"); */
	
	protected By txtEffStDate = By
			.xpath("//label[contains(.,'Effective Start Date')]/following::input[@name='Effective_Date__c']");

	protected By txtEffEndDateExtn  = By.xpath("//label[contains(.,'Effective  End Date')]/following::input[@name='End_Date__c']");
	
	protected By extLnkToday = By.xpath("//button[contains(text(),'Today') and contains(@class,'slds-button slds-align_absolute-center slds-text-link')]");
	
	
	

	protected By lnkToday = By
				.xpath("//button[contains(text(),'Today') and contains(@class,'today slds-button slds-align_absolute-center slds-text-link')]");//button[contains(text(),'Today') and contains(@class,'slds-button slds-align_absolute-center slds-text-link')]

	
	
	protected By txtNewExtn = By.xpath("//h2[contains(.,'New Extensions')]");
	protected By txtExtensionName = By.xpath("//span[contains(.,'Extensions Name')]/../..//input[@class=' input']");
	protected By drpExtnLength = By.xpath("//span[contains(.,'Extension Length')]/../..//div[@class='uiMenu']//a");
	protected By drpValueExtnRest = By.xpath("//div[@class='select-options']//li[2]//a");
	protected By txtMinExtnPrice = By
			.xpath("//span[contains(.,'Minimum Extension Price')]/../..//input[@class='input uiInputSmartNumber']");
	protected By txtMaxExtnPrice = By
			.xpath("//span[contains(.,'Maximum Extension Price')]/../..//input[@class='input uiInputSmartNumber']");
	protected By lnkClickRecord = By.xpath(
			"//span[@class='toastMessage slds-text-heading--small forceActionsText']//a[@class='forceActionLink']//div");
	protected By tabDetailsExtnRest = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[2]");
	protected By hypExtn = By
			.xpath("//span[contains(.,'Extensions')]/../..//div[@class='slds-form-element__control']//a");
	protected  By offSCDeatilsTab = By.xpath("(//a[@class='slds-tabs_default__link' and @id='detailTab__item'])[3]");
	protected By dtField = By.xpath("(//input[@class=' input'])[1]");
	protected By btnSaveNwExtn = By.xpath("(//button[@title='Save'])[3]");
	protected By lnkSalesChannel = By.xpath("//a[contains(.,'Sales Channel') and @class='slds-tabs_default__link']");
	protected By btnAddSalesChannel = By.xpath("//button[@title='Add Sales Channel']");
	protected By txtAddOffeRSlsChnnl = By.xpath("//h2[contains(.,'Add Offer Sales Channel')]");
	protected By txtSearchSlsChnnl = By.xpath("//input[@placeholder='Search Sales Channel']");
	protected By plusIcon = By.xpath("//label[@class='slds-checkbox_faux']");
	protected By hypOfferSlsChhnl = By.xpath("//table[contains(@class,'slds-table slds-table_header-fixed slds-table')]//tbody//a//span[starts-with(.,'OSC-')]");
	protected By offrSlsDetailsTab = By.xpath("(//a[@class='slds-tabs_default__link' and @id='detailTab__item'])[2]");
	protected By txtCarMinPrice = By.xpath(
			"(//span[contains(.,'Car Minimum Net price')]/../..//div[@class='slds-form-element__control']//lightning-formatted-number)[1]");
	protected By txtCarMaxPrice = By.xpath(
			"(//span[contains(.,'Car Maximum Net price')]/../..//div[@class='slds-form-element__control']//lightning-formatted-number)[1]");
	protected By tblsearchSlsChnnl = By.xpath(
			"//table[contains(@class,'slds-table slds-table_fixed-layout slds-table')]//tbody//tr//div[@title='Name']");
	protected By plusAdd = By.xpath("//label[@class='slds-checkbox_faux']");
	protected By drpGuestType = By.xpath("//select[@class='slds-select']");
	protected By tblBlank = By.xpath(
			"//table[contains(@class,'slds-table slds-table_fixed-layout slds-table_bordered slds-scrollable_none')]//tbody");
	protected By tabDraft = By.xpath("//a[@class='tabHeader slds-path__link' and @title='Draft']");
	protected By lnkExtnRestn = By.xpath("//a[contains(.,'Extension Restriction')]");
	protected By txtExtnRestriction = By.xpath("//h2[contains(.,'New Extension Restrictions')]");
	protected By txtSearchExtn = By.xpath("//span[contains(.,'Extensions')]/../..//input[@title='Search Extensions']");
	protected By plusIconExtn = By.xpath("//span[@title='New Extensions']");
	protected By btnSaveER = By.xpath("(//button[@title='Save'])[1]");
	protected By btnERSave = By.xpath("(//button[@title='Save'])[2]");
	protected By rdBAFOFfer = By.xpath("(//span[@class='slds-radio--faux'])[2]");
	//protected By selectNo = By.xpath("(//div[@class='slds-dueling-list__options']//li)[2]//span[@title='"+ testData.get("AllowedNumberofNights").trim() + "']");
	protected By txtAddExtnRest = By
			.xpath("//h2[contains(@class,'inlineTitle slds-p-top--large slds-p-horizontal--medium slds')]");
	protected By errorMsgSCT = By.xpath("//li[@class='form-element__help']");
	protected By lstData = By.xpath("//li[@class='slds-listbox__item']//*[@class='slds-media slds-listbox__option slds-media_center slds-listbox__option_entity']");
	protected By txtSearhExtn = By.xpath("//span[contains(.,'Extensions')]/../..//input[@placeholder='Search Extensions...']");
	protected By txtNewSalesChannel = By.xpath("//h2[contains(.,'New Sales Channel: Master Sales Channel')]");					
	protected By txtSalesChnlName = By.xpath("//span[contains(.,'Sales Channel Name')]/../..//input[@class=' input']");
	protected By drpCategory = By.xpath("(//span[contains(.,'Category')]/../..//a[@class='select'])");				
	protected By txtHrsOfOpt = By.xpath("//span[contains(.,'Hours of Operation')]/../..//input[@class=' input']");
	protected By txtAddress= By.xpath("//span[contains(.,'Address')]/../..//textarea[@class=' textarea']");
	protected By txtEffDtMSC = By.xpath("(//span[contains(.,'Effective Start Date')]/../..//input[@class=' input'])[2]");
	protected By btnSaveMSC = By.xpath("(//button[@title='Save']//span)[2]");
	protected By sucessMSG = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtNwSlsChnnl = By.xpath("//span[@title='New Sales Channel']");
	protected By txtNewSlsChannel = By.xpath("(//h2[contains(.,'New Sales Channel')])[2]");
	protected By txtSearchSaleschannel =By.xpath("//input[@title='Search Sales Channels']");
	protected By txtSearchSaleschannel1 =By.xpath("//input[@placeholder='Search Sales Channels...']");
	protected By searchMSC = By.xpath("(//table[contains(@class,'slds-table slds-table_fixed-layout slds')]/following::footer)[1]");
	protected By searchMSC1 = By.xpath("//div[@class='noResults slds-p-vertical--xx-large slds-m-top--large slds-align--absolute-center slds-grid--vertical slds-text-align--center slds-text-color--weak']//img");

	protected By lnkExtensionRestriction = By.xpath("//span[@title='Extension Restrictions']");
	protected By txtExtnRestn = By.xpath("(//h1[@title='Extension Restrictions'])[1]");
	protected By lstExtnRest = By.xpath("(//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds')])[2]//tbody//span//a[starts-with(.,'ER-')]");
	protected By txtAddOffrSlsChnnl = By.xpath("//h2[@class='slds-text-heading_medium slds-hyphenate']");
	protected  By txtEffStartDT = By.xpath("//label[contains(.,'Effective Start Date')]/following::input[@name='Effective_Start_Date__c']");
	protected  By txtEffEndDT = By.xpath("//label[contains(.,'Effective End Date')]/following::input[@name='End_Date__c']");
	protected By txtEffAddOffrStartDate = By.xpath(
			"//table[contains(@class,'slds-table slds-table_fixed-layout slds-table')]//tbody//tr//th[2]//a[@class='datePicker-openIcon display']");
	protected By txtEffEndDate = By.xpath(
			"//table[contains(@class,'slds-table slds-table_fixed-layout slds-table')]//tbody//tr//td[3]//input[@class=' input']");
	protected By erroMsgOSC = By.xpath("//span[@class='toastMessage forceActionsText']");

	// protected By lnkClickRecord = By.xpath("//div[@class='slds-theme--success
	// slds-notify--toast slds-notify slds-notify--toast
	// forceToastMessage']//a[@class='forceActionLink']//div");

	/* Xpath by Krishnaraj */
	protected By OffersTab = By.xpath("//a/span[text()='Offers']");
	protected By offerNameLink = By.xpath("//a[@title='" + testData.get("Offer") + "']");
	protected By newButton = By.xpath("//div[@title='New']");
	protected By recentlyViewedLink = By.xpath("//span[@class='triggerLinkText selectedListView uiOutputText']");
	protected By offersToBeValidatedLink = By
			.xpath("//div[@class='panel-content scrollable']//span[text()='Offers to be Validated']");
	protected By recentlyViewedOfffersInDropdown = By
			.xpath("//div[@class='panel-content scrollable']//span[text()='Recently Viewed']");
	protected By offersToBeValidatedTitle = By.xpath(
			"//span[@class='triggerLinkText selectedListView uiOutputText' and text()='Offers to be Validated']");
	
	protected By recentlyViewedTitle = By.xpath(
			"//span[@class='triggerLinkText selectedListView uiOutputText' and text()='Recently Viewed']");
	protected By offerSearchTextBox = By.xpath("//input[@placeholder='Search this list...']");
	protected By spinnerGrid = By.xpath("//div[@class='slds-spinner_container slds-grid']");

	/* New Offer choose Record Type elements */
	protected static String Destination1, Accommodation1;
	protected By recordTypePageTitle = By.xpath("//h2[text()='New Offer']");
	protected By regularOfferRadioBtn = By
			.xpath("//span[@class='slds-form-element__label' and text()='Regular Offer']");
	protected By bringFriendOfferRadioBtn = By
			.xpath("//span[@class='slds-form-element__label' and text()='Bring a Friend Offer']");
	protected By face2faceOfferRadioBtn = By
			.xpath("//span[@class='slds-form-element__label' and text()='Face to Face Offer']");
	protected By splEventOfferRadioBtn = By
			.xpath("//span[@class='slds-form-element__label' and text()='Special Event Offer']");
	protected By nextButton = By.xpath("//span[text()='Next']");

	/* Offer Record Type elements */
	protected By regularOfferPageTitle = By.xpath("//h2[text()='New Offer: Regular Offer']");
	protected By bringAFriendOfferPageTitle = By.xpath("//h2[text()='New Offer: Bring a Friend Offer']");
	protected By faceToFaceOfferPageTitle = By.xpath("//h2[text()='New Offer: Face to Face Offer']");
	protected By specialEventOfferPageTitle = By.xpath("//h2[text()='New Offer: Special Event Offer']");

	/* Face to Face Offer elements */
	protected By facetofaceMandFieldsError = By.xpath(
			"//li[text()='These required fields must be completed: Brand, Offer Name, Number of Adults, Number of Children, Offer Description']");

	/* New Offer Record Type elements */
	protected static String offerName;
	protected By offerNameText = By.xpath("//label[text()='Offer Name']/../..//input[@type='text']");
	protected By offerDescriptionText = By.xpath("//label[text()='Offer Description']/../..//textarea");
	protected By brandField = By.xpath(
			"//label[text()='Brand']//following::div[@class='slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right']/input");
	protected By brandDropdwn = (By.xpath("//span[@title='" + testData.get("Brand") + "']"));
	protected By noOfNightsDropDwn = By
			.xpath("//div[text()='Allowed Number of Nights']/following::li[" + testData.get("Nights") + "]/div");
	protected By addNightSelectButton = By
			.xpath("//div[text()='Allowed Number of Nights']/following::button[1][@title='Move selection to Chosen']");
	protected By expMonthsDropDwn = By.xpath("//div[text()='Expiration Months Available']/following::li[1]/div");
	protected By addMonthsSelectButton = By.xpath(
			"//div[text()='Expiration Months Available']/following::button[1][@title='Move selection to Chosen']");
	protected By noOfAdultsText = By.xpath("//label[text()='Number of Adults']/../..//input[@type='text']");
	protected By noOfChildText = By.xpath("//label[text()='Number of Children']/../..//input[@type='text']");
	protected By scrollOfferSection = By.xpath(
			"(//force-record-layout-block//div[@class='test-id__section-content slds-section__content section__content align-with-title'])[1]");
	//Updated to validate error message for same user
	protected By saveButtonOnIsVal = By.xpath("//button[@title='Save']//span[contains(@class,'label bBody')][normalize-space()='Save']");

	//xpath by Shubhendra for offers Availibility
	protected By offersAvailibility = By.xpath("/html/body/div[4]/div[1]/section/div[2]/div[1]/div[3]/ul/li/div/div/button/span");
	protected By editBoxEnterSalesChannelName = By.xpath("//input[@placeholder='Enter Sales Channel Name']");
	protected By buttonNextOnSalesChannelSelection = By.xpath("//button[contains(text(),'Next')]");
	protected By autoSuggestionQASales = By.xpath("//*[@id='a5D7h0000001nBOEAY']/div");
	protected By dropBoxStateOfRsidience = By.xpath("//select[@id='1192:0']");
	protected By stateCalforniaInDropDown = By.xpath("//option[contains(text(),'California')]");

//Start here

	protected By referralTitle = By.xpath("//span[text()='Referrals']");
	protected By referralSection = By.xpath(
			"(//force-record-layout-block//div[@class='test-id__section-content slds-section__content section__content align-with-title'])[2]");
	protected By effStartDateCal = By.xpath("//label[text()='Effective Start Date']//following::input[1]");
	protected By effEndDateCal = By.xpath("//label[text()='Effective End Date']//following::input[1]");
	protected By expirationDateCal = By.xpath("//label[text()='Expiration Date']//following::input[1]");
	protected By dateLink = By.xpath("//button[text()='Today']");
	protected By saveButton = By.xpath("//button[text()='Save']");
	protected By closeErrorDialogButton = By.xpath("//button[@title='Close error dialog']");
	protected By successMessageTitle = By
			.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By referralReqCheckBox = By.xpath("//span[text()='Referral Required?']/following::input[1]");
	protected By referralReqToTourCheckBox = By.xpath("//span[text()='Referral Required to Tour']/following::input[1]");

	/* Record specific elements - General */
	protected By offerToTest = By.xpath("//a[@title='Offer UWISK']");
	protected By markAsCompleteButton = By.xpath(
			"//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction current uiButton']");
	protected By draftTabActive = By.xpath("//a[@title='Draft' and @aria-selected = 'true']");
	protected By inProgressTabActive = By.xpath("//a[@title='In Progress' and @aria-selected = 'true']");
	protected By offerNameOnOfferDetail1 = By.xpath("(//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11'])[1]");
	protected By offerNameOnOfferDetail = By.xpath("//slot[@slot='primaryField']//lightning-formatted-text[contains(text(),'Automated')]");

	protected By reviewTabActive = By.xpath("//a[@title='Review' and @aria-selected = 'true']");
	protected By validateTabActive = By.xpath("//a[@title='Validate' and @aria-selected = 'true']");
	protected By completeTabActive = By.xpath("//a[@title='Complete' and @aria-selected = 'true']");

	/* Record specific elements - Destination */
	protected By offerDestinationTab = By.xpath("//a[text()='Destination']");
	protected By offerDestinationLink = By.xpath("//span[text()='Offer Destinations']");
	protected By addDestinationButton = By.xpath("//button[@title='Add Destination']");
	protected By addOfferDestTitle = By.xpath("//div[header[contains(., 'Add Offer Destination')]]//h2");
	protected By destinationSearchBox = By.xpath("//input[@placeholder='Search Destination']");
	protected By destinationSelectIcon = By.xpath("//label[@class='slds-checkbox_faux']");
	protected By destinationFirstItem = By.xpath("(//div[@class='slds-checkbox_add-button'])[1]");
	protected By destStartDateCal = By.xpath(
			"(//div[@class='form-element uiInput uiInputDate uiInput--default uiInput--input uiInput--datetime'])[1]/input");
	protected By destEndDateCal = By.xpath(
			"(//div[@class='form-element uiInput uiInputDate uiInput--default uiInput--input uiInput--datetime'])[2]/input");
	protected By toastSuccessMessage = By.xpath(
			"//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage' and @data-key = 'success']");
	protected By toastErrorMessage = By.xpath(
			"//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage' and @data-key = 'error']");
	protected By toastMessageTitle = By.xpath("//span[@class='toastMessage forceActionsText']");
	protected By saveSnagError = By.xpath("//div[@class='fieldLevelErrors']");
	protected By currentDate = By.xpath("//td[@class='slds-is-selected uiDayInMonthCell']");
	protected By NextDate = By.xpath("//td[@class='slds-is-today uiDayInMonthCell']");
	protected By nextMonth = By.xpath("//a[@title='Go to next month']");
	protected By startDate = By.xpath("//td[@class='uiDayInMonthCell']//span[text()='3']");
	protected By endDate = By.xpath("//td[@class='uiDayInMonthCell']/span[text()='9']");
	protected By endDate2 = By.xpath("//td[@class='uiDayInMonthCell']/span[text()='10']");
	protected By oldthirdDate = By.xpath("//td[@class='slds-is-selected uiDayInMonthCell']/following::td[2]");
	protected By nextMonthAccNetMax = By.xpath("//button[@title='Next Month']");
	protected By startDateAccNetMax = By.xpath("//span[text()='3']");
	protected By endDateAccNetMax = By.xpath("//span[text()='9']");
	protected By endDate2AccNetMax = By.xpath("//span[text()='10']");
	// protected By addedDestination =
	// By.xpath("(//th[@title='Destination'])/following::a[2]");
	protected By addedDestination = By.xpath("//a[@title='" + testData.get("Destination") + "']");
	protected By offerBreadcrumbLink = By.xpath(
			"//div[@class='windowViewMode-normal oneContent active lafPageHost']//ol[@class='slds-breadcrumb slds-list--horizontal slds-wrap']/li[2]");

	protected By toastInfoMandatAssignments = By.xpath(
			"//div[@class='slds-theme--info slds-notify--toast slds-notify slds-notify--toast forceToastMessage']");
	/* Record specific elements - Accommodation */
	protected String accomValue1, accomValue2;
	protected By offerAccommodationTab = By.xpath("//a[text()='Accommodation']");
	protected By addOfferAccommodation = By.xpath("//span[text()='Offer Accommodations']/following::div[@title='New']");
	protected By newOfferAccomTitle = By.xpath("//h2[text()='New Offer Accommodations']");
	protected By effStartDate = By.xpath("(//a[@class='datePicker-openIcon display'])[1]");
	protected By effEndDate = By.xpath("(//a[@class='datePicker-openIcon display'])[2]");
	protected By accomCurrentDate = By.xpath("//td[@class='slds-is-today slds-is-selected uiDayInMonthCell']");
	protected By addedAccommodation = By.xpath("(//th[@title='Accommodation'])/following::a[2]");
	protected By addedAccommodationItem2 = By.xpath("(//th[@title='Accommodation'])/following::a[5]");
	protected By accommodationLookUpBox = By.xpath("//input[@placeholder='Search Accommodations...']");
	protected By accomSelectItem1 = By.xpath(
			"//li[@class = 'lookup__item  default uiAutocompleteOption forceSearchInputLookupDesktopOption'][1]");
	protected By accomSelectItem2 = By.xpath(
			"//li[@class = 'lookup__item  default uiAutocompleteOption forceSearchInputLookupDesktopOption'][2]");
	protected By offerAccommodationLink = By.xpath("//span[text()='Offer Accommodations']");

	/* Record specific elements - Premium */
	protected static String premiumValue1, premiumValue2;
	protected By offerPremiumTab = By.xpath("//a[text()='Premium']");
	protected By addOfferPremium = By.xpath("//span[text()='Offers Premiums Assigneds']/following::div[@title='New']");
	protected By newOfferPremiumTitle = By.xpath("//h2[text()='New Offers Premiums Assigned']");
	protected By premiumCurrentDate = By.xpath("//td[@class='slds-is-today slds-is-selected uiDayInMonthCell']");
	protected By addedPremium = By.xpath("(//th[@title='Product'])/following::a[2]");
	protected By addedPremiumItem2 = By.xpath("(//th[@title='Product'])/following::a[5]");
	protected By premiumLookUpBox = By.xpath("//input[@placeholder='Search Products...']");
	protected By premiumSelectItem1 = By
			.xpath("//li[2]//span[@class = 'slds-listbox__option-text slds-listbox__option-text_entity']");
	protected By premiumSelectItem2 = By
			.xpath("//li[3]//span[@class = 'slds-listbox__option-text slds-listbox__option-text_entity']");
	protected By editPremiumlink = By.xpath("//a[contains(@title, 'OPA')]");
	protected By editButton = By.xpath("(//button[@name='Edit'])[2]");
	protected By OfferPremiumAssignedTitle = By.xpath("//div[text()='Offers Premiums Assigned']");
	protected By OfferPremiumEditTitle = By.xpath("//h2[contains(.,'Edit')]");
	protected By premiumDeleteIcon = By.xpath("(//a[@class='deleteAction'])[1]");
	protected By premiumSelectItem3 = By.xpath(
			"//li[@class = 'lookup__item  default uiAutocompleteOption forceSearchInputLookupDesktopOption'][3]");
	protected By newProductButton = By.xpath("//span[text()='New Product']");
	protected By newProductTitle = By.xpath("//h2[text()='New Product']");
	protected By tourRadioButton = By.xpath("//span[text()='Tour Premium']");
	protected By newProductTourPremiumTitle = By.xpath("//h2[text()='New Product: Tour Premium']");
	protected By premiumEffStartDate = By.xpath("(//a[@class='datePicker-openIcon display'])[3]");
	protected By premiumEffEndDate = By.xpath("(//a[@class='datePicker-openIcon display'])[4]");
	protected By productNameTextBox = By.xpath("//span[text()='Product Name']/following::input[1]");
	protected By productFamilyValue = By
			.xpath("(//a[@class='select'])[2]/following::div[@class='select-options']//li[a[@title='Activities']]");
	protected By productFamilyField = By.xpath("(//a[@class='select'])[1]");
	protected By productTypeField = By.xpath("(//a[@class='select'])[2]");
	protected By crmproductFamilyField = By.xpath("(//a[@class='select'])[2]");
	protected By crmproductTypeField = By.xpath("(//a[@class='select'])[1]");
	protected By productTypeDropdownValue = By
			.xpath("(//a[@class='select'])[2]/following::div[@class='select-options']//li[a[@title='Pre-Paid']]");
	protected By retailValueTextBox = By.xpath("//span[text()='Retail Value']/following::input[1]");
	protected By displayNameTextBox = By.xpath("//span[text()='Display Name']/following::input[1]");
	protected By costTextBox = By.xpath("//span[text()='Cost']/following::input[1]");
	protected By marketerCostTextBox = By.xpath("//span[text()='Marketer Cost']/following::input[1]");
	protected By saveButton2 = By.xpath("(//button[@title='Save'])[2]");
	protected By offerPremiumsAssignedLink = By.xpath("//span[text()='Offers Premiums Assigneds']");

	// button[@title='Clear Selection']

	/* Record specific elements - Offer Attribute */
	protected String attributeValue1, attributeValue2;
	protected By offerAttributeTab = By.xpath("//a[text()='Offer Attribute']");
	protected By addOfferAttribute = By.xpath("//span[@title='Offer Attributes']/following::button[text()='New']");
	protected By newOfferAttributeTitle = By.xpath("//h2[text()='New Offer Attributes']");
	protected By attributeName = By.xpath("//input[@name='Name']");
	protected By attributeName2 = By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']/input");
	protected By attributeValue = By.xpath("//label[text()='Value']/../..//textarea");
	protected By attributeDesc = By.xpath("//label[text()='Description']/../..//textarea");
	protected By addedAttribute = By.xpath("//span[@title='Name']/following::a[1]");
	protected By addedAttribute2 = By.xpath("//span[@title='Name']/following::a[3]");
	protected By OfferAttributeLink = By.xpath("//span[text()='Offer Attributes']");
	protected By offerAttributeTitle = By.xpath("//h1[text()='Offer Attributes']");
	protected By offerOption = By.xpath("//a[@class='pill focused uiPill--default uiPill']");

	/* Record specific elements - SalesChannel */
	protected By offerSalesChannelTab = By.xpath("//a[text()='Sales Channel']");
	protected By addSalesChannelButton = By.xpath("//button[@title='Add Sales Channel']");
	protected By addOfferSalesChTitle = By.xpath("//div[header[contains(., ' Add Offer Sales Channel')]]//h2");
	protected By salesChannelSearchBox = By.xpath("//input[@placeholder='Search Sales Channel']");
//	protected By salesChannelSelectIcon = By.xpath("//div[@class='slds-checkbox_add-button']");
	protected By salesChannelSelectIcon = By.xpath("//div[text()='" + testData.get("SalesChannel") + "']//preceding::div[1]");
	protected By guestSelectIcon = By.xpath("//span[@title='Guest Type']/following::div[3]/..//select");
	protected By offerSalesChannelLink = By.xpath("//span[text()='Offer Sales Channels']");
	protected By addedSalesChannel = By.xpath("//a[@title='" + testData.get("SalesChannel") + "']");

	/* Record specific elements - Offer SalesChannel Details */
	protected By OSCLink = By.xpath("//a[contains(@title, 'OSC')]");
	protected By offerSalesChannelDetails = By.xpath("//div[contains(text(),'Offer Sales Channel')]");
	protected By relatedSectionLink = By.xpath("//li[@class='slds-tabs_default__item' and @title='Related']");
	protected By offerPriceSection = By.xpath("//span[text()='Offer Prices']");
	protected By offerPriceNewButton = By
			.xpath("//div[@class='windowViewMode-normal oneContent active lafPageHost']//following::a[@title='New']");
	protected By newOfferPriceTitle = By.xpath("//h2[text()='New Offer Prices']");
	protected By offerPriceTypeField = By.xpath(
			"//label[text()='Offer Price Type']//following::div[@class='slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click'][1]");
	protected By typePriceOption = By.xpath("//lightning-base-combobox-item[ @data-value='Price']");
	protected By typePointsOption = By.xpath("//lightning-base-combobox-item[ @data-value='Points']");
	protected By typePricePointsOption = By.xpath("//lightning-base-combobox-item[ @data-value='Price & Points']");
	protected By noOfNights = By.xpath("//input[@name='No_of_Nights__c']");
	protected By offerPrice = By.xpath("//input[@name='Offer_Price__c']");
	protected By offerPoints = By.xpath("//input[@name='Offer_Points__c']");
	protected By savePriceButton = By
			.xpath("//div[@class='modal-container slds-modal__container']//button[@title='Save']");
	protected By detailsSectionLink = By
			.xpath("//div[@class='windowViewMode-normal oneContent active lafPageHost']//li[@title='Details']");
	protected By OfferNameLinkSalesCh = By.xpath("//span[text()='Offer']/following::div[2]");

	/* Record specific elements - Disclosure */
	protected By offerDisclosureTab = By.xpath("//a[text()='Disclosure']");
	protected By addDisclosureNewButton = By.xpath("//span[text()='Offer Disclosures']/following::button[text()='New']");
	protected By addDisclosureTitle = By.xpath("//h2[text() = 'New Offer Disclosures']");
	protected By salesChannelLookup = By.xpath("//input[@placeholder='Search Sales Channels...']");
	protected By salesChannelSelect = By.xpath(
			"//ul/li/lightning-base-combobox-item[@class='slds-media slds-listbox__option slds-media_center slds-listbox__option_entity']");
	protected By guestOption = By.xpath(
			"//lightning-base-combobox-item[@class='slds-media slds-listbox__option slds-media_center slds-media_small slds-listbox__option_plain' and @data-value='All']");
	protected By guestField = By.xpath(
			"//label[text()='Guest Type']//following::div[@class='slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click']");
	protected By offerDisclosureText = By.xpath("//label[text()='Offer Disclosure Text']/../..//textarea");
	protected By effStartDatePicker = By.xpath("(//a[@class='datePicker-openIcon display'])[1]");
	// protected By displayedOfferDisclosure = By.xpath("//span[text()='Offer
	// Disclosures Name']/following::th[8]/span/a");
	// protected By displayedOfferDisclosure = By.xpath("//a[@title='" +
	// addedDisclosure + "']");
	protected By offerDisclosuresLink = By.xpath("//span[text()='Offer Disclosures']");
	protected By addedOfferDisclosure = By
			.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']/a/div");

	/* Record specific elements - Business Rule */
	protected static String businessRuleName, salesChannelName;
	protected By businessRulesTab = By.xpath("//span[text()='Business Rules']");
	protected By addedBusinessRule = By.xpath("//span[text()='Business Rules Name']/following::a[6]");
	protected By salesChLinkedToBrule = By.xpath("//span[text()='Business Rules Name']/following::a[7]");

	/* Record specific elements - Legal Registration Program Number */
	protected By legalProgramEditLink = By.xpath(
			"//span[@class='bodyHeader slds-grid slds-grid_align-spread slds-path__coach-title']/a[@title='Edit Key Fields']");
	protected By keyHeaderEditLink = By.xpath(
			"//div[@class='windowViewMode-normal oneContent active lafPageHost']//following::a[@title='Edit Key Fields']");
	protected By editOfferPage = By.xpath("//h2[text()='Edit Offer']");
	protected By legalRegProgNumTextBox = By
			.xpath("//div[label[contains(., 'Legal Registration Program Number')]]/input");
	protected By editOfferSaveButton = By.xpath("//h2[text()='Edit Offer']/following::button[@title='Save']");

	/* Record specific elements - Validate Offer */
	protected By isValidatedPencilIcon = By.xpath("//button[@title='Edit Is Validated?']");
	protected By isValidatedImg = By.xpath("//span[@class='uiImage uiOutputCheckbox']/img");
	protected By isValidatedCheckBox = By.xpath("//div[label[contains(., 'Is Validated?')]]/input");
	protected By isValidatedError = By.xpath("//ul[@class='errorsList']");
	protected By cancelButton = By.xpath("//button[@title='Cancel']");
	protected By validatedUserPencilIcon = By.xpath("//button[@title='Edit Validated User']");
	protected By validatedUserTextBox = By.xpath("//input[@placeholder='Search People...']");
	protected By corpSuperUserValue = By.xpath("//div[@title='WVO Corporate Super User']");
	//UAT
	protected By corpSuperUserValueUAT = By.xpath("//div[@title='WVO Marketer']");
	protected By deleteIcon = By.xpath("//span[@class='deleteIcon']");

	/* Record specific elements - Extension Restriction */
	protected By offerExtRestrictionTab = By.xpath("//a[text()='Extension Restriction']");
	protected By addExtRestNewButton = By
			.xpath("//span[text()='Extension Restrictions']/following::div[@title='New'][1]");
	protected By extRestPageTitle = By.xpath("//h2[text() = 'New Extension Restrictions']");
	protected By searchExtensionBox = By.xpath("//input[@placeholder='Search Extensions...']");
	protected By comboListItem = By.xpath(
			"(//ul/li/lightning-base-combobox-item[@class='slds-media slds-listbox__option slds-media_center slds-listbox__option_entity'])[2]");
	protected By comboListItem1 = By
			.xpath("(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'])[2]/span");
	protected By comboListItem2 = By
			.xpath("(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'])[3]/span");
	protected By scrollModal = By.xpath("//div[@class='modal-container slds-modal__container']");
	protected By restrictionReasonTextbox = By.xpath("//label[text()='Restriction Reason']/../..//textarea");
	protected By effStartDateER = By.xpath("//label[text()='Effective Start Date']//following::input[@name='Effective_Date__c']");
	protected By saveExtResButton = By
			.xpath("//div[@class='modal-container slds-modal__container']//button[@title='Save']");
	protected By salesChannelBox = By.xpath("//input[@placeholder='Search Sales Channels...']");
	protected  By expMonth = By.xpath("//span[@title='3 months']");
	protected By btnSave = By.xpath("//button[@name='SaveEdit']");
	protected By selectNo = By.xpath("(//div[@class='slds-dueling-list__options'])[1]//span[@title='2']");
	
	//Shubhenndra Xpath
	protected By saveButtonToValidate = By.xpath("//button[@title='Save']//span[contains(@class,'label bBody')][normalize-space()='Save']");

	

	/*
	 * Description: Navigate to menu Icon Date: July/2020 Author: Jyoti Changes
	 * By: NA
	 */
	public void navigateToMenu(String Iterator) {
		waitUntilElementVisibleBy(driver, txtOffers, 120);
		clickElementJSWithWait(btnAppLauncher);
		waitUntilElementVisibleBy(driver, btnViewAll, 120);
		clickElementBy(btnViewAll);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		sendKeysBy(searchby, testData.get("Menu" + Iterator));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("(//mark[text()='" + testData.get("Menu" + Iterator) + "'])")).click();

	}

	/*
	 * Description: Click on Offers tab to explicitly navigate Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void navigateToOffersTab() {
		clickElementJSWithWait(hypOffers);
		verifyPageDisplayed(txtOfferPage, "Page Title", "OffersPage", "navigateToOffersTab");

	}

	/*
	 * Description: Click New button on Offer screen Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void clickNew() {
		clickElementJSWithWait(btnNew);
		verifyPageDisplayed(txtNewOffer, "Page Title", "OffersPage", "clickNew");

	}

	/*
	 * Description: Click New button on Offers screen and select Regular Offer
	 * Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void clickNew_RegularOffer() {
		waitUntilElementVisibleBy(driver, txtOffers, "ExplicitMedWait");
		clickElementJSWithWait(btnNew);
		clickElementBy(btnNext);
		verifyPageDisplayed(txtOfferType, "Page Title", "OffersPage", "clickNew_RegularOffer");
	}

	/*
	 * Description: Select Bring A Friend Offer Type Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void selectOfferType_BringAFrnd() {
		waitUntilElementVisibleBy(driver, txtNewOffer, "ExplicitMedWait");
		clickElementBy(rdBAFOFfer);
		clickElementBy(btnNext);
		verifyPageDisplayed(txtOfferType, "Page Title", "OffersPage", "selectOfferType_BringAFrnd");
	}

	/*
	 * Description: Create Bring A Friend Offer with overlapping dates Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	
	
	
	public void createOffer() throws AWTException {
        waitUntilElementVisibleBy(driver, txtOfferType, "ExplicitMedWait");
        String strOfferNm = getRandomStringText(4);    
        String strOfferName = "Offer " +strOfferNm;
        sendKeysBy(txtOfferName, strOfferName);
        String strOfferDes = getRandomStringText(4);
        String strOfferDesc = "Offer created for test" + strOfferDes;
        sendKeysBy(txtOfferDescription, strOfferDesc);
        clickElementBy(drpBrand);
        clickElementBy(drpValue);
        WebElement wbNoOfNights = driver.findElement(selectNo);
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        wbNoOfNights.click();
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        List<WebElement> button = driver.findElements(tblChosenLst);
        button.get(0).click();
        waitForSometime(tcConfig.getConfig().get("LowWait"));        
        clickElementBy(expMonth);        
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        List<WebElement> expMonths = driver.findElements(tblChosenLst);
        button.get(1).click();
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        String strAdults = getRandomString(1);
        sendKeysBy(txtNoOfAdults, strAdults);
        String strChild = getRandomString(1);
        sendKeysBy(txtNoOfChild, strChild); 
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementBy(txtEffDate);
        selectSystemDate(lnkToday);
        waitUntilElementVisibleBy(driver, txtEndDate, 120);
        clickElementBy(txtEndDate);
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/YYYY");
        Date dt = new Date();
        Calendar cl = Calendar.getInstance();
        cl.setTime(dt);
        cl.add(Calendar.DAY_OF_YEAR, -2);
        dt = cl.getTime();
        String str = df.format(dt);
        System.out.println("the past date  is " + str);
        WebElement el = driver.findElement(txtEndDate);
        el.sendKeys(str);
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        do {
            Robot rob = new Robot();
            rob.mouseWheel(3);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
        } while (!driver.findElement(tabReferals).isDisplayed());
        waitUntilElementVisibleBy(driver, tabReferals, 120);
        clickElementBy(chkRefReqd);
        clickElementBy(chkRefRqdTour);
        clickElementBy(btnSave);

 

    }

	protected By txtEndDT = By.xpath("//label[contains(.,'Effective End Date')]/following::input[@name='End_Date__c']");

	/*
	 * Method-Create Offer- Regular Offer Designed By-JYoti
	 */
	public void createRegularOffer() {
		waitUntilElementVisibleBy(driver, txtOfferType, "ExplicitLowWait");
		String strOfferName = getRandomStringText(4);
		String strData = "AutoRegularOffer_" + strOfferName;
		sendKeysBy(txtOfferName, strData);
		String strOfferDes = getRandomStringText(4);
		String strdesc = "Automation Data_" + strOfferDes;
		sendKeysBy(txtOfferDescription, strOfferDes);
		clickElementBy(drpBrand);
		clickElementBy(drpValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtAccom, 120);
		WebElement wbNoOfNights = driver
				.findElement(By.xpath("(//div[@class='slds-dueling-list__options']//li)[2]//span[@title='"
						+ testData.get("AllowedNumberofNights").trim() + "']"));
		wbNoOfNights.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtNoOfAdults, 120);
		String strAdults = getRandomString(1);
		sendKeysBy(txtNoOfAdults, strAdults);
		waitUntilElementVisibleBy(driver, txtNoOfChild, 120);
		String strChild = getRandomString(1);
		sendKeysBy(txtNoOfChild, strChild);
		clickElementBy(txtEffDate);
		selectSystemDate(lnkToday);
		/*
		 * WebElement wbEndDate = driver.findElement(lnkToday);
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); Actions
		 * actEndDate = new Actions(driver);
		 * actEndDate.moveToElement(wbEndDate).click().build().perform();
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 */
		clickElementBy(txtEndDT);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
	}

	/*
	 * Description: Select Sales Channel in Business Rules Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void validateErrorMsg() {
		/*Assert.assertTrue(verifyObjectDisplayed(errorMsg));
		tcConfig.updateTestReporter("OffersPage", "createOffer", Status.PASS,
				"Message displayed as :- " + getElementText(errorMsg));*/
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(errorMsg)){
			tcConfig.updateTestReporter("OffersPage", "validateErrorMsg", Status.PASS,
					"Error message is dispalyed sucessfully ");
		}else{
			tcConfig.updateTestReporter("OffersPage", "validateErrorMsg", Status.FAIL,
					"Error message is dispalyed NOT ");
		}
	}

	/*
	 * Description: Search for exsitng Offer Records Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void searchOffers() throws Exception {
		waitUntilElementVisibleBy(driver, txtGlobalSearch, "ExplicitMedWait");
		fieldDataEnter_modified(txtGlobalSearch, testData.get("Offer"));
		clickElementJSWithWait(imgClick);
		waitUntilElementVisibleBy(driver, tabDetails, 120);
		if (verifyObjectDisplayed(tabDetails) || verifyObjectDisplayed(tabDetail)) {
			tcConfig.updateTestReporter("OffersPage", "searchOffers", Status.PASS,
					"User navigated to Offer record details page Successfully");
		} else {
			tcConfig.updateTestReporter("OffersPage", "searchOffers", Status.FAIL,
					"Failed when User navigated to Offer record details page");
		}
	}

	/*
	 * Description: Create Extension Restriction in Business Rules Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void createExtensionRestriction() {
		waitUntilElementVisibleBy(driver, tabExtensnRestriction, "ExplicitMedWait");
		clickElementJSWithWait(tabExtensnRestriction);
		verifyPageDisplayed(lnkExtnsnRestn, "Page Title", "OffersPage", "createExtensionRestriction");
		clickElementJSWithWait(btnNewExtnRes);
		waitUntilElementVisibleBy(driver, txtExtnRest, "ExplicitLowWait");
		verifyPageDisplayed(txtExtnRestriction, "Page Title", "OffersPage", "createExtensionRestriction");
		clickElementBy(txtExtnRest);
		waitUntilElementVisibleBy(driver, addExtn, "ExplicitLowWait");
		clickElementBy(addExtn);
		createNewExtensionRecord();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtEffStDate, 120);
		/*clickElementBy(txtEffStDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtEffEndDateExtn);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveExtnRest); */
		
		clickElementBy(txtEffStDate);
		selectSystemDate(extLnkToday);
		clickElementBy(txtEffEndDateExtn);
		selectSystemDate(extLnkToday);
        clickElementBy(btnSaveER);

		waitUntilElementVisibleBy(driver, sucessMsg, 120);
		/*Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("OffersPage", "createExtensionRestriction", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));*/	
		
	}  

	/*
	 * Description: Create Extension Restriction in Business Rules Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void createNewExtensionRecord() {
		waitUntilElementVisibleBy(driver, txtNewExtn, "ExplicitMedWait");
		verifyPageDisplayed(txtNewExtn, "Page Title", "OffersPage", "createNewExtensionRecord");
		String strExtnNm = getRandomStringText(4);
		sendKeysBy(txtExtensionName, strExtnNm);
		clickElementBy(drpExtnLength);
		clickElementBy(drpValueExtnRest);
		String strMinPrice = getRandomString(2);
		sendKeysBy(txtMinExtnPrice, strMinPrice);
		String strMaxPrice = getRandomString(3);
		sendKeysBy(txtMaxExtnPrice, strMaxPrice);
		clickElementBy(txtEffStartDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtEfftEndDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveNwExtn);
		waitUntilElementVisibleBy(driver, sucessMsg, 120);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("OffersPage", "createNewExtensionRecord",
		 * Status.PASS, "Message displayed as :- " +
		 * getElementText(sucessMsg));
		 */

	}

	/*
	 * Method-Click on Extension Record Designed By-JYoti
	 */
	public void getExtensionRecord() {
		waitUntilElementVisibleBy(driver, hypExtn, "ExplicitMedWait");
		extensionRecord = driver.findElement(hypExtn).getText();
	}

	/*
	 * Method-Click on newly created record to navigate to details page Designed
	 * By-JYoti
	 */
	public void clickNewRecord() {
		//clickElementJSWithWait(lnkClickRecord);
		/*Assert.assertTrue(verifyObjectDisplayed(tabDetailsExtnRest));
		tcConfig.updateTestReporter("OffersPage", "clickNewRecord", Status.PASS,
				"User is navigate to record details page");*/
		clickElementJSWithWait(lnkExtensionRestriction);
		verifyPageDisplayed(txtExtnRestn, "Page Title", "OffersPage", "clickNewRecord");
		getListElement(lstExtnRest);
		verifyPageDisplayed(tabDetailsExtnRest, "Page Title", "OffersPage", "clickNewRecord");
	}
	
	
	/*
	 * Description: Validate master sales channel is not present and displayed
	 * in Add offers sales channel Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void validateMSCnotPresent() {
		waitUntilElementVisibleBy(driver, lnkSalesChannel, "ExplicitLongWait");
		clickElementJSWithWait(lnkSalesChannel);
		verifyPageDisplayed(lnkSalesChannel, "Page Title", "OffersPage", "validateMSCnotPresent");
		clickElementJSWithWait(btnAddSalesChannel);
		verifyPageDisplayed(txtAddOffrSlsChnnl, "Page Title", "OffersPage", "validateMSCnotPresent");
		fieldDataEnter_modified(txtSearchSlsChnnl, testData.get("MasterSalesChannel"));
		if (driver.findElement(tblBlank).getAttribute("nodeName").contains("TBODY")) {
			tcConfig.updateTestReporter("OffersPage", "validateMSCnotPresent", Status.PASS,
					"Master Sales Channel record is not present in Offer Sales Channel reocrds");
		} else {
			tcConfig.updateTestReporter("OffersPage", "validateMSCnotPresent", Status.FAIL,
					"Failed while validating Master Sales Channel record is present in Offer Sales Channel");
		}
	}
	

	/*
	 * Description: Validate Car minimum and maximaum Price are auto poplated
	 * from Sales channel page Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void createOfferSalesChannel(String slsChnnl) throws AWTException {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickNew_RegularOffer();
		createRegularOffer();
		waitUntilElementVisibleBy(driver, lnkSalesChannel, "ExplicitMedWait");
		addSalesChannelToOffer();
		
	}

	/*
	 * Description: Create Bring a Friend Offer Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void createBAFOffer() throws AWTException {
		waitUntilElementVisibleBy(driver, txtOfferType, "ExplicitMedWait");
		String strOfferName = getRandomStringText(4);
		String strData = "AutoBAFOffer "+strOfferName;
		sendKeysBy(txtOfferName, strData);
		String strOfferDes = getRandomStringText(4);
		sendKeysBy(txtOfferDescription, strOfferDes);
		clickElementBy(drpBrand);
		clickElementBy(drpValue);		
		WebElement wbNoOfNights = driver.findElement(selectNo);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		wbNoOfNights.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(0).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String strAdults = getRandomString(1);
		sendKeysBy(txtNoOfAdults, strAdults);
		String strChild = getRandomString(1);
		sendKeysBy(txtNoOfChild, strChild); 
		clickElementBy(txtEffStartDT);
		selectSystemDate(lnkToday);
		clickElementBy(txtEffEndDT);
		selectSystemDate(lnkToday);
		{
			Robot rob = new Robot();
			rob.mouseWheel(4);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		while (!driver.findElement(tabReferals).isDisplayed());
		waitUntilElementVisibleBy(driver, tabReferals, 120);
		clickElementBy(chkRefReqd);
		clickElementBy(chkRefRqdTour);		
		clickElementBy(btnSave);
		/*Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("OffersPage", "createBAFOffer", Status.PASS,
				"User is navigate to record details page" + getElementText(sucessMsg));*/
		verifyPageDisplayed(sucessMsg, "Page Title", "OffersPage", "createBAFOffer");
	}

	/*
	 * Description: validate the newly created offer status Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void validateOfferStatus() {
		waitUntilElementVisibleBy(driver, tabDraft, "ExplicitMedWait");
		WebElement wbDraft = driver.findElement(tabDraft);
		if (wbDraft.getCssValue("background-color").contains("rgba(0, 0, 0, 0)")) {
			tcConfig.updateTestReporter("OffersPage", "validateOfferStatus", Status.PASS,
					"User is navigate to record details page");
		} else {
			tcConfig.updateTestReporter("OffersPage", "validateOfferStatus", Status.FAIL,
					"User is navigate to record details page");
		}
	}

	/*
	 * Method-Global Search for an existing Offer Record Designed By-JYoti
	 */public void createExtensionRestrictionCSU() {
		waitUntilElementVisibleBy(driver, tabExtensnRestriction, 120);
		clickElementBy(tabExtensnRestriction);
		verifyPageDisplayed(tabExtensnRestriction, "Page Title", "OffersPage", "createExtensionRestrictionCSU");
		clickElementJSWithWait(btnNewExtnRes);
		verifyPageDisplayed(tabExtensnRestriction, "Page Title", "OffersPage", "createExtensionRestrictionCSU");
		waitUntilElementVisibleBy(driver, txtExtnRest, 120);
		clickElementBy(txtExtnRest);
		clickElementBy(addExtn);
		createNewExtensionRecordCSU();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtEffStartDate, 120);
		clickElementBy(txtEffStDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtEffEndDateExtn);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveER);
		/*Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		String strmsg = driver.findElement(sucessMsg).getText();
		tcConfig.updateTestReporter("OffersPage", "createExtensionRestriction", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));*/
		verifyPageDisplayed(sucessMsg, "Page Title", "OffersPage", "createExtensionRestrictionCSU");
	}

	/*
	 * Method-Create a new Extension record Designed By-JYoti
	 */
	public void createNewExtensionRecordCSU() {
		waitUntilElementVisibleBy(driver, txtNewExtn, "ExplicitMedWait");
		verifyPageDisplayed(txtNewExtn, "Page Title", "OffersPage", "createNewExtensionRecordCSU");
		String strExtnNm = getRandomStringText(4);
		sendKeysBy(txtExtensionName, strExtnNm);
		clickElementBy(drpExtnLength);
		clickElementBy(drpValueExtnRest);
		String strMinPrice = getRandomString(2);
		sendKeysBy(txtMinExtnPrice, strMinPrice);
		String strMaxPrice = getRandomString(3);
		sendKeysBy(txtMaxExtnPrice, strMaxPrice);
		clickElementBy(txtEffStartDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtExtnEndDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnERSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("OffersPage", "createNewExtensionRecord",
		 * Status.PASS, "Message displayed as :- " +
		 * getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "OffersPage", "createNewExtensionRecordCSU");

	}

	protected  By btnCancel = By.xpath("//button[@title='Cancel']");
	protected  By txtExtnEndDate = By.xpath("(//span[contains(.,'Effective End Date')]/following::input[@class=' input'])[3]");
			
	/*
	 * Description: Validate Master SaleChannel extension restriction in Offer
	 * page Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void assignMSCExtensionRestriction() {
		waitUntilElementVisibleBy(driver, tabExtensnRestriction, 120);
		clickElementBy(tabExtensnRestriction);
		verifyPageDisplayed(tabExtensnRestriction, "Page Title", "OffersPage", "assignMSCExtensionRestriction");
		clickElementJSWithWait(btnNewExtnRes);
		verifyPageDisplayed(tabExtensnRestriction, "Page Title", "OffersPage", "assignMSCExtensionRestriction");
		fieldDataEnter_modified(txtSearhExtn, testData.get("Extension"));
		clickElementBy(lstData);
		waitUntilElementVisibleBy(driver, txtSearchSaleschannel1, "ExplicitMedWait");
		fieldDataEnter_modified(txtSearchSaleschannel1, testData.get("MasterSalesChannel"));
		sendKeyboardKeys(Keys.ENTER);
		waitUntilElementVisibleBy(driver, searchMSC1, "ExplicitLongWait");	
		if (verifyObjectDisplayed(searchMSC1)) {
			tcConfig.updateTestReporter("OffersPage", "assignMSCExtensionRestriction", Status.PASS,
					"User cannot view the master sales channel record populating");
		} else {
			tcConfig.updateTestReporter("OffersPage", "assignMSCExtensionRestriction", Status.FAIL,
					"Failed when master sales channel record populating");
		}
		waitUntilElementVisibleBy(driver, btnCancel, "ExplicitMedWait");		
		clickElementJSWithWait(btnCancel);
		/*clickElementBy(txtNwSlsChnnl);
		waitUntilElementVisibleBy(driver, txtNewSlsChannel, "ExplicitMedWait");
		clickElementBy(btnNext);
		createMSUForTeams();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtEffStDate, 120);
		clickElementBy(txtEffStDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveER);
		Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
		tcConfig.updateTestReporter("OffersPage", "assignMSCExtensionRestriction", Status.PASS,
				"Message displayed as :- " + getElementText(errorMsgSCT));
		verifyPageDisplayed(errorMsgSCT, "Page Title", "OffersPage", "assignMSCExtensionRestriction");*/
	}

	/*
	 * Description: Create Master Sales Channel Rule Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void createMSUForTeams() {
		waitUntilElementVisibleBy(driver, txtNewSalesChannel, "ExplicitLowWait");
		verifyPageDisplayed(txtNewSalesChannel, "Page Title", "OffersPage", "createMSUForTeams");
		String strSlsNm = getRandomStringText(4);
		sendKeysBy(txtSalesChnlName, strSlsNm);
		clickElementBy(drpCategory);
		clickElementBy(drpValue);
		String strHrOpt = getRandomString(1);
		sendKeysBy(txtHrsOfOpt, strHrOpt);
		fieldDataEnter_modified(txtAddress, testData.get("Address"));
		clickElementBy(txtEffDtMSC);
		selectSystemDate(lnkToday);
		clickElementJSWithWait(btnSaveMSC);
		verifySuccessDisplayed(sucessMSG, "OffersPage", "createMSUForTeams");
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMSG));
		 * tcConfig.updateTestReporter("OffersPage", "createMSUForTeams",
		 * Status.PASS,
		 * "Master Sales Channel created sucessfully message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
	}
	/*
	 * Record specific elements - SalesChannel protected By offerSalesChannelTab
	 * = By.xpath("//a[text()='Sales Channel']"); protected By
	 * addSalesChannelButton = By.xpath("//button[@title='Add Sales Channel']");
	 * protected By addOfferSalesChTitle = By.xpath(
	 * "//div[header[contains(., ' Add Offer Sales Channel')]]//h2"); protected
	 * By salesChannelSearchBox = By.xpath(
	 * "//input[@placeholder='Search Sales Channel']"); protected By
	 * salesChannelSelectIcon =
	 * By.xpath("//div[@class='slds-checkbox_add-button']"); protected By
	 * guestSelectIcon = By.xpath(
	 * "//span[@title='Guest Type']/following::div[3]/..//select"); protected By
	 * addedSalesChannel = By.xpath(
	 * "//div[@class='windowViewMode-normal oneContent active lafPageHost']//span[@title='Offer Sales Channels']/following::a[1]"
	 * );
	 * 
	 * public void addSalesChannelToOffer() {
	 * 
	 * if (verifyObjectDisplayed(offerSalesChannelTab)) {
	 * clickElementBy(offerSalesChannelTab);
	 * verifyPageTitleDisplayed(addSalesChannelButton,
	 * "Navigated to Offer Sales Channel tab with button:", "OffersPage",
	 * "addSalesChannelToOffer"); clickElementJSWithWait(addSalesChannelButton);
	 * verifyPageTitleDisplayed(addOfferSalesChTitle,
	 * "Navigated to Page with Title", "OffersPage", "addSalesChannelToOffer");
	 * fieldEnterData(salesChannelSearchBox, OffersPage.slaeschannel); //
	 * fieldEnterData(salesChannelSearchBox, "UZTC");
	 * clickElementJSWithWait(salesChannelSelectIcon);
	 * selectByIndex(guestSelectIcon, 0); //
	 * selectElement("OffersPage",guestSelectIcon, "Guest Type drop // down");
	 * //chooseCalDate(effEndDate, nextMonth, endDate);
	 * tcConfig.updateTestReporter("OffersPage", "addSalesChannelToOffer",
	 * Status.PASS, "Able to enter data in the Offer Sales Channel form"); }
	 * else { tcConfig.updateTestReporter("OffersPage",
	 * "addSalesChannelToOffer", Status.FAIL,
	 * "Unable to enter data in the Offer Sales Channel form"); }
	 * clickElementJSWithWait(btnAdd);
	 * verifyPageTitleDisplayed(toastSuccessMessage,
	 * "Offer Sales Channel added successfully", "OffersPage",
	 * "addSalesChannelToOffer"); clickElementBy(offerSalesChannelTab);
	 * verifyPageTitleDisplayed(addedSalesChannel,
	 * "Newly added Offer Sales Channel displayed", "OffersPage",
	 * "addSalesChannelToOffer");
	 * 
	 * 
	 * waitUntilElementVisibleBy(driver, lnkSalesChannel, "ExplicitMedWait");
	 * clickElementBy(lnkSalesChannel); waitUntilElementVisibleBy(driver,
	 * hypOfferSlsChhnl, "ExplicitMedWait");
	 * clickElementJSWithWait(hypOfferSlsChhnl);
	 * waitUntilElementVisibleBy(driver, offrSlsDetailsTab, 120); if
	 * (verifyObjectDisplayed(txtCarMinPrice) &&
	 * verifyObjectDisplayed(txtCarMinPrice)) {
	 * tcConfig.updateTestReporter("OffersPage", "createOfferSalesChannel",
	 * Status.PASS, "Car Minimum price displayed as:- " +
	 * getElementText(txtCarMinPrice) +
	 * " and Car Maximum Price is displayed as:- " +
	 * getElementText(txtCarMinPrice)); } else {
	 * tcConfig.updateTestReporter("OffersPage", "createOfferSalesChannel",
	 * Status.FAIL,
	 * "Car Minimum and Maximum values present are wrongly displayed ");
	 * 
	 * } }
	 */

	/*
	 * Method: chooseOfferType Description: Select the offer type for which new
	 * Offer will be created Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void chooseOfferType() {
		clickElementJSWithWait(OffersTab);
		clickElementBy(newButton);
		verifyPageTitleDisplayed(recordTypePageTitle, "Navigated to Choose Offer Record page with Title", "OffersPage",
				"chooseOfferType");
	}

	/*
	 * Method: chooseRegularOffer Description: Choose Regular Offer Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseRegularOffer() {
		clickElementBy(regularOfferRadioBtn);
		clickElementBy(nextButton);
		verifyPageTitleDisplayed(regularOfferPageTitle, "Navigated to Regular Offer page with Title", "OffersPage",
				"chooseRegularOffer");
	}

	/*
	 * Method: chooseBAFOffer Description: Choose Bring a Friend Offer Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseBAFOffer() {
		clickElementBy(bringFriendOfferRadioBtn);
		clickElementBy(nextButton);
		verifyPageTitleDisplayed(bringAFriendOfferPageTitle, "Navigated to Bring a Friend Offer page with Title",
				"OffersPage", "chooseBAFOffer");
	}

	/*
	 * Method: chooseFTFOffer Description: Choose Face to Face Offer Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseFTFOffer() {
		clickElementBy(face2faceOfferRadioBtn);
		clickElementBy(nextButton);
		verifyPageTitleDisplayed(faceToFaceOfferPageTitle, "Navigated to Face To Face Offer page with Title",
				"OffersPage", "chooseFTFOffer");
	}

	/*
	 * Method: chooseSplEventOffer Description: Choose Special Event Offer Val
	 * Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseSplEventOffer() {
		clickElementBy(splEventOfferRadioBtn);
		clickElementBy(nextButton);
		verifyPageTitleDisplayed(specialEventOfferPageTitle, "Navigated to Special Event Offer page with Title",
				"OffersPage", "chooseSplEventOffer");
	}

	/*
	 * Method: getOfferNameRandom Description: Get Offer name Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void getOfferNameRandom() {
		offerName = "Automated Offer " + getRandomStringText(5);
		fieldEnterData(offerNameText, offerName);
	}

	/*
	 * Method: getOfferNameDataSheet Description: Get Offer name sheet Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void getOfferNameDataSheet() {
		fieldEnterData(offerNameText, testData.get("Offer"));
	}

	/*
	 * Method: enterOfferData Description: Enter Offer Data Val Date: July/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void enterOfferData() {
		if (verifyObjectDisplayed(offerDescriptionText)) {
			fieldEnterData(offerDescriptionText, "Offer description text " + getRandomStringText(7));
			clickElementJSWithWait(brandField);
			clickElementBy(brandDropdwn);
			tcConfig.updateTestReporter("OffersPage", "enterOfferData", Status.PASS,
					"Offer name and Brand details entered");
			clickElementJSWithWait(expMonthsDropDwn);
			clickElementJSWithWait(addMonthsSelectButton);
			clickElementJSWithWait(noOfNightsDropDwn);
			clickElementJSWithWait(addNightSelectButton);
			String adultNo = getRandomString(1);
			String childNo = getRandomString(1);
			fieldEnterData(noOfAdultsText, adultNo );
			fieldEnterData(noOfChildText, childNo);
			scrollDownForElementJSBy(effStartDateCal);
			clickElementJSWithWait(effStartDateCal);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("OffersPage", "enterOfferData", Status.PASS,
					"Able to enter data in the Offer creation page");
		} else {
			tcConfig.updateTestReporter("OffersPage", "enterOfferData", Status.FAIL,
					"Unable to enter data in the Offer creation page");
		}
	}

	/*
	 * Method: selectBringAFriendCheckBox Description: Select referral
	 * checkboxes specific to BAF offer Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void selectReferralCheckBoxes() {
		if (verifyObjectDisplayed(referralTitle)) {
			clickElementBy(referralSection);
			selectCheckBox(referralReqCheckBox);
			selectCheckBox(referralReqToTourCheckBox);
			tcConfig.updateTestReporter("OffersPage", "selectReferralCheckBoxes", Status.PASS,
					"Able to select Referral required Checkboxes");
		} else {
			tcConfig.updateTestReporter("OffersPage", "selectReferralCheckBoxes", Status.FAIL,
					"Unable to select Referral required Checkboxes");
		}
	}

	/*
	 * Method: saveOffer Description: Save Offer and verify the success message
	 * Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void saveOffer() {
		if (verifyObjectDisplayed(saveButton)) {
			clickElementBy(saveButton);
			verifyPageTitleDisplayed(successMessageTitle, "Offer created Successfully", "OffersPage", "saveOffer");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("OffersPage", "saveOffer", Status.FAIL, "Unable to create Offer");
		}
	}

	/*
	 * Method: enterMultipleExpMonth Description: Select Multiple Expiration
	 * Months while creating offers Data Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void enterMultipleExpMonth() {
		if (verifyObjectDisplayed(expMonthsDropDwn)) {
			clickElementJSWithWait(expMonthsDropDwn);
			clickElementJSWithWait(addMonthsSelectButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(expMonthsDropDwn);
			clickElementJSWithWait(addMonthsSelectButton);
			tcConfig.updateTestReporter("OffersPage", "enterMultipleExpMonth", Status.PASS,
					"Able to select Multiple Expiration Months");
		} else {
			tcConfig.updateTestReporter("OffersPage", "enterMultipleExpMonth", Status.FAIL,
					"Unable to select Multiple Expiration Months");
		}

	}

	/*
	 * Method: verifyMandatoryDetails Description: Verify error while saving
	 * without mandatory fields Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void verifyMandatoryFieldDetails() {
		clickElementBy(saveButton);
		verifyPageTitleDisplayed(saveSnagError,
				"Clicked on Save without entering details for Mandatory fields. Validation successful with the message:",
				"OffersPage", "verifyMandatoryDetails");
		clickElementBy(closeErrorDialogButton);
	}

	/*
	 * Method: addDestinationToOffer Description: Add destination to offer Val
	 * Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void addDestinationToOffer() {
	//	verifyProgressStatus(draftTabActive, "OffersPage", "addDestinationToOffer");
		markStatusAsComplete(inProgressTabActive, "OffersPage", "addDestinationToOffer");
		if (verifyObjectDisplayed(offerDestinationTab)) {
			clickElementJSWithWait(offerDestinationTab);
			verifyPageTitleDisplayed(addDestinationButton, "Navigated to Destination tab with button", "OffersPage",
					"addDestinationToOffer");
			clickElementJSWithWait(addDestinationButton);
			verifyPageTitleDisplayed(addOfferDestTitle, "Navigated to Page with Title", "OffersPage",
					"addDestinationToOffer");
			fieldEnterData(destinationSearchBox, testData.get("Destination"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(destinationFirstItem);
			chooseCalDate(effEndDate, nextMonth, endDate);
			tcConfig.updateTestReporter("OffersPage", "addDestinationToOffer", Status.PASS,
					"Able to enter data in the Offer Destination form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "addDestinationToOffer", Status.FAIL,
					"Unable to enter data in the Offer Destination form");
		}
		clickElementJSWithWait(btnAdd);
		verifyPageTitleDisplayed(toastSuccessMessage, "Destination added successfully", "OffersPage",
				"addDestinationToOffer");
		clickElementBy(offerDestinationTab);
		scrollDownForElementJSBy(offerDestinationLink);
		clickElementBy(offerDestinationLink);
		waitUntilElementInVisible(driver, spinnerGrid, 120);
		String expectedDest = testData.get("Destination");
		String actualDest = getElementText(addedDestination);
		// Assert.assertTrue(verifyObjectDisplayed(addedDestination));
		Assert.assertEquals(actualDest, expectedDest);
		tcConfig.updateTestReporter("OffersPage", "addDestinationToOffer", Status.PASS,
				"Newly added Destination displayed: " + actualDest);
		clickElementBy(offerBreadcrumbLink);
	}

	/*
	 * Method: addDestinationOverlapDates Description: Add destination with
	 * Overlapping dates Date: July/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void addDestinationOverlapDates() {
		// addDestinationToOffer();
		/*
		 * clickElementJSWithWait(OffersTab); // clickElementBy(offerNameLink);
		 * clickElementBy(offerToTest);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 */
		if (verifyObjectDisplayed(offerDestinationTab)) {
			clickElementBy(offerDestinationTab);
			verifyPageTitleDisplayed(addDestinationButton, "Navigated to Destination tab with button", "OffersPage",
					"addDestinationToOffer");
			clickElementJSWithWait(addDestinationButton);
			verifyPageTitleDisplayed(addOfferDestTitle, "Navigated to Page with Title", "OffersPage",
					"addDestinationToOffer");
			fieldEnterData(destinationSearchBox, testData.get("Destination"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(destinationFirstItem);
			clickElementJSWithWait(destStartDateCal);
			clickElementBy(dateLink);
			// chooseEndDate2();
			chooseCalDate(effEndDate, nextMonth, endDate2);
			tcConfig.updateTestReporter("OffersPage", "addDestinationOverlapDates", Status.PASS,
					"Entered overlapping dates in the Offer Destination form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "addDestinationOverlapDates", Status.FAIL,
					"Error while entering data in the Offer Destination form");
		}
		clickElementJSWithWait(btnAdd);
		verifyPageTitleDisplayed(toastErrorMessage,
				"Error message displayed - Unable to add destination with overlapping dates.", "OffersPage",
				"addDestinationOverlapDates");
	}

	/*
	 * Method: assignAccommodationToOffer Description: Assign Offer
	 * Accommodation Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void assignAccommodationToOffer() {
		if (verifyObjectDisplayed(offerAccommodationTab)) {
			clickElementBy(offerAccommodationTab);
			verifyPageTitleDisplayed(addOfferAccommodation, "Navigated to Offer Accommodation tab with button:",
					"OffersPage", "assignAccommodationToOffer");
			clickElementJSWithWait(addOfferAccommodation);
			verifyPageTitleDisplayed(newOfferAccomTitle, "Navigated to Page with Title", "OffersPage",
					"assignAccommodationToOffer");
			clickElementBy(accommodationLookUpBox);
			accomValue1 = getElementAttribute(comboListItem1, "title");
			clickElementBy(comboListItem1);
			clickElementJSWithWait(effStartDateCal);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("OffersPage", "assignAccommodationToOffer", Status.PASS,
					"Able to enter data in the Offer Accommodation form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "assignAccommodationToOffer", Status.FAIL,
					"Unable to enter data in the Offer Accommodation form");
		}

		verifySave("OffersPage", "assignAccommodationToOffer", saveButton, toastSuccessMessage, saveSnagError,
				cancelButton);
		navigateToOfferAssignment(offerAccommodationTab, offerAccommodationLink, spinnerGrid);
		By displayedAccommodatiomnText = By.xpath("//a[@title='" + accomValue1 + "']");
		String displayedAccommodation = getElementAttribute(displayedAccommodatiomnText, "title");
		verifyAddedOfferAssignments(displayedAccommodation, "Offer Accommodation", accomValue1);
		clickElementBy(offerBreadcrumbLink);
	}

	/*
	 * Method: assignMultiAccommToOffer Description: Assign multiple Offer
	 * Accommodation Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void assignMultiAccommToOffer() {
		if (verifyObjectDisplayed(addOfferAccommodation)) {
			clickElementJSWithWait(addOfferAccommodation);
			verifyPageTitleDisplayed(newOfferAccomTitle, "Navigated to again to Page with Title", "OffersPage",
					"assignAccommodationToOffer");
			clickElementBy(accommodationLookUpBox);
			accomValue2 = getElementAttribute(comboListItem2, "title");
			clickElementBy(comboListItem2);
			clickElementJSWithWait(effStartDateCal);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("OffersPage", "assignAccommodationToOffer", Status.PASS,
					"Able to enter data in the Offer Accommodation form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "assignAccommodationToOffer", Status.FAIL,
					"Unable to enter data in the Offer Accommodation form");
		}

		verifySave("OffersPage", "assignAccommodationToOffer", saveButton, toastSuccessMessage, saveSnagError,
				cancelButton);
		navigateToOfferAssignment(offerAccommodationTab, offerAccommodationLink, spinnerGrid);
		By displayedAccommodatiomnText = By.xpath("//a[@title='" + accomValue2 + "']");
		String displayedAccommodation = getElementAttribute(displayedAccommodatiomnText, "title");
		verifyAddedOfferAssignments(displayedAccommodation, "Offer Accommodation", accomValue2);
		clickElementBy(offerBreadcrumbLink);
	}

	/*
	 * Method: assignPremiumToOffer Description: Assign Offer Premium
	 * Accommodation Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void assignPremiumToOffer() {
		if (verifyObjectDisplayed(offerPremiumTab)) {
			clickElementBy(offerPremiumTab);
			verifyPageTitleDisplayed(addOfferPremium, "Navigated to Offer Premium tab with button:", "OffersPage",
					"assignPremiumToOffer");
			clickElementJSWithWait(addOfferPremium);
			verifyPageTitleDisplayed(newOfferPremiumTitle, "Navigated to Page with Title", "OffersPage",
					"assignPremiumToOffer");
			clickElementBy(premiumLookUpBox);
			waitUntilElementVisibleBy(driver, premiumSelectItem2, "ExplicitWaitLongerTime");
			premiumValue1 = getElementText(premiumSelectItem1);
			clickElementBy(premiumSelectItem1);
			clickElementJSWithWait(effStartDateCal);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("OffersPage", "assignPremiumToOffer", Status.PASS,
					"Able to enter data in the Offer Premium form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "assignPremiumToOffer", Status.FAIL,
					"Unable to enter data in the Offer Premium form");
		}
		/*
		 * clickElementJSWithWait(saveButton);
		 * verifyPageTitleDisplayed(toastSuccessMessage,
		 * "Offer Premium added successfully", "OffersPage",
		 * "assignPremiumToOffer");
		 */
		verifySave("OffersPage", "assignPremiumToOffer", saveButton, toastSuccessMessage, saveSnagError, cancelButton);
		navigateToOfferAssignment(offerPremiumTab, offerPremiumsAssignedLink, spinnerGrid);
		By displayedPremiumtxt = By.xpath("//a[@title='" + premiumValue1 + "']");
		String displayedPremium = getElementText(displayedPremiumtxt);
		verifyAddedOfferAssignments(displayedPremium, "Premium", premiumValue1);
		clickElementBy(offerBreadcrumbLink);
	}

	/*
	 * Method: assignMultiPremiumToOffer Description: Assign multiple Offer
	 * Premium Accommodation Date: July/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void assignMultiPremiumToOffer() {
		if (verifyObjectDisplayed(offerPremiumTab)) {
			clickElementBy(offerPremiumTab);
			verifyPageTitleDisplayed(addOfferPremium, "Navigated to Offer Premium tab with button:", "OffersPage",
					"assignPremiumToOffer");
			clickElementJSWithWait(addOfferPremium);
			verifyPageTitleDisplayed(newOfferPremiumTitle, "Navigated to Page with Title", "OffersPage",
					"assignPremiumToOffer");
			clickElementBy(premiumLookUpBox);
			// addOfferPremium();
			waitUntilElementVisibleBy(driver, premiumSelectItem2, "ExplicitWaitLongerTime");
			premiumValue2 = getElementText(premiumSelectItem2);
			clickElementBy(premiumSelectItem2);
			clickElementJSWithWait(effStartDateCal);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("OffersPage", "assignPremiumToOffer", Status.PASS,
					"Able to enter data in the Offer Premium form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "assignPremiumToOffer", Status.FAIL,
					"Unable to enter data in the Offer Premium form");
		}
		clickElementJSWithWait(saveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Another Offer Premium added successfully", "OffersPage",
				"assignMultiPremiumToOffer");
		navigateToOfferAssignment(offerPremiumTab, offerPremiumsAssignedLink, spinnerGrid);
		By displayedPremium2txt = By.xpath("//a[@title='" + premiumValue2 + "']");
		String displayedPremium2 = getElementText(displayedPremium2txt);
		verifyAddedOfferAssignments(displayedPremium2, "Premium", premiumValue2);
	}

	/*
	 * Method: editPremiumToOffer Description: Edit the Premium assigned to the
	 * Offer Premium Accommodation Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void editPremiumToOffer() {
		if (verifyObjectDisplayed(editPremiumlink)) {
			clickElementJSWithWait(editPremiumlink);
			verifyPageTitleDisplayed(OfferPremiumAssignedTitle, "Navigated to Offer Premium assigned Page with Title",
					"OffersPage", "editPremiumToOffer");
			clickElementBy(editButton);
			verifyPageTitleDisplayed(OfferPremiumEditTitle, "Navigated to Edit Offer Premium assigned Page",
					"OffersPage", "editPremiumToOffer");
			/*
			 * chooseCalDate(effStartDate, nextMonth, startDate);
			 * chooseCalDate(effEndDate, nextMonth, endDate);
			 */

			chooseCalDate(effStartDateCal, nextMonthAccNetMax, startDateAccNetMax);
			chooseCalDate(effEndDateCal, nextMonthAccNetMax, endDate2AccNetMax);
			tcConfig.updateTestReporter("OffersPage", "editPremiumToOffer", Status.PASS,
					"Able to edit data in the Offer Premium form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "editPremiumToOffer", Status.FAIL,
					"Unable to edit data in the Offer Premium form");
		}
		clickElementJSWithWait(saveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Offer Premium edited successfully", "OffersPage",
				"editPremiumToOffer");
	}

	/*
	 * Method: assignOfferAttributes Description: Assign Offer Attributes Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void assignOfferAttributes() {
		if (verifyObjectDisplayed(offerAttributeTab)) {
			clickElementBy(offerAttributeTab);
			verifyPageTitleDisplayed(addOfferAttribute, "Navigated to Offer Attributes tab with button:", "OffersPage",
					"assignOfferAttributes");
			clickElementJSWithWait(addOfferAttribute);
			verifyPageTitleDisplayed(newOfferAttributeTitle, "Navigated to Page with Title", "OffersPage",
					"assignOfferAttributes");
			attributeValue1 = "Attrib" + getRandomStringText(7);
			fieldEnterData(attributeName, attributeValue1);
			fieldEnterData(attributeValue, "AttribValue " + getRandomStringText(7));
			fieldEnterData(attributeDesc, "AttribDesc " + getRandomStringText(7));
			tcConfig.updateTestReporter("OffersPage", "assignOfferAttributes", Status.PASS,
					"Able to enter data in the Offer Attributes form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "assignOfferAttributes", Status.FAIL,
					"Unable to enter data in the Offer Attributes form");
		}
		clickElementJSWithWait(saveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Offer Attributes added successfully", "OffersPage",
				"assignOfferAttributes");
		clickElementBy(offerAttributeTab);
		assertExpectedValue(addedAttribute, attributeValue1, "Newly added Offer Attributes displayed:", "OffersPage",
				"assignOfferAttributes");
	}

	/*
	 * Method: setupOfferAttribute Description: Assign Offer Attributes through
	 * Offer Attribute link page Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void setupOfferAttribute() {

		if (verifyObjectDisplayed(OfferAttributeLink)) {
			clickElementJSWithWait(OfferAttributeLink);
			verifyPageTitleDisplayed(offerAttributeTitle, "Navigated to Page with Title", "OffersPage",
					"setupOfferAttribute");
			clickElementJSWithWait(addOfferAttribute);
			verifyPageTitleDisplayed(newOfferAttributeTitle, "Navigated to Page with Title", "OffersPage",
					"setupOfferAttribute");
			verifyPageTitleDisplayed(offerOption, " Offer value is pre-populated", "OffersPage", "setupOfferAttribute");
			attributeValue2 = "Attrib" + getRandomStringText(7);
			fieldEnterData(attributeName, attributeValue2);
			fieldEnterData(attributeValue, "AttribValue " + getRandomStringText(7));
			fieldEnterData(attributeDesc, "AttribDesc " + getRandomStringText(7));
			tcConfig.updateTestReporter("OffersPage", "setupOfferAttribute", Status.PASS,
					"Able to enter data in the Offer Attributes form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "setupOfferAttribute", Status.FAIL,
					"Unable to enter data in the Offer Attributes form");
		}
		clickElementJSWithWait(saveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Offer Attributes added successfully", "OffersPage",
				"setupOfferAttribute");
		assertExpectedValue(addedAttribute2, attributeValue2, "Newly added Offer Attributes displayed:", "OffersPage",
				"setupOfferAttribute");
	}

	/*
	 * Method: markCompleteWithoutAssignments Description: Verify error while
	 * marking offer complete without mandatory assignments Val Date: July/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void markCompleteWithoutAssignments() {
		verifyProgressStatus(draftTabActive, "OffersPage", "markCompleteWithoutAssignments");
		markStatusAsComplete(inProgressTabActive, "OffersPage", "markCompleteWithoutAssignments");
		clickElementJSWithWait(markAsCompleteButton);
		verifyPageTitleDisplayed(toastInfoMandatAssignments,
				"Clicked on Mark Status as Complete button without mandatory assignments. Error displayed with the message:",
				"OffersPage", "markCompleteWithoutAssignments");
	}

	/*
	 * Method: addBusRuleSalesChannel Description: Add Business Rule and Sales
	 * Channel Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void addBusRuleSalesChannel() {
		BusinessRulesPage bRules = new BusinessRulesPage(tcConfig);
		clickElementJSWithWait(businessRulesTab);
		bRules.clickNew();
		bRules.createSalesChannelBusinessRules();
		clickElementJSWithWait(businessRulesTab);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		businessRuleName = getObject(addedBusinessRule).getText();
		salesChannelName = getObject(salesChLinkedToBrule).getText();
		verifyPageTitleDisplayed(addedBusinessRule, "Added business rule", "BusinessRulesPage",
				"createSalesChannelBusinessRules");
		verifyPageTitleDisplayed(salesChLinkedToBrule, "Business Rule linked to Sales Channel:", "BusinessRulesPage",
				"createSalesChannelBusinessRules");
	}

	/*
	 * Method: addSalesChannelToOffer Description: Add Offer Sales Channel Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void addSalesChannelToOffer() {
		if (verifyObjectDisplayed(offerSalesChannelTab)) {
			clickElementBy(offerSalesChannelTab);
			verifyPageTitleDisplayed(addSalesChannelButton, "Navigated to Offer Sales Channel tab with button:",
					"OffersPage", "addSalesChannelToOffer");
			clickElementJSWithWait(addSalesChannelButton);
			verifyPageTitleDisplayed(addOfferSalesChTitle, "Navigated to Page with Title", "OffersPage",
					"addSalesChannelToOffer");
			fieldEnterData(salesChannelSearchBox, testData.get("SalesChannel"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(salesChannelSelectIcon);
			selectByIndex(guestSelectIcon, 2);
			chooseCalDate(effEndDate, nextMonth, endDate);
			tcConfig.updateTestReporter("OffersPage", "addSalesChannelToOffer", Status.PASS,
					"Able to enter data in the Offer Sales Channel form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "addSalesChannelToOffer", Status.FAIL,
					"Unable to enter data in the Offer Sales Channel form");
		}
		clickElementJSWithWait(btnAdd);
		verifyPageTitleDisplayed(toastSuccessMessage, "Offer Sales Channel added successfully", "OffersPage",
				"addSalesChannelToOffer");
		navigateToOfferAssignment(offerSalesChannelTab, offerSalesChannelLink, spinnerGrid);
		verifySavedOfferAssignments(addedSalesChannel, "Sales Channel", "SalesChannel");
		clickElementBy(offerBreadcrumbLink);
	}

	/*
	 * Method: addDisclosureToOffer Description: Add Offer Disclosure Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void addDisclosureToOffer() {
		if (verifyObjectDisplayed(offerDisclosureTab)) {
			clickElementBy(offerDisclosureTab);
			verifyPageTitleDisplayed(addDisclosureNewButton, "Navigated to Offer Disclosure tab with button:",
					"OffersPage", "addDisclosureToOffer");
			clickElementJSWithWait(addDisclosureNewButton);
			verifyPageTitleDisplayed(addDisclosureTitle, "Navigated to Page with Title", "OffersPage",
					"addDisclosureToOffer");
			// fieldEnterData(salesChannelLookup, OffersPage.salesChannelName);
			fieldEnterData(salesChannelLookup, testData.get("SalesChannel"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectElement("OffersPage", salesChannelSelect, "Sales Channel dropdown value");
			clickElementBy(guestField);
			selectElement("OffersPage", guestOption, "Guest dropdown value");
			scrollDownForElementJSBy(saveButton);
			clickElementJSWithWait(effStartDateCal);
			clickElementBy(dateLink);
			fieldEnterData(offerDisclosureText, "Disclosure text " + getRandomStringText(7));
			tcConfig.updateTestReporter("OffersPage", "addDisclosureToOffer", Status.PASS,
					"Able to enter data in the Offer Disclosure form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "addDisclosureToOffer", Status.FAIL,
					"Unable to enter data in the Offer Disclosure form");
		}
		verifySave("OffersPage", "addDisclosureToOffer", saveButton, addedOfferDisclosure, saveSnagError, cancelButton);
		String addedDisclosure = getElementText(addedOfferDisclosure);
		addedDisclosure = addedDisclosure.replace("\"", "");
		clickElementBy(offerDisclosuresLink);
		waitUntilElementInVisible(driver, loadingSpinner, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		By displayedOfferDisclosureText = By.xpath("//a[@title='" + addedDisclosure + "']");
		String displayedOfferDisclosure = getElementAttribute(displayedOfferDisclosureText, "title");
		verifyAddedOfferAssignments(displayedOfferDisclosure, "Offer Disclosure", addedDisclosure);
		clickElementBy(offerBreadcrumbLink);

	}

	/*
	 * Method: markStatusAsComplete Description: Mark Status as Complete in the
	 * Workflow Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	public void markStatusAsComplete(By by, String pageName, String functionName) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(markAsCompleteButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		verifyProgressStatus(by, pageName, functionName);
	}

	/*
	 * Method: markStatusAsComplete Description: Mark Status as Complete without
	 * Legal Registration Program Number in the Workflow Val Date: July/2020
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void markCompleteWithoutLegalRegNo() {
		markStatusAsComplete(reviewTabActive, "OffersPage", "markCompleteWithoutLegalRegNo");
		clickElementJSWithWait(markAsCompleteButton);
		verifyPageTitleDisplayed(toastInfoMandatAssignments,
				"Clicked on Mark Status as Complete buttton without Legal Registration Program Number. Error displayed with the message:",
				"OffersPage", "markCompleteWithoutLegalRegNo");
	}

	/*
	 * Method: enterLegalRegProgramNo Description: Enter Legal Registration
	 * Program Number in the Workflow Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void enterLegalRegProgramNo() {
		/*
		 * clickElementJSWithWait(OffersTab); clickElementBy(offerToTest);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"))
		 */;
		markStatusAsComplete(reviewTabActive, "OffersPage", "enterLegalRegProgramNo");
		if (verifyObjectDisplayed(keyHeaderEditLink)) {
			clickElementJSWithWait(keyHeaderEditLink);
			verifyPageTitleDisplayed(editOfferPage, "Navigated to Edit Offer page", "OffersPage",
					"enterLegalRegProgramNo");
			String legalRegProgNum = getRandomString(5);
			fieldEnterData(legalRegProgNumTextBox, legalRegProgNum);
			sendKeyboardKeys(Keys.ENTER);
			tcConfig.updateTestReporter("OffersPage", "enterLegalRegProgramNo", Status.PASS,
					"Able to enter Legal Registration Program Number");

		} else {
			tcConfig.updateTestReporter("OffersPage", "enterLegalRegProgramNo", Status.FAIL,
					"Unable to enter Legal Registration Program Number");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(editOfferSaveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Legal Registration Program Number entered successfully",
				"OffersPage", "enterLegalRegProgramNo");
		markStatusAsComplete(validateTabActive, "OffersPage", "enterLegalRegProgramNo");
	}

	/*
	 * Method: validateOfferSameUser Description: Validate error while same user
	 * validates the offer Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void validateOfferSameUser() {
		if (verifyObjectDisplayed(isValidatedPencilIcon)) {
			clickElementJSWithWait(isValidatedPencilIcon);
			verifyPageTitleDisplayed(isValidatedCheckBox, "Validate checkbox is displayed", "OffersPage",
					"validateOfferSameUser");
			clickElementJSWithWait(isValidatedCheckBox);
			tcConfig.updateTestReporter("OffersPage", "validateOfferSameUser", Status.PASS,
					"Is Validated? checkbox selected");
		} else {
			tcConfig.updateTestReporter("OffersPage", "validateOfferSameUser", Status.FAIL,
					"Unable to click on Is Validated? checkbox");
		}
		clickElementJSWithWait(saveButtonToValidate);
		verifyPageTitleDisplayed(isValidatedError, "Clicked on Is Validated? checkbox and Error displayed:",
				"OffersPage", "validateOfferSameUser");
		clickElementJSWithWait(cancelButton);

	}

	/*
	 * Method: selectOfferRecentlyViewed Description: Validate error while same
	 * user validates the offer Val Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void selectOfferRecentlyViewed() {
		clickElementJSWithWait(OffersTab);
		if (verifyObjectDisplayed(recentlyViewedLink)) {
			clickElementJSWithWait(recentlyViewedLink);
			clickElementJSWithWait(offersToBeValidatedLink);
			verifyPageTitleDisplayed(offersToBeValidatedTitle, "Navigate to Offers to be Validated page", "OffersPage",
					"selectOfferRecentlyViewed");
		} else {
			tcConfig.updateTestReporter("OffersPage", "selectOfferRecentlyViewed", Status.FAIL,
					"Unable to navigate to Offers to be Validated page");
		}
		fieldEnterData(offerSearchTextBox, offerName);
		sendKeyboardKeys(Keys.ENTER);
		clickElementBy(By.xpath("//a[@title='" + offerName + "']"));
		verifyPageTitleDisplayed(validateTabActive, "Navigated to Offer Details page", "OffersPage",
				"selectOfferRecentlyViewed");
	}

	/*
	 * Method: validateOffer Description: Validate Offer record Val Date:
	 * July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void validateOffer() {
		if (verifyObjectDisplayed(keyHeaderEditLink)) {
			clickElementJSWithWait(keyHeaderEditLink);
			verifyPageTitleDisplayed(editOfferPage, "Navigated to Edit Offer page", "OffersPage", "validateOffer");
			clickElementJSWithWait(effStartDate);
			clickElementBy(dateLink);
			clickElementJSWithWait(isValidatedCheckBox);
			tcConfig.updateTestReporter("OffersPage", "validateOffer", Status.PASS,
					"Date entered and Is Validated? checkbox selected");
		} else {
			tcConfig.updateTestReporter("OffersPage", "validateOffer", Status.FAIL,
					"Unable to select date and Is Validated? checkbox");
		}
		clickElementJSWithWait(editOfferSaveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Offer validated successfully", "OffersPage", "validateOffer");
		markStatusAsComplete(completeTabActive, "OffersPage", "validateOffer");

	}

	/*
	 * Method: saveValidationDetail Description: Mark the record as Validated
	 * and Save Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	public void saveValidationDetail() {
		if (verifyObjectDisplayed(keyHeaderEditLink)) {
			clickElementJSWithWait(keyHeaderEditLink);
			verifyPageTitleDisplayed(editOfferPage, "Navigated to Edit Offer page", "OffersPage", "validateOffer");
			clickElementJSWithWait(effStartDate);
			clickElementBy(dateLink);
			clickElementJSWithWait(isValidatedCheckBox);
			tcConfig.updateTestReporter("OffersPage", "saveValidationDetail", Status.PASS,
					"Date entered and Is Validated? checkbox selected");
		} else {
			tcConfig.updateTestReporter("OffersPage", "saveValidationDetail", Status.FAIL,
					"Unable to select date and Is Validated? checkbox");
		}
		clickElementJSWithWait(editOfferSaveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Saved Validation details", "OffersPage", "saveValidationDetail");
	}

	/*
	 * Method: changeValidatedUser Description: Change the user who Validated
	 * the Offer record Val Date: July/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void changeValidatedUser() {
		waitUntilElementVisibleBy(driver, validatedUserPencilIcon, "ExplicitMedWait");
		if (verifyObjectDisplayed(validatedUserPencilIcon)) {
			clickElementJSWithWait(validatedUserPencilIcon);
			scrollUpByPixel(400);
			verifyPageTitleDisplayed(saveButtonToValidate, "Validated User section displayed in edit mode", "OffersPage",
					"changeValidatedUser");
			clickElementJSWithWait(deleteIcon);
			//UAT2 
		    //  fieldEnterData(validatedUserTextBox, "WVO Corporate Super User");
           //UAT			
			fieldEnterData(validatedUserTextBox, "WVO Marketer");
			clickElementJSWithWait(corpSuperUserValueUAT);
			tcConfig.updateTestReporter("OffersPage", "changeValidatedUser", Status.PASS,
					"Selected Logged in user as Validated user");
		} else {
			tcConfig.updateTestReporter("OffersPage", "changeValidatedUser", Status.FAIL,
					"Unable to select Logged in user as Validated user");
		}
		clickElementJSWithWait(saveButtonToValidate);
		verifyPageTitleDisplayed(isValidatedError,
				"Unable to change the Validated user field value by the logged in user who created the offer:",
				"OffersPage", "changeValidatedUser");
		clickElementJSWithWait(cancelButton);
	}

	/*
	 * Method: clickOSClink Description: Click offer Sales channel link Channel
	 * Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void clickOSClink() {
		if (verifyObjectDisplayed(offerSalesChannelTab)) {
			navigateToOfferAssignment(offerSalesChannelTab, offerSalesChannelLink, spinnerGrid);
			clickElementJSWithWait(OSCLink);
			verifyPageTitleDisplayed(offerSalesChannelDetails,
					"Navigated to Offer Sales Channel details page with title:", "OffersPage",
					"addPriceToSalesChannel");
			clickElementJSWithWait(relatedSectionLink);
			verifyPageTitleDisplayed(offerPriceSection, "Navigated to Related Tab with the section:", "OffersPage",
					"clickOSClink");
		} else {
			tcConfig.updateTestReporter("OffersPage", "addPriceToSalesChannel", Status.FAIL,
					"Offer Sales Channel name link not displayed");
		}
	}

	/*
	 * Method: addPriceToSalesChannel Description: Add Offer price to Sales
	 * Channel Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void addPriceToSalesChannel(String str) {
		if (verifyObjectDisplayed(offerPriceNewButton)) {
			clickElementJSWithWait(offerPriceNewButton);
			verifyPageTitleDisplayed(newOfferPriceTitle, "Navigated to New Offer Price page:", "OffersPage",
					"addPriceToSalesChannel");

			if (str.equalsIgnoreCase("Price")) {
				String noNights1 = getRandomString(1);
				String offPrice1 = getRandomString(2);
				clickElementBy(offerPriceTypeField);
				selectElement("OffersPage", typePriceOption, "Price option");
				fieldDataEnter(noOfNights, noNights1);
				fieldDataEnter(offerPrice, offPrice1);
				sendKeyboardKeys(Keys.ENTER);

			} else if (str.equalsIgnoreCase("Points")) {
				String noNights2 = getRandomString(1);
				String offPoints2 = getRandomString(3);
				clickElementBy(offerPriceTypeField);
				selectElement("OffersPage", typePointsOption, "Points option");
				fieldDataEnter(noOfNights, noNights2);
				fieldDataEnter(offerPoints, offPoints2);
				sendKeyboardKeys(Keys.ENTER);

			} else if (str.equalsIgnoreCase("Price & Points")) {
				String noNights3 = getRandomString(1);
				String offPrice3 = getRandomString(2);
				String offPoints3 = getRandomString(3);
				clickElementBy(offerPriceTypeField);
				selectElement("OffersPage", typePricePointsOption, "Price and Points option");
				fieldDataEnter(noOfNights, noNights3);
				fieldDataEnter(offerPrice, offPrice3);
				fieldDataEnter(offerPoints, offPoints3);
				sendKeyboardKeys(Keys.ENTER);
			}

		} else {
			tcConfig.updateTestReporter("OffersPage", "addPriceToSalesChannel", Status.FAIL,
					"Issue while entering price and point details");
		}
		tcConfig.updateTestReporter("OffersPage", "addPriceToSalesChannel", Status.PASS,
				"Able to enter price and point details");
		/*
		 * clickElementJSWithWait(savePriceButton);
		 * verifyPageTitleDisplayed(toastSuccessMessage,
		 * "Offer price assigned successfully", "OffersPage",
		 * "addPriceToSalesChannel");
		 */
		verifySave("OffersPage", "addPriceToSalesChannel", savePriceButton, toastSuccessMessage, saveSnagError,
				cancelButton);
	}

	/*
	 * Method: navigateBackToOffers Description: Navigate back to Offers main
	 * workflow from the Offer price screen Date: July/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */

	public void navigateBackToOffers() {
		if (verifyObjectDisplayed(detailsSectionLink)) {
			clickElementBy(detailsSectionLink);
			clickElementBy(By.xpath("//a[text()='" + offerName + "']"));
			tcConfig.updateTestReporter("OffersPage", "navigateBackToOffers", Status.PASS,
					"Navigated back to Offers Page");
		}
	}

	/*
	 * Description: Create Offer Extension Restriction Date: July/2020 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void createOfferExtensionRestriction() {
		if (verifyObjectDisplayed(offerExtRestrictionTab)) {
			clickElementBy(offerExtRestrictionTab);
			verifyPageTitleDisplayed(addExtRestNewButton, "Navigated to Offer Extension Restriction tab with button:",
					"OffersPage", "createOfferExtensionRestriction");
			clickElementJSWithWait(addExtRestNewButton);
			verifyPageTitleDisplayed(extRestPageTitle, "Navigated to Page with Title", "OffersPage",
					"createOfferExtensionRestriction");
			clickElementBy(searchExtensionBox);
			selectElement("Extension Restriction", comboListItem1, "Extension");
			clickElementJSWithWait(comboListItem);
			scrollDownViaKeyboard(4);
			clickElementBy(effStartDateER);
			clickElementBy(dateLink);
			fieldEnterData(restrictionReasonTextbox, "Automation QA Restriction Reason " + getRandomStringText(7));
			tcConfig.updateTestReporter("OffersPage", "createOfferExtensionRestriction", Status.PASS,
					"Entered values in the fields");
		} else {
			tcConfig.updateTestReporter("OffersPage", "createOfferExtensionRestriction", Status.FAIL,
					"Unable to enter data in the Offer Extension Restriction form");
		}
		verifySave("OffersPage", "createOfferExtensionRestriction", saveExtResButton, successMessageTitle,
				saveSnagError, cancelButton);
	}

	/*
	 * Description: Add Offer Premium Date: August/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void addOfferPremium() {
		if (verifyObjectDisplayed(newProductButton)) {
			clickElementBy(newProductButton);
			verifyPageTitleDisplayed(newProductTitle, "New Product form displayed:", "OffersPage", "addOfferPremium");
			clickElementJSWithWait(tourRadioButton);
			clickElementBy(nextButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			verifyPageTitleDisplayed(newProductTourPremiumTitle, "Navigated to Page with Title", "OffersPage",
					"addOfferPremium");
			String productname1 = getRandomString(5);
			fieldEnterData(productNameTextBox, "Pdt_" + productname1);
			clickElementBy(productFamilyField);
			selectElement("OffersPage", productFamilyValue, "Product Family Dropdown value");
			clickElementBy(premiumEffStartDate);
			clickElementBy(dateLink);
			chooseCalDate(premiumEffEndDate, nextMonth, endDate);
			clickElementBy(productTypeField);
			selectElement("OffersPage", productTypeDropdownValue, "Product Family Dropdown value");
			String retailValue1 = getRandomString(3);
			fieldEnterData(retailValueTextBox, "Pdt" + retailValue1);
			String displayName = getRandomStringText(5);
			fieldEnterData(displayNameTextBox, "Pdt " + displayName);
			String cost = getRandomString(3);
			fieldEnterData(costTextBox, "Pdt" + cost);
			fieldEnterData(marketerCostTextBox, "Pdt" + cost);
			tcConfig.updateTestReporter("OffersPage", "addOfferPremium", Status.PASS,
					"Able to enter data in the New Product: Tour Premium form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "addOfferPremium", Status.FAIL,
					"Unable to enter data in the New Product: Tour Premium form");
		}
		clickElementBy(saveButton2);
		verifyPageTitleDisplayed(successMessageTitle, "Tour Premium added successfully", "OffersPage",
				"addOfferPremium");
	}

	/*
	 * Description: Add Offer Premium Date: August/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void addOfferPremiumCRM() {
		if (verifyObjectDisplayed(newProductButton)) {
			clickElementBy(newProductButton);
			verifyPageTitleDisplayed(newProductTitle, "New Product form displayed:", "OffersPage", "addOfferPremium");
			clickElementJSWithWait(tourRadioButton);
			clickElementBy(nextButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			verifyPageTitleDisplayed(newProductTourPremiumTitle, "Navigated to Page with Title", "OffersPage",
					"addOfferPremium");
			String productname1 = getRandomString(5);
			fieldEnterData(productNameTextBox, "Pdt_" + productname1);
			clickElementBy(crmproductFamilyField);
			selectElement("OffersPage", productFamilyValue, "Product Family Dropdown value");
			clickElementBy(premiumEffStartDate);
			clickElementBy(dateLink);
			chooseCalDate(premiumEffEndDate, nextMonth, endDate);
			clickElementBy(crmproductTypeField);
			selectElement("OffersPage", productTypeDropdownValue, "Product Family Dropdown value");
			String retailValue1 = getRandomString(3);
			fieldEnterData(retailValueTextBox, "Pdt" + retailValue1);
			String displayName = getRandomStringText(5);
			fieldEnterData(displayNameTextBox, "Pdt " + displayName);
			/*
			 * String cost = getRandomString(3); fieldEnterData(costTextBox,
			 * "Pdt" +cost); fieldEnterData(marketerCostTextBox, "Pdt" +cost);
			 */
			tcConfig.updateTestReporter("OffersPage", "addOfferPremium", Status.PASS,
					"Able to enter data in the New Product: Tour Premium form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "addOfferPremium", Status.FAIL,
					"Unable to enter data in the New Product: Tour Premium form");
		}
		clickElementBy(saveButton2);
		verifyPageTitleDisplayed(successMessageTitle, "Tour Premium added successfully", "OffersPage",
				"addOfferPremium");
	}

	/*
	 * Method: assignPremiumToOffer Description: Assign Offer Premium
	 * Accommodation Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void assignPremiumToOfferCRM() {
		/*
		 * clickElementJSWithWait(OffersTab); clickElementBy(offerToTest);
		 */
		if (verifyObjectDisplayed(offerPremiumTab)) {
			clickElementBy(offerPremiumTab);
			verifyPageTitleDisplayed(addOfferPremium, "Navigated to Offer Premium tab with button:", "OffersPage",
					"assignPremiumToOffer");
			clickElementJSWithWait(addOfferPremium);
			verifyPageTitleDisplayed(newOfferPremiumTitle, "Navigated to Page with Title", "OffersPage",
					"assignPremiumToOffer");
			clickElementBy(premiumLookUpBox);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			addOfferPremiumCRM();
			clickElementJSWithWait(effStartDate);
			clickElementBy(dateLink);
			tcConfig.updateTestReporter("OffersPage", "assignPremiumToOffer", Status.PASS,
					"Able to enter data in the Offer Premium form");
		} else {
			tcConfig.updateTestReporter("OffersPage", "assignPremiumToOffer", Status.FAIL,
					"Unable to enter data in the Offer Premium form");
		}
		clickElementJSWithWait(saveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Offer Premium added successfully", "OffersPage",
				"assignPremiumToOffer");
		clickElementBy(offerPremiumTab);
		verifyPageTitleDisplayed(addedPremium, "Newly added Offer Premium displayed", "OffersPage",
				"assignPremiumToOffer");

	}

	/*
	 * Description: Validate Car minimum and maximaum Price are auto poplated
	 * from Sales channel page Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void createOfferSalesChannel() throws AWTException {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickNew_RegularOffer();
		createRegularOffer();
		waitUntilElementVisibleBy(driver, lnkSalesChannel, "ExplicitMedWait");
		validateAddSalesChannelToOffer(BusinessRulesPage.slaeschannel);

	}

	/*
	 * Method: chooseOfferType Description: Add offer slaes channel and validate
	 * car fields Offer will be created Val Date: Author/2020 Author: Jyoti
	 * Changes By: NA
	 */	
	
	public void validateAddSalesChannelToOffer(String slsChnnl) {

		clickElementBy(offerSalesChannelTab);
		verifyPageTitleDisplayed(addSalesChannelButton, "Navigated to Offer Sales Channel tab with button:",
				"OffersPage", "validateAddSalesChannelToOffer");
		clickElementJSWithWait(addSalesChannelButton);
		verifyPageTitleDisplayed(addOfferSalesChTitle, "Navigated to Page with Title", "OffersPage",
				"validateAddSalesChannelToOffer");
		fieldEnterData(salesChannelSearchBox, slsChnnl);
		waitUntilElementVisibleBy(driver, addOfferSalesChTitle, "ExplicitLongWait");
		clickElementJSWithWait(plusIcon);
		selectByIndex(guestSelectIcon, 2);
		waitUntilElementVisibleBy(driver, addOfferSalesChTitle, "ExplicitLongWait");
		if (verifyObjectEnabled(btnAdd)) {
			clickElementJSWithWait(btnAdd);
			verifyPageTitleDisplayed(toastSuccessMessage, "Offer Sales Channel added successfully", "OffersPage",
					"validateAddSalesChannelToOffer");
			refreshPage();
			waitUntilElementVisibleBy(driver, offerSalesChannelTab, "ExplicitLongWait");			
		} else {
			tcConfig.updateTestReporter("OffersPage", "validateAddSalesChannelToOffer", Status.FAIL,
					"Failed while adding Offer Sales Channel record");
		}
		clickElementBy(offerSalesChannelTab);
		verifyPageTitleDisplayed(addedSalesChannel, "Newly added Offer Sales Channel displayed", "OffersPage",
				"validateAddSalesChannelToOffer");
		waitUntilElementVisibleBy(driver, lnkSalesChannel, "ExplicitMedWait");
		clickElementBy(lnkSalesChannel);
		waitUntilElementVisibleBy(driver, hypOfferSlsChhnl, "ExplicitMedWait");
		clickElementJSWithWait(hypOfferSlsChhnl);
		waitUntilElementVisibleBy(driver, offrSlsDetailsTab, 120);
		if (verifyObjectDisplayed(txtCarMinPrice) && verifyObjectDisplayed(txtCarMinPrice)) {
			tcConfig.updateTestReporter("OffersPage", "validateAddSalesChannelToOffer", Status.PASS,
					"Car Minimum price displayed as:- " + getElementText(txtCarMinPrice)
							+ " and Car Maximum Price is displayed as:- " + getElementText(txtCarMinPrice));
		} else {
			tcConfig.updateTestReporter("OffersPage", "validateAddSalesChannelToOffer", Status.FAIL,
					"Car Minimum and Maximum values present are wrongly displayed ");
		}
		}

	    /* Method to Click offers Availablity 
		Written By Shubhendra Dtae:8th Feb 2021*/	
	
	  public void clickOffersAvailablity() throws Exception {

		waitUntilElementVisibleBy(driver, offersAvailibility, "ExplicitMedWait");
		clickElementBy(offersAvailibility);		
		clickElementBy(editBoxEnterSalesChannelName);
		sendKeyboardKeys(Keys.ARROW_DOWN);


	   }
		
       /*Method to Click Next button  On  offers Availablity without Mandatory Fields
		Written By Shubhendra Date:8th Feb 2021*/
		
		public void clickNextOnOffersAvailablity() throws Exception {

			clickElementBy(buttonNextOnSalesChannelSelection);
			waitUntilElementVisibleBy(driver, toastInfoMandatAssignments, "ExplicitMedWait");
			
			
			
		}
		
		/*Method to Click Next button  On  offers Availablity with Mandatory Fields
		Written By Shubhendra Date:8th Feb 2021*/
		
		public void clickNextOnOffersAvailablityWithMandatoryFields() throws Exception {

			clickElementBy(editBoxEnterSalesChannelName);
			String salesChannelname = "PaulMartin_Saleschannel" ;
			sendKeysBy(editBoxEnterSalesChannelName ,salesChannelname);
			clickElementBy(autoSuggestionQASales);
			clickElementBy(dropBoxStateOfRsidience);
			sendKeyboardKeys(Keys.ARROW_DOWN.ENTER);
			clickElementBy(stateCalforniaInDropDown);
			clickElementBy(buttonNextOnSalesChannelSelection);

		}

		/*
		 * Method: selectOfferRecentlyViewed Description: Validate Recently created offer
		 * user validates the offer Val Date: March/2021 Author: Krishnaraj
		 * Shubhendra Changes By: NA
		 */
		public void selectOfferRecentlyCreated() {
			clickElementJSWithWait(OffersTab);
			if (verifyObjectDisplayed(recentlyViewedLink)) {
				clickElementJSWithWait(recentlyViewedLink);
				clickElementJSWithWait(recentlyViewedOfffersInDropdown);
				verifyPageTitleDisplayed(recentlyViewedTitle, "Navigate to Offers to be Validated page", "OffersPage",
						"selectOfferRecentlyCreated");
			} else {
				tcConfig.updateTestReporter("OffersPage", "selectOfferRecentlyCreated", Status.FAIL,
						"Unable to navigate to Offers to be Validated page");
			}
			fieldEnterData(offerSearchTextBox, offerName);
			sendKeyboardKeys(Keys.ENTER);
			clickElementBy(By.xpath("//a[@title='" + offerName + "']"));
			verifyPageTitleDisplayed(offerNameOnOfferDetail, "Navigated to Offer Details page", "OffersPage",
					"selectOfferRecentlyCreated");
		}

}
