package tgs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class HomePage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(HomePage.class);

	public HomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By homeTab = By.xpath("//span[contains(.,'Home')]");
	protected By leadsTab = By.xpath("//span[@class='slds-truncate' and contains(.,'Leads')]");
	@FindBy(xpath = "//button[@class='slds-button slds-show']")
	WebElement menuIcon;
	@FindBy(xpath = "//input[contains(@placeholder,'Search apps or items')]")
	WebElement txtSearchBy;
	protected By globalSearch = By.xpath("//input[@title='Search Salesforce']");

	protected By journeyTabHomePage = By.xpath("//div[contains(@class,'wn-banner_home cTMCMAHomePage')]");
	protected By lnkArrivalList = By.xpath("//img[contains(@src,'Reservation_List')]/..");
	protected By drpDownSalesStore = By.xpath("//input[@name='territoryType']");
	protected By lnkSearchGuest = By.xpath("//img[contains(@src,'All_Guests')]");
	protected By drpDwnTourOrRes = By.xpath("//select[@name='Dropdown']");
	protected By inputSearchTourRes = By.xpath("//input[contains(@placeholder,'Number')]");
	protected By lnkTourDetails = By.xpath("//button[contains(@title,'navigate to tour')]");
	protected By img_SearchInMobile = By.xpath("//img[contains(@src,'Search.svg')]");
	protected By lnkReportMobUi = By.xpath("//img[contains(@src,'Report')]/..");
	protected By errorMsg = By.xpath("//p[text()='If Reports and Dashboards are not present please refresh.']");
	protected By txtBoxSearchReports = By.xpath("//input[contains(@placeholder,'Search recent reports')]");

	protected By btnViewAll = By.xpath("//button[contains(text(),'View All')]");
	protected By noItems = By.xpath("//a[@title='No Items']//span");
	protected By joruneyDesktop = By.xpath("//p[contains(.,'Journey Desktop')]");

	/*
	 * Method-Naviagte to Menu Icon Designed By-JYoti
	 */
	public void navigateToMenu(String Iterator) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, homeTab, 120);
		if (verifyObjectDisplayed(homeTab)) {
			checkLoadingSpinner();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyElementDisplayed(menuIcon)) {
				tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
						"User is navigated to Homepage");
			} else {
				tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
						"User is NOT navigated to Homepage");
			}
			menuIcon.click();
			waitUntilElementVisibleBy(driver, btnViewAll, 120);
			clickElementBy(btnViewAll);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, txtSearchBy, "ExplicitWaitLongerTime");

			txtSearchBy.sendKeys(testData.get("Menu" + Iterator));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(By.xpath("(//mark[text()='" + testData.get("Menu" + Iterator) + "'])")).click();
		} else {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(noItems)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyElementDisplayed(menuIcon)) {
					tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.PASS,
							"User is navigated to Journey Desktop");
				} else {
					tcConfig.updateTestReporter("HomePage", "homePageValidation", Status.FAIL,
							"User is NOT navigated to Journey Desktop");
				}
				menuIcon.click();
				waitUntilElementVisibleBy(driver, joruneyDesktop, 120);
				clickElementBy(joruneyDesktop);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, homeTab, 120);
				menuIcon.click();
				waitUntilElementVisibleBy(driver, btnViewAll, 120);
				clickElementBy(btnViewAll);
				waitUntilElementVisibleBy(driver, txtSearchBy, "ExplicitWaitLongerTime");

				txtSearchBy.sendKeys(testData.get("Menu" + Iterator));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("(//mark[text()='" + testData.get("Menu" + Iterator) + "'])")).click();
			}
		}

	}

}
