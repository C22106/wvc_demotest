package tgs.pages;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OfferTransactionsPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(OfferTransactionsPage.class);

	public OfferTransactionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	/* Jyoti's Xpath */

	public String offrTransactn, offerTransactionNum, strOffPrice1, strOffPrice2, strTotalAmt, strBlanaceDue,
			strCreditAvial, strAmount, noOFNight, oldOTDetails;

	protected By txtOfferTransactionPage = By.xpath(
			"//span[contains(@class,'slds-var-p-right_x-small uiOutputText') and contains(.,'Offer Transactions')]");
	protected By txtSearchOfferTransaction = By.xpath("//input[@title='Search Offer Transactions and more']");
	protected By imgClick = By.xpath("(//img[@class='icon ' and contains(@src,'standard/quotes_120.png')])[1]");
	protected By txtOffrTranDetailPage = By.xpath("//div[@class='entityNameTitle slds-line-height_reset']");
	protected By actionButton = By.xpath("//div[@class='uiPopupTrigger']//lightning-primitive-icon");
	protected By btnTransferButton = By.xpath(
			"//div[contains(@class,'branding-actions actionMenu popupTargetContainer uiPopupTarget ')]//li//a[@title='Transfer Offer']");
	protected By txtTransferOffer = By.xpath("//h2[contains(.,'Transfer Offer')]");
	protected By txtSearchName = By.xpath("//input[@placeholder='Search Name, Member Number, Email, City, Zip...']");
	protected By chkRecord = By.xpath("(//label[@class='slds-checkbox_faux'])[1]");
	protected By tblRecords = By
			.xpath("(//table[@class='slds-table slds-table_bordered slds-table--header-fixed ']//tbody//tr)[1]");
	protected By btnTransfer = By.xpath("//button[contains(.,'Transfer') and @id='addButton']");
	protected By tabActive = By.xpath("//span[@class='title slds-path__title' and contains(.,'Active')]");
	protected By tabPendingPayment = By
			.xpath("//span[@class='title slds-path__title' and contains(.,'Pending Payments')]");
	protected By txtOfferTransactionNum = By.xpath(
			"(//div[contains(@class,'slds-page-header__title slds-m-right--small slds-align')]//span[@class='uiOutputText'])[1]");
	protected By txtOfferTranNew = By.xpath(
			"(//div[contains(@class,'slds-page-header__title slds-m-right--small slds-align')]//span[@class='uiOutputText'])[2]");
	protected By lstSearchHeader = By.xpath("//div[@class='slds-scrollable_y']");
	protected By oldOfferTransaction = By.xpath(
			"//span[contains(.,'Parent Offer Transaction')]/../..//div[@class='slds-form-element__control slds-grid itemBody']//a");
	protected By tabRelated = By.xpath("(//span[contains(.,'Related')])[1]");
	protected By lnkPaymentRecords = By.xpath("//span[@title='Payment Records']");
	protected By tblPayemntRecords = By.xpath(
			"//table[contains(@class,'slds-table forceRecordLayout slds-table--header')]//tbody//th//a[starts-with(.,'P-')]");
	protected By tblPayemntRecordsStatus = By.xpath(
			"(//table[contains(@class,'slds-table forceRecordLayout slds-table--header')]//tbody//*[starts-with(.,'REFUNDED')])[1]");
	protected By txtPaymentRecordsPage = By.xpath("//h1[@title='Payment Records']");
	protected By extnErrorMsg = By.xpath("//span[@class='toastMessage forceActionsText']");
	protected By btnAddExtension = By.xpath(
			"//div[contains(@class,'branding-actions actionMenu popupTargetContainer uiPopupTarget ')]//li//a[@title='Add Extension']");
	protected By hypLeadNm = By
			.xpath("((//span[@class='test-id__field-label' and contains(.,'Lead Name')])[2]/following::div//a)[1]");
	protected By hypCustomerNm = By
			.xpath("((//span[@class='test-id__field-label' and contains(.,'Owner Name')])[2]/following::div//a)[1]");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage forceActionsText']");
	protected By errorMsg = By.xpath("//span[@class='toastMessage forceActionsText']");
	protected By btnSave = By.xpath("//button[@title='Save']");
	protected By txtAddExtension = By.xpath("//h2[contains(.,'Add Extension')]");
	protected By txtCurrExpirationDt = By
			.xpath("//span[contains(.,'current expiration date: ')]//lightning-formatted-date-time");
	protected By txtUpdatedExpDt = By
			.xpath("//span[contains(.,'updated expiration date: ')]//lightning-formatted-date-time");
	protected By chkFilter = By.xpath(
			"(//table[contains(@class,'slds-table slds-table_fixed-layout slds')]//label[@class='slds-checkbox_faux'])[2]");
	protected By txtExtnPrice = By.xpath(
			"(//table[contains(@class,'slds-table slds-table_fixed')]//tbody//input[@class='slds-assistive-text' and @defaultchecked='true']//following::div[@title='Extension Maximum Price'])[1]");
	protected By wynIDPrice = By.xpath("//label[contains(.,'Wyndham Price')]/..//input[@class='slds-input']");
	protected By customerPrice = By.xpath("//label[contains(.,'Customer Price')]/..//input[@class='slds-input']");
	protected By btnOveride = By
			.xpath("//button[contains(@class,'slds-button slds-button_brand') and contains(.,'Override')]");
	protected By txtOverridePage = By.xpath("//h2[contains(.,'Overide ')]");
	protected By txtOverideCode = By.xpath("//label[contains(.,'Override Code')]/../..//input[@class='slds-input']");
	protected By btnSubmit = By.xpath("//button[@title='Submit']");
	protected By hypExtnRecord = By.xpath(
			"(//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')]//tbody//span[contains(.,'Extension Fees')]/../..//a)[1]");
	protected By lnkActiveLineItems = By
			.xpath("//span[@class='slds-truncate slds-m-right--xx-small' and @title='Active Line Items']");
	protected By txtActiveLinePage = By.xpath("//h1[@title='Active Line Items']");
	protected By txtOfferTransactDeailsPage = By.xpath(
			"//div[contains(.,'Offer Transaction Details') and @class='entityNameTitle slds-line-height--reset']");
	protected By lnkOffTrnstn = By.xpath(
			"(//span[contains(.,'Offer Transaction') and @class='test-id__field-label']/../..//div[@class='slds-form-element__control']//a//span)[2]");
	protected By txtDetailsType = By.xpath(
			"(//span[contains(.,'Detail Type') and @class='test-id__field-label'])[2]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By txtCost = By.xpath(
			"(//span[contains(.,'Cost') and @class='test-id__field-label'])/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By txtCustPrice = By.xpath(
			"(//span[contains(.,'Customer Price') and @class='test-id__field-label'])/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By chkActive = By.xpath(
			"(//span[contains(.,'Active') and @class='test-id__field-label']/../..//div[@class='slds-form-element__control']//span[@class='slds-checkbox_faux'])[2]");
	protected By chkOverrideCode = By.xpath(
			"(//span[contains(.,'Override Code Used') and @class='test-id__field-label']/../..//div[@class='slds-form-element__control']//span[@class='slds-checkbox_faux'])[1]");
	protected By hypSalesChannel = By.xpath(
			"(//span[contains(.,'Sales Channel') and @class='test-id__field-label'])[1]/../..//div[@class='slds-form-element__control']//a//span");
	protected By hypSalesTeam = By.xpath(
			"(//span[contains(.,'Sales Team') and @class='test-id__field-label'])[1]/../..//div[@class='slds-form-element__control']//a//span");
	protected By supervisor = By.xpath(
			"(//span[contains(.,'Supervisor') and @class='test-id__field-label'])[1]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By manager = By.xpath(
			"(//span[contains(.,'Manager') and @class='test-id__field-label'])[1]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By tabRelateds = By.xpath("(//span[@class='title' and contains(.,'Related')])[1]");
	protected By tabOfferInfo = By.xpath("//span[contains(.,'Offer Information')]");
	protected By txtOffExpDt = By.xpath(
			"(//span[contains(.,'Offer Expiration Date') and @class='test-id__field-label']/../..//div[contains(@class,'slds-form-element')]//span[@class='uiOutputDate'])[2]");
	protected By txtExtnLength = By.xpath(
			"(//span[contains(.,'Extension Length') and @class='test-id__field-label']/../..//div[contains(@class,'slds-form-element')]//span[@class='uiOutputNumber'])[2]");
	protected By balanceDue = By.xpath(
			"(//span[@title='Balance Due']/../..//div[@class='slds-form-element__control']//span[@class='forceOutputCurrency'])[2]");
	protected By txtVerifierAgent = By.xpath(
			"//label[@class='slds-form-element__label']/../..//input[contains(@placeholder,'Search Offers Agents...')]");
	protected By imgIcon = By
			.xpath("(//span[@class='slds-media__figure slds-listbox__option-icon']//lightning-icon)[2]");
	protected By txtVerifiedDT = By.xpath("//span[contains(.,'Verified Date')]/../../..//input[@name='Verified_Date']");
	protected By txtCallNum1 = By
			.xpath("//span[contains(.,'Call Recording Number 1')]/../../..//input[@name='Call_Recording_Number_1']");
	protected By txtCallNum2 = By
			.xpath("//span[contains(.,'Call Recording Number 2')]/../../..//input[@name='Call_Recording_Number_2']");
	protected By lnkToday = By.xpath(
			"//button[contains(text(),'Today') and contains(@class,'slds-button slds-align_absolute-center slds')]");
	protected By txtOfferTransactionNum1 = By.xpath(
			"//div[contains(.,'Offer Transaction')]/../..//div[contains(@class,'slds-page-header__title slds-m-right')]//span");
	protected By hypPaymenIDNew = By.xpath(
			"((//table[contains(@class,'slds-table forceRecordLayout slds-table')])[2]//tbody//a[starts-with(@title,'P-')])[2]");
	protected By txtPaymentIdPage = By.xpath(
			"//div[contains(.,'Payment Record') and contains(@class,'entityNameTitle slds')]/../..//lightning-formatted-text");
	protected By txtDecision = By.xpath(
			"//p[@class='slds-text-title slds-truncate' and contains(.,'Decision')]/..//lightning-formatted-text");
	protected By hypOldPaymentID = By.xpath(
			"((//span[contains(.,'Original Payment') and @class='test-id__field-label'])/following::span[starts-with(.,'P-')])[2]");
	protected By hypClickOldOT = By.xpath(
			"((//p[contains(.,'Offer Transaction') and contains(@class,'slds-text-title slds-truncate')])[2]/following::span)[1]");
	protected By txtBalanceDue = By
			.xpath("(//span[contains(.,'Balance Due')]/following::div//span[@class='forceOutputCurrency'])[1]");
	protected By txtMoneyTransferPg = By.xpath("//span[contains(.,'Money Transfer Flow')]");
	protected By sucessMsgMoneyTransfer = By.xpath("//span[contains(.,'Your Money Transfer was successful. ')]");
	protected By btnFinish = By.xpath("//button[@title='Finish']");
	protected By actionButtonMT = By.xpath(
			"(//a[contains(@class,'slds-grid slds-grid--vertical-align-center slds-')]//lightning-primitive-icon)[2]");
	protected By hypNotes = By.xpath("//span[@title='Notes']");
	protected By txtOldPayemntID = By.xpath(
			"(//div[contains(.,'Payment Record') and contains(@class,'entityNameTitle slds')]/../..//lightning-formatted-text)[2]");
	protected By txtDecisionOld = By.xpath(
			"(//p[@class='slds-text-title slds-truncate' and contains(.,'Decision')]/..//lightning-formatted-text)[2]");
	protected By txtPrevBalnaceDue = By.xpath(
			"((//span[contains(.,'Balance Due') and @class='slds-form-element__label slds-truncate'])[2]/following::span)[1]");
	protected By txtOfferPrice = By.xpath(
			"((//span[contains(.,'Offer Price') and @class='test-id__field-label'])[2]/following::span[@class='forceOutputCurrency'])[1]");
	protected By txtOfferPriceSec = By.xpath(
			"((//span[contains(.,'Offer Price') and @class='test-id__field-label'])[3]/following::span[@class='forceOutputCurrency'])[1]");
	protected By txtCreditAvailiable = By.xpath(
			"((//span[contains(.,'Credit Available') and contains(@class,'slds-form-element__label slds-truncate')])[2]/following::span[@class='forceOutputCurrency'])[1]");
	protected By txtCreditAvailiable1 = By.xpath(
			"(//span[@class='slds-form-element__label slds-truncate' and @title='Credit Available']/following::span[@class='forceOutputCurrency'])[1]");
	protected By btnMoneyTransfer = By.xpath(
			"//div[contains(@class,'branding-actions actionMenu popupTargetContainer uiPopupTarget')]//li//a[@title='Money Transfer']");
	protected By txtMoneyTransferPage = By.xpath("//span[@title='Offer Transaction Number']");
	protected By tblOTNum = By.xpath("//table//tbody//th//div[starts-with(.,'0')]");
	protected By txtRecord = By
			.xpath("(//div[contains(@class,'slds-page-header__title slds-m-right--small slds-align')]//span[@class='"
					+ testData.get("OfferTransactionnum1") + "'])[1]");
	protected By clickOT = By.xpath("//div[@title='" + testData.get("OfferTransactionNumber")
			+ "' and @class='slds-truncate']/../..//label[@class='slds-checkbox_faux']");
	protected By record = By.xpath("//div[@title='" + testData.get("OfferTransactionNumber") + "']");
	protected By ckbSelect = By.xpath(
			"//table[contains(@class,'slds-table slds-table_fixed')]//tbody//label[@class='slds-checkbox_faux']");
	protected By txtOfferPricePage = By.xpath(
			"//div[contains(@class,'flowruntimeRichTextWrapper flowruntimeDisplayText')]//u[contains(.,'Offer Price Selection')]");
	protected By txtDestinationPage = By.xpath(
			"//div[contains(@class,'flowruntimeRichTextWrapper')]//u[contains(.,'Destination Selection')]");
	protected By drpOfferType = By.xpath("//select[@class='slds-select']");
	protected By txtMarketer = By.xpath(
			"(//div/label[text()='Marketer' and @class='slds-form-element__label']/following::li[@class='slds-listbox__item eachItem'])[1]");
	protected By txtSaleschannel = By
			.xpath("(//div/label[text()='Sales Channel']/following::li[@class='slds-listbox__item eachItem'])[1]");
	protected By drpOfferTypes = By.xpath("//div[@class='slds-select_container']");
	protected By teamValue = By
			.xpath("//div//label[text()='Sales Team']/following::li[@class='slds-listbox__item eachItem']//div");
	protected By value = By
			.xpath("//select[@class='slds-select']//option[contains(.,'" + testData.get("OfferType") + "')]");
	protected By valueOffer = By
			.xpath("(//div[@class='slds-media slds-listbox__option_entity']//span[@class='verticalAlign slds-truncate' and contains(.,'"
					+ testData.get("Offer") + "')])[1]");
	protected By txtReceiptPage = By.xpath("//div[@class='review_container group']//h2[contains(.,'Receipt')]");
	protected By btnNext = By.xpath("//button[@title='Next']");
	protected By drpOfferValue = By.xpath(
			"(//div/label[text()='Offer Name' and @class='slds-form-element__label']/following::div//li[@class='slds-listbox__item eachItem'])[1]");
	protected By drpSalesTeam = By.xpath(
			"(//div/label[text()='Sales Team' and @class='slds-form-element__label']/following::div//li[@class='slds-listbox__item eachItem'])[1]");
	protected By drpSalesChannel = By.xpath(
			"(//div/label[text()='Sales Team' and @class='slds-form-element__label']/following::div//li[@class='slds-listbox__item eachItem'])[2]");
	protected By txtBalanceDueValue = By.xpath(
			"(//span[@title='Balance Due']/following::div[@class='slds-form-element__control']//span[@class='forceOutputCurrency'])[1]");
	protected By paymentPage = By.xpath("//div[@class='entityNameTitle slds-line-height--reset']");
	protected By txtAmount = By.xpath("(//p[contains(.,'Amount')]/following::lightning-formatted-text)[1]");
	protected By btnChargeBack = By
			.xpath("//button[contains(.,'Charge Back') and @name='TMPaymentRecord__c.Charge_Back']");
	protected By hypOfferTrnsac = By
			.xpath("(//p[@title='Offer Transaction']/following::div[@class='slds-grid']//span)[1]");

	/* TGS-End To End Scenarios Xpath */

	/* Search Offer Transactions */
	protected By txtSearchResults = By
			.xpath("//h1[@class='slds-nav-vertical__title slds-p-vertical_medium slds-is-relative']");
	protected By tblOfferTransaction = By.xpath(
			"((//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')])[2]//tbody//a[starts-with(.,'0')])[1]");
	protected By txtSearchBox = By.xpath("//div[@class='uiInput uiAutocomplete uiInput--default']//input");

	/* Search By Call Recording Number */
	protected By tabSearchAgent = By.xpath("//span[contains(.,'Sales Channel and Agent Information')]");
	protected By txtCallRecordNum = By
			.xpath("(//span[contains(.,'Call Recording Number 1')]/following::span[@class='uiOutputText'])[1]");

	/* Book Tour */
	protected By tabBookTour = By.xpath("//span[@class='title' and contains(.,'Book Tour')]");
	protected By rdbtnTour = By.xpath("(//span[contains(.,'Book Primary Guest Tour')])[2]");
	protected By rdBtn = By.xpath("//span[@class='slds-radio_faux']");
	protected By btnGetAppointment = By.xpath("//button[contains(.,'Get Appointments')]");
	protected By btnSelectSlot = By
			.xpath("(//button[@class='slds-button_neutral slds-size_1-of-1 panel-button']//div)[1]");
	protected By txtConfirmationMsg = By.xpath("//h2[contains(.,'Confirmation Message')]");
	protected By btnYes = By.xpath("//button[contains(.,'YES')]");
	protected By tourSucessMsg = By.xpath("//h2[contains(.,'Appointment Confirmed')]");
	protected By btnOk = By.xpath("//button[contains(.,'OK')]");
	protected By tabReservationSummary = By.xpath("//span[@class='title' and contains(.,'Reservation Summary')]");
	protected By hyplnkTourID = By.xpath(
			"((//table[contains(@class,'slds-table slds-table_cell-buffer slds')])[3]//div[@title='Cloudhub'])[1]");
	protected By txtSalesStore = By.xpath("(//span[@title='Sales Store']/following::a)[1]");
	protected By txtSaleStoreAppoint = By
			.xpath("(//span[@title='Sales Store Appointment Start Time']/following::span[@class='uiOutputText'])[1]");
	protected By txtStatus = By.xpath("(//span[@title='Status']/following::span[contains(.,'Booked')])[1]");
	protected By tabTourDetails = By.xpath("//span[@class='title' and contains(.,'Tour Details')]");
	protected By pgTimeSlot = By.xpath("//div[@class='header-title']//lightning-formatted-date-time");
	protected By tabReservationBook = By
			.xpath("//span[@class='title slds-path__title' and contains(.,'Reservation Booked')]");
	protected By txtOfferExpirationDt = By
			.xpath("((//span[@title='Offer Expiration Date'])[2]/following::span[@class='uiOutputDate'])[1]");

	/* Book Reservation */
	protected By tabBookReservation = By.xpath("//span[contains(.,'Book Reservation')]");
	protected By calenderDate = By.xpath(
			"//table[@class='ui-datepicker-calendar']//td[@class='ui-datepicker-week-end ui-state-active ui-datepicker-current-day']");
	protected By calenderClick = By.xpath(
			"//table[@class='ui-datepicker-calendar']//td[@class='colorGreenSelectable']");
	protected By pgAccomodation = By.xpath("//span[@title='Accommodation']");
	protected By btnSelectAccom = By.xpath(
			"((//table[contains(@class,'slds-table slds-table_fixed-layout slds')])[1]//label[@class='slds-checkbox_faux'])[1]");
	protected By pgRoomType = By.xpath("//span[@title='Room Type']");
	protected By pgPromoCode = By.xpath("//label[contains(.,'Promo Code')]");
	protected By btnConfirm = By.xpath("//button[@title='Confirm']");
	protected By pgPaymentOptions = By.xpath(
			"//div[@class='flowruntimeRichTextWrapper flowruntimeDisplayText']//b[contains(.,'Payment Options')]");
	protected By rdBtnSelect = By.xpath("(//span[@class='slds-radio--faux'])");
	protected By btnNextPayment = By.xpath("//div[@class='slds-float_right']//button[@title='Next']");
	protected By btnFinishOT = By.xpath("//button[contains(.,'Finish')]");
	protected By tabRelatedOT = By.xpath("(//span[contains(.,'Related')])[2]");
	protected By reserBookedTab = By
			.xpath("(//span[@class='title slds-path__title' and contains(.,'Reservation Booked')])[2]");
	protected By txtIntregrationStatus = By.xpath("((//span[@title='Integration Status'])[2]/following::span)[1]");
	protected By txtNoOfNights = By.xpath(
			"(//span[contains(.,'Number of Nights') and @class='test-id__field-label']/following::span[@class='uiOutputNumber'])[1]");
	protected By dtFrom = By.xpath("(//div[@class='slds-p-around_small']//span)[1]");
	protected By dtTo = By.xpath("(//div[@class='slds-p-around_small']//span)[2]");
	protected By btnPayNext = By.xpath("//button[contains(.,'Next')]");
	protected By txtReceipt = By.xpath("//div[@class='review_container group']//h2[contains(.,'Receipt')]");

	/* Reservation Modification */
	protected By rdBtnOfferModification = By
			.xpath("(//span[contains(.,'Offer Reservation Modification')])[2]/..//span[@class='slds-radio_faux']");
	protected By arrivalDate = By.xpath("//input[@name='arrivalDate']");
	protected By departureDate = By.xpath("//input[@name='depatureDate']");
	protected By destination = By
			.xpath("(//select[@class='slds-select select uiInput uiInputSelect uiInput--default uiInput--select'])[1]");
	protected By intStatus = By
			.xpath("((//span[@title='Integration Status'])/following::span[@class='uiOutputTextArea'])[1]");
	protected By destinationChange = By.xpath(
			"((//table[contains(@class,'forceRecordLayout slds-table slds-no-row-hover slds-table')])[1]//td[contains(.,'Destination Change')]/..//div[contains(@class,'outputLookupContainer')]//a)[1]");
	protected By pgInactiveLineItem = By.xpath("//h1[@title='Inactive Line Items']");
	protected By hypInactiveLineItem = By.xpath("//span[@title='Inactive Line Items']");
	protected By inactiveOTD = By.xpath(
			"(//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')]//span[contains(.,'Offer Reservation')]/../..//..//a[starts-with(.,'OTD-')])[1]");
//	protected By lblSelectAccom = By.xpath(
	//		"((//table[contains(@class,'slds-table slds-table_fixed-layout slds-table')])[2]//label[@class='slds-checkbox_faux'])[1]");
	protected By offerReservationDetail = By.xpath(
			"((//table[contains(@class,'forceRecordLayout slds-table slds-no-row-hover slds-table')])[1]//td[contains(.,'Offer Reservation')])[1]");
	protected By oldOTDNumber = By.xpath(
			"((//table[contains(@class,'forceRecordLayout slds-table slds-no-row-hover slds-table')])[1]//td[contains(.,'Offer Reservation')]/..//div[contains(@class,'outputLookupContainer')]//a)[1]");

	/* Reservation Cancellation */
	protected By reservationSummary = By.xpath("(//span[contains(.,'Reservation Summary') and @class='title'])[1]");
	protected By btnCancelReservation = By
			.xpath("//button[@class='slds-button slds-button_neutral slds-button slds-button_destructive']");
	protected By txtWarning = By.xpath("//h2[@class='slds-text-heading_medium slds-hyphenate']");
	protected By drpCancellationReason = By.xpath("//select[@class='slds-select']");
	protected By activetab = By.xpath("(//a[@title='Active'])[2]");
	protected By balanceDuefield = By
			.xpath("(//span[@title='Balance Due']/following::span[@class='forceOutputCurrency'])[1]");
	protected By creditAvailiable = By
			.xpath("(//span[@title='Credit Available']/following::span[@class='forceOutputCurrency'])[1]");
	protected By cancellationFees = By.xpath(
			"((//table[contains(@class,'forceRecordLayout slds-table slds-no-row-hover slds-table')])[1]//span[contains(.,'Cancellation Fees')])[1]");
	protected By btnProcessRefund = By.xpath("//div[@title='Process Refund']");
	protected By pgRefundOption = By.xpath(
			"//div[@class='flowruntimeRichTextWrapper flowruntimeDisplayText']//b[contains(.,'Refund Options')]");
	protected By pgRefundSucess = By.xpath("//span[contains(.,'Refund was successful. ')]");
	protected By refunded = By.xpath(
			"(//table[contains(@class,'forceRecordLayout slds-table slds-no-row-hover slds-table')])[5]//span[contains(.,'REFUNDED')]");

	/* Krishnaraj Xpath */

	/* Owner record elements */
	public static Map<String, String> objMap = new HashMap<String, String>();
	protected By ownersTab = By.xpath("//a/span[text()='Owners']");
	protected By ownerName = By.xpath("//a[@title='" + testData.get("Owner") + "']");
	protected By searchTextBox = By.xpath("//input[@placeholder='Search this list...']");
	protected By ownersTitle = By.xpath("//li/span[text()='Owners']");
	protected By ownersPageTitle = By.xpath("//h1/div[text()='Owner']");
	protected By oTtab = By.xpath("//a[@title='Offer Transactions']");
	protected By OfferT = By.xpath("//a[@title='00001867']");

	/* Lead record elements */
	protected By leadsTab = By.xpath("//a/span[text()='Leads']");
	protected By leadName = By.xpath("//a[@title='" + testData.get("Owner") + "']");
	protected By leadsTitle = By.xpath("//li/span[text()='Leads']");
	protected By leadsPageTitle = By.xpath("//h1/div[text()='Lead']");

	/* Create Offer Transaction page- Select Sales Channel elements */
	protected By createOfferTransactionButton = By.xpath("//button[text()='Create Offer Transaction']");
	protected By salesChannelSelectionPageTitle = By.xpath("//u[text()='Sales Channel Selection']");
	protected By marketerTextBox = By.xpath("//input[@placeholder='Enter Marketer Name']");
	protected By marketerTextvalue = By.xpath("//li[@class='slds-listbox__item eachItem']");
	protected By salesTeamTextBox = By.xpath("//input[@placeholder='Enter Team Name']");
	protected By salesTeamValue = By
			.xpath("//div/label[text()='Sales Team']/following::li[@class='slds-listbox__item eachItem']");
	protected By salesChannelTextBox = By.xpath("//input[@placeholder='Enter Sales Channel Name']");
	protected By salesChannelValue = By
			.xpath("//div/label[text()='Sales Channel']/following::span[text()='" + testData.get("SalesChannel") + "']");
	protected By offerNameTextBox = By.xpath("//input[@placeholder='Enter Offer Name']");
	protected By offerNameValue = By
			.xpath("//div/label[text()='Offer Name']/following::span[text()='" + testData.get("Offer") + "']");
	protected By nextButton = By.xpath("//button[text()='Next']");

	/* Create Offer Transaction page- Select Marketing Strategy elements */
	protected By marketingSelectionPageTitle = By.xpath("//u[text()='Marketing Strategy Selection']");
	protected By marketingStrategyDropdown = By.xpath("//select[@name='marketingStrategy']");
	protected By marketingStrategyDropValue = By.xpath("//select/option[@value='In-house Marketing Desk']");
	protected By marketingSourceTypeDropdown = By.xpath("//select[@name='marketingSourceType']");
	protected By marketingSourceTypeDropValue = By.xpath("//select/option[@value='Shell Owner']");
	protected By marketingSubSourceDropdown = By.xpath("//select[@name='marketingSubSource']");
	protected By marketingCampaignDropdown = By.xpath("//label[text()='Marketing Campaign ']/..//select");
	protected By marketingCampaignDropValue = By.xpath("//select/option[text()='Magazine']");
	protected By offerSelectionPageTitle = By.xpath("//u[text()='Offer Selection']");
	protected By offerSelectCheckbox = By.xpath("(//div[@class='slds-checkbox_add-button'])[3]");
	protected By checkboxSelected = By.xpath("//div[@class='slds-checkbox_add-button']//input[@checked='checked']");

	/* Create Offer Transaction page- Offer Selection elements */
	protected By expirationMonthdropdown = By.xpath("//select[@name='expirationMonth']");
	protected By expirationMonthValue = By.xpath("//select[@name='expirationMonth']/option[2]");
	protected By expirationMonthDisabled = By.xpath("//input[@name='expiration' and @disabled]");
	protected By numberOfNightsdropdown = By.xpath("//select[@name='numberOfNight']");
	protected By numberOfNightsValue = By.xpath("//select[@name='numberOfNight']/option[2]");
	protected By offerPriceTextBox = By.xpath("//input[@name='offerPrice']");
	protected By premiumCheckBox1 = By.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='0']");
	protected By premiumCheckBox2 = By.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='1']");
	protected By legalTermsCondTextBox = By
			.xpath("(//span[text()='Legal Terms and Conditions']//following::slot/lightning-formatted-text)[1]");
	protected By generalTermsCondTextBox = By
			.xpath("(//span[text()='Business Terms and Conditions']//following::slot/lightning-formatted-text)[1]");
	
	
	/* Create Offer Transaction page- Payment option elements */
	protected By creditCardRadioButton = By.xpath("//input[@value='CreditCardPayment']");
	protected By manualPaymentRadioButton = By.xpath("//input[@value='Manual_Payment']");
	protected By cyberSourceFrame = By.xpath("//iframe[@id='cybsFrame']");
	protected By vFrame = By.xpath("//iframe[@id='vfIframe']");
	protected By selectVisa = By.xpath("//input[@id='card_type_001']");
	protected By cardNumber = By.xpath("//input[@id='card_number']");
	protected By expMonthDropdown = By.xpath("//select[@id='card_expiry_month']");
	protected By expMonthValue = By.xpath("//select[@id='card_expiry_month']/option[@value='05']");
	protected By expYearDropdown = By.xpath("//select[@id='card_expiry_year']");
	protected By expYearValue = By.xpath("//select[@id='card_expiry_year']/option[@value='2025']");
	protected By payButton = By.xpath("//input[@value='Pay']");
	protected By verifierInfoTextBox = By.xpath("//input[@placeholder='Search Offers Agents...']");
	protected By verifierInfoAgentValue = By.xpath("//ul[@aria-label='Recent Offers Agents']/li[2]");
	protected By offerTransactionNumber = By.xpath("//div[text()='Offer Transaction']/following::span");
	protected By offerTransactionMoreButton = By
			.xpath("//li[@class='slds-button slds-button--icon-border-filled oneActionsDropDown']//a");
	protected By processPaymentButton = By.xpath("//a[@title='Process Payment']");

	/* Cancel Offer Transaction elements */
	protected By cancelOfferTransactionButton = By.xpath("//a[@title='Cancel Offer Transaction']");
	protected By yesRadioButton = By.xpath("//input[@value='Yes']");
	protected By cancelReasonDropdown = By
			.xpath("//select[@class='select uiInput uiInputSelect uiInput--default uiInput--select']");
	protected By cancelReasonValue = By
			.xpath("//select[@class='select uiInput uiInputSelect uiInput--default uiInput--select']/option[4]");
	protected By refundOriginalCardOption = By.xpath("//input[@value='Last4digitsonthecard']");
	protected By refundAlternateCardOption = By.xpath("//input[@value='CreditCardPayment']");
	protected By spinnerContainer = By.xpath("//div[@class='loadingSpinner slds-spinner_container']");
	protected By cancelSuccessMessage = By.xpath("//span[text()='Your Cancellation was successful. ']");
	protected By cancelledTabActive = By.xpath("//a[@title='Cancelled' and @aria-selected = 'true']");
	protected By activeTab = By.xpath("//a[@title='Active' and @aria-selected = 'true']");
	protected By pendingPaymentsTabActive = By.xpath("//a[@title='Pending Payments' and @aria-selected = 'true']");

	protected By paymentProcessorTransactionId = By.xpath("//input[@name='Payment_Processor_Transaction_Id']");
	protected By paymentText = By.xpath("//input[@name='Payment_Text']");

	/* Change Base Premium elements */
	 String premium1, premium2, premium1Value, premium2Value, inactivePremium1Value, activePremium2Value, cancelledPremium1Value;
	protected By changeBasePremiumButton = By.xpath("//a[@title='Change Base Premiums']");
	protected By changeBasePremiumScreen = By.xpath("//h2/span[text()='Offer Premium Flow']");
	protected By premiumName1 = By.xpath("//tr[@class='slds-hint-parent'][1]/th[1]/div");
	protected By premiumName2 = By.xpath("//tr[@class='slds-hint-parent'][2]/th[1]/div");
	protected By premium1Selected = By
			.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='0']//input[@checked='checked']");
	protected By premium2Selected = By
			.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='1']//input[@checked='checked']");
	protected By toastErrorMessage = By.xpath(
			"//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage' and @data-key = 'error']");

	/* Change destination elements */
	String destination1Value, destination2Value, destination3Value, inactivedestination1Value, activeDestination2Value,
			customerPriceValue, newCustomerPrice;
	protected By changeDestinationButton = By.xpath("//a[@title='Change Destination']");
	protected By destinationName1 = By.xpath("//tr[@class='slds-hint-parent'][1]/th[1]/div");
	protected By destinationName2 = By.xpath("//tr[@class='slds-hint-parent'][2]/th[1]/div");
	protected By destinationName3 = By.xpath("//tr[@class='slds-hint-parent'][3]/th[1]/div");
	protected By destinationCheckBox1 = By.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='0']");
	protected By destinationCheckBox2 = By.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='1']");
	protected By destinationCheckBox3 = By.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='2']");
	protected By destination1Selected = By
			.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='0']//input[@checked='checked']");
	protected By destination2Selected = By
			.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='1']//input[@checked='checked']");
	protected By destination3Selected = By
			.xpath("//div[@class='slds-checkbox_add-button' and @data-index ='2']//input[@checked='checked']");
	protected By saveButton = By.xpath("//button[@title='Save']");
	protected By toastSuccessMessage = By.xpath(
			"//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage' and @data-key = 'success']");
	protected By customerPriceBox = By.xpath("//input[@name='customerPrice']");
	protected By overrideButton = By.xpath("//button[text()='Override']");
	protected By overridePassword = By.xpath("//input[@type='password']");
	protected By submitButton = By.xpath("//button[@title='Submit']");

	/* Related Tab elements */
	protected By relatedTab = By.xpath("//a[@title='Related']");
	protected By activeLineItemLink = By.xpath("//span[@title='Active Line Items']");
	protected By inactiveLineItemLink = By.xpath("//span[@title='Inactive Line Items']");
	protected By activeBasePremiumLink = By
			.xpath("//span[text()='Base Premium']//preceding::a[contains(@title,'OTD')][1]");
	protected By activeLinePremium = By.xpath("//span[text()='Base Premium']//following::td[2]/span/span");
	protected By inactiveLinePremium = By.xpath("//h1[@title='Inactive Line Items']//following::td[4]/span/span");

	protected By offerTransactionHistoryLink = By.xpath("//span[@title='Offer Transaction History']");
	protected By pendingLineItemLink = By.xpath("//span[@title='Pending Line Items']");
	protected By cancelledPremiumOTHistory = By
			.xpath("//h1[@title='Offer Transaction History']//following::td[3]/span/span");
	protected By activeOTBreadcrumbLink = By.xpath("//ol/li[2]/a");
	protected By inactiveOTBreadcrumbLink = By.xpath("//h1[@title='Active Line Items']//following::ol/li[2]/a");

	/* Extra Room Booking elements */
	protected By bookReservationTab = By.xpath("//a[@title='Book Reservation']");
	String arrivalDate1, departureDate1, resArrivalDate, resDepartureDate, extraRoomRate, totalPrice, totalTax,
			totalPriceValueInAccomPage, totalTaxValueInAccomPage ,totalPrice1;
	String tax, priceBeforeSavings, savings, priceAfterSavings, remainingBalance;
	String discountPagetax, discountPageRemainingBalance, discountPagepriceAfterSavings;
	double extraRoomRateValue, totalTaxValue, totalPriceValueAccomPage, remainingBalanceValue1, taxValue1,
			priceAfterSavingsValue1;
	protected By offerTransactionsTab = By.xpath("//span[text()='Offer Transactions']");
	protected By offerTransactionsPage = By.xpath("//li/span[text()='Offer Transactions']");
	protected By offerTransactionNumberResult = By.xpath("//ul[@class='lookup__list  visible']/li[3]");
	protected By reservationBookedActive = By.xpath("//a[@title='Reservation Booked' and @aria-selected = 'true']");

	protected By extraRoomBookingRadioButton = By
			.xpath("//span[text()='Extra Room Booking']/preceding::span[@class='slds-radio_faux'][1]");
	protected By calendarAvailableState = By.xpath("//td[@class='ui-state-active ui-datepicker-current-day']");
	protected By accommodationPage = By.xpath("//span[text()='Accommodation Name']");
	protected By roomType1CheckBox = By.xpath(
			"//div[@class='slds-modal__content slds-text-body_small']/following::div[@class='slds-checkbox_add-button'][1]");
	protected By accomPagetotalPrice = By
			.xpath("//div[contains(text(), 'Total Price  :')]/span[@class='uiOutputCurrency']");
	protected By accomPagetotalTax = By
			.xpath("//div[contains(text(), 'Total Taxes  :')]/span[@class='uiOutputCurrency']");
	protected By accomPageExtraRoomRate = By.xpath("(//div[contains(@title, 'Extra Room Rate')])[1]");
	protected By discountDropdown = By.xpath("//span[text()='Applied Discount']/following::select[1]");
	protected By discountPercentDropdown = By.xpath("//span[text()='Discount Percentage']/following::select[1]");
	protected By resCheckinDate = By.xpath("(//div[@title='Cloudhub'])[6]");
	protected By resCheckoutDate = By.xpath("(//div[@title='Cloudhub'])[7]");
	protected By calArrivalDate = By.xpath("//input[@name='arrivalDate']");
	protected By calDepartureDate = By.xpath("//label[contains(text(), 'Select Depature Date')]/following::input[1]");
	protected By calCurrentDate = By.xpath("//td[@class='slds-is-selected']");
	// protected By tabReservationSummary =
	// By.xpath("//span[contains(.,'Reservation Summary')]");
	protected By accommodationDetailsPage = By.xpath("//h3[text()='Accommodation Details']");
	protected By discountPageTaxValue = By
			.xpath("//td[contains(text(), 'Taxes:')]/following::span[@class='uiOutputCurrency'][1]");
	protected By discountPagePriceBeforeSavings = By.xpath(
			"//td[contains(text(), 'Total Price Before Savings:')]/following::span[@class='uiOutputCurrency'][1]");
	protected By discountPagePriceAfterSavings = By.xpath(
			"//td[contains(text(), 'Total Price After Savings:')]/following::span[@class='uiOutputCurrency'][1]");
	protected By remainingBalanceField = By.xpath("//td[contains(text(), 'Remaining Balance:')]/following::td[1]");
	protected By discountPageSavings = By.xpath(
			"//td[contains(text(), 'Total Price Before Savings:')]/following::span[@class='uiOutputCurrency'][2]");
	protected By promocodeTextBox = By.xpath("//label[text()='Promo Code']/following::input[1]");
	protected By promocodeApplyButton = By.xpath("//button[text()='Apply']");
	protected By discountPagePromoCodeValue = By
			.xpath("//div[contains(text(),'Promo: " + testData.get("PromoCode") + "')]");
	protected By promoCodeAppliedExraRoom = By.xpath("//div[text()='Free Extra Room']");
	protected By OutBalanceSectionConfirmScreen = By.xpath("//h3[text()='Out Standing balance']");
	protected By confirmButton = By.xpath("//button[text()='Confirm']");

	protected By offerReservationTypeActive = By
			.xpath("//span[text()='Active Line Items']//preceding::span[text()='Offer Reservation']");
	protected By extraRoomTypeActive = By
			.xpath("//span[text()='Active Line Items']//following::span[normalize-space()='Extra Room Reservation']");

	/* Process Adjustment */

	protected By processAdjustmentButton = By.xpath("//a[@title='Process Adjustment']");
	protected By itemTypeSection = By.xpath("//th[contains(@class, 'slds-text-title_caps')]//span[text()='Item Type']");
	protected By adjustmentAmountSectionTitle = By.xpath("//label[contains(text(),'Adjustment Amount')]");
	protected By adjustmentReasonSectionTitle = By.xpath("//span[contains(text(),'Adjustment Reason')]");
	protected By offerAddButton = By.xpath("//div[text()='Offer']//preceding::div[@class='slds-checkbox_add-button']");
	protected By offerPriceValue = By.xpath("//div[text()='Offer']//following::span[@class='uiOutputCurrency'][1]");
	protected By adjustmentAmountBox = By.xpath("//label[contains(text(),'Adjustment Amount')]/following::input[1]");
	protected By adjustmentReasonDropdown = By
			.xpath("//span[contains(text(),'Adjustment Reason')]//following::select[1]");
	protected By creditAvailableValue = By.xpath("(//span[text()='Credit Available']//following::span[@class='forceOutputCurrency'])[1]");
	protected By priceAdjustmentActive = By.xpath("(//span[text()='Price Adjustment']//following::td[2]//span[contains(text(), 'Offer')])[1]");
	protected By processRefundButton = By.xpath("//a[@class='forceActionLink']/div[text()='Process Refund']");
	protected By RefundSuccessMessage = By.xpath("//span[text()='Refund was successful. ']");
	protected By creditAvailableZero = By.xpath("(//span[text()='Credit Available']//following::span[@class='forceOutputCurrency' and text() = '$0.00'])[1]");
		
	
	/*xpath for Display Premium Screen By Shubhendra*/
	protected By textBasePremMaxPrice = By.xpath("//div[@class='slds-text-title_caps']");
	protected By textColNamePremName = By.xpath("span[title='Premium Name']");
	protected By textColNamePremRetValue = By.xpath("//span[@title='Premium Retail Value']");
	protected By textColNamePremCost = By.xpath("//span[@title='Premium Cost']");
	
	//xpath By Shubhendra for Overide Cnacellation price
	
	protected By editBoxOverideFee = By.xpath("//input[@class='slds-input' and @name='overrideFee']");
	protected By nextButtonForOverride = By.xpath("//button[contains(text(),'Next')]");
	protected By buttonOverride = By.xpath("//button[normalize-space()='Override']");
	protected By textOverrideCode = By.xpath("//label[normalize-space()='Override Code']");
	protected By editFieldOverrideCode = By.xpath("//*[normalize-space()='Override Code']//*[@class='slds-input']");
	protected By buttonSubmit = By.xpath("//button[normalize-space()='Submit']");
	protected By balanceDueMsgOnBookRes = By.xpath("//div[@data-component-id='OMCCheckAvailability']");
	
	
	protected By txtMarketer1 = By.xpath(
			"(//div/label[text()='Marketer' and @class='slds-form-element__label']/following::li[@class='slds-listbox__item eachItem'])[1]");
    
	protected By drpSalesChannel1 = By.xpath(
			"(//div/label[text()='Sales Team' and @class='slds-form-element__label']/following::div//li[@class='slds-listbox__item eachItem'])[1]");

	
	   /*OTDUpdated xpath Shubhendra*/
		protected By lblSelectAccom = By.xpath("((//table[contains(@class,'slds-table slds-table_fixed-layout slds-table')])[2]//label[@class='slds-checkbox_faux'])[1]");
		protected By lblSelectAccom2 = By.xpath("((//table[contains(@class,'slds-table slds-table_fixed-layout slds-table')])[2]//label[@class='slds-checkbox_faux'])[2]");
		protected By lblSelectAccom3 = By.xpath("((//table[contains(@class,'slds-table slds-table_fixed-layout slds-table')])[2]//label[@class='slds-checkbox_faux'])[8]");

		protected By hypactiveLineItem = By.xpath("//span[@title='Active Line Items']");
		protected By lineItemBasePrem = By.xpath("//span[@class='slds-truncate'][normalize-space()='Base Premium']");

		
		protected  By btnConfirmOnExtra = By.xpath("//button[contains(.,'Confirm')]");
		/*Reservation Cancellation*/
		protected By btnCancelExtraRoomReservation = By.xpath("(//h1[contains(text(),'Extra Room Reservation')]/following::*//button[@class='slds-button slds-button_neutral slds-button slds-button_destructive'])");

		protected By payWithCardOnFile = By.xpath("//div[normalize-space()='Pay with Card On File']");
		protected By payWithNewCard = By.xpath("//div[normalize-space()='Pay with New Credit Card']");
	
		/*Tour Detail*/
		protected By headerTourRecord = By.xpath("//ul[contains(@class,'tabs__nav')]//following::a[@title='Incentives']");
		protected By tabIncentives = By.xpath("//li[contains(@class,'tabs__item uiTabItem')]//following::a[@title='Incentives']");
		protected By tabPayments = By.xpath("//span[contains(text(),'Payments')]");
		protected By lineItemPromisedItem = By.xpath("//div[@title='Promised Items']");
		protected By promisedItemPremiumBeforeChange = By.xpath("//a[*]//span[@class='uiOutputText']");

		protected By addOnPremium = By.xpath("//a[@title='Add-On Premium']");
		protected By headerAddOnPrem = By.xpath("//span[contains(text(),'Add-On Premium Flow')]");
		protected By crossIconOnAddOnPrem = By.xpath("//button[@title='Close this window']//lightning-primitive-icon");
		protected By editSearchBox = By.xpath("//input[@title='Search...']");
		
		//Xpath By Shubhendra
		protected By custmerNameSearched = By.xpath(
				"//tbody/tr[1]/td[2]/span[1]/span[1]/a[1]");
		protected By custmerEmailSearched = By.xpath(
				"(//a[@class='slds-truncate emailuiFormattedEmail'])[1]");
	/*
	 * Description: Search an Offer Transaction record and navigate to details
	 * page Date: Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void searchOfferTransaction() {
		waitUntilElementVisibleBy(driver, txtOfferTransactionPage, "ExplicitLongWait");
		verifyPageDisplayed(txtOfferTransactionPage, "Page Title", "OfferTransactionsPage", "searchOfferTransaction");
		fieldDataEnter_modified(txtSearchOfferTransaction, testData.get("OfferTransactionNumber"));
		clickElementJSWithWait(imgClick);
		waitUntilElementVisibleBy(driver, txtOffrTranDetailPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(txtOffrTranDetailPage));
		tcConfig.updateTestReporter("OfferTransactionsPage", "searchOfferTransaction", Status.PASS,
				"User navigated to Offer Transaction details page Successfully");
	}

	/*
	 * Method: validate the Offer Transaction status Date: Sept/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void verifyOfferTransactionStatus(String status) {
		waitUntilElementVisibleBy(driver, txtOffrTranDetailPage, "ExplicitMedWait");
		switch (status) {

		case "Active":

			Assert.assertTrue(verifyObjectDisplayed(tabActive));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateOfferTransactionStatus", Status.PASS,
					"Offer Transaction reocrd is displayed :-" + getElementText(txtOfferTransactionNum)
							+ " and status is :-" + getElementText(tabActive));
			break;

		case "Pending Payments":

			Assert.assertTrue(verifyObjectDisplayed(tabPendingPayment));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateOfferTransactionStatus", Status.PASS,
					"Offer Transaction reocrd is displayed :-" + getElementText(txtOfferTransactionNum)
							+ " and status is :-" + getElementText(tabPendingPayment));
			break;

		case "Reservation Booked":

			Assert.assertTrue(verifyObjectDisplayed(tabReservationBook));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateOfferTransactionStatus", Status.PASS,
					"Offer Transaction reocrd is displayed :-" + getElementText(txtOfferTransactionNum)
							+ " and status is :-" + getElementText(tabReservationBook));
			break;
		}
	}

	/*
	 * Description: Click on Transfer Offer button to Search for Lead/OWners
	 * Date: Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void navigateToTransferOffer() {
		waitUntilElementVisibleBy(driver, actionButton, "ExplicitMedWait");
		clickElementJSWithWait(actionButton);
		clickElementJSWithWait(btnTransferButton);
		waitUntilElementVisibleBy(driver, txtOffrTranDetailPage, "ExplicitMedWait");
		verifyPageDisplayed(txtTransferOffer, "Page Title", "OfferTransactionsPage", "navigateToTransferOffer");
		fieldDataEnter_modified(txtSearchName, testData.get("Lead"));
		clickElementJSWithWait(chkRecord);
	}

	/*
	 * Description: Click on Transfer Offer button to Search for Lead/OWners
	 * Date: Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void validateErrorMessage() {
		waitUntilElementVisibleBy(driver, actionButton, "ExplicitMedWait");
		clickElementJSWithWait(actionButton);
		clickElementJSWithWait(btnTransferButton);
		Assert.assertTrue(verifyObjectDisplayed(errorMsg));
		tcConfig.updateTestReporter("OfferTransactionsPage", "validateErrorMessage", Status.PASS,
				"Error Message displayed Successfully :-" + getElementText(errorMsg));
	}

	/*
	 * Description: Click on Transfer button if search Lead/Owner found Date:
	 * Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void validateTransferOffer() {
		waitUntilElementVisibleBy(driver, btnTransfer, "ExplicitLongWait");
		verifyPageDisplayed(btnTransfer, "Page Title", "OfferTransactionsPage", "validateTransferOffer");
		clickElementJSWithWait(btnTransfer);
		waitUntilElementVisibleBy(driver, txtOfferTranNew, "ExplicitMedWait");
		if (verifyObjectDisplayed(txtOfferTranNew)) {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateTransferOffer", Status.PASS,
					"User navigated to new Offer Transaction details page Successfully :-"
							+ getElementText(txtOfferTranNew));
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateTransferOffer", Status.FAIL,
					"User failed to Navigated to new Offer Transaction details page");
		}
	}

	/*
	 * Description: Validate the Lead name in screen Date: Sept/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void validateLeadName() {
		String leadName = testData.get("Lead");
		if (leadName.equalsIgnoreCase(getElementText(hypLeadNm))) {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateLeadName", Status.PASS,
					"Lead name displayed as:- " + getElementText(hypLeadNm));
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateLeadName", Status.FAIL,
					"Failed to validate the Lead Name in screen");
		}
	}

	/*
	 * Description: Validate the Customer name in screen Date: Sept/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void validateOwnerName() {
		String ownerName = testData.get("Owner");
		if (ownerName.equalsIgnoreCase(getElementText(hypCustomerNm))) {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateLeadName", Status.PASS,
					" Owner name dispalyed as :-" + getElementText(hypCustomerNm));
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateLeadName", Status.FAIL,
					"Failed to validate the Owner Name in screen");
		}
	}

	/*
	 * Description: Click on Transfer Offer button to Search for Lead/OWners
	 * Date: Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void searchByDifferentFields() {
		waitUntilElementVisibleBy(driver, actionButton, "ExplicitMedWait");
		clickElementJSWithWait(actionButton);
		clickElementJSWithWait(btnTransferButton);
		waitUntilElementVisibleBy(driver, txtTransferOffer, "ExplicitMedWait");
		verifyPageDisplayed(txtTransferOffer, "Page Title", "OfferTransactionsPage", "searchByDifferentFields");
		fieldDataEnter_modified(txtSearchName, testData.get("MemberNumber"));
		fieldDataEnter_modified(txtSearchName, testData.get("Email"));
		fieldDataEnter_modified(txtSearchName, testData.get("MobileNumber"));
		fieldDataEnter_modified(txtSearchName, testData.get("Lead"));
		sendKeyboardKeys(Keys.ENTER);
		waitUntilElementVisibleBy(driver, txtTransferOffer, "ExplicitLongWait");
		List<WebElement> elements = driver.findElements(lstSearchHeader);
		System.out.println("Number of elements in Qunatity on Product:" + elements.size());
		for (int i = 0; i <= elements.size() - 1; i++) {
			System.out.println("Search results consists of following columns :- " + elements.get(i).getText());
			if ((elements.get(i).getText()) != null) {
				tcConfig.updateTestReporter("OfferTransactionsPage", "searchByDifferentFields", Status.PASS,
						"Search results consists of following columns :- " + elements.get(i).getText());
			} else {
				tcConfig.updateTestReporter("OfferTransactionsPage", "searchByDifferentFields", Status.FAIL,
						"Failed while displaying the search results");
			}
		}
		clickElementJSWithWait(chkRecord);
		validateTransferOffer();
	}

	/*
	 * Description: VAlidate the Payment records on new Offer Transaction Date:
	 * Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void validatePayemntRecords() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");
		clickElementJSWithWait(tabRelated);
		scrollDownForElementJSBy(lnkPaymentRecords);
		clickElementJSWithWait(lnkPaymentRecords);
		verifyPageDisplayed(txtPaymentRecordsPage, "Page Title", "OfferTransactionsPage", "validatePayemntRecords");
		List<WebElement> elements = driver.findElements(tblPayemntRecords);
		System.out.println("Number of elements in Qunatity on Product:" + elements.size());
		for (int i = 0; i <= elements.size() - 1; i++) {
			System.out.println("Search results consists of following columns :- " + elements.get(i).getText());
			if ((elements.get(i).getText()) != null) {
				tcConfig.updateTestReporter("OfferTransactionsPage", "validatePayemntRecords", Status.PASS,
						"Payment records are dispalyed :- " + elements.get(i).getText());
			} else {
				tcConfig.updateTestReporter("OfferTransactionsPage", "validatePayemntRecords", Status.FAIL,
						"Failed while displaying the Payment records");
			}
		}
	}

	/*
	 * Description: Validate extension restriction error message on click of
	 * Offer Transaction Date: Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void validateExtensionRestrictionMsg() throws AWTException {
		waitUntilElementVisibleBy(driver, actionButton, "ExplicitMedWait");
		clickElementJSWithWait(actionButton);
		if (verifyObjectDisplayed(btnAddExtension)) {
			clickElementJSWithWait(btnAddExtension);
			Assert.assertTrue(verifyObjectDisplayed(extnErrorMsg));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateExtensionRestrictionMsg", Status.PASS,
					"User should get the message displayed as :-" + getElementText(extnErrorMsg));
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateExtensionRestrictionMsg", Status.FAIL,
					"System doesnot generate the error meassge and use ris able to procced");
		}
	}

	/*
	 * Description: Validate Add Extension after Override code added Date:
	 * Oct/2020 Author: Jyoti Changes By: NA
	 */
	public void validateAddExtension() throws AWTException {
		waitUntilElementVisibleBy(driver, actionButton, "ExplicitMedWait");
		clickElementJSWithWait(actionButton);
		clickElementJSWithWait(btnAddExtension);
		verifyPageDisplayed(txtAddExtension, "Page Title", "OfferTransactionsPage", "validateAddExtension");

		// Taking the current expiration date and Split
		String date = getElementText(txtCurrExpirationDt);
		String[] dateParts = date.split("/");
		String currmonth = dateParts[0];
		String currday = dateParts[1];
		String curryear = dateParts[2];

		System.out.println(
				"The string value in month are :-" + currmonth + "and day :-" + currday + "and year:- " + curryear);

		// select any Extension
		clickElementJSWithWait(chkFilter);
		waitUntilElementVisibleBy(driver, txtUpdatedExpDt, "ExplicitLongWait");
		tcConfig.updateTestReporter("OfferTransactionsPage", "validateAddExtension", Status.PASS,
				"The current expiration date is sucessfully validated and dispalyed as :-"
						+ getElementText(txtCurrExpirationDt));

		// Taking the Updated expiration date and Split
		String update = getElementText(txtUpdatedExpDt);
		String[] newDate = update.split("/");
		String up_month = newDate[0];
		String up_day = newDate[1];
		String up_year = newDate[2];

		System.out.println(
				"The updated expiration month are :-" + up_month + "and day :-" + up_day + "and year:- " + up_year);
		if (verifyObjectDisplayed(txtUpdatedExpDt)) {
			if (Integer.parseInt(dateParts[0]) <= 9) {
				newDate[0] = dateParts[0] + 3;
				newDate[1] = dateParts[1];
				newDate[2] = dateParts[2];

				System.out
						.println("The dates are : -" + newDate[0] + "day :-" + newDate[1] + "and year:-" + newDate[2]);
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateAddExtension", Status.PASS,
						"The updated expiration date is sucessfully validated and dispalyed as :-"
								+ getElementText(txtUpdatedExpDt));
			} else if (Integer.parseInt(dateParts[0]) > 9) {
				int date_ = Integer.parseInt(dateParts[0] + 3) - 12;
				newDate[0] = Integer.toString(date_);
				newDate[1] = dateParts[1];
				newDate[2] = dateParts[2] + 1;
				System.out
						.println("The dates are : -" + newDate[0] + "day :-" + newDate[1] + "and year:-" + newDate[2]);
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateAddExtension", Status.PASS,
						"The updated expiration date is sucessfully validated and dispalyed as :-"
								+ getElementText(txtUpdatedExpDt));
			}
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateAddExtension", Status.FAIL,
					"Failed while validating the Updated Expiration date after Extension is added");
		}
	}

	protected By txtOveridePage = By.xpath("//h2[contains(.,'Overide')]");

	/*
	 * Description: Validate Add Extension after Override code added Date:
	 * Oct/2020 Author: Jyoti Changes By: NA
	 */
	public void verifyOverrideCode() throws AWTException {
		double i, j = 0;
		String totalQuantity = getElementText(txtExtnPrice);
		i = Double.parseDouble(totalQuantity);
		j = i + 10;
		String str = Double.toString(j);
		clearElementBy(customerPrice);
		sendKeysBy(customerPrice, str);
		waitUntilElementVisibleBy(driver, btnOveride, "ExplicitLongWait");
		if (verifyObjectEnabled(btnOveride)) {
			clickElementJSWithWait(btnOveride);
			verifyPageDisplayed(txtOveridePage, "Page Title", "OfferTransactionsPage", "verifyOverrideCode");
			fieldDataEnter_modified(txtOverideCode, testData.get("OverrideCode"));
			clickElementJSWithWait(btnSubmit);
			Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateExtensionRestrictionMsg", Status.PASS,
					"User should get the message displayed as :-" + getElementText(sucessMsg));
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateExtensionRestrictionMsg", Status.FAIL,
					"System doesnot generate the error meassge and users able to procced");
		}
		clickElementJSWithWait(btnSave);
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("OfferTransactionsPage", "validateExtensionRestrictionMsg", Status.PASS,
				"User should get the message displayed as :-" + getElementText(sucessMsg));
	}

	/*
	 * Description: Validate the Add Extension feilds Date: Oct/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void validateExtensionFields() {
		waitUntilElementVisibleBy(driver, tabRelateds, "ExplicitMedWait");
		clickElementJSWithWait(tabRelateds);
		waitUntilElementVisibleBy(driver, lnkActiveLineItems, "ExplicitMedWait");
		clickElementJSWithWait(lnkActiveLineItems);
		waitUntilElementVisibleBy(driver, txtActiveLinePage, "ExplicitLongWait");
		verifyPageDisplayed(txtActiveLinePage, "Page Title", "OfferTransactionsPage", "validateExtensionFields");
		clickElementJSWithWait(hypExtnRecord);
		waitUntilElementVisibleBy(driver, txtOfferTransactDeailsPage, "ExplicitLongWait");
		verifyPageDisplayed(txtOfferTransactDeailsPage, "Page Title", "OfferTransactionsPage",
				"validateExtensionFields");
		if (verifyObjectDisplayed(lnkOffTrnstn) || verifyObjectDisplayed(txtDetailsType)
				|| verifyObjectDisplayed(txtCost) || verifyObjectDisplayed(txtCustPrice)
				|| verifyObjectDisplayed(chkActive) || verifyObjectDisplayed(chkOverrideCode)
				|| verifyObjectDisplayed(hypSalesChannel) || verifyObjectDisplayed(hypSalesTeam)
				|| verifyObjectDisplayed(supervisor) || verifyObjectDisplayed(manager)) {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateExtensionFields", Status.PASS,
					"Offer Transaction number is :-" + getElementText(lnkOffTrnstn) + " Details Type is :-"
							+ getElementText(txtDetailsType) + " Cost field is:-" + getElementText(txtCost)
							+ " Customer Price is :- " + getElementText(txtCustPrice));
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateExtensionFields", Status.FAIL,
					"System doesnot display the extension fields");
		}

	}

	/*
	 * Description: Validate the updated Extension length and balance due field
	 * Date: Oct/2020 Author: Jyoti Changes By: NA
	 */
	public void navigateOfferTransaction() {
		clickElementJSWithWait(lnkOffTrnstn);
		waitUntilElementVisibleBy(driver, txtOffrTranDetailPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(balanceDue));
		tcConfig.updateTestReporter("OfferTransactionsPage", "navigateOfferTransaction", Status.PASS,
				"Balance due feild displayed as :-" + getElementText(balanceDue));
		scrollDownByPixel(200);
		if (verifyObjectDisplayed(txtOffExpDt) && verifyObjectDisplayed(txtExtnLength)) {
			tcConfig.updateTestReporter("OfferTransactionsPage", "navigateOfferTransaction", Status.PASS,
					"Offer Expiration date is :-" + getElementText(txtOffExpDt) + " and Extension length added is :-"
							+ getElementText(txtExtnLength));
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "navigateOfferTransaction", Status.FAIL,
					"System doesnot display extension length and oofer expiration date");
		}
	}

	/*
	 * Description: Validate Add Extension after Override code added Date:
	 * Oct/2020 Author: Jyoti Changes By: NA
	 */
	public void validateMoneyTransfer_Modified() throws AWTException {
		clearElementBy(txtSearchOfferTransaction);
		fieldDataEnter_modified(txtSearchOfferTransaction, testData.get("OfferTransactionnum1"));
		clickElementJSWithWait(imgClick);

		strOffPrice2 = getElementText(txtOfferPriceSec);
		verifyPageTitleDisplayed(txtOTNew, "Offer Transaction created successfully", "Offer transaction Page",
				"validateMoneyTransfer_Modified" + getElementText(txtOTNew) + " and Offer Price is :-"
						+ getElementText(txtOfferPriceSec));

		waitUntilElementVisibleBy(driver, actionButton, "ExplicitMedWait");
		offrTransactn = testData.get("OfferTransactionNumber");
		clickElementJSWithWait(actionButton);
		clickElementJSWithWait(btnMoneyTransfer);
		verifyPageDisplayed(txtMoneyTransferPage, "Page Title", "OfferTransactionsPage",
				"validateMoneyTransfer_Modified");

		List<WebElement> tblData = driver.findElements(tblOTNum);
		for (int i = 1; i <= tblData.size(); i++) {
			do {
				Robot rob = new Robot();
				rob.mouseWheel(4);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} while (!driver.findElement(record).isDisplayed());

			String arrivalList = getElementText(record);
			if (arrivalList.equals(offrTransactn)) {
				driver.findElement(By.xpath("//div[@title='" + testData.get("OfferTransactionNumber")
						+ "' and @class='slds-truncate']/../..//label[@class='slds-checkbox_faux']")).click();
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateMoneyTransfer_Modified", Status.PASS,
						"Offer Transaction checkbox is clicked");
				break;
			} else {
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateMoneyTransfer_Modified", Status.FAIL,
						"Failed while clicking Offer Transaction checkbox");
				break;
			}
		}
		do {
			Robot rob = new Robot();
			rob.mouseWheel(4);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(btnNext).isDisplayed());
		clickElementJSWithWait(btnNext);
		verifyPageDisplayed(txtMoneyTransferPg, "Page Title", "OfferTransactionsPage",
				"validateMoneyTransfer" + getElementText(sucessMsgMoneyTransfer));
		selectElement("txtMoneyTransferPg", btnFinish, "User navigate to Offer Transaction Page");
	}

	/*
	 * Description: Validate Money Transfer on Offer Transaction Date: Oct/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void validateMoneyTransfer() throws AWTException {
		waitUntilElementVisibleBy(driver, actionButtonMT, "ExplicitLongWait");
		clickElementJSWithWait(actionButtonMT);
		clickElementJSWithWait(btnMoneyTransfer);
		verifyPageDisplayed(txtMoneyTransferPage, "Page Title", "OfferTransactionsPage", "validateMoneyTransfer");
		List<WebElement> tblData = driver.findElements(tblOTNum);
		for (int i = 1; i <= tblData.size(); i++) {
			Robot rob = new Robot();
			rob.mouseWheel(3);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(By.xpath("//div[@title='" + offerTransactionNum
					+ "' and @class='slds-truncate']/../..//label[@class='slds-checkbox_faux']")).click();
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateMoneyTransfer", Status.PASS,
					"Offer Transaction checkbox is clicked");
			break;
		}
		do {
			Robot rob = new Robot();
			rob.mouseWheel(4);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(btnNext).isDisplayed());
		clickElementJSWithWait(btnNext);
		verifyPageDisplayed(txtMoneyTransferPg, "Page Title", "OfferTransactionsPage",
				"validateMoneyTransfer" + getElementText(sucessMsgMoneyTransfer));
		selectElement("MoneyTransferPage", btnFinish, "User navigate to Offer Transaction Page");
	}

	/*
	 * Description: Validate the old and new Payment records Date: Oct/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void validateOldandNewPaymentFields() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitLongWait");
		clickElementJSWithWait(tabRelated);
		waitUntilElementVisibleBy(driver, lnkActiveLineItems, "ExplicitLongWait");
		do {
			Robot rob = new Robot();
			rob.mouseWheel(3);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(lnkPaymentRecords).isDisplayed());
		selectElement("validateOldandNewPaymentFields", lnkPaymentRecords, "User navigate to Payments Page");
		selectElement("validateOldandNewPaymentFields", hypPaymenIDNew, "User navigate to New created Payment Page");
		if (verifyObjectDisplayed(txtPaymentIdPage) && verifyObjectDisplayed(txtDecision)) {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateOldandNewPaymentFields", Status.PASS,
					"System display the Current payment ID as :-" + getElementText(txtPaymentIdPage)
							+ " and Decision is display as :-" + getElementText(txtDecision));

			clickElementJSWithWait(hypOldPaymentID);
			waitUntilElementVisibleBy(driver, txtPaymentIdPage, "ExplicitLongWait");
			if (verifyObjectDisplayed(txtOldPayemntID) && verifyObjectDisplayed(txtDecisionOld)) {
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateOldandNewPaymentFields", Status.PASS,
						"System display the Transferred payment ID as :-" + getElementText(txtOldPayemntID)
								+ " and Decision is display as :-" + getElementText(txtDecisionOld));
			} else {
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateOldandNewPaymentFields", Status.FAIL,
						"Failed while displaying the Transferred payment fields");
			}
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateOldandNewPaymentFields", Status.FAIL,
					"Failed while displaying the newly created payment fields");
		}
		waitUntilElementVisibleBy(driver, hypClickOldOT, "ExplicitLongWait");
		selectElement("validateOldandNewPaymentFields", hypClickOldOT,
				"User navigates to Transferred Payment Page in Offer Transaction created previously");
		verifyPageDisplayed(txtPrevBalnaceDue, "Page Title", "OfferTransactionsPage",
				"validateOldandNewPaymentFields" + getElementText(txtPrevBalnaceDue));
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(tabPendingPayment));
		 * tcConfig.updateTestReporter("OfferTransactionsPage",
		 * "validateOfferTransactionStatus", Status.PASS,
		 * "Offer Transaction reocrd is displayed :-"
		 * +getElementText(txtOfferTransactionNum)+ " and status is :-"
		 * +getElementText(tabPendingPayment));
		 */
	}

	public static Map<String, String> objMapFirst = new HashMap<String, String>();
	public static Map<String, String> objMapSec = new HashMap<String, String>();

	/*
	 * Description: Validate the transfer of Balance in credit availiable and
	 * payemnt screen Date: Oct/2020 Author: Jyoti Changes By: NA
	 */
	public void compareTransferBalance() {

		String strPrevOT, strNewOT, strCredit, strCreditBalance;
		int i, j, a;
		strPrevOT = strOffPrice1.substring(strOffPrice1.indexOf("$") + 1).replaceAll("(?<=^\\d+)\\.0*$", "").trim();
		strNewOT = strOffPrice2.substring(strOffPrice2.indexOf("$") + 1).replaceAll("(?<=^\\d+)\\.0*$", "").trim();
		i = Integer.parseInt(strPrevOT);
		j = Integer.parseInt(strNewOT);

		if (i > j) {
			a = i - j;
			strCredit = String.valueOf(a);
			String dis = getElementText(txtCreditAvailiable);
			strCreditBalance = dis.substring(dis.indexOf("$") + 1).replaceAll("(?<=^\\d+)\\.0*$", "").trim();

			if (strCredit.equalsIgnoreCase(strCreditBalance)) {
				tcConfig.updateTestReporter("OfferTransactionsPage", "compareTransferBalance", Status.PASS,
						"Unable to create Offer transaction" + getElementText(txtCreditAvailiable));
			} else {
				tcConfig.updateTestReporter("OfferTransactionsPage", "compareTransferBalance", Status.FAIL,
						"Failed while vlaidating the credit amount");
			}
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "compareTransferBalance", Status.FAIL,
					"Failed while storing the credit amount");
		}
	}

	/*
	 * Description: Validate the transfer of Balance Due Date: Oct/2020 Author:
	 * Jyoti Changes By: NA
	 */
	protected By creditBalUpdated = By.xpath(
			"((//span[contains(.,'Credit Available') and @class='slds-form-element__label slds-truncate'])[2]/following::span)[1]");
	
	public void compareBalanceDue() {
		
		String strPrevOT, strNewOT, strCredit, strCreditBalance,strCreditBalance1;
		int i, j, a;
		strPrevOT = strOffPrice1.substring(strOffPrice1.indexOf("$") + 1).replaceAll("(?<=^\\d+)\\.0*$", "").trim();
		strNewOT = strOffPrice2.substring(strOffPrice2.indexOf("$") + 1).replaceAll("(?<=^\\d+)\\.0*$", "").trim();
		i = Integer.parseInt(strPrevOT);
		j = Integer.parseInt(strNewOT);
		
		if (i >= j) {
			a = i - j;
			strCredit = String.valueOf(a);
			 waitForSometime(tcConfig.getConfig().get("MedWait"));
			String dis = getElementText(txtPrevBalnaceDue);
			strCreditBalance = dis.substring(dis.indexOf("$") + 1).replaceAll("(?<=^\\d+)\\.0*$", "").trim();
			//Value by Shubhendra
			String credUpdatVal = getElementText(creditBalUpdated);
			strCreditBalance1 = credUpdatVal.substring(dis.indexOf("$") + 1).replaceAll("(?<=^\\d+)\\.0*$", "").trim();

			System.out.println(strCredit);
			System.out.println(strCreditBalance);
			System.out.println(strCreditBalance1);
			if (strCredit.equalsIgnoreCase(strCreditBalance1)) {
				tcConfig.updateTestReporter("OfferTransactionsPage", "compareBalanceDue", Status.PASS,
						"Unable to create Offer transaction" + getElementText(txtPrevBalnaceDue));
			} else {
				tcConfig.updateTestReporter("OfferTransactionsPage", "compareBalanceDue", Status.FAIL,
						"Failed while vlaidating the credit amount");
			}
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "compareBalanceDue", Status.FAIL,
					"Failed while storing the credit amount");
		}
	}

	/*
	 * Modified Method: createOfferTransaction Description: Create Offer
	 * transactions for Bring A Friend Date: October/2020 Author: Jyoti Changes
	 * By: NA
	 */
	public void createOfferTransactionFirst() {
		if (verifyObjectDisplayed(salesChannelSelectionPageTitle)) {
			verifyPageTitleDisplayed(salesChannelSelectionPageTitle, "Sale Channel Selection Page",
					"Sale Channel Selection", "createOfferTransactionFirst");
			enterSalesChannelSelectionFieldNew();
			enterMarketingStrategyDetails();
			clickElementBy(expirationMonthdropdown);
			clickElementBy(expirationMonthValue);

			clickElementBy(numberOfNightsdropdown);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(numberOfNightsValue);
			fieldEnterData(offerPriceTextBox, testData.get("OfferPriceFirst"));
			clickElementBy(ckbSelect);

			verifyPageTitleDisplayed(txtOfferPricePage, "Offer Price Selection", "Offer Price",
					"createOfferTransactionFirst");
			scrollDownByPixel(100);
			selectElement("createOfferTransactionFirstOT", nextButton, "User navigate to Destination Page");
			clickElementJSWithWait(offerSelectCheckbox);
			verifyPageTitleDisplayed(txtDestinationPage, "Destination Selection", "Destination Selection",
					"createOfferTransactionFirst");
			selectElement("createOfferTransactionFirst", nextButton, "User navigate to Accomodation Page");
			selectElement("createOfferTransactionFirst", nextButton, "Next Button in the Premuim page");
			clickElementJSWithWait(premiumCheckBox1);
			selectElement("createOfferTransactionFirst", nextButton, "Next Button in the Select Premium page");
			verifyPageTitleDisplayed(nextButton, "Confirmation Details", "Confirmation Page",
					"createOfferTransactionFirst");
			selectElement("createOfferTransactionFirst", nextButton, "Next Button in the Confirmation page");
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "createOfferTransactionFirst", Status.FAIL,
					"Unable to create Offer transaction");
		}
	}

	/*
	 * Modified Method: createOfferTransaction Description: Create Offer
	 * transactions for Bring A Friend Date: October/2020 Author: Jyoti Changes
	 * By: NA
	 */
	public void createOfferTransactionSecond() {
		if (verifyObjectDisplayed(salesChannelSelectionPageTitle)) {
			verifyPageTitleDisplayed(salesChannelSelectionPageTitle, "Sale Channel Selection Page",
					"Sale Channel Selection", "createOfferTransactionSecond");
			enterSalesChannelSelectionSecond();
			enterMarketingStrategyDetails();
			clickElementBy(expirationMonthdropdown);
			clickElementBy(expirationMonthValue);

			clickElementBy(numberOfNightsdropdown);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(numberOfNightsValue);
			fieldEnterData(offerPriceTextBox, testData.get("OfferPriceSec"));
			clickElementBy(ckbSelect);

			verifyPageTitleDisplayed(txtOfferPricePage, "Offer Price Selection", "Offer Price",
					"createOfferTransactionSecond");
			scrollDownByPixel(100);
			selectElement("createOfferTransactionSecond", nextButton, "User navigate to Destination Page");
			clickElementJSWithWait(offerSelectCheckbox);
			verifyPageTitleDisplayed(txtDestinationPage, "Destination Selection", "Destination Selection",
					"createOfferTransactionSecond");
			selectElement("createOfferTransactionSecond", nextButton, "User navigate to Accomodation Page");
			selectElement("createOfferTransactionSecond", nextButton, "Next Button in the Premuim page");
			clickElementJSWithWait(premiumCheckBox1);
			selectElement("createOfferTransactionSecond", nextButton, "Next Button in the Select Premium page");
			verifyPageTitleDisplayed(nextButton, "Confirmation Details", "Confirmation Page",
					"createOfferTransactionSecond");
			selectElement("createOfferTransactionSecond", nextButton, "Next Button in the Confirmation page");
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "createOfferTransactionSecond", Status.FAIL,
					"Unable to create Offer transaction");
		}
	}

	/*
	 * Modified Method: enterSalesChannelSelection Description: Enter sales
	 * channel Page fields selection details Date: October/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void enterSalesChannelSelectionSecond() {
		if (verifyObjectDisplayed(marketerTextBox)) {
			fieldDataEnter_modified(marketerTextBox, testData.get("SupervisorName"));
			clickElementBy(txtMarketer);
			fieldDataEnter_modified(salesTeamTextBox, testData.get("Teams"));
			/* clickElementBy(salesTeamValue); */
			clickElementBy(drpSalesTeam);
			fieldDataEnter_modified(salesChannelTextBox, testData.get("SalesChannel"));
			/* clickElementBy(salesChannelValue); */
			clickElementBy(drpSalesChannel);
			clickElementBy(drpOfferTypes);
			clickElementBy(value);
			fieldDataEnter_modified(offerNameTextBox, testData.get("Offer"));
			/* clickElementBy(offerNameValue); */
			clickElementBy(drpOfferValue);
			verifyPageTitleDisplayed(salesChannelSelectionPageTitle, "Sales Channel Selection Fields display",
					"Sales Channel Selection", "enterSalesChannelSelectionFieldNew");
			selectElement("createOfferTransaction", nextButton, "Marketing Statergy Selection page is displayed");
		} else {
			tcConfig.updateTestReporter("Create Offer transaction", "enterSalesChannelSelectionFieldNew", Status.FAIL,
					"Unable to enter details in the sales channel selection screen");
		}
	}

	/*
	 * Modified Method: enterSalesChannelSelection Description: Enter sales
	 * channel Page fields selection details Date: October/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void enterSalesChannelSelectionFieldNew() {
		if (verifyObjectDisplayed(marketerTextBox)) {
			fieldDataEnter_modified(marketerTextBox, testData.get("SupervisorName"));
			clickElementBy(txtMarketer);
			fieldDataEnter_modified(salesTeamTextBox, testData.get("Teams"));
			/* clickElementBy(salesTeamValue); */
			clickElementBy(drpSalesTeam);
			fieldDataEnter_modified(salesChannelTextBox, testData.get("SalesChannel"));
			/* clickElementBy(salesChannelValue); */
			clickElementBy(drpSalesChannel);
			clickElementBy(drpOfferTypes);
			clickElementBy(value);
			fieldDataEnter_modified(offerNameTextBox, testData.get("Offer"));
			/* clickElementBy(offerNameValue); */
			clickElementBy(drpOfferValue);
			verifyPageTitleDisplayed(salesChannelSelectionPageTitle, "Sales Channel Selection Fields display",
					"Sales Channel Selection", "enterSalesChannelSelectionFieldNew");
			selectElement("createOfferTransaction", nextButton, "Marketing Statergy Selection page is displayed");
		} else {
			tcConfig.updateTestReporter("Create Offer transaction", "enterSalesChannelSelectionFieldNew", Status.FAIL,
					"Unable to enter details in the sales channel selection screen");
		}
	}

	/*
	 * Modified Method: ChoosePayment Description: Choose payment option Date:
	 * September/2020 Author: Jyoti Changes By: NA
	 */
	public void chooseCreditCardPaymentNew() {
		if (verifyObjectDisplayed(creditCardRadioButton)) {
			clickElementJSWithWait(creditCardRadioButton);
			selectElement("createOfferTransaction", nextButton, "Next Button in the Payment Options page");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			enterCreditCardDetails();
			/*
			 * fieldDataEnter_modified(txtVerifierAgent,
			 * testData.get("VerifierID")); clickElementBy(imgIcon);
			 * clickElementBy(txtVerifiedDT); selectSystemDate(lnkToday); String
			 * callNum1 = getRandomString(10); fieldEnterData(txtCallNum1,
			 * callNum1); String callNum2 = getRandomString(10);
			 * fieldEnterData(txtCallNum2, callNum2);
			 */
			verifyPageTitleDisplayed(nextButton, "Details entered in Verifier information", "Verifier information Page",
					"createOfferTransaction");
			selectElement("chooseCreditCardPayment", nextButton, "Next Button in the Verifier information page");
			offerTransactionNum = getElementText(txtOfferTransactionNum1);
			strOffPrice1 = getElementText(txtOfferPrice);
			verifyPageTitleDisplayed(offerTransactionNumber, "Offer Transaction created successfully",
					"Offer transaction Page", "chooseCreditCardPayment" + getElementText(txtOfferTransactionNum1)
							+ " and Offer Price is :-" + getElementText(txtOfferPrice));
		} else {
			tcConfig.updateTestReporter("Payment options", "chooseCreditCardPayment", Status.FAIL,
					"Unable to enter credit card payment");
		}
	}

	protected By txtOTNew = By.xpath(
			"(//div[contains(.,'Offer Transaction')]/../..//div[contains(@class,'slds-page-header__title slds-m-right')]//span)[2]");

	/*
	 * Method: noPaymentScenario Description: Create Offer Transaction without
	 * any payment Date: September/2020 Author: Jyoti Changes By: NA
	 */
	public void noPaymentScenario() {
		if (verifyObjectDisplayed(creditCardRadioButton)) {
			clickElementJSWithWait(creditCardRadioButton);
			selectElement("enterCreditCardDetails", nextButton, "Next Button in the Payment Options page");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectElement("enterCreditCardDetails", nextButton, "Next Button in the Credit card details page");
			/*
			 * fieldDataEnter_modified(txtVerifierAgent,
			 * testData.get("VerifierID")); clickElementBy(imgIcon);
			 * clickElementBy(txtVerifiedDT); selectSystemDate(lnkToday); String
			 * callNum1 = getRandomString(10); fieldEnterData(txtCallNum1,
			 * callNum1); String callNum2 = getRandomString(10);
			 * fieldEnterData(txtCallNum2, callNum2);
			 */
			verifyPageTitleDisplayed(nextButton, "Details entered in Verifier information", "Verifier information Page",
					"createOfferTransaction");
			selectElement("chooseCreditCardPayment", nextButton, "Next Button in the Verifier information page");
			strOffPrice2 = getElementText(txtOfferPriceSec);
			verifyPageTitleDisplayed(txtOTNew, "Offer Transaction created successfully", "Offer transaction Page",
					"noPaymentScenario" + getElementText(txtOTNew) + " and Offer Price is :-"
							+ getElementText(txtOfferPriceSec));
		} else {
			tcConfig.updateTestReporter("Payment options", "noPaymentScenario", Status.FAIL,
					"Failed while bypassing payment page");
		}
	}

	/*
	 * Method: selectOwnerRecord Description: Select Owner record from the list
	 * Val Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void selectOwnerRecord() {
		clickElementJSWithWait(ownersTab);
		if (verifyObjectDisplayed(ownersTitle)) {
			verifyPageTitleDisplayed(ownersTitle, "Navigate to Owners page", "OwnersPage", "selectOwnerRecord");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerRecord", Status.FAIL,
					"Unable to navigate to Owners page");
		}
		fieldEnterData(searchTextBox, testData.get("Owner"));
		sendKeyboardKeys(Keys.ENTER);
		clickElementBy(By.xpath("//a[@title='" + testData.get("Owner") + "']"));
		verifyPageTitleDisplayed(ownersPageTitle, "Navigated to Owner Details page", "OwnersPage", "selectOwnerRecord");
		selectElement("OwnersPage", createOfferTransactionButton, "Create Offer Transaction Button");
	}

	/*
	 * Method: selectLeadRecord Description: Select Lead record from the list
	 * Val Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void selectLeadRecord() {
		clickElementJSWithWait(leadsTab);
		if (verifyObjectDisplayed(leadsTitle)) {
			verifyPageTitleDisplayed(leadsTitle, "Navigate to Leads page", "LeadsPage", "selectLeadRecord");
		} else {
			tcConfig.updateTestReporter("LeadsPage", "selectLeadRecord", Status.FAIL,
					"Unable to navigate to Leads page");
		}
		fieldEnterData(searchTextBox, testData.get("Lead"));
		sendKeyboardKeys(Keys.ENTER);
		clickElementBy(By.xpath("//a[@title='" + testData.get("Lead") + "']"));
		verifyPageTitleDisplayed(leadsPageTitle, "Navigated to Lead Details page", "LeadsPage", "selectLeadRecord");
		selectElement("LeadsPage", createOfferTransactionButton, "Create Offer Transaction Button");
	}

	/*
	 * Method: enterSalesChannelSelection Description: Enter sales channel
	 * selection details Date: September/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void enterSalesChannelSelection() {
		if (verifyObjectDisplayed(marketerTextBox)) {
			fieldEnterData(marketerTextBox, testData.get("SupervisorName"));
			clickElementBy(marketerTextvalue);
			fieldEnterData(salesTeamTextBox, testData.get("Teams"));
			clickElementBy(salesTeamValue);
			fieldEnterData(salesChannelTextBox, testData.get("SalesChannel"));
			clickElementBy(salesChannelValue);
			fieldEnterData(offerNameTextBox, testData.get("Offer"));
			clickElementBy(offerNameValue);
			// selectElement("createOfferTransaction", offerNameValue, "Offer
			// name from dropdown");
			selectElement("createOfferTransaction", nextButton, "Next Button in the Sales Channel Selection page");
		} else {
			tcConfig.updateTestReporter("Create Offer transaction", "enterSalesChannelSelection", Status.FAIL,
					"Unable to enter details in the sales channel selection screen");
		}
	}

	/*
	 * Method: enterMarketingStrategyDetails Description: Enter marketing
	 * strategy details Date: September/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void enterMarketingStrategyDetails() {
		if (verifyObjectDisplayed(marketingStrategyDropdown)) {
			selectByIndex(marketingStrategyDropdown, 3);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByIndex(marketingSourceTypeDropdown, 1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectByIndex(marketingCampaignDropdown, 2);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectElement("enterMarketingStrategyDetails", nextButton, "Next Button in the Marketing Strategy Selection page");
		} else {
			tcConfig.updateTestReporter("Create Offer transaction", "enterMarketingStrategyDetails", Status.FAIL,
					"Unable to enter details in the Marketing Strategy screen");
		}

	}

	/*
	 * Method: enterCreditCardDetails Description: Enter credit card details in
	 * the cybersource screen Val Date: September/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void enterCreditCardDetails() {
		if (verifyObjectDisplayed(vFrame)) {
			switchToFrame(vFrame);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			switchToFrame(cyberSourceFrame);
			clickElementJSWithWait(selectVisa);
			fieldEnterData(cardNumber, (testData.get("creditCard")));
			selectByIndex(expMonthDropdown, 5);
			selectByIndex(expYearDropdown, 3);
			verifyPageTitleDisplayed(payButton, "Payment Details", "Payment Page", "createOfferTransaction");
			clickElementJSWithWait(payButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.switchTo().defaultContent();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectElement("enterCreditCardDetails", nextButton, "Next Button in the Credit card details page");
		} else {
			tcConfig.updateTestReporter("Cybersource", "enterCreditCardDetails", Status.FAIL,
					"Unable to make credit card payment");
		}

	}

	/*
	 * Method: createOfferTransaction Description: Create Offer transaction Val
	 * Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void createOfferTransaction() {
		if (verifyObjectDisplayed(salesChannelSelectionPageTitle)) {
			verifyPageTitleDisplayed(salesChannelSelectionPageTitle, "Navigated to Sales Channel selection page",
					"Offer Transaction page", "createOfferTransaction");
			enterSalesChannelSelection();
			enterMarketingStrategyDetails();
			// scrollDownForElementJSBy(nextButton);
			checkExpirationMonth();
			checkAccommodationNight();
			enterOfferPrice();
			selectElement("createOfferTransaction", nextButton, "Next Button in the Offer Price Selection page");
			clickElementJSWithWait(offerSelectCheckbox);
			// clickElementJSWithWait(destinationCheckBox2);
			// verifyElementSelected(premium1Selected, "createOfferTransaction",
			// "Destination 1");
			selectElement("createOfferTransaction", nextButton, "Next Button in the Destination page");
			waitUntilElementInVisible(driver, spinnerContainer, 120);
			// selectElement("createOfferTransaction", nextButton, "Next Button
			// in the Accommodation page");
			chooseAccommodation();
			clickElementJSWithWait(premiumCheckBox1);
			verifyElementSelected(premium1Selected, "createOfferTransaction", "Premium 1");
			selectElement("createOfferTransaction", nextButton, "Next Button in the Select Premium page");
			verifyPageTitleDisplayed(nextButton, "Confirmation Details", "Confirmation Page", "createOfferTransaction");
			selectElement("createOfferTransaction", nextButton, "Next Button in the Confirmation page");
			waitUntilElementInVisible(driver, spinnerContainer, 120);
		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerRecord", Status.FAIL,
					"Unable to create Offer transaction");
		}

	}

	/*
	 * Method: checkAccommodationNight Description: Select Accommodation nights
	 * Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void checkExpirationMonth() {
		if (verifyObjectDisplayed(expirationMonthdropdown)) {
			selectByIndex(expirationMonthdropdown, 1);
			verifyElementSelected(expirationMonthValue, "OfferTransaction", "Expiration Month");
		} else if (verifyObjectDisplayed(expirationMonthDisabled)) {
			tcConfig.updateTestReporter("OfferTransaction", "checkExpirationMonth", Status.PASS,
					"Expiration date is selected at offer level");
		} else {
			tcConfig.updateTestReporter("OfferTransaction", "checkExpirationMonth", Status.FAIL,
					"Issue selecting Expiration Month");

		}
	}

	/*
	 * Method: checkAccommodationNight Description: Select Accommodation nights
	 * Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void checkAccommodationNight() {
		if (verifyObjectDisplayed(offerSelectCheckbox)) {
			clickElementJSWithWait(offerSelectCheckbox);
			verifyElementSelected(checkboxSelected, "OfferTransaction", "Nights checkbox");
		} else if (verifyObjectDisplayed(numberOfNightsdropdown)) {
			selectByIndex(numberOfNightsdropdown, 1);
		} else {
			tcConfig.updateTestReporter("OfferTransaction", "checkAccommodation", Status.FAIL,
					"Issue selecting accommodation nights");
		}
	}

	/*
	 * Method: enterOfferPrice Description: Enter Offer Price Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void enterOfferPrice() {
		if (verifyObjectDisplayed(offerPriceTextBox)) {
			String offerPrice = getElementAttribute(offerPriceTextBox, "min");
			// int i=Integer.parseInt(offerPrice);

			fieldEnterData(offerPriceTextBox, offerPrice);
			verifyPageTitleDisplayed(nextButton, "Data entered in the Offer Price selection",
					"Offer Price Selection Page", "createOfferTransaction");
		} else {
			tcConfig.updateTestReporter("OfferTransaction", "enterOfferPrice", Status.FAIL,
					"Issue in entering offer price");
		}
	}

	/*
	 * Method: ChoosePayment Description: Choose payment option Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseCreditCardPayment() {
		if (verifyObjectDisplayed(creditCardRadioButton)) {
			clickElementJSWithWait(creditCardRadioButton);
			selectElement("createOfferTransaction", nextButton, "Next Button in the Payment Options page");
			waitUntilElementInVisible(driver, spinnerContainer, 120);
			enterCreditCardDetails();
			clickElementBy(verifierInfoTextBox);
			clickElementBy(verifierInfoAgentValue);
			verifyPageTitleDisplayed(nextButton, "Details entered in Verifier information", "Verifier information Page",
					"createOfferTransaction");
			selectElement("chooseCreditCardPayment", nextButton, "Next Button in the Verifier information page");
			verifyPageTitleDisplayed(offerTransactionNumber, "Offer Transaction created successfully",
					"Offer transaction details", "chooseCreditCardPayment");
		} else {
			tcConfig.updateTestReporter("Payment options", "chooseCreditCardPayment", Status.FAIL,
					"Unable to enter credit card payment");
		}
	}

	/*
	 * Method: ChooseManualPayment Description: Choose manual payment option
	 * Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseManualPayment() {
		if (verifyObjectDisplayed(manualPaymentRadioButton)) {
			clickElementJSWithWait(manualPaymentRadioButton);
			selectElement("chooseManualPayment", nextButton, "Next Button in the Payment Options page");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifyPageTitleDisplayed(nextButton, "Details entered in Manual Payment", "Manual Payment Page",
					"chooseManualPayment");
			selectElement("chooseManualPayment", nextButton, "Next Button in the Manual Payment page");
			clickElementBy(verifierInfoTextBox);
			clickElementBy(verifierInfoAgentValue);
			verifyPageTitleDisplayed(nextButton, "Details entered in Verifier information", "Verifier information Page",
					"chooseManualPayment");
			selectElement("chooseManualPayment", nextButton, "Next Button in the Verifier information page");
			verifyPageTitleDisplayed(offerTransactionNumber, "Offer Transaction created successfully",
					"Offer transaction details", "chooseManualPayment");
		} else {
			tcConfig.updateTestReporter("Payment options", "chooseManualPayment", Status.FAIL,
					"Unable to enter manual payment");
		}
	}

	/*
	 * Method: cancelOfferTransaction Description: Cancel Offer transaction
	 * Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void cancelOfferTransaction() {
		if (verifyObjectDisplayed(offerTransactionMoreButton)) {
			clickElementJSWithWait(offerTransactionMoreButton);
			verifyPageTitleDisplayed(cancelOfferTransactionButton, "Cancel Offer Transaction button displayed",
					"moreOptions", "cancelOfferTransaction");
			selectElement("cancelOfferTransaction", cancelOfferTransactionButton, "Cancel Offer Transaction Button");
			clickElementJSWithWait(yesRadioButton);
			clickElementJSWithWait(cancelReasonDropdown);
			clickElementBy(cancelReasonValue);
			selectElement("cancelOfferTransaction", nextButton, "Next Button in the Cancel Reason page");
		} else {
			tcConfig.updateTestReporter("Cancel Offer Transaction", "cancelOfferTransaction", Status.FAIL,
					"Unable to cancel Offer transaction");
		}

	}

	/*
	 * Method: enterPaymentProcessorDetails Description: Enter Payment Processor
	 * Details Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	public void enterPaymentProcessorDetails() {
		if (verifyObjectDisplayed(paymentProcessorTransactionId)) {
			fieldEnterData(paymentProcessorTransactionId, getRandomString(5));
			fieldEnterData(paymentText, getRandomStringText(5));
			verifyPageTitleDisplayed(nextButton, "Payment Processor details entered", "Payment Processor details",
					"enterPaymentProcessorDetails");
		} else {
			tcConfig.updateTestReporter("Payment Processor details", "enterPaymentProcessorDetails", Status.FAIL,
					"Unable to cancel Offer transaction");
		}

	}

	/*
	 * Method: refundToOriginalCard Description: Refund to Original card Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void refundToOriginalCard() {
		if (verifyObjectDisplayed(refundOriginalCardOption)) {
			clickElementJSWithWait(refundOriginalCardOption);
			verifyPageTitleDisplayed(nextButton, "Refund Option selected", "Refund option Selection Page",
					"refundToOriginalCard");
			waitUntilElementInVisible(driver, spinnerContainer, 120);
			selectElement("cancelOfferTransaction", nextButton, "Next Button in the refund options page");
			verifyPageTitleDisplayed(cancelSuccessMessage, "Cancel Success message displayed",
					"Refund option Selection Page", "refundToOriginalCard");
			selectElement("refundToOriginalCard", nextButton, "Next Button in the Cancel Success page");
			verifyProgressStatus(cancelledTabActive, "Cancel Offer Transaction", "refundToOriginalCard");

		} else {
			tcConfig.updateTestReporter("Cancel Offer Transaction", "refundToOriginalCard", Status.FAIL,
					"Unable to cancel Offer transaction");
		}

	}

	/*
	 * Method: refundToAlternateCard Description: Refund to Alternate card Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void refundToAlternateCard() {
		if (verifyObjectDisplayed(refundAlternateCardOption)) {
			clickElementJSWithWait(refundAlternateCardOption);
			verifyPageTitleDisplayed(nextButton, "Refund to Alternate card Option selected",
					"Refund option Selection Page", "refundToAlternateCard");
			selectElement("refundToAlternateCard", nextButton, "Next Button in the Refund option Selection Page");
			waitUntilElementInVisible(driver, spinnerContainer, 180);
			enterCreditCardDetails();
			verifyPageTitleDisplayed(cancelSuccessMessage, "Cancel Success message displayed",
					"Refund option Selection Page", "refundToAlternateCard");
			selectElement("refundToAlternateCard", nextButton, "Next Button in the Cancel Success page");
			verifyProgressStatus(cancelledTabActive, "Cancel Offer Transaction", "refundToAlternateCard");

		} else {
			tcConfig.updateTestReporter("Cancel Offer Transaction", "refundToAlternateCard", Status.FAIL,
					"Unable to cancel Offer transaction");
		}

	}

	/*
	 * Method: changeBasePremiumError Description: Validate change Base Premium
	 * error Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */

	public void changeBasePremiumError() {
		if (verifyObjectDisplayed(offerTransactionMoreButton)) {
			clickElementJSWithWait(offerTransactionMoreButton);
			verifyPageTitleDisplayed(changeBasePremiumButton, "Change Base Premium button displayed", "moreOptions",
					"changeBasePremium");
			selectElement("offerTransaction", changeBasePremiumButton, "Change Base Premium Button");
			verifyPageTitleDisplayed(toastErrorMessage, "Error message displayed - Unable to Change Base Premium.",
					"offerTransaction", "changeBasePremium");

		} else {
			tcConfig.updateTestReporter("offerTransaction", "changeBasePremiumError", Status.FAIL,
					"Issue while trying to Change base Premium");
		}

	}

	/*
	 * Method: checkTransactionStatus Description: Check the status of
	 * Transaction Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void checkTransactionStatus() {
		if (verifyObjectDisplayed(pendingPaymentsTabActive)) {
			sendKeyboardKeys(Keys.F5);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyObjectDisplayed(pendingPaymentsTabActive)) {
				sendKeyboardKeys(Keys.F5);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
			}
		} else if (verifyObjectDisplayed(activeTab)) {
			tcConfig.updateTestReporter("offerTransaction", "checkTransactionStatus", Status.PASS,
					"Payment is successful, Active tab displayed.");

		} else {
			tcConfig.updateTestReporter("offerTransaction", "checkTransactionStatus", Status.FAIL,
					"Cannot Proceed further, as the Transaction is not Active");
		}

	}

	/*
	 * Method: changeBasePremium Description: Change Base Premium Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void changeBasePremium() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(offerTransactionMoreButton)) {
			clickElementJSWithWait(offerTransactionMoreButton);
			verifyPageTitleDisplayed(changeBasePremiumButton, "Change Base Premium button displayed", "moreOptions",
					"changeBasePremium");
			selectElement("offerTransaction", changeBasePremiumButton, "Change Base Premium Button");
			premium1Value = getElementAttribute(premiumName1, "title");
			objMap.put("premium1", premium1Value);
			premium2Value = getElementAttribute(premiumName2, "title");
			objMap.put("premium2", premium2Value);
			verifyElementSelected(premium1Selected, "changeBasePremium", "Premium 1 ");
			clickElementJSWithWait(premiumCheckBox1);
			clickElementJSWithWait(premiumCheckBox2);
			verifyElementSelected(premium2Selected, "changeBasePremium", "Premium 1 deselected and new Premium 2 ");
			//selectElement("changeBasePremium", nextButton, "Next Button in the Change Base Premium");
			clickElementJSWithWait(nextButton);
		} else {
			tcConfig.updateTestReporter("offerTransaction", "changeBasePremium", Status.FAIL,
					"Issue while trying to Change base Premium");
		}

	}

	/*
	 * Method: overrideCustomerPrice Description: Override customer Price while
	 * changing the destination Date: September/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */

	public void overrideCustomerPrice() {
		if (verifyObjectDisplayed(customerPrice)) {
			customerPriceValue = getElementAttribute(customerPrice, "max");
			double inum = Double.parseDouble(customerPriceValue);
			double newInum = inum - 10;
			newCustomerPrice = String.valueOf(newInum);
			objMap.put("custPrice", newCustomerPrice);
			fieldEnterData(customerPriceBox, objMap.get("custPrice"));
			selectElement("overrideCustomerPrice", overrideButton, "Override Button in the Destination change page ");
			fieldEnterData(overridePassword, testData.get("OverrideCode"));
			selectElement("Override", submitButton, "Submit Button in the Override price page ");
			verifyPageTitleDisplayed(toastSuccessMessage, "Customer Price Override success", "ChangeDestination",
					"overrideCustomerPrice");
		} else {
			tcConfig.updateTestReporter("ChangeDestination", "selectPremium", Status.FAIL,
					"Issue while trying to override customer price");
		}

	}

	public void selectPremiumNew(By valueToSelect, By selectedValue, String premiumName) {
		if (verifyObjectDisplayed(valueToSelect)) {
			clickElementJSWithWait(valueToSelect);
			verifyElementSelected(selectedValue, "changeDestination", premiumName);
		} else {
			tcConfig.updateTestReporter("ChangeDestination", "selectPremium", Status.FAIL,
					"Issue while trying to select Premium");
		}

	}

	/*
	 * Method: selectPremium Description: Select Premium while changing the
	 * destination Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void selectPremium(String premiumValue, String premiumName) {
		if (verifyObjectDisplayed(premiumCheckBox1)) {
			premiumValue = getElementAttribute(premiumName1, "title");
			clickElementJSWithWait(premiumCheckBox1);
			verifyElementSelected(premium1Selected, "selectPremium", premiumName);

		} else {
			tcConfig.updateTestReporter("ChangeDestination", "selectPremium", Status.FAIL,
					"Issue while trying to select Premium");
		}
	}

	/*
	 * Method: navigateToChangeDestination Description: Change destination from
	 * the offer transaction Date: September/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void navigateToChangeDestination() {
		/*
		 * clickElementJSWithWait(oTtab); clickElementBy(OfferT);
		 */
		String destination1Value = "";
		String destination2Value = "";
		String destination3Value = "";
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(offerTransactionMoreButton)) {
			clickElementJSWithWait(offerTransactionMoreButton);
			verifyPageTitleDisplayed(changeDestinationButton, "Change Destination button displayed", "moreOptions",
					"changeDestination");
			selectElement("offerTransaction", changeDestinationButton, "Change Destination dropdown link");
			destination1Value = getElementAttribute(destinationName1, "title");
			destination2Value = getElementAttribute(destinationName2, "title");
			destination3Value = getElementAttribute(destinationName2, "title");
			objMap.put("destination1", destination1Value);
			objMap.put("destination2", destination2Value);
			objMap.put("destination3", destination3Value);
		} else {
			tcConfig.updateTestReporter("offerTransaction", "changeDestination", Status.FAIL,
					"Issue while trying to Choose Destination");
		}
	}

	/*
	 * Method: selectDestination Description: Select destination from the offer
	 * transaction Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */
	public void selectDestination(By valueToSelect, By selectedValue, String name) {
		if (verifyObjectDisplayed(valueToSelect)) {
			clickElementJSWithWait(valueToSelect);
			verifyElementSelected(selectedValue, "changeDestination", name);
		} else {
			tcConfig.updateTestReporter("offerTransaction", "changeDestination", Status.FAIL,
					"Issue while trying to select Destination");
		}
	}

	/*
	 * Method: changeDestination Description: Change destination from the offer
	 * transaction Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */

	public void changeDestination() {
		navigateToChangeDestination();
		verifyElementSelected(destination1Selected, "changeDestination",
				"Existing Destination 1 displayed as auto-selected ");
		selectDestination(destinationCheckBox2, destination2Selected, objMap.get("destination2"));
		selectElement("changeDestination", nextButton, "Next Button in the Change Destination");
		selectPremiumNew(premiumCheckBox1, premium1Selected, objMap.get("premium1"));
		selectElement("changeBasePremium", saveButton, "Save Button in the Select Premium page ");
		verifyPageTitleDisplayed(toastSuccessMessage, "Destination added successfully", "ChangeDestination",
				"selectPremium");
		navigateToChangeDestination();
		selectDestination(destinationCheckBox3, destination3Selected, objMap.get("destination3"));
		overrideCustomerPrice();
		selectElement("changeDestination", nextButton, "Next Button in the Change Destination");
		selectPremiumNew(premiumCheckBox2, premium2Selected, objMap.get("premium2"));
		selectElement("changeBasePremium", saveButton, "Save Button in the Select Premium page ");
		verifyPageTitleDisplayed(toastSuccessMessage, "Destination added successfully", "ChangeDestination",
				"selectPremium");
		waitUntilElementInVisible(driver, toastSuccessMessage, 120);
		clickElementBy(relatedTab);
		validateActiveLineItem(objMap.get("premium1"), objMap.get("activeLineValueID"), "Premium");
		clickElementBy(activeOTBreadcrumbLink);
		clickElementBy(relatedTab);
		validateInactiveLineItem(objMap.get("premium2"), objMap.get("inactiveLinePremium"), "Premium");
	}

	/*
	 * Method: validateOfferTransaction Description: Validate Offer Transaction
	 * details Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By:
	 * NA
	 */
	public void validateOfferTransaction() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(relatedTab)) {
			selectElement("OfferTransaction Related Tab", relatedTab, "Related Tab");
			validateActiveLineItem(premium2Value, activePremium2Value, "Premium");
			waitUntilElementVisibleBy(driver, relatedTab, "ExplicitMedWait");
			clickElementBy(activeOTBreadcrumbLink);
			clickElementBy(relatedTab);
			validateInactiveLineItem(premium1Value, inactivePremium1Value, "Premium");
			clickElementBy(inactiveOTBreadcrumbLink);
			/*
			 * clickElementBy(relatedTab); validateOTHistoryLineItem();
			 */
		} else {
			tcConfig.updateTestReporter("TransactionDetails", "validateOfferTransaction", Status.FAIL,
					"Issue while validating Offer Transaction details");
		}
	}

	/*
	 * Method: validateOfferTransactionNew Description: Validate Offer
	 * Transaction details Date: September/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void validateOfferTransactionNew(String oldValue, String newValue, String type) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(relatedTab)) {
			selectElement("OfferTransaction Related Tab", relatedTab, "Related Tab");
			validateActiveLineItem(newValue, activePremium2Value, type);
			clickElementBy(activeOTBreadcrumbLink);
			clickElementBy(relatedTab);
			validateInactiveLineItem(oldValue, inactivePremium1Value, type);
			clickElementBy(inactiveOTBreadcrumbLink);
			/*
			 * clickElementBy(relatedTab); validateOTHistoryLineItem();
			 */
		} else {
			tcConfig.updateTestReporter("TransactionDetails", "validateOfferTransactionNew", Status.FAIL,
					"Issue while validating Offer Transaction details");
		}
	}

	/*
	 * Method: validateActiveLineItem Description: Validate Premium displayed
	 * under Active Line item Date: September/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void validateActiveLineItem(String value, String activeLineValue, String type) {
		selectElement("OfferTransaction Related Tab", activeLineItemLink, "ActiveLine Item link");
		activeLineValue = getElementAttribute(activeLinePremium, "title");
		objMap.put("activeLineValueID", activeLineValue);
		if (value.equalsIgnoreCase(activeLineValue)) {
			tcConfig.updateTestReporter("TransactionDetails", "validateActiveLineItem", Status.PASS,
					type + ": " + activeLineValue + " displayed under Active Line item");
		} else {
			tcConfig.updateTestReporter("TransactionDetails", "validateActiveLineItem", Status.FAIL,
					type + ": " + activeLineValue + " not displayed under Active Line item");
		}
	}

	public void validateActiveLineItemRes(By item, String type) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(relatedTab)) {
			selectElement("OfferTransaction Related Tab", relatedTab, "Related Tab");
			Assert.assertTrue(verifyObjectDisplayed(item),
					type + " not displayed under Active Line Item");
			tcConfig.updateTestReporter("OfferTransactionPage", "validateActiveLineItemRes", Status.PASS,
					type + " displayed under Active Line Item");

		} else {
			tcConfig.updateTestReporter("OfferTransactionPage", "validateActiveLineItemRes", Status.FAIL,
					"Unable to navigate to Related tab");
		}
	}

	/*
	 * Method: validateInactiveLineItem Description: Validate Premium displayed
	 * under Inactive Line item Date: September/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void validateInactiveLineItem(String value, String inactiveLineValue, String type) {
        waitForSometime(tcConfig.getConfig().get("MedWait"));
        scrollDownByPixel(300);       
		selectElement("OfferTransaction Related Tab", inactiveLineItemLink, "InactiveLine Item link");
		inactiveLineValue = getElementAttribute(inactiveLinePremium, "title");
		objMap.put("inactiveLinePremium", inactiveLineValue);
		if (value.equalsIgnoreCase(inactiveLineValue)) {
			tcConfig.updateTestReporter("TransactionDetails", "validateInactiveLineItem", Status.PASS,
					type  + ": " + inactiveLineValue + " displayed under Inactive Line item");
		} else {
			tcConfig.updateTestReporter("TransactionDetails", "validateInactiveLineItem", Status.FAIL,
					type + ": " + inactiveLineValue + " not displayed under Inactive Line item");
		}
	}

	/*
	 * Method: validateOTHistoryLineItem Description: Validate Premium displayed
	 * under Transaction History Line item Date: September/2020 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	/*
	 * Method: processPayment Description: Validate Premium displayed under
	 * Active Line item Date: September/2020 Author: Krishnaraj Kaliyamoorthy
	 * Changes By: NA
	 */
	public void processPayment() {
		if (verifyObjectDisplayed(processPaymentButton)) {
			clickElementJSWithWait(processPaymentButton);
			clickElementJSWithWait(refundOriginalCardOption);
			verifyPageTitleDisplayed(nextButton, "Pay from Original Card", "Refund option", "refundToOriginalCard");
			selectElement("processPayment", nextButton, "Next Button in the Process Payment page");
			verifyPageTitleDisplayed(toastSuccessMessage, "Payment processed successfully", "ChangeDestination",
					"processPayment");
		} else {
			tcConfig.updateTestReporter("TransactionDetails", "validateActiveLineItem", Status.FAIL,
					"Issue while making payment");
		}
	}

	/*
	 * Method: chooseAccommodation Description: Select Accommodation while
	 * creating the offer transaction Date: September/2020 Author: Krishnaraj
	 * Kaliyamoorthy Changes By: NA
	 */
	public void chooseAccommodation() {
		if (verifyObjectDisplayed(nextButton)) {
			selectElement("createOfferTransaction", nextButton, "Next Button in the Accommodation page");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "changeBasePremium", Status.FAIL,
					"Accommodation not available to select");
		}

	}

	/*
	 * Description: Validate the Balance due on Offer Transaction on doing
	 * CahrgeBack Date: Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void validateChargebackBalanceDue() throws AWTException {
		clickElementJSWithWait(tabRelated);
		scrollDownForElementJSBy(lnkPaymentRecords);
		waitUntilElementVisibleBy(driver, lnkPaymentRecords, "ExplicitLongWait");
		clickElementJSWithWait(lnkPaymentRecords);
		waitUntilElementVisibleBy(driver, txtPaymentRecordsPage, "ExplicitLongWait");
		verifyPageDisplayed(txtPaymentRecordsPage, "Page Title", "OfferTransactionsPage",
				"validateChargebackBalanceDue");
		getListElement(tblPayemntRecords);
		verifyPageDisplayed(paymentPage, "Page Title", "OfferTransactionsPage", "validateChargebackBalanceDue");
		waitUntilElementVisibleBy(driver, btnChargeBack, "ExplicitMedWait");
		clickElementBy(btnChargeBack);
		verifyPageDisplayed(extnErrorMsg, "Page Title", "OfferTransactionsPage", "validateChargebackBalanceDue");
		refreshPage();
		waitUntilElementVisibleBy(driver, txtAmount, "ExplicitLongWait");
		strAmount = getElementValue(txtAmount);
		objMap.put("Amount", strAmount); 
		waitUntilElementVisibleBy(driver, hypOfferTrnsac, "ExplicitMedWait");
		clickElementBy(hypOfferTrnsac);
		waitUntilElementVisibleBy(driver, txtBalanceDueValue, "ExplicitLongWait");
		String strBalance = getObject(txtBalanceDueValue).getText();
		if (strAmount.equals(strBalance)) {
			tcConfig.updateTestReporter("offerTransactionPage", "validateChargebackBalanceDue", Status.PASS,
					"Balance due is displayed sucessfully");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateChargebackBalanceDue", Status.FAIL,
					"Failed while displaying Balance Due");
		}

	}

	protected By RelatedTab = By.xpath("(//span[contains(.,'Related')])[1]");

	/*
	 * Description: Validate the Balance due on Offer Transaction Date:
	 * Sept/2020 Author: Jyoti Changes By: NA
	 */
	public void validateBalanceDue() throws AWTException {
		clickElementJSWithWait(RelatedTab);
		scrollDownForElementJSBy(lnkPaymentRecords);
		waitUntilElementVisibleBy(driver, lnkPaymentRecords, "ExplicitLongWait");
		clickElementJSWithWait(lnkPaymentRecords);
		verifyPageDisplayed(txtPaymentRecordsPage, "Page Title", "OfferTransactionsPage",
				"validateChargebackBalanceDue");
		getListElement(tblPayemntRecords);
		verifyPageDisplayed(paymentPage, "Page Title", "OfferTransactionsPage", "validateChargebackBalanceDue");
		/*
		 * strAmount = getElementValue(txtAmount); objMap.put("Amount",
		 * strAmount);
		 */
		waitUntilElementVisibleBy(driver, hypOfferTrnsac, "ExplicitMedWait");
		clickElementBy(hypOfferTrnsac);
		waitUntilElementVisibleBy(driver, txtBalanceDueValue, "ExplicitLongWait");
		if (verifyObjectDisplayed(txtBalanceDueValue)) {
			tcConfig.updateTestReporter("offerTransactionPage", "validateChargebackBalanceDue", Status.PASS,
					"Balance due is displayed sucessfully");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateChargebackBalanceDue", Status.FAIL,
					"Failed while displaying Balance Due");
		}

	}

	/*
	 * Description: To search using Customer name Date: January/2021 Author:
	 * Jyoti Changes By: NA
	 */
	public void searchByCustomerName() {
		waitUntilElementVisibleBy(driver, txtOfferTransactionPage, "ExplicitMedWait");
		verifyPageDisplayed(txtOfferTransactionPage, "Page Title", "OfferTransactionsPage", "searchByCustomerName");
		fieldDataEnter(txtSearchOfferTransaction, testData.get("Lead"));
		sendKeyboardKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, txtSearchResults, "ExplicitMedWait");
		verifyPageDisplayed(txtSearchResults, "Page Title", "OfferTransactionsPage", "searchByCustomerName");
		List<WebElement> allOptions = getList(custmerNameSearched);
		if (allOptions.size() > 0) {
			for (WebElement we : allOptions) {
				assertTrue(verifyObjectDisplayed(we), "List of Offer Transaction number displayed");
				if (we.getText() != null) {
					tcConfig.updateTestReporter("offerTransactionPage", "searchByCustomerName", Status.PASS,
							"Customer Name is:- " + we.getText());
				}
			}
		}
	}

	/*
	 * Description: To search using Customer Phone Number Date: January/2021
	 * Author: Jyoti Changes By: NA
	 */
	public void searchByCustomerPhone() {
		waitUntilElementVisibleBy(driver, txtOfferTransactionPage, "ExplicitMedWait");
		verifyPageDisplayed(txtOfferTransactionPage, "Page Title", "OfferTransactionsPage", "searchByCustomerPhone");
		fieldDataEnter(txtSearchOfferTransaction, testData.get("PhoneNumber"));
		sendKeyboardKeys(Keys.ENTER);
		waitUntilElementVisibleBy(driver, txtSearchResults, "ExplicitMedWait");
		verifyPageDisplayed(txtSearchResults, "Page Title", "OfferTransactionsPage", "searchByCustomerPhone");
		List<WebElement> allOptions = getList(tblOfferTransaction);
		if (allOptions.size() > 0) {
			for (WebElement we : allOptions) {
				assertTrue(verifyObjectDisplayed(we), "List of Offer Transaction number displayed");
				if (we.getText() != null) {
					tcConfig.updateTestReporter("offerTransactionPage", "searchByCustomerPhone", Status.PASS,
							"Customer Phone number is:- " + we.getText());
				}
			}
		}
	}

	/*
	 * Description: To search using Customer Email Date: January/2021 Author:
	 * Jyoti Changes By: NA
	 */
	public void searchByCustomerEmail() {
		waitUntilElementVisibleBy(driver, txtOfferTransactionPage, "ExplicitMedWait");
		verifyPageDisplayed(txtOfferTransactionPage, "Page Title", "OfferTransactionsPage", "searchByCustomerPhone");
		fieldDataEnter(txtSearchOfferTransaction, testData.get("Email"));
		sendKeyboardKeys(Keys.ENTER);
		waitUntilElementVisibleBy(driver, txtSearchResults, "ExplicitMedWait");
		verifyPageDisplayed(txtSearchResults, "Page Title", "OfferTransactionsPage", "searchByCustomerPhone");
		List<WebElement> allOptions = getList(custmerEmailSearched);
		if (allOptions.size() > 0) {
			for (WebElement we : allOptions) {
				assertTrue(verifyObjectDisplayed(we), "List of Offer Transaction number displayed");
				if (we.getText() != null) {
					tcConfig.updateTestReporter("offerTransactionPage", "searchByCustomerEmail", Status.PASS,
							"Customer Email is:- " + we.getText());
				}
			}
		}
	}

	/*
	 * Description: To search using Offer Transaction Number Date: January/2021
	 * Author: Jyoti Changes By: NA
	 */
	public void searchByOfferTransactionNumber() {
		waitUntilElementVisibleBy(driver, txtOfferTransactionPage, "ExplicitMedWait");
		verifyPageDisplayed(txtOfferTransactionPage, "Page Title", "OfferTransactionsPage", "searchByOfferTransactionNumber");
		fieldDataEnter(txtSearchOfferTransaction, testData.get("OfferTransactionNumber"));
		sendKeyboardKeys(Keys.ENTER);
		waitUntilElementVisibleBy(driver, txtSearchResults, "ExplicitMedWait");
		verifyPageDisplayed(txtSearchResults, "Page Title", "OfferTransactionsPage", "searchByOfferTransactionNumber");
		List<WebElement> allOptions = getList(tblOfferTransaction);
		if (allOptions.size() > 0) {
			for (WebElement we : allOptions) {
				assertTrue(verifyObjectDisplayed(we), "List of Offer Transaction number displayed");
				if (we.getText() != null) {
					tcConfig.updateTestReporter("offerTransactionPage", "searchByOfferTransactionNumber", Status.PASS,
							"Offer Transaction number is:- " + we.getText());
				}
			}
		}
	}

	protected By offerTransactions = By.xpath(
			"//a[contains(text(),'Offer Transactions') and contains(@class,'slds-page-header__title slds-text-color--default slds-show--inline-block uiOutputURL')]");

	/*
	 * Description: To search using Call Record Number 1 of any offer
	 * transaction Date: January/2021 Author: Jyoti Changes By: NA
	 */
	public void searchCallRecordNumber() {
		String callRecordNum;
		scrollDownForElementJSBy(tabSearchAgent);
		waitUntilElementVisibleBy(driver, tabSearchAgent, "ExplicitMedWait");
		if (verifyObjectDisplayed(txtCallRecordNum)) {
			callRecordNum = getElementText(txtCallRecordNum);
			waitUntilElementVisibleBy(driver, txtSearchResults, "ExplicitMedWait");
			clearElementBy(txtSearchBox);
			fieldDataEnter(txtSearchBox, callRecordNum);
			sendKeyboardKeys(Keys.ENTER);
			tcConfig.updateTestReporter("offerTransactionPage", "searchCallRecordNumber", Status.PASS,
					"User is able to search using Call recording number present in Offer Transaction");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "searchCallRecordNumber", Status.FAIL,
					"Offer Transaction doesnot have Call recording number. Select Offer Transaction having Call recording number");
		}
		waitUntilElementVisibleBy(driver, txtSearchResults, "ExplicitMedWait");
		verifyPageDisplayed(txtSearchResults, "Page Title", "OfferTransactionsPage", "searchCallRecordNumber");
		scrollDownForElementJSBy(offerTransactions);
		List<WebElement> allOptions = getList(tblOfferTransaction);
		if (allOptions.size() > 0) {
			for (WebElement we : allOptions) {
				if (we.getText() != null) {
					tcConfig.updateTestReporter("offerTransactionPage", "searchCallRecordNumber", Status.PASS,
							"Offer Transaction number is:- " + we.getText());
				}
			}
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "searchCallRecordNumber", Status.FAIL,
					"Failed while validating related Offer Transaction");
		}
	}

	/*
	 * Description: To compare the OFfer Expiration date with System date Date:
	 * January/2021 Author: Jyoti Changes By: NA
	 */
	public void compareDates() {
		final String stringDate = getElementText(txtOfferExpirationDt);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = new Date();
		String strCurrDate = sdf.format(now);
		System.out.println("Current Date: " + strCurrDate);
		Date date1 = null;
		try {
			date1 = sdf.parse(strCurrDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date date2 = null;
		try {
			date2 = sdf.parse(stringDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("date1 : " + sdf.format(date1));
		System.out.println("date2 : " + sdf.format(date2));
		if (date1.compareTo(date2) < 0) {
			tcConfig.updateTestReporter("offerTransactionPage", "compareDates", Status.PASS,
					"Expiration Date is greater than system date");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "compareDates", Status.FAIL,
					"Failed while validating Offer Expiration date with System date");
		}
	}

	/*
	 * Method: validateOTHistoryLineItem Description: Validate Premium displayed
	 * under Transaction History Line item Date: September/2020 Author:
	 * Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	
	public void validateOTHistoryLineItem() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollDownForElementJSBy(pendingLineItemLink);
		selectElement("OfferTransaction Related Tab", offerTransactionHistoryLink, "offer Transaction History Link");
		cancelledPremium1Value = getElementAttribute(cancelledPremiumOTHistory, "title");
		if (premium1Value.equalsIgnoreCase(cancelledPremium1Value)) {
			tcConfig.updateTestReporter("TransactionDetails", "validateInactiveLineItem", Status.PASS,
					"Premium " + premium1Value + " displayed under offer Transaction History");
		} else {
			tcConfig.updateTestReporter("TransactionDetails", "validateInactiveLineItem", Status.FAIL,
					"Premium " + premium1Value + " not displayed under offer Transaction History");
		}
	}

	/*
	 * Description: To compare the OFfer Expiration date with System date Date:
	 * January/2021 Author: Jyoti Changes By: NA
	 */
	public void validateBookTour() {
		waitUntilElementVisibleBy(driver, tabBookTour, "ExplicitMedWait");
		clickElementJSWithWait(tabBookTour);
		verifyPageDisplayed(rdBtn, "Page Title", "OfferTransactionsPage", "validateBookTour");
		WebElement chk = driver.findElement(rdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions actChk = new Actions(driver);
		actChk.moveToElement(chk).click().build().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// clickElementBy(rdBtn);
		clickElementBy(btnNext);
		scrollDownJavaScriptExecutor(btnGetAppointment);
		verifyPageDisplayed(btnGetAppointment, "Page Title", "OfferTransactionsPage", "validateBookTour");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(btnGetAppointment);
		waitUntilElementVisibleBy(driver, pgTimeSlot, "ExplicitMedWait");
		verifyPageDisplayed(pgTimeSlot, "Page Title", "OfferTransactionsPage", "validateBookTour");
		clickElementBy(btnSelectSlot);
		verifyPageDisplayed(txtConfirmationMsg, "Page Title", "OfferTransactionsPage", "validateBookTour");
		clickElementBy(btnYes);
		verifyPageDisplayed(tourSucessMsg, "Page Title", "OfferTransactionsPage", "validateBookTour");
		clickElementBy(btnOk);
		verifyPageDisplayed(tabReservationSummary, "Page Title", "OfferTransactionsPage", "validateBookTour");
		clickElementBy(tabReservationSummary);
		scrollDownJavaScriptExecutor(hyplnkTourID);
		verifyPageDisplayed(hyplnkTourID, "Page Title", "OfferTransactionsPage", "validateBookTour");
		clickElementBy(hyplnkTourID);
		verifyPageDisplayed(hyplnkTourID, "Page Title", "OfferTransactionsPage", "validateBookTour");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// SwitchtoWindow();

	}

	protected By txtFrom = By.xpath("(//span[contains(.,'From')]/following::input[@class=' input'])[1]");
	protected By txtTo = By.xpath("(//span[contains(.,'To')]/following::input[@class=' input'])[2]");

	/*
	 * Description: To compare the OFfer Expiration date with System date Date:
	 * January/2021 Author: Jyoti Changes By: NA
	 */
	public void bookTourSelectDates() {
		waitUntilElementVisibleBy(driver, tabBookTour, "ExplicitMedWait");
		clickElementJSWithWait(tabBookTour);
		verifyPageDisplayed(rdBtn, "Page Title", "OfferTransactionsPage", "bookTourSelectDates");
		WebElement chk = driver.findElement(rdBtn);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Actions actChk = new Actions(driver);
		actChk.moveToElement(chk).click().build().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(btnNext);
		verifyPageDisplayed(btnGetAppointment, "Page Title", "OfferTransactionsPage", "bookTourSelectDates");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(txtFrom);
		selectSystemDate(lnkToday);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(txtTo);
		selectSystemDate(lnkToday);
		clickElementBy(btnGetAppointment);
		waitUntilElementVisibleBy(driver, pgTimeSlot, "ExplicitMedWait");
		verifyPageDisplayed(pgTimeSlot, "Page Title", "OfferTransactionsPage", "bookTourSelectDates");
		clickElementBy(btnSelectSlot);
		verifyPageDisplayed(txtConfirmationMsg, "Page Title", "OfferTransactionsPage", "bookTourSelectDates");
		clickElementBy(btnYes);
		verifyPageDisplayed(tourSucessMsg, "Page Title", "OfferTransactionsPage", "bookTourSelectDates");
		clickElementBy(btnOk);
		verifyPageDisplayed(tabReservationSummary, "Page Title", "OfferTransactionsPage", "bookTourSelectDates");
		clickElementBy(tabReservationSummary);
		scrollDownJavaScriptExecutor(hyplnkTourID);
		verifyPageDisplayed(hyplnkTourID, "Page Title", "OfferTransactionsPage", "bookTourSelectDates");
		clickElementBy(hyplnkTourID);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String parentElement = driver.getWindowHandle();
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windowHandles.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: validate the SalesStore and SaleStoreAppoitnment location Date:
	 * February/2020 Author: Jyoti Changes By: NA
	 */
	public void getSalesStoreAndAppointment() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		verifyPageDisplayed(txtSalesStore, "Page Title", "Tour Record Page", "getSalesStoreAndAppointment");
		if (verifyObjectDisplayed(txtSalesStore) && verifyObjectDisplayed(txtSaleStoreAppoint)) {
			tcConfig.updateTestReporter("offerTransactionPage", "getSalesStoreAndAppointment", Status.PASS,
					"SalesStore displayed as :-" + getElementText(txtSalesStore)
							+ " and Sales store Appointment displayed as:- " + getElementText(txtSaleStoreAppoint));
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "getSalesStoreAndAppointment", Status.FAIL,
					"SalesStore and Salestore appoitnment time not displayed");

		}
	}

	/*
	 * Method: Switch back TO Parent Page Date: February/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void switchToParent() {
		driver.close();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().window(windows.get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Description: To store the Number of Offer Nights Date: January/2021
	 * Author: Jyoti Changes By: NA
	 */
	public void storeNoOfNight() {
		scrollDownForElementJSBy(tabOfferInfo);
		waitUntilElementVisibleBy(driver, tabOfferInfo, "ExplicitMedWait");
		noOFNight = getElementText(txtNoOfNights);
		scrollUpByPixel(1000);
	}

	/*
	 * Description: To compare the Offer Expiration date with System date Date:
	 * January/2021 Author: Jyoti Changes By: NA
	 */
	public void validateBookReservation() {
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		clickElementJSWithWait(btnNext);
		changeCalenderAvailiability();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		validateCheckInDate();
		validateCheckOutDate();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		getListElement(btnSelectAccom);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		getListElement(lblSelectAccom);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		scrollDownForElementJSBy(btnNextPayment);
		if (verifyObjectDisplayed(btnNextPayment)) {
			clickElementJSWithWait(btnNextPayment);
			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.PASS,
					"Click on Next sucess");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.FAIL,
					"Failed to click Nxt button");

		}
		waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
		verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, btnConfirm, "ExplicitMedWait");
		verifyPageDisplayed(btnConfirm, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		clickElementJSWithWait(btnConfirm);
		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		WebElement elementRadio = driver.findElement(rdBtnSelect);
		elementRadio.click();
		clickElementJSWithWait(btnPayNext);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		enterPaymentDetails();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

	}

	/*
	 * Method: validate the Intregration Status Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateCalenderDates() {
		waitUntilElementVisibleBy(driver, spinnerContainer, "ExplicitLongWait");
		if (verifyObjectDisplayed(calenderDate)) {
			validateCalenderAvailiabilty();
			verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateCalenderDates");
			clickElementJSWithWait(btnNext);
			tcConfig.updateTestReporter("offerTransactionPage", "validateCalenderDates", Status.PASS,
					"Sucessfully displays");
		} else if (verifyObjectDisplayed(calenderClick)) {
			waitUntilElementVisibleBy(driver, calenderClick, "ExplicitMedWait");
			tcConfig.updateTestReporter("offerTransactionPage", "validateCalenderDates", Status.PASS,
					"Calender dates not explicitly dispalyed. User needs to click on Calender to procced");
			clickElementJSWithWait(calenderClick);
			clickElementJSWithWait(btnNext);
			tcConfig.updateTestReporter("offerTransactionPage", "validateCalenderDates", Status.PASS,
					"Sucessfully displays");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateCalenderDates", Status.FAIL,
					"Sucessfully displays");
		}
	}

	/*
	 * Description: To compare the check In date with System date Date:
	 * January/2021 Author: Jyoti Changes By: NA
	 */
	public void validateCheckInDate() {
		waitUntilElementVisibleBy(driver, dtFrom, "ExplicitMedWait");
		String stringDate = getElementText(dtFrom);
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date now = new Date();
		String strCurrDate = sdf.format(now);
		System.out.println("Current Date: " + strCurrDate);

		// Taking the Check In date and Split
		String date = getElementText(dtFrom);
		String[] dateParts = date.split("/");
		String currmonth = dateParts[0];
		String currday = dateParts[1];
		String curryear = dateParts[2];

		System.out.println(
				"The string value in month are :-" + currmonth + "and day :-" + currday + "and year:- " + curryear);

		// Taking the System date and Split
		String update = strCurrDate;
		String[] newDate = update.split("/");
		String up_month = newDate[0];
		String up_day = newDate[1];
		String up_year = newDate[2];

		System.out.println(
				"The updated expiration month are :-" + up_month + "and day :-" + up_day + "and year:- " + up_year);

		if (Integer.parseInt(dateParts[0]) > Integer.parseInt(newDate[0])) {
			tcConfig.updateTestReporter("offerTransactionPage", "validateCheckOutDate", Status.PASS,
					"Check out date precceds to next month");
		} else if (Integer.parseInt(dateParts[0]) == Integer.parseInt(newDate[0])) {

			int dtDiff = Integer.parseInt(dateParts[1]) - Integer.parseInt(newDate[1]);
			String dateCompare = Integer.toString(dtDiff);
			if (dtDiff >= 10) {
				tcConfig.updateTestReporter("offerTransactionPage", "validateCheckInDate", Status.PASS,
						"Validated sucessfully Check In date is system date + 10 days");
			} else if (dtDiff < 10) {
				tcConfig.updateTestReporter("offerTransactionPage", "validateCheckInDate", Status.PASS,
						"Check I ndate taken as system date due to calender date not availiable");
			} else {
				tcConfig.updateTestReporter("offerTransactionPage", "validateCheckInDate", Status.FAIL,
						"Failed while validating System date");
			}
		} else {
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateCheckInDate", Status.FAIL,
					"Failed while validating the CheckIn  date");
		}
	}

	/*
	 * Description: To validate that Check out dates comes after Check in date
	 * Date: January/2021 Author: Jyoti Changes By: NA
	 */
	public void validateCheckOutDate() {
		waitUntilElementVisibleBy(driver, dtTo, "ExplicitMedWait");

		// Taking the Check In date and Split
		String date = getElementText(dtFrom);
		String[] dateParts = date.split("/");
		String currmonth = dateParts[0];
		String currday = dateParts[1];
		String curryear = dateParts[2];

		System.out.println(
				"The string value in month are :-" + currmonth + "and day :-" + currday + "and year:- " + curryear);

		// Taking the System date and Split
		String update = getElementText(dtTo);
		String[] newDate = update.split("/");
		String up_month = newDate[0];
		String up_day = newDate[1];
		String up_year = newDate[2];

		System.out.println(
				"The updated expiration month are :-" + up_month + "and day :-" + up_day + "and year:- " + up_year);

		if (Integer.parseInt(newDate[0]) > Integer.parseInt(dateParts[0])) {
			tcConfig.updateTestReporter("offerTransactionPage", "validateCheckOutDate", Status.PASS,
					"Check out date precceds to next month");
		} else if (Integer.parseInt(newDate[0]) == Integer.parseInt(dateParts[0])) {
			int dtDiff = Integer.parseInt(newDate[1]) - Integer.parseInt(dateParts[1]);
			String dateCompare = Integer.toString(dtDiff);
			if (dateCompare.equalsIgnoreCase(noOFNight)) {
				tcConfig.updateTestReporter("offerTransactionPage", "validateCheckOutDate", Status.PASS,
						"Validated sucessfully Check out date is difference by Offer Nights");
			} else {
				tcConfig.updateTestReporter("offerTransactionPage", "validateCheckOutDate", Status.FAIL,
						"Failed while validating Check out date");
			}
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateCheckOutDate", Status.FAIL,
					"Failed while validating Check out date");
		}
	}

	/*
	 * Description: Validate the calender availiability dates are in colour
	 * Date: January/2021 Author: Jyoti Changes By: NA
	 */
	public void validateCalenderAvailiabilty() {
		waitUntilElementVisibleBy(driver, calenderDate, "ExplicitMedWait");
		if (verifyObjectDisplayed(calenderDate)) {
			String calDate = driver.findElement(calenderDate).getCssValue("bgColor");
			tcConfig.updateTestReporter("offerTransactionPage", "validateCalenderAvailiabilty", Status.PASS,
					"Calender Availiable dates are highlighted in Colour");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateCalenderAvailiabilty", Status.FAIL,
					"Failed while validating Calender Dates");
		}
	}

	/*
	 * Description: Enter the card details and make the payment Date:
	 * January/2021 Author: Jyoti Changes By: NA
	 */
	public void enterPaymentDetails() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(vFrame)) {
			switchToFrame(vFrame);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			switchToFrame(cyberSourceFrame);
			scrollDownForElementJSBy(payButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(selectVisa);
			fieldEnterData(cardNumber, testData.get("CardNumber"));
			selectByIndex(expMonthDropdown, 5);
			selectByIndex(expYearDropdown, 3);
			verifyPageTitleDisplayed(payButton, "Payment Details", "Payment Page", "enterPaymentDetails");
			clickElementJSWithWait(payButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifyPageTitleDisplayed(txtReceipt, "Payment Details", "Payment Page", "enterPaymentDetails");
			driver.switchTo().defaultContent();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, btnFinishOT, "ExplicitLongWait");
			scrollDownForElementJSBy(btnFinishOT);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectElement("enterPaymentDetails", btnFinishOT, "Finish button in payemnt page");
			tcConfig.updateTestReporter("offerTransactionPage", "enterPaymentDetails", Status.PASS,
					"Sucessfully made the Card payment");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "enterPaymentDetails", Status.FAIL,
					"Unable to make credit card payment");
		}
	}

	/*
	 * Method: validate the Intregration Status Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateOfferReservationPresent() {
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		waitUntilElementVisibleBy(driver, offerReservationDetail, "ExplicitMedWait");

		tcConfig.updateTestReporter("offerTransactionPage", "validateOfferReservationPresent", Status.PASS,
				"Offer Reservation detail type present in Active line items" + getElementText(oldOTDNumber));
		oldOTDetails = getElementText(oldOTDNumber);

	}

	/*
	 * Method: validate the Intregration Status Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateOfferBookModification() {
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		WebElement elementRadio = driver.findElement(rdBtnOfferModification);
		elementRadio.click();
		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		clickElementBy(btnNext);
		waitUntilElementVisibleBy(driver, spinnerContainer, "ExplicitLongWait");
		changeCalenderAvailiability();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
//		getListElement(btnSelectAccom);
		clickElementBy(btnSelectAccom);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		getListElement(lblSelectAccom);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitMedWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		scrollDownForElementJSBy(btnNextPayment);
		if (verifyObjectDisplayed(btnNextPayment)) {
			clickElementJSWithWait(btnNextPayment);
			tcConfig.updateTestReporter("offerTransactionPage", "validateOfferBookModification", Status.PASS,
					"Click on Next sucess");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateOfferBookModification", Status.FAIL,
					"Failed to click Nxt button");

		}
		waitUntilElementVisibleBy(driver, btnNextPayment, "ExplicitMedWait");
	//	clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
		verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, btnConfirm, "ExplicitMedWait");
		verifyPageDisplayed(btnConfirm, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		clickElementJSWithWait(btnConfirm);
		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		List<WebElement> rdbtn = driver.findElements(rdBtnSelect);
		rdbtn.get(0).click();
		clickElementJSWithWait(btnPayNext);
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
	//	enterPaymentDetails();
	//	waitForSometime(tcConfig.getConfig().get("ExplicitLongWait"));
	//	waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitLongWait");
	}

	/*
	 * Method: validate the Intregration Status Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void changeCalenderAvailiability() {
		waitUntilElementVisibleBy(driver, destination, "ExplicitMedWait");
		/*
		 * Select sele = new Select(driver.findElement(destination));
		 * sele.selectByVisibleText("Las Vegas, NV_UAT");
		 */
		waitUntilElementVisibleBy(driver, calenderClick, "ExplicitLongWait");
		if (verifyObjectDisplayed(calenderDate)) {
			clickElementJSWithWait(calenderClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "changeCalenderAvailiability");
			clickElementJSWithWait(btnNext);
			tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
					"Sucessfully displays");
		} else if (verifyObjectDisplayed(calenderClick)) {
			waitUntilElementVisibleBy(driver, calenderClick, "ExplicitMedWait");
			tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
					"Click on Calender dates to change date");
			clickElementJSWithWait(calenderClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} else if (!verifyObjectDisplayed(calenderClick)) {
			if (verifyObjectDisplayed(arrivalDate)) {
				tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
						"No Dates avialiable for Selected Destination");
				clickElementJSWithWait(arrivalDate);
				clickElementJSWithWait(lnkToday);
				tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
						"Dates availiable now");
			}
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.FAIL,
					"Failed while selecting dates");
		}
		clickElementJSWithWait(btnNext);
	}

	/*
	 * Method: validate the Intregration Status Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateIntregrationStatus(String status) {
		waitUntilElementVisibleBy(driver, txtOffrTranDetailPage, "ExplicitMedWait");
		switch (status) {

		case "Res Book Success":

			Assert.assertTrue(verifyObjectDisplayed(intStatus));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateIntregrationStatus", Status.PASS,
					"Intregration Status is displayed :-" + getElementText(intStatus));

			break;

		case "Res ReBook Success":

			Assert.assertTrue(verifyObjectDisplayed(intStatus));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateOfferTransactionStatus", Status.PASS,
					"Intregration Status is displayed :-" + getElementText(intStatus));
			break;

		case "Res Cancel Success":

			Assert.assertTrue(verifyObjectDisplayed(intStatus));
			tcConfig.updateTestReporter("OfferTransactionsPage", "validateOfferTransactionStatus", Status.PASS,
					"Intregration Status is displayed :-" + getElementText(intStatus));
			break;
		}
	}

	/*
	 * Method: validate the Intregration Status Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void displayOfferReservation() {
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		waitUntilElementVisibleBy(driver, offerReservationDetail, "ExplicitMedWait");
		if (verifyObjectDisplayed(offerReservationDetail)) {
			tcConfig.updateTestReporter("offerTransactionPage", "displayOfferReservation", Status.PASS,
					"New Offer Reservation detail type present in Active line items" + getElementText(oldOTDNumber));
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "displayOfferReservation", Status.FAIL,
					"Failed while validating Offer Reservation number in Active line items section");
		}
	}

	/*
	 * Method: validate the Intregration Status Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateActiveLineItems() {
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		waitUntilElementVisibleBy(driver, offerReservationDetail, "ExplicitMedWait");
		Assert.assertTrue(verifyObjectDisplayed(offerReservationDetail) && verifyObjectDisplayed(destinationChange));

		tcConfig.updateTestReporter("offerTransactionPage", "validateActiveLineItems", Status.PASS,
				"New Offer Reservation detail type present in Active line items" + getElementText(oldOTDNumber)
						+ " and Destination Change present :- " + getElementText(destinationChange));
		// Assert.assertEquals(true,
		// verifyObjectDisplayed(offerReservationDetail));

		waitUntilElementVisibleBy(driver, hypInactiveLineItem, "ExplicitMedWait");
		clickElementJSWithWait(hypInactiveLineItem);
		waitUntilElementVisibleBy(driver, pgInactiveLineItem, "ExplicitMedWait");
		verifyPageTitleDisplayed(pgInactiveLineItem, "Book Tour", "OfferTransactionsPage",
				"validateOfferBookModification");
		String oldOTD = getElementText(inactiveOTD);
		if (oldOTDetails.equalsIgnoreCase(oldOTD)) {
			tcConfig.updateTestReporter("offerTransactionPage", "validateActiveLineItems", Status.PASS,
					"Old Offer Reservation detail type present in In-Active line items");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateActiveLineItems", Status.FAIL,
					"Failed while validating Offer Reservation number in Active line items section");
		}
	}

	/*
	 * Method: Cancel the OFfer reservation Date: January/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void cancelReservation() {
		waitUntilElementVisibleBy(driver, reservationSummary, "ExplicitMedWait");
		clickElementJSWithWait(reservationSummary);
		verifyPageTitleDisplayed(reservationSummary, "Reservation Summary", "OfferTransactionsPage",
				"cancelReservation");
		waitUntilElementVisibleBy(driver, btnCancelReservation, "ExplicitMedWait");
		clickElementJSWithWait(btnCancelReservation);
		verifyPageTitleDisplayed(txtWarning, "Reservation Summary", "OfferTransactionsPage", "cancelReservation");
		selectByIndex(drpCancellationReason, 4);
		clickElementBy(btnYes);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, btnPayNext, "ExplicitMedWait");
		verifyPageTitleDisplayed(btnPayNext, "Reservation Summary", "OfferTransactionsPage", "cancelReservation");
		clickElementJSWithWait(btnPayNext);
		if (verifyObjectDisplayed(sucessMsg)) {
			tcConfig.updateTestReporter("offerTransactionPage", "cancelReservation", Status.PASS,
					"Reservation cancellation message dispalyed:- " + getElementText(sucessMsg));
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "cancelReservation", Status.FAIL,
					"Failed while clicking Next button");
		}
		// Assert.assertEquals(true, getElementText(sucessMsg));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, activetab, "ExplicitLongWait");
	}

	/*
	 * Method: validate the Balance due and Credit Avilaible after booking
	 * cancellation Date: January/2020 Author: Jyoti Changes By: NA
	 */
	public void validateBalnceDueandCreditAvailiable() {
		waitUntilElementVisibleBy(driver, balanceDuefield, "ExplicitMedWait");

		Assert.assertTrue(verifyObjectDisplayed(balanceDuefield) && verifyObjectDisplayed(creditAvailiable));
		tcConfig.updateTestReporter("offerTransactionPage", "validateBalnceDueandCreditAvailiable", Status.PASS,
				"Balance due updated to :-" + getElementText(balanceDuefield) + " and credit availiable to:- "
						+ getElementText(creditAvailiable));
	}

	/*
	 * Method: validate the Cancellation Line items Date: January/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void cancellationLineItems() {
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		waitUntilElementVisibleBy(driver, cancellationFees, "ExplicitMedWait");

		Assert.assertEquals(true, verifyObjectDisplayed(cancellationFees));
		tcConfig.updateTestReporter("offerTransactionPage", "cancellationLineItems", Status.PASS,
				"Cancellation Fee detail type present in Active line items" + getElementText(cancellationFees));
		waitUntilElementVisibleBy(driver, hypInactiveLineItem, "ExplicitMedWait");
		clickElementJSWithWait(hypInactiveLineItem);
		waitUntilElementVisibleBy(driver, pgInactiveLineItem, "ExplicitMedWait");
		verifyPageTitleDisplayed(pgInactiveLineItem, "Inactive Line Item", "OfferTransactionsPage",
				"cancellationLineItems");

		Assert.assertTrue(verifyObjectDisplayed(inactiveOTD));

		tcConfig.updateTestReporter("OfferTransactionsPage", "cancellationLineItems", Status.PASS,
				"Old Reservation dispalyed in Inactive Line Item");
		driver.navigate().back();
	}

	/*
	 * Method: Process the refund to give money on booking Date: January/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void processRefundOnBooking() {
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(btnProcessRefund);
		verifyPageTitleDisplayed(pgRefundOption, "Inactive Line Item", "OfferTransactionsPage",
				"cancellationLineItems");
		List<WebElement> rdbtn = driver.findElements(rdBtnSelect);
		rdbtn.get(1).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnPayNext);
/*		waitForSometime(tcConfig.getConfig().get("MedWait"));
		enterCreditCardDetails();
		waitForSometime(tcConfig.getConfig().get("MedWait"));*/
		clickElementJSWithWait(btnPayNext);
		Assert.assertTrue(verifyObjectDisplayed(pgRefundSucess));

		tcConfig.updateTestReporter("OfferTransactionsPage", "processRefundOnBooking", Status.PASS,
				"Refund is sucessfully processed");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnPayNext);
	//	Assert.assertEquals(true, getElementText(creditAvailiable));
		tcConfig.updateTestReporter("OfferTransactionsPage", "processRefundOnBooking", Status.PASS,
				"credit availiable is dispalyed as :- " + getElementText(creditAvailiable));
	}

	/*
	 * Method: Validate that Payment line item contains Refund as Decision Date:
	 * January/2020 Author: Jyoti Changes By: NA
	 */
	public void validateRefundStatusInPaymentTab() {
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		scrollDownForElementJSBy(lnkPaymentRecords);
		clickElementJSWithWait(lnkPaymentRecords);
		verifyPageDisplayed(txtPaymentRecordsPage, "Page Title", "OfferTransactionsPage",
				"validateRefundStatusInPaymentTab");
		List<WebElement> elements = driver.findElements(tblPayemntRecordsStatus);
		System.out.println("Number of elements in Qunatity on Product:" + elements.size());
		for (int i = 0; i <= elements.size(); i++) {
			System.out.println("Search results consists of following columns :- " + elements.get(i).getText());
			if (elements.get(i).getText().equalsIgnoreCase("REFUNDED")) {
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateRefundStatusInPaymentTab", Status.PASS,
						"Refund in Payment  line item is added");
				break;
			} else {
				tcConfig.updateTestReporter("OfferTransactionsPage", "validateRefundStatusInPaymentTab", Status.FAIL,
						"Failed while displaying the Payment records");
			}
		}
	}

	/*
	 * Method: navigateToOfferTransaction Description: Navigate to Offer
	 * Transactions Date: February/2021 Author: Krishnaraj Kaliyamoorthy Changes
	 * By: NA
	 */

	public void navigateToOfferTransaction() {
		if (verifyObjectDisplayed(offerTransactionsTab)) {
			clickElementJSWithWait(offerTransactionsTab);
			verifyPageTitleDisplayed(offerTransactionsPage, "Offer Transactions Page displayed",
					"offerTransactionsPage", "navigateToOfferTransaction");
			fieldDataEnter(txtSearchOfferTransaction, testData.get("OfferTransactionNumber"));
			selectElement("offerTransaction", offerTransactionNumberResult, "offer Transaction Number Result");
			verifyPageTitleDisplayed(reservationBookedActive,
					"Offer Transaction details page displayed with Reservation in Booked Status", "offerTransaction",
					"navigateToOfferTransaction");
			validateActiveLineItemRes(offerReservationTypeActive, "Offer Reservation");

		} else {
			tcConfig.updateTestReporter("offerTransaction", "navigateToOfferTransaction", Status.FAIL,
					"Issue while navigating to Offer transaction details");
		}

	}
	/*
	 * Method: bookExtraRoom Description: Book Extra Room Date: February/2021
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */

	public void bookExtraRoom() throws Exception {
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		getDatesfromReservationSummary();
		selectExtraRoomOption();
		validateDateDetails();
		waitUntilElementInVisible(driver, spinnerContainer, 120);
		verifyPageTitleDisplayed(accommodationPage, "Accommodation Page displayed", "offerTransaction",
				"bookExtraRoom");
		scrollDownForElementJSBy(btnNext);
		getListElement(btnSelectAccom);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		getListElement(roomType1CheckBox);
		waitUntilElementInVisible(driver, spinnerContainer, 120);
		calculateTotalPriceAccommodationPage();
		selectByIndex(discountDropdown, 1);
		selectByIndex(discountPercentDropdown, 2);
		applyPromoCode();
		calculatePriceAfterDiscount();
		verifyPageTitleDisplayed(OutBalanceSectionConfirmScreen, "Confirmation screen displayed", "offerTransaction",
				"bookExtraRoom");
		clickElementBy(confirmButton);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, extraRoomTypeActive, "ExplicitLongWait");
		validateActiveLineItemRes(extraRoomTypeActive, "Offer Reservation type");
	}

	/*
	 * Method: getDatesfromReservationSummary Description: Get reservation check
	 * in and check out dates from reservation summary Date: February/2021
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void getDatesfromReservationSummary() {
		clickElementBy(tabReservationSummary);
		scrollDownForElementJSBy(resCheckinDate);
		resArrivalDate = getElementText(resCheckinDate);
		resDepartureDate = getElementText(resCheckoutDate);
		verifyPageTitleDisplayed(resCheckinDate, "Reservation Check in, checkout dates captured", "offerTransaction",
				"getDatesfromReservationSummary");
		scrollDownForElementJSBy(tabReservationSummary);
		scrollUpViaKeyboard(7);
	}

	/*
	 * Method: selectExtraRoomOption Description: select Extra room Date:
	 * February/2021 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void selectExtraRoomOption() {
		clickElementBy(tabBookReservation);
		clickElementBy(extraRoomBookingRadioButton);
		selectElement("offerTransactionsPage", btnNext, "After selecting Extra Room radio button, Next button");
	}

	/*
	 * Method: selectExtraRoomOption Description: select Extra room Date:
	 * February/2021 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void validateDateDetails() throws Exception {
		clickElementBy(calArrivalDate);
		arrivalDate1 = getElementAttribute(calCurrentDate, "data-value");
		clickElementBy(calDepartureDate);
		departureDate1 = getElementAttribute(calCurrentDate, "data-value");
		Assert.assertEquals(convertDate(arrivalDate1), resArrivalDate);
		Assert.assertEquals(convertDate(departureDate1), resDepartureDate);
		scrollDownForElementJSBy(btnNext);
		verifyPageTitleDisplayed(calendarAvailableState, "Dates displayed as per the Offer Reservation booking dates",
				"offerTransaction", "bookExtraRoom");
		selectElement("offerTransactionsPage", btnNext, "After validating calendar details, Next button");
	}

	public void calculateTotalPriceAccommodationPage() {
		extraRoomRate = removeDollarSign(getElementText(accomPageExtraRoomRate));
		totalPrice = removeDollarSign(getElementText(accomPagetotalPrice));
		totalTax = removeDollarSign(getElementText(accomPagetotalTax));
		extraRoomRateValue = Double.parseDouble(extraRoomRate);
		totalTaxValue = Double.parseDouble(totalTax);
		totalPriceValueAccomPage = extraRoomRateValue + totalTaxValue;
	//	totalPriceValueInAccomPage = String.valueOf(totalPriceValueAccomPage);
		totalPriceValueInAccomPage = String.format("%.2f", totalPriceValueAccomPage);
		totalTaxValueInAccomPage = String.valueOf(totalTaxValue);
		Assert.assertEquals(totalPriceValueInAccomPage, totalPrice, "Total Price calculation is incorrect");
		tcConfig.updateTestReporter("OfferTransactionsPage", "calculateTotalPriceAccommodationPage", Status.PASS,
				"Total price on the Available Accommodation screen is inclusive of the Taxes $"
						+ totalPriceValueInAccomPage + "Extra Room Rate: " + extraRoomRateValue + "Tax: "
						+ totalTaxValueInAccomPage);
		scrollDownForElementJSBy(btnNext);
		clickElementJSWithWait(btnNext);
	}

	public void applyPromoCode() {
		fieldEnterData(promocodeTextBox, testData.get("PromoCode"));
		clickElementJSWithWait(promocodeApplyButton);
		Assert.assertTrue(verifyObjectDisplayed(promoCodeAppliedExraRoom), "Free Extra Room details not displayed");
		Assert.assertTrue(verifyObjectDisplayed(discountPagePromoCodeValue), "Promocode not displayed");
		tcConfig.updateTestReporter("OfferTransactionsPage", "applyPromoCode", Status.PASS,
				"Discount and Promo Code applied successfully ");
	}

	public void calculatePriceAfterDiscount() {
		priceAfterSavings = removeDollarSign(getElementText(discountPagePriceAfterSavings));
		priceBeforeSavings = removeDollarSign(getElementText(discountPagePriceBeforeSavings));
		tax = removeDollarSign(getElementText(discountPageTaxValue));
		savings = removeDollarSign(getElementText(discountPageSavings));
		remainingBalance = removeDollarSign(getElementText(remainingBalanceField));
		taxValue1 = Double.parseDouble(tax);
		discountPagetax = String.valueOf(taxValue1);
		remainingBalanceValue1 = Double.parseDouble(priceBeforeSavings) - Double.parseDouble(savings);
		remainingBalanceValue1 = Math.round(remainingBalanceValue1 * 100.0) / 100.0;
//		discountPageRemainingBalance = String.valueOf(remainingBalanceValue1);
		discountPageRemainingBalance = String.format("%.2f", remainingBalanceValue1);
		priceAfterSavingsValue1 = Double.parseDouble(priceBeforeSavings) - Double.parseDouble(savings);
		priceAfterSavingsValue1 = Math.round(priceAfterSavingsValue1 * 100.0) / 100.0;
		discountPagepriceAfterSavings = String.valueOf(priceAfterSavingsValue1);
		System.out.println("RemBal " + discountPageRemainingBalance + "PriceAFSAV " + discountPagepriceAfterSavings
				+ "tax " + discountPagetax + "PBeforeSa " + priceBeforeSavings);
		Assert.assertEquals(discountPagetax, totalTaxValueInAccomPage, "Tax calculation is incorrect");
		Assert.assertEquals(discountPageRemainingBalance, remainingBalance,
				"Remaining Balance calculation is incorrect");
		Assert.assertTrue(verifyObjectDisplayed(promoCodeAppliedExraRoom), "Free Extra Room details not displayed");
		Assert.assertTrue(verifyObjectDisplayed(discountPagePromoCodeValue), "Promocode not displayed");
		tcConfig.updateTestReporter("OfferTransactionsPage", "calculateTotalPriceAccommodationPage", Status.PASS,
				"Amount = Total Extra Room price after discount with inclusive of Tax. "
						+ "Free Extra Room upgrade promocode applied " + "Price Before Savings: " + priceBeforeSavings
						+ "Price after Savings: " + discountPagepriceAfterSavings + "Remaining Balance: "
						+ discountPageRemainingBalance + "Discount Page tax: " + discountPagetax);
		scrollDownForElementJSBy(btnNext);
		clickElementJSWithWait(btnNext);
	}

	String offerPriceValueDisplayed, adjustmentAmount;

	/*
	 * Method: convertDate Description: convertDate validation Check Date:
	 * Feb/2021 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void processAdjustment() {
		
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (verifyObjectDisplayed(offerTransactionMoreButton)) {
						clickElementBy(offerTransactionMoreButton);
						verifyPageTitleDisplayed(processAdjustmentButton, "Process Adjustment Button button displayed", "moreOptions",
								"processAdjustment");
						selectElement("offerTransaction", processAdjustmentButton, "Process Adjustment Button");
						waitForSometime(tcConfig.getConfig().get("LongWait"));
						Assert.assertTrue(verifyObjectDisplayed(itemTypeSection), "item Type Section not displayed");
						Assert.assertTrue(verifyObjectDisplayed(adjustmentAmountSectionTitle), "Adjustment Amount Section Title not displayed");
						Assert.assertTrue(verifyObjectDisplayed(adjustmentReasonSectionTitle), "Adjustment Reason Section Title not displayed");
						clickElementJSWithWait(offerAddButton);
						offerPriceValueDisplayed = getElementText(offerPriceValue).replaceAll("[$,]", "");
						double amount = Double.parseDouble(offerPriceValueDisplayed);
						double newAmount = amount - 10.00;
						adjustmentAmount = String.valueOf(newAmount);
						fieldEnterData(adjustmentAmountBox, adjustmentAmount);
						selectByIndex(adjustmentReasonDropdown, 4);
						tcConfig.updateTestReporter("OfferTransactionsPage", "processAdjustment", Status.PASS,
							"Able to enter the Adjustment amount as less than the Payment Amount ");
				}
					clickElementBy(nextButton);
					checkLoadingSpinner();
					validateActiveLineItemRes(priceAdjustmentActive,"Price Adjustment");
					clickElementJSWithWait(processRefundButton);
					refundPriceAdjustment();
	}
	
	/*
	 * Method: refundPriceAdjustment Description: Process Refund to Original card Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void refundPriceAdjustment() {
		if (verifyObjectDisplayed(refundOriginalCardOption)) {
			clickElementJSWithWait(refundOriginalCardOption);
			verifyPageTitleDisplayed(nextButton, "Refund Option selected", "Refund option Selection Page",
					"refundPriceAdjustment");
			selectElement("refundOptionsPage", nextButton, "Next Button in the refund options page");
			waitUntilElementInVisible(driver, spinnerContainer, 120);
			verifyPageTitleDisplayed(RefundSuccessMessage, "Refund Success message displayed",
					"Refund option Selection Page", "refundPriceAdjustment");
			selectElement("refundPriceAdjustment", nextButton, "Next Button in the Refund Success page");
			checkLoadingSpinner();
			Assert.assertTrue(verifyObjectDisplayed(creditAvailableZero), "Credit Available is not 0 after refund");

		} else {
			tcConfig.updateTestReporter("Cancel Offer Transaction", "refundToOriginalCard", Status.FAIL,
					"Unable to Process Refund");
		}

	}
	
	/*
	 * Method: createOfferTransaction Description: Create Offer transaction Val
	 * Date: September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void validateOfferTermsandConditions() {
		if (verifyObjectDisplayed(salesChannelSelectionPageTitle)) {
			verifyPageTitleDisplayed(salesChannelSelectionPageTitle, "Navigated to Sales Channel selection page",
					"Offer Transaction page", "createOfferTransaction");
			enterSalesChannelSelection();
			enterMarketingStrategyDetails();
			checkExpirationMonth();
			checkAccommodationNight();
			enterOfferPrice();
			selectElement("createOfferTransaction", nextButton, "Next Button in the Offer Price Selection page");
			clickElementJSWithWait(offerSelectCheckbox);
			selectElement("createOfferTransaction", nextButton, "Next Button in the Destination page");
			waitUntilElementInVisible(driver, spinnerContainer, 120);
			chooseAccommodation();
			clickElementJSWithWait(premiumCheckBox1);
			verifyElementSelected(premium1Selected, "createOfferTransaction", "Premium 1");
			selectElement("createOfferTransaction", nextButton, "Next Button in the Select Premium page");
			verifyPageTitleDisplayed(nextButton, "Confirmation Details", "Confirmation Page", "createOfferTransaction");
			scrollDownForElementJSBy(legalTermsCondTextBox);
			System.out.println("Legal: " +  getElementText(legalTermsCondTextBox) + "Busi: " + getElementText(generalTermsCondTextBox));
			Assert.assertTrue(getElementText(legalTermsCondTextBox).contentEquals("Testing Legal TandC"), "Legal Terms and conditions not displayed");
			Assert.assertTrue(getElementText(generalTermsCondTextBox).contentEquals("Testing General Or business TandC"),"Business Terms and conditions not displayed");
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerRecord", Status.PASS,
					"Legal and Business Terms and Conditions displayed when the conditions are met");
		} else {
			tcConfig.updateTestReporter("OwnersPage", "selectOwnerRecord", Status.FAIL,
					"Unable to verify Legal and Business Terms and Conditions");
		}

	}

	/*
	 * Method: convertDate Description: convertDate validation Check Date:
	 * Feb/2021 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public String convertDate(String strDate) throws Exception {
		if (strDate.contains("/")) {
			return strDate;
		} else {
			return convertDateFormat("yyyy-MM-dd", "M/d/yyyy", strDate);
		}
	}

	/*
	 * Method: dateConvert Description: remove dollar from Price Date: Feb/2021
	 * Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public String removeDollarSign(String price) {
		return price.substring(1);
	}

	public void slectOfferOnOfferSelection() {
		
		
		clickElementBy(offerSelectCheckbox);
		clickElementBy(nextButton);
		
}


public void slectNextButtonOnOfferDeatail() {
	
	
	
	clickElementBy(nextButton);
	
}
//Method to select destination as per offer and sales channel combination
//By Shubhendra Vaish

public void selectDestinationAndTapNext() throws Exception{
	
	waitUntilElementVisibleBy(driver, destinationName1, "ExplicitLongWait");
	clickElementBy(destinationCheckBox1);
	clickElementBy(nextButton);
}

//Method to select Next on Available Accomodation
//By Shubhendra Vaish

public void tapNextOnAvailableAccomodation() throws Exception{
	

	clickElementBy(btnNext);
}

//Method to validate elements on display Premium page
//By Shubhendra Vaish

public void validateFieldsOnDisplayPrem() throws Exception{
	
	waitUntilElementVisibleBy(driver, textBasePremMaxPrice, "ExplicitLongWait");
	waitUntilElementVisibleBy(driver, textColNamePremName, "ExplicitLongWait");
	waitUntilElementVisibleBy(driver, textColNamePremRetValue, "ExplicitLongWait");
	waitUntilElementVisibleBy(driver, textColNamePremCost, "ExplicitLongWait");
	clickElementBy(btnFinish);
}

	
	/*
	 * Method: validate the Modification of Reservation Dates to Check Tour ID cancellation
	 * TC027_CRS_5145_tour_in_booked_status_is_cancelled_if_the_tour_book_date_does_not_falls_between_checkin_and_checkout_date_while_offer_reservation_modification
	 *  Date: January/2020 Author:
	 * Shubhendra Vaish By: NA
	 */

	public void validateOfferBookModificationToCheckBookedStatusIsCancelled(){
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		WebElement elementRadio = driver.findElement(rdBtnOfferModification);
		elementRadio.click();
		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		clickElementBy(btnNext);				
		waitUntilElementVisibleBy(driver, spinnerContainer, "ExplicitLongWait");
		changeCalenderAvailiabilityAsTourBookDateDoesNotFallBetweenChkinChkout();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");				
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		getListElement(btnSelectAccom);		
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		clickElementJSWithWait(lblSelectAccom3);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitMedWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		scrollDownForElementJSBy(btnNextPayment);
		if(verifyObjectDisplayed(btnNextPayment))
		{																
			clickElementJSWithWait(btnNextPayment);	
			tcConfig.updateTestReporter("offerTransactionPage", "validateOfferBookModification", Status.PASS,
					"Click on Next sucess");
		}
		else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateOfferBookModification", Status.FAIL,
					"Failed to click Nxt button");
								
		}
		waitUntilElementVisibleBy(driver, btnNextPayment, "ExplicitMedWait");
		waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
		verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");		
		clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, btnConfirm, "ExplicitMedWait");
		verifyPageDisplayed(btnConfirm, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");		
		clickElementJSWithWait(btnConfirm);
		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
/*		scrollDownByPixel(500);
		waitUntilElementVisibleBy(driver, payWithCardOnFile, "ExplicitLongWait");	
		clickElementJSWithWait(payWithCardOnFile);
		waitUntilElementVisibleBy(driver, nextButton, "ExplicitLongWait");	
		clickElementJSWithWait(nextButton);*/
	//	waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitLongWait");
	//	List<WebElement> rdbtn = driver.findElements(rdBtnSelect);
	//	rdbtn.get(1).click();
		clickElementJSWithWait(payWithNewCard);
		clickElementJSWithWait(btnPayNext);
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		enterPaymentDetails();
		waitForSometime(tcConfig.getConfig().get("ExplicitLongWait"));
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitLongWait");
	}
	
	public void changeCalenderAvailiabilityAsTourBookDateDoesNotFallBetweenChkinChkout(){
		waitUntilElementVisibleBy(driver, destination, "ExplicitMedWait");				
		 /*Select sele = new Select(driver.findElement(destination));
		    sele.selectByVisibleText("Las Vegas, NV_UAT");*/
		waitUntilElementVisibleBy(driver, calenderClick, "ExplicitLongWait");
		if(verifyObjectDisplayed(calenderDate))
		{					
			clickElementJSWithWait(calenderClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "changeCalenderAvailiability");
			clickElementJSWithWait(btnNext);	
			tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
					"Sucessfully displays");
		}
		else if(verifyObjectDisplayed(calenderClick))
		{			
			waitUntilElementVisibleBy(driver, calenderClick, "ExplicitMedWait");
			tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
					"Click on Calender dates to change date");			
			clickElementJSWithWait(calenderClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		else if(!verifyObjectDisplayed(calenderClick)){				
		  if(verifyObjectDisplayed(arrivalDate)){
			tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
					"No Dates avialiable for Selected Destination");
			clickElementJSWithWait(arrivalDate);	
			clickElementJSWithWait(lnkToday);	
			tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.PASS,
					"Dates availiable now");					
		}}
		else{
			 tcConfig.updateTestReporter("offerTransactionPage", "changeCalenderAvailiability", Status.FAIL,
						"Failed while selecting dates");
		}	
		clickElementJSWithWait(btnNext);				
	}
	
	
	public void extraRoomModification(){
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		WebElement elementRadio = driver.findElement(rdBtnOfferModification);
		elementRadio.click();
		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "extraRoomModification");
		clickElementBy(btnNext);
		waitUntilElementVisibleBy(driver, spinnerContainer, "ExplicitLongWait");
		clickElementJSWithWait(btnNext);
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "extraRoomModification");
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		getListElement(btnSelectAccom);	
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");	
		clickElementJSWithWait(lblSelectAccom2);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitMedWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "extraRoomModification");
		scrollDownForElementJSBy(btnNextPayment);
		
		if(verifyObjectDisplayed(btnNextPayment))
		{																
			clickElementJSWithWait(btnNextPayment);	
			tcConfig.updateTestReporter("offerTransactionPage", "extraRoomModification", Status.PASS,
					"Click on Next sucess");
		}
		else {
			tcConfig.updateTestReporter("offerTransactionPage", "extraRoomModification", Status.FAIL,
					"Failed to click Nxt button");
								
		}
		
		waitUntilElementVisibleBy(driver, btnNextPayment, "ExplicitMedWait");
		clickElementJSWithWait(btnNextPayment);

		clickElementBy(btnConfirm);
		
		
		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "extraRoomModification");

		clickElementBy(payWithNewCard);
		clickElementJSWithWait(btnPayNext);		
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		enterPaymentDetails();
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitLongWait");
	}
	
	/*
	 * Method: Cancel the Extra Room reservation
	 *  Date: Feb/2021 Author:
	 * Shubhendra Changes By: NA
	 */
	public void cancelExtraRoomReservation(){
		waitUntilElementVisibleBy(driver, reservationSummary, "ExplicitMedWait");
		clickElementJSWithWait(reservationSummary);
		verifyPageTitleDisplayed(reservationSummary, "Reservation Summary", "OfferTransactionsPage",
				"cancelExtraRoomReservation");
		waitUntilElementVisibleBy(driver, btnCancelExtraRoomReservation, "ExplicitMedWait");
		clickElementJSWithWait(btnCancelExtraRoomReservation);
		verifyPageTitleDisplayed(txtWarning, "Reservation Summary", "OfferTransactionsPage",
				"cancelExtraRoomReservation");
		selectByIndex(drpCancellationReason, 4);
		clickElementBy(btnYes);
		verifyPageTitleDisplayed(sucessMsg, "Reservation Summary", "OfferTransactionsPage",
				"cancelExtraRoomReservation");
		if(verifyObjectDisplayed(sucessMsg)){					
			tcConfig.updateTestReporter("offerTransactionPage", "cancelExtraRoomReservation", Status.PASS,
					"Reservation cancellation message dispalyed:- "+getElementText(sucessMsg));	
		}else{
			tcConfig.updateTestReporter("offerTransactionPage", "cancelExtraRoomReservation", Status.FAIL,
				"Failed while clicking Next button");	}				
		//Assert.assertEquals(true, getElementText(sucessMsg));
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, activetab, "ExplicitLongWait");	
		
	}
	

	
	/*
	 * Method: Process the  refund to give money on booking to orignal Card
	 *  Date: January/2020 Author:
	 * Shubhendra Changes By: NA
	 */
	public void processRefundOnBookingtoOrignalCard(){				
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(btnProcessRefund);
		verifyPageTitleDisplayed(pgRefundOption, "Inactive Line Item", "OfferTransactionsPage",
				"processRefundOnBookingtoOrignalCard");	
		List<WebElement> rdbtn = driver.findElements(rdBtnSelect);
		rdbtn.get(1).click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnPayNext);
		//waitForSometime(tcConfig.getConfig().get("MedWait"));
		//enterCreditCardDetails();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//clickElementJSWithWait(btnPayNext);
		Assert.assertTrue(verifyObjectDisplayed(pgRefundSucess));
		
		tcConfig.updateTestReporter("OfferTransactionsPage", "processRefundOnBooking", Status.PASS,
				"Refund is sucessfully processed");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnPayNext);
	//	Assert.assertEquals(getElementText(creditAvailiable),true);
		tcConfig.updateTestReporter("OfferTransactionsPage", "processRefundOnBooking", Status.PASS,
				"credit availiable is dispalyed as :- " +getElementText(creditAvailiable));				
	}	
	
	
	/*
	 * Description: Check Accommodation Availability with NO Accommodations on Offer and Sales Channel has Accommodation Restrictions
	 *  Date: Feb/2021 Author: Shuhendra Changes By: NA
	 */
	public void validateBookReservationWithNoAccomodationOnOfferAndSaleChannelhasAccomodationRestrictions(){				
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		clickElementJSWithWait(btnNext);						
		changeCalenderAvailiability();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		validateCheckInDate();	
		validateCheckOutDate();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		getListElement(btnSelectAccom);		
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		getListElement(lblSelectAccom);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		scrollDownForElementJSBy(btnNextPayment);
		if(verifyObjectDisplayed(btnNextPayment))
		{																
			clickElementJSWithWait(btnNextPayment);	
			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.PASS,
					"Click on Next sucess");
		}
		else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.FAIL,
					"Failed to click Nxt button");
								
		}
		waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
		verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateBookReservation");		
		clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, btnConfirm, "ExplicitMedWait");
		verifyPageDisplayed(btnConfirm, "Page Title", "OfferTransactionsPage", "validateBookReservation");		
		/*clickElementJSWithWait(btnConfirm);
		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		WebElement elementRadio = driver.findElement(rdBtnSelect);
		elementRadio.click();
		clickElementJSWithWait(btnPayNext);		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		enterPaymentDetails();
		waitForSometime(tcConfig.getConfig().get("MedWait"));*/
				
	}
	
	
	
	/*
	 * Method: Cancel the OFfer reservation having cancel fees can be overrridden price
	 * TC003_CRS_463_Cancel_Special_Event_Reservation_which_has_No_Tour_and_No_Referral_and_Cancel_Date_less_than_15_days_from_OT_Create_Date_With_Cancel_Fee
	 *  Date: January/2020 Author:
	 * Shubhendra Changes By: NA
	 */
	public void cancelReservationforOverRiddenCode(){
		waitUntilElementVisibleBy(driver, reservationSummary, "ExplicitMedWait");
		clickElementJSWithWait(reservationSummary);
		verifyPageTitleDisplayed(reservationSummary, "Reservation Summary", "OfferTransactionsPage",
				"cancelReservation");
		waitUntilElementVisibleBy(driver, btnCancelReservation, "ExplicitMedWait");
		clickElementJSWithWait(btnCancelReservation);
		verifyPageTitleDisplayed(txtWarning, "Reservation Summary", "OfferTransactionsPage",
				"cancelReservation");
		selectByIndex(drpCancellationReason, 4);
		clickElementBy(btnYes);
		
		clearElementBy(editBoxOverideFee);
		sendKeysBy(editBoxOverideFee, "15");
		clickElementJSWithWait(buttonOverride );
		verifyPageTitleDisplayed(textOverrideCode, "Reservation Summary", "OfferTransactionsPage",
				"cancelReservationforOverRiddenCode");
		
		clearElementBy(editFieldOverrideCode);
		sendKeysBy(editFieldOverrideCode, "OSC_2021");
		clickElementJSWithWait(buttonSubmit);
		
		verifyPageTitleDisplayed(btnPayNext, "Reservation Summary", "OfferTransactionsPage",
				"cancelReservation");
		clickElementJSWithWait(btnPayNext);
		if(verifyObjectDisplayed(sucessMsg)){					
			tcConfig.updateTestReporter("offerTransactionPage", "cancelReservation", Status.PASS,
					"Reservation cancellation message dispalyed:- "+getElementText(sucessMsg));	
		}else{
			tcConfig.updateTestReporter("offerTransactionPage", "cancelReservation", Status.FAIL,
				"Failed while clicking Next button");	}				
		//Assert.assertEquals(true, getElementText(sucessMsg));
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, activetab, "ExplicitLongWait");				
	}
	
	/*
	 * Method: changeDestination Description: Change destination from the offer
	 * transaction Date: Feb/2021 Author: Shubhendra Vaish Changes
	 * By: NA
	 */

	public void changeDestinationHavingUpgradeFees() {
		navigateToChangeDestination();
		verifyElementSelected(destination1Selected, "changeDestinationHavingUpgradeFees",
				"Existing Destination 1 displayed as auto-selected ");
		selectDestination(destinationCheckBox2, destination2Selected, objMap.get("destination2"));
		clickElementBy(saveButton);
		verifyPageTitleDisplayed(toastSuccessMessage, "Destination is updated successfully", "changeDestinationHavingUpgradeFees",
				"select destination");
		
	
	}
	
	
	/*
	 * Method: validate the Active line item for distnation change
	 *  Date: Feb/2021 Author:
	 * Shuhendra Changes By: NA
	 */
	public void validateActiveLineItemsforDistnationChange(){
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		waitUntilElementVisibleBy(driver, destinationChange, "ExplicitMedWait");
		
														
	}
	
	public void validateBookingNotAllowedOnBalanceDueDuringReservation(){

		
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		verifyPageDisplayed(balanceDueMsgOnBookRes, "Page Title", "OfferTransactionsPage", "validateBookingNotAllowedOnBalanceDueDuringReservation");
		Assert.assertEquals(getObject(balanceDueMsgOnBookRes).getText(), "There is a balance due on this transaction, Offer booking/rebooking is not allowed for this transaction");
														
	}
	
	public void processPaymentForBalanceDue() {
		if (verifyObjectDisplayed(processPaymentButton)) {
			clickElementJSWithWait(processPaymentButton);
			clickElementJSWithWait(payWithCardOnFile);
			verifyPageTitleDisplayed(nextButton, "Pay from Original Card", "Refund option", "refundToOriginalCard");
			selectElement("processPayment", nextButton, "Next Button in the Process Payment page");
			verifyPageTitleDisplayed(toastSuccessMessage, "Payment processed successfully", "ChangeDestination",
					"processPayment");
		} else {
			tcConfig.updateTestReporter("TransactionDetails", "validateActiveLineItem", Status.FAIL,
					"Issue while making payment");
		}
	}
	
	
	/*
	 * Method: Click More and navigate to Change Premium
	 * Feb/2021 Author: Shubhendra Changes By: NA
	 */
	public void validateOpeningAndClosingofAddOnPrimiumWindow() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String premium1Value = "";
		String premium2Value = "";
		if (verifyObjectDisplayed(offerTransactionMoreButton)) {
			clickElementJSWithWait(offerTransactionMoreButton);
			verifyPageTitleDisplayed(addOnPremium, "Change Base Premium button displayed", "moreOptions",
					"validateOpeningAndClosingofAddOnPrimiumWindow");
			selectElement("offerTransaction", addOnPremium, "Add-on Premium Button");
			verifyPageTitleDisplayed(headerAddOnPrem, "header On Add-on Premium", "offerTransaction", "validateOpeningAndClosingofAddOnPrimiumWindow");
			clickElementJSWithWait(crossIconOnAddOnPrem);
		} else {
			tcConfig.updateTestReporter("offerTransaction", "changeBasePremium", Status.FAIL,
					"Issue while trying to Change base Premium");
		}

	}
	
	/*
	 * Method: validate Base Premium Line Item on Active line Item Deatil Page and Navigate back to Detail page
	 *  Date: Febe/2020 Author:
	 * Shubhendra Changes By: NA
	 */
	public void validateActiveLineItemsBasePremiumOnRelated(){
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		waitUntilElementVisibleBy(driver, offerReservationDetail, "ExplicitMedWait");

		waitUntilElementVisibleBy(driver, hypactiveLineItem, "ExplicitMedWait");
		clickElementJSWithWait(hypactiveLineItem);
		waitUntilElementVisibleBy(driver, lineItemBasePrem, "ExplicitMedWait");
		verifyPageTitleDisplayed(lineItemBasePrem, "line Item Base Prem", "OfferTransactionsPage",
				"validateActiveLineItemsBasePremiumOnRelated");	
		
		fieldDataEnter_modified(editSearchBox, testData.get("OfferTransactionNumber"));
		clickElementJSWithWait(imgClick);
		waitUntilElementVisibleBy(driver, txtOffrTranDetailPage, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(txtOffrTranDetailPage));
		tcConfig.updateTestReporter("OfferTransactionsPage", "searchOfferTransaction", Status.PASS,
				"User navigated to Offer Transaction details page Successfully");
		
													
	}
	
	/* Validate Promised item updated in Incentive tab of Tour details when Premium is changed
	 * Created By: Shubhendra*/
	
	public void validatePromisedItemOnIncetiveTabOfTourDetail(){
		waitUntilElementVisibleBy(driver, reservationSummary, "ExplicitMedWait");
		clickElementJSWithWait(reservationSummary);
		verifyPageTitleDisplayed(reservationSummary, "Reservation Summary", "OfferTransactionsPage",
				"cancelReservation");
		waitUntilElementVisibleBy(driver, btnCancelReservation, "ExplicitMedWait");
		clickElementJSWithWait(hyplnkTourID);
		//waitUntilElementVisibleBy(driver, headerTourRecord, "ExplicitMedWait");
		//waitUntilElementVisibleBy(driver, tabIncentives, "ExplicitMedWait");
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		SwitchtoWindow();
		clickElementJSWithWait(tabIncentives);
		
		waitUntilElementVisibleBy(driver, lineItemPromisedItem, "ExplicitMedWait");
		waitUntilElementVisibleBy(driver, promisedItemPremiumBeforeChange, "ExplicitMedWait");
		SwitchtoWindow();
		
	}
	
	
	
	/*
	 * Description: To compare the Offer Expiration date with System date
	 *  Date: January/2021 Author: Shubhendra Changes By: NA
	 */
	public void validateBookReservationUpdated(){				
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		clickElementJSWithWait(btnNext);						
		changeCalenderAvailiability();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		validateCheckInDate();	
		validateCheckOutDate();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		getListElement(btnSelectAccom);		
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		//getListElement(lblSelectAccom);    Updating with below value lblSelectAccom and method by shubhendra
		clickElementJSWithWait(lblSelectAccom2);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		scrollDownForElementJSBy(btnNextPayment);
		if(verifyObjectDisplayed(btnNextPayment))
		{																
			clickElementJSWithWait(btnNextPayment);	
			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.PASS,
					"Click on Next sucess");
		}
		else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.FAIL,
					"Failed to click Nxt button");
								
		}
		waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
		verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateBookReservation");		
		clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, btnConfirm, "ExplicitMedWait");
		verifyPageDisplayed(btnConfirm, "Page Title", "OfferTransactionsPage", "validateBookReservation");		
		clickElementJSWithWait(btnConfirm);
		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "validateBookReservation");
		WebElement elementRadio = driver.findElement(rdBtnSelect);
		clickElementJSWithWait(payWithNewCard);
		
		
		clickElementJSWithWait(btnPayNext);		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		enterPaymentDetailsUpdated();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
				
	}
	
	
	/*
	 * Description: Enter the card details and make the payment
	 *  Date: January/2021 Author: Shubhendra Changes By: NA
	 */
	public void enterPaymentDetailsUpdated(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(vFrame)) {
			switchToFrame(vFrame);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			switchToFrame(cyberSourceFrame);
			scrollDownForElementJSBy(payButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(selectVisa);
			fieldEnterData(cardNumber, testData.get("CardNumber"));
			selectByIndex(expMonthDropdown, 5);
			selectByIndex(expYearDropdown, 3);
			verifyPageTitleDisplayed(payButton, "Payment Details", "Payment Page", "enterPaymentDetails");
			clickElementJSWithWait(payButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			verifyPageTitleDisplayed(txtReceipt, "Payment Details", "Payment Page", "enterPaymentDetails");
			driver.switchTo().defaultContent();
			waitForSometime(tcConfig.getConfig().get("MedWait"));	
			waitUntilElementVisibleBy(driver, btnFinishOT, "ExplicitLongWait");
		//	scrollDownForElementJSBy(btnFinishOT); updated by shubhendra
			clickElementJSWithWait(btnFinishOT);
		//	selectElement("enterPaymentDetails", btnFinishOT, "Finish button in payemnt page");
			tcConfig.updateTestReporter("offerTransactionPage", "enterPaymentDetails", Status.PASS,
					"Sucessfully made the Card payment");
		} else {
			tcConfig.updateTestReporter("offerTransactionPage", "enterPaymentDetails", Status.FAIL,
					"Unable to make credit card payment");
		}
	}
	
	
	/*
	 * Method: validate the Intregration Status
	 *  Date: March/2021 Author:
	 * Shubhendra Changes By: NA
	 */
	public void validateOfferBookModificationUpdated(){
		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
		clickElementBy(tabBookReservation);
		WebElement elementRadio = driver.findElement(rdBtnOfferModification);
		elementRadio.click();
		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		clickElementBy(btnNext);				
		waitUntilElementVisibleBy(driver, spinnerContainer, "ExplicitLongWait");
		changeCalenderAvailiability();
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");				
		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
		getListElement(btnSelectAccom);		
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
		//getListElement(lblSelectAccom3);
		clickElementJSWithWait(lblSelectAccom3);
		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitMedWait");
		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		scrollDownForElementJSBy(btnNextPayment);
		if(verifyObjectDisplayed(btnNextPayment))
		{																
			clickElementJSWithWait(btnNextPayment);	
			tcConfig.updateTestReporter("offerTransactionPage", "validateOfferBookModification", Status.PASS,
					"Click on Next sucess");
		}
		else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateOfferBookModification", Status.FAIL,
					"Failed to click Nxt button");
								
		}
		waitUntilElementVisibleBy(driver, btnNextPayment, "ExplicitMedWait");
		waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
		verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");		
		clickElementJSWithWait(btnNextPayment);
	//	waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
	//	verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");		
	//	clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, btnConfirm, "ExplicitMedWait");
		verifyPageDisplayed(btnConfirm, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");		
		clickElementJSWithWait(btnConfirm);
		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "validateOfferBookModification");
		List<WebElement> rdbtn = driver.findElements(rdBtnSelect);
		rdbtn.get(1).click();				
		clickElementJSWithWait(btnPayNext);		
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		enterPaymentDetailsUpdated();
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitLongWait");				
	}
	
	
	/*
	 * Method: validate the Intregration Status
	 *  Date: March/2021 Author:
	 * Shubhendra Changes By: NA
	 */
	public void validateActiveLineItemsUpdated(){
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitMedWait");
		clickElementJSWithWait(RelatedTab);
		waitUntilElementVisibleBy(driver, offerReservationDetail, "ExplicitMedWait");
		Assert.assertTrue(verifyObjectDisplayed(offerReservationDetail));
			
		//	tcConfig.updateTestReporter("offerTransactionPage", "validateActiveLineItems", Status.PASS,
		//			"New Offer Reservation detail type present in Active line items" + getElementText(oldOTDNumber)+
		//			" and Destination Change present :- " + getElementText(destinationChange));
			//Assert.assertEquals(true, verifyObjectDisplayed(offerReservationDetail));
			
		waitUntilElementVisibleBy(driver, hypInactiveLineItem, "ExplicitMedWait");
		clickElementJSWithWait(hypInactiveLineItem);
		waitUntilElementVisibleBy(driver, pgInactiveLineItem, "ExplicitMedWait");
		verifyPageTitleDisplayed(pgInactiveLineItem, "Book Tour", "OfferTransactionsPage",
				"validateOfferBookModification");
		String oldOTD = getElementText(inactiveOTD);
		if (oldOTDetails.equalsIgnoreCase(oldOTD)) {
			tcConfig.updateTestReporter("offerTransactionPage", "validateActiveLineItems", Status.PASS,
					"Old Offer Reservation detail type present in In-Active line items");
		}else {
			tcConfig.updateTestReporter("offerTransactionPage", "validateActiveLineItems", Status.FAIL,
					"Failed while validating Offer Reservation number in Active line items section");
		}														
	}
	
	/*
     * Method: validateBAFReferralDescription: Validate BAF Referral
     * Date: February/2020 Author: Krishnaraj Kaliyamoorthy
     * Changes By: NA
     */
     public void validateBAFReferral() {
                     if (verifyObjectDisplayed(bookReservationTab)) {
                                     clickElementJSWithWait(bookReservationTab);
                                     clickElementBy(nextButton);
                                     verifyPageTitleDisplayed(toastErrorMessage, "Referral required error displayed", "offerTransaction",
                                                                     "validateBAFReferral");
                     } else {
                                     tcConfig.updateTestReporter("TransactionDetails", "validateActiveLineItem", Status.FAIL,
                                                                     "Issue while making payment");
                     }
     }
	
     
     /*
 	 * Description: To compare the Offer Expiration date with System date Date:
 	 * Mar/2021 Author: Shubhendra Changes By: NA
 	 */
 	public void validateBookReservationForDestChange() {
 		waitUntilElementVisibleBy(driver, tabBookReservation, "ExplicitMedWait");
 		clickElementBy(tabBookReservation);
 		verifyPageDisplayed(btnNext, "Page Title", "OfferTransactionsPage", "validateBookReservation");
 		clickElementJSWithWait(btnNext);
 		changeCalenderAvailiability();
 		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
 		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
 	//	validateCheckInDate();
 	//	validateCheckOutDate();
 		waitUntilElementVisibleBy(driver, pgAccomodation, "ExplicitLongWait");
 		getListElement(btnSelectAccom);
 		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
 		clickElementJSWithWait(lblSelectAccom3);
 		waitUntilElementVisibleBy(driver, pgRoomType, "ExplicitLongWait");
 		verifyPageDisplayed(pgAccomodation, "Page Title", "OfferTransactionsPage", "validateBookReservation");
 		scrollDownForElementJSBy(btnNextPayment);
 		if (verifyObjectDisplayed(btnNextPayment)) {
 			clickElementJSWithWait(btnNextPayment);
 			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.PASS,
 					"Click on Next sucess");
 		} else {
 			tcConfig.updateTestReporter("offerTransactionPage", "validateBookReservation", Status.FAIL,
 					"Failed to click Nxt button");

 		}
 		waitUntilElementVisibleBy(driver, pgPromoCode, "ExplicitLongWait");
 		verifyPageDisplayed(pgPromoCode, "Page Title", "OfferTransactionsPage", "validateBookReservation");
 		clickElementJSWithWait(btnNextPayment);
 		waitUntilElementVisibleBy(driver, btnConfirm, "ExplicitMedWait");
 		verifyPageDisplayed(btnConfirm, "Page Title", "OfferTransactionsPage", "validateBookReservation");
 		clickElementJSWithWait(btnConfirm);
 		waitUntilElementVisibleBy(driver, pgPaymentOptions, "ExplicitMedWait");
 		verifyPageDisplayed(pgPaymentOptions, "Page Title", "OfferTransactionsPage", "validateBookReservation");
 	//	WebElement elementRadio = driver.findElement(rdBtnSelect);
 	//	elementRadio.click();
 	//	clickElementJSWithWait(btnPayNext);
 	//	waitForSometime(tcConfig.getConfig().get("MedWait"));
 		//enterPaymentDetails();
 		waitUntilElementVisibleBy(driver, payWithCardOnFile, "ExplicitMedWait");
		clickElementJSWithWait(payWithCardOnFile);
	//	scrollDownByPixel(100);       
		clickElementJSWithWait(btnNextPayment);
		waitUntilElementVisibleBy(driver, RelatedTab, "ExplicitLongWait");

 	}
	
	/* Validate tour details are not present in reservation summary once reservation dates are modified 
	 * Created By: Shubhendra*/
	
	public void validateTourDetailsNotPresentInOfferTransactionOnceReservationDatesModified(){
		waitUntilElementVisibleBy(driver, reservationSummary, "ExplicitMedWait");
		clickElementJSWithWait(reservationSummary);
		verifyPageTitleDisplayed(reservationSummary, "Reservation Summary", "OfferTransactionsPage",
				"validateTourDetailsNotPresentInOfferTransactionOnceReservationDatesModified");
		waitUntilElementVisibleBy(driver, btnCancelReservation, "ExplicitMedWait");
	//	clickElementJSWithWait(hyplnkTourID);
	//	Assert.assertTrue(verifyObjectDisplayed(hyplnkTourID));
		Assert.assertFalse(verifyObjectDisplayed(hyplnkTourID), "Tour deatils not present after Modification of Reservaation");
	
		
	}
	
	//test
	/*
	 * Method: ChoosePayment Description: Choose payment option Date:
	 * September/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void chooseCreditCardPaymentN() {
		if (verifyObjectDisplayed(manualPaymentRadioButton)) {
			clickElementJSWithWait(manualPaymentRadioButton);
			selectElement("createOfferTransaction", nextButton, "Next Button in the Payment Options page");
		//	waitUntilElementInVisible(driver, spinnerContainer, 120);
		//	enterCreditCardDetails();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJSWithWait(nextButton);
			clickElementBy(verifierInfoTextBox);
			clickElementBy(verifierInfoAgentValue);
			verifyPageTitleDisplayed(nextButton, "Details entered in Verifier information", "Verifier information Page",
					"createOfferTransaction");
			selectElement("chooseCreditCardPayment", nextButton, "Next Button in the Verifier information page");
			verifyPageTitleDisplayed(offerTransactionNumber, "Offer Transaction created successfully",
					"Offer transaction details", "chooseCreditCardPayment");
		} else {
			tcConfig.updateTestReporter("Payment options", "chooseCreditCardPayment", Status.FAIL,
					"Unable to enter credit card payment");
		}
	}
				
		


}
