package tgs.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BusinessRulesPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(BusinessRulesPage.class);

	public BusinessRulesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	public static String slaeschannel;
	public String aTSLRule;
	protected By btnNew = By.xpath("//div[@title='New']");
	protected By txtNwBusinessRule = By.xpath("//h2[contains(.,'New Business Rules')]");
	protected By btnNext = By.xpath("//span[contains(.,'Next')]");
	protected By txtSlsChnnlBusinessRule = By
			.xpath("//h2[contains(.,'New Business Rules: Sales Channel Business Rules')]");
	protected By txtBusinessRuleName = By
			.xpath("//label[contains(.,'Business Rules Name')]/../..//input[@name='Name']");
	protected By drpSalesChannel = By
			.xpath("//label[contains(.,'Guest Type')]/../..//input[@class='slds-input slds-combobox__input']");
	protected By drpValue = By.xpath("(//div[@class='select-options'])[3]//a[@title='Call Center']");
	protected By txtAddonPrmMax = By
			.xpath("//label[contains(.,'Add-on Premium Maximum')]/../..//input[@name='Add_on_Premium_Maximum__c']");
	protected By txtMinOfferPrice = By
			.xpath("//label[contains(.,'Minimum Offer Price')]/../..//input[@name='Minimum_Offer_Price__c']");
	protected By txtMaxOfferPrice = By
			.xpath("//label[contains(.,'Maximum Offer Price')]/../..//input[@name='Maximum_Offer_Price__c']");
	protected By txtBasePrmMinNetPrice = By.xpath(
			"//label[contains(.,'Base Premium Minimum Net Price')]/../..//input[@name='Base_Premium_Minimum_Net_Price__c']");
	protected By txtBasePrmMaxNetPrice = By.xpath(
			"//label[contains(.,'Base Premium Maximum Net Price')]/../..//input[@name='Base_Premium_Maximum_Net_Price__c']");
	protected By txtCarMinNetPrice = By
			.xpath("//label[contains(.,'Car Minimum Net price')]/../..//input[@name='Car_Minimum_Net_price__c']");
	protected By txtCarMaxNetPrice = By
			.xpath("//label[contains(.,'Car Maximum Net price')]/../..//input[@name='Car_Maximum_Net_price__c']");
	protected By txtEffStartDate = By
			.xpath("//label[contains(.,'Effective Start Date')]/../..//input[@name='Effective_Start_Date__c']");
	protected By lnkToday = By.xpath("//button[contains(text(),'Today')]");
	protected By btnSave = By.xpath("//button[@title='Save']");
	protected By btnSave1 = By.xpath("//button[@name='SaveEdit']");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By tabBusinessRules = By.xpath("(//span[@class='slds-truncate' and contains(.,'Business Rules')])[1]");
	protected By txtSalesChannel = By
			.xpath("//label[contains(.,'Sales Channel')]/../..//input[@placeholder='Search Sales Channels...']");
	protected By plusIcon = By.xpath("//span[@title='New Sales Channel']");
	protected By txtNwSalesChannel = By.xpath("//h2[contains(.,'New Sales Channel')]");
	protected By radiobtnSelect = By.xpath("(//span[@class='slds-radio--faux'])");
	protected By rdbtnSalesChnnl = By.xpath("(//span[@class='slds-radio--faux'])[2]");
	protected By txtNewSalesChannel = By.xpath("//h2[@class='title slds-text-heading--medium slds-hyphenate']");
	protected By txtSalesChnlName = By.xpath("//span[contains(.,'Sales Channel Name')]/../..//input[@class=' input']");
	protected By drpSlsChannlType = By.xpath("(//span[contains(.,'Type')]/../..//a[@class='select'])[1]");
	protected By drpMarketingLocType = By
			.xpath("//span[contains(.,'Marketing Location Type')]/../..//a[@class='select']");
	protected By txtHrsOfOpt = By.xpath("//span[contains(.,'Hours of Operation')]/../..//input[@class=' input']");
	protected By txtAddress = By.xpath("//span[contains(.,'Address')]/../..//textarea[@class=' textarea']");
	protected By txtSlsChnlEffDt = By
			.xpath("(//span[contains(.,'Effective Start Date')]/../..//input[@class=' input'])[1]");
	protected By txtMasterSlsChnnl = By
			.xpath("//span[contains(.,'Master Sales Channel')]/../..//input[@title='Search Sales Channels']");
	protected By lstTeam = By.xpath("(//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark)[1]");
	protected By btnSaveSlsChnlNew = By.xpath("//button[@title='Save']//span[contains(@class,'label bBody')][normalize-space()='Save']");
	protected By drpValueMktng = By.xpath("(//div[@class='select-options'])[3]//a[@title='On-Site']");
	protected By txtSalesChannelBR = By
			.xpath("//span[contains(.,'Sales Channel')]/../..//div[@class='slds-form-element__control']//a");
	protected By txtCarMinPriceBR = By.xpath(
			"//span[contains(.,'Car Minimum Net price')]/../..//div[@class='slds-form-element__control']//lightning-formatted-number");
	protected By txtCarMaxPriceBr = By.xpath(
			"//span[contains(.,'Car Maximum Net price')]/../..//div[@class='slds-form-element__control']//lightning-formatted-number");
	protected By hypOffers = By.xpath(
			"(//a[@class='slds-context-bar__label-action dndItem']//span[@class='slds-truncate' and contains(.,'Offers')])[1]");
	protected By txtOffers = By.xpath("//span[@title='Offers']");
	protected By rdbtnPrmRest = By
			.xpath("//span[contains(.,'Premium Restrictions')]/../..//span[@class='slds-radio--faux']");
	protected By txtNwBusnRule = By
			.xpath("//h2[contains(@class,'slds-modal__title slds-hyphenate slds-text-heading--medium')]");
	protected By txtPremRestricted = By
			.xpath("//label[contains(.,'Premium Restricted')]/../..//input[@placeholder='Search Products...']");
	protected By select = By.xpath(
			"//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']//lightning-base-combobox-formatted-text");
	protected By stOfResidence = By.xpath("//div[@class='slds-dueling-list__options'][1]//li//span[@title='Alaska']");
	protected By tblChosenLst = By.xpath("//button[@title='Move selection to Chosen']");
	protected By txtSearchBRule = By.xpath("//input[@title='Search Business Rules and more']");
	protected By imgClick = By.xpath("(//img[@class='icon '])[1]");
	protected By tabDetails = By.xpath("//a[@class='slds-tabs_default__link' and @id='detailTab__item']");
	protected By btnEditEffEndDt = By.xpath(
			"(//span[contains(.,'Effective End Date')]/../..//button[@title='Edit Effective End Date']//span)[3]");
	protected By txtEndDt = By.xpath("(//input[@name='End_Date__c'])[1]");
	protected By txtEffEndDt = By.xpath(
			"//span[contains(.,'Effective End Date')]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By rdbtnATSL = By.xpath("//span[contains(.,'ATSL')]/../..//span[@class='slds-radio--faux']");
	protected By drpBrand = By
			.xpath("(//label[contains(.,'Brand')]/following::input[@class='slds-input slds-combobox__input'])[1]");
	protected By drpBValue = By
			.xpath("//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds')]//span[@title='"
					+ testData.get("Brand") + "']");
	protected By searchMSC = By
			.xpath("(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']//span)[1]");
	protected By txtBusinessRule = By.xpath(
			"(//div[contains(@class,'entityNameTitle slds-line-height--reset') and contains(.,'Business Rules')]/../..//lightning-formatted-text)[1]");
	protected By noResult = By.xpath("(//span[contains(@class,'itemLabel slds-truncate slds-show--inline-block')])[1]");
	protected By drpCategory = By.xpath("(//span[contains(.,'Category')]/../..//a[@class='select'])");
	protected By errorMsgSCT = By.xpath("//ul[@class='errorsList slds-list_dotted slds-m-left_medium']//a");
	protected By txtEffDtMSC = By
			.xpath("(//span[contains(.,'Effective Start Date')]/../..//input[@class=' input'])[1]");
	protected By btnSaveMSC = By.xpath("(//span[contains(.,'Save')])[3]");
	protected By sucessMSG = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtMSlsChhnl = By.xpath(
			"//span[contains(.,'Sales Channel Name')]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By txtBusinessRulePage = By
			.xpath("//span[@class='slds-var-p-right_x-small uiOutputText forceBreadCrumbItem']");
	protected By txtMSChannel = By.xpath("//h2[contains(.,'New Sales Channel: Master Sales Channel')]");
	protected By btnEdit = By.xpath("(//button[@name='Edit'])[1]");
	protected By delIcon = By.xpath(
			"//button[@class='slds-button slds-button_icon slds-input__icon slds-input__icon_right']//lightning-primitive-icon");
	protected By txtSearchSaleschannel = By.xpath("//input[@placeholder='Search Sales Channels...']");
	protected By imgNewSalesChannel = By.xpath("//span[@title='New Sales Channel']");
	protected By btnEditSave = By.xpath("//button[@title='Save']");
	protected By detailstab = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[1]");
	protected By txtReservationFee = By
			.xpath("//label[contains(.,'Reservation Fee %')]/../..//input[@name='Reservation_Fee__c']");
	protected By hyplnkSalesChannel = By
			.xpath("//span[contains(.,'Sales Channel')]/../..//div[@class='slds-form-element__control']//a");
	protected By tabRelated = By.xpath("(//a[@id='relatedListsTab__item' and contains(.,'Related')])[1]");
	protected By tabRelatedSlsChnnl = By.xpath("(//a[@id='relatedListsTab__item' and contains(.,'Related')])[2]");
	protected By txtReservationFeeValue = By.xpath(
			"//span[contains(.,'Reservation Fee %')]/../..//div[@class='slds-form-element__control']//lightning-formatted-number");
	protected By txtbusinessRule = By
			.xpath("//div[@class='entityNameTitle slds-line-height--reset' and contains(.,'Business Rules')]");
	protected By buinessRulePage = By.xpath(
			"(//div[contains(@class,'entityNameTitle slds-line-height--reset') and contains(.,'Business Rules')])[2]");
	protected By afterDelEndDt = By.xpath(
			"(//span[contains(.,'Effective End Date')]/../..//div[@class='slds-form-element__control']//lightning-formatted-text)[2]");
	protected By txtCarMinPrice = By
			.xpath("(//span[contains(.,'Car Minimum Net price')])[1]/../..//lightning-formatted-number");
	protected By txtCarMaxPrice = By
			.xpath("(//span[contains(.,'Car Maximum Net price')])[1]/../..//lightning-formatted-number");
	protected By txtBRSlsEndDt = By
			.xpath("//label[contains(.,'Effective End Date')]/../..//input[@name='End_Date__c']");
	protected By drpBusinessUnit = By.xpath("//span[contains(.,'Business Unit')]/../..//div[@class='uiMenu']//a");
	protected By drpBUValue = By.xpath(
			"(//div[contains(@class,'select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short visible positioned')])//li//a[@title='Anaheim WBW (13303-00186)']");
	protected By txtEffEndDT = By.xpath("//span[contains(.,'Effective  End Date')]/../..//input[@class=' input']");
	protected By drpGuestType = By
			.xpath("//label[contains(.,'Guest Type')]/following::input[@class='slds-input slds-combobox__input']");
	protected By drpValueGuest = By.xpath(
			"//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds')]//span[@title='All']");
	protected By txtEndDate = By.xpath("//label[contains(.,'Effective End Date')]/../..//input[@name='End_Date__c']");

	//Updated By Shubhendra
	protected By  btnCancel = By.xpath("//button[@name='CancelEdit']");	
	
	/*
	 * Description: User to navigate Business Rules Tab Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void navigateToBusinessRulesTab() {
		clickElementJSWithWait(tabBusinessRules);
		verifyPageDisplayed(txtBusinessRulePage, "Page Title", "BusinessRulesPage", "navigateToBusinessRulesTab");
	}

	/*
	 * Description: Click New button to proceed Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void clickNew() {
		waitUntilElementVisibleBy(driver, btnNew, "ExplicitLowWait");
		clickElementBy(btnNew);
		verifyPageDisplayed(txtNwBusinessRule, "Page Title", "BusinessRulesPage", "clickNew");

	}

	/*
	 * Description: Create Sales Channel Business Rule Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void createSalesChannelBusinessRules() {
		waitUntilElementVisibleBy(driver, txtNwBusinessRule, "ExplicitMedWait");
		clickElementBy(btnNext);
		waitUntilElementVisibleBy(driver, txtSlsChnnlBusinessRule, 120);
		String strBusinessRuleNm = getRandomStringText(4);
		String strData = "AutomationBR " + strBusinessRuleNm;
		sendKeysBy(txtBusinessRuleName, strData);
		clickElementBy(txtSalesChannel);
		sendKeysBy(txtSalesChannel, " ");
		waitUntilElementVisibleBy(driver, plusIcon, 30);
		clickElementBy(plusIcon);
		verifyPageDisplayed(txtNwSalesChannel, "Page Title", "SalesChannelPage", "createSalesChannelBusinessRules");
		createSalesChannel();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(drpGuestType);
		clickElementBy(drpValueGuest);
		String strAddonPrice = getRandomString(3);
		sendKeysBy(txtAddonPrmMax, strAddonPrice);
		String strMinPrice = getRandomString(2);
		sendKeysBy(txtMinOfferPrice, strMinPrice);
		String strMaxPrice = getRandomString(3);
		sendKeysBy(txtMaxOfferPrice, strMaxPrice);
		String strBasePrmMinPrice = getRandomString(2);
		sendKeysBy(txtBasePrmMinNetPrice, strBasePrmMinPrice);
		String strBasePrmMaxPrice = getRandomString(3);
		sendKeysBy(txtBasePrmMaxNetPrice, strBasePrmMaxPrice);
		String strReservFee = getRandomString(3);
		sendKeysBy(txtReservationFee, strReservFee);
		String strCarMinPrice = getRandomString(2);
		sendKeysBy(txtCarMinNetPrice, strCarMinPrice);
		String strCarMaxPrice = getRandomString(3);
		sendKeysBy(txtCarMaxNetPrice, strCarMaxPrice);
		clickElementBy(txtEffStartDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtBRSlsEndDt);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave1);
		// waitUntilElementVisibleBy(driver, sucessMsg, 120);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("BusinessRulesPage",
		 * "createSalesChannelBusinessRules", Status.PASS,
		 * "New Sales Channel Bsuiness Rule created sucessfully and Message displayed as :- "
		 * );
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "SalesChannelPage", "createSalesChannelBusinessRules");
	}

	/*
	 * Description: Display Car Minimum and MAximum Price Date: August/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void verifyCarPrices() {
		Assert.assertTrue(verifyObjectDisplayed(txtCarMinPrice) && verifyObjectDisplayed(txtCarMaxPrice));
		tcConfig.updateTestReporter("BusinessRulesPage", "verifyCarPrices", Status.PASS,
				"Car Minimum Price displayed as :- " + getElementText(txtCarMinPrice)
						+ " and Car Maximum Price displayed as :- " + getElementText(txtCarMaxPrice));
	}

	/*
	 * Description: Validate Sucess Messgae Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateSucessMsg() {
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("BusinessRulesPage", "validateSucessMsg", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));

	}

	/*
	 * Description: Select Sales Channel in Business Rules Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void selectSalesChannel() {
		waitUntilElementVisibleBy(driver, txtNwSalesChannel, "ExplicitLowWait");
		List<WebElement> lstelements = driver.findElements(radiobtnSelect);
		System.out.println("Number of elements:" + lstelements.size());
		lstelements.get(lstelements.size() - 1).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(btnNext);

	}

	/*
	 * Description: Create Sales Channel Rule in Business Rule Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void createSalesChannel() {
		waitUntilElementVisibleBy(driver, txtNwSalesChannel, "ExplicitLowWait");
		clickElementBy(rdbtnSalesChnnl);
		clickElementBy(btnNext);
		/*
		 * verifyPageDisplayed(txtNewSalesChannel, "Page Title",
		 * "SalesChannelPage", "createSalesChannel");
		 */
		waitUntilElementVisibleBy(driver, txtNewSalesChannel, 120);
		String strSlsNm = getRandomStringText(4);
		String strData = "AutoSalesChannel " + strSlsNm;
		sendKeysBy(txtSalesChnlName, strData);
		clickElementBy(drpSlsChannlType);
		clickElementBy(drpValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(drpBusinessUnit);
		clickElementBy(drpBUValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(drpMarketingLocType);
		clickElementBy(drpValueMktng);
		waitUntilElementVisibleBy(driver, txtHrsOfOpt, 120);
		String strHrsOpt = getRandomString(1);
		sendKeysBy(txtHrsOfOpt, strHrsOpt);
		waitUntilElementVisibleBy(driver, txtAddress, 120);
		fieldDataEnter_modified(txtAddress, testData.get("Address"));
		clickElementBy(txtSlsChnlEffDt);
		selectSystemDate(lnkToday);
		clickElementBy(txtEffEndDT);
		selectSystemDate(lnkToday);
		fieldDataEnter_modified(txtMasterSlsChnnl, testData.get("MasterSalesChannel"));
		clickElementBy(lstTeam);
		clickElementJSWithWait(btnSaveSlsChnlNew);
		// waitUntilElementVisibleBy(driver, sucessMsg, 120);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("BusinessRulesPage",
		 * "createSalesChannel", Status.PASS,
		 * "Sales Channel create sucessfully and Message displayed as :- ");
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "SalesChannelPage", "createSalesChannel");
	}

	/*
	 * Description: To display the Car Minimum and Car Maximum Price Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void clickOfferTab() {

		slaeschannel = getObject(txtSalesChannelBR).getText();
		// slaeschannel = driver.findElement(txtSalesChannelBR).getText();
		if (verifyObjectDisplayed(txtCarMinPriceBR) && verifyObjectDisplayed(txtCarMaxPriceBr)) {
			tcConfig.updateTestReporter("BusinessRulesPage", "clickOfferTab", Status.PASS,
					"Car Minimum Price displayed as :- " + getElementText(txtCarMinPriceBR)
							+ " and Car Maximum Price displayed as :-" + getElementText(txtCarMaxPriceBr));
			clickElementJSWithWait(hypOffers);
		}
	}

	protected By txtPremRest = By.xpath("//h2[contains(.,'New Business Rules: Premium Restrictions')]");

	/*
	 * Description: To select Premium Restriction Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void selectPremiumRestriction() {
		waitUntilElementVisibleBy(driver, txtNwBusinessRule, "ExplicitMedWait");
		clickElementBy(rdbtnPrmRest);
		verifyPageDisplayed(btnNext, "Page Title", "selectPremiumRestriction", "setAccommNetMaxPricePerNight");
		clickElementBy(btnNext);
		verifyPageDisplayed(txtPremRest, "Page Title", "BusinessRulesPage", "selectPremiumRestriction");

	}

	/*
	 * Description: To Add new Premium Restriction Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void createPremiumRestriction() {
		waitUntilElementVisibleBy(driver, txtNwBusnRule, "ExplicitMedWait");
		String strBRNm = getRandomStringText(4);
		sendKeysBy(txtBusinessRuleName, strBRNm);
		fieldDataEnter_modified(txtPremRestricted, testData.get("Product"));
		clickElementBy(select);
		clickElementBy(txtEffStartDate);
		selectSystemDate(lnkToday);
		WebElement wbResidence = driver.findElement(stOfResidence);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		wbResidence.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(0).click();
		clickElementBy(txtEndDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("BusinessRulesPage",
		 * "createPremiumRestriction", Status.PASS,
		 * "Premium Restriction created and Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "BusinessRulesPage", "createPremiumRestriction");
	}

	/*
	 * Description: To select ATSL Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void selectATSLRule() {
		waitUntilElementVisibleBy(driver, txtNwBusinessRule, "ExplicitMedWait");
		verifyPageDisplayed(txtNwBusinessRule, "Page Title", "BusinessRulesPage", "selectATSLRule");
		clickElementBy(rdbtnATSL);
		clickElementBy(btnNext);
		verifyPageDisplayed(txtNwBusnRule, "Page Title", "BusinessRulesPage", "selectATSLRule");
	}

	protected By pickATSL = By.xpath(
			"((//span[contains(.,'Record Type')]/../..//div[@class='slds-form-element__control']//span[contains(.,'ATSL')])[2]//ancestor::div[18]//span[contains(.,'Business Rules Name')])[1]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");

	/*
	 * Description: To create a new ATSL rule Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void createATSLRule() {
		waitUntilElementVisibleBy(driver, txtNwBusnRule, "ExplicitMedWait");
		String strBRNm = getRandomStringText(4);
		sendKeysBy(txtBusinessRuleName, strBRNm);
		clickElementBy(drpBrand);
		clickElementBy(drpBValue);
		clickElementBy(txtEffStartDate);
		selectSystemDate(lnkToday);
		WebElement wbResidence = driver.findElement(stOfResidence);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		wbResidence.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> button = driver.findElements(tblChosenLst);
		button.get(0).click();
		/*
		 * clickElementBy(txtEndDate); selectSystemDate(lnkToday);
		 */
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("BusinessRulesPage", "createATSLRule",
		 * Status.PASS, "ATSL rule created and Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "BusinessRulesPage", "createATSLRule");
		waitUntilElementVisibleBy(driver, txtBusinessRule, "ExplicitWaitLongerTime");
		// aTSLRule = driver.findElement(txtBusinessRule).getText();
		aTSLRule = getObject(pickATSL).getText();
		System.out.println("Rule picked is:-" + aTSLRule);

	}

	/*
	 * Description: To search the newly created ATSL Business Rule Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void searchATSLBusinessRule() {
		waitUntilElementVisibleBy(driver, txtSearchBRule, "ExplicitMedWait");
		fieldDataEnter_modified(txtSearchBRule, aTSLRule);
		clickElementBy(imgClick);
		verifyPageDisplayed(buinessRulePage, "Page Title", "BusinessRulesPage", "searchATSLBusinessRule");
		waitUntilElementVisibleBy(driver, buinessRulePage, "ExplicitMedWait");

	}

	/*
	 * Description: Click on the Sales Channel record to navigate to page Date:
	 * August/2020 Author: Jyoti Changes By: NA
	 */
	public void navigateToSaleChannelPage() {
		waitUntilElementVisibleBy(driver, txtSearchBRule, "ExplicitMedWait");
		clickElementBy(hyplnkSalesChannel);
		verifyPageDisplayed(tabRelatedSlsChnnl, "Page Title", "SalesChannelPage", "navigateToSaleChannelPage");
	}

	/*
	 * Description: Get Reservation Fee value Date: August/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void getReservationFee() {
		waitUntilElementVisibleBy(driver, txtSearchBRule, "ExplicitMedWait");
		getElementText(txtReservationFeeValue);
		Assert.assertTrue(verifyObjectDisplayed(txtReservationFeeValue));
		tcConfig.updateTestReporter("BusinessRulesPage", "getReservationFee", Status.PASS,
				"Reservation Fee displayed as :- " + getElementText(txtReservationFeeValue));
	}

	/*
	 * Description: View updated Reservation Fee in Business Rule Page Date:
	 * August/2020 Author: Jyoti Changes By: NA
	 */
	public void displayUpdatedReservationFee() {
		waitUntilElementVisibleBy(driver, txtbusinessRule, "ExplicitMedWait");
		verifyPageDisplayed(txtbusinessRule, "Page Title", "BusinessRulesPage", "displayUpdatedReservationFee");
		Assert.assertTrue(verifyObjectDisplayed(txtReservationFeeValue));
		tcConfig.updateTestReporter("BusinessRulesPage", "displayUpdatedReservationFee", Status.PASS,
				"Updated Reservation Fee displayed as :- " + getElementText(txtReservationFeeValue));
	}

	/*
	 * Description: To search a Business Rule Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void searchBusinessRule() {
		waitUntilElementVisibleBy(driver, txtSearchBRule, "ExplicitMedWait");
		fieldDataEnter_modified(txtSearchBRule, testData.get("BusinessRule"));
		clickElementBy(imgClick);
		verifyPageDisplayed(tabRelated, "Page Title", "BusinessRulesPage", "searchBusinessRule");
	}

	/*
	 * Description: To delete ATSL Business Rule Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void deleteATSLRule() {
		waitUntilElementVisibleBy(driver, tabDetails, "ExplicitMedWait");
		clickElementJSWithWait(btnEditEffEndDt);
		clickElementJSWithWait(txtEndDt);
		selectSystemDate(lnkToday);
		clickElementJSWithWait(btnSave);
		Assert.assertTrue(verifyObjectDisplayed(afterDelEndDt));
		tcConfig.updateTestReporter("BusinessRulesPage", "deleteATSLRule", Status.PASS,
				"ATSL Business Rule is deleted and End date displayed as :- " + getElementText(afterDelEndDt));
	}

	/*
	 * Description: To validate System restricts Master Sales Channel on Sales
	 * Channel in Business rule page Date: July/2020 Author: Jyoti Changes By:
	 * NA
	 */
	
	
	
	public void validateSaleChannelRestrict() {
        waitUntilElementVisibleBy(driver, txtNwBusinessRule, "ExplicitMedWait");
        clickElementBy(btnNext);
        verifyPageDisplayed(txtSlsChnnlBusinessRule, "Page Title", "BusinessRulesPage", "validateSaleChannelRestrict");
        String strBusinessRuleNm = getRandomStringText(4);
        String strData = "AutoBusinessRule " + strBusinessRuleNm;
        sendKeysBy(txtBusinessRuleName, strData);
        fieldDataEnter_modified(txtSalesChannel, testData.get("MasterSalesChannel"));
        if (verifyObjectDisplayed(searchMSC)) {
            tcConfig.updateTestReporter("BusinessRulesPage", "validateSaleChannelRestrict", Status.PASS,
                    "User cannot view the master sales channel record populating");
        } else {
            tcConfig.updateTestReporter("BusinessRulesPage", "validateSaleChannelRestrict", Status.FAIL,
                    "Failed when master sales channel record populating");
        }
        clickElementBy(plusIcon);
        clickElementBy(btnNext);
        verifyPageDisplayed(txtMSChannel, "Page Title", "BusinessRulesPage", "validateSaleChannelRestrict");
        createMasterSaleChannel();
        clickElementBy(drpGuestType);
        clickElementBy(drpValueGuest);
        String strAddonPrice = getRandomString(2);
        sendKeysBy(txtAddonPrmMax, strAddonPrice);
        String strMinPrice = getRandomString(1);
        sendKeysBy(txtMinOfferPrice, strMinPrice);
        String strMaxPrice = getRandomString(2);
        sendKeysBy(txtMaxOfferPrice, strMaxPrice);
        String strBasePrmMinPrice = getRandomString(1);
        sendKeysBy(txtBasePrmMinNetPrice, strBasePrmMinPrice);
        String strBasePrmMaxPrice = getRandomString(2);
        sendKeysBy(txtBasePrmMaxNetPrice, strBasePrmMaxPrice);
        String strCarMinPrice = getRandomString(1);
        sendKeysBy(txtCarMinNetPrice, strCarMinPrice);
        String strCarMaxPrice = getRandomString(2);
        sendKeysBy(txtCarMaxNetPrice, strCarMaxPrice);
        clickElementBy(txtEffStartDate);
        selectSystemDate(lnkToday);
        clickElementBy(btnSave1);
        /*
         * Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
         * tcConfig.updateTestReporter("BusinessRulesPage",
         * "assignMSCToSaleChannelStoreAssigned", Status.PASS,
         * "Message displayed as :- " +getElementText(errorMsgSCT));
         */
        verifyPageDisplayed(errorMsgSCT, "Page Title", "BusinessRulesPage", "validateSaleChannelRestrict");
        clickElementJSWithWait(btnCancel);
    }

	protected By drpCategoryMSC = By.xpath("(//div[@class='select-options'])//a[@title='Off-Site']");
	protected By txtEffEndDTMSC = By.xpath("(//span[contains(.,'Effective  End Date')]/../..//input[@class=' input'])");

	/*
	 * Description: Create Master Sales Channel Rule Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void createMasterSaleChannel() {
		waitUntilElementVisibleBy(driver, txtNewSalesChannel, "ExplicitLowWait");
		String strSlsNm = getRandomStringText(4);
		String strData = "AutoMSC_" + strSlsNm;
		sendKeysBy(txtSalesChnlName, strData);
		clickElementBy(drpCategory);
		clickElementBy(drpCategoryMSC);
		String strHrOpt = getRandomString(1);
		sendKeysBy(txtHrsOfOpt, strHrOpt);
		fieldDataEnter_modified(txtAddress, testData.get("Address"));
		clickElementBy(txtEffDtMSC);
		selectSystemDate(lnkToday);
		clickElementBy(txtEffEndDTMSC);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveMSC);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMSG));
		 * tcConfig.updateTestReporter("BusinessRulesPage",
		 * "createMasterSaleChannel", Status.PASS,
		 * "Master Sales Channel created sucessfully message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMSG, "Page Title", "BusinessRulesPage", "createMasterSaleChannel");
		waitUntilElementVisibleBy(driver, txtMSlsChhnl, "ExplicitLongWait");
	}

	/*
	 * Description: Create Master Sales Channel Rule Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void editSalesChannel() {
		waitUntilElementVisibleBy(driver, detailstab, "ExplicitLowWait");
		clickElementBy(btnEdit);
		clickElementBy(delIcon);
		fieldDataEnter_modified(txtSearchSaleschannel, testData.get("MasterSalesChannel"));
		Assert.assertTrue(verifyObjectDisplayed(imgNewSalesChannel));
		tcConfig.updateTestReporter("BusinessRulesPage", "editSalesChannel", Status.PASS,
				"Master Sales Channel not displayed");
		clickElementBy(imgNewSalesChannel);
		clickElementBy(btnNext);
		verifyPageDisplayed(txtMSChannel, "Page Title", "BusinessRulesPage", "validateSaleChannelRestrict");
		createMasterSaleChannel();
		waitUntilElementVisibleBy(driver, txtNwBusnRule, "ExplicitMedWait");
		clickElementJSWithWait(btnEditSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
		 * tcConfig.updateTestReporter("BusinessRulesPage", "editSalesChannel",
		 * Status.PASS, "Message displayed as :- "
		 * +getElementText(errorMsgSCT));
		 */
		verifyPageTitleDisplayed(errorMsgSCT, "Edit Sales Channel", "BusinessRulesPage", "editSalesChannel");
	}

	/* Record specific elements - Business Rule */
	protected String businessRuleName, salesChannelName;
	protected By businessRulesTab = By.xpath("//span[text()='Business Rules']");
	protected By addedBusinessRule = By.xpath("//span[text()='Business Rules Name']/following::a[6]");
	protected By salesChLinkedToBrule = By.xpath("//span[text()='Business Rules Name']/following::a[7]");

	/*
	 * Method: addBusRuleSalesChannel Description: Add Business Rule and Sales
	 * Channel Date: July/2020 Author: Krishnaraj Kaliyamoorthy Changes By: NA
	 */
	public void addBusRuleSalesChannel() {
		/*
		 * BusinessRulesPage bRules = new BusinessRulesPage(tcConfig); //
		 * clickElementJSWithWait(businessRulesTab); bRules.clickNew();
		 * bRules.createSalesChannelBusinessRules();
		 * clickElementJSWithWait(businessRulesTab);
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		businessRuleName = getObject(addedBusinessRule).getText();
		// salesChannelName = getObject(salesChLinkedToBrule).getText();
		verifyPageTitleDisplayed(addedBusinessRule, "Added business rule", "BusinessRulesPage",
				"createSalesChannelBusinessRules");
		/*
		 * verifyPageTitleDisplayed(salesChLinkedToBrule,
		 * "Business Rule linked to Sales Channel:", "BusinessRulesPage",
		 * "createSalesChannelBusinessRules");
		 */
	}

}
