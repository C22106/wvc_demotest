package tgs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class TeamsPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(TeamsPage.class);

	public TeamsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	protected By txtTeamsPage = By.xpath(
			"//span[contains(.,'Teams') and @class='slds-var-p-right_x-small uiOutputText forceBreadCrumbItem']");
	protected By btnNew = By.xpath("//a[@class='forceActionLink']//div[@title='New']");
	protected By txtNewTeams = By
			.xpath("//h2[contains(@class,'slds-modal__title slds-hyphenate slds-text-heading--medium')]");
	protected By txtTeamsName = By.xpath("//label[contains(.,'Teams Name')]/following::div[1]/input");
	protected By txtSupervisorName = By.xpath("//label[contains(.,'Supervisor Name')]/following::input[1]");
	protected By txtManagerName = By.xpath("//label[contains(.,'Manager Name')]/following::input[1]");
	protected By txtEffectiveStartDate = By.xpath("//input[@name='Effective_Start_Date__c']");
	protected By txtEffectiveEndDate = By.xpath("//input[@name='Effective_Start_Date__c']");
	protected By txtTeamsEffectiveStartDate = By.xpath("//label[text()='Effective Start Date']//following::input[1]");
	protected By lnkToday = By.xpath("//button[contains(text(),'Today')]");
	protected By btnSave = By.xpath("//button[@title='Save']");
	protected By btnAgentSave = By.xpath("//button[contains(@title,'Save')]//span[contains(@class,'label bBody')][normalize-space()='Save']");
	protected By btnSaveOnNewTeams = By.xpath("//button[@name='SaveEdit']");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtAgentValue = By
			.xpath("(//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark)[1]");
	protected By txtManagerValue = By
			.xpath("(//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark)[2]");
	protected By txtSupNwAgent = By.xpath("(//span[@title='New Offers Agent'])[1]");
	protected By txtMangerNwAgent = By.xpath("(//span[@title='New Offers Agent'])[2]");
	protected By txtNewOfferSAgent = By
			.xpath("//h2[contains(@class,'title slds-text-heading--medium slds-hyphenate')]");
	protected By txtOfferAgentName = By.xpath("//span[contains(.,'Offer Agents Name')]/../..//input[@class=' input']");
	protected By txtFirstName = By.xpath("//span[contains(.,'First Name')]/../..//input[@class=' input']");
	protected By txtLastName = By.xpath("//span[contains(.,'Last Name')]/../..//input[@class=' input']");
	protected By txtAgentWWID = By.xpath("//span[contains(.,'Agent WWID')]/../..//input[@class=' input']");
	protected By drpAgentType = By.xpath("//span[contains(.,'Agent Type')]/../..//div[@class='uiMenu']//a");
	protected By drpAgentValue = By.xpath("(//div[@class='select-options'])[2]//ul//li//a[@title='Offer Sales Agent']");

	protected By dateLink = By.xpath("//button[text()='Today']");
	
	//Xpath by Shubhendra
	protected By txtTeamsEffectiveStartDateNew = By.xpath("(//a[@class='datePicker-openIcon display'])[1]");
	protected By txtTeamsEffectiveEndDateNew = By.xpath("(//a[@class='datePicker-openIcon display'])[2]");

	/*
	 * Description: Click on New button on Page Date: August/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void clickNew() {
		waitUntilElementVisibleBy(driver, txtTeamsPage, "ExplicitMedWait");
		verifyPageDisplayed(txtTeamsPage, "Page Title", "TeamsPage", "clickNew");
		clickElementJSWithWait(btnNew);
		verifyPageDisplayed(txtNewTeams, "Page Title", "TeamsPage", "clickNew");
	}

	/*
	 * Description: Create a new Teams record Date: August/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void createNewTeams() {
		waitUntilElementVisibleBy(driver, txtNewTeams, "ExplicitMedWait");
		String strTeamsNm = getRandomStringText(7);
		sendKeysBy(txtTeamsName, strTeamsNm);
		clickElementBy(txtSupervisorName);
		clickElementBy(txtSupNwAgent);
		verifyPageDisplayed(txtNewOfferSAgent, "Page Title", "TeamsPage", "createNewTeams");
		createOfferAgent();
		waitUntilElementVisibleBy(driver, txtNewTeams, "ExplicitMedWait");
		clickElementBy(txtManagerName);
		clickElementJSWithWait(txtMangerNwAgent);
		sendKeysBy(txtMangerNwAgent, " ");
		verifyPageDisplayed(txtNewOfferSAgent, "Page Title", "TeamsPage", "createNewTeams");
		createOfferAgent();
		clickElementBy(txtTeamsEffectiveStartDate);
		selectSystemDate(lnkToday);
		clickElementJSWithWait(btnSaveOnNewTeams);
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("TeamsPage", "createNewTeams", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));
	}

	/*
	 * Description: Create a new Offer Agent record Date: August/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void createOfferAgent() {
		waitUntilElementVisibleBy(driver, txtNewOfferSAgent, "ExplicitMedWait");
		String strOffNm = getRandomStringText(5);
		sendKeysBy(txtOfferAgentName, strOffNm);
		String strFirstNm = getRandomStringText(5);
		sendKeysBy(txtFirstName, strFirstNm);
		String strLastNm = getRandomStringText(5);
		sendKeysBy(txtLastName, strLastNm);
		String strAgentWWID = getRandomString(6);
		sendKeysBy(txtAgentWWID, strAgentWWID);
		clickElementBy(drpAgentType);
		clickElementJSWithWait(drpAgentValue);
        waitForSometime(tcConfig.getConfig().get("MedWait"));
        clickElementBy(txtTeamsEffectiveStartDateNew);
        clickElementJSWithWait(dateLink);
        waitForSometime(tcConfig.getConfig().get("MedWait"));
        clickElementBy(txtTeamsEffectiveEndDateNew);
        clickElementJSWithWait(dateLink);
		clickElementBy(btnAgentSave);
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("TeamsPage", "createOfferAgent", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));
	}
	
	
	/*TGS -E2E Scenario Methods*/
	
	
	protected By txtSupervisorNm = By.xpath("(//label[contains(.,'Supervisor Name')]/following::input[@placeholder='Search Offers Agents...'])[1]");
	protected By txtManagerNm = By .xpath("(//label[contains(.,'Manager Name')]/following::input[@placeholder='Search Offers Agents...'])[1]");
	protected By txtSelectSup = By.xpath("(//span[@class='slds-media__body']//lightning-base-combobox-formatted-text)[1]");
	protected By txtSelectMan = By.xpath("(//span[@class='slds-media__body']//lightning-base-combobox-formatted-text)[3]");
	protected By txtEndDT = By.xpath("//label[contains(.,'Effective End Date')]/following::input[@name='End_Date__c']");
	protected  By btnSaveTeams = By.xpath("//button[contains(.,'Save') and @name='SaveEdit']");

	
	/*
	 * Description: Create a new Teams record Date: August/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void createNewTeam() {
		waitUntilElementVisibleBy(driver, txtNewTeams, "ExplicitMedWait");		
		String strTeamsNm = getRandomStringText(4);
		String strTeam = "Teams " +strTeamsNm;
		sendKeysBy(txtTeamsName, strTeam);		
		fieldDataEnter(txtSupervisorNm, testData.get("SupervisorName"));		
		clickElementBy(txtSelectSup);
		waitUntilElementVisibleBy(driver, txtManagerNm, "ExplicitMedWait");	
		fieldDataEnter(txtManagerNm, testData.get("ManagerName"));		
		clickElementBy(txtSelectMan);		
		clickElementBy(txtTeamsEffectiveStartDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtEndDT);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveTeams);
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("TeamsPage", "createNewTeams", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));
	}

}