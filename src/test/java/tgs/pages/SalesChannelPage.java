package tgs.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SalesChannelPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(SalesChannelPage.class);

	public SalesChannelPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	public String OfferText;
	public String masterSalesChannel;
	JavascriptExecutor je = (JavascriptExecutor) driver;
	protected By txtGlobalSearch = By.xpath("//input[@placeholder='Search Sales Channels and more...']");
	protected By imgClick = By.xpath("//img[@class='icon ' and contains(@src,'custom/custom59_120.png')]");

	protected By tabDetails = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[1]");
	protected By tabRelated = By.xpath("(//a[@id='relatedListsTab__item' and contains(.,'Related')])[1]");
	protected By hypSalesChannelDestination = By.xpath("//span[@title='Sales Channel Destinations']");
	protected By lnkOfferDisclosure = By.xpath("//span[@title='Offer Disclosures']");
	protected By txtOfferDisclosure = By.xpath("//h1[@title='Offer Disclosures']");
	protected By editOfferDisclosure = By
			.xpath("(//span[@title='Offer Disclosures'])[1]//..//..//..//..//..//..//table/tbody/tr[1]/td[4]/div");
	protected By btnEdit = By.xpath("//a[@title='Edit']");
	protected By lnkSalesStore = By.xpath(
			"//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')]//a[starts-with(.,'SCSA-')]");
	protected By hypRecord = By.xpath(
			"//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table')]//a[starts-with(.,'SCPA-')]");
	protected By txtEditForm = By.xpath("//div[@class='modal-header slds-modal__header']//h2");
	protected By txtOfferDisclosuretext = By.xpath("//span[contains(.,'Offer Disclosure Text')]/../..//textarea");
	protected By txtSalesTeam = By
			.xpath("//span[@class='slds-truncate slds-m-right--xx-small' and @title='Sales Channel Teams']");
	protected By txtSalesTeamPage = By.xpath("//h1[@class='slds-page-header__title listViewTitle slds-truncate']");
	protected By lstSup = By.xpath("(//div[@class='primaryLabel slds-truncate slds-lookup__result-text']//mark)[1]");
	protected By sucessMsg = By.xpath("//span[contains(@class,'toastMessage slds-text-heading')]");
//	protected By btnSave = By.xpath("//button[@title='Save']");
	protected By offerDisclosurerecord = By
			.xpath("(//span[@title='Offer Disclosures'])[1]//..//..//..//..//..//..//table/tbody/tr[1]/th/div/a");
	protected By offerDisclosuretext = By
			.xpath("(//span[contains(.,'Offer Disclosure Text')])[1]/../..//div[2]//lightning-formatted-text");
	protected By tblButton = By.xpath(
			"(//table[@data-aura-class='uiVirtualDataTable'])[2]//tbody//tr[1]//td[9]//lightning-primitive-icon");
	protected By hyplnkRecord = By.xpath("(//table[@data-aura-class='uiVirtualDataTable'])[2]//tbody//tr[1]//th//a");
	//protected By lnkSalesChanneTeams = By.xpath("(//span[@title='Sales Channel Teams'])[1]");
	protected By btnNew = By.xpath("//div[@title='New']");
	protected By txtNewSalesChannelTeam = By.xpath("//h2[contains(.,'New Sales Channel Team')]");
	protected By txtSearchTeam = By.xpath("//input[@placeholder='Search Teams...']");
	protected By lstTeam = By.xpath("//*[@class='lookup__item  default uiAutocompleteOption forceSearchInputLookupDesktopOption']//a[*]");
	protected By dtEffectiveStartDt = By
			.xpath("//span[contains(.,'Effective Start Date')]/../..//input[@name='Effective_Start_Date__c']");
	protected By dtEffEndDate = By
			.xpath("//label[contains(.,'Effective End Date')]/../..//input[@name='Effective_End_Date__c']");
	protected By lnkToday = By.xpath("//button[contains(text(),'Today')]");
	protected By plusIcon = By.xpath("//span[@title='New Teams']");
	protected By txtNewTeams = By.xpath("//h2[contains(.,'New Teams')]");
	protected By txtTeamsName = By.xpath("//span[contains(.,'Teams Name')]/../..//input[@class=' input']");
	protected By txtSupervisorName = By
			.xpath("//span[contains(.,'Supervisor Name')]/../..//input[@placeholder='Search Offers Agents...']");
	protected By txtManagerName = By
			.xpath("//span[contains(.,'Manager Name')]/../..//input[@placeholder='Search Offers Agents...']");
	protected By sucessMSG = By.xpath("(//span[@class='toastMessage slds-text-heading--small forceActionsText'])[1]");
	protected By hypSalesTeam = By.xpath(
			"(//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table')])[2]//a[starts-with(.,'SCT-')]");
	protected By detailstab = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[2]");
	protected By txtTeamsEffDate = By
			.xpath("(//span[contains(.,'Effective Start Date')]/../..//input[@class=' input'])[1]");
	protected By txtTeamsEndDate = By
			.xpath("(//span[contains(.,'Effective End Date')]/../..//input[@class=' input'])[1]");
	protected By btnSaveNwTeam = By.xpath("(//button[@title='Save']//span)");
	//protected By btnNewSaleTeam = By.xpath("//span[@title='Sales Channel Teams']/following::div[@title='New']");
	protected By txtTeams = By.xpath("//span[contains(.,'Teams')]/../..//input[@placeholder='Search Teams...']");
	// protected By txtSlsEndDt = By.xpath("(//label[contains(.,'Effective End
	// Date')]/../..//input[@name='Effective_End_Date__c'])[1]");
	// protected By txtSlsEffDt = By.xpath("//label[contains(.,'Effective Start
	// Date')]/following::input[@name='Effective_Date__c']");
	protected By lnkSalesChnlPremumAssgnd = By.xpath("//span[@title='Sales Channel Premiums Assigneds']");
	//protected By btnNewPremAssg = By.xpath("(//span[@title='Sales Channel Premiums Assigneds']/following::a[@class='forceActionLink'])[1]//div[@title='New']");
	protected By txtNwSlsPremAssgn = By.xpath("//h2[contains(.,'New Sales Channel Premiums Assigned')]");
	protected By txtSearchProducts = By
			.xpath("//span[contains(.,'Product')]/../..//input[@placeholder='Search Products...']");
	protected By txtProduct = By
			.xpath("(//span[@class='slds-media__body']//lightning-base-combobox-formatted-text)[1]");
	protected By errorMsg = By.xpath("//h2[@class='slds-truncate slds-text-heading_medium']");
	protected By btnEditOD = By.xpath("(//button[@name='Edit'])[2]");
	//protected By btnCancel = By.xpath("//button[@title='Cancel']");
	protected By txtNewSalesChannel = By.xpath(
			"//h2[contains(@class,'inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p-bottom--medium slds-text-heading--medium')]");
	protected By txtSalesChnlName = By.xpath("//label[contains(.,'Sales Channel Name')]/../..//input[@name='Name']");
	protected By drpCategory = By
			.xpath("//label[contains(.,'Category')]/following::input[@class='slds-input slds-combobox__input']");
	protected By txtHrsOfOpt = By
			.xpath("//label[contains(.,'Hours of Operation')]/../..//input[@name='Hours_of_Operation__c']");
	protected By txtAddress = By.xpath("//label[contains(.,'Address')]/../..//textarea[@class='slds-textarea']");
	protected By txtSlsChnlEffDt = By
			.xpath("(//label[contains(.,'Effective Start Date')]/../..//input[@name='Effective_Start_Date__c'])");
	protected By btnNext = By.xpath("//span[contains(.,'Next')]");
	protected By drpValue = By.xpath(
			"//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds-')]//span[@title='3rd party']");
	protected By drpMarketingLocType = By.xpath(
			"(//label[contains(.,'Marketing Location Type')]/following::input[@class='slds-input slds-combobox__input'])[1]");
	protected By drpValueMktng = By.xpath(
			"//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds')]//span[@title='On-Site']");
	protected By txtMasterSlsChnnl = By
			.xpath("//label[contains(.,'Master Sales Channel')]/../..//input[@placeholder='Search Sales Channels...']");
	protected By drpSlsValue = By
			.xpath("//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds')]//span[@title='"
					+ testData.get("FeeType") + "']");
	protected By txtNewSC = By.xpath("//h2[@class='slds-modal__title slds-hyphenate slds-text-heading--medium']");
	protected By drpType = By
			.xpath("//label[contains(.,'Type')]/following::input[@class='slds-input slds-combobox__input']");
	protected By txtMSlsChhnl = By.xpath(
			"//span[contains(.,'Sales Channel Name')]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By rdbtnSlsChnnl = By.xpath("(//span[@class='slds-radio--faux'])[2]");
	protected By btnSavePrmAssign = By.xpath("//button[@name='SaveEdit']");
	protected By erroMsgSls = By.xpath("//ul[@class='errorsList slds-list_dotted slds-m-left_medium']//li");
	// protected By lstData =
	// By.xpath("(//span[@class='slds-listbox__option-text
	// slds-listbox__option-text_entity']//lightning-base-combobox-formatted-text)[1]");
	protected By lnkTermsCondition = By.xpath("//span[@title='Terms & Conditions']");
	//protected By btnSlsTeamNew = By.xpath("(//div[contains(@class,'slds-page-header slds-page-header_joined slds-page-header')])[5]//div[@title='New']");
	protected By tabDetailsSlsAssgn = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[3]");
	protected By delIcon = By.xpath("//button[@title='Clear Selection']//lightning-primitive-icon");
	protected By txtNwSlsChnnl = By.xpath("//span[@title='New Sales Channel']");
	protected By lnkOffrDiscloures = By.xpath(
			"((//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')])[2]//tbody//tr//th//a[@data-aura-class='forceOutputLookup'])[1]");
	protected By txtOffdis = By
			.xpath("(//span[contains(.,'Offer Disclosure Text')]/following::lightning-formatted-text)[1]");
	protected By txtSearchSaleschannel = By.xpath("//input[@placeholder='Search Sales Channels...']");
	protected By noResult = By.xpath("(//span[@class='slds-media__body']//span[@class='slds-truncate'])[1]");
	protected By txtNewSlsChannel = By.xpath("(//h2[contains(.,'New Sales Channel')])[2]");
	protected By saerchTeams = By.xpath("//label[contains(.,'Teams')]/../..//input[@placeholder='Search Teams...']");
	protected By errorMsgSCT = By.xpath("//div[@class='fieldLevelErrors']//ul");
	//protected By btnSaveMSC = By.xpath("(//button[@title='Save'])[2]");
	protected By txtEffDtMSC = By
			.xpath("(//span[contains(.,'Effective Start Date')]/../..//input[@class=' input'])[1]");
	protected By txtEndDtMSC = By
			.xpath("(//span[contains(.,'Effective  End Date')]/following::div[@class='form-element']//input)[2]");
	protected By txtMSChannel = By.xpath("//h2[@class='title slds-text-heading--medium slds-hyphenate']");
	protected By txtMSCName = By.xpath("//span[contains(.,'Sales Channel Name')]/../..//input[@class=' input']");
	protected By drpCategoryMSC = By.xpath("//span[contains(.,'Category')]/following::a[@class='select']");
	protected By drpValueMSC = By.xpath("//div[@class='select-options']//a[@title='3rd party']");
	protected By tstHRSOPT = By.xpath("//span[contains(.,'Hours of Operation')]/../..//input[@class=' input']");
	protected By txtMSCAddress = By.xpath("//span[contains(.,'Address')]/../..//textarea[@class=' textarea']");
	protected By searchMSC = By
			.xpath("(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']//span)[1]");
	protected By txtSalesChannel = By
			.xpath("//span[@class='slds-var-p-right_x-small uiOutputText forceBreadCrumbItem']");
	protected By btnOffrDisEdit = By.xpath("(//a[@title='Edit'])[2]");
	protected By txtOffrDis = By
			.xpath("//h2[contains(@class,'slds-modal__title slds-hyphenate slds-text-heading--medium')]");
	protected By delIconSlsChnnal = By.xpath("(//button[@title='Clear Selection']//lightning-primitive-icon)[2]");
	protected By searchSlsChannl = By.xpath(
			"(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']//span[@class='slds-truncate'])[1]");
	protected By txtSaleChnnlStoreAssgined = By.xpath("//h2[contains(.,'New Sales Channel  Stores Assigned')]");
	protected By lnkSaleChannlStoreAssigned = By.xpath("//span[@title='Sales Channel Stores Assigned']");
	protected By txtSlsStoreAssgined = By.xpath("//h1[@title='Sales Channel Stores Assigned']");
	protected By lstSaleStore = By.xpath(
			"//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')]//tbody//td[2]//a");
	protected By lnkSalesChannelBusinessRule = By.xpath(
			"(//span[@title='Sales Channel Business Rules' and @class='slds-truncate slds-m-right--xx-small'])[2]");
	protected By txtSlsChnnlBusinesSRule = By.xpath("//h1[@title='Sales Channel Business Rules']");
	protected By actionBtn = By.xpath(
			"//table[contains(@class,'slds-table slds-table_header-fixed slds-table')]//button[contains(@class,'slds-button slds-button_icon-border slds')]//lightning-primitive-icon");
	protected By btnEditBR = By.xpath("(//div[@class='branding-actions actionMenu']//ul//li//a[@title='Edit'])[1]");
	protected By txtEditRecord = By.xpath("//h2[@class='slds-modal__title slds-hyphenate slds-text-heading--medium']");
	protected By txtReservationFee = By
			.xpath("//label[contains(.,'Reservation Fee %')]/../..//input[@name='Reservation_Fee__c']");
	protected By btnSaveEdit = By.xpath("//button[@title='Save']");
	protected By txtReservationFeeValue = By.xpath(
			"//span[contains(.,'Reservation Fee %')]/../..//div[@class='slds-form-element__control']//lightning-formatted-number");
	protected By hyplnkSlsChlBR = By.xpath("(//table[@data-aura-class='uiVirtualDataTable'])[2]//tbody//tr//th//a");
	protected By tabRelatedSlsChnnl = By.xpath("(//a[@id='relatedListsTab__item' and contains(.,'Related')])[2]");
	protected By lnkSlsBR = By.xpath(
			"(//span[@title='Sales Channel Business Rules' and @class='slds-truncate slds-m-right--xx-small'])[2]");
	protected By tabRelatedClick = By.xpath("(//a[@id='relatedListsTab__item' and contains(.,'Related')])[3]");
	protected By txtCarMaxNetPrice = By
			.xpath("//label[contains(.,'Car Maximum Net price')]/../..//input[@name='Car_Maximum_Net_price__c']");
	protected By drpBusinessUnit = By.xpath(
			"(//label[contains(.,'Business Unit')]/following::input[@class='slds-input slds-combobox__input'])[1]");
	protected By drpBUValue = By.xpath(
			"//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds')]//span[@title='Anaheim SVC (387-000HM)']");
	protected By txtEffEndDT = By
			.xpath("//label[contains(.,'Effective  End Date')]/../..//input[@name='Effective_End_Date__c']");
	protected By btnAccomodationRestricted = By.xpath(
			"//span[@title='Accommodations Restricted']//ancestor::article//button[@name='New']");
	protected By txtsearchAccomodation = By
			.xpath("//label[contains(.,'Accommodation')]/../..//input[@placeholder='Search Accommodations...']");
	protected By btnSCPremAssigned = By.xpath(
			"((//div[contains(@class,'slds-page-header slds-page-header_joined slds-page-header_bleed slds-shrink-none test')])[3]//span[@title='Sales Channel Premiums Assigneds']/following::div[@title='New'])[1]");
	protected By txtEffStartDT = By
			.xpath("//label[contains(.,'Effective Start Date')]/following::input[@name='Effective_Start_Date__c']");
	protected By txtEffecEndDate = By.xpath("//input[@name='Effective_End_Date__c']");
	//protected By btnSlsChnStoreAssign = By.xpath("//div[@class='windowViewMode-normal oneContent active lafPageHost']//a[@title='New']");
	protected By txtSearchSalesStore = By.xpath("//label[contains(.,'Sales Store')]/following::input[1]");
	protected By drpGuestType = By.xpath(
			"(//label[text()='Guest Type']//following::div[@class='slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click'])[1]");
	protected By drpGTValue = By.xpath(
			"//lightning-base-combobox-item[@class='slds-media slds-listbox__option slds-media_center slds-media_small slds-listbox__option_plain' and @data-value='All']");
	protected By txtSlsEffDt = By.xpath("//input[@name='Effective_Date__c']");
	protected By txtSlsEndDt = By.xpath("//input[@name='End_Date__c']");
	protected By lstData = By.xpath("//div[@class='modal-container slds-modal__container']//li[1]");
	protected By saveButtonModal = By
			.xpath("//div[@class='modal-container slds-modal__container']//button[@name='SaveEdit']");
	protected By toastSuccessMessage = By.xpath(
			"//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage' and @data-key = 'success']");
	protected By lnkClickRecord = By.xpath(
			"//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']//a[@class='forceActionLink']");
	protected By txtMSC = By.xpath("//h2[@class='slds-modal__title slds-hyphenate slds-text-heading--medium']");
	protected By tsxtMSEndDT = By
			.xpath("//label[contains(.,'Effective  End Date')]/following::input[@name='Effective_End_Date__c']");
	protected By effStartDate = By.xpath("(//span[contains(.,'Effective Start Date')])[6]");
	protected By teams = By.xpath("(//span[contains(.,'Teams')])[4]");
	protected By lnkOffers = By.xpath("//span[@title='Offers' and @class='slds-truncate slds-m-right--xx-small']");
	protected By txtOffersPage = By
			.xpath("//h1[@title='Offers' and contains(@class,'slds-page-header__title listViewTitle slds-truncate')]");
	protected By actionBtnOffer = By.xpath(
			"(//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')]//tbody//td[7]//a//lightning-primitive-icon)[1]");
	protected By txtBasePrmMinNetPrice = By.xpath(
			"(//span[contains(.,'Base Premium Minimum Net Price')])[3]/../..//input[@class='input uiInputSmartNumber']");
	protected By txtBPMnetPrice = By.xpath(
			"//span[contains(.,'Base Premium Minimum Net Price')]/../..//div[@class='slds-form-element__control']//lightning-formatted-text");
	protected By hypOSC = By.xpath(
			"((//table[contains(@class,'slds-table forceRecordLayout slds-table--header-fixed slds-table')])[2]//tbody//tr//th)[1]//a");
	protected By txtOSCPage = By
			.xpath("//div[@class='entityNameTitle slds-line-height--reset' and contains(.,'Offer Sales Channel')]");
	protected By tabDeatils = By.xpath("(//a[@id='detailTab__item' and contains(.,'Details')])[2]");
	protected By txtEffStartDt = By
			.xpath("//label[contains(.,'Effective Start Date')]/following::input[@name='Effective_Start_Date__c']");
	protected By txtEndDT = By
			.xpath("//label[contains(.,'Effective End Date')]/following::input[@name='Effective_End_Date__c']");
	
	protected By hypOffers = By.xpath("(//span[@title='Offers'])[2]");
	
	
	
	protected  By txtsatrtDate  = By.xpath("//label[contains(.,'Effective Start Date')]/following::input[@name='Effective_Date__c']");
	
	
	protected By btnNewSaleTeam = By.xpath("//span[@title='Sales Channel Teams']//ancestor::article//button[@name='New']");	

	protected By btnNewPremAssg = By.xpath("//span[@title='Sales Channel Premiums Assigneds']//ancestor::article//button[@name='New']");

	protected By  btnCancel = By.xpath("//button[@name='CancelEdit']");	
	
	protected By  btnCancel1 = By.xpath("//button[@title='Cancel']");	

	protected By  btnSave = By.xpath("//button[@name='SaveEdit']");

	protected By lnkSalesChanneTeams = By.xpath("//span[@title='Sales Channel Teams']");


	protected By btnSlsTeamNew = By.xpath("//span[@title='Sales Channel Teams']//ancestor::article//button[@name='New']");

	protected By btnSlsChnStoreAssign = By.xpath("//span[@title='Sales Channel Stores Assigned']//ancestor::article//button[@name='New']");

	protected By btnSaveMSC = By.xpath("(//button[@title='Save'])[1]");
	
	
	
	/*
	 * Description: Global Search for a Sales Channel and navigate to details
	 * page Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void searchSalesChannel() throws Exception {
		waitUntilElementVisibleBy(driver, txtGlobalSearch, "ExplicitMedWait");
		verifyPageDisplayed(txtSalesChannel, "Page Title", "SalesChannelPage", "searchSalesChannel");
		fieldDataEnter_modified(txtGlobalSearch, testData.get("SalesChannel"));
		clickElementJSWithWait(imgClick);
		waitUntilElementVisibleBy(driver, tabDetails, "ExplicitMedWait");
		Assert.assertTrue(verifyObjectDisplayed(tabDetails));
		tcConfig.updateTestReporter("SalesChannelPage", "searchSalesChannel", Status.PASS,
				"User navigated to Sales Channel details page Successfully");
	}

	/*
	 * Description: Click on Related tab of a record Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void clickRelatedTab() throws Exception {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");
		clickElementJSWithWait(tabRelated);
		waitUntilElementVisibleBy(driver, tabDetails, "ExplicitMedWait");
		verifyPageDisplayed(tabRelated, "Page Title", "SalesChannelPage", "clickRelatedTab");
	}

	/*
	 * Description: Navigate to Offer Disclosure section to edit any Text field
	 * Date: July/2020 Author: Jyoti Changes By: NA
	 */
	
	public void validateEditOfferDisclosure() throws AWTException
	{
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");		
		do {
			 Robot r = new Robot();
			 r.mouseWheel(6);
			 waitForSometime(tcConfig.getConfig().get("MedWait"));
	      } while (!driver.findElement(hypOffers).isDisplayed());	
		scrollDownByPixel(300);
		waitUntilElementVisibleBy(driver, lnkOfferDisclosure, "ExplicitLongWait");		
		clickElementJSWithWait(lnkOfferDisclosure);			
		waitUntilElementVisibleBy(driver, lnkOffrDiscloures, "ExplicitLongWait");
		verifyPageDisplayed(lnkOffrDiscloures, "Page Title", "SalesChannelPage",
				"validateEditOfferDisclosure");
		clickElementBy(lnkOffrDiscloures); 
		//waitUntilElementVisibleBy(driver, lnkOffrDiscloures, "ExplicitLongWait");
		waitUntilElementVisibleBy(driver, txtOffdis, 120);		
		verifyPageDisplayed(txtOffdis, "Page Title", "SalesChannelPage",
				"validateEditOfferDisclosure");		
		waitUntilElementVisibleBy(driver, btnEditOD, 120);	
		clickElementJSWithWait(btnEditOD);	
		waitUntilElementVisibleBy(driver, txtMSC, "ExplicitLongWait");	
	    verifyPageDisplayed(txtMSC, "Edit Offer Disclosure", "SalesChannelPage",
				"validateEditOfferDisclosure");	    	    
	    clearElementBy(txtOfferDisclosuretext);	    
	     OfferText = getRandomStringText(4);
	     sendKeysBy(txtOfferDisclosuretext, OfferText);               
        clickElementBy(btnSave);      
        verifyPageDisplayed(sucessMsg, "Page Title", "SalesChannelPage",
				"validateEditOfferDisclosure");
        /*Assert.assertTrue(verifyObjectDisplayed(sucessMsg));	       
        tcConfig.updateTestReporter("SalesChannelPage", "validateEditOfferDisclosure", LogStatus.PASS,
				"Message displayed as :-" +getElementText(sucessMsg));*/       	    
	}


	/*
	 * Description: Navigate to Offer Disclosure section to and restrict the
	 * master sales channel Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void assignMSUToOfferDisclosure() {
		clickElementJSWithWait(btnEditOD);
		waitUntilElementVisibleBy(driver, txtOffrDis, "ExplicitMedWait");
		verifyPageDisplayed(txtOffrDis, "Page Title", "SalesChannelPage", "assignMSUToOfferDisclosure");
		clickElementBy(delIconSlsChnnal);
		waitUntilElementVisibleBy(driver, txtSearchSaleschannel, 120);
		fieldDataEnter_modified(txtSearchSaleschannel, testData.get("MasterSalesChannel"));
		if (verifyObjectDisplayed(searchSlsChannl)) {
			tcConfig.updateTestReporter("SalesChannelPage", "assignMSUToOfferDisclosure", Status.PASS,
					"User cannot view the master sales channel record populating");
		} else {
			tcConfig.updateTestReporter("SalesChannelPage", "assignMSUToOfferDisclosure", Status.FAIL,
					"Failed when master sales channel record populating");
		}
		clickElementBy(txtNwSlsChnnl);
		waitUntilElementVisibleBy(driver, txtNewSlsChannel, "ExplicitLongWait");
		verifyPageDisplayed(btnNext, "Page Title", "SalesChannelPage", "assignMSUToOfferDisclosure");
		clickElementBy(btnNext);
		createMSUForTeams();
		waitUntilElementVisibleBy(driver, txtNewSlsChannel, "ExplicitLongWait");
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
		 * tcConfig.updateTestReporter("SalesChannelPage",
		 * "assignMSUToOfferDisclosure", Status.PASS, "Message displayed as :- "
		 * +getElementText(errorMsgSCT));
		 */
		verifyPageDisplayed(errorMsgSCT, "Error Message displayed", "SalesChannelPage", "assignMSUToOfferDisclosure");
	}

	/*
	 * Description: Validate Offer Disclosure is updated Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void verifyFieldUpdated() {
		// (driver, hyplnkRecord, "ExplicitMedWait");
		// clickElementJSWithWait(hyplnkRecord);
		waitUntilElementVisibleBy(driver, offerDisclosuretext, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(offerDisclosuretext));
		tcConfig.updateTestReporter("SalesChannelPage", "verifyFieldUpdated", Status.PASS,
				"Offer Disclosure record is edited Successfully and displayed as:- " + OfferText);
	}

	/*
	 * Description: Navigate to Offer Disclosure section to edit any Text field
	 * Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void createNewSalesChannelTeams() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitLongWait");
/*		do {
			 Robot r = new Robot();
			 r.mouseWheel(6);
		} while (!driver.findElement(lnkTermsCondition).isDisplayed());*/
//		Robot r = new Robot();
//		r.mouseWheel(10);
        scrollDownByPixel(900);       
		waitUntilElementVisibleBy(driver, lnkSalesChanneTeams, "ExplicitMedWait");
		clickElementJSWithWait(btnSlsTeamNew);
		waitUntilElementVisibleBy(driver, txtNewSalesChannelTeam, "ExplicitLowWait");
		verifyPageDisplayed(txtNewSalesChannelTeam, "Page Title", "SalesChannelPage", "createNewSalesChannelTeams");
		clickElementBy(txtTeams);
		scrollDownForElementJSBy(plusIcon);
		waitUntilElementVisibleBy(driver, plusIcon, 120);
		clickElementBy(plusIcon);
		createNewTeams();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(txtEffStartDt);
		selectSystemDate(lnkToday);
		clickElementBy(txtEndDT);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("SalesChannelPage",
		 * "createNewSalesChannelTeams", Status.PASS, "Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "SalesChannelPage", "createNewSalesChannelTeams");
	}

	/*
	 * Description: Validate Sucess Messgae Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateSucessMsg() {
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("SalesChannelPage", "validateSucessMsg", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));

	}

	/*
	 * Description: Click to navigate to newly created record details page Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void clickNewRecord() {
		clickElementJSWithWait(txtSalesTeam);
		waitUntilElementVisibleBy(driver, txtSalesTeamPage, "ExplicitLongWait");
		getListElement(hypSalesTeam);
		waitUntilElementVisibleBy(driver, detailstab, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(detailstab));
		tcConfig.updateTestReporter("SalesChannelPage", "clickNewRecord", Status.PASS,
				"Sucessfully navigate to details page");

	}

	/*
	 * Description: User needs to use a new Teams record that is not assigned to
	 * Sales Channel Teams Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void createNewTeams() {
		waitUntilElementVisibleBy(driver, txtTeamsName, "ExplicitMedWait");
		verifyPageDisplayed(txtNewTeams, "Page Title", "SalesChannelPage", "createNewTeams");
		String teamName = getRandomStringText(4);
		String strData = "AutomationDataTeams_" + teamName;
		sendKeysBy(txtTeamsName, strData);
		waitUntilElementVisibleBy(driver, txtSupervisorName, 120);
		fieldDataEnter_modified(txtSupervisorName, testData.get("SupervisorName"));
		clickElementBy(lstSup);
		fieldDataEnter_modified(txtManagerName, testData.get("ManagerName"));
		clickElementBy(lstTeam);
		waitUntilElementVisibleBy(driver, txtTeamsEffDate, "ExplicitMedWait");
		clickElementBy(txtTeamsEffDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtTeamsEndDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveNwTeam);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("SalesChannelPage", "createNewTeams",
		 * Status.PASS, "Message displayed as :- " +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "SalesChannelPage", "createNewTeams");
	}

	/*
	 * Description:Create Sales Channel Premium Assigned Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	
	
	public void verifySaleChnlPremiumAssignOverlap() {
        waitUntilElementVisibleBy(driver, lnkSalesChnlPremumAssgnd, 120);
        clickElementJSWithWait(btnNewPremAssg);
        verifyPageDisplayed(txtNwSlsPremAssgn, "Page Title", "SalesChannelPage", "verifySaleChnlPremiumAssignOverlap");
        fieldDataEnter_modified(txtSearchProducts, testData.get("Product"));
        waitForSometime(tcConfig.getConfig().get("LowWait"));
        clickElementBy(lstData);
        fieldDataEnter_modified(txtEffStartDT, testData.get("EffectiveStartDate"));
        fieldDataEnter_modified(txtEffecEndDate, testData.get("EffectiveEndDate"));
        clickElementBy(btnSave);

 

    }

	/*
	 * Description:Validate Error Message display Date: July/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void validateErrorMsg() {
		// Assert.assertTrue(verifyObjectDisplayed(errorMsg));
		/*
		 * String strmsg =driver.findElement(errorMsg).getText();
		 * tcConfig.updateTestReporter("SalesChannelPage", "validateErrorMsg",
		 * Status.PASS, "Error message displayed sucessfully " +strmsg);
		 */
		waitUntilElementVisibleBy(driver, errorMsg, 120);
		if (verifyObjectDisplayed(errorMsg)) {
			tcConfig.updateTestReporter("SalesChannelPage", "validateErrorMsg", Status.PASS,
					"Error message displayed sucessfully");
		} else {
			tcConfig.updateTestReporter("SalesChannelPage", "validateErrorMsg", Status.FAIL,
					"Error message displayed sucessfully");
		}
	}

	/*
	 * Description:Click Cancel button to proceed with next Test case Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void clickCancel() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnCancel1);

	}
	
	/*
	 * Description:Click Cancel button to proceed with next Test case Date: to differentiate Cancel but on Ext page 
	 * Mar/2021 Author: Shubhendra Changes By: NA
	 */
	public void clickCancelOnNewExt() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnCancel);

	}

	/*
	 * Description:Click Cancel button to on new sales channel team and Sales channel Store Assigned team
	 * Mar/2021 Author: Shubhendra Changes By: NA
	 */
	public void clickCancelUpdated() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnCancel);

	}
	/*
	 * Description:Verify Sales Channel Team Overlapping dates Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void verifySaleChnlTeamOverlap() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, 120);
		do {
			Robot r = new Robot();
			r.mouseWheel(6);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(lnkSalesChanneTeams).isDisplayed());
		waitUntilElementVisibleBy(driver, lnkSalesChanneTeams, 120);
		clickElementJSWithWait(btnNewSaleTeam);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		verifyPageDisplayed(txtNewSalesChannelTeam, "Page Title", "SalesChannelPage", "verifySaleChnlTeamOverlap");
		fieldDataEnter_modified(txtTeams, testData.get("Teams"));
		clickElementBy(lstData);
		fieldDataEnter_modified(txtEffStartDT, testData.get("EffectiveStartDate"));
		fieldDataEnter_modified(txtEffecEndDate, testData.get("EffectiveEndDate"));
		clickElementBy(btnSave);
	}

	/*
	 * Description: Select Master Sales Channel and Click Next Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void selectMasterSalesChannel() {
		waitUntilElementVisibleBy(driver, txtSalesChannel, "ExplicitLowWait");
		verifyPageDisplayed(txtSalesChannel, "Page Title", "SalesChannelPage", "selectMasterSalesChannel");
		clickElementBy(btnNew);
		verifyPageDisplayed(txtNewSalesChannel, "Page Title", "BusinessRulesPage", "selectMasterSalesChannel");
		clickElementJSWithWait(btnNext);
		verifyPageDisplayed(txtMSC, "Page Title", "BusinessRulesPage", "selectMasterSalesChannel");
	}

	/*
	 * Description: Create Master Sales Channel Rule Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void createMasterSalesChannel() {
		waitUntilElementVisibleBy(driver, txtMSC, "ExplicitLowWait");
		String strSlsNm = getRandomStringText(4);
		String strData = "AutoMSC_" + strSlsNm;
		sendKeysBy(txtSalesChnlName, strData);
		clickElementBy(drpCategory);
		clickElementBy(drpValue);
		String strHrOpt = getRandomString(1);
		sendKeysBy(txtHrsOfOpt, strHrOpt);
		fieldDataEnter_modified(txtAddress, testData.get("Address"));
		clickElementBy(txtSlsChnlEffDt);
		selectSystemDate(lnkToday);
		clickElementBy(tsxtMSEndDT);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMSG));
		 * tcConfig.updateTestReporter("SalesChannelPage",
		 * "createMasterSalesChannel", Status.PASS,
		 * "Master Sales Channel created sucessfully message displayed as :- ");
		 */
		verifyPageDisplayed(sucessMSG, "Page Title", "SalesChannelPage", "createMasterSalesChannel");
		waitUntilElementVisibleBy(driver, txtMSlsChhnl, "ExplicitLongWait");
		masterSalesChannel = driver.findElement(txtMSlsChhnl).getText();
	}

	/*
	 * Description: Select Sales Channel and Click Next Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void selectSalesChannel() {
		waitUntilElementVisibleBy(driver, txtSalesChannel, "ExplicitLowWait");
		verifyPageDisplayed(txtSalesChannel, "Page Title", "SalesChannelPage", "selectSalesChannel");
		clickElementBy(btnNew);
		verifyPageDisplayed(txtNewSalesChannel, "Page Title", "SalesChannelPage", "selectSalesChannel");
		clickElementBy(rdbtnSlsChnnl);
		clickElementBy(btnNext);
		verifyPageDisplayed(txtNewSC, "Page Title", "SalesChannelPage", "selectSalesChannel");
	}

	/*
	 * Description: Create a new Sales Channel Record Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void createSalesChannel() {
		waitUntilElementVisibleBy(driver, txtNewSC, "ExplicitLowWait");
		String strSlsNm = getRandomStringText(4);
		String strData = "AtuoSaleChannel_" + strSlsNm;
		sendKeysBy(txtSalesChnlName, strData);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(drpType);
		clickElementBy(drpSlsValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(drpBusinessUnit);
		clickElementBy(drpBUValue);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(drpMarketingLocType);
		clickElementBy(drpValueMktng);
		String strHrOpt = getRandomString(1);
		sendKeysBy(txtHrsOfOpt, strHrOpt);
		fieldDataEnter_modified(txtAddress, testData.get("Address"));
		clickElementBy(txtSlsChnlEffDt);
		selectSystemDate(lnkToday);
		clickElementBy(txtEffEndDT);
		selectSystemDate(lnkToday);
		fieldDataEnter_modified(txtMasterSlsChnnl, masterSalesChannel);
		clickElementBy(txtProduct);
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMSG));
		 * tcConfig.updateTestReporter("SalesChannelPage", "createSalesChannel",
		 * Status.PASS,
		 * "Sales Channel record created sucessfully and Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMSG, "Page Title", "SalesChannelPage", "createSalesChannel");
	}

	/*
	 * Description: Create a new Sales Channel Premium Assigned Record Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void createSalesChannelPremiumAssign() {
		scrollDownByPixel(500);
		waitUntilElementVisibleBy(driver, lnkSalesChnlPremumAssgnd, "ExplicitMedWait");
		if (verifyObjectDisplayed(btnNewPremAssg)) {
			clickElementJSWithWait(btnNewPremAssg);
			waitUntilElementVisibleBy(driver, txtNwSlsPremAssgn, "ExplicitMedWait");
			verifyPageDisplayed(txtNwSlsPremAssgn, "Page Title", "SalesChannelPage", "createSalesChannelPremiumAssign");
			fieldDataEnter_modified(txtSearchProducts, testData.get("Product"));
			clickElementBy(lstData);
			clickElementBy(dtEffectiveStartDt);
			selectSystemDate(lnkToday);
			clickElementBy(dtEffEndDate);
			selectSystemDate(lnkToday);
			clickElementBy(btnSavePrmAssign);
			/*
			 * Assert.assertTrue(verifyObjectDisplayed(sucessMSG));
			 * tcConfig.updateTestReporter("SalesChannelPage",
			 * "createSalesChannelPremiumAssign", Status.PASS,
			 * "Sales Channel Premium Assigned record created sucessfully");
			 */
			verifyPageDisplayed(sucessMSG, "Page Title", "SalesChannelPage", "createSalesChannelPremiumAssign");
			waitUntilElementVisibleBy(driver, lnkSalesChnlPremumAssgnd, "ExplicitLongWait");
			clickElementJSWithWait(lnkSalesChnlPremumAssgnd);
			waitUntilElementVisibleBy(driver, txtSalesTeamPage, "ExplicitLongWait");
			getListElement(hypRecord);
			verifyPageDisplayed(tabDetailsSlsAssgn, "Page Title", "SalesChannelPage",
					"createSalesChannelPremiumAssign");
		} else {
			tcConfig.updateTestReporter("SalesChannelPage", "createSalesChannelPremiumAssign", Status.FAIL,
					"Failed while creating Sales Channel Premium Assigned record");
		}
	}

	/*
	 * Description: Assign Master Sales channel to a Sales Channel Teams record
	 * Date: July/2020 Author: Jyoti Changes By: NA
	 */
	
	
	public void assignMSCtoSalesChannelTeams() throws AWTException {
        waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");

 

 /*       do {
            Robot r = new Robot();
            r.mouseWheel(4);
            waitForSometime(tcConfig.getConfig().get("LowWait"));
        } while (!driver.findElement(lnkTermsCondition).isDisplayed());*/
        scrollDownByPixel(600);       
        waitUntilElementVisibleBy(driver, lnkSalesChanneTeams, "ExplicitMedWait");
        clickElementJSWithWait(btnSlsTeamNew);
        verifyPageDisplayed(txtNewSalesChannelTeam, "Page Title", "SalesChannelPage", "assignMSCtoSalesChannelTeams");
        clickElementBy(delIcon);
        fieldDataEnter_modified(txtSearchSaleschannel, testData.get("MasterSalesChannel"));
        if (verifyObjectDisplayed(noResult)) {
            tcConfig.updateTestReporter("SalesChannelPage", "assignMSCtoSCT", Status.PASS,
                    "User cannot view the master sales channel record populating");
        } else {
            tcConfig.updateTestReporter("SalesChannelPage", "assignMSCtoSCT", Status.FAIL,
                    "Failed when master sales channel record populating");
        }
        clickElementBy(txtNwSlsChnnl);
        waitUntilElementVisibleBy(driver, txtNewSlsChannel, "ExplicitMedWait");
        clickElementBy(btnNext);
        createMSUForTeamModified();
        waitUntilElementVisibleBy(driver, saerchTeams, "ExplicitMedWait");
        fieldDataEnter_modified(saerchTeams, testData.get("Teams"));
        clickElementBy(lstTeam);
        clickElementBy(txtEffStartDT);
        selectSystemDate(lnkToday);
        clickElementBy(btnSave);
        /*
         * Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
         * tcConfig.updateTestReporter("SalesChannelPage", "assignMSCtoSCT",
         * Status.PASS, "Message displayed as :- "
         * +getElementText(errorMsgSCT));
         */
        verifyPageDisplayed(errorMsgSCT, "Page Title", "SalesChannelPage", "assignMSCtoSalesChannelTeams");

 

    }
	/*
	 * Description: Create Master Sales Channel Rule Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */

	public void createMSUForTeams() {
		waitUntilElementVisibleBy(driver, txtMSChannel, "ExplicitLowWait");
		verifyPageDisplayed(txtMSChannel, "Page Title", "SalesChannelPage", "createMSUForTeams");
		String strSlsNm = getRandomStringText(4);
		String strData = "AutomationDataMSC " + strSlsNm;
		sendKeysBy(txtMSCName, strData);
		clickElementBy(drpCategoryMSC);
		clickElementBy(drpValueMSC);
		String strHrOpt = getRandomString(1);
		sendKeysBy(tstHRSOPT, strHrOpt);
		fieldDataEnter_modified(txtMSCAddress, testData.get("Address"));
		clickElementBy(txtEffDtMSC);
		selectSystemDate(lnkToday);
		clickElementBy(txtEndDtMSC);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveMSC);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMSG));
		 * tcConfig.updateTestReporter("SalesChannelPage", "createMSUForTeams",
		 * Status.PASS,
		 * "Master Sales Channel created sucessfully message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMSG, "Page Title", "SalesChannelPage", "createMSUForTeams");
		waitUntilElementVisibleBy(driver, txtMSlsChhnl, "ExplicitLongWait");
		masterSalesChannel = driver.findElement(txtMSlsChhnl).getText();
	}

	/*
	 * Description: Create Master Sales Channel Rule Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */

	public void createMSUForTeamModified() {
		waitUntilElementVisibleBy(driver, txtMSChannel, "ExplicitLowWait");
		verifyPageDisplayed(txtMSChannel, "Page Title", "SalesChannelPage", "createMSUForTeams");
		String strSlsNm = getRandomStringText(4);
		String strData = "AutomationDataMSC " + strSlsNm;
		sendKeysBy(txtMSCName, strData);
		clickElementBy(drpCategoryMSC);
		clickElementBy(drpValueMSC);
		String strHrOpt = getRandomString(1);
		sendKeysBy(tstHRSOPT, strHrOpt);
		fieldDataEnter_modified(txtMSCAddress, testData.get("Address"));
		clickElementBy(txtEffDtMSC);
		selectSystemDate(lnkToday);
		clickElementBy(txtEndDtMSC);
		selectSystemDate(lnkToday);
		clickElementBy(btnSaveMSC);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMSG));
		 * tcConfig.updateTestReporter("SalesChannelPage", "createMSUForTeams",
		 * Status.PASS,
		 * "Master Sales Channel created sucessfully message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		verifyPageDisplayed(sucessMSG, "Page Title", "SalesChannelPage", "createMSUForTeams");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleBy(driver, txtMSlsChhnl, "ExplicitLongWait");
		masterSalesChannel = driver.findElement(txtMSlsChhnl).getText();
	}

	/*
	 * Description: assign Master Sale Channel to Sale Store Assigned Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	


public void assignMSCToSaleChannelStoreAssigned() throws AWTException {
    waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");                
    /*do {
         Robot r = new Robot();
         r.mouseWheel(3);
         waitForSometime(tcConfig.getConfig().get("LowWait"));
      } while (!driver.findElement(btnSlsChnStoreAssign).isDisplayed());    */
    scrollDownByPixel(50);       
    waitUntilElementVisibleBy(driver, btnSlsChnStoreAssign, "ExplicitMedWait");        
    clickElementJSWithWait(btnSlsChnStoreAssign);    
    verifyPageDisplayed(txtSaleChnnlStoreAssgined, "Page Title", "SalesChannelPage",
            "assignMSCToSaleChannelStoreAssigned");    
    waitUntilElementVisibleBy(driver, txtSaleChnnlStoreAssgined, "ExplicitMedWait");    
    clickElementJSWithWait(delIcon);                                                                                   
    fieldDataEnter_modified(txtSearchSaleschannel, testData.get("MasterSalesChannel"));    
    if (verifyObjectDisplayed(searchMSC)) {
        tcConfig.updateTestReporter("SalesChannelPage", "assignMSCToSaleChannelStoreAssigned", Status.PASS,
                "User cannot view the master sales channel record populating");
    } else {
        tcConfig.updateTestReporter("SalesChannelPage", "assignMSCToSaleChannelStoreAssigned", Status.FAIL,
                "Failed when master sales channel record populating");
    }
    clickElementBy(txtNwSlsChnnl);                    
    waitUntilElementVisibleBy(driver, txtNewSlsChannel, "ExplicitMedWait");
    clickElementBy(btnNext);
    createMSUForTeamModified();
    waitUntilElementVisibleBy(driver, txtSearchSalesStore, "ExplicitMedWait");
    fieldDataEnter_modified(txtSearchSalesStore, testData.get("SalesStore"));    
    clickElementBy(lstData);
    clickElementBy(drpGuestType);
    clickElementBy(drpGTValue);
    clickElementJSWithWait(txtsatrtDate);
    selectSystemDate(lnkToday);
    clickElementBy(btnSave);      
    /*
     * Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
     * tcConfig.updateTestReporter("SalesChannelPage",
     * "assignMSCToSaleChannelStoreAssigned", Status.PASS,
     * "Message displayed as :- " +getElementText(errorMsgSCT));
     */
    verifyPageDisplayed(errorMsgSCT, "Page Title", "SalesChannelPage", "assignMSCToSaleChannelStoreAssigned");



}
	/*
	 * Description: assign Master Sale Channel to Accomodation Restricted Date:
	 * July/2020 Author: Jyoti Changes By: NA
	 */
	public void assignMSCToAccomodationRestricted() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");
	/*	do {
			Robot r = new Robot();
			r.mouseWheel(5);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(btnAccomodationRestricted).isDisplayed());*/
        scrollDownByPixel(4000);       
		waitUntilElementVisibleBy(driver, btnAccomodationRestricted, "ExplicitMedWait");
		clickElementJSWithWait(btnAccomodationRestricted);
		verifyPageDisplayed(txtsearchAccomodation, "Page Title", "BusinessRulesPage",
				"assignMSCToAccomodationRestricted");
		clickElementBy(delIcon);
		fieldDataEnter_modified(txtSearchSaleschannel, testData.get("MasterSalesChannel"));
		if (verifyObjectDisplayed(searchMSC)) {
			tcConfig.updateTestReporter("SalesChannelPage", "assignMSCToAccomodationRestricted", Status.PASS,
					"User cannot view the master sales channel record populating");
		} else {
			tcConfig.updateTestReporter("SalesChannelPage", "assignMSCToAccomodationRestricted", Status.FAIL,
					"Failed when master sales channel record populating");
		}
		clickElementBy(txtNwSlsChnnl);
		waitUntilElementVisibleBy(driver, txtMSChannel, "ExplicitLongWait");
		clickElementBy(btnNext);
		waitUntilElementVisibleBy(driver, txtMSChannel, 120);
		verifyPageDisplayed(txtMSChannel, "Page Title", "BusinessRulesPage", "assignMSCToAccomodationRestricted");
		createMSUForTeams();
		waitUntilElementVisibleBy(driver, txtsearchAccomodation, "ExplicitMedWait");
		fieldDataEnter_modified(txtsearchAccomodation, testData.get("Accommodation"));
		clickElementBy(lstData);
		clickElementBy(txtEffStartDT);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
		 * tcConfig.updateTestReporter("SalesChannelPage",
		 * "assignMSCToAccomodationRestricted", Status.PASS,
		 * "Message displayed as :- " +getElementText(errorMsgSCT));
		 */
		verifyPageDisplayed(errorMsgSCT, "Page Title", "BusinessRulesPage", "assignMSCToAccomodationRestricted");

	}

	/*
	 * Description: assign Master Sale Channel to Sales Channel Premium Assigned
	 * Date: July/2020 Author: Jyoti Changes By: NA
	 */
	
	
	public void assignMSCToSaleChannelPremiumAssigned() throws AWTException {
        waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");               
 /*       do {
             Robot r = new Robot();
             r.mouseWheel(4);
             waitForSometime(tcConfig.getConfig().get("LowWait"));
          } while (!driver.findElement(btnSCPremAssigned).isDisplayed()); */ 
        scrollDownByPixel(100);       
        waitUntilElementVisibleBy(driver, lnkSalesChnlPremumAssgnd, 120);              
        clickElementJSWithWait(btnNewPremAssg);       
        verifyPageDisplayed(txtNwSlsPremAssgn, "Page Title", "SalesChannelPage",
                "assignMSCToSaleChannelPremiumAssigned");   
        waitUntilElementVisibleBy(driver, txtNwSlsPremAssgn, "ExplicitMedWait");   
        clickElementJSWithWait(delIcon);                                                                               
        fieldDataEnter_modified(txtSearchSaleschannel, testData.get("MasterSalesChannel"));   
        if (verifyObjectDisplayed(searchMSC)) {
            tcConfig.updateTestReporter("SalesChannelPage", "assignMSCToSaleChannelPremiumAssigned", Status.PASS,
                    "User cannot view the master sales channel record populating");
        } else {
            tcConfig.updateTestReporter("SalesChannelPage", "assignMSCToSaleChannelPremiumAssigned", Status.FAIL,
                    "Failed when master sales channel record populating");
        }
        clickElementBy(txtNwSlsChnnl);                                       
        clickElementBy(btnNext);
        createMSUForTeamModified();
        waitUntilElementVisibleBy(driver, txtSearchProducts, "ExplicitMedWait");
        fieldDataEnter_modified(txtSearchProducts, testData.get("Product"));   
        clickElementBy(lstData);                  
        clickElementJSWithWait(txtEffStartDt);   
        selectSystemDate(lnkToday);                   
        clickElementBy(btnSave);
        /*
         * Assert.assertTrue(verifyObjectDisplayed(errorMsgSCT));
         * tcConfig.updateTestReporter("SalesChannelPage",
         * "assignMSCToAccomodationRestricted", Status.PASS,
         * "Message displayed as :- " +getElementText(errorMsgSCT));
         */
        verifyPageDisplayed(errorMsgSCT, "Page Title", "SalesChannelPage", "assignMSCToSaleChannelPremiumAssigned");
    }
	/*
	 * Description: Edit Reservation Fee in Sales Channel PAge Date: July/2020
	 * Author: Jyoti Changes By: NA
	 */
	public void editReservationFee() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelatedSlsChnnl, "ExplicitMedWait");
		clickElementJSWithWait(tabRelatedSlsChnnl);
		verifyPageDisplayed(tabRelatedSlsChnnl, "Page Title", "SalesChannelPage", "EditReservationFee");
		waitUntilElementVisibleBy(driver, lnkSalesChannelBusinessRule, "ExplicitMedWait");
		clickElementJSWithWait(actionBtn);
		clickElementJSWithWait(btnEditBR);
		verifyPageDisplayed(txtEditRecord, "Page Title", "SalesChannelPage", "EditReservationFee");
		clearElementBy(txtReservationFee);
		String strReservFee = getRandomString(2);
		sendKeysBy(txtReservationFee, strReservFee);
		clickElementBy(txtCarMaxNetPrice);
		waitUntilElementVisibleBy(driver, txtEditRecord, "ExplicitMedWait");
		clickElementJSWithWait(btnSaveEdit);
		verifySuccessDisplayed(sucessMSG, "SalesChannelPage", "EditReservationFee");
		waitUntilElementVisibleBy(driver, tabRelatedClick, "ExplicitMedWait");
		clickElementJSWithWait(tabRelatedClick);
		waitUntilElementVisibleBy(driver, lnkSalesChannelBusinessRule, "ExplicitMedWait");
		clickElementJSWithWait(lnkSlsBR);
		verifyPageDisplayed(txtSlsChnnlBusinesSRule, "Page Title", "SalesChannelPage", "editReservationFee");
		clickElementJSWithWait(hyplnkSlsChlBR);
	}

	protected By txtEndDate = By
			.xpath("(//label[contains(.,'Effective End Date')]/../..//input[@name='End_Date__c'])[1]");

	/*
	 * Description: assign SaleStore to Sale Channel Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void assignSaleStoreToSaleChannel() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");
		do {
			Robot r = new Robot();
			r.mouseWheel(4);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(lnkTermsCondition).isDisplayed());
		waitUntilElementVisibleBy(driver, btnSlsChnStoreAssign, "ExplicitMedWait");
		clickElementJSWithWait(lnkSaleChannlStoreAssigned);
		verifyPageDisplayed(txtSlsStoreAssgined, "Page Title", "SalesChannelPage", "assignSaleStoreToSaleChannel");
		List<WebElement> saleStore = driver.findElements(lstSaleStore);
		System.out.println(saleStore.size());
		boolean flag = false;
		for (int i = 0; i <= (saleStore.size() - 1); i++) {
			if (saleStore.get(i).getText() != null) {
				flag = true;
				tcConfig.updateTestReporter("SalesChannelPage", "assignSaleStoreToSaleChannel", Status.PASS,
						"Validated sucessfully Sales Store having Parent " + saleStore.get(i).getText());
			} else {
				flag = false;
			}
		}
		clickElementJSWithWait(btnSlsChnStoreAssign);
		verifyPageDisplayed(txtSaleChnnlStoreAssgined, "Page Title", "SalesChannelPage",
				"assignSaleStoreToSaleChannel");
		waitUntilElementVisibleBy(driver, txtSearchSalesStore, "ExplicitMedWait");
		fieldDataEnter_modified(txtSearchSalesStore, testData.get("SalesStore"));
		clickElementBy(lstData);
		clickElementBy(drpGuestType);
		clickElementBy(drpGTValue);
		clickElementBy(txtSlsEffDt);
		selectSystemDate(lnkToday);
		clickElementBy(txtSlsEndDt);
		selectSystemDate(lnkToday);
		tcConfig.updateTestReporter("SalesChannelPage", "assignSaleStoreToSaleChannel", Status.PASS,
				"Fields with data values entered");
		clickElementBy(saveButtonModal);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(toastSuccessMessage));
		 * tcConfig.updateTestReporter("SalesChannelPage",
		 * "assignSaleStoreToSaleChannel", Status.PASS,
		 * "Message displayed as :- " +getElementText(toastSuccessMessage));
		 */
		verifyPageDisplayed(toastSuccessMessage, "Page Title", "SalesChannelPage", "assignSaleStoreToSaleChannel");
		/*
		 * clickElementJSWithWait(lnkSaleChannlStoreAssigned);
		 * verifyPageDisplayed(txtSlsStoreAssgined, "Page Title",
		 * "SalesChannelPage", "assignSaleStoreToSaleChannel");
		 * getListElement(lnkSalesStore); verifyPageDisplayed(tabDeatils,
		 * "Page Title", "SalesChannelPage", "assignSaleStoreToSaleChannel");
		 */

	}

	/*
	 * Description: To validate that the error is displayed when user tries to
	 * add new sales channel team to assigned sales channel without entering
	 * mandatory fields. Date: July/2020 Author: Jyoti Changes By: NA
	 */
	public void validateSalesChannelTeams() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");
		do {
			Robot r = new Robot();
			r.mouseWheel(4);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(lnkTermsCondition).isDisplayed());
		waitUntilElementVisibleBy(driver, lnkSalesChanneTeams, "ExplicitMedWait");
		clickElementJSWithWait(btnSlsTeamNew);
		verifyPageDisplayed(txtNewSalesChannelTeam, "Page Title", "SalesChannelPage", "validateSalesChannelTeams");
		Assert.assertTrue(verifyObjectDisplayed(effStartDate) && verifyObjectDisplayed(teams)
				&& verifyObjectDisplayed(btnSave) && verifyObjectDisplayed(btnCancel));
		tcConfig.updateTestReporter("SalesChannelPage", "validateSalesChannelTeams", Status.PASS,
				"Validate sucessfully fields present are :- Effective Start date, Teams, Save and Cancel button");
		clickElementBy(btnSave);
		verifyPageDisplayed(erroMsgSls, "Page Title", "SalesChannelPage", "validateSalesChannelTeams");
		clickElementBy(txtSlsEffDt);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
		verifyPageDisplayed(erroMsgSls, "Page Title", "SalesChannelPage", "validateSalesChannelTeams");
		clearElementBy(txtSlsEffDt);
		waitUntilElementVisibleBy(driver, saerchTeams, "ExplicitMedWait");
		fieldDataEnter_modified(saerchTeams, testData.get("Teams"));
		clickElementBy(lstTeam);
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(errorMsg));
		 * tcConfig.updateTestReporter("SalesChannelPage",
		 * "validateSalesChannelTeams", Status.PASS, "Message displayed as :- "
		 * +getElementText(errorMsg));
		 */
		verifyPageDisplayed(erroMsgSls, "Page Title", "SalesChannelPage", "validateSalesChannelTeams");
	}

	/*
	 * Description:Dispaly all offers present in page Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void validateOffers() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");
/*		do {
			Robot r = new Robot();
			r.mouseWheel(4);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} while (!driver.findElement(lnkOffers).isDisplayed());*/
	//	scrollDownJavaScriptExecutor(lnkOffers);
        scrollDownByPixel(900);       

	//	waitForSometime(tcConfig.getConfig().get("LowWait"));
	//	scrollDownForElementJSBy(lnkOffers);
		waitUntilElementVisibleBy(driver, lnkOffers, "ExplicitMedWait");
		clickElementJSWithWait(lnkOffers);
		verifyPageDisplayed(txtOffersPage, "Page Title", "SalesChannelPage", "validateOffers");
		List<WebElement> saleStore = driver.findElements(lstSaleStore);
	
		boolean flag = false;
		for (int i = 0; i <= (saleStore.size() - 1); i++) {
			if (saleStore.get(i).getText() != null) {
				flag = true;
				tcConfig.updateTestReporter("SalesChannelPage", "validateOffers", Status.PASS,
						"Offers present in page " + saleStore.get(i).getText());
			} else {
				flag = false;
			}
		}
		clickElementJSWithWait(hypOSC);
		verifyPageDisplayed(txtOSCPage, "Page Title", "SalesChannelPage", "editOffers");
		Assert.assertTrue(verifyObjectDisplayed(txtBPMnetPrice));
		tcConfig.updateTestReporter("SalesChannelPage", "editOffers", Status.PASS,
				"Base Premium minimum Net Price before edit displayed as :- " + getElementText(txtBPMnetPrice));
		driver.navigate().back();
		scrollDownJavaScriptExecutor(lnkOffers);
		clickElementJSWithWait(lnkOffers);
	} 

	/*
	 * Description: Edit Mandatory field in Offer record Date: July/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void editOffers() throws AWTException {
		waitUntilElementVisibleBy(driver, txtOffersPage, "ExplicitMedWait");
		verifyPageDisplayed(txtOffersPage, "Page Title", "SalesChannelPage", "editOffers");
		clickElementJSWithWait(actionBtnOffer);
		clickElementJSWithWait(btnEditBR);
		verifyPageDisplayed(txtEditRecord, "Page Title", "SalesChannelPage", "editOffers");
		waitUntilElementVisibleBy(driver, txtBasePrmMinNetPrice, "ExplicitMedWait");
		scrollDownByPixel(200);
		clearElementBy(txtBasePrmMinNetPrice);
		String strBasePrice = getRandomString(1);
		sendKeysBy(txtBasePrmMinNetPrice, strBasePrice);
		clickElementBy(saveButtonModal);
		validateMessage("SalesChannelPage", "editOffers", sucessMSG, "Message displayed as :");
		/*
		 * Assert.assertTrue(verifyObjectDisplayed("SalesChannelPage",
		 * "editOffers", sucessMSG));
		 * tcConfig.updateTestReporter("SalesChannelPage", "editOffers",
		 * Status.PASS, "Message displayed as :- " +getElementText(sucessMsg));
		 */
		clickElementJSWithWait(hypOSC);
		verifyPageDisplayed(txtOSCPage, "Page Title", "SalesChannelPage", "editOffers");
		validateMessage("SalesChannelPage", "editOffers", txtBPMnetPrice,
				"Base Premium minimum Net Price after edit displayed as :");
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(txtBPMnetPrice));
		 * tcConfig.updateTestReporter("SalesChannelPage", "editOffers",
		 * Status.PASS,
		 * "Base Premium minimum Net Price after edit displayed as :- "
		 * +getElementText(txtBPMnetPrice));
		 */

	}
	
	//Updated By Shubhendra 8th March 2021
	
	
	public void createNewSalesChannelTeamsNew() throws AWTException {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitLongWait");
		scrollDownByPixel(100);
		scrollDownForElementJSBy(lnkSalesChanneTeams);
		waitUntilElementVisibleBy(driver, lnkSalesChanneTeams, "ExplicitMedWait");
		clickElementJSWithWait(btnSlsTeamNew);
		waitUntilElementVisibleBy(driver, txtNewSalesChannelTeam, "ExplicitLowWait");
		verifyPageDisplayed(txtNewSalesChannelTeam, "Page Title", "SalesChannelPage", "createNewSalesChannelTeams");
		clickElementBy(txtTeams);
		scrollDownForElementJSBy(plusIcon);
		waitUntilElementVisibleBy(driver, plusIcon, 120);
		clickElementBy(plusIcon);
		createNewTeams();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(txtEffStartDt);
		selectSystemDate(lnkToday);
		clickElementBy(txtEndDT);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("SalesChannelPage",
		 * "createNewSalesChannelTeams", Status.PASS, "Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
		verifyPageDisplayed(sucessMsg, "Page Title", "SalesChannelPage", "createNewSalesChannelTeams");
	}

	
	
	

}