package tgs.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class PromoCodePage extends TGSBasePage{
	
	public static final Logger log = Logger.getLogger(PromoCodePage.class);

	public PromoCodePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}
	
	protected By txtPromoCodePage = By.xpath(
			"//span[contains(.,'Promo Codes') and @class='slds-var-p-right_x-small uiOutputText forceBreadCrumbItem']");
	protected By btnNew = By.xpath("//a[@class='forceActionLink']//div[@title='New']");
	protected By txtNewPromocode = By
			.xpath("//h2[contains(@class,'slds-modal__title slds-hyphenate slds-text-heading--medium')]");
	protected By txtPromoCode = By.xpath("//label[contains(.,'Promo Code')]/following::div[1]/input");
	
	protected By txtPromoCodeEffectiveStartDate = By.xpath("//label[text()='Effective Start Date']//following::input[1]");
	protected By lnkToday = By.xpath("//button[contains(text(),'Today')]");
	protected By btnSave = By.xpath("//button[@name='SaveEdit']");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");

	protected By tabDetails = By.xpath("(//a[@class='slds-tabs_default__link' and contains(.,'Details')])[1]");
	protected By tabRelated = By.xpath("(//a[@id='relatedListsTab__item' and contains(.,'Related')])[1]");

	protected By btnNewBesidePromoCodeAssigned = By.xpath("//button[normalize-space()='New']");
	protected By searchSalesChannel = By.xpath("//input[@placeholder='Search Sales Channels...' and @type='text']");
	protected By searchDesination = By.xpath("//input[@placeholder='Search Destinations...' and @type='text']");
	protected By searchOffers = By.xpath("//input[@placeholder='Search Offers...' and @type='text']");
	
	protected By suggestionFirstSalesChannel = By.xpath("(//*[@class='slds-truncate' and contains(.,'AutomationSalesChannel1')])[2]");
	protected By suggestionFirstDestinatio = By.xpath("(//*[@class='slds-truncate' and contains(.,'Branson, MO')])[2]");
	protected By suggestionFirstOffer = By.xpath("(//*[@class='slds-truncate' and contains(.,'Shubh_Offer1')])[2]");
	
	protected By suggestionFirstSalesChannel2 = By.xpath("(//*[@class='slds-truncate' and contains(.,'AutomationMultiplePrem')])[2]");
	protected By suggestionFirstDestinatio2 = By.xpath("(//*[@class='slds-truncate' and contains(.,'Destin, FL')])[2]");
	protected By suggestionFirstOffer2 = By.xpath("(//*[@class='slds-truncate' and contains(.,'Automation2UAT2')])[2]");
	
	public void clickNewOnPromoCode() {
		waitUntilElementVisibleBy(driver, txtPromoCodePage, "ExplicitMedWait");
		verifyPageDisplayed(txtPromoCodePage, "Page Title", "PromoCodePage", "clickNewOnPromoCode");
		clickElementJSWithWait(btnNew);
		verifyPageDisplayed(txtNewPromocode, "Page Title", "PromoCodePage", "clickNewOnPromoCode");
	}
	
	public void createNewPromoCode() {
		waitUntilElementVisibleBy(driver, txtNewPromocode, "ExplicitMedWait");
		String strTeamsNm = getRandomStringText(7);
		sendKeysBy(txtPromoCode, strTeamsNm);
		clickElementBy(txtPromoCodeEffectiveStartDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("PromoCodePage", "createNewPromoCode", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));
	}

	public void clickRelatedTabofPromocodeDetail() throws Exception {
		waitUntilElementVisibleBy(driver, tabRelated, "ExplicitMedWait");
		clickElementJSWithWait(tabRelated);
		waitUntilElementVisibleBy(driver, tabDetails, "ExplicitMedWait");
		verifyPageDisplayed(tabRelated, "Page Title", "PromoCodePage", "clickRelatedTabofPromocodeDetail");
	}
	
	public void AddMultipleDesinationAndOffer1() {
		
		waitUntilElementVisibleBy(driver, btnNewBesidePromoCodeAssigned, "ExplicitMedWait");
		clickElementJSWithWait(btnNewBesidePromoCodeAssigned);
		
		sendKeysBy(searchSalesChannel, "AutomationSalesChannel1");
		verifyPageDisplayed(suggestionFirstSalesChannel, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(suggestionFirstSalesChannel);
		
		sendKeysBy(searchDesination, "Branson, MO");
		verifyPageDisplayed(suggestionFirstDestinatio, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(suggestionFirstDestinatio);
		
		sendKeysBy(searchOffers, "Shubh_Offer1");
		verifyPageDisplayed(suggestionFirstOffer, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(suggestionFirstOffer);

		
		clickElementBy(txtPromoCodeEffectiveStartDate);
		clickElementBy(txtPromoCodeEffectiveStartDate);
		verifyPageDisplayed(lnkToday, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		//selectSystemDate(lnkToday);
		clickElementBy(lnkToday);
		verifyPageDisplayed(btnSave, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(btnSave);
		
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("PromoCodePage", "createNewPromoCode", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));
		
		
		
		
	}
	
	public void AddMultipleDesinationAndOffer2() {
		
		waitUntilElementVisibleBy(driver, btnNewBesidePromoCodeAssigned, "ExplicitMedWait");
		clickElementJSWithWait(btnNewBesidePromoCodeAssigned);
		
		sendKeysBy(searchSalesChannel, "AutomationMultiplePrem");
		verifyPageDisplayed(suggestionFirstSalesChannel2, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(suggestionFirstSalesChannel2);
		
		sendKeysBy(searchDesination, "Destin, FL");
		verifyPageDisplayed(suggestionFirstDestinatio2, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(suggestionFirstDestinatio2);
		
		sendKeysBy(searchOffers, "Automation2UAT2");
		verifyPageDisplayed(suggestionFirstOffer2, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(suggestionFirstOffer2);

		
		clickElementBy(txtPromoCodeEffectiveStartDate);
		clickElementBy(txtPromoCodeEffectiveStartDate);
		verifyPageDisplayed(lnkToday, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		//selectSystemDate(lnkToday);
		clickElementBy(lnkToday);
		verifyPageDisplayed(btnSave, "Page Title", "PromoCodePage", "AddMultipleDesinationAndOffer");
		clickElementBy(btnSave);
		
		Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("PromoCodePage", "createNewPromoCode", Status.PASS,
				"Message displayed as :- " + getElementText(sucessMsg));
		
		
		
		
	}



}
