package tgs.pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OfferFeesPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(OfferFeesPage.class);

	public OfferFeesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}
	
	
	protected By btnNew = By.xpath("//div[@title='New']");
	protected By txtNwOfferFees = By.xpath("//h2[contains(.,'New Offer Fees')]");
	protected By txtOffFeesNm = By.xpath("//label[contains(.,'Offer Fees Name')]/following::input[@name='Name']");
	protected By drpFeeType = By.xpath("(//label[contains(.,'Fee Type')]/following::div[@class='slds-form-element__control']//input[@class='slds-input slds-combobox__input'])[1]");
	protected By drpValue = By.xpath("//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds')]//lightning-base-combobox-item//span[@title='" + testData.get("FeeType").trim() + "']");
	protected By drpFeeUnitType = By.xpath("(//label[contains(.,'Fee Unit Type')]/following::div[@class='slds-form-element__control']//input[@class='slds-input slds-combobox__input'])[1]");
	protected By drpFeeUnitValue = By.xpath("//div[contains(@class,'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid slds-dropdown_left slds')]//lightning-base-combobox-item//span[@title='" + testData.get("FeeUnitType").trim() + "']");
	protected By txtFee = By.xpath("//label[contains(.,'Fee')]/following::input[@name='Fee__c']");
	protected By txtEffDate = By.xpath("//label[contains(.,'Effective Start Date')]/following::div[contains(@class,'slds-form-element__control slds-input')]//input[@name='Effective_Start_Date__c']");
	protected By txtEndDate = By.xpath("//label[contains(.,'Effective End Date')]/following::div[contains(@class,'slds-form-element__control slds-input')]//input[@name='Effective_End_Date__c']");
	protected By btnSave = By.xpath("(//button[@name='SaveEdit'])[1]");
	protected By errorMsg = By.xpath("//h2[@class='slds-truncate slds-text-heading_medium']");
	protected By lnkToday = By.xpath("//button[contains(text(),'Today')]");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtOfferFees = By.xpath("//span[@class='slds-var-p-right_x-small uiOutputText forceBreadCrumbItem']");
	protected By txtOfferFeePage = By.xpath("(//div[@class='slds-media__body']//lightning-formatted-text)[1]");
	
	
	/*
	 * Description: click New button on Offer fees screen
	 * Date: July/2020
	 * Author: Jyoti 
	 * Changes By: NA
	 */
	public void clickNewbtn(){
		waitUntilElementVisibleBy(driver, txtOfferFees, "ExplicitMedWait");	
		verifyPageDisplayed(txtOfferFees, "Page Title", "OfferFeesPage",
				"clickNewbtn");
		clickElementJSWithWait(btnNew);	
		verifyPageDisplayed(txtNwOfferFees, "Page Title", "OfferFeesPage",
				"clickNewbtn");
	}
	
	
	/*
	 * Description: Create an Offer Fees record
	 * Date: July/2020
	 * Author: Jyoti 
	 * Changes By: NA
	 */	
	public void createOfferFees(){
		waitUntilElementVisibleBy(driver, txtNwOfferFees, "ExplicitMedWait");			
		String strOffFeesNm = getRandomStringText(4);
		String strData = "AutomationDataOfferFee_" + strOffFeesNm;
		sendKeysBy(txtOffFeesNm, strData);	    
	    clickElementBy(drpFeeType);	  
	    clickElementBy(drpValue);	    	    
	    clickElementJSWithWait(drpFeeUnitType);	    
	    clickElementBy(drpFeeUnitValue);	
	    waitUntilElementVisibleBy(driver, txtFee, "ExplicitMedWait");
	    String strFee = getRandomString(2);		
		sendKeysBy(txtFee, strFee);			
		clickElementJSWithWait(txtEffDate);	
		selectSystemDate(lnkToday);
		clickElementJSWithWait(txtEndDate);	
		selectSystemDate(lnkToday);
		clickElementBy(btnSave);
				      
	}
	
	
	
	/*
	 * Description: Validate Sucess Messgae
	 * Date: July/2020
	 * Author: Jyoti 
	 * Changes By: NA
	 */
	public void validateSucessMsg(){		
		/*Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		tcConfig.updateTestReporter("OfferFeesPage", "validateSucessMsg", Status.PASS,
				"Message displayed as :- " +getElementText(sucessMsg));*/			
		verifyPageDisplayed(sucessMsg, "Page Title", "OfferFeesPage",
				"validateSucessMsg");
		
	}
	
	/*
	 * Method-Validate Error Message on Offer Fees
	 * Designed By-JYoti
	 * */
	public void validateErrorMsg(){
	 //Assert.assertTrue(verifyObjectDisplayed(errorMsg));
	    /*String strmsg =driver.findElement(errorMsg).getText();
		tcConfig.updateTestReporter("OfferFeesPage", "validateErrorMsg", Status.PASS,
				"Message displayed as :- "+strmsg);*/
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(errorMsg)){
			tcConfig.updateTestReporter("OfferFeesPage", "validateErrorMsg", Status.PASS,
					"Error message is dispalyed sucessfully ");
		}else{
			tcConfig.updateTestReporter("OfferFeesPage", "validateErrorMsg", Status.FAIL,
					"Error message is dispalyed NOT ");
		}
	}

	
	
}