package tgs.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class OfferAgentsPage extends TGSBasePage {

	public static final Logger log = Logger.getLogger(OfferAgentsPage.class);

	public OfferAgentsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}

	public static String offerAgent;
	protected By txtOfferAgentsPage = By.xpath(
			"//span[contains(.,'Offers Agents') and @class='slds-var-p-right_x-small uiOutputText forceBreadCrumbItem']");
	protected By btnNew = By.xpath(
			"//div[contains(@class,'slds-col slds-no-flex slds-grid slds-align-top slds-p-bottom')]//ul//li//div[@title='New']");
	protected By txtNewOfferSAgent = By
			.xpath("//h2[contains(@class,'slds-modal__title slds-hyphenate slds-text-heading--medium')]");
	protected By txtOfferAgentName = By.xpath("//label[contains(.,'Offer Agents Name')]/following::div[1]/input");
	protected By txtFirstName = By.xpath("//label[contains(.,'First Name')]/following::div[1]/input");
	protected By txtLastName = By.xpath("//label[contains(.,'Last Name')]/following::div[1]/input");
	protected By txtAgentWWID = By.xpath("//label[contains(.,'Agent WWID')]/following::div[1]/input");
	protected By drpAgentType = By.xpath("//label[contains(.,'Agent Type')]/following::div[3]/input");
	protected By drpAgentValue = By.xpath("//lightning-base-combobox-item[@data-value='Offer Sales Agent']");
	protected By txtEffectiveStartDate = By.xpath("//input[@name='Effective_Start_Date__c']");
	protected By txtEffectiveEndDate = By.xpath("//input[@name='Effective_Start_Date__c']");
	protected By lnkToday = By.xpath("//button[contains(text(),'Today')]");
	protected By btnSave = By.xpath("//button[@title='Save']");
	protected By btnSave1 = By.xpath("//button[@name='SaveEdit']");
	protected By sucessMsg = By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']");
	protected By txtOfferAgentRecord = By.xpath("//div[@class='slds-media__body']//lightning-formatted-text");

	/*
	 * Description: Click on New button on Page Date: August/2020 Author: Jyoti
	 * Changes By: NA
	 */
	public void clickNew() {
		waitUntilElementVisibleBy(driver, txtOfferAgentsPage, "ExplicitMedWait");
		verifyPageDisplayed(txtOfferAgentsPage, "Page Title", "OfferAgentsPage", "clickNew");
		 waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(btnNew);
		verifyPageDisplayed(txtNewOfferSAgent, "Page Title", "OfferAgentsPage", "clickNew");
	}

	/*
	 * Description: Create a new Offer Agent record Date: August/2020 Author:
	 * Jyoti Changes By: NA
	 */
	public void createOfferAgent() {
		waitUntilElementVisibleBy(driver, txtNewOfferSAgent, "ExplicitMedWait");
		String strOffNm = getRandomStringText(4);
		sendKeysBy(txtOfferAgentName, strOffNm);
		String strFirstNm = getRandomStringText(3);
		sendKeysBy(txtFirstName, strFirstNm);
		String strLastNm = getRandomStringText(3);
		sendKeysBy(txtLastName, strLastNm);
		String strAgentWWID = getRandomString(4);
		sendKeysBy(txtAgentWWID, strAgentWWID);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJSWithWait(drpAgentType);
	
		clickElementJSWithWait(drpAgentValue);
		 waitForSometime(tcConfig.getConfig().get("MedWait"));
		 
		clickElementBy(txtEffectiveStartDate);
		selectSystemDate(lnkToday);
		clickElementBy(txtEffectiveEndDate);
		selectSystemDate(lnkToday);
		clickElementBy(btnSave1);
		validateMessage("OfferAgentsPage", "createOfferAgent", sucessMsg, "Message displayed as :");
		/*
		 * Assert.assertTrue(verifyObjectDisplayed(sucessMsg));
		 * tcConfig.updateTestReporter("OfferAgentsPage", "createOfferAgent",
		 * Status.PASS, "Message displayed as :- "
		 * +getElementText(sucessMsg));
		 */
	}
}