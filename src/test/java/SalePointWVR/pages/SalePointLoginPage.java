package SalePointWVR.pages;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class SalePointLoginPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointLoginPage.class);

	public SalePointLoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='logon']")
	protected WebElement buttonLogInSalepoint;
	@FindBy(xpath = "//table[@class='client_table']/tbody/tr[2]/td")
	protected WebElement buildVersion;
	@FindBy(xpath = "//div[@id='error' and contains(.,'Password does not match')]")
	protected WebElement loginError;
	@FindBy(xpath = "//select[@id='companyList']")
	protected WebElement companySelect;
	@FindBy(name = "j_username")
	protected WebElement textUsername;
	String strusernameBk = "";

	/*
	 * Method: setUserName Description:Set username Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */
	public void setUserName(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsername, "ExplicitLowWait");
		textUsername.sendKeys(strUserName);
		strusernameBk = strUserName;
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Username is entered for SalePoint Login");
	}

	@FindBy(name = "j_password")
	protected WebElement textPassword;

	/*
	 * Method: setPassword Description:Set password Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */

	public void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		textPassword.sendKeys(dString);

		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Entered password for SalePoint login");
	}

	@FindBy(xpath = "//input[@name='logon']")
	protected WebElement buttonLogIn;
	@FindBy(xpath = "//img[@src='/webapp/ccis/images/log_off.gif']")
	protected WebElement LogoutBtn;

	/*
	 * Method: launchApplication Description:Launch Application Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void launchApplication() {
		String url = testData.get("URL");
		driver.manage().deleteAllCookies();
		navigateToAppURL(url);

		try {
			driver.manage().window().maximize();
		} catch (Exception e) {
			log.info("Browser is Maximized");
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Application is Launched Successfully , URL : " + url);

	}

	/*
	 * Method: loginToApp Description:Login to App Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */

	public void loginToApp() throws Exception {
		waitUntilElementVisible(driver, textUsername, "ExplicitLowWait");
		setUserName(testData.get("UserID"));
		setPassword(testData.get("Password"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", buttonLogIn);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log in button clicked");

	}

	/*
	 * Method: Logout Description:Log Out Date: 2020 Author:Abhijeet Roy Changes
	 * By: NA
	 */

	public void Logout() throws Exception {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", LogoutBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(buttonLogIn)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Log Out Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Log Out Failed" + e.getMessage());
		}
		// driver.close();
		// driver.quit();
	}

	/*
	 * Method: loginToSP Description:Login to Salepoint Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void loginToSP(String strBrowser) throws Exception {


		driver.manage().window().setSize(new Dimension(1024, 768));
		driver.navigate().to((testData.get("SalepointURL")));
		driver.manage().window().maximize();
		waitUntilElementVisible(driver, textUsername, "ExplicitLongWait");
		if (verifyElementDisplayed(textUsername)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Salepoint launched-Build version:" + buildVersion.getText().substring(53));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Salepoint buildversion text not displayed");
		}
		waitUntilElementVisible(driver, textUsername, "ExplicitLongWait");
		setUserNameSP(testData.get("UserID"));
		setPasswordSP(testData.get("Password"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", buttonLogInSalepoint);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			waitUntilElementVisible(driver, loginError, "ExplicitMedWait");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "1st Login attempt is failed");

			if (loginError.isDisplayed()) {

				driver.navigate().to(testData.get("URL_SALE"));
				driver.manage().window().maximize();
				waitUntilElementVisible(driver, textUsername, "ExplicitLongWait");

				setUserNameSP(testData.get("SP_UserID"));
				setPasswordSP(testData.get("SP_Password"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				JavascriptExecutor executor_repeat = (JavascriptExecutor) driver;
				executor_repeat.executeScript("arguments[0].click();", buttonLogIn);

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "2nd Login attempt is made");

			}
		} catch (Exception e) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "1st Login attemp is successful");

		}

		waitUntilElementVisible(driver, companySelect, "ExplicitLowWait");

		if ((companySelect).isDisplayed()) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log is successful");

		}
	}

	/*
	 * Method: setUserNameSP Description:Set Username Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */
	public void setUserNameSP(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsername, "ExplicitLongWait");
		/*
		 * driver.manage().deleteAllCookies(); driver.navigate().refresh();
		 */
		waitUntilElementVisible(driver, textUsername, "ExplicitLongWait");
		textUsername.sendKeys(strUserName);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Username is entered for SalePoint Login");
	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	/*
	 * Method: setPasswordSP Description:Set Password Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */

	public void setPasswordSP(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		textPassword.sendKeys(dString);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Entered password for SalePoint login");
	}

	@FindBy(xpath = "//img[@src='/webapp/ccis/images/log_off.gif']")
	protected WebElement LogoutBtnSP;

	/*
	 * Method: LogoutSP Description:Log Out Date: 2020 Author:Abhijeet Roy
	 * Changes By: NA
	 */

	public void LogoutSP() throws Exception {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", LogoutBtnSP);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(buttonLogIn)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log Out Successful");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Log Out Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Log Out Failed" + e.getMessage());
		}
		// driver.close();
		// driver.quit();
	}

}
