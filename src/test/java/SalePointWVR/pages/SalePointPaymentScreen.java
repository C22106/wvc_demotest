package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;
import SalePointWVR.pages.SalePointOwnerEditPage;

public class SalePointPaymentScreen extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointHomePage.class);
	SalePointOwnerEditPage spOwnerEdit = new SalePointOwnerEditPage(tcConfig);
	public static String strTotalPaymentAmount;

	public SalePointPaymentScreen(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@class='button' and @id='loan']")
	protected WebElement PaymentEdit;

	@FindBy(xpath = "//input[contains(@name,'cc_payment_member_name')]")
	protected WebElement ccName;

	@FindBy(xpath = "//input[contains(@name,'cc_payment_account_number') or contains(@name,'cc_account_number')]")
	protected WebElement ccNumber;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_type') or contains(@name,'cc_type')]")
	protected WebElement ccType;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_month') or contains(@name,'cc_exp_month')]")
	protected WebElement ccMonth;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_year') or contains(@name,'cc_exp_year')]")
	protected WebElement ccYear;

	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	protected WebElement cashSelect;
	

	@FindBy(xpath = "//td[contains(.,'CC Auto')]/input[@type='radio']")
	protected WebElement ccSelect;

	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achSelect;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculate;

	@FindBy(xpath = "//input[@value='Save and Print' and contains(@onclick,'Print')]")
	protected WebElement savecontract;

	@FindBy(xpath = "//table[contains(@class,'client_table')]//td[contains(@id,'totalId')]")
	protected WebElement txtTotalAmount;
	
	@FindBy(xpath = "//input[contains(@name,'cc_payment_account_number')]")
	WebElement creditCardNumber;
	
	@FindBy(xpath = "//input[contains(@name,'cc_account_number')]")
	WebElement creditCardDiscAccNumber;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_type')]")
	WebElement creditCardcType;
	
	@FindBy(xpath = "//select[contains(@name,'cc_type')]")
	WebElement creditCardDiscType;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_month')]")
	WebElement creditCardcMonth;
	
	@FindBy(xpath = "//select[contains(@name,'cc_exp_month')]")
	WebElement creditCardDiscMonth;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_year')]")
	WebElement creditCardcYear;
	
	@FindBy(xpath = "//select[contains(@name,'cc_exp_year')]")
	WebElement creditCardDiscYear;
	
	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achAutoPaySelect;
	@FindBy(xpath = "//input[@name='bank_name']")
	protected WebElement bank_name;
	@FindBy(xpath = "//input[@name='bank_city']")
	protected WebElement bank_city;
	@FindBy(xpath = "//input[@name='bank_postal_code']")
	protected WebElement bank_postal_code;
	@FindBy(xpath = "//input[@name='bank_account_number']")
	protected WebElement bank_account_number;
	@FindBy(xpath = "//input[@name='account_holder_name']")
	protected WebElement account_holder_name;
	@FindBy(xpath = "//input[@name='bank_routing_number']")
	protected WebElement bank_routing_number;
	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;

	/*
	 * Method: sp_WVR_edit_Payment Description:Edit payment Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WVR_edit_Payment() throws Exception {

		if (verifyObjectDisplayed(PaymentEdit)) {

			PaymentEdit.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (ExpectedConditions.alertIsPresent() != null) {
				try {
					driver.switchTo().alert().accept();
				} catch (Exception e) {
					log.info("no alert is displayed");
				}
			}
		}
		if (testData.get("PaymentBy").equalsIgnoreCase("CCAutoPay")) {
			if (verifyObjectDisplayed(ccSelect)) {
				fieldDataEnter(ccName, testData.get("CCHolder"));
				if(testData.get("SalesType").equalsIgnoreCase("discovery")){
					selectByText((creditCardDiscType), testData.get("CCType"));
					fieldDataEnter(creditCardDiscAccNumber, testData.get("CCNumber"));
					selectByText(creditCardDiscMonth, testData.get("CCMonth"));
					selectByText(creditCardDiscYear, testData.get("CCYear"));
				}else{
					selectByValue((creditCardcType), testData.get("CCType").toLowerCase());
					fieldDataEnter(creditCardNumber, testData.get("CCNumber"));
					selectByText(creditCardcMonth, testData.get("CCMonth"));
					selectByText(creditCardcYear, testData.get("CCYear"));
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "CC Select not present");
			}

		} else if (testData.get("PaymentBy").equalsIgnoreCase("Cash")) {
			if (verifyObjectDisplayed(cashSelect)) {
				clickElementJS(cashSelect);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJS(cashSelect);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJS(cashSelect);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cash Select not present");
			}

		} else if (testData.get("PaymentBy").contains("ACH Auto Pay")) {
			if (verifyObjectDisplayed(achAutoPaySelect)) {
				clickElementJS(achAutoPaySelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(achAutoPaySelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				bank_name.sendKeys(testData.get("BankName"));
				bank_city.sendKeys(testData.get("BankCity"));
				bank_postal_code.sendKeys(testData.get("BankPostalCode"));
				bank_account_number.sendKeys(testData.get("BankAccNumber"));
				account_holder_name.sendKeys(testData.get("AccountHolderName"));
				bank_routing_number.sendKeys(testData.get("BankRoutingNumber"));
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "ACH Auto Pay Selected");

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"CC Auto Pay radio button not present");
			}
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextClick);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try{
			driver.switchTo().alert().accept();
		}catch(Exception e){
			log.info("Alert is not present");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");
			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try{
				driver.switchTo().alert().accept();
			}catch(Exception e){
				log.info("Alert is not present");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Money Screen not present");
		}
	}
	
}



