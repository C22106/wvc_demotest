package SalePointWVR.pages;



import java.awt.AWTException;
import java.awt.Robot;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;
import salepointWBW.pages.SalePointBasePage;

public class SalePointAdminPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointAdminPage.class);

	public SalePointAdminPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static int Tomorrow;
	private By viewContractTitle = By.xpath("//tr[@class='page_title']//div[contains(.,'View Contract')]");
	private By contractNoClick = By.xpath("//input[@name='contract_number']");
	private By searchButton = By.xpath("//input[@name='search']");
	private By contractSummaryPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Contract Summary')]");

	public void viewContractSearch(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		if(verifyObjectDisplayed(viewContractTitle)){
			String strContract=testData.get("strContractNo"); 
			
			tcConfig.updateTestReporter("SalePointViewContract", "viewContractSearch", Status.PASS, "ViewContract Page Displayed");
			
				driver.findElement(contractNoClick).sendKeys(strContract);
				
				tcConfig.updateTestReporter("SalePointViewContract", "viewContractSearch", Status.PASS, "Contract No Entered as: "+strContract);
			
				driver.findElement(searchButton).click();
				
				
				waitForSometime(tcConfig.getConfig().get("LongWait"));
			
				if(verifyObjectDisplayed(contractSummaryPage)){
					tcConfig.updateTestReporter("SalePointViewContract", "viewContractSearch", Status.PASS, "Contract Summary Page Displayed");
				}else{
					tcConfig.updateTestReporter("SalePointViewContract", "viewContractSearch", Status.FAIL, "Contract Summary Page not displayed");
				}
			
		}else{
			
			tcConfig.updateTestReporter("SalePointViewContract", "viewContractSearch", Status.FAIL, "Error in View Contract Page");
		}
	}
		
		public By admintab=By.xpath("//div//span//span[text()='Admin']");
		public By Profiletab=By.xpath("//div//span//span[text()='My Profile']");
		public By Cataegory=By.xpath("//select[@name='fvl_searchCategory']");
		public By searchvalue=By.xpath("//input[@name='fvl_searchValue']");
		public By searchbtn=By.xpath("//input[@value='Search']");
		public void go_to_Admin_SSAssignment(){
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisible(driver, admintab, "ExplicitLowWait");
			driver.navigate().to(testData.get("MenuURL"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(Cataegory)){
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Catagory Field is visible");
				Select dropDown = new Select(driver.findElement(Cataegory));
				List<WebElement> e = dropDown.getOptions();
				int itemCount = e.size();

				for(int l = 0; l < itemCount; l++)
				{
				    System.out.println(e.get(l).getText());
				    
				    if(e.get(l).getText().contains("Salesman Number") ||e.get(l).getText().contains("All salesmen per site")||e.get(l).getText().contains("Name") ){
				    	
				    	tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Option "+e.get(l).getText()+" is present");
				    }
				    else{tcConfig.updateTestReporter("SalePointViewContract", "viewCatagory", Status.FAIL, "Error in View catagory");}
				    
				}
			}else{
				
				tcConfig.updateTestReporter("SalePointViewContract", "viewCatagory", Status.FAIL, "Error in View catagory");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchvalue)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search value Field is visible");
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search value Field is not visible");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchbtn)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search button is visible");
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search button is not visible");
			}

		
	}
		

		public By dataTable =By.xpath("//table[@id='salesrepTable']");
		public By statusCol= By.xpath("//tr[@class='row']//td[3]");
		
		
		public void All_Salesman_Validation(){
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisible(driver, admintab, "ExplicitLowWait");
			driver.navigate().to(testData.get("MenuURL"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(Cataegory)){
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Catagory Field is visible");
				Select dropDown = new Select(driver.findElement(Cataegory));
				dropDown.selectByVisibleText("All salesmen per site");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.FAIL, "Catagory Field is not visible");
			}
			waitUntilElementVisible(driver, searchvalue, "ExplicitLowWait");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchvalue)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search value Field is visible");
				driver.findElement(searchvalue).sendKeys(testData.get("SiteNumber"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search value Field is not visible");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchbtn)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search button is visible");
				driver.findElement(searchbtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search button is not visible");
			}
			
			waitUntilElementVisible(driver, dataTable, "ExplicitLowWait");
			if(verifyObjectDisplayed(dataTable)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Searched data is visible");
			
				List<WebElement> status=driver.findElements(statusCol);
				for(int i=0;i<=status.size()-1;i++){
					
					String stat= status.get(i).getText();
					if(stat.contains("A")){
						tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Status is A for Salesman "+i);
					}
					else{
						tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Status is not A");
					}
				}
	
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Searched data is not visible");
			}
			
			
			
		
	}
		public By salessite= By.xpath("//tr[@class='row']//td[3]");
		
		public void Search_Salesman_Name_Validation(){
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisible(driver, admintab, "ExplicitLowWait");
			driver.navigate().to(testData.get("MenuURL"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(Cataegory)){
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Catagory Field is visible");
				Select dropDown = new Select(driver.findElement(Cataegory));
				dropDown.selectByVisibleText("Name");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.FAIL, "Catagory Field is not visible");
			}
			waitUntilElementVisible(driver, searchvalue, "ExplicitLowWait");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchvalue)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search value Field is visible");
				driver.findElement(searchvalue).sendKeys(testData.get("strSalesmanName"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search value Field is not visible");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchbtn)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search button is visible");
				driver.findElement(searchbtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search button is not visible");
			}
			
			waitUntilElementVisible(driver, dataTable, "ExplicitLowWait");
			if(verifyObjectDisplayed(dataTable)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Searched data is visible");
			
				List<WebElement> status=driver.findElements(statusCol);
				for(int i=0;i<=2;i++){
					
					String stat= status.get(i).getText();
					if(stat.contains("A")){
//						String sitenumber=driver.findElement(By.xpath("(//tr[@class='row']//td[2])["+i+"]")).getText();
						tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Status is A for all Salesite ");
					}
					else{
						tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Status is not A");
					}
				}
	
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Searched data is not visible");
			}
			
			
			
		
	}

		public By firstname=By.xpath("//tr[5]//td[1]");
		public By lastname=By.xpath("//tr[5]//td[3]");
		public By Avaialblesite=By.xpath("//td[text()='Available Sites']");
		public By Assignedsite=By.xpath("//td[text()='Assigned Sites']");
		public By SalesmanTable =By.xpath("//table[@class='client_table']");
		public void Search_Salesman_id_Validation() throws AWTException{
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitUntilElementVisible(driver, admintab, "ExplicitLowWait");
			driver.navigate().to(testData.get("MenuURL"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(Cataegory)){
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Catagory Field is visible");
				Select dropDown = new Select(driver.findElement(Cataegory));
				dropDown.selectByVisibleText("Salesman Number");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.FAIL, "Catagory Field is not visible");
			}
			waitUntilElementVisible(driver, searchvalue, "ExplicitLowWait");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchvalue)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search value Field is visible");
				driver.findElement(searchvalue).sendKeys(testData.get("strSalesmanID"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search value Field is not visible");
			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyObjectDisplayed(searchbtn)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Search button is visible");
				driver.findElement(searchbtn).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Search button is not visible");
			}
			
			waitUntilElementVisible(driver, SalesmanTable, "ExplicitLowWait");
			if(verifyObjectDisplayed(SalesmanTable)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Searched data is visible");
			
				
	
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Searched data is not visible");
			}
			
			
			waitUntilElementVisible(driver, firstname, "ExplicitLowWait");
			if(verifyObjectDisplayed(firstname)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Searched data is "+driver.findElement(firstname).getText());
			
				
	
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Searched data is not visible");
			}
			
			
			waitUntilElementVisible(driver, lastname, "ExplicitLowWait");
			if(verifyObjectDisplayed(lastname)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Searched data is "+driver.findElement(lastname).getText());
			
				
	
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Searched data is not visible");
			}
			
			
			waitUntilElementVisible(driver, Avaialblesite, "ExplicitLowWait");
			if(verifyObjectDisplayed(Avaialblesite)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Searched Avaialble site is vvisible");
			
				
	
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Searched Avaialble site is not visible");
			}
			
			
			waitUntilElementVisible(driver, Assignedsite, "ExplicitLowWait");
			if(verifyObjectDisplayed(Assignedsite)){
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Searched Assigned site is visible");
			
				
	
			}
			else{
				tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Searched Assigned site is not visible");
			}
			
		}
			
			
		
		public By select_assignsite=By.xpath("//select[@name='availableSites']");
		public By assignbutton=By.xpath("//input[contains(@value,'Assign Site')]");
		public By calander_img=By.xpath("//input[@name='effectiveDate']//following-sibling::img");
		public By set_date=By.xpath("//tr//td//a[text()='13']");
		public By savebtn=By.xpath("//input[@name='save']");
		public By Successmsg=By.xpath("//td//div[@class='success_elem']");
			public void Select_Mutliple_Site_Assign(){
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisible(driver, Avaialblesite, "ExplicitLowWait");
//				driver.navigate().to(testData.get("MenuURL"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if(verifyObjectDisplayed(Avaialblesite)){
					tcConfig.updateTestReporter("SalePointAdmin", "Avaialblesite", Status.PASS, "Avaialblesite Field is visible");
					String index_site=testData.get("IndextoChooseSite");
					String[] site_index=index_site.split(",");
					Select combobox= new Select(driver.findElement(select_assignsite));
					combobox.selectByIndex(Integer.parseInt(site_index[0]));
					combobox.selectByIndex(Integer.parseInt(site_index[1]));
					combobox.selectByIndex(Integer.parseInt(site_index[2]));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(assignbutton).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					/*driver.findElement(calander_img).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					DateFormat sdf = new SimpleDateFormat("dd");
					Calendar cal = Calendar.getInstance();
					String currentdate=sdf.format(cal.getTime());
					int Today=Integer.parseInt(currentdate);
					Tomorrow=Today+1;
					if(Tomorrow==32 || Tomorrow==31){
						Tomorrow=1;
					}
			        waitForSometime(tcConfig.getConfig().get("MedWait"));
					driver.findElement(By.xpath("//tr//td//a[text()='"+Tomorrow+"']")).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));*/
					driver.findElement(savebtn).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
					waitUntilElementVisible(driver, Successmsg, "ExplicitLowWait");
					if(verifyObjectDisplayed(Successmsg)){
						tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.PASS, "Success message is populating with message :"+driver.findElement(Successmsg).getText());
					}
					else{
						tcConfig.updateTestReporter("SalePointAdmin", "SearchValue", Status.FAIL, "Success message is not visible");
					}
					
				}else{
					
					tcConfig.updateTestReporter("SalePointViewContract", "viewCatagory", Status.FAIL, "Error in selecting Multiple Sites. ");
				}
			
	}

			public void  Acess_Validation_Positive() throws AWTException{
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisible(driver, admintab, "ExplicitLowWait");
				driver.navigate().to(testData.get("MenuURL"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if(verifyObjectDisplayed(Cataegory)){
					tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Access validation is successfull");
					Select dropDown = new Select(driver.findElement(Cataegory));
					dropDown.selectByVisibleText("Salesman Number");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
				else{
					tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.FAIL, "Access validation is not successfull");
				}
				
			}
			
			

			public void  Acess_Validation_Negative() throws AWTException{
				try{
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				

				waitUntilElementVisible(driver, Profiletab, "ExplicitLowWait");
//				
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if(!verifyObjectDisplayed(admintab)){
					
					tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Access validation is successfull");
					}
				else{
					
					driver.findElement(admintab).click();
					String source=driver.getPageSource();
					if(source.contains("name='Sales Site Assignment'")){
						System.out.println("Fail");
						tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.FAIL, "Access validation is not successfull");
						
					}else{
						System.out.println("Pass");
						tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Access validation is successfull");
					}
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					
				}
				
			}catch(Exception e){
				e.printStackTrace();
				tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Access validation is successfull");
			}
				
			}
			
			public void validate_selected_category() throws AWTException{
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisible(driver, admintab, "ExplicitLowWait");
				driver.navigate().to(testData.get("MenuURL"));
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if(verifyObjectDisplayed(Cataegory)){
					tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Catagory Field is visible");
					Select dropDown = new Select(driver.findElement(Cataegory));
					String value=dropDown.getFirstSelectedOption().getText();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if(value.contains("Salesman Number")){
						tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.PASS, "Default selected value is :"+value);	
					}else{
						tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.FAIL, "Default selected value is :"+value);
					}
					}
				else{
					tcConfig.updateTestReporter("SalePointAdmin", "viewCatagory", Status.FAIL, "Catagory Field is not visible");
				}
			}

			
			
			
			
			
}
