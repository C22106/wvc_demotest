package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class SalePointHomePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointHomePage.class);

	public SalePointHomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='ok']")
	protected WebElement okBtn;

	@FindBy(name = "tourID")
	protected WebElement tourID;

	@FindBy(name = "tourTime")
	protected WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement next1;

	@FindBy(id = "wbwcommissions_salesMan")
	protected WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteSec;

	@FindBy(id = "saveContract")
	protected WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	protected WebElement generate;

	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	protected WebElement conNo;

	@FindBy(xpath = "//SELECT[@id='companyList']")
	protected WebElement companySelect;

	/*
	 * Method: companySelect Description:Select Company Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void companySelect() throws Exception {

		waitUntilElementVisibleIE(driver, companySelect, "ExplicitMedWait");

		String strCompany = testData.get("strCompany");

		if (strCompany.equalsIgnoreCase("WBW")) {

			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Worldmark By Wyndham");

		} else if (strCompany.equalsIgnoreCase("WVR")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts");
		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.switchTo().activeElement().sendKeys(Keys.TAB);

		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Company Selected as: " + strCompany);

		clickElementJS(okBtn);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Actions act = new Actions(driver);

		if (strCompany.equalsIgnoreCase("WVRAP")) {
			System.out.println("No Alert Present");

		} else {

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				System.out.println("No Alert Present after Login");
			}
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "HomePage Navigation Successful");
	}

	public String Location_URL = "http://salepointtest2.corproot.com/webapp/ccis/SearchUserServlet?action=currentUser";
	@FindBy(xpath = "//table[@class='client_table']")
	protected WebElement User_table;

	/*
	 * Method: Update_DefaultLocation Description:Update Default Location Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void Update_DefaultLocation() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().to(Location_URL);
		waitUntilElementVisibleIE(driver, User_table, "ExplicitMedWait");
		if (verifyObjectDisplayed(User_table)) {

			Select sel = new Select(driver.findElement(By.xpath("//select[@name='location']")));
			sel.selectByValue(testData.get("Location_Code"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

	/*
	 * Method: companySelectSalePoint Description:Select Company Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void companySelectSalePoint() throws Exception {
		// waitUntilElementVisibleIE(driver, companySelect, 10);
		String strCompany = testData.get("strCompany");

		if (strCompany.equalsIgnoreCase("WBW")) {

			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Worldmark By Wyndham");

		} else if (strCompany.equalsIgnoreCase("WVR")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts");
		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Company Selected as: " + strCompany);

		clickElementJS(okBtn);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Actions act = new Actions(driver);

		if (strCompany.equalsIgnoreCase("WVRAP")) {
			System.out.println("No Alert Present");

		} else {

			try {
				new WebDriverWait(driver, 120).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				System.out.println("No Alert Present after Login");
			}
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "HomePage Navigation Successful");
	}
	
	
	public void navigateToPage(String strPagename){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().to(testData.get(strPagename));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigated to "+strPagename);		
	}

}
