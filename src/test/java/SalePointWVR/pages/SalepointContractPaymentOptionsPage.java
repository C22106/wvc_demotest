/*package com.wvo.UIScripts.SalePoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;

public class SalepointContractPaymentOptionsPage extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(SalepointContractPaymentOptionsPage.class);

	public SalepointContractPaymentOptionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	By cashSelect = By.xpath("//td[contains(.,'Cash Sale')]/input[@type='radio']");
	By cashSelectdisc = By.xpath("//td[contains(.,'Cash Sale')]");
	By ccSelect = By.xpath("//td[contains(.,'CC Auto')]/input[@type='radio']");

	By achSelect = By.xpath("//td[contains(.,'ACH Auto Pay')]/input[@type='radio']");

	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;

	By nextClick = By.xpath("//input[@value='Next']");
	
	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	WebElement cashRadioSelect;

	
	public By payMethod = By.xpath("//select[@name='down_payment_method0']");

	public void selectPayment() throws Exception {
		waitUntilElementVisibleIE(driver, nextClick, 120);

		if (verifyObjectDisplayed(cashSelect)) {
			
			clickElementJS(cashRadioSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(cashRadioSelect);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			new Select(driver.findElement(payMethod)).selectByVisibleText(testData.get("PaymentMethod"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			// final Amount = payAmount.getText();

		}

		else {
			tcConfig.updateTestReporter("SalepointContractPaymentOptionsPage", "selectPayment", Status.FAIL,
					"Cash Select not present");
		}

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

}
*/