package SalePointWVR.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;
import salepointWBW.pages.SalePointBasePage;

public class WBWPurchaseInformationPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWPurchaseInformationPage.class);

	public WBWPurchaseInformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Purchase Information')]")
	protected WebElement purchaseInformationHeader;

	@FindBy(name = "paymentType")
	protected List<WebElement> paymentSelect;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//select[@name='explID']")
	protected WebElement selectPackage;

	/*
	 * Method: purchaseInformationNavigation Description:Navigate to Purchase
	 * Info Page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void purchaseInformationNavigation() throws Exception {
		waitUntilElementVisible(driver, purchaseInformationHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(purchaseInformationHeader), "Purchase Information page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Navigaetd To Purchase Information Page");

	}

	/*
	 * Method: selectPaymentType Description:Select payment Type Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectPaymentType() {
		if (testData.get("PaymentType").toLowerCase().contains("financed")) {
			paymentSelect.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSelect.get(0).click();
		} else {
			paymentSelect.get(1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSelect.get(1).click();
		}

	}

	/*
	 * Method: selectPointsPackage Description:Select Points Package Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectPointsPackage() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectPackage, testData.get("strPoints"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextButton);

	}
}