package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class WSANewProposalPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WSANewProposalPage.class);
	public static String createdPitchID;
	public static String customerLastName;

	public WSANewProposalPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//a[text()='Commissions']")
	protected WebElement commisionsLink;

	@FindBy(xpath = "//input[@id='salesAssociate']")
	protected WebElement salesAssociatetxt;

	@FindBy(xpath = "//input[@id='sellingRep']")
	protected WebElement salesRepresentativetxt;

	@FindBy(xpath = "//button[@name='commission-done']")
	protected WebElement comissionDoneBtn;

	@FindBy(xpath = "//a[text()='Select Inventory']")
	protected WebElement linkSelectInv;

	@FindBy(xpath = "//input[@id='newPoints1']")
	protected WebElement newPointstxt;

	@FindBy(xpath = "//button[@name='inventory-done']")
	protected WebElement inventoryDoneBtn;

	@FindBy(xpath = "(//a[contains(text(),'Click to adjust')])[1]")
	protected WebElement adjustLink;

	@FindBy(xpath = "(//a[contains(text(),'Click to adjust')])[3]")
	protected WebElement adjustWBWLink;

	@FindBy(xpath = "//button[@id='band-submit1']")
	protected WebElement paymentDoneBtn;

	@FindBy(xpath = "(//button[@id='band-submit'])[6]")
	protected WebElement paymentSubmitBtn;

	@FindBy(xpath = "//div[@id='requiredDownPayment1']")
	protected WebElement txtRequiredAmt;

	@FindBy(xpath = "//div[@id='requiredToQualify1']")
	protected WebElement txtRequiredQlfyAmt;

	@FindBy(xpath = "//div[@id='requiredToQualify3']")
	protected WebElement txtRequiredQlfyAmtWBW;

	@FindBy(xpath = "//input[@id='paymentTypeForm_offer1_downPaymentList_0__dpAmount']")
	protected WebElement txtAmountLine1;

	@FindBy(xpath = "//input[@id='paymentTypeForm_offer1_downPaymentList_1__dpAmount']")
	protected WebElement txtAmountLine2;

	@FindBy(xpath = "//button[@name='final-offer']")
	protected WebElement finalBtn;

	@FindBy(xpath = "//button[@name='finalizeSummary']")
	protected WebElement finalBtnWBW;

	@FindBy(xpath = "//select[@name='titleOption']")
	protected WebElement titleDrpDwn;

	@FindBy(xpath = "//select[@id='paymentTypeForm_offer1_downPaymentList_1__paymentTypeName']")
	protected WebElement depositTypeDropDown;

	@FindBy(xpath = "(//input[@name='flag'])[1]")
	protected WebElement selectFlagRadioBtn;

	@FindBy(xpath = "(//input[contains(@name,'flagForWorksheet')])[3]")
	protected WebElement selectFlagRadioBtnWBW;

	@FindBy(xpath = "//h2[contains(text(),'Name:')]")
	protected WebElement CustNameTxt;

	@FindBy(xpath = "//div[@id='page-body-container']//h1")
	protected WebElement pitchNumberHeader;

	/*
	 * Method: createNewPitchWSA Description:Create New Pitch Date: 2020
	 * Author:Utsav Changes By: NA
	 */ 
	
	public void createNewPitchWSA() throws Exception {
		double amountone, amounttwo;

		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("UDI")
				|| testData.get("SubPitchType").equalsIgnoreCase("Fixed Week")) {
			log.info("Entered create New Pitch");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisible(driver, commisionsLink, "ExplicitLongWait");
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.click();
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.click();
			salesAssociatetxt.sendKeys(Keys.TAB);
			salesAssociatetxt.sendKeys(Keys.ENTER);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Sales Associate number entered");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			// mouseoverAndClick(comissionDoneBtn);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(linkSelectInv)) {
				linkSelectInv.click();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Select inventory link is displayed");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Select inventory link is NOT displayed");
			}

			newPointstxt.clear();
			newPointstxt.sendKeys(testData.get("Points"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Points entered");
			inventoryDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			mouseoverAndClick(adjustLink);
		//	clickElementJS(adjustLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Adjust link not displayed");
			}

			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(depositTypeDropDown).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Payment done button clicked");

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(titleDrpDwn)) {
				// new
				// Select(driver.findElement(titleDrpDwn)).selectByVisibleText("Single
				// Man");
				new Select(titleDrpDwn).selectByIndex(1);

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Title Displayed");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Title NOT displayed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectFlagRadioBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitUntilElementVisible(driver, pitchNumberHeader, "ExplicitLongWait");
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = pitchNumberHeader.getText();
				String newheaderdetails = pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails = newheaderdetails.replace("-1 Selected", "");
				createdPitchID = headerdetails.trim();
				String CustName = CustNameTxt.getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Pitch # " + headerdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"No pitch number is generated");
			}

		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			waitUntilElementVisible(driver, commisionsLink, "ExplicitLongWait");
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisible(driver, salesRepresentativetxt, "ExplicitLongWait");
			salesRepresentativetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Sales Representative number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			adjustWBWLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredQlfyAmtWBW.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);

			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Adjust link not displayed");
			}

			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(depositTypeDropDown).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSubmitBtn.click();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectFlagRadioBtnWBW.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtnWBW.click();
			waitUntilElementVisible(driver, pitchNumberHeader, "ExplicitLongWait");
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = pitchNumberHeader.getText();
				String newheaderdetails = pageheaderdetails.replace("Final Summary for Reference # ", "");
				createdPitchID = newheaderdetails.trim();
				String CustName = CustNameTxt.getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Pitch # " + newheaderdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"No pitch number is generated");
			}
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")
				&& testData.get("SubPitchType").equalsIgnoreCase("Discovery")) {
			waitUntilElementVisible(driver, commisionsLink, "ExplicitLongWait");
			commisionsLink.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			salesAssociatetxt.sendKeys(testData.get("SalesAssociate"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Sales Associate number entered");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			comissionDoneBtn.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			adjustLink.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String TotalAmt = txtRequiredAmt.getText().trim();
			double dbl = Double.parseDouble(TotalAmt);
			amountone = dbl % 100;
			amounttwo = (dbl - dbl % 100);
			String strAmntOne = String.format("%.2f", amountone);
			String strAmntTwo = String.format("%.2f", amounttwo);

			if (verifyElementDisplayed(txtAmountLine1)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Adjust Link clicked");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Adjust link not displayed");
			}
			txtAmountLine1.clear();
			txtAmountLine1.sendKeys(strAmntOne);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					strAmntOne + " Amount entered in amount line 1");
			new Select(depositTypeDropDown).selectByVisibleText("Cash");
			txtAmountLine2.sendKeys(strAmntTwo);
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					strAmntTwo + " Amount entered in amount line 2");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			paymentDoneBtn.click();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Payment done button clicked");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			selectFlagRadioBtn.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			finalBtn.click();
			waitUntilElementVisible(driver, pitchNumberHeader, "ExplicitLongWait");
			if (verifyObjectDisplayed(pitchNumberHeader)) {
				String pageheaderdetails = pitchNumberHeader.getText();
				String newheaderdetails = pageheaderdetails.replace("New Proposal: Reference #", "");
				String headerdetails = newheaderdetails.replace("-1 Selected", "");
				createdPitchID = headerdetails.trim();
				String CustName = CustNameTxt.getText().replaceAll("Name:", "");
				String[] arr = CustName.split(" ");
				customerLastName = arr[1].trim();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Pitch # " + headerdetails.trim() + " created");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"No pitch number is generated");
			}
		}
	}

}