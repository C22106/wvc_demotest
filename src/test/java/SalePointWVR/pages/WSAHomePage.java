package SalePointWVR.pages;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.DatatypeConverter;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class WSAHomePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WSAHomePage.class);

	public WSAHomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//img[@title='Change Sales Site']")
	protected WebElement changeSiteicon;

	@FindBy(xpath = "//button[@name='cs-submit']")
	protected WebElement confirmOKBtn;

	@FindBy(xpath = "//button[@id='submit-btn']")
	protected WebElement saveBtn;

	@FindBy(xpath = "//p[text()='Your site was updated successfully.']")
	protected WebElement txtSuccessMsg;

	@FindBy(xpath = "(//input[@name='selectedServiceEntity'])[1]")
	protected WebElement radioBtnWVRServiceEntity;

	@FindBy(xpath = "(//input[@name='selectedServiceEntity'])[2]")
	protected WebElement radioBtnWBWServiceEntity;

	@FindBy(xpath = "(//input[@name='selectedServiceEntity'])[3]")
	protected WebElement radioBtnWVRAPServiceEntity;

	@FindBy(xpath = "//select[@id='selectedSite']")
	protected WebElement siteDropDown;

	@FindBy(xpath = "(//img[@alt='Close this window'])[2]")
	protected WebElement closePopupIcon;

	/*
	 * Method: selectEntityWSA Description:Select Entity Date: 2020
	 * Author:Utsav Changes By: NA
	 */
	
	public void selectEntityWSA() throws Exception {
		if (verifyElementDisplayed(changeSiteicon)) {
			changeSiteicon.click();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Sales Site Icon Clicked");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Sales Site Icon Not Displayed");
		}

		if (verifyElementDisplayed(confirmOKBtn)) {
			confirmOKBtn.click();
		}
		waitUntilElementVisible(driver, radioBtnWVRServiceEntity, "ExplicitLongWait");
		if (testData.get("ServiceEntity").equalsIgnoreCase("WVR")) {
			radioBtnWVRServiceEntity.click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WBW")) {
			radioBtnWBWServiceEntity.click();
		} else if (testData.get("ServiceEntity").equalsIgnoreCase("WVRAP")) {
			radioBtnWVRAPServiceEntity.click();
		}
		new Select(siteDropDown).selectByVisibleText(testData.get("SiteName"));
		saveBtn.click();
		waitUntilElementVisible(driver, closePopupIcon, "ExplicitLongWait");
		if (verifyElementDisplayed(txtSuccessMsg)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Site changed to " + testData.get("ServiceEntity") + " success message is diplayed properly");
			closePopupIcon.click();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Site changed to " + testData.get("ServiceEntity") + " success message is Not Displayed");
		}
	}

	/*
	 * Method: pitchTypeSelectionWSA Description:Select Pitch Type Date: 2020
	 * Author:Utsav Changes By: NA
	 */

	public void pitchTypeSelectionWSA() throws Exception {

		WebElement pitchType = driver.findElement(By.xpath("//a[text()='" + testData.get("PitchType") + "']"));
		waitUntilElementVisible(driver, pitchType, "ExplicitLongWait");

		pitchType.click();
		// Actions action=new Actions(driver);
		// action.moveToElement(pitchType).click().perform();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		final WebElement subPitchType = driver.findElement(By.xpath(" //a[text()='" + testData.get("PitchType")
				+ "']/following-sibling::ul/descendant::a[contains(text(),'" + testData.get("SubPitchType") + "')]"));
		// subPitchType.click();
		if (verifyElementDisplayed(subPitchType)) {
			subPitchType.click();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					testData.get("SubPitchType") + " selected from " + testData.get("PitchType") + " menu");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					testData.get("SubPitchType") + " NOT selected from " + testData.get("PitchType") + " menu");
		}
	}

}
