package SalePointWVR.pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class WBWContractManagementPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWContractManagementPage.class);

	public WBWContractManagementPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Contract Management')]")
	protected WebElement contractMangementHeader;

	@FindBy(xpath = "//select[@name='beginDateDay']")
	protected WebElement selectBeginDateDay;

	@FindBy(xpath = "//select[@name='beginDateMonth']")
	protected WebElement selectBeginDateMonth;

	@FindBy(xpath = "//select[@name='beginDateYear']")
	protected WebElement selectBeginDateYear;

	@FindBy(name = "next")
	protected WebElement nextButton;

	/*
	 * Method: contractManagementPageNavigation Description:Contract management
	 * Page Nav Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void contractManagementPageNavigation() throws Exception {
		driver.navigate().to(testData.get("contractMgmt"));
		waitUntilElementVisible(driver, contractMangementHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(contractMangementHeader), "Contract Management page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Navigated To Contract Management Page");
	}

	/*
	 * Method: selectBeginDate Description:Select Begin date Page Nav Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectBeginDate() {
		DateTime dateTime = new DateTime();
		DateTime getYesterdaysDate = (dateTime.minusDays(1));
		Date convert = getYesterdaysDate.toDate();
		DateFormat formatDate = new SimpleDateFormat("dd-MMM-yyyy");
		String getDate = formatDate.format(convert);
		String getDay = getDate.split("-")[0];
		String getMonth = getDate.split("-")[1];
		String getYear = getDate.split("-")[2];
		if (getDay.startsWith("0")) {
			getDay = getDay.replace("0", "");
		}
		selectByText(selectBeginDateDay, getDay);
		selectByText(selectBeginDateMonth, getMonth);
		selectByText(selectBeginDateYear, getYear);

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}

	/*
	 * Method: selectContract Description:Select Contract Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectContract() {
		String strMemberNo = testData.get("strMemberNumber");
		waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), "ExplicitLongWait");
		if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Member No Verified and is: " + strMemberNo);
			WebElement selectMember = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
			clickElementJS(selectMember);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Member No Not Verified");

		}

	}
}
