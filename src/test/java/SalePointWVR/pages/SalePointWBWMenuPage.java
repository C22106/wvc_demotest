/*package com.wvo.UIScripts.SalePoint.pages;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;

public class SalePointWBWMenuPage extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(SalePointWBWMenuPage.class);

	public SalePointWBWMenuPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	String strContractNo = "";
	String strMemberNo = "";

	@FindBy(xpath = "//input[@name='ok']")
	WebElement okBtn;
	
	protected By printOption = By.xpath("//select[@id = 'justification']");
	
	@FindBy(id = "print")
	WebElement print;

	@FindBy(name = "tourID")
	WebElement tourID;

	@FindBy(name = "tourTime")
	WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	WebElement next1;	
	
	@FindBy(xpath = "//input[@name='numCredits']")
	WebElement credits;
	
	@FindBy(xpath = "//input[@name='procFeeCollected']")
	WebElement procFeeCollected;


	@FindBy(xpath = "//input[@name='next']")
	WebElement next2;

	// socialSecurityNumber
	@FindBy(id = "wbwcommissions_salesMan")
	WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	WebElement deleteSec;

	@FindBy(id = "saveContract")
	WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	WebElement generate;

	// FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]

	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	WebElement conNo;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	WebElement memberNo;

	@FindBy(xpath = "//span[contains(text(), 'Contracts')]")
	WebElement contracts;

	

	By tourIdEnter = By.name("tourID");

	By deleteOwner = By.xpath("//a[contains(.,'Delete')]");

	By paymentSelect = By.name("paymentType");

	By nextClick = By.xpath("//input[@name='next']");

	By salePersonId = By.id("wbwcommissions_salesMan");

	By saveContractClick = By.id("saveContract");

	By printClick = By.xpath("//input[@name='print']");

	By memberNoCheck = By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]");
	
	By ssnSelect =By.xpath("//input[@name='socialSecurityNumber']");
	
	By homePhoneSelect = By.xpath("//input[@name='homePhone']");
	
	By primaryOwnerPage= By.xpath("//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]");
	
	By ownerDetailsPage =By.xpath("//tr[@class='page_title']//div[contains(.,'Owners')]");
	
	 changes By Utsav
	By MonthFromDrpDwn=By.xpath("//select[@name='beginDateMonth']");
	By DateFromDrpDwn=By.xpath("//select[@name='beginDateDay']");
	By YearFromDrpDwn=By.xpath("//select[@name='beginDateYear']");
	 changes By Utsav
	@FindBy(xpath = "//input[@value='Experience'and @type='radio']")
	WebElement experienceClick;
	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;
	
	@FindBy(xpath = "//button[@value='Next']")
	WebElement nextBtn;

	@FindBy(xpath = "//input[@value='WorldMark']")
	WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	WebElement finalizeButton;

	By experienceSelect = By.xpath("//input[@value='Experience']");

	By summaryPage = By.xpath("//tr[@class='page_title']//div");

	By worldMarkSelect = By.xpath("//input[@value='WorldMark']");

	By nextValue = By.xpath("//input[@value='Next']");

	@FindBy(xpath = "//input[@name='memberNo']")
	WebElement Member_search;
	
	@FindBy(xpath = "//input[@value='Search']")
	WebElement search_btn;
	
	@FindBy(xpath="//input[@name='tsc']")
	WebElement TSC_number;
	
	@FindBy(xpath="//select[@id='duesPACId']")
	WebElement Dues_Payment_Method;
	
	@FindBy(xpath="//input[@id='duesCCPACInfo.cardMemberName']")      
	WebElement Account_holder_name;
	
	@FindBy(xpath="//input[@id='duesCCPACInfo.cardNumber']")            
	WebElement Card_number;
	
	@FindBy(xpath="//select[@id='duesCCPACInfo.expireMonth']")                 
	WebElement Card_expireMonth;
	
	@FindBy(xpath="//select[@id='duesCCPACInfo.expireYear']")                
	WebElement Card_expireYear;
	

	public void sp_Ex_ContractFlow() throws Exception {

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourIdEnter, 120);// to be deleted

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			//clickElementJS(next1);
			next1.click();
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, next1, 20);
		
		if(verifyElementDisplayed(driver.findElement(primaryOwnerPage))){
			
			driver.findElement(ssnSelect).click();
			driver.findElement(ssnSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
			
			driver.findElement(homePhoneSelect).click();
			driver.findElement(homePhoneSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(homePhoneSelect).sendKeys(testData.get("HomePhone"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			//clickElementJS(next1);
			next1.click();
			
		}else if(verifyElementDisplayed(driver.findElement(ownerDetailsPage))){
			log.info("OwnerDetails Page Directly Opened GTG");
		}else {
				log.info("Unhandled Error fow WBW Page");
		}
		
		
		
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, ownerDetailsPage, 30);

		if (verifyElementDisplayed(deleteSec)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> list = driver.findElements(deleteOwner);

			
			clickElementJSWithWait(list.get(0));
			try{
				clickElementJSWithWait(list.get(0));
			}catch(Exception e){
				
			}
			

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("No Secondary owner good to go");
		}

		clickElementJS(next1);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, paymentSelect, 20);
		List<WebElement> list1 = driver.findElements(paymentSelect);

		clickElementJS(list1.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(list1.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
		new Select(driver.findElement(By.name("explID"))).selectByVisibleText(testData.get("strPoints"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitUntilElementVisibleIE(driver, nextClick, 20);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJS(nextClick);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, salePersonId, 50);

		saleper.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		saleper.sendKeys(testData.get("strSalePer"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, saveContractClick, 50);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, 50);

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJSWithWait(generate);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(printOption)) {
				selectByIndex(printOption , 2);
				tcConfig.updateTestReporter("SalepointContractPrintPage", "contractClickPrintButton", Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(generate);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), 50);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}
	
	public void updateContractNumberToSheet(String testName)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		FunctionalComponents.writeDataInExcel(testName, "GeneratedContract", strContractNo);

	}
	
	public void updateMemberNumberToSheet(String testName)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		FunctionalComponents.writeDataInExcel(testName, "GeneratedMember", strMemberNo);

	}

	
	

	public void sp_Ex_contractSearch() throws Exception {
		
		String fromDate=addDateInSpecificFormat("MM/dd/yyyy",-2);
		String[] arrDate=fromDate.split("/");
		String strMonthFromDrpDwn=arrDate[0];
		String strDateFromDrpDwn=arrDate[1];
		String strYearFromDrpDwn=arrDate[2];
		
		if (strMonthFromDrpDwn.startsWith("0")){
			strMonthFromDrpDwn=strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")){
			strDateFromDrpDwn=strDateFromDrpDwn.replace("0", "");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, experienceClick, 20);

		if (verifyElementDisplayed(experienceClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
					"Contract Management Page Displayed");
			
			 changes By Utsav
			
			new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwn)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwn)).selectByValue(strYearFromDrpDwn);
			
			 changes By Utsav

			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(experienceClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), 20);
			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryPage, 50);

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(finalizeButton)) {

					clickElementJSWithWait(finalizeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					// driver.switchTo().activeElement().sendKeys(Keys.ENTER);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Finalize')]"), 20);
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));

					if (list2.get(0).getText().contains("Finalize")) {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.PASS,
								"Contract Finalized");
					} else {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
								"Contract did notr finalize");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}
		
		
		
		
		

	}

	
	public void menuNavigationandfetchTour() throws Exception {

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourIdEnter, 20);// to be deleted

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"WorldMark Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"WorldMark Page Navigation Error");

		}
		
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, next1, 20);
	}
	public void sp_WM_ContractFlow() throws Exception {

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, tourIdEnter, 120);// to be deleted

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"WorldMark Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			clickElementJS(next1);
		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"WorldMark Page Navigation Error");

		}
		
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, next1, 120);
		
		if(verifyElementDisplayed(driver.findElement(primaryOwnerPage))){
			driver.findElement(ssnSelect).click();
			driver.findElement(ssnSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(ssnSelect).sendKeys(testData.get("ssnNumber"));
		
			driver.findElement(homePhoneSelect).click();
			driver.findElement(homePhoneSelect).clear();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(homePhoneSelect).sendKeys(testData.get("HomePhone"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			
			clickElementJS(next1);
			
			
			
		}else if(verifyElementDisplayed(driver.findElement(ownerDetailsPage))){
			log.info("OwnerDetails Page Directly Opened GTG");
		}else {
				log.info("Unhandled Error fow WBW Page");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, ownerDetailsPage, 120);

		if (verifyElementDisplayed(deleteSec)) {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> list = driver.findElements(deleteOwner);

			clickElementJSWithWait(list.get(0));

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				//driver.navigate().refresh();

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("No Secondary owner good to go");
		}

		clickElementJS(next1);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, paymentSelect, 20);
		List<WebElement> list1 = driver.findElements(paymentSelect);

		clickElementJS(list1.get(1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(list1.get(1));
		
		credits.click();
		credits.clear();
		driver.switchTo().alert().accept();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		credits.sendKeys(testData.get("strPoints"));
		
		procFeeCollected.click();
		procFeeCollected.clear();
		//driver.switchTo().alert().accept();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		procFeeCollected.sendKeys(testData.get("ProcessingFee"));
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitUntilElementVisibleIE(driver, nextValue, 20);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		clickElementJS(nextValue);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, salePersonId, 120);

		saleper.click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		saleper.sendKeys(testData.get("strSalePer"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(next1);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, saveContractClick, 120);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Contract Save Page Navigation Error");

		}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, printClick, 120);
		

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(printOption)) {
				selectByIndex(printOption , 2);
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(print);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			
		}else{
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		
		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJSWithWait(generate);
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		//waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), 50);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractFlow", Status.FAIL,
					"Member number could not be retrieved");
		}

	}
	
	
public void sp_WM_contractSearch() throws Exception {
		
		String fromDate=addDateInSpecificFormat("MM/dd/yyyy",-2);
		String[] arrDate=fromDate.split("/");
		String strMonthFromDrpDwn=arrDate[0];
		String strDateFromDrpDwn=arrDate[1];
		String strYearFromDrpDwn=arrDate[2];
		
		if (strMonthFromDrpDwn.startsWith("0")){
			strMonthFromDrpDwn=strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")){
			strDateFromDrpDwn=strDateFromDrpDwn.replace("0", "");
		}
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, worldMarkSelect, 120);
		

		if (verifyElementDisplayed(worldmarkClick)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
					"Contract Management Page Displayed");
			 changes By Utsav
						
			new Select(driver.findElement(MonthFromDrpDwn)).selectByValue(strMonthFromDrpDwn);
			new Select(driver.findElement(DateFromDrpDwn)).selectByValue(strDateFromDrpDwn);
			new Select(driver.findElement(YearFromDrpDwn)).selectByValue(strYearFromDrpDwn);
			
			 changes By Utsav
			
			
			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(worldmarkClick);

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"), 120);
			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryPage, 120);

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(finalizeButton)) {

					clickElementJSWithWait(finalizeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					// driver.switchTo().activeElement().sendKeys(Keys.ENTER);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td[contains(.,'Finalize')]"), 120);
					List<WebElement> list2 = driver.findElements(By.xpath("//tr/td[contains(.,'Finalize')]"));

					if (list2.get(0).getText().contains("Finalize")) {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.PASS,
								"Contract Finalized");
					} else {
						tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
								"Contract did notr finalize");

					}

				}

			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WM_contractSearch", Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}
	public void WBW_discovery_upgrade_Contract_info() throws Exception{
		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, Member_search, 20);
		if (verifyElementDisplayed(Member_search)) {
			Member_search.click();
			Member_search.sendKeys(testData.get("WBW_Member"));
			search_btn.click();
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				Alert alert = driver.switchTo().alert();
				alert.accept();
			} 
			catch (Exception e) 
			{
				log.info("No Alert Prersent");
			}
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.PASS,
					"Member number Entered");
			
	
			waitUntilElementVisibleIE(driver, TSC_number, 20);
			if(verifyElementDisplayed(TSC_number)) {
				TSC_number.click();
				TSC_number.sendKeys(testData.get("strTourId"));
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.PASS,
						"TSC Entered");
			}
				else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.FAIL,
							"Error while entering TSC Number");

				}
					
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			
			if(verifyElementDisplayed(nextBtn)) {
				nextBtn.click();
			}
				try {
					new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
					Alert alert = driver.switchTo().alert();
					alert.accept();
				} 
				catch (Exception e) 
				{
					log.info("No Alert Prersent");
				}
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.FAIL,
					"Upgrade-Purchase Information  Navigation Failed");
		}
		
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, Dues_Payment_Method, 20);
			if (verifyElementDisplayed(Dues_Payment_Method)) {
				Dues_Payment_Method.click();

				Select Payment_Method = new Select(Dues_Payment_Method);
				Payment_Method.selectByIndex(2);
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.PASS,
						"Dues payment method click");
			}
			waitUntilElementVisibleIE(driver, Account_holder_name, 20);
			if (verifyElementDisplayed(Account_holder_name)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.PASS,
						"WorldMark - Payment Options Screen Navigation Successful");
				Account_holder_name.click();
				Account_holder_name.sendKeys(testData.get("Account_holder_name"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Card_number.click();
				Card_number.sendKeys(testData.get("Card_Number"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Card_expireMonth.click();
				Select expmnth = new Select(Card_expireMonth);
				expmnth.selectByIndex(3);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Card_expireYear.click();
				Select expyear = new Select(Card_expireYear);
				expyear.selectByIndex(3);
				
				clickElementJS(nextVal);
				
				
				try {
					new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
					Alert alert = driver.switchTo().alert();
					alert.accept();
				} 
				catch (Exception e) 
				{
					log.info("No Alert Prersent");
				}

			}
			
			else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.FAIL,
						"WorldMark - Payment Options Screen Navigation Failed");
			}
			
			
			waitUntilElementVisibleIE(driver, salePersonId, 50);

			saleper.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			saleper.sendKeys(testData.get("strSalePer"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, saveContractClick, 50);

			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
						"Contract Save Page Navigation Error");

			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, printClick, 50);

			if (verifyElementDisplayed(generate)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
						"Contract Review Page Displayed and Generate Button Clicked");
				clickElementJSWithWait(generate);
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
						"Generate contract Page Navigation Error");
			}

			waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), 50);

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText();

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				log.info("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
						"Member number could not be retrieved");
			}
	}
	
	@FindBy(xpath="//input[@id='member_number']")
	WebElement Owner_member1;

	@FindBy(xpath="//input[@name='trade']")
	WebElement Add_contract;
	
	@FindBy(xpath="//input[@id='member_number']")
	WebElement Owner_member2;
	
	@FindBy(xpath="//input[@name='process']")
	WebElement Process_Combine;
	
	@FindBy(xpath="//select[@name='loanPACId']")
	WebElement Payment_method_combine;
	
	@FindBy(xpath="//input[@name='ccPacNameOnCard']")
	WebElement Combine_card_holder_name;
	
	@FindBy(xpath="//input[@name='ccPacCardNum']")
	WebElement Combine_card_number;
	
	@FindBy(xpath="//select[@name='ccPacCardExpiresMonth']")
	WebElement Combine_card_Expire_month;
	
	@FindBy(xpath="//select[@name='ccPacCardExpiresYear']")
	WebElement Combine_card_Expire_year;
	
	@FindBy(xpath="//select[@name='pac']")
	WebElement Payment_method_combine_due;
	
	
	public void wbw_Combine_Contract_info() throws Exception{
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Owner_member1, 20);
		if (verifyElementDisplayed(Owner_member1)) {
		tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
				"Navigated to Contract Processing - Combine Contract page successfully");

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Owner_member1, 20);
		if (verifyElementDisplayed(Owner_member1)) {
			Owner_member1.click();
			Owner_member1.sendKeys(testData.get("Owner_Member_1"));
			
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"owner number 1 Entered");
			
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Error while entering owner number 1");
		}
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Add_contract, 20);
		if(verifyElementDisplayed(Add_contract)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Navigated to Contract Processing - Combine Information page Successfully ");
			Add_contract.click();

		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Error while clicking on Add contract button ");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, Owner_member2, 20);
		if (verifyElementDisplayed(Owner_member2)) {
			Owner_member2.click();
			Owner_member2.sendKeys(testData.get("Owner_Member_2"));
			
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"owner number 2 Entered");
			
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Error while entering owner number 2");
		}
		
		clickElementJS(nextVal);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, Process_Combine, 20);
		if (verifyElementDisplayed(Process_Combine)) {
			Process_Combine.click();	
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Process combine button clicked");
			
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Error while clicking on process combine button");
		}
		clickElementJS(nextVal);
		tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
				"Successfully navigated to Combine - Purchase Information page");
		clickElementJS(nextVal);
		if (verifyElementDisplayed(Payment_method_combine)) {
			Payment_method_combine.click();

			Select Payment_Method_combine = new Select(Payment_method_combine);
			Payment_Method_combine.selectByIndex(2);
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Payment method selected");
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"error while selecting Payment method");
		}
		if (verifyElementDisplayed(Combine_card_holder_name)) {
			Combine_card_holder_name.click();
			Combine_card_holder_name.clear();
			Combine_card_holder_name.sendKeys(testData.get("Account_holder_name"));
			Combine_card_number.click();
			Combine_card_number.clear();
			Combine_card_number.sendKeys(testData.get("Card_Number"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Combine_card_Expire_month.click();
			Select combineexpmnth = new Select(Combine_card_Expire_month);
			combineexpmnth.selectByIndex(3);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Combine_card_Expire_year.click();
			Select combineexpyear = new Select(Combine_card_Expire_year);
			combineexpyear.selectByIndex(6);
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Combine - Finance Information Navigation Successful and payment data entered");
			clickElementJS(nextVal);
			
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Payment Options Screen Navigation Failed");
		}
		if (verifyElementDisplayed(Payment_method_combine_due)) {
			Payment_method_combine_due.click();

			Select Payment_Method_combine2 = new Select(Payment_method_combine_due);
			Payment_Method_combine2.selectByIndex(2);
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Payment method selected");
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"error while selecting Payment method");
		}
		if (verifyElementDisplayed(Combine_card_holder_name)) {
			Combine_card_holder_name.click();
			Combine_card_holder_name.clear();
			Combine_card_holder_name.sendKeys(testData.get("Account_holder_name"));
			Combine_card_number.click();
			Combine_card_number.clear();
			Combine_card_number.sendKeys(testData.get("Card_Number"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Combine_card_Expire_month.click();
			Select combineexpmnth = new Select(Combine_card_Expire_month);
			combineexpmnth.selectByIndex(3);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Combine_card_Expire_year.click();
			Select combineexpyear = new Select(Combine_card_Expire_year);
			combineexpyear.selectByIndex(6);
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Combine - Finance Information Navigation Successful and payment data entered");
			clickElementJS(nextVal);
			
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Payment Options Screen Navigation Failed");
		}
		
		
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, saveContractClick, 50);

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Contract Save Page Navigation Error");

		}
		
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, 50);

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			clickElementJSWithWait(generate);
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), 50);

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
					"Member number could not be retrieved");
		}

	}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Navigation to Contract Processing - Combine Contract page failed");
			
	}
	
	}
	
	@FindBy(xpath="//input[@name='tourID']")
	WebElement Conversion_TSC;
	
	@FindBy(xpath="//select[@name='tourTime']")
	WebElement Conversion_WAVE;
	
	@FindBy(xpath="//input[@id='useSameAutoPayId']")
	WebElement Autopay_checkbox;
	
	public void wbw_Conversion_Contract_info() throws Exception{
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Owner_member1, 20);
		if (verifyElementDisplayed(Owner_member1)) {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
					"Navigated to Contract Processing - Conversion page successfully");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, Owner_member1, 20);
			if (verifyElementDisplayed(Owner_member1)) {
				Owner_member1.click();
				Owner_member1.sendKeys(testData.get("Owner_Member_1"));
				
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
						"owner number 1 Entered");
				
			}
			else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
						"Error while entering owner number 1");
			}
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyElementDisplayed(nextVal)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
						"Navigated to WorldMark - Primary Owner Information Page");
			clickElementJS(nextVal);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}
			}
			else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
						"Navigation to WorldMark - Primary Owner Information Page Failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyElementDisplayed(nextVal)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
						"Navigated to WorldMark - Owners  Page");
			clickElementJS(nextVal);
			}
			else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
						"Navigation to WorldMark - Owners Page Failed");
			}
			if(verifyElementDisplayed(Conversion_TSC)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
						"Navigated to WorldMark - Tour Information Page");
				Conversion_TSC.click();
				Conversion_TSC.sendKeys(testData.get("strTourId"));
				
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Conversion_WAVE.click();
				Select conversion_tsc = new Select(Conversion_WAVE);
				conversion_tsc.selectByIndex(6);
				clickElementJS(nextVal);
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.PASS,
						"Tour Information Details Entered");
			}
			else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Combine_Contract_info", Status.FAIL,
						"Navigation to WorldMark - Tour Information Page Failed");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJS(nextVal);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if(verifyElementDisplayed(Autopay_checkbox)) {
				Autopay_checkbox.click();
			}
			if (verifyElementDisplayed(Dues_Payment_Method)) {
				Dues_Payment_Method.click();

				Select Payment_Method = new Select(Dues_Payment_Method);
				Payment_Method.selectByIndex(2);
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.PASS,
						"Dues payment method clicked and selected");
			}
			else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.PASS,
						"error while selecting Payment method");
			}
			waitUntilElementVisibleIE(driver, Account_holder_name, 20);
			if (verifyElementDisplayed(Account_holder_name)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.PASS,
						"WorldMark - Payment Options Screen Navigation Successful");
				Account_holder_name.click();
				Account_holder_name.sendKeys(testData.get("Account_holder_name"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Card_number.click();
				Card_number.sendKeys(testData.get("Card_Number"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Card_expireMonth.click();
				Select expmnth = new Select(Card_expireMonth);
				expmnth.selectByIndex(3);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				Card_expireYear.click();
				Select expyear = new Select(Card_expireYear);
				expyear.selectByIndex(3);
				clickElementJS(nextVal);
				try {
					new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

					Alert alert = driver.switchTo().alert();
					alert.accept();
				} catch (Exception e) {
					log.info("No Alert Prersent");
				}
				
				}
			else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "WBW_discovery_upgrade_Contract_info", Status.FAIL,
						"WorldMark - Payment Options Screen Navigation Failed");
				}
			
			waitUntilElementVisibleIE(driver, salePersonId, 50);

			saleper.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			saleper.sendKeys(testData.get("strSalePer"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			clickElementJS(next1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, saveContractClick, 50);

			if (verifyElementDisplayed(savecontract)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
						"Contract Review Page Displayed and Save Button Clicked");

				clickElementJS(savecontract);
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
						"Contract Save Page Navigation Error");

			}
			
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, printClick, 50);

			if (verifyElementDisplayed(generate)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.PASS,
						"Contract Review Page Displayed and Generate Button Clicked");
				clickElementJSWithWait(generate);
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_Ex_ContractFlow", Status.FAIL,
						"Generate contract Page Navigation Error");
			}

			waitUntilElementVisibleIE(driver, By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), 50);

			if (verifyElementDisplayed(conNo)) {

				// testData.put("strContract", conNo.getText());
				strContractNo = conNo.getText();

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
						"Contract Created with number: " + conNo.getText());
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
						"Contract Creation failed ");
			}

			if (verifyElementDisplayed(memberNo)) {

				strMemberNo = memberNo.getText().split("\\:")[1].trim();
				log.info("MemberNo " + strMemberNo);

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.PASS,
						"Member No: " + strMemberNo);

			} else {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_ex_contractFlow", Status.FAIL,
						"Member number could not be retrieved");
			}
			
	
			}
	}
	@FindBy(xpath="//input[@name='process' and @value='Process Split']")
	WebElement Process_split;
	@FindBy(xpath="//input[@name='credits2']")
	WebElement Credit2;
	@FindBy(xpath="//input[@id='saveContract']")
	WebElement saveContract;
	@FindBy(xpath="//a[@href='/webapp/ccis/tw/PrintTradeServlet?tradeNumber=0' and contains(.,'Contract documents: Transferor')]")
	WebElement memebersplit;
	@FindBy(xpath="//form=[@id='pdfForm']")
	WebElement pdfgenerated;
	public void wbw_Split_contract() throws Exception{
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, Owner_member1, 20);
			if (verifyElementDisplayed(Owner_member1)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
						"Navigated to Contract Processing - Split Contract page successfully");

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleIE(driver, Owner_member1, 20);
				if (verifyElementDisplayed(Owner_member1)) {
					Owner_member1.click();
					Owner_member1.sendKeys(testData.get("WBW_Member"));
					
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
							"owner number Entered");
					
				}
				else {
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.FAIL,
							"Error while entering owner number");
				}
				clickElementJS(nextVal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if(verifyElementDisplayed(Process_split)){
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
							"Navigated to Contract Processing - Split Information  page successfully");
					Process_split.click();
					
				}
				else{
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.FAIL,
							"Navigation to Contract Processing - Split Information  page failed");
				}
				if(verifyElementDisplayed(Credit2)){
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
							"Navigated to Contract Processing - Split page successfully");
					Credit2.click();
					Credit2.clear();
					Credit2.sendKeys(testData.get("Credit_point"));
					saveContract.click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
							"Contract saved successfully");
					try {
						new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

						Alert alert = driver.switchTo().alert();
						alert.accept();
					} catch (Exception e) {
						log.info("No Alert Prersent");
					}
				}
				else{
					tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.FAIL,
							"Navigation to Contract Processing - Split page failed");
				}
			if(verifyElementDisplayed(memebersplit)){
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
						"Navigated to Split - Print Contracts page successfully");
				memebersplit.click();
			}
			else{
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.FAIL,
						"Navigation to Split - Print Contracts page failed");
			}
			if (verifyElementDisplayed(generate)) {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
						"Navigated to Split - Print Contract Forms page successfully");
				clickElementJSWithWait(generate);
			} else {
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.FAIL,
						"Navigation to Split - Print Contract Forms page failed");
				}
			if(verifyElementDisplayed(pdfgenerated)){
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
						"Navigated to Contract Printing page successful");
			}
			else{
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Split_contract", Status.PASS,
						"Navigation to Contract Printing page failed");
			}
			
			}
			
		}
	@FindBy(xpath="//input[@name='socialSecurityNumber']")
	WebElement WintoWynSSn;
	@FindBy(xpath="//input[@name='firstName']")
	WebElement WintoWynFname;
	@FindBy(xpath="//input[@name='lastName']")
	WebElement WintoWynLname;
	@FindBy(xpath="//input[@name='address1']")
	WebElement WintoWynadd1;
	@FindBy(xpath="//input[@name='address2']")
	WebElement WintoWynadd2;
	@FindBy(xpath="//input[@name='city']")
	WebElement WintoWyncity;
	@FindBy(xpath="//select[@name='stateAbbreviation']")
	WebElement WintoWynstate;
	@FindBy(xpath="//input[@name='postalCode']")
	WebElement WintoWynzip;
	@FindBy(xpath="//input[@name='country']")
	WebElement WintoWyncountry;
	@FindBy(xpath="//input[@name='homePhone']")
	WebElement WintoWynhomephn;
	@FindBy(xpath="//input[@name='workPhone']")
	WebElement WintoWynwrkphn;
	@FindBy(xpath="//input[@name='emailAddress']")
	WebElement WintoWyneaddress;
	@FindBy(xpath="//select[@name='ccPacCardType']")
	WebElement CC;
	
	public void wbw_Windows_to_Wyndham() throws Exception{
		driver.navigate().to(testData.get("MenuURL"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, tourIdEnter, 20);// to be deleted

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			clickElementJS(next1);
		} 
		else {

			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.FAIL,
					"Experience Page Navigation Error");

		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if(verifyElementDisplayed(WintoWynSSn)){
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.PASS,
					"Navigated to Experience - Primary Owner Information page successful");
			WintoWynSSn.click();
			WintoWynSSn.sendKeys(testData.get("ssnNumber"));
			WintoWynFname.click();
			WintoWynFname.sendKeys(testData.get("firstName"));
			WintoWynLname.click();
			WintoWynFname.sendKeys(testData.get("lastName"));
			WintoWynadd1.click();
			WintoWynFname.sendKeys(testData.get("address1"));
			WintoWynadd2.click();
			WintoWynadd2.sendKeys(testData.get("address2"));
			WintoWyncity.click();
			WintoWyncity.sendKeys(testData.get("city"));
			
			new Select(driver.findElement(By.name("stateAbbreviation"))).selectByValue(testData.get("state"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			WintoWynzip.click();
			WintoWynzip.sendKeys(testData.get("postalCode"));
			WintoWyncountry.click();
			//new Select(driver.findElement(By.name("country"))).selectByValue(testData.get("country"));
			new Select(driver.findElement(By.name("country"))).selectByVisibleText("United States");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			WintoWynhomephn.click();
			WintoWynhomephn.sendKeys(testData.get("homePhone"));
			WintoWynwrkphn.click();
			WintoWynwrkphn.sendKeys(testData.get("workPhone"));
			WintoWyneaddress.click();
			WintoWyneaddress.sendKeys(testData.get("emailAddress"));
			clickElementJS(nextVal);
			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());

				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (Exception e) {
				log.info("No Alert Prersent");
			}
		}
		else
		{
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.FAIL,
					"Navigation to Experience - Primary Owner Information page Failed");
		}
		if(verifyElementDisplayed(nextVal)){
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.PASS,
					"Navigated to Experience - Owners  page successful");
			clickElementJS(nextVal);
			
		}
		else{
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.FAIL,
					"Navigation to Experience - Owners  page failed");
		}
		
		if(verifyElementDisplayed(nextVal)){
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.PASS,
					"Navigated to Experience - Purchase Information page successful");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			new Select(driver.findElement(By.name("explID"))).selectByIndex(1);
			clickElementJS(nextVal);
			
		}
		else{
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.FAIL,
					"Navigation to Experience - Purchase Information page failed");
		}
		if(verifyElementDisplayed(nextVal)){
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.PASS,
					"Navigated to Experience - Down Payments page successful");
			clickElementJS(nextVal);
			
		}
		else{
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.FAIL,
					"Navigation to Experience - Down Payments page failed");
		}
		if (verifyElementDisplayed(Combine_card_holder_name)) {
			Combine_card_holder_name.click();
			Combine_card_holder_name.clear();
			Combine_card_holder_name.sendKeys(testData.get("Account_holder_name"));
			Combine_card_number.click();
			Combine_card_number.clear();
			Combine_card_number.sendKeys(testData.get("Card_Number"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Combine_card_Expire_month.click();
			Select combineexpmnth = new Select(Combine_card_Expire_month);
			combineexpmnth.selectByIndex(3);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Combine_card_Expire_year.click();
			Select combineexpyear = new Select(Combine_card_Expire_year);
			combineexpyear.selectByIndex(6);
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.PASS,
					"Combine - Finance Information Navigation Successful and payment data entered");
			clickElementJS(nextVal);
			
		}
		else {
			tcConfig.updateTestReporter("SalePointWBWMenuPage", "wbw_Windows_to_Wyndham", Status.FAIL,
					"Payment Options Screen Navigation Failed");
		}
		
	}
}


*/