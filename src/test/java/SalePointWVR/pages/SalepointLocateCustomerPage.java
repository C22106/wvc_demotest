package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class SalepointLocateCustomerPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointLocateCustomerPage.class);

	public SalepointLocateCustomerPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(id = "customerNumber")
	protected WebElement customerNoInput;

	@FindBy(id = "customerNumber")
	protected WebElement customerNo;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement next1;

	/*
	 * Method: navigateToMenu Description:Navigate to Menuo Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void navigateToMenu(String Iterator) throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().to(testData.get("MenuURL" + Iterator));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: provideCRSNumber Description:Provide CRS Number Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void provideCRSNumber() throws Exception {

		waitUntilElementVisibleIE(driver, customerNoInput, "ExplicitLowWait");

		if (verifyElementDisplayed(customerNo)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			customerNo.click();
			customerNo.sendKeys(testData.get("strTourId"));
			clickElementJS(next1);
			if(testData.get("AutomationID").toLowerCase().contains("auto")){			
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.alertIsPresent());
		
				if (ExpectedConditions.alertIsPresent() != null) {
					try {
						driver.switchTo().alert().accept();
						tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Automation Pop Up message");
		
					} catch (Exception e) {
						log.info("no Alert is displayed");
					}
		
				} else {
		
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
							"Automation Pop Up is not available");
				}
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Page Navigation Error");

		}
	}
}