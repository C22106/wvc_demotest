package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class SalepointContractPaymentPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractPaymentPage.class);

	public SalepointContractPaymentPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/*@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;*/
	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achAutoPaySelect;
	@FindBy(xpath = "//input[@name='bank_name']")
	protected WebElement bank_name;
	@FindBy(xpath = "//input[@name='bank_city']")
	protected WebElement bank_city;
	@FindBy(xpath = "//input[@name='bank_postal_code']")
	protected WebElement bank_postal_code;
	@FindBy(xpath = "//input[@name='bank_account_number']")
	protected WebElement bank_account_number;
	@FindBy(xpath = "//input[@name='account_holder_name']")
	protected WebElement account_holder_name;
	@FindBy(xpath = "//input[@name='bank_routing_number']")
	protected WebElement bank_routing_number;
	@FindBy(xpath = "//input[contains(@id,'fairsharePlusPaymentType' and @value = '2']")
	protected WebElement selectCCAutopay;
	@FindBy(xpath = "//td[@id = 'requiredId']")
	protected WebElement payRequired;
	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;
	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	protected WebElement cashSelect;
	@FindBy(xpath = "//td[contains(.,'Cash Sale')]")
	protected WebElement cashSelectdisc;
	@FindBy(xpath = "//td[contains(.,'CC Auto')]/input[@type='radio']")
	protected WebElement ccSelect;
	@FindBy(xpath = "//input[@type='radio' and @value='loan_cc']")
	protected WebElement ccSelectDisc;
	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achSelect;
	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculate;
	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	protected WebElement cashRadio;
	@FindBy(xpath = "//td[contains(.,'Cash Sale')]")
	protected WebElement cashSale;
	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculateClick;
	@FindBy(xpath = "//select[@name='down_payment_method0']")
	protected WebElement payMethod;
	// public By payAmount = By.xpath("//input[@id='down_payment_amount0']");

	@FindBy(xpath = "//input[@id='down_payment_amount0']")
	protected WebElement payAmount;

	@FindBy(xpath = "//input[@value = 'Next']")
	protected WebElement clickNext;

	@FindBy(xpath = "//input[@id = 'fairSharePlus.creditCard.cardMemberName']")
	protected WebElement ccHolder;

	@FindBy(xpath = "//select[@id = 'fairSharePlus.creditCard.cardType']")
	protected WebElement ccType;

	@FindBy(xpath = "//input[@id = 'fairSharePlus.creditCard.cardNumber']")
	protected WebElement ccNumber;

	@FindBy(xpath = "//select[@id = 'fairSharePlus.creditCard.expireMonth']")
	protected WebElement ccMonth;

	@FindBy(xpath = "//select[@id = 'fairSharePlus.creditCard.expireYear']")
	protected WebElement ccYear;

	@FindBy(xpath = "//input[@id = 'useSameAutoPayId']")
	WebElement useSameAutopay;

	@FindBy(xpath = "//input[contains(@name,'cc_payment_member_name')]")
	WebElement ccName;

	@FindBy(xpath = "//input[contains(@name,'cc_payment_account_number')]")
	WebElement creditCardNumber;
	
	@FindBy(xpath = "//input[contains(@name,'cc_account_number')]")
	WebElement creditCardDiscAccNumber;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_type')]")
	WebElement creditCardcType;
	
	@FindBy(xpath = "//select[contains(@name,'cc_type')]")
	WebElement creditCardDiscType;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_month')]")
	WebElement creditCardcMonth;
	
	@FindBy(xpath = "//select[contains(@name,'cc_exp_month')]")
	WebElement creditCardDiscMonth;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_year')]")
	WebElement creditCardcYear;
	
	@FindBy(xpath = "//select[contains(@name,'cc_exp_year')]")
	WebElement creditCardDiscYear;
	
	protected By txtDownPay=By.xpath("//input[contains(@name,'down_payment_amount')]");
	protected By txtdownPayError=By.xpath("//div[contains(@id,'error') and contains(text(),'Down Payment is required')]");

	/*
	 * Method: contractProcessingPayment Description:Contract Procesing Payment
	 * validation Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractProcessingPayment() throws Exception {
		selectPaymentMethod();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		processPaymentConfirm();
		waitUntilElementVisibleIE(driver, calculateClick, "ExplicitLongWait");
		if (verifyElementDisplayed(calculate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");
			clickElementJS(calculate);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextClick);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try{
				driver.switchTo().alert().accept();
			}catch(Exception e){
				log.info("Alert is not present");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Money Screen not present");
		}
	}
	
	public void selectPaymentMethod(){
		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");
		if (testData.get("PaymentBy").equalsIgnoreCase("CCAutoPay")) {
			if (verifyObjectDisplayed(ccSelect)) {
				clickElementJS(ccSelect);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJS(ccSelect);
				if(testData.get("SalesType").equalsIgnoreCase("discovery")){
					sendKeyboardKeys(Keys.ARROW_RIGHT);
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				fieldDataEnter(ccName, testData.get("CCHolder"));
				if(testData.get("SalesType").equalsIgnoreCase("discovery")){
					selectByText((creditCardDiscType), testData.get("CCType"));
					fieldDataEnter(creditCardDiscAccNumber, testData.get("CCNumber"));
					selectByText(creditCardDiscMonth, testData.get("CCMonth"));
					selectByText(creditCardDiscYear, testData.get("CCYear"));
				}else{
					selectByValue((creditCardcType), testData.get("CCType").toLowerCase());
					fieldDataEnter(creditCardNumber, testData.get("CCNumber"));
					selectByText(creditCardcMonth, testData.get("CCMonth"));
					selectByText(creditCardcYear, testData.get("CCYear"));
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "CC Select not present");
			}

		} else if (testData.get("PaymentBy").equalsIgnoreCase("Cash")) {
			if (verifyObjectDisplayed(cashSelect)) {
				clickElementJS(cashRadio);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJS(cashRadio);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJS(cashRadio);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Cash Select not present");
			}

		} else if (testData.get("PaymentBy").contains("ACH Auto Pay")) {
			if (verifyObjectDisplayed(achAutoPaySelect)) {
				clickElementJS(achAutoPaySelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJS(achAutoPaySelect);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				bank_name.sendKeys(testData.get("BankName"));
				bank_city.sendKeys(testData.get("BankCity"));
				bank_postal_code.sendKeys(testData.get("BankPostalCode"));
				bank_account_number.sendKeys(testData.get("BankAccNumber"));
				account_holder_name.sendKeys(testData.get("AccountHolderName"));
				bank_routing_number.sendKeys(testData.get("BankRoutingNumber"));
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "ACH Auto Pay Selected");

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"CC Auto Pay radio button not present");
			}
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Payment details for "
				+ testData.get("PaymentBy")+" has been entered successfully");
	}
	
	public void enterInvalidPaymentAmount(){
		String strAmountDiplayed=getElementAttribute(txtDownPay, "value");
		String strActualAmount=strAmountDiplayed.substring(1,strAmountDiplayed.length());
		fieldDataEnter(txtDownPay, strActualAmount);
		processPaymentConfirm();
		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info(e);
		}
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		driver.switchTo().activeElement().sendKeys(Keys.PAGE_UP);
		if(verifyObjectDisplayed(txtdownPayError)){
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Error message is displayed for "
					+ "invalid down payment amount");
		}else{
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Error message is NOT displayed for "
					+ "invalid down payment amount");
		}		
	}
	
	public void processPaymentConfirm(){
		new Select((payMethod)).selectByVisibleText(testData.get("PaymentMethod"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementJS(nextClick);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			log.info(e);
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

}
