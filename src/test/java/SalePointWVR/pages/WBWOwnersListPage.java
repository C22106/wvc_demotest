package SalePointWVR.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class WBWOwnersListPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWOwnersListPage.class);
	WBWOwnerInformationPage ownerInfo = new WBWOwnerInformationPage(tcConfig);

	public WBWOwnersListPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Owner Information')]")
	protected WebElement ownerInformationHeader;

	@FindBy(xpath = "(//tr[@class='page_title']//div[contains(.,'Owners')] | //tr[@class='page_title' and contains(.,'Owner Information')])")
	protected WebElement ownerDetailsPage;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected List<WebElement> deleteSecondaryOwner;

	@FindBy(xpath = "//tr/td[contains(text(),'Owner Information')]/../td/input[@type='button']")
	protected WebElement editPrimaryOwner;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement secondaryOwners;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[1]")
	protected WebElement deleteFirstOwner;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	protected WebElement primaryOwner;

	@FindBy(name = "next")
	protected WebElement nextButton;

	@FindBy(xpath = "//a[contains(.,'Make Primary')]")
	protected WebElement makeOwnerPrimary;

	/*
	 * Method: ownerInfoPageValidation Description:Validate navigation to Owner
	 * Information page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void ownerInformationPageNavigation() {
		waitUntilElementVisible(driver, ownerDetailsPage, "ExplicitMedWait");
		if (verifyObjectDisplayed(ownerInformationHeader)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Navigated to Primary Owner Information Page", true);
			ownerInfo.enterOwnerInformation();
		} else if (verifyObjectDisplayed(ownerDetailsPage)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigated to Owner Details Page",
					true);
			actionOnSecondaryOwner();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Owner Information Page navigation Failed", true);
		}

		waitUntilElementVisible(driver, ownerDetailsPage, "ExplicitMedWait");
		clickNextButton();
	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}

	/*
	 * Method: actionOnSecondaryOwner Description:Take appropriate action on
	 * Secondary Owners page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void actionOnSecondaryOwner() {
		int totalSecondaryOwner;
		if (verifyObjectDisplayed(secondaryOwners)) {
			totalSecondaryOwner = deleteSecondaryOwner.size();

			if (verifyElementDisplayed(primaryOwner)) {
				deleteSecondaryOwners(totalSecondaryOwner);
			} else {
				deleteSecondaryOwners(totalSecondaryOwner - 1);
				makeOneOwnerPirmary();
			}

		}
		editPrimaryOwnerInformation();

	}

	/*
	 * Method: editPrimaryOwnerInformation Description:Edit Primary Owner
	 * Information page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void editPrimaryOwnerInformation() {
		if (verifyObjectDisplayed(editPrimaryOwner)) {
			editPrimaryOwner.click();
			ownerInfo.enterOwnerInformation();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Primary Owner Edit CTA not present");

		}
	}

	/*
	 * Method: deleteSecondaryOwners Description:Delete Secondary owners* Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void deleteSecondaryOwners(int howManySecondaryOwnersToDelete) {
		for (int i = 0; i < howManySecondaryOwnersToDelete; i++) {

			clickElementJSWithWait(deleteFirstOwner);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					log.info(e);
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}
	}

	/*
	 * Method: makeOneOwnerPirmary Description:Make a Secondary Owner Primary *
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void makeOneOwnerPirmary() {
		if (verifyObjectDisplayed(makeOwnerPrimary)) {
			clickElementJSWithWait(makeOwnerPrimary);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

}