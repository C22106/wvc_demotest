package SalePointWVR.pages;

import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class WSARetrieveTourPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WSARetrieveTourPage.class);

	public WSARetrieveTourPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//a[text()='Retrieve']")
	protected WebElement retriveLink;

	@FindBy(xpath = "//input[@name='startDate']")
	protected WebElement txtStartDate;

	@FindBy(xpath = "//input[@name='endDate']")
	protected WebElement txtEndDate;

	@FindBy(xpath = "//input[@name='siteNumber']")
	protected WebElement txtSiteNumber;

	@FindBy(xpath = "//button[@name='lu-submit']")
	protected WebElement submitBtnRtrv;

	@FindBy(name = "tourNumber")
	protected WebElement txtTourNumberRetrv;

	@FindBy(xpath = "//input[@name='controlNumber']")
	protected WebElement txtReferenceNumber;

	@FindBy(xpath = "//select[contains(@title,'Number of results to display per page')]")
	protected WebElement NoOfResDrpdwn;

	@FindBy(xpath = "//select[@id='pitch-lookup-type']")
	protected WebElement lookUpDrpDwn;

	/*
	 * Method: navigatetoretrievePitch Description:navigate To Ririeve Page
	 * Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void navigatetoretrievePitch() throws Exception {
		waitUntilElementVisible(driver, retriveLink, "ExplicitLongWait");
		if (verifyElementDisplayed(retriveLink)) {
			retriveLink.click();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Retrieve link clicked successfully");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Retrieve link not displayed");
		}
		waitUntilElementVisible(driver, lookUpDrpDwn, "ExplicitLongWait");
	}

	/*
	 * Method: retrievePitch Description:Rerieve Pitch Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void retrievePitch() throws Exception {
		// LocalDate today=LocalDate.now();
		String CheckinDate = "", strSiteNumber = null;

		CheckinDate = addDateInSpecificFormat("MM/dd/yyyy", 0);
		waitUntilElementVisible(driver, lookUpDrpDwn, "ExplicitLongWait");

		new Select(lookUpDrpDwn).selectByVisibleText(testData.get("LookUpBy"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (testData.get("LookUpBy").equalsIgnoreCase("Date Range")) {
			if (verifyElementDisplayed(txtStartDate)) {
				txtStartDate.click();
				txtStartDate.clear();
				txtStartDate.sendKeys(CheckinDate);
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Start date entered as : " + CheckinDate);
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Start date txtbox is not displayed");
			}
			txtEndDate.click();
			txtEndDate.clear();
			txtEndDate.sendKeys(CheckinDate);
			// if(testData.get("SiteName").contains("WYNDHAM BONNET CREEK
			// (00064)")) {
			// strSiteNumber="00064";
			// }
			strSiteNumber = testData.get("SiteName").substring(testData.get("SiteName").indexOf("(") + 1,
					testData.get("SiteName").indexOf(")"));
			txtSiteNumber.click();
			txtSiteNumber.clear();
			txtSiteNumber.sendKeys(strSiteNumber);
			txtSiteNumber.sendKeys(Keys.TAB);
			submitBtnRtrv.click();
			waitUntilElementVisible(driver, NoOfResDrpdwn, "ExplicitLongWait");
			new Select(NoOfResDrpdwn).selectByValue("50");

			waitUntilElementVisible(driver, By.xpath("//td[text()='" + WSANewProposalPage.createdPitchID + "']"),
					"ExplicitMedWait");
			if (verifyObjectDisplayed(By.xpath("//td[text()='" + WSANewProposalPage.createdPitchID + "']"))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Pitch retrieve successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Unable to retrieve the pitch");
			}
		} else if (testData.get("LookUpBy").equalsIgnoreCase("Tour Number")) {
			if (verifyElementDisplayed(txtTourNumberRetrv)) {
				txtTourNumberRetrv.sendKeys(testData.get("TourNumber"));
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Tour Number entered");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Tour Number field is not displayed");
			}
			submitBtnRtrv.click();
			waitUntilElementVisible(driver, By.xpath("//td[text()='" + WSANewProposalPage.createdPitchID + "']"),
					"ExplicitLongWait");
			if (verifyObjectDisplayed(By.xpath("//td[text()='" + WSANewProposalPage.createdPitchID + "']"))) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Pitch #" + WSANewProposalPage.createdPitchID + "retrieved successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Unable to retrieve the pitch #" + WSANewProposalPage.createdPitchID);
			}
		} else if (testData.get("LookUpBy").equalsIgnoreCase("Reference Number")) {
			if (verifyElementDisplayed(txtReferenceNumber)) {
				txtReferenceNumber.sendKeys(WSANewProposalPage.createdPitchID.trim());
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Reference number entered");

				submitBtnRtrv.click();
				waitUntilElementVisible(driver, By.xpath("//td[text()='" + WSANewProposalPage.createdPitchID + "']"),
						"ExplicitLongWait");
				if (verifyObjectDisplayed(By.xpath("//td[text()='" + WSANewProposalPage.createdPitchID + "']"))) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
							"Pitch #" + WSANewProposalPage.createdPitchID + "retrieved successfully");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
							"Unable to retrieve the pitch #" + WSANewProposalPage.createdPitchID);
				}

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Unable to retrieve the pitch #" + WSANewProposalPage.createdPitchID);
			}
		}

	}

}