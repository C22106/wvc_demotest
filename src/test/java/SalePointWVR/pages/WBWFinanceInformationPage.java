package SalePointWVR.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class WBWFinanceInformationPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWFinanceInformationPage.class);

	public WBWFinanceInformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Finance Information')]")
	protected WebElement financeInformationHeader;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//select[@id='loanPACId']")
	protected WebElement selectPaymentMethod;

	@FindBy(xpath = "//input[contains(@name,'Token']")
	protected WebElement tokenInputBox;

	@FindBy(xpath = "//select[@name='ccPacCardType']")
	protected WebElement selectCardType;

	@FindBy(name = "ccPacNameOnCard")
	protected WebElement nameOnCard;

	@FindBy(name = "ccPacCardNum")
	protected WebElement enterCardNumber;

	@FindBy(xpath = "//select[@name='ccPacCardExpiresMonth']")
	protected WebElement selectExpiryMonth;

	@FindBy(xpath = "//select[@name='ccPacCardExpiresYear']")
	protected WebElement selectExpiryYear;

	/*
	 * Method: financeInformationPageNavigation Description:Finance Info Page Nav Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void financeInformationPageNavigation() throws Exception {
		waitUntilElementVisibleIE(driver, financeInformationHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(financeInformationHeader), "Finance Info page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Finance Info Page");

	}

	/*
	 * Method: selectPaymentMethod Description:Select Payment Method Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectPaymentMethod() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectPaymentMethod, testData.get("PaymentBy"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method: enterPaymentDetails Description:enter Payment Details Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterPaymentDetails() {
		if (testData.get("PaymentBy").toLowerCase().contains("cc")) {
			waitUntilElementVisible(driver, tokenInputBox, "ExplicitMedWait");
			enterCCAutoPayDetails();
		} else if (testData.get("PaymentBy").toLowerCase().contains("no")) {
			log.info("No Auto Pay Selected");
		} else {
			waitUntilElementVisibleIE(driver, tokenInputBox, "ExplicitMedWait");
			// write code for Auto Pay
		}

	}

	/*
	 * Method: enterCcAutoPayDetails Description:enter Auto Pay Payment Details
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void enterCCAutoPayDetails() {
		selectByText(selectCardType, testData.get("CCType"));
		nameOnCard.sendKeys(testData.get("CCHolder"));
		enterCardNumber.sendKeys(testData.get("CCNumber"));
		selectByText(selectExpiryMonth, testData.get("CCMonth"));
		selectByText(selectExpiryYear, testData.get("CCYear"));
	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}
}