package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

import salepointWBW.pages.SalePointBasePage;

public class SalepointPICInformationPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointPICInformationPage.class);

	public SalepointPICInformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//div[contains(text(),'PIC Information')]")
	protected WebElement pagePICInfoTitle;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//input[@name='AddInfo']")
	protected WebElement addPIC;

	@FindBy(xpath = "//div[contains(text(),'Add PIC')]")
	protected WebElement pageAddPIC;

	@FindBy(xpath = "//input[@name='resortName']")
	protected WebElement resortName;

	@FindBy(xpath = "//input[@name='resortID']")
	protected WebElement resortID;

	@FindBy(xpath = "//input[@name='resortCity']")
	protected WebElement resortCity;

	@FindBy(xpath = "//input[@name='resortState']")
	protected WebElement resortState;

	@FindBy(xpath = "//input[@name='fixed_unit']")
	protected WebElement fixedunit;

	@FindBy(xpath = "//input[@name='fixed_week']")
	protected WebElement fixedweek;

	@FindBy(xpath = "//select[@name='usage_type']")
	protected WebElement usagetype;

	@FindBy(xpath = "//select[@name='ownership_type']")
	protected WebElement ownershipType;

	@FindBy(xpath = "//select[@name='season']")
	protected WebElement demand;

	@FindBy(xpath = "//select[@name='unit_type']")
	protected WebElement unittype;

	@FindBy(xpath = "//select[@name='non_lock_off_unit_type']")
	protected WebElement nonLockOffOption;

	@FindBy(xpath = "//input[@name='add']")
	protected WebElement btnAdd;

	@FindBy(xpath = "//input[@name = 'Finished']")
	protected WebElement buttonFinished;

	/*
	 * Method: addPICInformation Description:Add PIC Info Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void addPICInformation() throws Exception {
		waitUntilElementVisibleIE(driver, addPIC, "ExplicitLongWait");
		if (verifyElementDisplayed(pagePICInfoTitle)) {

			clickElementJS(addPIC);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyElementDisplayed(pageAddPIC)) {
				resortName.click();
				resortName.clear();
				resortName.sendKeys(testData.get("PIC_ResortName"));

				resortID.click();
				resortID.clear();
				resortID.sendKeys(testData.get("PIC_ResortID"));

				resortCity.click();
				resortCity.clear();
				resortCity.sendKeys(testData.get("PIC_ResortCity"));

				resortState.click();
				resortState.clear();
				resortState.sendKeys(testData.get("PIC_ResortState"));

				fixedunit.click();
				fixedunit.clear();
				fixedunit.sendKeys(testData.get("PIC_fixedunit"));

				fixedweek.click();
				fixedweek.clear();
				fixedweek.sendKeys(testData.get("PIC_fixedweek"));

				String strUsageType = testData.get("PIC_usagetype");
				usagetype.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'" + strUsageType + "')]")).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				String strOwnershipType = testData.get("PIC_ownershipType");
				ownershipType.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'" + strOwnershipType + "')]")).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				String strDemand = testData.get("PIC_Demand");
				demand.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'" + strDemand + "')]")).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				String strUnitType = testData.get("PIC_UnitType");
				unittype.click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(By.xpath("//select/option[contains(.,'" + strUnitType + "')]")).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyElementDisplayed(nonLockOffOption)) {
					String strNonLockOffOpt = testData.get("PIC_NonLockOffOption").trim();
					new Select(driver.findElement(By.xpath("//select[@id = 'non_lock_off_unit_type']")))
							.selectByVisibleText(strNonLockOffOpt);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
							"Successfully Entered PIC Information");
				}
				btnAdd.click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Add a PIC entry button not displayed");
			}
		}
	}

	/*
	 * Method: clickButonFinished Description:Click Finished Button Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void clickButonFinished() {
		waitUntilElementVisibleIE(driver, buttonFinished, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(buttonFinished), "Finished Button not displayed");
		buttonFinished.click();
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Successfully Clicked on Button Finished");
	}

}