package SalePointWVR.pages;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import salepointWBW.pages.SalePointBasePage;

public class SalepointContractManagementPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractManagementPage.class);

	public SalepointContractManagementPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//a[contains(.,'ALL BY DATE')]")
	protected WebElement selectAllByDate;
	@FindBy(xpath = "//a[contains(.,'OPEN')]")
	protected WebElement selectOpen;
	@FindBy(xpath = "//div[contains(.,'Contracts In Batch Report')]")
	protected WebElement headerBatchReport;
	@FindBy(xpath = "//input[@name='getData']")
	protected WebElement getData;
	@FindBy(xpath = "//input[@class='button' and @value='Void']")
	protected WebElement voidBtn;
	@FindBy(xpath = "//tr[@class='section_title']/following::tr[1]/td")
	protected WebElement statusChangeMessage;
	@FindBy(xpath = "//td[contains(.,'Contract status changed successfully')]")
	protected WebElement trnsmitSuccessMessage;
	@FindBy(xpath = "//input[@value='Change Status']")
	protected WebElement statusChangeClick;
	@FindBy(xpath = "//input[@value='Transmit']")
	protected WebElement transmitClick;
	@FindBy(xpath = "//tr/td/a")
	protected List<WebElement> totalResult;
	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	protected WebElement reportBack;
	@FindBy(xpath = "//tr/td[contains(.,'T')]")
	protected WebElement logStatusVali;
	@FindBy(xpath = "//input[@value='Change Status']")
	protected WebElement changeStatus;
	@FindBy(xpath = "//input[@value='Transmit']")
	protected WebElement Transmit;
	@FindBy(xpath = "//a[contains(.,'Return to Batch Report')]")
	protected WebElement returnBack;
	@FindBy(xpath = "//tr/td[contains(.,'T')]")
	protected WebElement validateStatus;

	
	/*
	 * Method: navigateToContractManagementPage Description:Navigate to Contract
	 * management Page Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void navigateToContractManagementPage() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		driver.navigate().to(testData.get("contractMgmt"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, selectAllByDate, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(selectAllByDate), "Failed to navigate to Contract Management Page");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Contract in batch report Page Displayed");
	}

	/*
	 * Method: clickOptionAllByDate Description:Click Option All by date
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickOptionAllByDate() {
		clickElementJS(selectAllByDate);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, headerBatchReport, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerBatchReport), "Failed to navigate to Batch Report Page");

	}

	
	/*
	 * Method: clickOptionOpen Description:Click Option Open
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickOptionOpen() {
		clickElementJS(selectOpen);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, headerBatchReport, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerBatchReport), "Failed to navigate to Batch Report Page");

	}

	/*
	 * Method: getBatchReportData Description:Get batch report data
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void getBatchReportData() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Calendar cal = Calendar.getInstance();
		int monthVal = (cal.get(Calendar.MONTH));

		String strMonth = String.valueOf(monthVal).trim();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		String strDate = String.valueOf(dayOfMonth - 1).trim();

		waitUntilElementVisibleIE(driver, By.name("startmonth"), "ExplicitLowWait");

		new Select(driver.findElement(By.name("startmonth"))).selectByValue(strMonth);

		new Select(driver.findElement(By.name("startday"))).selectByValue(strDate);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(getData);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: getDataClickDateOption Description:click get data
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void getDataClickDateOption() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(getData);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: checkReportResult Description:check report result
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkReportResult() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		int resultTotal = (totalResult).size();
		if (resultTotal > 0) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Batch Report Opened and total Result: " + resultTotal);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Failed to load Batch Report");
		}
	}

	/*
	 * Method: transmitContract Description:Transmit Contract
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void transmitContract() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver,
				By.xpath("//tr/td/a[contains(.,'" + SalepointContractPrintPage.strContractNumber + "')]"), "ExplicitLowWait");

		if (verifyElementDisplayed(driver.findElement(
				By.xpath("//tr/td/a[contains(.,'" + SalepointContractPrintPage.strContractNumber + "')]")))) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract No Verified and is: " + SalepointContractPrintPage.strContractNumber);
			WebElement contractInBatchReport = driver.findElement(
					By.xpath("//tr/td/a[contains(.,'" + SalepointContractPrintPage.strContractNumber + "')]"));
			clickElementJS(contractInBatchReport);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract No Not Verified");
		}

		waitUntilElementVisibleIE(driver, statusChangeClick, "ExplicitLongWait");
		if (verifyElementDisplayed(changeStatus)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Summary Page Navigation Successful and Verified");
			clickElementJSWithWait(changeStatus);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleIE(driver, transmitClick, "ExplicitLongWait");
			clickElementJSWithWait(Transmit);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, trnsmitSuccessMessage, "ExplicitLongWait");
			if (verifyElementDisplayed(trnsmitSuccessMessage)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Contract Transmitted Successfully");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Contract Transmitted Successfully");
			}

		}
	}

	/*
	 * Method: voidContract Description:Void Contract
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void voidContract() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver,
				By.xpath("//tr/td/a[contains(.,'" + SalepointContractPrintPage.strContractNumber + "')]"), "ExplicitLowWait");

		if (verifyElementDisplayed(driver.findElement(
				By.xpath("//tr/td/a[contains(.,'" + SalepointContractPrintPage.strContractNumber + "')]")))) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract No Verified and is: " + SalepointContractPrintPage.strContractNumber);
			WebElement contractInBatchReport = driver.findElement(
					By.xpath("//tr/td/a[contains(.,'" + SalepointContractPrintPage.strContractNumber + "')]"));
			clickElementJS(contractInBatchReport);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract No Not Verified");
		}

		waitUntilElementVisibleIE(driver, statusChangeClick, "ExplicitLongWait");
		if (verifyElementDisplayed(changeStatus)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Summary Page Navigation Successful and Verified");
			clickElementJSWithWait(changeStatus);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleIE(driver, voidBtn, "ExplicitLongWait");
			clickElementJS(voidBtn);

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				log.info("No Alert Present");
			}

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			String msg = (statusChangeMessage).getText();
			if (msg.equalsIgnoreCase("Contract status changed successfully.")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Contract Status change to Void");

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Error in changing the status");
			}

		}
	}

	/*
	 * Method: voidContractAutomation Description:void automation contract
	 *  Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void voidContractAutomation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver,
				By.xpath("//tr/td/a[contains(.,'" + SalepointContractSummaryPage.strContractNo + "')]"), "ExplicitLowWait");

		if (verifyElementDisplayed(driver.findElement(
				By.xpath("//tr/td/a[contains(.,'" + SalepointContractSummaryPage.strContractNo + "')]")))) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract No Verified and is: " + SalepointContractSummaryPage.strContractNo);
			WebElement contractInBatchReport = driver.findElement(
					By.xpath("//tr/td/a[contains(.,'" + SalepointContractSummaryPage.strContractNo + "')]"));
			clickElementJS(contractInBatchReport);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract No Not Verified");
		}

		waitUntilElementVisibleIE(driver, statusChangeClick, "ExplicitLongWait");
		if (verifyElementDisplayed(changeStatus)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Summary Page Navigation Successful and Verified");
			clickElementJSWithWait(changeStatus);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleIE(driver, voidBtn, "ExplicitLongWait");
			clickElementJS(voidBtn);

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} catch (Exception e) {
				log.info("No Alert Present");
			}

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			String msg = (statusChangeMessage).getText();
			if (msg.equalsIgnoreCase("Contract status changed successfully.")) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Contract Status change to Void");

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Error in changing the status");
			}

		}
	}
}
