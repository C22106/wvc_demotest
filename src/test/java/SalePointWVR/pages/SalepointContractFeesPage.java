package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import salepointWBW.pages.SalePointBasePage;

public class SalepointContractFeesPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractFeesPage.class);

	public SalepointContractFeesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;
	

	/*
	 * Method: contractProcessingPayment Description:Contract Procesing Payment
	 * validation Date: 2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void contractFeesScreen() throws Exception {

		waitUntilElementVisibleIE(driver, nextClick, "ExplicitLongWait");
		clickElementJS(nextClick);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		//if (ExpectedConditions.alertIsPresent() != null) {
			try {
				driver.switchTo().alert().accept();
			} catch (Exception e) {
				log.info("No Alert for Fees");
			}
		//}		
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Next button clicked in Contract Fees Screen");		
	}

}
