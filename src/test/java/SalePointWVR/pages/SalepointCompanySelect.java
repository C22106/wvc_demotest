package SalePointWVR.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;
import salepointWBW.pages.SalePointBasePage;

public class SalepointCompanySelect extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointCompanySelect.class);

	public SalepointCompanySelect(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='ok']")
	protected WebElement okBtn;

	@FindBy(xpath = "//SELECT[@id='companyList']")
	protected WebElement companySelect;

	/*
	 * Method: companySelect Description:Select Company
	 * Date: 2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void companySelect() throws Exception {

		waitUntilElementVisibleIE(driver, companySelect, "ExplicitMedWait");

		String strCompany = testData.get("strCompany");

		if (strCompany.equalsIgnoreCase("WBW")) {

			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Worldmark By Wyndham");

		} else if (strCompany.equalsIgnoreCase("WVR")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts");
		} else if (strCompany.equalsIgnoreCase("WVRAP")) {
			new Select(driver.findElement(By.xpath("//SELECT[@id='companyList']")))
					.selectByVisibleText("Wyndham Vacation Resorts Asia Pacific");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Enter Valid Company 1.WBW 2.WVR 3.WVRAP");
		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// driver.switchTo().activeElement().sendKeys(Keys.TAB);

		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Company Selected as: " + strCompany);

		clickElementJS(okBtn);

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// Actions act = new Actions(driver);

		if (strCompany.equalsIgnoreCase("WVRAP")) {
			log.info("No Alert Present");

		} else {

			try {
				new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				log.info("No Alert Present after Login");
			}
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"HomePage Navigation Successful");
	}
}