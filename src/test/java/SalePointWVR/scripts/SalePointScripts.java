
package SalePointWVR.scripts;

import java.util.Map;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;

import SalePointWVR.pages.SalePointAdminPage;
import SalePointWVR.pages.SalePointHomePage;
import SalePointWVR.pages.SalePointLoginPage;
import SalePointWVR.pages.SalePointOwnerPage;
import SalePointWVR.pages.SalePointPaymentScreen;
import SalePointWVR.pages.SalePointWBW;
import SalePointWVR.pages.SalePointWVR;
import SalePointWVR.pages.SalepointCommissionandIncentivePage;
import SalePointWVR.pages.SalepointCompanySelect;
import SalePointWVR.pages.SalepointContractDataEntryPage;
import SalePointWVR.pages.SalepointContractFeesPage;
import SalePointWVR.pages.SalepointContractManagementPage;
import SalePointWVR.pages.SalepointContractPaymentPage;
import SalePointWVR.pages.SalepointContractPriceOverridePage;
import SalePointWVR.pages.SalepointContractPrintPage;
import SalePointWVR.pages.SalepointContractProcessingOwners;
import SalePointWVR.pages.SalepointContractSummaryPage;
import SalePointWVR.pages.SalepointDiscUpgradePage;
import SalePointWVR.pages.SalepointDiscoveryPackagePage;
import SalePointWVR.pages.SalepointEditContractPage;
import SalePointWVR.pages.SalepointEditUserInformation;
import SalePointWVR.pages.SalepointInventorySelectionPage;
import SalePointWVR.pages.SalepointLocateCustomerPage;
import SalePointWVR.pages.SalepointMiscellaneousInformationPage;
import SalePointWVR.pages.SalepointTradesPage;
import SalePointWVR.pages.WBWComissionsPage;
import SalePointWVR.pages.WBWContractManagementPage;
import SalePointWVR.pages.WBWDownPaymentsPage;
import SalePointWVR.pages.WBWFinanceInformationPage;
import SalePointWVR.pages.WBWOwnerInformationPage;
import SalePointWVR.pages.WBWOwnersListPage;
import SalePointWVR.pages.WBWPaymentOptionsPage;
import SalePointWVR.pages.WBWPurchaseInformationPage;
import SalePointWVR.pages.WBWSummaryPage;
import SalePointWVR.pages.WSAHomePage;
import SalePointWVR.pages.WSALoginPage;
import SalePointWVR.pages.WSANewProposalPage;
import SalePointWVR.pages.WSARetrieveTourPage;
import SalePointWVR.pages.WSATourLookupPage;
import automation.core.TestBase;

public class SalePointScripts extends TestBase {
	public static final Logger log = Logger.getLogger(SalePointScripts.class);

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc001_SP_WVRContractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
		SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			sp.launchApplication();
			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc011_SP_WVRContractCreation_Void(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);
		SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);
		
		
		try {
			sp.launchApplication();					
			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spDataEntry.clickNextButton();
			spPriceOver.clicknextButton();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.voidContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event:
	 * tc_031_SP_WVR_Discovery_Contaract_AutomationFlow Description: WVR
	 * Discovery Contaract Automation Flow
	 */

	@Test(dataProvider = "testData")
	public void tc_031_SP_WVR_Discovery_Contaract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		WSARetrieveTourPage wsaRetrieve = new WSARetrieveTourPage(tcconfig);

		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalePointWVR spWVR = new SalePointWVR(tcconfig);
		SalePointOwnerPage spOwnerPage = new SalePointOwnerPage(tcconfig);
		SalePointPaymentScreen spPayment = new SalePointPaymentScreen(tcconfig);
		SalepointContractSummaryPage spContract = new SalepointContractSummaryPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			wsaLogin.launchApplication();					
			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();
			spLogin.loginToSP("IE");	
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			try {
				spWVR.sp_WVR_ValidateAutoPopUp();
			} catch (Exception e) {
				tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.INFO,
						"Automation Pop Up message is not Displyaed for first Search");
				try {
					spWVR.sp_WVR_ValidateAutoPopUp();
				} catch (Exception e2) {
					tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.FAIL,
							"Automation Pop Up message is not Displyaed for Second Search");
				}
			}
			spOwnerPage.sp_WVR_edit_PrimaryOwner();
			spPayment.sp_WVR_edit_Payment();
			spContract.sp_Contract_WVR_Discovery_Save();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.voidContractAutomation();
			spLogin.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	* Name: Utsav Function/event: salepoint_tc_018_WVR_EditUDIContract
	* Description: TC018_Salepoint_WVR_Verify that the user is able to edit UDI manual contract
	*/
		
	@Test(dataProvider = "testData")
	public void salepoint_tc_018_WVR_EditUDIContract(Map<String, String> testData) throws Exception {
			setupTestData(testData);
			SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
			SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
			SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
			SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
			SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
			SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
			SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
			SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
			SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
			SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
			SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
			SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
			SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
			SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);
			SalePointHomePage spHomePage=new SalePointHomePage(tcconfig);
			SalepointEditContractPage spEditContractPage=new SalepointEditContractPage(tcconfig);

			try {
				sp.launchApplication();						
				sp.loginToApp();
				spCompanySelect.companySelect();
				spEditUser.Update_DefaultLocation();
				spLocateCustomer.navigateToMenu("");
				spLocateCustomer.provideCRSNumber();
				spContractOwners.contractProcessingOwnersNew();
				spDataEntry.contractProcessingDataEntrySelection();
				spDataEntry.clickNextButton();
				spInventory.contractProcessingInventrySelectionPhase();
				spFeesPage.contractFeesScreen();
				spPriceOver.clicknextButton();
				spPayment.contractProcessingPayment();
				spSalesPerson.selectSalesperson();
				spmiscellaneous.contrctProcessingMiscellaneousInformation();
				spContractSummaryPage.clicksave();
				spContractPrintPage.contractClickPrintButton();
				spContractPrintPage.contractProcessingContractPrinting();
				spHomePage.navigateToPage("EditContractURL");
				spEditContractPage.editContract();				
				sp.Logout();
				
			} catch (Exception e) {
				Assert.assertTrue(false,
						getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			}
			
			
	}
	
	
	@Test(dataProvider = "testData")
	public void tc010_WD_SaleRep_Edit_Assign_Site_Multiple_WVR(Map<String, String> testData) throws Exception {
				setupTestData(testData);

				SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
				SalePointAdminPage spAdminpage= new SalePointAdminPage(tcconfig);
				SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
				try {
					sp.launchApplication();						
					sp.loginToApp();
					spCompanySelect.companySelect();
					spAdminpage.Search_Salesman_id_Validation();
					spAdminpage.Select_Mutliple_Site_Assign();
					sp.Logout();

				} catch (Exception e) {
					Assert.assertTrue(false,
							getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
				}

			}

	}
		
		
	
