package nextgen.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import nextgen.pages.BuyACWTimeshare_IOS;
import nextgen.pages.BuyACWTimeshare_Web;
import nextgen.pages.CUILoginPage_Android;
import nextgen.pages.CUILoginPage_Web;
import nextgen.pages.CUIModifyTravelerPage_Android;
import nextgen.pages.CUIModifyTravelerPage_Web;
import nextgen.pages.CUIUpcomingVacationPage_Android;
import nextgen.pages.CUIUpcomingVacationPage_Web;
import nextgen.pages.HowToUseCWTimeshare_IOS;
import nextgen.pages.HowToUseCWTimeshare_Web;
import nextgen.pages.NextGenBuzzWorthyNewsPage_IOS;
import nextgen.pages.NextGenBuzzWorthyNewsPage_Web;
import nextgen.pages.NextGenDiffResortsVal_IOS;
import nextgen.pages.NextGenDiffResortsVal_Web;
import nextgen.pages.NextGenFAQPage_IOS;
import nextgen.pages.NextGenFAQPage_Web;
import nextgen.pages.NextGenFeaturedDestinations_IOS;
import nextgen.pages.NextGenFeaturedDestinations_Web;
import nextgen.pages.NextGenFooterPage_IOS;
import nextgen.pages.NextGenFooterPage_Web;
import nextgen.pages.NextGenHelpPage_IOS;
import nextgen.pages.NextGenHelpPage_Web;
import nextgen.pages.NextGenHomepage_Android;
import nextgen.pages.NextGenHomepage_IOS;
import nextgen.pages.NextGenHomepage_Web;
import nextgen.pages.NextGenLegalLinks_IOS;
import nextgen.pages.NextGenLegalLinks_Web;
import nextgen.pages.NextGenOwnerExclusivePage_IOS;
import nextgen.pages.NextGenOwnerExclusivePage_Web;
import nextgen.pages.NextGenOwnerGuidePage_IOS;
import nextgen.pages.NextGenOwnerGuidePage_Web;
import nextgen.pages.NextGenResortsNewsPage_IOS;
import nextgen.pages.NextGenResortsNewsPage_Web;
import nextgen.pages.NextGenResortsPage_IOS;
import nextgen.pages.NextGenResortsPage_Web;
import nextgen.pages.NextGenSearchResultPage_IOS;
import nextgen.pages.NextGenSearchResultPage_Web;
import nextgen.pages.NextGenSweepsPage_IOS;
import nextgen.pages.NextGenSweepsPage_Web;
import nextgen.pages.NextGenTravelInspiration_IOS;
import nextgen.pages.NextGenTravelInspiration_Web;
import nextgen.pages.NextGenTravelPage_IOS;
import nextgen.pages.NextGenTravelPage_Web;
import nextgen.pages.OwnerTestimonialPage_IOS;
import nextgen.pages.OwnerTestimonialPage_Web;
import nextgen.pages.WhyClubWyndhamPage_IOS;
import nextgen.pages.WhyClubWyndhamPage_Web;

public class NextGenScripts_MobileIOS extends TestBase {

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_001_NGCW_SearchFunctionalityVal � Description: Search validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_001_NGCW_SearchFunctionalityVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenSearchResultPage_Web searchResult = new NextGenSearchResultPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickHamburger();
			homepage.searchField();
			searchResult.searchResultsValidatins();
			/*
			 * homepage.resortSearch(); searchResult.resortSearchValidatins();
			 */

			/* homepage.navigateToHomepage("logo"); */

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_001_NGCW_SearchFunctionalityVal � Description: Search validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_002_NGCW_HomepageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenSearchResultPage_Web searchResult = new NextGenSearchResultPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			homepage.videoFunctionalityVal();
			homepage.videoFunctionalityHomePage();
			homepage.homepageHeroImageVal();
			homepage.homepageDealsNOffersVal();
			homepage.homepageDestinationGuidesVal();
			homepage.benefitOfOwnerShipVal();
			homepage.hpFeaturedResortVal();

			// homepage.hpIndulgeAustinVal(); content updated

			// homepage.travelInspirationVal(); content updated

			// homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_003_NGCW_HomepageFormVal � Description: FOrm validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_003_NGCW_HomepageFormVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();					
			homepage.formValidations();

			// //homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_004_NGCW_OwnerTestimonialVal � Description: Owner Testimonial Page Val
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_004_NGCW_OwnerTestimonialVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					
			whyClubWyn.navigateToOwnerTestimonial();
			whyClubWyn.ownerTestimonialValidations();
			whyClubWyn.ownerCardsSectionVal();
			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_005_NGCW_prideOfOwnershipVal � Description: Pride of ownership Page
	 * Val
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_005_NGCW_prideOfOwnershipVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			whyClubWyn.navToPrideOfOwnershipPage();
			whyClubWyn.prideOfOwnerValidations();
			whyClubWyn.peopleLikeYouVal();
			whyClubWyn.lifeTimeVacayVal();
			whyClubWyn.topNotchVal();
			whyClubWyn.truePeaceVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_006_NGCW_WhyClubWyndhamVal � Description: Why Club Wyndham Val
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_006_NGCW_WhyClubWyndhamVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickHamburger();
			whyClubWyn.navigateToWhyClubWyndhamPage();
			homepage.formValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_007_NGCW_TourTheClubVal � Description: Tour The Club Val
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_007_NGCW_TourTheClubVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			whyClubWyn.navigateToTourClub();
			whyClubWyn.tourTheclubVal();
			whyClubWyn.exploreBucketListVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_008_NGCW_CWTimeShareUseVal � Description: How To Use Club Wyndham
	 * Timeshare Val
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_008_NGCW_CWTimeShareUseVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_IOS(tcconfig);
		HowToUseCWTimeshare_Web cwTimeshare = new HowToUseCWTimeshare_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			whyClubWyn.navigateToWhyClubWyndhamPage();

			cwTimeshare.navigateToTimeSharePage();
			cwTimeshare.timesharePageValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_009_NGCW_BuyACWTimeShareVal � Description: Buying a club wyndham
	 * timeshare page nav and validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_009_NGCW_BuyACWTimeShareVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_IOS(tcconfig);
		BuyACWTimeshare_Web buyATimeshare = new BuyACWTimeshare_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			whyClubWyn.navigateToWhyClubWyndhamPage();
			buyATimeshare.navigateToBuyCWTimeshare();
			buyATimeshare.validateBuyCWTimeshare();
			buyATimeshare.slidingTabsValidations();
			buyATimeshare.twoContainerValidations();
			homepage.incorrectFormVal();
			homepage.formValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_010_NGCW_OwnerTestimonialArticlesVal � Description: OwnerTestimonials
	 * article validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_010_NGCW_OwnerTestimonialArticlesVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_IOS(tcconfig);
		OwnerTestimonialPage_Web ownerTestimonial = new OwnerTestimonialPage_IOS(tcconfig);

		try {
			String urlCheck = testData.get("URL");
			homepage.launchAppNextGen();
					

			whyClubWyn.navigateToOwnerTestimonial();
			whyClubWyn.ownerTestimonialValidations();
			whyClubWyn.ownerCardsSectionVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_011_NGCW_FeaturedResortsVal � Description: Featured Resorts Orlando
	 * article validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_011_NGCW_FeaturedResortsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			resortPage.featuredResortsNav();
			resortPage.searchResortLocation();
			resortPage.orlandoValidations();
			resortPage.validateExpandAll();
			resortPage.validateAmenities();

			// Content Update 5/28
			// resortPage.orlandoTheaterMuseum();
			// resortPage.orlandoOutdoors();
			// resortPage.orlandoThemePark();
			// resortPage.discoverOrlando();
			// content updated 1121
			// resortPage.ownerOffersOrlando();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_012_NGCW_FeaturedDestinationsPageVal � Description: Featured
	 * destinations page validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_012_NGCW_FeaturedDestinationsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_IOS(tcconfig);
		NextGenFeaturedDestinations_Web featDest = new NextGenFeaturedDestinations_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			resortPage.featuredResortsNav();
			featDest.featDestHeroVal();
			featDest.destinationGuidesVal();
			featDest.speakEasiesVal();
			featDest.travelInspVal();
			featDest.sunSandFamlyResort();
			featDest.dealsOffersVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_013_NGCW_AustinResortVal � Description: Featured Resorts Austin
	 * article validations
	 */

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_013_NGCW_AustinResortVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			resortPage.featuredResortsNav();
			resortPage.austinValidations();
			resortPage.austinDiscoverVal();
			resortPage.musicSceneVal();
			resortPage.cityGuideValidations();
			resortPage.discoverTexasVal();
			resortPage.dealsOffersVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_014_NGCW_FooterValidations � Description: Next gen Footer Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_014_NGCW_FooterValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenFooterPage_Web FooterPage = new NextGenFooterPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			FooterPage.footerValidations();
			FooterPage.footerBeInClubSubLinks();
			FooterPage.footerResortsSubLink();
			FooterPage.footerOwnerGdeVal();
			FooterPage.footerDealsSubLinks();
			FooterPage.footerHelpSubLinks();

			// //homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_015_NGCW_SocialLegalLinksVal � Description: Next gen Footer Social and
	 * Legal links Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_015_NGCW_SocialLegalLinksVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenFooterPage_Web FooterPage = new NextGenFooterPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			FooterPage.footerSocialIcons();
			FooterPage.footerLegalLinks();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_016_NGCW_HelpPageValidations � Description: Next gen help
	 * pageValidations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_016_NGCW_HelpPageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickHamburger();			
			helpPage.helpPageNavigation();
			homepage.formValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_017_NGCW_WynCaresValidations � Description: Next gen Wyndham Cares
	 * page Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_017_NGCW_WynCaresValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();					
			homepage.clickHamburger();
			helpPage.helpPageNavigation();
			helpPage.wyndhamCaresNav();
			helpPage.wynCaresPageFormVal();
			helpPage.needHelpSectionValidation();
			helpPage.wyndhamCaresSectionVal();
			helpPage.navigateToScambuster();
			helpPage.wynCaresPageFormVal();

			/*
			 * helpPage.wynCareFormSubmit();
			 * homepage.navigateToHomepage("logo");
			 */
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_018_NGCW_AboutUsValidations � Description: Next gen About Us page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_018_NGCW_AboutUsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			helpPage.aboutUsNav();
			helpPage.aboutUsContentVal();
			helpPage.aboutUsTwoConVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_019_NGCW_LegalLinksValidations � Description: Next gen Lega Links page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_019_NGCW_LegalLinksValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenLegalLinks_Web legalLinks = new NextGenLegalLinks_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
			legalLinks.termsOfUseNavigations();
			legalLinks.privacyNoticeValdns();
			legalLinks.privacySettingsValdns();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_020_NGCW_PublicationsValidations � Description: NextGen Publication
	 * Page Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch1"})

	public void TC_020_NGCW_PublicationsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			helpPage.publicationsPageNav();

			// homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("Firefox")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_021_NGCW_OwnerExclusiveVal � Description: NextGen Owner exclusive
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_021_NGCW_OwnerExclusiveVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerExclusivePage_Web ownerExc = new NextGenOwnerExclusivePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerExc.ownerExclusiveNav();
			ownerExc.ownerExPageVal();
			ownerExc.ownerPerksSectionval();
			ownerExc.clubWynStoreVal();
			// ownerExc.wynRewardsSecVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_022_NGCW_TravelDealsValidation � Description: NextGen Travel Deals
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_022_NGCW_TravelDealsValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenTravelPage_Web travelPage = new NextGenTravelPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			travelPage.travelDealsPageNav();
			travelPage.travelDealsPageVal();
			travelPage.travelCardsVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_023_NGCW_OwnersPageValidation � Description: NextGen Owner Page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_023_NGCW_OwnersPageValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenTravelPage_Web travelPage = new NextGenTravelPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			travelPage.ownerPageNav();
			travelPage.ownerPageVal();
			// travelPage.ownerCarouselVal();
			travelPage.ownerSlidingTabsVal();
			travelPage.twoContainerValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_024_NGCW_NonOwnersPageValidation � Description: NextGen Owner Page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_024_NGCW_NonOwnersPageValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenTravelPage_Web travelPage = new NextGenTravelPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			travelPage.nonOwnerPageNav();
			travelPage.nonOwnerPageVal();
			travelPage.nonOwnerCardsVal();
			travelPage.nonOwnerBannerVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_025_NGCW_VIPOffersVal � Description: NextGen VIP Now Offers
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_025_NGCW_VIPOffersVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerExclusivePage_Web ownerExc = new NextGenOwnerExclusivePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerExc.vipOfferspageNav();
			ownerExc.vipOffersPageVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_026_NGCW_OwnerGuidePageVal � Description: NextGen Owner Guide
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_026_NGCW_OwnerGuidePageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerGuide.ownerGuideNav();
			ownerGuide.ownerGuidePageVal();
			ownerGuide.youveGotPointsVal();
			ownerGuide.liveEduSessionVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_027_NGCW_NewOwnerStartVal � Description: NextGen VIP Now Offers
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_027_NGCW_NewOwnerStartVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerGuide.newOwnerQuickGuideNav();
			ownerGuide.newOwnerPageVal();
			ownerGuide.readySetVacVal();
			// ownerGuide.vacationMasterVal();
			ownerGuide.vacationStarterVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_028_NGCW_saveBigWithBenefitVal � Description: Save Big with these
	 * benefits Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_028_NGCW_saveBigWithBenefitVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerGuide.newOwnerQuickGuideNav();
			ownerGuide.saveBigWithBenefit();
			// ownerGuide.helpFulHintsValidations();
			ownerGuide.recommendedReadsVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_029_NGCW_TwotimeshareBasicsVal � Description: Two Timeshare Basics
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_029_NGCW_TwotimeshareBasicsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerGuide.newOwnerQuickGuideNav();
			ownerGuide.twoTimeShareBasicsVal();
			// ownerGuide.helpFulHintsValidations();
			ownerGuide.recommendedReadsVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_030_NGCW_StepsTooBookTimeshareVal � Description: Steps to book
	 * timeshare validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_030_NGCW_StepsTooBookTimeshareVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerGuide.digIntoClubLifeNav();
			ownerGuide.bookingYourDreamNav();
			// ownerGuide.helpFulHintsValidations();
			ownerGuide.recommendedReadsVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_031_NGCW_TipsToMaxPointsVal � Description: Reasons to Love Ownerships
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_031_NGCW_TipsToMaxPointsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			ownerGuide.newOwnerQuickGuideNav();
			ownerGuide.tipstoMaximizePointsVal();
			ownerGuide.recommendedReadsVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_032_NGCW_ResortNewsPageVal � Description: resort News Pages
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_032_NGCW_ResortNewsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_IOS(tcconfig);

		try {

			homepage.launchAppNextGen();
					

			resortNews.resortNewsPageNav();
			resortNews.resortNewsPageVal();
			resortNews.resortNewsCards();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_033_NGCW_URLValidations � Description: New URL redirects validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_033_NGCW_URLValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);

		try {

			homepage.urlNavigations(strBrowserInUse, Integer.parseInt(testData.get("totalLoop")));
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_034_NGCW_FAQPageValidations � Description: FAQ page validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_034_NGCW_FAQPageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenFAQPage_Web faqPage = new NextGenFAQPage_IOS(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			faqPage.faqPageNav();
			faqPage.faqPageVal();
			// faqPage.faqSearchVal();
			faqPage.faqAccordianVal();
			helpPage.navigateToGlossary();
			helpPage.glossaryPageValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_035_NGCW_TravelInspirationPage � Description: Travel inspiration page
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_035_NGCW_TravelInspirationPage(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenTravelInspiration_Web travelInsp = new NextGenTravelInspiration_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			travelInsp.travelInspiNav();
			travelInsp.travelInspiValidations();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_036_NGCW_HiddenTreasurePage � Description: Travel inspiration Hidden
	 * Treasure page validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_036_NGCW_HiddenTreasurePage(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenTravelInspiration_Web travelInsp = new NextGenTravelInspiration_IOS(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			travelInsp.hiddenTreasuresnav();
			travelInsp.hiddenTreasuresVal();
			ownerGuide.recommendedReadsVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_037_NGCW_RenovationPageVal � Description: Resort News Renovation page
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_037_NGCW_RenovationPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortNews.resortNewsPageNav();
			resortNews.renovationsNav();
			resortNews.renovationsVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_038_NGCW_BestInHawaiiPageVal � Description: Resort News Best In Hawaii
	 * page validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_038_NGCW_BestInHawaiiPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_IOS(tcconfig);
		try {
			homepage.launchAppNextGen();
					
			resortNews.bestInHawaiiNav();
			resortNews.bestInHawaiiVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_039_NGCW_BlueBeardsPageVal � Description: Resort News Bluebeards
	 * rebuilding page validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_039_NGCW_BlueBeardsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortNews.resortNewsPageNav();
			resortNews.bluebeardsNav();
			resortNews.bluebeardsVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_040_NGCW_ResortsPageVal � Description: Resort page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch2"})

	public void TC_040_NGCW_ResortsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsPage_Web resortPage = new NextGenResortsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortPage.navigateToResortsPage();
			resortPage.resortsaAndMapVal();

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_041_NGCW_kingsTownReefVal � Description: KingsTown Reef rebuilding
	 * page validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_041_NGCW_kingsTownReefVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortNews.resortNewsPageNav();
			resortNews.kingsTownReefNav();
			resortNews.kingsTownReefVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_042_NGCW_WebsiteWelcomeVal � Description: Website Welcome rebuilding
	 * page validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_042_NGCW_WebsiteWelcomeVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortNews.resortNewsPageNav();
			resortNews.websiteWelcomeNav();
			resortNews.websiteWelcomeVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_043_NGCW_DreamDiveExploreVal � Description:Dream Dive Explore page
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_043_NGCW_DreamDiveExploreVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortNews.resortNewsPageNav();
			resortNews.dreamDiveExploreNav();
			resortNews.dreamDiveExploreVal();

			homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Oct/2019 � � Version: 1 � function/event:
	 * TC_044_NGCW_SurfersparadiseResortVal � Description: Resort page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_044_NGCW_SurfersparadiseResortVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenResortsPage_Web resortPage = new NextGenResortsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortPage.navigateToResortsPage();
			resortPage.surfersParadiseVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Oct/2019 � � Version: 1 � function/event:
	 * TC_045_NGCW_NewOrleansResortVal � Description: New Orleans Resort page
	 * Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_045_NGCW_NewOrleansResortVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenFeaturedDestinations_Web featDestPage = new NextGenFeaturedDestinations_IOS(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			resortPage.featuredResortsNav();

			featDestPage.newOrleanspageVal();

			homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Nov/2019 � � Version: 1 � function/event:
	 * TC_046_NGCW_GlobalNavScrollVal � Description:Global Nav Scroll Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_046_NGCW_GlobalNavScrollVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);

		NextGenResortsPage_Web resortPage = new NextGenResortsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					
			homepage.globalNavScroll();
			resortPage.navigateToResortsPage();
			homepage.globalNavScroll();
			resortPage.resortPageScrollNav();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Dec/2019 � � Version: 1 � function/event:
	 * TC_047_NGCW_WyndhamSweepsVal � Description:Wyndham Sweeps Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_047_NGCW_WyndhamSweepsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);

		NextGenSweepsPage_Web sweepsPage = new NextGenSweepsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					

			sweepsPage.wyndhamSweepsNav();
			sweepsPage.wyndhamSweepsVal();
			sweepsPage.wyndhamSweepsNotAgreeVal();

			// homepage.navigateToHomepage("breadcrumb");

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Dec/2019 � � Version: 1 � function/event:
	 * TC_048_NGCW_GlossaryPageVal � Description:Glossary Page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_048_NGCW_GlossaryPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);

		NextGenHelpPage_Web helpPage = new NextGenHelpPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
					

			helpPage.navigateToGlossary();
			helpPage.glossaryPageValidations();

			homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Dec/2019 � � Version: 1 � function/event:
	 * TC_049_NGCW_ResortDetailsPageVal � Description:Resort Details Page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_049_NGCW_ResortDetailsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);

		NextGenResortsPage_Web resortsPage = new NextGenResortsPage_IOS(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickHamburger();
			resortsPage.navigateToResortsPage();
			resortsPage.navigateToResortDetails();
			resortsPage.resortDetailsVal();
			resortsPage.resortReviewsVal();
			resortsPage.resortformValidations();

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_050_NGCW_BuzzworthyNewsValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_IOS(tcconfig);
		NextGenBuzzWorthyNewsPage_Web buzzNewsPage = new NextGenBuzzWorthyNewsPage_IOS(tcconfig);
		// NextGenHelpPage helpPage = new NextGenHelpPage(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickHamburger();
			buzzNewsPage.buzzNewsPageNav();
			homepage.formValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups={"nextgen", "mobile", "ios", "batch3"})

	public void TC_071_NGCW_verifyUnableToBookForIneligibleMember(Map<String, String> testData) throws Exception {

		setupTestData( testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Android(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Android(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacations = new CUIUpcomingVacationPage_Android(tcconfig);
		CUIModifyTravelerPage_Web travelerPage = new CUIModifyTravelerPage_Android(tcconfig);

		try {

			homepage.launchAppNextGen();
					
			homepage.clickHamburger();
			homepage.clickSignInButtonMobile();

			login.setUserName();
			login.setPassword();
			login.loginCTAClick();

			// Unable to book Script//

			/*
			 * resortsPage.navigateToResortsPage();
			 * resortsPage.navigateToResortDetails();
			 * resortsPage.clickViewAvailability();
			 * 
			 * 
			 * login.setUserName(); login.setPassword();
			 * login.loginCTAClickPopup();
			 * 
			 * resortsPage.IneligibleMember("UNABLE TO BOOK");
			 * login.logOutApplication();
			 * 
			 * resortsPage.navigateToResortsPage();
			 * resortsPage.navigateToResortDetails();
			 * resortsPage.clickViewAvailability();
			 * 
			 * login.setAnotherUserName(); login.setAnotherPassword();
			 * login.loginCTAClickPopup();
			 * 
			 * resortsPage.validateCalendarNavigation();
			 */

			homepage.clickHamburger();
			upcomingVacations.clickToUpcomingVacations();

			upcomingVacations.verifyUpcomingReservationCardPresent();
			upcomingVacations.selectReservationToModify();

			travelerPage.clickModifyCTA();
			travelerPage.backToModifyReservation();

			login.logOutApplication();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

}