package nextgen.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import nextgen.pages.BuyACWTimeshare_Web;
import nextgen.pages.CUIAccountSettingPage_Web;
import nextgen.pages.CUIBookConfirmationPage_Web;
import nextgen.pages.CUIBookPage_Web;
import nextgen.pages.CUIBookingUpgradePage_Web;
import nextgen.pages.CUICancelReservation_Web;
import nextgen.pages.CUICompleteModificationPage_Web;
import nextgen.pages.CUILoginPage_Web;
import nextgen.pages.CUIModifyReservationPage_Web;
import nextgen.pages.CUIModifySuccessPage_Web;
import nextgen.pages.CUIModifyTravelerPage_Web;
import nextgen.pages.CUIMyAccountPage_Web;
import nextgen.pages.CUIMyOwnershipPage_Web;
import nextgen.pages.CUIMyOwnershipPayPalPage_Web;
import nextgen.pages.CUIPointProtectionPage_Web;
import nextgen.pages.CUIPointSummaryPage_Web;
import nextgen.pages.CUIRCIAndDepositPage_Web;
import nextgen.pages.CUIReservationBalancePage_Web;
import nextgen.pages.CUIReservationDetailsPage_Web;
import nextgen.pages.CUIResortSearchResultsPage_Web;
import nextgen.pages.CUISearchPage_Web;
import nextgen.pages.CUITravellerInformationPage_Web;
import nextgen.pages.CUITripPlannerExperiencePage_Web;
import nextgen.pages.CUITripPlannerLocationPage_Web;
import nextgen.pages.CUITripPlannerResultPage_Web;
import nextgen.pages.CUITripPlannerTravelSeasonPage_Web;
import nextgen.pages.CUITripPlannerTravelersPage_Web;
import nextgen.pages.CUITripPlannerYourBudgetPage_Web;
import nextgen.pages.CUIUpcomingVacationPage_Web;
import nextgen.pages.HowToUseCWTimeshare_Web;
import nextgen.pages.NextGenBucketListPage_Web;
import nextgen.pages.NextGenBuzzWorthyNewsPage_Web;
import nextgen.pages.NextGenDiffResortsVal_Web;
import nextgen.pages.NextGenFAQPage_Web;
import nextgen.pages.NextGenFeaturedDestinations_Web;
import nextgen.pages.NextGenFooterPage_Web;
import nextgen.pages.NextGenHelpPage_Web;
import nextgen.pages.NextGenHomepage_Web;
import nextgen.pages.NextGenLegalLinks_Web;
import nextgen.pages.NextGenOwnerExclusivePage_Web;
import nextgen.pages.NextGenOwnerGuidePage_Web;
import nextgen.pages.NextGenResortsNewsPage_Web;
import nextgen.pages.NextGenResortsPage_Web;
import nextgen.pages.NextGenSearchResultPage_Web;
import nextgen.pages.NextGenSweepsPage_Web;
import nextgen.pages.NextGenTravelInspiration_Web;
import nextgen.pages.NextGenTravelPage_Web;
import nextgen.pages.NextGenVIPExclusivePage_Web;
import nextgen.pages.OwnerTestimonialPage_Web;
import nextgen.pages.WhyClubWyndhamPage_Web;

public class NextGenScripts_Web extends TestBase {
	public static final Logger log = Logger.getLogger(NextGenScripts_Web.class);

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_001_NGCW_SearchFunctionalityVal � Description: Search validations
	 */

	@Test(dataProvider = "testData", groups = { "nextgen", "batch2" })

	public void TC_001_NGCW_SearchFunctionalityVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenSearchResultPage_Web searchResult = new NextGenSearchResultPage_Web(tcconfig);

		try {
			
			homepage.launchAppNextGen();
			homepage.searchField();
			searchResult.searchResultsValidatins();
			/*
			 * homepage.resortSearch(); searchResult.resortSearchValidatins();
			 */

			/* homepage.navigateToHomepage("logo"); */

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_001_NGCW_SearchFunctionalityVal � Description: Search validations
	 */

	@Test(dataProvider = "testData")

	public void TC_002_NGCW_HomepageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.homepageHeroImageVal();
			homepage.hpFeaturedResortVal();
			homepage.validateVacationReadyGo();
			homepage.homepageDealsNOffersVal();
			homepage.homepageDestinationGuidesVal();
			homepage.validateWelcomeTotheClubSection();
			homepage.videoFunctionalityHomePage();
			homepage.benefitOfOwnerShipVal();
			homepage.validateSuiteSavings();

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_003_NGCW_HomepageFormVal � Description: FOrm validations
	 */

	@Test(dataProvider = "testData")

	public void TC_003_NGCW_HomepageFormVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.headerValidations();
			//// homepage.incorrectFormVal();
			homepage.formValidations();

			// //homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_004_NGCW_OwnerTestimonialVal � Description: Owner Testimonial Page Val
	 */

	@Test(dataProvider = "testData", groups = { "search", "regression" })

	public void TC_004_NGCW_OwnerTestimonialVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			whyClubWyn.navigateToOwnerTestimonial();
			whyClubWyn.ownerTestimonialValidations();
			homepage.formValidations();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_005_NGCW_prideOfOwnershipVal � Description: Pride of ownership Page
	 * Val
	 */

	@Test(dataProvider = "testData", groups = { "search", "group1" })

	public void TC_005_NGCW_prideOfOwnershipVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			whyClubWyn.navToPrideOfOwnershipPage();
			whyClubWyn.prideOfOwnerValidations();
			whyClubWyn.peopleLikeYouVal();
			whyClubWyn.lifeTimeVacayVal();
			whyClubWyn.topNotchVal();
			whyClubWyn.truePeaceVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_006_NGCW_WhyClubWyndhamVal � Description: Why Club Wyndham Val
	 */

	@Test(dataProvider = "testData")

	public void TC_006_NGCW_WhyClubWyndhamVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			whyClubWyn.navigateToWhyClubWyndhamPage();
			whyClubWyn.whyClubWyndhamVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_007_NGCW_TourTheClubVal � Description: Tour The Club Val
	 */

	@Test(dataProvider = "testData")

	public void TC_007_NGCW_TourTheClubVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			whyClubWyn.navigateToTourClub();
			whyClubWyn.tourTheclubVal();
			whyClubWyn.exploreBucketListVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_008_NGCW_CWTimeShareUseVal � Description: How To Use Club Wyndham
	 * Timeshare Val
	 */

	@Test(dataProvider = "testData", groups = { "search", "group1" })

	public void TC_008_NGCW_CWTimeShareUseVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_Web(tcconfig);
		HowToUseCWTimeshare_Web cwTimeshare = new HowToUseCWTimeshare_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			whyClubWyn.navigateToWhyClubWyndhamPage();
			cwTimeshare.navigateToTimeSharePage();
			cwTimeshare.timesharePageValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_009_NGCW_BuyACWTimeShareVal � Description: Buying a club wyndham
	 * timeshare page nav and validations
	 */

	@Test(dataProvider = "testData")

	public void TC_009_NGCW_BuyACWTimeShareVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_Web(tcconfig);
		BuyACWTimeshare_Web buyATimeshare = new BuyACWTimeshare_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			whyClubWyn.navigateToWhyClubWyndhamPage();
			buyATimeshare.navigateToBuyCWTimeshare();
			buyATimeshare.validateBuyCWTimeshare();
			buyATimeshare.slidingTabsValidations();
			buyATimeshare.twoContainerValidations();
			homepage.formValidations();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_010_NGCW_OwnerTestimonialArticlesVal � Description: OwnerTestimonials
	 * article validations
	 */

	@Test(dataProvider = "testData")

	public void TC_010_NGCW_OwnerTestimonialArticlesVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		WhyClubWyndhamPage_Web whyClubWyn = new WhyClubWyndhamPage_Web(tcconfig);
		OwnerTestimonialPage_Web ownerTestimonial = new OwnerTestimonialPage_Web(tcconfig);

		try {
			String urlCheck = testData.get("URL");
			homepage.launchAppNextGen();
			whyClubWyn.navigateToOwnerTestimonial();
			whyClubWyn.ownerTestimonialValidations();
			whyClubWyn.ownerCardsSectionVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_011_NGCW_FeaturedResortsVal � Description: Featured Resorts Orlando
	 * article validations
	 */

	@Test(dataProvider = "testData")

	public void TC_011_NGCW_FeaturedResortsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			resortPage.featuredResortsNav();
			resortPage.searchResortLocation();
			resortPage.orlandoValidations();
			resortPage.validateExpandAll();
			resortPage.validateAmenities();

			// Content Update 5/28
			// resortPage.orlandoTheaterMuseum();
			// resortPage.orlandoOutdoors();
			// resortPage.orlandoThemePark();
			// resortPage.discoverOrlando();
			// content updated 1121
			// resortPage.ownerOffersOrlando();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_012_NGCW_FeaturedDestinationsPageVal � Description: Featured
	 * destinations page validations
	 */

	@Test(dataProvider = "testData")

	public void TC_012_NGCW_FeaturedDestinationsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_Web(tcconfig);
		NextGenFeaturedDestinations_Web featDest = new NextGenFeaturedDestinations_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			resortPage.featuredDestNav();
			featDest.featDestHeroVal();
			featDest.destinationGuidesVal();
			featDest.travelInspVal();
			featDest.sunSandFamlyResort();
			featDest.dealsOffersVal();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_013_NGCW_AustinResortVal � Description: Featured Resorts Austin
	 * article validations
	 */

	@Test(dataProvider = "testData")

	public void TC_013_NGCW_AustinResortVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			resortPage.featuredResortsNav();
			resortPage.austinValidations();
			resortPage.austinDiscoverVal();
			resortPage.musicSceneVal();
			resortPage.cityGuideValidations();
			resortPage.discoverTexasVal();
			resortPage.dealsOffersVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_014_NGCW_FooterValidations � Description: Next gen Footer Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_014_NGCW_FooterValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenFooterPage_Web FooterPage = new NextGenFooterPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			FooterPage.footerValidations();
			FooterPage.footerBeInClubSubLinks();
			FooterPage.footerResortsSubLink();
			FooterPage.footerOwnerGdeVal();
			FooterPage.footerDealsSubLinks();
			FooterPage.footerHelpSubLinks();

			// //homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_015_NGCW_SocialLegalLinksVal � Description: Next gen Footer Social and
	 * Legal links Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_015_NGCW_SocialLegalLinksVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenFooterPage_Web FooterPage = new NextGenFooterPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			FooterPage.footerSocialIcons();
			FooterPage.footerLegalLinks();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_016_NGCW_HelpPageValidations � Description: Next gen help
	 * pageValidations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_016_NGCW_HelpPageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			//
			helpPage.helpPageNavigation();
			/*
			 * helpPage.contactClubWyndhamVal(); helpPage.helpPageCardsVal();
			 * helpPage.clubWynOwnerVal();
			 */
			// homepage.incorrectFormVal();
			homepage.formValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_017_NGCW_WynCaresValidations � Description: Next gen Wyndham Cares
	 * page Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups = { "forms", "exclude" })

	public void TC_017_NGCW_WynCaresValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			helpPage.helpPageNavigation();
			helpPage.wyndhamCaresNav();
			helpPage.wynCaresPageFormVal();
			helpPage.needHelpSectionValidation();
			helpPage.wyndhamCaresSectionVal();
			helpPage.wynCaresContentBlkSection();
			helpPage.navigateToScambuster();
			helpPage.wynCaresPageFormVal();

			/*
			 * helpPage.wynCareFormSubmit();
			 * homepage.navigateToHomepage("logo");
			 */
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_018_NGCW_AboutUsValidations � Description: Next gen About Us page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_018_NGCW_AboutUsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			helpPage.aboutUsNav();
			helpPage.aboutUsContentVal();
			helpPage.aboutUsTwoConVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_019_NGCW_LegalLinksValidations � Description: Next gen Lega Links page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_019_NGCW_LegalLinksValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenLegalLinks_Web legalLinks = new NextGenLegalLinks_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			legalLinks.termsOfUseNavigations();
			legalLinks.privacyNoticeValdns();
			legalLinks.privacySettingsValdns();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_020_NGCW_PublicationsValidations � Description: NextGen Publication
	 * Page Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_020_NGCW_PublicationsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			helpPage.publicationsPageNav();

			// homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("Firefox")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_021_NGCW_OwnerExclusiveVal � Description: NextGen Owner exclusive
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_021_NGCW_OwnerExclusiveVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerExclusivePage_Web ownerExc = new NextGenOwnerExclusivePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			ownerExc.ownerExclusiveNav();
			ownerExc.ownerExPageVal();
			ownerExc.ownerPerksSectionval();
			ownerExc.clubWynStoreVal();
			// ownerExc.wynRewardsSecVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_022_NGCW_TravelDealsValidation � Description: NextGen Travel Deals
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_022_NGCW_TravelDealsValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenTravelPage_Web travelPage = new NextGenTravelPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			travelPage.travelDealsPageNav();
			travelPage.travelDealsPageVal();
			travelPage.travelCardsVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_023_NGCW_OwnersPageValidation � Description: NextGen Owner Page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_023_NGCW_OwnersPageValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenTravelPage_Web travelPage = new NextGenTravelPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			travelPage.ownerPageNav();
			// travelPage.ownerPageVal();
			// travelPage.ownerCarouselVal();
			travelPage.ownerSlidingTabsVal();
			travelPage.twoContainerValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_024_NGCW_NonOwnersPageValidation � Description: NextGen Owner Page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_024_NGCW_NonOwnersPageValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenTravelPage_Web travelPage = new NextGenTravelPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			travelPage.nonOwnerPageNav();
			travelPage.nonOwnerPageVal();
			travelPage.validateExtraHolidaysImage();
			travelPage.validateSuiteDealsSection();
			travelPage.nonOwnerCardsVal();
			travelPage.nonOwnerBannerVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_025_NGCW_VIPOffersVal � Description: NextGen VIP Now Offers
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_025_NGCW_VIPOffersVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerExclusivePage_Web ownerExc = new NextGenOwnerExclusivePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			ownerExc.vipOfferspageNav();
			ownerExc.vipOffersPageVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_026_NGCW_OwnerGuidePageVal � Description: NextGen Owner Guide
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_026_NGCW_OwnerGuidePageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			ownerGuide.ownerGuideNav();
			ownerGuide.ownerGuidePageVal();
			ownerGuide.youveGotPointsVal();
			ownerGuide.liveEduSessionVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_027_NGCW_NewOwnerStartVal � Description: NextGen VIP Now Offers
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_027_NGCW_NewOwnerStartVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			ownerGuide.newOwnerQuickGuideNav();
			ownerGuide.newOwnerPageVal();
			ownerGuide.readySetVacVal();
			// ownerGuide.vacationMasterVal();
			ownerGuide.vacationStarterVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_028_NGCW_saveBigWithBenefitVal � Description: Save Big with these
	 * benefits Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_028_NGCW_saveBigWithBenefitVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			ownerGuide.newOwnerQuickGuideNav();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_029_NGCW_TwotimeshareBasicsVal � Description: Two Timeshare Basics
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_029_NGCW_TwotimeshareBasicsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			ownerGuide.newOwnerQuickGuideNav();
			ownerGuide.twoTimeShareBasicsVal();
			// ownerGuide.helpFulHintsValidations();
			ownerGuide.recommendedReadsVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_030_NGCW_StepsTooBookTimeshareVal � Description: Steps to book
	 * timeshare validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_030_NGCW_StepsTooBookTimeshareVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			ownerGuide.digIntoClubLifeNav();
			/*ownerGuide.bookingYourDreamNav();
			ownerGuide.recommendedReadsVal();*/
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_031_NGCW_TipsToMaxPointsVal � Description: Reasons to Love Ownerships
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_031_NGCW_TipsToMaxPointsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenOwnerGuidePage_Web ownerGuide = new NextGenOwnerGuidePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			ownerGuide.newOwnerQuickGuideNav();
			/*ownerGuide.tipstoMaximizePointsVal();
			ownerGuide.recommendedReadsVal();*/
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_032_NGCW_ResortNewsPageVal � Description: resort News Pages
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_032_NGCW_ResortNewsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();

			resortNews.resortNewsPageNav();
			resortNews.resortNewsPageVal();
			// resortNews.resortNewsCards();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_033_NGCW_URLValidations � Description: New URL redirects validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_033_NGCW_URLValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);

		try {

			homepage.urlNavigations(strBrowserInUse, Integer.parseInt(testData.get("totalLoop")));
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Aug/2019 � � Version: 1 � function/event:
	 * TC_034_NGCW_FAQPageValidations � Description: FAQ page validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_034_NGCW_FAQPageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenFAQPage_Web faqPage = new NextGenFAQPage_Web(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			faqPage.faqPageNav();
			faqPage.faqPageVal();
			// faqPage.faqSearchVal();
			faqPage.faqAccordianVal();
			helpPage.navigateToGlossary();
			helpPage.glossaryPageValidations();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_035_NGCW_TravelInspirationPage � Description: Travel inspiration page
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_035_NGCW_TravelInspirationPage(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenTravelInspiration_Web travelInsp = new NextGenTravelInspiration_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			travelInsp.travelInspiNav();
			travelInsp.travelInspiValidations();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_036_NGCW_VIPExclusivePage � Description: Travel inspiration Hidden
	 * Treasure page validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_036_NGCW_VIPExclusivePage(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenVIPExclusivePage_Web vipExclusivePage = new NextGenVIPExclusivePage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			vipExclusivePage.vipExclusiveNav();
			vipExclusivePage.vipExclusivePageVal();
			vipExclusivePage.validateSeeAllDealsLink();
			vipExclusivePage.vipExclusivesSectionval();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_037_NGCW_RenovationPageVal � Description: Resort News Renovation page
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_037_NGCW_RenovationPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortNews.resortNewsPageNav();
			resortNews.renovationsNav();
			resortNews.renovationsVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_038_NGCW_BestInHawaiiPageVal � Description: Resort News Best In Hawaii
	 * page validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_038_NGCW_BestInHawaiiPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_Web(tcconfig);
		try {
			homepage.launchAppNextGen();
			resortNews.bestInHawaiiNav();
			resortNews.bestInHawaiiVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_039_NGCW_BlueBeardsPageVal � Description: Resort News Bluebeards
	 * rebuilding page validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_039_NGCW_BlueBeardsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortNews.resortNewsPageNav();
			resortNews.bluebeardsNav();
			resortNews.bluebeardsVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_040_NGCW_ResortsPageVal � Description: Resort page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_040_NGCW_ResortsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsPage_Web resortPage = new NextGenResortsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortPage.navigateToResortsPage();
			resortPage.resortsaAndMapVal();

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_041_NGCW_kingsTownReefVal � Description: KingsTown Reef rebuilding
	 * page validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_041_NGCW_kingsTownReefVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortNews.resortNewsPageNav();
			resortNews.kingsTownReefNav();
			resortNews.kingsTownReefVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_042_NGCW_WebsiteWelcomeVal � Description: Website Welcome rebuilding
	 * page validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_042_NGCW_WebsiteWelcomeVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortNews.resortNewsPageNav();
			resortNews.websiteWelcomeNav();
			resortNews.websiteWelcomeVal();

			// homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Sep/2019 � � Version: 1 � function/event:
	 * TC_043_NGCW_DreamDiveExploreVal � Description:Dream Dive Explore page
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_043_NGCW_DreamDiveExploreVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsNewsPage_Web resortNews = new NextGenResortsNewsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortNews.resortNewsPageNav();
			resortNews.dreamDiveExploreNav();
			resortNews.dreamDiveExploreVal();

			homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Oct/2019 � � Version: 1 � function/event:
	 * TC_044_NGCW_SurfersparadiseResortVal � Description: Resort page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_044_NGCW_SurfersparadiseResortVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsPage_Web resortPage = new NextGenResortsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortPage.navigateToResortsPage();
			resortPage.surfersParadiseVal();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Oct/2019 � � Version: 1 � function/event:
	 * TC_045_NGCW_NewOrleansResortVal � Description: New Orleans Resort page
	 * Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_045_NGCW_NewOrleansResortVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenFeaturedDestinations_Web featDestPage = new NextGenFeaturedDestinations_Web(tcconfig);
		NextGenDiffResortsVal_Web resortPage = new NextGenDiffResortsVal_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortPage.featuredResortsNav();

			featDestPage.newOrleanspageVal();

			homepage.navigateToHomepage("breadcrumb");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Nov/2019 � � Version: 1 � function/event:
	 * TC_046_NGCW_GlobalNavScrollVal � Description:Global Nav Scroll Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_046_NGCW_GlobalNavScrollVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenResortsPage_Web resortPage = new NextGenResortsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.globalNavScroll();
			resortPage.navigateToResortsPage();
			homepage.globalNavScroll();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Dec/2019 � � Version: 1 � function/event:
	 * TC_047_NGCW_WyndhamSweepsVal � Description:Wyndham Sweeps Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_047_NGCW_WyndhamSweepsVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);

		NextGenSweepsPage_Web sweepsPage = new NextGenSweepsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();

			sweepsPage.wyndhamSweepsNav();
			sweepsPage.wyndhamSweepsVal();
			sweepsPage.wyndhamSweepsNotAgreeVal();

			// homepage.navigateToHomepage("breadcrumb");

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Dec/2019 � � Version: 1 � function/event:
	 * TC_048_NGCW_GlossaryPageVal � Description:Glossary Page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_048_NGCW_GlossaryPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);

		NextGenHelpPage_Web helpPage = new NextGenHelpPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			helpPage.navigateToGlossary();
			helpPage.glossaryPageValidations();

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * � Name:Unnat Date:Dec/2019 � � Version: 1 � function/event:
	 * TC_049_NGCW_ResortDetailsPageVal � Description:Resort Details Page Val
	 * 
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_049_NGCW_ResortDetailsPageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);

		NextGenResortsPage_Web resortsPage = new NextGenResortsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			resortsPage.navigateToResortsPage();
			resortsPage.navigateToResortDetails();
			resortsPage.resortDetailsVal();
			resortsPage.resortRoomTypeVal();
			resortsPage.resortReviewsVal();

			// homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_050_NGCW_BuzzworthyNewsValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenBuzzWorthyNewsPage_Web buzzNewsPage = new NextGenBuzzWorthyNewsPage_Web(tcconfig);
		// NextGenHelpPage helpPage = new NextGenHelpPage(tcconfig);
		try {

			homepage.launchAppNextGen();
			buzzNewsPage.buzzNewsPageNav();
			buzzNewsPage.buzzNewsPageVal();
			// homepage.incorrectFormVal();
			homepage.formValidationsnew();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_051_NGCW_LazyLoaded_Resort_Search_Result(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIResortSearchResultsPage_Web resortSearch = new CUIResortSearchResultsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			resortSearch.validateNumberOfResortCard();
			resortSearch.validateResortDetailsPage();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			
		}

	}

	@Test(dataProvider = "testData")

	public void TC_052_NGCW_Bucket_List_Resort_With_Lazy_Loads_Validation(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUISearchPage_Web search = new CUISearchPage_Web(tcconfig);
		CUIResortSearchResultsPage_Web resortSearch = new CUIResortSearchResultsPage_Web(tcconfig);
		NextGenBucketListPage_Web bucketListPage = new NextGenBucketListPage_Web(tcconfig);
		// NextGenHelpPage helpPage = new NextGenHelpPage(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			bucketListPage.navigatetoMyBucketListPage();
			bucketListPage.bucketListDisplayValidation();
			bucketListPage.removeaResortCard();
			bucketListPage.bucketListDisplayValidation();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			
		}

	}

	@Test(dataProvider = "testData")

	public void TC_053_NGCW_Timeshare_URL_Validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		try {

			homepage.launchAppTimeshare();
			homepage.validateTimeshareLogo();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_054_NGCW_PanoRamaURL_Validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		try {

			homepage.launchAppTimeshare();
			homepage.validatePanoramacoLogo();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_055_NGCW_MGVCURL_Validation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		try {

			homepage.launchAppTimeshare();
			homepage.validateMGVCLogo();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "VIPUpgrade", "wip" })

	public void TC_056_NGCW_PlatinumVIP_Upgrades(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web search = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			search.verifyFirstResort();
			search.clickFirstAvailableUnit();
			search.clickBookButton();
			upgradePage.verifyFirstStep();
			upgradePage.verifyAndOptForFutureUpgrade();
			upgradePage.clickContinueButton();
			travelPage.clickContinueBooking();
			reservationBalancePage.selectCheckBox_ReservationBalanace("PurchaseReservationTransaction");
			reservationBalancePage.clickContinueBooking();
			pointProtectionPage.selectPointProtection();
			pointProtectionPage.clickContinueBooking();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookingConfPage.validateConfirmationHeader();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.clickModifyVIPUpgradesButton();
			upgradePage.modifyForFutureUpgradeDisclaimer();
			travelPage.clickContinueButton();
			modifyCompletePage.verifyVIPUpgradeChanges();
			modifySuccessPage.textVIPUpgradeConfirm();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "" })

	public void TC_057_NGCW_Points_SummaryPage_Transaction_Types(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web search = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);

		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		CUIPointSummaryPage_Web pointsSummaryPage = new CUIPointSummaryPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacations = new CUIUpcomingVacationPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();

			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			search.verifyFirstResort();
			search.clickFirstAvailableUnit();
			search.clickBookButton();
			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}
			travelPage.wrongGuestInfo();
			travelPage.wrongGuestErrorVal();
			travelPage.enterGuestInfo();
			travelPage.selectAgreeCheckBxGuestConfirmationCredits();
			travelPage.clickContinueBooking();
			bookPage.verifyCompleteBookingPage();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookingConfPage.validateConfirmationHeader();
			pointsSummaryPage.navigateToPointSummary();
			
			 /** pointsSummaryPage.pointSummaryPageSection();
			 * pointsSummaryPage.defaultTransactionHistory();
			 * pointsSummaryPage.selectUseYearTransaction(testData.get(
			 * "currentUseYear")); pointsSummaryPage.tableHeaderValidation();
			 * pointsSummaryPage.expanderValidations();
			 * pointsSummaryPage.validateTransactionTypes();*/
			 
			pointsSummaryPage.sortByDate();
			pointsSummaryPage.expanderValidations();
			pointsSummaryPage.validateLocationBooked();
			pointsSummaryPage.validateReservationNumber();
			upcomingVacations.navigateToUpcomingVacation();
			upcomingVacations.loadAllReservations();
			upcomingVacations.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			login.logOutApplicationViaDashboard();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	/*
	 * Method:
	 * tc_098_CUI_VerifyReservationConfirmationPageForVIPGoldMemberAndInstantUpgrade
	 * Description: Alternate Search with MAP when no exact and flex result
	 * present Date: Jun/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "date" })

	public void tc_058_NGCW_VerifyReservationConfirmation_Paypal_Payment(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web search = new CUISearchPage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);

		try {
			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			search.verifyFirstResort();
			search.clickFirstAvailableUnit();
			search.clickBookButton();

			upgradePage.verifyFirstStepOnPageLoad();
			if (testData.get("User_Type").equalsIgnoreCase("VIP")) {
				upgradePage.clickContinueButton();
			}

			travelPage.selectGuestOption();
			travelPage.verifyErrorGuestInfo();
			travelPage.enterGuestInfo();
			travelPage.selectAgreeCheckBxGuestConfirmationCredits();
			travelPage.clickContinueBooking();
			reservationBalancePage.validateReservationBalancePage();
			reservationBalancePage.verifyInstructionText("ReservationPoint");
			/*reservationBalancePage.selectCheckBox_ReservationBalanace("BorrowPoint");
			reservationBalancePage.borrowValidPoint();*/
			reservationBalancePage.selectCheckBox_ReservationBalanace("RentPoint");
			reservationBalancePage.rentValidPoint();
			reservationBalancePage.selectCheckBox_ReservationBalanace("PurchaseReservationTransaction");
			reservationBalancePage.verifyInstructionText("ReservationPoint");
			reservationBalancePage.clickContinueBooking();

			// pointProtectionPage.selectPointProtection();
			// pointProtectionPage.clickContinueBooking();

			bookPage.verifyCompleteBookingPage();
			//bookPage.validateBorrowedPointsValue();
			bookPage.validateRentedPointsIfSelected();
			bookPage.selectPaymentMethod();
			bookPage.paymentViaPaypal();
			bookPage.validateCancellationPolicyLink();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookingConfPage.validateConfirmationHeader();
			bookingConfPage.getReservationDates();
			bookingConfPage.validateReservationSummaryDetails();
			bookingConfPage.validateBookingDetails();
			bookingConfPage.validateChargesSummaryForRentingPoint();
			bookingConfPage.validatePaymentChargesForPaypalPayment();
			bookingConfPage.validateRentedPointUnderPaymentCharges();
			// bookingConfPage.validatePointProtectionUnderPaymentCharges("YES");
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.validateCheckInDate();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();

			login.logOutApplicationViaDashboard();

		} catch (Exception e) {
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			// loginPage.logoutAppliction();
		}
	}

	/*
	 * Method: tc_127_CUI_PaymentwithPayPal_OwnershipPage Description: Ownership
	 * Page Payment with Paypal Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	@Test(dataProvider = "testData", groups = { "tooltip" })

	public void tc_059_NGCW_PaymentwithPayPal_OwnershipPage_ContractBalance(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			//myaccountPage.validateToolTip();
			myOwnershipPage.navigateToMyOwnershipPage();
			//myOwnershipPage.validateToolTip();
			myOwnershipPage.financialDetailsHeader();
			/*myOwnershipPage.makePaymentHeader();*/
			myOwnershipPage.speedPayLinkPresence();
			myOwnershipPage.clickPayPalLink();
			ownershipPayPal.validateLinkBackToOwnership();
			ownershipPayPal.verifyHeaderPaymentType();
			ownershipPayPal.validatePaymentTypeTextAndField();
			ownershipPayPal.checkOptionsInPaymentTypeField();
			ownershipPayPal.selectPaymentType();
			ownershipPayPal.contractsCount();
			ownershipPayPal.verifyCurrentDueInContractCard();
			ownershipPayPal.verifyDueDateInContractCard();
			login.logOutApplicationViaDashboard();

		} catch (Exception e) {
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_105_CUI_NewOwnerTipsValidation Description: New owner (With or
	 * Without Points) owner Tips Validation Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	@Test(dataProvider = "testData")

	public void tc_060_NGCW_NewOwnerTipsValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myAccountPage = new CUIMyAccountPage_Web(tcconfig);

		try {
			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myAccountPage.newOwnerGetStartedTips();
			myAccountPage.newTipsTextValidation();
			myAccountPage.tipsLinksValidations();
			myAccountPage.tipsModalValidations();
			myAccountPage.newOwnerCompletedTips();
			myAccountPage.newTipsCompleteTextValidation();
			myAccountPage.tipCheckMarkedValidation();
			myAccountPage.allTipCompleteValidation();

			login.logOutApplicationViaDashboard();

		} catch (Exception e) {
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	/*
	 * Method: tc_061_NGCW_ValidateUnitDetails Description: Search Availablity
	 * Search Results Unit Details Window Date: Jan/2021 Author: Priya Das
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "UnitDetails", "UnitDetails" })
	public void tc_061_NGCW_ValidateUnitDetails(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.validateSearchResultResort();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			searchPage.clickFirstAvailableUnit();
			searchPage.getUnitTypeAndPrice();
			searchPage.verifyUnitDetails();
			searchPage.verifyRoomAmenities();
			searchPage.verifyUnitImageNumber();
			searchPage.validateFloorPlan();
			searchPage.validateVIPBenefitApply();
			//searchPage.verifyUnitDetailsCloseButton();
			loginPage.logOutApplicationViaDashboard();

		} catch (Exception e) {
			loginPage.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * � Name:Unnat Date:Jul/2019 � � Version: 1 � function/event:
	 * TC_020_NGCW_PublicationsValidations � Description: NextGen Publication
	 * Page Validations
	 * 
	 */
	@Test(dataProvider = "testData")

	public void TC_062_NGCW_PublicationsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		NextGenHelpPage_Web helpPage = new NextGenHelpPage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			helpPage.navigatetoPublicationPage();
			helpPage.validatePublicationFormfields();
			helpPage.validateFormError();
			helpPage.fillUpOrderDirectoryForm();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");

			if (strBrowserInUse.equalsIgnoreCase("Firefox")) {
				cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void tc_063_NGCW_PaymentwithPayPal_OwnershipPage_ContractBalance_Loan_Option_Availability(
			Map<String, String> testData) throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyOwnershipPage_Web myOwnershipPage = new CUIMyOwnershipPage_Web(tcconfig);
		CUIMyOwnershipPayPalPage_Web ownershipPayPal = new CUIMyOwnershipPayPalPage_Web(tcconfig);

		try {
			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myOwnershipPage.navigateToMyOwnershipPage();
			myOwnershipPage.financialDetailsHeader();
			/*myOwnershipPage.makePaymentHeader();*/
			myOwnershipPage.speedPayLinkPresence();
			myOwnershipPage.clickPayPalLink();
			ownershipPayPal.validateLinkBackToOwnership();
			ownershipPayPal.verifyHeaderPaymentType();
			ownershipPayPal.validatePaymentTypeOption();
			login.logOutApplicationViaDashboard();

		} catch (Exception e) {
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}
	}

	@Test(dataProvider = "testData")

	public void TC_064_NGCW_Resort_Search_Result_View_Monthly_Availability(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);

		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIResortSearchResultsPage_Web resortSearch = new CUIResortSearchResultsPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();

			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			resortSearch.clickFirstAvailableUnit();
			resortSearch.clickBookButton();
			resortSearch.navigateBackToResultSearchPage();
			resortSearch.validateExitBookingPopUp();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_065_NGCW_CWAndRCIPointsDepositConfirmation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIRCIAndDepositPage_Web depositRCIPage = new CUIRCIAndDepositPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			depositRCIPage.navigateToDepositandRCIPage();
			depositRCIPage.clickCTARCIPointDeposit();
			depositRCIPage.selectPointsAndContinueCTA();
			depositRCIPage.emailConfirmation();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_066_NGCW_DiscoveryPlusMemberContractValadition(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIMyOwnershipPage_Web membershipPage = new CUIMyOwnershipPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			membershipPage.navigateToMyOwnershipPage();
			membershipPage.verifyOwnerMembershipType();
			login.logOutApplicationViaDashboard();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_067_NGCW_forgetUsernamePasswordLinkValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.forgetUserNameClick();
			login.forgetUserNameLinkValadiation();
			login.navigateBack();
			login.forgetPasswordClick();
			login.forgetPasswordLinkValadiation();
			login.navigateBack();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_068_NGCW_pointOwnedLabelValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUIMyOwnershipPage_Web membershipPage = new CUIMyOwnershipPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			membershipPage.navigateToMyOwnershipPage();
			membershipPage.verifyDiscoveryPoints();
			membershipPage.userPointsOwnedValidations();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_069_NGCW_verifyTourAcknowledgementDiscovery(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.clickFirstAvailableUnit();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_070_NGCW_BrokenLinkValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		try {

			homepage.launchApplicationforBrokenLinkValidation();
			homepage.brokenLinkValidation();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData")

	public void TC_071_NGCW_verifyUnableToBookForIneligibleMember(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		NextGenResortsPage_Web resortsPage = new NextGenResortsPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacations = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web travelerPage = new CUIModifyTravelerPage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();

			login.setUserName();
			login.setPassword();
			login.loginCTAClick();

			// Unable to book Script//

			/*
			 * resortsPage.navigateToResortsPage();
			 * resortsPage.navigateToResortDetails();
			 * resortsPage.clickViewAvailability();
			 * 
			 * 
			 * login.setUserName(); login.setPassword();
			 * login.loginCTAClickPopup();
			 * 
			 * resortsPage.IneligibleMember("UNABLE TO BOOK");
			 * login.logOutApplication();
			 * 
			 * resortsPage.navigateToResortsPage();
			 * resortsPage.navigateToResortDetails();
			 * resortsPage.clickViewAvailability();
			 * 
			 * login.setAnotherUserName(); login.setAnotherPassword();
			 * login.loginCTAClickPopup();
			 * 
			 * resortsPage.validateCalendarNavigation();
			 */

			upcomingVacations.navigateToUpcomingVacationsPage();
			upcomingVacations.verifyUpcomingReservationCardPresent();
			upcomingVacations.selectReservationToModify();

			travelerPage.clickModifyCTA();
			travelerPage.backToModifyReservation();

			login.logOutApplication();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData")

	public void TC_072_NGCW_verifySingleOwnerCanModifyTraveler(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);
		NextGenResortsPage_Web resortsPage = new NextGenResortsPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingVacations = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyTravelerPage_Web travelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);

		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.clickFirstAvailableUnit();
			searchPage.clickBookButton();
			upgradePage.verifyFirstStep();
			upgradePage.clickContinueButton();
			travelPage.clickContinueBooking();
			/*reservationBalancePage.selectCheckBox_ReservationBalanace("PurchaseReservationTransaction");
			reservationBalancePage.clickContinueBooking();*/
			bookPage.selectPaymentMethod();
			bookPage.dataEntryForCreditCard();
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookingConfPage.validateConfirmationHeader();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			travelerPage.clickModifyCTA();
			travelerPage.verifyLabelModifyTraveler();
			travelerPage.selectOwnerfromList();
			travelerPage.clickContinueButton();
			travelerPage.clickConfirmChangesCTA();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			login.logOutApplicationViaDashboard();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "VIPUpgrade", "wip" })

	public void TC_074_NGCW_verifyVIPOptInUpgrades_Multiple(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web search = new CUISearchPage_Web(tcconfig);
		CUIBookingUpgradePage_Web upgradePage = new CUIBookingUpgradePage_Web(tcconfig);
		CUITravellerInformationPage_Web travelPage = new CUITravellerInformationPage_Web(tcconfig);
		CUIReservationBalancePage_Web reservationBalancePage = new CUIReservationBalancePage_Web(tcconfig);
		CUIPointProtectionPage_Web pointProtectionPage = new CUIPointProtectionPage_Web(tcconfig);
		CUIBookPage_Web bookPage = new CUIBookPage_Web(tcconfig);
		CUIBookConfirmationPage_Web bookingConfPage = new CUIBookConfirmationPage_Web(tcconfig);
		CUIUpcomingVacationPage_Web upcomingPage = new CUIUpcomingVacationPage_Web(tcconfig);
		CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcconfig);
		CUICompleteModificationPage_Web modifyCompletePage = new CUICompleteModificationPage_Web(tcconfig);
		CUIModifySuccessPage_Web modifySuccessPage = new CUIModifySuccessPage_Web(tcconfig);
		CUICancelReservation_Web cancelReservation = new CUICancelReservation_Web(tcconfig);
		CUIReservationDetailsPage_Web reservationDetails = new CUIReservationDetailsPage_Web(tcconfig);
		CUIModifyTravelerPage_Web travelerPage = new CUIModifyTravelerPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			search.verifyFirstResort();
			search.clickFirstAvailableUnit();
			search.clickBookButton();
			upgradePage.verifyFirstStep();
			upgradePage.verifyAndOptForFutureUpgradeMultipleOption();
			upgradePage.selectFirstUpgradeOption();
			upgradePage.clickContinueButton();
			travelPage.clickContinueBooking();
			reservationBalancePage.selectCheckBox_ReservationBalanace("PurchaseReservationTransaction");
			reservationBalancePage.clickContinueBooking();
			/*
			 * pointProtectionPage.selectPointProtection();
			 * pointProtectionPage.clickContinueBooking();
			 */
			bookPage.clickBookNowButton();
			bookPage.getReservationNumber();
			bookingConfPage.validateConfirmationHeader();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToModify();
			modifyPage.verifyHeaderModifyReservation();
			modifyPage.clickModifyVIPUpgradesButton();
			upgradePage.modifyForFutureUpgradeMultipleOption();
			upgradePage.selectFirstUpgradeOption();
			travelPage.clickContinueButton();
			modifyCompletePage.verifyVIPUpgradeChanges();
			modifySuccessPage.textVIPUpgradeConfirm();
			myaccountPage.navigateToDashBoardPage();
			upcomingPage.navigateToUpcomingVacation();
			upcomingPage.loadAllReservations();
			upcomingPage.selectReservationToCancel();
			cancelReservation.reservationNumberCheck();
			cancelReservation.membershipReimbursementSection();
			cancelReservation.cancelButtonClick();
			reservationDetails.cancelledReservationContentBox();
			reservationDetails.cancelledDateValidation();
			reservationDetails.reservationCancelledBy();
			login.logOutApplicationViaDashboard();

			// homepage.navigateToHomepage("logo");
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "Hyperlink", "hyperlink" })

	public void TC_075_NGCW_OwnerGuide_ResortAvailability_Hyperlink_Validation(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web search = new CUISearchPage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			login.setUserName();
			login.setPassword();
			login.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			myaccountPage.clickSearchAvailabilityButton();
			search.verifyFirstResort();
			search.clickExactResortName();
			search.validateOwnerGuideHyperlink();
			search.validateSearchAvailableResortLink();
			login.logOutApplicationViaDashboard();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "pc" })

	public void TC_076_NGCW_PointsCalculator_Appropriate_Resort_Result(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUITripPlannerYourBudgetPage_Web budget = new CUITripPlannerYourBudgetPage_Web(tcconfig);
		CUITripPlannerTravelersPage_Web group = new CUITripPlannerTravelersPage_Web(tcconfig);
		CUITripPlannerTravelSeasonPage_Web season = new CUITripPlannerTravelSeasonPage_Web(tcconfig);
		CUITripPlannerLocationPage_Web vacation = new CUITripPlannerLocationPage_Web(tcconfig);
		CUITripPlannerExperiencePage_Web experience = new CUITripPlannerExperiencePage_Web(tcconfig);
		CUITripPlannerResultPage_Web result = new CUITripPlannerResultPage_Web(tcconfig);

		try {

			homepage.launchAppPointsCalculator();
			login.setUserName();
			login.setPassword();
			login.loginCTAClickForTripPlaner();

			budget.validateLessPointsError();
			budget.enterPointsToSpend();
			budget.clickContinueCTA();

			group.selectGroupSize();
			group.clickContinueCTA();

			season.selectSeason();
			season.clickContinueCTA();

			vacation.selectVacationPlace();
			vacation.clickContinueCTA();

			experience.selectExperience();
			experience.clickContinueCTA();

			result.validateResultPage();
			result.validateSelection();
			result.verifyFirstResort();
			result.validateNewSearchCTA();

			login.logOutApplicationViaDashboard();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			//cleanUp();
		}

	}

	@Test(dataProvider = "testData", groups = { "pc" })

	public void TC_077_NGCW_PointsCalculator_No_Resort_Result(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web login = new CUILoginPage_Web(tcconfig);
		CUITripPlannerYourBudgetPage_Web budget = new CUITripPlannerYourBudgetPage_Web(tcconfig);
		CUITripPlannerTravelersPage_Web group = new CUITripPlannerTravelersPage_Web(tcconfig);
		CUITripPlannerTravelSeasonPage_Web season = new CUITripPlannerTravelSeasonPage_Web(tcconfig);
		CUITripPlannerLocationPage_Web vacation = new CUITripPlannerLocationPage_Web(tcconfig);
		CUITripPlannerExperiencePage_Web experience = new CUITripPlannerExperiencePage_Web(tcconfig);
		CUITripPlannerResultPage_Web result = new CUITripPlannerResultPage_Web(tcconfig);

		try {

			homepage.launchAppPointsCalculator();
			login.setUserName();
			login.setPassword();
			login.loginCTAClickForTripPlaner();

			// budget.validateLessPointsError();
			budget.enterPointsToSpend();
			budget.clickContinueCTA();

			group.selectGroupSize();
			group.clickContinueCTA();

			season.selectSeason();
			season.clickContinueCTA();

			vacation.selectVacationPlace();
			vacation.clickContinueCTA();

			experience.selectExperience();
			experience.selectSecondExperience();
			experience.clickContinueCTA();

			result.validateEditSelection();
			result.validateSelection();
			result.verifyFirstResort();
			result.validateNewSearchCTA();

			login.logOutApplicationViaDashboard();
			if (strBrowserInUse.equalsIgnoreCase("FF") || strBrowserInUse.equalsIgnoreCase("EDGE")) {
				cleanUp();
			}
		} catch (Exception e) {
			login.logOutApplicationViaDashboard();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
			//cleanUp();
		}

	}
	
	/*
	 * Method: tc_001_CUI_LoginPageValidations Description: CUI Login page
	 * Validations Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch1"})

	public void tc_078_CUI_LoginPageValidations(Map<String, String> testData) throws Exception {
		String dataUsed = testData.get("Data_Used");
		setupTestData(testData);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		try {

			homepage.launchAppNextGen();
			homepage.clickSignInButton();

			loginPage.acceptCookie();
			loginPage.globalHeaderPresence();
			loginPage.globalFooterPresence();
			loginPage.loginModalHeaders();
			loginPage.usernameFieldValidation();
			loginPage.passwordFieldValidation();
			loginPage.newAccountSection();
			loginPage.notAnOwnerSection();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();

			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}
	
	/*
	 * Method: tc_013_CUI_AccountSettingsPageValidation Description: CUI Account
	 * settings page Validations Date: Feb/2020 Author: Unnat Jain Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData")

	public void tc_079_CUI_AccountInformationChanges(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIAccountSettingPage_Web accountSettingPage = new CUIAccountSettingPage_Web(tcconfig);
		
		try {
			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			accountSettingPage.accountSettingsNavigation();
			accountSettingPage.personalInformationSection();
			accountSettingPage.personalInformationNameSection();
			accountSettingPage.personalInformationEmailSection();
			accountSettingPage.personalInformationAddressSection();
			accountSettingPage.personalInformationPhoneSection();
			accountSettingPage.ownerProfilePicture();
			accountSettingPage.imageUploadedvalidation();
			accountSettingPage.ownerUsernameValidation();
			accountSettingPage.usernameVerification();
			accountSettingPage.ownerSecurityQuestionsValidation();
			accountSettingPage.securityQuestion1Validation();
			accountSettingPage.securityQuestion2Validation();
			accountSettingPage.securityQuestion3Validation();
			accountSettingPage.ownerPasswordSection();
			accountSettingPage.passwordVerification("CUI_password", "Yes");
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}
	
	/*
	 * /* Method: tc_026_CUI_SearchFilterValidation Description: CUI Serach
	 * Filter Validations Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	@Test(dataProvider = "testData", groups = { "CUI", "regression", "batch2" })

	public void tc_080_CUI_SearchFilterValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		NextGenHomepage_Web homepage = new NextGenHomepage_Web(tcconfig);
		CUILoginPage_Web loginPage = new CUILoginPage_Web(tcconfig);
		CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcconfig);
		CUISearchPage_Web searchPage = new CUISearchPage_Web(tcconfig);

		try {
			homepage.launchAppNextGen();
			homepage.clickSignInButton();
			loginPage.acceptCookie();
			loginPage.setUserName();
			loginPage.setPassword();
			loginPage.loginCTAClick();
			myaccountPage.validateSearchHeader();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.clickSearchAvailabilityButton();
			searchPage.verifyFirstResort();
			searchPage.selectSort();
			searchPage.verifySortInAZResort();
			searchPage.verifyResortViewAvailabilityAndHideRates();
			loginPage.logOutApplicationViaDashboard();
		} catch (Exception e) {

			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

}