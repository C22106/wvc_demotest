package nextgen.pages;

import static org.testng.Assert.assertTrue;

import java.awt.Window;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUISearchPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUISearchPage_Web.class);

	public CUISearchPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcConfig);
	protected By headerAccessibleUnit = By.xpath("//h2[text() = 'Accessible Features']");
	protected By guaranteedAccessibleFeatures = By.xpath("//span[text() = 'Guaranteed Accessible Features']/../ul/li");
	protected By nonguaranteedAccessibleFeatures = By
			.xpath("//span[text() = 'Non-Guaranteed Accessible Features']/../ul/li");
	protected By checkoutdateInPriceBreakDownArea = By.xpath("(//span[text() = 'Checkout']/../span)[1]");
	protected By textCheckOutDaeInPriceBreakDownArea = By.xpath("//span[text() = 'Checkout']");
	protected By checkindateInPriceBreakDownArea = By
			.xpath("(//h2[text() = 'Price Breakdown']/..//li//span[contains(@class,'left')])[1]");
	protected By headerPriceBreakdown = By.xpath("//h2[text() = 'Price Breakdown']");
	protected By unitImageList = By.xpath("//img[@class = 'orbit-image']");
	protected By unitImageNumber = By.xpath("//p[@class = 'orbit-bullets']/button");
	protected By flexResultCheckboxNotSelected = By
			.xpath("//h1[contains(text(),'Flexible On Your Dates')]/..//div[@class = 'pill']");
	protected By holdTightloading = By.xpath("//p[text()='Hold tight, your resorts are loading...']");
	protected By nextMonthChange = By.xpath("//div[@aria-label = 'Move forward to switch to the next month.']");
	protected By feedbackSelectNo = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By checkAccessibleUnit = By
			.xpath("//div[@class='resort-card__options__content']//a[@data-open='unitDetails' and contains(.,'-')]");
	protected By availableUnit = By.xpath("//a[@data-open='unitDetails']");
	protected By showMapToggle = By.xpath("//h4[text() = 'SHOW MAP']/..//label[@for = 'map-toggle']");
	protected By map = By.xpath("//div[@class = 'availability-map ']");
	protected By textFilterHeader = By.xpath("//p[text() = 'FILTER & SORT']");
	protected By numberofResort = By.xpath("//h2[contains(text(),'Resorts')]");
	protected By buttonReset = By.xpath("//button[text() = 'Reset']");
	protected By sortAZ = By.xpath("//label[@for = 'sort-alphabetical-a-z']");
	protected By sortPopular = By.xpath("//label[@for = 'sort-popular']");
	protected By selectAccessibleUnit = By.xpath("//label[@for = 'include-accessible-units-checkbox']");
	protected By sortDistance = By.xpath("//label[@for = 'sort-distance']");
	protected By sortZA = By.xpath("//label[@for = 'sort-alphabetical-z-a']");
	protected By buttonFilterSelect = By.xpath("//button[contains(@class,'view-resorts')]");
	protected By filterByUnitType = By.xpath("//div[@id = 'filter-by-unit-type']");
	protected By SearchResult = By.xpath("//h2[contains(@class,'black padding')]");
	protected By buttonFilter = By.xpath("//button[text() = 'Filter & Sort']");
	protected By labelFilter = By.xpath("//p[@id='filter-and-sort-label']");
	protected By resortCard = By.xpath("//div[@class = 'resort-card']");
	protected By mapText = By.xpath("//h4[text() = 'SHOW MAP']");
	protected By objcheckFlexDateText = By.xpath("//h1[text() = 'Flexible Date Results']");
	protected By objFlexDateList = By.xpath("//div[contains(@id,'flex')]//div[@class = 'accordion-item']");
	protected By textResortName = By.xpath("//div[contains(@class,'resort-card')]//a");
	protected By ObjResortCardViewLabel = By
			.xpath("//div[contains(@class,'resort-card__content')]/following-sibling::div/a[contains(.,'Choose')]");
	protected By sortByArea = By.xpath("//a[text() = 'Sort By']");
	protected By unitTypeArea = By.xpath("//a[text() = 'Unit Type'] | //a[text() = 'Suite Type']");
	protected By experienceArea = By.xpath("//a[text()='Experiences']");
	protected By sortByCollapseCheck = By.xpath("//a[text() = 'Sort By']/../div");
	protected By textActualPriceList = By
			.xpath("//span[@class = 'resort-card__unit__discount']/span[contains(text(),'pts')]");
	protected By textDiscountPriceList = By.xpath("//span[@class = 'resort-card__unit__discount']/../strong");
	protected By resortCardContent = By.xpath("//div[@class = 'resort-card__options__content']");
	protected By textExtraDiscount = By
			.xpath("//span[@class = 'resort-card__unit__discount']/span[@class = 'savings']");
	protected By clearlocationtext = By.xpath("//button[@class = 'input-clear-button']");
	protected By locationChange = By.xpath("//input[@placeholder = 'Enter a location']");
	protected By textBoxDateSelect = By.xpath("//input[@id = 'searchDate']");
	protected By listCalendarMonthName = By
			.xpath("//div[contains(@class,'CalendarMonth') and @data-visible = 'true']//strong");
	protected By buttonCalendarClose = By.xpath("(//button[text() ='Close'] | //button[text() ='Done'])");
	protected By firstResort = By.xpath("//div[contains(@class,'resort-card')]//a"
			/*"//div[contains(@class,'resort-card')]//following::section//div[@class='resort-card__name']/a"*/);
	protected By hideResortElement = By
			.xpath("//div[contains(@class,'resort-card__content')]/div/a[contains(.,'Hide')]");
	protected By resortNotAbleToBook = By.xpath("//span[contains(text(),'not available for your membership type')]");
	protected By noFlexibleResultText = By
			.xpath("//h3[contains(.,'No Result')]/following-sibling::p[contains(.,'Flexible date')]");
	protected By noExactResultText = By
			.xpath("//h3[contains(.,'No Result')]/following-sibling::p[contains(.,'matching your search')]");
	protected By flexibleResultList = By.xpath("//h1[contains(.,'Flexible')]/following-sibling::div/div");
	protected By flexibleResultDate = By
			.xpath("//h1[contains(.,'Flexible')]/following-sibling::div/div//a[contains(@id,'accordion')]");
	protected By altarnateMessage = By.xpath("//h1[contains(text(),'Looking For Something Different')]");
	protected By searchLoadingImage = By.xpath("//div[@class='loading-state__content']");
	protected By textUnitDetails = By.xpath("//h2[text() = 'Unit Details'] | //h2[text() = 'Suite Details']");
	protected By unitDetailsTooltip = By.xpath("//span[@id = 'unit-details-tooltip']");
	protected By viewPlanCTA = By.xpath("//a[text() = 'View Floor Plan']");
	protected By textSize = By.xpath("//h3[text() = 'Size']");
	protected By textAccomadates = By.xpath("//h3[text() = 'Accommodates']");
	protected By textKitchen = By.xpath("//h3[text() = 'Kitchen']");
	protected By textBaths = By.xpath("//h3[text() = 'Baths']");
	protected By textBeds = By.xpath("//h3[text() = 'Beds']");
	protected By headerRoomAminities = By.xpath("//h2[text() = 'Room Amenities']");
	protected By textRoomAminities = By.xpath("//h2[text() = 'Room Amenities']/..//li");
	protected By headerFloorPlan = By.xpath("//h2[text() = 'Floor Plan']");
	protected By imageFloorPlan = By.xpath("//img[@alt = 'Floorplan image']");
	protected By unitNextButton = By.xpath("//a[contains(text(),'Next')]");
	protected By unitPrevoiusButton = By.xpath("//a[contains(text(),'Previous')]");
	protected By resortTotalUnit = By.xpath("//div[contains(@class,'resort-card__unit')]");
	protected By totalBookButton = By.xpath("//a[contains(text(),'Book')]");
	protected By textBookDetails = By.xpath("//span[text() = 'Reservation Details']");
	protected By unitCloseButton = By.xpath(
			"//p[text() = 'Unit Details']/..//button[@class='close-button'] | //p[text() = 'Suite Details']/..//button[@class='close-button']");
	protected By unitDetails = By.xpath("//p[text()='Unit Details'] | //p[text() = 'Suite Details']");
	protected By unitTypeText = By.xpath("//h2[contains(text(),'Bedroom')]");
	protected By amountText = By.xpath("//div[@class = 'unit-details reveal__content']/div/section/div/span");
	protected By objcheckFlexDateTextPill = By.xpath("//h1[text() = 'Flexible On Your Dates?']");
	protected By objFlexDateListPill = By.xpath("//div[contains(@class,'pill-container')]//div[@class = 'pill']");
	protected By flexibleResultListPill = By.xpath("//div[contains(@class,'pill-container')]//div[@class = 'pill']");
	protected By flexibleResultDatePill = By
			.xpath("//div[contains(@class,'pill-container')]//div[@class = 'pill']/a//span[@class='desktop']");
	protected By learnMoreToolTip = By.xpath("//div[contains(@class,'unit-details__section-vip')]//p//a[contains(.,'Learn More')]");

	// Monideep
	protected By accountPoints = By.className("account-info--points");
	protected By unitPriceObj = By.xpath("//div[@class='resort-card__options__content']/div//strong");
	protected By unitPriceObjNoDiscount = By
			.xpath("//div[contains(@class,'resort-card__unit')]/div/p[contains(text(),'pts')]");
	protected By textNearbyResortName = By
			.xpath(/*"//h2[text()='Nearby Resorts']/ancestor::section/following-sibling::div//h2/a"*/
					"//div[text()='Nearby Resorts']/ancestor::section/following-sibling::div[@class='resort-card']//div[@class='resort-card__name']/a");

	protected By SearchResultResort = By.xpath("//div[contains(text(),'Results for')]/following::div[contains(text(),'resort found')]");
	protected By resortCardExactMatch = By
			.xpath("//div[contains(text(),'resort found')]//following::section/div[@class='resort-card']");
	protected By NearbySearchResultResort = By.xpath("//div[contains(text(),'Nearby Resorts')]/following-sibling::div");

	// Kamalesh
	protected By maxReservationsExceededHeader = By
			.xpath("//div[contains(@class,'title') and text()='Max Reservations Exceeded']");
	protected By notEnoughPointsHeader = By.xpath("//div[contains(@class,'title') and text()='Not Enough Points']");
	protected By closeBackToAvailability = By.xpath("//button[@class='close-button']");
	protected By notEnoughPointsText = By.xpath("//p[contains(text(),'You do not have enough points')]");
	protected By maxReservationsExceededText = By
			.xpath("//p[contains(text(),'it looks like you have reached your max number')]");
	protected By BackToAvailabilityButton = By.xpath("//button[contains(text(),'Back to Availability')]");
	protected By warningSymbol = By.xpath("//*[@id='icon/alert/warning_24px']");
	protected By warningIcon = By.xpath("//*[@id='icon/alert/warning_24px']");
	protected By closeModalCTA = By.xpath("//div[@id='errorDetails']//button[@class='close-button']");;
	protected By backToAvailabilityBtn = By.xpath("//button[text()='Back to Availability']");
	protected By unableToBookMsg = By.xpath(
			"//p[contains(.,'This member number is unable to complete this transaction.') and contains(.,'Contact our Vacation Planning Center for assistance.') and contains(.,'1-866-921-5144')]");

	protected By VIPBenefitHeader = By.xpath("//span[contains(text(),'VIP Benefits apply to this suite.')]");
	protected By VIPBenefitSubDesc = By.xpath("//div[contains(@class,'unit-details__section-vip')]//p");

	/*
	 * Method: validateSearchResult Description:Validate Seacrh Result Date field
	 * Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateSearchResult() {
		waitUntilElementVisibleBy(driver, map, 120);
		Assert.assertTrue(verifyObjectDisplayed(SearchResult));
		String result = getObject(SearchResult).getText().split(" ")[0].trim();
		int resultFinal = Integer.parseInt(result);
		if (resultFinal > 0) {
			if (getList(resortCard).size() == resultFinal) {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.PASS,
						"Total number of Exact resort is: " + getList(resortCard).size());
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.FAIL,
						"Total Number of Resort Card and Search result found did not match");
			}

		}
	}

	/*
	 * Method: validateMoreThanTenResults Description:Validate Seacrh Result more
	 * than 10 Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void validateMoreThanTenResults() {
		waitUntilElementVisibleBy(driver, map, 120);
		Assert.assertTrue(verifyObjectDisplayed(SearchResult));
		String result = getObject(SearchResult).getText().split(" ")[0].trim();
		int resultFinal = Integer.parseInt(result);
		if (resultFinal > 10) {
			if (getList(resortCard).size() == resultFinal) {
				tcConfig.updateTestReporter("CUISearchPage", "validateMoreThanTenResults", Status.PASS,
						"Total number of Exact resort is: " + getList(resortCard).size());
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateMoreThanTenResults", Status.FAIL,
						"Resort number and the header count does not match");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateMoreThanTenResults", Status.FAIL,
					"Less than 10 results displayed, Count: " + resultFinal);
		}

	}

	/*
	 * Method: filterButtonClick Description:Click Filter Button Date field Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void filterButtonClick() {
		waitUntilElementVisibleBy(driver, buttonFilter, 120);
		if (verifyObjectDisplayed(buttonFilter)) {
			getElementInView(buttonFilter);
			clickElementJSWithWait(buttonFilter);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			Assert.assertTrue(verifyObjectDisplayed(labelFilter));

			tcConfig.updateTestReporter("CUISearchPage", "filterButtonClick", Status.PASS,
					"Successfully clicked on Filter button");

		}
	}

	/*
	 * Method: selectSort Description:Select Sort Validate Date: Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	protected By clickSort = By.xpath("//select[@name='select']");
	public void selectSort() {		
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		clickElementBy(clickSort);
		String sortBy = testData.get("SortBy").trim();
		if (sortBy.equals("Alphabetical (A-Z)")) {
			getElementInView(sortAZ);
			clickElementJSWithWait(sortAZ);
		} else if (sortBy.equals("Distance")) {
			getElementInView(sortDistance);
			clickElementJSWithWait(sortDistance);
		} else if (sortBy.equals("Alphabetical (Z-A)")) {
			getElementInView(sortZA);
			clickElementJSWithWait(sortZA);
		} else if (sortBy.equals("Popular")) {
			getElementInView(sortPopular);
			clickElementJSWithWait(sortPopular);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "selectSort", Status.FAIL,
					"Failed to click on sort filter! Check Data");
		}

	}

	/*
	 * Method: selectUnitType Description:Select Unit Type Validate Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void selectUnitType() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String UnitType = testData.get("UnitType").trim();
		int unit = 0;
		String unitTypePart1 = "//label[@for and contains(., '";
		String unitTypePart2 = "')]";
		String unitarr[] = UnitType.split(";");
		for (int unitIterator = 0; unitIterator < unitarr.length; unitIterator++) {
			WebElement click = getObject(By.xpath(unitTypePart1 + unitarr[unitIterator] + unitTypePart2));
			getElementInView(click);
			clickElementJSWithWait(click);
			tcConfig.updateTestReporter("CUISearchPage", "selectUnitType", Status.PASS,
					"Clicked on Unit Type and selected unit type is: " + unitarr[unitIterator]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			unit = unit + 1;
		}
		validateCheckBoxClicked(unit, unitarr, "Unit Type");
	}

	/*
	 * Method: selectExperience Description:Select Experience Validate Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectExperience() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		int exp = 0;
		String Experiences = testData.get("Experiences").trim();
		String exparr[] = Experiences.split(";");
		for (int experienceIterator = 0; experienceIterator < exparr.length; experienceIterator++) {
			WebElement clkexp = getObject(
					By.xpath("//label[@for and contains(.,'" + exparr[experienceIterator] + "')]"));
			getElementInView(clkexp);
			clickElementJSWithWait(clkexp);
			tcConfig.updateTestReporter("CUISearchPage", "selectExperience", Status.PASS,
					"Clicked on Experiences and selected experience is: " + exparr[experienceIterator]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			exp = exp + 1;

		}
		validateCheckBoxClicked(exp, exparr, "Experience");

	}

	/*
	 * Method: validateCheckBoxClicked Description: Validate Check Box Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateCheckBoxClicked(int totalClicked, String[] totalSelected, String validationFor) {

		if (totalClicked == totalSelected.length) {
			tcConfig.updateTestReporter("CUISearchPage", "selectExperience", Status.PASS,
					validationFor + " option clicked successfully");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "selectExperience", Status.FAIL,
					"Failed while clicking on " + validationFor + " option");
		}
	}

	/*
	 * Method: verifysortInAZ Description:Sort by A-Z Date: Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void verifySortInAZ() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> obtainedList = new ArrayList<>();
		for (WebElement resortName : getList(textResortName)) {
			obtainedList.add(resortName.getText());
		}
		ArrayList<String> sortedList = new ArrayList<>();
		for (String resortsToSort : obtainedList) {
			sortedList.add(resortsToSort);
		}

		Collections.sort(sortedList);
		try {
			Assert.assertTrue(sortedList.equals(obtainedList));
			tcConfig.updateTestReporter("CUISearchPage", "verifySortInAZ", Status.PASS,
					"Sorted in A-Z order" + sortedList);
		} catch (AssertionError error) {
			tcConfig.updateTestReporter("CUISearchPage", "verifysortInAZ", Status.FAIL,
					"Not sorted in A-Z order" + error.getMessage());
		}
	}

	/*
	 * Method: validateFilterHeader Description:Validate Filter Header text Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateFilterHeader() {
		waitUntilElementVisibleBy(driver, textFilterHeader, 120);
		if (verifyObjectDisplayed(textFilterHeader)) {
			tcConfig.updateTestReporter("CUISearchPage", "validateFilterHeader", Status.PASS,
					"Successfully validated Filter header");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateFilterHeader", Status.FAIL,
					"Filter header validation failed");
		}
	}

	/*
	 * Method: validateFilterHeader Description:Validate Filter Header text Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifySortInAZResort() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> obtainedList = new ArrayList<>();
		for (WebElement resortName : getList(textNearbyResortName)) {
			obtainedList.add(resortName.getText());
		}
		ArrayList<String> sortedList = new ArrayList<>();
		for (String resortsToSort : obtainedList) {
			sortedList.add(resortsToSort);
		}

		Collections.sort(sortedList);
		try {
			Assert.assertTrue(sortedList.equals(obtainedList));
			tcConfig.updateTestReporter("CUISearchPage", "verifySortInAZ", Status.PASS,
					"Sorted in A-Z order" + sortedList);
		} catch (AssertionError error) {
			tcConfig.updateTestReporter("CUISearchPage", "verifysortInAZ", Status.FAIL,
					"Not sorted in A-Z order" + error.getMessage());
		}
	}

	/*
	 * Method: filterButtonClickAfterSelection Description:Validate Click Filter
	 * Button Date:Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void filterButtonClickAfterSelection() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(buttonFilterSelect)) {
			clickElementBy(buttonFilterSelect);
			tcConfig.updateTestReporter("CUISearchPage", "filterButtonClickAfterSelection", Status.PASS,
					"Successfully clicked on filter button after selecting the filter options");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "filterButtonClickAfterSelection", Status.FAIL,
					"Failed while clicking on filter button after selecting the filter options");
		}
	}

	/*
	 * Method: verifyResortFound Description:Verify Resort Found Date:Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyResortFound() {
		waitUntilElementVisibleBy(driver, map, 120);
		if (verifyObjectDisplayed(numberofResort)) {
			tcConfig.updateTestReporter("CUISearchPage", "verifyResortFound", Status.PASS,
					"Numbers of resort found is: " + getObject(numberofResort).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyResortFound", Status.FAIL,
					"Resort Found Header not found");
		}
	}

	/*
	 * Method: verifysortInZAOnlyClubWyndhamResort Description:Verify sorting of
	 * Only Club Wyndham Resort in Z-A Order Date:Mar/2020 Author: Monideep Changes
	 * By: NA
	 */
	public void verifysortInZAOnlyClubWyndhamResort() {

		String Worldmarkpresent = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> obtainedList = new ArrayList<>();
		for (int resortNameIterator = 0; resortNameIterator < getList(textNearbyResortName)
				.size(); resortNameIterator++) {
			String resortname = getList(textNearbyResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				int lastClubWyndhamIndex = resortNameIterator - 1;
				for (int resortClubWyndhamIterator = 0; resortClubWyndhamIterator <= lastClubWyndhamIndex; resortClubWyndhamIterator++) {
					obtainedList.add(getList(textNearbyResortName).get(resortClubWyndhamIterator).getText());
					Worldmarkpresent = "Y";
				}
				break;
			}

		}

		if (!(Worldmarkpresent == "Y")) {
			for (WebElement we : getList(textNearbyResortName)) {
				obtainedList.add(we.getText());
			}
		}

		ArrayList<String> sortedList = new ArrayList<>();
		for (String s : obtainedList) {
			sortedList.add(s);
		}
		Collections.sort(sortedList);
		Collections.reverse(sortedList);
		try {
			Assert.assertTrue(sortedList.equals(obtainedList));
			tcConfig.updateTestReporter("CUISearchPage", "verifysortInAZ", Status.PASS,
					"Sorted in Z-A order" + sortedList);
		} catch (AssertionError error) {
			tcConfig.updateTestReporter("CUISearchPage", "verifysortInAZ", Status.FAIL,
					"Not sorted in Z-A order" + error.getMessage());
		}

	}

	/*
	 * Method: verifyFirstResort Description:Verify First Resort Found Date:Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyFirstResort() {
		waitUntilElementVisibleBy(driver, map, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(firstResort));
		String location = testData.get("CUI_Location").trim();
		String locationFirst = getObject(firstResort).getText().split("›")[0].trim();
		if (location.equalsIgnoreCase(locationFirst)) {
			tcConfig.updateTestReporter("CUISearchPage", "verifyFirstResort", Status.PASS,
					"First resort name matched with search result and the resort is : " + locationFirst);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "sverifyFirstResort", Status.FAIL,
					"First resort after searching does not match with the searched one");
		}
	}
	
	/*
	 * Method: verifyFirstResort Description:Verify First Resort Found Date:Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	
	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	protected By resortTitle = By.xpath("(//div[contains(@class,'title-1')])[1]");

	public void clickExactResortName() {
		String location = testData.get("CUI_Location").trim();
		String locationFirst = getObject(firstResort).getText().split("›")[0].trim();
		if (location.equalsIgnoreCase(locationFirst)) {
			clickElementBy(firstResort);
			waitUntilElementVisible(driver, driver.findElement(homeBreadcrumb), 120);

			WebElement homeBreadcrumbdb = driver.findElement(homeBreadcrumb);
			getElementInView(homeBreadcrumbdb);
			String strResortTitle = driver.findElement(resortTitle).getText().trim();
			Assert.assertEquals(strResortTitle, locationFirst);
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
					"Navigated to Resort Details Page " + strResortTitle);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "sverifyFirstResort", Status.FAIL,
					"First resort after searching does not match with the searched one");
		}
	}

	protected By ownerGuideLink = By
			.xpath("//div[contains(@class,'notification__body')]//a[contains(text(),'Owner Guide section.')]");
	protected By breadcrumbOwnerGuide = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Owner Guide')]");

	public void validateOwnerGuideHyperlink() {
		getElementInView(ownerGuideLink);
		String href = getElementAttribute(ownerGuideLink, "href");
		clickElementBy(ownerGuideLink);
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if (tabs.size() == 2) {
			driver.switchTo().window(tabs.get(1));
			waitUntilElementVisible(driver, driver.findElement(breadcrumbOwnerGuide), 120);
			assertTrue(driver.getCurrentUrl().trim().contains(href), "Page navigation not Successful");
			tcConfig.updateTestReporter("CUISearchPage", "validateOwnerGuideHyperlink()", Status.PASS,
					"Navigated to Resort Details Page");
			driver.close();
			driver.switchTo().window(tabs.get(0));
			waitUntilElementVisible(driver, driver.findElement(ownerGuideLink), 120);
		} else {
			waitUntilElementVisible(driver, driver.findElement(breadcrumbOwnerGuide), 120);
			assertTrue(driver.getCurrentUrl().trim().contains(href), "Page navigation not Successful");
			tcConfig.updateTestReporter("CUISearchPage", "validateOwnerGuideHyperlink()", Status.PASS,
					"Navigated to Resort Details Page");
			driver.navigate().back();
			pageCheck();
			waitUntilElementVisible(driver, driver.findElement(ownerGuideLink), 120);
		}

	}

	protected By searchAvailableResortBtn = By
			.xpath("//div[contains(@class,'notification__body')]//button[contains(text(),'Search Available Resorts')]");

	public void validateSearchAvailableResortLink() {
		String location = testData.get("CUI_Location").trim();
		getElementInView(searchAvailableResortBtn);
		clickElementBy(searchAvailableResortBtn);		
		waitUntilElementVisible(driver, driver.findElement(firstResort), 120);
		String locationFirst = getObject(firstResort).getText().split("›")[0].trim();
		assertTrue(location.equalsIgnoreCase(locationFirst), "Page navigation not Successful");
		tcConfig.updateTestReporter("CUISearchPage", "validateOwnerGuideHyperlink()", Status.PASS,
				"Navigated to Resort Search Page");

	}

	/*
	 * Method: verifyResortViewAvailabilityAndHideRates Description:Click View
	 * Availability and then Hide Rates in Resort Cards Date:Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void verifyResortViewAvailabilityAndHideRates() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		int totalClubWyndhamresort = 0;
		String hideandViewCTAPresence = null;
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("Wyndham")) {
				totalClubWyndhamresort = totalClubWyndhamresort + 1;
			}
		}
		if (getList(ObjResortCardViewLabel).size() > 0) {
			if (totalClubWyndhamresort == getList(ObjResortCardViewLabel).size()) {
				for (int resortViewLabelIterator = 0; resortViewLabelIterator < getList(ObjResortCardViewLabel)
						.size(); resortViewLabelIterator++) {
					clickResortCardViewLabel(resortViewLabelIterator);
					clickElementJSWithWait(hideResortElement);
					hideandViewCTAPresence = "Y";
				}
			} else {
				hideandViewCTAPresence = "N";
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyResortViewAvailabilityAndHideRates", Status.FAIL,
					"View and Hide Label is not present in any of the resort card");
		}
		if (hideandViewCTAPresence == "Y") {
			tcConfig.updateTestReporter("CUISearchPage", "verifyResortViewAvailabilityAndHideRates", Status.PASS,
					"View and Hide CTA present for each resort card");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyResortViewAvailabilityAndHideRates", Status.FAIL,
					"View and Hide CTA not present for each resort card");
		}
	}

	/*
	 * Method: checkExperienceSelectedPresent Description:Check Experience selected
	 * in Filter present in Resort card Date:Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void checkExperienceSelectedPresent() {

		String Experiences = testData.get("Experiences").trim();
		String exparr[] = Experiences.split(";");
		ArrayList<String> obtainedList = new ArrayList<>();
		for (int experienceSelected = 0; experienceSelected < exparr.length; experienceSelected++) {
			obtainedList.add(exparr[experienceSelected]);
		}
		validateExperiencePresent(obtainedList);
	}

	/*
	 * Method: validateExperiencePresent Description:Check Experience selected in
	 * Filter present in Resort card Date:Mar/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void validateExperiencePresent(ArrayList<String> experienceToCheckFrom) {
		int checkExperiencePresent = 0;
		for (int resortCardIterator = 1; resortCardIterator <= getList(resortCard).size(); resortCardIterator++) {
			List<WebElement> experienceList = getList(By.xpath("(//div[@class = 'resort-card'])[" + resortCardIterator
					+ "]//div[@class = 'resort-card__tags']/p"));
			for (int experienceSizeIterator = 1; experienceSizeIterator <= experienceList
					.size(); experienceSizeIterator++) {
				String experience = experienceList.get(experienceSizeIterator - 1).getText();
				if (experienceToCheckFrom.contains(experience)) {
					checkExperiencePresent = checkExperiencePresent + 1;
				} else {
					checkExperiencePresent = 0;
					break;
				}

			}
		}
		if (checkExperiencePresent > 0) {
			tcConfig.updateTestReporter("CUISearchPage", "validateExperiencePresent", Status.PASS,
					"Experience validated successfully");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateExperiencePresent", Status.FAIL,
					"Experience validation is unsuccessful");
		}
	}

	/*
	 * Method: resetFilter Description:Click Reset Filter Button Date:Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void resetFilter() {
		waitUntilElementVisibleBy(driver, buttonReset, 120);
		if (verifyObjectDisplayed(buttonReset)) {
			clickElementJSWithWait(buttonReset);
			tcConfig.updateTestReporter("CUISearchPage", "resetFilter", Status.PASS,
					"Successfully Clicked on Reset Button");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "resetFilter", Status.FAIL,
					"Failed while clicking on reset button");
		}
	}

	/*
	 * Method: verifysortInZA Description:Sort by Z-A Date: Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void verifysortInZAOnlyClubWyndham() {
		String Worldmarkpresent = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> obtainedList = new ArrayList<>();
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				int lastClubWyndhamIndex = resortNameIterator - 1;
				for (int resortClubWyndhamIterator = 0; resortClubWyndhamIterator <= lastClubWyndhamIndex; resortClubWyndhamIterator++) {
					obtainedList.add(getList(textResortName).get(resortClubWyndhamIterator).getText());
					Worldmarkpresent = "Y";
				}
				break;
			}

		}

		if (!(Worldmarkpresent == "Y")) {
			for (WebElement we : getList(textResortName)) {
				obtainedList.add(we.getText());
			}
		}

		ArrayList<String> sortedList = new ArrayList<>();
		for (String s : obtainedList) {
			sortedList.add(s);
		}
		Collections.sort(sortedList);
		Collections.reverse(sortedList);
		try {
			Assert.assertTrue(sortedList.equals(obtainedList));
			tcConfig.updateTestReporter("CUISearchPage", "verifysortInAZ", Status.PASS,
					"Sorted in Z-A order" + sortedList);
		} catch (AssertionError error) {
			tcConfig.updateTestReporter("CUISearchPage", "verifysortInAZ", Status.FAIL,
					"Not sorted in Z-A order" + error.getMessage());
		}
	}

	/*
	 * Method: worldmarkAddress Description:Check Worldmark Address present in
	 * Worldmark resort card Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void worldmarkAddress() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				int resortWorrldmarkPosition = resortNameIterator;
				for (int resortWorldmarkIterator = resortWorrldmarkPosition; resortWorldmarkIterator <= getList(
						textResortName).size() - 1; resortWorldmarkIterator++) {
					String resortNameWorldmark = getList(textResortName).get(resortWorldmarkIterator).getText().trim();
					WebElement resortadd = getObject(
							By.xpath("(//p[@class = 'resort-card__address'])[" + resortWorldmarkIterator + "]"));
					verifyWorldMarkAddress(resortadd, resortNameWorldmark);
				}
				break;
			}
		}
	}

	/*
	 * Method: verifyWorldMarkAddress Description:Check Worldmark Address present in
	 * Worldmark resort card Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyWorldMarkAddress(WebElement resortadd, String resortNameWorldmark) {
		if (verifyObjectDisplayed(resortadd)) {
			getElementInView(resortadd);
			tcConfig.updateTestReporter("CUISearchPage", "worldmarkAddress", Status.PASS,
					"Address for " + resortNameWorldmark + " is : " + resortadd.getText().trim());

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "worldmarkAddress", Status.FAIL,
					"Address for " + resortNameWorldmark + " validation failed");
		}
	}

	/*
	 * Method: WorldMarkImage Description:Check Worldmark Image present in Worldmark
	 * resort card Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void WorldMarkImage() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				int resortWorldmarkPosition = resortNameIterator;
				for (int resortWorldmarkIterator = resortWorldmarkPosition; resortWorldmarkIterator <= getList(
						textResortName).size() - 1; resortWorldmarkIterator++) {
					String resortnameworldmark = getList(textResortName).get(resortWorldmarkIterator).getText().trim();
					WebElement resortImage = getObject(By.xpath(
							"(//figure[@class = 'resort-card__image-container'])[" + resortWorldmarkIterator + "]"));
					if (verifyObjectDisplayed(resortImage)) {
						getElementInView(resortImage);
						tcConfig.updateTestReporter("CUISearchPage", "WorldMarkImage", Status.PASS,
								"Resort image present for resort: " + resortnameworldmark);

					} else {
						tcConfig.updateTestReporter("CUISearchPage", "WorldMarkImage", Status.FAIL,
								"Resort Image not present for resort: " + resortnameworldmark);
					}
				}
				break;
			}

		}
	}

	/*
	 * Method: WorldMarkContact Description:Check Worldmark Contract present in
	 * Worldmark resort card Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void WorldMarkContact() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				int resortWorldmarkPosition = resortNameIterator;
				for (int resortWorldmarkIterator = resortWorldmarkPosition; resortWorldmarkIterator <= getList(
						textResortName).size() - 1; resortWorldmarkIterator++) {
					List<WebElement> WorldMarkcontact = getList(
							By.xpath("//span[contains(text(),'To book Club Pass resorts')]"));
					for (int worldMarkContractIterator = 1; worldMarkContractIterator <= WorldMarkcontact
							.size(); worldMarkContractIterator++) {
						String resortNameWorldmark = getList(textResortName).get(resortWorldmarkIterator).getText()
								.trim();
						// WebElement resortContact = driver
						// .findElement(By.xpath("//span[contains(text(),'To
						// book Club Pass resorts')]["
						// + worldMarkContractIterator + "]"));
						WebElement resortContact = getList(
								By.xpath("//span[contains(text(),'To book Club Pass resorts')]"))
										.get(worldMarkContractIterator - 1);

						if (verifyObjectDisplayed(resortContact)) {
							getElementInView(resortContact);
							tcConfig.updateTestReporter("CUISearchPage", "WorldMarkContact", Status.PASS,
									"Contract for " + resortNameWorldmark + " is : " + resortContact.getText().trim());

						} else {
							tcConfig.updateTestReporter("CUISearchPage", "WorldMarkContact", Status.FAIL,
									"Contract for " + resortNameWorldmark + " validation failed");
						}
					}

				}
				break;
			}

		}
	}

	/*
	 * Method: checkFlexDateDisplayed Description:Check Flex Date Displayed Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkFlexDateDisplayed() {
		waitUntilElementVisibleBy(driver, objcheckFlexDateText, 120);
		if (verifyObjectDisplayed(objcheckFlexDateText)) {
			getElementInView(objcheckFlexDateText);
			if (getList(objFlexDateList).size() > 0) {
				tcConfig.updateTestReporter("CUISearchPage", "checkFlexDateDisplayed", Status.PASS,
						"Flex Date List Present");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkFlexDateDisplayed", Status.FAIL,
						"Flex Date List not Present");
			}
		}
	}

	/*
	 * Method: validateMapPresent Description:Check Map Displayed Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void validateMapPresent() {
		waitUntilElementVisibleBy(driver, map, 120);
		if (verifyObjectDisplayed(map)) {
			tcConfig.updateTestReporter("CUISearchPage", "validateMapPresent", Status.PASS,
					"Map Present in Search Page");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateMapPresent", Status.FAIL,
					"Map not Present in Search Page");
		}
	}

	/*
	 * Method: validateNumberOfResortCard Description:Validate total Number of
	 * Resort Card Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateNumberOfResortCard() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(resortCard).size() > 0) {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard", Status.PASS,
					"Number of Exact Resort is : " + getList(resortCard).size());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard", Status.FAIL,
					"Exact Number of Resort not found");
		}
	}

	/*
	 * Method: validateSearchResultResort Description:Validate Search Result Resort
	 * Date: Mar/2020 Author: Monideep Changes By: NA
	 */
	public void validateSearchResultResort() {

		waitUntilElementVisibleBy(driver, map, 120);
		Assert.assertTrue(verifyObjectDisplayed(SearchResultResort), "Exact search result not displayed");
		String result = getObject(SearchResultResort).getText().split(" ")[0].trim();
		int resultFinal = Integer.parseInt(result);
		if (resultFinal > 0) {
			if (getList(resortCardExactMatch).size() == resultFinal) {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.PASS,
						"Total number of Exact resort is: " + getList(resortCardExactMatch).size());
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.FAIL,
						"Total Number of Exact Resort Card and Search result found did not match");
			}

		}

		/*Assert.assertTrue(verifyObjectDisplayed(NearbySearchResultResort), "Nearby search result not displayed");
		int nearbyResortNum = Integer.parseInt(getObject(NearbySearchResultResort).getText().split(" ")[0].trim());
		if (nearbyResortNum > 0) {
			if ((getList(resortCard).size() - resultFinal) == nearbyResortNum) {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.PASS,
						"Total number of Exact resort is: " + (getList(resortCard).size() - 1));
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.FAIL,
						"Total Number of Nearby Resort Card and Nearby Search result found did not match");
			}

		}*/

	}

	/*
	 * Method: checkFlexDateNotDisplayed Description:Flex Date not displayed Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkFlexDateNotDisplayed() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		try {
			getElementInView(objcheckFlexDateText);
			if (getList(objFlexDateList).size() > 0) {
				tcConfig.updateTestReporter("CUISearchPage", "checkFlexDateNotDisplayed", Status.FAIL,
						"Flex Date List Present even flexible date check bon not selected");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkFlexDateNotDisplayed", Status.PASS,
						"Flex Date List not Present as flexible date checkbox is not selected");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISearchPage", "checkFlexDateNotDisplayed", Status.PASS,
					"Flex Date List not Present as flexible date checkbox is not selected");
		}

	}

	/*
	 * Method: checkSortAZOnlyWorldmark Description:Check Sorting AZ for WorldMark
	 * Resort only Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkSortAZOnlyWorldmark() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> obtainedList = new ArrayList<>();
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				int resortWorldmarkPosition = resortNameIterator;
				for (int resortWorldmarkIterator = resortWorldmarkPosition; resortWorldmarkIterator <= getList(
						textResortName).size() - 1; resortWorldmarkIterator++) {
					obtainedList.add(getList(textResortName).get(resortWorldmarkIterator).getText());
				}
				break;
			}
		}
		ArrayList<String> sortedList = new ArrayList<>();
		for (String resortToSort : obtainedList) {
			sortedList.add(resortToSort);
		}
		Collections.sort(sortedList);
		try {
			Assert.assertTrue(sortedList.equals(obtainedList));
			tcConfig.updateTestReporter("CUISearchPage", "checkSortAZOnlyWorldmark", Status.PASS,
					"WorldMark Resort are sorted in AZ" + sortedList);
		} catch (AssertionError error) {
			tcConfig.updateTestReporter("CUISearchPage", "checkSortAZOnlyWorldmark", Status.FAIL,
					"WorldMark Resort are not sorted in AZ" + error.getMessage());
		}

	}

	/*
	 * Method: checkSortByCollapse Description:Validate Check Sort By Area Collapse
	 * Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkSortByCollapse() {
		waitUntilElementVisibleBy(driver, sortByArea, 120);
		if (verifyObjectDisplayed(sortByArea)) {
			clickElementBy(sortByArea);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getObject(sortByCollapseCheck).getAttribute("aria-hidden").equalsIgnoreCase("True")) {
				tcConfig.updateTestReporter("CUISearchPage", "checkSortByCollapse", Status.PASS,
						"Sort By Area Collapsed");
				clickElementBy(sortByArea);
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkSortByCollapse", Status.FAIL,
						"Sort By Area did not collapse");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkSortByCollapse", Status.FAIL,
					"Sort By Area not present");
		}

	}

	/*
	 * Method: checkDiscountPrice Description: Validate Discount Price Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkDiscountMembershipPrice() {
		String memberType = testData.get("User_Type");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortViewLabelIterator = 0; resortViewLabelIterator < getList(ObjResortCardViewLabel)
					.size(); resortViewLabelIterator++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickResortCardViewLabel(resortViewLabelIterator);
				if (memberType.equalsIgnoreCase("Gold")) {
					validateMembershipDiscount(0.35);
				} else if (memberType.equalsIgnoreCase("Silver")) {
					validateMembershipDiscount(0.25);
				} else if (memberType.equalsIgnoreCase("Platinum")) {
					validateMembershipDiscount(0.50);
				} else {
					tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPrice", Status.FAIL,
							"Please provide valid Member type in Test Data Excel");
				}
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPrice", Status.FAIL, "Resort Card not present");
		}
	}

	/*
	 * Method: validateDiscount Description: Validate Discount Price calculated and
	 * from screen Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateMembershipDiscount(double value) {
		Assert.assertTrue(verifyObjectDisplayed(resortCardContent));
		for (int discountPriceIterator = 0; discountPriceIterator < getList(textDiscountPriceList)
				.size(); discountPriceIterator++) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			double actualPriceText = Integer
					.parseInt((getList(textActualPriceList).get(discountPriceIterator).getText().split(" "))[0].trim()
							.replace(",", ""));
			double discountPriceCalculated = (actualPriceText - (actualPriceText * value));
			double discountPriceFromScreen = Integer
					.parseInt(((getList(textDiscountPriceList).get(discountPriceIterator).getText().split(" "))[0])
							.trim().replace(",", ""));
			double discountPriceCalculatedFinal = Math.round(discountPriceCalculated);
			if (Double.compare(discountPriceCalculatedFinal, discountPriceFromScreen) == 0) {
				tcConfig.updateTestReporter("CUISearchPage", "validateDiscount", Status.PASS,
						"Discount Price validated Successfully and dicount price is: " + discountPriceFromScreen);
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateDiscount", Status.FAIL,
						"Discount Price calculated and from screen did not match");
			}
		}
		clickElementJSWithWait(hideResortElement);
	}
	/*
	 * Method: changeLocation Description:Select Location Date field Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */

	public void changeLocation() {
		String newlocation = testData.get("newLocation").trim();
		waitUntilElementVisibleBy(driver, map, 120);
		Assert.assertTrue(verifyObjectDisplayed(locationChange));
		clickElementBy(locationChange);
		clearElementBy(locationChange);
		fieldDataEnter(locationChange, newlocation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		sendKeysToElement(locationChange, Keys.ARROW_DOWN);
		sendKeysToElement(locationChange, Keys.ENTER);
		tcConfig.updateTestReporter("CUISearchPage", "changeLocation", Status.PASS,
				"Location changed successfully and Location selected is: " + newlocation);
	}

	/*
	 * Method: changeCheckinDate Description:Select New Check in date from calander
	 * Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void changeCheckinDate() {
		String changedCheckinDate = testData.get("changedCheckindate").trim();
		waitUntilElementVisibleBy(driver, map, 120);
		Assert.assertTrue(verifyObjectDisplayed(textBoxDateSelect));
		clickElementBy(textBoxDateSelect);
		tcConfig.updateTestReporter("CUISearchPage", "changeCheckinDate()", Status.PASS,
				"Successfully clicked on select date text box");
		String day = (changedCheckinDate.split(" ")[0]).trim();
		String month = (changedCheckinDate.split(" ")[1]).trim();
		String year = (changedCheckinDate.split(" ")[2]).trim();
		String monthyear = month.concat(" ").concat(year);
		myaccountPage.selectDate(monthyear, day, changedCheckinDate, "Changed Checkin");

	}

	/*
	 * Method: changeCheckoutDate Description:Select new Check out date from
	 * calander Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void changeCheckoutDate() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String changedCheckoutDate = testData.get("changedCheckoutdate").trim();
		String day = (changedCheckoutDate.split(" ")[0]).trim();
		String month = (changedCheckoutDate.split(" ")[1]).trim();
		String year = (changedCheckoutDate.split(" ")[2]).trim();
		String monthyear = month.concat(" ").concat(year);
		myaccountPage.selectDate(monthyear, day, changedCheckoutDate, "Changed Checkout");

	}

	/*
	 * Method: selectCalendarClose Description:Click Close button present in
	 * calendar Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCalendarClose() {
		waitUntilElementVisibleBy(driver, buttonCalendarClose, 120);
		if (verifyObjectDisplayed(buttonCalendarClose)) {
			clickElementJSWithWait(buttonCalendarClose);
			tcConfig.updateTestReporter("CUISearchPage", "selectCalendarClose", Status.PASS,
					"Successfully clicked on Close Calander Button");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "selectCalendarClose", Status.FAIL,
					"Failed while clicking on Close Calander Button");
		}
	}

	/*
	 * Method: includeAccessibleTypeSelect Description:Select Accessible unit
	 * checkbox Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void includeAccessibleTypeSelect() {
		waitUntilElementVisibleBy(driver, selectAccessibleUnit, 120);
		if (verifyObjectDisplayed(selectAccessibleUnit)) {
			getElementInView(selectAccessibleUnit);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(selectAccessibleUnit);
			tcConfig.updateTestReporter("CUISearchPage", "includeAccessibleTypeSelect", Status.PASS,
					"Successfully clicked on Accessible Unit CheckBox");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "includeAccessibleTypeSelect", Status.FAIL,
					"Failed while clicking on Accessible Unit CheckBox");
		}
	}

	/*
	 * Method: checkAccessibleUnit Description:Select Accessible unit checkbox Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkAccessibleUnit() {
		String checkAccessibleUnitPresence = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortViewIterator = 0; resortViewIterator < getList(ObjResortCardViewLabel)
					.size(); resortViewIterator++) {
				clickResortCardViewLabel(resortViewIterator);
				if (getList(checkAccessibleUnit).size() > 0) {
					checkAccessibleUnitPresence = "Y";
				}
				clickElementJSWithWait(hideResortElement);
			}
			if (checkAccessibleUnitPresence.equals("Y")) {
				tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnit", Status.PASS,
						"Accessible Unit present");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnit", Status.FAIL,
						"Accessible Unit not present for single resort");
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnit", Status.FAIL, "Resort Card not present");
		}

	}

	/*
	 * Method: checkUnitTypeCollapse Description:Validate Check Unit Type Area
	 * Collapse Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkUnitTypeCollapse() {
		waitUntilElementVisibleBy(driver, unitTypeArea, 120);
		if (verifyObjectDisplayed(unitTypeArea)) {
			getElementInView(unitTypeArea);
			clickElementJSWithWait(unitTypeArea);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getObject(unitTypeArea).getAttribute("aria-expanded").equalsIgnoreCase("False")) {
				tcConfig.updateTestReporter("CUISearchPage", "checkUnitTypeCollapse", Status.PASS,
						"Unit Type Area Collapsed");
				clickElementJSWithWait(unitTypeArea);
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkUnitTypeCollapse", Status.FAIL,
						"Unit Type Area did not collapse");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkUnitTypeCollapse", Status.FAIL,
					"Unit Type Area not present");
		}
	}

	/*
	 * Method: checkAccessibleUnitNotPresent Description:Validate Check Unit Type
	 * Area Collapse Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkAccessibleUnitNotPresent() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortViewLabelIterator = 0; resortViewLabelIterator < getList(ObjResortCardViewLabel)
					.size(); resortViewLabelIterator++) {
				clickResortCardViewLabel(resortViewLabelIterator);
				if (getList(checkAccessibleUnit).size() > 0) {
					tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnitNotPresent", Status.FAIL,
							"Accessible Unit present even if Accessible checkbox is not selected and Accessible units are: "
									+ checkAccessibleUnit);
				} else {
					tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnitNotPresent", Status.PASS,
							"Accessible Unit not present as Accessible checkbox is not selected for resort: "
									+ getList(textResortName).get(resortViewLabelIterator).getText().trim());
				}
				clickElementJSWithWait(hideResortElement);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnitNotPresent", Status.FAIL,
					"Resort Card not present");
		}
	}

	/*
	 * Method: clickResortCardViewLabel Description:Click View Label in Resort Card
	 * Type Area Collapse Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickResortCardViewLabel(int viewLabelIterator) {
		WebElement viewclk = getList(ObjResortCardViewLabel).get(viewLabelIterator);
		getElementInView(viewclk);
		clickElementJSWithWait(viewclk);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method: resortNotAbleToBook Description:Validate Check Unit Type Area
	 * Collapse Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void resortNotAbleToBook() {
		waitUntilElementVisibleBy(driver, map, 120);
		Assert.assertTrue(verifyObjectDisplayed(resortNotAbleToBook));
		tcConfig.updateTestReporter("CUISearchPage", "resortNotAbleToBook", Status.PASS,
				"Text Present as: " + getObject(resortNotAbleToBook).getText());
	}

	/*
	 * Method: resortSearchedNotFoundValidation Description:Validate Check Unit Type
	 * Area Collapse Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void resortSearchedNotFoundValidation() {
		String location = testData.get("resortNotToBeFound").trim();
		waitUntilElementVisibleBy(driver, map, 120);
		ArrayList<String> listResortName = new ArrayList<>();
		for (WebElement resortNameIterator : getList(textResortName)) {
			listResortName.add(resortNameIterator.getText());
		}
		if (listResortName.contains(location)) {
			tcConfig.updateTestReporter("CUISearchPage", "resortSearchedNotFoundValidation", Status.FAIL,
					"Resort: " + location + " Present in Resort List: " + listResortName);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "resortSearchedNotFoundValidation", Status.PASS,
					"Resort Searched: " + location + " not present in Resort List: " + listResortName);
		}
	}

	/*
	 * Method: checkInventoryDiscount Description: Validate Inventory Discount Price
	 * Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void checkInventoryDiscount() {
		int inventoryDiscount = Integer.parseInt(testData.get("DiscountInventory").trim());
		Assert.assertTrue(verifyObjectDisplayed(resortCardContent));
		for (int discountPriceIterator = 0; discountPriceIterator < getList(textDiscountPriceList)
				.size(); discountPriceIterator++) {
			WebElement textDiscountPercent = getObject(
					By.xpath("(//span[@class = 'resort-card__unit__discount']/../strong)[" + (discountPriceIterator + 1)
							+ "]/..//span[contains(@class,'savings')]"));
			String extraDiscount = textDiscountPercent.getText().split(" ")[1].trim();
			int extraDiscountFinal = Integer.parseInt(extraDiscount.split("%")[0].trim());
			if (inventoryDiscount == extraDiscountFinal) {
				double actualPriceText = Integer
						.parseInt((getList(textActualPriceList).get(discountPriceIterator).getText().split(" "))[0]
								.trim().replace(",", ""));
				double discountPrice = 100 - extraDiscountFinal;
				double discountPriceCalculated = (discountPrice * actualPriceText) / 100;
				double discountPriceFromScreen = Integer
						.parseInt(((getList(textDiscountPriceList).get(discountPriceIterator).getText().split(" "))[0])
								.trim().replace(",", ""));
				double discountPriceCalculatedFinal = Math.round(discountPriceCalculated);
				if (Double.compare(discountPriceCalculatedFinal, discountPriceFromScreen) == 0) {
					tcConfig.updateTestReporter("CUISearchPage", "validateDiscount", Status.PASS,
							"Discount Price validated Successfully and dicount price is: " + discountPriceFromScreen);
				} else {
					tcConfig.updateTestReporter("CUISearchPage", "validateDiscount", Status.FAIL,
							"Discount Price calculated and from screen did not match");
				}
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateDiscount", Status.FAIL,
						"Discount percentage from inventory did not match with what is passed in excel");
			}

		}
	}

	/*
	 * Method: checkDiscountPrice Description: Validate Discount Price Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void inventoryDiscountPriceValidation() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int viewLabelIterator = 0; viewLabelIterator < getList(ObjResortCardViewLabel)
					.size(); viewLabelIterator++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickResortCardViewLabel(viewLabelIterator);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				checkInventoryDiscount();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(hideResortElement);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPricePlatinum", Status.FAIL,
					"Resort Card not present");
		}
	}

	/*
	 * Method: checkDiscountPriceResort Description: Validate Discount Price Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void checkDiscountPriceResort() {
		String presenceResort = null;
		String memberType = testData.get("User_Type").trim();
		String location = testData.get("CUI_Location").trim();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortNameIterator = 0; resortNameIterator < getList(textResortName)
					.size(); resortNameIterator++) {
				String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
				if (resortname.equals(location)) {
					clickViewResort(resortNameIterator);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (memberType.equalsIgnoreCase("Gold")) {
						validateMembershipDiscount(0.35);
						presenceResort = "Y";
						break;
					} else if (memberType.equalsIgnoreCase("Silver")) {
						validateMembershipDiscount(0.25);
						presenceResort = "Y";
						break;
					} else if (memberType.equalsIgnoreCase("Platinum")) {
						validateMembershipDiscount(0.50);
						presenceResort = "Y";
						break;
					} else {
						tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPrice", Status.FAIL,
								"Please provide valid Member type in Test Data Excel");
					}
				}
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPrice", Status.FAIL, "Resort Card not present");
		}
		if (presenceResort == "N") {
			tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPriceResort", Status.FAIL,
					"Resort name not present in list");
		}
	}

	/*
	 * Method: discountNotPresentResort Description: Validate Discount Price Date:
	 * Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateDiscountNotPresentWithNoInventory() {
		String presenceResort = null;
		String location = testData.get("CUI_Location").trim();
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortNameIterator = 0; resortNameIterator < getList(textResortName)
					.size(); resortNameIterator++) {
				String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
				if (resortname.equals(location)) {
					clickViewResort(resortNameIterator);
					checkDiscountListNotPresentResort(location);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(hideResortElement);
					presenceResort = "Y";
					break;
				}
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateDiscountNotPresentWithNoInventory", Status.FAIL,
					"Resort Card not present");
		}
		if (presenceResort == "N") {
			tcConfig.updateTestReporter("CUISearchPage", "validateDiscountNotPresentWithNoInventory", Status.FAIL,
					"Resort name not present in list");
		}
	}

	/*
	 * Method: discountNotPresentResort Description: Validate Discount Price Date:
	 * Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void validateDiscountPresenceWithInventory() {
		String presenceResort = null;
		String location = testData.get("CUI_Location").trim();
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortNameIterator = 0; resortNameIterator < getList(textResortName)
					.size(); resortNameIterator++) {
				String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
				if (resortname.equals(location)) {
					clickViewResort(resortNameIterator);
					checkInventoryDiscount();
					clickElementJSWithWait(hideResortElement);
					presenceResort = "Y";
					break;
				}

			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateDiscountPresenceWithInventory", Status.FAIL,
					"Resort Card not present");
		}
		if (presenceResort == "N") {
			tcConfig.updateTestReporter("CUISearchPage", "validateDiscountPresenceWithInventory", Status.FAIL,
					"Resort name not present in list");
		}
	}

	/*
	 * Method: clickviewResort Description: Validate Discount Price Date: Mar/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void clickViewResort(int count) {
		WebElement viewclk = getList(ObjResortCardViewLabel).get(count);
		getElementInView(viewclk);
		clickElementJSWithWait(viewclk);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method: checkDiscountListNotPresentResort Description: Validate Discount
	 * Price Date: Apr/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkDiscountListNotPresentResort(String Location) {
		if (getList(textDiscountPriceList).size() > 0) {
			tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPriceResort", Status.FAIL,
					"Discount Price Present");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkDiscountPriceResort", Status.PASS,
					"Discount Price not Present for resort: " + Location);
		}
	}

	/*
	 * Method: exactResultNotDisplayed Description:Validate Exact Result not
	 * displayed Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void exactResultNotDisplayed() {
		waitUntilElementVisibleBy(driver, noExactResultText, 120);
		if (verifyObjectDisplayed(noExactResultText)) {
			tcConfig.updateTestReporter("CUISearchPage", "exactResultNotDisplayed", Status.PASS,
					"No Exact Result Found, Text Displayed");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "exactResultNotDisplayed", Status.FAIL,
					"No Exact Result Found, Text not Displayed");
		}
	}

	/*
	 * Method: flexibleResultNotDisplayed Description:Validate Flexible Result not
	 * displayed text Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void noFlexibleResultTextDisplayed() {
		if (verifyObjectDisplayed(noFlexibleResultText)) {
			getElementInView(noFlexibleResultText);
			tcConfig.updateTestReporter("CUISearchPage", "flexibleResultNotDisplayed", Status.PASS,
					"No Flexible Result Found, Text Displayed");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "flexibleResultNotDisplayed", Status.FAIL,
					"No Flexible Result Found, Text not Displayed");
		}
	}

	/*
	 * Method: flexibleResultNotPresent Description:Validate Flexible Result header
	 * not displayed Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void flexibleResultNotPresent() {
		if (!verifyObjectDisplayed(objcheckFlexDateText)) {
			tcConfig.updateTestReporter("CUISearchPage", "flexibleResultNotPresent", Status.PASS,
					"Flexible results header not displayed, Flexible Reult No Present");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "flexibleResultNotPresent", Status.FAIL,
					"Flexible Results Header displayed");
		}
	}

	/*
	 * Method: flexibleSearchResult Description:Validate Flexible Search Result when
	 * flexible checkbox is selected Date: apr/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void flexibleSearchResult() {
		waitUntilElementVisibleBy(driver, objcheckFlexDateText, 120);
		if (verifyObjectDisplayed(objcheckFlexDateText)) {
			getElementInView(objcheckFlexDateText);
			if (getList(flexibleResultList).size() > 0) {
				tcConfig.updateTestReporter("CUISearchPage", "flexibleSearchResult", Status.PASS,
						"Total number of Flexible Date List is: " + getList(flexibleResultList).size());
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "flexibleSearchResult", Status.FAIL,
						"No Flexible date list");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "flexibleSearchResult", Status.FAIL,
					"No Flexible Dates displayed");
		}

	}

	/*
	 * Method: exactResultPresence Description:Validate Exact Result displayed or
	 * not Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void exactResultPresence() {
		tcConfig.updateTestReporter("CUISearchPage", "exactResultPresence", Status.PASS,
				"Checking For " + testData.get("Data_Used"));
		if (testData.get("ExactResults").contains("Yes")) {
			validateSearchResult();
		} else {
			exactResultNotDisplayed();
		}
	}

	/*
	 * Method: checkInDateMinus3Check Description: Check In date minus three
	 * flexible dates check Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkInDateMinus3CheckWithoutHold() throws Exception {
		// Check In Date
		String checkInDate = testData.get("Checkindate");
		// Variable to compare dates
		long compareDate;
		// list of UI Dates
		List<String> allFlexibleDates = returnConvertedDate(getListString(flexibleResultDate),
				getYearFromTestData(checkInDate));
		for (int totalDate = 0; totalDate < 3; totalDate++) {
			compareDate = flexibleDateCompare(checkInDate, flexibleResultDate, checkInDate, totalDate + 1);
			flexibleDatesDifferenceValidation(compareDate, allFlexibleDates.get(totalDate), checkInDate);
		}
	}

	/*
	 * Method: flexibleDateCheckWithHold Description: Check Flexible dates when hold
	 * is applied in UI Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void flexibleDateCheckWithHold() throws Exception {
		// Check In Date
		String checkInDate = testData.get("Checkindate");
		// Variable to compare dates
		long compareDate;
		// list of UI Dates
		List<String> allFlexibleDates = returnConvertedDate(getListString(flexibleResultDate),
				getYearFromTestData(checkInDate));
		for (int totalDate = 0; totalDate < getList(flexibleResultDate).size(); totalDate++) {
			compareDate = flexibleDateCompare(checkInDate, flexibleResultDate, checkInDate, totalDate + 1);
			flexibleDatesDifferenceValidation(compareDate, allFlexibleDates.get(totalDate), checkInDate);
		}

	}

	/*
	 * Method: checkInDatePlus3CheckWithoutHold Description:Check In date plus three
	 * flexible dates check Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkInDatePlus3CheckWithoutHold() throws Exception {
		// Check In Date
		String checkInDate = testData.get("Checkindate");
		// Check out Date
		String checkOutDate = testData.get("Checkoutdate");
		// Variable to compare dates
		long compareDate;
		// list of UI Dates
		List<String> allFlexibleDates = returnConvertedDate(getListString(flexibleResultDate),
				getYearFromTestData(checkInDate));
		for (int totalDate = 3; totalDate < getList(flexibleResultDate).size(); totalDate++) {
			compareDate = flexibleDateCompare(checkInDate, flexibleResultDate, checkInDate, totalDate + 1);
			flexibleDatesDifferenceValidation(compareDate, allFlexibleDates.get(totalDate), checkOutDate);
		}
	}

	/*
	 * Method: flexibleDateCompare Description:Compare Flexible date to check in
	 * date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public long flexibleDateCompare(String comapreWith, By compareTo, String addYear, int selectListDate) {
		// List of ddates to convert
		List<String> flexibleDates = new ArrayList<String>();
		// String for storing dates
		String convert = null;
		// Date Format
		DateTimeFormatter dateFormat = null;
		try {
			convert = convertDateFormat("dd MMM yyyy", "yyyy-MM-dd", comapreWith.trim());
			flexibleDates = returnConvertedDate(getListString(compareTo), getYearFromTestData(addYear));
			flexibleDates = convertDateFormat("MM/dd/yyyy", "yyyy-MM-dd", flexibleDates);
			dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISearchPage", "flexibleDateCompare", Status.FAIL,
					"Error in validating flexible dates");
		}
		return ChronoUnit.DAYS.between(LocalDate.parse(convert, dateFormat),
				LocalDate.parse(flexibleDates.get(selectListDate - 1), dateFormat));
	}

	/*
	 * Method: dateOnHoldList Description: List of dates that are on hold from
	 * testdata Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public List<String> dateOnHoldList() {
		// List of all dates on hold
		List<String> dateOnHold = new ArrayList<String>();
		for (int totalDate = 1; totalDate < 4; totalDate++) {
			if (testData.get("onHoldDate" + totalDate).contains("/")) {
				dateOnHold.add(testData.get("onHoldDate" + totalDate));
			}
		}
		return dateOnHold;
	}

	/*
	 * Method: flexibleDateOnHoldNotPresentInUI Description: Check all the flexible
	 * dates which are not present in UI due to hold appllied Date: Apr/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void flexibleDateOnHoldNotPresentInUI() throws ParseException {
		// Get check in date from test data
		String checkInDate = testData.get("Checkindate");
		// temporary variable for validation purpose
		Boolean temp = false;
		// list of all flexible dates
		List<String> allFlexibleDates = returnConvertedDate(getListString(flexibleResultDate),
				getYearFromTestData(checkInDate));
		// list of all dates on hold
		List<String> datesOnHold = dateOnHoldList();
		for (int holdDates = 0; holdDates < datesOnHold.size(); holdDates++) {
			Date holdDate = new SimpleDateFormat("MM/dd/yyy").parse(datesOnHold.get(holdDates));

			for (int displayedDate = 0; displayedDate < allFlexibleDates.size(); displayedDate++) {
				Date flexibleDate = new SimpleDateFormat("MM/dd/yyy").parse(allFlexibleDates.get(displayedDate));
				temp = compareFlexibleDateonHold(holdDate, flexibleDate, temp);
				if (temp == false) {
					break;
				}
			}

			checkFlexibleDateNotPresent(temp, holdDate);
		}

	}

	/*
	 * Method: compareFlexibleDateonHold Description: Compare flexible dates on hold
	 * in UI and test data Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public Boolean compareFlexibleDateonHold(Date holdDate, Date flexibleDate, Boolean temp) {
		if (holdDate.compareTo(flexibleDate) == 0) {
			temp = false;
		} else {
			temp = true;
		}

		return temp;
	}

	/*
	 * Method: checkFlexibleDateNotPresent Description: Report print if flexible
	 * date is not present in UI Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkFlexibleDateNotPresent(Boolean temp, Date holdDate) {
		if (temp == true) {
			tcConfig.updateTestReporter("CUISearchPage", "checkFlexibleDateNotPresent", Status.PASS,
					holdDate + " date is not present in flexible date as hold is applied");
		} else if (temp == false) {
			tcConfig.updateTestReporter("CUISearchPage", "checkFlexibleDateNotPresent", Status.FAIL,
					holdDate + " date is present in flexible date even though hold is applied");
		}
	}

	/*
	 * Method: differenceInDatesCompare Description: Compare Difference in two dates
	 * in days Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void differenceInDatesCompare(long compareWith, long compareTo, long elseCompareTo, String comparedDate,
			String checkInDate) {
		if (compareWith == compareTo || compareWith == elseCompareTo) {
			tcConfig.updateTestReporter("CUISearchPage", "differenceinDates", Status.PASS,
					"Difference between " + comparedDate + " date and Check in date " + checkInDate
							+ " is displayed correctly that is " + compareWith);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "differenceinDates", Status.FAIL, "Difference between "
					+ comparedDate + " date and Check in " + checkInDate + " date is not displayed correctly");
		}
	}

	/*
	 * Method: flexibleDatesDifferenceValidation Description:difference between
	 * flexible dates and check kin date validationsDate: Apr/2020 Author: Unnat
	 * Jain Changes By:
	 * 
	 * NA
	 */
	public void flexibleDatesDifferenceValidation(Long compareDate, String strComparedDate, String checkInDate) {
		// diffrence between two dates
		int intDifference = compareDate.intValue();
		switch (intDifference) {
		case 1:
		case -365:
			differenceInDatesCompare(compareDate, 1L, -365L, strComparedDate, checkInDate);
			break;
		case 2:
		case -364:
			differenceInDatesCompare(compareDate, 2L, -364L, strComparedDate, checkInDate);
			break;
		case 3:
		case -363:
			differenceInDatesCompare(compareDate, 3L, -363L, strComparedDate, checkInDate);
			break;
		case -1:
		case 364:
			differenceInDatesCompare(compareDate, -1L, 364L, strComparedDate, checkInDate);
			break;
		case -2:
		case 363:
			differenceInDatesCompare(compareDate, -2L, 363L, strComparedDate, checkInDate);
			break;
		case -3:
		case 362:
			differenceInDatesCompare(compareDate, -3L, 362L, strComparedDate, checkInDate);
			break;
		default:
			tcConfig.updateTestReporter("CUISearchPage", "flexibleDatesDifferenceValidation", Status.FAIL,
					"Error in Flexible Dates Validation");
			break;
		}

	}

	/*
	 * Method: flexibleDatesSorting Description:Sorting check for two flexible dates
	 * Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void flexibleDatesSorting() {
		waitUntilElementVisibleBy(driver, objcheckFlexDateText, 120);

		try {
			ascendingOrderDateCheck(returnConvertedDate(getListString(flexibleResultDate),
					getYearFromTestData(testData.get("Checkindate"))), "MM/dd/yyyy", "Flexible Dates");
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISearcPage", "flexibleDatesSorting", Status.FAIL,
					"Sorting Validation Failed");

		}
	}

	/*
	 * Method: returnConvertedDate Description:Return converted flexible date into
	 * String Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public List<String> returnConvertedDate(List<String> strDate, String addYear) {
		// List for updated flexible dates displayed in UI
		List<String> updatedDate = new ArrayList<String>();
		try {
			for (int totdalDate = 0; totdalDate < strDate.size(); totdalDate++) {
				// Check In Date from UI in format(Monday 4/13 → Thursday 4/16)
				String strCheckIn = strDate.get(totdalDate).split("→")[0];
				strCheckIn = strCheckIn.toLowerCase().split("day")[1].trim();
				strCheckIn = strCheckIn.concat("/" + addYear);
				updatedDate.add(strCheckIn);

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISearcPage", "returnConvertedDate", Status.FAIL,
					"Date format not supported");
		}

		return updatedDate;

	}

	/*
	 * Method: getYearFromTestData Description: Get year from date in test data
	 * Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public String getYearFromTestData(String strDate) {
		// get year from check in date
		strDate = strDate.trim().substring(strDate.length() - 4);
		return strDate;
	}

	/*
	 * Method: checkWorldmarkResortPresece Description:Check Worldmark Resort
	 * Presence Date:Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkWorldmarkResortPreseceAtLast() {
		String resortWorldmarkPresent = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				resortWorldmarkPresent = "Y";
				int iteratorStartWorldmark = resortNameIterator;
				for (int IteratorWorldmarkResort = iteratorStartWorldmark; IteratorWorldmarkResort < getList(
						textResortName).size(); IteratorWorldmarkResort++) {
					String resortname1 = getList(textResortName).get(IteratorWorldmarkResort).getText().trim();
					if (resortname1.contains("Wyndham")) {
						getElementInView(getList(textResortName).get(IteratorWorldmarkResort));
						tcConfig.updateTestReporter("CUISearchPage", "checkWorldmark", Status.FAIL,
								"WorldMark resort not present at last");

					} else {
						int indexWorldMarkResort = IteratorWorldmarkResort + 1;
						getElementInView(getList(textResortName).get(IteratorWorldmarkResort));
						tcConfig.updateTestReporter("CUISearchPage", "checkWorldmark", Status.PASS,
								"WorldMark Resort with resort name: " + resortname1 + " present with index: "
										+ indexWorldMarkResort);
					}
				}
				break;
			}

		}
		if (resortWorldmarkPresent == "Y") {
			tcConfig.updateTestReporter("CUISearchPage", "checkworldmark", Status.PASS, "Worldmark Resort Present");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkworldmark", Status.FAIL, "Worldmark resort not present");
		}

	}

	/*
	 * Method: checkWorldmarkResortAvailableUnitNotPresent Description:Check
	 * Worldmark Resort Presence Date:Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkWorldmarkResortAvailableUnitNotPresent() {
		String resortWorldmarkPresent = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				resortWorldmarkPresent = "Y";
				try {
					WebElement worldmarkresort = getObject(By.xpath("(//div[contains(@class,'resort-card__content')])["
							+ (resortNameIterator + 1) + "]/div/a[contains(.,'View')]"));
					if (verifyObjectDisplayed(worldmarkresort)) {
						getElementInView(worldmarkresort);
						tcConfig.updateTestReporter("CUISearchPage", "checkWorldmarkResortViewAvailableUnitNotPresent",
								Status.FAIL, "View Available unit type present");
					} else {
						tcConfig.updateTestReporter("CUISearchPage", "checkWorldmarkResortViewAvailableUnitNotPresent",
								Status.PASS, "View Available unit type not present for resort: " + resortname);
					}
				} catch (Exception e) {
					tcConfig.updateTestReporter("CUISearchPage", "checkWorldmarkResortViewAvailableUnitNotPresent",
							Status.PASS, "View Available unit type not present for resort: " + resortname);
				}
			}
		}
		if (resortWorldmarkPresent == "Y") {
			tcConfig.updateTestReporter("CUISearchPage", "checkWorldmarkResortViewAvailableUnitNotPresent", Status.PASS,
					"Worldmark Resort Present");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkWorldmarkResortViewAvailableUnitNotPresent", Status.FAIL,
					"Worldmark resort not present");
		}
	}

	/*
	 * Method: checkNoWorldMarkResort Description:Check No Worldmark Resort Presence
	 * Date:Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkNoWorldMarkResort() {
		String resortWorldmarkPresent = "N";
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				resortWorldmarkPresent = "Y";

			}

		}

		if (resortWorldmarkPresent == "N") {
			tcConfig.updateTestReporter("CUISearchPage", "checkNoWorldMarkResort", Status.PASS,
					"Worldmark Resort not Present for Discovery Member");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkNoWorldMarkResort", Status.FAIL,
					"Worldmark resort present for discovery member");
		}
	}

	/*
	 * Method: flexibleResultPresence Description:Validate Flexible Result displayed
	 * or not when check box selected Date: Apr/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void flexibleResultPresence() {
		if (testData.get("FlexibleResults").contains("Yes")) {
			flexibleSearchResult();
		} else {
			if (testData.get("SelectFlexDate").contains("Yes")) {
				noFlexibleResultTextDisplayed();
			} else {
				flexibleResultNotPresent();
			}
		}
	}

	/*
	 * Method: validateAlternateMessagePresent Description:Check Alternate Message
	 * displayed or not when check box selected Date: Apr/2020 Author: Abhijeet
	 * Changes By: NA
	 */
	public void validateAlternateMessagePresent() {
		waitUntilElementVisibleBy(driver, altarnateMessage, 120);
		if (verifyObjectDisplayed(altarnateMessage)) {
			tcConfig.updateTestReporter("CUISearcPage", "validateAlternateMessagePresent", Status.PASS,
					"Alternate Message validation was successful");
		} else {
			tcConfig.updateTestReporter("CUISearcPage", "validateAlternateMessagePresent", Status.FAIL,
					"Failed while validating the Alternate Message");
		}
	}

	/*
	 * Method: validateAlternateResult Description:Check Alternate Results count
	 * Message displayed or not when check box selected Date: Apr/2020 Author:
	 * Abhijeet Changes By: NA
	 */
	public void validateAlternateResult() {
		waitUntilElementVisibleBy(driver, resortCard, 120);
		if (getList(resortCard).size() > 0) {
			tcConfig.updateTestReporter("CUISearchPage", "validateAlternateResult", Status.PASS,
					"Total number of Alternate resort is: " + getList(resortCard).size());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateAlternateResult", Status.FAIL,
					"No Alternate result found");
		}
	}

	/*
	 * Method: validateAlternateMessagePresent Description:Check Alternate Message
	 * displayed or not when check box selected Date: Apr/2020 Author: Abhijeet
	 * Changes By: NA
	 */
	public void validateAlternateMessageNotPresent() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (!verifyObjectDisplayed(altarnateMessage)) {
			tcConfig.updateTestReporter("CUISearcPage", "validateAlternateMessagePresent", Status.PASS,
					"Alternate Result Message not Found");
		} else {
			tcConfig.updateTestReporter("CUISearcPage", "validateAlternateMessagePresent", Status.FAIL,
					"Alternate Result Message is Displayed");
		}
	}

	/*
	 * Method: clubPassResortNotPresentInAlternateSearch Description:Check Club Pass
	 * Resort not Present in Alternate Search Date: Apr/2020 Author: Abhijeet
	 * Changes By: NA
	 */
	public void clubPassResortNotPresentInAlternateSearch() {
		String resortWorldmarkPresent = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int resortNameIterator = 0; resortNameIterator < getList(textResortName).size(); resortNameIterator++) {
			getElementInView(textResortName);
			String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
			if (resortname.contains("WorldMark")) {
				resortWorldmarkPresent = "Y";
				break;
			} else {
				resortWorldmarkPresent = "N";
			}
		}
		if (resortWorldmarkPresent.equals("N")) {
			tcConfig.updateTestReporter("CUISearcPage", "clubPassResortNotPresentInAlternateSearch", Status.PASS,
					"Club Pass Resort Not Present in Alternate Search");
		} else {
			tcConfig.updateTestReporter("CUISearcPage", "clubPassResortNotPresentInAlternateSearch", Status.FAIL,
					"Club Pass Resort Present in Alternate Search");
		}
	}

	/*
	 * Method: clubPassResortNotPresentInAlternateSearch Description:Check Club Pass
	 * Resort not Present in Alternate Search Date: Apr/2020 Author: Abhijeet
	 * Changes By: NA
	 */
	public void inventoryDiscountPriceValidationOnlyResortSearched() {
		String location = testData.get("CUI_Location").trim();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortNameIterator = 0; resortNameIterator < getList(textResortName)
					.size(); resortNameIterator++) {
				String resortname = getList(textResortName).get(resortNameIterator).getText().trim();
				if (resortname.equals(location)) {
					clickResortCardViewLabel(resortNameIterator);
					checkInventoryDiscount();
					clickElementJSWithWait(hideResortElement);
					break;
				}
				clickElementJSWithWait(hideResortElement);

			}
		}
	}

	/*
	 * Method: verifyFilterAndSortButtonPresence Description: Verify Filter and Sort
	 * Button Presence Date:April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void verifyFilterAndSortButtonPresence() {
		waitUntilElementVisibleBy(driver, buttonFilter, 120);
		if (verifyObjectDisplayed(buttonFilter)) {
			tcConfig.updateTestReporter("CUISearchPage", "verifyFilterAndSort", Status.PASS,
					"Filter and sort button present");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyFilterAndSort", Status.FAIL,
					"Filter and sort button not present");
		}
	}

	/*
	 * Method: checkExperiencesTypeCollapse Description:Validate Experience Unit
	 * Type Area Collapse Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void checkExperiencesTypeCollapse() {
		waitUntilElementVisibleBy(driver, experienceArea, 120);
		if (verifyObjectDisplayed(experienceArea)) {
			getElementInView(experienceArea);
			clickElementJSWithWait(experienceArea);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getObject(experienceArea).getAttribute("aria-expanded").equalsIgnoreCase("False")) {
				tcConfig.updateTestReporter("CUISearchPage", "checkExperiencesTypeCollapse()", Status.PASS,
						"Experience Type Area Collapsed");
				clickElementJSWithWait(experienceArea);
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkExperiencesTypeCollapse()", Status.FAIL,
						"Experience Type Area did not collapse");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkExperiencesTypeCollapse()", Status.FAIL,
					"Experience Type Area not present");
		}
	}

	/*
	 * Method: validateProcessingImage Description:Validate Image Loading Date field
	 * Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void validateProcessingImage() {
		if (verifyObjectDisplayed(searchLoadingImage)) {
			if (getObject(searchLoadingImage).isDisplayed()) {
				tcConfig.updateTestReporter("CUISearchPage", "validateProcessingImage", Status.PASS,
						"Loading Image is present when while searching for resort");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateProcessingImage", Status.FAIL,
						"Loading Image not present when while searching for resort");
			}
		}
	}

	/*
	 * Method: validateSearchResult Description:Validate Seacrh Result Date field
	 * Date: April/2020 Author: Ajib PArida Changes By: NA
	 */
	public void validateSearchResultZero() {
		if (verifyObjectDisplayed(SearchResult)) {
			String result = getObject(SearchResult).getText().split(" ")[0].trim();
			int resultfinal = Integer.parseInt(result);
			if (resultfinal == 0) {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.PASS,
						" Result found with count : " + resultfinal);
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.FAIL,
						"Search result found with count: " + resultfinal + " ,expected value should be 0");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateSearchResult", Status.FAIL,
					"Search result header not found");
		}

	}

	/*
	 * Method: verifyFilterAndSort verifyFilterAndSort Found Date:April/2020 Author:
	 * Ajib Parida Changes By: NA
	 */
	public void verifyFilterAndSortDisable() {
		if (verifyObjectDisplayed(buttonFilter)) {
			if (!(getObject(buttonFilter)).isEnabled()) {
				tcConfig.updateTestReporter("CUISearchPage", "verifyFilterAndSort", Status.PASS,
						"Filter and sort button is disabled");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "verifyFilterAndSort", Status.FAIL,
						"Filter and sort button not disabled");
			}
		}
	}

	/*
	 * Method: validateHoldLoading Description:Validate Hold Tight Loading Date
	 * field Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void validateHoldLoading() {
		if (verifyObjectDisplayed(holdTightloading)) {
			String holdTightLoading = getElementText(holdTightloading);
			tcConfig.updateTestReporter("CUISearchPage", "validateProcessingImage", Status.PASS,
					"Hold Tight Message: " + holdTightLoading);
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateProcessingImage", Status.FAIL,
					"Hold Tight Message not Present");
		}
	}

	/*
	 * Method: clickFirstAvailableUnit Description:Validate Hold Tight Loading Date
	 * field Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void clickFirstAvailableUnit() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ObjResortCardViewLabel, 120);
		if (getList(ObjResortCardViewLabel).size() > 0) {
			WebElement viewclk = getList(ObjResortCardViewLabel).get(0);
			scrollDownForElementJSWb(viewclk);
			clickElementJSWithWait(viewclk);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "clickAvailableUnit", Status.FAIL, "Resort Card not present");
		}

	}

	/*
	 * Method: clickBookButtonWithoutValidation Description:Click on book button
	 * field Date: April/2020 Author: Monideep Roychowshury Changes By: NA
	 */
	public void clickBookButtonWithoutValidation() {
		String unitForBooking = testData.get("bookUnitType");
		if (getList(totalBookButton).size() > 0) {
			clickElementJSWithWait(getObject(By.xpath("//a[contains(text(),'" + unitForBooking
					+ "')]/ancestor::div[contains(@class,'resort-card__unit')]//a[contains(text(),'Book')]")));
			tcConfig.updateTestReporter("CUISearchPage", "clickBookButtonWithoutValidation", Status.PASS,
					"Book button has been clicked");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "clickBookButtonWithoutValidation", Status.FAIL,
					"Book button couldn't be found");
		}
	}

	/*
	 * Method: getUnitTypeAndPrice Description:Validate Hold Tight Loading Date
	 * field Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void getUnitTypeAndPrice() {
		String selectedUnitType = null;
		String firstAmount = null;
		if (getList(availableUnit).size() > 0) {
			WebElement firstAvailableUnit = getList(availableUnit).get(0);
			selectedUnitType = getList(availableUnit).get(0).getText().trim();
			if (verifyObjectDisplayed(textDiscountPriceList)) {
				firstAmount = getList(textDiscountPriceList).get(0).getText().trim();
			} else {
				firstAmount = getList(textActualPriceList).get(0).getText().trim();
			}
			clickElementJSWithWait(firstAvailableUnit);
			tcConfig.updateTestReporter("CUISearchPage", "getUnitTypeAndPrice", Status.PASS,
					"Clicked on First Available Unit: " + selectedUnitType + " from the First Resort Card");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "getUnitTypeAndPrice", Status.FAIL,
					"Available Units not present in First Resort Card");
		}
		verifyUnitDetailsUnitType_Amount(selectedUnitType, firstAmount);
	}

	/*
	 * Method: verifyUnitDetails Description:Validate Hold Tight Loading Date field
	 * Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void verifyUnitDetailsUnitType_Amount(String unitType, String amount) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, unitDetails, 120);
		if (verifyObjectDisplayed(unitDetails)) {
			tcConfig.updateTestReporter("CUISearchPage", "verifyUnitDetails", Status.PASS,
					"Unit Details Header Present");
			String textUnit = getElementText(unitTypeText).trim();
			String textAmount = getElementText(amountText).trim();
			if (textUnit.contains(unitType) && textAmount.contains(amount)) {
				tcConfig.updateTestReporter("CUISearchPage", "verifyUnitDetails", Status.PASS,
						"Unit Type and Amount Validated");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "verifyUnitDetails", Status.FAIL,
						"Failed while validating Unit Type and Amount");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyUnitDetails", Status.FAIL,
					"Failed while validatig Unit Type Header");
		}
	}

	/*
	 * Method: verifyUnitDetails Description:Validate Hold Tight Loading Date field
	 * Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void verifyUnitDetails() {
		elementPresenceVal(textUnitDetails, "Unit Details", "CUISearchPage", "verifyUnitDetails");
		elementPresenceVal(unitDetailsTooltip, "Unit Details Tool Tip", "CUISearchPage", "verifyUnitDetails");
		elementPresenceVal(viewPlanCTA, "View Plan CTA", "CUISearchPage", "verifyUnitDetails");
		elementPresenceVal(textSize, "SIZE", "CUISearchPage", "verifyUnitDetails");
		elementPresenceVal(textAccomadates, "ACCOMADATES", "CUISearchPage", "verifyUnitDetails");
		elementPresenceVal(textKitchen, "KITCHEN", "CUISearchPage", "verifyUnitDetails");
		elementPresenceVal(textBaths, "BATHS", "CUISearchPage", "verifyUnitDetails");
		elementPresenceVal(textBeds, "BEDS", "CUISearchPage", "verifyUnitDetails");

	}

	public void validateVIPBenefitApply() {
		elementPresenceVal(VIPBenefitHeader, "VIP BENEFITS APPLY TO THIS SUITE.", "SuiteDetailsPage",
				"validateVIPBenefitApply");
		elementPresenceVal(VIPBenefitSubDesc, "VIP BENEFITS APPLY Description", "SuiteDetailsPage",
				"validateVIPBenefitApply");
	}

	/*
	 * Method: verifyUnitDetails Description:Validate Hold Tight Loading Date field
	 * Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void verifyRoomAmenities() {
		waitUntilElementVisibleBy(driver, headerRoomAminities, 120);
		if (verifyObjectDisplayed(headerRoomAminities)) {
			getElementInView(headerRoomAminities);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			ArrayList<String> obtainedList = new ArrayList<>();
			for (WebElement roomAminities : getList(textRoomAminities)) {
				obtainedList.add(roomAminities.getText().trim());
			}
			ArrayList<String> sortedList = new ArrayList<>();
			for (String sortAmiities : obtainedList) {
				sortedList.add(sortAmiities);
			}

			Collections.sort(sortedList);
			try {
				Assert.assertTrue(sortedList.equals(obtainedList));
				tcConfig.updateTestReporter("CUISearchPage", "verifyRoomAmenities", Status.PASS,
						"Room Amenities are Sorted" + sortedList);
			} catch (AssertionError error) {
				tcConfig.updateTestReporter("CUISearchPage", "verifyRoomAmenities", Status.FAIL,
						"Room Amenities are not sorted" + error.getMessage());
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyRoomAmenities", Status.FAIL,
					"Room Amneties Header not present");
		}

	}

	/*
	 * Method: validateFloorPlan Description:Validate Hold Tight Loading Date field
	 * Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void validateFloorPlan() {
		waitUntilElementVisibleBy(driver, headerFloorPlan, 120);
		if (verifyObjectDisplayed(headerFloorPlan)) {
			getElementInView(headerFloorPlan);
			if (verifyObjectDisplayed(imageFloorPlan)) {
				tcConfig.updateTestReporter("CUISearchPage", "validateFloorPlan", Status.PASS,
						"Floor Plan Header and Image Present");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateFloorPlan", Status.FAIL,
						"Floor Plan Image not Present");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateFloorPlan", Status.FAIL,
					"Floor Plan Header not Present");
		}
	}
	/*
	 * Method: validateLearnMoreToolTip Description:Validate Learn More clicked
	 * Date: January/2021 Author: Umashankar Pati Changes By: NA
	 */
	public void validateLearnMoreToolTip() {
		waitUntilElementVisibleBy(driver, learnMoreToolTip, 120);
		if (verifyObjectDisplayed(learnMoreToolTip)) {
			getElementInView(learnMoreToolTip);
			clickElementJSWithWait(learnMoreToolTip);
			tcConfig.updateTestReporter("CUISearchPage", "validateLearnMoreToolTip", Status.PASS,
					"Learn More Tool Tip Clicked");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateLearnMoreToolTip", Status.FAIL,
					"Learn More Tool Tip not Clicked");
		}
	}

	/*
	 * Method: verifyUnitDetailsCloseButton Description:Validate Hold Tight Loading
	 * Date field Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void verifyUnitDetailsCloseButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, unitCloseButton, 120);
		if (verifyObjectDisplayed(unitCloseButton)) {
			getElementInView(unitCloseButton);
			clickElementJSWithWait(unitCloseButton);
			tcConfig.updateTestReporter("CUISearchPage", "verifyUnitDetailsCloseButton", Status.PASS,
					"Clicked on Unit Details Closed Button Successfully");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyUnitDetailsCloseButton", Status.FAIL,
					"Failed to click on unit details close button");
		}
	}

	/*
	 * Method: verifyUnitNextPrevoiusButton Description:Validate Hold Tight Loading
	 * Date field Date: April/2020 Author: Ajib Parida Changes By: NA
	 */
	public void verifyUnitNextPrevoiusButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		int totalUnits = getList(availableUnit).size();
		WebElement firstAvailableUnit = getList(availableUnit).get(0);
		clickElementJSWithWait(firstAvailableUnit);
		if (totalUnits == 1) {
			if (getObject(unitNextButton).getAttribute("class").contains("disabled")
					&& getObject(unitPrevoiusButton).getAttribute("class").contains("disabled")) {
				tcConfig.updateTestReporter("CUISearchPage", "verifyUnitNextButton", Status.PASS,
						"Next and Previous Unit Button is disabled as only one unit is present");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "verifyUnitNextButton", Status.FAIL,
						"Either Next or Previous Unit Button is not disabled even if one unit is present");
			}
		} else if (totalUnits > 1) {
			if (!getObject(unitNextButton).getAttribute("class").contains("disabled")) {
				clickElementJSWithWait(unitNextButton);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (!getObject(unitPrevoiusButton).getAttribute("class").contains("disabled")) {
					clickElementJSWithWait(unitPrevoiusButton);
					tcConfig.updateTestReporter("CUISearchPage", "verifyUnitNextButton", Status.PASS,
							"Next and Prevoius Unit Button is enabled as more than one unit is present");
				} else {
					tcConfig.updateTestReporter("CUISearchPage", "verifyUnitNextButton", Status.FAIL,
							"Previous Unit Button is not enabled even if more than one unit is present");
				}
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "verifyUnitNextButton", Status.FAIL,
						"Next Unit Button is not enabled even if more than one unit is present");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyUnitNextButton", Status.FAIL,
					"No Unit is present in Resort Card Selected");
		}
	}

	/*
	 * Method: clickBookButton Description:Validate Hold Tight Loading Date field
	 * Date: April/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickBookButton() {
		String unitForBooking = testData.get("bookUnitType");
		if (getList(totalBookButton).size() > 0) {
			clickElementJSWithWait(getObject(By.xpath("//a[contains(text(),'" + unitForBooking
					+ "')]/ancestor::div[contains(@class,'resort-card__unit')]//a[contains(text(),'Book')]")));
			waitUntilElementVisibleBy(driver, textBookDetails, 120);
			if (verifyObjectDisplayed(textBookDetails)) {
				tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.PASS,
						"Successfully clicked on Book Button");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.FAIL,
						"Failed to click on Book Button");
			}
		}
	}

	/*
	 * Method: flexResultWhenCheckBoxNotSelected Description:Validate Hold Tight
	 * Loading Date field Date: April/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void flexResultWhenCheckBoxNotSelected() {
		waitUntilElementVisibleBy(driver, flexResultCheckboxNotSelected, 120);
		if (verifyObjectDisplayed(flexResultCheckboxNotSelected)) {
			if (getList(flexResultCheckboxNotSelected).size() > 0) {
				getElementInView(flexResultCheckboxNotSelected);
				tcConfig.updateTestReporter("CUISearchPage", "flexResultWhenCheckBoxNotSelected", Status.PASS,
						"Flex Result Present even when Flex Checkbox is not selected");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "flexResultWhenCheckBoxNotSelected", Status.FAIL,
						"Flex Result not present");
			}
		}
	}

	/*
	 * Method: flexibleSearchResultPillFormat Description:Validate Flexible Search
	 * Result when flexible checkbox is selected Date: apr/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void flexibleSearchResultPillFormat() {
		waitUntilElementVisibleBy(driver, objcheckFlexDateTextPill, 120);
		if (verifyObjectDisplayed(objcheckFlexDateTextPill)) {
			getElementInView(objcheckFlexDateTextPill);
			if (getList(flexibleResultListPill).size() > 0) {
				tcConfig.updateTestReporter("CUISearchPage", "flexibleSearchResult", Status.PASS,
						"Total number of Flexible Date List is: " + getList(flexibleResultListPill).size());
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "flexibleSearchResult", Status.FAIL,
						"No Flexible date list");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "flexibleSearchResult", Status.FAIL,
					"No Flexible Dates displayed");
		}

	}

	/*
	 * Method: flexibleResultPresence Description:Validate Flexible Result displayed
	 * or not when check box selected Date: Apr/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void flexibleResultPresencePillFormat() {
		if (testData.get("FlexibleResults").contains("Yes")) {
			flexibleSearchResultPillFormat();
		} else if (testData.get("FlexibleResults").contains("No")) {
			if (verifyObjectDisplayed(objcheckFlexDateTextPill)) {
				tcConfig.updateTestReporter("CUISearcPage", "flexibleResultPresencePillFormat", Status.PASS,
						"Flexible Results not present");
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "flexibleResultPresencePillFormat", Status.FAIL,
						"Error in validating felxible pill format");
			}
		}
	}

	/*
	 * Method: flexibleDateCheckWithHold Description: Check Flexible dates when hold
	 * is applied in UI Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void flexibleDateCheckWithHoldPillFormat() throws Exception {
		// Check In Date
		String checkInDate = testData.get("Checkindate");
		// Variable to compare dates
		long compareDate;
		// list of UI Dates
		List<String> allFlexibleDates = returnConvertedDate(getListString(flexibleResultDatePill),
				getYearFromTestData(checkInDate));
		for (int totalDate = 0; totalDate < getList(flexibleResultDatePill).size(); totalDate++) {
			compareDate = flexibleDateCompare(checkInDate, flexibleResultDatePill, checkInDate, totalDate + 1);
			flexibleDatesDifferenceValidation(compareDate, allFlexibleDates.get(totalDate), checkInDate);
		}

	}

	/*
	 * Method: flexibleDateOnHoldNotPresentInUIPill Description: Check all the
	 * flexible dates which are not present in UI due to hold appllied Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void flexibleDateOnHoldNotPresentInUIPill() throws ParseException {
		// Get check in date from test data
		String checkInDate = testData.get("Checkindate");
		// temporary variable for validation purpose
		Boolean temp = false;
		// list of all flexible dates
		List<String> allFlexibleDates = returnConvertedDate(getListString(flexibleResultDatePill),
				getYearFromTestData(checkInDate));
		// list of all dates on hold
		List<String> datesOnHold = dateOnHoldList();
		for (int holdDates = 0; holdDates < datesOnHold.size(); holdDates++) {
			Date holdDate = new SimpleDateFormat("MM/dd/yyy").parse(datesOnHold.get(holdDates));

			for (int displayedDate = 0; displayedDate < allFlexibleDates.size(); displayedDate++) {
				Date flexibleDate = new SimpleDateFormat("MM/dd/yyy").parse(allFlexibleDates.get(displayedDate));
				temp = compareFlexibleDateonHold(holdDate, flexibleDate, temp);
				if (temp == false) {
					break;
				}
			}

			checkFlexibleDateNotPresent(temp, holdDate);
		}

	}

	/*
	 * Method: flexibleDatesSortingPill Description:Sorting check for two flexible
	 * dates Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void flexibleDatesSortingPill() {
		waitUntilElementVisibleBy(driver, objcheckFlexDateTextPill, 120);

		try {
			ascendingOrderDateCheck(returnConvertedDate(getListString(flexibleResultDatePill),
					getYearFromTestData(testData.get("Checkindate"))), "MM/dd/yyyy", "Flexible Dates");
		} catch (Exception e) {
			tcConfig.updateTestReporter("CUISearcPage", "flexibleDatesSorting", Status.FAIL,
					"Sorting Validation Failed");

		}
	}

	/*
	 * Method: verifyCheckinDateSelected Description:compare checkin date in screen
	 * and selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyCheckinDateSelected() throws Exception {
		String checkInDate = testData.get("Checkindate").trim();
		if (verifyObjectDisplayed(textBoxDateSelect)) {
			String checkInDateScreen = (getObject(textBoxDateSelect).getAttribute("value").split("→")[0]).trim();
			String convertedCheckInDate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkInDate);
			if (checkInDateScreen.compareTo(convertedCheckInDate) == 0) {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.PASS,
						"Check in date selected is : " + checkInDate + " and from screen is: " + convertedCheckInDate);
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckinDateSelected", Status.FAIL,
						"Check in date selected and in screen did not matched");
			}

		} else {
			tcConfig.updateTestReporter("CUISearcPage", "verifyResortCheckinDateSelected", Status.FAIL,
					"Date Field in Search Page not Present");
		}

	}

	/*
	 * Method: verifyCheckoutDateSelected Description:compare checkout date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyCheckoutDateSelected() throws Exception {
		String checkOutDate = testData.get("Checkoutdate").trim();
		if (verifyObjectDisplayed(textBoxDateSelect)) {
			String checkInDateScreen = (getObject(textBoxDateSelect).getAttribute("value").split("→")[1]).trim();
			String convertedCheckOutDate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkOutDate);
			if (checkInDateScreen.compareTo(convertedCheckOutDate) == 0) {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.PASS,
						"Check out date selected is : " + checkOutDate + " and from screen is: "
								+ convertedCheckOutDate);
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.FAIL,
						"Check out date selected and in screen did not matched");
			}

		} else {
			tcConfig.updateTestReporter("CUISearcPage", "verifyCheckoutDateSelected", Status.FAIL,
					"Date Field in Search Page not Present");
		}

	}

	/*
	 * Method: validatePriceAssociatedWithUnitType Description:compare checkout date
	 * in screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void validatePriceAssociatedWithUnitType() {
		String checkUnitPricePresence = null;
		if (verifyObjectDisplayed(resortCardContent)) {
			for (int resortUnitIterator = 0; resortUnitIterator < getList(resortTotalUnit)
					.size(); resortUnitIterator++) {
				WebElement textUnitPrice = getObject(By.xpath("(//div[contains(@class,'resort-card__unit')])["
						+ (resortUnitIterator + 1) + "]//p[contains(.,'pts')]"));
				if (verifyObjectDisplayed(textUnitPrice)) {
					checkUnitPricePresence = "Y";

				} else {
					checkUnitPricePresence = "N";
					break;
				}
			}
			if (checkUnitPricePresence == "Y") {
				tcConfig.updateTestReporter("CUISearcPage", "checkPriceAssociatedWithUnitType", Status.PASS,
						"Unit Price is associated with each Unit Type");
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "checkPriceAssociatedWithUnitType", Status.PASS,
						"Unit Price is not associated with each Unit Type");
			}
		} else {
			tcConfig.updateTestReporter("CUISearcPage", "checkPriceAssociatedWithUnitType", Status.PASS,
					"Resort card Content not Present");
		}

	}

	/*
	 * Method: CheckUnitPriceAssociatedWithUnitType Description:compare checkout
	 * date in screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void CheckUnitPriceAssociatedWithUnitType() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int viewLabelIterator = 0; viewLabelIterator < getList(ObjResortCardViewLabel)
					.size(); viewLabelIterator++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickResortCardViewLabel(viewLabelIterator);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				validatePriceAssociatedWithUnitType();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(hideResortElement);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "CheckUnitPriceAssociatedWithUnitType", Status.FAIL,
					"Resort Card not present");
		}
	}

	/*
	 * Method: clickAvailableUnit Description:compare checkout date in screen and
	 * selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickAvailableUnit() {
		String textUnitType = testData.get("UnitTypetoSelect");
		if (verifyObjectDisplayed(resortCardContent)) {
			WebElement linkUnitType = getObject(
					By.xpath("//a[@data-open='unitDetails' and text() ='" + textUnitType + "']"));
			getElementInView(linkUnitType);
			clickElementJSWithWait(linkUnitType);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(unitDetails)) {
				tcConfig.updateTestReporter("CUISearchPage", "clickAvailableUnit", Status.PASS,
						"Successfully clicked on Unit Type");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "clickAvailableUnit", Status.FAIL,
						"Failed to click on Unit Type");
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "clickAvailableUnit", Status.FAIL,
					"Resort Card Content not found");
		}
	}

	/*
	 * Method: validateBookButtonAssociatedWithUnitType Description:compare checkout
	 * date in screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void validateBookButtonAssociatedWithUnitType() {
		String checkBookButtonPresence = null;
		if (verifyObjectDisplayed(resortCardContent)) {
			for (int resortUnitIterator = 0; resortUnitIterator < getList(resortTotalUnit)
					.size(); resortUnitIterator++) {
				WebElement buttonBook = getObject(By.xpath("(//div[contains(@class,'resort-card__unit')])["
						+ (resortUnitIterator + 1) + "]//a[contains(text(),'Book')]"));
				if (verifyObjectDisplayed(buttonBook)) {
					checkBookButtonPresence = "Y";

				} else {
					checkBookButtonPresence = "N";
					break;
				}
			}
			if (checkBookButtonPresence == "Y") {
				tcConfig.updateTestReporter("CUISearcPage", "validateBookButtonAssociatedWithUnitType", Status.PASS,
						"Book Button is associated with each Unit Type");
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "validateBookButtonAssociatedWithUnitType", Status.PASS,
						"Book Button is not associated with each Unit Type");
			}
		} else {
			tcConfig.updateTestReporter("CUISearcPage", "validateBookButtonAssociatedWithUnitType", Status.PASS,
					"Resort card Content not Present");
		}

	}

	/*
	 * Method: CheckBookAssociatedWithUnitType Description:compare checkout date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void CheckBookAssociatedWithUnitType() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int viewLabelIterator = 0; viewLabelIterator < getList(ObjResortCardViewLabel)
					.size(); viewLabelIterator++) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickResortCardViewLabel(viewLabelIterator);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				validateBookButtonAssociatedWithUnitType();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementJSWithWait(hideResortElement);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "CheckUnitPriceAssociatedWithUnitType", Status.FAIL,
					"Resort Card not present");
		}
	}

	/*
	 * Method: hideAllUnitsOpened Description:compare checkout date in screen and
	 * selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void hideAllUnitsOpened() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		for (int hideIterator = 0; hideIterator < getList(hideResortElement).size(); hideIterator++) {
			WebElement hideclk = getList(hideResortElement).get(hideIterator);
			getElementInView(hideclk);
			clickElementJSWithWait(hideclk);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}
	}

	/*
	 * Method: verifyresortNameInEachResortCard Description:compare checkout date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyResortNameInEachResortCard() {
		String checkResortName = null;
		for (int resortCardIterator = 1; resortCardIterator <= getList(resortCard).size(); resortCardIterator++) {
			WebElement webelementResortName = getObject(
					By.xpath("(//div[contains(@class,'resort-card')]/div/h2/a)[" + resortCardIterator + "]"));
			if (verifyObjectDisplayed(webelementResortName)) {
				checkResortName = "Y";
			} else {
				checkResortName = "N";
				break;
			}
		}
		if (checkResortName == "Y") {
			tcConfig.updateTestReporter("CUISearchPage", "verifyresortNameInEachResortCard", Status.PASS,
					"Resort Name present in each resort card");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyresortNameInEachResortCard", Status.FAIL,
					"Resort Name not present in each resort card");
		}

	}

	/*
	 * Method: validateExperienceInResortCard Description:Check Experience selected
	 * in Filter present in Resort card Date:Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void validateExperienceInResortCard() {
		int checkExperiencePresent = 0;
		for (int resortCardIterator = 1; resortCardIterator <= getList(resortCard).size(); resortCardIterator++) {
			List<WebElement> experienceList = getList(By.xpath("(//div[@class = 'resort-card'])[" + resortCardIterator
					+ "]//div[@class = 'resort-card__tags']/p"));
			if (experienceList.size() > 0) {
				checkExperiencePresent = 1;
			} else {
				checkExperiencePresent = 0;
				break;
			}
		}
		if (checkExperiencePresent > 0) {
			tcConfig.updateTestReporter("CUISearchPage", "validateExperienceInResortCard", Status.PASS,
					"Experience present in Each Resort Card");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateExperienceInResortCard", Status.FAIL,
					"Validation for Experience in Resort card failed");
		}
	}

	/*
	 * Method: clickMapToogleButton Description:compare checkout date in screen and
	 * selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickMapToogleButton() {
		waitUntilElementVisibleBy(driver, showMapToggle, 120);
		if (verifyObjectDisplayed(showMapToggle)) {
			getElementInView(showMapToggle);
			clickElementJSWithWait(showMapToggle);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (!verifyObjectDisplayed(map)) {
				tcConfig.updateTestReporter("CUISearchPage", "clickMapToogleButton", Status.PASS,
						"Successfully clicked on Map Toggle Button and map not present anymore");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "clickMapToogleButton", Status.FAIL,
						"Even after clicking on Map Toggle Button Map Present");
			}

		}
	}

	/*
	 * Method:clickFirstAvailableUnitView Description:compare checkout date in
	 * screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void clickFirstAvailableUnitView() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ObjResortCardViewLabel, 120);
		if (getList(ObjResortCardViewLabel).size() > 0) {
			WebElement viewclk = getList(ObjResortCardViewLabel).get(0);
			scrollDownForElementJSWb(viewclk);
			clickElementJSWithWait(viewclk);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "clickAvailableUnit", Status.FAIL, "Resort Card not present");
		}

	}

	/*
	 * Method: verifyresortAddressInEachResortCard Description:compare checkout date
	 * in screen and selected Date field Date: Mar/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyResortAddressInEachResortCard() {
		String checkResortAddress = null;
		for (int resortCardIterator = 1; resortCardIterator <= getList(resortCard).size(); resortCardIterator++) {
			WebElement webelementResortAddress = getObject(
					By.xpath("(//p[@class = 'resort-card__address'])[" + resortCardIterator + "]"));
			if (verifyObjectDisplayed(webelementResortAddress)) {
				checkResortAddress = "Y";
			} else {
				checkResortAddress = "N";
				break;
			}
		}
		if (checkResortAddress == "Y") {
			tcConfig.updateTestReporter("CUISearchPage", "verifyresortAddressInEachResortCard", Status.PASS,
					"Resort Address present in each resort card");
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyresortAddressInEachResortCard", Status.FAIL,
					"Resort Address not present in each resort card");
		}

	}

	/*
	 * Method: verifyUnitImageNumber Description:Verify Unit Image Number Date field
	 * Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyUnitImageNumber() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(unitImageList).size() == getList(unitImageNumber).size()) {
			ArrayList<String> imageAttributesClass = new ArrayList<>();
			for (WebElement imageListNumber : getList(unitImageNumber)) {
				imageAttributesClass.add(imageListNumber.getAttribute("class"));
			}
			if (imageAttributesClass.contains("is-active")) {
				tcConfig.updateTestReporter("CUISearchPage", "verifyUnitImageNumber", Status.PASS,
						"Unit Image Number and Total Unit Image Present Matched");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "verifyUnitImageNumber", Status.FAIL,
						"Image Number does not contains active in any of the image button");
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "verifyUnitImageNumber", Status.PASS,
					"Unit Image Number and Total Unit Image Present Did Not Matched");
		}
	}

	/*
	 * Method: checkPriceBreakDownAssociated Description:Verify Price associated
	 * with Each dates Date field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkPriceBreakDownAssociated() throws Exception {
		String checkindate = testData.get("Checkindate").trim();
		String checkoutdate = testData.get("Checkoutdate").trim();
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		long dateDifferencevalue = dateDifference("dd MMM yyyy", checkindate, checkoutdate);
		getElementInView(headerPriceBreakdown);
		for (int pricebreakdowniterator = 1; pricebreakdowniterator <= dateDifferencevalue; pricebreakdowniterator++) {
			String dateValueFromScreen = getObject(
					By.xpath("(//h2[text() = 'Price Breakdown']/..//li//span[contains(@class,'left')])["
							+ pricebreakdowniterator + "]")).getText().trim();
			String priceValueFromScreen = getObject(
					By.xpath("(//h2[text() = 'Price Breakdown']/..//li//span[contains(@class,'right')])["
							+ pricebreakdowniterator + "]")).getText().trim();
			if (dateValueFromScreen.isEmpty() || dateValueFromScreen == null || priceValueFromScreen.isEmpty()
					|| priceValueFromScreen == null) {
				tcConfig.updateTestReporter("CUISearchPage", "checkPriceBreakdownDownAssociated", Status.FAIL,
						"Either date not displayed or price not displayed in Price Breakdown Area");
				break;
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkPriceBreakdownDownAssociated", Status.PASS,
						"Price Associated with date: " + dateValueFromScreen + " is: " + priceValueFromScreen);
			}
		}
	}

	/*
	 * Method: validateCheckInDateInPriceBreakDownArea Description:Verify CheckIn
	 * Date in Price BrreakDown Area Date field Date: May/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void validateCheckInDateInPriceBreakDownArea() throws Exception {
		String checkindate = testData.get("Checkindate").trim();
		String dateValueFromScreen = getObject(checkindateInPriceBreakDownArea).getText().trim();
		if (dateValueFromScreen.isEmpty() || dateValueFromScreen == null) {
			tcConfig.updateTestReporter("CUISearcPage", "checkindateInPriceBreakDownArea", Status.FAIL,
					"Check In date from Price BreakDown area is not present in screen");

		} else {
			String convertedcheckindate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkindate);
			String converteddateValueFromScreen = convertDateFormat("MMMM dd,yyyy", "MM/dd/yy", dateValueFromScreen);
			if (convertedcheckindate.compareTo(converteddateValueFromScreen) == 0) {
				tcConfig.updateTestReporter("CUISearcPage", "checkindateInPriceBreakDownArea", Status.PASS,
						"Check in date selected is : " + checkindate + " and from screen is: " + dateValueFromScreen);
			} else {
				tcConfig.updateTestReporter("CUISearcPage", "checkindateInPriceBreakDownArea", Status.FAIL,
						"Check in date selected and from screen in pricebreakdown area did not matched");
			}
		}
	}

	/*
	 * Method: validateCheckoutDatePriceBreakDownArea Description:Verify CheckOut
	 * Date in Price BrreakDown Area Date field Date: May/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void validateCheckoutDatePriceBreakDownArea() throws Exception {
		if (verifyObjectDisplayed(textCheckOutDaeInPriceBreakDownArea)) {
			String checkoutdate = testData.get("Checkoutdate").trim();
			String dateValueFromScreen = getObject(checkoutdateInPriceBreakDownArea).getText().trim();
			if (dateValueFromScreen.isEmpty() || dateValueFromScreen == null) {
				tcConfig.updateTestReporter("CUISearcPage", "validateCheckoutDatePriceBreakDownArea", Status.FAIL,
						"Check out date from Price BreakDown area is not present in screen");

			} else {
				String convertedcheckoutdate = convertDateFormat("dd MMMM yyyy", "MM/dd/yy", checkoutdate);
				String converteddateValueFromScreen = convertDateFormat("MMMM dd,yyyy", "MM/dd/yy",
						dateValueFromScreen);
				if (convertedcheckoutdate.compareTo(converteddateValueFromScreen) == 0) {
					tcConfig.updateTestReporter("CUISearcPage", "validateCheckoutDatePriceBreakDownArea", Status.PASS,
							"Check out date selected is : " + checkoutdate + " and from screen is: "
									+ dateValueFromScreen);
				} else {
					tcConfig.updateTestReporter("CUISearcPage", "validateCheckoutDatePriceBreakDownArea", Status.FAIL,
							"Check out date selected and in screen from pricebreakdown area did not matched");
				}
			}
		} else {
			tcConfig.updateTestReporter("CUISearcPage", "validateCheckoutDatePriceBreakDownArea", Status.FAIL,
					"Check out date text not found in Price Breakdown Area");
		}

	}

	/*
	 * Method: checkDateIntervalPriceBreakdown Description:Verify Interval Between
	 * dates in Price Breakdown Area Date field Date: May/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void checkDateIntervalPriceBreakdown() throws Exception {
		String checkindate = testData.get("Checkindate").trim();
		String checkoutdate = testData.get("Checkoutdate").trim();
		waitUntilElementVisibleBy(driver, headerPriceBreakdown, 120);
		if (verifyObjectDisplayed(headerPriceBreakdown)) {
			getElementInView(headerPriceBreakdown);
			long dateDifferencevalue = dateDifference("dd MMM yyyy", checkindate, checkoutdate);
			for (int pricebreakdowniterator = 1; pricebreakdowniterator < dateDifferencevalue; pricebreakdowniterator++) {
				String preDateValue = getObject(
						By.xpath("(//h2[text() = 'Price Breakdown']/..//li//span[contains(@class,'left')])["
								+ pricebreakdowniterator + "]")).getText().trim();
				String postDateValue = getObject(
						By.xpath("(//h2[text() = 'Price Breakdown']/..//li//span[contains(@class,'left')])["
								+ (pricebreakdowniterator + 1) + "]")).getText().trim();
				if (preDateValue.isEmpty() || preDateValue == null || postDateValue.isEmpty()
						|| postDateValue == null) {
					tcConfig.updateTestReporter("CUISearchPage", "checkDateIntervalPriceBreakdown", Status.FAIL,
							"Date from screen in price breakdown area are empty or nul");
					break;
				} else {
					long intervalBetweenDates = dateDifference("MMMM dd,yyyy", preDateValue, postDateValue);
					if (intervalBetweenDates == 1) {
						tcConfig.updateTestReporter("CUISearchPage", "checkDateIntervalPriceBreakdown", Status.PASS,
								"Date at Index " + pricebreakdowniterator + " is: " + preDateValue + " while at index "
										+ (pricebreakdowniterator + 1) + " is: " + postDateValue);
					} else {
						tcConfig.updateTestReporter("CUISearchPage", "checkDateIntervalPriceBreakdown", Status.FAIL,
								"Mismatch Between date intervals at Unit Details PopUp in Price Break Down Area");
					}
				}
			}
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkDateIntervalPriceBreakdown", Status.FAIL,
					"Price Break Down Header Not Present");
		}

	}

	/*
	 * Method: clickfirstAvailableAccessibleUnit Description:Click First Available
	 * Accessible Unit Date field Date: May/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickFirstAvailableAccessibleUnit() {
		String checkAccessibleUnitPresence = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (getList(ObjResortCardViewLabel).size() > 0) {
			for (int resortViewIterator = 0; resortViewIterator < getList(ObjResortCardViewLabel)
					.size(); resortViewIterator++) {
				clickResortCardViewLabel(resortViewIterator);
				if (getList(checkAccessibleUnit).size() > 0) {
					WebElement viewclk = getList(checkAccessibleUnit).get(0);
					scrollDownForElementJSWb(viewclk);
					clickElementJSWithWait(viewclk);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					checkAccessibleUnitPresence = "Y";
					break;
				} else {
					clickElementJSWithWait(hideResortElement);
				}
			}
			if (checkAccessibleUnitPresence.equals("Y")) {
				tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnit", Status.PASS,
						"Accessible Unit present");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnit", Status.FAIL,
						"Accessible Unit not present for single resort");
			}

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "checkAccessibleUnit", Status.FAIL, "Resort Card not present");
		}
	}

	/*
	 * Method: validateAccesibleUnitDetailsInUnitDetailsPopup Description:Validate
	 * Accessible Units Details in Unit Details Popup Date field Date: May/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void validateAccessibleUnitDetailsInUnitDetailsPopup() {
		waitUntilElementVisibleBy(driver, headerAccessibleUnit, 120);
		if (verifyObjectDisplayed(headerAccessibleUnit)) {
			if (getList(guaranteedAccessibleFeatures).size() > 0) {
				getElementInView(guaranteedAccessibleFeatures);
				tcConfig.updateTestReporter("CUISearchPage", "validateAccesibleUnitDetailsInUnitDetailsPopup",
						Status.PASS, "Guaranteed Accessible Features area Present");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateAccesibleUnitDetailsInUnitDetailsPopup",
						Status.PASS, "Guaranteed Accessible Features area not Present");
			}

			if (getList(nonguaranteedAccessibleFeatures).size() > 0) {
				tcConfig.updateTestReporter("CUISearchPage", "validateAccesibleUnitDetailsInUnitDetailsPopup",
						Status.PASS, "Non Guaranteed Accessible Features area Present");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "validateAccesibleUnitDetailsInUnitDetailsPopup",
						Status.PASS, "Non Guaranteed Accessible Features area not Present");
			}
		}

	}

	/*
	 * Method: clickBookButtonOfHigherCostThanAccountPoints
	 * Description:clickBookButtonOfHigherCostThanAccountPoints Date field Date:
	 * June/2020 Author: Monideep Roy Changes By: NA
	 */
	public void clickBookButtonOfHigherCostThanAccountPoints() {

		boolean blnUnitFoundFlag = false;
		int availablePoints = parsePoints(driver.findElement(accountPoints));
		tcConfig.getTestData().put("AccountPoints", availablePoints + "");

		List<WebElement> listOfPrices = getList(unitPriceObj);

		if (listOfPrices.size() == 0) {
			listOfPrices = getList(unitPriceObjNoDiscount);
		}

		for (int i = 0; i < listOfPrices.size(); i++) {

			int unitPricePoint = parsePoints(listOfPrices.get(i));
			if (unitPricePoint > availablePoints) {

				driver.findElement(By.xpath("(//a[text()='Book'])[" + (i + 1) + "]")).click();
				waitUntilElementVisibleBy(driver, textBookDetails, 120);
				if (verifyObjectDisplayed(textBookDetails)) {
					tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.PASS,
							"Successfully clicked on Book Button, Unit Value : " + unitPricePoint);
				} else {
					tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.FAIL,
							"Failed to click on Book Button");
				}
				blnUnitFoundFlag = true;
				tcConfig.getTestData().put("UnitPoints", unitPricePoint + "");
				break;
			}
		}

		if (!blnUnitFoundFlag) {
			tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.FAIL,
					"Failed to find unit with cost higher than account balance");
		}

	}

	/*
	 * Method: parsePoints Description: Parse Points Date field Date: June/2020
	 * Author: Monideep Roy Changes By: NA
	 */
	protected int parsePoints(WebElement ele) {
		int accountPointsValue = Integer.parseInt(getElementText(ele).split(" ")[0].replace(",", ""));
		return accountPointsValue;

	}

	/*
	 * Method: verifyBackToAvailability Description: Parse Points Date field Date:
	 * June/2020 Author: Kamalesh Roy Changes By: NA
	 */
	public void verifyBackToAvailability() {
		String unitForBooking = testData.get("bookUnitType");
		if (getList(totalBookButton).size() > 0) {
			clickElementJSWithWait(getObject(By.xpath("//a[contains(text(),'" + unitForBooking
					+ "')]/ancestor::div[contains(@class,'resort-card__unit')]//a[contains(text(),'Book')]")));
			tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
					"Book Button Cliked against " + testData.get("bookUnitType"));
		}
		waitUntilElementVisibleBy(driver, BackToAvailabilityButton, 120);
		if (testData.get("BackToAvailabilityHeader").equals("NotEnoughPoints")) {
			Assert.assertTrue(verifyObjectDisplayed(notEnoughPointsHeader),
					"Not Enough Points Pop Up Modal is not displayed");
			tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
					"Not Enough Points Pop Up Modal is displayed and the title is present as: "
							+ getElementText(notEnoughPointsHeader));

			Assert.assertTrue(verifyObjectDisplayed(notEnoughPointsText), "An instruction text is not present");
			tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
					"An instruction text is present as: " + getElementText(notEnoughPointsText));

		} else if (testData.get("BackToAvailabilityHeader").equals("MaxReservationsExceeded")) {
			Assert.assertTrue(verifyObjectDisplayed(maxReservationsExceededHeader),
					"Max Reservations Exceeded Pop Up Modal is not displayed");
			tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
					"Max Reservations Exceeded Pop Up Modal is displayed and the title is present as: "
							+ getElementText(maxReservationsExceededHeader));

			Assert.assertTrue(verifyObjectDisplayed(maxReservationsExceededText), "An instruction text is not present");
			tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
					"An instruction text is present as: " + getElementText(maxReservationsExceededText));
		}

		Assert.assertTrue(verifyObjectDisplayed(warningSymbol), "A warning sign is not present on the Pop Up Modal");
		tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
				"A warning sign is present on the Pop Up Modal");

		Assert.assertTrue(
				verifyObjectDisplayed(
						getList(closeBackToAvailability).get(getList(closeBackToAvailability).size() - 1)),
				"Close symbol is not present");
		tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
				"Close symbol is present to close the Pop Up Modal");

		Assert.assertTrue(verifyObjectDisplayed(BackToAvailabilityButton),
				"Back To Availability Button is not present");
		tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
				"Back To Availability Button is present");
		clickElementBy(BackToAvailabilityButton);
		Assert.assertTrue(verifyObjectDisplayed(firstResort), "Back To Availability Button is not present");
		tcConfig.updateTestReporter("CUISearchPage", "verifyBackToAvailability", Status.PASS,
				"On Clicking Back To Availability Button, user navigated to the Search Availability results page");
	}

	/*
	 * Method: validateUnableToBookModal Description: Validate Unable to book modal
	 * popup Date: June/2020 Author: Monideep Roychowdhury Changes By: NA
	 */
	public void validateUnableToBookModal() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(warningIcon), "Warning Icon is not visible");
		Assert.assertTrue(verifyObjectDisplayed(closeModalCTA), "Close modal CTA is not visible");
		Assert.assertTrue(verifyObjectDisplayed(backToAvailabilityBtn), "Back to availablility button is not visible");
		Assert.assertTrue(verifyObjectDisplayed(unableToBookMsg),
				"Message displayed on modal popup doesn't match expected message");
		tcConfig.updateTestReporter("CUISearchPage", "validateUnableToBookModal", Status.PASS,
				"Unable to Book Modal popup has been validated successfully");
	}

	/*
	 * Method: clickCloseModalCTA Description: Click on Close CTA Date: June/2020
	 * Author: Monideep Roychowdhury Changes By: NA
	 */
	public void clickCloseModalCTA() {
		clickElementBy(closeModalCTA);
		tcConfig.updateTestReporter("CUISearchPage", "clickCloseModalCTA", Status.PASS,
				"Close CTA has been clicked successfully");
	}

}
