package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NextGenLegalLinks_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenLegalLinks_Web.class);

	public NextGenLegalLinks_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// termsLink
	@FindBy(xpath = /* "//div[@gtm_component='footer']//a[contains(.,'Terms')]" */
	"//div[contains(@class,'footer')]//a[contains(.,'Terms')]")
	WebElement termsFooterLink;
	protected By termsHeader = By.xpath(/* "//div[@class='wyn-headline ']/h2[contains(.,'Terms')]" */
			"(//div[contains(@class,'title-1') and contains(text(),'TERMS')])[1]");
	protected By termsHeroImg = By.xpath(/* "//div[@class='wyn-hero__image']//img" */
			"(//div[contains(@class,'title-1') and contains(text(),'TERMS')])[1]/../../../..//div[@class='image ']/img");
	protected By termsNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Term')]");
	protected By termsLeftHdr = By.xpath("//h3/span[contains(.,'Terms')]");
	protected By privacyLinks = By.xpath("//div[@gtm_component_type='text']//h3//a");
	protected By termsSideNav = By.xpath("//h3[contains(.,'Terms')]/following-sibling::p/a");
	protected By termsMainHeaders = By.xpath("//div[@gtm_component_type='text']//h4");
	protected By copyrightMailLink = By.xpath("//a[contains(.,'Copyright.Violations@wyn.com')]");
	protected By adrRulesLink = By.xpath("//p[contains(.,'https://www.adr.org')]");

	// privacyNotice
	protected By privacyNoticeLinks = By.xpath(/*
												 * "//div[@gtm_component_type='text']//h3//a[contains(.,'Privacy Notice')]"
												 */
			"//div[contains(@class,'footer')]//a[contains(.,'Privacy Notice')]");
	protected By privacyNoticeHdr = By.xpath(/* "//div[@class='wyn-headline ']/h2[contains(.,'Privacy Notice')]" */
			"(//div[contains(@class,'title-1') and contains(text(),'PRIVACY')])[1]");
	protected By privacyNoticeImg = By.xpath(/* "//div[@class='wyn-hero__image']//img" */
			"(//div[contains(@class,'title-1') and contains(text(),'PRIVACY')])[1]/../../../..//div[@class='image ']/img");
	protected By privacyNtcNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Privacy Notice')]");
	protected By privacyNtcSideHdr = By.xpath("//h3/span[contains(.,'Privacy Notice')]");
	protected By privacyNtsSideNav = By.xpath("//h3[contains(.,'Notice')]/following-sibling::p/a");
	protected By privacyNtsMainHdrs = By.xpath("//h2[contains(.,'Privacy Notice')]/following-sibling::h3");
	protected By privacyAdsLink = By.xpath("//a[contains(.,'www.aboutads')]");
	protected By privacyNetworkLink = By.xpath("//a[contains(.,'www.networkadv')]");
	protected By privacyGoogleLink = By.xpath("//a[contains(.,'http://tools.google.com')]");
	protected By privacyMailAddress = By.xpath("//a[contains(.,'Privacy@wyn.com')]");
	protected By privacyAddress = By.xpath("//p[contains(.,'6277 Sea Harbor')][1]");
	protected By privacyLastUpdate = By.xpath("//p[contains(.,'Last Updated')]");

	// privacy Setting
	protected By privacyStngLinks = By.xpath(/*
												 * "//div[@gtm_component_type='text']//h3//a[contains(.,'Privacy Setting')]"
												 */
			"//div[contains(@class,'footer')]//a[contains(.,'Privacy Settings')]");
	protected By privacySettingHdr = By.xpath(/* "//div[@class='wyn-headline ']/h2[contains(.,'Privacy Setting')]" */
			"(//div[contains(@class,'title-1') and contains(text(),'PRIVACY')])[1]");
	protected By privacySettingImg = By.xpath(/* "//div[@class='wyn-hero__image']//img" */
			"(//div[contains(@class,'title-1') and contains(text(),'PRIVACY')])[1]/../../../..//div[@class='image ']/img");
	protected By privacyStngNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Privacy Setting')]");
	protected By privacyStngSideHdr = By.xpath("//h3/span[contains(.,'Privacy Setting')]");
	protected By leftRailLinks = By.xpath("//h3/span/a");
	protected By privacyStngSubHdr = By.xpath(/* "//h3[contains(.,'User Generated')]" */
			"//h4[contains(.,'User Generated')]");

	/*
	 * Method: termsOfUseNavigations Description: Terms of Use Navigation: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void termsOfUseNavigations() {

		waitUntilElementVisibleWb(driver, termsFooterLink, "120");
		getElementInView(termsFooterLink);
		if (verifyElementDisplayed(termsFooterLink)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.PASS,
					"Terms of use Link Present");

			if (browserName.equalsIgnoreCase("EDGE")) {
				clickElementJSWithWait(termsFooterLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				scrollDownForElementJSWb(termsFooterLink);
				scrollUpByPixel(150);
				Actions action = new Actions(driver);
				action.moveToElement(termsFooterLink);
				action.click(termsFooterLink);
				Action ac = action.build();
				ac.perform();
			}
			driver.switchTo().activeElement().sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(termsHeader)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.PASS,
						"Navigated to Terms of Use Link page");

				if (verifyObjectDisplayed(termsHeroImg)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.PASS,
							"Terms of Use Image present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.FAIL,
							"Terms of Use Image not present");

				}

				if (verifyObjectDisplayed(termsNav)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.PASS,
							"Terms of Use Navigation present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.FAIL,
							"Terms of Use Navigation not present");

				}

				/*
				 * if (verifyObjectDisplayed(termsLeftHdr)) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations",
				 * Status.PASS, "Terms of Use Left Rail Header present");
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "termsOfUseNavigations", Status.FAIL,
				 * "Terms of Use Left Rail Header not  present");
				 * 
				 * }
				 */
				/*
				 * List<WebElement> leftRailSubLinks = driver.findElements(termsSideNav);
				 * List<WebElement> mainHeaders = driver.findElements(termsMainHeaders);
				 * 
				 * for (int i = 0; i < mainHeaders.size(); i++) {
				 * getElementInView(mainHeaders.get(i)); if
				 * (mainHeaders.get(i).getText().toUpperCase()
				 * .contains(leftRailSubLinks.get(i).getText().toUpperCase())) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations",
				 * Status.PASS, "The " + (i + 1) +
				 * " header in main content and left rail nav is is: " +
				 * mainHeaders.get(i).getText()); } else {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations",
				 * Status.FAIL, "The " + (i + 1) +
				 * " header does not matches with the left rail header"); }
				 * 
				 * }
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "termsOfUseNavigations", Status.FAIL,
				 * "Navigation to Terms of Use link failed");
				 * 
				 * }
				 * 
				 * getElementInView(copyrightMailLink);
				 * 
				 * if (verifyObjectDisplayed(copyrightMailLink)) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations",
				 * Status.PASS, "Copy right Mail link is present");
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "termsOfUseNavigations", Status.FAIL, "Copy right Mail link is not present");
				 * 
				 * }
				 * 
				 * getElementInView(adrRulesLink);
				 * 
				 * if (verifyObjectDisplayed(adrRulesLink)) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations",
				 * Status.PASS, "ADR rules link is present");
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "termsOfUseNavigations", Status.FAIL, "ADR rules link is not present");
				 * 
				 * }
				 * 
				 * try { List<WebElement> privacyLinkList = driver.findElements(privacyLinks);
				 * 
				 * tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations",
				 * Status.PASS, "Side Nav Legal Link present as: " +
				 * privacyLinkList.get(0).getText() + " and " +
				 * privacyLinkList.get(1).getText());
				 * 
				 * } catch (Exception e) { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "termsOfUseNavigations", Status.FAIL, "Side Nav privacy links not present");
				 * }
				 */
			}
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.FAIL,
					"About us Link not Present");

		}

	}

	/*
	 * Method: privacyNoticeValdns Description: Privacy notice Nav and Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void privacyNoticeValdns() {

		getElementInView(privacyNoticeLinks);
		if (verifyObjectDisplayed(privacyNoticeLinks)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.PASS,
					"Privacy Notice Link present");

			clickElementJSWithWait(privacyNoticeLinks);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(privacyNoticeHdr)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.PASS,
						"Navigated to Privacy Notices Link page");

				if (verifyObjectDisplayed(privacyNoticeImg)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.PASS,
							"Privacy Notices Image present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.FAIL,
							"Privacy Notices Image not present");

				}
				getElementInView(privacyNtcNav);
				if (verifyObjectDisplayed(privacyNtcNav)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.PASS,
							"Privacy Notices Navigation present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.FAIL,
							"Privacy Notices Navigation not present");

				}

				/*
				 * if (verifyObjectDisplayed(privacyNtcSideHdr)) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "Privacy Notices Left Rail Header present");
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL,
				 * "Privacy Notices Left Rail Header not  present");
				 * 
				 * }
				 * 
				 * List<WebElement> leftRailSubLinks = driver.findElements(privacyNtsSideNav);
				 * List<WebElement> mainHeaders = driver.findElements(privacyNtsMainHdrs);
				 * 
				 * for (int i = 0; i < mainHeaders.size(); i++) {
				 * getElementInView(mainHeaders.get(i)); String strMainHdrs =
				 * mainHeaders.get(i).getText(); if (i==0) { if
				 * (mainHeaders.get(0).getText().contains(":")) { strMainHdrs =
				 * mainHeaders.get(0).getText().replace(":", ""); } }
				 * 
				 * if (strMainHdrs.toUpperCase().contains(leftRailSubLinks.get(i).getText().
				 * toUpperCase())) { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.PASS, "The " + (i + 1) +
				 * " header in main content and left rail nav is is: " +
				 * mainHeaders.get(i).getText()); } else {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.FAIL, "The " + (i + 1) +
				 * " header does not matches with the left rail header"); }
				 * 
				 * }
				 * 
				 * if (verifyObjectDisplayed(privacyAdsLink)) {
				 * getElementInView(privacyAdsLink);
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "Privacy Notices Ads link present");
				 * 
				 * clickElementBy(privacyAdsLink);
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 * if (driver.getCurrentUrl().contains(testData.get("header1"))) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "Navigated To About Ads page"); } else {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.FAIL, "Navigation To About Ads page failed"); }
				 * 
				 * driver.navigate().back();
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL, "Privacy Notices Ads link not present");
				 * 
				 * }
				 * 
				 * if (verifyObjectDisplayed(privacyNetworkLink)) {
				 * getElementInView(privacyNetworkLink);
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "Privacy Notices Network link present");
				 * 
				 * clickElementBy(privacyNetworkLink);
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 * if (driver.getCurrentUrl().contains(testData.get("header2"))) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "Navigated To Network Adv page"); } else {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.FAIL, "Navigation To Network Adv page failed"); }
				 * 
				 * driver.navigate().back();
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL,
				 * "Privacy Notices Network link not present");
				 * 
				 * }
				 * 
				 * if (verifyObjectDisplayed(privacyGoogleLink)) {
				 * getElementInView(privacyGoogleLink);
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "Privacy Notices Google link present");
				 * 
				 * clickElementBy(privacyGoogleLink);
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 * if (driver.getCurrentUrl().contains(testData.get("header3"))) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "Navigated To Google Tools page"); } else {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.FAIL, "Navigation To Google page failed"); }
				 * 
				 * driver.navigate().back();
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL, "Privacy Notices Google not present");
				 * 
				 * }
				 * 
				 * if (verifyObjectDisplayed(privacyMailAddress)) {
				 * getElementInView(privacyMailAddress);
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "privacy Mail Address present as: " +
				 * getElementText(privacyMailAddress));
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL,
				 * "Privacy Notices Mail Address not present");
				 * 
				 * }
				 * 
				 * if (verifyObjectDisplayed(privacyAddress)) {
				 * getElementInView(privacyAddress);
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "privacy Address present as: " +
				 * getElementText(privacyAddress));
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL, "Privacy Notices Address not present");
				 * 
				 * }
				 * 
				 * if (verifyObjectDisplayed(privacyLastUpdate)) {
				 * getElementInView(privacyLastUpdate);
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns",
				 * Status.PASS, "privacy Mail Last Update date present as: " +
				 * getElementText(privacyLastUpdate));
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL,
				 * "Privacy Notices Last Update date  not present");
				 * 
				 * }
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacyNoticeValdns", Status.FAIL,
				 * "Navigation to Privacy Notices link failed");
				 * 
				 * }
				 */
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.FAIL,
					"Privacy Notice Link not present");
		}

	}

	/*
	 * Method: privacySettingsValdns Description:Privacy Settings Nav and
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void privacySettingsValdns() {

		getElementInView(privacyStngLinks);
		if (verifyObjectDisplayed(privacyStngLinks)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.PASS,
					"Privacy Setting Link present");

			clickElementJSWithWait(privacyStngLinks);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(privacySettingHdr)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.PASS,
						"Navigated to Privacy Settings Link page");

				if (verifyObjectDisplayed(privacySettingImg)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.PASS,
							"Privacy Settings Image present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.FAIL,
							"Privacy Settings Image not present");

				}

				if (verifyObjectDisplayed(privacyStngNav)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.PASS,
							"Privacy Settings Navigation present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.FAIL,
							"Privacy Settings Navigation not present");

				}

				/*
				 * if (verifyObjectDisplayed(privacyStngSideHdr)) {
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns",
				 * Status.PASS, "Privacy Settings Left Rail Header present");
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacySettingsValdns", Status.FAIL,
				 * "Privacy Settings Left Rail Header not  present");
				 * 
				 * }
				 * 
				 * try { List<WebElement> leftRailLinksList =
				 * driver.findElements(leftRailLinks);
				 * 
				 * tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns",
				 * Status.PASS, "Side Nav Legal Link present as: " +
				 * leftRailLinksList.get(0).getText() + " and " +
				 * leftRailLinksList.get(1).getText());
				 * 
				 * } catch (Exception e) { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "privacySettingsValdns", Status.FAIL, "Side Nav links not present"); }
				 */

				if (verifyObjectDisplayed(privacyStngSubHdr)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.PASS,
							"Privacy Settings Sub Header present as: " + getElementText(privacyStngSubHdr));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.FAIL,
							"Privacy Settings subHeader not  present");

				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.FAIL,
						"Navigation to Privacy Settings link failed");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.FAIL,
					"Privacy Setting Link not present");
		}

	}

}
