package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CUIModifySuccessPage_Web extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(CUIModifySuccessPage_Web.class);

	public CUIModifySuccessPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	protected By modifyTravelConfirm = By.xpath("//h1[text() = 'Modify Traveler Confirmation'] | //div[text() = 'Modify Traveler Confirmation']");
	protected By modifyVIPUpgradeConfirmation = By.xpath("//h1[text() = 'Modify VIP Upgrade Confirmation'] | //div[text() = 'Modify VIP Upgrade Confirmation']");
	protected By textCongrats = By.xpath("//h2[contains(.,'Congratulations')] | //div[contains(@class,'congrats')]");
	protected By confirmModifyTraveler = By.xpath("//p[contains(.,'changed the traveler')]");
	protected By textModificationSummary = By.xpath("//h1[text() = 'Modification Summary'] | //h1[text() = 'Modification Summary']");
	protected By textModificationDetails = By.xpath("//h2[text() = 'Modification Details'] | //div[text() = 'Modification Details']");
	protected By sectionCancelPolicy = By.xpath("//h2[text()  = 'Cancellation Policy']/..");
	protected By modifiedTravelerName = By.xpath("//th[text() = 'Traveler']/following-sibling::td/span");
	protected By modifiedDate = By.xpath("//th[text() = 'Date Modified']/../td/strong | //th[text() = 'Date Modified']/../td/span");
	protected By modifiedBy = By.xpath("//th[contains(text(),'Modified by')]/../td/strong | //th[contains(text(),'Modified by')]/../td/span");
	protected By reservationDetailsCTA = By.xpath("//a[text() = 'View Reservation Details']");
	protected By sectionReservationSummary = By.xpath("//section[@class = 'reservation-details-summary']");
	protected By makeAnotherModification = By.xpath("//a[text() = 'Make Another Modification']");
	
	public void textModifyTravelConfirm(){
		waitUntilElementVisibleBy(driver, modifyTravelConfirm, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(modifyTravelConfirm), "Modify Travel Confirmation Text not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "textModifyTravelConfirm", Status.PASS,
				"Modify Travel Confirmation text present");
	}
	
	public void textVIPUpgradeConfirm(){
		waitUntilElementVisibleBy(driver, modifyVIPUpgradeConfirmation, "ExplicitLongWait");
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		Assert.assertTrue(verifyObjectDisplayed(modifyVIPUpgradeConfirmation), "Modify VIP Upgrade Confirmation Text not present");
		tcConfig.updateTestReporter("CUIModifySuccessPage", "textVIPUpgradeConfirm", Status.PASS,
				"Modify VIP Upgrade Confirmation text present");
	}
	
	public void textCongratsVerify(){
		if (verifyObjectDisplayed(textCongrats)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textCongratsVerify", Status.PASS,
					"Congrats Present as : "+getElementText(textCongrats));
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textCongratsVerify", Status.FAIL,
					"Congrats text not present");
		}
	}
	
	public void textModifyTravelerConfirm(){
		if (verifyObjectDisplayed(confirmModifyTraveler)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModifyTravelerConfirm", Status.PASS,
					""+getElementText(confirmModifyTraveler));
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModifyTravelerConfirm", Status.FAIL,
					"Modify Traveler confirmation not present");
		}
	}
	
	public void textModificationSummary(){
		if (verifyObjectDisplayed(textModificationSummary)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationSummary", Status.PASS,
					"Text Modification Summary Present");
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationSummary", Status.FAIL,
					"Text Modification Summary not Present");
		}
	}
	
	public void textModificationDetails(){
		if (verifyObjectDisplayed(textModificationDetails)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationDetails", Status.PASS,
					"Text Modification Details Present");
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "textModificationDetails", Status.FAIL,
					"Text Modification Details not Present");
		}
	}
	
	
	public void verifyModifiedName(){
		String modifyToOwner = testData.get("ModifyOwner");
		if (getElementText(modifiedTravelerName).equals(modifyToOwner)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedName", Status.PASS,
					"Modified Traveler is: "+getElementText(modifiedTravelerName));
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedName", Status.FAIL,
					"Owner changed and modified traveler from screen did not match");
		}
	}
	
	public void verifyModifiedDate(){
		if (verifyObjectDisplayed(modifiedDate)) {
			if (isValidDate(getElementText(modifiedDate), "M/dd/yyyy")) {
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedDate", Status.PASS,
					"Modified date is: "+getElementText(modifiedDate));
			}else{
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedDate", Status.FAIL,
						"Modified date is not in required format");
			}
		}
	}
	
	public void verifyModifiedBy(){
		if (verifyObjectDisplayed(modifiedBy)) {
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedBy", Status.PASS,
					"Modified By: "+getElementText(modifiedBy));
			}else{
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyModifiedDate", Status.FAIL,
						"Modified By field not found");
			}
		}
	
	public void verifyReservationDetailsCTA(){
		if (verifyObjectDisplayed(reservationDetailsCTA)) {
			clickElementJSWithWait(reservationDetailsCTA);
			waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
			if (verifyObjectDisplayed(sectionReservationSummary)) {
				navigateBack();
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyReservationDetailsCTA", Status.PASS,
						"Reservation Details CTA present and navigated to reservation page");
			}else{
				tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyReservationDetailsCTA", Status.FAIL,
						"Failed to navigate to reservation page");
			}
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyReservationDetailsCTA", Status.FAIL,
					"Reservation Details CTA not present");
		}
	}
	
	public void verifySectionCancel(){
		waitUntilElementVisibleBy(driver, sectionCancelPolicy, "ExplicitLongWait");
		if (verifyObjectDisplayed(sectionCancelPolicy)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifySectionCancel", Status.PASS,
					"Cancel Policy Section Present");
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifySectionCancel", Status.FAIL,
					"Cancel Policy Section not Present");
		}
	}
	
	public void verifyMakeAnotherModificationLink(){
		if (verifyObjectDisplayed(makeAnotherModification)) {
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyMakeAnotherModificationLink", Status.PASS,
					"Make Another Modification link present");
		}else{
			tcConfig.updateTestReporter("CUIModifySuccessPage", "verifyMakeAnotherModificationLink", Status.FAIL,
					"Make Another Modification link not present");
		}
	}
}

