package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUITripPlannerYourBudgetPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUITripPlannerYourBudgetPage_Web.class);

	public CUITripPlannerYourBudgetPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	//WebElement labelPointToSpend2 = driver.findElement(By.xpath("//div[@class='subtitle-2 text-center' and contains(.,'How many points would you like to use?')]"));
	protected By hamburger = By.xpath("//button[contains(@class,'hamburger')]");
	protected By hamburgerClose = By.xpath("//button[contains(@class,'hamburger-menu open')]");
	protected By labelPointToSpend = By.xpath("//div[@class='subtitle-2 text-center' and contains(.,'How many points would you like to use?')]");
	protected By fieldPointToSpend = By.xpath("//label[@for='pointsToSpendInput']//input");
	protected By labelInfo = By.xpath("//span[@class='has-tip border-none text-secondary']");
	protected By labelPointToSpendError = By.xpath("//p[@id='helper-pointsToSpendInput' and contains(.,'The number of points you have entered is less than 40,000.')]");
	protected By labelCurrentSpendYear = By.xpath("(//table[contains(@class,'points-calc-input')]/tbody/tr/td)[1]");
	protected By labelNextSpendYear = By.xpath("(//table[contains(@class,'points-calc-input')]/tbody/tr/td)[3]");
	protected By labelCurrentSpendYearPoints = By.xpath("(//table[contains(@class,'points-calc-input')]/tbody/tr/td)[2]");
	protected By labelNextSpendYearPoints = By.xpath("(//table[contains(@class,'points-calc-input')]/tbody/tr/td)[4]");
	protected By buttonNext = By.xpath("(//button[contains(@class,'button expanded') and contains(.,'Next')])[1] | (//button[contains(@class,'button expanded') and contains(.,'Next')])[2]");
	protected By labelVacationingWith = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'Who are you vacationing with? ')]");
	
	/*
	 * Method: validateLessPointsError Description:validate Less Points Error
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void validateLessPointsError() {
		waitUntilElementVisibleBy(driver, labelPointToSpend, 120);
		Assert.assertFalse(getObject(buttonNext).isEnabled(),"Nextbutton is Enabled");
		driver.findElement(fieldPointToSpend).sendKeys(testData.get("negativePointsToSpend"));
		driver.findElement(fieldPointToSpend).sendKeys(Keys.TAB);
		getElementInView(fieldPointToSpend);
		Assert.assertTrue(verifyObjectDisplayed(labelPointToSpendError),"Error message is not displayed");	
		tcConfig.updateTestReporter("CUITripPlannerYourBudgetPage", "validateLessPointsError", Status.PASS,
				"Error message is displayed");
		
		driver.findElement(fieldPointToSpend).sendKeys(Keys.BACK_SPACE);
		driver.findElement(fieldPointToSpend).sendKeys(Keys.BACK_SPACE);
		driver.findElement(fieldPointToSpend).sendKeys(Keys.BACK_SPACE);
		driver.findElement(fieldPointToSpend).sendKeys(Keys.BACK_SPACE);
		driver.findElement(fieldPointToSpend).sendKeys(Keys.BACK_SPACE);
		driver.findElement(fieldPointToSpend).sendKeys(Keys.TAB);
	}

	/*
	 * Method: enterPointsToSpend Description:Enter Points To Spend
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void enterPointsToSpend() {
		
		
		fieldDataEnter(fieldPointToSpend, testData.get("PointToSpend"));
		driver.findElement(fieldPointToSpend).sendKeys(Keys.TAB);
		Assert.assertTrue(getObject(buttonNext).isEnabled(),"Nextbutton is not enabled");
		tcConfig.updateTestReporter("CUITripPlannerYourBudgetPage", "enterPointsToSpend", Status.PASS,
				"Successfully entered "+testData.get("PointToSpend") +" Points");
	}
	
	/*
	 * Method: enterPointsToSpend Description:Enter Points To Spend
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void clickContinueCTA() {
		clickElementJSWithWait(buttonNext);
		waitUntilElementVisibleBy(driver, labelVacationingWith, 120);
		tcConfig.updateTestReporter("CUITripPlannerYourBudgetPage", "clickContinueCTA", Status.PASS,
				"Successfully clicked next button");
	}
	
}
