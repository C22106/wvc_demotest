package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUITripPlannerResultPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUITripPlannerResultPage_Web.class);

	public CUITripPlannerResultPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	protected By labelPageHeader = By.xpath("//div[contains(@class,'title-3 medium') and contains(.,'Your Customized Resort List')]");
	protected By labelYourBudgetPoints = By.xpath("//div[contains(@class,'edit-selections__container--item')]/div/div[contains(@class,'--info')]/div[contains(.,'points')]");
	protected By labelTravelers = By.xpath("(//div[contains(@class,'edit-selections__container--item')]/div/div[contains(@class,'--info')]/div/div)[1]");
	protected By labelTravelSeason = By.xpath("(//div[contains(@class,'edit-selections__container--item')]/div/div[contains(@class,'--info')]/div/div)[2]");
	protected By labelLocation = By.xpath("(//div[contains(@class,'edit-selections__container--item')]/div/div[contains(@class,'--info')]/div/ul)[1]");
	protected By labelExperince = By.xpath("//div[contains(@class,'--info')]/div[contains(.,'Experience')]/parent::div/div/ul/li");
	
	protected By buttonEditYourBudgetPoints = By.xpath("(//div[contains(@class,'__container')]//div[contains(@class,'body-2-link')])[1]");
	protected By buttonEditTravelers = By.xpath("(//div[contains(@class,'__container')]//div[contains(@class,'body-2-link')])[2]");
	protected By buttonEditTravelSeason = By.xpath("(//div[contains(@class,'__container')]//div[contains(@class,'body-2-link')])[3]");
	protected By buttonEditLocation = By.xpath("(//div[contains(@class,'__container')]//div[contains(@class,'body-2-link')])[4]");
	protected By buttonEditExperince = By.xpath("(//div[contains(@class,'__container')]//div[contains(@class,'body-2-link')])[5]");
	
	protected By checkboxRelax = By.xpath("//label[contains(.,'Relax and Play by the Water')]");
	protected By checkboxBreathe = By.xpath("//label[contains(.,'Breathe in the Mountain Air')]");
	protected By checkboxExplore = By.xpath("//label[contains(.,'Explore the City')]");
	protected By checkboxExperience = By.xpath("//label[contains(.,'Experience Attractions')]");
	protected By checkboxHitTheLinks = By.xpath("//label[contains(.,'Hit the Links')]");
	protected By checkboxAnything = By.xpath("//label[contains(.,'Anything')]");
	protected By buttonNewSearch = By.xpath("(//a[@class='button hollow secondary body-1-link expanded'])[1] | (//a[@class='button hollow secondary body-1-link expanded'])[2]");
	protected By firstResort = By.xpath("(//div[contains(@class,'resort-card')]//a)[1]");
	protected By labelPointToSpend = By.xpath("//div[@class='subtitle-2 text-center' and contains(.,'How many points would you like to use?')]");
	protected By labelEditSelections = By.xpath("//span[@class='margin-right-1' and contains(.,'Edit')]");
	/*
	 * Method: validateResultPage Description:validate Result Page
	 *  Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void validateResultPage() {
		Assert.assertTrue(verifyObjectDisplayed(labelPageHeader),"Nextbutton is Enabled");
		//clickElementBy();
		tcConfig.updateTestReporter("CUITripPlannerResultPage", "validateResultPage", Status.PASS,
				"Sccessfully navigated to Result page");
	}
	
	/*
	 * Method: validateResultPage Description:validate Result Page
	 *  Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void validateEditSelection() {
		Assert.assertTrue(verifyObjectDisplayed(labelEditSelections),"Nextbutton is Enabled");
		//clickElementBy();
		tcConfig.updateTestReporter("CUITripPlannerResultPage", "validateResultPage", Status.PASS,
				"Sccessfully navigated to Result page");
	}
	
	/*
	 * Method: validateResultPage Description:validate Result Page
	 *  Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void validateSelection() {
		String labelExp = getObject(labelExperince).getText();
		String flag = getObject(labelYourBudgetPoints).getText().split(" ")[0].trim();
		String labelBudget = flag.replace(",", ""); 
		String labelTravs = getObject(labelTravelers).getText();
		String labelSeason = getObject(labelTravelSeason).getText();
		String labelLoc = getObject(labelLocation).getText();
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(labelBudget.equalsIgnoreCase(testData.get("PointToSpend")),"Your Budget data is not matching");
		Assert.assertTrue(labelTravs.equalsIgnoreCase(testData.get("GroupSize")),"Travellers details is not matching");
		Assert.assertTrue(labelSeason.equalsIgnoreCase(testData.get("Season")),"TRAVEL season is not matching");
		Assert.assertTrue(labelLoc.equalsIgnoreCase(testData.get("VacationPlace")),"Location is not matching");
		Assert.assertTrue(labelExp.equalsIgnoreCase(testData.get("Experiences")),"Experince is not matching");
		tcConfig.updateTestReporter("CUITripPlannerResultPage", "validateSelection", Status.PASS,
				"Sccessfully validated the Selections");
	}
	
	
	
	/*
	 * Method:  Description:
	 *  Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void verifyFirstResort() {
		String flag = testData.get("WillResortShow");
		if (flag.equals("Yes"))
		{
			Assert.assertTrue(verifyObjectDisplayed(firstResort),"Resort not displayed");
			String locationFirst = getObject(firstResort).getText().trim();
			if (verifyObjectDisplayed(firstResort)) {
				tcConfig.updateTestReporter("CUITripPlannerResultPage", "verifyFirstResort", Status.PASS,
					"First resort name is : " + locationFirst);
			} else {
				tcConfig.updateTestReporter("CUITripPlannerResultPage", "verifyFirstResort", Status.FAIL,
					"Resot not found");
			}
		}
		else
		{
			Assert.assertFalse(verifyObjectDisplayed(firstResort),"Resort is displayed");
			if (verifyObjectDisplayed(firstResort)) {
				tcConfig.updateTestReporter("CUITripPlannerResultPage", "verifyFirstResort", Status.FAIL,
					"Resort s found");
			} else {
				tcConfig.updateTestReporter("CUITripPlannerResultPage", "verifyFirstResort", Status.PASS,
					"Resot not found");
			}
		}	
	}
	
	/*
	 * Method: validateNewSearchCTA Description: validate New Search CTA
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void validateNewSearchCTA() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJSWithWait(buttonNewSearch);
		waitUntilElementVisibleBy(driver, labelYourBudgetPoints, 120);
		tcConfig.updateTestReporter("CUITripPlannerExperiencePage", "validateNewSearchCTA", Status.PASS,
				"Successfully clicked new search button");
	}
	
	

}
