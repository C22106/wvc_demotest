package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenDiffResortsVal_Android extends NextGenDiffResortsVal_Web {

	public static final Logger log = Logger.getLogger(NextGenDiffResortsVal_Android.class);

	public NextGenDiffResortsVal_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
