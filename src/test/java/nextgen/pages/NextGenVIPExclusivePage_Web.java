package nextgen.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NextGenVIPExclusivePage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenVIPExclusivePage_Web.class);

	public NextGenVIPExclusivePage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By dealsOffersHeader = By.xpath(/*
												 * "//div[@class='wyn-header__nav-container']//li[@class='has-submenu' and contains(.,'Deals')]"
												 */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Deals & Offers']");

	protected By ownerExNav = By.xpath(/* "//div[@class='wyn-fly-out']//li[contains(.,'Owner Exclu')]" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'Owner Exclusives')]");
	protected By ownerExHdr = By.xpath(/* "//h2[contains(.,'Owner Exclusive')]" */
			"(//div[contains(text(),'OWNER EXCLUSIVES')])[1]");
	protected By ownerExHeroImg = By.xpath(/*
											 * "//h2[contains(.,'Owner Exclusive')]/ancestor::div[@class='wyn-hero wyn-hero--large']//img"
											 */
			"(//div[contains(text(),'OWNER EXCLUSIVES')])[1]/ancestor::div[@class='cardBanner']//img");
	protected By ownerExDesc = By.xpath(/*
										 * "//h2[contains(.,'Owner Exclusive')]/ancestor::div[@class='wyn-hero wyn-hero--large']//p"
										 */
			"(//div[contains(text(),'OWNER EXCLUSIVES')])[1]/ancestor::div[@class='cardBanner']//div[@class='body-1']/p");
	protected By ownerExBrdCrm = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Owner Exclusive')]");

	protected By ownerPerksHdr = By.xpath(/* "//h1[contains(.,'Owner Perks')]" */
			"//div[contains(text(),'Owner Perks')]");
	protected By owerPerksCardsImg = By.xpath(/* "//div[@class='two-columns-container']//img" */
			"(//div[contains(text(),'Owner Perks')]//..//..//..//../div[@class='cardComponent'])[1]//img");
	protected By ownerPerksCardsHdr = By.xpath(/* "//div[@class='two-columns-container']//h2" */
			"(//div[contains(text(),'Owner Perks')]//..//..//..//../div[@class='cardComponent'])[1]//div[@class='card-section']//div[contains(@class,'subtitle-2')]");
	protected By ownerPerksCardsDesc = By.xpath(/* "//div[@class='two-columns-container']//p" */
			"(//div[contains(text(),'Owner Perks')]//..//..//..//../div[@class='cardComponent'])[1]//div[@class='card-section']//div[contains(@class,'body-1')]/p");
	protected By ownerPerksCardsCTA = By.xpath(/*
												 * "//div[@class='two-columns-container']//div[@class='wyn-card__cta']/a"
												 */
			/*
			 * "(//div[contains(text(),'Owner Perks')]//..//..//..//../div[@class='cardComponent'])[1]//div[@class='card-section']//a/div[@class='body-1-link']"
			 */
			"(//div[contains(text(),'Owner Perks')]//..//..//..//../div[@class='cardComponent'])[1]//div[@class='card-section']//a/div[contains(@class,'body-1-link')]");

	protected By clubWyndhamStoreHdr = By.xpath(/* "//h1[contains(.,'Club Wyndham Store')]" */
			"//div[contains(text(),'CLUB WYNDHAM STORE')]");
	protected By clubWynImage = By.xpath(
			/*
			 * "//h1[contains(.,'Club Wyndham Store')]/ancestor::div/following-sibling::div[@class='contentblock-carousel']//img"
			 */
			"//div[contains(text(),'CLUB WYNDHAM STORE')]//following::div[@class='imageSlice']//img");
	protected By clubWyndSubHdr = By.xpath(
			/*
			 * "//h1[contains(.,'Club Wyndham Store')]/ancestor::div/following-sibling::div[@class='contentblock-carousel']//h2"
			 */
			"//div[contains(text(),'CLUB WYNDHAM STORE')]/../h3[contains(@class,'title-1')]");
	protected By clubWynDesc = By.xpath(
			/*
			 * "//h1[contains(.,'Club Wyndham Store')]/ancestor::div/following-sibling::div[@class='contentblock-carousel']//p"
			 */
			"//div[contains(text(),'CLUB WYNDHAM STORE')]/../div[contains(@class,'body-1')]/p");
	protected By clubWynCTA = By.xpath(
			/*
			 * "//h1[contains(.,'Club Wyndham Store')]/ancestor::div/following-sibling::div[@class='contentblock-carousel']//a"
			 */
			"//div[contains(text(),'CLUB WYNDHAM STORE')]/../div[contains(@class,'body-1')]//..//..//..//..//a");

	protected By wyndhamRewardHdr = By.xpath("//h2[contains(.,'Wyndham Rewards')]");
	protected By wynRewardsImg = By
			.xpath("//h2[contains(.,'Wyndham Rewards')]/ancestor::div[@class='banner-carousel']//img");
	protected By wynRewardsSubHdr = By
			.xpath("//h2[contains(.,'Wyndham Rewards')]/ancestor::div[@class='banner-carousel']//h3");
	protected By wynRewardsDesc = By
			.xpath("//h2[contains(.,'Wyndham Rewards')]/ancestor::div[@class='banner-carousel']//div/p");
	protected By wynrewardsCTA = By
			.xpath("//h2[contains(.,'Wyndham Rewards')]/ancestor::div[@class='banner-carousel']//a");

	// VIP Offers
	protected By vipNowOffersNav = By.xpath("//div[@class='wyn-fly-out']//li[contains(.,'VIP Now')]");
	protected By vipOffersHdr = By.xpath("//div[@class='wyn-headline ']/p[contains(.,'VIP Offers')]");
	protected By vipOffersHeroImg = By
			.xpath("//p[contains(.,'VIP Offers')]/ancestor::div[@gtm_component='banner']//img");
	protected By vipOffersBrdCrmb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'VIP')]");
	protected By vipOffrsForCWHdr = By.xpath("//h1[contains(.,'VIP OFFERS FOR CLUB')]");
	protected By vipOffersCardsImg = By.xpath("//div[@class='two-columns-container']//img");
	protected By vipOffersCardsHdr = By.xpath("//div[@class='two-columns-container']//h2");
	protected By vipOffersText = By.xpath("//div[@class='two-columns-container']//p");
	protected By learnMoreCTA = By.xpath("//div[@class='two-columns-container']//a[contains(.,'Learn')]");

	// VIP Exclusives
	protected By VIPExclusiveSubNav = By
			.xpath("//div[contains(@class,'global-navigation')]//li/a[contains(.,'VIP Exclusives')]");
	protected By VIPExHeader = By.xpath("(//div[contains(text(),'VIP BY WYNDHAM')])[1]");
	protected By VIPExImageBanner = By
			.xpath("(//div[contains(text(),'VIP BY WYNDHAM')])[1]/ancestor::div[@class='cardBanner']//img");
	protected By VIPExSubHeader = By.xpath(
			"(//div[contains(text(),'VIP BY WYNDHAM')])[1]/ancestor::div[@class='cardBanner']//div[@class='title-1']");
	protected By VIPExSubDesc = By.xpath(
			"(//div[contains(text(),'VIP BY WYNDHAM')])[1]/ancestor::div[@class='cardBanner']//div[@class='body-1']/p");
	protected By VIPExBreadCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'VIP Exclusive')]");
	protected By VIPExclusiveHeaderSection = By.xpath("//div[contains(text(),'VIP EXCLUSIVES')]");
	protected By VIPExclusiveCardComponent = By.xpath(
			"//div[contains(text(),'VIP EXCLUSIVES')]//..//..//..//..//..//..//..//..//..//..//..//..//div[@class='cardComponent']//img");
	protected By VIPExclusiveCardHeader = By.xpath(
			"//div[contains(text(),'VIP EXCLUSIVES')]//..//..//..//..//..//..//..//..//..//..//..//..//div[@class='cardComponent']//div[@class='card-section']//div[contains(@class,'subtitle-2')]");
	protected By VIPExclusiveCardDescription = By.xpath(
			"//div[contains(text(),'VIP EXCLUSIVES')]//..//..//..//..//..//..//..//..//..//..//..//..//div[@class='cardComponent']//div[@class='card-section']//div[contains(@class,'body-1')]/p");
	protected By VIPExclusiveCardCTA = By.xpath(
			"//div[contains(text(),'VIP EXCLUSIVES')]//..//..//..//..//..//..//..//..//..//..//..//..//div[@class='cardComponent']//div[@class='card-section']//div[contains(@class,'body-1-link')]");
	protected By seeAllDealsOffersLink = By.xpath("//a[contains(text(),'See All Deals & Offers')]");

	protected By loginBtn = By.xpath("//div[@class='wyn-header__nav-container']//a[contains(.,'Login')]");
	protected By loginPopUp = By.xpath(/* "//div[@id='wyn-modal-content']" */
			"//div[@id='formModal']");
	protected By loginPopUpHdr = By.xpath(/* "//div[@id='wyn-modal-content']//h2" */
			"//div[@id='formModal']//div[@class='caption-1']");
	protected By loginPopUpBtn = By.xpath(/* "//div[@id='wyn-modal-content']//a" */
			"//div[@id='formModal']//div[@class='caption-1']//..//a");
	protected By loginPopUpClose = By.xpath(/* "//div[@id='wyn-modal-content']//span" */
			"(//button[@class='close-button']/span)[1]");

	public void vipExclusiveNav() {

		waitUntilElementVisibleBy(driver, dealsOffersHeader, 120);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(dealsOffersHeader)).build().perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(VIPExclusiveSubNav)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusiveNav", Status.PASS,
					"VIP Exclusive present in Sub Nav");
			driver.findElement(VIPExclusiveSubNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusiveNav", Status.FAIL,
					"VIP Exclusive not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, VIPExHeader, 120);

		if (verifyObjectDisplayed(VIPExHeader)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusiveNav", Status.PASS,
					"VIP Exclusive Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusiveNav", Status.FAIL,
					"VIP Exclusive Page Navigation failed");
		}

	}

	public void vipExclusivePageVal() {
		if (verifyObjectDisplayed(VIPExHeader)) {

			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.PASS,
					"VIP Exclusive header present as " + getElementText(VIPExHeader));

		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.FAIL,
					"VIP Exclusive header not present");
		}

		if (verifyObjectDisplayed(VIPExImageBanner)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.PASS,
					"VIP Exclusive  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.FAIL,
					"VIP Exclusive  page hero Image not present");
		}

		getElementInView(VIPExBreadCrumb);
		if (verifyObjectDisplayed(VIPExBreadCrumb)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.PASS,
					"VIP Exclusive Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.FAIL,
					"VIP Exclusive Breadcrumb not present");
		}

		if (verifyObjectDisplayed(VIPExSubHeader)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.PASS,
					"VIP Exclusive Sub Header present");

		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.FAIL,
					"VIP Exclusive Sub Header not  present");
		}

		if (verifyObjectDisplayed(VIPExSubDesc)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.PASS,
					"VIP Exclusive Sub Description present");

		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivePageVal", Status.FAIL,
					"VIP Exclusive Sub Description not  present");
		}

	}

	public void validateSeeAllDealsLink() {
		getElementInView(seeAllDealsOffersLink);
		if (verifyObjectDisplayed(seeAllDealsOffersLink)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "validateSeeAllDealsLink", Status.PASS,
					"See all deals and offers link present on VIP Exclusive page");

			clickElementJSWithWait(seeAllDealsOffersLink);

			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (driver.getTitle().toUpperCase().contains(testData.get("AllDeals").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenVIPExclusivePage", "validateSeeAllDealsLink", Status.PASS,
						"Navigated to All deals and offers page");

				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("NextGenVIPExclusivePage", "validateSeeAllDealsLink", Status.FAIL,
						"All deals and offers page navigation failed");
			}
			driver.navigate().back();
			waitUntilObjectVisible(driver, seeAllDealsOffersLink, 120);

		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "validateSeeAllDealsLink", Status.FAIL,
					"See all deals and offers link not present on homepage");
		}
	}

	/*
	 * Method: ownerPerksSectionval Description: Owner Ex Perks section val: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void vipExclusivesSectionval() {

		getElementInView(VIPExclusiveHeaderSection);

		if (verifyObjectDisplayed(VIPExclusiveHeaderSection)) {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.PASS,
					"VIP Exclusive header present in the page");

			try {
				List<WebElement> cardsImageList = driver.findElements(VIPExclusiveCardComponent);
				List<WebElement> cardsHdrsList = driver.findElements(VIPExclusiveCardHeader);
				List<WebElement> cardsDescList = driver.findElements(VIPExclusiveCardDescription);

				tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.PASS,
						"VIP Exclusive Two Content Blocks present Total: " + cardsHdrsList.size());

				for (int i = 0; i < cardsHdrsList.size(); i++) {
					getElementInView(cardsHdrsList.get(i));
					tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.PASS,
							"Two Content Block " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

					if (verifyElementDisplayed(cardsImageList.get(i))) {
						tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.PASS,
								"Image present in " + (i + 1) + " content block");
					} else {
						tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.FAIL,
								"Image not present in " + (i + 1) + " content block");
					}

					if (verifyElementDisplayed(cardsDescList.get(i))) {
						tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.PASS,
								"Description present in " + (i + 1) + " content block");
					} else {
						tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.FAIL,
								"Description not present in " + (i + 1) + " content block");
					}

				}

				for (int j = 0; j < cardsHdrsList.size(); j++) {
					List<WebElement> cardsLearnMoreList1 = driver.findElements(VIPExclusiveCardCTA);
					if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {

						cardsLearnMoreList1.get(j).click();

						waitForSometime(tcConfig.getConfig().get("LongWait"));

						waitUntilElementVisibleBy(driver, loginPopUp, 50);

						if (verifyObjectDisplayed(loginPopUp)) {
							tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval",
									Status.PASS, "Login Button pop up opened");

							List<WebElement> headers = driver.findElements(loginPopUpHdr);
							List<WebElement> headersBtn = driver.findElements(loginPopUpBtn);

							if (headers.size() == 2) {
								tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval",
										Status.PASS, "Login Pop Up Headers present");
							} else {
								tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval",
										Status.FAIL, "Login Pop Up Headers not present");
							}

							if (headersBtn.size() == 2) {
								tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval",
										Status.PASS, "Login Pop Up Headers CTA present");
							} else {
								tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval",
										Status.FAIL, "Login Pop Up Headers CTA not present");
							}

							if (verifyObjectDisplayed(loginPopUpClose)) {
								clickElementBy(loginPopUpClose);
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} else {
								tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval",
										Status.FAIL, "Login Pop Up close CTA not present");
							}

						} else {
							tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval",
									Status.FAIL, "Login Button pop up not opened");
						}

					} else {
						tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.FAIL,
								" CTA link not present in the content for " + (j + 1) + " block");
					}
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.FAIL,
						"Owner Perks Cards are not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenVIPExclusivePage", "vipExclusivesSectionval", Status.FAIL,
					"Owner perks header not present in the page");
		}

	}

	/*
	 * Method: clubWynStoreVal Description: Club Wyn Store Val: Date Jul/2019
	 * Author: Unnat Jain Changes By
	 */
	public void clubWynStoreVal() {

		getElementInView(clubWyndhamStoreHdr);
		if (verifyObjectDisplayed(clubWyndhamStoreHdr)) {

			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.PASS,
					"Club Wynham Store header present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.FAIL,
					"Club Wynham Store header not present");
		}
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(clubWynImage)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.PASS,
					"Club Wyn Store Hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.FAIL,
					"Club Wyn Store Hero Image not present");
		}

		if (verifyObjectDisplayed(clubWyndSubHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.PASS,
					"Club Wyn Store Subheader present as " + getElementText(clubWyndSubHdr));

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.FAIL,
					"Club Wyn Store Subheader not present");
		}

		if (verifyObjectDisplayed(clubWynDesc)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.PASS,
					"Club wyndham Description presents");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "clubWynStoreVal", Status.FAIL,
					"Club wyn Store Description not  presents");
		}

		if (verifyObjectDisplayed(clubWynCTA)) {

			clickElementJSWithWait(clubWynCTA);

			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));

			if (driver.getCurrentUrl().toUpperCase().contains(testData.get("strSearch").toUpperCase())) {

				tcConfig.updateTestReporter("NextGenOwnerExPage", "clubWynStoreVal", Status.PASS,
						"Navigated to associated page");

				/*
				 * if (driver.getCurrentUrl().toUpperCase().contains("HTML")) {
				 * tcConfig.updateTestReporter("NextGenOwnerExPage", "clubWynStoreVal",
				 * Status.FAIL, "Link Contains .html " + driver.getCurrentUrl()); } else {
				 * tcConfig.updateTestReporter("NextGenOwnerExPage", "clubWynStoreVal",
				 * Status.PASS, "Link is displayed correctly " + driver.getCurrentUrl()); }
				 */

			} else {
				tcConfig.updateTestReporter("NextGenOwnerExPage", "clubWynStoreVal", Status.FAIL,
						"Navigation to associated page failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.close();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			driver.switchTo().window(tabs2.get(0));

			waitUntilElementVisibleBy(driver, ownerPerksHdr, 120);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExPage", "clubWynStoreVal", Status.FAIL,
					" CTA link not present in the content for club wyndham store block");
		}

	}

	/*
	 * Method: wynRewardsSecVal Description: Wyndham Rewards Section Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void wynRewardsSecVal() {

		getElementInView(wyndhamRewardHdr);
		if (verifyObjectDisplayed(wyndhamRewardHdr)) {

			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.PASS,
					"Wyndham Rewards ham Section header present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.FAIL,
					"Wyndham Rewards ham Section header not present");
		}

		if (verifyObjectDisplayed(wynRewardsImg)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.PASS,
					"Wyndham Rewards  Section Hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.FAIL,
					"Wyndham Rewards  Section Hero Image not present");
		}

		if (verifyObjectDisplayed(wynRewardsSubHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.PASS,
					"Wyndham Rewards  Section Subheader present as " + getElementText(wynRewardsSubHdr));

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.FAIL,
					"Wyndham Rewards  Section Subheader not present");
		}

		if (verifyObjectDisplayed(wynRewardsDesc)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.PASS,
					"Wyndham Rewards  Description presents");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "wynRewardsSecVal", Status.FAIL,
					"Wyndham Rewards  Section Description not  presents");
		}

		if (verifyObjectDisplayed(wynrewardsCTA)) {

			clickElementBy(wynrewardsCTA);

			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));

			if (driver.getCurrentUrl().toUpperCase().contains(testData.get("subHeader1").toUpperCase())) {

				tcConfig.updateTestReporter("NextGenOwnerExPage", "wynRewardsSecVal", Status.PASS,
						"Navigated to associated page");

			} else {
				tcConfig.updateTestReporter("NextGenOwnerExPage", "wynRewardsSecVal", Status.FAIL,
						"Navigation to associated page failed");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.close();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			driver.switchTo().window(tabs2.get(0));

			waitUntilElementVisibleBy(driver, ownerPerksHdr, 120);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExPage", "wynRewardsSecVal", Status.FAIL,
					" CTA link not present in the content for Wyndham Rewards  block");
		}

	}

	/*
	 * Method: vipOfferspageNav Description: Vip Offers page Navigation: Date
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void vipOfferspageNav() {

		waitUntilElementVisibleBy(driver, dealsOffersHeader, 120);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(dealsOffersHeader)).build().perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(vipNowOffersNav)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOfferspageNav", Status.PASS,
					"VIP Now Offers present in Sub Nav");
			driver.findElement(vipNowOffersNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOfferspageNav", Status.FAIL,
					"VIP Now Offers not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, vipOffersHdr, 120);

		if (verifyObjectDisplayed(vipOffersHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOfferspageNav", Status.PASS,
					"VIP Now Offer Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOfferspageNav", Status.FAIL,
					"VIP Now Offer Page Navigation failed");
		}

	}

	/*
	 * Method: vipOffersPageVal Description: VIP Offers Validations: Date Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void vipOffersPageVal() {
		if (verifyObjectDisplayed(vipOffersHdr)) {

			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.PASS,
					"VIP Offers header present as " + getElementText(vipOffersHdr));

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.FAIL,
					"VIP Offers header not present");
		}

		if (verifyObjectDisplayed(vipOffersHeroImg)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.PASS,
					"VIP Offers  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.FAIL,
					"VIP Offers  page hero Image not present");
		}

		if (verifyObjectDisplayed(vipOffersBrdCrmb)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.PASS,
					"VIP Offers Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.FAIL,
					"VIP Offers Breadcrumb not present");
		}

		if (verifyObjectDisplayed(vipOffrsForCWHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.PASS,
					"VIP Offers CW header present " + getElementText(vipOffrsForCWHdr));

			try {
				List<WebElement> cardsImageList = driver.findElements(vipOffersCardsImg);
				List<WebElement> cardsHdrsList = driver.findElements(vipOffersCardsHdr);
				List<WebElement> cardsDescList = driver.findElements(vipOffersText);
				// List<WebElement> cardsLearnMoreList =
				// driver.findElements(rciLearnMore);

				tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.PASS,
						"Vip Offers Content Blocks present Total: " + cardsHdrsList.size());

				for (int i = 0; i < cardsHdrsList.size(); i++) {
					getElementInView(cardsHdrsList.get(i));
					tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.PASS,
							"Content Block " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

					if (verifyElementDisplayed(cardsImageList.get(i))) {
						tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.PASS,
								"Image present in " + (i + 1) + " content block");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.FAIL,
								"Image not present in " + (i + 1) + " content block");
					}

					if (verifyElementDisplayed(cardsDescList.get(i))) {
						tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.PASS,
								"Description present in " + (i + 1) + " content block");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.FAIL,
								"Description not present in " + (i + 1) + " content block");
					}

				}

				for (int j = 0; j < cardsHdrsList.size(); j++) {
					List<WebElement> cardsLearnMoreList1 = driver.findElements(learnMoreCTA);

					if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {
						// getElementInView(cardsLearnMoreList1.get(j));
						scrollDownForElementJSWb(cardsLearnMoreList1.get(j));
						scrollUpByPixel(300);
						cardsLearnMoreList1.get(j).click();

						waitForSometime(tcConfig.getConfig().get("LongWait"));

						ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
						driver.switchTo().window(tabs2.get(1));
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						if (driver.getCurrentUrl().toUpperCase().contains(testData.get("subHeader1").toUpperCase())) {

							tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.PASS,
									"Navigated to " + (j + 1) + " page");

						} else {
							tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.FAIL,
									"Navigated to " + (j + 1) + " page failed");
						}
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						driver.switchTo().window(tabs2.get(0));

						waitUntilElementVisibleBy(driver, vipOffersHdr, 120);

					} else {
						tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.FAIL,
								" CTA link not present in the content for " + (j + 1) + " block");
					}

				}
			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenOwnerExPage", "vipOffersPageVal", Status.FAIL,
						"VIP Offers Cards are not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vipOffersPageVal", Status.FAIL,
					"VIP Offers CW header not present");
		}

	}

}
