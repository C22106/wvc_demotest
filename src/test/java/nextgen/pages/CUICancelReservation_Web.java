package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CUICancelReservation_Web extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(CUICancelReservation_Web.class);

	public CUICancelReservation_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By reservationNumber = By.xpath("//h3[contains(@class,'reservation-id')]");
	protected By reservationName = By.xpath("//h3[contains(@class,'reservation-name')]");
	protected By checkInDate = By.xpath("//section[contains(@class,'check-in')]/span/following-sibling::span");
	protected By checkOutDate = By.xpath("//section[contains(@class,'check-out')]/span/following-sibling::span");
	protected By reimbursementTitle = By.xpath("//main/h5[contains(@class,'cancel-reservation__title')]");
	protected By membershipReimburse = By.xpath("//span[contains(.,'Membership Reimbursement')]");
	protected By pointsDropdown = By.xpath("//h3/span[contains(.,'points')]");
	protected By totalPoints = By.xpath("//span[contains(.,'points')]/parent::h3/following-sibling::span");
	protected By pointsDropDownValue = By.xpath(
			"//span[contains(.,'points')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span");
	protected By houseKeepingCredits = By.xpath("//h3//span[contains(@class,'has-tip')]");
	protected By houseKeepingPoints = By
			.xpath("//span[contains(@class,'has-tip')]/ancestor::div[contains(@class,'dropdown')]/span");
	protected By houseKeepingDropdownValue = By.xpath(
			"//span[contains(@class,'has-tip')]/ancestor::div[contains(@class,'dropdown')]/following-sibling::div//span");
	protected By nonRefundableText = By.xpath(
			"//span[contains(@class,'has-tip')]/ancestor::section[contains(@class,'dropdown')]/following-sibling::div/small");

	protected By policyHeader = By.xpath("// section[contains(@class,'policy')]//h5");
	protected By policyText = By.xpath("// section[contains(@class,'policy')]//p");
	protected By policyCTA = By.xpath("// section[contains(@class,'policy')]//a");
	protected By cancelCTA = By.xpath("// footer[contains(@class,'cancel')]//a[contains(.,'Cancel')]");

	protected By cancelledResortHeader = By.xpath("// div[contains(@class,'reservation')]//h1/a");
	public static String pointsReimbursed;

	/*
	 * Method: reservationNumberCheck Description: Reservation Number Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationNumberCheck() {
		// reservation number from test data
		//String strReservationID = testData.get("ReservationNumber").toUpperCase();
		String textreservationNumber = CUIBookPage_Web.reservationNumber.toUpperCase();
		if (getElementText(reservationNumber).toUpperCase().contains(textreservationNumber)) {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationNumberCheck", Status.PASS,
					"Reservation Number Displayed correctly " + textreservationNumber);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationNumberCheck", Status.FAIL,
					"Reservation Number not Displayed correctly " + textreservationNumber);
		}

	}

	/*
	 * Method: reservationResortName Description: Reservation Resort Name Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationResortName() {
		// reservation Resort Name from test data
		String strReservationName = testData.get("ReservationResortName").toUpperCase();
		if (getElementText(reservationName).toUpperCase().contains(strReservationName)) {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationResortName", Status.PASS,
					"Reservation Resort Name Displayed correctly " + strReservationName);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "reservationResortName", Status.FAIL,
					"Reservation Resort name not Displayed correctly " + strReservationName);
		}
	}

	/*
	 * Method: checkInDateCheck Description: Check In Date check Date: June/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void checkInDateCheck() {
		// reservation Check In from test data
		String strReservationChekcInDate = testData.get("Checkindate");
		if (getElementText(checkInDate).contains(strReservationChekcInDate)) {
			tcConfig.updateTestReporter("CUICancelReservation", "checkInDateCheck", Status.PASS,
					"Reservation Check In Date Displayed correctly " + strReservationChekcInDate);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "checkInDateCheck", Status.FAIL,
					"Reservation Check In Date not Displayed correctly " + strReservationChekcInDate);
		}
	}

	/*
	 * Method: checkOutDateCheck Description: Reservation Resort Name Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void checkOutDateCheck() {
		// reservation Check Out from test data
		String strReservationCheckOutDate = testData.get("Checkoutdate").toUpperCase();
		if (getElementText(checkOutDate).contains(strReservationCheckOutDate)) {
			tcConfig.updateTestReporter("CUICancelReservation", "checkOutDateCheck", Status.PASS,
					"Reservation  Check Out Date Displayed correctly " + strReservationCheckOutDate);
		} else {
			tcConfig.updateTestReporter("CUICancelReservation", "checkOutDateCheck", Status.FAIL,
					"Reservation  Check Out Date not Displayed correctly " + strReservationCheckOutDate);
		}
	}

	/*
	 * Method: reimbursementSectionHeader Description: Reimbursement Section
	 * Headers validation Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reimbursementSectionHeader() {
		elementPresenceVal(reimbursementTitle, "Reimbursement Header ", "CUICancelReservation",
				"reimbursementSectionHeader");

	}

	/*
	 * Method: cancellationSection Description: Cancellation Section validation
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancellationSection() {
		getPopUpElementInView(policyHeader, "No");
		elementPresenceVal(policyHeader, "Cancellation Policy Header ", "CUICancelReservation", "cancellationSection");
		elementPresenceVal(policyText, "Cancellation Policy Description ", "CUICancelReservation",
				"cancellationSection");
		elementPresenceVal(policyCTA, "Cancellation Policy CTA ", "CUICancelReservation", "cancellationSection");

	}

	/*
	 * Method: membershipReimbursementSection Description: Membership
	 * reimbursement Section validation Date: June/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void membershipReimbursementSection() {
		pointsReimbursed = getElementText(totalPoints);
		elementPresenceVal(membershipReimburse, "Membership Reimburse Header ", "CUICancelReservation",
				"membershipReimbursementSection");
		fieldPresenceVal(pointsDropdown, "Total Reimburse Points header ", "CUICancelReservation",
				"membershipReimbursementSection", " with points " + pointsReimbursed);
	}

	/*
	 * Method: cancelButtonClick Description: Cancel Button Click Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelButtonClick() {
		Assert.assertTrue(verifyObjectDisplayed(cancelCTA));
		tcConfig.updateTestReporter("CUICancelReservation", "cancelButtonClick", Status.PASS,
				"Cancel Cta present in the Pop Up");
		clickElementBy(cancelCTA);
		waitUntilElementVisibleBy(driver, cancelledResortHeader, "ExplicitLongWait");

	}
	
	

}

