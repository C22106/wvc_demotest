package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenOwnerExclusivePage_IOS extends NextGenOwnerExclusivePage_Web {

	public static final Logger log = Logger.getLogger(NextGenOwnerExclusivePage_IOS.class);

	public NextGenOwnerExclusivePage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
