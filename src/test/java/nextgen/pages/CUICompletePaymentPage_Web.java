package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUICompletePaymentPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUICompletePaymentPage_Web.class);

	public CUICompletePaymentPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	CUIBookPage_Web bookPage = new CUIBookPage_Web(tcConfig);
	protected By textCompletePayment = By.xpath("//h1[text() = 'Complete Payment']");
	protected By textReviewPayment = By.xpath("//h2[text() = 'Review Payment']");
	protected By textPaywithPayPal = By.xpath("//h2[text() = 'Pay with PayPal']");
	protected By textContractNumber = By.xpath("//h3[text() = 'Contract']/../p/span");
	protected By totalPaymentBreakdown = By.xpath("//h2[text() = 'Payment Details']/..//table//tr");
	protected By textTotalChargeDue = By.xpath("//th[text() = 'Total Payment Due']/../td/strong");
	protected By buttonContinue = By.xpath("(//button[text() = 'Continue'])[1]");
	protected By loadingSpinner = By.xpath("//*[local-name()='svg' and @class='spinner -secondary undefined']");

	/*
	 * Method: verifyHeadersinCompletePaymentPage Description:verify Headers in
	 * Complete Payment Page Date field Date: June/2020 Author: Abhijeet Roy Changes
	 * By: NA
	 */
	public void verifyHeadersinCompletePaymentPage() {
		waitUntilElementVisibleBy(driver, textReviewPayment, 120);
		elementPresenceVal(textCompletePayment, "Header Complete Payment", "CUICompletePaymentPage",
				"verifyHeadersinCompletePaymentPage");
		elementPresenceVal(textReviewPayment, "Header Review Payment", "CUICompletePaymentPage",
				"verifyHeadersinCompletePaymentPage");
		elementPresenceVal(textPaywithPayPal, "Header Pay with Paypal", "CUICompletePaymentPage",
				"verifyHeadersinCompletePaymentPage");
	}

	/*
	 * Method: verifyContractNumber Description:verify Contract Number Date field
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyContractNumber() {
		String contractNumber = testData.get("ContractNumber");
		if (contractNumber.equals(getElementText(textContractNumber))) {
			tcConfig.updateTestReporter("CUICompletePaymentPage", "verifyContractNumber", Status.PASS,
					"Contract Number matched and the contract is: " + getElementText(textContractNumber));
		}
	}

	/*
	 * Method: totalPaymentBreakdown Description:total Payment Breakdown Date field
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void totalPaymentBreakdown() {
		for (int iteratorpaybreak = 1; iteratorpaybreak <= getList(totalPaymentBreakdown).size(); iteratorpaybreak++) {
			String labelFor = getElementText(getObject(
					By.xpath("(//h2[text() = 'Payment Details']/..//table//tr/th)[" + iteratorpaybreak + "]"))).trim();
			String chargeValue = getElementText(getObject(
					By.xpath("(//h2[text() = 'Payment Details']/..//table//td/strong)[" + iteratorpaybreak + "]")))
							.trim();
			if (labelFor.isEmpty() || labelFor == null || chargeValue.isEmpty() || chargeValue == null) {
				tcConfig.updateTestReporter("CUICompletePaymentPage", "totalPaymentBreakdown", Status.FAIL,
						"Either label for or charge value is not displayed");
			} else {
				tcConfig.updateTestReporter("CUICompletePaymentPage", "totalPaymentBreakdown", Status.PASS,
						"Charge For: " + labelFor + " is displayed as: " + chargeValue);
			}
		}
	}

	/*
	 * Method: validateTotalChargeDue Description:validate Total Charge Due Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTotalChargeDue() {
		if (verifyObjectDisplayed(textTotalChargeDue)) {
			tcConfig.updateTestReporter("CUICompletePaymentPage", "validateTotalChargeDue", Status.PASS,
					"Total Charge Due is: " + getElementText(textTotalChargeDue));
		} else {
			tcConfig.updateTestReporter("CUICompletePaymentPage", "validateTotalChargeDue", Status.FAIL,
					"Total Charge is not displayed");
		}
	}

	/*
	 * Method: paymentViaPaypal Description:payment Via Paypal Date field Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void paymentViaPaypal() {
		bookPage.paymentViaPaypal();
	}

	/*
	 * Method: clickContinueButton Description:click Continue Button Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		if (verifyObjectDisplayed(buttonContinue)) {
			getElementInView(buttonContinue);
			clickElementJSWithWait(buttonContinue);
		}

	}
}