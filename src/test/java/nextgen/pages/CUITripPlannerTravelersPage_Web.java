package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUITripPlannerTravelersPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUITripPlannerTravelersPage_Web.class);

	public CUITripPlannerTravelersPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	protected By labelVacationingWith = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'Who are you vacationing with? ')]");
	protected By radioButtonJustMe = By.xpath("//label[contains(.,'Just Me')]");
	protected By radioButtonACoupleOfUs = By.xpath("//label[contains(.,'A Couple Of Us')]");
	protected By radioButtonASmallGroup = By.xpath("//label[contains(.,'A Small Group')]");
	protected By radioButtonItsAParty = By.xpath("//label[contains(.,'It’s A Party')]");
	protected By buttonNext = By.xpath("(//button[contains(@class,'button expanded') and contains(.,'Next')])[1] | (//button[contains(@class,'button expanded') and contains(.,'Next')])[2]");
	protected By buttonGoBack = By.xpath("(//button[contains(@class,'prev-link-caret') and contains(.,'Go Back')])");
	protected By labelPlanningToStay = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'When are you looking to travel?')]");
	
	/*
	 * Method: selectGroupSize Description: Select Group Size
	 *  Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void selectGroupSize() {
		Assert.assertFalse(getObject(buttonNext).isEnabled(),"Nextbutton is Enabled");
		WebElement selectRadio = getObject(
				By.xpath("//label[contains(.,'"+ testData.get("GroupSize") +"')]"));
		getElementInView(selectRadio);
		clickElementJSWithWait(selectRadio);
		tcConfig.updateTestReporter("CUITripPlannerTravelersPage", "selectGroupSize", Status.PASS,
				"Clicked on and selected group size is: "  + testData.get("GroupSize"));
	}

	/*
	 * Method: clickContinueCTA Description: click Continue CTA
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void clickContinueCTA() {
		clickElementJSWithWait(buttonNext);
		waitUntilElementVisibleBy(driver, labelPlanningToStay, 120);
		tcConfig.updateTestReporter("CUITripPlannerYourBudgetPage", "clickContinueCTA", Status.PASS,
				"Successfully clicked next button");
	}
}
