package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenTravelInspiration_IOS extends NextGenTravelInspiration_Web {

	public static final Logger log = Logger.getLogger(NextGenTravelInspiration_IOS.class);

	public NextGenTravelInspiration_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
}
