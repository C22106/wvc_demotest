package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUITripPlannerLocationPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUITripPlannerLocationPage_Web.class);

	public CUITripPlannerLocationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	protected By labelWhereYouWant = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'Where do you want to vacation?')]");
	protected By radioButtonEastCoast = By.xpath("//label[contains(.,'East Coast Escapes')]");
	protected By radioButtonWestCoast = By.xpath("//label[contains(.,'West Coast Adventures')]");
	protected By radioButtonSouthernStays = By.xpath("//label[contains(.,'Southern Stays')]");
	protected By radioButtonMidwest = By.xpath("//label[contains(.,'Midwest Memories')]");
	protected By radioButtonExociticGetaways = By.xpath("//label[contains(.,'Global Getaways')]");
	protected By radioButtonAnywhere = By.xpath("//label[contains(.,'Anywhere')]");
	protected By buttonNext = By.xpath("(//button[contains(@class,'button expanded') and contains(.,'Next')])[1] | (//button[contains(@class,'button expanded') and contains(.,'Next')])[2]");
	protected By buttonGoBack = By.xpath("(//button[contains(@class,'prev-link-caret') and contains(.,'Go Back')])");
	protected By labelPlanningToStay = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'What do you want to do on your vacation?')]");
	/*
	 * Method: selectSeason Description: Select Season
	 *  Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void selectVacationPlace() {
		Assert.assertFalse(getObject(buttonNext).isEnabled(),"Nextbutton is Enabled");
		WebElement selectRadio = getObject(
				By.xpath("//label[contains(.,'"+ testData.get("VacationPlace") +"')]"));
		getElementInView(selectRadio);
		clickElementJSWithWait(selectRadio);
		tcConfig.updateTestReporter("CUITripPlannerLocationPage", "selectVacationPlace", Status.PASS,
				"Clicked on and selected season is: "  + testData.get("Season"));
	}
	
	/*
	 * Method: clickContinueCTA Description: click Continue CTA
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void clickContinueCTA() {
		clickElementJSWithWait(buttonNext);
		waitUntilElementVisibleBy(driver, labelPlanningToStay, 120);
		tcConfig.updateTestReporter("CUITripPlannerLocationPage", "clickContinueCTA", Status.PASS,
				"Successfully clicked next button");
	}

}
