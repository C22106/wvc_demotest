package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NextGenBucketListPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenBucketListPage_Web.class);

	public NextGenBucketListPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By myBucketListPage = By
			.xpath("(//li[contains(@id,'my-wishlist')]//span[contains(text(),'My Bucket List')])[3]");

	protected By myBucketListPageHdr = By.xpath("//div//h1[contains(text(),'My Bucket List')]");

	@FindBy(xpath = "//div//h1[contains(text(),'My Bucket List')]/../p/span")
	WebElement resortCount;

	protected By resortCard = By.xpath("//section[contains(@class,'resort-card')]/..");

	protected By wishListHeart = By.xpath("//*[contains(text(),'Wish List Heart')]/../..");

	/*
	 * Method: faqPageNav Description: FAQ page navigation Date: Aug/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void navigatetoMyBucketListPage() {

		waitUntilElementVisibleBy(driver, myBucketListPage, 120);

		driver.findElement(myBucketListPage).click();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(myBucketListPageHdr)) {
			tcConfig.updateTestReporter("MyBucketListPage", "navigatetoMyBucketListPage", Status.PASS,
					"My Bucket List Page Navigation Successfull");
		} else {
			tcConfig.updateTestReporter("MyBucketListPage", "navigatetoMyBucketListPage", Status.FAIL,
					"My Bucket List Page Navigation failed");
		}

	}

	public int getMyBucketListResortCount() {

		String strResortCount = resortCount.getText().split("\\s+")[0].trim();

		tcConfig.updateTestReporter("MyBucketListPage", "getMyBucketListResortCount", Status.PASS,
				"Total Resort Count: " + strResortCount);

		return Integer.parseInt(strResortCount);

	}

	public int getTotalResortCardCount() {

		return driver.findElements(resortCard).size();

	}

	/*
	 * Method: travelInspirationVal Description: Travel Inspiration Validations:
	 * Date Jun/2019 Author: Unnat Jain Changes By
	 */

	public void bucketListDisplayValidation() {

		waitUntilElementVisibleBy(driver, resortCard, 120);
		int intCountofResorts = getMyBucketListResortCount();

		if (verifyObjectDisplayed(resortCard)) {

			tcConfig.updateTestReporter("MyBucketListPage", "getTotalResortCardCount", Status.PASS,
					"Resort cards are displaying Properly");

			int intInitialCardCount = getTotalResortCardCount();

			if (intCountofResorts > 10) {

				Assert.assertEquals(10, intInitialCardCount);
				tcConfig.updateTestReporter("MyBucketListPage", "bucketListDisplayValidation", Status.PASS,
						"Resort cards are displaying properly before Lazy Load in Bucket List Page having more than 10 resorts");
				scrollDownForElementJSBy(By.xpath("(//section[contains(@class,'resort-card')]/..)[10]"));
			}

			else {

				Assert.assertEquals(intCountofResorts, intInitialCardCount);

				tcConfig.updateTestReporter("MyBucketListPage", "bucketListDisplayValidation", Status.PASS,
						"Resort cards are displaying properly before Lazy Load in Bucket List Page having <= 10 resorts");
				scrollDownForElementJSBy(
						By.xpath("(//section[contains(@class,'resort-card')]/..)['" + intInitialCardCount + "']"));
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			System.out.println("After Lazy Load :" + getTotalResortCardCount());

			Assert.assertEquals(getTotalResortCardCount(), intCountofResorts);

			tcConfig.updateTestReporter("MyBucketListPage", "bucketListDisplayValidation", Status.PASS,
					"Resort cards are displaying properly After Lazy Load");

		} else {
			tcConfig.updateTestReporter("MyBucketListPage", "getTotalResortCardCount", Status.FAIL,
					"Resort Cards are not displaying properly");

		}

	}

	public void removeaResortCard() {

		getElementInView(myBucketListPageHdr);
		// driver.switchTo().activeElement().sendKeys(Keys.PAGE_UP);
		int InitialResortCount = getMyBucketListResortCount();
		driver.findElement(wishListHeart).click();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().alert().accept();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		int ResortCountAfterRemoval = getMyBucketListResortCount();

		Assert.assertEquals(ResortCountAfterRemoval, InitialResortCount - 1);

		tcConfig.updateTestReporter("MyBucketListPage", "removeaResortCard", Status.PASS,
				"Resort Card Removed Successfully");

	}

}
