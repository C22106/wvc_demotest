package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenOwnerExclusivePage_Android extends NextGenOwnerExclusivePage_Web {

	public static final Logger log = Logger.getLogger(NextGenOwnerExclusivePage_Android.class);

	public NextGenOwnerExclusivePage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
