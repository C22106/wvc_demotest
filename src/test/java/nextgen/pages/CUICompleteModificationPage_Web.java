package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CUICompleteModificationPage_Web extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(CUICompleteModificationPage_Web.class);

	public CUICompleteModificationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	CUIModifyReservationPage_Web modifyPage = new CUIModifyReservationPage_Web(tcConfig);
	protected By headerCompleteModification = By.xpath("(//h1[text() = 'Complete Modification'] | //div[ contains(@class,'title') and contains(.,'Modification')])");
	protected By textReviewCharges = By.xpath("//h3[text() = 'Review Changes']");
	protected By textTravelInfo = By.xpath("//h2[text() = 'Traveler Info']");
	protected By textCurrentOwner = By.xpath("//th[text() = 'Current']/../td");
	protected By textRevisedOwner = By.xpath("//th[text() = 'Revised']/../td");
	protected By verifyCancelPolicy = By.xpath("//h2[text() = 'Cancellation Policy']/..//span");
	protected By linkfullCancelPolicy = By.xpath("//h2[text() = 'Cancellation Policy']/../p/a");
	protected By fullCancelPolicy = By.xpath("//h3[text() = 'Canceling a Reservation']");
	protected By buttonConfirmCharges = By.xpath("//button[text() = 'Confirm Changes']");
	protected By modifyTravelConfirm = By.xpath("//h1[text() = 'Modify Traveler Confirmation'] | //div[text() = 'Modify Traveler Confirmation']");
	
	public void textReviewCharges(){
		waitUntilElementVisibleBy(driver, headerCompleteModification, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerCompleteModification));
		if (verifyObjectDisplayed(headerCompleteModification)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textReviewCharges", Status.PASS,
					"Text Review Charges is Present");
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textReviewCharges", Status.FAIL,
					"Text Review Charges not Present");
		}
	}
	

	public void textTravelerInfo(){
		if (verifyObjectDisplayed(textTravelInfo)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textTravelerInfo", Status.PASS,
					"Text Travel Info is Present");
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "textTravelerInfo", Status.FAIL,
					"Text Travel Info not Present");
		}
	}
	
	public void verifyCurrentOwner(){
		if (verifyObjectDisplayed(textCurrentOwner)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.PASS,
					"Current Owner is: "+getElementText(textCurrentOwner));
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
					"Current Owner text and value not present");
		}
	}
	
	public void verifyRevisedOwner(){
		String modifyToOwner = testData.get("ModifyOwner");
		if (verifyObjectDisplayed(textRevisedOwner)) {
			if (getElementText(textRevisedOwner).equals(modifyToOwner)) {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyRevisedOwner", Status.PASS,
						"Revised Owner is: "+getElementText(textRevisedOwner));
			}else{
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyRevisedOwner", Status.FAIL,
						"Owner changed and revised from screen did not match");
			}
			
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyRevisedOwner", Status.FAIL,
					"Revised Owner text and value not present");
		}
	}
	
	public void verifyRevisedGuest(){
		String modifyToGuest = testData.get("GuestFullName");
		if (verifyObjectDisplayed(textRevisedOwner)) {
			if (getElementText(textRevisedOwner).equals(modifyToGuest)) {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.PASS,
						"Revised Owner is: "+getElementText(textRevisedOwner));
			}else{
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
						"Owner changed and revised from screen did not match");
			}
			
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyCurrentOwner", Status.FAIL,
					"Revised Owner text and value not present");
		}
	}
	
	public void validateCancelPolicy(){
		if (verifyObjectDisplayed(verifyCancelPolicy)) {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "validateCancelPolicy", Status.PASS,
					"Cancellation Policy text present with description as: "+getElementText(verifyCancelPolicy));
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "validateCancelPolicy", Status.PASS,
					"Failed to validate Cancellation Policy");
		}
	}
	
	public void verifyFullCancelPolicy(){
		if (verifyObjectDisplayed(linkfullCancelPolicy)) {
			clickElementJSWithWait(linkfullCancelPolicy);
			waitUntilElementVisibleBy(driver, fullCancelPolicy, "ExplicitLongWait");
			if (verifyObjectDisplayed(fullCancelPolicy)) {
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyFullCancelPolicy", Status.PASS,
						"Navigated to Full Cancel Page");
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				navigateBack();
			}else{
				tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyFullCancelPolicy", Status.FAIL,
						"Failed to Navigated to Full Cancel Page");
			}
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyFullCancelPolicy", Status.FAIL,
					"Link to navigate to full cancellation policy page not found");
		}
	}
	
	public void buttonConfirmCharges() {
		if (verifyObjectDisplayed(buttonConfirmCharges)) {
			clickElementJSWithWait(buttonConfirmCharges);
			waitUntilElementVisibleBy(driver, modifyTravelConfirm, "ExplicitLongWait");
			Assert.assertTrue(verifyObjectDisplayed(modifyTravelConfirm));
			tcConfig.updateTestReporter("CUICompleteModificationPage", "buttonConfirmCharges", Status.PASS,
					"Navigated to Modify Success Page");
		} else {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "buttonConfirmCharges", Status.FAIL,
					"Confirm Charges button not present");
		}
	}
	
	public void verifyVIPUpgradeChanges(){
		waitUntilElementVisibleBy(driver, textCurrentOwner, "ExplicitLongWait");
		if (verifyObjectDisplayed(textCurrentOwner) && verifyObjectDisplayed(textRevisedOwner)) {
			if(!getElementText(textCurrentOwner).trim().equals(getElementText(textRevisedOwner).trim()))
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyVIPUpgradeChanges", Status.PASS,
					"VIP Upgrade opt-in is Successfully");
			clickElementJSWithWait(buttonConfirmCharges);
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "verifyVIPUpgradeChanges", Status.FAIL,
					"VIP Upgrade opt-in is not Successful");
		}
	}
	
	public void validateSectionReservationSummary(){
		modifyPage.checkSectionReservationSummary();
		modifyPage.checkReservationSummaryDetails();
	}
}
