package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUILoginPage_Android extends CUILoginPage_Web {

	public static final Logger log = Logger.getLogger(CUILoginPage_Android.class);

	protected By hamburgerMenu = By.xpath("//button[@class='hamburger-menu']");

	public CUILoginPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		headerNavigation = By.xpath("//div[@class='hamburger-menu-container']");
		headerLogo = By.xpath("//img[@class='mobile-logo']");
		logOutCTA = By.xpath("//a[@class='nav-logout-button' and span[text()='Log Out']]");
		invalidPassword = By.xpath("//p[contains(.,'Username and Password combination is incorrect')]");
		accountLocked = By.xpath("//p[contains(.,'Your account has been temporarily locked')]");
		modalHeader = By.xpath("//div[@id='loginHeadline']");
	}

	/*
	 * Method: closeCovidAlert Description: Close Covid Date: Apr/2020 Author: Unnat
	 * Jain Changes By: Kamalesh
	 */
	public void closeCovidAlert() {
		try {
			WebElement covidClose = getList(covidAlertClose).get(1);
			if (verifyObjectDisplayed(covidClose)) {
				clickElementBy(covidClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CUILoginPage", "closeAlert", Status.PASS, "Successfully Closed Alert");
			}

		} catch (Exception e) {
			log.error("Covid Message Not Present");

		}
	}

	/*
	 * Method: invalidPassword Description: Set password with incorrect data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void invalidPassword() throws Exception {
		// invalid password
		String invalidPassword = testData.get("Invalid_Pass");
		// decodeed password
		String decodedPassword = decodePassword(invalidPassword);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password", decodedPassword);
		tcConfig.updateTestReporter("CUILoginPage", "invalidPassword()", Status.PASS, "Entered password for CUI login");
	}

	/*
	 * Method: invalidUsername Description: Set Username with incorrect data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void invalidUsername() {
		// invalid username
		String strUserName = testData.get("Invalid_UserName");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Username", strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "invalidUsername()", Status.PASS,
				"Username is entered for CUI Login");
	}

	/*
	 * Method: logOutApplication Description: Logout from application clicking the
	 * Hamburger menu Feb/2020 Author: Unnat Jain Changes By: Monideep
	 * Roychowdhury(Android)
	 */
	public void logOutApplication() {
		// to be removed once log out is fixed
		clickElementBy(hamburgerMenu);
		waitUntilElementVisibleBy(driver, logOutCTA, 120);
		getElementInView(logOutCTA);
		clickElementBy(logOutCTA);
		waitUntilElementVisibleBy(driver, usernameText, 120);
		tcConfig.updateTestReporter("CUILoginPage", "logOutApplication()", Status.PASS, "Page logged out successfully");

	}

	/*
	 * Method: setUserName Description: Set Username with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: Monideep Roychowdhury(Android)
	 */
	public void setUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, 120);
		getElementInView(modalHeader);
		// Username entry
		String strUserName = testData.get("CUI_username");
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Username*", strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "setUserName()", Status.PASS, "Username is entered for CUI Login");
	}

	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: Monideep Roychowdhury(Android)
	 */
	public void setPassword() throws Exception {
		// password data
		String password = testData.get("CUI_password");
		// decodeed password
		String decodedPassword = decodePassword(password);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password*", decodedPassword);
		// fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("CUILoginPage", "setPassword()", Status.PASS, "Entered password for CUI login");
	}

	/*
	 * Method: enterPasswordOnly Description: pre Entered Username password enter
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void enterPasswordOnly() throws Exception {
		waitUntilElementVisibleBy(driver, fieldPassword, 120);
		// password data
		String password = testData.get("CUI_password").trim();
		String decodedPassword = decodePassword(password);
		clickElementBy(fieldUsername);
		sendKeyboardKeys(Keys.TAB);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Password *", decodedPassword);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: multipleInvalidLogin Description: Account Lock Validations Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void multipleInvalidLogin() throws Exception {
		// total login attempt count
		int loginAttempt = Integer.parseInt(testData.get("Attempts").trim());
		for (int attempt = 1; attempt <= loginAttempt; attempt++) {
			waitUntilElementVisibleBy(driver, fieldUsername, 120);
			getElementInView(modalHeader);
			setUserName();
			invalidPassword();
			waitUntilElementVisibleBy(driver, fieldUsername, 120);
			// multilpeLoginClick();
			// verifyLoginError(attempt);
			driver.navigate().refresh();

		}
	}

	/*
	 * Method: verifyLoginError Description: To Verify LoginErrors Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void verifyAccountLocked() {
		// total count of login attempt
		int attemptCount = Integer.parseInt(testData.get("Attempts"));
		// waitForElementError(attemptCount);
		if (verifyObjectDisplayed(accountLocked) && attemptCount == 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyError", Status.PASS,
					"Account is Locked and User Cannt Login");
		} else if (verifyObjectDisplayed(welcomeHeader) && attemptCount < 5) {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.PASS,
					"Account is not locked after 4 attempts");
			logOutApplication();

		} else {
			tcConfig.updateTestReporter("CUILoginPage", "verifyLoginError", Status.FAIL,
					"Failed to validate account locked login");
		}
	}

	/*
	 * Method: logOutApplicationViaDashboard Description: Account Log out via
	 * dasboard page Validations Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void logOutApplicationViaDashboard() {
		navigateToURL(tcConfig.getConfig().get("NewCUILogOutUrl"));
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));
		clickElementBy(hamburgerMenu);
		waitUntilElementVisibleBy(driver, logOutCTA, 120);
		getElementInView(logOutCTA);
		clickElementBy(logOutCTA);
		// waitUntilElementVisibleBy(driver, usernameText, 120);
		tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS, "User Logged Out");
	}

}
