package nextgen.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUIRCIAndDepositPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUIRCIAndDepositPage_Web.class);

	public CUIRCIAndDepositPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By buttonCancel = By.xpath("//button[text() = 'Cancel']");
	protected By linknavigateDepositandRCI = By
			.xpath("//div[@class='accountNavigation']//a[contains(.,'Deposits & RCI')]");
	protected By textDepositandRCI = By.xpath("//h1[contains(.,'DEPOSITS & RCI')]");
	protected By headerCWPointDeposit = By.xpath("//span[text() = 'Club Wyndham Points Deposit']");
	protected By sectionRCI = By.xpath("//span[text() = 'RCI']/..");
	protected By sectionPointDeposit = By
			.xpath("//span[contains(text(),'Deposit all or portion of your current Use Year points')]/..");
	protected By pointDepositLearnMoreCTA = By.xpath("//span[contains(text(),'Club Wyndham Points Deposit')]/../a");
	protected By RCILearnMoreCTA = By.xpath("//span[contains(text(),'RCI')]/../a");
	protected By pointDepositInformationPage = By.xpath("//h3[text() = 'Points Deposit Feature']");
	protected By headerRCIOverview = By.xpath("//h3[text() = 'RCI Overview']");
	protected By loadingSpinner = By.xpath("//*[local-name()='svg' and @class='spinner -secondary undefined']");
	protected By linkCWDepositPointCTA = By.xpath("//a[@id = 'deposit-cw-link']");
	protected By linkRCIDepositPointCTA = By.xpath("//a[@id = 'deposit-rci-link']");
	protected By headerSelectDeposit = By.xpath("//h1[contains(.,'Select Deposit')]");
	protected By headerRCIDepositText = By.xpath("//h1[text() = 'RCI Points Deposits']");
	protected By headerPointToDeposit = By.xpath("//h6[contains(text(),'Points to Deposit')]");
	protected By buttonExploreRCIResorts = By.xpath("//button[contains(text(),'Explore RCI Resorts')]");
	protected By headerExploreRCIResort = By.xpath("//p[@id = 'exploreRciLabel']");
	protected By checkBoxAcceptRCITerms = By.xpath("//label[@for = 'acceptRciTermsAndCond']");
	protected By buttonContinueRCI = By.xpath("//input[@value = 'Continue to RCI.com']");
	protected By linkRCIPage = By.xpath("//img[@alt = 'Club Wyndham Plus']");
	protected By headerSelectDeposit1 = By.xpath("//div[@class='steps__title']");
	protected By checkBoxYearSelect = By.xpath("//label[@for='checkbox-use-year-0']");
	protected By labelPointsToDeposit = By
			.xpath("//div[@class='form__points-to-deposit--container open']//h6[contains(text(),'Points to Deposit')]");
	protected By fieldDepositPoint = By.xpath("//div[@class='form__points-to-deposit--container open']//input");
	protected By labelRCIDepositPoint = By.xpath("//span[@class='has-tip border-none text-secondary']");
	protected By buttonContinue = By.xpath("//div[@class='grid-x grid-margin-x show-for-medium']//button");
	protected By labelEmailConfirm = By.xpath("//h3[@class='subtitle-2 text-black small-margin-bottom']");
	protected By fieldEmailConfirm = By.xpath("//label[@for='deposit-email']//input");
	protected By buttonConfirmDeposit = By.xpath("//footer[@class='booking__footer']//button");
	protected By fieldEmailConfirmation = By.xpath("//input[@name='deposit-email']");
	protected By labelDepositConfirm = By.xpath("//h2[@class='cell title-1 small-margin-bottom']");

	/*
	 * Method: navigateToDepositandRCIPage Description:Navigate to Deposit and
	 * RCI Page Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void navigateToDepositandRCIPage() {
		waitUntilElementVisibleBy(driver, linknavigateDepositandRCI, 120);
		clickElementJSWithWait(linknavigateDepositandRCI);
		waitUntilElementInVisible(driver, loadingSpinner, 120);
		waitUntilElementVisibleBy(driver, textDepositandRCI, 120);
		Assert.assertTrue(verifyObjectDisplayed(textDepositandRCI));
		tcConfig.updateTestReporter("CUI_RCI_DepositPage", "navigateToDepositandRCIPage", Status.PASS,
				"Successfully Navigated to RCI and Deposit Page");

	}

	/*
	 * Method: navigateToDepositandRCIPage Description:Navigate to Deposit and
	 * RCI Page Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyOptionsInRCIandDepositPage() {
		elementPresenceVal(sectionPointDeposit, "Section Point Deosit", "CUI_RCI_DepositPage",
				"verifyOptionsInRCIandDepositPage");
		elementPresenceVal(sectionRCI, "Section RCI", "CUI_RCI_DepositPage", "verifyOptionsInRCIandDepositPage");
		elementPresenceVal(headerCWPointDeposit, "Header Club Wyndham Point Deposit", "CUI_RCI_DepositPage",
				"verifyOptionsInRCIandDepositPage");

	}

	/*
	 * Method: validatePointDepositLearnMoreCTA Description:Validate Point
	 * Deposit Learn More CTA Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void validatePointDepositLearnMoreCTA() {
		if (verifyObjectDisplayed(pointDepositLearnMoreCTA)) {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.PASS,
					"Learn More CTA for Point Deposit is present in RCI and Deposit Page");
			clickElementJSWithWait(pointDepositLearnMoreCTA);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, pointDepositInformationPage, 120);
			if (verifyObjectDisplayed(pointDepositInformationPage)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.PASS,
						"Successfully clicked on Learn CTA and navigated to Information Page");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.FAIL,
						"Failed to navigate point deposit information page");
			}
		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validatePointDepositLearnMoreCTA", Status.FAIL,
					"Learn More CTA for Point Deposit is not present in RCI and Deposit Page");
		}
	}

	/*
	 * Method: clickCTACWPointDeposit Description:Click Club Wyndham Point
	 * deposit CTA Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickCTACWPointDeposit() {
		if (verifyObjectDisplayed(linkCWDepositPointCTA)) {
			clickElementJSWithWait(linkCWDepositPointCTA);
			waitUntilElementInVisible(driver, loadingSpinner, 120);
			waitUntilElementVisibleBy(driver, headerSelectDeposit, 120);
			if (verifyObjectDisplayed(headerSelectDeposit) && verifyObjectDisplayed(headerPointToDeposit)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTACWPointDeposit", Status.PASS,
						"Successfully clicked on Club Wyndham Point Deposit Link and navigated to Point Deposit Page");
			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTACWPointDeposit", Status.FAIL,
						"Failed to navigated Point Deposit Page");
			}

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTACWPointDeposit", Status.FAIL,
					"Failed to navigated Point Deposit Page");
		}

	}

	/*
	 * Method: pagenavigateBack Description:Navigate Back Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void pagenavigateBack() {
		navigateBack();
		waitUntilElementInVisible(driver, loadingSpinner, 120);
	}

	/*
	 * Method: validateRCILearnMoreCTA Description:Validate RCI Learn More CTA
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateRCILearnMoreCTA() {
		if (verifyObjectDisplayed(RCILearnMoreCTA)) {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.PASS,
					"Learn More CTA for RCI is present in RCI and Deposit Page");
			clickElementJSWithWait(RCILearnMoreCTA);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, headerRCIOverview, 120);
			if (verifyObjectDisplayed(headerRCIOverview)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.PASS,
						"Successfully clicked on Learn CTA and navigated to RCI Overview Page");
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.FAIL,
						"Failed to navigate RCI Overview page");
			}
		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "validateRCILearnMoreCTA", Status.FAIL,
					"Learn More CTA for RCI is not present in RCI and Deposit Page");
		}
	}

	/*
	 * Method: clickButtonRCIExploreResort Description:Click Button RCI Explore
	 * Resort Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickButtonRCIExploreResort() {
		waitUntilElementVisibleBy(driver, buttonExploreRCIResorts, 120);
		if (verifyObjectDisplayed(buttonExploreRCIResorts)) {
			clickElementJSWithWait(buttonExploreRCIResorts);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleBy(driver, headerExploreRCIResort, 120);
			if (verifyObjectDisplayed(headerExploreRCIResort)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickButtonRCIExploreResort", Status.PASS,
						"Successfully clicked on RCI explore Resort Button");
			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickButtonRCIExploreResort", Status.FAIL,
						"Failed to click on RCI explore Resort Button");
			}

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickButtonRCIExploreResort", Status.FAIL,
					"Button Explore Resort not present in RCI and Deposit Point");
		}
	}

	/*
	 * Method: acceptRCITermsandContinue Description:Accept RCI Terms and
	 * Condition and Continue Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void acceptRCITermsandContinue() {
		if (verifyObjectDisplayed(checkBoxAcceptRCITerms)) {
			getElementInView(checkBoxAcceptRCITerms);
			clickElementJSWithWait(checkBoxAcceptRCITerms);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getObject(buttonContinueRCI).getAttribute("class").contains("disabled")) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "acceptRCITermsandContinue", Status.FAIL,
						"Continue to RCI.com button not disabled even terms and condition checkbox not checked");
			} else {
				clickElementJSWithWait(buttonContinueRCI);
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "acceptRCITermsandContinue", Status.PASS,
						"Successfuly clicked on Continue to RCI.com Button");
			}
		}
	}

	/*
	 * Method: verifyRCILink Description:Verify Page RCI.com page Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyRCILink() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(tabs);
		waitUntilElementVisibleBy(driver, linkRCIPage, 120);
		if (verifyObjectDisplayed(linkRCIPage)) {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "verifyRCILink", Status.PASS,
					"Successfully navigated to RCI Page ");
			navigateToMainTab(tabs);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(buttonCancel);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

	}

	/*
	 * Method: clickCTARCIPointDeposit Description:Click CTA RCI Point Deposit
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickCTARCIPointDeposit() {
		if (verifyObjectDisplayed(linkRCIDepositPointCTA)) {
			clickElementJSWithWait(linkRCIDepositPointCTA);
			waitUntilElementInVisible(driver, loadingSpinner, 120);
			waitUntilElementVisibleBy(driver, headerRCIDepositText, 120);
			if (verifyObjectDisplayed(headerRCIDepositText)) {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTARCIPointDeposit", Status.PASS,
						"Successfully clicked on RCI Point Deposit Link and navigated to Point Deposit Page");
			} else {
				tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTARCIPointDeposit", Status.FAIL,
						"Failed to navigated Point Deposit Page");
			}

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "clickCTARCIPointDeposit", Status.FAIL,
					"Link RCI Deposit Point not found");
		}

	}

	/*
	 * Method: selectPointsAndContinueCTA Description:Validate RCI Learn More
	 * CTA Date: September/2020 Author: Saket Sharma Changes By: NA
	 */
	public void selectPointsAndContinueCTA() {
		if (verifyObjectDisplayed(headerSelectDeposit1)) {
			clickElementJSWithWait(checkBoxYearSelect);
			waitUntilElementInVisible(driver, loadingSpinner, 120);
			waitUntilElementVisibleBy(driver, labelPointsToDeposit, 120);

			Assert.assertTrue(verifyObjectDisplayed(labelPointsToDeposit));
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "selectPointsAndContinueCTA", Status.PASS,
					"Successfully Checked the year checkbox");

			getElementInView(headerSelectDeposit1);
			String strUserName = testData.get("header1");
			fieldDataEnter(fieldDepositPoint, strUserName);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(fieldDepositPoint).sendKeys(Keys.TAB);

			getElementInView(fieldDepositPoint);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementJSWithWait(buttonContinue);
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "selectPointsAndContinueCTA", Status.PASS,
					"Successfully clicked the continue button");

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "selectPointsAndContinueCTA", Status.FAIL,
					"Link RCI Deposit Point page not navigated");
		}

	}

	public void emailConfirmation() {

		if (verifyObjectDisplayed(labelEmailConfirm)) {

			Assert.assertTrue(verifyObjectDisplayed(labelEmailConfirm));
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "emailConfirmation", Status.PASS,
					"Successfully Navigated to Email Confirmation page");

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			Assert.assertTrue(!getElementAttribute(fieldEmailConfirmation, "value").equals(""));
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "emailConfirmation", Status.PASS,
					"Successfully Showed the default email address");

			getElementInView(buttonConfirmDeposit);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJSWithWait(buttonConfirmDeposit);
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "emailConfirmation", Status.PASS,
					"Successfully clicked the Confirm Deposit button");
			pageCheck();
			Assert.assertTrue(verifyObjectDisplayed(labelDepositConfirm));
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "emailConfirmation", Status.PASS,
					"Successfully Navigated to CONGRATULATIONS page");

		} else {
			tcConfig.updateTestReporter("CUI_RCI_DepositPage", "emailConfirmation", Status.FAIL,
					"Link RCI Deposit Point not found");
		}

	}

}
