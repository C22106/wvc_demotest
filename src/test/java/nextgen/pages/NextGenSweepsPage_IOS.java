package nextgen.pages;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenSweepsPage_IOS extends NextGenSweepsPage_Web {

	public static final Logger log = Logger.getLogger(NextGenSweepsPage_IOS.class);

	public NextGenSweepsPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		searchContainer = By.xpath("//div//input[contains(@class,'mobile-search__input')]");
		searchButton = By.xpath("(//div//button[contains(@class,'mobile-search__icon')])[1]");

	}

	@Override
	public void wyndhamSweepsNavigation() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, searchIcon, 120);

		if (verifyObjectDisplayed(searchIcon)) {

			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS,
					"Search Icon Present on Homepage");

			clickElementBy(searchIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchContainer1)) {

				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS,
						"Search Container displayed with text: "
								+ driver.findElement(searchContainer1).getAttribute("placeholder"));

				driver.findElement(searchContainer1).click();
				driver.findElement(searchContainer1).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS,
						"Search Keyword Entered");

				clickElementJSWithWait(searchButton1);

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

				if (verifyObjectDisplayed(searchResultsHeader)) {
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
							"Navigated to Search Results page");

					List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
							"No Of Results on first page " + listOfResults.size());

					for (int i = 0; i < listOfResults.size(); i++) {

						if (listOfResults.get(i).getText().equalsIgnoreCase("Wyndham Sweeps")) {
							listOfResults.get(i).click();

							waitUntilElementVisibleBy(driver, homeBreadcrumb, 120);
							break;
						}

					}

				} else {
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
							"Unable to navigate to search results page");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.FAIL,
						"Error in getting the search container");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.FAIL,
					"Error in getting the search Icon");
		}

	}

	@Override
	public void wyndhamSweepsValidation() throws AWTException {

		getElementInView(pageHeader);
		if (verifyObjectDisplayed(pageHeader)) {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
					"Navigated To Wyndham Sweeps Page, Main Header Present");

			if (verifyObjectDisplayed(sweepsForm)) {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Form Present");

				if (verifyObjectDisplayed(sweepsFirstName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"First Name Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name*", testData.get("firstName"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsLastName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Last Name Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name*", testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsAddress)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Address Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Address*", testData.get("Address"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Address Field not present");
				}

				if (verifyObjectDisplayed(sweepsCity)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"City Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "City*", testData.get("City"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"City Field not present");
				}

				if (verifyObjectDisplayed(sweepsPhone)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Mobile Phone", testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(sweepsEmail)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email", testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(sweepsZip)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Zip Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Zip Code*", testData.get("ZipCode"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Zip Field not present");
				}

				if (verifyObjectDisplayed(sweepsHomePh)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Home Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Home Phone*", testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Home Phone Field not present");
				}

				if (verifyObjectDisplayed(maritalStatus)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Marital Status Drop Down present");

					new Select(driver.findElement(maritalStatus)).selectByVisibleText(testData.get("Marital Status"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Marital Status Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsAge)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Age Drop Down present");

					new Select(driver.findElement(sweepsAge)).selectByVisibleText(testData.get("Age"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Age Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsIncome)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Income Drop Down present");

					new Select(driver.findElement(sweepsIncome)).selectByVisibleText(testData.get("Income"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Income Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsState)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"State Drop Down present");

					new Select(driver.findElement(sweepsState)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(accessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code present as: " + getElementText(accessCode));

					strAccesCode = getElementText(accessCode);
					System.out.println(strAccesCode);

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code not present");
				}

				if (verifyObjectDisplayed(agreeBnDisable)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Disabled before enetering Acces code");

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button Enabled before enetering Acces code");
				}

				if (verifyObjectDisplayed(sweepsAccessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code Field Present");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// Robot rob = new Robot();
					// rob.mouseWheel(2);
					// clickElementJSWithWait(sweepsAccessCode);
					driver.findElement(sweepsAccessCode).click();

					driver.findElement(sweepsAccessCode).sendKeys(strAccesCode);
					// fieldDataEnter(sweepsAccessCode, strAccesCode);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code Field not present");
				}

				if (verifyObjectDisplayed(agreeBnEnable)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Enabled After enetering Access code");
					clickElementBy(agreeBnEnable);

					waitUntilElementVisibleBy(driver, thankYouNote, 240);

					if (verifyObjectDisplayed(thankYouNote)) {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
								"Form Submission Successful");

					} else {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
								"Form Submission not Successful");
					}

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button disabled after enetering Acces code");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Form not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
					"Wyndham Sweeps Navigation Failed");
		}

	}

	@Override
	public void wyndhamSweepsNav() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(searchContainer)) {

			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS,
					"Search Container displayed with text: "
							+ driver.findElement(searchContainer).getAttribute("placeholder"));

			driver.findElement(searchContainer).click();
			driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS, "Search Keyword Entered");

			clickElementJSWithWait(searchButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

			if (verifyObjectDisplayed(searchResultsHeader)) {
				tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
						"Navigated to Search Results page");

				List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
				tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
						"No Of Results on first page " + listOfResults.size());

				for (int i = 0; i < listOfResults.size(); i++) {

					if (listOfResults.get(i).getText().equalsIgnoreCase("Wyndham Sweeps")) {
						String href = getElementAttribute(listOfResults.get(i), "href").trim();
						listOfResults.get(i).click();
						pageCheck();
						assertTrue(getCurrentURL().trim().equals(href), "Navigation Failed");
						break;
					}

				}

			} else {
				tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
						"Unable to navigate to search results page");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.FAIL,
					"Error in getting the search container");
		}

	}

	@Override
	public void wyndhamSweepsVal() throws AWTException {

		getElementInView(pageHeader);
		if (verifyObjectDisplayed(pageHeader)) {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
					"Navigated To Wyndham Sweeps Page, Main Header Present");

			if (verifyObjectDisplayed(sweepsForm)) {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Form Present");

				getElementInView(sweepsFirstName);

				if (verifyObjectDisplayed(sweepsFirstName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"First Name Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name *", testData.get("firstName"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsLastName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Last Name Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name*", testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsAddress)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Address Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Address*", testData.get("Address"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Address Field not present");
				}

				if (verifyObjectDisplayed(sweepsCity)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"City Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "City*", testData.get("City"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"City Field not present");
				}

				if (verifyObjectDisplayed(sweepsState)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"State Drop Down present");

					new Select(driver.findElement(sweepsState)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsZip)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Zip Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Zip Code*", testData.get("ZipCode"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Zip Field not present");
				}
				if (verifyObjectDisplayed(sweepsEmail)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email", testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(sweepsPhone)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Mobile Phone", testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(sweepsHomePh)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Home Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Home Phone*", testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Home Phone Field not present");
				}

				if (verifyObjectDisplayed(maritalStatus)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Marital Status Drop Down present");

					new Select(driver.findElement(maritalStatus)).selectByVisibleText(testData.get("Marital Status"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Marital Status Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsAge)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Age Drop Down present");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					new Select(driver.findElement(sweepsAge)).selectByVisibleText(testData.get("Age"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Age Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsIncome)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Income Drop Down present");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					new Select(driver.findElement(sweepsIncome)).selectByVisibleText(testData.get("Income"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Income Drop Down not present");
				}

				if (verifyObjectDisplayed(accessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code present as: " + getElementText(accessCode));

					strAccesCode = getElementText(accessCode);
					System.out.println(strAccesCode);

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code not present");
				}

				if (verifyObjectDisplayed(agreeBnDisable)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Disabled before enetering Acces code");

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button Enabled before enetering Acces code");
				}

				if (verifyObjectDisplayed(sweepsAccessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code Field Present");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(sweepsAccessCode).click();

					driver.findElement(sweepsAccessCode).sendKeys(strAccesCode);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code Field not present");
				}

				if (verifyObjectDisplayed(agreeBnEnable)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Enabled After enetering Access code");
					clickElementBy(agreeBnEnable);

					waitUntilElementVisibleBy(driver, thankYouNote, 240);

					if (verifyObjectDisplayed(thankYouNote)) {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
								"Form Submission Successful");

					} else {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
								"Form Submission not Successful");
					}

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button disabled after enetering Acces code");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Form not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
					"Wyndham Sweeps Navigation Failed");
		}

	}
}
