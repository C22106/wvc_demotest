package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenFAQPage_IOS extends NextGenFAQPage_Web {

	public static final Logger log = Logger.getLogger(NextGenFAQPage_IOS.class);

	public NextGenFAQPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
