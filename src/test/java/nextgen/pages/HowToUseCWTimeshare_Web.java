package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class HowToUseCWTimeshare_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(HowToUseCWTimeshare_Web.class);

	public HowToUseCWTimeshare_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By howToUseTimeShare = By.xpath(/* "//h2[contains(.,'How to Use a Club Wyndham')]" */
			"//div[contains(text(),'How to Use a Club Wyndham')]");
	protected By howToUseTimeShareCTA = By.xpath(
			/*
			 * "//h2[contains(.,'How to Use a Club Wyndham')]/ancestor::div[@class='wyn-card__content']//a[contains(.,'Read')]"
			 */
			"//div[contains(text(),'How to Use a Club Wyndham')]//..//..//a[contains(.,'Read')]");

	protected By howToUseCWTimeshareHeader = By.xpath(/* "//h1[contains(.,'How to Use Your')]" */
			"(//div[contains(text(),'HOW TO USE YOUR')])[1]");
	protected By timeShareHeroImg = By.xpath(/*
												 * "//h1[contains(.,'How to Use Your')]//ancestor::div[@class='wyn-hero wyn-hero--large']//img"
												 */
			"(//div[contains(text(),'HOW TO USE YOUR')])[1]//ancestor::div[@class='cardBanner']//img");
	protected By timeshareBreadcrumb = By.xpath(/*
												 * "//nav[@class='wyn-breadcrumbs']//a[contains(.,'How To Use Club')]"
												 */
			"//div[@class='breadcrumb']//li/a[text()='How to use Club Wyndham Timeshare']");
	protected By stepsHeader = By.xpath(/* "//div[@class='wyn-steps']//h2" */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1')]");
	protected By stepNumber = By.xpath("//div[@class='wyn-steps__step']");
	protected By stepImage = By.xpath(/* "//div[@class='wyn-carousel-block__image']//img" */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='imageSlice']");

	protected By whyClubWyndhamHeader = By.xpath(
			"//div[@class='wyn-header__nav-container']//li[@class='has-submenu' and contains(.,'Be in the Club)]");
	protected By prideNav = By.xpath("//div[@class='wyn-fly-out']//li[contains(.,'Pride of Owner')]");

	// knowYourProg
	protected By knowProgImg = By.xpath(
			/*
			 * "//h2[contains(.,'Know Your Program')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//img"
			 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Know Your Program')]/../../../../..//div[@class='imageSlice']");
	protected By knowProgDesc = By.xpath(
			/*
			 * "//h2[contains(.,'Know Your Program')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//p"
			 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Know Your Program')]//..//div[contains(@class,'body-1')]/p");
	protected By knowProgStepno = By.xpath(
			"//h2[contains(.,'Know Your Program')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//div[@class='wyn-steps__step']");
	protected By knowProgCurrentTab = By.xpath(
			"//h2[contains(.,'Know Your Program')]/ancestor::div[@class='wyn-carousel-block__content']//div[@class='wyn-sliding-tabs__container']//div[@class='slick-list draggable']//a/parent::div");
	protected By knowSlidingTabTitle = By.xpath(
			/*
			 * "//h2[contains(.,'Know Your Program')]/ancestor::div[@class='wyn-carousel-block__content']//div[@class='wyn-sliding-tabs__container']//div[@class='slick-list draggable']//a"
			 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Know Your Program')]//..//..//..//..//div[@class='tabSlice']//ul[@class='tabs AEMTab']/li/a");

	// ChooseTravelDates
	protected By travelDatesImg = By.xpath(
			"//h2[contains(.,'Choose Your Travel')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//img");
	protected By travelDatesDesc = By.xpath(
			/*
			 * "//h2[contains(.,'Choose Your Travel')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//p"
			 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Travel Dates')]//..//div[contains(@class,'body-1')]/p");
	protected By travelDateStepno = By.xpath(
			"//h2[contains(.,'Choose Your Travel')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//div[@class='wyn-steps__step']");
	protected By travelDateCurrentTab = By.xpath(
			"//h2[contains(.,'Choose Your Travel')]/ancestor::div[@class='wyn-carousel-block__content']//div[@class='wyn-sliding-tabs__container']//div[@class='slick-list draggable']//a/parent::div");
	protected By travelDatesSlidingTabTitle = By.xpath(
			/*
			 * "//h2[contains(.,'Choose Your Travel')]/ancestor::div[@class='wyn-carousel-block__content']//div[@class='wyn-sliding-tabs__container']//div[@class='slick-list draggable']//a"
			 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Travel Dates')]//..//..//..//..//div[@class='tabSlice']//ul[@class='tabs AEMTab']/li/a");

	// findResorts
	protected By findResortImg = By.xpath(
			"// h2[contains(.,'Find Resorts')]/ancestor::div[@class='wyn-l-margin-large--bottomwyn-steps__container']//div[@class='wyn-carousel-block__image']//img");
	protected By findResortDesc = By.xpath(
			/*
			 * "//h2[contains(.,'Find Resorts')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//p"
			 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Find Resorts')]//..//div[contains(@class,'body-1')]/p");
	protected By findResortStepno = By.xpath(
			"//h2[contains(.,'Find Resorts')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//div[@class='wyn-steps__step']");

	protected By findResortIcons = By.xpath(
			"//h2[contains(.,'Find Resorts')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//span[@class='wyn-icon__container']//img");

	// Room Type
	protected By roomTypeImg = By.xpath(
			"//h2[contains(.,'Room Type')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//img");
	protected By roomTypeDesc = By.xpath(
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Pick Your Room')]//..//div[contains(@class,'body-1')]/p");
	protected By roomTypeStepno = By.xpath(
			"//h2[contains(.,'Room Type')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//div[@class='wyn-steps__step']");
	protected By roomTypeCurrentTab = By.xpath(
			"//h2[contains(.,'Room Type')]/ancestor::div[@class='wyn-carousel-block__content']//div[@class='wyn-sliding-tabs__container']//div[@class='slick-list draggable']//a/parent::div");
	protected By roomTypeSlidingTabTitle = By.xpath(
			/*
			 * "//h2[contains(.,'Room Type')]/ancestor::div[@class='wyn-carousel-block__content']//div[@class='wyn-sliding-tabs__container']//div[@class='slick-list draggable']//a"
			 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Pick Your Room Type')]//..//..//..//..//div[@class='tabSlice']//ul[@class='tabs AEMTab']/li/a");

	// Room Size
	protected By roomSizeImg = By.xpath(
			"//h2[contains(.,'Room Size')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//img");
	protected By roomSizeDesc = By.xpath(
			"//h2[contains(.,'Room Size')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//p");
	protected By roomSizeStepno = By.xpath(
			"//h2[contains(.,'Room Size')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//div[@class='wyn-steps__step']");
	protected By roomSizeCurrentTab = By.xpath(
			"//h2[contains(.,'Room Size')]/ancestor::div[@class='wyn-carousel-block__content']//div[@class='wyn-sliding-tabs__container']//div[@class='slick-list draggable']//a/parent::div");
	protected By roomSizeSlidingTabTitle = By.xpath(
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Pick Your Room Size')]//..//..//..//..//div[@class='tabSlice']//ul[@class='tabs AEMTab']/li/a");

	// Book Your Vacay
	protected By bookVacayImg = By.xpath(
			"// h2[contains(.,'Book Your Vaca')]/ancestor::div[@class='wyn-l-margin-large--bottomwyn-steps__container']//div[@class='wyn-carousel-block__image']//img");
	protected By bookVacayDesc = By.xpath(
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Book Your')]//..//div[contains(@class,'body-1')]/p");
	protected By bookVacayStepno = By.xpath(
			"//h2[contains(.,'Book Your Vaca')]/ancestor::div[@class='wyn-l-margin-large--bottom wyn-steps__container']//div[@class='wyn-steps__step']");

	protected By bookVacayButton = By.xpath(
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']/..//div[contains(@class,'title-1') and contains(text(),'Book Your')]/../../../..//a");

	protected By twoColumnVal = By.xpath("//div[@class='cardComponent']//img");
	protected By twoCoumnHeader = By.xpath(
			"//div[@class='cardComponent']//img//..//..//div[@class='card-section']//div[contains(@class,'subtitle-2')]");
	protected By readMoreLink = By.xpath(
			"//div[@class='cardComponent']//img//..//..//div[@class='card-section']//a//div[contains(text(),'Read More')]");

	/*
	 * Method navigateToTimeSharePage Description CW Timeshare page navigation Date
	 * Jul/2019 Author Unnat Jain Changes By
	 */
	public void navigateToTimeSharePage() {

		waitUntilElementVisibleBy(driver, howToUseTimeShare, 120);

		getElementInView(howToUseTimeShare);

		if (verifyObjectDisplayed(howToUseTimeShare)) {
			tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.PASS,
					"How To Use Timeshare block present in the page");

			clickElementBy(howToUseTimeShareCTA);

			waitUntilElementVisibleBy(driver, howToUseCWTimeshareHeader, 120);

			if (verifyObjectDisplayed(howToUseCWTimeshareHeader)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.PASS,
						"How To Use Timeshare page navigated");

			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.FAIL,
						"How To Use Timeshare page navigation failed");

			}

			if (verifyObjectDisplayed(timeshareBreadcrumb)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.PASS,
						"How To Use Timeshare breadcrumb present");

			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.FAIL,
						"How To Use Timeshare breadcrumb not present");

			}

			if (verifyObjectDisplayed(timeShareHeroImg)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.PASS,
						"How To Use Timeshare hero image present");

			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.FAIL,
						"How To Use Timeshare hero image not present");

			}

		} else {
			tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.FAIL,
					"How To Use Timeshare block not present in the page");
		}

	}

	/*
	 * Method timesharePageValidations Description CW Timeshare page Validations
	 * Date Jul/2019 Author Unnat Jain Changes By
	 */
	public void timesharePageValidations() {

		try {
			List<WebElement> headerList = driver.findElements(stepsHeader);
			// List<WebElement> stepNoList = driver.findElements(stepNumber);
			List<WebElement> imageList = driver.findElements(stepImage);

			tcConfig.updateTestReporter("HowToUseCWTimeshare", "timesharePageValidations", Status.PASS,
					"How To Use Timeshare steps present, total: " + headerList.size());

			if (headerList.size() == imageList.size()) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "timesharePageValidations", Status.PASS,
						"Image present for all the steps");
			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "timesharePageValidations", Status.FAIL,
						"Image not present for all the steps");
			}

			for (int i = 0; i < headerList.size(); i++) {

				getElementInView(headerList.get(i));
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "timesharePageValidations", Status.PASS,
						"Title number " + (i + 1) + " is "
								+ headerList.get(i).getText()/*
																 * + " and step No: " + stepNoList.get(i).getText()
																 */);

				stepsValidations(i);

			}

			getElementInView(twoColumnVal);

			if (verifyObjectDisplayed(twoColumnVal)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "timesharePageValidations", Status.PASS,
						"Two Coloumn section present in the page");
				List<WebElement> sectionHeader = driver.findElements(twoCoumnHeader);
				/* List<WebElement> readMoreList = driver.findElements(readMoreLink); */
				int totalSize = sectionHeader.size();
				for (int i = 0; i < totalSize; i++) {
					List<WebElement> sectionHeader1 = driver.findElements(twoCoumnHeader);
					List<WebElement> readMoreList = driver.findElements(readMoreLink);
					tcConfig.updateTestReporter("HowToUseCWTimeshare", "timesharePageValidations", Status.PASS,
							(i + 1) + " header is: " + sectionHeader1.get(i).getText());

					clickElementJSWithWait(readMoreList.get(i));

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					// driver.navigate().refresh();
					System.out.println(driver.getTitle().toUpperCase());
					System.out.println(testData.get("header" + (i + 1) + "").toUpperCase());
					if (driver.getTitle().toUpperCase().contains(testData.get("header" + (i + 1) + "").toUpperCase())) {

						tcConfig.updateTestReporter("HowToUseCWTimeshare", "timesharePageValidations", Status.PASS,
								"Navigated to " + (i + 1) + " page");

						driver.navigate().back();

						waitUntilElementVisibleBy(driver, twoColumnVal, 120);

					}

				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Two Coloumn section not present in the page");
			}

		} catch (Exception e) {

			tcConfig.updateTestReporter("HowToUseCWTimeshare", "navigateToTimeSharePage", Status.FAIL,
					"Error In Validating timeshare Steps");
		}

	}

	/*
	 * Method stepsValidations Description CW Timeshare page different steps
	 * validation Date Jul/2019 Author Unnat Jain Changes By
	 */
	public void stepsValidations(int stepNo) {

		switch (stepNo) {
		case 0:

			if (verifyObjectDisplayed(knowProgDesc) && verifyObjectDisplayed(knowProgImg)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Know your program desc present");
			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.FAIL,
						"Know your program desc not  present");
			}

			List<WebElement> knowPorgSlidingTabs = driver.findElements(knowSlidingTabTitle);
			tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
					"Total Tabs present: " + knowPorgSlidingTabs.size());
			for (int j = 0; j < knowPorgSlidingTabs.size(); j++) {
				knowPorgSlidingTabs.get(j).click();
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Header " + (j + 1) + " is: " + knowPorgSlidingTabs.get(j).getText());

			}

			break;
		case 1:

			if (verifyObjectDisplayed(travelDatesDesc)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Choose Your Travel Dates desc present");
			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.FAIL,
						"Choose Your Travel Dates desc not  present");
			}

			List<WebElement> travelDatesSlidingTabs = driver.findElements(travelDatesSlidingTabTitle);
			tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
					"Total Tabs present: " + travelDatesSlidingTabs.size());
			for (int j = 0; j < travelDatesSlidingTabs.size(); j++) {
				travelDatesSlidingTabs.get(j).click();
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Header " + (j + 1) + " is: " + travelDatesSlidingTabs.get(j).getText());

			}
			break;
		case 2:

			if (verifyObjectDisplayed(findResortDesc)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Find Resort desc present");
			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.FAIL,
						"Find Resorts desc not  present");
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * List<WebElement> findResortIconList = driver.findElements(findResortIcons);
			 * 
			 * if (verifyElementDisplayed(findResortIconList.get(0))) {
			 * tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations",
			 * Status.PASS, "Find Resort icon images present, total " +
			 * findResortIconList.size()); } else {
			 * tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations",
			 * Status.FAIL, "Find Resorts icon image not  present"); }
			 */

			break;
		case 3:

			if (verifyObjectDisplayed(roomTypeDesc)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Room Type desc present");
			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.FAIL,
						"Room Types desc not  present");
			}

			List<WebElement> roomTypeSlidingTabs = driver.findElements(roomTypeSlidingTabTitle);
			tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
					"Total Tabs present: " + roomTypeSlidingTabs.size());
			for (int j = 0; j < roomTypeSlidingTabs.size(); j++) {
				roomTypeSlidingTabs.get(j).click();
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Header " + (j + 1) + " is: " + roomTypeSlidingTabs.get(j).getText());

			}
			break;
		case 4:

			/*
			 * if (verifyObjectDisplayed(roomSizeDesc)) {
			 * tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations",
			 * Status.PASS, "Room Size desc present"); } else {
			 * tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations",
			 * Status.FAIL, "Room Sizes desc not  present"); }
			 */

			List<WebElement> roomSizeSlidingTabs = driver.findElements(roomSizeSlidingTabTitle);
			tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
					"Total Tabs present: " + roomSizeSlidingTabs.size());
			for (int j = 0; j < roomSizeSlidingTabs.size(); j++) {
				roomSizeSlidingTabs.get(j).click();
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Header " + (j + 1) + " is: " + roomSizeSlidingTabs.get(j).getText());
			}
			break;

		case 5:

			if (verifyObjectDisplayed(bookVacayDesc)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Book Your Vacay desc present");
			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.FAIL,
						"Book Your Vacay desc not  present");
			}

			if (verifyObjectDisplayed(bookVacayButton)) {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.PASS,
						"Button present to book your vacay");
				clickElementBy(bookVacayButton);

				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (driver.getCurrentUrl().toUpperCase().contains(testData.get("exploreResortUrl").toUpperCase())) {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.PASS,
							"Navigated to associated page");

					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.FAIL,
							"associated page navigation failed");
				}

				driver.navigate().back();

			} else {
				tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.FAIL,
						"Button not present in Book Your Vacay");
			}

			break;

		default:
			tcConfig.updateTestReporter("HowToUseCWTimeshare", "stepsValidations", Status.FAIL,
					"Error in Timeshare Steps Validation");
			break;
		}

	}

}
