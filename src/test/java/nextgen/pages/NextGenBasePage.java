package nextgen.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class NextGenBasePage extends FunctionalComponents {

	public NextGenBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	protected By loadingSpinner = By.xpath("//div[@class='loading-wrap']");

	/*
	 * Check Loading Spinner in Trip Application
	 */
	public void checkLoadingSpinnerTrip() {
		waitUntilElementInVisible(driver, loadingSpinner, 120);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser Author : CTS Date : 2019 Change By : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change By
	 * : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

}
