package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenTravelInspiration_Android extends NextGenTravelInspiration_Web {

	public static final Logger log = Logger.getLogger(NextGenTravelInspiration_Android.class);

	public NextGenTravelInspiration_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
