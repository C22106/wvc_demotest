package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenFeaturedDestinations_IOS extends NextGenFeaturedDestinations_Web {

	public static final Logger log = Logger.getLogger(NextGenFeaturedDestinations_IOS.class);

	public NextGenFeaturedDestinations_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
