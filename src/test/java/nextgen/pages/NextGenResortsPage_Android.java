package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenResortsPage_Android extends NextGenResortsPage_Web {

	public static final Logger log = Logger.getLogger(NextGenResortsPage_Android.class);

	public NextGenResortsPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		resortsHeader = By.xpath("//ul[contains(@class,'mobile')]//li/a[text()='Resorts']");
		firstNameField = By.xpath("//input[@id='firstName']");
		lastNameField = By.xpath("//input[@id='lastName']");
		phoneField = By.id("phone");
		emailField = By.id("email");
		stateSelect = By.xpath("//select[@name='state']");
		submitButton = By.xpath("//form//button[contains(.,'Submit')]");
		thankConfirm = By.xpath("//div[contains(@class,'notification')]//div[contains(@class,'title') and contains(text(),'Thank you')]");

	}
	
	String location="";
	
	protected By locationObj = By.xpath("//span[@class='pac-matched' and contains(text(),'" + location + "')]");

	@Override
	public void navigateToResortsPage() {
		waitUntilElementVisibleBy(driver, resortsHeader, 120);

		if (verifyObjectDisplayed(resortsHeader)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.PASS,
					"Resorts present in main Nav");
			String href = getElementAttribute(resortsHeader, "href");
			driver.findElement(resortsHeader).click();
			pageCheck();
			if (href.equals(getCurrentURL().trim())) {
				tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.FAIL,
						"Resort Page Navigated");
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.PASS,
						"Navigation Failed");
			}

		}
	}

	@Override
	public void navigateToResortDetails() {
		location = testData.get("strSearch");
		waitUntilElementVisibleBy(driver, searchByKeywor, 120);
		sendKeysByScriptExecutor((RemoteWebDriver) driver, "Location", location);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		getObject(locationObj).click();
		try {
			waitUntilElementVisibleBy(driver, resortsTitle, 30);
		} catch (Exception e) {

		}

		Boolean tempVal = false;
		List<WebElement> resortList = driver.findElements(resortsTitle);
		for (int i = 0; i < resortList.size(); i++) {

			if (resortList.get(i).getText().split("›")[0].toUpperCase()
					.contains(testData.get("strSearch").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.PASS,
						resortList.get(i).getText() + " Resort dispalyed");

				clickElementWb(resortList.get(i));

				waitUntilElementVisibleBy(driver, resortPageTitle, 120);

				getElementInView(resortPageTitle);
				if (getElementText(resortPageTitle).toUpperCase().contains(testData.get("strSearch").toUpperCase())) {

					tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.PASS,
							"Navigated To Resorts Page with Title: " + getElementText(resortPageTitle));
					tempVal = true;
					break;

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.FAIL,
							"Resorts Page Navigation Failed");
				}

			} else {
				tempVal = false;
				if (i == (resortList.size() - 1)) {
					break;
				}
			}

		}

		if (tempVal == false) {
			tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.FAIL,
					"Searched Resort Not Displayed");
		}

	}

	@Override
	public void resortDetailsVal() {

		if (verifyObjectDisplayed(resortImage)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Resort Hero Image Present");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Resort Hero Image not Present");
		}

		if (verifyObjectDisplayed(resortPageTitle)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Resort Name is: " + getElementText(resortPageTitle));
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Resort Type not Present");
		}

		if (verifyObjectDisplayed(resortAddress)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Resort Address Present");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Resort Address not Present");
		}

		if (verifyObjectDisplayed(shareCTA)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS, "Share CTA Present");

			clickElementBy(shareCTA);
			getElementInView(shareCTA);

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL, "Share CTA not Present");
		}

		if (verifyObjectDisplayed(bookStay)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Book Stay Section Present");
			// getElementInView(checkAvailability);
			clickElementBy(checkAvailability);

			waitUntilElementVisibleBy(driver, loginPopUp, 50);

			if (verifyObjectDisplayed(loginPopUp)) {
				tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
						"Login Button pop up opened");

				List<WebElement> headers = driver.findElements(loginPopUpHdr);
				List<WebElement> headersBtn = driver.findElements(loginPopUpBtn);

				if (headers.size() == 2) {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
							"Login Pop Up Headers present");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
							"Login Pop Up Headers not present");
				}

				if (headersBtn.size() == 2) {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
							"Login Pop Up Headers CTA present");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
							"Login Pop Up Headers CTA not present");
				}

				if (verifyObjectDisplayed(loginPopUpClose)) {
					clickElementBy(loginPopUpClose);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
							"Login Pop Up close CTA not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
						"Login Button pop up not opened");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Book Stay Section not Present");
		}

	}

	@Override
	public void resortformValidations() {

		getElementInView(firstNameField);
		if (verifyObjectDisplayed(firstNameField)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS, "First Name Field Present");

			// fieldDataEnter(firstNameField, testData.get("firstName"));
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name *", testData.get("firstName"));

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"First Name Field not present");
		}

		if (verifyObjectDisplayed(lastNameField)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS, "Last Name Field Present");

			// fieldDataEnter(lastNameField, testData.get("lastName"));
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name *", testData.get("lastName"));//

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Last Name Field not present");
		}

		if (verifyObjectDisplayed(phoneField)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS, "Phone Field Present");

			// fieldDataEnter(phoneField, testData.get("phoneNo"));
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Phone Number *", testData.get("phoneNo"));

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL, "Phone Field not present");
		}

		if (verifyObjectDisplayed(emailField)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS, "Email Field Present");

			// fieldDataEnter(emailField, testData.get("email"));
			sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address *", testData.get("email"));

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL, "Email Field not present");
		}

		if (verifyObjectDisplayed(stateSelect)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS, "State Drop Down present");
			new Select(driver.findElement(stateSelect)).selectByVisibleText(testData.get("stateSel"));

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"State Drop Down not present");
		}

		if (verifyObjectDisplayed(checkBox)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS, "T&C checkbox present");

			clickElementJSWithWait(checkBox);

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL, "T&C checkbox not present");
		}

		if (verifyObjectDisplayed(submitButton)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS, "Submit button present");

			if (verifyObjectDisplayed(submitButton))
				clickElementJSWithWait(submitButton);

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Submit Button not present");
		}

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, thankConfirm, 120);

		if (verifyObjectDisplayed(thankConfirm)) {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
					"Form Submission Successful");

			
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Submission not Successful");
		}

	}

}
