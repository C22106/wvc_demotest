package nextgen.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUIMyAccountPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUIMyAccountPage_Web.class);

	public CUIMyAccountPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	/* Owner Name Section */
	protected By welcomeBackHeader = By.xpath("//h4[contains(.,'WELCOME BACK')]");
	protected By welcomeToClubHeader = By.xpath("//h4[contains(.,'WELCOME TO THE CLUB')]");
	protected By ownerFullName = By.xpath("//h4[contains(.,'WELCOME')]/parent::div/h2");
	protected By vipStatus = By
			.xpath("//h4[contains(.,'WELCOME')]/parent::div//div/span[contains(.,'Member #')]/preceding-sibling::span");
	protected By memberNumber = By.xpath("//h4[contains(.,'WELCOME')]/parent::div//div/span[contains(.,'Member #')]");
	protected By leftRailOwnershipLink = By
			.xpath("//div[contains(@class,'grid-container')]//span[contains(.,'My Ownership')]");
	protected By leftRailMembershipLink = By
			.xpath("//div[contains(@class,'grid-container')]//span[contains(.,'My Membership')]");
	protected By imageSpace = By.xpath("//div[@class='img-wrapper']");
	protected By imageInitials = By.xpath("//div[@class='img-wrapper']/h4");
	protected By imageUploaded = By.xpath("//div[@class='img-wrapper' and @style]");
	protected By viewAccountLink = By.xpath("//a[contains(.,'View or Edit Account')]");
	protected By accountSettingsHeader = By.xpath("//h1[contains(.,'ACCOUNT SETTING')]");
	protected By homeBreadCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	protected By ownerFullNameSettings = By.xpath("//div/h5[contains(.,'OWNER')]/following-sibling::p");
	/* Ownership Section */
	protected By myOwnerShipHeader = By.xpath("//h5[contains(.,'MY OWNERSHIP')]");
	protected By myMemberShipHeader = By.xpath("//h5[contains(.,'MY MEMBERSHIP')]");
	protected By viewOwnerShipLink = By.xpath("//a[contains(.,'View My')]");
	protected By resultsHeader = By.xpath("//h5[contains(.,'MY OWNERSHIP')]/parent::div//span[contains(.,'Contract')]");
	protected By contractNumbers = By.xpath("//h5[contains(.,'MY OWNERSHIP')]/parent::div//strong");
	protected By myOwnerShipPage = By.xpath("//h1[contains(.,'SHIP')]");

	/* Point Summary Section */
	protected By useYearPoints = By.xpath("//span[contains(.,'Current Use')]/following-sibling::strong");
	protected By useYearPendingPoints = By
			.xpath("//span[contains(.,'Current Use')]/following-sibling::strong[contains(.,'Pending')]");
	protected By pointsComingSoon = By
			.xpath("//span[contains(.,'Current Use')]/following-sibling::strong[contains(.,'Coming Soon')]");
	protected By useYearDate = By.xpath("//span[contains(.,'Use Year Date')]/following-sibling::strong");
	protected By useYearDatePending = By.xpath("//span[contains(.,'Use Year Date')]/following-sibling::p");
	protected By useYearPendingDates = By
			.xpath("//span[contains(.,'Use Year Date')]/following-sibling::p[contains(.,'Pending')]");
	protected By datesComingSoon = By
			.xpath("//span[contains(.,'Use Year Date')]/following-sibling::p[contains(.,'Coming Soon')]");
	protected By pointsSummaryHeader = By.xpath("//h5[contains(.,'POINTS SUMMARY')]");
	protected By currentUseYearHeader = By
			.xpath("//h5[contains(.,'POINTS SUMMARY')]/parent::div//span[contains(.,'Current Use Year')]");
	protected By viewMyPointsLink = By.xpath("//a[contains(.,'Manage My Points')]");

	protected By useYearDateHeader = By
			.xpath("//h5[contains(.,'POINTS SUMMARY')]/parent::div//span[contains(.,'Use Year Date')]");

	protected By pointsSummaryPage = By.xpath("//h1[contains(.,'POINTS SUMMARY')]");

	/* Find Your Vacation Section */
	protected By searchAvailabilityForm = By.id("search-availability-form");
	protected By textSearchHeader = By.xpath(
			"(//div[contains(@class,'for-medium')]//div[text() = 'Find Your Next Vacation']|//div[contains(@class,'for-medium')]//div[text() = 'Find Your Next Vacation']//div[contains(@id,'availability')]/form/div[text() = 'Find Your Next Vacation'])");
	protected By textLocationField = By
			.xpath("//div[contains(@class,'for-medium')]//span[text() = 'Search for a location']");
	protected By textDateField = By.xpath("//div[contains(@class,'for-medium')]//span[text() = 'Select Dates']");
	protected By textLocation = By
			.xpath("//div[contains(@class,'for-medium')]//input[@placeholder = 'Enter a location']");
	protected By nextMonthChange = By.xpath(
			"//div[contains(@class,'for-medium')]//div[@aria-label = 'Move forward to switch to the next month.']");
	protected By textBoxDateSelect = // By.xpath("//div[contains(@class,'for-medium')]//input[contains(@name,'searchDate')]/../span
										// |
										// //div[contains(@class,'for-small')]//input[contains(@name,'searchDate')]/../span");
			By.xpath(
					/* "(//div[contains(@class,'for-medium')]//input[contains(@name,'searchDate')]/../span | //div[contains(@class,'for-small')]//input[contains(@name,'searchDate')]/../span)[1] | (//div[contains(@class,'for-medium')]//input[contains(@name,'searchDate')]/../span | //div[contains(@class,'for-small')]//input[contains(@name,'searchDate')]/../span)[2]" */
					"//div[contains(@class,'for-medium')]//input[contains(@name,'searchDate')]/../span");
	protected By listCalendarMonthName = By.xpath(
			/*"//div[contains(@class,'for-medium')]//div[contains(@class,'CalendarMonth') and @data-visible = 'true']//strong | //div[contains(@class,'for-small')]//div[contains(@class,'CalendarMonth') and @data-visible = 'true']//strong"*/
			"//div[contains(@class,'for-medium')]//div[contains(@class,'CalendarMonth') and @data-visible = 'true']//strong");
	protected By buttonSearchAvailability = By
			.xpath("//div[contains(@class,'for-medium')]//button[text() ='Search Availability']");
	protected By buttonCalendarDone = By.xpath(
			"(//div[contains(@class,'for-medium')]//button[text() ='Close'] | //div[contains(@class,'for-medium')]//button[text() ='Done'])");
	protected By selectFlexDateCheckbox = By.xpath("//div[contains(@class,'for-medium')]//label[@for = 'flex-dates']");

	/* Upcoming Reservation */
	protected By noUpcomingReservations = By
			.xpath("//div[@id='upcoming-reservations-carousel']//h6[contains(.,'You Have No Upcoming Trips')]");
	protected By upcomingReservationCards = By
			.xpath("//div[@id='upcoming-reservations-carousel']//div[@class='upcoming-reservation-card']");
	protected By activeReservationCards = By.xpath("//div[@class='upcoming-reservation-card']/parent::div/parent::li");
	protected By reservationPresent = By.xpath("//h6[contains(.,' UPCOMING')]");
	protected By reservationNotPresent = By.xpath("//h6[contains(.,'UPCOMING')]");
	protected By paginationLinks = By.xpath("//nav[@class='orbit-bullets carousel__bullets']//button");
	protected By reservationName = By.xpath("//div[@class='upcoming-reservation-card']//h3");
	protected By viewAllLink = By
			.xpath("//nav[@class='orbit-bullets carousel__bullets']//a//span[contains(.,'View All')]");
	protected By cardImage = By.xpath("//div[@class='upcoming-reservation-card']//img");
	protected By cardAddress = By.xpath("//div[@class='upcoming-reservation-card']//a/following-sibling::p");
	protected By cardMapLink = By.xpath("//div[@class='upcoming-reservation-card']//a[contains(.,'View on Map')]");
	protected By checkInDate = By
			.xpath("//div[@class='upcoming-reservation-card']//h4[contains(.,'In')]/following-sibling::p");
	protected By checkOutDate = By
			.xpath("//div[@class='upcoming-reservation-card']//h6[contains(.,'out')]/following-sibling::p");
	protected By unitType = By
			.xpath("//div[@class='upcoming-reservation-card']//h6[contains(.,'Type')]/following-sibling::p");
	protected By confirmationNumber = By
			.xpath("//div[@class='upcoming-reservation-card']//h6[contains(.,'Confirmation')]/following-sibling::p");
	protected By modifyReservation = By
			.xpath("//li[contains(@class,'active')]//div[@class='upcoming-reservation-card']//a[contains(.,'Modify')]");
	protected By modifyReservationHeader = By
			.xpath("//h1[contains(@class,'title') and contains(.,'Modify Reservation')]");
	protected By cancelReservation = By.xpath(
			"//li[contains(@class,'active')]//div[@class='upcoming-reservation-card']//button[contains(.,'Cancel')]");
	protected By cancelPopUpCloseCTA = By
			.xpath("//div[@id='cancel-reservation-modal' and @aria-hidden='false']//button[@aria-label='Close modal']");
	protected By closedCancelPopUp = By.xpath("//div[@id='cancel-reservation-modal' and @aria-hidden='true']");
	protected By reservationDetailsLink = By
			.xpath("//div[@class='upcoming-reservation-card']//a[contains(.,'View Details')]");
	protected By mapPageHeader = By.xpath("//div[@class='section-hero-header-title-description']//h1");
	protected By reservationSummaryPage = By.xpath("//h2[contains(.,'Reservation Summary')]");
	protected By cancleReservationPopUp = By.id("cancel-reservation-modal-label");
	protected By activeCheckInDate = By
			.xpath("//li[contains(@class,'active')]//h4[contains(.,'In')]/following-sibling::p");

	/* Need Help Section */
	protected By needHelpHeader = By.xpath("//h3/preceding-sibling::div[contains(.,'NEED HELP')]");
	protected By educationHeader = By.xpath("//h3[contains(.,'EDUCATION')]");
	protected By needHelpLinks = By.xpath(
			"//div[@id='owner-education']//div[@class='imageSlice']//following-sibling::div[@class='ctaSlice']//a");
	protected By needHelpImages = By
			.xpath("//div[@id='owner-education']//div[@class='ctaSlice']/preceding-sibling::div[@class='imageSlice']");

	/* Wishlist section */
	protected By myWishListHeader = By.xpath("//h2[contains(.,'MY BUCKET LIST')]");
	protected By viewAllWishlistLink = By
			.xpath("//h2[contains(.,'MY BUCKET LIST')]/following-sibling::a[contains(.,'View All')]");
	protected By wishListItems = By.xpath("//div[contains(@class,'wish-list__wish-items')]");
	protected By wishListResorts = By.xpath("//div[contains(@class,'wish-list__wish-items')]/a//h5");
	protected By exploreAllResorts = By
			.xpath("//div[contains(@class,'wish-list__wish-items')]//a[contains(.,'Explore All')]");
	protected By locationField = By.xpath("//input[contains(@placeholder,'location')]");
	protected By wishListPageHeader = By.xpath("//h1[contains(.,'MY BUCKET LIST')]");
	protected By wishListImages = By.xpath("//div[contains(@class,'wish-list__wish-items')]/a//img");
	protected By wishListHearts = By
			.xpath("//div[contains(@class,'wish-list__wish-items')]/a//div[@class='wish-list__resort-heart']");
	protected By wishListLocation = By.xpath("//div[contains(@class,'wish-list__wish-items')]/a//h6");
	protected By wishListTags = By.xpath("//div[contains(@class,'wish-list__wish-items')]/a//ul");
	protected By emptyWishListIcon = By.xpath("//div[@class='wish-list']//*[name()='svg']");
	protected By emptyWishListHeader = By.xpath("//div[@class='wish-list']//h6[contains(.,'List is Empty')]");
	protected By emptyWishListDescription = By.xpath("//div[@class='wish-list']//p");
	protected By totalWishList = By.xpath("//div[contains(@class,'wish-list__wish-item grid')]");

	/* New Owner Tips */
	protected By newUserTipsHeader = By.xpath("//h3[contains(.,'Tips')]");
	protected By getStartedText = By.xpath("//div[@id='new-owner-flow__tracker']/p");
	protected By tipsDescription = By
			.xpath("//h3[contains(.,'Tips')]/following-sibling::p[contains(@id,'new-owner-flow__tracker')]");
	protected By tipsImage = By.xpath("//button[contains(@id,'tip')]//img[@class='blue-tip']");
	protected By tipsLink = By.xpath("//button[contains(@id,'tip')]//p/span");
	protected By tipsSectionAboveVacation = By.xpath(
			"//section[contains(@class,'reservation')]/preceding-sibling::section[@id='my-account__component--new-owner']");
	protected By tipsSectionBelowVacation = By.xpath(
			"//section[contains(@class,'reservation')]/following-sibling::section[@id='my-account__component--new-owner']");
	protected By newOwnerModalTitle = By.id("new-owner-modal__title");
	protected By modalImages = By.xpath("// div[@id='new-owner-modal']//div[@class='imageSlice']");
	protected By modalTitle = By.xpath(
			"(//div[@id='new-owner-modal']//div[contains(@class,'title')] | //div[@id='new-owner-modal']//h5[contains(@class,'title')])");
	protected By modalText = By.xpath("// div[@id='new-owner-modal']//div[contains(@class,'contentSlice')]//p");
	protected By modalButton = By.xpath("// div[@id='new-owner-modal']//div[contains(@class,'ctaSlice')]//a");
	protected By modalPreviousCTA = By.xpath("// div[@id='new-owner-modal']//a[contains(@class,'prev')]");
	protected By modalNextCTA = By.xpath("// div[@id='new-owner-modal']//a[contains(@class,'next')]");
	protected By modalCloseCTA = By.xpath("// div[@id='new-owner-modal']//button[contains(@class,'close')]");
	protected By newOwnerCheckMark = By.xpath("// span[@class='checkmark']");
	protected By completedText = By.xpath("// p[@id='new-owner-flow__tracker-p']");

	/* Travel Deals/ Popular Resorts Section */
	protected By travelDealsHeader = By.xpath("//h2[contains(.,'TRAVEL DEALS')]");
	protected By popularResortHeader = By.xpath("//h2[contains(.,'Popular Resorts')]");
	protected By totalDeals = By.xpath("//div[@class='cardComponent']//div[@class='grid-x']");
	protected By viewAllDealsLink = By.xpath("//h2[contains(.,'TRAVEL')]/following-sibling::a");
	protected By viewAllResortsLink = By.xpath("//h2[contains(.,'Popular Resorts')]/following-sibling::a");
	protected By travelResortsImage = By.xpath("//div[@class='cardComponent']//img");
	protected By travelResortsName = By.xpath("//div[@class='cardComponent']//div[contains(@class,'subtitle-2')]");
	protected By travelResortsDetails = By.xpath("//div[@class='cardComponent']//p");
	protected By travelBookNowCta = By.xpath("//div[@class='cardComponent']//a");
	protected By headerLogoUpdated = By.xpath("//div[@class='global-navigation__wrapper']//img");
	protected By tipsCompleteDescription = By
			.xpath("//h3[contains(.,'Tips')]/following-sibling::p[contains(@id,'new-owner-flow__tracker-complete')]");
	protected By toolTip = By.xpath("//span[@data-tooltip-class='tooltip info']");
	protected By tootlTipText = By.xpath("//div[@role='tooltip' and contains(.,'Your contract type')]");
	/*
	 * Method: seasonedOwnerHeader Description:Validate Seasoned Owner Welcome
	 * Header Date: Mar/2020 Author: Unnat JainChanges By: NA
	 */

	public void seasonedOwnerHeader() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(welcomeBackHeader));

		tcConfig.updateTestReporter("CUIMyAccountPage", "seasonedOwnerHeader", Status.PASS,
				"Welcome Back Header displayed for Seasoned Owner");

	}

	/*
	 * * *************************Method: toolTipValidations ***
	 * ***************Description: toolTipValidations****
	 */

	public void toolTipValidations(By tootlTipXpath, String toolTipFor, By tootlTipTextXpath) {

		if (verifyObjectDisplayed(tootlTipXpath)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Tool Tip Present in the page for : " + toolTipFor);
			getElementInView(tootlTipXpath);
			clickElementBy(tootlTipXpath);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			toolTipCTAValidations(tootlTipTextXpath, toolTipFor);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Tool Tip not Present in the page for : " + toolTipFor);
		}

	}

	/*
	 * Method: toolTipCTAValidations Description: Tool Tip CTA Validations
	 * 
	 */
	public void toolTipCTAValidations(By tootlTipTextXpath, String toolTipFor) {

		String strFlag = getElementAttribute(tootlTipTextXpath, "aria-hidden");
		if (strFlag.contains("false")) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Tool Tip opened for " + toolTipFor);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Failed to open Tool tip for" + toolTipFor);
		}
	}

	/*
	 * Method: validateToolTip Description:validate tool tip Page Date: Feb/2021
	 * Author: Saket Sharma Changes By: NA
	 */
	public void validateToolTip() {
		waitUntilElementVisibleBy(driver, toolTip, 80);
		toolTipValidations(toolTip, "Accessible suites", tootlTipText);
	}

	/*
	 * Method: userNameValidation Description:Validate User name Header Date:
	 * Apr/2020 Author: Unnat JainChanges By: NA
	 */

	public void userNameValidation() {
		// Owner Name from App
		String strOwnerName = getElementText(ownerFullName).trim().toUpperCase();
		if (strOwnerName.contains(" ")) {
			strOwnerName = strOwnerName.replace(" ", "");
		}
		if (verifyObjectDisplayed(ownerFullName)) {
			if (strOwnerName.contains(getAssociatedName())) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "userNameValidation", Status.PASS,
						"Owner name header present and  displayed correctly: " + getElementText(ownerFullName));
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "userNameValidation", Status.FAIL,
						"Owner Name Displayed is not correct " + getElementText(ownerFullName));
			}
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "userNameValidation", Status.FAIL,
					"Owner name not present");
		}
	}

	/*
	 * Method: getAssociatedName Description:To get Company/Individaul Username
	 * Date: Aor/2020 Author: Unnat Jain Changes By: NA
	 */
	public String getAssociatedName() {
		// Store temporary name
		String tempName = null;

		if (testData.get("AccountAssociated").toLowerCase().contains("individual")) {
			tempName = testData.get("FirstName");
			tempName = tempName.toUpperCase();
		} else if (testData.get("AccountAssociated").toLowerCase().contains("company")) {
			tempName = testData.get("CompanyName").toUpperCase();
		} else {
			tcConfig.updateTestReporter("CUIMYAccountPage", "getAssoicatedName", Status.FAIL,
					"Individual or Company Account not Specified");
		}

		return tempName;
	}

	/*
	 * Method: imageValidations Description:To get Company/Individaul Image
	 * Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void imageValidations() {
		if (verifyObjectDisplayed(imageInitials)) {
			// image Initials form App
			String strImageInit = getElementText(imageInitials).trim();
			// temp initials
			StringBuilder tempInitial = null;
			if (testData.get("AccountAssociated").toLowerCase().contains("individual")) {
				tempInitial = individualNameInitial(tempInitial);
			} else if (testData.get("AccountAssociated").toLowerCase().contains("company")) {
				tempInitial = companyNameInitial(tempInitial);
			}
			if (strImageInit.contains(tempInitial)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "imageValidations", Status.PASS,
						"Profile Pic not uploaded hence initials present as: " + tempInitial);
			} else {

				tcConfig.updateTestReporter("CUIMyAccountPage", "imageValidations", Status.FAIL,
						"Initials not present when profile pic not uploaded");
			}

		} else if (verifyObjectDisplayed(imageUploaded)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "imageValidations", Status.PASS,
					"Owner Image Uploaded and  present");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "imageValidations", Status.FAIL, "Owner Image not present");
		}
	}

	/*
	 * Method:individualNameInitial Description:To get Company/Individaul Image
	 * Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public StringBuilder individualNameInitial(StringBuilder tempInitial) {
		char tempFName = testData.get("FirstName").toUpperCase().charAt(0);
		char tempLName = testData.get("LastName").toUpperCase().charAt(0);
		tempInitial = new StringBuilder().append(tempFName).append(tempLName);

		return tempInitial;
	}

	/*
	 * Method: companyNameInitial Description:To get Company/Individaul Image
	 * Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public StringBuilder companyNameInitial(StringBuilder tempInitial) {
		char tempFName = testData.get("CompanyName").toUpperCase().charAt(0);
		tempInitial = new StringBuilder().append(tempFName).append("");

		return tempInitial;
	}

	/*
	 * Method: imageValidations Description:Points Header Validations Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsHeaderValidations() {
		elementPresenceVal(pointsSummaryHeader, "Points Summary Header ", "CUIMyAccountPage",
				"pointsHeaderValidations");
		elementPresenceVal(currentUseYearHeader, "Current User Year Header ", "CUIMyAccountPage",
				"pointsHeaderValidations");
		elementPresenceVal(useYearDateHeader, "Current User Date Header ", "CUIMyAccountPage",
				"pointsHeaderValidations");
	}

	/*
	 * Method: imageValidations Description:OwnerUser Year Dates val Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void ownerUseYearDates() {

		waitUntilElementVisibleBy(driver, useYearDate, 120);
		String strDates = getElementText(useYearDate).replace(" ", "-");
		strDates = strDates.replace(",", "");
		if (isValidDate(strDates, "MMM-dd-yyyy")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerUseYearDates", Status.PASS,
					"Dates Displayed for this Owner: " + strDates);
		} else if (verifyObjectDisplayed(useYearPendingDates) || verifyObjectDisplayed(datesComingSoon)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerUseYearDates", Status.FAIL,
					"Pending Dates Displayed for this Owner");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerUseYearDates", Status.FAIL,
					"Dates not Displayed for this Owner");
		}

	}

	/*
	 * Method: pendingUseYearDates Description: Pending Use Year Date Val Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void pendingUseYearDates() {

		String strDates = getElementText(useYearDatePending);
		if (isValidDate(strDates, "MMM dd, yyyy")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "pendingUseYearDates", Status.FAIL,
					"Dates Displayed for pending Owner: " + strDates);
		} else if (verifyObjectDisplayed(useYearPendingDates) || verifyObjectDisplayed(datesComingSoon)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "pendingUseYearDates", Status.PASS,
					"Pending Dates Displayed for this Owner");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "pendingUseYearDates", Status.FAIL,
					"Dates not Displayed for this Owner");
		}

	}

	/*
	 * Method: viewProfileValidations Description:Profile Link Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void viewProfileValidations() {
		clickElementBy(getList(viewAccountLink).get(0));
		waitUntilElementVisibleBy(driver, accountSettingsHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(accountSettingsHeader));

		tcConfig.getTestData().put("OwnerNameFull", getElementText(ownerFullNameSettings));

		tcConfig.updateTestReporter("CUIMyAccountPage", "viewProfileValidations", Status.PASS,
				"Account Settings page navigated and header is present");
		navigateBack();
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));
	}

	/*
	 * Method: viewPointLinkValidations Description:Points Link Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void viewPointLinkValidations() {
		clickElementBy(getList(viewMyPointsLink).get(0));
		waitUntilElementVisibleBy(driver, pointsSummaryPage, 120);
		Assert.assertTrue(verifyObjectDisplayed(pointsSummaryPage));

		tcConfig.updateTestReporter("CUIMyAccountPage", "viewPointLinkValidations", Status.PASS,
				"Points Summary page navigated and header is present");
		navigateBack();
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));

	}

	/*
	 * Method: contractNumbersValidations Description: Contract Numbers
	 * Validation Elements Validations * Date field Date: Apr/2020 Author: Unnat
	 * Jain Changes By: NA
	 */
	public void contractNumbersValidations() {
		if (verifyObjectDisplayed(resultsHeader)) {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "contractNumbersValidations", Status.PASS,
					"Contract Header Displayed as " + getElementText(resultsHeader));
			// Contract Number List
			List<WebElement> contractNoList = getList(contractNumbers);

			for (int totalContracts = 0; totalContracts < contractNoList.size(); totalContracts++) {
				tcConfig.updateTestReporter("CUIOwnerAccountPage", "contractNumbersValidations", Status.PASS,
						"Contract #" + (totalContracts + 1) + " is displayed as: "
								+ getElementText(contractNoList.get(totalContracts)));
			}

		} else {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "contractNumbersValidations", Status.FAIL,
					"Total Contract result not displayed");
		}

	}

	/*
	 * Method: myOwnerShipValidations Description:Ownership Link Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void myOwnershipLinkValidations() {
		clickElementBy((viewOwnerShipLink));
		waitUntilElementVisibleBy(driver, myOwnerShipPage, 120);
		Assert.assertTrue(verifyObjectDisplayed(myOwnerShipPage));

		tcConfig.updateTestReporter("CUIMyAccountPage", "myOwnershipLinkValidations", Status.PASS,
				"My Ownership page navigated and header is present");
		navigateBack();
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));
	}

	/*
	 * Method: newOwnerHeader Description:Validate New Owner Welcome Header
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void newOwnerHeader() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(welcomeToClubHeader));

		tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerHeader", Status.PASS,
				"Welcome To The Club Header displayed for New Owner");

	}

	/*
	 * Method: pendingOwnerSearchForm Description:Validate pending Owner Search
	 * Form Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void pendingOwnerSearchForm() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		if (!testData.get("User_Type").contains("LE") && verifyObjectDisplayed(searchAvailabilityForm)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "pendingOwnerSearchForm", Status.FAIL,
					"Search Availability form Displayed for a Pending Owner");
		} else if (testData.get("User_Type").contains("LE") && verifyObjectDisplayed(searchAvailabilityForm)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "pendingOwnerSearchForm", Status.PASS,
					"Search Availability form Displayed for a LE New Owner");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "pendingOwnerSearchForm", Status.PASS,
					"Search Availability form not Displayed for a Pending Owner");
		}

	}

	/*
	 * Method: ownerWithPointsSearchForm Description:Validate Owner with point
	 * Search Form Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void ownerWithPointsSearchForm() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		if (verifyObjectDisplayed(searchAvailabilityForm)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithPointsSearchForm", Status.PASS,
					"Search Availability form Displayed for a Owner with Points");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithPointsSearchForm", Status.FAIL,
					"Search Availability form not Displayed for a Owner with Points");
		}
	}

	/*
	 * Method: ownerWithReservations Description:Validate Reservation Card
	 * Header Date: Mar/2020 Author: Unnat JainChanges By: NA
	 */

	public void ownerWithReservations() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);

		List<WebElement> reservationList = getList(upcomingReservationCards);

		if (reservationList.size() > 0) {
			// Reservation Number
			int totalRervation = totalUpcomingReservations();
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithReservations", Status.PASS,
					"Owner Reservation Card Displayed for Seasoned owner with total reservation Number : "
							+ totalRervation);
		} else if (verifyObjectDisplayed(noUpcomingReservations)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutReservations", Status.PASS,
					"No Upcoming Reservation for a Seasoned Owner");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithReservations", Status.FAIL,
					"Owner Reservation Card not Displayed for Seasoned owner");
		}
	}

	/*
	 * Method: totalUpcomingReservations Description: Toal reserv numbers Date:
	 * Mar/2020 Author: Unnat JainChanges By: NA
	 */

	public int totalUpcomingReservations() {
		// Reservation Number in Int
		int reservationNo = 0;
		try {
			// Reservation Number in String
			String strReservation = getElementText(reservationPresent).trim();
			strReservation = strReservation.split(" ")[0].trim();
			reservationNo = Integer.parseInt(strReservation);

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "totalUpcomingReservations", Status.FAIL,
					"Reservation Number not present for Seasoned owner");
		}
		return reservationNo;
	}

	/*
	 * Method: ownerWithoutReservationsButPoints Description:Validate New Owner
	 * reservation section with pOint Header Date: Mar/2020 Author: Unnat Jain
	 * Changes By: NA
	 */

	public void ownerWithoutReservationsButPoints() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		List<WebElement> reservationList = getList(upcomingReservationCards);
		if (verifyObjectDisplayed(noUpcomingReservations)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutReservationsButPoints", Status.PASS,
					"No Upcoming Reservation Header displayed for New Owner with points");
		} else if (reservationList.size() > 0) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutReservationsButPoints", Status.FAIL,
					"Upcoming Reservation Card displayed for New Owner with points");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutReservationsButPoints", Status.FAIL,
					"No Upcoming Reservation Header not displayed for New Owner with points");
		}

	}

	/*
	 * Method: ownerWithoutReservationsAndPoints Description:Validate pending
	 * owner reservation section Welcome Header Date: Mar/2020 Author: Unnat
	 * Jain Changes By: NA
	 */

	public void ownerWithoutReservationsAndPoints() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		List<WebElement> reservationList = getList(upcomingReservationCards);
		if (verifyObjectDisplayed(noUpcomingReservations)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutReservationsAndPoints", Status.PASS,
					" Upcoming Reservation section displayed for Pending Owner");
		} else if (reservationList.size() > 0) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutReservationsAndPoints", Status.FAIL,
					"Upcoming Reservation Card displayed for Pending Owner");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutReservationsAndPoints", Status.PASS,
					"Upcoming Reservation Section not displayed for Pending Owner");
		}

	}

	/*
	 * Method: ownerWithPoints Description:Validate Owner With points Header
	 * Date: Mar/2020 Author: Unnat JainChanges By: NA
	 */

	public void ownerWithPoints() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		String strPoints = getElementText(useYearPoints).replace(",", "");
		if (isNumericString(strPoints)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithPoints", Status.PASS,
					"Points Displayed for this Owner: " + strPoints);
		} else if (verifyObjectDisplayed(useYearPendingPoints) || verifyObjectDisplayed(pointsComingSoon)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithPoints", Status.FAIL,
					"Pending Points Displayed for this Owner");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithPoints", Status.FAIL,
					"Points not Displayed for this Owner");
		}
	}

	/*
	 * Method: ownerWithoutPoints Description:Validate Owner Without points
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void ownerWithoutPoints() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		String strPoints = getElementText(useYearPoints).replace(",", "");
		if (verifyObjectDisplayed(useYearPendingPoints) || verifyObjectDisplayed(pointsComingSoon)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutPoints", Status.PASS,
					"No Points displayed for a pendig owner");
		} else if (testData.get("User_Type").contains("LE") && isNumericString(strPoints)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutPoints", Status.PASS,
					"Points Displayed for a LE New Member");
		} else if (!testData.get("User_Type").contains("LE") && isNumericString(strPoints)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutPoints", Status.FAIL,
					"Points displayed for a pending owner");
		} else {

			tcConfig.updateTestReporter("CUIMyAccountPage", "ownerWithoutPoints", Status.FAIL,
					"Pending/ Coming soon points not displayed for a pendig owner");
		}
	}

	/*
	 * Method: validateTextLocationField Description:Validate the text present
	 * in Location field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTextLocationField() {
		waitUntilElementVisibleBy(driver, textLocationField, 120);
		if (verifyObjectDisplayed(textLocationField)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "validateTextLocationField", Status.PASS,
					"The text present in location field is: " + getObject(textLocationField).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "validateTextLocationField", Status.FAIL,
					"Failed while validating text in Location field");
		}
	}

	/*
	 * Method: validateSearchHeader Description:Validate Search Header Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void validateSearchHeader() {
		waitUntilElementVisibleBy(driver, textSearchHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(textSearchHeader));
		tcConfig.updateTestReporter("CUIMyAccountPage", "validateSearchHeader", Status.PASS,
				"Search Header Present with text: " + getObject(textSearchHeader).getText().trim());
	}

	/*
	 * Method: validateTextDateField Description:Validate the text present in
	 * Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void validateTextDateField() {
		waitUntilElementVisibleBy(driver, textDateField, 120);
		if (verifyObjectDisplayed(textDateField)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "validateTextDateField", Status.PASS,
					"The text Present in date field is: " + getObject(textDateField).getText().trim());

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "validateTextDateField", Status.FAIL,
					"Failed while validating text in Date field");
		}
	}

	/*
	 * Method: enterLocation Description:Select Location Date field Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void enterLocation() throws InterruptedException {
		String location = testData.get("CUI_Location").trim();
		waitUntilElementVisibleBy(driver, textLocation, 120);
		Assert.assertTrue(verifyObjectDisplayed(textLocationField));
		// fieldDataEnter(textLocationField, location);
		clickElementBy(textLocationField);
		fieldDataEnter(textLocation, location);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getObject(textLocation).sendKeys(Keys.ARROW_DOWN);
		getObject(textLocation).sendKeys(Keys.ENTER);
		tcConfig.updateTestReporter("CUIMyAccountPage", "enterLocation", Status.PASS,
				"Location entered successfully as: " + location);
	}

	/*
	 * Method: selectCheckinDate Description:Select Check in date from calander
	 * Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCheckinDate() {
		// to store check in date
		String checkindate = testData.get("Checkindate").trim();
		waitUntilElementVisibleBy(driver, textBoxDateSelect, 120);
		Assert.assertTrue(verifyObjectDisplayed(textBoxDateSelect));
		clickElementBy(textBoxDateSelect);

		tcConfig.updateTestReporter("CUIMyAccountPage", "selectCheckinDate", Status.PASS,
				"Successfully clicked on select date text box");

		// sendKeyboardKeys(Keys.TAB);
		// To store day from date
		String day = (checkindate.split(" ")[0]).trim();
		// To store month from date
		String month = (checkindate.split(" ")[1]).trim();
		// To store year from date
		String year = (checkindate.split(" ")[2]).trim();

		String monthyear = month.concat(" ").concat(year);
		selectDate(monthyear, day, checkindate, "Checkin");

	}

	/*
	 * Method: selectCheckoutDate Description:Select Check out date from
	 * calander Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCheckoutDate() {
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		String checkoutdate = testData.get("Checkoutdate").trim();
		String day = (checkoutdate.split(" ")[0]).trim(); // To store day from
															// date
		String month = (checkoutdate.split(" ")[1]).trim();// To store month
															// from date
		String year = (checkoutdate.split(" ")[2]).trim();// To store year from
															// date
		String monthyear = month.concat(" ").concat(year);// To concat month and
															// year in required
															// format
		selectDate(monthyear, day, checkoutdate, "CheckOut");

	}
	/*
	 * Method: clickSearchavailabilitybutton Description:Click Search
	 * Availability Button Date field Date: Mar/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */

	public void clickSearchAvailabilityButton() {
		waitUntilElementVisibleBy(driver, buttonSearchAvailability, 120);
		if (verifyObjectDisplayed(buttonSearchAvailability)) {
			clickElementJSWithWait(buttonSearchAvailability);
			tcConfig.updateTestReporter("CUIMyAccountPage", "clickSearchavailabilitybutton", Status.PASS,
					"Clicked on Search Availability Button");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "clickSearchavailabilitybutton", Status.FAIL,
					"Failed while clicking on Search Availability button");
		}
	}

	/*
	 * Method: buttonSearchavailabilityDisabled Description:Click Search
	 * Availability Button Disabled check Date field Date: Mar/2020 Author:
	 * Abhijeet Roy Changes By: NA
	 */
	public void buttonSearchAvailabilityDisabled() throws Exception {

		if (verifyObjectDisplayed(buttonSearchAvailability)) {
			String searchAvailabilityButtonStatus = getObject(buttonSearchAvailability).getAttribute("disabled");

			if (searchAvailabilityButtonStatus.equalsIgnoreCase("TRUE")) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "buttonSearchavailabilityDisabled", Status.PASS,
						"Search Availability Button present and disabled");

			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "buttonSearchavailabilityDisabled", Status.FAIL,
						"Search Availability button is enabled even Location or date field is blank");
			}
		}

	}

	/*
	 * Method: selectCalendarDone Description:Click Done button present in
	 * calendar Date field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectCalendarDone() {
		waitUntilElementVisibleBy(driver, buttonCalendarDone, 120);
		if (verifyObjectDisplayed(buttonCalendarDone)) {
			clickElementJSWithWait(buttonCalendarDone);
			tcConfig.updateTestReporter("CUIMyAccountPage", "selectCalendarDone", Status.PASS,
					"Successfully clicked on Done Calander Button");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "selectCalendarDone", Status.FAIL,
					"Failed while clicking on Done Calander Button");
		}
	}

	/*
	 * Method: selectFlexDateCheckbox Description:Select Flex Date Checkbox Date
	 * field Date: Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectFlexDateCheckbox() {
		waitUntilElementVisibleBy(driver, selectFlexDateCheckbox, 120);
		Assert.assertTrue(verifyObjectDisplayed(selectFlexDateCheckbox));
		clickElementJSWithWait(selectFlexDateCheckbox);
		tcConfig.updateTestReporter("CUIMyAccountPage", "verifyresortfound", Status.PASS,
				"Successfully clicked on Flex date check box");
	}

	/*
	 * Method: leftRailTextValidation Description:Left Rail Text Validations *
	 * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void leftRailTextValidation() {
		// Check User type selected
		String strMemberType = testData.get("User_Type");
		if (strMemberType.contains("Discover") || strMemberType.contains("ClubGo")) {
			leftRailMembershipText();
		} else {
			leftRailOwnershipText();
		}
	}

	/*
	 * Method: leftRailOwnershipText Description:Left Rail Text Validations *
	 * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void leftRailOwnershipText() {
		if (verifyObjectDisplayed(leftRailOwnershipLink)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "leftRailOwnershipText", Status.PASS,
					"My Ownership Text Displayed for the non Discover and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "leftRailOwnershipText", Status.FAIL,
					"My Ownership Text not Displayed for the non Discover and Club Go members");
		}
	}

	/*
	 * Method: leftRailMembershipText Description:Left Rail Text Validations *
	 * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void leftRailMembershipText() {
		if (verifyObjectDisplayed(leftRailMembershipLink)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "leftRailMembershipText", Status.PASS,
					"My Membership Text Displayed for the  Discovery and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "leftRailMembershipText", Status.FAIL,
					"My Membership Text not Displayed for the Discovery and Club Go members");
		}
	}

	/*
	 * Method: vipStatusValidation Description:VIP status Validations * Date
	 * field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void vipStatusValidation() {
		// Check User type selected
		if (testData.get("UserStatus").contains("VIP")) {
			String strMemberType = testData.get("User_Type");
			if (strMemberType.contains("Discover") || strMemberType.contains("ClubGo")) {
				vipMemberStatus();
			} else {
				vipOwnerStatus();
			}
		} else if (!testData.get("UserStatus").contains("VIP")) {
			nonVipOwner();
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "vipOwnerStatus", Status.FAIL, "Error in data entry ");
		}
	}

	/*
	 * Method: vipOwnerStatus Description:VIP status Validations * Date field
	 * Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void vipOwnerStatus() {
		if (getElementText(vipStatus).toLowerCase().contains("owner")) {

			tcConfig.updateTestReporter("CUIMyAccountPage", "vipOwnerStatus", Status.PASS,
					"VIP Owner Status Displayed for the non Discover and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "vipOwnerStatus", Status.FAIL,
					"VIP Member Status Displayed for the non Discover and Club Go members");
		}
	}

	/*
	 * Method: vipMemberStatus Description:VIP status Text Validations * Date
	 * field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void vipMemberStatus() {
		if (getElementText(vipStatus).toLowerCase().contains("member")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "vipMemberStatus", Status.PASS,
					"VIP Member Text Displayed for the  Discovery and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "vipMemberStatus", Status.FAIL,
					"VIP owner Text Displayed for the Discovery and Club Go members");
		}
	}

	/*
	 * Method: nonVipOwner Description: Non Vip Owner Status Validation * Date
	 * field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void nonVipOwner() {
		if (verifyObjectDisplayed(vipStatus)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "nonVipOwner", Status.FAIL,
					"VIP status displayed even when the owner is non VIP");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "nonVipOwner", Status.PASS,
					"VIP Status not displayed for non owner");
		}
	}

	/*
	 * Method: membershipTextValidation Description:My Ownership header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void membershipTextValidation() {
		// Check User type selected
		String strMemberType = testData.get("User_Type");
		if (strMemberType.contains("Discover") || strMemberType.contains("ClubGo")) {
			myMembershipText();
		} else {
			myOwnershipText();
		}
	}

	/*
	 * Method: myOwnershipText Description:My Ownership header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void myOwnershipText() {
		if (verifyObjectDisplayed(myOwnerShipHeader)
				&& getElementText(viewOwnerShipLink).toLowerCase().contains("owner")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "myOwnershipText", Status.PASS,
					"My Ownership Header and link Displayed for the non Discover and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "myOwnershipText", Status.FAIL,
					"My Ownership Text and link not Displayed for the non Discover and Club Go members");
		}
	}

	/*
	 * Method: myMembershipText Description:My Membership header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void myMembershipText() {
		if (verifyObjectDisplayed(myMemberShipHeader)
				&& getElementText(viewOwnerShipLink).toLowerCase().contains("member")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "myMembershipText", Status.PASS,
					"My Membership Header and link Displayed for the  Discovery and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "myMembershipText", Status.FAIL,
					"My Membership header and link not Displayed for the Discovery and Club Go members");
		}
	}

	/*
	 * Method: educationTextValidation Description:My Education header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void educationTextValidation() {
		// Check User type selected
		String strMemberType = testData.get("User_Type");
		getElementInView(educationHeader);
		elementPresenceVal(needHelpHeader, "Need Help Header Displayed", "CUIMyAccountPage", "educationTextValidation");
		if (strMemberType.contains("Discover") || strMemberType.contains("ClubGo")) {
			educationMembershipText();
		} else {
			educationOwnershipText();
		}

	}

	/*
	 * Method: educationTextValidation Description:My Education header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void newTipsTextValidation() {
		// Check User type selected
		String strMemberType = testData.get("User_Type");
		getElementInView(newUserTipsHeader);
		elementPresenceVal(getStartedText, "Get Started Header Displayed", "CUIMyAccountPage", "newTipsTextValidation");
		elementPresenceVal(tipsDescription, "Tips Description Displayed", "CUIMyAccountPage", "newTipsTextValidation");
		if (strMemberType.contains("Discover") || strMemberType.contains("ClubGo")) {
			newMemberTipText();
		} else {
			newOwnerTipText();
		}

	}

	/*
	 * Method: educationLinksValidations Description:My Education header and
	 * link Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes
	 * By: NA
	 */

	public void educationLinksValidations() {

		if (getList(needHelpLinks).size() == getList(needHelpImages).size()) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "educationLinksValidations", Status.PASS,
					"Education Links And Icons Displayed, Total: " + getList(needHelpLinks).size());
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "educationLinksValidations", Status.FAIL,
					"Education Links And Icons Validationfailed");
		}

	}

	/*
	 * Method: tipsLinksValidations Description:My Tips header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void tipsLinksValidations() {

		if (getList(tipsLink).size() == getList(tipsImage).size()) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "tipsLinksValidations", Status.PASS,
					"Tips Links And Icons Displayed, Total: " + getList(tipsLink).size());
			for (int tipsList = 0; tipsList < getList(tipsLink).size(); tipsList++) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "tipsLinksValidations", Status.PASS,
						"Tip Link #" + tipsList + " is " + getList(tipsLink).get(tipsList).getText());
			}

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "tipsLinksValidations", Status.FAIL,
					"Tips Links And Icons Validation failed");
		}

	}

	/*
	 * Method: educationOwnershipText Description:My Education header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void educationOwnershipText() {
		String educationLinks = String.join(" ", getListString(needHelpLinks));
		if (getElementText(educationHeader).toLowerCase().contains("owner")
				&& !educationLinks.toLowerCase().contains("member")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "educationOwnershipText", Status.PASS,
					"Owner Education Header and link Displayed for the non Discover and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "educationOwnershipText", Status.FAIL,
					"Owner Education Text and link not Displayed for the non Discover and Club Go members");
		}
	}

	/*
	 * Method: educationMembershipText Description:My Education header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void educationMembershipText() {
		String educationLinks = String.join(" ", getListString(needHelpLinks));
		if (getElementText(educationHeader).toLowerCase().contains("member")
				&& !educationLinks.toLowerCase().contains("owner")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "myMembershipText", Status.PASS,
					"Member Education Header and link Displayed for the  Discovery and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "myMembershipText", Status.FAIL,
					"Member Education header and link not Displayed for the Discovery and Club Go members");
		}
	}

	/*
	 * Method: newMemberTipTexts Description:My Education header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newMemberTipText() {
		String educationLinks = String.join(" ", getListString(tipsLink));
		if (getElementText(newUserTipsHeader).toLowerCase().contains("member")
				&& !educationLinks.toLowerCase().contains("owner")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newMemberTipText", Status.PASS,
					"Member Tips Header and link Displayed for the  Discovery and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newMemberTipText", Status.FAIL,
					"Member Tips header and link not Displayed for the Discovery and Club Go members");
		}
	}

	/*
	 * Method: newOwnerTipText Description:My Education header and link
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newOwnerTipText() {
		String educationLinks = String.join(" ", getListString(tipsLink));
		if (getElementText(newUserTipsHeader).toLowerCase().contains("owner")
				&& !educationLinks.toLowerCase().contains("member")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerTipText", Status.PASS,
					"Owner Tips Header and link Displayed for the  non Discovery and Club Go members");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerTipText", Status.FAIL,
					"Owner Tips header and link not Displayed for the non Discovery and Club Go members");
		}
	}

	/*
	 * Method: newOwnerCompletedTips Description:New Owner Completed Tips
	 * Validation Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newOwnerCompletedTips() {
		refreshPage();
		pageCheck();
		getElementInView(tipsSectionBelowVacation);
		if (verifyObjectDisplayed(tipsSectionBelowVacation)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.PASS,
					"Tips Section present below Vacation section after completing ");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.FAIL,
					"Tips Section not present below Vacation section after completing ");
		}
	}

	/*
	 * Method: upcomingReservationSection Description:Reservation Section
	 * Elements Validations * Date field Date: Apr/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void upcomingReservationSection() {
		int totalCards = getList(upcomingReservationCards).size();
		elementListPresenceVal(reservationName, totalCards, "Card Name ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(cardImage, totalCards, "Card Images ", "CUIMyAccountPage", "upcomingReservationSection");
		elementListPresenceVal(cardAddress, totalCards, "Card Address", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(cardMapLink, totalCards, "Card Map Link ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(checkInDate, totalCards, "Check In date ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(checkOutDate, totalCards, "Check out Date", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(unitType, totalCards, "Unit Type ", "CUIMyAccountPage", "upcomingReservationSection");
		elementListPresenceVal(confirmationNumber, totalCards, "Confirmation Number ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementListPresenceVal(reservationDetailsLink, totalCards, "Reservation Details Link ", "CUIMyAccountPage",
				"upcomingReservationSection");
		elementPresenceVal(viewAllLink, "View All Link ", "CUIMyAccountPage", "upcomingReservationSection");
	}

	/*
	 * Method: mapLinkValidations Description:Map Section Elements Validations *
	 * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void mapLinkValidations() {
		getElementInView(getList(cardMapLink).get(0));
		clickElementBy(getList(cardMapLink).get(0));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(newWindow);
		waitUntilElementVisibleBy(driver, mapPageHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(mapPageHeader));

		tcConfig.updateTestReporter("CUIMyAccountPage", "mapLinkValidations", Status.PASS, "Navigated to Map page");
		navigateToMainTab(newWindow);
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));
	}

	/*
	 * Method: reservationLinkValidations Description:Reservation Link Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationLinkValidations() {
		getElementInView(getList(reservationDetailsLink).get(0));
		clickElementBy(getList(reservationDetailsLink).get(0));
		waitUntilElementVisibleBy(driver, reservationSummaryPage, 120);
		Assert.assertTrue(verifyObjectDisplayed(reservationSummaryPage));

		tcConfig.updateTestReporter("CUIMyAccountPage", "reservationLinkValidations", Status.PASS,
				"Navigated to Details page");
		navigateBack();
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));
	}

	/*
	 * Method: modifyLinkValidations Description:Modify Link Section Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void modifyLinkValidations() {

		try {
			clickElementBy(getList(modifyReservation).get(0));
			waitUntilElementVisibleBy(driver, modifyReservationHeader, 120);
			Assert.assertTrue(verifyObjectDisplayed(modifyReservationHeader));
			tcConfig.updateTestReporter("CUIMyAccountPage", "modifyLinkValidations", Status.PASS,
					"Navigated to Modify reservation page");
			navigateBack();
			waitUntilElementVisibleBy(driver, ownerFullName, 120);
			Assert.assertTrue(verifyObjectDisplayed(ownerFullName));

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "modifyLinkValidations", Status.PASS,
					"Modify Link Not Present in this card");
		}
	}

	/*
	 * Method: cancelLinkValidations Description:Cancel Link Elements
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelLinkValidations() {

		try {
			getElementInView(cancelReservation);
			clickElementBy(getList(cancelReservation).get(0));
			waitUntilElementVisibleBy(driver, cancleReservationPopUp, 120);
			Assert.assertTrue(verifyObjectDisplayed(cancleReservationPopUp));
			tcConfig.updateTestReporter("CUIMyAccountPage", "cancelLinkValidations", Status.PASS,
					"Navigated to Cancel Pop up");
			refreshPage();
			waitUntilElementVisibleBy(driver, ownerFullName, 120);
			Assert.assertTrue(verifyObjectDisplayed(ownerFullName));

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "cancelLinkValidations", Status.PASS,
					"Cancel Link Not Present in this card");
		}
	}

	/*
	 * Method: reservatioSortingCheck Description:Reservation Sorting
	 * Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationSortingAndPaginationCheck() throws Exception {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		List<WebElement> paginationList = getList(paginationLinks);
		List<WebElement> cardsList = getList(activeReservationCards);
		List<String> stringList = new ArrayList<String>();
		String strList;
		getElementInView(paginationList.get(0));
		for (int pageNumber = 0; pageNumber < paginationList.size(); pageNumber++) {
			clickElementBy(paginationList.get(pageNumber));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			strList = getElementText(activeCheckInDate).substring(3).trim();
			strList = strList.substring(0, 10);
			stringList.add(strList);
			if (cardsList.get(pageNumber).getAttribute("class").contains("active")) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "reservationSortingAndPaginationCheck", Status.PASS,
						"Navigated to " + (pageNumber + 1) + " card");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "reservationSortingAndPaginationCheck", Status.FAIL,
						"Pagination Validation error");

			}
		}
		ascendingOrderDateCheck(stringList, "MM/dd/yyyy", "Reservation Cards");
	}

	/*
	 * Method: reservationPaginationCheck Description:Reservation Pagination
	 * Elements Validations * Date field Date: Apr/2020 Author: Unnat Jain
	 * Changes By: NA
	 */
	public void reservationPaginationCheck() {
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		List<WebElement> paginationList = getList(paginationLinks);
		List<WebElement> cardsList = getList(activeReservationCards);
		getElementInView(paginationList.get(0));
		for (int pageNumber = 0; pageNumber < cardsList.size(); pageNumber++) {
			clickElementBy(paginationList.get(pageNumber));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (cardsList.get(pageNumber).getAttribute("class").contains("active")) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "reservationPaginationCheck", Status.PASS,
						"Navigated to " + (pageNumber + 1) + " card");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "reservationPaginationCheck", Status.FAIL,
						"Pagination Validation error");
				break;
			}

		}

	}

	public void selectDate(String monthAndYear, String dayOfMonth, String selectDate, String whichDate) {
		String monthName = null;
		int monthPresent = 0;
		do {
			for (int monthIncrease = 0; monthIncrease <= getList(listCalendarMonthName).size() - 1; monthIncrease++) {
				// To get month name from screen 1 by 1
				monthName = getList(listCalendarMonthName).get(monthIncrease).getText().trim();
				if (monthName.equals(monthAndYear)) {
					getObject(By.xpath("//div[contains(@class,'for-medium')]//strong[text() = '" + monthAndYear
							+ "']/../../..//div[contains(@class,'CalendarMonth') and @data-visible = 'true']//td[text() = '"
							+ dayOfMonth + "']")).click();

					monthPresent = monthPresent + 1;
					break;
				}

			}
			if (monthPresent == 0) {
				clickElementBy(nextMonthChange);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} while (!monthName.equals(monthAndYear));
		if (monthPresent == 1) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "selectCheckoutDate", Status.PASS,
					"Selected" + whichDate + "date is: " + selectDate);
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "selectCheckoutDate", Status.FAIL,
					whichDate + "date selection failed");
		}

	}

	/*
	 * Method: flexibleDateSelectOption Description:Select/not Flex Date
	 * Checkbox Date field Date: Apr/2020 Author: Unnat jain Changes By: NA
	 */
	public void flexibleDateSelectOption() {
		if (testData.get("SelectFlexDate").contains("Yes")) {
			selectFlexDateCheckbox();
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "flexibleDateSelectOption", Status.PASS,
					"Flexible Date Option not selected");
		}
	}

	/*
	 * Method: dashboardAccountInfoSection Description:Dashboard page
	 * validations Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void dashboardAccountInfoSection() {
		elementPresenceVal(ownerFullName, "User Name ", "CUIMyAccountPage", "dashboardAccountInfoSection");
		elementPresenceVal(memberNumber, getElementText(memberNumber) + " ", "CUIMyAccountPage",
				"dashboardAccountInfoSection");
		elementPresenceVal(imageSpace, "Image Space ", "CUIMyAccountPage", "dashboardAccountInfoSection");
	}
	/*
	 * Method: dashboardAccountInfoSection Description:Dashboard page
	 * validations Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */

	public void dashboardMyOwnerandPointsHeader() {
		elementPresenceVal(pointsSummaryHeader, "Points Summary Header ", "CUIMyAccountPage",
				"dashboardMyOwnerandPointsHeader");

		if (verifyObjectDisplayed(myOwnerShipHeader)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "dashboardMyOwnerandPointsHeader", Status.PASS,
					"My Ownership Header is displayed");
		} else if (verifyObjectDisplayed(myMemberShipHeader)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "dashboardMyOwnerandPointsHeader", Status.PASS,
					"My Membership Header is displayed");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "dashboardMyOwnerandPointsHeader", Status.FAIL,
					"My Ownership Header is not displayed");
		}
	}

	/*
	 * Method: memberNumberValidation Description:Dashboard page validations
	 * Date: May/2020 Author: Unnat Jain Changes By: NA
	 */

	public void memberNumberValidation() {
		if (getElementText(memberNumber).toLowerCase().contains(testData.get("MemberNumber"))) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "memberNumberValidation", Status.PASS,
					"Member Number Validates and is: " + getElementText(memberNumber));
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "memberNumberValidation", Status.FAIL,
					"Member Validation Failed");
		}
	}

	/*
	 * Method: validateSearchHeader Description:Validate Search Header Date:
	 * Mar/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void validateSearchHeaderSmoke() {
		waitUntilElementVisibleBy(driver, textSearchHeader, 120);
		if (verifyObjectDisplayed(textSearchHeader)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "validateSearchHeader", Status.PASS,
					"Search Header Present with text: " + getObject(textSearchHeader).getText().trim());
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "validateSearchHeader", Status.PASS,
					"Search header not present for pending owner");
		}
	}

	/*
	 * Method: travelDealsHeaderValidation Description:Travel Deals Header Date:
	 * May/2020 Author: Unnat Jain Changes By: NA
	 */

	public void travelDealsHeaderValidation() {

		if (verifyObjectDisplayed(travelDealsHeader)) {
			getElementInView(viewAllDealsLink);
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsHeaderValidation", Status.PASS,
					"Travel Deals Header Present");
			travelDealsViewLink();
		} else if (verifyObjectDisplayed(popularResortHeader)) {
			getElementInView(popularResortHeader);
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsHeaderValidation", Status.PASS,
					"Popular Resort Header present ");
			poupularResortViewLink();
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsHeaderValidation", Status.FAIL,
					"Travel Deals Header not Present");
		}
	}
	/*
	 * Method: travelDealsViewLink Description:Travel Deals Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */

	public void travelDealsViewLink() {

		if (verifyObjectDisplayed(viewAllDealsLink)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsViewLink", Status.PASS,
					"Travel Deals Link Present");
			clickElementBy(viewAllDealsLink);
			waitUntilElementVisibleBy(driver, headerLogoUpdated, 120);
			Assert.assertTrue(verifyObjectDisplayed(headerLogoUpdated));
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsViewLink", Status.PASS,
					"Navigated To Respective page");
			navigateBack();
			waitUntilElementVisibleBy(driver, ownerFullName, 120);
			Assert.assertTrue(verifyObjectDisplayed(ownerFullName));

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsViewLink", Status.FAIL,
					"Travel Deals View All Link not Present");
		}
	}

	/*
	 * Method: poupularResortViewLink Description:Travel Deals Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void poupularResortViewLink() {
		if (verifyObjectDisplayed(viewAllResortsLink)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "poupularResortViewLink", Status.PASS,
					"Popular Resort Link present ");
			clickElementBy(viewAllResortsLink);
			waitUntilElementVisibleBy(driver, headerLogoUpdated, 120);
			Assert.assertTrue(verifyObjectDisplayed(headerLogoUpdated));

			tcConfig.updateTestReporter("CUIMyAccountPage", "poupularResortViewLink", Status.PASS,
					"Navigated To Respective page");
			navigateBack();
			waitUntilElementVisibleBy(driver, ownerFullName, 120);
			Assert.assertTrue(verifyObjectDisplayed(ownerFullName));

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "poupularResortViewLink", Status.FAIL,
					"Popular Resorts View All Link not Present");
		}
	}

	/*
	 * Method: travelDealsImageValidations Description:Travel Deals Section
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void travelDealsCardsValidations() {
		int totalTravelDeals = getList(totalDeals).size();
		travelDealsImageValidations(totalTravelDeals);
		travelDealsNameValidations(totalTravelDeals);
		travelDealsDescriptionValidations(totalTravelDeals);
		travelDealsDetailsCTA(totalTravelDeals);
		checkLinkNavigationList(travelBookNowCta, homeBreadCrumb, ownerFullName);

	}

	/*
	 * Method: travelDealsImageValidations Description:Travel Deals Section
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void travelDealsImageValidations(int totalTravelDeals) {
		List<WebElement> travelDealsImg = driver.findElements(travelResortsImage);
		if (travelDealsImg.size() == totalTravelDeals) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsImageValidations", Status.PASS,
					"Images Present in all the travel Deals");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsImageValidations", Status.FAIL,
					"Images not Present in all the travel Deals");
		}
	}

	/*
	 * Method: travelDealsNameValidations Description:Travel Deals Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void travelDealsNameValidations(int totalTravelDeals) {
		List<WebElement> travelDealsName = driver.findElements(travelResortsName);
		if (travelDealsName.size() == totalTravelDeals) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsNameValidations", Status.PASS,
					"Name Present in all the travel Deals");
			for (int i = 0; i < travelDealsName.size(); i++) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsNameValidations", Status.PASS,
						(i + 1) + " Travel Deals Articles name is: " + travelDealsName.get(i).getText());

			}

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsNameValidations", Status.FAIL,
					"Name not Present in all the travel Deals");
		}
	}

	/*
	 * Method: travelDealsDescriptionValidations Description:Travel Deals
	 * Section Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void travelDealsDescriptionValidations(int totalTravelDeals) {
		List<WebElement> travelDealsDesc = driver.findElements(travelResortsDetails);
		if (travelDealsDesc.size() == totalTravelDeals) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsImageValidations", Status.PASS,
					"Description Present in Each Travel Deals without discount");
		} else if (travelDealsDesc.size() >= totalTravelDeals) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsImageValidations", Status.PASS,
					"Description Present in Each Travel Deals with discount");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsImageValidations", Status.FAIL,
					"Description not Present in all the travel Deals");
		}
	}

	/*
	 * Method: travelDealsDetailsCTA Description:Travel Deals Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void travelDealsDetailsCTA(int totalTravelDeals) {
		List<WebElement> travelDealsLinks = driver.findElements(travelBookNowCta);
		if (travelDealsLinks.size() == totalTravelDeals) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsImageValidations", Status.PASS,
					"Links Present in all the travel Deals");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "travelDealsImageValidations", Status.FAIL,
					"Link not Present in all the travel Deals");
		}
	}

	/*
	 * Method: wishListHeaderValidations Description:WishList Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListHeaderValidations() {
		waitUntilElementVisibleBy(driver, myWishListHeader, 120);
		getElementInView(myWishListHeader);
		if (verifyObjectDisplayed(myWishListHeader)) {
			getElementInView(myWishListHeader);
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "wishListSectionVal", Status.PASS,
					"My Wish List Header present");
			viewAllBucketList();
			exploreAllResortValdation();
		} else {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "wishListSectionVal", Status.FAIL,
					"My Wish List Header not present");
		}
	}

	/*
	 * Method: viewAllBucketList Description: Wish List Section Date: Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void viewAllBucketList() {
		if (verifyObjectDisplayed(viewAllWishlistLink)) {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "viewAllBucketList", Status.PASS,
					"View All WishList Link A present");
			clickElementBy(viewAllWishlistLink);
			waitUntilElementVisibleBy(driver, wishListPageHeader, 120);
			Assert.assertTrue(verifyObjectDisplayed(wishListPageHeader));
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "viewAllBucketList", Status.PASS,
					"Navigated To Associated Page");
			navigateBack();
			waitUntilElementVisibleBy(driver, ownerFullName, 120);
			Assert.assertTrue(verifyObjectDisplayed(ownerFullName));

		} else {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "viewAllBucketList", Status.FAIL,
					"View All WishList Link  not present");
		}
	}

	/*
	 * Method: exploreAllResortVal Description: Wish List Section Date: Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void exploreAllResortValdation() {
		if (verifyObjectDisplayed(exploreAllResorts)) {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "exploreAllResortValdation", Status.PASS,
					"Explore Resort CTA present");
			clickElementBy(exploreAllResorts);
			waitUntilElementVisibleBy(driver, locationField, 120);
			Assert.assertTrue(verifyObjectDisplayed(locationField));
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "exploreAllResortValdation", Status.PASS,
					"Navigated To Associated Page");
			navigateBack();
			waitUntilElementVisibleBy(driver, ownerFullName, 120);
			Assert.assertTrue(verifyObjectDisplayed(ownerFullName));

		} else {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "exploreAllResortValdation", Status.FAIL,
					"Explore Resort CTA not present");
		}
	}

	/*
	 * Method: wishListSectionValidations Description:No Wish List Section Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListSectionValidations() {
		if (verifyObjectDisplayed(emptyWishListHeader)) {
			noWishListValidations();
		} else if (verifyObjectDisplayed(wishListItems)) {
			wishListValidations();
			checkLinkNavigationList(wishListResorts, homeBreadCrumb, ownerFullName);
		} else {
			tcConfig.updateTestReporter("CUIOwnerAccountPage", "wishListSectionVal", Status.FAIL,
					"My Wish List Validation failed");
		}
	}

	/*
	 * Method: noWishListValidations Description:No Wish List Section Date:
	 * Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void noWishListValidations() {
		elementPresenceVal(emptyWishListHeader, "No Wishlist Header ", "CUIMyAccountPage", "noWishListValidations");
		elementPresenceVal(emptyWishListIcon, "No Wishlist Icon ", "CUIMyAccountPage", "noWishListValidations");
		elementPresenceVal(emptyWishListDescription, "No Wishlist Description ", "CUIMyAccountPage",
				"noWishListValidations");
	}

	/*
	 * Method: wishListValidations Description:Wish List Section Date: Apr/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void wishListValidations() {
		int totalWishListedItems = getList(totalWishList).size();
		wishListImageValidations(totalWishListedItems);
		wishListNameValidations(totalWishListedItems);
		wishListLocationValidations(totalWishListedItems);
		wishListTagsValidations(totalWishListedItems);
		wishListHeart(totalWishListedItems);
	}

	/*
	 * Method: wishListImageValidations Description:Wish List Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListImageValidations(int totalwishList) {
		List<WebElement> wishListImg = driver.findElements(wishListImages);
		if (wishListImg.size() == totalwishList) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.PASS,
					"Images Present in all the Wish List");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.FAIL,
					"Images not Present in all the Wish List");
		}
	}

	/*
	 * Method: wishListNameValidations Description:Wish List Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListNameValidations(int totalwishList) {
		List<WebElement> wishListName = driver.findElements(wishListResorts);
		if (wishListName.size() == totalwishList) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListNameValidations", Status.PASS,
					"Name Present in all the Wish List");
			for (int i = 0; i < wishListName.size(); i++) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "wishListNameValidations", Status.PASS,
						(i + 1) + " Wish List Articles name is: " + wishListName.get(i).getText());

			}

		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListNameValidations", Status.FAIL,
					"Name not Present in all the Wish List");
		}
	}

	/*
	 * Method: wishListDescriptionValidations Description:Wish List Section
	 * Date: Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListLocationValidations(int totalwishList) {
		List<WebElement> wishListDesc = driver.findElements(wishListLocation);
		if (wishListDesc.size() == totalwishList) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.PASS,
					"Loaction Present in Each Wish List");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.FAIL,
					"Location not Present in all the Wish List");
		}
	}

	/*
	 * Method: wishListDetailsCTA Description:Wish List Section Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void wishListHeart(int totalwishList) {
		List<WebElement> wishListLinks = driver.findElements(wishListHearts);
		if (wishListLinks.size() == totalwishList) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.PASS,
					"Hearts Present in all the Wish List");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.FAIL,
					"Heart not Present in all the Wish List");
		}
	}

	/*
	 * Method: wishListTagsValidations Description:Wish List Section Date:
	 * Mar/2020 Author: Unnat Jain Changes By: NA
	 */
	public void wishListTagsValidations(int totalwishList) {
		List<WebElement> wishListTag = driver.findElements(wishListTags);
		if (wishListTag.size() == totalwishList) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.PASS,
					"Tags Present in Each Wish List");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "wishListImageValidations", Status.FAIL,
					"Tags not Present in all the Wish List");
		}
	}

	/*
	 * Method: newOwnerGetStartedTips Description:New Owner Get Started Tips
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newOwnerGetStartedTips() {

		if (verifyObjectDisplayed(tipsSectionAboveVacation)) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.PASS,
					"Tips Section present above Vacation section before completing ");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.FAIL,
					"Tips Section not present above Vacation section before completing ");
		}
	}

	/*
	 * Method: newTipsCompleteTextValidation Description:My Education header and
	 * link Validations * Date field Date: Apr/2020 Author: Unnat Jain Changes
	 * By: NA
	 */

	public void newTipsCompleteTextValidation() {

		elementPresenceVal(newUserTipsHeader, "Tips Header", "CUIMyAccountPage", "newTipsCompleteTextValidation");
		elementPresenceVal(tipsCompleteDescription, "Tips Complete Description Displayed", "CUIMyAccountPage",
				"newTipsCompleteTextValidation");

	}

	/*
	 * Method: tipsModalValidations Description:New Owner tips open Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void tipsModalValidations() {
		for (int tipCount = 0; tipCount < getList(tipsLink).size(); tipCount++) {
			String titleName = getList(tipsLink).get(tipCount).getText();
			clickElementBy(getList(tipsLink).get(tipCount));
			waitUntilElementVisibleBy(driver, newOwnerModalTitle, 120);
			Assert.assertTrue(verifyObjectDisplayed(newOwnerModalTitle));
			tcConfig.updateTestReporter("CUIMyAccountPage", "newOwnerGetStartedTips", Status.PASS,
					"Modal opened for " + titleName);
			tipsModalContent(titleName);
			previousCtaValidation(tipCount);
			nextCtaValidation(tipCount);
			previousCtaFunctionality(tipCount);
			nextCtaFunctionality(tipCount);
			clickElementBy(modalCloseCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	/*
	 * Method: tipsModalContent Description:Tips Modal Content Date: June/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void tipsModalContent(String titleName) {
		elementPresenceVal(modalImages, "Modal Image ", "CUIMyAccountPage", titleName);
		elementPresenceVal(modalTitle, "Modal Header ", "CUIMyAccountPage", titleName);
		elementPresenceVal(modalText, "Modal Text ", "CUIMyAccountPage", titleName);
		elementPresenceVal(modalButton, "Modal CTA ", "CUIMyAccountPage", titleName);
		elementPresenceVal(modalCloseCTA, "Modal Close CTA ", "CUIMyAccountPage", titleName);
	}

	/*
	 * Method: tipCheckMarkedValidation Description:Tips Modal Complete Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void tipCheckMarkedValidation() {
		if (getList(newOwnerCheckMark).size() == 6) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "tipCompletedValidation", Status.PASS,
					"All the tip are now completed and checked");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "tipCompletedValidation", Status.FAIL,
					"All the tip are completed but not checked");
		}

	}

	/*
	 * Method: allTipCompleteValidation Description:Tips Modal Complete Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void allTipCompleteValidation() {
		if (getElementText(completedText).contains("Completed 6 of 6")) {
			tcConfig.updateTestReporter("CUIMyAccountPage", "allTipCompleteValidation", Status.PASS,
					"All tip completed text displayed");
		} else {
			tcConfig.updateTestReporter("CUIMyAccountPage", "allTipCompleteValidation", Status.FAIL,
					"All tip completed text not displayed");
		}

	}

	/*
	 * Method: previousNextCtaValidation Description:Tips Modal Complete Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */

	public void previousCtaValidation(int tipCount) {
		if (tipCount != 0) {
			elementPresenceVal(modalPreviousCTA, "Previous CTA ", "CUIMyAccountPage", "previousCtaValidation");

		} else {
			if (verifyObjectDisplayed(modalPreviousCTA)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "previousCtaValidation", Status.FAIL,
						"Previous CTA present in first modal");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "previousCtaValidation", Status.PASS,
						"Previous CTA not present in first modal");
			}
		}

	}

	/*
	 * Method: nextCtaValidation Description:Tips Modal Complete Date: June/2020
	 * Author: Unnat Jain Changes By: NA
	 */

	public void nextCtaValidation(int tipCount) {
		if (tipCount != 6) {
			elementPresenceVal(modalNextCTA, "Next CTA ", "CUIMyAccountPage", "nextCtaValidation");

		} else {
			if (verifyObjectDisplayed(modalNextCTA)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "nextCtaValidation", Status.FAIL,
						"Next CTA present in Last modal");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "nextCtaValidation", Status.PASS,
						"Next CTA not present in Last modal");
			}
		}

	}

	/*
	 * Method: nextCtaFunctionality Description:Tips Modal Complete Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */

	public void nextCtaFunctionality(int tipCount) {
		if (tipCount == 0) {
			clickElementBy(modalNextCTA);
			if (verifyObjectDisplayed(modalPreviousCTA)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "nextCtaFunctionality", Status.PASS,
						"Next CTA Functionality Working as Expected");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "nextCtaFunctionality", Status.FAIL,
						"Next CTA Functionality not Working as Expected");
			}

		}

	}

	/*
	 * Method: previousCtaFunctionality Description:Tips Modal Complete Date:
	 * June/2020 Author: Unnat Jain Changes By: NA
	 */

	public void previousCtaFunctionality(int tipCount) {
		if (tipCount == 5) {
			clickElementBy(modalNextCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(modalPreviousCTA);
			if (verifyObjectDisplayed(modalNextCTA)) {
				tcConfig.updateTestReporter("CUIMyAccountPage", "previousCtaFunctionality", Status.PASS,
						"Previous CTA Functionality Working as Expected");
			} else {
				tcConfig.updateTestReporter("CUIMyAccountPage", "previousCtaFunctionality", Status.FAIL,
						"Previous CTA Functionality not Working as Expected");
			}

		}
	}

	/*
	 * Method: navigateToDashBoardPage Description:navigate To DashBoard Page
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void navigateToDashBoardPage() {
		navigateToURL(tcConfig.getConfig().get("DashBoardPageURL"));
		waitUntilElementVisibleBy(driver, ownerFullName, 60);
	}
}
