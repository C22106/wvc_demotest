package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NextGenTravelInspiration_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenTravelInspiration_Web.class);

	public NextGenTravelInspiration_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By travelInspirationTitle = By.xpath("//h2[contains(.,'Travel Inspiration')]");
	protected By seeAllTravelInspi = By.xpath("//a[contains(.,'travel inspiration')]");
	protected By travelInspirationHdr = By.xpath("//div[@class='wyn-headline ']//h2[contains(.,'Travel Inspiration')]");
	protected By travelInspirationImg = By.xpath("//div[@class='wyn-hero__image']//img");
	protected By travelInspiBrdCrm = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Travel')]");
	protected By travelInspiContenthdr = By.xpath("//div[@class='cards']//h2[contains(.,'Travel')]");
	protected By travelInspiCards = By.xpath("//a[@class='wyn-card ']");
	protected By travelInspCardsImg = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-card__image']");
	protected By travleInspCardsTitle = By.xpath("//a[@class='wyn-card ']//div[@class='wyn-title']");
	protected By travelInspiCardsDesc = By.xpath("//a[@class='wyn-card ']//div/p");
	protected By travelInspiReadMoreCTA = By.xpath("//div[@class='wyn-button-cta' and contains(.,'Read More')]");
	protected By destinationMagNav = By.xpath("//div[@class='wyn-card__content']//p[contains(.,'Destinations Mag')]");
	protected By cardHeadline = By.xpath("//div[@class='wyn-headline ']//h1");
	protected By travelBrdCrm = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel')]");

	// hidden Treasures validations
	protected By hiddenTreasuresCard = By
			.xpath("//h3[contains(.,'Must-See Hidden')]/ancestor::a//div[@class='wyn-button-cta']");
	protected By hiddenTreasureImg = By.xpath("//div[@class='wyn-hero__image']//img");
	protected By hiddenContentHeader = By.xpath("//div[@class='text']//h3");
	protected By hiddenTreasureHdr = By.xpath("//h1[contains(.,'Hidden Treasure')]");
	protected By hiddenTreasureTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By hiddenTreasureFacebook = By.xpath(
			"//h1[contains(.,'Hidden Treasure')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By hiddenTreasureTwitter = By.xpath(
			"//h1[contains(.,'Hidden Treasure')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By hiddenTreasureNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Hidden Treasure')]");
	protected By hiddenTreasureContent = By.xpath("//div[@class='text']");
	// protected By saveBigOwnerDealsLink =
	// By.xpath("//div[@class='content']//a[contains(.,'Deals')]");

	/*
	 * Method: travelInspiNav Description: Travel Inspiration Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */
	public void travelInspiNav() {

		// waitUntilElementVisibleBy(driver, travelInspirationTitle, 120);

		if (verifyObjectDisplayed(travelInspirationTitle)) {
			getElementInView(travelInspirationTitle);

			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
					"Travel Inspirations section with title present on the homepage");

			if (verifyObjectDisplayed(seeAllTravelInspi)) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
						"See all Travel Inspirations link present on homepage");

				clickElementBy(seeAllTravelInspi);

			} else {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.FAIL,
						"See all Travel Inspirations link not present on homepage");
			}

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
					"Travel Inspirations section not present on the homepage navigating via link");

			String urlCheck = testData.get("URL");

			urlCheck = urlCheck.concat("/resorts/travel-inspiration");

			driver.navigate().to(urlCheck);
		}

		waitUntilElementVisibleBy(driver, travelInspirationHdr, 120);
		if (verifyObjectDisplayed(travelInspirationHdr)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
					"Navigated to All Travel Inspirations page, Header present as: "
							+ getElementText(travelInspirationHdr));

			if (verifyObjectDisplayed(travelInspiBrdCrm)) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
						"BreadCrumb Present");

				// waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.FAIL,
						"Bread Crumb Not present on the page.");
			}

			if (verifyObjectDisplayed(travelInspirationImg)) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
						"Hero Image present on the page");

				// waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.FAIL,
						"Hero Image not present on the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.FAIL,
					"All Travel Inspirations page navigation failed");
		}

		// driver.navigate().back();

	}

	/*
	 * Method: travelInspiValidations Description: Travel Inspiration Validations:
	 * Date Sep/2019 Author: Unnat Jain Changes By
	 */
	public void travelInspiValidations() {

		waitUntilElementVisibleBy(driver, travelInspiContenthdr, 120);
		getElementInView(travelInspiContenthdr);
		if (verifyObjectDisplayed(travelInspiContenthdr)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.PASS,
					"Travel Inspirations content header present on the page");

			try {
				List<WebElement> travelInspCardsList = driver.findElements(travelInspiCards);
				List<WebElement> travelInspImgList = driver.findElements(travelInspCardsImg);
				List<WebElement> travelInsptitleList = driver.findElements(travleInspCardsTitle);
				List<WebElement> travelInspDescList = driver.findElements(travelInspiCardsDesc);
				List<WebElement> travelInspReadMoreList = driver.findElements(travelInspiReadMoreCTA);

				if (verifyObjectDisplayed(travelInspiCards)) {
					tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.PASS,
							"Travel inspiration Cards Present, Total: " + travelInspCardsList.size());

					if (travelInspCardsList.size() == travelInspImgList.size()) {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.PASS,
								"Travel Inspiration Images present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.FAIL,
								"Travel Inspiration Images not present in all cards");
					}

					if (travelInspCardsList.size() == travelInsptitleList.size()) {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.PASS,
								"Travel Inspiration Title present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.FAIL,
								"Travel Inspiration Title not present in all cards");
					}

					if (travelInspCardsList.size() == travelInspDescList.size()) {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.PASS,
								"Travel Inspiration Desc present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.FAIL,
								"Travel Inspiration Desc not present in all cards");
					}

					if (travelInspCardsList.size() == travelInspReadMoreList.size()) {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.PASS,
								"Travel Inspiration Read More CTA present in all cards");
						int tempSize = travelInspReadMoreList.size();
						for (int i = 0; i < tempSize; i++) {
							List<WebElement> readMoreTempList = driver.findElements(travelInspiReadMoreCTA);
							List<WebElement> titleTempList = driver.findElements(travleInspCardsTitle);
							getElementInView(readMoreTempList.get(i));

							tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations",
									Status.PASS,
									"Clicking on first Card with Title: " + titleTempList.get(i).getText());

							clickElementWb(readMoreTempList.get(i));

							waitUntilElementVisibleBy(driver, destinationMagNav, 120);

							if (verifyObjectDisplayed(travelBrdCrm)) {
								tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations",
										Status.PASS,
										"Navigated To " + (i + 1) + " page with title" + getElementText(cardHeadline));
							} else {
								tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations",
										Status.FAIL, "Failed to navigate to related page");
							}

							driver.navigate().back();
							waitUntilElementVisibleBy(driver, travelInspirationHdr, 120);

						}

					} else {
						tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.FAIL,
								"Travel Inspiration Read More not present in all cards");
					}

				} else {
					tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.FAIL,
							"Cards not present on the page");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.FAIL,
						"Error in Travel Inspirations Cards validation");
			}

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiValidations", Status.FAIL,
					"Travel Inspirations content header not present on the page");
		}

	}
	/*
	 * Method: hiddenTreasuresnav Description: Hidden Travels Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void hiddenTreasuresnav() {
		if (verifyObjectDisplayed(travelInspirationTitle)) {
			getElementInView(travelInspirationTitle);

			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
					"Travel Inspirations section with title present on the homepage");

			if (verifyObjectDisplayed(seeAllTravelInspi)) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
						"See all Travel Inspirations link present on homepage");

				clickElementBy(seeAllTravelInspi);

			} else {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.FAIL,
						"See all Travel Inspirations link not present on homepage");

			}

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "travelInspiNav", Status.PASS,
					"Travel Inspirations section not present on the homepage navigating via link");

			String urlCheck = testData.get("URL");

			urlCheck = urlCheck.concat("/resorts/travel-inspiration");

			driver.navigate().to(urlCheck);
		}

		waitUntilElementVisibleBy(driver, travelInspirationHdr, 120);
		if (verifyObjectDisplayed(travelInspirationHdr)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasuresnav", Status.PASS,
					"Navigated to All Travel Inspirations page, Header present as: "
							+ getElementText(travelInspirationHdr));

			if (browserName.equalsIgnoreCase("Firefox")) {
				scrollDownForElementJSBy(hiddenTreasuresCard);
				scrollUpByPixel(300);
			} else {
				getElementInView(hiddenTreasuresCard);
			}
			waitUntilElementVisibleBy(driver, hiddenTreasuresCard, 120);

			if (verifyObjectDisplayed(hiddenTreasuresCard)) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasuresnav", Status.PASS,
						"Hidden Treasure Cards present in the page");
				clickElementJSWithWait(hiddenTreasuresCard);

				waitUntilElementVisibleBy(driver, hiddenTreasureHdr, 120);

				if (verifyObjectDisplayed(hiddenTreasureHdr)) {
					tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasuresnav", Status.PASS,
							"Hidden Treasure page Navigated");

				} else {
					tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasuresnav", Status.FAIL,
							"Hidden Treasure page Navigation failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasuresnav", Status.FAIL,
						"Hidden Treasure Cards not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasuresnav", Status.FAIL,
					"All Travel Inspirations page navigation failed");
		}

		// driver.navigate().back();

	}

	/*
	 * Method: hiddenTreasuresVal Description: Hidden Travels Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void hiddenTreasuresVal() {

		waitUntilElementVisibleBy(driver, hiddenTreasureHdr, 120);

		if (verifyObjectDisplayed(hiddenTreasureHdr)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
					getElementText(hiddenTreasureHdr) + " header present");
		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
					"Hidden treasure Header not present");
		}

		if (verifyObjectDisplayed(hiddenTreasureImg)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
					"Hiden Treasure Image present");
		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
					"Hidden treasure image not present");
		}

		if (verifyObjectDisplayed(hiddenTreasureTagLine)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
					getElementText(hiddenTreasureTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
					"Hidden Treasure Header tag line not present");
		}

		if (verifyObjectDisplayed(hiddenTreasureNav)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
					"Hidden treasure Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(hiddenTreasureFacebook)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
					"Hidden treasure facebook icon present");

			clickElementBy(hiddenTreasureFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
					"Hidden treasure  facebook icon not present");

		}

		if (verifyObjectDisplayed(hiddenTreasureTwitter)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
					"Hidden treasure twitter icon present");

			clickElementBy(hiddenTreasureTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
					"Hidden treasure on the bookstwitter icon not present");

		}

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(hiddenTreasureContent)) {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
					"Hidden Treasure content present on the page");

			try {
				List<WebElement> headerList = driver.findElements(hiddenContentHeader);
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
						"Hidden Treasure Headers present, Total: " + headerList.size());

				for (int i = 0; i < headerList.size(); i++) {
					tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.PASS,
							"Hidden Treasure Headers" + (i + 1) + " is " + headerList.get(i).getText());
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
						"Hidden Treasure Main Headers not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenTravelInspiration", "hiddenTreasure", Status.FAIL,
					"Hidden Treasure content not present on the page");
		}

	}
}
