package nextgen.pages;

import java.awt.AWTException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenSweepsPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenSweepsPage_Web.class);

	public NextGenSweepsPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By searchResultsHeader = By.xpath(/* "//h1[contains(.,'Search results')]" */
			// "//h1[contains(.,'Search')]");
			"//p[contains(text(),'Results')]");
	protected By searchResultCount = By
			.xpath("//div[@class='wyn-l-content wyn-l-padding-medium--top wyn-l-content-wrapper']");
	protected By searchResultsFor = By.xpath("//div[@class='search']//b");
	protected By searchResultsTitle = By.xpath(/* "//div[@class='wyn-aem-search__results']/a" */
			"//div[@class='margin-bottom-3']//a");
	protected By pagination = By.xpath("//div[@class='wyn-aem-search__pagination wyn-l-margin-xxsmall--bottom']//a");
	protected By NextGenCWLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']");
	protected By nextGenHeroImage = By
			.xpath("//div[@class='slick-slide slick-current slick-active']//div[@class='wyn-hero__image']");
	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Sweeps')]");
	protected By homeBreadcrumbNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Home')]");
	protected By acceptButton = By.xpath("//button[contains(.,'Accept')]");
	protected By searchIcon = By.xpath("//button[@type='submit']/span");
	protected By searchContainer = By.xpath(/* "//div[@role='combobox']//input" */
			"(//div//input[@class='desktop-search__input open'])[1]");
	protected By searchContainer1 = By.xpath(/* "//div[@role='combobox']//input" */
			"(//div//input[@class='desktop-search__input open'])[1]");
	protected By searchButton = By.xpath(/* "//button[contains(.,'Search')]" */
			"(//button[contains(.,'Search')])[1]");
	protected By searchButton1 = By.xpath(/* "//button[contains(.,'Search')]" */
			"(//button[contains(.,'Search')])[1]");
	protected By searchSuggestion = By
			.xpath("//div[@class='react-autosuggest__section-container react-autosuggest__section-container--first']");

	// Wyndham Sweeps
		protected By heroImage = By.xpath(/*"//div[@class='wyn-hero__image']//img"*/
				"(//div[contains(@class,'aem-GridColumn')])[2]//div[contains(@class,'show-for-large')]//img[1]");
		protected By sweepsBrdCrm = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Sweeps')]");
		protected By pageHeader = By.xpath(/*"//div[@gtm_component='content']//h2"*/
				"//h3[contains(@class,'title-1')]");
		protected By sweepsForm = By.xpath(/*"//div[@gtm_component='form']"*/
				"//form[@id='SweepsForm']");
		protected By sweepsFirstName = By.xpath(/*"//input[@id='firstName']"*/
				/*"//input[@name='FirstName']"*/
				"//input[@name='firstName']");
		protected By sweepsLastName = By.xpath(/*"//input[@id='lastName']"*/
				/*"//input[@name='LastName']"*/
				"//input[@name='lastName']");
		protected By sweepsAddress = By.xpath(/*"//input[@id='address']"*/
				/*"//input[@name='Address']"*/
				"//input[@name='address']");
		protected By sweepsCity = By.xpath(/*"//input[@id='city']"*/
				/*"//input[@name='City']"*/
				"//input[@name='city']");
		protected By sweepsZip = By.xpath(/*"//input[@id='zipCode']"*/
				"//input[@name='zipCode']");
		protected By sweepsEmail = By.xpath(/*"//input[@id='email']"*/
				"//input[@name='email']");
		protected By sweepsPhone = By.xpath(/*"//input[@id='altphone']"*/
				/*"//input[@name='altphoneNumber']"*/
				"//input[@name='altPhoneNumber']");
		protected By sweepsHomePh = By.xpath(/*"//input[@id='PhoneNumber']"*/
				//"//input[@name='phoneNumberNumber']");
	            "//input[@name='phoneNumber']");
		protected By maritalStatus = By.xpath(/*"//select[@name='maritalStatus*']"*/
				"//select[@name='maritalStatus']");
		protected By sweepsState = By.xpath("//select[@name='state']");
		protected By sweepsAge = By.xpath("//select[@name='age']");
		protected By sweepsIncome = By.xpath("//select[@name='income']");
		protected By accessCode = By.xpath(/*"//span[contains(.,'Access')]/parent::div/following-sibling::label"*/
				"//div[contains(text(),'Access')]/../div[@id='captcha']");
		protected By sweepsAccessCode = By.xpath(/*"//span[contains(.,'Access Code')]/preceding-sibling::input"*/
				/*"//div[contains(text(),'Access')]/../div[@id='captcha']"*/
				//"//span[contains(.,'Access')]//..//../label");
	            "//span[contains(text(),'Access Code*')]/../input");
		
		protected By agreeBnDisable = By.xpath(/*"//button[contains(.,'I agree') and @disabled]"*/
				"//button[contains(text(),'I agree') and contains(@class,'disabled')]");
		protected By notAgreeDisable = By.xpath(/*"//button[contains(.,'I do not') and @disabled]"*/
				"//button[contains(text(),'I do not') and contains(@class,'disabled')]");
		protected By agreeBnEnable = By.xpath("//button[contains(text(),'I agree')]");
		protected By notAgreeEnable = By.xpath("//button[contains(text(),'I do not')]");
		protected By thankYouNote = By.xpath(/*"//h4[contains(.,'Thanks for your')]"*/
				"//div[contains(text(),'Thank you')]");
		protected By closePopUp = By.xpath("//span[@class='wyn-modal__close wyn-modal__close--secondary']");
		String strAccesCode;

	/*
	 * Method: wyndhamSweepsNav Description: Wyndham Sweeps Navigation: Date
	 * Dec/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamSweepsNav() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, searchIcon, 120);

		if (verifyObjectDisplayed(searchIcon)) {

			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS,
					"Search Icon Present on Homepage");

			clickElementBy(searchIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchContainer)) {

				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS,
						"Search Container displayed with text: "
								+ driver.findElement(searchContainer).getAttribute("placeholder"));

				driver.findElement(searchContainer).click();
				driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.PASS,
						"Search Keyword Entered");

				clickElementJSWithWait(searchButton);

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

				if (verifyObjectDisplayed(searchResultsHeader)) {
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
							"Navigated to Search Results page");

					List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
							"No Of Results on first page " + listOfResults.size());

					for (int i = 0; i < listOfResults.size(); i++) {

						if (listOfResults.get(i).getText().equalsIgnoreCase("Wyndham Sweeps")) {
							listOfResults.get(i).click();

							waitUntilElementVisibleBy(driver, homeBreadcrumb, 120);
							break;
						}

					}

				} else {
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
							"Unable to navigate to search results page");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.FAIL,
						"Error in getting the search container");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsNav", Status.FAIL,
					"Error in getting the search Icon");
		}

	}

	/*
	 * Method: wyndhamSweepsVal Description: Wyndham Sweeps Validations: Date
	 * Dec/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamSweepsVal() throws AWTException {

		getElementInView(pageHeader);
		if (verifyObjectDisplayed(pageHeader)) {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
					"Navigated To Wyndham Sweeps Page, Main Header Present");

			if (verifyObjectDisplayed(heroImage)) {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Hero Image Present");
			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Hero Image not Present");
			}

			if (verifyObjectDisplayed(sweepsBrdCrm)) {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Breadcrumb Present");
			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Breadcrumb not Present");
			}

			if (verifyObjectDisplayed(sweepsForm)) {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Form Present");

				getElementInView(sweepsFirstName);

				if (verifyObjectDisplayed(sweepsFirstName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(sweepsFirstName);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(sweepsFirstName, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsLastName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(sweepsLastName, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsAddress)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Address Field Present");

					fieldDataEnter(sweepsAddress, testData.get("Address"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Address Field not present");
				}

				if (verifyObjectDisplayed(sweepsCity)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"City Field Present");

					fieldDataEnter(sweepsCity, testData.get("City"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"City Field not present");
				}

				if (verifyObjectDisplayed(sweepsPhone)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(sweepsPhone, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(sweepsEmail)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(sweepsEmail, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(sweepsZip)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Zip Field Present");

					fieldDataEnter(sweepsZip, testData.get("ZipCode"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Zip Field not present");
				}

				if (verifyObjectDisplayed(sweepsHomePh)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Home Phone Field Present");

					fieldDataEnter(sweepsHomePh, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Home Phone Field not present");
				}

				if (verifyObjectDisplayed(maritalStatus)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Marital Status Drop Down present");

					new Select(driver.findElement(maritalStatus)).selectByVisibleText(testData.get("Marital Status"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Marital Status Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsAge)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Age Drop Down present");

					new Select(driver.findElement(sweepsAge)).selectByVisibleText(testData.get("Age"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Age Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsIncome)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Income Drop Down present");

					new Select(driver.findElement(sweepsIncome)).selectByVisibleText(testData.get("Income"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Income Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsState)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"State Drop Down present");

					new Select(driver.findElement(sweepsState)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(accessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code present as: " + getElementText(accessCode));

					strAccesCode = getElementText(accessCode);
					System.out.println(strAccesCode);

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code not present");
				}

				if (verifyObjectDisplayed(agreeBnDisable)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Disabled before enetering Acces code");

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button Enabled before enetering Acces code");
				}

				/*
				 * if (verifyObjectDisplayed(notAgreeDisable)) {
				 * tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal",
				 * Status.PASS, "Do Not Agree Button Disabled before enetering Acces code");
				 * 
				 * } else { tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal",
				 * Status.FAIL, "Do Not Agree Button Enabled before enetering Acces code"); }
				 */

				if (verifyObjectDisplayed(sweepsAccessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code Field Present");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					// Robot rob = new Robot();
					// rob.mouseWheel(2);
					// clickElementJSWithWait(sweepsAccessCode);
					driver.findElement(sweepsAccessCode).click();

					driver.findElement(sweepsAccessCode).sendKeys(strAccesCode);
					// fieldDataEnter(sweepsAccessCode, strAccesCode);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code Field not present");
				}

				if (verifyObjectDisplayed(agreeBnEnable)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Enabled After enetering Access code");
					clickElementBy(agreeBnEnable);

					waitUntilElementVisibleBy(driver, thankYouNote, 240);

					if (verifyObjectDisplayed(thankYouNote)) {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
								"Form Submission Successful");

						/*
						 * if (verifyObjectDisplayed(closePopUp)) { clickElementBy(closePopUp); } else {
						 * tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal",
						 * Status.FAIL, "Close Btn not present"); }
						 */

					} else {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
								"Form Submission not Successful");
					}

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button disabled after enetering Acces code");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Form not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
					"Wyndham Sweeps Navigation Failed");
		}

	}

	/*
	 * Method: wyndhamSweepsNotAgreeVal Description: Wyndham Sweeps Validations:
	 * Date Dec/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamSweepsNotAgreeVal() {

		if (browserName.equalsIgnoreCase("EDGE")) {
			String strURL = driver.getCurrentUrl();
			driver.navigate().to(strURL);
		} else {
			driver.navigate().refresh();
		}
		waitUntilElementVisibleBy(driver, pageHeader, 120);
		if (verifyObjectDisplayed(pageHeader)) {

			if (verifyObjectDisplayed(sweepsForm)) {

				getElementInView(sweepsFirstName);

				if (verifyObjectDisplayed(sweepsFirstName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(sweepsFirstName);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(sweepsFirstName, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsLastName)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(sweepsLastName, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsAddress)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Address Field Present");

					fieldDataEnter(sweepsAddress, testData.get("Address"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Address Field not present");
				}

				if (verifyObjectDisplayed(sweepsCity)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"City Field Present");

					fieldDataEnter(sweepsCity, testData.get("City"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"City Field not present");
				}

				if (verifyObjectDisplayed(sweepsPhone)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(sweepsPhone, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(sweepsEmail)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(sweepsEmail, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(sweepsZip)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Zip Field Present");

					fieldDataEnter(sweepsZip, testData.get("ZipCode"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Zip Field not present");
				}

				if (verifyObjectDisplayed(sweepsHomePh)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Home Phone Field Present");

					fieldDataEnter(sweepsHomePh, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Home Phone Field not present");
				}

				if (verifyObjectDisplayed(maritalStatus)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Marital Status Drop Down present");

					new Select(driver.findElement(maritalStatus)).selectByVisibleText(testData.get("Marital Status"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Marital Status Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsAge)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Age Drop Down present");

					new Select(driver.findElement(sweepsAge)).selectByVisibleText(testData.get("Age"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Age Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsIncome)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Income Drop Down present");

					new Select(driver.findElement(sweepsIncome)).selectByVisibleText(testData.get("Income"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Income Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsState)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"State Drop Down present");

					new Select(driver.findElement(sweepsState)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(accessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code present as: " + getElementText(accessCode));

					if (strAccesCode.equals(getElementText(accessCode))) {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
								"Access Code same as previous present");
					} else {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
								"Access Code different from previous present");
					}

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code not present");
				}

				if (verifyObjectDisplayed(sweepsAccessCode)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Access Code Field Present");
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.findElement(sweepsAccessCode).sendKeys(getElementText(accessCode));
					// fieldDataEnter(sweepsAccessCode, getElementText(accessCode));
					driver.switchTo().activeElement().sendKeys(Keys.TAB);

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code Field not present");
				}

				if (verifyObjectDisplayed(notAgreeEnable)) {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
							"Do Not Agree Button Enabled After enetering Access code");
					clickElementBy(notAgreeEnable);

					waitUntilElementVisibleBy(driver, thankYouNote, 120);

					if (verifyObjectDisplayed(thankYouNote)) {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.PASS,
								"Form Submission Successful");

						/*
						 * if (verifyObjectDisplayed(closePopUp)) { clickElementBy(closePopUp); } else {
						 * tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal",
						 * Status.FAIL, "Close Btn not present"); }
						 */

					} else {
						tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
								"Form Submission not Successful");
					}

				} else {
					tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
							"Do Not Agree Button disabled before enetering Acces code");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Form not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSweepsPage", "wyndhamSweepsVal", Status.FAIL,
					"Wyndham Sweeps Navigation Failed");
		}

	}

	public void wyndhamSweepsNavigation() {
		// TODO Auto-generated method stub
		
	}

	public void wyndhamSweepsValidation() throws AWTException {
		// TODO Auto-generated method stub
		
	}

}
