package nextgen.pages;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUILoginPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUILoginPage_Web.class);

	public CUILoginPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By headerNavigation = By.xpath("//div[@class='global-navigation__wrapper']");
	protected By headerLogo = By
			.xpath("//div[@class='global-navigation__wrapper']//li[@class='global-navigation__logo']");
	protected By headerLogoUpdated = By.xpath("//div[@class='global-navigation__wrapper']//img");
	protected By logOutNextGen = By.xpath(
			"(//form[@id='leadForm'] | //input[@id='username']//following-sibling::span[contains(.,'Username*')] )");
	protected By footerNavigation = By.xpath("//div[contains(@class,'footer-holiday')]");
	protected By footerLogo = By
			.xpath("//div[contains(@class,'footer-holiday')]//a[@data-eventlabel='FooterLogo']/img");
	protected By usernameText = By.xpath("//input[@id='username']//following-sibling::span[contains(.,'Username*')]");
	protected By passwordText = By.xpath("//input[@id='password']//following-sibling::span[contains(.,'Password *')]");
	protected By modalText = By.xpath(
			"(//p[contains(.,'ARE YOU A CLUB WYNDHAM OWNER')] | //div[contains(@class,'caption') and contains(.,'ARE YOU A CLUB')])");
	protected By modalHeader = By
			.xpath("(//h1[contains(.,'GET YOU ON VACATION')] | //div[contains(@id,'loginHeadline')])");
	protected By showCTA = By.xpath("//input[@id='password']//following-sibling::a[contains(.,'show')]");
	protected By createAccountDescription = By.xpath(
			"//h3[contains(.,'Need To Create An Account')]//following-sibling::p | //div[contains(@class,'caption') and contains(.,'Create An Account')]//following-sibling::div/p");
	protected By notAnOwnerYetDescription = By.xpath(
			"(//h3[contains(.,'Not An Owner Yet')]//following-sibling::p | //div[text() = 'Not An Owner Yet? ']//following-sibling::div/p)");
	protected By welcomeHeader = By.xpath(/* "//h4[contains(.,'WELCOME')]" */
			"//div[contains(text(),'WELCOME')]");
	protected By errorMessage = By.id("error-message");
	protected By invalidPassword = By.xpath(
			"(//p[@id='error-message']//p[contains(.,'Username and Password combination is incorrect')] | //div[@id='error-message']//p[contains(.,'Username and Password combination is incorrect')])");
	protected By accountLocked = By.xpath(
			"(//p[@id='error-message']//p[contains(.,'Your account has been temporarily locked')] | //div[@id='error-message']//p[contains(.,'Your account has been temporarily locked')])");
	protected By invalidLoginError = By
			.xpath("//div[@class='error-block']//p[@id and contains(.,'Something unexpected just happened')]");
	protected By clickFeedbackNo = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By linkForgotPassword = By.xpath("//a[text() = 'Forgot Password']");
	protected By linkForgotUsername = By.xpath("//a[text() = 'Forgot Username']");
	protected By linkCreateAccount = By.xpath(
			"(//h3[text() ='Need To Create An Account?'] | //div[contains(@class,'caption') and contains(.,'Create An Account')])");
	protected By linkRegisterNow = By.xpath("//a[contains(.,'Register Now')]");
	protected By linkNotAnOwner = By
			.xpath("(//h3[text() = 'Not An Owner Yet? '] | //div[text() = 'Not An Owner Yet? '])");
	protected By linkLearnAboutWyndham = By.xpath("//a[contains(.,'Learn More About Club Wyndham')]");
	protected By fieldUsername = By.xpath("//input[@id = 'username']");
	protected By fieldPassword = By.xpath("//input[@id = 'password']");
	protected By loginButton = By.xpath("//button[text()='Login']");
	protected By ownerFullName = By.xpath("//div[contains(text(),'WELCOME')]/parent::div/div[contains(@class,'title-2')]");
	protected By loginUsername = By.xpath("//input[@id = 'username']");
	protected By cookieAccept = By.xpath("//button[contains(.,'Accept')]");
	protected By logOutCTA = By
			.xpath("//div[@class='top-bar-right']//div[@class='global-account__links']//span[contains(text(),'Log')]");
	protected By myAccountDropDown = By
			.xpath("//div[@class='global-navigation__wrapper']//button[contains(@class,'container')]");
	protected By dropDownLogOut = By.xpath(
			"//section[contains(@class,'desktop')]//div[@class='global-account__links']//a[contains(@class,'logout')]//span");
	protected By covidAlertClose = By.xpath("//button[contains(@class,'setCookie') and contains(.,'Close')]");
	protected By bannerAlertClose = By.xpath("//button[contains(@class,'close-button')]");

	protected By feedbackContainer = By.xpath("//div[@id = 'oo_invitation_prompt']");
	protected By FeedbackNoOption = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By labelForgetPassword = By.xpath("//h2[@class='text-white title-1']");
	protected By labelForgetUsername = By.xpath("//h1[@class='text-white title-1']");
	protected By labelLoginHeader = By.xpath("//div[@id='loginHeadline']");
	protected By acceptButton = By.xpath("//button[contains(.,'Accept')]");
	protected By labelUserName = By.xpath("//li[@class='loggedIn']/span");
	
	protected By lablePointToSpend = By.xpath("//div[@class='subtitle-2 text-center' and contains(.,'How many points would you like to use?')]");
	/*
	 * Method: loginCTAClick Description: Click On login cta Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void loginCTAClickForTripPlaner() throws Exception {
		waitUntilElementVisibleBy(driver, loginButton, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, lablePointToSpend, 120);
			getElementInView(lablePointToSpend);

			Assert.assertTrue(verifyObjectDisplayed(lablePointToSpend));

			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.PASS, "User Login successful");

		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * Method: acceptCookie Description: Accept Cookie Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void acceptCookie() {
		waitUntilElementVisibleBy(driver, cookieAccept, "ExplicitMedWait");
		if (verifyObjectDisplayed(cookieAccept)) {
			clickElementBy(cookieAccept);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("CUILoginPage", "acceptCookie", Status.PASS, "Successfully accepted Cookie");
			closeBannerAlert();
			closeCovidAlert();
		}

	}
	
	/*
	 * Method: closeBannerAlert Description: Close Banner Date: May/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void closeBannerAlert() {
		try {
			WebElement bannerClose = getList(bannerAlertClose).get(0);
			if (verifyObjectDisplayed(bannerClose)) {
				clickElementBy(bannerClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CUILoginPage", "closeBannerAlert", Status.PASS,
						"Successfully Closed Alert");
			}

		} catch (Exception e) {
			log.error("Banner Message Not Present");

		}
	}

	/*
	 * Method: closeCovidAlert Description: Close Covid Date: Apr/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void closeCovidAlert() {
		try {
			WebElement covidClose = getList(covidAlertClose).get(0);
			if (verifyObjectDisplayed(covidClose)) {
				clickElementBy(covidClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("CUILoginPage", "closeAlert", Status.PASS, "Successfully Closed Alert");
			}

		} catch (Exception e) {
			log.error("Covid Message Not Present");

		}
	}
	
	/*
	 * Method: globalHdrPresence Description: Login page global Header and logo
	 * Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void globalHeaderPresence() {

		if (verifyObjectDisplayed(headerNavigation)) {
			tcConfig.updateTestReporter("CUILoginPage", "globalHeaderPresence", Status.PASS,
					"Global Header is present in the login page");

			elementPresenceVal(headerLogoUpdated, "Header Logo ", "CUILoginPage", "globalHeaderPresence");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "globalHeaderPresence", Status.FAIL,
					"Global Header is not present in the login page");
		}

	}
	
	/*
	 * Method: globalFooterPresence Description: Login page global footer and
	 * logo Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void globalFooterPresence() {

		if (verifyObjectDisplayed(footerNavigation)) {
			tcConfig.updateTestReporter("CUILoginPage", "globalFooterPresence", Status.PASS,
					"Global Footer is present in the login page");

			elementPresenceVal(footerLogo, "Footer Logo ", "CUILoginPage", "globalFooterPresence");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "globalFooterPresence", Status.FAIL,
					"Global Footer is not present in the login page");
		}

	}
	
	/*
	 * Method: loginModalHdrs Description: Modal Headers and Text Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void loginModalHeaders() {

		if (verifyObjectDisplayed(modalHeader)) {
			tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.PASS,
					"Modal Header present as: " + getElementText(modalHeader));

			if (verifyObjectDisplayed(modalText)) {
				tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.PASS,
						"Modal Inline text present as: " + getElementText(modalHeader));
			} else {
				tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.FAIL,
						"Modal Inline Text not present");
			}
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginModalHeaders", Status.FAIL,
					"Modal Headers not present");
		}

	}
	
	/*
	 * Method: usernameFieldVal Description: Username Field Presence and text
	 * Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void usernameFieldValidation() {
		waitUntilElementVisibleBy(driver, linkForgotPassword, "ExplicitLongWait");
		if (verifyObjectDisplayed(fieldUsername)) {
			tcConfig.updateTestReporter("CUILoginPage", "usernameFieldValidation", Status.PASS,
					"Username Field present with pretext " + getElementText(usernameText));
			elementPresenceVal(linkForgotUsername, "Forgot Username ", "CUILoginPage", "usernameFieldValidation");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "usernameFieldValidation", Status.FAIL,
					"Username field not present");
		}

	}

	/*
	 * Method: passwordFieldVal Description: Password Field Presence and text
	 * Val Date: Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void passwordFieldValidation() {

		if (verifyObjectDisplayed(fieldPassword)) {
			tcConfig.updateTestReporter("CUILoginPage", "passwordFieldValidation", Status.PASS,
					"Password Field present with pretext " + getElementText(passwordText));
			elementPresenceVal(showCTA, "Show CTA ", "CUILoginPage", "passwordFieldValidation");
			elementPresenceVal(linkForgotPassword, "Forgot Password Link", "CUILoginPage", "passwordFieldValidation");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "passwordFieldValidation", Status.FAIL,
					"Password field not present");
		}

	}
	
	/*
	 * Method: newAccountSection Description: New Account Section Val Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void newAccountSection() {

		waitUntilElementVisibleBy(driver, linkCreateAccount, "ExplicitLongWait");
		getElementInView(linkCreateAccount);
		if (verifyObjectDisplayed(linkCreateAccount)) {
			tcConfig.updateTestReporter("CUILoginPage", "newAccountSection", Status.PASS,
					"Create Account Link present");
			elementPresenceVal(createAccountDescription, "Create Account Description", "CUILoginPage",
					"newAccountSection");
			elementPresenceVal(linkRegisterNow, "Register Now Link ", "CUILoginPage", "newAccountSection");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "newAccountSection", Status.FAIL,
					"Create Account Link not present");
		}

	}

	/*
	 * Method: multilpeLoginClick Description: Click On login cta Date: Mar/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void multilpeLoginClick() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			getElementInView(loginButton);
			clickElementBy(loginButton);
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginButtonClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * Method: notAnOwnerSection Description: Not an owner Section Val Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void notAnOwnerSection() {
		// getElementInView(link_notanOwner);
		if (verifyObjectDisplayed(linkNotAnOwner)) {
			tcConfig.updateTestReporter("CUILoginPage", "notAnOwnerSection", Status.PASS,
					"Not an Owner link present");

			elementPresenceVal(notAnOwnerYetDescription, "Not an Owner Desc", "CUILoginPage", "notAnOwnerSection");
			elementPresenceVal(linkLearnAboutWyndham, "Learn About Wyndham Link ", "CUILoginPage", "notAnOwnerSection");
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "notAnOwnerSection", Status.FAIL,
					"Not an Owner link not present");
		}

	}
	/*
	 * Method: loginBtnClick Description: Click On login cta Date: Mar/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void loginButtonClick() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			getElementInView(loginButton);
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// checkErrorClickLoginAgain();
		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginButtonClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * Method: loginCTAClick Description: Click On login cta Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void loginCTAClick() throws Exception {
		waitUntilElementVisibleBy(driver, loginButton, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, welcomeHeader, 120);
			getElementInView(welcomeHeader);

			Assert.assertTrue(verifyObjectDisplayed(welcomeHeader));

			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.PASS, "User Login successful");

		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * Method: setUserName Description: Set Username with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, 120);
		getElementInView(modalHeader);
		// Username entry
		String strUserName = testData.get("CUI_username");
		fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "setUserName", Status.PASS,
				"Username is entered for CUI Login as " + strUserName);
	}

	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setPassword() throws Exception {
		// password data
		String password = testData.get("CUI_password");
		// decodeed password
		byte[] decodedString = DatatypeConverter.parseBase64Binary(password);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(fieldPassword).sendKeys(dString);
		tcConfig.updateTestReporter("CUILoginPage", "setPassword", Status.PASS, "Entered password for CUI login ");
	}

	/*
	 * Method: logOutApplicationViaDashboard Description: Account Log out via
	 * dasboard page Validations Date: Apr/2020 Author: Unnat Jain Changes By: NA
	 */
	public void logOutApplicationViaDashboard() {
		navigateToURL(tcConfig.getConfig().get("NewCUILogOutUrl"));
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));
		if (getList(logOutCTA).size() == 1) {
			getElementInView(getList(logOutCTA).get(0));
			clickElementBy(getList(logOutCTA).get(0));
		} else if (getList(logOutCTA).size() == 3) {
			getElementInView(getList(logOutCTA).get(2));
			clickElementBy(getList(logOutCTA).get(2));
		}
		waitUntilElementVisibleBy(driver, usernameText, 120);
		tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS, "User Logged Out");
	}

	/*
	 * Method: forgetUserNameClick Description: click forget username dasboard page
	 * Validations Date: sept/2020 Author: Saket Sharma Changes By: NA
	 */

	public void forgetUserNameClick() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(linkForgotUsername));
		tcConfig.updateTestReporter("CUILoginPage", "forgetUserNameClick", Status.PASS, "Forget Username Displayed");

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(linkForgotUsername);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(labelForgetUsername));
		tcConfig.updateTestReporter("CUILoginPage", "forgetUserNameClick", Status.PASS,
				"Forget Username page Displayed sucessfully");

	}

	public void forgetUserNameLinkValadiation() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(labelForgetUsername));
		tcConfig.updateTestReporter("CUILoginPage", "forgetUserNameLinkValadiation", Status.PASS,
				"Forget Username correct URL displayed");

		Assert.assertTrue(driver.getCurrentUrl().toUpperCase().contains(testData.get("header1").toUpperCase()));
		tcConfig.updateTestReporter("CUILoginPage", "forgetUserNameLinkValadiation", Status.PASS,
				"Forget Username correct URL not displayed");

	}

	public void forgetPasswordLinkValadiation() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(labelForgetPassword));
		tcConfig.updateTestReporter("CUILoginPage", "forgetPasswordLinkValadiation", Status.PASS,
				"Forget Password correct URL displayed");

		Assert.assertTrue(driver.getCurrentUrl().toUpperCase().contains(testData.get("header2").toUpperCase()));
		tcConfig.updateTestReporter("CUILoginPage", "forgetPasswordLinkValadiation", Status.PASS,
				"Forget Password correct URL not displayed");

	}

	public void forgetPasswordClick() throws Exception {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(linkForgotPassword));
		tcConfig.updateTestReporter("CUILoginPage", "forgetPasswordClick", Status.PASS, "Forget password Displayed");

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(linkForgotPassword);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(labelForgetPassword));
		tcConfig.updateTestReporter("CUILoginPage", "forgetPasswordClick", Status.PASS,
				"Forget Username page password sucessfully");

	}

	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * Feb/2020 Author: Saket Sharma Changes By: NA
	 */

	public void setAnotherUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, 120);
		getElementInView(modalHeader);
		// Username entry
		String strUserName = testData.get("header2");
		fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "setAnotherUserName", Status.PASS,
				"Username is entered for CUI Login as " + strUserName);
	}

	public void setAnotherPassword() throws Exception {
		// password data
		String password = testData.get("header3");
		// decodeed password
		byte[] decodedString = DatatypeConverter.parseBase64Binary(password);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(fieldPassword).sendKeys(dString);
		tcConfig.updateTestReporter("CUILoginPage", "setAnotherUserName", Status.PASS,
				"Entered password for CUI login ");
	}

	public void navigateBack() {
		// TODO Auto-generated method stub
		driver.navigate().back();

	}

	/*
	 * public void logOutApplication() {
	 * navigateToURL(tcConfig.getConfig().get("NewCUILogOutUrl"));
	 * waitUntilElementVisibleBy(driver, labelLoginHeader, 120);
	 * 
	 * try { waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * waitUntilObjectVisible(driver, acceptButton, 20); if
	 * (verifyObjectDisplayed(acceptButton)) {
	 * tcConfig.updateTestReporter("Club Wyndham Homepgage", "launchApplication",
	 * Status.PASS, "Accept Cookies Button Present");
	 * 
	 * clickElementJSWithWait(acceptButton);
	 * waitForSometime(tcConfig.getConfig().get("LowWait")); }
	 * 
	 * } catch (Exception e) { System.out.println("No Button"); }
	 * 
	 * try { List<WebElement> covidClose = driver.findElements(covidAlertClose);
	 * waitUntilObjectVisible(driver, covidClose.get(0), 20); if
	 * (covidClose.get(0).isDisplayed()) { covidClose.get(0).click();
	 * waitForSometime(tcConfig.getConfig().get("LowWait"));
	 * tcConfig.updateTestReporter("Club Wyndham Homepgage", "closeAlert",
	 * Status.PASS, "Successfully Closed Alert"); }
	 * 
	 * } catch (Exception e) { log.error("Covid Message Not Present");
	 * 
	 * } Assert.assertTrue(verifyObjectDisplayed(labelLoginHeader));
	 * tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS,
	 * "User Logged Out");
	 * 
	 * }
	 */

	public void logOutApplication() {

		if (verifyObjectDisplayed(myAccountDropDown)) {
			clickElementBy(myAccountDropDown);
			waitUntilElementVisibleBy(driver, logOutCTA, 120);
			clickElementBy(logOutCTA);

			try {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, acceptButton, 20);
				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("Club Wyndham Homepgage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementJSWithWait(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No Button");
			}

			try {
				List<WebElement> covidClose = driver.findElements(covidAlertClose);
				waitUntilObjectVisible(driver, covidClose.get(0), 20);
				if (covidClose.get(0).isDisplayed()) {
					covidClose.get(0).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("Club Wyndham Homepgage", "closeAlert", Status.PASS,
							"Successfully Closed Alert");
				}

			} catch (Exception e) {
				log.error("Covid Message Not Present");
			}

			waitUntilElementVisibleBy(driver, labelLoginHeader, 120);
			Assert.assertTrue(verifyObjectDisplayed(labelLoginHeader));
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.PASS,
					"User loged out");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.FAIL,
					"User failed to Navigated to My Ownership page");
		}
	}

	/*
	 * Method: loginCTAClickpopup Description: Click On login cta Date: oct/2020
	 * Author: Saket Sharama Changes By: NA
	 */
	public void loginCTAClickPopup() throws Exception {
		waitUntilElementVisibleBy(driver, loginButton, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			Assert.assertTrue(verifyObjectDisplayed(labelUserName));

			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClickPopup", Status.PASS, "User Login successful");

		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClickPopup", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	public void errorMessasgeValidation(String string) {
		// TODO Auto-generated method stub

	}

	public void clearCache() {
		// TODO Auto-generated method stub

	}

}
