package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenTravelPage_Android extends NextGenTravelPage_Web {

	public static final Logger log = Logger.getLogger(NextGenTravelPage_Android.class);

	public NextGenTravelPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
