package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class HowToUseCWTimeshare_Android extends HowToUseCWTimeshare_Web {

	public static final Logger log = Logger.getLogger(HowToUseCWTimeshare_Android.class);

	public HowToUseCWTimeshare_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
