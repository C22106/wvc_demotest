package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class OwnerTestimonialPage_IOS extends OwnerTestimonialPage_Web {

	public static final Logger log = Logger.getLogger(OwnerTestimonialPage_IOS.class);

	public OwnerTestimonialPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}