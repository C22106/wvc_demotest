package nextgen.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.safari.SafariDriver.WindowType;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenHomepage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenHomepage_Web.class);

	public NextGenHomepage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By NextGenCWLogo = By.xpath(/* "//div[@class='column is-narrow wyn-header__logo']" */
			"(//img[@alt='Wyndham'])[1]");
	// protected By nextGenHeroImage = By.xpath("(//div[@class='cardBanner
	// ']//div[@class='image wow animate-fade-in-image']//img)[1]");
	protected By nextGenHeroImage = By.xpath(/* "//div[@class='blockVideo']/div[@class='video']" */
			"//div[@id='homepage']//div[@aria-hidden='false']//section[contains(@class,'banner')]//img");
	// .xpath("//div//section[@class='banner animation-wrapper
	// ']//div//div[1]//video");

	protected By featuredResortHdr = By.xpath("//h2[contains(.,'Featured')]");
	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	protected By resortsPageBreadCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Resorts')]");
	protected By homeBreadcrumbNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Home')]");
	protected By acceptButton = By.xpath("//button[contains(.,'Accept')] | //button[contains(text(),'Got It!')]");
	protected By searchIcon2 = By.xpath("//button[contains(.,'search')]");
	protected By searchContainer1 = By.xpath("(//div//input[@class='desktop-search__input open'])[1]");
	protected By searchContainer = By.xpath("(//div[@class='react-autosuggest__container']//input)[1]");
	protected By searchButton = By.xpath("(//button[contains(.,'Search')])[1]");
	protected By searchButton1 = By.xpath("(//div//button[contains(@class,'search__button')])[1]");
	protected By searchSuggestion = By.xpath("(//div[contains(@class,'search--results')])[2]//li");

	protected By getStartedBtn = By.xpath(/*
											 * "//div[@class='wyn-card wyn-card__accent ']//a[contains(.,'Explore Resorts')]"
											 */
			"//div[@id='homepage']//div[@aria-hidden='false']//div[contains(@class,'bannerCard ')]//div[contains(@class,'title-1')]/../a");
	protected By heroImageTitle = By.xpath(/* "//div[@class='video']//div[@class='title-1']" */
			/*
			 * "//div[@id='homepage']//div[@aria-hidden='false']//div[contains(@class,'bannerCard ')]//div[contains(@class,'title-1') and contains(text(),'VACATIONS')]"
			 * );
			 */
			"//div[@id='homepage']//div[@aria-hidden='false']//div[contains(@class,'bannerCard ')]//div[contains(@class,'title-1')]");
	// .xpath("//div[@class='cell large-5 bannerCard wow
	// animate-fade-in-card']//h1");
	protected By heroImageDesc = By.xpath(/* "//div[@class='video']//div[@class='title-1']/../div[@class='body-1']/p" */
			"//div[@id='homepage']//div[@aria-hidden='false']//div[contains(@class,'bannerCard ')]//div[contains(@class,'title-1')]/../div[@class='body-1']/p");
	// "//div[@class='cell large-5 bannerCard wow
	// animate-fade-in-card']//div[3]//p");
	// title[contains(.,'Find Resorts')]
	protected By dealsNOffersTitle = By.xpath("//div[contains(text(),'Deals & Offers')]");
	protected By seeAllDeals = By.xpath("//a[contains(.,'See All Deals')]"); // Deals
	protected By slickDotsActive = By.xpath("//span[contains(@class,'swiper-pagination-bullet-active')]"); // &l
	// Deals And Offers // Offers

	protected By totalCardsDO = By.xpath(
			"//div[contains(text(),'Deals & Offers')]/../../../../../../.././following-sibling::div//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-4']");
	// "//h2[contains(.,'Deals &
	// Offers')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='column
	// is-flex wyn-card-list__card is-3-desktop']");
	protected By cardsImageDO = By.xpath(
			"//div[contains(text(),'Deals & Offers')]/../../../../../../.././following-sibling::div//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-4']//div//a//img");
	/*
	 * "//h2[contains(.,'Deals & Offers')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-card__image']"
	 * );
	 */
	protected By cardsTitleDO = By.xpath(
			"//div[contains(text(),'Deals & Offers')]/../../../../../../.././following-sibling::div//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-4']//div//a//div[(@class='subtitle-2 bold')]");
	/*
	 * "//h2[contains(.,'Deals & Offers')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-title']//h3"
	 * );
	 */
	protected By cardsDescrDO = By.xpath(
			"//div[contains(text(),'Deals & Offers')]/../../../../../../.././following-sibling::div//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-4']//div//a//div//p");
	/*
	 * "//h2[contains(.,'Deals & Offers')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-l-content wyn-card__copy']/p"
	 * );
	 */
	protected By cardsSeeOffersDO = By.xpath(
			"//div[contains(text(),'Deals & Offers')]/../../../../../../.././following-sibling::div//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-4']//div//a[2]");
	/*
	 * "//h2[contains(.,'Deals & Offers')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-card__cta' and contains(.,'See Offers')]"
	 * );
	 */

	// Destinations Guides
	protected By destinationGuidesTitle = By.xpath("//div[contains(text(),'Destination guide')]");
	protected By totalCardsDG = By.xpath(
			"//div[contains(text(),'Destination guides')]/../../../../../../../following-sibling::div[2]//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-3']");
	/*
	 * "//h2[contains(.,'Destination Guide')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='column is-flex wyn-card-list__card is-3-desktop']"
	 * );
	 */
	protected By cardsImageDG = By.xpath(
			"//div[contains(text(),'Destination guides')]/../../../../../../../following-sibling::div[2]//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-3']//div/a//img");
	/*
	 * "//h2[contains(.,'Destination Guide')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-card__image']"
	 * );
	 */
	protected By cardsTitleDG = By.xpath(
			"//div[contains(text(),'Destination guides')]/../../../../../../../following-sibling::div[2]//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-3']//div/a//div[(@class='subtitle-2 bold')]");
	/*
	 * "//h2[contains(.,'Destination Guide')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-title']//h3"
	 * );
	 */
	protected By cardsDescrDG = By.xpath(
			"//div[contains(text(),'Destination guides')]/../../../../../../../following-sibling::div[2]//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-3']//div/a//div[3]//p");
	/*
	 * "//h2[contains(.,'Destination Guide')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-l-content wyn-card__copy']/p"
	 * );
	 */
	protected By cardsReadMoreDG = By.xpath(
			"//div[contains(text(),'Destination guides')]/../../../../../../../following-sibling::div[2]//div[@class='grid-x grid-margin-x ']//div[@class='cell medium-3']//div//a[2]");
	/*
	 * "//h2[contains(.,'Destination Guide')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-card__cta' and contains(.,'Read More')]"
	 * );
	 */
	protected By seeAllDestinations = By.xpath("//a[contains(text(),'See All Guides')]"); // Deals
	protected By featDestBC = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Featured')]");
	protected By featuredResortsHeader = By.xpath(/* "//div[contains(text(),'MORE CACHING UPDATES')]" */
			"//div[contains(@class,'caption-1') and contains(text(),'FEATURED RESORTS')]");// changed
	protected By caraouselVal = By.xpath("(//div[@class='carousel']//div[contains(@class,'carouselComponent')])[3]");
	/*
	 * (
	 * "//div[contains(text(),'Featured Resorts')]/../../../../../../../following-sibling::div//div[@class='cell large-5 bannerCard wow animate-fade-in-card']"
	 * );
	 */
	protected By seeAllResorts = By.xpath("//a[contains(text(),'See All Resorts')]");
	protected By slickBuutons = By.xpath("//div[@id='homepageFeaturedResort']//li");
	protected By nextArrow = By.xpath("(//button[contains(@class,'swiper-button-next')])[1]");
	protected By prevArrow = By.xpath("(//button[contains(@class,'swiper-button-prev')])[1]");
	protected By carouselTitle = By.xpath(
			"//div[contains(text(),'FEATURED RESORTS')]//following::div[@id='homepageFeaturedResort']//div[@aria-hidden='false']//div[@class='title-1']");
	/*
	 * .xpath(
	 * "//div[@class='contentblock-carousel']//div[@class='slick-slide slick-current slick-active']//h2"
	 * );
	 */
	protected By carouselAddress = By.xpath(
			"//div[contains(text(),'FEATURED RESORTS')]//following::div[@id='homepageFeaturedResort']//div[@aria-hidden='false']//div[@class='subtitle-3']");
	/*
	 * .xpath(
	 * "//div[@class='contentblock-carousel']//div[@class='slick-slide slick-current slick-active']//h3"
	 * );
	 */
	protected By carouselImage = By.xpath(
			"//div[contains(text(),'FEATURED RESORTS')]//following::div[@id='homepageFeaturedResort']//div[contains(@class,'slick-active')]//section//div//div//img");
	/*
	 * .xpath(
	 * "//div[@class='contentblock-carousel']//div[@class='slick-slide slick-current slick-active']//img"
	 * );
	 */
	protected By carouselExploreBtn = By.xpath(
			/*
			 * "//div[contains(text(),'FEATURED RESORTS')]//following::div[@id='homepageFeaturedResort']//div[@aria-hidden='false']//div//a[text()='Explore Resort']"
			 */
			"//div[contains(text(),'FEATURED RESORTS')]//following::div[@id='homepageFeaturedResort']//div[@aria-hidden='false']//div//a[text()='Read More']");
	protected By carouselExploreBtnval = By.xpath("//div[contains(@class,'swiper-slide-active')]//a");
	/*
	 * "//div[@class='contentblock-carousel']//div[@class='slick-slide slick-current slick-active']//a[contains(.,'Explore Resort')]"
	 * );
	 */
	protected By indulgeAustinHeader = By.xpath("//h2[contains(.,'Indulge in Austin')]");
	protected By getDetailsBtn = By.xpath("//a[contains(.,'Get Details')]");
	protected By resortMainHeader = By.xpath("//div[contains(text(),'Club Wyndham Resort')]");

	protected By roomTypeHeader = By.xpath("//div[contains(text(),'Suite Type')]");
	protected By totalBedRoom = By.xpath(/* "//div[@class='tabs-panel is-active' and @id='room0']//h6" */
			"//div[@class='tabs-panel is-active' and @id='room0']//div[contains(@class,'subtitle-2')]");
	protected By roomLocation = By
			.xpath("//div[@class='wyn-bedroom-card__detail__row']//div[@class='wyn-type-title-2 wyn-color-black'][2]");

	protected By liveLikeYouMeanHeader = By.xpath("(//div//section//div[@class='videoBanner'])[1]");
	protected By videoBannerHomepage = By.xpath(/* "(//div[@class='video'])[1]" */
			"(//div//section//div[@class='videoBanner'])[1]//div[@class='title-1']");
	protected By videoOpen = By.xpath(/* "(//a[@class='playButton korModalActivate'])[1]" */
			"(//div[@class='videoBanner']//a[@class='playButton korModalActivate'])[1]");
	protected By timeShareVideoOpen = By.xpath(/* "(//a[@class='playButton korModalActivate'])[2]" */
			"(//div[@class='videoBanner']//a[@class='playButton korModalActivate'])[1]");

	protected By openedVideoHeader = By
			.xpath("//div[@class='cell small-11 large-6' and contains(.,'Live Like You Mean It')]");
	/*
	 * .xpath(
	 * "//div[@class='wyn-modal__container']//h1[contains(.,'Live Like You Mean It')]"
	 * );
	 */
	protected By playButton = By.xpath("(//iframe[@class='video'])[1]");
	protected By closeBtn = By.xpath(/* "(//button[@class='close-button'])[1]" */
			"(//div[@id='videoModal']//button[@class='close-button'])[1]");
	protected By timeShareCloseBtn = By
			.xpath("//div[contains(.,'Live Like You Mean It')]//button[@class='close-button']");
	protected By benefitOfOwnershipHeader = By.xpath("//div[contains(text(),'BENEFITS OF OWNERSHIP')]");
	protected By benefitsTitle = By.xpath(
			"//div[contains(text(),'BENEFITS OF OWNERSHIP')]//../../../../..//div[contains(@class,'text small')]/div[contains(@class,'subtitle-1')]");
	protected By benefitsDesc = By.xpath(
			"(//div[contains(text(),'Benefits of ownership')]//..//..//..//..//div[@class='iconList']//ul)[1]//li//p");
	/*
	 * "//h4[contains(.,'Benefits')]/ancestor::div[@class='icon-list']//div[@class='wyn-icon-card__copy']/p"
	 * );
	 */
	protected By discoverLink = By.xpath(
			"((//div[contains(text(),'BENEFITS OF OWNERSHIP')]//..//..//..//..//div[@class='iconList'])[1]//div[contains(@class,'body-1')])[1]");
	/*
	 * "//h4[contains(.,'Benefits')]/ancestor::div[@class='icon-list']//a[contains(.,'Discover the Difference')]"
	 * );
	 */// Why

	protected By travelInspirationTitle = By.xpath("//h2[contains(.,'Travel Inspiration')]");
	protected By totalCardsTI = By.xpath(
			"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='column is-flex wyn-card-list__card is-3-desktop']");
	protected By cardsImageTI = By.xpath(
			"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-card__image']");
	protected By cardsTitleTI = By.xpath(
			"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-title']//h3");
	protected By cardsDescrTI = By.xpath(
			"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-l-content wyn-card__copy']/p");
	protected By cardsReadMoreTI = By.xpath(
			"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='wyn-l-large-wrapper']//div[@class='wyn-card__cta' and contains(.,'Read More')]");
	protected By seeAllTravelInspi = By.xpath("//a[contains(.,'travel inspiration')]");

	protected By travelInspirationBread = By
			.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel Inspiration')]");

	protected By formTitle = By.xpath("//div[contains(@class,'title-2') and contains(text(),'Interested')]");
	protected By formTitlenew = By.xpath("(//form[@id='CWInterestedOwnership']/fieldset/div/div/div/div)[1]");
	protected By formSubTitle = By.xpath("(//form//div[@class='body-1'])[1]");
	protected By formSubTitleold = By.xpath("//form//div[contains(.,'Interested')]");
	protected By firstNameField = By.xpath("//input[@id='firstName']//following-sibling::span");
	protected By firstNameFieldold = By.id("firstName");
	protected By lastNameField = By.xpath("//input[@id='lastName']//following-sibling::span");
	protected By lastNameFieldold = By.id("lastName");
	protected By phoneField = By.xpath(/* "//input[@id='PhoneNumber']//following-sibling::span" */
			"//input[contains(@id,'phoneNumber')]//following-sibling::span");
	protected By phoneFieldold = By.id("phone");
	protected By emailField = By.xpath("//input[@id='email']//following-sibling::span");
	protected By emailFieldold = By.id("email");
	protected By memberNumField = By.xpath("//input[@id='membershipNumber']//following-sibling::span");
	protected By firstNameError = By.xpath("//span[contains(@class,'firstNameError')]");
	protected By lastNameError = By.xpath("//span[contains(@class,'lastNameError')]");
	protected By phoneError = By.xpath(/* "//span[contains(@class,'PhoneNumberError')]" */
			"//span[contains(@class,'phoneNumber')]");
	protected By emailError = By.xpath("//span[contains(@class,'emailError')]");
	protected By memberNumberError = By.xpath("//span[contains(@class,'membershipNumberError')]");
	protected By checkBox = By.xpath("//form//input[@type='checkbox']");
	protected By submitButton = By.xpath("//form//button[contains(.,'SUBMIT')]");
	protected By submitButtonold = By.xpath("//form//button[contains(.,'Submit')]");
	protected By thankConfirm = By.xpath("//div[@class='title-2' and contains(.,'Thank you')]");
	protected By thankConfirmold = By.xpath("//div[@class='wyn-message__content']//h4");
	protected By closeMsg = By.xpath("//span[@class='wyn-modal__close wyn-modal__close--secondary']");
	protected By mainHeaders = By
			.xpath("//ul[contains(@class,'global-navigation')]//li[contains(@class,'dropdown-submenu-parent')]/a");
	protected By loginBtn = By.xpath("(//li[@class='loggedOut']/a)[2]");
	protected By loginPopUp = By.xpath("//div[@class='korContentContainer']//div[@class='grid-x']");
	protected By loginPopUpHdr = By.xpath(
			"//div[@class='korContent']//div//div[contains(@class,'cell large-6')]//div[contains(@class,'title-2')]");
	protected By loginPopUpBtn = By.xpath("//div[@class='korContent']//div//div[contains(@class,'cell large-6')]//a");
	protected By loginPopUpClose = By
			.xpath("//div[@class='korContentContainer']//div[@class='grid-x']//..//..//div[@class='modalExitButton']");
	protected By searchBtn = By.xpath("(//button[contains(.,'Search')])[1]");
	protected By ieSupportBanner = By.xpath("//div[@class='wyn-ie-alert-message__panel']");
	protected By closeieSupport = By.xpath("//a[@id='alertClose']");
	protected By stateSelect = By.xpath("//select[@name='state']");
	protected By stateSelectold = By.xpath("//select[@name='State']");

	protected By globalNavScroll = By.xpath("//header[contains(@class,'main-header')]");

	// protected By suiteSavingsHeader =
	// By.xpath("(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1'
	// and contains(text(),'SUITE')])[1]");
	protected By suiteSavingsHeader = By.xpath(
			"(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1' and contains(text(),'SUITE')])[1]");
	protected By vacationReadyGoHeader = By.xpath(
			"(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1' and contains(text(),'VACATION')])[1]");

	protected By suiteSavingsDesc = By.xpath(
			"(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1' and contains(text(),'SUITE')])[1]//..//div[@class='body-1']/p");
	protected By vacationReadtDesc = By.xpath(
			"(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1' and contains(text(),'VACATION')])[1]//..//div[@class='body-1']/p");

	// protected By suiteSavingsDesc =
	// By.xpath("(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1'])[1]//..//div[@class='body-1']/p");

	protected By bookNowBtn = By.xpath(
			"(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1' and contains(text(),'SUITE')])[1]//..//a");
	protected By learnMoreCTA = By.xpath(
			"(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1' and contains(text(),'VACATION')])[1]//..//a");

	// protected By bookNowBtn =
	// By.xpath("(//div[@class='banner']//div[@class='cardBanner']//div[@class='title-1'])[1]//..//a");

	/******************* Ritam ********************************/

	protected By modalHeader = By
			.xpath("(//h1[contains(.,'GET YOU ON VACATION')] | //div[contains(@id,'loginHeadline')])");
	protected By fieldUsername = By.xpath("//input[@id = 'username']");
	protected By fieldPassword = By.xpath("//input[@id = 'password']");
	protected By loginButton = By.xpath("//button[text()='Login']");
	protected By welcomeHeader = By.xpath("//h4[contains(.,'WELCOME')]");
	protected By feedbackContainer = By.xpath("//div[@id = 'oo_invitation_prompt']");
	protected By FeedbackNoOption = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By logOutCTA = By.xpath("//span[@class='account-navigation__text' and contains(.,'Out')]");
	protected By ownerFullName = By.xpath("//h4[contains(.,'WELCOME')]/parent::div/h2");
	protected By usernameText = By.xpath("//input[@id='username']//following-sibling::span[contains(.,'Username*')]");

	/*
	 * Method: launchAppNextGen Description: Launch NextGen Application: Date
	 * Jun/2019 Author: Unnat Jain Changes By
	 */
	public void launchAppNextGen() {

		driver.navigate().to(testData.get("URL"));
		String urlCheck = testData.get("URL");
		// driver.manage().window().maximize();

		waitUntilElementVisibleBy(driver, nextGenHeroImage, 120);

		if (verifyObjectDisplayed(nextGenHeroImage)) {
			if (urlCheck.toUpperCase().contains("QA")) {
				System.out.println("Testing in QA link");
			} else if (urlCheck.toUpperCase().contains("PROD")) {
				System.out.println("Testing in PROD link");
			} else if (urlCheck.toUpperCase().contains("STAGE")) {
				System.out.println("Testing in Stage link");
			}
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
					"Next Gen Club Wyndham Navigation Successful");

			try {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, acceptButton, 20);
				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("Club Wyndham Homepgage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementJSWithWait(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No Button");
			}

			try {
				List<WebElement> covidClose = driver.findElements(covidAlertClose);
				waitUntilObjectVisible(driver, covidClose.get(0), 20);
				if (covidClose.get(0).isDisplayed()) {
					covidClose.get(0).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("Club Wyndham Homepgage", "closeAlert", Status.PASS,
							"Successfully Closed Alert");
				}

			} catch (Exception e) {
				log.error("Covid Message Not Present");

			}

		} else {
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.FAIL,
					"Next Gen Club Wyndham Navigation Unsuccessful");
		}

	}

	/*
	 * Method: closeAlert Description: Accept Cookie Date: Feb/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	protected By covidAlertClose = By.xpath("//button[contains(@class,'setCookie') and contains(.,'Close')]");

	public void closeAlert() {
		try {
			// WebElement covidClose = getList(covidAlertClose).get(0);
			waitUntilElementVisibleBy(driver, covidAlertClose, 20);
			if (verifyObjectDisplayed(covidAlertClose)) {
				clickElementBy(covidAlertClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("Club Wyndham Homepgage", "closeAlert", Status.PASS,
						"Successfully Closed Alert");
			}
		} catch (Exception e) {
			log.error("Covid Message Not Present");

		}

	}

	/*
	 * Method: navigateToHomepage Description:Navigate To Homepage : Date
	 * Jun/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToHomepage(String strHome) {

		if (strHome.equalsIgnoreCase("logo")) {

			waitUntilElementVisibleBy(driver, NextGenCWLogo, 120);
			tcConfig.updateTestReporter("NextGenLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage via logo");

			if (browserName.equalsIgnoreCase("Firefox")) {
				scrollDownForElementJSBy(NextGenCWLogo);
				scrollUpByPixel(300);
			} else {
				getElementInView(NextGenCWLogo);
			}

			clickElementBy(NextGenCWLogo);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, featuredResortHdr, 120);

			if (verifyObjectDisplayed(nextGenHeroImage) || verifyObjectDisplayed(featuredResortHdr)) {

				tcConfig.updateTestReporter("NextGenLoginPage", "navigateToHomepage", Status.PASS,
						"NextGen CW Navigation Successful");

			} else {
				tcConfig.updateTestReporter("NextGenLoginPage", "navigateToHomepage", Status.FAIL,
						"NextGen CW Navigation Unsuccessful");
			}

		} else if (strHome.equalsIgnoreCase("breadcrumb")) {
			waitUntilElementVisibleBy(driver, NextGenCWLogo, 120);
			tcConfig.updateTestReporter("NextGenLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage via breadcrumb");

			if (verifyObjectDisplayed(homeBreadcrumb)) {
				getElementInView(homeBreadcrumb);
				clickElementJSWithWait(homeBreadcrumb);
			} else if (verifyObjectDisplayed(homeBreadcrumbNav)) {
				getElementInView(homeBreadcrumbNav);
				clickElementJSWithWait(homeBreadcrumbNav);
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilObjectVisible(driver, nextGenHeroImage, 120);

			if (verifyObjectDisplayed(nextGenHeroImage)) {

				tcConfig.updateTestReporter("NextGenLoginPage", "navigateToHomepage", Status.PASS,
						"NextGen CW Navigation Successful");

			} else {
				tcConfig.updateTestReporter("NextGenLoginPage", "navigateToHomepage", Status.FAIL,
						"NextGen CW Navigation Unsuccessful");
			}
		} else {
			tcConfig.updateTestReporter("NextGenLoginPage", "navigateToHomepage", Status.FAIL,
					"Unknown keyword entered, Failed to navigate to homepage");

		}

	}

	/*
	 * Method: searchField Description:Search Filed Validtions : Date Jun/2019
	 * Author: Unnat Jain Changes By
	 */

	protected By searchIcon = By.xpath("//button[@type='submit']/span");

	public void searchField() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, searchIcon, 120);

		if (verifyObjectDisplayed(searchIcon)) {

			tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.PASS,
					"Search Icon Present on Homepage");

			clickElementBy(searchIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchContainer1)) {

				tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.PASS,
						"Search Container displayed with text: "
								+ driver.findElement(searchContainer1).getAttribute("placeholder"));

				driver.findElement(searchContainer1).click();
				driver.findElement(searchContainer1).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.PASS, "Search Keyword Entered");
				/*
				 * waitUntilObjectVisible(driver, searchSuggestion, 120); if
				 * (verifyObjectDisplayed(searchSuggestion)) {
				 * tcConfig.updateTestReporter("NextGenHomePage", "searchField",
				 * Status.PASS, "Search suggestions displayed"); } else {
				 * tcConfig.updateTestReporter("NextGenHomePage", "searchField",
				 * Status.FAIL, "Search suggestions not displayed"); }
				 */

				clickElementJSWithWait(searchButton1);

				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.FAIL,
						"Error in getting the search container");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.FAIL,
					"Error in getting the search Icon");
		}

	}

	/*
	 * Method: resortSearch Description: Resort Search Validations: Date
	 * Jun/2019 Author: Unnat Jain Changes By
	 */
	public void resortSearch() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilObjectVisible(driver, searchIcon2, 120);

		if (verifyObjectDisplayed(searchIcon2)) {

			tcConfig.updateTestReporter("NextGenHomePage", "resortSearch", Status.PASS,
					"Search Icon Present on Homepage");

			clickElementBy(searchIcon2);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchContainer)) {

				tcConfig.updateTestReporter("NextGenHomePage", "resortSearch", Status.PASS,
						"Search Container displayed with text: "
								+ driver.findElement(searchContainer).getAttribute("value"));

				driver.findElement(searchContainer).click();
				driver.findElement(searchContainer).sendKeys(testData.get("strResortSearch"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("NextGenHomePage", "resortSearch", Status.PASS, "Search Keyword Entered");

				if (verifyObjectDisplayed(searchSuggestion)) {
					tcConfig.updateTestReporter("NextGenHomePage", "resortSearch", Status.PASS,
							"Search suggestions displayed");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "resortSearch", Status.FAIL,
							"Search suggestions not displayed");
				}

				clickElementBy(searchButton);

				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "resortSearch", Status.FAIL,
						"Error in getting the search container");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "resortSearch", Status.FAIL,
					"Error in getting the search Icon");
		}

	}

	/*
	 * Method: homepageHeroImageVal Description: Homepage Hero Image
	 * Validations: Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void homepageHeroImageVal() {

		waitUntilElementVisibleBy(driver, heroImageTitle, "ExplicitLongWait");
		List<WebElement> wynHeroTitleList = driver.findElements(heroImageTitle);

		List<WebElement> wynHeroImageList = driver.findElements(nextGenHeroImage);

		if (verifyElementDisplayed(wynHeroTitleList.get(0)) && verifyElementDisplayed(wynHeroImageList
				.get(0)) /* driver.findElement(nextGenHeroImage)) */) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Hero Image present on the homepage with title " + wynHeroTitleList.get(0).getText().trim());
			List<WebElement> wynHeroDescList = driver.findElements(heroImageDesc);
			if (verifyElementDisplayed(wynHeroDescList.get(0))) {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Hero Image description present on the homepage" + wynHeroDescList.get(0).getText().trim());
			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Hero Image description not present on the homepage");
			}
			// content updated

			if (verifyObjectDisplayed(getStartedBtn)) {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Get Started Button present on the hero image");
				getElementInView(getStartedBtn);
				String href = getElementAttribute(getStartedBtn, "href").trim();
				clickElementBy(getStartedBtn);

				pageCheck();

				if (driver.getCurrentUrl().trim().equalsIgnoreCase(href)) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Page Navigated");
				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
							"Deals Page Navigation error");
				}

				driver.navigate().back();

			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Explore Resort Button not present on the hero image");
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Hero Image not present on the homepage");
		}

	}

	/*
	 * Method: homepageDealsNOffersVal Description: Deals and Offers section
	 * validations: Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void homepageDealsNOffersVal() {

		waitUntilElementVisibleBy(driver, dealsNOffersTitle, 120);

		if (verifyObjectDisplayed(dealsNOffersTitle)) {
			scrollDownForElementJSBy(dealsNOffersTitle);

			tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
					"Deals and offers section with title present on the homepage");

			if (verifyObjectDisplayed(totalCardsDO)) {
				List<WebElement> totalCardsList = driver.findElements(totalCardsDO);
				List<WebElement> totalCardsImageList = driver.findElements(cardsImageDO);
				List<WebElement> totalCardsTitleList = driver.findElements(cardsTitleDO);
				List<WebElement> totalCardsDescList = driver.findElements(cardsDescrDO);
				List<WebElement> totalCardsSeeOffList = driver.findElements(cardsSeeOffersDO);

				tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
						"Total Cards in Deals and Offers section is " + totalCardsList.size());

				if (totalCardsList.size() == totalCardsImageList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
							"Image present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.FAIL,
							"Image not present in all cards");
				}

				if (totalCardsList.size() == totalCardsTitleList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
							"Title present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.FAIL,
							"Title not present in all cards");
				}

				if (totalCardsList.size() == totalCardsDescList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
							"Description present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.FAIL,
							"Description not present in all cards");
				}

				if (totalCardsList.size() == totalCardsSeeOffList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
							"See Offers present in all cards");

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
							"Clicking on first Card with title " + totalCardsTitleList.get(0).getText());

					String firstTitle = totalCardsTitleList.get(0).getText().toUpperCase();
					totalCardsSeeOffList.get(0).click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains(driver.getTitle().toUpperCase())) {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.PASS,
								"Clicked Resort page navigated" + driver.getTitle());

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.FAIL,
								"Clicked resort page navigation error, title " + driver.getTitle());
					}

					driver.navigate().back();
					// closeNextGenPopUp();
					waitUntilObjectVisible(driver, dealsNOffersTitle, 120);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.FAIL,
							"See Offers not present in all cards");
				}

				waitUntilObjectVisible(driver, seeAllDeals, 120);
				if (verifyObjectDisplayed(seeAllDeals)) {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.PASS,
							"See all deals and offers link present on homepage");

					clickElementJSWithWait(seeAllDeals);

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains(testData.get("AllDeals").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.PASS,
								"Navigated to All deals and offers page");

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.FAIL,
								"All deals and offers page navigation failed");
					}
					driver.navigate().back();
					// closeNextGenPopUp();
					waitUntilObjectVisible(driver, dealsNOffersTitle, 120);

					// driver.navigate().to("https://stage-clubwyndham.wyndhamdestinations.com/");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.FAIL,
							"See all deals and offers link not present on homepage");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.FAIL,
						"Cards not present in Deals and offers page");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "homepageDealsNOffersVal", Status.FAIL,
					"Deals and offers section with title not present on the homepage");
		}

	}

	protected By welcomeToClubHeader = By
			.xpath("(//div[contains(@class,'caption-1') and contains(text(),'WELCOME TO THE CLUB')])[2]");
	protected By welcomeToTheClubImage = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'WELCOME TO THE CLUB')])[2]/../../../../div[@class='banner']//img");
	protected By welcomeToTheClubHeader = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'WELCOME TO THE CLUB')])[2]/../../../../div[@class='banner']//div[contains(@class,'blockCard')]//div[contains(@class,'title-1')]");
	protected By welcomeToTheClubSubHeader = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'WELCOME TO THE CLUB')])[2]/../../../../div[@class='banner']//div[contains(@class,'blockCard')]//div[contains(@class,'subtitle-3')]");
	protected By welcomeDescription = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'WELCOME TO THE CLUB')])[2]/../../../../div[@class='banner']//div[contains(@class,'blockCard')]//div[contains(@class,'body-1')]");
	protected By welcomeLearnMoreCTA = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'WELCOME TO THE CLUB')])[2]/../../../../div[@class='banner']//div[contains(@class,'blockCard')]//a");

	public void validateWelcomeTotheClubSection() {
		waitUntilElementVisibleBy(driver, welcomeToClubHeader, 120);

		getElementInView(welcomeToClubHeader);
		if (verifyObjectDisplayed(welcomeToClubHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.PASS,
					"Welcome To The Club header present in homepage");

			if (verifyObjectDisplayed(welcomeToTheClubImage)) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.PASS,
						"Welcome To The Club Image present in homepage");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.FAIL,
						"Welcome To The Club Image not present in homepage");
			}

			if (verifyObjectDisplayed(welcomeToTheClubHeader)) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.PASS,
						"Ready Set Go Header present in homepage");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.FAIL,
						"Ready Set Go Header not present in homepage");
			}

			if (verifyObjectDisplayed(welcomeToTheClubSubHeader)) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.PASS,
						"Ready Set Go Sub Header present in homepage");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.FAIL,
						"Ready Set Go Sub Header not present in homepage");
			}

			if (verifyObjectDisplayed(welcomeDescription)) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.PASS,
						"Ready Set Go Description present in homepage");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.FAIL,
						"Ready Set Go Description not present in homepage");
			}

			if (verifyObjectDisplayed(welcomeLearnMoreCTA)) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.PASS,
						"GetStarted button is present in homepage");
				clickElementBy(welcomeLearnMoreCTA);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.getCurrentUrl().toUpperCase().contains(testData.get("header3").toUpperCase())) {
					tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.PASS,
							"Navigated to Respective Page");
					driver.navigate().back();
				}
				waitUntilObjectVisible(driver, welcomeLearnMoreCTA, 120);

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.FAIL,
						"GetStarted button is not present in homepage");
			}
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "validateWelcomeTotheClubSection", Status.FAIL,
					"Welcome to The Club Header is not present in the Homepage");
		}
	}

	/*
	 * Method: homepageDestinationGuidesVal Descriptio;n: Destinations Guides
	 * Validations : Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void homepageDestinationGuidesVal() {

		waitUntilObjectVisible(driver, destinationGuidesTitle, 120);

		if (verifyObjectDisplayed(destinationGuidesTitle)) {

			scrollDownForElementJSBy(destinationGuidesTitle);

			tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
					"Destination Guides section with title present on the homepage");

			if (verifyObjectDisplayed(totalCardsDG)) {
				List<WebElement> totalCardsList = driver.findElements(totalCardsDG);
				List<WebElement> totalCardsImageList = driver.findElements(cardsImageDG);
				List<WebElement> totalCardsTitleList = driver.findElements(cardsTitleDG);
				List<WebElement> totalCardsDescList = driver.findElements(cardsDescrDG);
				List<WebElement> totalCardsReadMoreList = driver.findElements(cardsReadMoreDG);

				tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
						"Total Cards in Destination Guides section is " + totalCardsList.size());

				if (totalCardsList.size() == totalCardsImageList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
							"Image present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
							"Image not present in all cards");
				}

				if (totalCardsList.size() == totalCardsTitleList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
							"Title present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
							"Title not present in all cards");
				}

				if (totalCardsList.size() == totalCardsDescList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
							"Description present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
							"Description not present in all cards");
				}

				if (totalCardsList.size() == totalCardsReadMoreList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
							"Read More present in all cards");

					scrollDownForElementJSWb(totalCardsTitleList.get(0));
					scrollUpByPixel(300);
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
							"Clicking on first Card with title " + totalCardsTitleList.get(0).getText());

					String firstTitle = totalCardsTitleList.get(0).getText().toUpperCase();
					totalCardsReadMoreList.get(0).click();

					waitUntilObjectVisible(driver, featDestBC, 120);
					if (verifyObjectDisplayed(featDestBC)) {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
								"Clicked Resort page navigated" + driver.getTitle());

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
								"Clicked resort page navigation error, title " + driver.getTitle());
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, destinationGuidesTitle, 120);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
							"Read More not present in all cards");
				}

				getElementInView(seeAllDestinations);

				if (verifyObjectDisplayed(seeAllDestinations)) {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.PASS,
							"See all Destination Guides link present on homepage");

					clickElementJSWithWait(seeAllDestinations);

					// ArrayList<String> tabs2 = new
					// ArrayList<String>(driver.getWindowHandles());
					// driver.switchTo().window(tabs2.get(1));

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getCurrentUrl().toUpperCase().contains(testData.get("AllDestinations").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.PASS,
								"Navigated to All Destination Guides page");

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.FAIL,
								"All Destination Guides page navigation failed");
					}

					/*
					 * waitForSometime(tcConfig.getConfig().get("LowWait"));
					 * driver.close();
					 * waitForSometime(tcConfig.getConfig().get("MedWait"));
					 * 
					 * driver.switchTo().window(tabs2.get(0));
					 */
					driver.navigate().back();

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
							"See all Destination Guides link not present on homepage");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
						"Cards not present in Destination Guides page");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "homepageDestinationGuidesVal", Status.FAIL,
					"Deals and offers section with title not present on the homepage");
		}

	}

	/*
	 * Method: hpFeaturedResortVal Description: Featured Resorts Validations:
	 * Date Jun/2019 Author: Unnat Jain Changes By
	 */

	protected By slideNextButton = By.xpath("(//div[@id='homepageFeaturedResort']//..//div[@class='flexed'])[2]");
	protected By featuredResortCardImage = By.xpath("//div[@class='column']//div[@class='cardComponent']//img");
	protected By featuredResortName = By.xpath(
			"//div[@class='column']//div[@class='cardComponent']//div[@class='card-section']//div[contains(@class,'subtitle-2')]");
	protected By featuredResortLocationName = By.xpath(
			"//div[@class='column']//div[@class='cardComponent']//div[@class='card-section']//div[contains(@class,'subtitle-3')]");
	protected By featuredResortLocationDescription = By.xpath(
			"//div[@class='column']//div[@class='cardComponent']//div[@class='card-section']//div[contains(@class,'body-1')]/p");
	protected By featuredResortCTA = By.xpath(
			"//div[@class='column']//div[@class='cardComponent']//div[@class='card-section']//div[contains(@class,'body-1-link')]");

	protected By featuredResortImage = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'FEATURED RESORTS')]//following::div[contains(@class,'blockBannerLeft')]//img)[1]");
	protected By featuredResortHeader = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'FEATURED RESORTS')]//following::div[contains(@class,'blockCard')]//div[contains(@class,'title-1')])[1]");
	protected By featuredResortLocation = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'FEATURED RESORTS')]//following::div[contains(@class,'blockCard')]//div[contains(@class,'subtitle-3')])[1]");
	protected By featuredResortDesc = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'FEATURED RESORTS')]//following::div[contains(@class,'blockCard')]//div[contains(@class,'body-1')])[1]");
	protected By exploreResortButton = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'FEATURED RESORTS')]//following::div[contains(@class,'blockCard')]//a)[1]");
	protected By resortCard = By.xpath("//div[contains(@class,'resort-cards')]");

	public void hpFeaturedResortVal() {

		waitUntilElementVisibleBy(driver, featuredResortsHeader, "ExplicitLongWait");

		scrollDownForElementJSBy(featuredResortsHeader);

		if (verifyObjectDisplayed(featuredResortsHeader)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Featured Resort section with title present on the homepage");

			if (verifyObjectDisplayed(seeAllResorts)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"See all Resorts link present on homepage");

				clickElementJSWithWait(seeAllResorts);

				pageCheck();
				assertTrue(verifyObjectDisplayed(resortCard), "All Resorts page navigation failed");
				driver.navigate().back();
				pageCheck();
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"See all Resorts link not present on homepage");
			}

			waitUntilElementVisibleBy(driver, featuredResortsHeader, "ExplicitLongWait");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(featuredResortImage)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Image Present on the homepage");

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Image not  Present on the homepage");
			}

			if (verifyObjectDisplayed(featuredResortHeader)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Header Present on the homepage");

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Header not  Present on the homepage");
			}

			if (verifyObjectDisplayed(featuredResortLocation)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Location Present on the homepage");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
						"Location not  Present on the homepage");
			}

			if (verifyObjectDisplayed(featuredResortDesc)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Description Present on the homepage");

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Description not  Present on the homepage");
			}

			getElementInView(exploreResortButton);
			if (verifyObjectDisplayed(exploreResortButton)) {
				String href = getElementAttribute(exploreResortButton, "href").trim();
				clickElementJSWithWait(exploreResortButton);
				waitUntilElementVisibleBy(driver, resortsPageBreadCrumb, "ExplicitLongWait");
				if (driver.getCurrentUrl().contains(href)) {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
							"Navigate to Resort Page");
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, exploreResortButton, "ExplicitLongWait");

				} else {
					tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
							"Page Navigation is Unsuccessfull");
				}

			}

			List<WebElement> totalCards = driver.findElements(featuredResortCardImage);
			List<WebElement> strResortName = driver.findElements(featuredResortName);
			List<WebElement> strResortLocationName = driver.findElements(featuredResortLocationName);
			List<WebElement> strResortLocationDesc = driver.findElements(featuredResortLocationDescription);
			List<WebElement> btnFeaturedResortCTA = driver.findElements(featuredResortCTA);
			for (int i = 0; i < totalCards.size(); i++) {
				assertTrue(verifyObjectDisplayed(totalCards.get(i)), "Image is not Displayed");
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Image Present on the homepage");
				assertTrue(getElementText(strResortName.get(i)).length() > 0, "Resort Name is not Displayed");
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Resort name Present on the homepage: " + getElementText(featuredResortName));
				assertTrue(getElementText(strResortLocationName.get(i)).length() > 0, "Location name is not Displayed");
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Location Name Present on the homepage:" + getElementText(featuredResortLocationName));
				assertTrue(getElementText(strResortLocationDesc.get(i)).length() > 0, "Description is not Displayed");
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Description Present on the homepage:" + getElementText(featuredResortLocationDescription));
				assertTrue(verifyObjectDisplayed(btnFeaturedResortCTA.get(i)), "Explore Resort CTA is not Displayed");
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"CTA Present on the homepage");

			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Featured Resort section with title present on the homepage");
		}

	}

	/*
	 * Method: validateSuiteSavings Description: Suite Savings Banner
	 * validations: Date Jun/2020 Author: Priya Das Changes By
	 */

	public void validateSuiteSavings() {
		try {
			waitUntilElementVisibleBy(driver, suiteSavingsHeader, 120);

			getElementInView(suiteSavingsHeader);
			if (verifyObjectDisplayed(suiteSavingsHeader)) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateSuiteSavings", Status.PASS,
						"Suite Savings header present in homepage");
				if (verifyObjectDisplayed(suiteSavingsDesc)) {
					tcConfig.updateTestReporter("NextGenHomePage", "validateSuiteSavings", Status.PASS,
							"Suite Savings Description present in homepage");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "validateSuiteSavings", Status.FAIL,
							"Suite Savings Description not present in homepage");
				}

				if (verifyObjectDisplayed(bookNowBtn)) {
					tcConfig.updateTestReporter("NextGenHomePage", "validateSuiteSavings", Status.PASS,
							"Book Now button is present in homepage");
					clickElementBy(bookNowBtn);
					ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tab.get(1));
					if (driver.getCurrentUrl().toUpperCase().contains(testData.get("header1").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenHomePage", "validateSuiteSavings", Status.PASS,
								"Navigated to Respective Page");
						driver.close();
					}
					driver.switchTo().window(tab.get(0));
					waitUntilObjectVisible(driver, heroImageTitle, 120);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "validateSuiteSavings", Status.FAIL,
							"Book Now button is not present in homepage");
				}
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "hpIndulgeAustinVal", Status.FAIL,
						"Suite Saving Header is not present in the Homepage");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHomePage", "hpIndulgeAustinVal", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	public void validateVacationReadyGo() {
		try {
			waitUntilElementVisibleBy(driver, vacationReadyGoHeader, 120);

			getElementInView(vacationReadyGoHeader);
			if (verifyObjectDisplayed(vacationReadyGoHeader)) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateVacationReadyGo", Status.PASS,
						"Vacation Ready Go header present in homepage");
				if (verifyObjectDisplayed(vacationReadtDesc)) {
					tcConfig.updateTestReporter("NextGenHomePage", "validateVacationReadyGo", Status.PASS,
							"Vacation Ready Go Description present in homepage");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "validateVacationReadyGo", Status.FAIL,
							"Vacation Ready Go Description not present in homepage");
				}

				if (verifyObjectDisplayed(learnMoreCTA)) {
					tcConfig.updateTestReporter("NextGenHomePage", "validateVacationReadyGo", Status.PASS,
							"Learn More button is present in homepage");
					clickElementBy(learnMoreCTA);
					if (driver.getCurrentUrl().toUpperCase().contains(testData.get("header2").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenHomePage", "validateVacationReadyGo", Status.PASS,
								"Navigated to Respective Page");
						driver.navigate().back();
						waitUntilObjectVisible(driver, learnMoreCTA, 120);

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "validateVacationReadyGo", Status.FAIL,
								"Navigated to Respective Page Failed");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "validateSuiteSavings", Status.FAIL,
							"Book Now button is not present in homepage");
				}
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "hpIndulgeAustinVal", Status.FAIL,
						"Suite Saving Header is not present in the Homepage");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHomePage", "hpIndulgeAustinVal", Status.FAIL,
					"Failed in Method" + e.getMessage());
		}
	}

	/*
	 * Method: hpIndulgeAustinVal Description: Indulge in Austin Validations:
	 * Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void hpIndulgeAustinVal() {

		waitUntilElementVisibleBy(driver, indulgeAustinHeader, 120);

		getElementInView(indulgeAustinHeader);

		if (verifyObjectDisplayed(indulgeAustinHeader)) {

			tcConfig.updateTestReporter("NextGenHomePage", "hpIndulgeAustinVal", Status.PASS,
					"Indulge in Austin header present in homepage");

			if (verifyObjectDisplayed(getDetailsBtn)) {
				tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
						"Get Details Btn present");

				clickElementBy(getDetailsBtn);

				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (driver.getCurrentUrl().toUpperCase().contains("AUSTIN")) {
					tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
							"Navigated to Austin page");

					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
							"Austin page navigation failed");
				}

				driver.navigate().back();

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
						"See all Resorts link not present on homepage");
			}
			waitUntilElementVisibleBy(driver, indulgeAustinHeader, 120);

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "hpIndulgeAustinVal", Status.FAIL,
					"Indulge in Austin header not present in homepage");

		}

	}

	/*
	 * Method: videoFunctionalityVal Description: Homepage Video Functionality
	 * Vals: Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void videoFunctionalityVal() {

		waitUntilElementVisibleBy(driver, liveLikeYouMeanHeader, 120);

		getElementInView(videoBannerHomepage);

		if (verifyObjectDisplayed(videoBannerHomepage)) {

			tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
					"Video banner Header present on homepage");

			if (verifyObjectDisplayed(videoOpen)) {
				tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
						"Video present in the section");

				clickElementBy(videoOpen);

				/*
				 * waitUntilElementVisibleBy(driver, openedVideoHeader, 120);
				 * 
				 * if (verifyObjectDisplayed(openedVideoHeader)) {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "videoFunctionalityVal", Status.PASS,
				 * "Header present in the section"); } else {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "videoFunctionalityVal", Status.FAIL,
				 * "Header not present in the section"); }
				 */

				WebElement eleVideo = driver.findElement(By.xpath("(//iframe[@class='video'])[1]"));
				driver.switchTo().frame(eleVideo);

				driver.switchTo().defaultContent();

				if (verifyObjectDisplayed(closeBtn)) {
					tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
							"Close Video Button present");
					clickElementBy(closeBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, videoBannerHomepage, 120);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.FAIL,
							"Close Video Button not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
						"Video not  present in the section");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.FAIL,
					"Live Like You Mean It Header not present on homepage");

		}

	}

	public void videoFunctionalityHomePage() {

		waitUntilElementVisibleBy(driver, liveLikeYouMeanHeader, 120);

		getElementInView(liveLikeYouMeanHeader);

		if (verifyObjectDisplayed(liveLikeYouMeanHeader)) {

			tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
					"Video banner Header present on homepage");

			if (verifyObjectDisplayed(timeShareVideoOpen)) {
				tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
						"Video present in the section");

				clickElementBy(timeShareVideoOpen);

				waitUntilElementVisibleBy(driver, openedVideoHeader, 120);

				if (verifyObjectDisplayed(openedVideoHeader)) {
					tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
							"Header present in the section");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.FAIL,
							"Header not present in the section");
				}

				WebElement eleVideo = driver.findElement(By.xpath("(//iframe[@class='video'])[1]"));
				driver.switchTo().frame(eleVideo);

				driver.switchTo().defaultContent();

				if (verifyObjectDisplayed(timeShareCloseBtn)) {
					tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
							"Close Video Button present");
					clickElementBy(timeShareCloseBtn);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitUntilElementVisibleBy(driver, videoBannerHomepage, 120);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.FAIL,
							"Close Video Button not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
						"Video not  present in the section");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.FAIL,
					"Live Like You Mean It Header not present on homepage");

		}

	}

	/*
	 * Method: benefitOfOwnerShipVal Description: Benefits of ownership val:
	 * Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void benefitOfOwnerShipVal() {

		waitUntilElementVisibleBy(driver, benefitOfOwnershipHeader, 120);

		getElementInView(benefitOfOwnershipHeader);

		if (verifyObjectDisplayed(benefitOfOwnershipHeader)) {

			tcConfig.updateTestReporter("NextGenHomePage", "benefitOfOwnerShipVal", Status.PASS,
					"Benefits of ownerships Header present on homepage");

			List<WebElement> benefitsTitleList = driver.findElements(benefitsTitle);
			tcConfig.updateTestReporter("NextGenHomePage", "benefitOfOwnerShipVal", Status.PASS,
					"Total " + benefitsTitleList.size() + " title present in benefits of ownerships");

			for (int i = 0; i < benefitsTitleList.size(); i++) {

				tcConfig.updateTestReporter("NextGenHomePage", "benefitOfOwnerShipVal", Status.PASS,
						"Title " + (i + 1) + " " + benefitsTitleList.get(i).getText());

			}

			if (verifyObjectDisplayed(discoverLink)) {
				tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
						"Discover the difference link present");

				clickElementBy(discoverLink);

				waitForSometime(tcConfig.getConfig().get("LongWait"));
				System.out.println(driver.getTitle().toUpperCase());
				if (driver.getTitle().toUpperCase().contains(testData.get("benefitsOfOwnerships").toUpperCase())) {
					tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
							"Navigated to Associated page");

					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
							"Associate page navigation failed");
				}

				driver.navigate().back();

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
						"Discover the difference link not present");
			}
			waitUntilElementVisibleBy(driver, benefitOfOwnershipHeader, 120);

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "benefitOfOwnerShipVal", Status.FAIL,
					"Benefits of ownerships Header not present on homepage");
		}

	}

	/*
	 * Method: travelInspirationVal Description: Travel Inspiration Validations:
	 * Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void travelInspirationVal() {

		waitUntilElementVisibleBy(driver, travelInspirationTitle, 120);

		if (verifyObjectDisplayed(travelInspirationTitle)) {
			getElementInView(travelInspirationTitle);

			tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
					"Travel Inspirations section with title present on the homepage");

			if (verifyObjectDisplayed(totalCardsTI)) {
				List<WebElement> totalCardsList = driver.findElements(totalCardsTI);
				List<WebElement> totalCardsImageList = driver.findElements(cardsImageTI);
				List<WebElement> totalCardsTitleList = driver.findElements(cardsTitleTI);
				List<WebElement> totalCardsDescList = driver.findElements(cardsDescrTI);
				List<WebElement> totalCardsReadMoreList = driver.findElements(cardsReadMoreTI);

				tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
						"Total Cards in Travel Inspirations section is " + totalCardsList.size());

				if (totalCardsList.size() == totalCardsImageList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
							"Image present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
							"Image not present in all cards");
				}

				if (totalCardsList.size() == totalCardsTitleList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
							"Title present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
							"Title not present in all cards");
				}

				if (totalCardsList.size() == totalCardsDescList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
							"Description present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
							"Description not present in all cards");
				}

				if (totalCardsList.size() == totalCardsReadMoreList.size()) {

					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
							"Read More present in all cards");

					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
							"Clicking on first Card with title " + totalCardsTitleList.get(0).getText());

					totalCardsReadMoreList.get(0).click();

					waitUntilElementVisibleBy(driver, travelInspirationBread, 50);
					if (verifyObjectDisplayed(travelInspirationBread)) {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
								"Clicked Resort page navigated" + driver.getTitle());

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
								"Clicked resort page navigation error, title " + driver.getTitle());
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, travelInspirationTitle, 120);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
							"Read More not present in all cards");
				}

				if (verifyObjectDisplayed(seeAllTravelInspi)) {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
							"See all Travel Inspirations link present on homepage");

					clickElementBy(seeAllTravelInspi);

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains(testData.get("AllTravelInsp").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.PASS,
								"Navigated to All Travel Inspirations page");

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
								"All Travel Inspirations page navigation failed");
					}

					// driver.navigate().back();

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
							"See all Travel Inspirations link not present on homepage");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
						"Cards not present in Travel Inspirations page");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "homepageTravelInspirationsVal", Status.FAIL,
					"Deals and offers section with title not present on the homepage");
		}

	}

	/*
	 * Method: headerValidations Description: Homepage Header validations: Date
	 * Jun/2019 Author: Unnat Jain Changes By
	 */

	// protected By covidAlertClose =
	// By.xpath("//button[contains(@class,'setCookie') and
	// contains(.,'Close')]");
	protected By homepageText = By.xpath("//div[@id='loginHeadline']");

	public void headerValidations() {

		waitUntilElementVisibleBy(driver, searchBtn, 120);

		List<WebElement> mainNavList = driver.findElements(mainHeaders);

		for (int i = 0; i < mainNavList.size(); i++) {

			if (mainNavList.get(i).getText().toUpperCase()
					.contains(testData.get("header" + (i + 1) + "").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
						"Header No " + (i + 1) + " is present as " + (mainNavList.get(i).getText()));
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL, "Header No " + (i + 1)
						+ " is not present  " + (testData.get("header" + (i + 1) + "").toUpperCase()));
			}

		}

		if (verifyObjectDisplayed(loginBtn)) {
			tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
					"Login Button present in the header");

			clickElementBy(loginBtn);

			waitUntilElementVisibleBy(driver, homepageText, 120);

			if (verifyObjectDisplayed(homepageText)) {
				/*
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "headerValidations", Status.PASS,
				 * "Login Button pop up opened"); List<WebElement> headers =
				 * driver.findElements(loginPopUpHdr); List<WebElement>
				 * headersBtn = driver.findElements(loginPopUpBtn);
				 * 
				 * if (headers.size() == 2) {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "headerValidations", Status.PASS,
				 * "Login Pop Up Headers present"); } else {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "headerValidations", Status.FAIL,
				 * "Login Pop Up Headers not present"); }
				 * 
				 * if (headersBtn.size() == 2) {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "headerValidations", Status.PASS,
				 * "Login Pop Up Headers CTA present"); } else {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "headerValidations", Status.FAIL,
				 * "Login Pop Up Headers CTA not present"); }
				 * 
				 * if (verifyObjectDisplayed(loginPopUpClose)) {
				 * clickElementBy(loginPopUpClose);
				 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else
				 * { tcConfig.updateTestReporter("NextGenHomePage",
				 * "headerValidations", Status.FAIL,
				 * "Login Pop Up close CTA not present"); }
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHomePage",
				 * "headerValidations", Status.FAIL,
				 * "Login Button pop up not opened"); }
				 */

				tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
						"Login page is displayed successfully");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
					"Login Button not present in the header");
		}

		driver.navigate().back();
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, searchBtn, 120);
		if (verifyObjectDisplayed(searchBtn)) {
			tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
					"Search Button present in the header");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
					"Search Button not present in the header");
		}

	}

	/*
	 * Method: incorrectFormVal Description: Homepage Incorrect form
	 * validations: Date Jun/2019 Author: Unnat Jain Changes By
	 */
	public void incorrectFormVal() {

		waitUntilElementVisibleBy(driver, formTitle, 120);
		getElementInView(formTitle);
		if (verifyObjectDisplayed(formTitle)) {

			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(formTitle).trim());
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle).trim());

				waitUntilObjectVisible(driver, firstNameField, 120);
				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameField);
						e.sendKeys(testData.get("firstNameWrongData") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {

						waitForSometime(tcConfig.getConfig().get("MedWait"));
						fieldDataEnterAction(firstNameField, testData.get("firstNameWrongData") + Keys.TAB);
					}

					waitUntilObjectVisible(driver, firstNameError, 120);
					if (verifyObjectDisplayed(firstNameError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"First Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"First Name Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}
				waitUntilObjectVisible(driver, lastNameField, 120);
				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					fieldDataEnterAction(lastNameField, testData.get("lastNameWrongData") + Keys.TAB);
					waitUntilObjectVisible(driver, lastNameError, 120);
					if (verifyObjectDisplayed(lastNameError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Last Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Last Name Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}
				waitUntilObjectVisible(driver, phoneField, 120);
				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(phoneField, testData.get("phoneWrongData") + Keys.TAB);
					waitUntilObjectVisible(driver, phoneError, 120);
					if (verifyObjectDisplayed(phoneError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Phone Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Phone Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}
				waitUntilObjectVisible(driver, emailField, 120);
				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(emailField, testData.get("emailWrongData") + Keys.TAB);
					waitUntilObjectVisible(driver, emailError, 120);
					if (verifyObjectDisplayed(emailError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Email Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Email Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	protected By needHelpFormHdr = By.xpath(/* "//form/div[contains(.,'Need Help')]" */
			"//div[contains(@class,'title-2') and contains(text(),'Ownership Concerns? Get Solutions.')]|//div[contains(@class,'title-2') and contains(text(),'Need Help')]");
	protected By submitBtnDisabled = By.xpath("//button[contains(@class,'formSubmit disabled')]");

	public void incorrectFormValCares() {

		waitUntilElementVisibleBy(driver, needHelpFormHdr, 120);
		getElementInView(needHelpFormHdr);
		if (verifyObjectDisplayed(needHelpFormHdr)) {

			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(needHelpFormHdr).trim());
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle).trim());
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*
				 * if (verifyObjectDisplayed(firstNameField)) {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.PASS, "First Name Field Present");
				 * 
				 * if (browserName.equalsIgnoreCase("EDGE")) { WebElement e =
				 * driver.findElement(firstNameField);
				 * e.sendKeys(testData.get("firstNameWrongData") + Keys.TAB);
				 * driver.switchTo().activeElement().sendKeys(Keys.TAB); } else
				 * {
				 * 
				 * fieldDataEnterAction(firstNameField,
				 * testData.get("firstNameWrongData") + Keys.TAB); }
				 * 
				 * waitUntilObjectVisible(driver, firstNameError, 120); if
				 * (verifyObjectDisplayed(firstNameError)) {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.PASS, "First Name Error Present");
				 * } else { tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.FAIL,
				 * "First Name Error not Present"); }
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.FAIL,
				 * "First Name Field not present"); }
				 */
				/*
				 * waitUntilObjectVisible(driver, lastNameField, 120); if
				 * (verifyObjectDisplayed(lastNameField)) {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.PASS, "Last Name Field Present");
				 * 
				 * fieldDataEnterAction(lastNameField,
				 * testData.get("lastNameWrongData") + Keys.TAB);
				 * waitUntilObjectVisible(driver, lastNameError, 120); if
				 * (verifyObjectDisplayed(lastNameError)) {
				 * tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.PASS, "Last Name Error Present");
				 * } else { tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.FAIL,
				 * "Last Name Error not Present"); }
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHomePage",
				 * "incorrectFormVal", Status.FAIL,
				 * "Last Name Field not present"); }
				 */
				waitUntilObjectVisible(driver, phoneField, 120);
				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(phoneField, testData.get("phoneWrongData") + Keys.TAB);
					waitUntilObjectVisible(driver, phoneError, 120);
					if (verifyObjectDisplayed(phoneError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Phone Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Phone Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}
				waitUntilObjectVisible(driver, emailField, 120);
				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(emailField, testData.get("emailWrongData") + Keys.TAB);
					waitUntilObjectVisible(driver, emailError, 120);
					if (verifyObjectDisplayed(emailError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Email Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Email Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				waitUntilObjectVisible(driver, memberNumField, 120);
				if (verifyObjectDisplayed(memberNumField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Member Number Field Present");

					fieldDataEnterAction(memberNumField, testData.get("memberNumberWrongData") + Keys.TAB);
					waitUntilObjectVisible(driver, memberNumberError, 120);
					if (verifyObjectDisplayed(memberNumberError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Member Number Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Member Number Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Member Number Field not present");
				}

				try {
					List<WebElement> checkBoxList = driver.findElements(checkBox);
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Total " + checkBoxList.size() + " terms and condition chechkbox present in the page");

					for (int i = 0; i < checkBoxList.size(); i++) {
						clickElementJSWithWait(checkBoxList.get(i));
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"T&C checkbox error");
				}

				waitUntilObjectVisible(driver, submitBtnDisabled, 120);
				getElementInView(submitBtnDisabled);
				assertTrue(verifyObjectDisplayed(submitBtnDisabled), "Submit button is not Disabled");
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
						"Submit Button is Disabled");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	public void incorrectFormValold() {

		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			getElementInView(formTitle);
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formSubTitleold)) {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitleold));

				if (verifyObjectDisplayed(firstNameFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameFieldold);
						e.sendKeys(testData.get("firstNameWrongData") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {

						fieldDataEnter(firstNameFieldold, testData.get("firstNameWrongData"));
					}

					if (verifyObjectDisplayed(firstNameError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"First Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"First Name Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(lastNameFieldold, testData.get("lastNameWrongData"));

					if (verifyObjectDisplayed(lastNameError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Last Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Last Name Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(phoneFieldold, testData.get("phoneWrongData"));

					if (verifyObjectDisplayed(phoneError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Phone Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Phone Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(emailFieldold, testData.get("emailWrongData"));

					if (verifyObjectDisplayed(emailError)) {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
								"Email Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Email Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	/*
	 * Method: formValidations Description: Form validations on homepage: Date
	 * Jun/2019 Author: Unnat Jain Changes By
	 */
	public void formValidations() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			scrollDownForElementJSBy(formTitle);
			tcConfig.updateTestReporter("NextGenHomePage", "formValidations", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHomePage", "formValidations", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameField);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnterAction(firstNameField, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnterAction(lastNameField, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(phoneField, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Email Field Present");

					fieldDataEnterAction(emailField, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(stateSelect)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"State Drop Down present");
					new Select(driver.findElement(stateSelect)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(submitButton) || verifyObjectDisplayed(submitButtonold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Submit button present");

					if (verifyObjectDisplayed(submitButton))
						clickElementJSWithWait(submitButton);
					else
						clickElementJSWithWait(submitButtonold);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, thankConfirm, 120);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Form Submission Successful");

					/*
					 * if (verifyObjectDisplayed(closeMsg)) {
					 * clickElementBy(closeMsg); } else {
					 * tcConfig.updateTestReporter("NextGenHomePage",
					 * "incorrectFormVal", Status.FAIL, "Close Btn not present"
					 * ); }
					 */

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	public void formValidationsnew() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, formTitlenew, 120);

		if (verifyObjectDisplayed(formTitlenew)) {
			scrollDownForElementJSBy(formTitlenew);
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(formTitlenew));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameField);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnterAction(firstNameField, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnterAction(lastNameField, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(phoneField, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Email Field Present");

					fieldDataEnterAction(emailField, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(stateSelect)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"State Drop Down present");
					new Select(driver.findElement(stateSelect)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(submitButton) || verifyObjectDisplayed(submitButtonold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Submit button present");

					if (verifyObjectDisplayed(submitButton))
						clickElementJSWithWait(submitButton);
					else
						clickElementJSWithWait(submitButtonold);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, thankConfirm, 120);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Form Submission Successful");

					/*
					 * if (verifyObjectDisplayed(closeMsg)) {
					 * clickElementBy(closeMsg); } else {
					 * tcConfig.updateTestReporter("NextGenHomePage",
					 * "incorrectFormVal", Status.FAIL, "Close Btn not present"
					 * ); }
					 */

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	public void formValidationsold() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			getElementInView(formTitle);
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formSubTitleold)) {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitleold));

				if (verifyObjectDisplayed(firstNameFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameFieldold);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnterAction(firstNameFieldold, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnterAction(lastNameFieldold, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(phoneFieldold, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailFieldold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(emailFieldold, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(stateSelectold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"State Drop Down present");
					new Select(driver.findElement(stateSelectold)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(submitButtonold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButtonold);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitUntilElementVisibleBy(driver, thankConfirmold, 50);

				if (verifyObjectDisplayed(thankConfirmold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Form Submission Successful");

					if (verifyObjectDisplayed(closeMsg)) {
						clickElementBy(closeMsg);
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
								"Close Btn not present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	/*
	 * Method: urlNavigations Description: Launch NextGen Old URLs and check
	 * redirects: Date Aug/2019 Author: Unnat Jain Changes By
	 */

	public void urlNavigations(String strBrowser, int tempLoop) {
		this.driver = checkAndInitBrowser(strBrowser);
		for (int i = 0; i < tempLoop; i++) {
			String oldUrl = testData.get("oldURL" + (i + 1));
			String redirectUrl = testData.get("redirectURL" + (i + 1));
			if (oldUrl.contains("qa-")) {
				if (testData.get("URL").toUpperCase().contains("//CLUBWYNDHAM")
						|| testData.get("URL").toUpperCase().contains("WWW")) {
					oldUrl = oldUrl.replace("qa-", "");
					redirectUrl = redirectUrl.replace("qa-", "");
				} else if (testData.get("URL").toUpperCase().contains("Stage")) {
					oldUrl = oldUrl.replace("qa-", "stage-");
					redirectUrl = redirectUrl.replace("qa-", "stage-");
				} else {
					System.out.println("QA ENV");
				}
			} else {
				System.out.println("Direct Redirect");
			}
			driver.navigate().to(oldUrl);
			// String urlCheck = testData.get("URL");
			driver.manage().window().maximize();

			waitUntilElementVisibleBy(driver, NextGenCWLogo, 120);

			if (driver.getCurrentUrl().toUpperCase().contains(redirectUrl.toUpperCase())) {

				tcConfig.updateTestReporter("NextGenHomePage", "urlNavigations", Status.PASS,
						"Navigated to correct URL" + driver.getCurrentUrl());
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "urlNavigations", Status.FAIL,
						"Did not navigate to correct URL" + driver.getCurrentUrl());
			}

		}

	}

	/*
	 * Method: globalNavScroll Description: Global nav scroll check redirects:
	 * Date Nov/2019 Author: Unnat Jain Changes By
	 */

	public void globalNavScroll() {

		pageCheck();
		if (verifyObjectDisplayed(globalNavScroll)) {
			tcConfig.updateTestReporter("NextGenHomePage", "globalNavScroll", Status.PASS,
					"Global Nav is sticky when scrolled");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "globalNavScroll", Status.FAIL,
					"Global Nav is not sticky when scrolled");
		}
		driver.switchTo().activeElement().sendKeys(Keys.HOME);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/************** Ritam **************************/

	/*
	 * Method: setUserName Description: Set Username with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setUserName() throws Exception {
		waitUntilElementVisibleBy(driver, fieldUsername, 120);
		getElementInView(modalHeader);
		// Username entry
		String strUserName = testData.get("CUI_username");
		fieldDataEnter(fieldUsername, strUserName);
		tcConfig.updateTestReporter("CUILoginPage", "setUserName", Status.PASS,
				"Username is entered for CUI Login as " + strUserName);
	}

	/*
	 * Method: setPassword Description: Set Password with correct data Date:
	 * Feb/2020 Author: Unnat Jain Changes By: NA
	 */
	public void setPassword() throws Exception {
		// password data
		String password = testData.get("CUI_password");
		// decodeed password
		String decodedPassword = decodePassword(password);
		fieldDataEnter(fieldPassword, decodedPassword);
		tcConfig.updateTestReporter("CUILoginPage", "setPassword", Status.PASS, "Entered password for CUI login ");
	}

	/*
	 * Method: loginCTAClick Description: Click On login cta Date: Feb/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void loginCTAClick() throws Exception {
		waitUntilElementVisibleBy(driver, loginButton, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(loginButton)) {
			clickElementBy(loginButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			acceptAlert();
			checkFeedbackContainer();
			waitUntilElementVisibleBy(driver, welcomeHeader, 120);
			getElementInView(welcomeHeader);

			Assert.assertTrue(verifyObjectDisplayed(welcomeHeader));

			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.PASS, "User Login successful");

		} else {
			tcConfig.updateTestReporter("CUILoginPage", "loginCTAClick", Status.FAIL,
					"Login button is disabled even after providing username and password");
		}

	}

	/*
	 * Method: checkFeedbackContainer Description: Feedbac No Date: Apr/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void checkFeedbackContainer() {
		try {
			waitUntilElementVisibleBy(driver, feedbackContainer, 120);
			if (verifyObjectDisplayed(feedbackContainer)) {
				getObject(FeedbackNoOption).click();

			}
		} catch (Exception e) {
			log.error("No feedback Container");
		}
	}

	/*
	 * Method: logOutApplicationViaDashboard Description: Account Log out via
	 * dasboard page Validations Date: Apr/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void logOutApplicationViaDashboard() {
		navigateToURL(tcConfig.getConfig().get("NewCUILogOutUrl"));
		waitUntilElementVisibleBy(driver, ownerFullName, 120);
		Assert.assertTrue(verifyObjectDisplayed(ownerFullName));
		if (getList(logOutCTA).size() == 1) {
			getElementInView(getList(logOutCTA).get(0));
			clickElementBy(getList(logOutCTA).get(0));
		} else if (getList(logOutCTA).size() == 3) {
			getElementInView(getList(logOutCTA).get(2));
			clickElementBy(getList(logOutCTA).get(2));
		}
		waitUntilElementVisibleBy(driver, usernameText, 120);
		tcConfig.updateTestReporter("CUILoginPage", "logOutApplication", Status.PASS, "User Logged Out");
	}

	protected By homeBreadCrumb = By.xpath("//div[contains(@class,'breadcrumb')]//a[contains(text(),'Timeshare.com')]");

	public void clickHomeBreadcrumb() {
		getElementInView(homeBreadCrumb);
		waitUntilObjectVisible(driver, homeBreadCrumb, 120);
		if (verifyObjectDisplayed(homeBreadCrumb)) {
			tcConfig.updateTestReporter("TimeshareHomePage", "clickHomeBreadcrumb", Status.PASS,
					"Timeshare Breadcrumb is displayed");
			clickElementJSWithWait(homeBreadCrumb);
		} else {
			tcConfig.updateTestReporter("TimeshareHomePage", "clickHomeBreadcrumb", Status.FAIL,
					"Timeshare Breadcrumb is not displayed");
		}
	}

	protected By panoRamaHomeBreadCrumb = By.xpath("//div[contains(@class,'breadcrumb')]//a[contains(text(),'Home')]");

	public void clickHomeBreadcrumbPanoRama() {
		getElementInView(panoRamaHomeBreadCrumb);
		waitUntilObjectVisible(driver, panoRamaHomeBreadCrumb, 120);
		if (verifyObjectDisplayed(panoRamaHomeBreadCrumb)) {
			tcConfig.updateTestReporter("TimeshareHomePage", "clickHomeBreadcrumb", Status.PASS,
					"PanoRamaco Breadcrumb is displayed");
			clickElementJSWithWait(panoRamaHomeBreadCrumb);
		} else {
			tcConfig.updateTestReporter("TimeshareHomePage", "clickHomeBreadcrumb", Status.FAIL,
					"PanoRamaCo Breadcrumb is not displayed");
		}
	}

	protected By MGVCHomeBreadCrumb = By.xpath("//div[contains(@class,'breadcrumb')]//a[contains(text(),'en')]");

	public void clickHomeBreadcrumbMGVC() {
		getElementInView(MGVCHomeBreadCrumb);
		waitUntilObjectVisible(driver, MGVCHomeBreadCrumb, 120);
		if (verifyObjectDisplayed(MGVCHomeBreadCrumb)) {
			tcConfig.updateTestReporter("TimeshareHomePage", "clickHomeBreadcrumb", Status.PASS,
					"MGVC Breadcrumb is displayed");
			clickElementJSWithWait(MGVCHomeBreadCrumb);
		} else {
			tcConfig.updateTestReporter("TimeshareHomePage", "clickHomeBreadcrumb", Status.FAIL,
					"MGVC Breadcrumb is not displayed");
		}
	}

	protected By homepageTitle = By
			.xpath("//div[@aria-hidden='true']//div[contains(@class,'title-1') and contains(text(),'Discover')]");
	protected By timeshareLogo = By
			.xpath("//a[img[contains(@class,'logo')] and parent::div[contains(@class,'large')]]");
	protected By panoramacoLogo = By
			.xpath("(//a[img[contains(@class,'logo')] and parent::div[contains(@class,'small')]])[1]");
	protected By mgvcHeader = By.xpath("(//div[contains(@class,'MGVCHeader')])[1]");

	public void validateTimeshareLogo() {
		pageCheck();
		assertTrue(verifyObjectDisplayed(timeshareLogo), "Header Timeshare logo is not present");
		tcConfig.updateTestReporter("TimeshareHomePage", "validateLogo", Status.PASS,
				"Header Timeshare logo is displayed");

	}

	public void validatePanoramacoLogo() {
		assertTrue(verifyObjectDisplayed(panoramacoLogo), "Header Panoramaco logo is not present");
		tcConfig.updateTestReporter("PanoramacoHomePage", "validateLogo", Status.PASS,
				"Header Panoramaco logo is displayed");

	}

	public void validateMGVCLogo() {
		assertTrue(verifyObjectDisplayed(mgvcHeader), "Header MGVC logo is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateLogo", Status.PASS, "Header MGVC logo is displayed");

	}

	protected By heroBennerImage = By
			.xpath("//section[@class='banner']//div[contains(@class,'title-1') and contains(text(),'TIMESHARE')]");

	public void launchAppTimeshare() {

		driver.navigate().to(testData.get("URL"));
		String urlCheck = testData.get("URL");
		// driver.manage().window().maximize();
		/*
		 * waitForSometime(tcConfig.getConfig().get("LongWait"));
		 * waitUntilElementVisibleBy(driver, heroBennerImage, 240);
		 * 
		 * if (verifyElementDisplayed(driver.findElement(heroBennerImage))) {
		 * tcConfig.updateTestReporter("DestinationsLoginPage",
		 * "launchApplication", Status.PASS,
		 * "URL has been launched successfully");
		 * 
		 * } else { tcConfig.updateTestReporter("DestinationsLoginPage",
		 * "launchApplication", Status.FAIL, "Not able to launch the URL"); }
		 */

	}

	public void logOutApplication() {
		// TODO Auto-generated method stub

	}

	protected By signInButton = By.xpath(
			"//div[contains(@class,'global-navigation')]//li[contains(@class,'loggedOut')]/a[contains(text(),'Sign In')]");

	public void clickSignInButton() {
		waitUntilElementVisibleBy(driver, signInButton, 120);
		if (verifyElementDisplayed(driver.findElement(signInButton))) {
			tcConfig.updateTestReporter("NextGenHomePage", "clickSignInButton", Status.PASS,
					"Sign In button is displayed");
			clickElementBy(signInButton);
		}
	}

	public void clickHamburger() {
		// TODO Auto-generated method stub

	}

	public void clickSignInButtonMobile() {
		// TODO Auto-generated method stub
	}

	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("html")) {
					System.out.println("URL contians html");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it.");
				 * report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components",
							"brokenLinkValidation", Status.FAIL, "[Response Code: " + respCode + "] [Response message: "
									+ respMessage + "] [URL:" + url + "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}

	public void launchApplicationforBrokenLinkValidation() {
		driver.get(tcConfig.getTestData().get("URL"));

	}

	/*
	 * Method: launchPointsCalculator Description: Launch points calculator
	 * Application: Date Jan/2021 Author: Saket Sharma Changes By
	 */
	public void launchAppPointsCalculator() {
		/*
		 * ((JavascriptExecutor) driver).executeScript("window.open()");
		 * ArrayList<String> tabs = new
		 * ArrayList<String>(driver.getWindowHandles());
		 * driver.switchTo().window(tabs.get(1));
		 */
		driver.navigate().to(testData.get("URL"));
		String urlCheck = testData.get("URL");
		// driver.manage().window().maximize();

		waitUntilElementVisibleBy(driver, modalHeader, 120);

		if (verifyObjectDisplayed(modalHeader)) {
			if (urlCheck.toUpperCase().contains("QA")) {
				System.out.println("Testing in QA link");
			} else if (urlCheck.toUpperCase().contains("PROD")) {
				System.out.println("Testing in PROD link");
			} else if (urlCheck.toUpperCase().contains("STAGE")) {
				System.out.println("Testing in Stage link");
			}
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
					"Points calculator Navigation Successful");

			try {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, acceptButton, 20);
				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("Club Wyndham Homepgage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementJSWithWait(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No Button");
			}

		} else {
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.FAIL,
					"Next Gen Club Wyndham Navigation Unsuccessful");
		}

	}
}
