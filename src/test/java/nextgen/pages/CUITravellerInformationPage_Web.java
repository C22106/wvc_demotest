package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUITravellerInformationPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUITravellerInformationPage_Web.class);

	public CUITravellerInformationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By headerReservationBalance = By.xpath(
			"//h1[contains(.,'Reservation Balance')] | //div[@class = 'steps__title' and contains(.,'Reservation Balance')]");
	protected By headerPointProtection = By.xpath(
			"//h1[contains(.,'Points Protection')] | //div[@class = 'steps__title' and contains(.,'Points Protection')]");
	protected By headerCompleteBooking = By.xpath(
			"//h1[contains(.,'Complete Booking')] | //div[@class = 'steps__title' and contains(.,'Complete Booking')]");
	protected By travelerGuest = By.xpath("//label[@for='traveler_guest']");
	protected By bookingHeader = By.xpath("//div[@class='booking-header__desktop']//li[@class='step -active']/span");
	protected By headerTravelerInfo = By.xpath("//h1[contains(.,'Traveler Info')]");
	protected By textTravelInfo = By.xpath("//h1[text()='Traveler Info']");
	protected By textBookDetails = By.xpath("//span[text() = 'Reservation Details']");
	protected By travelerEmailField = By.xpath("//input[@id = 'travelerEmail']");
	protected By travelerNameField = By.id("travelerName");
	protected By buttonContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By TermsAndConditionsCheckBx = By.xpath("//label[@for='discovery-agreement-checkbox']");
	protected By fieldGuestFirstName = By.xpath("//label[@for='guestFirstName']");
	protected By fieldGuestLastName = By.xpath("//label[@for='guestLastName']");
	protected By fieldGuestEmail = By.xpath("//label[@for='guestEmail']");
	protected By fieldGuestPhone = By.xpath("//label[@for='guestPhone']");
	protected By fieldGuestAddress = By.xpath("//label[@for='guestAddress']");
	protected By fieldGuestCity = By.xpath("//label[@for='guestCity']");
	protected By fieldGuestZip = By.xpath("//label[@for='guestZip']");
	protected By guestAgree = By.xpath("//label[@for='agreeGuestIs21']");
	protected By fieldGuestCountry = By.id("guestCountry");
	protected By fieldGuestState = By.id("guestState");
	protected By headerGuestConfirmationCredits = By.xpath("//h1[text()='Guest Confirmation Credits']");
	protected By labelRequired = By
			.xpath("//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Required')]");
	protected By valueRequired = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Required')]/following-sibling::td");
	protected By labelAvailable = By
			.xpath("//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Available')]");
	protected By valueTotalCost = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/ancestor::div//th[contains(text(),'Total Cost')]/following-sibling::td");
	protected By labelTotalCost = By
			.xpath("//h1[text()='Guest Confirmation Credits']/ancestor::div//th[contains(text(),'Total Cost')]");

	protected By valueAvailable = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/parent::div//th[contains(text(),'Available')]/following-sibling::td");
	protected By instructionTxtGuestConfirmationCredits = By.xpath(
			"//h1[text()='Guest Confirmation Credits']/parent::div//p[contains(text(),'In order to have a guest')]");
	protected By selectGuestChkBox = By.xpath("//label[@for='traveler_guest']");
	protected By guestFirstName = By.id("guestFirstName");
	protected By guestLastName = By.id("guestLastName");
	protected By guestEmail = By.id("guestEmail");
	protected By guestPhn = By.id("guestPhone");
	protected By guestAddress = By.id("guestAddress");
	protected By guestCity = By.id("guestCity");
	protected By guestState = By.id("guestState");
	protected By guestZip = By.id("guestZip");
	protected By agreeGuest21ChkBox = By.xpath("//label[@for='agreeGuestIs21']");
	protected By discoveryChkBox = By.xpath("//label[@for='discovery-agreement-checkbox']");
	protected By totalUnitcost = By.xpath("//span[contains(.,'PTS') and not(contains(@class,'strikethrough'))]");;
	protected By buttonBookingContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By wrongGuestFName = By.xpath("//p[@id='helper-guestFirstName']");
	protected By wrongGuestLName = By.xpath("//p[@id='helper-guestLastName']");
	protected By wrongGuestEmail = By.xpath("//p[@id='helper-guestEmail']");
	protected By wrongGuestAddress = By.xpath("//p[@id='helper-guestAddress']");
	protected By wrongGuestCity = By.xpath("//p[@id='helper-guestCity']");
	protected By wrongGuestZip = By.xpath("//p[@id='helper-guestZip']");
	protected By wrongGuestPhn = By.xpath("//p[@id='helper-guestPhone']");
	protected By errorMessageFirstName = By.xpath("//p[@id='helper-guestFirstName' and contains(text(),'This is a required field.')]");
	protected By errorMessageLastName = By.xpath("//p[@id='helper-guestLastName' and contains(text(),'This is a required field.')]");
	protected By errorMessageEmail = By.xpath("//p[@id='helper-guestEmail' and contains(text(),'This is a required field.')]");
	protected By errorMessagePhone = By.xpath("//p[@id='helper-guestPhone' and contains(text(),'This is a required field.')]");
	protected By errorMessageAddress = By.xpath("//p[@id='helper-guestAddress' and contains(text(),'This is a required field.')]");
	protected By errorMessageCity = By.xpath("//p[@id='helper-guestCity' and contains(text(),'This is a required field.')]");
	protected By errorMessagZipCode = By.xpath("//p[@id='helper-guestZip' and contains(text(),'This is a required field.')]");
	
	
	protected static String prepopulatedEmail;
	
	/*
	 * Method: enterGuestInfo Description:Veriying the first step of the booking
	 * header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyErrorGuestInfo() {
		waitUntilElementVisibleBy(driver, fieldGuestFirstName, 120);

		clickElementBy(fieldGuestFirstName);
		sendKeyboardKeys(Keys.TAB);
		elementPresenceVal(errorMessageFirstName, "Error Message on First Name", "Guest Info Page", "verifyErrorGuestInfo");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fieldGuestLastName);
		sendKeyboardKeys(Keys.TAB);
		elementPresenceVal(errorMessageLastName, "Error Message on Last Name", "Guest Info Page", "verifyErrorGuestInfo");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fieldGuestEmail);
		sendKeyboardKeys(Keys.TAB);
		elementPresenceVal(errorMessageEmail, "Error Message on Email", "Guest Info Page", "verifyErrorGuestInfo");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fieldGuestPhone);
		sendKeyboardKeys(Keys.TAB);
		elementPresenceVal(errorMessagePhone, "Error Message on Phone Number", "Guest Info Page", "verifyErrorGuestInfo");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fieldGuestAddress);
		sendKeyboardKeys(Keys.TAB);
		elementPresenceVal(errorMessageAddress, "Error Message on Address", "Guest Info Page", "verifyErrorGuestInfo");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fieldGuestCity);
		sendKeyboardKeys(Keys.TAB);
		elementPresenceVal(errorMessageCity, "Error Message on City", "Guest Info Page", "verifyErrorGuestInfo");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementBy(fieldGuestZip);
		sendKeyboardKeys(Keys.TAB);
		elementPresenceVal(errorMessagZipCode, "Error Message on Zip Code", "Guest Info Page", "verifyErrorGuestInfo");
	}

	/*
	 * Method: checkDiscoveryTermsChkBox Description:check Discovery Terms ChkBox
	 * Information Date field Date: June/2020 Author: Monideep Roy Changes By: NA
	 */
	/*
	 * Method: wrongGuestInfo Description: Enter wrong guest information 
	 * Information Date field Date: January/2021 Author: Umashankar Pati Changes By: NA
	 */
	public void wrongGuestInfo(){
		clickElementBy(selectGuestChkBox);
		waitUntilElementVisibleBy(driver, guestFirstName, 120);
		Assert.assertTrue(verifyObjectDisplayed(fieldGuestFirstName));
		String guestFirstName = testData.get("wrongGuestFN");
		String guestLastName = testData.get("wrongGuestLN");
		String guestEmail = testData.get("wrongGuestEmailID");
		String guestPhoneNumber = testData.get("wrongGuestPhone");
		clickElementBy(fieldGuestAddress);
		clickElementBy(fieldGuestCity);
		String guestState = testData.get("guestStateDropDown");
		String guestZipCode = testData.get("wrongGuestZip");

		fieldDataEnter(fieldGuestFirstName, guestFirstName);
		fieldDataEnter(fieldGuestLastName, guestLastName);
		fieldDataEnter(fieldGuestEmail, guestEmail);
		fieldDataEnter(fieldGuestPhone, guestPhoneNumber);
		selectByText(fieldGuestState, guestState);
		fieldDataEnter(fieldGuestZip, guestZipCode);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestInfo", Status.PASS,
				"Wrong Guest Information entered");
		
	}
	/*
	 * Method: wrongGuestErrorVal Description:Validate wrong guest information error message
	 * Information Date field Date: January/2021 Author: Umashankar Pati Changes By: NA
	 */
	public void wrongGuestErrorVal(){
		waitUntilElementVisibleBy(driver, wrongGuestFName, 120);
		Assert.assertTrue(verifyObjectDisplayed(wrongGuestFName));
		getElementInView(wrongGuestFName);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestErrorVal",
				Status.PASS, "Guest first name entered is invalid");
		
		waitUntilElementVisibleBy(driver, wrongGuestLName, 120);
		Assert.assertTrue(verifyObjectDisplayed(wrongGuestLName));
		getElementInView(wrongGuestLName);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestErrorVal",
				Status.PASS, "Guest last name entered is invalid");
		
		waitUntilElementVisibleBy(driver, wrongGuestEmail, 120);
		Assert.assertTrue(verifyObjectDisplayed(wrongGuestEmail));
		getElementInView(wrongGuestEmail);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestErrorVal",
				Status.PASS, "Guest email ID entered is invalid");
		
		waitUntilElementVisibleBy(driver, wrongGuestPhn, 120);
		Assert.assertTrue(verifyObjectDisplayed(wrongGuestPhn));
		getElementInView(wrongGuestPhn);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestErrorVal",
				Status.PASS, "Guest phone number entered is invalid");
		
		waitUntilElementVisibleBy(driver, wrongGuestAddress, 120);
		Assert.assertTrue(verifyObjectDisplayed(wrongGuestAddress));
		getElementInView(wrongGuestAddress);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestErrorVal",
				Status.PASS, "Guest address entered is invalid");
		
		waitUntilElementVisibleBy(driver, wrongGuestCity, 120);
		Assert.assertTrue(verifyObjectDisplayed(wrongGuestCity));
		getElementInView(wrongGuestCity);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestErrorVal",
				Status.PASS, "Guest city entered is invalid");
		
		
		waitUntilElementVisibleBy(driver, wrongGuestZip, 120);
		Assert.assertTrue(verifyObjectDisplayed(wrongGuestZip));
		getElementInView(wrongGuestZip);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "wrongGuestErrorVal",
				Status.PASS, "Guest zip entered is invalid");
	}
	/*
	 * Method: continueBtnDisabled Description:Validate continue Btn is disabled
	 * Information Date field Date: January/2021 Author: Umashankar Pati Changes By: NA
	 */
	public void continueBtnDisabled(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertFalse(driver.findElement(buttonBookingContinue).isEnabled());
		tcConfig.updateTestReporter("CUITravellerInformationPage", "continueBtnDisabled",
				Status.PASS, getElementText(buttonBookingContinue) + " Continue Button is disabled");
	}
	
	public void checkDiscoveryTermsChkBox() {

		clickElementBy(discoveryChkBox);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "checkDiscoveryTermsChkBox", Status.PASS,
				"Discovery terms and agreement check box has been checked");
	}

	/*
	 * Method: fillTravellerInformation Description:Validate and fill Traveller
	 * Information Date field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void fillTravellerInformation() {
		String emailtraveller = testData.get("TravellerEmailID");
		waitUntilElementVisibleBy(driver, textBookDetails, 120);
		Assert.assertTrue(verifyObjectDisplayed(textBookDetails));
		if (verifyObjectDisplayed(textTravelInfo)) {
			getElementInView(travelerEmailField);
			if (verifyObjectDisplayed(travelerEmailField)) {
				clickElementBy(travelerEmailField);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clearElementBy(travelerEmailField);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				fieldDataEnter(travelerEmailField, emailtraveller);
				tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.PASS,
						"Fields successfully entered in Default Traveller Information Page");
			} else {
				tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.FAIL,
						"Email field not present");
			}

		} else {
			tcConfig.updateTestReporter("CUITravellerInformationPage", "fillTravelerInformation", Status.FAIL,
					"Traveller Information Header Not present");
		}
	}

	/*
	 * Method: validateDefaultUserName Description:Validate default username matches
	 * account details Date field Date: June/2020 Author: Monideep Roychowdhury Roy
	 * Changes By: NA
	 */
	public void validateDefaultUserName() {

		waitUntilElementVisibleBy(driver, textBookDetails, 120);
		Assert.assertTrue(verifyObjectDisplayed(textBookDetails));

		Select nameSelect = new Select(getObject(travelerNameField));
		Assert.assertEquals(nameSelect.getFirstSelectedOption().getText().trim(),
				tcConfig.getTestData().get("OwnerNameFull"), "Primary Owner name not selected by default");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "validateDefaultUserName", Status.PASS,
				"Primary Owner name not selected by default");

	}

	/*
	 * Method: selectAndPopulateGuestInfo Description:selectAndPopulateGuestInfo
	 * Date: June/2020 Author: Monideep Roychowdhury Roy Changes By: NA
	 */

	public void selectAndPopulateGuestInfo() {

		clickElementBy(selectGuestChkBox);
		waitUntilElementVisibleBy(driver, guestFirstName, 120);

		sendKeysBy(guestFirstName, "GuestFirst");
		sendKeysBy(guestLastName, "GuestLast");
		tcConfig.getTestData().put("GuestName", "GuestFirst GuestLast");
		sendKeysBy(guestEmail, "guest.email@gmail.com");
		tcConfig.getTestData().put("GuestEmail", "guest.email@gmail.com");
		sendKeysBy(guestPhn, "9962001374");
		sendKeysBy(guestAddress, "44 Center Grove Road");
		sendKeysBy(guestCity, "Randolph");
		selectByText(guestState, "New Jersey");
		sendKeysBy(guestZip, "07869");
		clickElementBy(agreeGuest21ChkBox);
	}

	/*
	 * Method: clickContinueBooking Description:click Continue Booking Date field
	 * Date: June/2020 Author: Kamalesh Roy Changes By: NA
	 */
	public void clickContinueBooking() {
		clickContinueBtn(buttonContinue, "CUITravellerInformationPage");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		pageCheck();
		Assert.assertTrue(verifyObjectDisplayed(headerReservationBalance)
				|| verifyObjectDisplayed(headerPointProtection) || verifyObjectDisplayed(headerCompleteBooking));
		tcConfig.updateTestReporter("clickContinueBooking", "clickContinueBooking", Status.PASS,
				"Successfully navigated to Next Page");
	}

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button
	 * 
	 */
	public void clickContinueBtn(By by, String pageName) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(by);
		if (verifyObjectDisplayed(by) && verifyObjectEnabled(by)) {
			clickElementJSWithWait(by);
			tcConfig.updateTestReporter(pageName, "clickContinueButton", Status.PASS,
					"Continue Button is enabled and displayed successfully and it is clicked");
		} else {
			tcConfig.updateTestReporter(pageName, "clickContinueButton", Status.FAIL,
					"Continue Button is not dispalyed");
		}
	}

	/*
	 * Method: verifyPageHeaderAndBookingHeaderForTravelerInfo Description:Veriying
	 * the first step of the booking header Date field Date: June/2020 Author:
	 * Kamalesh Changes By: NA
	 */
	public void verifyFirstStep() {
		waitUntilElementVisibleBy(driver, headerTravelerInfo, 120);
		Assert.assertTrue(verifyObjectDisplayed(headerTravelerInfo));
		String headerValue = getElementText(bookingHeader);
		if (headerValue.contains("TRAVELER INFO") && getElementText(headerTravelerInfo).contains("1. TRAVELER INFO")) {
			tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyFirstStep", Status.PASS,
					"The First step of the booking header is " + headerValue + " and the page header is "
							+ getElementText(headerTravelerInfo));
			;
		} else {
			tcConfig.updateTestReporter("CUITravellerInformationPage",
					"verifyPageHeaderAndBookingHeaderForTravelerInfo", Status.FAIL,
					headerValue + " page not displayed");
		}

	}

	/*
	 * Method: verifyOwnerNamePrepopulated Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */

	public void verifyOwnerNamePrepopulated() {
		waitUntilElementVisibleBy(driver, travelerNameField, 120);
		prepopulatedEmail = getElementAttribute(travelerNameField, "value").trim();
		Assert.assertTrue(verifyObjectDisplayed(travelerNameField) && getElementAttribute(travelerNameField, "value")
				.trim().equalsIgnoreCase(CUIAccountSettingPage_Web.strMemberName));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyOwnerNamePrepopulated", Status.PASS,
				"Owner Name Field is present and is prepopulated with value "
						+ getElementAttribute(travelerNameField, "value").trim());
	}

	/*
	 * Method: verifyOwnerNamePrepopulatedSingleOwner Description:Veriying the first
	 * step of the booking header Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */

	public void verifyOwnerNamePrepopulatedSingleOwner() {
		waitUntilElementVisibleBy(driver, travelerNameField, 120);
		prepopulatedEmail = getElementAttribute(travelerNameField, "value").trim();
		Assert.assertTrue(verifyObjectDisplayed(travelerNameField)
				&& getElementAttribute(travelerNameField, "readOnly").equals("true"));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyOwnerNamePrepopulated", Status.PASS,
				"Owner Name Field is present and is prepopulated with value for Single Owner and it is not editable "
						+ getElementAttribute(travelerNameField, "value").trim());
	}

	/*
	 * Method: verifyOwnerEmailPrepopulated Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyOwnerEmailPrepopulated() {
		waitUntilElementVisibleBy(driver, travelerEmailField, 120);
		Assert.assertTrue(verifyObjectDisplayed(travelerEmailField) && getElementAttribute(travelerEmailField, "value")
				.trim().equalsIgnoreCase(CUIAccountSettingPage_Web.strOwnerEmail));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyOwnerEmailPrepopulated", Status.PASS,
				"Owner Email Address Field is present and is prepopulated with value "
						+ getElementAttribute(travelerEmailField, "value").trim());

	}

	/*
	 * Method: verifyGuestOptionNotPresent Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyGuestOptionNotPresent() {
		waitUntilElementVisibleBy(driver, travelerEmailField, 120);
		Assert.assertFalse(verifyObjectDisplayed(travelerGuest), "Guest Option is displayed on the traveler page");
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyGuestOptionNotPresent", Status.PASS,
				"Guest Option is not displayed on the traveler page");
	}

	/*
	 * Method: verifyGuestOptionNotPresent Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void selectGuestOption() {
		waitUntilElementVisibleBy(driver, travelerGuest, 120);
		Assert.assertTrue(verifyObjectDisplayed(travelerGuest));
		getElementInView(travelerGuest);
		clickElementBy(travelerGuest);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyGuestOptionNotPresent", Status.PASS,
				"Guest Option is displayed on the traveler page");
	}

	/*
	 * Method: selectTermsAndConditionsCheckBx Description:Veriying the first step
	 * of the booking header Date field Date: June/2020 Author: Kamalesh Changes By:
	 * NA
	 */
	public void selectTermsAndConditionsCheckBx() {
		waitUntilElementVisibleBy(driver, TermsAndConditionsCheckBx, 120);
		Assert.assertTrue(verifyObjectDisplayed(TermsAndConditionsCheckBx));
		clickElementBy(TermsAndConditionsCheckBx);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "selectTermsAndConditionsCheckBx", Status.PASS,
				"Terms and Conditions Check Box is selected");

	}

	/*
	 * Method: selectAnotherValueFromDropdown Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void selectAnotherValueFromDropdown() {
		Select select = new Select(driver.findElement(travelerNameField));
		java.util.List<WebElement> options = select.getOptions();
		for (int i = 0; i < options.size() - 1; i++) {
			if (options.get(i).getText().equalsIgnoreCase(prepopulatedEmail)) {
				options.remove(i);
			}
		}
		select.selectByVisibleText(options.get(options.size() - 1).getText());
		Assert.assertFalse(getElementAttribute(travelerEmailField, "value").trim().isEmpty());
		tcConfig.updateTestReporter("CUITravellerInformationPage", "selectAnotherValueFromDropdown", Status.PASS,
				"Different owner is selected");
	}

	/*
	 * Method: verifyLabelAndTextGuestConfirmationCredits Description:Veriying the
	 * first step of the booking header Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void verifyLabelAndTextGuestConfirmationCredits() {
		waitUntilElementVisibleBy(driver, headerGuestConfirmationCredits, 120);
		Assert.assertTrue(verifyObjectDisplayed(headerGuestConfirmationCredits));
		getElementInView(headerGuestConfirmationCredits);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Guest Confirmation Credits section present");

		Assert.assertTrue(verifyObjectDisplayed(labelRequired) && verifyObjectDisplayed(valueRequired));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Required Label and Value is present");

		Assert.assertTrue(verifyObjectDisplayed(labelAvailable) && verifyObjectDisplayed(valueAvailable));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Available Label and Value is present");

		Assert.assertTrue(verifyObjectDisplayed(instructionTxtGuestConfirmationCredits));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Instruction Text is present on the Guest Confirmation Credits section");
	}

	/*
	 * Method: verifyCreditRateSingleOwner Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyCreditRateSingleOwner() {
		Assert.assertTrue(verifyObjectDisplayed(labelTotalCost) && verifyObjectDisplayed(valueTotalCost));
		tcConfig.updateTestReporter("CUITravellerInformationPage", "verifyLabelAndTextGuestConfirmationCredits",
				Status.PASS, "Total Cost Label and Value is present");

	}

	/*
	 * Method: checkRelativeAndAvailableValue Description:Veriying the first step of
	 * the booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void checkRequiredAndAvailableValue(String Owner) {
		switch (Owner) {
		case "single-Owner":
			Assert.assertTrue(getElementText(valueAvailable).equals("0") && getElementText(valueRequired).equals("1"));
			tcConfig.updateTestReporter("CUITravellerInformationPage", "checkRequiredAndAvailableValue", Status.PASS,
					"Available value is 0 and Required Value is 1");

			break;

		case "multi-Owner":
			Assert.assertTrue(getElementText(valueRequired).equals("1") && Integer
					.parseInt(getElementText(valueAvailable)) > Integer.parseInt(getElementText(valueRequired)));
			tcConfig.updateTestReporter("CUITravellerInformationPage", "checkRequiredAndAvailableValue", Status.PASS,
					"Guest has enough credit to apply");
			break;
		}

	}

	/*
	 * Method: enterGuestInfo Description:Veriying the first step of the booking
	 * header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void enterGuestInfo() {
		driver.navigate().refresh();
		clickElementBy(selectGuestChkBox);
		waitUntilElementVisibleBy(driver, fieldGuestFirstName, 120);
		Assert.assertTrue(verifyObjectDisplayed(fieldGuestFirstName));
		String guestFirstName = testData.get("guestFirstName");
		String guestLastName = testData.get("guestLastName");
		String guestEmail = testData.get("guestEmail");
		String guestPhoneNumber = testData.get("guestPhoneNumber");
		String guestCountry = testData.get("guestCountry");
		String guestStreetAddress = testData.get("guestStreetAddress");
		String guestCity = testData.get("guestCity");
		String guestState = testData.get("guestState");
		String guestZipCode = testData.get("guestZipCode");

		fieldDataEnter(fieldGuestFirstName, guestFirstName);
		fieldDataEnter(fieldGuestLastName, guestLastName);
		fieldDataEnter(fieldGuestEmail, guestEmail);
		fieldDataEnter(fieldGuestPhone, guestPhoneNumber);
		selectByText(fieldGuestCountry, guestCountry);
		fieldDataEnter(fieldGuestAddress, guestStreetAddress);
		fieldDataEnter(fieldGuestCity, guestCity);
		selectByText(fieldGuestState, guestState);
		fieldDataEnter(fieldGuestZip, guestZipCode);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "enterGuestInfo", Status.PASS,
				"Guest Information entered successfully");
	}

	/*
	 * Method: selectAgreeCheckBxGuestConfirmationCredits Description:Veriying the
	 * first step of the booking header Date field Date: June/2020 Author: Kamalesh
	 * Changes By: NA
	 */
	public void selectAgreeCheckBxGuestConfirmationCredits() {
		waitUntilElementVisibleBy(driver, guestAgree, 120);
		Assert.assertTrue(verifyObjectDisplayed(guestAgree));
		clickElementBy(guestAgree);
		tcConfig.updateTestReporter("CUITravellerInformationPage", "selectAgreeCheckBxGuestConfirmationCredits",
				Status.PASS, getElementText(guestAgree) + " Check Box is selected");

	}

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(buttonBookingContinue);
		clickElementJSWithWait(buttonBookingContinue);
	}

}