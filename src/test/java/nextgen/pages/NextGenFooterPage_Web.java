package nextgen.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenFooterPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenFooterPage_Web.class);

	public NextGenFooterPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By footerPresence = By.xpath(/* "//div[contains(@class,'show-for-large')]//div[@class='footer']" */
			"//div[contains(@class,'aem-Grid')]//div[contains(@class,'footer')]");
	protected By footerMainLinks = By.xpath(/* "//div[@class='footer']//ui[@class='menu expanded']/li/a/span" */
			"//div[contains(@class,'aem-Grid')]//div[contains(@class,'footer')]//ul[@class='menu expanded ']/li/a/span");
	protected By footerMainLinksClick = By.xpath(/*
													 * "//div[@class='footer']//ui[@class='menu expanded']/li/a/span//..//../a"
													 */
			"//div[contains(@class,'aem-Grid')]//div[contains(@class,'footer')]//ul[@class='menu expanded ']/li/a/span//..//../a");
	protected By footerSubLinks = By.xpath("//div[@class='footer']//ul[@class='no-bullet']/li/a");
	protected By generalNav = By.xpath("//div[@class='breadcrumb']");
	protected By breadcrumbNav = By.xpath("//div[@class='breadcrumb']");
	protected By footerSocialIcons = By.xpath(/*
												 * "//div[@class='show-for-large ']//div[contains(@class,'socialMedia')]//img"
												 */
			"//div[contains(@class,'footer')]//div[contains(@class,'socialMedia')]//img");
	protected By beInClubSubLinks = By.xpath(
			/*
			 * "//div[@class='footer']//ui[@class='menu expanded']/li/a[contains(@data-eventlabel,'Be in')]//following-sibling::ul/li/ul/li/a"
			 */
			"//div[contains(@class,'footer')]//ul[@class='menu expanded ']/li/a[contains(@data-eventlabel,'Club Benefits')]//following-sibling::ul/li/ul/li/a");
	protected By resortsSubLinks = By.xpath(
			/*
			 * "//div[@class='footer']//ui[@class='menu expanded']/li/a[contains(@data-eventlabel,'Resorts')]//following-sibling::ul/li/ul/li/a"
			 */
			"//div[contains(@class,'footer')]//ul[@class='menu expanded ']/li/a[contains(@data-eventlabel,'Resorts')]//following-sibling::ul/li/ul/li/a");
	protected By ownerSubLinks = By.xpath(
			/*
			 * "//div[@class='footer']//ui[@class='menu expanded']/li/a[contains(@data-eventlabel,'Owner')]//following-sibling::ul/li/ul/li/a"
			 */
			"//div[contains(@class,'footer')]//ul[@class='menu expanded ']/li/a[contains(@data-eventlabel,'Owner')]//following-sibling::ul/li/ul/li/a");
	protected By dealsSubLinks = By.xpath(
			/*
			 * "//div[@class='footer']//ui[@class='menu expanded']/li/a[contains(@data-eventlabel,'Deals')]//following-sibling::ul/li/ul/li/a"
			 */
			"//div[contains(@class,'footer')]//ul[@class='menu expanded ']/li/a[contains(@data-eventlabel,'Deals')]//following-sibling::ul/li/ul/li/a");
	protected By helpSubLinks = By.xpath(
			/*
			 * "//div[@class='footer']//ui[@class='menu expanded']/li/a[contains(@data-eventlabel,'Help')]//following-sibling::ul/li/ul/li/a"
			 */
			"//div[contains(@class,'footer')]//ul[@class='no-bullet']/li/a[contains(@data-eventlabel,'Help')]//following-sibling::ul/li/ul/li/a");
	protected By disclaimerLink = By.xpath(
			/*
			 * "//div[@class='show-for-large ']//div[contains(@class,'disclaimer') and contains(.,'THIS ADV')]"
			 */
			"//div[contains(@class,'footer')]//div[contains(@class,'disclaimer')]");

	protected By footerLegalLinks = By.xpath(/* "//div[@class='footer']//ul[contains(@class,'quickLinks')]//li" */
			"//div[contains(@class,'footer')]//div[contains(@class,'quickLinks')]//li/a");
	protected By footerLogo = By.xpath(/*
										 * "//div[@class='show-for-large ']//div[@class='footer']//img[@alt='Wyndham Logo']"
										 */
			"//div[contains(@class,'footer')]//img[@alt='Wyndham Logo']");
	protected By wyndhamDestinationsLogo = By.xpath(/* "//img[@alt='Wyndham Destinations logo'][1]" */
			"//div[contains(@class,'global-navigation')]//img[@alt='Wyndham']");
	protected By footerCopyRightText = By.xpath(/* "//div[@class='footer']//ul//li[contains(.,'2020')]" */
			"//div[contains(@class,'footer')]//div[contains(text(),'©2021 Club Wyndham.')]//..//div[contains(text(),' All Rights Reserved.')]");
	protected By termsOfUseNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Terms of Use')]");

	/*
	 * Method: footerValidations Description: Footer Nav and Links Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */

	public void footerValidations() {

		waitUntilElementVisibleBy(driver, footerPresence, 120);

		getElementInView(footerPresence);

		if (verifyObjectDisplayed(footerPresence)) {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerValidations", Status.PASS,
					"Footer is present in the page");
		} else {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerValidations", Status.FAIL,
					"Footer is not present in the page");
		}

		List<WebElement> footerMainLinkList = driver.findElements(footerMainLinks);
		tcConfig.updateTestReporter("NextGenFooterPage", "footerValidations", Status.PASS,
				"Total " + footerMainLinkList.size() + " main links present in the footer");
		for (int i = 0; i < footerMainLinkList.size(); i++) {
			List<WebElement> footerMainLinkList1 = driver.findElements(footerMainLinks);
			String strmainNav = footerMainLinkList1.get(i).getText().toUpperCase();
			List<WebElement> footerLinkClick = driver.findElements(footerMainLinksClick);
			if (strmainNav.toUpperCase().contains(testData.get("header" + (i + 1) + "").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerValidations", Status.PASS,
						"Main Link No " + (i + 1) + " is present as " + (strmainNav));

				// clickElementJSWithWait(footerLinkClick.get(i));
				Actions action = new Actions(driver);
				action.moveToElement(footerLinkClick.get(i));
				action.click(footerLinkClick.get(i));
				Action ac = action.build();
				ac.perform();
				driver.switchTo().activeElement().sendKeys(Keys.ENTER);

				if (i != 1) {
					waitUntilElementVisibleBy(driver, generalNav, 120);
				} else {
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.getCurrentUrl().toUpperCase()
						.contains(testData.get("subHeader" + (i + 1) + "").toUpperCase())) {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerValidations", Status.PASS,
							"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

					// driver.switchTo().activeElement().sendKeys(Keys.END);
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, footerPresence, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerValidations", Status.FAIL,
							"Navigation to " + (i + 1) + " link failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerValidations", Status.FAIL, "Main Link No "
						+ (i + 1) + " is not present  " + (testData.get("header" + (i + 1) + "").toUpperCase()));
			}

		}

	}

	/*
	 * Method: footerBeInClubSubLinks Description: Footer Be In The Clubs Sub Links
	 * Val: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void footerBeInClubSubLinks() {

		List<WebElement> beInClubSublnkList = driver.findElements(beInClubSubLinks);
		tcConfig.updateTestReporter("NextGenFooterPage", "footerBeInClubSubLinks", Status.PASS,
				"Total " + beInClubSublnkList.size() + " Sub links present in Be in the club");
		for (int i = 0; i < 1; i++) {
			List<WebElement> beInClubSublnkList1 = driver.findElements(beInClubSubLinks);
			String strmainNav = beInClubSublnkList1.get(i).getText().toUpperCase();
			// List<WebElement> footerLinkClick=
			// driver.findElements(footerMainLinksClick);
			if (verifyElementDisplayed(beInClubSublnkList1.get(i))) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerBeInClubSubLinks", Status.PASS,
						"Sub Link No " + (i + 1) + " is present as " + (strmainNav));

				// clickElementJSWithWait(footerLinkClick.get(i));
				Actions action = new Actions(driver);
				action.moveToElement(beInClubSublnkList1.get(i));
				action.click(beInClubSublnkList1.get(i));
				Action ac = action.build();
				ac.perform();
				// driver.switchTo().activeElement().sendKeys(Keys.ENTER);

				waitUntilElementVisibleBy(driver, generalNav, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (getElementText(generalNav).toUpperCase().contains(strmainNav)) {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerBeInClubSubLinks", Status.PASS,
							"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

					// driver.switchTo().activeElement().sendKeys(Keys.END);
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, footerPresence, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerBeInClubSubLinks", Status.FAIL,
							"Navigation to " + (i + 1) + " link failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerBeInClubSubLinks", Status.FAIL,
						"Error in getting Sub links");
			}

		}
	}

	/*
	 * Method: footerResortsSubLink Description: Footer Resorts Sub Links Val Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void footerResortsSubLink() {

		List<WebElement> resortsSublnkList = driver.findElements(resortsSubLinks);
		tcConfig.updateTestReporter("NextGenFooterPage", "footerResortsSubLinks", Status.PASS,
				"Total " + resortsSublnkList.size() + " Sub links present in Resorts");
		for (int i = 0; i < 1; i++) {
			List<WebElement> resortsSublnkList1 = driver.findElements(resortsSubLinks);
			String strmainNav = resortsSublnkList1.get(i).getText().toUpperCase();
			// List<WebElement> footerLinkClick=
			// driver.findElements(footerMainLinksClick);
			if (verifyElementDisplayed(resortsSublnkList1.get(i))) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerResortsSubLinks", Status.PASS,
						"Sub Link No " + (i + 1) + " is present as " + (strmainNav));

				// clickElementJSWithWait(footerLinkClick.get(i));
				Actions action = new Actions(driver);
				action.moveToElement(resortsSublnkList1.get(i));
				action.click(resortsSublnkList1.get(i));
				Action ac = action.build();
				ac.perform();

				// waitUntilElementVisibleBy(driver, generalNav, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (/* getElementText(generalNav).toUpperCase().contains(strmainNav) */
				driver.getCurrentUrl().toUpperCase().contains(testData.get("exploreResortUrl").toUpperCase())) {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerResortsSubLinks", Status.PASS,
							"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

					// driver.switchTo().activeElement().sendKeys(Keys.END);
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, footerPresence, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerResortsSubLinks", Status.FAIL,
							"Navigation to " + (i + 1) + " link failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerResortsSubLinks", Status.FAIL,
						"Error in getting Sub links");
			}

		}

	}

	/*
	 * Method: footerOwnerGdeVal Description: Footer Owner Guide Sub Links Val: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void footerOwnerGdeVal() {

		List<WebElement> ownerSublnkList = driver.findElements(ownerSubLinks);
		tcConfig.updateTestReporter("NextGenFooterPage", "footerOwnerSubLinks", Status.PASS,
				"Total " + ownerSublnkList.size() + " Sub links present in Owner Guide");
		for (int i = 0; i < 1; i++) {
			List<WebElement> ownerSublnkList1 = driver.findElements(ownerSubLinks);
			String strmainNav = ownerSublnkList1.get(i).getText().toUpperCase();
			// List<WebElement> footerLinkClick=
			// driver.findElements(footerMainLinksClick);
			if (verifyElementDisplayed(ownerSublnkList1.get(i))) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerOwnerSubLinks", Status.PASS,
						"Sub Link No " + (i + 1) + " is present as " + (strmainNav));

				// clickElementJSWithWait(footerLinkClick.get(i));
				Actions action = new Actions(driver);
				action.moveToElement(ownerSublnkList1.get(i));
				action.click(ownerSublnkList1.get(i));
				Action ac = action.build();
				ac.perform();

				waitUntilElementVisibleBy(driver, generalNav, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (getElementText(generalNav).toUpperCase().contains("OWNER")) {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerOwnerSubLinks", Status.PASS,
							"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

					// driver.switchTo().activeElement().sendKeys(Keys.END);
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, footerPresence, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerOwnerSubLinks", Status.FAIL,
							"Navigation to " + (i + 1) + " link failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerOwnerSubLinks", Status.FAIL,
						"Error in getting Sub links");
			}

		}

	}

	/*
	 * Method: footerDealsSubLinks Description:Footer Deals and Offers Sub Links Val
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void footerDealsSubLinks() {
		// getElementInView(footerPresence);
		List<WebElement> dealsSublnkList = driver.findElements(dealsSubLinks);
		tcConfig.updateTestReporter("NextGenFooterPage", "footerDealsSubLinks", Status.PASS,
				"Total " + dealsSublnkList.size() + " Sub links present in Deals & Offers");
		for (int i = 0; i < 1; i++) {
			List<WebElement> dealsSublnkList1 = driver.findElements(dealsSubLinks);
			String strmainNav = dealsSublnkList1.get(i).getText().toUpperCase();
			// List<WebElement> footerLinkClick=
			// driver.findElements(footerMainLinksClick);
			if (verifyElementDisplayed(dealsSublnkList1.get(i))) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerDealsSubLinks", Status.PASS,
						"Sub Link No " + (i + 1) + " is present as " + (strmainNav));

				// clickElementJSWithWait(footerLinkClick.get(i));
				Actions action = new Actions(driver);
				action.moveToElement(dealsSublnkList1.get(i));
				action.click(dealsSublnkList1.get(i));
				Action ac = action.build();
				ac.perform();
				driver.switchTo().activeElement().sendKeys(Keys.ENTER);
				waitUntilElementVisibleBy(driver, generalNav, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (getElementText(generalNav).toUpperCase().contains(strmainNav)) {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerDealsSubLinks", Status.PASS,
							"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

					// driver.switchTo().activeElement().sendKeys(Keys.END);
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, footerPresence, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerDealsSubLinks", Status.FAIL,
							"Navigation to " + (i + 1) + " link failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerDealsSubLinks", Status.FAIL,
						"Error in getting Sub links");
			}

		}

	}

	/*
	 * Method: footerHelpSubLinks Description: Featured Resorts Help Sub Links Val:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void footerHelpSubLinks() {

		List<WebElement> helpSublnkList = driver.findElements(helpSubLinks);
		tcConfig.updateTestReporter("NextGenFooterPage", "footerHelpSubLinks", Status.PASS,
				"Total " + helpSublnkList.size() + " Sub links present in Help");
		for (int i = 0; i < 1; i++) {
			List<WebElement> helpSublnkList1 = driver.findElements(helpSubLinks);
			String strmainNav = helpSublnkList1.get(i).getText().toUpperCase();
			// List<WebElement> footerLinkClick=
			// driver.findElements(footerMainLinksClick);
			if (verifyElementDisplayed(helpSublnkList1.get(i))) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerHelpSubLinks", Status.PASS,
						"Sub Link No " + (i + 1) + " is present as " + (strmainNav));

				// clickElementJSWithWait(footerLinkClick.get(i));
				Actions action = new Actions(driver);
				action.moveToElement(helpSublnkList1.get(i));
				action.click(helpSublnkList1.get(i));
				Action ac = action.build();
				ac.perform();

				waitUntilElementVisibleBy(driver, generalNav, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (getElementText(generalNav).toUpperCase().contains(strmainNav)) {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerHelpSubLinks", Status.PASS,
							"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

					// driver.switchTo().activeElement().sendKeys(Keys.END);
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, footerPresence, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				} else {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerHelpSubLinks", Status.FAIL,
							"Navigation to " + (i + 1) + " link failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerHelpSubLinks", Status.FAIL,
						"Error in getting Sub links");
			}

		}

	}

	/*
	 * Method: footerSocialIcons Description: Footer Social Icons Val Validtions:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void footerSocialIcons() {

		getElementInView(footerPresence);

		if (verifyObjectDisplayed(disclaimerLink)) {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerSocialIcons", Status.PASS,
					"Disclaimer Text present in the footer");
		} else {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerSocialIcons", Status.FAIL,
					"Disclaimer Text not present in the footer");
		}

		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("NextGenFooterPage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());

			for (int i = 0; i < socialIconsList.size(); i++) {
				String strSiteName = null;

				switch (i) {
				case 0:
					strSiteName = "FACEBOOK";
					break;
				case 1:
					strSiteName = "TWITTER";
					break;
				case 2:
					strSiteName = "YOUTUBE";
					break;
				case 3:
					strSiteName = "INSTAGRAM";
					break;
				case 4:
					strSiteName = "LINKED";
					break;

				default:
					tcConfig.updateTestReporter("NextGenFooterPage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}

				clickElementJS(socialIconsList.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getTitle().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("NextGenFooterPage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

			}

		} else {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}

	}

	/*
	 * Method: footerLegalLinks Description: Footer Legal Links Validations : Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void footerLegalLinks() {

		if (verifyObjectDisplayed(footerLogo)) {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
					"Footer Logo present for Wyndham Destinations");
			clickElementBy(footerLogo);

			waitUntilElementVisibleBy(driver, wyndhamDestinationsLogo, 120);

			if (verifyObjectDisplayed(wyndhamDestinationsLogo)) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
						"Navigated to wyndham destinations page");

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.FAIL,
						"Navigated to wyndham destinations page failed");
			}

			// driver.navigate().back();
			waitUntilElementVisibleBy(driver, footerLogo, 120);

		} else {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.FAIL,
					"Footer Logo not present for Wyndham Destinations");
		}

		getElementInView(footerCopyRightText);
		if (verifyObjectDisplayed(footerCopyRightText)) {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
					"Footer Copy right text present");
		} else {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.FAIL,
					"Footer Copy right text not present");
		}

		List<WebElement> footerLegalList = driver.findElements(footerLegalLinks);

		tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
				"Total " + footerLegalList.size() + " Legal links present in Footer");

		Boolean temp = false;

		for (int i = 0; i < footerLegalList.size(); i++) {
			List<WebElement> footerLegalList1 = driver.findElements(footerLegalLinks);
			String strmainNav = footerLegalList1.get(i).getText().toUpperCase();
			// List<WebElement> footerLinkClick=
			// driver.findElements(footerMainLinksClick);
			if (verifyElementDisplayed(footerLegalList1.get(i))) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
						"Legal Link No " + (i + 1) + " is present as " + (strmainNav));

				if (browserName.equalsIgnoreCase("Firefox")) {
					scrollDownForElementJSWb(footerLegalList1.get(i));
					scrollUpByPixel(150);
					clickElementJSWithWait(footerLegalList1.get(i));
				} else {
					// clickElementJSWithWait(footerLinkClick.get(i));
					Actions action = new Actions(driver);
					action.moveToElement(footerLegalList1.get(i));
					action.click(footerLegalList1.get(i));
					Action ac = action.build();
					ac.perform();
				}

				if (strmainNav.contains("SITEMAP")) {
					temp = true;
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					System.out.println("Current Url : " + driver.getCurrentUrl());
					if (driver.getCurrentUrl().toUpperCase().contains("SITEMAP")) {
						tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
								"Navigation to " + (i + 1) + " link sucessful with header Sitemap");

						// driver.switchTo().activeElement().sendKeys(Keys.END);
						driver.navigate().back();
						waitUntilElementVisibleBy(driver, footerPresence, 120);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else {
						tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.FAIL,
								"Navigation to " + (i + 1) + " link failed");
					}

				} else {
					waitUntilElementVisibleBy(driver, generalNav, 120);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					if (getElementText(generalNav).toUpperCase().contains(strmainNav)) {
						tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
								"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

						// driver.switchTo().activeElement().sendKeys(Keys.END);
						driver.navigate().back();
						waitUntilElementVisibleBy(driver, footerPresence, 120);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					} else if (i == 4 && getElementText(generalNav).toUpperCase().contains("FORM")) {
						tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.PASS,
								"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

					} else {
						tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.FAIL,
								"Navigation to " + (i + 1) + " link failed");
					}

				}

			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.FAIL,
						"Error in getting Legal links");
			}

		}
		if (temp == false) {
			tcConfig.updateTestReporter("NextGenFooterPage", "footerLegalLinks", Status.FAIL,
					"Site map link was not present in footer");
		}
	}

}
