package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class WhyClubWyndhamPage_Android extends WhyClubWyndhamPage_Web {

	public static final Logger log = Logger.getLogger(WhyClubWyndhamPage_Android.class);

	public WhyClubWyndhamPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		
		whyClubWyndhamHeader = By.xpath("//ul[contains(@class,'mobile-navigation__list')]//li/a[text()='Club Benefits']");

	}
	
	@Override
	public void navigateToWhyClubWyndhamPage() {
		waitUntilElementVisibleBy(driver, whyClubWyndhamHeader, 120);

		if (verifyObjectDisplayed(whyClubWyndhamHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.PASS,
					"Why Club Wyndham present in main Nav");
			String href = getElementAttribute(whyClubWyndhamHeader, "href").trim();
			driver.findElement(whyClubWyndhamHeader).click();
			pageCheck();
			
			if (getCurrentURL().trim().equals(href)) {
				tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.PASS,
						"Why Club Wyndham page Navigated, Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.FAIL,
						"Why Club Wyndham page nav failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.FAIL,
					"Why Club Wyndham not present in main Nav");
		}
	}

}

