package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenBuzzWorthyNewsPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenBuzzWorthyNewsPage_Web.class);

	public NextGenBuzzWorthyNewsPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By clubWyndhamNav = By.xpath(/*
											 * "//div[@class='grid-container ']//li[@role='menuitem' and contains(.,'Help')]"
											 */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Club Benefits']");
	protected By buzzWorthyPageNav = By.xpath(/* "//a[@data-eventname='header']//span[contains(.,'FAQ')]" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'Buzz')]");
	protected By buzzCaption = By.xpath(/* "//div[@class='wyn-headline ']//h2" */
			"(//div[@class='caption-1' and contains(text(),'TRENDING')])[1]");
	protected By buzzHeader = By.xpath("(//div[@class='title-1' and contains(text(),'INSIDE')])[1]");
	protected By buzzDesc = By
			.xpath("(//div[@class='title-1' and contains(text(),'INSIDE')])[1]/../div[@class='body-1']/p");
	protected By buzzHeroImg = By.xpath(/* "//div[@class='wyn-hero__image']//img" */
			"(//div[@class='title-1' and contains(text(),'INSIDE')])[1]/ancestor::div[@class='banner']//img");
	protected By buzzBrdCrmb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Buzzworthy')]");
	protected By greatNewsLabel = By.xpath("//h3[contains(@class,'title-1') and contains(text(),'Great News')]");
	protected By greatNewsDesc = By.xpath(
			"//h3[contains(@class,'title-1') and contains(text(),'Great News')]//..//..//..//..//..//div[contains(@class,'body-1')]/p");
	protected By greatNewsBanner = By.xpath("//div[@class='videoBanner']//img");
	protected By greatNewsHeader = By.xpath("//div[@class='videoBanner']//img//..//..//div[@class='title-1']");
	protected By greatNewsBannerDesc = By.xpath("//div[@class='videoBanner']//img//..//..//div[@class='body-1']/p");

	protected By videobutton = By.xpath("//div[@class='videoBanner']//a[contains(@class,'playButton')]");
	protected By videoTitle = By.xpath("//div[@id='player']//h1/a");
	protected By videoFrame = By.xpath("//iframe[@class='video']");
	protected By videoCloseBtn = By.xpath("//button[@class='close-button']");

	protected By bannerDesc = By.xpath("(//section[@class='banner'])[2]//div[@class='title-3']");

	/*
	 * Method: faqPageNav Description: FAQ page navigation Date: Aug/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void buzzNewsPageNav() {

		waitUntilElementVisibleBy(driver, clubWyndhamNav, 120);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(clubWyndhamNav)).build().perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(buzzWorthyPageNav)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "buzzNewsPageNav", Status.PASS,
					"Buzz Worthy News present in Sub Nav");
			driver.findElement(buzzWorthyPageNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "buzzNewsPageNav", Status.FAIL,
					"Buzz Worthy News not present in Sub Nav");
		}

		waitUntilElementVisibleBy(driver, buzzCaption, 120);

		if (verifyObjectDisplayed(buzzCaption)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "buzzNewsPageNav", Status.PASS,
					"Buzz Worthy News Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "buzzNewsPageNav", Status.FAIL,
					"Buzz Worthy News Page Navigation failed");
		}

	}

	/*
	 * Method: faqPageVal Description: FAQ page Validations: Date Aug/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void buzzNewsPageVal() {
		waitUntilObjectVisible(driver, buzzHeader, 120);
		if (verifyObjectDisplayed(buzzHeader)) {

			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
					"Buzz News Page header present as " + getElementText(buzzHeader));

		} else {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
					"Buzz News Page header not present");
		}

		if (verifyObjectDisplayed(buzzDesc)) {

			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
					"Buzz News Page description present as " + getElementText(buzzDesc));

		} else {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
					"Buzz News Page Description not present");
		}

		if (verifyObjectDisplayed(buzzHeroImg)) {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
					"Buzz News Page hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
					"Buzz News Page hero Image not present");
		}

		getElementInView(buzzBrdCrmb);
		if (verifyObjectDisplayed(buzzBrdCrmb)) {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
					"Buzz News Page Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
					"Buzz News Page Breadcrumb not present");
		}

		if (verifyObjectDisplayed(greatNewsLabel)) {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
					"Great News heading present");

		} else {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
					"Great News heading not present");
		}

		if (verifyObjectDisplayed(greatNewsDesc)) {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
					"Great News Description present");

		} else {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
					"Great News description not present");
		}

		if (verifyObjectDisplayed(greatNewsBanner)) {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS, "Great News Banner present");
			if (verifyObjectDisplayed(greatNewsHeader)) {
				tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
						"Great News header present" + getElementText(greatNewsHeader));
				if (verifyObjectDisplayed(greatNewsBannerDesc)) {
					tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
							"Great News Banner Description present" + getElementText(greatNewsBannerDesc));
					if (verifyObjectDisplayed(videobutton)) {
						tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
								"Video button present");
						clickElementBy(videobutton);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						WebElement eleVideoFrame = driver.findElement(videoFrame);
						driver.switchTo().frame(eleVideoFrame);
						if (verifyObjectDisplayed(videoTitle)) {
							tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
									"Video is opened");
						} else {
							tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
									"Video is not opened");
						}

						driver.switchTo().defaultContent();
						clickElementBy(videoCloseBtn);

					} else {
						tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
								"Video button not present");
					}
					getElementInView(bannerDesc);
					if (verifyObjectDisplayed(bannerDesc)) {
						tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.PASS,
								"Banner description is present");
					} else {
						tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
								"Banner description is not present");
					}
				} else {
					tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
							"Great News Banner Description not present");
				}
			} else {
				tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
						"Great News header not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenBuzzPage", "buzzNewsPageVal", Status.FAIL,
					"Great News description not present");
		}

	}

	public void buzzNewsPageNavigation() {
		// TODO Auto-generated method stub
		
	}

}
