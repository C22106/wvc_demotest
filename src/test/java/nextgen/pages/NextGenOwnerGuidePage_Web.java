package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenOwnerGuidePage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenOwnerGuidePage_Web.class);

	public NextGenOwnerGuidePage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By ownerGuideHeader = By.xpath(/*
												 * "//div[@class='grid-container ']//li[@role='menuitem' and contains(.,'Owner Guide')]"
												 */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Owner Guide']");

	protected By ownerGuideHdr = By.xpath(/* "//div[@class='wyn-hero__content' and contains(.,'Owner')]" */
			"(//div[contains(text(),'Owner Guide')])[1]");
	protected By ownerGuideImage = By.xpath(/*
											 * "//div[@class='wyn-hero__content' and contains(.,'Owner')]/parent::div//img"
											 */
			"(//div[contains(text(),'Owner Guide')])[1]/ancestor::div[@class='cardBanner']//img");
	protected By ownerGuideBrdCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Owner Guide')]");
	protected By ownerGuideCardsImg = By.xpath(/* "//div[@gtm_component='cards']//img" */
			"((//div[contains(text(),'Owner Guide')])[1]//following::div[@class='cardComponent'])[1]//img");
	protected By ownerGuideCardsHdr = By.xpath(/* "//div[@gtm_component='cards']//h3" */
			"((//div[contains(text(),'Owner Guide')])[1]//following::div[@class='cardComponent'])[1]//div[contains(@class,'subtitle-2')]");
	protected By ownerGuideCardsDesc = By.xpath(/* "//div[@gtm_component='cards']//p" */
			"((//div[contains(text(),'Owner Guide')])[1]//following::div[@class='cardComponent'])[1]//div[contains(@class,'body-1')]/p");
	protected By ownerGudieCardsCTA = By.xpath(/* "//div[@gtm_component='cards']//div[@class='wyn-button-cta']" */
			"((//div[contains(text(),'Owner Guide')])[1]//following::div[@class='cardComponent'])[1]//div[contains(@class,'body-1-link')]");
	protected By youHaveGotPointsHdr = By.xpath(/* "//div[@class='icon-list']//h2[contains(.,'Points ')]" */
			"//div[contains(@class,'title-1') and contains(text(),'Points')]");
	protected By pointsDesc = By.xpath(/*
										 * "//div[@class='icon-list']//div[@class='wyn-headline ']/following-sibling::p"
										 */
			"//div[contains(@class,'title-1') and contains(text(),'Points')]/../div[@class='body-1']");
	protected By pointsSubHeader = By.xpath(/* "//div[@class='icon-list']//div[@class='wyn-icon-card']//h3" */
			"//div[contains(@class,'title-1') and contains(text(),'Points')]/../div[@class='body-1']//..//..//ul[@class='no-bullet']//div[@class='subtitle-1']");
	protected By pointsSubDesc = By.xpath(/* "//div[@class='icon-list']//div[@class='wyn-icon-card']//p" */
			"//div[contains(@class,'title-1') and contains(text(),'Points')]/../div[@class='body-1']//..//..//ul[@class='no-bullet']//p[@class='body-1']");
	protected By pointsCTA = By.xpath(/* "//div[@class='icon-list']//div[@class='wyn-icon-card']//a" */
			"//div[contains(@class,'title-1') and contains(text(),'Points')]/../div[@class='body-1']//..//..//ul[@class='no-bullet']//a");
	protected By liveEduSesionhdr = By.xpath(/*
												 * "//div[@class='wyn-carousel__container']//h2[contains(.,'Live Education')]"
												 */
			/* "(//div[contains(@class,'title-1') and contains(text(),'LIVE EDU')])[1]"); */
			"//div[contains(@class,'text-primary title-1') and contains(text(),'Live Ed')]");
	protected By liveEduDesc = By.xpath(/*
										 * "//h2[contains(.,'Live Education')]/ancestor::div[@class='wyn-carousel__container']//p"
										 */
			"//div[contains(@class,'text-primary title-1') and contains(text(),'Live Ed')]//../div[contains(@class,'body-1')]/p");
	protected By liveEduImage = By.xpath(/*
											 * "//h2[contains(.,'Live Education')]/ancestor::div[@class='wyn-carousel__container']//img"
											 */
			"(//div[@class='hide-for-small show-for-large'])[1]");
	protected By liveEduCTA = By.xpath(/*
										 * "//h2[contains(.,'Live Education')]/ancestor::div[@class='wyn-carousel__container']//a"
										 */
			/*
			 * "(//div[contains(@class,'title-1') and contains(text(),'LIVE EDU')])[1]/ancestor::div[@class='banner']//a"
			 * );
			 */
			"(//div[contains(@class,'cell small-12')]/a)[1]");
	// Dig into Club life
	protected By digIntoLifeNav = By.xpath(/* "//a[@data-eventname='header']//span[contains(.,'New Owner Quick')]" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'Dig into')]");

	protected By digIntoClubHeader = By.xpath(/* "//div[@class='wyn-hero__content' and contains(.,'New Owner')]" */
			"(//div[@class='title-1' and contains(text(),'DIG')])[1]");

	// New Owner Quick Guide
	protected By newOwnerNav = By.xpath(/* "//a[@data-eventname='header']//span[contains(.,'New Owner Quick')]" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'New Owner')]");
	protected By newOwnerGuideHeader = By.xpath(/* "//div[@class='wyn-hero__content' and contains(.,'New Owner')]" */
			"(//div[@class='caption-1' and contains(text(),'NEW OWNER')])[1]");
	protected By newOwnerHeroImage = By.xpath(/*
												 * "//div[@class='wyn-hero__content' and contains(.,'New Owner')]/parent::div//img"
												 */
			"(//div[@class='caption-1' and contains(text(),'NEW OWNER')])[1]/ancestor::div[@class='banner']//img");
	protected By newOwnerDesc = By.xpath(
			"//div[@gtm_component='banner']//div[@class='wyn-headline ' and contains(.,'Quick Start')]/ancestor::div[@gtm_component]//h3");
	protected By newOwnerDescUp = By.xpath(
			"//div[@gtm_component='banner']//div[@class='wyn-headline ' and contains(.,'Quick Start')]/ancestor::div[@gtm_component]//div[@class='wyn-l-content wyn-card__copy']");
	protected By newOwnerBrdCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'New Owner')]");
	protected By readySetVacation = By.xpath(/* "//h2[contains(.,'Ready. Set')]" */
			"//div[contains(text(),'READY. SET')]");
	protected By readySetDesc = By
			.xpath("//h2[contains(.,'Ready. Set')]/ancestor::div[@gtm_component='cards']//div[@class='column']//p");
	protected By readySetCardsImg = By.xpath(/*
												 * "//h2[contains(.,'Ready. Set')]/ancestor::div[@gtm_component='cards']//img"
												 */
			"(//div[contains(text(),'READY. SET')]//following::div[@class='cardComponent'])[1]//img");
	protected By readySetCardsHdr = By.xpath(/*
												 * "//h2[contains(.,'Ready. Set')]/ancestor::div[@gtm_component='cards']//h3"
												 */
			"(//div[contains(text(),'READY. SET')]//following::div[@class='cardComponent'])[1]//div[contains(@class,'subtitle-2')]");
	protected By readySetCardsDesc = By.xpath(
			/*
			 * "//h2[contains(.,'Ready. Set')]/ancestor::div[@gtm_component='cards']//div[@class='wyn-l-content wyn-card__copy']"
			 */
			"(//div[contains(text(),'READY. SET')]//following::div[@class='cardComponent'])[1]//div[contains(@class,'body-1')]/p");
	protected By readysetCardsCTA = By.xpath(/*
												 * "//h2[contains(.,'Ready. Set')]/ancestor::div[@gtm_component='cards']//div[@class='wyn-card__cta']"
												 */
			"(//div[contains(text(),'READY. SET')]//following::div[@class='cardComponent'])[1]//div[contains(@class,'body-1-link')]");
	protected By videoIcon = By
			.xpath("//div[@class='slick-slide slick-current slick-active']//div[@class='wyn-video-player__icon']");
	protected By videoIconClose = By.xpath("//span[@class='wyn-modal__close']");
	protected By slickBtn = By.xpath("//li[@role='presentation']");
	protected By nextBtn = By.xpath("//button[@aria-label='Next']");
	protected By videoOpen = By.xpath("//h1[contains(.,'Vacation')]");
	protected By vacationMasterHdr = By.xpath(/*
												 * "//div[@class='slick-slide slick-current slick-active']//h2[contains(.,'Vacation Starter')]"
												 */
			"//div[@class='title-1' and contains(text(),'Vacation Starters')]");
	protected By vacationMasterCardsImg = By.xpath(
			/*
			 * "//h2[contains(.,'Vacation Starter')]/ancestor::div[@class='slick-slide slick-current slick-active']//img"
			 */
			"//div[@class='title-1' and contains(text(),'Vacation Starters')]/ancestor::div[@class='banner']//img");
	protected By vacationMasterCardsHdr = By.xpath(
			"//h2[contains(.,'Vacation Starter')]/ancestor::div[@class='slick-slide slick-current slick-active']//h3");
	protected By vacayMasterCardsDesc = By.xpath(
			/*
			 * "//h2[contains(.,'Vacation Starter')]/ancestor::div[@class='slick-slide slick-current slick-active']//p"
			 */
			"//div[@class='title-1' and contains(text(),'Vacation Starters')]//../div[contains(@class,'body-1')]/p");
	protected By vacayMasterCarsCTA = By.xpath(
			/*
			 * "//h2[contains(.,'Vacation Starter')]/ancestor::div[@gtm_component='cards']//div[@class='wyn-card__cta']"
			 */
			"//div[@class='title-1' and contains(text(),'Vacation Starters')]//..//a[text()='Learn More']");
	protected By recommendedReadsTagLine = By.xpath("//p[@class='wyn-headline__tagline']");

	// GetYourVacayOnTheBooks
	protected By getYourVacayArticle = By.xpath("//h3[contains(.,'Get Your Vacation')]");
	protected By getYourVacayHdr = By.xpath("//h1[contains(.,'Get Your')]");
	protected By getYourVacayTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By getYourVacayFacebook = By.xpath(
			"//h1[contains(.,'Get Your')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By getYourVacayTwitter = By.xpath(
			"//h1[contains(.,'Get Your')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By getYourVacayrNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Get Your')]");
	protected By getYourVacayQuestions = By.xpath("//h5[contains(.,'Q')]");
	protected By helpfulHints = By.xpath("//span[contains(.,'Helpful')]");
	protected By expandAllCta = By.xpath("//a[contains(.,'Expand')]");
	protected By collapseAllCta = By.xpath("//a[contains(.,'Collapse')]");
	protected By hintsTabLines = By.xpath("//div[@role='tabpanel']//li");
	protected By accordionActive = By.xpath("//div[@class='wyn-js-accordion is-active']");
	protected By accordionDeactive = By.xpath("//div[@class='wyn-js-accordion']");
	protected By accordionButton = By.xpath("//div[@class='wyn-accordion__expand']//button");
	// span[@class='wyn-accordion__indicator']
	protected By recommendReads = By.xpath(/* "//h2[contains(.,'Recommend')]" */
			"//div[contains(@class,'caption-1') and contains(text(),'Recommended')]");
	protected By recommendReadsCardsImg = By.xpath(/*
													 * "//h2[contains(.,'Recommend')]/ancestor::div[@gtm_component='cards']//img"
													 */
			"//div[contains(@class,'caption-1') and contains(text(),'Recommended')]//following::div[@class='cardComponent']//img");
	protected By recommendCardsHdr = By.xpath(/*
												 * "//h2[contains(.,'Recommend')]/ancestor::div[@gtm_component='cards']//h3"
												 */
			"//div[contains(@class,'caption-1') and contains(text(),'Recommended')]//following::div[@class='cardComponent']//div[contains(@class,'subtitle-2')]");
	protected By reccomendCardsDesc = By.xpath(/*
												 * "//h2[contains(.,'Recommend')]/ancestor::div[@gtm_component='cards']//p"
												 */
			"//div[contains(@class,'caption-1') and contains(text(),'Recommended')]//following::div[@class='cardComponent']//div[contains(@class,'body-1')]/p");
	protected By reccomendCardsCTA = By.xpath(/*
												 * "//h2[contains(.,'Recommend')]/ancestor::div[@gtm_component='cards']//div[@class='wyn-card__cta']"
												 */
			"//div[contains(@class,'caption-1') and contains(text(),'Recommended')]//following::div[@class='cardComponent']//a//div[contains(@class,'body-1-link')]");

	protected By dealsBrdCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Deals')]");
	protected By bokingWindowsBrdCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Booking')]");
	protected By featuredDestBrdCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Featured')]");

	// saveBig
	protected By saveBigWithOwnerArticle = By.xpath(/* "//h3[contains(.,'Save BIG with')]" */
			"//div[contains(text(),'Save BIG with')]");
	protected By saveBigWithOwnerHdr = By.xpath(/* "//h1[contains(.,'Save BIG with')]" */
			"//h3[contains(.,'Save BIG with')]");
	protected By saveBigWithOwnerTagLine = By.xpath(/* "//p[@class='wyn-headline__tagline']" */
			"//h3[contains(.,'Save BIG with')]//..//div[contains(@class,'body-1')]");
	protected By saveBigWithOwnerFacebook = By.xpath(
			"//h1[contains(.,'Save BIG')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By saveBigWithOwnerTwitter = By.xpath(
			"//h1[contains(.,'Save BIG')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By saveBigWithOwnerrNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Save')]");
	protected By saveBigWithOwnerContent = By.xpath(/* "//div[@class='content']" */
			"//div[contains(@class,'caption-1') and contains(text(),'New Owner')]//..//a");
	protected By saveBigOwnerDealsLink = By.xpath("//div[@class='content']//a[contains(.,'Deals')]");

	// two Timewshare Basics
	protected By twoTimeshareBasicsArticle = By.xpath(/* "//h3[contains(.,'Timeshare Basics')]" */
			"//div[contains(text(),'Timeshare Basics')]");
	protected By twoTimeshareBasicsHdr = By.xpath("//h3[contains(.,'Timeshare Basics')]");
	protected By twoTimeshareBasicsTagLine = By.xpath(/* "//p[@class='wyn-headline__tagline']" */
			"//h3[contains(.,'Timeshare Basics')]//..//div[contains(@class,'body-1')]");
	protected By twoTimeshareBasicsFacebook = By.xpath(
			"//h1[contains(.,'Timeshare Basics')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By twoTimeshareBasicsTwitter = By.xpath(
			"//h1[contains(.,'Timeshare Basics')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By twoTimeshareBasicsrNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Timeshare Basics')]");
	protected By twoTimeshareBasicsContent = By.xpath(/* "//div[@class='content']" */
			"//div[contains(@class,'caption-1') and contains(text(),'New Owner')]//..//div[contains(@class,'body-1')]/p");
	protected By twoTimeshareBasicsSubHdrs = By.xpath(/* "//div[@class='content']//h3" */
			"//div[contains(@class,'caption-1') and contains(text(),'New Owner')]//..//h3/b");
	protected By wyndhamRewardsLink = By.xpath(/* "//div[@class='content']//a[contains(.,'Wyndham Rewards')]" */
			"//div[contains(@class,'caption-1') and contains(text(),'New Owner')]//..//a[text()='here']");
	protected By ownerExclusiveLink = By.xpath(/* "//div[@class='content']//a[contains(.,'owner exclusive')]" */
			"//div[contains(@class,'caption-1') and contains(text(),'New Owner')]//..//a[text()='owner exclusives']");
	protected By newOwnerQuickStartLink = By.xpath(/* "//div[@class='content']//a[contains(.,'New Owner Quick')]" */
			"//div[contains(@class,'caption-1') and contains(text(),'New Owner')]//..//a[text()='New Owner Quick Start page']");
	protected By wyndhamRewardsNav = By.xpath("//div[@class='main-header-logo']//img[@alt='Wyndham Rewards']");
	protected By ownerExclusiveNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Exclusive')]");
	protected By newOwnerStartNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'New Owner')]");

	// tips to maximize points
	protected By tipsToMaximizePointsArticle = By.xpath(/* "//h3[contains(.,'Tips to Maximize')]" */
			"//div[contains(text(),'Tips to Maximize')]");
	protected By tipsToMaximizePointsHdr = By.xpath("//h3[contains(.,'Tips to Maximize')]");
	protected By tipsToMaximizePointsTagLine = By.xpath(/* "//p[@class='wyn-headline__tagline']" */
			"//h3[contains(.,'Tips to Maximize')]//..//div[contains(@class,'body-1')]");
	protected By tipsToMaximizePointsFacebook = By.xpath(
			"//h1[contains(.,'Tips to Maximize')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By tipsToMaximizePointsTwitter = By.xpath(
			"//h1[contains(.,'Tips to Maximize')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By tipsToMaximizePointsrNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Tips to Maximize')]");
	protected By tipsToMaximizePointsContent = By.xpath(/* "//div[@class='content']" */
			"//h3[contains(.,'Tips to Maximize')]//..//div[contains(@class,'body-1')]");
	protected By tipsToMaximizePointsSubHdrs = By.xpath(/* "//div[@class='content']//h3" */
			"//h3[contains(.,'Tips to Maximize')]//..//div[contains(@class,'body-1')]//h5/b");
	protected By suggestedResortsLink = By.xpath(/* "//h3[3]/following-sibling::p[position()=1]/a" */
			"//h5[3]/following-sibling::p[position()=1]/a");
	protected By dealsOffersLink = By.xpath(/* "//div[@class='content']//a[contains(.,'Deals')]" */
			"//h3[contains(.,'Tips to Maximize')]//..//a[contains(.,'Deals & Offers')]");
	protected By vacationStarterLink = By.xpath(/* "//div[@class='content']//a[contains(.,'Vacation Starter')]" */
			"//a[contains(.,'Vacation Starter')]");
	protected By suggestedResortNav = By.xpath(/* "//div[@class='breadcrumb']//a[contains(.,'Wyndham Hotels')]" */
			"//div[@class='breadcrumb']//a[contains(.,'Resorts')]");

	// travelNeedsTakenCareOf
	protected By travelNeedsArticle = By.xpath("//h3[contains(.,'Travel Needs')]");
	protected By travelNeedsHdr = By.xpath("//h1[contains(.,'Travel Needs')]");
	protected By travelNeedsTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By travelNeedsFacebook = By.xpath(
			"//h1[contains(.,'Travel Needs')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By travelNeedsTwitter = By.xpath(
			"//h1[contains(.,'Travel Needs')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By travelNeedsrNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel Needs')]");
	protected By travelNeedsQuestions = By.xpath("//div[@class='content']//h5");

	// 5 steps to booking your dream timeshare resort

	protected By bookingYourDreamArticle = By.xpath(/* "//h3[contains(.,'Book Your Dream')]" */
			"//div[contains(text(),'Book Your Dream')]");
	protected By bookingYourDreamHdr = By.xpath(/* "//h1[contains(.,'Book Your Dream')]" */
			"//h3[contains(.,'Book Your Dream')]");
	protected By bookingYourDreamTagLine = By.xpath(/* "//p[@class='wyn-headline__tagline']" */
			"//h3[contains(.,'Book Your Dream')]//..//div[contains(@class,'body-1')]");
	protected By bookingYourDreamFacebook = By.xpath(
			"//h1[contains(.,'Book Your Dream')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By bookingYourDreamTwitter = By.xpath(
			"//h1[contains(.,'Book Your Dream')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By bookingYourDreamNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Book Your Dream')]");
	protected By bookingYourDreamSteps = By.xpath(/* "//div[@class='content']//h3[contains(.,'STEP')]" */
			"//h3[contains(.,'Book Your Dream')]//..//h5[contains(.,'STEP')]/b");
	protected By bookingDreamsARPLink = By.xpath(/* "//div[@class='content']//p/a[contains(.,'Advance')]" */
			"//h3[contains(.,'Book Your Dream')]//..//p/a[contains(.,'Advance')]/b");
	protected By bookingDreamsFeaturedDestLink = By.xpath(/* "//div[@class='content']//p/a[contains(.,'Featured')]" */
			"//h3[contains(.,'Book Your Dream')]//..//p/a[contains(.,'Featured')]");
	// 3 reasons to love your timeshare
	protected By reasonsToLoveTSArticle = By.xpath("//h3[contains(.,'Travel Needs')]");
	protected By reasonsToLoveTSHdr = By.xpath("//h1[contains(.,'Travel Needs')]");
	protected By reasonsToLoveTSTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By reasonsToLoveTSFacebook = By.xpath(
			"//h1[contains(.,'Travel Needs')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By reasonsToLoveTSTwitter = By.xpath(
			"//h1[contains(.,'Travel Needs')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By reasonsToLoveTSrNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel Needs')]");
	protected By reasonsToLoveTSReasonHdr = By.xpath("//div[@class='content']//h3");

	/*
	 * Method: newOwnerQuickGuideNav Description: New Owner page navigation Date:
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void newOwnerQuickGuideNav() {

		waitUntilElementVisibleBy(driver, ownerGuideHeader, 120);
		hoverOnElement(ownerGuideHeader);
		waitUntilElementVisibleBy(driver, newOwnerNav, 120);

		if (verifyObjectDisplayed(newOwnerNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerQuickGuideNav", Status.PASS,
					"New Owner Quick guide present in Sub Nav");
			driver.findElement(newOwnerNav).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerQuickGuideNav", Status.FAIL,
					"New Owner Quick guide not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, newOwnerGuideHeader, 120);

		if (verifyObjectDisplayed(newOwnerGuideHeader)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerQuickGuideNav", Status.PASS,
					"New Owner Quick guide Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerQuickGuideNav", Status.FAIL,
					"New Owner Quick guide Page Navigation failed");
		}

	}

	public void digIntoClubLifeNav() {

		waitUntilElementVisibleBy(driver, ownerGuideHeader, 120);
		hoverOnElement(ownerGuideHeader);
		waitUntilElementVisibleBy(driver, digIntoLifeNav, 120);

		if (verifyObjectDisplayed(digIntoLifeNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "digIntoClubLifeNav", Status.PASS,
					"New Owner Quick guide present in Sub Nav");
			driver.findElement(digIntoLifeNav).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "digIntoClubLifeNav", Status.FAIL,
					"New Owner Quick guide not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, digIntoClubHeader, 120);

		if (verifyObjectDisplayed(digIntoClubHeader)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "digIntoClubLifeNav", Status.PASS,
					"Dig into Club Life Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "digIntoClubLifeNav", Status.FAIL,
					"Dig into Club Life Page Navigation failed");
		}

	}

	/*
	 * Method: newOwnerPageVal Description: New Owner Validations: Date Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void newOwnerPageVal() {
		if (verifyObjectDisplayed(newOwnerGuideHeader)) {

			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.PASS,
					"New Owner Guide header present as " + getElementText(newOwnerGuideHeader));

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.FAIL,
					"New Owner Guide header not present");
		}

		if (verifyObjectDisplayed(newOwnerHeroImage)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.PASS,
					"New Owner Guide  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.FAIL,
					"New Owner Guide  page hero Image not present");
		}

		if (verifyObjectDisplayed(newOwnerBrdCrumb)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.PASS,
					"New Owner Guide Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.FAIL,
					"New Owner Guide Breadcrumb not present");
		}

//		if (verifyObjectDisplayed(newOwnerDesc) || verifyObjectDisplayed(newOwnerDescUp) ) {
//			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.PASS,
//					"New Owner Guide Desc present");
//
//		} else {
//			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "newOwnerPageVal", Status.FAIL,
//					"New Owner Guide Desc not present");
//		}

	}

	/*
	 * Method: readySetVacVal Description:Ready Set Vacation validations : Date
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void readySetVacVal() {
		getElementInView(readySetVacation);

		if (verifyObjectDisplayed(readySetVacation)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "readySetVacayVal", Status.PASS,
					"Header present as: " + getElementText(readySetVacation));

			try {
				List<WebElement> cardsImageList = driver.findElements(readySetCardsImg);
				List<WebElement> cardsHdrsList = driver.findElements(readySetCardsHdr);
				List<WebElement> cardsDescList = driver.findElements(readySetCardsDesc);
				// List<WebElement> cardsLearnMoreList =
				// driver.findElements(rciLearnMore);

				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.PASS,
						"Ready Set Vacation Cards present Total: " + cardsHdrsList.size());

				for (int i = 0; i < cardsHdrsList.size(); i++) {
					getElementInView(cardsHdrsList.get(i));
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.PASS,
							"Card No " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

					if (verifyElementDisplayed(cardsImageList.get(i))) {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.PASS,
								"Image present in " + (i + 1) + " Card");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.FAIL,
								"Image not present in " + (i + 1) + " cards");
					}

					if (verifyElementDisplayed(cardsDescList.get(i))) {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.PASS,
								"Description present in " + (i + 1) + " cards");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.FAIL,
								"Description not present in " + (i + 1) + " cards");
					}

				}

				for (int j = 0; j < cardsHdrsList.size() - 1; j++) {
					List<WebElement> cardsLearnMoreList1 = driver.findElements(readysetCardsCTA);
					if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {
						getElementInView(cardsLearnMoreList1.get(j));

						cardsLearnMoreList1.get(j).click();

						waitForSometime(tcConfig.getConfig().get("LongWait"));

						if (driver.getCurrentUrl().toUpperCase()
								.contains(testData.get("header" + (j + 1)).toUpperCase())) {

							tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.PASS,
									"Navigated to " + (j + 1) + " page");

						} else {
							tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.FAIL,
									"Navigated to " + (j + 1) + " page failed");
						}

						driver.navigate().back();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						waitUntilElementVisibleBy(driver, readySetVacation, 120);

					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.FAIL,
								" CTA link not present in the cards for " + (j + 1) + " block");
					}
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.FAIL,
						"Ready Set Vacay Cards validation Error");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "readySetVacayVal", Status.FAIL,
					"Ready Set Vacay reads header not present");
		}

	}

	/*
	 * Method: vacationMasterVal Description:Vacation Master Validations Date:
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void vacationMasterVal() {

		/*
		 * getElementInView(videoIcon); if (verifyObjectDisplayed(videoIcon)) {
		 * tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vacationMasterVal",
		 * Status.PASS, "Video Icon Present in the banner");
		 * 
		 * clickElementBy(videoIcon);
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * 
		 * waitUntilElementVisibleBy(driver, videoIconClose, 120);
		 * 
		 * if (verifyObjectDisplayed(videoOpen)) {
		 * tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vacationMasterVal",
		 * Status.PASS, "Video opened"); clickElementBy(videoIconClose);
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vacationMasterVal",
		 * Status.FAIL, "Video opening failed"); }
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerExclusivePage",
		 * "vacationMasterVal", Status.FAIL, "Video Icon not Present in the banner");
		 * 
		 * }
		 */

		getElementInView(vacationMasterHdr);

		if (verifyObjectDisplayed(vacationMasterHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "vacationMasterVal", Status.PASS,
					"Header present as: " + getElementText(vacationMasterHdr));

			try {
				List<WebElement> slickBtnList = driver.findElements(slickBtn);

				// List<WebElement> cardsLearnMoreList =
				// driver.findElements(rciLearnMore);

				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.PASS,
						"Vacation Masters Cards present Total: " + slickBtnList.size());
				int slickList = slickBtnList.size();
				for (int i = 0; i < slickList - 1; i++) {

					List<WebElement> cardsImageList = driver.findElements(vacationMasterCardsImg);
					List<WebElement> cardsHdrsList = driver.findElements(vacationMasterCardsHdr);
					List<WebElement> cardsDescList = driver.findElements(vacayMasterCardsDesc);
					getElementInView(cardsHdrsList.get(0));
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.PASS,
							"Card No " + (i + 1) + " header is: " + cardsHdrsList.get(0).getText());

					if (verifyElementDisplayed(cardsImageList.get(0))) {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.PASS,
								"Image present in " + (i + 1) + " Card");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.FAIL,
								"Image not present in " + (i + 1) + " cards");
					}

					if (verifyElementDisplayed(cardsDescList.get(0))) {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.PASS,
								"Description present in " + (i + 1) + " cards");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.FAIL,
								"Description not present in " + (i + 1) + " cards");
					}

					if (verifyObjectDisplayed(nextBtn)) {
						if (browserName.equalsIgnoreCase("Firefox")) {
							scrollDownForElementJSBy(nextBtn);
							scrollUpByPixel(300);

						} else {
							getElementInView(nextBtn);
						}
						clickElementBy(nextBtn);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						break;
					}
				}

				/*
				 * for (int j = 0; j < cardsHdrsList.size(); j++) { List<WebElement>
				 * cardsLearnMoreList1 = driver.findElements(vacayMasterCarsCTA); if
				 * (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {
				 * 
				 * cardsLearnMoreList1.get(j).click();
				 * 
				 * waitForSometime(tcConfig.getConfig().get("LongWait"));
				 * 
				 * if (driver.getCurrentUrl().toUpperCase() .contains(testData.get("subHeader" +
				 * (j + 1)).toUpperCase())) {
				 * 
				 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal",
				 * Status.PASS, "Navigated to " + (j + 1) + " page");
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
				 * "vacationMasterVal", Status.FAIL, "Navigated to " + (j + 1) +
				 * " page failed"); }
				 * 
				 * driver.navigate().back();
				 * 
				 * waitUntilElementVisibleBy(driver, vacationMasterHdr, 120);
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
				 * "vacationMasterVal", Status.FAIL, " CTA link not present in the cards for " +
				 * (j + 1) + " block"); } }
				 */

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.FAIL,
						"Vacation Masters validations failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationMasterVal", Status.FAIL,
					"Vacation Masters reads header not present");
		}

	}

	public void vacationStarterVal() {
		try {
			getElementInView(vacationMasterHdr);
			if (verifyObjectDisplayed(vacationMasterHdr)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.PASS,
						"Vacation Starter header is displayed");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.FAIL,
						"Vacation Starter header is not displayed");
			}

			if (verifyObjectDisplayed(vacationMasterCardsImg)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.PASS,
						"Vacation Starter Image is displayed");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.FAIL,
						"Vacation Starter image is not displayed");
			}

			if (verifyObjectDisplayed(vacayMasterCardsDesc)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.PASS,
						"Vacation Starter Description is displayed");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.FAIL,
						"Vacation Starter Description is not displayed");
			}

			if (verifyObjectDisplayed(vacayMasterCarsCTA)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.PASS,
						"Vacation Starter CTA is displayed");
				clickElementBy(vacayMasterCarsCTA);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (driver.getCurrentUrl().toUpperCase().contains(testData.get("header5").toUpperCase())) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.PASS,
							"Navigated to Respective Page");
				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.FAIL,
							"Not Navigated to Respective Page");
				}
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.FAIL,
						"Vacation Starter CTA is not displayed");
			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "vacationStarterVal", Status.FAIL,
					"FAILED IN METHOD" + e.getMessage());
		}

	}

	/*
	 * Method: ownerGuideNav Description:Owner Guide page navigation Date: Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void ownerGuideNav() {

		waitUntilElementVisibleBy(driver, ownerGuideHeader, 120);

		if (verifyObjectDisplayed(ownerGuideHeader)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuideNav", Status.PASS,
					"Owner guide present in Nav");
			driver.findElement(ownerGuideHeader).click();
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuideNav", Status.FAIL,
					"Owner guide not present in  nav");
		}

		waitUntilElementVisibleBy(driver, ownerGuideHdr, 120);

		if (verifyObjectDisplayed(ownerGuideHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuideNav", Status.PASS,
					"Owner guide Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuideNav", Status.FAIL,
					"Owner guide Page Navigation failed");
		}

	}

	/*
	 * Method: ownerGuidePageVal Description: Owner Guide Validations: Date Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void ownerGuidePageVal() {
		waitUntilElementVisibleBy(driver, ownerGuideHdr, 120);
		if (verifyObjectDisplayed(ownerGuideHdr)) {

			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
					" Owner Guide header present as " + getElementText(ownerGuideHdr));

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
					" Owner Guide header not present");
		}
		waitUntilElementVisibleBy(driver, ownerGuideImage, 120);

		if (verifyObjectDisplayed(ownerGuideImage)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
					" Owner Guide  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
					" Owner Guide  page hero Image not present");
		}

		waitUntilElementVisibleBy(driver, ownerGuideBrdCrumb, 120);
		if (verifyObjectDisplayed(ownerGuideBrdCrumb)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
					" Owner Guide Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
					" Owner Guide Breadcrumb not present");
		}

		try {
			List<WebElement> cardsImageList = driver.findElements(ownerGuideCardsImg);
			List<WebElement> cardsHdrsList = driver.findElements(ownerGuideCardsHdr);
			List<WebElement> cardsDescList = driver.findElements(ownerGuideCardsDesc);
			// List<WebElement> cardsLearnMoreList =
			// driver.findElements(rciLearnMore);

			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
					"Owner Guide Cards Blocks present Total: " + cardsHdrsList.size());

			for (int i = 0; i < cardsHdrsList.size(); i++) {
				getElementInView(cardsHdrsList.get(i));
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
						"Cards" + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

				waitUntilElementVisibleWb(driver, cardsImageList.get(i), "120");
				if (verifyElementDisplayed(cardsImageList.get(i))) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
							"Image present in " + (i + 1) + " Cardsblock");
				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
							"Image not present in " + (i + 1) + " Cardsblock");
				}

				waitUntilElementVisibleWb(driver, cardsDescList.get(i), "120");
				if (verifyElementDisplayed(cardsDescList.get(i))) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
							"Description present in " + (i + 1) + " Cardsblock");
				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
							"Description not present in " + (i + 1) + " Cardsblock");
				}

			}

			for (int j = 0; j < cardsHdrsList.size(); j++) {
				List<WebElement> cardsLearnMoreList1 = driver.findElements(ownerGudieCardsCTA);
				waitUntilElementVisibleWb(driver, cardsLearnMoreList1.get(j), "120");
				if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {

					cardsLearnMoreList1.get(j).click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (driver.getCurrentUrl().toUpperCase().contains(testData.get("header" + (j + 1)).toUpperCase())) {

						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
								"Navigated to " + (j + 1) + " page");

					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
								"Navigated to " + (j + 1) + " page failed");
					}
					driver.navigate().back();

					waitUntilElementVisibleBy(driver, ownerGuideHdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
							" CTA link not present in the Cardsfor " + (j + 1) + " block");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
					"Owner Gudie Cards are not present in the page");
		}

	}

	/*
	 * Method: liveEduSessionVal Description: Owner Guide Live Edu Session
	 * Validations: Date Aug/2019 Author: Unnat Jain Changes By
	 */
	public void liveEduSessionVal() {
		getElementInView(liveEduSesionhdr);

		if (verifyObjectDisplayed(liveEduSesionhdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.PASS,
					getElementText(liveEduSesionhdr) + " present in the page ");

			if (verifyObjectDisplayed(liveEduDesc)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.PASS,
						"Live Education Desc present");

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.FAIL,
						"Live Education Desc not present");
			}

			if (verifyObjectDisplayed(liveEduImage)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.PASS,
						"Live Education Image present");

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.FAIL,
						"Live Education Image not present");
			}

			if (verifyObjectDisplayed(liveEduCTA)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.PASS,
						"Learn More CTA present");

				clickElementBy(liveEduCTA);

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				/*
				 * ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				 * driver.switchTo().window(tabs2.get(1));
				 */

				if (driver.getCurrentUrl().toUpperCase().contains(testData.get("subHeader4").toUpperCase())) {

					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.PASS,
							"Navigated to associate page");

					/*
					 * if (driver.getCurrentUrl().toUpperCase().contains("HTML")) {
					 * tcConfig.updateTestReporter("NextGenOwnerExPage", "clubWynStoreVal",
					 * Status.FAIL, "Link Contains .html " + driver.getCurrentUrl()); } else if
					 * (driver.getCurrentUrl().toUpperCase().contains("ASPX")) {
					 * tcConfig.updateTestReporter("NextGenOwnerExPage", "clubWynStoreVal",
					 * Status.PASS, "Link is displayed correctly " + driver.getCurrentUrl()); }
					 */

				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.FAIL,
							"Navigated to associated page failed");
				}
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				/*
				 * driver.close(); waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 * driver.switchTo().window(tabs2.get(0));
				 */

				waitUntilElementVisibleBy(driver, ownerGuideHdr, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.FAIL,
						"Learn More CTA not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "liveEduSessionVal", Status.FAIL,
					"Live Education Session not present");
		}

	}

	/*
	 * Method: youveGotPointsVal Description: Owner Guide You have got points
	 * Session Validations: Date Aug/2019 Author: Unnat Jain Changes By
	 */
	public void youveGotPointsVal() {
		getElementInView(youHaveGotPointsHdr);

		if (verifyObjectDisplayed(youHaveGotPointsHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.PASS,
					getElementText(youHaveGotPointsHdr) + " present in the page ");
			if (verifyObjectDisplayed(pointsDesc)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.PASS,
						"You have got points Desc present");

				try {

					List<WebElement> pointsHdrsList = driver.findElements(pointsSubHeader);
					List<WebElement> pointsDescList = driver.findElements(pointsSubDesc);
					// List<WebElement> cardsLearnMoreList =
					// driver.findElements(rciLearnMore);

					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
							"Points Sub Headers present Total: " + pointsHdrsList.size());

					for (int i = 0; i < pointsHdrsList.size(); i++) {
						getElementInView(pointsHdrsList.get(i));
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
								"Sub Header" + (i + 1) + " is: " + pointsHdrsList.get(i).getText());

						if (verifyElementDisplayed(pointsDescList.get(i))) {
							tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
									"Description present in " + (i + 1) + " subheader");
						} else {
							tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
									"Description not present in " + (i + 1) + " sub header");
						}

					}

					for (int j = 0; j < pointsHdrsList.size(); j++) {
						List<WebElement> pointsLearnMoreList1 = driver.findElements(pointsCTA);
						if (verifyElementDisplayed(pointsLearnMoreList1.get(j))) {
							getElementInView(pointsLearnMoreList1.get(j));
							pointsLearnMoreList1.get(j).click();

							waitForSometime(tcConfig.getConfig().get("LongWait"));

							if (driver.getCurrentUrl().toUpperCase()
									.contains(testData.get("subHeader" + (j + 1)).toUpperCase())) {

								tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.PASS,
										"Navigated to " + (j + 1) + " page");

							} else {
								tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
										"Navigated to " + (j + 1) + " page failed");
							}
							driver.navigate().back();

							waitUntilElementVisibleBy(driver, ownerGuideHdr, 120);

						} else {
							tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
									" CTA link not present in the SubHeaders for " + (j + 1) + " block");
						}
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "ownerGuidePageVal", Status.FAIL,
							"Points Sub headers are not present in the page");
				}

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.FAIL,
						"Youve got points Desc not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.FAIL,
					"You have got points not  present in the page ");
		}

	}

	/*
	 * Method: getYourVacayNav Description: Navigate to Get Your Vacay page Session
	 * Validations: Date Aug/2019 Author: Unnat Jain Changes By
	 */
	public void getYourVacayNav() {
		getElementInView(getYourVacayArticle);

		if (verifyObjectDisplayed(getYourVacayArticle)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.PASS,
					getElementText(getYourVacayArticle) + " present in the page ");
			clickElementBy(getYourVacayArticle);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.FAIL,
					"Get Your Vacation article not present in the page");
		}

		waitUntilElementVisibleBy(driver, getYourVacayHdr, 120);

		if (verifyObjectDisplayed(getYourVacayHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.PASS,
					"Get You vacation On Th Books page navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.FAIL,
					"Get You vacation On Th Books Page Navigation failed");
		}

		if (verifyObjectDisplayed(getYourVacayTagLine)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.PASS,
					getElementText(getYourVacayTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.FAIL,
					"Get You vacation Header tag line not present");
		}

		if (verifyObjectDisplayed(getYourVacayrNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "youveGotPointsVal", Status.FAIL,
					"Get You vacation Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(getYourVacayFacebook)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.PASS,
					"Get Your Vacation on the books facebook icon present");

			clickElementBy(getYourVacayFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.FAIL,
					"Get Your Vacation on the books facebook icon not present");

		}

		if (verifyObjectDisplayed(getYourVacayTwitter)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.PASS,
					"Get Your Vacation on the books twitter icon present");

			clickElementBy(getYourVacayTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "getYourVacayNav", Status.FAIL,
					"Get Your Vacation on the bookstwitter icon not present");

		}

		try {
			List<WebElement> questionsList = driver.findElements(getYourVacayQuestions);
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
					"Questions present on the page, Total: " + questionsList.size());
		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
					"No Questions present on the page");
		}

	}

	/*
	 * Method: travelNeedsNav Description: Navigate to Travel Needs page Session
	 * Validations: Date Aug/2019 Author: Unnat Jain Changes By
	 */
	public void travelNeedsNav() {
		getElementInView(travelNeedsArticle);

		if (verifyObjectDisplayed(travelNeedsArticle)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.PASS,
					getElementText(travelNeedsArticle) + " present in the page ");
			clickElementBy(travelNeedsArticle);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.FAIL,
					"Travel Needs taken Care of article not present in the page");
		}

		waitUntilElementVisibleBy(driver, travelNeedsHdr, 120);

		if (verifyObjectDisplayed(travelNeedsHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.PASS,
					"Travel Needs taken Care of page navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.FAIL,
					"Travel Needs taken Care of Page Navigation failed");
		}

		if (verifyObjectDisplayed(travelNeedsTagLine)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.PASS,
					getElementText(travelNeedsTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.FAIL,
					"Travel Needs taken Care of Header tag line not present");
		}

		if (verifyObjectDisplayed(travelNeedsrNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsArticle", Status.FAIL,
					"Travel Needs taken Care of Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(travelNeedsFacebook)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.PASS,
					"Travel Needs taken Care of on the books facebook icon present");

			clickElementBy(travelNeedsFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.FAIL,
					"Travel Needs taken Care of on the books facebook icon not present");

		}

		if (verifyObjectDisplayed(travelNeedsTwitter)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.PASS,
					"Travel Needs taken Care of on the books twitter icon present");

			clickElementBy(travelNeedsTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.FAIL,
					"Travel Needs taken Care of on the bookstwitter icon not present");

		}

		try {
			List<WebElement> questionsList = driver.findElements(travelNeedsQuestions);
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.PASS,
					"Questions present on the page, Total: " + questionsList.size());
		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "travelNeedsNav", Status.FAIL,
					"No Questions present on the page");
		}

	}

	/*
	 * Method: bookingYourDreamNav Description: Navigate to Steps Booking your dream
	 * page Session Validations: Date Aug/2019 Author: Unnat Jain Changes By
	 */
	public void bookingYourDreamNav() {
		getElementInView(bookingYourDreamArticle);

		if (verifyObjectDisplayed(bookingYourDreamArticle)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
					getElementText(bookingYourDreamArticle) + " present in the page ");
			clickElementBy(bookingYourDreamArticle);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
					"Booking Your Dream Timeshare article not present in the page");
		}

		waitUntilElementVisibleBy(driver, bookingYourDreamHdr, 120);

		if (verifyObjectDisplayed(bookingYourDreamHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
					"Booking Your Dream Timeshare page navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
					"Booking Your Dream Timeshare Page Navigation failed");
		}

		if (verifyObjectDisplayed(bookingYourDreamTagLine)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
					getElementText(bookingYourDreamTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
					"Booking Your Dream Timeshare Header tag line not present");
		}

		if (verifyObjectDisplayed(bookingYourDreamNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
					"Booking Your Dream Timeshare Header breadcrumb not present");
		}

		/*
		 * if (verifyObjectDisplayed(bookingYourDreamFacebook)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal",
		 * Status.PASS, "Booking Your Dream Timeshare facebook icon present");
		 * 
		 * clickElementBy(bookingYourDreamFacebook);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * } waitForSometime(tcConfig.getConfig().get("MedWait")); if
		 * (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal",
		 * Status.PASS, "Navigated to facebook page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal",
		 * Status.FAIL, "Navigation to facebook page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "bookingYourDreamVal", Status.FAIL,
		 * "Booking Your Dream Timeshare  facebook icon not present");
		 * 
		 * }
		 * 
		 * if (verifyObjectDisplayed(bookingYourDreamTwitter)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal",
		 * Status.PASS, "Booking Your Dream Timeshare  twitter icon present");
		 * 
		 * clickElementBy(bookingYourDreamTwitter);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * }
		 * 
		 * if (driver.getTitle().toUpperCase().contains("TWITTER")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal",
		 * Status.PASS, "Navigated to twitter page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal",
		 * Status.FAIL, "Navigation to twitter page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "bookingYourDreamVal", Status.FAIL,
		 * "Booking Your Dream Timeshare twitter icon not present");
		 * 
		 * }
		 */

		try {
			List<WebElement> questionsList = driver.findElements(bookingYourDreamSteps);
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
					"Steps present on the page, Total: " + questionsList.size());

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
					"No Steps present on the page");
		}

		getElementInView(bookingDreamsARPLink);

		if (verifyObjectDisplayed(bookingDreamsARPLink)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
					"ARP link displayed in the content");

			clickElementBy(bookingDreamsARPLink);

			waitUntilElementVisibleBy(driver, bokingWindowsBrdCrumb, 120);

			if (verifyObjectDisplayed(bokingWindowsBrdCrumb)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
						"ARP Booking Window page navigated");

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, bookingYourDreamHdr, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
						"ARP Booking Window page not snavigated");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
					"ARP link not present on the page");
		}

		getElementInView(bookingDreamsFeaturedDestLink);

		if (verifyObjectDisplayed(bookingDreamsFeaturedDestLink)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
					"Featured Dest link displayed in the content");

			clickElementBy(bookingDreamsFeaturedDestLink);

			waitUntilElementVisibleBy(driver, featuredDestBrdCrumb, 120);

			if (verifyObjectDisplayed(featuredDestBrdCrumb)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.PASS,
						"Featured destination page navigated");

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, bookingYourDreamHdr, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
						"Featured destination page not navigated");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "bookingYourDreamVal", Status.FAIL,
					"Featured Dest link not displayed in the content");
		}

	}

	/*
	 * Method: reasonsToLoveTSNav Description: Navigate to Reasons to love timeshare
	 * page Session Validations: Date Aug/2019 Author: Unnat Jain Changes By
	 */
	public void reasonsToLoveTSNav() {
		getElementInView(reasonsToLoveTSArticle);

		if (verifyObjectDisplayed(reasonsToLoveTSArticle)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.PASS,
					getElementText(reasonsToLoveTSArticle) + " present in the page ");
			clickElementBy(reasonsToLoveTSArticle);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.FAIL,
					"Reason To Love CW Timeshare article not present in the page");
		}

		waitUntilElementVisibleBy(driver, reasonsToLoveTSHdr, 120);

		if (verifyObjectDisplayed(reasonsToLoveTSHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.PASS,
					"Reason To Love CW Timeshare page navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.FAIL,
					"Reason To Love CW Timeshare Page Navigation failed");
		}

		if (verifyObjectDisplayed(reasonsToLoveTSTagLine)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.PASS,
					getElementText(reasonsToLoveTSTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.FAIL,
					"Reason To Love CW Timeshare Header tag line not present");
		}

		if (verifyObjectDisplayed(reasonsToLoveTSrNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSArticle", Status.FAIL,
					"Reason To Love CW Timeshare Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(reasonsToLoveTSFacebook)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.PASS,
					"Reason To Love CW Timeshare on the books facebook icon present");

			clickElementBy(reasonsToLoveTSFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.FAIL,
					"Reason To Love CW Timeshare on the books facebook icon not present");

		}

		if (verifyObjectDisplayed(reasonsToLoveTSTwitter)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.PASS,
					"Reason To Love CW Timeshare on the books twitter icon present");

			clickElementBy(reasonsToLoveTSTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.FAIL,
					"Reason To Love CW Timeshare on the bookstwitter icon not present");

		}

		try {
			List<WebElement> questionsList = driver.findElements(reasonsToLoveTSReasonHdr);
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.PASS,
					"Reasons present on the page, Total: " + questionsList.size());
		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "reasonsToLoveTSNav", Status.FAIL,
					"No Reasons present on the page");
		}

	}

	/*
	 * Method: helpFulHintsValidations Description: Helpful hints validations : Date
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void helpFulHintsValidations() {
		getElementInView(helpfulHints);

		if (verifyObjectDisplayed(helpfulHints)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
					"Helpful hints section present in the page ");

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
					"Helpful hints section not present in the page");
		}

		if (verifyObjectDisplayed(expandAllCta)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
					"Expand All CTA present");
			clickElementBy(expandAllCta);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				List<WebElement> hintsList = driver.findElements(hintsTabLines);
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
						"Hints present on the page, Total: " + hintsList.size());
			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
						"No Hints present on the page");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(collapseAllCta)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
						"Collpase All CTA present");
				clickElementBy(collapseAllCta);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
						"Collaspe All CTA not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
					"Expand All CTA not present");
		}
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(accordionButton)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
					"Accordian + button present");
			clickElementBy(accordionButton);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.findElement(accordionActive).getAttribute("class").contains("active")) {
				try {
					List<WebElement> hintsList = driver.findElements(hintsTabLines);
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
							"Hints present on the page, Total: " + hintsList.size());
				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
							"No Hints present on the page");
				}
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(accordionButton)) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
							"Accordian - button present");
					clickElementBy(accordionButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					if (driver.findElement(accordionDeactive).getAttribute("class").contains("active")) {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
								"Error in closing the hints via - button");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.PASS,
								"Closed the panel");
					}

				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
							"Accordian - button not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
						"Error in opening the hints via + button");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "helpFulHintsValidations", Status.FAIL,
					"Accordian + button not present");
		}

	}

	/*
	 * Method: recommendedReadsVal Description: Recommended Reads validations : Date
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void recommendedReadsVal() {
		getElementInView(recommendReads);

		if (verifyObjectDisplayed(recommendReads)) {
			tcConfig.updateTestReporter("NextGenOwnerExclusivePage", "recommendedReadsVal", Status.PASS,
					"Recommended Reads header present " + getElementText(recommendReads));

			try {
				List<WebElement> cardsImageList = driver.findElements(recommendReadsCardsImg);
				List<WebElement> cardsHdrsList = driver.findElements(recommendCardsHdr);
				List<WebElement> cardsDescList = driver.findElements(reccomendCardsDesc);
				// List<WebElement> cardsLearnMoreList =
				// driver.findElements(rciLearnMore);

				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.PASS,
						"Recommended Reads Cards present Total: " + cardsHdrsList.size());

				for (int i = 0; i < cardsHdrsList.size(); i++) {
					getElementInView(cardsHdrsList.get(i));
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.PASS,
							"Card No " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

					if (verifyElementDisplayed(cardsImageList.get(i))) {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.PASS,
								"Image present in " + (i + 1) + " Card");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.FAIL,
								"Image not present in " + (i + 1) + " cards");
					}

					if (verifyElementDisplayed(cardsDescList.get(i))) {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.PASS,
								"Description present in " + (i + 1) + " cards");
					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.FAIL,
								"Description not present in " + (i + 1) + " cards");
					}

				}

				for (int j = 0; j < cardsHdrsList.size(); j++) {
					List<WebElement> cardsLearnMoreList1 = driver.findElements(reccomendCardsCTA);
					waitUntilElementVisibleWb(driver, cardsLearnMoreList1.get(j), "120");
					if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {

						cardsLearnMoreList1.get(j).click();

						/* waitUntilElementVisibleBy(driver, recommendedReadsTagLine, 120); */
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						if (driver.getCurrentUrl().toUpperCase()
								.contains(testData.get("subHeader" + (j + 1) + "").toUpperCase())) {

							tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.PASS,
									"Navigated to " + (j + 1) + " page");

						} else {
							tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.FAIL,
									"Navigated to " + (j + 1) + " page failed");
						}

						driver.navigate().back();

						waitUntilElementVisibleBy(driver, recommendReads, 120);

					} else {
						tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.FAIL,
								" CTA link not present in the cards for " + (j + 1) + " block");
					}
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.FAIL,
						"Recommeded reads Cards are not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "recommendedReadsVal", Status.FAIL,
					"Recommeded reads header not present");
		}

	}

	/*
	 * Method: saveBigWithBenefit Description: Save big with this owner benefit
	 * Validations: Date Sep/2019 Author: Unnat Jain Changes By
	 */

	public void saveBigWithBenefit() {

		getElementInView(saveBigWithOwnerArticle);

		if (verifyObjectDisplayed(saveBigWithOwnerArticle)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.PASS,
					getElementText(saveBigWithOwnerArticle) + " present in the page ");
			clickElementBy(saveBigWithOwnerArticle);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.FAIL,
					"Save big with these owner article not present in the page");
		}

		waitUntilElementVisibleBy(driver, saveBigWithOwnerrNav, 120);
		getElementInView(saveBigWithOwnerrNav);
		if (verifyObjectDisplayed(saveBigWithOwnerrNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.FAIL,
					"Save big with these owner Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(saveBigWithOwnerHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.PASS,
					"Save big with these owner page navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.FAIL,
					"Save big with these owner Page Navigation failed");
		}

		if (verifyObjectDisplayed(saveBigWithOwnerTagLine)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.PASS,
					getElementText(saveBigWithOwnerTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.FAIL,
					"Save big with these owner Header tag line not present");
		}

		/*
		 * if (verifyObjectDisplayed(saveBigWithOwnerFacebook)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit",
		 * Status.PASS, "Save big with these owner facebook icon present");
		 * 
		 * clickElementBy(saveBigWithOwnerFacebook);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * } waitForSometime(tcConfig.getConfig().get("MedWait"));
		 * 
		 * if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit",
		 * Status.PASS, "Navigated to facebook page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit",
		 * Status.FAIL, "Navigation to facebook page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "saveBigWithBenefit", Status.FAIL,
		 * "Save big with these owner  facebook icon not present");
		 * 
		 * }
		 * 
		 * if (verifyObjectDisplayed(saveBigWithOwnerTwitter)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit",
		 * Status.PASS, "Save big with these owner twitter icon present");
		 * 
		 * clickElementBy(saveBigWithOwnerTwitter);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * }
		 * 
		 * if (driver.getTitle().toUpperCase().contains("TWITTER")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit",
		 * Status.PASS, "Navigated to twitter page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit",
		 * Status.FAIL, "Navigation to twitter page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "saveBigWithBenefit", Status.FAIL,
		 * "Save big with these owner on the bookstwitter icon not present");
		 * 
		 * }
		 */

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		// getElementInView(saveBigOwnerDealsLink);
		waitUntilElementVisibleBy(driver, saveBigWithOwnerContent, 120);

		if (verifyObjectDisplayed(saveBigWithOwnerContent)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.PASS,
					"Save big Deals Link present on the page");

			clickElementBy(saveBigWithOwnerContent);

			waitUntilElementVisibleBy(driver, dealsBrdCrumb, 120);

			if (verifyObjectDisplayed(dealsBrdCrumb)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.PASS,
						"Deals and Offers page navigated");

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, saveBigWithOwnerContent, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.FAIL,
						"Deals and Offers page not navigated");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "saveBigWithBenefit", Status.FAIL,
					"Save big Deals Link not present on the page");
		}

	}

	/*
	 * Method: twoTimeShareBasicsVal Description: Two timeshare basics Validations:
	 * Date Sep/2019 Author: Unnat Jain Changes By
	 */

	public void twoTimeShareBasicsVal() {

		getElementInView(twoTimeshareBasicsArticle);

		if (verifyObjectDisplayed(twoTimeshareBasicsArticle)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
					getElementText(twoTimeshareBasicsArticle) + " present in the page ");
			clickElementBy(twoTimeshareBasicsArticle);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
					"Two Timeshare Basics article not present in the page");
		}

		getElementInView(twoTimeshareBasicsrNav);
		if (verifyObjectDisplayed(twoTimeshareBasicsrNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
					"Two Timeshare Basics Header breadcrumb not present");
		}

		// waitUntilElementVisibleBy(driver, twoTimeshareBasicsHdr, 120);

		if (verifyObjectDisplayed(twoTimeshareBasicsHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
					"Two Timeshare Basics page navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
					"Two Timeshare Basics Page Navigation failed");
		}

		if (verifyObjectDisplayed(twoTimeshareBasicsTagLine)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
					getElementText(twoTimeshareBasicsTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
					"Two Timeshare Basics Header tag line not present");
		}

		/*
		 * if (verifyObjectDisplayed(twoTimeshareBasicsFacebook)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics",
		 * Status.PASS, "Two Timeshare Basics on the books facebook icon present");
		 * 
		 * clickElementBy(twoTimeshareBasicsFacebook);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * } waitForSometime(tcConfig.getConfig().get("MedWait")); if
		 * (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics",
		 * Status.PASS, "Navigated to facebook page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics",
		 * Status.FAIL, "Navigation to facebook page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "twoTimeshareBasics", Status.FAIL,
		 * "Two Timeshare Basics on the books facebook icon not present");
		 * 
		 * }
		 * 
		 * if (verifyObjectDisplayed(twoTimeshareBasicsTwitter)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics",
		 * Status.PASS, "Two Timeshare Basics on the books twitter icon present");
		 * 
		 * clickElementBy(twoTimeshareBasicsTwitter);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * }
		 * 
		 * if (driver.getTitle().toUpperCase().contains("TWITTER")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics",
		 * Status.PASS, "Navigated to twitter page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics",
		 * Status.FAIL, "Navigation to twitter page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "twoTimeshareBasics", Status.FAIL,
		 * "Two Timeshare Basics on the bookstwitter icon not present");
		 * 
		 * }
		 */

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(twoTimeshareBasicsContent)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
					"Two Timeshare Basics content present on the page");

			List<WebElement> timeshareBasics = driver.findElements(twoTimeshareBasicsSubHdrs);

			if (timeshareBasics.size() == 2) {

				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
						"2 Timeshare basics headers present");

				for (int i = 0; i < timeshareBasics.size(); i++) {
					getElementInView(timeshareBasics.get(i));
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
							"2 Timeshare basics headers " + (i + 1) + " is " + timeshareBasics.get(i).getText());

				}

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
						" 2 Timeshare basics headers are not present");
			}

			getElementInView(wyndhamRewardsLink);

			if (verifyObjectDisplayed(wyndhamRewardsLink)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
						"Wyndham Rewards Link present on the page");

				clickElementBy(wyndhamRewardsLink);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}
				waitUntilElementVisibleBy(driver, wyndhamRewardsNav, 120);

				if (verifyObjectDisplayed(wyndhamRewardsNav)) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
							"Wyndham Rewards page navigated");

					// driver.navigate().back();
					driver.close();
					driver.switchTo().window(winHandlesBefor);

					waitUntilElementVisibleBy(driver, twoTimeshareBasicsContent, 120);

				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
							"Wyndham Rewards Link page not navigated");
				}

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
						"Wyndham Rewards link not present on the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
					"Two Timeshare Basics content not present on the page");
		}

		getElementInView(ownerExclusiveLink);

		if (verifyObjectDisplayed(ownerExclusiveLink)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
					"Owner Exclusive Link present on the page");

			clickElementBy(ownerExclusiveLink);

			waitUntilElementVisibleBy(driver, ownerExclusiveNav, 120);

			if (verifyObjectDisplayed(ownerExclusiveNav)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
						"Owner Exclusive page navigated");

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, twoTimeshareBasicsContent, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
						"Owner Exclusive Link page not navigated");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
					"Owner Exclusive Link not present on the page");
		}

		getElementInView(newOwnerQuickStartLink);

		if (verifyObjectDisplayed(newOwnerQuickStartLink)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
					"New Owner Quick Start Link present on the page");

			clickElementBy(newOwnerQuickStartLink);

			waitUntilElementVisibleBy(driver, newOwnerStartNav, 120);

			if (verifyObjectDisplayed(newOwnerStartNav)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.PASS,
						"New Owner Quick Start  page navigated");

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, twoTimeshareBasicsContent, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
						"New Owner Quick Start  Link page not navigated");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "twoTimeshareBasics", Status.FAIL,
					"New Owner Quick Start Link not present on the page");
		}

	}

	/*
	 * Method: tipstoMaximizePoints Description: Tips to maximize points val
	 * Validations: Date Sep/2019 Author: Unnat Jain Changes By
	 */

	public void tipstoMaximizePointsVal() {

		getElementInView(tipsToMaximizePointsArticle);

		if (verifyObjectDisplayed(tipsToMaximizePointsArticle)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					getElementText(tipsToMaximizePointsArticle) + " present in the page ");
			clickElementBy(tipsToMaximizePointsArticle);

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Tips To Maximize Points article not present in the page");
		}

		waitUntilElementVisibleBy(driver, tipsToMaximizePointsHdr, 120);

		if (verifyObjectDisplayed(tipsToMaximizePointsHdr)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					"Tips To Maximize Points page navigated");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Tips To Maximize Points Page Navigation failed");
		}

		if (verifyObjectDisplayed(tipsToMaximizePointsTagLine)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					getElementText(tipsToMaximizePointsTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Tips To Maximize Points Header tag line not present");
		}

		if (verifyObjectDisplayed(tipsToMaximizePointsrNav)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Tips To Maximize Points Header breadcrumb not present");
		}

		/*
		 * if (verifyObjectDisplayed(tipsToMaximizePointsFacebook)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints",
		 * Status.PASS, "Tips To Maximize Points on the books facebook icon present");
		 * 
		 * clickElementBy(tipsToMaximizePointsFacebook);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * }
		 * 
		 * if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints",
		 * Status.PASS, "Navigated to facebook page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints",
		 * Status.FAIL, "Navigation to facebook page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "tipsToMaximizePoints", Status.FAIL,
		 * "Tips To Maximize Points on the books facebook icon not present");
		 * 
		 * }
		 * 
		 * if (verifyObjectDisplayed(tipsToMaximizePointsTwitter)) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints",
		 * Status.PASS, "Tips To Maximize Points on the books twitter icon present");
		 * 
		 * clickElementBy(tipsToMaximizePointsTwitter);
		 * 
		 * String winHandlesBefor = driver.getWindowHandle();
		 * 
		 * for (String winHandle : driver.getWindowHandles()) {
		 * driver.switchTo().window(winHandle);
		 * 
		 * }
		 * 
		 * if (driver.getTitle().toUpperCase().contains("TWITTER")) {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints",
		 * Status.PASS, "Navigated to twitter page"); } else {
		 * tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints",
		 * Status.FAIL, "Navigation to twitter page failed"); }
		 * 
		 * driver.close();
		 * 
		 * driver.switchTo().window(winHandlesBefor);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenOwnerGuidePage",
		 * "tipsToMaximizePoints", Status.FAIL,
		 * "Tips To Maximize Points on the bookstwitter icon not present");
		 * 
		 * }
		 */

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(tipsToMaximizePointsContent)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					"Tips To Maximize Points content present on the page");

			List<WebElement> maximizeTipsList = driver.findElements(tipsToMaximizePointsSubHdrs);

			if (maximizeTipsList.size() == 4) {

				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
						"Tips to Maximize Points Tips headers present");

				for (int i = 0; i < maximizeTipsList.size(); i++) {
					getElementInView(maximizeTipsList.get(i));
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
							" Tip " + (i + 1) + " is " + maximizeTipsList.get(i).getText());

				}

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
						"  Tips are not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Tips To Maximize Points content not present on the page");
		}

		try {
			List<WebElement> resortsLinkList = driver.findElements(suggestedResortsLink);

			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					"Suggested Resorts link is present, total: " + resortsLinkList.size());
			int totalLinks = resortsLinkList.size();
			for (int i = 0; i < totalLinks; i++) {
				List<WebElement> resortsLinkTemp = driver.findElements(suggestedResortsLink);

				getElementInView(resortsLinkTemp.get(i));
				resortsLinkTemp.get(i).click();

				waitUntilElementVisibleBy(driver, suggestedResortNav, 120);

				if (verifyObjectDisplayed(suggestedResortNav)) {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
							"Navigated To associated page wit title: " + driver.getTitle());
				} else {
					tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
							"Navigation to assoicated page failed");
				}

				driver.navigate().back();
				waitUntilElementVisibleBy(driver, tipsToMaximizePointsContent, 120);

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Suggested Resorts link is not present");
		}

		getElementInView(dealsOffersLink);

		if (verifyObjectDisplayed(dealsOffersLink)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					"Deals Link present on the page");

			clickElementBy(dealsOffersLink);

			waitUntilElementVisibleBy(driver, dealsBrdCrumb, 120);

			if (verifyObjectDisplayed(dealsBrdCrumb)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
						"Deals and Offers page navigated");

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, tipsToMaximizePointsContent, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
						"Deals and Offers page not navigated");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Deals Link not present on the page");
		}

		getElementInView(vacationStarterLink);

		if (verifyObjectDisplayed(vacationStarterLink)) {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
					"Vacation Starter Link present on the page");

			clickElementBy(vacationStarterLink);

			waitUntilElementVisibleBy(driver, newOwnerStartNav, 120);

			if (verifyObjectDisplayed(newOwnerStartNav)) {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.PASS,
						"Vacation Starter page navigated");

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, tipsToMaximizePointsContent, 120);

			} else {
				tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
						"Vacation starter page not navigated");
			}

		} else {
			tcConfig.updateTestReporter("NextGenOwnerGuidePage", "tipsToMaximizePoints", Status.FAIL,
					"Vacation Starter Link not present on the page");
		}

	}

}

