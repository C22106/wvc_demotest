package nextgen.pages;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenTravelPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenTravelPage_Web.class);

	public NextGenTravelPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By dealsOffersHeader = By.xpath(/*
												 * "//div[@class='wyn-header__nav-container']//li[@class='has-submenu' and contains(.,'Deals')]"
												 */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Deals & Offers']");
	protected By travelDealNav = By.xpath(/* "//div[@class='wyn-fly-out']//li[contains(.,'Travel Deal')]/a" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'Partner Offers')]");
	protected By travelDealsHdr = By.xpath(/* "//p[contains(.,'Travel Deals')]" */
			"(//div[contains(text(),'Partner Offers')])[1]");
	protected By travelSubHdr = By.xpath(/*
											 * "//p[contains(.,'Travel Deals')]/ancestor::div[@class='wyn-hero wyn-hero--large']//h2"
											 */
			"(//div[contains(text(),'Partner Offers')])[1]/../div[@class='title-1']");
	protected By travelImage = By.xpath(/*
										 * "//p[contains(.,'Travel Deals')]/ancestor::div[@class='wyn-hero wyn-hero--large']//img"
										 */
			"(//div[contains(text(),'Partner Offers')])[1]/../div[@class='title-1']/../../../..//div[@class='image ']/img");
	protected By travelDesc = By.xpath(
			/*
			 * "//p[contains(.,'Travel Deals')]/ancestor::div[@class='wyn-hero wyn-hero--large']//div[@class='wyn-l-content wyn-card__copy']/p"
			 */
			"(//div[contains(text(),'Partner Offers')])[1]/../div[@class='body-1']/p");
	protected By travelBrdcrmbs = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Partner Offers')]");

	protected By travelCardsImg = By.xpath(/* "//div[@gtm_component='two-columns-container']//img" */
			"((//div[contains(text(),'Partner Offers')])[1]//following::div[@class='cardComponent'])[1]//img");
	protected By travelCardshdr = By.xpath(/* "//div[@gtm_component='two-columns-container']//h2" */
			"((//div[contains(text(),'Partner Offers')])[1]//following::div[@class='cardComponent'])[1]//div[contains(@class,'subtitle-2')]");
	protected By travelCardsDesc = By.xpath(/* " //div[@gtm_component='two-columns-container']//p" */
			"((//div[contains(text(),'Partner Offers')])[1]//following::div[@class='cardComponent'])[1]//div[contains(@class,'body-1')]/p");
	protected By travelCardsCTA = By.xpath(/* "//div[@gtm_component='two-columns-container']//a" */
			"((//div[contains(text(),'Partner Offers')])[1]//following::div[@class='cardComponent'])[1]//div[contains(@class,'body-1-link')]");

	// owner
	protected By ownersNav = By.xpath(/* "//div[@class='wyn-fly-out']//ul/li//li[contains(.,'Owners')]/a" */
			"//div[contains(@class,'global-navigation')]//li/a[text()='Owner Travel Deals']");
	protected By ownerHeader = By.xpath("//h1[contains(.,'Travel Deals')]/parent::div");
	protected By ownerHeroImage = By.xpath(/*
											 * "//h1[contains(.,'Travel Deals')]/ancestor::div[@class='wyn-hero wyn-hero--full']//img"
											 */
			// "//div[@id='owner']//div[@aria-hidden='false']//div[@class='cardBanner
			// carousel']//div[@class='title-1' and contains(text(),'Club
			// Wyndham')]//..//..//..//../div[@class='image ']");
			// "//div[@id='owner']//div[@aria-hidden='false']//div[@class='title-1' and
			// contains(text(),'Club Wyndham')]//..//..//..//../div[@class='image ']");
			/* "//div[@class='title-1' and contains(text(),'HOTTEST')]" */
			"(//div[@class='caption-1'])[3]/../../../../..//div[@class='cardBanner']//img");
	// protected By ownerDesc =
	// By.xpath("//div[@id='owner']//div[@aria-hidden='false']//div[@class='cardBanner
	// carousel']//div[@class='title-1' and contains(text(),'Club
	// Wyndham')]/../div[@class='subtitle-1']/p");
	protected By ownerSubHeader = By.xpath("(//div[@class='caption-1'])[3]/..//div[@class='title-1']");
	protected By fallSaleBreadCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Fall Sale')]");

	protected By ownerDesc = By.xpath(// "//div[@id='owner']//div[@aria-hidden='false']//div[@class='title-1' and
										// contains(text(),'Club Wyndham')]/../div[@class='subtitle-3']/p");
			/* "//p[contains(text(),'Some of')]" */
			"(//div[@class='caption-1'])[3]/..//div[@class='body-1']");
	protected By ownerBookNowCTA = By.xpath("(//div[@class='caption-1'])[3]/..//a");

	// protected By ownerSubDesc =
	// By.xpath("//div[@id='owner']//div[@aria-hidden='false']//div[@class='cardBanner
	// carousel']//div[@class='title-1' and contains(text(),'Club
	// Wyndham')]/../div[@class='body-1']/p");

	protected By ownerSubDesc = By.xpath(
			"//div[@id='owner']//div[@aria-hidden='false']//div[@class='title-1' and contains(text(),'Club Wyndham')]/../div[@class='body-1']/p");

	protected By owerBrdCrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Owner')]");
	protected By resortSpecialHdr = By
			.xpath("//div[contains(@class,'caption-1') and contains(text(),'RESORT SPECIALS')]");

	protected By slickBuutons = By.xpath("//ul[@class='slick-dots']//button");
	protected By nextArrow = By.xpath(/* "//button[@class='slick-next slick-arrow']" */
			"//div[@id='owner']//..//div[contains(@class,'slick-button-next')]");
	protected By prevArrow = By.xpath("//button[@class='slick-prev slick-arrow']");
	protected By slickDotsActive = By.xpath("//ul[@class='slick-dots']/li");
	protected By carouselTitle = By
			.xpath("//div[@class='banner-carousel']//div[@class='slick-slide slick-current slick-active']//h2");
	protected By carouselAddress = By
			.xpath("//div[@class='banner-carousel']//div[@class='slick-slide slick-current slick-active']//h3");
	protected By carouselImage = By
			.xpath("//div[@class='banner-carousel']//div[@class='slick-slide slick-current slick-active']//img");
	protected By carouselExploreBtn = By.xpath(
			"//div[@class='banner-carousel']//div[@class='slick-slide slick-current slick-active']//a[contains(.,'Book')]");
	protected By exclusivePntsDis = By.xpath(/* "//h2[contains(.,'Exclusive Points Discount')]" */
			"//h3[contains(.,'EXCLUSIVE POINTS DISCOUNTS')]");
	protected By exclusivePointsDec = By.xpath(/*
												 * "//h2[contains(.,'Exclusive Points Discount')]/ancestor::div[@class='icon-list']//div/p"
												 */
			"//div[contains(text(),'RESORT SPECIALS')]/../../../../..//div[contains(@class,'body-1')]/p");
	protected By regionTabList = By.xpath(/* "//div[@class='tabs']//li/a" */
			"//div[contains(@class,'tabs')]//..//li/a");
	protected By activeTabs = By.xpath(/* "//div[@class='tabs']//li" */
			"//div[contains(@class,'tabs')]//..//li");
	protected By caraouselVal = By.xpath("//div[@class='banner-carousel']");

	protected By tabsCards = By.xpath(/* "//div[@class='wyn-tabs__content is-active ']//h3" */
			/*
			 * "(//div[@class='tabs-content']//div[contains(@class,'cardComponent')])[1]//img"
			 */
			"//div[contains(@class,'tabs-panel is-active') and contains(@role,'tabpanel')]//img");
	protected By tabsCardsImage = By.xpath(/* "//div[@class='wyn-tabs__content is-active ']//img" */
			/*
			 * "(//div[@class='tabs-content']//div[contains(@class,'cardComponent')])[1]//img"
			 */
			"//div[contains(@class,'tabs-panel is-active') and contains(@role,'tabpanel')]//img");
	protected By tabsCardsLocation = By.xpath(/* "//div[@class='wyn-tabs__content is-active ']//h4/p" */
			"//div[contains(@class,'tabs-panel is-active') and contains(@role,'tabpanel')]//div[contains(@class,'subtitle-2')]");
	protected By tabsCardsDesc = By.xpath(/* "//div[@class='wyn-tabs__content is-active ']//div/p" */
			/*
			 * "(//div[@class='tabs-content']//div[contains(@class,'cardComponent')])[1]//div[contains(@class,'subtitle-3')]"
			 */
			"//div[contains(@class,'tabs-panel is-active') and contains(@role,'tabpanel')]//div[contains(@class,'subtitle-3')]");
	protected By cardSubDescription = By.xpath(
			"//div[contains(@class,'tabs-panel is-active') and contains(@role,'tabpanel')]//div[@class='body-1']/p");
	protected By tabsCardsBookNowBtn = By.xpath(
			/*
			 * "//div[@class='wyn-tabs__content is-active ']//div[@class='wyn-card__cta']/div[contains(.,'Book Now')]"
			 */
			// "(//div[@class='tabs-content']//div[contains(@class,'cardComponent')])[1]//div[@class='body-1-link']");
			"//div[contains(@class,'tabs-panel is-active') and contains(@role,'tabpanel')]//div[contains(@class,'body-1-link')]");

	protected By twoColumnImage = By.xpath("//div[@class='two-columns-container']//img");
	protected By twoColumnHeader = By.xpath("//div[@class='two-columns-container']//h2");
	protected By twoColoumnDesc = By.xpath("//div[@class='two-columns-container']//p");
	protected By twoColoumnCTA = By.xpath("//div[@class='two-columns-container']//a");

	protected By twoColumnVal = By.xpath("//div[@class='cardComponent']//img");
	protected By twoCoumnHeader = By.xpath(
			"//div[@class='cardComponent']//img//..//..//div[@class='card-section']//div[contains(@class,'subtitle-2')]");
	// protected By readMoreLink =
	// By.xpath("//div[@class='cardComponent']//img//..//..//div[@class='card-section']//a//div[@class='body-1-link']");

	protected By readMoreLink = By.xpath(
			"//div[@class='cardComponent']//img//..//..//div[@class='card-section']//a//div[contains(@class,'body-1-link')]");

	// nonOwners

	protected By nonOwnersNav = By.xpath(/* "//div[@class='wyn-fly-out']//ul/li//li[contains(.,'Non-owner')]/a" */
			"//div[contains(@class,'global-navigation')]//li/a[text()='Non-Owner Travel Deals']");
	protected By nonOwnerHeader = By.xpath(/* "//h1[contains(.,'Travel Deals')]/parent::div" */
			"(//div[@class='caption-1' and contains(text(),'Non-Owner')])[1]");
	protected By nonOwnerHeroImage = By.xpath(/*
												 * "//h1[contains(.,'Travel Deals')]/ancestor::div[@class='wyn-hero wyn-hero--full']//img"
												 */
			"(//div[@class='caption-1' and contains(text(),'Non-Owner')])[1]/ancestor::div[@class='cardBanner']//img");
	protected By nonOwnerBrdCrumb = By.xpath(/* "//div[@class='breadcrumb']//a[contains(.,'Non-owners')]" */
			"//div[@class='breadcrumb']//a[contains(.,'Non-Owner')]");
	protected By readyToStartHdr = By.xpath("//h2[contains(.,'Ready to start')]");
	protected By exploreBucketSubHdr = By.xpath("//h3[contains(.,'Explore Bucket')]");
	protected By nonOwnerCardsImage = By.xpath(/* "//div[@class='cards']//img" */
			"//div[contains(@class,'card')]//a[@data-eventname='Card']//img");
	protected By nonOwnerCardsHdr = By.xpath(/* "//div[@class='cards']//h3" */
			"//div[contains(@class,'card')]//a[@data-eventname='Card']//..//div[@class='card-section']//div[contains(@class,'subtitle-2')]");
	protected By nonOwnerCardsDesc = By.xpath(/* "//div[@class='cards']//p" */
			"//div[contains(@class,'card')]//a[@data-eventname='Card']//..//div[@class='card-section']//div[contains(@class,'body-1')]/p");
	protected By nonOwnerCardsCta = By.xpath(/* "//div[@class='cards']//div[@class='wyn-card__cta']/div" */
			"//div[contains(@class,'card')]//a[@data-eventname='Card']//..//div[@class='card-section']//div[contains(@class,'body-1-link')]");

	protected By nonOwnerBannerHdr = By.xpath(/* "//div[@class='banner-carousel']//h2" */
			"//section[contains(@class,'banner')]//div[contains(@class,'caption-1') and contains(text(),'Extra')]");
	protected By nonOwnerBannerImag = By.xpath(/* "//div[@class='banner-carousel']//img" */
			"//section[contains(@class,'banner')]//div[contains(@class,'caption-1') and contains(text(),'Extra')]/ancestor::div[@class='cardBanner']//img");
	protected By nonOwnerBannerDesc = By.xpath(/*
												 * "//div[@class='banner-carousel']//div[@class='wyn-l-content wyn-card__copy']//p"
												 */
			"//section[contains(@class,'banner')]//div[contains(@class,'caption-1') and contains(text(),'Extra')]/../div[@class='body-1']/p");
	protected By nonOwnerBannerCTA = By.xpath(/* "//div[@class='banner-carousel']//a" */
			"//section[contains(@class,'banner')]//div[contains(@class,'caption-1') and contains(text(),'Extra')]/../a");

	/*
	 * Method: travelDealsPageNav Description: Travel Deals Page Nav Date: Jul/2019
	 * Author: Unnat Jain Changes By:
	 */
	public void travelDealsPageNav() {

		waitUntilElementVisibleBy(driver, dealsOffersHeader, 120);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(dealsOffersHeader)).build().perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(travelDealNav)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageNav", Status.PASS,
					"Travel Deals present in Sub Nav");
			driver.findElement(travelDealNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageNav", Status.FAIL,
					"Travel Deals not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, travelDealsHdr, 120);

		if (verifyObjectDisplayed(travelDealsHdr)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageNav", Status.PASS,
					"Travel Deals Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageNav", Status.FAIL,
					"Travel Deals Page Navigation failed");
		}

	}

	/*
	 * Method: travelDealsPageVal Description: Travel Deals Page valdations Date:
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void travelDealsPageVal() {
		if (verifyObjectDisplayed(travelDealsHdr)) {

			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.PASS,
					"Travel Deals header present as " + getElementText(travelDealsHdr));

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.FAIL,
					"Travel Deals header not present");
		}

		if (verifyObjectDisplayed(travelSubHdr)) {

			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.PASS,
					"Travel Deals subheader present as " + getElementText(travelSubHdr));

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.FAIL,
					"Travel Deals subheader not present");
		}

		if (verifyObjectDisplayed(travelImage)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.PASS,
					"Travel Deals  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.FAIL,
					"Travel Deals  page hero Image not present");
		}

		if (verifyObjectDisplayed(travelBrdcrmbs)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.PASS,
					"Travel Deals Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.FAIL,
					"Travel Deals Breadcrumb not present");
		}

		if (verifyObjectDisplayed(travelDesc)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.PASS,
					"Travel Deals Description presents");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "travelDealsPageVal", Status.FAIL,
					"Travel Deals Description not  presents");
		}

	}

	/*
	 * Method: travelCardsVal Description: Travel Cards validations Date: Jul/2019
	 * Author: Unnat Jain Changes By
	 */
	public void travelCardsVal() {

		try {
			List<WebElement> cardsImageList = driver.findElements(travelCardsImg);
			List<WebElement> cardsHdrsList = driver.findElements(travelCardshdr);
			List<WebElement> cardsDescList = driver.findElements(travelCardsDesc);
			// List<WebElement> cardsLearnMoreList =
			// driver.findElements(rciLearnMore);

			tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.PASS,
					"Travel Deals cards present Total: " + cardsHdrsList.size());

			for (int i = 0; i < cardsHdrsList.size(); i++) {
				getElementInView(cardsHdrsList.get(i));
				tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.PASS,
						"Card No " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

				if (verifyElementDisplayed(cardsImageList.get(i))) {
					tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.PASS,
							"Image present in " + (i + 1) + " content block");
				} else {
					tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.FAIL,
							"Image not present in " + (i + 1) + " content block");
				}

				if (verifyElementDisplayed(cardsDescList.get(i))) {
					tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.PASS,
							"Description present in " + (i + 1) + " content block");
				} else {
					tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.FAIL,
							"Description not present in " + (i + 1) + " content block");
				}

			}

			for (int j = 0; j < cardsHdrsList.size(); j++) {
				List<WebElement> cardsLearnMoreList1 = driver.findElements(travelCardsCTA);
				if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {

					clickElementJSWithWait(cardsLearnMoreList1.get(j));

					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (driver.getCurrentUrl().toUpperCase()
							.contains(testData.get("header" + (j + 1) + "").toUpperCase())) {

						tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.PASS,
								"Navigated to " + (j + 1) + " page");

					} else {
						tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.FAIL,
								"Navigated to " + (j + 1) + " page failed");
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, travelDealsHdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.FAIL,
							" CTA link not present in the content for " + (j + 1) + " block");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenTravelpage", "travelDealsCardsVal", Status.FAIL,
					"Travel Deals Cards are not present in the page");
		}
	}

	/*
	 * Method: ownerPageNav Description: Owner page navigation Date: Jul/2019
	 * Author: Unnat Jain Changes By
	 */
	public void ownerPageNav() {

		waitUntilElementVisibleBy(driver, dealsOffersHeader, 120);
		hoverOnElement(dealsOffersHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(ownersNav)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageNav", Status.PASS, "Owner present in Sub Nav");
			driver.findElement(ownersNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageNav", Status.FAIL,
					"Owner not present in sub nav");
		}

		// waitUntilElementVisibleBy(driver, ownerHeader, 120);
		waitForSometime(tcConfig.getConfig().get("LongWait"));
		if (driver.getCurrentUrl().toUpperCase().contains(testData.get("strSearch").toUpperCase())) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageNav", Status.PASS, "Owner Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageNav", Status.FAIL,
					"Owner Page Navigation failed");
		}

	}

	/*
	 * Method: ownerPageVal Description: Owner Page Validations Date: Jul/2019
	 * Author: Unnat Jain Changes By
	 */
	public void ownerPageVal() {

		/*
		 * if (verifyObjectDisplayed(ownerHeader)) {
		 * 
		 * tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.PASS,
		 * "Owner Travel Deals header present as " + getElementText(ownerHeader));
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal",
		 * Status.FAIL, "Owner Travel Deals header not present"); }
		 */

		waitUntilElementVisibleBy(driver, ownerHeroImage, 120);
		if (verifyObjectDisplayed(ownerHeroImage)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.PASS,
					"Owner Travel Deals  hero Image present");

			// clickElementBy(nextArrow);

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.FAIL,
					"Owner Travel Deals  page hero Image not present");
		}

		if (verifyObjectDisplayed(ownerSubHeader)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.PASS,
					"Owner Travel Deals  Sub Header present");
		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.FAIL,
					"Owner Travel Deals  Sub Header not present");
		}

		if (verifyObjectDisplayed(ownerDesc)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.PASS,
					"Owner Travel Deals  Description present");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.FAIL,
					"Owner Travel Deals  Description not present");
		}

		if (verifyObjectDisplayed(ownerBookNowCTA)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.PASS, "BOOK Now CTA is present");
			String getCTAAttribute = getElementAttribute(ownerBookNowCTA, "href");
			clickElementJSWithWait(ownerBookNowCTA);
			waitUntilObjectVisible(driver, fallSaleBreadCrumb, 120);
			assertTrue(getElementAttribute(fallSaleBreadCrumb, "href").contains(getCTAAttribute),
					"Page is not navigated Successfully");
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.PASS,
					"Fall Sale Page is navigated Successfully");
			driver.navigate().back();
			waitUntilObjectClickable(driver, owerBrdCrumb, 120);

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.FAIL,
					"BOOK Now CTA is not present");
		}

		if (verifyObjectDisplayed(owerBrdCrumb)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.PASS,
					"Owner Travel Deals Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerPageVal", Status.FAIL,
					"Owner Travel Deals Breadcrumb not present");
		}

	}

	/*
	 * Method: ownerCarouselVal Description: Owner Carousel validations Date:
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void ownerCarouselVal() {
		if (verifyObjectDisplayed(caraouselVal)) {
			getElementInView(caraouselVal);
			tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
					"Carousel Present on the homepage");

			List<WebElement> slickBtnList = driver.findElements(slickDotsActive);

			for (int i = 0; i < slickBtnList.size(); i++) {
				if (slickBtnList.get(i).getAttribute("class").contains("active")) {
					tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
							(i + 1) + " Card is active in Carousel with title: " + getElementText(carouselTitle));
					String title = getElementText(carouselTitle).toUpperCase();
					if (verifyObjectDisplayed(carouselImage)) {
						tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
								"Image present");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
								"Image not present");
					}

					if (verifyObjectDisplayed(carouselAddress)) {
						tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
								"Address present " + getElementText(carouselAddress));
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
								"Address not present");
					}

					if (verifyObjectDisplayed(carouselExploreBtn)) {
						tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
								"Book button present");
						if (i == slickBtnList.size() - 1) {
							clickElementBy(carouselExploreBtn);

							waitForSometime(tcConfig.getConfig().get("LongWait"));
							// Updated on 1128 as per Prod content
							ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
							// driver.switchTo().window(tabs2.get(1));
							waitForSometime(tcConfig.getConfig().get("LongWait"));

							if (tabs2.size() > 1) {
								driver.switchTo().window(tabs2.get(1));

								if (driver.getCurrentUrl().toUpperCase()
										.contains(testData.get("subHeader1").toUpperCase())) {
									tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
											"Navigated to My Club Wyndham Login Page with title " + driver.getTitle());
									waitForSometime(tcConfig.getConfig().get("LowWait"));
								} else {
									tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
											"My Club Wyn Login page navigation failed");
								}
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								// driver.navigate().back();
								driver.close();
								driver.switchTo().window(tabs2.get(0));
								driver.navigate().refresh();
								waitForSometime(tcConfig.getConfig().get("LowWait"));

							} else if (tabs2.size() == 1) {
								if (driver.getCurrentUrl().toUpperCase()
										.contains(testData.get("subHeader1").toUpperCase())) {
									tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.PASS,
											"Navigated to My Club Wyndham Login Page with title " + driver.getTitle());
									waitForSometime(tcConfig.getConfig().get("LowWait"));
								} else {
									tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
											"My Club Wyn Login page navigation failed");
								}
								waitForSometime(tcConfig.getConfig().get("LowWait"));
								driver.navigate().back();
								// driver.close();

								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}

							waitUntilElementVisibleBy(driver, ownerHeader, 120);

						}

						clickElementBy(nextArrow);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
								"Explore resort btn  not present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
							"First Card is not active in Carousel");
				}

			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
					"Carousel not  Present on the homepage");
		}

	}

	/*
	 * Method: ownerSlidingTabsVal Description: Owner Sliding Tabs Validations Date:
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void ownerSlidingTabsVal() {
		getElementInView(resortSpecialHdr);
		if (verifyObjectDisplayed(resortSpecialHdr)) {

			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
					"Resort Special header present as " + getElementText(resortSpecialHdr));

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
					"Resort Special header not present");
		}

		if (verifyObjectDisplayed(exclusivePntsDis)) {

			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
					"Exclusive Points header present as " + getElementText(exclusivePntsDis));

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
					"Exclusive Points header not present");
		}

		if (verifyObjectDisplayed(exclusivePointsDec)) {

			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
					"Exclusive Points Desc present");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
					"Exclusive Points Desc not present");
		}

		List<WebElement> slidingTabsList = driver.findElements(regionTabList);

		try {

			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
					"There are " + slidingTabsList.size() + " tabs in the page");
			for (int i = 0; i < slidingTabsList.size(); i++) {
				slidingTabsList.get(i).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> activeTabsList = driver.findElements(activeTabs);
				if (activeTabsList.get(i).getAttribute("class").contains("active")) {
					tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
							(i + 1) + " Active Tab Title is " + slidingTabsList.get(i).getText());

					List<WebElement> totalCards = driver.findElements(tabsCards);
					List<WebElement> cardsImagesList = driver.findElements(tabsCardsImage);
					List<WebElement> cardsLocationList = driver.findElements(tabsCardsLocation);
					List<WebElement> cardsDescList = driver.findElements(tabsCardsDesc);
					List<WebElement> cardsSubDescList = driver.findElements(cardSubDescription);
					List<WebElement> cardsCTAList = driver.findElements(tabsCardsBookNowBtn);

					tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
							"Toatl Cards in this active tab is " + totalCards.size());

					if (totalCards.size() == cardsImagesList.size()) {

						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
								"Image present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
								"Image not present in all cards");
					}

					if (totalCards.size() == cardsLocationList.size()) {

						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
								"Location present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
								"Location not present in all cards");
					}

					if (totalCards.size() == cardsDescList.size()) {

						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
								"Description present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
								"Description not present in all cards");
					}

					if (totalCards.size() == cardsSubDescList.size()) {

						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
								"Sub Description present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
								"Sub Description not present in all cards");
					}

					if (totalCards.size() == cardsCTAList.size()) {

						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.PASS,
								"Book Now Button present in all cards");

					} else {
						tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
								"Book Now Button not present in all cards");
					}

				} else {
					tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
							"Error in clicking tabs");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerSlidingTabsVal", Status.FAIL,
					"Error in validating Sliding Tabs");
		}

	}

	/*
	 * Method: ownerTwoCardsVal Description: Owner Two Cards Validations Date:
	 * 02/Aug/2019 Author: Unnat Jain Changes By
	 */
	public void ownerTwoCardsVal() {

		try {
			List<WebElement> cardsImageList = driver.findElements(twoColumnImage);
			List<WebElement> cardsHdrsList = driver.findElements(twoColumnHeader);
			List<WebElement> cardsDescList = driver.findElements(twoColoumnDesc);
			// List<WebElement> cardsLearnMoreList =
			// driver.findElements(rciLearnMore);
			getElementInView(cardsHdrsList.get(0));

			tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.PASS,
					"Owner Two Content Blocks present Total: " + cardsHdrsList.size());

			for (int i = 0; i < cardsHdrsList.size(); i++) {
				getElementInView(cardsHdrsList.get(i));
				tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.PASS,
						"Two Content Block " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

				if (verifyElementDisplayed(cardsImageList.get(i))) {
					tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.PASS,
							"Image present in " + (i + 1) + " content block");
				} else {
					tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.FAIL,
							"Image not present in " + (i + 1) + " content block");
				}

				if (verifyElementDisplayed(cardsDescList.get(i))) {
					tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.PASS,
							"Description present in " + (i + 1) + " content block");
				} else {
					tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.FAIL,
							"Description not present in " + (i + 1) + " content block");
				}

			}

			for (int j = 0; j < cardsHdrsList.size(); j++) {
				List<WebElement> cardsLearnMoreList1 = driver.findElements(twoColoumnCTA);
				if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {

					cardsLearnMoreList1.get(j).click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));

					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));

					if (driver.getCurrentUrl().toUpperCase()
							.contains(testData.get("header" + (j + 1) + "").toUpperCase())) {

						tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.PASS,
								"Navigated to " + (j + 1) + " page");

					} else {
						tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.FAIL,
								"Navigated to " + (j + 1) + " page failed");
					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

					waitUntilElementVisibleBy(driver, ownerHeader, 120);

				} else {
					tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.FAIL,
							" CTA link not present in the content for " + (j + 1) + " block");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenTravelPage", "ownerTwoCardsVal", Status.FAIL,
					"Owner Cards are not present in the page");
		}

	}

	/*
	 * Method: nonOwnerPageNav Description: Non Owner page navigation Date: Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void nonOwnerPageNav() {

		waitUntilElementVisibleBy(driver, dealsOffersHeader, 120);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(dealsOffersHeader)).build().perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(nonOwnersNav)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageNav", Status.PASS,
					"Non Owner present in Sub Nav");
			driver.findElement(nonOwnersNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageNav", Status.FAIL,
					"Non Owner not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, nonOwnerHeader, 120);

		if (verifyObjectDisplayed(nonOwnerHeader)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageNav", Status.PASS,
					"Non Owner Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageNav", Status.FAIL,
					"Non Owner Page Navigation failed");
		}

	}

	/*
	 * Method: nonOwnerPageVal Description: Non Owner Page Validations Date:
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void nonOwnerPageVal() {

		if (verifyObjectDisplayed(nonOwnerHeader)) {

			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal", Status.PASS,
					"Non Owner Travel Deals header present as " + getElementText(nonOwnerHeader));

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal", Status.FAIL,
					"Non Owner Travel Deals header not present");
		}

		if (verifyObjectDisplayed(nonOwnerHeroImage)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal", Status.PASS,
					"Non Owner Travel Deals  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal", Status.FAIL,
					"Non Owner Travel Deals  page hero Image not present");
		}

		if (verifyObjectDisplayed(nonOwnerBrdCrumb)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal", Status.PASS,
					"Non Owner Travel Deals Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal", Status.FAIL,
					"Non Owner Travel Deals Breadcrumb not present");
		}

		/*
		 * if (verifyObjectDisplayed(readyToStartHdr)) {
		 * tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal",
		 * Status.PASS, "Ready To Start header present as " +
		 * getElementText(readyToStartHdr));
		 * 
		 * if (verifyObjectDisplayed(exploreBucketSubHdr)) {
		 * tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal",
		 * Status.PASS, "Sub header present as " + getElementText(exploreBucketSubHdr));
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal",
		 * Status.FAIL, "Sub Header not present"); }
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerPageVal",
		 * Status.FAIL, "Ready To Start Header not present"); }
		 */

	}

	protected By extraHolidaysImage = By.xpath(
			"//div[@gtm_component='column']//div[@class='imageSlice']//div[contains(@class,'show-for-large')]//img[contains(@src,'extra')]");
	protected By suiteDealsHeader = By
			.xpath("//div[contains(@class,'title-1') and contains(text(),'Suite Deals On Great Stays')]");
	protected By suiteDealsDesc = By.xpath(
			"//div[contains(@class,'title-1') and contains(text(),'Suite Deals On Great Stays')]/../div[contains(@class,'body-1')]");
	protected By suiteDealsDescLink = By.xpath(
			"//div[contains(@class,'title-1') and contains(text(),'Suite Deals On Great Stays')]/../div[contains(@class,'body-1')]//a");

	public void validateSuiteDealsSection() {
		assertTrue(verifyObjectDisplayed(suiteDealsHeader), "Suite Deals Header is not Displayed");
		tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateSuiteDealsSection", Status.PASS,
				"Suite Deals Header is Displayed");

		assertTrue(verifyObjectDisplayed(suiteDealsDesc), "Suite Deals Description is not Displayed");
		tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateSuiteDealsSection", Status.PASS,
				"Suite Deals Description is Displayed");

		List<WebElement> links = driver.findElements(suiteDealsDescLink);
		for (int i = 0; i < links.size(); i++) {
			clickElementBy(links.get(i));
			ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
			if (tabs.size() == 2) {
				driver.switchTo().window(tabs.get(1));
				if (driver.getCurrentUrl().contains(testData.get("subHeader1"))) {
					tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateSuiteDealsSection", Status.PASS,
							"Page Navigation is Successful");
					driver.close();
					driver.switchTo().window(tabs.get(0));
				} else {
					tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateSuiteDealsSection", Status.FAIL,
							"Page Navigation is Not Successful");
				}
			} else {
				if (driver.getCurrentUrl().contains(testData.get("subHeader2"))) {
					tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateSuiteDealsSection", Status.PASS,
							"Page Navigation is Successful");
					driver.navigate().back();
					waitUntilObjectVisible(driver, suiteDealsDesc, 120);
				} else {
					tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateSuiteDealsSection", Status.FAIL,
							"Page Navigation is Not Successful");
				}
			}
		}
	}

	public void validateExtraHolidaysImage() {
		getElementInView(extraHolidaysImage);
		if (verifyObjectDisplayed(extraHolidaysImage)) {
			tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateExtraHolidaysImage", Status.PASS,
					"Extra Holidays Image is Displayed");
		} else {
			tcConfig.updateTestReporter("NextGenNonOwnerPage", "validateExtraHolidaysImage", Status.FAIL,
					"Extra Holidays Image is not Displayed");
		}
	}

	/*
	 * Method: nonOwnerCardsVal Description: Non owner Cards Validations Date:
	 * 02/Aug/2019 Author: Unnat Jain Changes By
	 */
	public void nonOwnerCardsVal() {

		try {
			List<WebElement> cardsImageList = driver.findElements(nonOwnerCardsImage);
			List<WebElement> cardsHdrsList = driver.findElements(nonOwnerCardsHdr);
			List<WebElement> cardsDescList = driver.findElements(nonOwnerCardsDesc);
			// List<WebElement> cardsLearnMoreList =
			// driver.findElements(rciLearnMore);
			getElementInView(cardsHdrsList.get(0));

			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.PASS,
					"Non Owner Cards Section present Total: " + cardsHdrsList.size());

			for (int i = 0; i < cardsHdrsList.size(); i++) {
				getElementInView(cardsHdrsList.get(i));
				tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.PASS,
						"Card No " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

				if (verifyElementDisplayed(cardsImageList.get(i))) {
					tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.PASS,
							"Image present in " + (i + 1) + " card");
				} else {
					tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.FAIL,
							"Image not present in " + (i + 1) + " card");
				}

				if (verifyElementDisplayed(cardsDescList.get(i))) {
					tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.PASS,
							"Description present in " + (i + 1) + " card");
				} else {
					tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.FAIL,
							"Description not present in " + (i + 1) + " card");
				}

			}

			for (int j = 0; j < cardsHdrsList.size(); j++) {
				List<WebElement> cardsLearnMoreList1 = driver.findElements(nonOwnerCardsCta);
				if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {

					cardsLearnMoreList1.get(j).click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
					if (tabs.size() == 2) {
						driver.switchTo().window(tabs.get(1));
						if (driver.getCurrentUrl().toUpperCase().contains(testData.get("subHeader1").toUpperCase())) {

							tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.PASS,
									"Navigated to " + (j + 1) + " page");
							driver.close();
							driver.switchTo().window(tabs.get(0));

						} else {
							tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.FAIL,
									"Navigated to " + (j + 1) + " page failed");
						}
					}
					waitUntilElementVisibleBy(driver, nonOwnerHeader, 120);

				} else {
					tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.FAIL,
							" CTA link not present in the card for " + (j + 1) + " block");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerTwoCardsVal", Status.FAIL,
					"Owner Cards are not present in the page");
		}

	}

	/*
	 * Method: nonOwnerBannerVal Description: Non owner Banner Validations Date:
	 * 02/Aug/2019 Author: Unnat Jain Changes By
	 */
	public void nonOwnerBannerVal() {
		getElementInView(nonOwnerBannerHdr);

		if (verifyObjectDisplayed(nonOwnerBannerHdr)) {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.PASS,
					"Non Owner Banner Present in the page with header " + getElementText(nonOwnerBannerHdr));

			if (verifyObjectDisplayed(nonOwnerBannerImag)) {
				tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.PASS,
						"Non Owner Image Present in the page ");

			} else {
				tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.FAIL,
						"Non Owner Image not Present in the page");
			}

			if (verifyObjectDisplayed(nonOwnerBannerDesc)) {
				tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.PASS,
						"Non Owner Desc Present in the page ");

			} else {
				tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.FAIL,
						"Non Owner Desc not Present in the page");
			}

			if (verifyObjectDisplayed(nonOwnerBannerCTA)) {
				tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.PASS,
						"Non Owner CTA Present in the page ");
				clickElementBy(nonOwnerBannerCTA);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
				if (tabs.size() == 2) {
					driver.switchTo().window(tabs.get(1));
					if (driver.getCurrentUrl().toUpperCase().contains(testData.get("subHeader1").toUpperCase())) {

						tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.PASS,
								"Navigated to page");
						driver.close();
						driver.switchTo().window(tabs.get(0));

					} else {
						tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.FAIL,
								"Navigated to page failed");
					}
				}

				waitUntilElementVisibleBy(driver, nonOwnerHeader, 120);

			} else {
				tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.FAIL,
						"Non Owner CTA not Present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenTravelPage", "nonOwnerBannerVal", Status.FAIL,
					"Non Owner Banner not Present in the page");
		}

	}

	public void twoContainerValidations() {

		getElementInView(twoColumnVal);

		if (verifyObjectDisplayed(twoColumnVal)) {
			tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
					"Two Coloumn section present in the page");
			List<WebElement> sectionHeader = driver.findElements(twoCoumnHeader);
			// List<WebElement> readMoreList= driver.findElements(readMoreLink);
			int totalSize = sectionHeader.size();
			for (int i = 0; i < totalSize; i++) {
				List<WebElement> sectionHeader1 = driver.findElements(twoCoumnHeader);
				List<WebElement> readMoreList = driver.findElements(readMoreLink);
				tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
						(i + 1) + " header is: " + sectionHeader1.get(i).getText());

				readMoreList.get(i).click();

				waitForSometime(tcConfig.getConfig().get("LongWait"));
				if (driver.getTitle().toUpperCase().contains(testData.get("header" + (i + 1) + "").toUpperCase())) {

					tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
							"Navigated to " + (i + 1) + " page");

					driver.navigate().back();
					waitUntilElementVisibleBy(driver, twoColumnVal, 120);

				}

			}

		} else {
			tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.FAIL,
					"Two Coloumn section not present in the page");
		}

	}

}
