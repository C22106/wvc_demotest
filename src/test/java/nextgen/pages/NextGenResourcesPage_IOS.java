package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenResourcesPage_IOS extends NextGenResourcesPage_Web {

	public static final Logger log = Logger.getLogger(NextGenResourcesPage_IOS.class);

	public NextGenResourcesPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
