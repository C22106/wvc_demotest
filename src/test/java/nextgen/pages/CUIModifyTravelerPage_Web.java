package nextgen.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUIModifyTravelerPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUIModifyTravelerPage_Web.class);

	public CUIModifyTravelerPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	// CUIModifyReservationPage_Web modifyPage = new
	// CUIModifyReservationPage_Web(tcConfig);
	protected By errorMsg = By.xpath("//*[@id='error-message']/span/p");
	protected By guestRequiredCredit = By.xpath("//th[text() = 'Required']/../td");
	protected By guestAvailableCredit = By.xpath("//th[text() = 'Available']/../td");
	protected By textGuestCreditRequired = By.xpath("//p[contains(text(),'In order to have a guest check-in')]");
	protected By fieldGuestFirstName = By.xpath("//input[@id = 'guestFirstName']");
	protected By fieldGuestLastName = By.xpath("//input[@id = 'guestLastName']");
	protected By fieldGuestEmail = By.xpath("//input[@id = 'guestEmail']");
	protected By fieldGuestFlag = By.xpath("//div[@class = 'flag-select']");
	protected By fieldGuestPhone = By.xpath("//input[@id = 'guestPhone']");
	protected By fieldGuestCountry = By.xpath("//select[@id = 'guestCountry']");
	protected By fieldGuestAddress = By.xpath("//input[@id = 'guestAddress']");
	protected By fieldGuestCity = By.xpath("//input[@id = 'guestCity']");
	protected By fieldGuestState = By.xpath("//select[@id = 'guestState']");
	protected By fieldGuestZip = By.xpath("//input[@id = 'guestZip']");
	protected By guestAgree = By.xpath("//label[contains(@for,'agreeGuest')]");
	protected By buttonContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By stepsModify = By.xpath("//ol[@class = 'booking-header__desktop__steps']");
	protected By stepModifyTraveler = By.xpath("//span[text() = 'Modify Traveler']");
	protected By headerModifyTraveler = By
			.xpath("(//h1[text() = 'Modify Traveler'] | //div[ contains(@class,'title') and contains(.,'Modify')])");
	protected By textWhoCheckin = By.xpath("//span[contains(text(),'Who will be checking in upon arrival')]");
	protected By radioOwnerTraveler = By.xpath("//label[@for = 'traveler_owner']");
	protected By radioGuestTraveler = By.xpath("//label[@for = 'traveler_guest']");
	protected By sectionTravelerOwner = By.xpath("//select[@id = 'travelerName']");
	protected By backModifyReservation = By.xpath("//a[text() = 'Back to Modify Reservation']");
	protected By textGuestInformation = By
			.xpath("//div[contains(text(),'guests are required to provide contact information')]");
	protected By tooltipGuestConfirmationFees = By.xpath("//span[@id = 'guestDisclaimerTooltip']");
	protected By tooltipText = By.xpath("//div[@role = 'tooltip']");
	protected By textGuestConfirmationCredit = By.xpath("//h1[text() = 'Guest Confirmation Credits']");
	protected By currentTraveler = By.xpath(
			"//h2[text() = 'Current Traveler']/../p/span | //span[text() = 'Traveler']/../..//p[contains(@class,'body')]");
	protected By modifyTravelerCTA = By.xpath("//a[contains(text(),'Modify Traveler')]");
	protected By modifyReservationPage = By.xpath("//div[@class = 'modify-reservation']");
	protected By labelTravelerName = By.xpath("//div[@class='steps__title' and contains(.,'Modify')]");
	
	protected By confirmChangesCTA = By.xpath("//button[contains(.,'Confirm Changes')]");
	protected By labelCongratulations = By.xpath("//div[@class='steps__confirmation__congrats_header text-primary' and contains(.,'Congratulations')]");

	/*
	 * Method: checkStepsModify Description:check Steps Modify Date: June/2020
	 * Author: Abhijeet Roy Changes By: NA
	 */
	public void checkStepsModify() {
		waitUntilElementVisibleBy(driver, stepsModify, 120);
		if (verifyObjectDisplayed(stepsModify)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkStepsModify", Status.PASS,
					"Total Steps is shown for Modify Reservation");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkStepsModify", Status.FAIL,
					"Total Steps not shown for Modify Reservation");
		}
	}

	/*
	 * Method: textStepModifyTraveler Description:text Step Modify Traveler Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textStepModifyTraveler() {
		waitUntilElementVisibleBy(driver, stepModifyTraveler, 120);
		if (verifyObjectDisplayed(stepModifyTraveler)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "textStepModifyTraveler", Status.PASS,
					"Step Modify Traveler Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "textStepModifyTraveler", Status.FAIL,
					"Step Modify Traveler not Present");
		}
	}

	/*
	 * Method: checkCurrentTraveler Description:check Current Traveler Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkCurrentTraveler() {
		waitUntilElementVisibleBy(driver, headerModifyTraveler, 120);
		getElementInView(headerModifyTraveler);
		Assert.assertTrue(verifyObjectDisplayed(headerModifyTraveler), "Modify Traveler header not present");
		if (verifyObjectDisplayed(currentTraveler)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkCurrentTraveler", Status.PASS,
					"Current Traveler is: " + getElementText(currentTraveler));
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkCurrentTraveler", Status.FAIL,
					"Current Traveler text not displayed");
		}
	}

	/*
	 * Method: textWhoWillBeCheckingIn Description:text Who Will Be CheckingIn Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void textWhoWillBeCheckingIn() {
		waitUntilElementVisibleBy(driver, textWhoCheckin, 120);
		if (verifyObjectDisplayed(textWhoCheckin)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "textWhoWillBeCheckingIn", Status.PASS,
					"Text: " + getElementText(textWhoCheckin) + " is Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "textWhoWillBeCheckingIn", Status.FAIL,
					"Text: " + getElementText(textWhoCheckin) + " not Present");
		}
	}

	/*
	 * Method: checkModifyOwnerGuestRadioPresenceDiscovery Description:check Modify
	 * Owner Guest Radio Presence Discovery Date: June/2020 Author: Abhijeet Roy
	 * Changes By: NA
	 */
	public void checkModifyOwnerGuestRadioPresenceDiscovery() {
		if (!verifyObjectDisplayed(radioOwnerTraveler)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.PASS,
					"Owner Radio button Not Present for discovery member");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.FAIL,
					"Owner Radio button Present for Discovery Member");
		}

		if (!verifyObjectDisplayed(radioGuestTraveler)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.PASS,
					"Guest Radio button Not Present for Discovery Member");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.FAIL,
					"Guest Radio button Present for Discovery Member");
		}
	}

	/*
	 * Method: checkModifyOwnerGuestRadioPresence Description:check Modify Owner
	 * Guest Radio Presence Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void checkModifyOwnerGuestRadioPresence() {
		if (verifyObjectDisplayed(radioOwnerTraveler)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.PASS,
					"Owner Radio button Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.FAIL,
					"Owner Radio button not Present");
		}

		if (verifyObjectDisplayed(radioGuestTraveler)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.PASS,
					"Guest Radio button Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "checkModifyOwnerGuestRadioPresence", Status.FAIL,
					"Guest Radio button not Present");
		}
	}

	/*
	 * Method: selectRadioModifyTraveler Description:select Radio Modify Traveler
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectRadioModifyTraveler() {
		String modifyTravelerTo = testData.get("ModifyTravelerTo");
		if (modifyTravelerTo.equalsIgnoreCase("Owner")) {
			clickElementJSWithWait(radioOwnerTraveler);
			tcConfig.updateTestReporter("CUIModifyTravelPage", "selectRadioModifyTraveler", Status.PASS,
					"Radio Button selected as Owner");
		} else if (modifyTravelerTo.equalsIgnoreCase("Guest")) {
			clickElementJSWithWait(radioGuestTraveler);
			tcConfig.updateTestReporter("CUIModifyTravelPage", "selectRadioModifyTraveler", Status.PASS,
					"Radio Button selected as Guest");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "selectRadioModifyTraveler", Status.FAIL,
					"Please provide valid data in excel i.e. Owner/Guest");
		}
	}

	/*
	 * Method: selectOwnerfromList Description:select Owner from List Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void selectOwnerfromList() {
		String modifyToOwner = testData.get("ModifyOwner");
		if (verifyObjectDisplayed(sectionTravelerOwner)) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			selectByText(sectionTravelerOwner, modifyToOwner);
			tcConfig.updateTestReporter("CUIModifyTravelPage", "sectionTravelerOwner", Status.PASS,
					"Owner changed to: " + modifyToOwner);
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "sectionTravelerOwner", Status.FAIL,
					"Field to change owner not found");
		}
	}

	/*
	 * Method: backToModifyReservation Description:back To Modify Reservation Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void backToModifyReservation() {
		if (verifyObjectDisplayed(backModifyReservation)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "backToModifyReservation", Status.PASS,
					"Link Back to Modify Reservation Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "backToModifyReservation", Status.FAIL,
					"Link Back to Modify Reservation Present");
		}
		getElementInView(backModifyReservation);
		clickElementJSWithWait(backModifyReservation);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		assertTrue(verifyObjectDisplayed(modifyReservationPage), "Not navigated back to modify traveler page");
		tcConfig.updateTestReporter("CUIModifyTravelPage", "clickModifyCTA", Status.PASS,
				"Navigated back to modify traveler page");

	}

	/*
	 * Method: validateSectionReservationSummary Description:validate Section
	 * Reservation Summary Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	/*
	 * public void validateSectionReservationSummary() {
	 * modifyPage.checkSectionReservationSummary();
	 * modifyPage.checkReservationSummaryDetails(); }
	 */
	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void clickContinueButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(buttonContinue);
		clickElementJSWithWait(buttonContinue);
	}

	/*
	 * Method: verifyGuestInformationText Description:verify Guest Information Text
	 * Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyGuestInformationText() {
		if (verifyObjectDisplayed(textGuestInformation)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "backToModifyReservation", Status.PASS,
					"Guest Information Text Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "backToModifyReservation", Status.FAIL,
					"Guest Information Text not Present");
		}
	}

	/*
	 * Method: verifytooltipGuestInformation Description:verify tooltip Guest
	 * Information Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	/*
	 * public void verifytooltipGuestInformation() {
	 * toolTipValidations(tooltipGuestConfirmationFees, "Guest Confirmation Fees",
	 * tooltipText); }
	 */

	/*
	 * Method: dataEntryForGuestOwner Description:data Entry For Guest Owner Date:
	 * June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void dataEntryForGuestOwner() {
		String guestFirstName = testData.get("guestFirstName");
		String guestLastName = testData.get("guestLastName");
		String guestEmail = testData.get("guestEmail");
		// String guestSelectFlag = testData.get("guestSelectFlag");
		String guestPhoneNumber = testData.get("guestPhoneNumber");
		String guestCountry = testData.get("guestCountry");
		String guestStreetAddress = testData.get("guestStreetAddress");
		String guestCity = testData.get("guestCity");
		String guestState = testData.get("guestState");
		String guestZipCode = testData.get("guestZipCode");

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (verifyObjectDisplayed(fieldGuestFirstName)) {
			fieldDataEnter(fieldGuestFirstName, guestFirstName);
			fieldDataEnter(fieldGuestLastName, guestLastName);
			fieldDataEnter(fieldGuestEmail, guestEmail);
			// clickElementJSWithWait(fieldGuestFlag);
			// clickElementJSWithWait(By.xpath("//div[@class =
			// 'flag-select__choices']//a[contains(text(),'"+guestSelectFlag+"')]"));
			fieldDataEnter(fieldGuestPhone, guestPhoneNumber);
			selectByText(fieldGuestCountry, guestCountry);
			fieldDataEnter(fieldGuestAddress, guestStreetAddress);
			fieldDataEnter(fieldGuestCity, guestCity);
			selectByText(fieldGuestState, guestState);
			fieldDataEnter(fieldGuestZip, guestZipCode);
			clickElementJSWithWait(guestAgree);
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "dataEntryForGuestOwner", Status.FAIL,
					"Fields for Guest data Entry not displayed");
		}
	}

	/*
	 * Method: verifyTextGuestConfirmationCredit Description:verify Text Guest
	 * Confirmation Credit Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyTextGuestConfirmationCredit() {
		if (verifyObjectDisplayed(textGuestConfirmationCredit)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyTextGuestConfirmationCredit", Status.PASS,
					"Guest Confirmation Credit Text Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyTextGuestConfirmationCredit", Status.FAIL,
					"Guest Confirmation Credit Text Not Present");
		}
	}

	/*
	 * Method: verifyGuestConfirmationCreditDetails Description:verify Guest
	 * Confirmation Credit Details Date: June/2020 Author: Abhijeet Roy Changes By:
	 * NA
	 */
	public void verifyGuestConfirmationCreditDetails() {
		if (verifyObjectDisplayed(guestRequiredCredit)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyGuestConfirmationCreditDetails", Status.PASS,
					"Guest Required Credit is: " + getElementText(guestRequiredCredit));
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyGuestConfirmationCreditDetails", Status.PASS,
					"Guest Required Credit Data not Present");
		}

		if (verifyObjectDisplayed(guestAvailableCredit)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyGuestConfirmationCreditDetails", Status.PASS,
					"Guest Available Credit is: " + getElementText(guestAvailableCredit));
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyGuestConfirmationCreditDetails", Status.PASS,
					"Guest Available Credit Data not Present");
		}
	}

	/*
	 * Method: verifyGuestCreditRequiredText Description:verify Guest Credit
	 * Required Text Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void verifyGuestCreditRequiredText() {
		if (verifyObjectDisplayed(textGuestCreditRequired)) {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyGuestCreditRequiredText", Status.PASS,
					"Guest Credit Required Text Present");
		} else {
			tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyGuestConfirmationCreditDetails", Status.PASS,
					"Guest Credit Required Text not Present");
		}
	}

	/*
	 * Method: verifyOverlappingErrorMsg Description: verifyOverlappingErrorMsg
	 * Date: June/2020 Author: Kamalesh Gupta Changes By: NA
	 */
	public void verifyOverlappingErrorMsg() {
		getElementInView(errorMsg);
		assertTrue(verifyObjectDisplayed(errorMsg), "Overlapping Error message not displayed");
		tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyOverlappingErrorMsg", Status.PASS,
				"Overlapping Error message displayed");
	}

	/*
	 * Method: clickModifyCTA Description:Click modify Button Date field Date:
	 * nov/2020 Author: Saket Sharma Changes By: NA
	 */
	public void clickModifyCTA() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(modifyTravelerCTA);
		clickElementJSWithWait(modifyTravelerCTA);
		pageCheck();
		assertTrue(verifyObjectDisplayed(backModifyReservation), "Not navigated to modify traveler page");
		tcConfig.updateTestReporter("CUIModifyTravelPage", "clickModifyCTA", Status.PASS,
				"Navigated to modify traveler page");
	}
	
	/**
	 * @author c40118
	 * @category Validation
	 * Method: verifyLableModifyTraveler Description:verify Label Modify Traveler
	 * Date field Date: nov/2020 Changes By: NA
	 */
	public void verifyLabelModifyTraveler() {
		getElementInView(labelTravelerName);
		assertTrue(verifyObjectDisplayed(labelTravelerName), "Unable to navigate to Modify owner page");
		tcConfig.updateTestReporter("CUIModifyTravelPage", "verifyLableModifyTraveler", Status.PASS,
				"Navigated to modify traveler  page");
	}
	
	/**
	 * @author c40118
	 * @category Validation
	 * Method: verifyCompleteModification Description:Click CompleteModification Button
	 * Date field Date: nov/2020 Author: Saket Sharma Changes By: NA
	 */
	
	public void clickConfirmChangesCTA() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(confirmChangesCTA);
		clickElementJSWithWait(confirmChangesCTA);
		pageCheck();
		waitUntilElementVisibleBy(driver, labelCongratulations, 240);
		assertTrue(verifyObjectDisplayed(labelCongratulations), "Not navigated to Congratulations page");
		tcConfig.updateTestReporter("CUIModifyTravelPage", "clickConfirmChangesCTA", Status.PASS,
				"Navigated to Congratulations page");
	}

}
