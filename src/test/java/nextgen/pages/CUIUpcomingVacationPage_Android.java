package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIUpcomingVacationPage_Android extends CUIUpcomingVacationPage_Web {

	public static final Logger log = Logger.getLogger(CUIUpcomingVacationPage_Android.class);

	public CUIUpcomingVacationPage_Android(TestConfig tcconfig) {
		super(tcconfig);

	}

	public By upcomingVacationsCTA = By
			.xpath("(//span[@class='account-navigation__text'and text()='Upcoming Vacations'])[1]");

	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadAllReservations() {
		String reservationNumber = testData.get("ReservationNumber");
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber + "']");
		do {
			scrollDownByPixel(400);
			;
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} while (!verifyObjectDisplayed(modifyReservation));
	}

	/*
	 * Method: loadAllReservations Description:Load all Upcoming Reservations Date
	 * field Date: June/2020 Author: Abhijeet Roy Changes By: NA
	 */
	public void loadAllReservations(String reservationNumber) {
		By modifyReservation = By.xpath("//p[text() = '" + reservationNumber + "']");
		do {
			scrollDownByPixel(400);
			;
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} while (!verifyObjectDisplayed(modifyReservation));
	}

	/*
	 * Method: navigate to Upcoming Reservation Description:Load all Upcoming
	 * Reservations Date field Date: June/2020 Author: Saket Sharma Changes By: NA
	 */
	public void clickToUpcomingVacations() {

		waitUntilElementVisibleBy(driver, upcomingVacationsCTA, 120);
		if (verifyObjectDisplayed(upcomingVacationsCTA)) {
			clickElementBy(upcomingVacationsCTA);
			waitUntilElementVisibleBy(driver, labelUpcomingVacations, 120);
			Assert.assertTrue(verifyObjectDisplayed(labelUpcomingVacations));
			tcConfig.updateTestReporter("CUIUpcomingVacationPage", "clickToUpcomingVacations", Status.PASS,
					"User Navigated to Upcoming Vacations page");

		} else {
			tcConfig.updateTestReporter("CUIUpcomingVacationPage", "clickToUpcomingVacations", Status.FAIL,
					"User failed to Navigated to Upcoming Vacations page");
		}
	}

}
