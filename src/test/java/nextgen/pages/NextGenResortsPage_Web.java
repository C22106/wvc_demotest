package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenResortsPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenResortsPage_Web.class);

	public NextGenResortsPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By resortsHeader = By.xpath(/* "//div[@class='grid-container ']//a[@title='Resorts']" */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Resorts']");

	protected By resortsList = By.xpath(/* "//div[@class='media-object callout ']" */
			"//div[contains(@class,'resort-card')]");
	// h1[contains(.,'®')]
	protected By resortsMap = By.xpath(/* "//div[@id='map']" */
			"//div[@class='availability-map ']");
	protected By totalresorts = By.xpath(/* "//h2[@role='status' and contains(.,'Show')]" */
			/* "//h2[contains(text(),'Resorts')]" */
			"//div[contains(@class,'resort-card')]//a");
	protected By mapiFrame = By.xpath("//div[@id='map']//iframe");
	protected By totalMapPins = By.xpath("//div[@id='map']//img");
	protected By searchByKeywor = By.xpath(/* "//input[@id='autocomplete']" */
			"//label[contains(@class,'location')]/input");
	protected By searchBySuggestion = By.xpath("//input[@id='autocompletePlaces']");
	protected By vacationType = By.xpath("//div[1]/span[contains(.,'Vacation')]");
	protected By toggleSwitch = By.xpath("//label[@for='searchSwitch']");
	protected By suggestionBox = By.xpath("//li[@class='ui-menu-item']");
	// protected By resortsTitle =
	// By.xpath("//div[@class='cell']//div[@class='wyn-title__title
	// cardTitleSize']");
	protected By ramadaPin = By.xpath("//div[@id='map']//map[@id='gmimap226']");
	protected By resortsTitle = By.xpath(/* "//a/div[@class='media-object callout ']//div[@class='title bold']" */
			"//div[contains(@class,'resort-card')]//a");
	protected By resortPageTitle = By.xpath(/* "//div[@class='wyn-headline ']//h1" */
			"(//div[contains(@class,'title-1')])[1]");

	protected By noResultTitle = By.xpath("//div[@class='noResult']");
	protected By noResultMessage = By.xpath("//div[@class='noResultMessage']");
	protected By firstLoadNoResult = By.xpath("//h2[contains(.,'No results')]");
	protected By clearSearch = By.xpath("//input[@class='button clearSearch']");
	// a/div[@class='media-object callout ']/div/div[contains(.,'Explore')]
	// resorts/wyndham-hotels-resorts/australia/surfers-paradise/wyndham-surfers-paradise

	protected By exploreLinks = By.xpath(/* "//div[@class='exploreLink wyn-type-button']" */
			"//ul[@class='menu expanded ']//a[contains(text(),'Explore Resorts')]");
	protected By aboutResort = By.xpath("//div[@class='wyn-js-expand-disabled wyn-expandable-card ']");
	protected By aboutActiveNav = By.xpath("//li[@class='is-active']/a[contains(.,'About')]");
	protected By amenitiesActiveNav = By.xpath("//li[@class='is-active']/a[contains(.,'Amenities')]");
	protected By roomActiveNav = By.xpath("//li[@class='is-active']/a[contains(.,'Rooms')]");
	protected By reviewsActiveNav = By.xpath("//li[@class='is-active']/a[contains(.,'Reviews')]");
	protected By amenitiesResort = By.xpath("//a[contains(.,'Show More Amenities')]");
	protected By roomResort = By.xpath("//a[contains(.,'Show More Room Amenities')]");
	protected By time = By.xpath("//time"); // Reviews
	protected By reviewNavTab = By.xpath("//li[@class]/a[contains(.,'Review')]");

	// resortDetailsVeri
	protected By resortImage = By.xpath(/* "//div[@class='wyn-image-gallery']//img" */
			"//div[@class='masonry-gallery']");
	protected By wynBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Resorts')]");
	protected By resortType = By.xpath(/* "//div[@class='wyn-headline ']//p" */
			"//h2[contains(text(),'Club Wyndham Resort')]");
	protected By resortName = By.xpath("//h1[@class='wyn-headline__title']");
	protected By shareCTA = By.xpath(/* "//img[@class='wyn-image wyn-share-button__toggle']" */
			"//a[@class='wyn-share-button__toggle']");
	protected By shareOptions = By.xpath(/* "//div[@id='wyn-resort-share-buttons']/div" */
			"//div[contains(@class,'share-buttons')]/div"); // data-network
	protected By resortAddress = By.xpath(/* "//h2[@class='wyn-headline__subtitle']" */
			"//div[contains(@class,'title-1')]/../div[contains(@class,'body-1')]");
	protected By resortReview = By.xpath(/* "//a[@class='wyn-resort-phoneNumber']" */
			"//div[@id='ReactReviewsHeader']");
	protected By reviewDesc = By.xpath(/* "//section[@id='description']//div[@class='wyn-review']" */
			"//div[@id='ReactReviewsHeader']//following::p[contains(text(),'There')]");
	protected By amenitiesHeader = By.xpath("//h2[contains(.,'Amenities')]");
	protected By amenitiesList = By
			.xpath("//div[@class='wyn-amenities wyn-js-amenities']//li[@class='wyn-type--left']");
	protected By moreAmenities = By.xpath("//a[contains(.,'More Amenities')]");
	protected By roomType = By.xpath(/* "//h2[contains(.,'Room Type')]" */
			/* "//h2[contains(.,'Suite Types')]" */
			"//div[contains(text(),'Suite Types')]");
	protected By bedroomSlidingTab = By.xpath(/* "//div[@class='wyn-sliding-tabs__container']" */
			"//div[contains(@class,'show')]//ul[contains(@id,'unit-types-tabs')]/li/a");
	protected By roomTypeList = By.xpath(/* "//div[@class='wyn-bedroom-card']" */
			/*
			 * "//div[contains(@class,'show')]//div[contains(@class,'tabs-content')]//h6[contains(@class,'subtitle-2')]"
			 */
			"//div[contains(@class,'show')]//div[contains(@class,'tabs-content')]//div[contains(@class,'subtitle-2')]");
	protected By roomTypeHdr = By.xpath(/*
										 * "//div[@class='wyn-bedroom-card']//div[@class='wyn-type-title-2 wyn-color-black'][1]"
										 */
			/*
			 * "//div[contains(@class,'show')]//div[contains(@class,'tabs-content')]//h6[contains(@class,'subtitle-2')]"
			 */
			"//div[contains(@class,'show')]//div[contains(@class,'tabs-content')]//div[contains(@class,'subtitle-2')]");
	protected By roomLocationHdr = By.xpath(/*
											 * "//div[@class='wyn-bedroom-card']//div[@class='wyn-type-title-2 wyn-color-black' and @style]"
											 */
			"//div[contains(@class,'show')]//div[contains(@class,'tabs-content')]//h6[contains(@class,'subtitle-2')]/../p[1]");
	protected By moreRoom = By.xpath(/* "//a[contains(.,'More Room')]" */
			"//button[contains(text(),'View Suite')]");
	protected By helpfulHints = By.xpath("//span[contains(.,'Helpful Hints')]");
	protected By pointsCharts = By.xpath("//span[contains(.,'Points Chart')]");
	protected By accordionPlusCTA = By.xpath("//button[@class='wyn-accordion__toggle wyn-js-sticky']");
	protected By expandAllCTA = By.xpath("//a[contains(.,'Expand All')]");
	protected By collaspeAllCTA = By.xpath("//a[contains(.,'Collapse All')]");
	protected By closedAccordian = By.xpath("//div[@class='wyn-js-accordion']");
	protected By opneAccordian = By.xpath("//div[@class='wyn-js-accordion is-active']");
	protected By accordianContent = By.xpath("//div[@class='wyn-accordion__content']");
	protected By resortMap = By.xpath(/* "//div[@gtm_component='map']" */
			"//h2[text()='Explore the Area']/../div[@class='map']");
	protected By reviewSection = By.xpath(/* "//section[@id='review']//div[@class='wyn-review']" */
			"//div[@class='review-listing']");
	protected By totalReviews = By.xpath(/*
											 * "//div[@id='ReactReviews']//div[@class='wyn-l-margin-medium--top']/following-sibling::div"
											 */
			/*"//div[@class='review-listing']//div[contains(@class,'review')]//h5"*/
			"//div[contains(@class,'review-list')]//div[@class='row review']");
	protected By loadMoreReviews = By.xpath(/* "//button[contains(.,'Load More')]" */
			"//button[contains(.,'See More')]");
	protected By moreResorts = By.xpath(/*"//h2[contains(.,'More Club Wyndham')]"*/
			"//div[contains(text(),'MORE CLUB WYNDHAM')]");
	protected By totalCards = By.xpath(/*"//a[@class='wyn-card ']/ancestor::div[@class='slick-track']/div[@id]"*/
			"//section[contains(@class,'resort-availability')]//img");
	protected By bookStay = By.xpath(/* "//h2[contains(.,'Love this resort')]" */
			"//div[contains(text(),'Love this resort')]");
	protected By checkAvailability = By.xpath(/* "//a[contains(.,'Check availability')]" */
			"(//button[contains(text(),'View Availability')])[1]");
	protected By loginBtn = By.xpath("//div[@class='wyn-header__nav-container']//a[contains(.,'Login')]");
	protected By loginPopUp = By.xpath(/* "//div[@id='wyn-modal-content']" */
			"//div[@id='formModal']");
	protected By loginPopUpHdr = By.xpath(/* "//div[@id='wyn-modal-content']//h2" */
			"//div[@id='formModal']//div[@class='caption-1']");
	protected By loginPopUpBtn = By.xpath(/* "//div[@id='wyn-modal-content']//a" */
			"//div[@id='formModal']//div[@class='caption-1']//..//a");
	protected By loginPopUpClose = By.xpath(/* "//div[@id='wyn-modal-content']//span" */
			"(//button[@class='close-button']/span)[1]");
	protected By roomTypeHdrEdge = By.xpath(
			"//div[@class='wyn-bedroom-card']//div[@class='wyn-type-title-2 wyn-color-black' and @style]/preceding-sibling::div");
	protected By formTitle = By.xpath("//form/div[1]");
	protected By formSubTitle = By.xpath("//form//p/p");
	protected By firstNameField = By.xpath("//input[@id='firstName']");
	protected By lastNameField = By.xpath("//input[@id='lastName']");
	protected By phoneField = By.id("phone");
	protected By emailField = By.id("email");
	protected By stateSelect = By.xpath("//select[@name='State']");
	protected By submitButton = By.xpath("//form//button[contains(.,'Submit')]");
	protected By thankConfirm = By.xpath("//div[@class='wyn-message__content']//h4");
	protected By closeMsg = By.xpath("//span[@class='wyn-modal__close wyn-modal__close--secondary']");
	protected By checkBox = By.xpath("//form//input[@type='checkbox']");

	protected By timeShareForm = By.xpath("//form[contains(@id,'leadForm')]");
	protected By timeShareFormTitle = By.xpath("//form[contains(@id,'leadForm')]//div[contains(@class, 'headline')]");
	protected By timeShareFormSubTitle = By.xpath("//form[contains(@id,'leadForm')]//div//div");
	protected By timeShareStateSelect = By.xpath("//select[@name='state']");
	protected By timeShareSubmitButton = By.xpath("//form//button[contains(text(),'SUBMIT')]");

	protected By timeShareConfirmMsg = By.xpath("//div[contains(text(),'Thank you for reaching out!')]");
	protected By lableUnableToBook = By.xpath("//div[@class='cell']//div[@class='subtitle-2']");
	protected By lableCalendar = By.xpath("//div[@class='text-center']/p");
	protected By lableHeaderCalendarPage = By.xpath("//h2[contains(.,'Club')]");

	/*
	 * Method: navigateToResortsPage Description: Navigate to Resorts Page : Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToResortsPage() {
		waitUntilElementVisibleBy(driver, resortsHeader, 120);

		if (verifyObjectDisplayed(resortsHeader)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.PASS,
					"Resorts present in main Nav");
			driver.findElement(resortsHeader).click();

			/*
			 * try { boolean flag = false;
			 * 
			 * for (long i = 0; i < 10 / 2; i++) { try {
			 * 
			 * if (driver.findElement(firstLoadNoResult).isDisplayed()) { flag = true;
			 * waitForSometime("2"); break; } else {
			 * 
			 * System.out.println(firstLoadNoResult + " Element not visible"); }
			 * 
			 * } catch (Exception e) { // TODO Auto-generated catch block //
			 * e.printStackTrace(); System.out.println(firstLoadNoResult +
			 * " Element not found"); waitForSometime("2"); }
			 * 
			 * } if (!flag) {
			 * 
			 * System.out.println("I am catched here!!!"); driver.navigate().refresh();
			 * 
			 * try { driver.switchTo().alert(); waitForSometime("10"); } catch (Exception e)
			 * { // TODO: handle exception } }
			 */

			if (verifyObjectDisplayed(firstLoadNoResult)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.FAIL,
						"No Results first load error Present");
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.PASS,
						"No Results first load error not Present");
			}

		} /*
			 * catch (Exception e) { tcConfig.updateTestReporter("NextGenResortsPage",
			 * "navigateToResortsPage", Status.PASS,
			 * "No Results first load error not Present"); }
			 */

		waitUntilElementVisibleBy(driver, totalresorts, 120);

		if (verifyObjectDisplayed(totalresorts)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.PASS,
					"Resorts page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortsPage", Status.FAIL,
					"Resorts page Navigation failed");
		}

	}/*
		 * else { tcConfig.updateTestReporter("NextGenResortsPage",
		 * "navigateToResortsPage", Status.FAIL, "Resorts not present in main Nav"); }
		 */

	/*
	 * Method: resortsaAndMapVal Description:Resorts Page Validations: Date Sep/2019
	 * Author: Unnat Jain Changes By
	 */
	public void resortsaAndMapVal() {

		if (verifyObjectDisplayed(searchByKeywor)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.PASS,
					"Search Via Keyword option present");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.FAIL,
					"Search Via Keyword option not present");
		}

		/*
		 * if (verifyObjectDisplayed(vacationType)) {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal",
		 * Status.PASS, "Vacation Type option present"); } else {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal",
		 * Status.FAIL, "Vacation Type option not present"); }
		 */

		/*
		 * if (verifyObjectDisplayed(toggleSwitch)) {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal",
		 * Status.PASS, "Toggle Option Present"); } else {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal",
		 * Status.FAIL, "Toggle option not present"); }
		 */
		waitUntilElementVisibleBy(driver, resortsTitle, 30);
		List<WebElement> totalResortList = driver.findElements(resortsList);

		if (totalResortList.size() > 0) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.PASS,
					"Resorts Cell present, Total " + (totalResortList.size() / 2));
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.FAIL,
					"Resorts not displayed");
		}

		if (verifyObjectDisplayed(resortsMap)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.PASS, "Resorts Map Present");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.FAIL,
					"Resorts Map not Present");
		}

		String destinationsCards = getElementText(totalresorts);
		if (verifyObjectDisplayed(totalresorts)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.PASS,
					"Total Destinations Resort dispalyed are: " + destinationsCards);

			// clickElementBy(toggleSwitch);
			// noResultVal();

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			ramadaValidations();

			/*
			 * if (verifyObjectDisplayed(totalresorts)) { String resortsCards =
			 * getElementText(totalresorts);
			 * 
			 * if (destinationsCards.equalsIgnoreCase(resortsCards)) {
			 * tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal",
			 * Status.FAIL, "Toggle Switch did not work" );
			 * 
			 * } else { tcConfig.updateTestReporter("NextGenResortsPage",
			 * "resortsaAndMapVal", Status.PASS, "Total Destinations Resort dispalyed are: "
			 * + resortsCards);
			 * 
			 * }
			 * 
			 * } else { tcConfig.updateTestReporter("NextGenResortsPage",
			 * "resortsaAndMapVal", Status.FAIL, "Total Destinations Resort not displayed");
			 * }
			 * 
			 * } else { tcConfig.updateTestReporter("NextGenResortsPage",
			 * "resortsaAndMapVal", Status.FAIL, "Total Destinations Resort not displayed");
			 */
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortsaAndMapVal", Status.PASS,
					"Destionation Resorts are not getting displayed");
		}

	}

	/*
	 * Method: ramadaValidations Description:Ramada Resorts Page Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */
	public void ramadaValidations() {
		waitUntilElementVisibleBy(driver, searchByKeywor, 120);

		driver.findElement(searchByKeywor).sendKeys(testData.get("strSearch"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			waitUntilElementVisibleBy(driver, resortsTitle, 30);
		} catch (Exception e) {

		}

		Boolean tempVal = false;
		List<WebElement> resortList = driver.findElements(resortsTitle);
		for (int i = 0; i < resortList.size(); i++) {

			if (resortList.get(i).getText().toUpperCase().contains(testData.get("strResortSearch").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.PASS,
						resortList.get(i).getText() + " Resort dispalyed");

				clickElementWb(resortList.get(i));

				getElementInView(resortPageTitle);
				waitUntilElementVisibleBy(driver, resortPageTitle, 120);

				if (getElementText(resortPageTitle).toUpperCase()
						.contains(testData.get("strResortSearch").toUpperCase())) {

					tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.PASS,
							"Navigated To Resorts Page with Title: " + getElementText(resortPageTitle));
					tempVal = true;
					break;

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.FAIL,
							"Resorts Page Navigation Failed");
				}

			} else {
				tempVal = false;
				if (i == (resortList.size() - 1)) {
					break;
				}
			}

		}

		if (tempVal == false) {
			tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.FAIL,
					"Resorts Page Navigation Failed");
		}

	}

	/*
	 * Method: surfersParadiseVal Description:Resorts Page Validations: Date
	 * Oct/2019 Author: Unnat Jain Changes By
	 */
	public void surfersParadiseVal() {
		waitUntilElementVisibleBy(driver, searchByKeywor, 120);

		// fieldDataEnter(searchByKeywor, testData.get("strSearch"));
		driver.findElement(searchByKeywor).sendKeys(testData.get("strSearch"));

		driver.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			waitUntilElementVisibleBy(driver, resortsTitle, 30);

			Boolean tempVal = false;
			List<WebElement> resortList = driver.findElements(resortsTitle);
			if (verifyElementDisplayed(resortList.get(0))) {

				for (int i = 0; i < resortList.size(); i++) {

					if (resortList.get(i).getText().toUpperCase()
							.contains(testData.get("strResortSearch").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.PASS,
								resortList.get(i).getText() + " Resort dispalyed");

						clickElementWb(resortList.get(i));

						waitUntilElementVisibleBy(driver, resortPageTitle, 120);

						if (getElementText(resortPageTitle).toUpperCase()
								.contains(testData.get("strResortSearch").toUpperCase())) {
							if (driver.getCurrentUrl().contains(testData.get("redirectURL1"))) {
								tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.PASS,
										"Navigated To Resorts Page with Title: " + getElementText(resortPageTitle));
								tempVal = true;
								break;
							} else {
								tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.FAIL,
										"Resorts Page Navigation Failed");
							}
						} else {
							tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.FAIL,
									"Resorts Page Navigation Failed");
						}

					} else {
						tempVal = false;
						if (i == (resortList.size() - 1)) {
							break;
						}
					}
				}

				if (tempVal = false) {
					tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.FAIL,
							"Surfers Paradise Resort Not Displayed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.FAIL,
						"Resorts Not Loaded Validation Failed");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenResortsPage", "surfersParadiseVal", Status.FAIL,
					"Resorts Validation Failed");
		}

	}

	/*
	 * Method: noResultVal Description:Resorts Page Validations: Date 18/Oct/2019
	 * Author: Unnat Jain Changes By
	 */
	public void noResultVal() {
		waitUntilElementVisibleBy(driver, searchByKeywor, 120);

		// fieldDataEnter(searchByKeywor, testData.get("strSearch"));
		driver.findElement(searchByKeywor).sendKeys(testData.get("noResult"));

		driver.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleBy(driver, noResultTitle, 30);

		if (verifyObjectDisplayed(noResultTitle)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "noResultVal", Status.PASS,
					"No Result Title Displayed with message: " + getElementText(noResultMessage));
			if (verifyObjectDisplayed(clearSearch)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "noResultVal", Status.PASS,
						"Clear Search Option Displayed");
				clickElementBy(clearSearch);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "noResultVal", Status.FAIL,
						"Clear Search Option not Displayed");
			}

		} else if (verifyObjectDisplayed(resortsTitle)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "noResultVal", Status.FAIL,
					"Results Displayed for " + testData.get("noResult"));
			if (verifyObjectDisplayed(clearSearch)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "noResultVal", Status.PASS,
						"Clear Search Option Displayed");
				clickElementBy(clearSearch);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "noResultVal", Status.FAIL,
						"Clear Search Option not Displayed");
			}
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "noResultVal", Status.FAIL,
					"No Result Title not Displayed");
		}

	}

	/*
	 * Method: resortPageScrollNav Description:Resorts Page scroll Validations: Date
	 * 05/Nov/2019 Author: Unnat Jain Changes By
	 */
	public void resortPageScrollNav() {

		List<WebElement> exploreResortCTA = driver.findElements(exploreLinks);

		driver.switchTo().activeElement().sendKeys(Keys.HOME);

		exploreResortCTA.get(0).click();

		waitUntilElementVisibleBy(driver, amenitiesResort, 120);

		getElementInView(aboutResort);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(aboutActiveNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.PASS,
					"About Tab is active in sub nav");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.FAIL,
					"About Tab is not active in sub nav");

		}

		getElementInView(amenitiesResort);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(amenitiesActiveNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.PASS,
					"Amenities Tab is active in sub nav");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.FAIL,
					"Amenities Tab is not active in sub nav");

		}

		getElementInView(roomResort);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(roomActiveNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.PASS,
					"Room Tab is active in sub nav");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.FAIL,
					"Room Tab is not active in sub nav");

		}

		List<WebElement> timeReview = driver.findElements(time);
		getElementInView(timeReview.get(timeReview.size() - 1));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(reviewsActiveNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.PASS,
					"Review Tab is active in sub nav");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav", Status.FAIL,
					"Review Tab is not active in sub nav");

		}

		/*
		 * clickElementBy(aboutNavTab);
		 * 
		 * waitForSometime(tcConfig.getConfig().get("LowWait"));
		 * 
		 * if (verifyObjectDisplayed(aboutActiveNav)) {
		 * tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav",
		 * Status.PASS, "About Tab is active in sub nav"); } else {
		 * tcConfig.updateTestReporter("NextGenHomePage", "resortPageScrollNav",
		 * Status.FAIL, "About Tab is not active in sub nav");
		 * 
		 * }
		 */
	}

	/*
	 * Method: navigateToResortDetails Description:Resort Page Navigation: Date
	 * 05/Nov/2019 Author: Unnat Jain Changes By
	 */
	
	protected By searchLocation = By.xpath("//label[contains(@class,'location')]/input");
	public void navigateToResortDetails() {
		waitUntilElementVisibleBy(driver, searchLocation, 120);
		driver.findElement(searchLocation).sendKeys(testData.get("strSearch"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			waitUntilElementVisibleBy(driver, resortsTitle, 30);
		} catch (Exception e) {

		}

		Boolean tempVal = false;
		List<WebElement> resortList = driver.findElements(resortsTitle);
		for (int i = 0; i < resortList.size(); i++) {

			if (resortList.get(i).getText().split("›")[0].toUpperCase().contains(testData.get("strSearch").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.PASS,
						resortList.get(i).getText() + " Resort dispalyed");

				clickElementWb(resortList.get(i));

				waitUntilElementVisibleBy(driver, resortPageTitle, 120);

				getElementInView(resortPageTitle);
				if (getElementText(resortPageTitle).toUpperCase()
						.contains(testData.get("strSearch").toUpperCase())) {

					tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.PASS,
							"Navigated To Resorts Page with Title: " + getElementText(resortPageTitle));
					tempVal = true;
					break;

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.FAIL,
							"Resorts Page Navigation Failed");
				}

			} else {
				tempVal = false;
				if (i == (resortList.size() - 1)) {
					break;
				}
			}

		}

		if (tempVal == false) {
			tcConfig.updateTestReporter("NextGenResortsPage", "navigateToResortDetails", Status.FAIL,
					"Searched Resort Not Displayed");
		}

	}

	/*
	 * Method: resortDetailsVal Description:Resort Details Page Validation: Date
	 * 13/Dec/2019 Author: Unnat Jain Changes By
	 */
	public void resortDetailsVal() {

		if (verifyObjectDisplayed(resortImage)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Resort Hero Image Present");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Resort Hero Image not Present");
		}

		if (verifyObjectDisplayed(wynBreadcrumb)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Resort Breadcrumb Present");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Resort Breadcrumb not Present");
		}

		if (verifyObjectDisplayed(resortPageTitle)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Resort Name is: " + getElementText(resortPageTitle));
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Resort Type not Present");
		}

		if (verifyObjectDisplayed(resortAddress)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Resort Address Present");
		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Resort Address not Present");
		}

		/*
		 * getElementInView(resortReview); if (verifyObjectDisplayed(resortReview)) {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal",
		 * Status.PASS, "Resort Review section is displayed"); } else {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal",
		 * Status.FAIL, "Resort Review section is displayed"); } if
		 * (verifyObjectDisplayed(reviewDesc)) {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal",
		 * Status.PASS, "Resort Reviews Present"); } else {
		 * tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal",
		 * Status.FAIL, "Resort Reviews not Present"); }
		 */

		if (verifyObjectDisplayed(shareCTA)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS, "Share CTA Present");

			clickElementBy(shareCTA);
			getElementInView(shareCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			List<WebElement> shareOptionList = driver.findElements(shareOptions);

			if (shareOptionList.size() == 4) {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
						"Share Options Present, total: " + shareOptionList.size());

				clickElementJS(shareOptionList.get(0));

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
							"Navigated to facebook page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
							"Navigation to facebook page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
						"All the share options not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL, "Share CTA not Present");
		}

		if (verifyObjectDisplayed(bookStay)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.PASS,
					"Book Stay Section Present");
			// getElementInView(checkAvailability);
			clickElementBy(checkAvailability);

			waitUntilElementVisibleBy(driver, loginPopUp, 50);

			if (verifyObjectDisplayed(loginPopUp)) {
				tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
						"Login Button pop up opened");

				List<WebElement> headers = driver.findElements(loginPopUpHdr);
				List<WebElement> headersBtn = driver.findElements(loginPopUpBtn);

				if (headers.size() == 2) {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
							"Login Pop Up Headers present");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
							"Login Pop Up Headers not present");
				}

				if (headersBtn.size() == 2) {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.PASS,
							"Login Pop Up Headers CTA present");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
							"Login Pop Up Headers CTA not present");
				}

				if (verifyObjectDisplayed(loginPopUpClose)) {
					clickElementBy(loginPopUpClose);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
							"Login Pop Up close CTA not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "headerValidations", Status.FAIL,
						"Login Button pop up not opened");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortDetailsVal", Status.FAIL,
					"Book Stay Section not Present");
		}

	}

	/*
	 * Method: resortAmenitiesVal Description:Resort Details Page Validation: Date
	 * 13/Dec/2019 Author: Unnat Jain Changes By
	 */
	public void resortAmenitiesVal() {
		if (verifyObjectDisplayed(amenitiesHeader)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortAmenitiesVal", Status.PASS,
					"Resort Amenities Header Present");
			getElementInView(amenitiesHeader);

			List<WebElement> amenitiesList1 = driver.findElements(amenitiesList);

			tcConfig.updateTestReporter("NextGenResortsPage", "resortAmenitiesVal", Status.PASS,
					"Initially " + amenitiesList1.size() + " amenities displayed");

			if (verifyObjectDisplayed(moreAmenities)) {
				clickElementBy(moreAmenities);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> amenitiesList2 = driver.findElements(amenitiesList);
				if (amenitiesList2.size() > amenitiesList1.size()) {
					tcConfig.updateTestReporter("NextGenResortsPage", "resortAmenitiesVal", Status.PASS,
							"Amenities List Increased after clicking Show More");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "resortAmenitiesVal", Status.FAIL,
							"Amenities Show More Link not working");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortAmenitiesVal", Status.FAIL,
						"Amenities Show More Link not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortAmenitiesVal", Status.FAIL,
					"Resort Amenities Header not Present");
		}

		formValidations();

	}

	/*
	 * Method: formValidations Description: Form validations on resort page: Date
	 * Jun/2019 Author: Unnat Jain Changes By
	 */
	public void formValidations() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			getElementInView(formTitle);
			tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameField);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(firstNameField, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(lastNameField, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(phoneField, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(emailField, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(stateSelect)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"State Drop Down present");

					new Select(driver.findElement(stateSelect)).selectByValue(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(submitButton)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButton);

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"Submit Button not present");
				}

				waitUntilElementVisibleBy(driver, thankConfirm, 50);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"Form Submission Successful");

					if (verifyObjectDisplayed(closeMsg)) {
						clickElementBy(closeMsg);
					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"Close Btn not present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	/*
	 * Method: roomTypeVal Description:Resort Details Page Validation: Date
	 * 13/Dec/2019 Author: Unnat Jain Changes By
	 */
	public void resortRoomTypeVal() {
		if (verifyObjectDisplayed(roomType)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.PASS, "Room Type Header Present");
			getElementInView(roomType);

			List<WebElement> slidingTabsList = driver.findElements(bedroomSlidingTab);
			List<WebElement> totalRoom = driver.findElements(roomTypeList);
			List<WebElement> roomHdrList = driver.findElements(roomTypeHdr);
			// List<WebElement> roomHdrListEd =
			// driver.findElements(roomTypeHdrEdge);
			// List<WebElement> roomLocationList =
			// driver.findElements(roomLocationHdr);
			List<WebElement> showMoreRoomList = driver.findElements(moreRoom);

			tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.PASS,
					"Room Type Sliding Tab Present" + slidingTabsList.size());

			tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.PASS,
					"Room Cards Present, total: " + totalRoom.size());

			/*
			 * if (browserName.contains("EDGE")) { if (roomHdrListEd.size() ==
			 * totalRoom.size()) { tcConfig.updateTestReporter("NextGenResortsPage",
			 * "roomTypeVal", Status.PASS, "Room Card Headers Present in all cards, total: "
			 * + roomHdrList.size()); } else {
			 * tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.FAIL,
			 * "Room Card Headers not Present in all cards"); } }
			 */
			if (roomHdrList.size() == totalRoom.size()) {
				tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.PASS,
						"Room Card Headers Present in all cards, total: " + roomHdrList.size());
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.FAIL,
						"Room Card Headers not Present in all cards");
			}

			/*
			 * if (roomLocationList.size() == totalRoom.size()) {
			 * tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.PASS,
			 * "Room Card Location Present in all cards, total: " +
			 * roomLocationList.size()); } else {
			 * tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.FAIL,
			 * "Room Card Location not Present in all cards"); }
			 */

			if (showMoreRoomList.size() == totalRoom.size()) {
				tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.PASS,
						"Room Card More CTA Present in all cards, total: " + showMoreRoomList.size());
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.FAIL,
						"Room Card More CTA not Present in all cards");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "roomTypeVal", Status.FAIL,
					"Room Type Header not Present");
		}
	}

	/*
	 * Method: resortAccordionVal Description:Resort Details Page Validation: Date
	 * 13/Dec/2019 Author: Unnat Jain Changes By
	 */
	public void resortAccordionVal() {
		if (verifyObjectDisplayed(helpfulHints)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.PASS,
					"Helpful Hints Header Present");
			getElementInView(helpfulHints);
			List<WebElement> closedAccList1 = driver.findElements(closedAccordian);

			if (verifyElementDisplayed(closedAccList1.get(0)) && verifyElementDisplayed(closedAccList1.get(1))) {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.PASS,
						"Accordion Closed By Default");
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.FAIL,
						"Accordion not Closed By Default");
			}

			clickElementBy(helpfulHints);
			clickElementBy(pointsCharts);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> openedAccList1 = driver.findElements(opneAccordian);

			if (verifyElementDisplayed(openedAccList1.get(0)) && verifyElementDisplayed(openedAccList1.get(1))) {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.PASS,
						"Both Accordion Opnened when both cta are clicked");

				List<WebElement> accContent = driver.findElements(accordianContent);
				if (accContent.size() == 2) {
					tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.PASS,
							"Content Present in both Accordian");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.FAIL,
							"Content not Present in ACcordian");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.FAIL,
						"Both Accordion not Opnened when CTA is clicked");
			}

			if (verifyObjectDisplayed(pointsCharts)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.PASS,
						"Points Charts Header Present");
			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.FAIL,
						"Points Charts Header not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortAccordionVal", Status.FAIL,
					"Helpful Hints Header not Present");
		}
	}

	/*
	 * Method: resortReviewsVal Description:Resort Details Page Validation: Date
	 * 13/Dec/2019 Author: Unnat Jain Changes By
	 */
	public void resortReviewsVal() {

		if (verifyObjectDisplayed(reviewSection)) {
			getElementInView(resortMap);
			tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.PASS,
					"Resort Map Section Present");

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.FAIL,
					"Resort Map Section not Present");
		}

		if (verifyObjectDisplayed(reviewSection)) {
			getElementInView(totalReviews);
			tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.PASS,
					"Resort Review Section Present");

			List<WebElement> reviewsList1 = driver.findElements(totalReviews);

			tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.PASS,
					"Initially " + reviewsList1.size() + " Reviews displayed");

			/*if (verifyObjectDisplayed(loadMoreReviews)) {
				clickElementBy(loadMoreReviews);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				List<WebElement> reviewsList2 = driver.findElements(totalReviews);
				if (reviewsList2.size() > reviewsList1.size()) {
					tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.PASS,
							"Reviews Increased after clicking Show More");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.FAIL,
							"Reviews Show More Link not working");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.FAIL,
						"Reviews Show More Link not present");
			}*/

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortAmenitiesVal", Status.FAIL,
					"Reviews Header not Present");
		}

		
		if (verifyObjectDisplayed(moreResorts)) {
			getElementInView(moreResorts);
			tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.PASS,
					"More Resort Section Present");

			List<WebElement> resortList = driver.findElements(totalCards);
			tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.PASS,
					"Total " + resortList.size() + " resorts cards displayed");

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "resortReviewsVal", Status.FAIL,
					"More Resort Section not Present");
		}
		 
	}
	
	public void validateTimeShareSignUpFormSubmissionProcess() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, timeShareForm, 120);

		if (verifyObjectDisplayed(timeShareForm)) {
			getElementInView(timeShareForm);
			tcConfig.updateTestReporter("NextGenResortPage", "timeShareFormValidations", Status.PASS,
					"Presence of TimeShareUser Details form validated Successfully");

			if (verifyObjectDisplayed(timeShareFormTitle)) {

				tcConfig.updateTestReporter("NextGenResortPage", "timeShareformValidations", Status.PASS,
						"Form Title present with header " + getElementText(timeShareFormTitle));
				if (verifyObjectDisplayed(timeShareFormSubTitle)) {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
							"Form SubTitle present with Details: " + getElementText(timeShareFormSubTitle));

					if (verifyObjectDisplayed(firstNameField)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"First Name Field Present");

						if (browserName.equalsIgnoreCase("EDGE")) {
							WebElement e = driver.findElement(firstNameField);
							e.clear();
							e.sendKeys(testData.get("firstName") + Keys.TAB);
							driver.switchTo().activeElement().sendKeys(Keys.TAB);
						} else {
							fieldDataEnter(firstNameField, testData.get("firstName"));
						}

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"First Name Field not present");
					}

					if (verifyObjectDisplayed(lastNameField)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"Last Name Field Present");

						fieldDataEnter(lastNameField, testData.get("lastName"));

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"Last Name Field not present");
					}

					if (verifyObjectDisplayed(phoneField)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"Phone Field Present");

						fieldDataEnter(phoneField, testData.get("phoneNo"));

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"Phone Field not present");
					}

					if (verifyObjectDisplayed(emailField)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"Phone Field Present");

						fieldDataEnter(emailField, testData.get("email"));

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"Email Field not present");
					}

					if (verifyObjectDisplayed(timeShareStateSelect)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"State Drop Down present");

						driver.findElement(By.xpath("//span[contains(text(),'State *')]")).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.findElement(By.xpath("//*[contains(text(),'" + testData.get("stateSel") + "')]"))
								.click();
						// timeShareStateSelect.click();

						// new
						// Select(driver.findElement(timeShareStateSelect)).selectByValue(testData.get("stateSel"));

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"State Drop Down not present");
					}

					if (verifyObjectDisplayed(checkBox)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"T&C checkbox present");

						clickElementJSWithWait(checkBox);

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"T&C checkbox not present");
					}

					if (verifyObjectDisplayed(timeShareSubmitButton)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"Submit Present present");

						clickElementJSWithWait(timeShareSubmitButton);

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"Submit Button not present");
					}

					waitUntilElementVisibleBy(driver, timeShareConfirmMsg, 50);

					if (verifyObjectDisplayed(timeShareConfirmMsg)) {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.PASS,
								"Form Submission Successful");

					} else {
						tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
								"Form Submission not Successful");
					}

				} else {
					tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
							"Form SubTitle not present with header ");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortPage", "formValidations", Status.FAIL,
						"Form Title not present with header ");
			}

		}

	}

	/*
	 * Method: clickViewAvailability Description:Resort Details Page Validation:
	 * Date nov/2020 Author: Saket Sharma Changes By
	 */

	public void clickViewAvailability() {

		if (verifyObjectDisplayed(bookStay)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.PASS,
					"Book Stay Section Present");
			// getElementInView(checkAvailability);
			clickElementBy(checkAvailability);

			waitUntilElementVisibleBy(driver, loginPopUp, 50);

			if (verifyObjectDisplayed(loginPopUp)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.PASS,
						"Login Button pop up opened");

				List<WebElement> headers = driver.findElements(loginPopUpHdr);
				List<WebElement> headersBtn = driver.findElements(loginPopUpBtn);

				if (headers.size() == 2) {
					tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.PASS,
							"Login Pop Up Headers present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.FAIL,
							"Login Pop Up Headers not present");
				}

				if (headersBtn.size() == 2) {
					tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.PASS,
							"Login Pop Up Headers CTA present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.FAIL,
							"Login Pop Up Headers CTA not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.FAIL,
						"Login Button pop up not opened");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "clickViewAvailability", Status.FAIL,
					"Book Stay Section not Present");
		}

	}

	public void IneligibleMember(String validation) {
		waitUntilElementVisibleBy(driver, resortPageTitle, 120);
		Assert.assertTrue(getElementText(lableUnableToBook).toUpperCase().equals(validation));
		tcConfig.updateTestReporter("NextGenResortsPage", "IneligibleMember", Status.PASS,
				"Unable to book label is present");
	}

	public void validateCalendarNavigation() {
		waitUntilElementVisibleBy(driver, lableHeaderCalendarPage, 120);
		Assert.assertTrue(verifyObjectDisplayed(lableCalendar));
		tcConfig.updateTestReporter("NextGenResortsPage", "validateCalendarNavigation", Status.PASS,
				"User is navigated to select Calendar page");
	}

	public void resortformValidations() {
		// TODO Auto-generated method stub
		
	}

	public void navigationToResortsPage() {
		// TODO Auto-generated method stub
		
	}

}
