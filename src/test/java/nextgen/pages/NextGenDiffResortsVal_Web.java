package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenDiffResortsVal_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenDiffResortsVal_Web.class);

	public NextGenDiffResortsVal_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By resortsHeader = By.xpath(/* "//div[@class='grid-container ']//a[@title='Resorts']" */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Resorts']");
	protected By featuredResortNav = By.xpath(/* "//a[@data-eventname='header']//span[contains(.,'Featured')]" */
			"//ul[contains(@class,'first-sub')]//li//div//ul//li//a[contains(.,'Explore')]");
	protected By featuredDestinationsNav = By
			.xpath("//ul[contains(@class,'first-sub')]//li//div//ul//li//a[contains(.,'Featured')]");
	protected By featuredResortHeader = By.xpath(/* "//p[contains(.,'Featured')]" */
			"//div[contains(text(),'Resorts')]");
	protected By searchLocation = By.xpath("//label[contains(@class,'location')]/input");
	protected By orlandoArticle = By.xpath(/* "//h3[@class='wyn-title__title' and contains(.,'Orlando')]" */
			"(//div[contains(@class,'resort-card')]//a)[1]");
	protected By orlandoTitle = By.xpath(/* "//div[@class='wyn-headline ']/p" */
			"//div[contains(@class,'title-1')]/../div[contains(@class,'body-1')]");
	protected By orlandoHeader = By.xpath(/*
											 * "//div[@class='wyn-headline ']/p/ancestor::div[@class='column wyn-hero__column ']//h2"
											 */
			"//div[contains(@class,'title-1') and contains(text(),'" + testData.get("strSearch") + "')]");
	protected By orlandoDesc = By.xpath(
			/*
			 * "//div[@class='wyn-headline ']/p/ancestor::div[@class='column wyn-hero__column ']//div[@class='wyn-l-content wyn-card__copy']/p"
			 */
			"//div[contains(@class,'title-1')]/../div/p");
	protected By orlandoBradCrumb = By.xpath(/* "//nav[@class='wyn-breadcrumbs']//a[contains(.,'Orlando')]" */
			"//div[@class='breadcrumb']//a[contains(.,'" + testData.get("strSearch") + "')]");
	protected By orlandoHappyPlace = By.xpath("//h2[contains(.,'TAKE A BITE')]");
	protected By orlandoHappyPlSubTitle = By
			.xpath("//h2[contains(.,'TAKE A BITE')]/ancestor::div[@gtm_component='icon-list']//h3");
	protected By orlandoHappyPlcDesc = By
			.xpath("//h2[contains(.,'TAKE A BITE')]/ancestor::div[@gtm_component='icon-list']//p");
	protected By theaterMuseumsHeader = By.xpath("//h1[contains(.,'Theaters & Museum')]");
	protected By twoColumnVal = By
			.xpath("//div[@gtm_component='two-columns-container']//div[@class='contentblock-card']//img");
	protected By twoCoumnHeader = By
			.xpath("//div[@gtm_component='two-columns-container']//div[@class='contentblock-card']//h2");
	protected By readMoreLink = By.xpath(
			"//div[@gtm_component='two-columns-container']//div[@class='contentblock-card']//a[contains(.,'Read')]");

	protected By localFav = By.xpath("//div[@gtm_component_type='featured']//h2[contains(.,'Local Fav')]");
	protected By favArticleImage = By
			.xpath("//div[@gtm_component_type='featured']//img[contains(@class,'wyn-image lazyloaded')]");
	protected By favArticleHeader = By.xpath("//div[@gtm_component_type='featured']//h3");
	protected By favArticleDesc = By.xpath("//div[@gtm_component_type='featured']//p");

	protected By exploreGreatOutDoors = By.xpath("//h1[contains(.,'Explore the Great Outdoor')]");
	protected By totalCarouselArtcl = By.xpath("//ul[@class='slick-dots']//li");
	protected By carouselHeader = By
			.xpath("//div[@gtm_component_type='contentblock-carousel']//div[@class='wyn-title']//h2");
	protected By carouselImage = By.xpath("//div[@gtm_component_type='contentblock-carousel']//img");
	protected By carouselDescp = By.xpath("//div[@gtm_component_type='contentblock-carousel']//p");
	protected By carouselPrev = By
			.xpath("//div[@gtm_component_type='contentblock-carousel']//button[contains(.,'Prev')]");
	protected By carouselNext = By
			.xpath("//div[@gtm_component_type='contentblock-carousel']//button[contains(.,'Next')]");

	protected By themePark = By.xpath("//h1[contains(.,'Orlando Theme Park')]");
	protected By themeParkHeader = By.xpath("//div[@class='two-columns-container']//h2/b");
	protected By themeParkImages = By.xpath("//div[@class='two-columns-container']//img");
	protected By themeParkDesc = By.xpath("//div[@class='two-columns-container']//h4/b");

	protected By discoverOrlandoHdr = By.xpath("//h2[contains(.,'Discover more Orlando')]");
	protected By discoverArticleImages = By.xpath(
			"//h2[contains(.,'Discover more Orlando')]/ancestor::div[@gtm_component='cards']//img[contains(@class,'wyn-image ')]");
	protected By discoverArticleHeader = By
			.xpath("//h2[contains(.,'Discover more Orlando')]/ancestor::div[@gtm_component='cards']//h3");
	protected By discoverArticleLocation = By
			.xpath("//h2[contains(.,'Discover more Orlando')]/ancestor::div[@gtm_component='cards']//h4");
	protected By seeAllResorts = By.xpath(
			"//h2[contains(.,'Discover more Orlando')]/ancestor::div[@gtm_component='cards']//a[contains(.,'See all')]");

	protected By ownerOffersHeadr = By.xpath("//h2[contains(.,'Owner Offers')]");
	protected By ownerOfrsImages = By
			.xpath("//h2[contains(.,'Owner Offers')]/ancestor::div[@gtm_component='cards']//img");
	protected By ownerOffrsHeaders = By
			.xpath("//h2[contains(.,'Owner Offers')]/ancestor::div[@gtm_component='cards']//h3");
	protected By ownerOffrsDesc = By.xpath("//h2[contains(.,'Owner Offers')]/ancestor::div[@gtm_component='cards']//p");
	protected By seeAllOffers = By.xpath(
			"//h2[contains(.,'Owner Offers')]/ancestor::div[@gtm_component='cards']//a[contains(.,'See All Offers')]");
	protected By ownerOffersBookNow = By
			.xpath("//h2[contains(.,'Owner Offers')]/ancestor::div[@gtm_component='cards']//a[contains(.,'Book Now')]");

	protected By museumHeader = By.xpath("//h1[contains(.,'Museum')]");
	protected By museumHeroImg = By
			.xpath("//h1[contains(.,'Museum')]/ancestor::div[@class='wyn-hero wyn-hero--large']//img");
	protected By museumBrdCrmb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Museum')]");
	protected By exploreOrlandoHdr = By.xpath("//h1[contains(.,'Explore Orlando')]");
	protected By exploreOrlandoDesc = By
			.xpath("//h1[contains(.,'Explore Orlando')]/ancestor::div[@class='wyn-card__content']//div[2]/p");
	protected By museumSubSectionImages = By.xpath("//img/ancestor::div[@class='wyn-content--last']");
	protected By museumSubSectionHdr = By.xpath("//img/ancestor::div[@class='wyn-content--last']//h2");
	protected By museumSubSectionDesc = By.xpath("//img/ancestor::div[@class='wyn-content--last']//p");

	protected By theaterHeader = By.xpath("//h2[contains(.,'Theater')]");
	protected By theaterHeroImg = By
			.xpath("//h2[contains(.,'Theater')]/ancestor::div[@class='wyn-hero wyn-hero--large']//img");
	protected By theaterBrdCrmb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Theater')]");
	protected By theaterSubSectionImages = By.xpath("//img/ancestor::div[@class='wyn-content--last']");
	protected By theaterSubSectionHdr = By.xpath("//img/ancestor::div[@class='wyn-content--last']//h2");
	protected By theaterSubSectionDesc = By.xpath("//img/ancestor::div[@class='wyn-content--last']//p");

	/*
	 * Method: featuredResortsNav Description: Featured Resorts Page navigations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void featuredResortsNav() {
		waitUntilElementVisibleBy(driver, resortsHeader, 120);

		hoverOnElement(resortsHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(featuredResortNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredResortsNav", Status.PASS,
					"Featured resorts present in Sub Nav");
			clickElementJSWithWait(featuredResortNav);
			pageCheck();
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredResortsNav", Status.FAIL,
					"Featured resorts not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, featuredResortHeader, 120);

		if (verifyObjectDisplayed(featuredResortHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredResortsNav", Status.PASS,
					"Featured resorts Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredResortsNav", Status.FAIL,
					"Featured resorts Page Navigation failed");
		}

	}

	protected By featuredRsrtHeader = By.xpath("(//div[contains(text(),'Featured Destinations')])[1]");

	public void featuredDestNav() {
		waitUntilElementVisibleBy(driver, resortsHeader, 120);

		hoverOnElement(resortsHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(featuredDestinationsNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredDestNav", Status.PASS,
					"Featured resorts present in Sub Nav");
			clickElementJSWithWait(featuredDestinationsNav);
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredDestNav", Status.FAIL,
					"Featured resorts not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, featuredRsrtHeader, 120);

		if (verifyObjectDisplayed(featuredRsrtHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredDestNav", Status.PASS,
					"Featured Destinations Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredDestNav", Status.FAIL,
					"Featured Destinations Page Navigation failed");
		}

	}

	/*
	 * Method: searchResortLocation Description: Search Locations: Date Jun/2020
	 * Author: Priya Das Changes By
	 */

	public void searchResortLocation() {
		try {
			/*
			 * waitUntilElementVisibleBy(driver, searchLocation, 120);
			 * clickElementWithJS(driver.findElement(searchLocation));
			 * //fieldDataEnterAction(searchLocation, testData.get("strSearch") +
			 * Keys.ENTER);
			 * driver.findElement(searchLocation).sendKeys(testData.get("strSearch") +
			 * Keys.ENTER);
			 */

			waitUntilElementVisibleBy(driver, searchLocation, 120);

			driver.findElement(searchLocation).sendKeys(testData.get("strSearch"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().activeElement().sendKeys(Keys.ENTER);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter("NextGenHomePage", "featuredResortsNav", Status.PASS,
					"Search resort with " + testData.get("strSearch"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHomePage", "featuredResortsNav", Status.FAIL,
					"Not able to search with the Resort " + e.getMessage());
		}
	}

	/*
	 * Method: orlandoValidations Description: Orlando page Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void orlandoValidations() {

		//getElementInView(orlandoArticle);
		waitUntilElementVisibleBy(driver, orlandoArticle, 120);
		if (verifyObjectDisplayed(orlandoArticle)) {
			tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.PASS,
					"Orlando article present in the Page ");
			String strLocationName = getElementText(orlandoArticle).split("›")[0].trim();
			clickElementJSWithWait(orlandoArticle);

			waitUntilElementVisibleBy(driver, orlandoHeader, 120);
			String strLocationHeaderName = getElementText(orlandoHeader);
			if (verifyObjectDisplayed(orlandoHeader) && strLocationName.equals(strLocationHeaderName)) {
				tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.PASS,
						"Orlando Resort Navigated");

				if (verifyObjectDisplayed(orlandoBradCrumb)) {
					tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.PASS,
							"Orlando Breadcrumb present");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.FAIL,
							"Orlando Breadcrumb not present");
				}

				if (verifyObjectDisplayed(orlandoTitle)) {
					tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.PASS,
							"Orlando Title present as: " + getElementText(orlandoTitle));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.FAIL,
							"Orlando Title not present");
				}

				if (verifyObjectDisplayed(orlandoDesc)) {
					tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.PASS,
							"Orlando description  present");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.FAIL,
							"Orlando description not  present");
				}

				/*
				 * getElementInView(orlandoHappyPlace);
				 * 
				 * if (verifyObjectDisplayed(orlandoHappyPlace)) {
				 * tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations",
				 * Status.PASS, "Its a happy place header section present ");
				 * 
				 * List<WebElement> happyPlaceSubTitle =
				 * driver.findElements(orlandoHappyPlSubTitle); List<WebElement>
				 * happyPlaceSubDesc = driver.findElements(orlandoHappyPlcDesc);
				 * tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations",
				 * Status.PASS, "Total Subtitle: " + happyPlaceSubTitle.size()); for (int i = 0;
				 * i < happyPlaceSubTitle.size(); i++) {
				 * tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations",
				 * Status.PASS, (i + 1) + " title is " + happyPlaceSubTitle.get(i).getText());
				 * 
				 * }
				 * 
				 * if (happyPlaceSubDesc.size() >= happyPlaceSubTitle.size()) {
				 * tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations",
				 * Status.PASS, "Its a happy place descriptions present ");
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations",
				 * Status.FAIL,
				 * "Its a happy place descriptions not present in all the sub titles " );
				 * 
				 * }
				 * 
				 * } else { tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations",
				 * Status.FAIL, "Its a happy place header section not present "); }
				 */

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.FAIL,
						"Orlando Resort Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "orlandoValidations", Status.FAIL,
					"Orlando article not present in the Page ");
		}

	}

	protected By expandAllList = By.xpath("//ul[@class='accordion']//li[@class='accordion-item ']//a[@role='tab']");
	protected By resortLinks = By.xpath("//ul[@class='accordion']/li/a");

	public void validateExpandAll() {
		try {
			waitUntilElementVisibleBy(driver, expandAllList, 120);
			getElementInView(expandAllList);
			List<WebElement> expandAll = driver.findElements(expandAllList);
			List<WebElement> resortLinksList = driver.findElements(resortLinks);

			for (int i = 0; i < expandAll.size(); i++) {

				tcConfig.updateTestReporter("NextGenHomePage", "validateExpandAll", Status.PASS,
						"Link is present as " + resortLinksList.get(i).getText());
				expandAll.get(i).click();

			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHomePage", "validateExpandAll", Status.FAIL, "Link is not present");
		}
	}

	protected By amenitiesHeader = By.xpath("//div[text()='Amenities']");
	protected By imgamenitiesList = By.xpath("//h2[text()='Amenities']/../ul/li/img");
	protected By amenitiesText = By.xpath("//h2[text()='Amenities']/../ul/li/span");

	public void validateAmenities() {
		waitUntilElementVisibleBy(driver, amenitiesHeader, 120);
		getElementInView(amenitiesHeader);
		if (verifyObjectDisplayed(amenitiesHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "validateAmenities", Status.PASS,
					"Amenities Header is present");
			List<WebElement> imgList = driver.findElements(imgamenitiesList);
			List<WebElement> amenitieslist = driver.findElements(amenitiesText);

			for (int i = 0; i < imgList.size(); i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "validateAmenities", Status.PASS,
						"Amenities image is present with " + amenitieslist.get(i).getText());
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "validateAmenities", Status.FAIL,
					"Amenities Header is not present");
		}
	}

	/*
	 * Method: orlandoTheaterMuseum Description: Orlando Theater and Museum Section
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void orlandoTheaterMuseum() {

		getElementInView(theaterMuseumsHeader);

		if (verifyObjectDisplayed(theaterMuseumsHeader)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.PASS,
					"Theaters and museum section present in the page");
			if (verifyObjectDisplayed(twoColumnVal)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.PASS,
						"Two Coloumn section present in the page");
				List<WebElement> sectionHeader = driver.findElements(twoCoumnHeader);
				// List<WebElement> readMoreList=
				// driver.findElements(readMoreLink);
				int totalSize = sectionHeader.size();
				for (int i = 0; i < totalSize; i++) {
					List<WebElement> sectionHeader1 = driver.findElements(twoCoumnHeader);
					List<WebElement> readMoreList = driver.findElements(readMoreLink);
					tcConfig.updateTestReporter("NextGenResortsPageNextGenResortsPage", "orlandoTheaterMuseum",
							Status.PASS, (i + 1) + " header is: " + sectionHeader1.get(i).getText());

					readMoreList.get(i).click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					// driver.navigate().refresh();
					theaterMuseumPageVal(i);

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, twoColumnVal, 120);

				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.FAIL,
						"Two Coloumn section not present in the page");
			}
			waitUntilElementVisibleBy(driver, localFav, 120);
			if (verifyObjectDisplayed(localFav)) {
				getElementInView(localFav);
				tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.PASS,
						"Local Fav section present in th page");

				if (verifyObjectDisplayed(favArticleHeader)) {
					List<WebElement> articleListHdr = driver.findElements(favArticleHeader);
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.PASS,
							"Local fav articles present, total: " + articleListHdr.size());
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.FAIL,
							"Local fav articles not present");
				}

				if (verifyObjectDisplayed(favArticleImage)) {
					List<WebElement> articleListImg = driver.findElements(favArticleImage);
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.PASS,
							"Local fav articles images present, total: " + articleListImg.size());
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.FAIL,
							"Local fav articles  images not present");
				}

				if (verifyObjectDisplayed(favArticleDesc)) {
					List<WebElement> articleListDesc = driver.findElements(favArticleDesc);
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.PASS,
							"Local fav articles Desc present, total: " + articleListDesc.size());
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.FAIL,
							"Local fav articles  Desc not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.FAIL,
						"Local Fav section not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoTheaterMuseum", Status.FAIL,
					"Theaters and museum section not present in the page");
		}

	}

	/*
	 * Method: orlandoOutdoors Description: Orlando Outdoors Section Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void orlandoOutdoors() {

		getElementInView(exploreGreatOutDoors);

		if (verifyObjectDisplayed(exploreGreatOutDoors)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS,
					"Explore Great Resorts Outdoors section present in the page ");

			List<WebElement> carouselArtList = driver.findElements(totalCarouselArtcl);
			List<WebElement> carouselHeaders = driver.findElements(carouselHeader);
			List<WebElement> carouselImageList = driver.findElements(carouselImage);
			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS,
					"Total articles present in the carousel is: " + carouselArtList.size());
			for (int i = 0; i < carouselArtList.size(); i++) {
				tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS,
						"Header no " + (i + 1) + " is " + carouselHeaders.get(i).getText());

				if (carouselImageList.size() >= 5) {
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS, "Image present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.FAIL,
							"Image not present");
				}

				clickElementBy(carouselNext);

			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.FAIL,
					"Explore Great Resorts Outdoors section not present in the page ");
		}

	}

	/*
	 * Method: orlandoThemePark Description: Orlando Theme Park Section Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void orlandoThemePark() {

		getElementInView(themePark);
		if (verifyObjectDisplayed(themePark)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS,
					"Orlando Theme Parksection present");

			List<WebElement> themeParkArtList = driver.findElements(themeParkHeader);
			List<WebElement> themeparkArtImages = driver.findElements(themeParkImages);

			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS,
					"Total articles present in the Theme Park section is: " + themeParkArtList.size());
			for (int i = 0; i < themeParkArtList.size(); i++) {

				getElementInView(themeParkArtList.get(i));

				tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS,
						"Header no " + (i + 1) + " is " + themeParkArtList.get(i).getText());

				if (verifyElementDisplayed(themeparkArtImages.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.PASS,
							"Image present in the article");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.FAIL,
							"Image present in the article");
				}

			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "orlandoOutdoors", Status.FAIL,
					"Orlando Theme Park section not present");
		}

	}

	/*
	 * Method: discoverOrlando Description: Discover Orlando Section Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void discoverOrlando() {
		getElementInView(discoverOrlandoHdr);
		if (verifyObjectDisplayed(discoverOrlandoHdr)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.PASS,
					"Discover Orlando Header present in the page");

			List<WebElement> discoverOrArticleHdr = driver.findElements(discoverArticleHeader);
			List<WebElement> discoverOrArticleImg = driver.findElements(discoverArticleImages);
			List<WebElement> discoverOrArticleLoc = driver.findElements(discoverArticleLocation);
			tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.PASS,
					"Total articles present in the section is: " + discoverOrArticleHdr.size());
			for (int i = 0; i < discoverOrArticleHdr.size(); i++) {
				tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.PASS,
						"Header no " + (i + 1) + " is " + discoverOrArticleHdr.get(i).getText());

				if (verifyElementDisplayed(discoverOrArticleImg.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.PASS, "Image present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.FAIL,
							"Image not present");
				}

				if (verifyElementDisplayed(discoverOrArticleLoc.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.PASS,
							"Location present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.FAIL,
							"Location not present");
				}

			}

			if (verifyObjectDisplayed(seeAllResorts)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.PASS,
						"See All Resorts Present");
				clickElementBy(seeAllResorts);

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().contains(testData.get("exploreResortUrl"))) {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.PASS,
							"Explore Resort Page Navigated");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "homepageHeroImageVal", Status.FAIL,
							"Explore Resort Page Navigation error");
				}

				driver.navigate().back();

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.FAIL,
						"See All Resorts not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "discoverOrlando", Status.FAIL,
					"Discover Orlando Header not present in the page");
		}
	}

	/*
	 * Method: theaterMuseumPageVal Description: Theater Museum page Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void theaterMuseumPageVal(int validationSection) {

		if (validationSection == 0) {
			waitUntilElementVisibleBy(driver, theaterHeader, 120);
			if (verifyObjectDisplayed(theaterHeader)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
						"Navigated to  Orlando Theater page, Header present");

				if (verifyObjectDisplayed(theaterHeroImg)) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Hero Image Present in Orlando Theater Page");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Hero Image not Present in Orlando Theater Page");
				}

				if (verifyObjectDisplayed(theaterBrdCrmb)) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"BreadCrumb Present in Orlando Theater Page");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Breadcrumb not Present in Orlando Theater Page");
				}

				List<WebElement> subSectionList = driver.findElements(theaterSubSectionHdr);
				tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
						"Total Sub Articles: " + subSectionList.size());
				for (int i = 0; i < subSectionList.size(); i++) {
					getElementInView(subSectionList.get(i));
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Header No " + (i + 1) + " is " + subSectionList.get(i).getText());
				}

				List<WebElement> subSectionImage = driver.findElements(theaterSubSectionImages);
				List<WebElement> subSectionDesc = driver.findElements(theaterSubSectionDesc);
				if (subSectionImage.size() == subSectionList.size()) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Images present in all section");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Images not present in all section");
				}

				if (subSectionDesc.size() >= subSectionList.size()) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Desc present in all section");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Desc not present in all section");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
						"Navigation to  Orlando Theater page failed");
			}

		} else if (validationSection == 1) {

			waitUntilElementVisibleBy(driver, museumHeader, 120);
			if (verifyObjectDisplayed(museumHeader)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
						"Navigated to  Orlando museum page, Header present");

				if (verifyObjectDisplayed(museumHeroImg)) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Hero Image Present in Orlando museum Page");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Hero Image not Present in Orlando museum Page");
				}

				if (verifyObjectDisplayed(museumBrdCrmb)) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"BreadCrumb Present in Orlando museum Page");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Breadcrumb not Present in Orlando museum Page");
				}

				if (verifyObjectDisplayed(exploreOrlandoHdr)) {
					getElementInView(exploreOrlandoHdr);
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Explore Orlando Section present");
					if (verifyObjectDisplayed(exploreOrlandoDesc)) {
						tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
								"Explore Orlando description present");
					} else {
						tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
								"Explore Orlando description not present");
					}
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Explore Orlando Section not present");
				}

				List<WebElement> subSectionList = driver.findElements(museumSubSectionHdr);
				tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
						"Total Sub Articles: " + subSectionList.size());
				for (int i = 0; i < subSectionList.size(); i++) {
					getElementInView(subSectionList.get(i));
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Header No " + (i + 1) + " is " + subSectionList.get(i).getText());
				}

				List<WebElement> subSectionImage = driver.findElements(museumSubSectionImages);
				List<WebElement> subSectionDesc = driver.findElements(museumSubSectionDesc);
				if (subSectionImage.size() == subSectionList.size()) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Images present in all section");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Images not present in all section");
				}

				if (subSectionDesc.size() >= subSectionList.size()) {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.PASS,
							"Desc present in all section");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "theaterMuseumPageVal", Status.FAIL,
							"Desc not present in all section");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "museumMuseumPageVal", Status.FAIL,
						"Navigation to  Orlando museum page failed");
			}

		}

	}

	/*
	 * Method: ownerOffersOrlando Description: Owner Offers Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void ownerOffersOrlando() {

		getElementInView(ownerOffersHeadr);
		if (verifyObjectDisplayed(ownerOffersHeadr)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.PASS,
					"Owner Offers Header present in the page");

			List<WebElement> ownerOffrsArtHdr = driver.findElements(ownerOffrsHeaders);
			List<WebElement> ownerOffrsArtImg = driver.findElements(ownerOfrsImages);
			List<WebElement> ownerOffrsArtDesc = driver.findElements(ownerOffrsDesc);
			List<WebElement> ownerOffrsArtBookNow = driver.findElements(ownerOffersBookNow);
			tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.PASS,
					"Total articles present in the section is: " + ownerOffrsArtHdr.size());
			for (int i = 0; i < ownerOffrsArtHdr.size(); i++) {
				tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.PASS,
						"Header no " + (i + 1) + " is " + ownerOffrsArtHdr.get(i).getText());

				if (verifyElementDisplayed(ownerOffrsArtImg.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.PASS,
							"Image present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.FAIL,
							"Image not present");
				}

				if (verifyElementDisplayed(ownerOffrsArtDesc.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.PASS,
							"Description is present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.FAIL,
							"Description not present");
				}

				if (verifyElementDisplayed(ownerOffrsArtBookNow.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.PASS,
							"Book Now button is present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.FAIL,
							"Book Now button not present");
				}

			}

			if (verifyObjectDisplayed(seeAllOffers)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.PASS,
						"See All Offers Present");
				clickElementBy(seeAllOffers);

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().contains(testData.get("AllDeals"))) {
					tcConfig.updateTestReporter("NextGenHomePage", "ownerOffersOrlando", Status.PASS,
							"Deals and Offers Page Navigated");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "ownerOffersOrlando", Status.FAIL,
							"Deals and Offers Page Navigation error");
				}

				driver.navigate().back();

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.FAIL,
						"See All Offers not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "ownerOffersOrlando", Status.FAIL,
					"Owner Offers Header not present in the page");
		}

	}

	// Austin Resort Validations

	protected By austinArticle = By.xpath("//h3[@class='wyn-title__title' and contains(.,'Austin')]");
	protected By austinTitle = By.xpath("//div[@class='wyn-headline ']/p");
	protected By austinHeader = By
			.xpath("//div[@class='wyn-headline ']/p/ancestor::div[@class='column wyn-hero__column ']//h2");
	protected By austinDesc = By.xpath(
			"//div[@class='wyn-headline ']/p/ancestor::div[@class='column wyn-hero__column ']//div[@class='wyn-l-content wyn-card__copy']/p");
	protected By austinBreadCrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Austin')]");

	protected By discoverWhatToDoHdr = By.xpath("//h2[contains(.,'DISCOVER WHAT TO DO')]");
	protected By sectionSubHeader = By
			.xpath("//h2[contains(.,'DISCOVER WHAT')]/ancestor::div[@gtm_component='icon-list']//h3");
	protected By subsectionDesc = By
			.xpath("//h2[contains(.,'DISCOVER WHAT')]/ancestor::div[@gtm_component='icon-list']//p");

	protected By musicSceneHdr = By.xpath("//h1[contains(.,'MUSIC SCENE')]");
	protected By musicSceneArtImage = By.xpath("//div[@gtm_component='two-columns-container']//img");
	protected By musicSceneArtDesc = By.xpath("//div[@gtm_component='two-columns-container']//p");
	protected By musicSceneArtHdr = By.xpath("//div[@gtm_component='two-columns-container']//h2");

	protected By foodSectionHdr = By.xpath("//h2[contains(.,'Food')]");
	protected By foodArticleImages = By.xpath("//h2[contains(.,'Food')]//ancestor::div[@gtm_component='cards']//img");
	protected By foodArticleHeader = By.xpath("//h2[contains(.,'Food')]//ancestor::div[@gtm_component='cards']//h3");
	protected By foodArticleDesc = By.xpath("//h2[contains(.,'Food')]//ancestor::div[@gtm_component='cards']//p");
	protected By oddBallHdr = By.xpath("//h1[contains(.,'ODdball')]");

	protected By quoteContainer = By.xpath("//div[@class='wyn-quote__container']");
	protected By quoteTxt = By.xpath("//div[@class='wyn-quote__container']//h4");
	protected By quoterName = By.xpath("//div[@class='wyn-quote__container']//p[@class='wyn-quote__attribute']");

	protected By cityGuideHdr = By.xpath("//h1[contains(.,'austin city guide')]");
	protected By cityGuideSubHdr = By.xpath("//div[@class='wyn-content--last']//h2");
	protected By cityGuidesImages = By.xpath("//div[@class='wyn-content--last']//img");
	protected By cityGuidesDesc = By.xpath("//div[@class='wyn-content--last']//h4/b");

	protected By discoverMoreHdr = By.xpath("//h2[contains(.,'Discover more Texas')]");
	protected By discoverMoreCardsImg = By
			.xpath("//h2[contains(.,'Discover more Texas')]/ancestor::div[@gtm_component='cards']//img");
	protected By discoverMoreCardsHdr = By
			.xpath("//h2[contains(.,'Discover more Texas')]/ancestor::div[@gtm_component='cards']//h3");
	protected By discoverMoreCardsLocation = By
			.xpath("//h2[contains(.,'Discover more Texas')]/ancestor::div[@gtm_component='cards']//h4");
	protected By discoverMoreCardsSeeAll = By.xpath(
			"//h2[contains(.,'Discover more Texas')]/ancestor::div[@gtm_component='cards']//a[contains(.,'See all')]");

	protected By dealsOffrshdr = By.xpath("//h2[contains(.,'Deals and Offers')]");
	protected By dealsOffrsSeeAll = By
			.xpath("//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[contains(.,'See All Deals')]");
	protected By dealsOffrsArtImages = By
			.xpath("//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//img");
	protected By dealsOffrsArtHdr = By
			.xpath("//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//h3");
	protected By dealsOffrsArtDesc = By
			.xpath("//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//p");
	protected By dealsOffrsArtBtn = By.xpath(
			"//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//div[@class='wyn-button-cta']");
	protected By travelDealsBRCm = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel')]");

	/*
	 * Method: austinValidations Description: Austin Resorts page Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void austinValidations() {
		getElementInView(austinArticle);

		if (verifyObjectDisplayed(austinArticle)) {
			tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.PASS,
					"Austin article present in the Page ");

			clickElementBy(austinArticle);

			waitUntilElementVisibleBy(driver, austinHeader, 120);

			if (verifyObjectDisplayed(austinHeader)) {
				tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.PASS,
						"Austin Resort Navigated");

				if (verifyObjectDisplayed(austinBreadCrumb)) {
					tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.PASS,
							"Austin Breadcrumb present");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.FAIL,
							"Austin Breadcrumb not present");
				}

				if (verifyObjectDisplayed(austinTitle)) {
					tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.PASS,
							"Austin Title present as: " + getElementText(austinTitle));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.FAIL,
							"Austin Title not present");
				}

				if (verifyObjectDisplayed(austinDesc)) {
					tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.PASS,
							"Austin description  present");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.FAIL,
							"Austin description not  present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.FAIL,
						"Austin Resort Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "austinValidations", Status.PASS,
					"Austin article not present in the Page ");
		}

	}

	/*
	 * Method: austinDiscoverVal Description: Descover Austin Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void austinDiscoverVal() {

		getElementInView(discoverWhatToDoHdr);

		if (verifyObjectDisplayed(discoverWhatToDoHdr)) {
			tcConfig.updateTestReporter("NextGenHomePage", "austinDiscoverVal", Status.PASS,
					"Discover What To Do  header section present ");

			List<WebElement> whatToDoSubTitle = driver.findElements(sectionSubHeader);
			List<WebElement> whatToDoSubDesc = driver.findElements(subsectionDesc);
			tcConfig.updateTestReporter("NextGenHomePage", "austinDiscoverVal", Status.PASS,
					"Total Subtitle: " + whatToDoSubTitle.size());
			for (int i = 0; i < whatToDoSubTitle.size(); i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "austinDiscoverVal", Status.PASS,
						(i + 1) + " title is " + whatToDoSubTitle.get(i).getText());

			}

			if (whatToDoSubDesc.size() >= whatToDoSubTitle.size()) {
				tcConfig.updateTestReporter("NextGenHomePage", "austinDiscoverVal", Status.PASS,
						"Discover What To Do  descriptions present ");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "austinDiscoverVal", Status.FAIL,
						"Discover What To Do description  not present in all the sub titles ");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "austinDiscoverVal", Status.FAIL,
					"Discover What To Do header section not present ");
		}

	}

	/*
	 * Method: musicSceneVal Description: Austin Music Scene Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void musicSceneVal() {

		getElementInView(musicSceneHdr);

		if (verifyObjectDisplayed(musicSceneHdr)) {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
					"Music Scene section present in the page");

			List<WebElement> musicArtileHdrList = driver.findElements(musicSceneArtHdr);
			List<WebElement> musicArtileImage = driver.findElements(musicSceneArtImage);
			List<WebElement> musicArtileHdrDesc = driver.findElements(musicSceneArtDesc);

			for (int i = 0; i < 2; i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
						"Header " + (i + 1) + " in music scene is " + musicArtileHdrList.get(i).getText());

				if (verifyElementDisplayed(musicArtileImage.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
							"Image Present for this article");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
							"Image not Present for this article");
				}

				if (verifyElementDisplayed(musicArtileHdrDesc.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
							"Desc Present for this article");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
							"Desc not  Present for this article");
				}
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
					"Music Scene section not present in the page");
		}

		getElementInView(foodSectionHdr);

		if (verifyObjectDisplayed(foodSectionHdr)) {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
					"Food section present in the page");

			List<WebElement> foodArtileHdrList = driver.findElements(foodArticleHeader);
			List<WebElement> foodArtileImageList = driver.findElements(foodArticleImages);
			List<WebElement> foodArtileHdrDesc = driver.findElements(foodArticleDesc);

			for (int i = 0; i < foodArtileHdrList.size(); i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
						"Header " + (i + 1) + " in Food is " + foodArtileHdrList.get(i).getText());

				if (verifyElementDisplayed(foodArtileImageList.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
							"Image Present for this article");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
							"Image not Present for this article");
				}

				if (verifyElementDisplayed(foodArtileHdrDesc.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
							"Desc Present for this article");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
							"Desc not  Present for this article");
				}
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
					"FOOD section not present in the page");
		}

		getElementInView(oddBallHdr);

		if (verifyObjectDisplayed(oddBallHdr)) {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
					"Oddball Landmarks  section present in the page");

			List<WebElement> odballLMArtileHdrList = driver.findElements(musicSceneArtHdr);
			List<WebElement> odballLMArtileImage = driver.findElements(musicSceneArtImage);
			List<WebElement> odballLMArtileHdrDesc = driver.findElements(musicSceneArtDesc);

			for (int i = 2; i < 4; i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
						"Header " + (i + 1) + " in music scene is " + odballLMArtileHdrList.get(i).getText());

				if (verifyElementDisplayed(odballLMArtileImage.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
							"Image Present for this article");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
							"Image not Present for this article");
				}

				if (verifyElementDisplayed(odballLMArtileHdrDesc.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
							"Desc Present for this article");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
							"Desc not  Present for this article");
				}
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
					"Oddball Landmarks section not present in the page");
		}

		getElementInView(quoteContainer);

		if (verifyObjectDisplayed(quoteTxt)) {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.PASS,
					"Quote Present in the page by " + getElementText(quoterName));
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "musicSceneVal", Status.FAIL,
					"Quote not Present in the page");
		}

	}

	/*
	 * Method: cityGuideValidations Description: Austin City Guides Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void cityGuideValidations() {

		getElementInView(cityGuideHdr);

		if (verifyObjectDisplayed(cityGuideHdr)) {
			tcConfig.updateTestReporter("NextGenHomePage", "cityGuideValidations", Status.PASS,
					"City Guide section present in the page");

			List<WebElement> cityGdeArtileHdrList = driver.findElements(cityGuideSubHdr);
			List<WebElement> cityGdeArtileImage = driver.findElements(cityGuidesImages);
			List<WebElement> cityGdeArtileHdrDesc = driver.findElements(cityGuidesDesc);

			for (int i = 0; i < cityGdeArtileHdrList.size(); i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "cityGuideValidations", Status.PASS,
						"Header " + (i + 1) + " in music scene is " + cityGdeArtileHdrList.get(i).getText());

				if (verifyElementDisplayed(cityGdeArtileImage.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "cityGuideValidations", Status.PASS,
							"Image Present for this article");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "cityGuideValidations", Status.FAIL,
							"Image not Present for this article");
				}

				if (verifyElementDisplayed(cityGdeArtileHdrDesc.get(i))) {
					tcConfig.updateTestReporter("NextGenHomePage", "cityGuideValidations", Status.PASS,
							"Desc Present for this article");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "cityGuideValidations", Status.FAIL,
							"Desc not  Present for this article");
				}
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "cityGuideValidations", Status.FAIL,
					"City Guide section not present in the page");
		}
	}

	/*
	 * Method: discoverTexasVal Description: Austin Discover Texas Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void discoverTexasVal() {

		getElementInView(discoverMoreHdr);
		if (verifyObjectDisplayed(discoverMoreHdr)) {
			tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexasVal", Status.PASS,
					"Discover Texas Header present in the page");

			List<WebElement> discoverTxArticleHdr = driver.findElements(discoverMoreCardsHdr);
			List<WebElement> discoverTxArticleImg = driver.findElements(discoverMoreCardsImg);
			List<WebElement> discoverTxArticleLoc = driver.findElements(discoverMoreCardsLocation);
			tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.PASS,
					"Total articles present in the section is: " + discoverTxArticleHdr.size());
			for (int i = 0; i < discoverTxArticleHdr.size(); i++) {
				tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.PASS,
						"Header no " + (i + 1) + " is " + discoverTxArticleHdr.get(i).getText());

				if (verifyElementDisplayed(discoverTxArticleImg.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.PASS, "Image present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.FAIL,
							"Image not present");
				}

				if (verifyElementDisplayed(discoverTxArticleLoc.get(i))) {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.PASS, "Location present");
				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.FAIL,
							"Location not present");
				}

			}

			if (verifyObjectDisplayed(discoverMoreCardsSeeAll)) {
				tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.PASS,
						"See All Resorts Present");
				clickElementBy(discoverMoreCardsSeeAll);

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().contains(testData.get("exploreResortUrl"))) {
					tcConfig.updateTestReporter("NextGenHomePage", "discoverTexas", Status.PASS,
							"Explore Resort Page Navigated");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "discoverTexas", Status.FAIL,
							"Explore Resort Page Navigation error");
				}

				driver.navigate().back();

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.FAIL,
						"See All Resorts not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "discoverTexas", Status.FAIL,
					"Discover Texas Header not present in the page");
		}

	}

	/*
	 * Method: dealsOffersVal Description: Austin Deals and Offers Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void dealsOffersVal() {

		waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

		if (verifyObjectDisplayed(dealsOffrshdr)) {
			getElementInView(dealsOffrshdr);

			tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
					"Deals and offers section with title present on the page");

			if (verifyObjectDisplayed(dealsOffrsArtHdr)) {
				List<WebElement> totalCardsList = driver.findElements(dealsOffrsArtHdr);
				List<WebElement> totalCardsImageList = driver.findElements(dealsOffrsArtImages);
				List<WebElement> totalCardsTitleList = driver.findElements(dealsOffrsArtHdr);
				List<WebElement> totalCardsDescList = driver.findElements(dealsOffrsArtDesc);
				List<WebElement> totalCardsSeeOffList = driver.findElements(dealsOffrsArtBtn);

				tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
						"Total Cards in Deals and Offers section is " + totalCardsList.size());

				if (totalCardsList.size() == totalCardsImageList.size()) {

					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
							"Image present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.FAIL,
							"Image not present in all cards");
				}

				if (totalCardsList.size() == totalCardsTitleList.size()) {

					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
							"Title present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.FAIL,
							"Title not present in all cards");
				}

				if (totalCardsList.size() == totalCardsDescList.size()) {

					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
							"Description present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.FAIL,
							"Description not present in all cards");
				}

				if (totalCardsList.size() == totalCardsSeeOffList.size()) {

					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
							"See Offers present in all cards");

					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
							"Clicking on first Card with title " + totalCardsTitleList.get(0).getText());

					String firstTitle = totalCardsTitleList.get(0).getText().toUpperCase();
					totalCardsSeeOffList.get(0).click();

					waitUntilElementVisibleBy(driver, travelDealsBRCm, 120);
					if (verifyObjectDisplayed(travelDealsBRCm)) {
						tcConfig.updateTestReporter("NextGenResortsPage", "homepageHeroImageVal", Status.PASS,
								"Clicked Resort page navigated" + driver.getTitle());

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenResortsPage", "homepageHeroImageVal", Status.FAIL,
								"Clicked resort page navigation error, title " + driver.getTitle());
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.FAIL,
							"See Offers not present in all cards");
				}

				if (verifyObjectDisplayed(dealsOffrsSeeAll)) {
					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.PASS,
							"See all deals and offers link present on homepage");

					clickElementJSWithWait(dealsOffrsSeeAll);

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains(testData.get("AllDeals").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenResortsPage", "homepageHeroImageVal", Status.PASS,
								"Navigated to All deals and offers page");

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenResortsPage", "homepageHeroImageVal", Status.FAIL,
								"All deals and offers page navigation failed");
					}
					driver.navigate().back();

					waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.FAIL,
							"See all deals and offers link not present on page");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.FAIL,
						"Cards not present in Deals and offers page");

			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsPage", "dealsOffersVal", Status.FAIL,
					"Deals and offers section with title not present on the page");
		}

	}

}
