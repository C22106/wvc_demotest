package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenTravelPage_IOS extends NextGenTravelPage_Web {

	public static final Logger log = Logger.getLogger(NextGenTravelPage_IOS.class);

	public NextGenTravelPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
