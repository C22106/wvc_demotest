package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class BuyACWTimeshare_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(BuyACWTimeshare_Web.class);

	public BuyACWTimeshare_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By buyCWTimeshareHeader = By.xpath(/* "//h2[contains(.,'Buying a Club Wyndham Timeshare')]" */
			"(//div[contains(text(),'BUYING A CLUB WYNDHAM TIMESHARE')])[1]");
	protected By buyCWTimeshareImage = By.xpath(
			/*
			 * "//h2[contains(.,'Buying a Club Wyndham Timeshare')]//ancestor::div[@class='wyn-hero wyn-hero--large']//img"
			 */
			"(//div[contains(text(),'BUYING A CLUB WYNDHAM TIMESHARE')])[1]/ancestor::div[@class='cardBanner']//img");
	protected By buyCWTimeshareDesc = By.xpath(/*
												 * "//h2[contains(.,'Buying a Club Wyndham Timeshare')]//parent::div/h3"
												 */
			"(//div[contains(text(),'BUYING A CLUB WYNDHAM TIMESHARE')])[1]/..//div[@class='body-1']/p");
	protected By buyCWTimeshareBreadCrumb = By.xpath(/* "//nav[@class='wyn-breadcrumbs']//a[contains(.,'Buying')]" */
			"//div[@class='breadcrumb']//a[contains(.,'Buying')]");
	protected By sectionHeaders = By.xpath(/* "//div[@class='wyn-rich-text wyn-l-content']//h1" */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']//h3");
	protected By sectionText = By.xpath(/*
										 * "//div[@class='two-columns-container']//div[@class='wyn-rich-text wyn-l-content']//p"
										 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']//h3//..//..//..//..//..//div[contains(@class,'body-1')]/p");
	protected By typesOfTimeshareHeader = By.xpath(/* "//h1[contains(.,'Types of Timeshare Costs')]" */
			"//div[contains(text(),'Type of timeshare cost')]");
	protected By slidingTabsHeader = By.xpath("//ul[@role='tablist']//a");
	protected By slidingTabsActive = By.xpath("//ul[@role='tablist']/li[@class]");
	protected By slidingTabsDesc = By.xpath(/* "//ul[@role='tablist']/following-sibling::div//p" */
			"//ul[@role='tablist']//following::div[@class='tabs-content']//div[contains(@class,'body-1')]/p");
	// div[@gtm_component='two-columns-container']
	protected By buyATimeShare = By.xpath(/* "//h2[contains(.,'Buying a Club')]" */
			"//div[contains(text(),'Buying a Club')]");
	protected By buyATimeShareCTA = By.xpath(
			/*
			 * "//h2[contains(.,'Buying a Club')]/ancestor::div[@class='wyn-card__content']//a[contains(.,'Read')]"
			 */
			"//div[contains(text(),'Buying a Club')]//..//../a/div[contains(text(),'Read More')]");
	protected By twoColumnVal = By.xpath("//div[@class='cardComponent']//img");
	protected By twoCoumnHeader = By.xpath(
			"//div[@class='cardComponent']//img//..//..//div[@class='card-section']//div[contains(@class,'subtitle-2')]");
	protected By readMoreLink = By.xpath(
			"//div[@class='cardComponent']//img//..//..//div[@class='card-section']//a//div[contains(text(),'Read More')]");

	/*
	 * Method: navigateToBuyCWTimeshare Description: CW Timeshare Page Nav Date:
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToBuyCWTimeshare() {

		waitUntilElementVisibleBy(driver, buyATimeShare, 120);

		getElementInView(buyATimeShare);

		if (verifyObjectDisplayed(buyATimeShare)) {
			tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.PASS,
					"Buying A Timeshare block present in the page");

			clickElementBy(buyATimeShareCTA);

			waitUntilElementVisibleBy(driver, buyCWTimeshareHeader, 120);

			if (verifyObjectDisplayed(buyCWTimeshareHeader)) {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.PASS,
						"Buying A Timeshare page navigated");

			} else {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.FAIL,
						"Buying A Timeshare page navigation failed");

			}

			if (verifyObjectDisplayed(buyCWTimeshareBreadCrumb)) {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.PASS,
						"Buying A Timeshare breadcrumb present");

			} else {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.FAIL,
						"Buying A Timeshare breadcrumb not present");

			}

			if (verifyObjectDisplayed(buyCWTimeshareImage)) {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.PASS,
						"Buying A Timeshare hero image present");

			} else {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.FAIL,
						"Buying A Timeshare hero image not present");

			}

			if (verifyObjectDisplayed(buyCWTimeshareDesc)) {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.PASS,
						"Buying A Timeshare description present");

			} else {
				tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.FAIL,
						"Buying A Timeshare description not present");

			}

		} else {
			tcConfig.updateTestReporter("BuyACWTimeshare", "navigateToBuyTimeshare", Status.FAIL,
					"Buying A Timeshare block not present in the page");
		}

	}

	/*
	 * Method: validateBuyCWTimeshare Description: CW Timeshare Page Validations
	 * Date: Jul/2019 Author: Unnat Jain Changes By
	 */
	public void validateBuyCWTimeshare() {

		List<WebElement> sectionalHeaderList = driver.findElements(sectionHeaders);
		List<WebElement> sectionTextList = driver.findElements(sectionText);

		tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.PASS,
				"Sectional Headers present, total " + sectionalHeaderList.size());

		if (sectionalHeaderList.size() == sectionTextList.size()) {
			tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.PASS,
					"All the headers contains description");

		} else {
			tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.FAIL,
					"All the headers does not contains description");
		}

		for (int i = 0; i < sectionalHeaderList.size(); i++) {
			getElementInView(sectionalHeaderList.get(i));
			tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.PASS,
					(i + 1) + " Header is: " + sectionalHeaderList.get(i).getText());

		}

	}

	/*
	 * Method: slidingTabsValidations Description: CW Timeshare Page Sliding Tabs
	 * Val Date: Jul/2019 Author: Unnat Jain Changes By
	 */
	public void slidingTabsValidations() {
		getElementInView(typesOfTimeshareHeader);
		if (verifyObjectDisplayed(typesOfTimeshareHeader)) {
			tcConfig.updateTestReporter("BuyACWTimeshare", "slidingTabsValidations", Status.PASS,
					"Types of timeshare header present");

			List<WebElement> slidingTabs = driver.findElements(slidingTabsHeader);
			List<WebElement> slidingTabsDescList = driver.findElements(slidingTabsDesc);
			tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.PASS,
					"Total Tabs: " + slidingTabs.size());
			for (int i = 0; i < slidingTabs.size(); i++) {

				slidingTabs.get(i).click();
				tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.PASS,
						(i + 1) + " tab header is " + slidingTabs.get(i).getText());

				if (verifyElementDisplayed(slidingTabsDescList.get(i))) {
					tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.PASS,
							"Description present " + slidingTabsDescList.get(i).getText());
				} else {
					tcConfig.updateTestReporter("BuyACWTimeshare", "validateBuyCWTimeshare", Status.FAIL,
							"Description not present");
				}

			}

		} else {
			tcConfig.updateTestReporter("BuyACWTimeshare", "slidingTabsValidations", Status.FAIL,
					"Types of timeshare header not present");
		}

	}

	/*
	 * Method twoContainerValidations Description CW Timeshare page sliding Tabs Val
	 * Date Jul/2019 Author Unnat Jain Changes By
	 */
	public void twoContainerValidations() {

		getElementInView(twoColumnVal);

		if (verifyObjectDisplayed(twoColumnVal)) {
			tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
					"Two Coloumn section present in the page");
			List<WebElement> sectionHeader = driver.findElements(twoCoumnHeader);
			// List<WebElement> readMoreList= driver.findElements(readMoreLink);
			int totalSize = sectionHeader.size();
			for (int i = 0; i < totalSize; i++) {
				List<WebElement> sectionHeader1 = driver.findElements(twoCoumnHeader);
				List<WebElement> readMoreList = driver.findElements(readMoreLink);
				tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
						(i + 1) + " header is: " + sectionHeader1.get(i).getText());

				readMoreList.get(i).click();

				waitForSometime(tcConfig.getConfig().get("LongWait"));
				// driver.navigate().refresh();
				System.out.println(driver.getTitle().toUpperCase());
				System.out.println(testData.get("header" + (i + 1) + ""));
				if (driver.getTitle().toUpperCase().contains(testData.get("header" + (i + 1) + "").toUpperCase())) {

					tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
							"Navigated to " + (i + 1) + " page");

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, twoColumnVal, 120);

				}

			}

		} else {
			tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.FAIL,
					"Two Coloumn section not present in the page");
		}

	}

}
