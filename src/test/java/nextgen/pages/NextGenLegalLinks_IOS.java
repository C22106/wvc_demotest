package nextgen.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenLegalLinks_IOS extends NextGenLegalLinks_Web {

	public static final Logger log = Logger.getLogger(NextGenLegalLinks_IOS.class);

	public NextGenLegalLinks_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	
	@Override
	public void termsOfUseNavigations() {

		waitUntilElementVisibleWb(driver, termsFooterLink, "120");
		getElementInView(termsFooterLink);
		if (verifyElementDisplayed(termsFooterLink)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.PASS,
					"Terms of use Link Present");
			String href = getElementAttribute(termsFooterLink, "href");
			clickElementJSWithWait(termsFooterLink);
			pageCheck();
			assertTrue(getCurrentURL().equals(href), "Navigation Failed");
			driver.navigate().back();
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "termsOfUseNavigations", Status.FAIL,
					"Terms of Use Link not Present");

		}

	}
	
	@Override
	public void privacyNoticeValdns() {

		getElementInView(privacyNoticeLinks);
		if (verifyObjectDisplayed(privacyNoticeLinks)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.PASS,
					"Privacy Notice Link present");
			String href = getElementAttribute(privacyNoticeLinks, "href");
			clickElementJSWithWait(privacyNoticeLinks);
			pageCheck();
			assertTrue(getCurrentURL().equals(href), "Navigation Failed");
			driver.navigate().back();

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacyNoticeValdns", Status.FAIL,
					"Privacy Notice Link not present");
		}

	}
	
	@Override
	public void privacySettingsValdns() {

		getElementInView(privacyStngLinks);
		if (verifyObjectDisplayed(privacyStngLinks)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.PASS,
					"Privacy Setting Link present");
			String href = getElementAttribute(privacyStngLinks, "href");
			clickElementJSWithWait(privacyStngLinks);
			pageCheck();
			assertTrue(getCurrentURL().equals(href), "Navigation Failed");
			driver.navigate().back();

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "privacySettingsValdns", Status.FAIL,
					"Privacy Setting Link not present");
		}

	}

}

