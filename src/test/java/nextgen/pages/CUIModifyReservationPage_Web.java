package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CUIModifyReservationPage_Web extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(CUIModifyReservationPage_Web.class);

	public CUIModifyReservationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	
	protected By headerReservationDetails = By.xpath("//span[text() = 'Reservation Details']");
	protected By sectionLocationContainer = By.xpath("//section[@class = 'reservation-summary__location-container']/div/h2");
	protected By sectionDateContainerCheckin = By.xpath("(//span[contains(@class,'reservation-summary__dates')]/time)[1]");
	protected By sectionDateContainerCheckout = By.xpath("(//span[contains(@class,'reservation-summary__dates')]/time)[2]");
	protected By sectionUnitTypeContainer = By.xpath("//section[@class = 'reservation-summary__unit-type-container']/span");
	protected By sectionTotalCostContainer = By.xpath("//section[@class = 'reservation-summary__unit-cost-container']/span");
	protected By headerModifyReservation = By.xpath("//h1[text() = 'Modify Reservation']");
	protected By cardModifyReservation = By.xpath("//section[contains(@class,'modify-reservation__card notification')]");
	protected By textCardTitle = By.xpath("//h3[text() = 'Who’s Traveling']");
	protected By currentTraveler = By.xpath("(//p[text() = 'Traveler']/..//p)[2]");
	protected By buttonModifyTraveler = By.xpath("//a[text() = 'Modify Traveler']");
	protected By modificationHistoryList = By.xpath("//div[contains(@class,'holiday modification-history')]");
	protected By headerModification = By.xpath("//p[text() = 'Modifications']");
	protected By headerDate = By.xpath("//div[text() = 'Date']");
	protected By headerModificationType = By.xpath("//div[text() = 'Modification Type']");
	protected By sectionReservationSummary = By.xpath("//section[@class = 'reservation-summary']");
	protected By btnModifyUpgradeSelection = By.xpath("//a[text() = 'Modify Upgrade Selection']");
	
	public void verifyHeaderModifyReservation(){
		waitUntilElementVisibleBy(driver, headerModifyReservation, "ExplicitLongWait");
		elementPresenceVal(headerModifyReservation, "Header Modify Reservation", "CUIModifyReservationPage",
				"verifyHeaderModifyReservation");
	}
	
	public void verifyModifyCardDetails(){
		if (verifyObjectDisplayed(cardModifyReservation)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.PASS,
					"Modify Reservation Card is Present");
			elementPresenceVal(textCardTitle, "Card Title", "CUIModifyReservationPage","verifyModifyCardDetails");
			if (verifyObjectDisplayed(currentTraveler)) {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.PASS,
						"Current Traveler is: "+getObject(currentTraveler).getText().trim());
			}else{
				tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.FAIL,
						"Current Traveler name not present");
			}
		}else{
			tcConfig.updateTestReporter("CUIModifyReservationPage", "verifyModifyCardDetails", Status.FAIL,
					"Card Modify Reservation not Present");
		}
	}
	
	public void listModificationHistory(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		elementPresenceVal(headerModification, "Header Modification", "CUIModifyReservationPage","listModificationHistory");
		if (getList(modificationHistoryList).size() > 0) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "listModificationHistory", Status.PASS,
					"Total Number of Modification done are: "+getList(modificationHistoryList).size());
		}else{
			tcConfig.updateTestReporter("CUIModifyReservationPage", "listModificationHistory", Status.PASS,
					"No modification made");
		}
	}
	
	public void checkModificationHistoryCollapsedMode(){
		String modificationCollapse = null;
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(modificationHistoryList);
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList).size(); listmodificationiterator++) {
				WebElement eachModificationList = getObject(By.xpath("//div[contains(@class,'holiday modification-history')]["+listmodificationiterator+"]//div/a"));
				if (eachModificationList.getAttribute("aria-expanded").equalsIgnoreCase("false")) {
					getElementInView(eachModificationList);
					clickElementJSWithWait(eachModificationList);
					modificationCollapse = "Y";
					checkModificationDescription(listmodificationiterator);
					clickElementJSWithWait(eachModificationList);
				}else{
					modificationCollapse = "N";
					break;
				}
			}
			if (modificationCollapse.equals("Y")) {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode", Status.PASS,
						"All Modification History are in collapsed mode");
			}else{
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode", Status.PASS,
						"All Modification History are not in collapsed mode");
			}
		}else{
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode", Status.FAIL,
					"No modification history");
		}
		
		
	}
	public void vaildateHeaderDateinModificationHistory(){
		if (verifyObjectDisplayed(headerDate)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "vaildateHeaderDateinModificationHistory", Status.PASS,
					"Date Header is present in Modification History");
		}else{
			tcConfig.updateTestReporter("CUIModifyReservationPage", "vaildateHeaderDateinModificationHistory", Status.FAIL,
					"Date Header is not present in Modification History");
		}
	}
	public void checkDateInModificationHistory(){
		String formatModificationHistoryDate = null;
		if (getList(modificationHistoryList).size() > 0) {
			getElementInView(modificationHistoryList);
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList).size(); listmodificationiterator++) {
				String eachModificationDateList = getObject(By.xpath("//div[contains(@class,'holiday modification-history')]["+listmodificationiterator+"]//a[contains(@id,'accordion-label')]/p/span")).getText().trim();
				if (isValidDate(eachModificationDateList, "MM/dd/yy")) {
					formatModificationHistoryDate = "Y";
				}else{
					formatModificationHistoryDate = "N";
					break;
				}
		}
			if (formatModificationHistoryDate.equals("Y")) {
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkDateInModificationHistory", Status.PASS,
						"All Modification Dates are in required Format");
			}else{
				tcConfig.updateTestReporter("CUIModifyReservationPage", "checkDateInModificationHistory", Status.FAIL,
						"All Modification Dates are not in required Format");
			}
	}else{
		tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode", Status.FAIL,
				"No modification history");
	}
		
	}
	
	public void vaildateHeaderModificationTypeinModificationHistory(){
		if (verifyObjectDisplayed(headerModificationType)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "vaildateHeaderModificationTypeinModificationHistory", Status.PASS,
					"Modification Type Header is present in Modification History");
		}else{
			tcConfig.updateTestReporter("CUIModifyReservationPage", "vaildateHeaderModificationTypeinModificationHistory", Status.FAIL,
					"Modification Type Header is not present in Modification History");
		}
	}
	
	public void checkModificationTypeInModificationHistory(){
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList).size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(By.xpath("//div[contains(@class,'holiday modification-history')]["+listmodificationiterator+"]//a[contains(@id,'accordion-label')]/div"));
				if (verifyObjectDisplayed(eachModificationTypeList)) {
					String modificationType = getElementText(eachModificationTypeList);
					tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationTypeInModificationHistory", Status.PASS,
							"Modification Type is : "+modificationType);
				}else{
					tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationTypeInModificationHistory", Status.FAIL,
							"Modification Type not Present");
				}
			}	
	}else{
		tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode", Status.FAIL,
				"No modification history");
		}	
	}
	
	public void checkModificationDescription(int iterator){
		WebElement eachModificationdescription = getObject(By.xpath("//div[contains(@class,'holiday modification-history')]["+iterator+"]//div/a"));
		if (verifyObjectDisplayed(eachModificationdescription)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationDescription", Status.PASS,
					"Modification Description present as: "+getElementText(eachModificationdescription));
		}else{
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationDescription", Status.FAIL,
					"Modification Description not Present");
		}
	}
	
	public void checkSectionReservationSummary(){
		waitUntilElementVisibleBy(driver, sectionReservationSummary, "ExplicitLongWait");
		if (verifyObjectDisplayed(sectionReservationSummary)) {
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkSectionReservationSummary", Status.PASS,
					"Reservation section is present in the Modify reservation page");
		}else{
			tcConfig.updateTestReporter("CUIModifyReservationPage", "checkSectionReservationSummary", Status.PASS,
					"Reservation section is not present in the Modify reservation page");
		}
	}
	
	
	public void checkReservationSummaryDetails(){
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		elementPresenceVal(headerReservationDetails, "Header Reservation Details", "CUIModifyReservationPage",
				"checkReservationSummaryDetails");
		elementPresenceVal(sectionLocationContainer, "Location "+getElementText(sectionLocationContainer), "CUIModifyReservationPage",
				"checkReservationSummaryDetails");
		elementPresenceVal(sectionDateContainerCheckin, "Checkin Date "+getElementText(sectionDateContainerCheckin), "CUIModifyReservationPage",
				"checkReservationSummaryDetails");
		elementPresenceVal(sectionDateContainerCheckout, "Checkout Date "+getElementText(sectionDateContainerCheckout), "CUIModifyReservationPage",
				"checkReservationSummaryDetails");
		elementPresenceVal(sectionUnitTypeContainer, "Unit Type "+getElementText(sectionUnitTypeContainer), "CUIModifyReservationPage",
				"checkReservationSummaryDetails");
		elementPresenceVal(sectionTotalCostContainer, "Total Cost "+getElementText(sectionTotalCostContainer), "CUIModifyReservationPage",
				"checkReservationSummaryDetails");	
	}
	
	public void clickModifyTravelerButton(){
		Assert.assertTrue(verifyObjectDisplayed(buttonModifyTraveler));
		tcConfig.updateTestReporter("CUIModifyReservationPage", "clickModifyTravelerButton", Status.PASS,
				"Successfully Navigated to Modify Traveler page");
		clickElementJSWithWait(buttonModifyTraveler);
		
	}
	
	public void clickModifyVIPUpgradesButton(){
		Assert.assertTrue(verifyObjectDisplayed(btnModifyUpgradeSelection));
		tcConfig.updateTestReporter("CUIModifyReservationPage", "clickModifyUpgradeSelectionButton", Status.PASS,
				"Successfully Navigated to Modify Upgrade Selection page");
		clickElementJSWithWait(btnModifyUpgradeSelection);
		
	}
	
	
	
	public void checkOwnerModifiedAfterModification(){
		String modifyTraveler = null;
		if (getList(modificationHistoryList).size() > 0) {
			for (int listmodificationiterator = 1; listmodificationiterator <= getList(modificationHistoryList).size(); listmodificationiterator++) {
				WebElement eachModificationTypeList = getObject(By.xpath("//div[contains(@class,'holiday modification-history')]["+listmodificationiterator+"]//a[contains(@id,'accordion-label')]/div"));
				if (verifyObjectDisplayed(eachModificationTypeList)) {
					String modificationType = getElementText(eachModificationTypeList);
					if (modificationType.equalsIgnoreCase("Modify Traveler")) {
						modifyTraveler = "Y";
						break;
					}else{
						modifyTraveler = "N";
					}
					
				}else{
					tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationTypeInModificationHistory", Status.FAIL,
							"Modification Type not Present");
				}
			}	
	}else{
		tcConfig.updateTestReporter("CUIModifyReservationPage", "checkModificationHistoryCollapsedMode", Status.FAIL,
				"No modification history");
		}
		if (modifyTraveler == "Y") {
			tcConfig.updateTestReporter("CUICompleteModificationPage", "checkOwnerModifiedAfterModification", Status.PASS,
					"Modification history list updated");
		}else{
			tcConfig.updateTestReporter("CUICompleteModificationPage", "checkOwnerModifiedAfterModification", Status.FAIL,
					"Failed to Update modification History List");
		}
	}
	
	
}
