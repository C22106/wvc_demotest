package nextgen.pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenHelpPage_IOS extends NextGenHelpPage_Web {

	public static final Logger log = Logger.getLogger(NextGenHelpPage_IOS.class);

	public NextGenHelpPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		helpNavHeader = By.xpath(/* "//div[@class='grid-container ']//a[@title='Help']" */
				"//ul[contains(@class,'mobile-navigation__list')]//li/a[text()='Help']");
		scamBustersHdr = By.xpath("(//div[@class='title-1' and contains(text(),'SCAMBUSTERS')])[4]");

	}
	
	protected By footerWyndhamCare = By.xpath("//div[contains(@class,'footer')]//a[contains(text(),'Wyndham Cares')]");
	
	@Override
	public void helpPageNavigation() {

		waitUntilElementVisibleBy(driver, helpNavHeader, 120);

		if (verifyObjectDisplayed(helpNavHeader)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
					"Help tab present in main Nav");
			String href = getElementAttribute(helpNavHeader, "href").trim();
			driver.findElement(helpNavHeader).click();

			pageCheck();

			if (getCurrentURL().trim().equals(href)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
						"Help page Navigated, Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
						"Help page nav failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
					"Help Tab not present in main Nav");
		}

	}

	@Override
	public void wyndhamCaresNav() {

		waitUntilElementVisibleBy(driver, wyndhamCaresLink, "120");
		scrollDownForElementJSBy(wyndhamCaresLink);
		if (verifyObjectDisplayed(wyndhamCaresLink)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.PASS,
					"Wyndham cares Link Present");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			String href = getElementAttribute(wyndhamCaresLink, "href");
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
				clickElementBy(footerWyndhamCare);
			}else{
				clickElementJSWithWait(wyndhamCaresLink);
			}
			
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (href.equals(getCurrentURL().trim())) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.PASS,
						"Navigated to Wyndham Cares Link page");

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.FAIL,
						"Navigation to Wyndham Cares link failed");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.FAIL,
					"Wyndham cares Link not Present");

		}

	}
	
	@Override
	public void validateFormError() {
		clickElementJSWithWait(memberInputfield);
		fieldDataEnter(memberInputfield, testData.get("memberNo") + Keys.ENTER);
		assertTrue(verifyObjectDisplayed(memberInputError), "Error Message for Wrong Member ID is not displayed");
		tcConfig.updateTestReporter("PublicationPage", "validateFormError", Status.PASS,
				"Error Message for Wrong Member ID is displayed");
		driver.navigate().refresh();

	}

	@Override
	public void wynCaresPageFormVal() {

		waitUntilElementVisibleBy(driver, needHelpFormHdr, 120);
		scrollDownForElementJSBy(needHelpFormHdr);
		if (verifyObjectDisplayed(needHelpFormHdr)) {

			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
					"Form Title present with header " + getElementText(needHelpFormHdr));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(wynFirstNameField) || verifyObjectDisplayed(FirstNameField2)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"First Name Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name *", testData.get("firstName"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(wynLastNameField) || verifyObjectDisplayed(LastNameField2)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Last Name Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name *", testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Phone Number *", testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address *", testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(memberNo)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wynCaresPageFormVal", Status.PASS,
							"Member Number Field present");
					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Member Number", testData.get("memberNo"));
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wynCaresPageFormVal", Status.FAIL,
							"Member Number Field not present");
				}

				try {
					List<WebElement> checkBoxList = driver.findElements(checkBox);
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Total " + checkBoxList.size() + " terms and condition chechkbox present in the page");

					for (int i = 0; i < checkBoxList.size(); i++) {
						clickElementJSWithWait(checkBoxList.get(i));
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"T&C checkbox error");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(submitButton1)) {
					scrollDownForElementJSBy(submitButton1);
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButton1);

				} else if (verifyObjectDisplayed(submitButton)) {
					scrollDownForElementJSBy(submitButton);
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButton);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitUntilElementVisibleBy(driver, thankConfirm, 240);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Form Submission Successful");

					/*
					 * if (verifyObjectDisplayed(closeMsg)) {
					 * clickElementBy(closeMsg); } else {
					 * tcConfig.updateTestReporter("NextGenHelpPage",
					 * "incorrectFormVal", Status.FAIL, "Close Btn not present"
					 * ); }
					 */

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	@Override
	public void navigateToScambuster() {
		String appURL = testData.get("URL");

		appURL = appURL.concat("/us/en/help/wyndham-cares/scambusters");
		driver.navigate().to(appURL);
		pageCheck();

		/*waitUntilElementVisibleBy(driver, scamBustersHdr, 120);

		if (verifyObjectDisplayed(scamBustersHdr)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToScambuster", Status.PASS,
					"Scambusters page Navigated, Header present");

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToScambuster", Status.FAIL,
					"Scambusters Page navigation failed");

		}*/

	}

	@Override
	public void navigatetoPublicationPage() {
		waitUntilElementVisibleBy(driver, publicationLink, "120");
		scrollDownForElementJSBy(publicationLink);
		if (verifyObjectDisplayed(publicationLink)) {
			tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.PASS,
					"Publications Link Present");
			String href = getElementAttribute(publicationLink, "href");
			clickElementBy(publicationLink);
			pageCheck();
			if (getCurrentURL().trim().equals(href)) {
				tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.PASS,
						"Navigated to Publications Link page");
			} else {
				tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.FAIL,
						"Not Navigated to Publications Link page");
			}
		} else {
			tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.FAIL,
					"Publications Link not Present");
		}
	}

	@Override
	public void fillUpOrderDirectoryForm() {
		if (verifyObjectDisplayed(formHeader)) {

			tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
					"Order Directory form is Present");

			scrollDownForElementJSBy(Address1Inputfield);

			if (verifyObjectDisplayed(Address1Inputfield)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Address 1 Field Present");

				sendKeysByScriptExecutor((RemoteWebDriver) driver, "Address 1 *", testData.get("Address"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Address 1 Field not present");
			}

			if (verifyObjectDisplayed(Address2Inputfield)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Address 2 Field Present");

				
				sendKeysByScriptExecutor((RemoteWebDriver) driver, "Address 2", testData.get("Address"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Address 2 Field not present");
			}

			if (verifyObjectDisplayed(cityInputfield)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"City Field Present");

				sendKeysByScriptExecutor((RemoteWebDriver) driver, "City *", testData.get("City"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"City Field not present");
			}

			if (verifyObjectDisplayed(stateDropDown)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"State Drop Down present");

				new Select(driver.findElement(stateDropDown)).selectByVisibleText(testData.get("stateSel"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"State Drop Down not present");
			}

			if (verifyObjectDisplayed(zipCodeInput)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Zip Field Present");

				fieldDataEnter(zipCodeInput, testData.get("ZipCode"));
				sendKeysByScriptExecutor((RemoteWebDriver) driver, "Zip Code *", testData.get("ZipCode"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Zip Field not present");
			}

			if (getElementAttribute(submitBtn, "class").contains("disabled")) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Submit Button Disabled before selecting the Confirm Checkbox");

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Agree Button Enabled before selecting the Confirm Checkbox");
			}

			scrollDownForElementJSBy(leadIDCheckbox);
			if (verifyObjectDisplayed(leadIDCheckbox)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Confirm Checkbox is Present");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(leadIDCheckbox);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().activeElement().sendKeys(Keys.TAB);

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Confirm Checkbox is not Present");
			}
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyObjectDisplayed(submitBtn)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Agree Button Enabled After selecting the Confirm Checkbox");
				clickElementJSWithWait(submitBtn);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				waitUntilElementVisibleBy(driver, successMessage, 240);

				if (verifyObjectDisplayed(successMessage) && verifyObjectDisplayed(successSubDescription)) {
					tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
							"Form Submission Successful");

				} else {
					tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Agree Button disabled after selecting the Confirm Checkbox");
			}

		}
	}

}
