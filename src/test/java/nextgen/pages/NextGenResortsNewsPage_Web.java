package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NextGenResortsNewsPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenResortsNewsPage_Web.class);

	public NextGenResortsNewsPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By resortHeader = By.xpath(/* "//div[@class='grid-container ']//a[@title='Resorts']" */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Resorts']");
	protected By resortNewsNav = By.xpath(/* "//a[@data-eventname='header']//span[contains(.,'Resort News')]" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'Resort News')]");
	protected By resortNewsHdr = By.xpath(/* "//h2[contains(.,'Resort News')]" */
			"(//div[@class='title-1' and contains(text(),'RESORT NEWS')])[1]");
	protected By resortNewsDesc = By.xpath(/* "//h2[contains(.,'Resort News')]/following-sibling::h3" */
			"(//div[@class='title-1' and contains(text(),'RESORT NEWS')])[1]//..//div[@class='body-1']/p");
	protected By resortNewsImage = By.xpath(/*
											 * "//h2[contains(.,'Resort News')]/ancestor::div[@class='wyn-hero wyn-hero--large']//img"
											 */
			"(//div[@class='title-1' and contains(text(),'RESORT NEWS')])[1]/ancestor::div[@class='banner']//img");
	protected By resortNewsBrdCrm = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Resort News')]");
	protected By rebuildinghdr = By.xpath(/* "//h2[contains(.,'Let the Rebuilding')]" */
			"(//div[@class='align-center cell large-5 small-12 wow animate-fade-in-card text']/div)[1]");
	protected By rebuildingHero = By.xpath(/*
											 * "//h2[contains(.,'Let the Rebuilding')]/ancestor::div[@class='wyn-hero wyn-hero--large']"
											 */
			"//div[@class='text-white grid-container ']");
	protected By rebuildingheroImg = By.xpath(/*
												 * "//h2[contains(.,'Let the Rebuilding')]/ancestor::div[@class='wyn-hero wyn-hero--large']//img"
												 */
			"//div[@class='image wow animate-fade-in-image']");
	protected By rebuildingDesc = By.xpath(/*
											 * "//h2[contains(.,'Let the Rebuilding')]/ancestor::div[@class='wyn-hero wyn-hero--large']//p"
											 */
			"(//div[@class='align-center cell large-5 small-12 wow animate-fade-in-card text']/div)[2]");
	protected By rebuildingCTA = By.xpath(/*
											 * "//h2[contains(.,'Let the Rebuilding')]/ancestor::div[@class='wyn-hero wyn-hero--large']//a"
											 */
			"(//div[@class='align-center cell large-5 small-12 wow animate-fade-in-card text']/div)[3]");
	protected By resortNewsCardsImg = By.xpath(/* "//div[@gtm_component='cards']//img" */
			/*
			 * "//div[contains(@class,'slick-active')]//div/a[@data-eventname='Card']/img");
			 */
			"//div[contains(@class,'slick-slide slick-active')]//div[@class='card align-center-middle ']/img");
	protected By resortNewsCardsTitl = By.xpath(/* "//div[@gtm_component='cards']//div[@class='wyn-title']" */
			"//div[contains(@class,'slick-active')]//div[@class='subtitle-2 bold']");
	protected By resortNewsCardsDesc = By.xpath(/* "//div[@gtm_component='cards']//p" */
			"//div[contains(@class,'slick-active')]//div[@class='body-1']/p");
	protected By resortNewsCardsCTA = By.xpath(/* "//div[@gtm_component='cards']//div[@class='wyn-button-cta']" */
			"//div[contains(@class,'slick-active')]//div[@class='link-caret-dynamic body-1-link']");
	protected By wynHdrtitle = By.xpath("//div[@class='wyn-card__title']");

	//
	// hidden Treasures validations
	protected By renovationCard = By.xpath("//h3[contains(.,'Renovation')]/ancestor::a//div[@class='wyn-button-cta']");
	protected By renovationImg = By.xpath("//div[@class='wyn-hero__image']//img");
	// protected By renovationContentHeader =
	// By.xpath("//div[@class='text']//h3");
	protected By renovationHdr = By.xpath("//h1[contains(.,'Renovation')]");
	protected By renovationTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By renovationFacebook = By.xpath(
			"//h1[contains(.,'Renovation')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By renovationTwitter = By.xpath(
			"//h1[contains(.,'Renovation')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By renovationNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Renovation')]");
	protected By renovationContent = By.xpath("//div[@class='text']");
	protected By videoIframe = By.xpath("//div[@gtm_component='text']//iframe");
	protected By renovationVideo = By.xpath("//div[@id='player']");
	protected By renovationVideoPlay = By.xpath("//button[@aria-label='Play']");
	protected By renovationVideoPause = By.xpath("//button[@aria-label='Pause (k)']");

	// Best In Hawaii validations
	protected By bestInHawaiiCard = By
			.xpath("//h3[contains(.,'Best in Hawaii')]/ancestor::a//div[@class='wyn-button-cta']");
	protected By bestInHawaiiImg = By.xpath("//div[@class='wyn-hero__image']//img");

	protected By bestInHawaiiHdr = By.xpath("//h1[contains(.,'Best in Hawaii')]");
	protected By bestInHawaiiTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By bestInHawaiiFacebook = By.xpath(
			"//h1[contains(.,'Best in Hawaii')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By bestInHawaiiTwitter = By.xpath(
			"//h1[contains(.,'Best in Hawaii')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By bestInHawaiiNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Best in Hawaii')]");
	protected By bestInHawaiiContent = By.xpath("//div[@class='text']//h3");

	// Bluebeards Rebuilding
	protected By bluebeardsCard = By
			.xpath("//h2[contains(.,'Let the Rebuilding Begin')]/ancestor::div[@class='wyn-card__content']//a");
	protected By bluebeardsImg = By.xpath("//div[@class='wyn-hero__image']//img");

	protected By bluebeardsHdr = By.xpath("//h1[contains(.,'Rebuilding Begin')]");
	protected By bluebeardsTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By bluebeardsFacebook = By.xpath(
			"//h1[contains(.,'Rebuilding Begin')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By bluebeardsTwitter = By.xpath(
			"//h1[contains(.,'Rebuilding Begin')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By bluebeardsNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Rebuilding')]");
	protected By bluebeardsContent = By.xpath("//div[@class='text']");

	// Kingstown Reef Expansion
	protected By setYourSightsCard = By
			.xpath("//h3[contains(.,'Set Your Sights')]/ancestor::a//div[@class='wyn-button-cta']");
	protected By setYourSightsImg = By.xpath("//div[@class='wyn-hero__image']//img");

	protected By setYourSightsHdr = By.xpath("//h1[contains(.,'Set Your Sights')]");
	protected By setYourSightsTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By setYourSightsFacebook = By.xpath(
			"//h1[contains(.,'Set Your Sights')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By setYourSightsTwitter = By.xpath(
			"//h1[contains(.,'Set Your Sights')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By setYourSightsNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Kingstown')]");
	protected By setYourSightsContent = By.xpath("//div[@class='text']//h3");
	protected By setYourSightsKingstownLink = By.xpath("//div[@class='text']//a[contains(.,'Kingstown')]");

	// Website Welcome
	protected By startofSmthngGrtCard = By
			.xpath("//h3[contains(.,'Start of Something')]/ancestor::a//div[@class='wyn-button-cta']");
	protected By startofSmthngGrtImg = By.xpath("//div[@class='wyn-hero__image']//img");

	protected By startofSmthngGrtHdr = By.xpath("//h1[contains(.,'Start of Something')]");
	protected By startofSmthngGrtTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By startofSmthngGrtFacebook = By.xpath(
			"//h1[contains(.,'Start of Something')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By startofSmthngGrtTwitter = By.xpath(
			"//h1[contains(.,'Start of Something')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By startofSmthngGrtNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Website Welcome')]");
	protected By startofSmthngGrtContent = By.xpath("//div[@role='main']//h2");

	// DreamsDiveExplore
	protected By dreamDiveExCard = By
			.xpath("//h3[contains(.,'Better Than a Vacation')]/ancestor::a//div[@class='wyn-button-cta']");
	protected By dreamDiveExImg = By.xpath("//div[@class='wyn-hero__image']//img");

	protected By dreamDiveExHdr = By.xpath("//h1[contains(.,'Dream Bigger')]");
	protected By dreamDiveExTagLine = By.xpath("//p[@class='wyn-headline__tagline']");
	protected By dreamDiveExFacebook = By.xpath(
			"//h1[contains(.,'Dream Bigger')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By dreamDiveExTwitter = By.xpath(
			"//h1[contains(.,'Dream Bigger')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By dreamDiveExNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Dream Bigger')]");
	protected By dreamDiveExContent = By.xpath("//div[@class='content']");
	protected By dreamDiveExThankMsg = By.xpath("//div[@class='content']//p[contains(.,'Thank you')]");

	/*
	 * Method: resortNewsPageNav Description: resort News page navigation Date:
	 * Aug/2019 Author: Unnat Jain Changes By
	 */
	public void resortNewsPageNav() {

		waitUntilElementVisibleBy(driver, resortHeader, 120);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(resortHeader)).build().perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(resortNewsNav)) {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "resortNewsPageNav", Status.PASS,
					"Resort News present in Sub Nav");
			driver.findElement(resortNewsNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "resortNewsPageNav", Status.FAIL,
					"resort news not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, resortNewsHdr, 120);

		if (verifyObjectDisplayed(resortNewsHdr)) {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "resortNewsPageNav", Status.PASS,
					"Resort News Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "resortNewsPageNav", Status.FAIL,
					"Resort news Page Navigation failed");
		}

	}

	/*
	 * Method: resortNewsPageVal Description: resort News Validations: Date Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void resortNewsPageVal() {
		if (verifyObjectDisplayed(resortNewsHdr)) {

			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News header present as " + getElementText(resortNewsHdr));

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News header not present");
		}

		if (verifyObjectDisplayed(resortNewsImage)) {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News  page hero Image not present");
		}

		if (verifyObjectDisplayed(resortNewsBrdCrm)) {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News Breadcrumb not present");
		}

		if (verifyObjectDisplayed(resortNewsDesc)) {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News Desc present");

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News Desc not present");
		}

		getElementInView(rebuildingHero);

		if (verifyObjectDisplayed(rebuildingheroImg)) {

			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News Rebuilding Section  present with the image");

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News Rebuilding Section not present with the image");
		}

		if (verifyObjectDisplayed(rebuildinghdr)) {

			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News Rebuilding Section  Header  present ");

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News Rebuilding Section  Header not present ");
		}

		if (verifyObjectDisplayed(rebuildingDesc)) {

			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News Rebuilding Section  Desc  present ");

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News Rebuilding Section  Desc not present ");
		}

		if (verifyObjectDisplayed(rebuildingCTA)) {

			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
					"Resort News Rebuilding Section  CTA  present ");
			// clickElementBy(rebuildingCTA);
			// waitUntilElementVisibleBy(driver, wynHdrtitle, 120);

			if (driver.getCurrentUrl().toUpperCase().contains(testData.get("header1").toUpperCase())) {

				tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.PASS,
						"Navigated to rebuilding page");

			} else {
				tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
						"Navigated to rebuilding page failed");
			}

			// driver.navigate().back();
			waitUntilElementVisibleBy(driver, resortNewsHdr, 120);

		} else {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "newOwnerPageVal", Status.FAIL,
					"Resort News Rebuilding Section  CTA not present ");
		}

	}

	/*
	 * Method: resortNewsCards Description: resort News Cards Validations: Date
	 * Aug/2019 Author: Unnat Jain Changes By
	 */

	public void resortNewsCards() {

		getElementInView(resortNewsCardsTitl);

		try {
			List<WebElement> cardsImageList = driver.findElements(resortNewsCardsImg);
			List<WebElement> cardsHdrsList = driver.findElements(resortNewsCardsTitl);
			List<WebElement> cardsDescList = driver.findElements(resortNewsCardsDesc);
			// List<WebElement> cardsLearnMoreList =
			// driver.findElements(rciLearnMore);

			tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.PASS,
					"Resort News Cards present Total: " + cardsHdrsList.size());

			for (int i = 0; i < cardsHdrsList.size() - 1; i++) {
				getElementInView(cardsHdrsList.get(i));
				tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.PASS,
						"Card No " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

				if (verifyElementDisplayed(cardsImageList.get(i))) {
					tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.PASS,
							"Image present in " + (i + 1) + " Card");
				} else {
					tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.FAIL,
							"Image not present in " + (i + 1) + " cards");
				}

				if (verifyElementDisplayed(cardsDescList.get(i))) {
					tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.PASS,
							"Description present in " + (i + 1) + " cards");
				} else {
					tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.FAIL,
							"Description not present in " + (i + 1) + " cards");
				}

			}
			List<WebElement> cardsLearnMoreList = driver.findElements(resortNewsCardsCTA);
			for (int j = 0; j < cardsLearnMoreList.size(); j++) {
				List<WebElement> cardsLearnMoreList1 = driver.findElements(resortNewsCardsCTA);
				if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {
					getElementInView(cardsLearnMoreList1.get(j));
					cardsLearnMoreList1.get(j).click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (driver.getCurrentUrl().toUpperCase()
							.contains(testData.get("subHeader" + (j + 1) + "").toUpperCase())) {

						tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.PASS,
								"Navigated to " + (j + 1) + " page");

					} else {
						tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.FAIL,
								"Navigated to " + (j + 1) + " page failed");
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, resortNewsHdr, 120);

				} else {
					tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.FAIL,
							" CTA link not present in the cards for " + (j + 1) + " block");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextgenResortnewsPage", "resortNewsCards", Status.FAIL,
					"Resort News Cards are not present in the page");
		}

	}

	/*
	 * Method: renovationNav Description: Renovations Validations: Date Sep/2019
	 * Author: Unnat Jain Changes By
	 */

	public void renovationsNav() {

		try {
			String strTem = getElementText(renovationCard);
			getElementInView(renovationCard);
			waitUntilElementVisibleBy(driver, renovationCard, 120);

			if (verifyObjectDisplayed(renovationCard)) {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "renovationsnav", Status.PASS,
						"Renovation Cards present in the page");
				clickElementBy(renovationCard);

				waitUntilElementVisibleBy(driver, renovationHdr, 120);

				if (verifyObjectDisplayed(renovationHdr)) {
					tcConfig.updateTestReporter("NextGenResortsNewsPage", "renovationsnav", Status.PASS,
							"Renovation page Navigated");

				} else {
					tcConfig.updateTestReporter("NextGenResortsNewsPage", "renovationsnav", Status.FAIL,
							"Renovation page Navigation failed");
				}

			} else {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "renovationsnav", Status.FAIL,
						"Renovation Cards not present in the page");
			}

		} catch (Exception e) {
			String envURL = testData.get("URL").trim();
			String renoURL = envURL.concat("/us/en/resorts/resort-news/destination-renovation");

			driver.navigate().to(renoURL);

			waitUntilElementVisibleBy(driver, renovationHdr, 120);

			if (verifyObjectDisplayed(renovationHdr)) {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "renovationsnav", Status.PASS,
						"Renovation page Navigated");

			} else {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "renovationsnav", Status.FAIL,
						"Renovation page Navigation failed");
			}
		}

	}

	/*
	 * Method: hiddenTreasuresVal Description: Renovation Validations: Date Sep/2019
	 * Author: Unnat Jain Changes By
	 */

	public void renovationsVal() {

		waitUntilElementVisibleBy(driver, renovationHdr, 120);

		if (verifyObjectDisplayed(renovationHdr)) {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
					getElementText(renovationHdr) + " header present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
					"Renovation Header not present");
		}

		if (verifyObjectDisplayed(renovationImg)) {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
					"Hiden Treasure Image present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
					"Renovation image not present");
		}

		if (verifyObjectDisplayed(renovationTagLine)) {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
					getElementText(renovationTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
					"Renovation Header tag line not present");
		}

		if (verifyObjectDisplayed(renovationNav)) {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
					"Renovation Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(renovationFacebook)) {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
					"Renovation facebook icon present");

			clickElementBy(renovationFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
					"Renovation  facebook icon not present");

		}

		if (verifyObjectDisplayed(renovationTwitter)) {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
					"Renovation twitter icon present");

			clickElementBy(renovationTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
					"Renovation on the bookstwitter icon not present");

		}

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(renovationContent)) {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
					"Renovation content present on the page");

			try {

				WebElement eleVideo = driver.findElement(videoIframe);
				driver.switchTo().frame(eleVideo);

				getElementInView(renovationVideo);
				if (verifyObjectDisplayed(renovationVideo)) {
					tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
							"Renovation Video present on the page");

					if (verifyObjectDisplayed(renovationVideoPlay)) {
						tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.PASS,
								"Renovation Video Play Button present on the page");

						/*
						 * clickElementBy(renovationVideoPlay);
						 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
						 * (verifyObjectDisplayed(renovationVideoPause)) {
						 * tcConfig.updateTestReporter("NextGenResortNews", "renovationVal",
						 * Status.PASS, "Renovation Video Pause buttton present on the page");
						 * clickElementBy(renovationVideoPause);
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 * 
						 * } else { tcConfig.updateTestReporter("NextGenResortNews", "renovationVal",
						 * Status.FAIL, "Renovation Video Pause not present on the page"); }
						 */

					} else {
						tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
								"Renovation Video Play not present on the page");
					}

				} else {
					tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
							"Renovation Video not present on the page");
				}

				driver.switchTo().defaultContent();

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
						"Renovation Video not present on the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "renovationVal", Status.FAIL,
					"Renovation content not present on the page");
		}

	}

	/*
	 * Method: bestInHawaiNav Description: Best In Hawaii Validations: Date Sep/2019
	 * Author: Unnat Jain Changes By
	 */

	public void bestInHawaiiNav() {

		// getElementInView(bestInHawaiiCard);
		// waitUntilElementVisibleBy(driver, bestInHawaiiCard, 120);
		//
		// if (verifyObjectDisplayed(bestInHawaiiCard)) {
		// tcConfig.updateTestReporter("NextGenResortsNewsPage",
		// "bestInHawaiisnav", Status.PASS,
		// "Best in hawaii Cards present in the page");
		// clickElementBy(bestInHawaiiCard);
		//
		// waitUntilElementVisibleBy(driver, bestInHawaiiHdr, 120);
		//
		// if (verifyObjectDisplayed(bestInHawaiiHdr)) {
		// tcConfig.updateTestReporter("NextGenResortsNewsPage",
		// "bestInHawaiisnav", Status.PASS,
		// "Best in hawaii page Navigated");
		//
		// } else {
		// tcConfig.updateTestReporter("NextGenResortsNewsPage",
		// "bestInHawaiisnav", Status.FAIL,
		// "Best in hawaii page Navigation failed");
		// }
		//
		// } else {
		// tcConfig.updateTestReporter("NextGenResortsNewsPage",
		// "bestInHawaiisnav", Status.FAIL,
		// "Best in hawaii Cards not present in the page");
		// }

		String urlCheck = testData.get("URL");

		urlCheck = urlCheck.concat("/resorts/resort-news/best-in-hawaii-resorts");

		driver.navigate().to(urlCheck);

	}

	/*
	 * Method: bestInHawaiVal Description: Best In Hawaii Validations: Date Sep/2019
	 * Author: Unnat Jain Changes By
	 */

	public void bestInHawaiiVal() {

		waitUntilElementVisibleBy(driver, bestInHawaiiHdr, 120);

		if (verifyObjectDisplayed(bestInHawaiiHdr)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
					getElementText(bestInHawaiiHdr) + " header present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
					"Best In Hawaii Header not present");
		}

		if (verifyObjectDisplayed(bestInHawaiiImg)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
					"Hiden Treasure Image present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
					"Best In Hawaii image not present");
		}

		if (verifyObjectDisplayed(bestInHawaiiTagLine)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
					getElementText(bestInHawaiiTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
					"Best In Hawaii Header tag line not present");
		}

		if (verifyObjectDisplayed(bestInHawaiiNav)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
					"Best In Hawaii Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(bestInHawaiiFacebook)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
					"Best In Hawaii facebook icon present");

			clickElementBy(bestInHawaiiFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
					"Best In Hawaii  facebook icon not present");

		}

		if (verifyObjectDisplayed(bestInHawaiiTwitter)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
					"Best In Hawaii twitter icon present");

			clickElementBy(bestInHawaiiTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
					"Best In Hawaii on the bookstwitter icon not present");

		}

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(bestInHawaiiContent)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.PASS,
					"Best In Hawaii content present on the page with header " + getElementText(bestInHawaiiContent));

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bestInHawaiiVal", Status.FAIL,
					"Best In Hawaii content not present on the page");
		}

	}

	/*
	 * Method: kingsTownNav Description:Kingstown reef Validations: Date Sep/2019
	 * Author: Unnat Jain Changes By
	 */

	public void kingsTownReefNav() {

		getElementInView(setYourSightsCard);
		waitUntilElementVisibleBy(driver, setYourSightsCard, 120);

		if (verifyObjectDisplayed(setYourSightsCard)) {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "setYourSightssnav", Status.PASS,
					"Set You Sights Cards present in the page");
			clickElementBy(setYourSightsCard);

			waitUntilElementVisibleBy(driver, setYourSightsHdr, 120);

			if (verifyObjectDisplayed(setYourSightsHdr)) {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "setYourSightssnav", Status.PASS,
						"Set You Sights page Navigated");

			} else {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "setYourSightssnav", Status.FAIL,
						"Set You Sight spage Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "setYourSightssnav", Status.FAIL,
					"Set You Sights Cards not present in the page");
		}

	}

	/*
	 * Method: kingsTownReefVal Description: Kingstown Reef Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void kingsTownReefVal() {

		waitUntilElementVisibleBy(driver, setYourSightsHdr, 120);

		if (verifyObjectDisplayed(setYourSightsHdr)) {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
					getElementText(setYourSightsHdr) + " header present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
					"SetYourSights  Header not present");
		}

		if (verifyObjectDisplayed(setYourSightsImg)) {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
					"SetYourSights Image present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
					"SetYourSights  image not present");
		}

		if (verifyObjectDisplayed(setYourSightsTagLine)) {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
					getElementText(setYourSightsTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
					"SetYourSights  Header tag line not present");
		}

		if (verifyObjectDisplayed(setYourSightsNav)) {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
					"SetYourSights  Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(setYourSightsFacebook)) {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
					"SetYourSights  facebook icon present");

			clickElementBy(setYourSightsFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
					"setYourSights   facebook icon not present");

		}

		if (verifyObjectDisplayed(setYourSightsTwitter)) {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
					"setYourSights  twitter icon present");

			clickElementBy(setYourSightsTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
					"SetYourSights  on the bookstwitter icon not present");

		}

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(setYourSightsContent)) {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
					"SetYourSights  content present on the page with header " + getElementText(setYourSightsContent));

			if (verifyObjectDisplayed(setYourSightsKingstownLink)) {
				tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.PASS,
						"SetYourSights  Kingstown link present on the page");

			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
						"SetYourSights  Kingstown link not present on the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "kingsTownReefVal", Status.FAIL,
					"SetYourSights  content not present on the page");
		}

	}

	/*
	 * Method: bluebeardsNav Description:Bluebeards Rebuilding Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void bluebeardsNav() {

		getElementInView(bluebeardsCard);
		waitUntilElementVisibleBy(driver, bluebeardsCard, 120);

		if (verifyObjectDisplayed(bluebeardsCard)) {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "bluebeardssnav", Status.PASS,
					"Bluebeards Rebuilding Cards present in the page");
			clickElementBy(bluebeardsCard);

			waitUntilElementVisibleBy(driver, bluebeardsHdr, 120);

			if (verifyObjectDisplayed(bluebeardsHdr)) {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "bluebeardssnav", Status.PASS,
						"Bluebeards Rebuilding page Navigated");

			} else {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "bluebeardssnav", Status.FAIL,
						"Bluebeards Rebuilding page Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "bluebeardssnav", Status.FAIL,
					"Bluebeards Rebuilding Cards not present in the page");
		}

	}

	/*
	 * Method: bestInHawaiVal Description: Bluebeards Rebuilding Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void bluebeardsVal() {

		waitUntilElementVisibleBy(driver, bluebeardsHdr, 120);

		if (verifyObjectDisplayed(bluebeardsHdr)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
					getElementText(bluebeardsHdr) + " header present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
					"Bluebeards Rebuilding Header not present");
		}

		if (verifyObjectDisplayed(bluebeardsImg)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS, "Bluebeards Image present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
					"Bluebeards Rebuilding image not present");
		}

		if (verifyObjectDisplayed(bluebeardsTagLine)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
					getElementText(bluebeardsTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
					"Bluebeards Rebuilding Header tag line not present");
		}

		if (verifyObjectDisplayed(bluebeardsNav)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
					"Bluebeards Rebuilding Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(bluebeardsFacebook)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
					"Bluebeards Rebuilding facebook icon present");

			clickElementBy(bluebeardsFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
					"Bluebeards Rebuilding  facebook icon not present");

		}

		if (verifyObjectDisplayed(bluebeardsTwitter)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
					"Bluebeards Rebuilding twitter icon present");

			clickElementBy(bluebeardsTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
					"Bluebeards Rebuilding on the bookstwitter icon not present");

		}

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(bluebeardsContent)) {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.PASS,
					"Bluebeards Rebuilding content present on the page with header ");

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "bluebeardsVal", Status.FAIL,
					"Bluebeards Rebuilding content not present on the page");
		}

	}

	/*
	 * Method: websiteWelcomeNav Description:Website Welcome Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void websiteWelcomeNav() {

		getElementInView(startofSmthngGrtCard);
		waitUntilElementVisibleBy(driver, startofSmthngGrtCard, 120);

		if (verifyObjectDisplayed(startofSmthngGrtCard)) {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "startOfSmthngGrtsnav", Status.PASS,
					"Website Welcome Cards present in the page");
			clickElementBy(startofSmthngGrtCard);

			waitUntilElementVisibleBy(driver, startofSmthngGrtHdr, 120);

			if (verifyObjectDisplayed(startofSmthngGrtHdr)) {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "startOfSmthngGrtsnav", Status.PASS,
						"Website Welcome page Navigated");

			} else {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "startOfSmthngGrtsnav", Status.FAIL,
						"Website Welcome page Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "startOfSmthngGrtsnav", Status.FAIL,
					"Website Welcome Cards not present in the page");
		}

	}

	/*
	 * Method: websiteWelcomeVal Description:Website Welcome Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void websiteWelcomeVal() {

		waitUntilElementVisibleBy(driver, startofSmthngGrtHdr, 120);

		if (verifyObjectDisplayed(startofSmthngGrtHdr)) {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
					getElementText(startofSmthngGrtHdr) + " header present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
					"Start of Something Great  Header not present");
		}

		if (verifyObjectDisplayed(startofSmthngGrtImg)) {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
					"Start of Something Great Image present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
					"Start of Something Great  image not present");
		}

		if (verifyObjectDisplayed(startofSmthngGrtTagLine)) {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
					getElementText(startofSmthngGrtTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
					"Start of Something Great  Header tag line not present");
		}

		if (verifyObjectDisplayed(startofSmthngGrtNav)) {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
					"Start of Something Great  Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(startofSmthngGrtFacebook)) {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
					"Start of Something Great  facebook icon present");

			clickElementBy(startofSmthngGrtFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
					"Start of Something Great   facebook icon not present");

		}

		if (verifyObjectDisplayed(startofSmthngGrtTwitter)) {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
					"Start of Something Great  twitter icon present");

			clickElementBy(startofSmthngGrtTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
					"Start of Something Great  on the bookstwitter icon not present");

		}

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(startofSmthngGrtContent)) {
			List<WebElement> headersList = driver.findElements(startofSmthngGrtContent);

			if (headersList.size() == 3) {
				tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
						"Total Header present on page: " + headersList.size());
				for (int i = 0; i < headersList.size(); i++) {

					tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
							(i + 1) + " Header is " + headersList.get(i).getText());

				}

			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.PASS,
						headersList.size() + " headres present on the page instead of 3");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "websiteWelcomeVal", Status.FAIL,
					"Start of Something Great  content not present on the page");
		}

	}

	/*
	 * Method: dreamDiveExploreNav Description:Dream Dive Explore Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void dreamDiveExploreNav() {

		getElementInView(dreamDiveExCard);
		waitUntilElementVisibleBy(driver, dreamDiveExCard, 120);

		if (verifyObjectDisplayed(dreamDiveExCard)) {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "dreamDiveExsnav", Status.PASS,
					"Dream Dive and Explore Cards present in the page");
			clickElementBy(dreamDiveExCard);

			waitUntilElementVisibleBy(driver, dreamDiveExHdr, 120);

			if (verifyObjectDisplayed(dreamDiveExHdr)) {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "dreamDiveExsnav", Status.PASS,
						"Dream Dive and Explore page Navigated");

			} else {
				tcConfig.updateTestReporter("NextGenResortsNewsPage", "dreamDiveExsnav", Status.FAIL,
						"Dream Dive and Explore page Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenResortsNewsPage", "dreamDiveExsnav", Status.FAIL,
					"Dream Dive and Explore Cards not present in the page");
		}

	}

	/*
	 * Method: dreamDiveExploreVal Description:Dream Dive Explore Validations: Date
	 * Sep/2019 Author: Unnat Jain Changes By
	 */

	public void dreamDiveExploreVal() {

		waitUntilElementVisibleBy(driver, dreamDiveExHdr, 120);

		if (verifyObjectDisplayed(dreamDiveExHdr)) {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
					getElementText(dreamDiveExHdr) + " header present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
					"Dream Dive Explore  Header not present");
		}

		if (verifyObjectDisplayed(dreamDiveExImg)) {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
					"Dream Dive Explore Image present");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
					"Dream Dive Explore  image not present");
		}

		if (verifyObjectDisplayed(dreamDiveExTagLine)) {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
					getElementText(dreamDiveExTagLine) + " present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
					"Dream Dive Explore  Header tag line not present");
		}

		if (verifyObjectDisplayed(dreamDiveExNav)) {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
					"Breadcrumb present in the page ");
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
					"Dream Dive Explore  Header breadcrumb not present");
		}

		if (verifyObjectDisplayed(dreamDiveExFacebook)) {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
					"Dream Dive Explore  facebook icon present");

			clickElementBy(dreamDiveExFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
					"Dream Dive Explore   facebook icon not present");

		}

		if (verifyObjectDisplayed(dreamDiveExTwitter)) {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
					"Dream Dive Explore  twitter icon present");

			clickElementBy(dreamDiveExTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
					"Dream Dive Explore  on the bookstwitter icon not present");

		}

		// List<WebElement> questionsList =
		// driver.findElements(getYourVacayQuestions);

		if (verifyObjectDisplayed(dreamDiveExContent)) {

			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
					"Dream Dive Explore  content present on the page");

			if (verifyObjectDisplayed(dreamDiveExThankMsg)) {

				tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.PASS,
						"Dream Dive Explore  Thank You Msg present on the page");

			} else {
				tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
						"Dream Dive Explore  Thank You Msg not present on the page");
			}
		} else {
			tcConfig.updateTestReporter("NextGenResortNews", "dreamDiveExploreVal", Status.FAIL,
					"Dream Dive Explore  content not present on the page");
		}

	}

}
