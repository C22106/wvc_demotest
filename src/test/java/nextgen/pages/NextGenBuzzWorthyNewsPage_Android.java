package nextgen.pages;

import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenBuzzWorthyNewsPage_Android extends NextGenBuzzWorthyNewsPage_Web {

	public static final Logger log = Logger.getLogger(NextGenBuzzWorthyNewsPage_Android.class);

	public NextGenBuzzWorthyNewsPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		clubWyndhamNav = By.xpath("//ul[contains(@class,'mobile')]//li/a[text()='Club Benefits']");
		buzzWorthyPageNav = By.xpath(
				"//ul[contains(@class,'mobile')]//li/a[text()='Club Benefits']/../ul/li/a[contains(text(),'Buzzworthy News')]");

	}

	protected By toggleButton = By
			.xpath("//ul[contains(@class,'mobile')]//li/a[text()='Club Benefits']/../button[@title='Toggle menu']");

	@Override
	public void buzzNewsPageNav() {
		String href;
		waitUntilElementVisibleBy(driver, clubWyndhamNav, 120);
		clickElementBy(toggleButton);

		if (verifyObjectDisplayed(buzzWorthyPageNav)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "buzzNewsPageNav", Status.PASS,
					"Buzz Worthy News present in Sub Nav");
			href = getElementAttribute(buzzWorthyPageNav, "href").trim();
			driver.findElement(buzzWorthyPageNav).click();
			pageCheck();
			assertTrue(getCurrentURL().trim().equals(href), "Navigation not Successful");
		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "buzzNewsPageNav", Status.FAIL,
					"Buzz Worthy News not present in Sub Nav");
		}

	}

}

