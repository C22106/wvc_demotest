package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenResourcesPage_Android extends NextGenResourcesPage_Web {

	public static final Logger log = Logger.getLogger(NextGenResourcesPage_Android.class);

	public NextGenResourcesPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
