package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class OwnerTestimonialPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(OwnerTestimonialPage_Web.class);

	public OwnerTestimonialPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// whereAreWeGoingThisSummer
	// div[@aria-hidden='false']//h3
	protected By ourFutureBucketArticle = By.xpath("//div[@aria-hidden='false']//h3[contains(.,'Our Future Bucket')]");
	protected By ourFutureBucketReadMore = By.xpath(
			"//h3[contains(.,'Our Future Bucket')]//ancestor::div[@class='wyn-card__content']//div[@class='wyn-card__cta' and contains(.,'Read')]");
	protected By whareAreWeGoingHeader = By.xpath("//h1[contains(.,'Our Future Bucket')]");
	protected By ourFutureBucketImage = By.xpath(
			"//h1[contains(.,'Our Future Bucket')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//img");
	protected By ourFutureBucketFacebook = By.xpath(
			"//h1[contains(.,'Our Future Bucket')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By ourFutureBucketTwitter = By.xpath(
			"//h1[contains(.,'Our Future Bucket')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By ourFutureBucketNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Our Future Bucket')]");
	protected By textHeader = By.xpath("//div[@class='wrapper-container']//h3");
	protected By authorName = By.xpath("//div[@class='wrapper-container']//p//b");
	protected By contentLocation = By.xpath("//div[@class='wrapper-container']//p");
	protected By nextArrow = By.xpath("//button[@class='slick-next slick-arrow']");

	// Operation: All 50 States
	protected By operationStatesArticle = By.xpath("//div[@aria-hidden='false']//h3[contains(.,'Fifty by Fifty')]");
	protected By operationStatesReadMore = By.xpath(
			"//h3[contains(.,'Fifty by Fifty')]//ancestor::div[@class='wyn-card__content']//div[@class='wyn-card__cta' and contains(.,'Read')]");
	protected By operationStatesHeader = By.xpath("//h1[contains(.,'Fifty by Fifty')]");
	protected By operationStatesImage = By
			.xpath("//h1[contains(.,'Fifty by Fifty')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//img");
	protected By operationStatesFacebook = By.xpath(
			"//h1[contains(.,'Fifty by Fifty')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By operationStatesTwitter = By.xpath(
			"//h1[contains(.,'Fifty by Fifty')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By operationStatesNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Fifty by Fifty')]");

	// Island Escape Full of Adventures
	protected By ourKidsInAweArticle = By.xpath("//div[@aria-hidden='false']//h3[contains(.,'Our Kids Will')]");
	protected By ourKidsInAweReadMore = By.xpath(
			"//h3[contains(.,'Our Kids Will')]//ancestor::div[@class='wyn-card__content']//div[@class='wyn-card__cta' and contains(.,'Read')]");
	protected By ourKidsInAweHeader = By.xpath("//h1[contains(.,'Our Kids Will')]");
	protected By ourKidsInAweImage = By
			.xpath("//h1[contains(.,'Our Kids Will')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//img");
	protected By ourKidsInAweFacebook = By.xpath(
			"//h1[contains(.,'Our Kids Will')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By ourKidsInAweTwitter = By.xpath(
			"//h1[contains(.,'Our Kids Will')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By ourKidsInAweNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Our Kids Will')]");

	// Mother & Daughter Take On Australia
	protected By motherDaughterArticle = By.xpath("//div[@aria-hidden='false']//h3[contains(.,'Mother & Daughter')]");
	protected By motherDaughterReadMore = By.xpath(
			"//h3[contains(.,'Mother & Daughter')]//ancestor::div[@class='wyn-card__content']//div[@class='wyn-card__cta' and contains(.,'Read')]");
	protected By motherDaughterHeader = By.xpath("//h1[contains(.,'Mother-Daughter')]");
	protected By motherDaughterImage = By
			.xpath("//h1[contains(.,'Mother-Daughter')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//img");
	protected By motherDaughterFacebook = By.xpath(
			"//h1[contains(.,'Mother-Daughter')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By motherDaughterTwitter = By.xpath(
			"//h1[contains(.,'Mother-Daughter')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By motherDaughterNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Mother-Daughter')]");

	// Making vacation a priority
	protected By MakingVacationaPriorityArticle = By
			.xpath("//div[@aria-hidden='false']//h3[contains(.,'Making Vacation a Priority')]");
	protected By MakingVacationaPriorityReadMore = By.xpath(
			"//h3[contains(.,'Making Vacation a Priority')]//ancestor::div[@class='wyn-card__content']//div[@class='wyn-card__cta']//div[@class='wyn-button-cta']");
	protected By MakingVacationaPriorityHeader = By.xpath("//h1[contains(.,'Making Vacation a')]");
	protected By MakingVacationaPriorityImage = By.xpath(
			"//h1[contains(.,'Making Vacation a')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//img");
	protected By MakingVacationaPriorityFacebook = By.xpath(
			"//h1[contains(.,'Making Vacation a')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By MakingVacationaPriorityTwitter = By.xpath(
			"//h1[contains(.,'Making Vacation a')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By MakingVacationaPriorityNav = By
			.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Making Vacation a Priority')]");

	// Vacationing on Duty
	protected By vacationingOnDutyArticle = By.xpath("//div[@aria-hidden='false']//h3[contains(.,'Vacationing')]");
	protected By vacationingOnDutyHeader = By.xpath("//div[@aria-hidden='false']//h1[contains(.,'Vacationing')]");
	protected By vacationingOnDutyImage = By
			.xpath("//h1[contains(.,'Vacationing')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//img");
	protected By vacationingOnDutyFacebook = By.xpath(
			"//h1[contains(.,'Vacationing')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='facebook']");
	protected By vacationingOnDutyTwitter = By.xpath(
			"//h1[contains(.,'Vacationing')]//ancestor::div[@class='wyn-hero wyn-hero--full is-side']//div[@class='wyn-hero__share']//div[@data-network='twitter']");
	protected By vacationingOnDutyNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Vacationing')]");

	/*
	 * Method: navigateToourFutureBucket Description: Future Bucket List navigation:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToourFutureBucket() {

		if (verifyObjectDisplayed(ourFutureBucketArticle)) {

		} else {
			try {
				clickElementBy(nextArrow);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {

			}
		}

		// waitUntilElementVisibleBy(driver, ourFutureBucketArticle, 120);

		getElementInView(ourFutureBucketArticle);

		if (verifyObjectDisplayed(ourFutureBucketArticle)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
					"Our Future block present in the page");

			clickElementBy(ourFutureBucketArticle);

			waitUntilElementVisibleBy(driver, whareAreWeGoingHeader, 120);

			if (verifyObjectDisplayed(whareAreWeGoingHeader)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
						"Our Future page navigated");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
						"Our Future page navigation failed");

			}

			if (verifyObjectDisplayed(ourFutureBucketNav)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
						"Our Future breadcrumb present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
						"Our Future breadcrumb not present");

			}

			if (verifyObjectDisplayed(ourFutureBucketImage)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
						"Our Future hero image present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
						"Our Future hero image not present");

			}

			if (verifyObjectDisplayed(ourFutureBucketFacebook)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
						"Where Are We Going This facebook icon present");

				clickElementBy(ourFutureBucketFacebook);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
							"Navigated to facebook page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
							"Navigation to facebook page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
						"Where Are We Going This facebook icon not present");

			}

			if (verifyObjectDisplayed(ourFutureBucketTwitter)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
						"Where Are We Going This twitter icon present");

				clickElementBy(ourFutureBucketTwitter);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("TWITTER")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
							"Navigated to twitter page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
							"Navigation to twitter page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
						"Where Are We Going This twitter icon not present");

			}

			if (verifyObjectDisplayed(authorName)) {
				List<WebElement> contentParaList = driver.findElements(contentLocation);

				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.PASS,
						"Our Future author name present as: " + getElementText(authorName) + " and locations as "
								+ contentParaList.get(contentParaList.size() - 1).getText());

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
						"Our Future author name not present");

			}

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourFutureBucket", Status.FAIL,
					"Our Future block not present in the page");
		}

		driver.navigate().back();
		driver.navigate().back();
	}

	/*
	 * Method: navigateToAll50States Description: All 50 States Page Navigations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToAll50States() {

		if (verifyObjectDisplayed(operationStatesArticle)) {

		} else {
			try {
				clickElementBy(nextArrow);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {

			}
		}
		// waitUntilElementVisibleBy(driver, operationStatesArticle, 120);

		getElementInView(operationStatesArticle);

		if (verifyObjectDisplayed(operationStatesArticle)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
					"Fifty By Fifty block present in the page");

			clickElementBy(operationStatesArticle);

			waitUntilElementVisibleBy(driver, operationStatesHeader, 120);

			if (verifyObjectDisplayed(operationStatesHeader)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
						"Fifty By Fifty page navigated");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
						"Fifty By Fifty page navigation failed");

			}

			if (verifyObjectDisplayed(operationStatesNav)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
						"Fifty By Fifty breadcrumb present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
						"Fifty By Fifty breadcrumb not present");

			}

			if (verifyObjectDisplayed(operationStatesImage)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
						"Fifty By Fifty hero image present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
						"Fifty By Fifty hero image not present");

			}

			if (verifyObjectDisplayed(operationStatesFacebook)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
						"Fifty By Fiftyfacebook icon present");

				clickElementBy(operationStatesFacebook);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
							"Navigated to facebook page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
							"Navigation to facebook page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
						"Fifty By Fifty facebook icon not present");

			}

			if (verifyObjectDisplayed(operationStatesTwitter)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
						"Fifty By Fifty twitter icon present");

				clickElementBy(operationStatesTwitter);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("TWITTER")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
							"Navigated to twitter page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
							"Navigation to twitter page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
						"Fifty By Fiftytwitter icon not present");

			}

			if (verifyObjectDisplayed(authorName)) {
				List<WebElement> contentParaList = driver.findElements(contentLocation);

				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.PASS,
						"Fifty By Fifty author name present as: " + getElementText(authorName) + " and locations as "
								+ contentParaList.get(contentParaList.size() - 1).getText());

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
						"Fifty By Fifty author name not present");

			}

			driver.navigate().back();
			driver.navigate().back();
		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToOperationStates", Status.FAIL,
					"Fifty By Fifty block not present in the page");
		}

	}

	/*
	 * Method: navigateToOurKidsInAwe Description: Our Kids In Awe page navigation:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToOurKidsInAwe() {

		if (verifyObjectDisplayed(ourKidsInAweArticle)) {

		} else {
			try {
				clickElementBy(nextArrow);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {

			}
		}
		// waitUntilElementVisibleBy(driver, ourKidsInAweArticle, 120);

		getElementInView(ourKidsInAweArticle);

		if (verifyObjectDisplayed(ourKidsInAweArticle)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
					"Our Kids In Awe block present in the page");

			clickElementBy(ourKidsInAweArticle);

			waitUntilElementVisibleBy(driver, ourKidsInAweHeader, 120);

			if (verifyObjectDisplayed(ourKidsInAweHeader)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
						"Our Kids In Awe page navigated");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
						"Our Kids In Awe page navigation failed");

			}

			if (verifyObjectDisplayed(ourKidsInAweNav)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
						"Our Kids In Awe breadcrumb present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
						"Our Kids In Awe breadcrumb not present");

			}

			if (verifyObjectDisplayed(ourKidsInAweImage)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
						"Our Kids In Awe hero image present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
						"Our Kids In Awe hero image not present");

			}

			if (verifyObjectDisplayed(ourKidsInAweFacebook)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
						"Our Kids In Awe facebook icon present");

				clickElementBy(ourKidsInAweFacebook);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
							"Navigated to facebook page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
							"Navigation to facebook page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
						"Our Kids In Awe facebook icon not present");

			}

			if (verifyObjectDisplayed(ourKidsInAweTwitter)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
						"Our Kids In Awe twitter icon present");

				clickElementBy(ourKidsInAweTwitter);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("TWITTER")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
							"Navigated to twitter page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
							"Navigation to twitter page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
						"Our Kids In Awetwitter icon not present");

			}

			if (verifyObjectDisplayed(authorName)) {
				List<WebElement> contentParaList = driver.findElements(contentLocation);

				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.PASS,
						"Our Kids In Awe author name present as: " + getElementText(authorName) + " and locations as "
								+ contentParaList.get(contentParaList.size() - 1).getText());

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
						"Our Kids In Awe author name not present");

			}

			driver.navigate().back();
			driver.navigate().back();

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToourKidsInAwe", Status.FAIL,
					"Our Kids In Awe block not present in the page");
		}

	}

	/*
	 * Method: navigateToMotherDaughter Description: Mother Daughter Page
	 * Navigation: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToMotherDaughter() {

		waitUntilElementVisibleBy(driver, motherDaughterArticle, 120);

		getElementInView(motherDaughterArticle);

		if (verifyObjectDisplayed(motherDaughterArticle)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
					"Mother & Daughter Take On Australia block present in the page");

			clickElementBy(motherDaughterReadMore);

			waitUntilElementVisibleBy(driver, motherDaughterHeader, 120);

			if (verifyObjectDisplayed(motherDaughterHeader)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
						"Mother & Daughter Take On Australia page navigated");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
						"Mother & Daughter Take On Australia page navigation failed");

			}

			if (verifyObjectDisplayed(motherDaughterNav)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
						"Mother & Daughter Take On Australia breadcrumb present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
						"Mother & Daughter Take On Australia breadcrumb not present");

			}

			if (verifyObjectDisplayed(motherDaughterImage)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
						"Mother & Daughter Take On Australia hero image present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
						"Mother & Daughter Take On Australia hero image not present");

			}

			if (verifyObjectDisplayed(motherDaughterFacebook)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
						"Mother & Daughter Take On Australia facebook icon present");

				clickElementBy(motherDaughterFacebook);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
							"Navigated to facebook page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
							"Navigation to facebook page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
						"Mother & Daughter Take On Australia facebook icon not present");

			}

			if (verifyObjectDisplayed(motherDaughterTwitter)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
						"Mother & Daughter Take On Australia twitter icon present");

				clickElementBy(motherDaughterTwitter);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("TWITTER")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
							"Navigated to twitter page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
							"Navigation to twitter page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
						"Mother & Daughter Take On Australiatwitter icon not present");

			}

			if (verifyObjectDisplayed(textHeader)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
						"Mother & Daughter Take On Australia content header present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
						"Mother & Daughter Take On Australia content header not present");

			}

			if (verifyObjectDisplayed(authorName)) {
				List<WebElement> contentParaList = driver.findElements(contentLocation);

				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.PASS,
						"Mother & Daughter Take On Australia author name present as: " + getElementText(authorName)
								+ " and locations as " + contentParaList.get(contentParaList.size() - 1).getText());

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
						"Mother & Daughter Take On Australia author name not present");

			}

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMotherDaughter", Status.FAIL,
					"Mother & Daughter Take On Australia block not present in the page");
		}

	}

	/*
	 * Method: navigateToMakingVacationAPriority Description: Making Vacation a
	 * priority nav: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToMakingVacationAPriority() {

		// waitUntilElementVisibleBy(driver, MakingVacationaPriorityArticle,
		// 120);
		//
		// getElementInView(MakingVacationaPriorityArticle);
		//
		// if (verifyObjectDisplayed(MakingVacationaPriorityArticle)) {
		// tcConfig.updateTestReporter("OwnerTestimonialPage",
		// "navigateToMakingVacationAPriority", Status.PASS,
		// "Making Vacation A Priority present in the page");
		//
		// clickElementBy(MakingVacationaPriorityReadMore);

		driver.navigate().to(testData.get("URL") + "/be-in-the-club/owner-testimonials/making-vacation-a-priority");

		waitUntilElementVisibleBy(driver, MakingVacationaPriorityHeader, 120);

		if (verifyObjectDisplayed(MakingVacationaPriorityHeader)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
					"Making Vacation A Priority page navigated");

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
					"Making Vacation A Priority page navigation failed");

		}

		if (verifyObjectDisplayed(MakingVacationaPriorityNav)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
					"Making Vacation A Priority breadcrumb present");

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
					"Making Vacation A Priority breadcrumb not present");

		}

		if (verifyObjectDisplayed(MakingVacationaPriorityImage)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
					"Making Vacation A Priority hero image present");

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
					"Making Vacation A Priority hero image not present");

		}

		if (verifyObjectDisplayed(MakingVacationaPriorityFacebook)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
					"Making Vacation A Priority facebook icon present");

			clickElementBy(MakingVacationaPriorityFacebook);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
						"Navigated to facebook page");
			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
						"Navigation to facebook page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
					"Making Vacation A Priority facebook icon not present");

		}

		if (verifyObjectDisplayed(MakingVacationaPriorityTwitter)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
					"Making Vacation A Priority twitter icon present");

			clickElementBy(MakingVacationaPriorityTwitter);

			String winHandlesBefor = driver.getWindowHandle();

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);

			}

			if (driver.getTitle().toUpperCase().contains("TWITTER")) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
						"Navigated to twitter page");
			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
						"Navigation to twitter page failed");
			}

			driver.close();

			driver.switchTo().window(winHandlesBefor);

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
					"Making Vacation A Priority twitter icon not present");

		}

		if (verifyObjectDisplayed(textHeader)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
					"Making Vacation A Priority content header present");

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
					"Making Vacation A Priority content header not present");

		}

		if (verifyObjectDisplayed(authorName)) {
			List<WebElement> contentParaList = driver.findElements(contentLocation);

			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.PASS,
					"Making Vacation A Priority author name present as: " + getElementText(authorName)
							+ " and locations as " + contentParaList.get(contentParaList.size() - 1).getText());

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToMakingVacationAPriority", Status.FAIL,
					"Making Vacation A Priority author name not present");

		}

		driver.navigate().back();
		driver.navigate().back();
		// } else {
		// tcConfig.updateTestReporter("OwnerTestimonialPage",
		// "navigateToMakingVacationAPriority", Status.FAIL,
		// "Making Vacation A Priority block not present in the page");
		// }

	}

	/*
	 * Method: v Description: Family Page navigation: Date Jul/2019 Author: Unnat
	 * Jain Changes By
	 */
	public void navigateToVacationingOnDuty() {

		// waitUntilElementVisibleBy(driver, vacationingOnDutyArticle, 120);

		if (verifyObjectDisplayed(vacationingOnDutyArticle)) {

		} else {
			try {
				clickElementBy(nextArrow);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} catch (Exception e) {

			}
		}
		getElementInView(vacationingOnDutyArticle);

		if (verifyObjectDisplayed(vacationingOnDutyArticle)) {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
					"Vacationing On Duty block present in the page");

			clickElementBy(vacationingOnDutyArticle);

			waitUntilElementVisibleBy(driver, vacationingOnDutyHeader, 120);

			if (verifyObjectDisplayed(vacationingOnDutyHeader)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
						"Vacationing On Duty page navigated");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
						"Vacationing On Duty page navigation failed");

			}

			if (verifyObjectDisplayed(vacationingOnDutyNav)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
						"Vacationing On Duty breadcrumb present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
						"Vacationing On Duty breadcrumb not present");

			}

			if (verifyObjectDisplayed(vacationingOnDutyImage)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
						"Vacationing On Duty hero image present");

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
						"Vacationing On Duty hero image not present");

			}

			if (verifyObjectDisplayed(vacationingOnDutyFacebook)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
						"Vacationing On Duty facebook icon present");

				clickElementBy(vacationingOnDutyFacebook);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("FACEBOOK")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
							"Navigated to facebook page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
							"Navigation to facebook page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
						"Vacationing On Duty facebook icon not present");

			}

			if (verifyObjectDisplayed(vacationingOnDutyTwitter)) {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
						"Vacationing On Duty twitter icon present");

				clickElementBy(vacationingOnDutyTwitter);

				String winHandlesBefor = driver.getWindowHandle();

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}

				if (driver.getTitle().toUpperCase().contains("TWITTER")) {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
							"Navigated to twitter page");
				} else {
					tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
							"Navigation to twitter page failed");
				}

				driver.close();

				driver.switchTo().window(winHandlesBefor);

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
						"Vacationing On Dutytwitter icon not present");

			}

			// if (verifyObjectDisplayed(textHeader)) {
			// tcConfig.updateTestReporter("OwnerTestimonialPage",
			// "navigateToIslandEscape", Status.PASS,
			// "Vacationing On Duty content header present");
			//
			// } else {
			// tcConfig.updateTestReporter("OwnerTestimonialPage",
			// "navigateToIslandEscape", Status.FAIL,
			// "Vacationing On Duty content header not present");
			//
			// }

			if (verifyObjectDisplayed(authorName)) {
				List<WebElement> contentParaList = driver.findElements(contentLocation);

				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.PASS,
						"Vacationing On Duty author name present as: " + getElementText(authorName)
								+ " and locations as " + contentParaList.get(contentParaList.size() - 1).getText());

			} else {
				tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
						"Vacationing On Duty author name not present");

			}

		} else {
			tcConfig.updateTestReporter("OwnerTestimonialPage", "navigateToIslandEscape", Status.FAIL,
					"Vacationing On Duty block not present in the page");
		}

	}

}