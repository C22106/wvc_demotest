package nextgen.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBookConfirmationPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUIBookPage_Web.class);

	protected By rentedPointLabelUnderPayment = By
			.xpath("//section[contains(@class,'payment-charges')]//span[text()='Rented Points']");
	protected By rentedPointValueUnderPayment = By.xpath(
			"//section[contains(@class,'payment-charges')]//span[text()='Rented Points']/following-sibling::span");

	protected By pointProtectionLabelUnderPayment = By
			.xpath("//section[contains(@class,'payment-charges')]//span[text()='Points Protection']");
	protected By pointProtectionValueUnderPayment = By.xpath(
			"//section[contains(@class,'payment-charges')]//span[text()='Points Protection']/following-sibling::span");

	protected By myAccountLink = By.xpath("//span[contains(@class,'account-info--text')]");
	protected By upcomingVacationsLink = By
			.xpath("//li[@class='account-navigation__list-item' and not(@role)]/a[contains(@href,'/vacations')]");
	protected By reservationCards = By.xpath("//div[@class='upcoming-reservation-card__reservation-info']");
	protected By reservationConfirmationHeader = By.xpath("//h2[.='Reservation Confirmation']");
	protected By reservationConfirmationMessage = By
			.xpath("//h2[.='Reservation Confirmation']/following-sibling::h1[contains(text(),'Thank')]");

	protected By reservationSummaryHeader = By.xpath("//div[contains(@class,'cell')]/h2[.='Reservation Summary']");
	protected By reservationNumberHeader = By.xpath("//div[contains(@class,'cell')]/h3[text()='Confirmation Number']");
	protected By reservationNumber = By
			.xpath("//div[contains(@class,'cell')]/h3[text()='Confirmation Number']/following-sibling::p");
	protected By memberNumberHeader = By.xpath("//div[contains(@class,'cell')]/h3[text()='Member Number']");
	protected By memberNumber = By
			.xpath("//div[contains(@class,'cell')]/h3[text()='Member Number']/following-sibling::p");
	protected By travelerHeader = By.xpath("//div[contains(@class,'cell')]/h3[text()='Traveler']");
	protected By travellerName = By.xpath("//div[contains(@class,'cell')]/h3[text()='Traveler']/following-sibling::p");
	protected By emailHeader = By.xpath("//div[contains(@class,'cell')]/h3[text()='Email Address']");
	protected By email = By.xpath("//div[contains(@class,'cell')]/h3[text()='Email Address']/following-sibling::p");
	protected By resortName = By.xpath("//a[contains(@class,'resort__name')]");
	protected By resortAddress = By.xpath("//p[contains(@class,'resort__address')]");
	protected By resortPhone = By.xpath("//a[contains(@class,'resort__phone') and contains(@href,'tel:')]");
	protected By bookingDates = By
			.xpath("//div[contains(@class,'reservation-summary')]//h3[.='Dates']/following-sibling::p");
	protected By unitType = By
			.xpath("//div[contains(@class,'reservation-summary')]//h3[.='Suite Type']/following-sibling::p");

	protected By chargerSummaryHeader = By.xpath("//h2[.='Charges Summary']");
	protected By membershipChargesHeader = By
			.xpath("//h2[.='Charges Summary']/parent::div/section//span[.='Membership Charges']");
	protected By houseKeepingCost = By
			.xpath("//span[contains(.,'Total Housekeeping Credits')]/following-sibling::span");
	protected By totalHouseKeepingExp = By.xpath("//span[contains(.,'Total Housekeeping Credits')]/*[2]");
	protected By totalHouseKeepingExpanded = By
			.xpath("//span[contains(.,'Total Housekeeping Credits')]/*[2][contains(@style,'transform')]");
	protected By houseKeepingCurrentYear = By
			.xpath("//span[contains(.,'Housekeeping Credits - Current Use Year')]//following-sibling::span");
	protected By houseKeepingborrowed = By
			.xpath("//span[contains(.,'Borrowed Housekeeping Credits - Next Use Year')]//following-sibling::span");
	protected By reservationTransactionTooltip = By.xpath("//span[contains(.,'Reservation Transaction')]");
	protected By reservationTransactionText = By
			.xpath("//*[contains(.,'You’ll receive one Reservation Transaction for every 77,000 points you own')]");
	protected By reservationTransaction = By
			.xpath("//span[contains(.,'Reservation Transaction')]/following-sibling::span");

	protected By ccOwnerName = By.xpath("//span[.='Payment Charges']/parent::section/section//span");
	protected By ccNum = By.xpath(
			"//span[.='Payment Charges']/parent::section/section//div/p[contains(@class,'payment-type')]/span[contains(text(),'Ending in')]");
	protected By reservationTransactionAmount = By
			.xpath("//span[.='Payment Charges']/parent::section/section//span[contains(.,'Reservation Transaction')]");
	protected By housekeepingAmount = By
			.xpath("//span[.='Payment Charges']/parent::section/section//span[contains(.,'Housekeeping')]");
	protected By cancellationPolicyText = By.xpath(
			"(//h3[.='Cancellation Policy']/parent::div/p[contains(text(),'If your reservation is cancelled 15 days or more prior to the check-in date')])[1]");
	protected By cancellationPolicyDetailsLink = By.xpath(
			"(//h3[.='Cancellation Policy']/parent::div/a[contains(@href,'canceling-reservation') and contains(.,'View')])[1]");
	protected By cancellationPolicyHeader = By.xpath("//h3[.='Canceling a Reservation']");
	protected By instantUpgradeLabel = By.xpath("//div[@class='upgrade-applied']");

	// Kamalesh
	protected By totalPointLabel = By.xpath("//span[text()='Total Points']");
	protected By totalPointValue = By.xpath("//span[text()='Total Points']/following-sibling::span");

	protected By availablePointLabel = By.xpath("//span[contains(text(),'Available Points')]");
	protected By availablePointValue = By.xpath("//span[contains(text(),'Available Points')]/following-sibling::span");
	protected By rentedPointLabel = By.xpath("//span[text()='Rented Points']");
	protected By rentedPointValue = By.xpath("//span[text()='Rented Points']/following-sibling::span");
	protected By borrowedPointLabel = By.xpath("//span[contains(text(), 'Borrowed Points')]");
	protected By borrowedPointValue = By.xpath("//span[contains(text(), 'Borrowed Points')]/following-sibling::span");
	protected By housekeepingCreditsLabel = By.xpath("//span[text()='Housekeeping Credits']");
	protected By housekeepingCreditsValue = By
			.xpath("//span[text()='Housekeeping Credits']/parent::span/following-sibling::span");

	protected By reservationTransactionLabel = By.xpath("//span[text()='Reservation Transaction']");
	protected By reservationTransactionValue = By
			.xpath("//span[text()='Reservation Transaction']/parent::span/following-sibling::span");
	protected By specialRequestHeader = By.xpath("//h3[text()='Special Suite Request']");
	protected By specialRequestValue = By.xpath("//h3[text()='Special Suite Request']/following-sibling::p");
	protected By paymentChargesHeader = By.xpath("//span[text()='Payment Charges']");
	protected By paypalEmail = By.xpath("//p[@class='payment-type paypal']/span");
	protected By labelDates = By.xpath("//div[contains(@class,'cell small-12 margin-bottom-1') and contains(.,'Dates')]//p");
	protected static String reservationStartDate;
	protected static String reservationEndDate;

	public CUIBookConfirmationPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	/*
	 * Method: getReservationDates Description: get reservation dates field Date:
	 * Feb/2021 Author: saket Sharma Changes By:
	 */
	

	
	public void getReservationDates(){
		getElementInView(labelDates);
		assertTrue(verifyObjectDisplayed(labelDates),
				"Reservation dates are present in booking confirmation page ");
		String strStartDate = getElementText(labelDates).substring(0,8).trim();
		String strEndDate = getElementText(labelDates).substring(11,19).trim();
		reservationStartDate = strStartDate;
		reservationEndDate = strEndDate;
	}

	/*
	 * Method: navigateToUpcomingVacations Description: Navigate to upcoming booking
	 * page from reservation confirmation Page Date field Date: June/2020 Author:
	 * Monideep Roychowdhury Changes By: NA
	 */

	public void navigateToUpcomingVacations() {

		waitUntilElementVisibleBy(driver, myAccountLink, 120);
		clickElementBy(myAccountLink);

		getElementInView(upcomingVacationsLink);
		assertTrue(verifyObjectDisplayed(upcomingVacationsLink), "Upcoming Vacations link not present");
		clickElementBy(upcomingVacationsLink);

		waitUntilElementVisibleBy(driver, reservationCards, 120);
		assertTrue(verifyObjectDisplayed(reservationCards), "Unable to navigate to upcoming vacations page");

		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "navigateToUpcomingVacations", Status.PASS,
				"Successfully navigated to Upcoming vacations page");
	}

	/*
	 * Method: validateConfirmationHeader Description: validate Confirmation Header
	 * Date field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 */

	public void validateConfirmationHeader() {

		assertTrue(verifyObjectDisplayed(reservationConfirmationHeader), "Reservation confirmation header not present");
		assertTrue(verifyObjectDisplayed(reservationConfirmationMessage),
				"Reservation confirmation message not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateConfirmationHeader", Status.PASS,
				"Reservation confirmation header and message present");
	}

	/*
	 * Method: validateReservationSummary Description: Navigate to upcoming booking
	 * page from reservation confirmation Page Date field Date: June/2020 Author:
	 * Monideep Roychowdhury Changes By:
	 */

	public void validateReservationSummary() {

		assertTrue(verifyObjectDisplayed(reservationSummaryHeader), "Reservation summary header not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Summary header and message present");

		assertTrue(verifyObjectDisplayed(reservationNumberHeader), "Reservation number header not present");
		assertTrue(getElementText(reservationNumber).length() > 0, "Reservation number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Number present : " + getElementText(reservationNumber));

		assertTrue(verifyObjectDisplayed(memberNumberHeader), "Member number header not present");
		assertTrue(getElementText(memberNumber).length() > 0, "Member number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Member Number present : " + getElementText(memberNumber));

		assertTrue(verifyObjectDisplayed(travelerHeader), "Traveller header not present");
		assertTrue(getElementText(travellerName).equalsIgnoreCase(tcConfig.getTestData().get("GuestName")),
				"Traveller name doesn't match guest information");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Traveller Name matches guest information");

		assertTrue(verifyObjectDisplayed(emailHeader), "Email header not present");
		assertTrue(getElementText(email).equalsIgnoreCase(tcConfig.getTestData().get("GuestEmail")),
				"Traveller email doesn't match guest information");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Traveller Email matches guest information");

	}

	/*
	 * Method: validateBookingDetails Description: validate Booking Details Date:
	 * June/2020 Author: Monideep Roychowdhury Changes By:
	 */

	public void validateBookingDetails() {
		assertTrue(verifyObjectDisplayed(resortName), "Resort Name not present");
		assertTrue(verifyObjectDisplayed(resortAddress), "Resort Address not present");
		assertTrue(verifyObjectDisplayed(resortPhone), "Resort Phone not present");
		assertTrue(getElementText(bookingDates).length() > 0, "Booking dates not present");
		assertTrue(getElementText(unitType).length() > 0, "Unit type booked not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateBookingDetails", Status.PASS,
				"Booking details validated successfully");
	}

	/*
	 * Method: validateChargesSummary Description: validate Charges Summary:
	 * June/2020 Author: Monideep Roychowdhury Changes By:
	 */
	public void validateChargesSummary() {

		assertTrue(verifyObjectDisplayed(chargerSummaryHeader), "Charges summary header not present");
		assertTrue(verifyObjectDisplayed(membershipChargesHeader), "Membership charges header not present");

		assertTrue(getElementText(houseKeepingCost).equalsIgnoreCase(tcConfig.getTestData().get("HouseKeepingCost")),
				"House keeping credits dont match");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateChargesSummary", Status.PASS,
				"House keeping credits required for booking matches");

		assertTrue(verifyObjectDisplayed(totalHouseKeepingExp), "Total House keeping drop down not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Total House keeping dropdown exists");

		clickElementBy(totalHouseKeepingExp);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(totalHouseKeepingExpanded), "Total House keeping drop down not expanded");

		assertTrue(
				getElementText(houseKeepingCurrentYear)
						.equalsIgnoreCase(tcConfig.getTestData().get("HouseKeepingCreditsAvailable")),
				"Current year house keeping credits does not matches");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Current year house keeping credits matches");

		assertTrue(
				getElementText(houseKeepingborrowed)
						.equalsIgnoreCase(tcConfig.getTestData().get("HouseKeepingBorrowPoints")),
				"Borrowed house keeping credits does not matches");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Borrowed house keeping credits matches");

		hoverOnElement(reservationTransactionTooltip);
		assertTrue(verifyObjectDisplayed(reservationTransactionText), "Tooltip text not present");

		assertTrue(getElementText(reservationTransaction).equalsIgnoreCase("1"),
				"Reservation transaction value does not matches");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummary", Status.PASS,
				"Reservation transaction value matches");

	}

	/*
	 * Method: validatePaymentCharges Description: validatePaymentCharges Date:
	 * June/2020 Author: Monideep Roychowdhury Changes By:
	 */

	public void validatePaymentCharges() {

		assertTrue(getElementText(ccOwnerName).contains(tcConfig.getTestData().get("ccFirstName")),
				"CC owner name does not match");

		assertTrue(getElementText(ccNum).contains(tcConfig.getTestData().get("creditCardNumber").substring(13)),
				"CC number does not match");

		tcConfig.updateTestReporter("CUIBookPage", "validatePaymentCharges", Status.PASS,
				"Payment charges validated sucessfully");

	}

	/*
	 * Method: validateCancellationPolicy Description: Navigate to upcoming booking
	 * page from reservation confirmation Page Date field Date: June/2020 Author:
	 * Monideep Roychowdhury Changes By: Kamalesh
	 */

	public void validateCancellationPolicy() {

		assertTrue(verifyObjectDisplayed(cancellationPolicyText), "Cancellation Text not present");
		assertTrue(verifyObjectDisplayed(cancellationPolicyDetailsLink),
				"Cancellation policy details link not present");

		clickElementBy(cancellationPolicyDetailsLink);
		waitUntilElementVisibleBy(driver, cancellationPolicyHeader, 120);
		tcConfig.updateTestReporter("CUIBookPage", "validateCancellationPolicy", Status.PASS,
				"Cancellation policy text and link validated sucessfully");

	}

	/*
	 * Method: validateReservationCharges Description: validate Reservation Charges
	 * Date field Date: June/2020 Author: Monideep Roychowdhury Changes By:
	 */

	public void validateReservationCharges() {

		if (tcConfig.getTestData().get("PurchaseReservationTransaction").equalsIgnoreCase("YES")) {
			assertTrue(verifyObjectDisplayed(reservationTransactionAmount),
					"Reservation transaction amount should be present");
		} else {
			assertFalse(verifyObjectDisplayed(reservationTransactionAmount),
					"Reservation transaction amount shouldn't be present");
		}

		tcConfig.updateTestReporter("CUIBookPage", "validateReservationChargesAbsence", Status.PASS,
				"Reservation transaction charges validated successfully");
	}

	/*
	 * Method: validateHousekeepingCreditChargesAbsence Description: validate
	 * Housekeeping Credit Charges Absence Date field Date: June/2020 Author:
	 * Monideep Roychowdhury Changes By:
	 */

	public void validateHousekeepingCreditChargesAbsence() {
		assertFalse(verifyObjectDisplayed(housekeepingAmount), "Housekeeping credit amount shouldn't be present");
		tcConfig.updateTestReporter("CUIBookPage", "validateHousekeepingCreditChargesAbsence", Status.PASS,
				"House keeping credit charges not present");
	}

	/*
	 * Method: validateInstantUpgradeDetails Description: Navigate to upcoming
	 * booking page from reservation confirmation Page Date field Date: June/2020
	 * Author: Kamalesh Changes By:
	 */

	public void validateInstantUpgradeDetails() {
		getElementInView(instantUpgradeLabel);
		assertTrue(verifyObjectDisplayed(instantUpgradeLabel), "Instant Upgrade Details not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateInstantUpgradeDetails", Status.PASS,
				"Instant Upgrade Details  present");
	}

	/*
	 * Method: validateChargesSummaryForRentingPoint Description: Navigate to
	 * upcoming booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Kamalesh Changes By:
	 */
	public void validateChargesSummaryForRentingPoint() {
		getElementInView(chargerSummaryHeader);
		assertTrue(verifyObjectDisplayed(chargerSummaryHeader), "Charges summary header not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Charges summary header  present");
		assertTrue(verifyObjectDisplayed(membershipChargesHeader), "Membership charges header not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Membership charges header  present");

		assertTrue(verifyObjectDisplayed(totalPointLabel), "Total Point Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Total Point Label  present");
		assertTrue(verifyObjectDisplayed(totalPointValue), "Total Point Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Total Point value present");

		clickElementBy(totalPointLabel);
		getElementInView(availablePointLabel);
		assertTrue(verifyObjectDisplayed(availablePointLabel), "Available Point Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Available Point Label  present");
		assertTrue(verifyObjectDisplayed(availablePointValue), "Available Point Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Available Point value present");

		assertTrue(verifyObjectDisplayed(rentedPointLabel), "Rented Point Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Rented Point Label  present");
		assertTrue(
				verifyObjectDisplayed(rentedPointValue)
						&& getElementText(rentedPointValue).trim().equals(CUIReservationBalancePage_Web.pointRented),
				"Rented Point Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Rented Point value present and is equal to the value rented on Reservation Balance Page");

		assertFalse(verifyObjectDisplayed(borrowedPointLabel), "Borrow Point present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Borrowed Point Section not  present");

		getElementInView(housekeepingCreditsLabel);
		assertTrue(verifyObjectDisplayed(housekeepingCreditsLabel), "Housekeeping Credits Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Housekeeping Credits  Label  present");
		assertTrue(verifyObjectDisplayed(housekeepingCreditsValue), "Housekeeping Credits Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Housekeeping Credits value present");

		assertTrue(verifyObjectDisplayed(reservationTransactionLabel), "Reservation Transaction Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Reservation Transaction Label  present");
		assertTrue(verifyObjectDisplayed(reservationTransactionValue), "Reservation Transaction Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForRentingPoint", Status.PASS,
				"Reservation Transaction value present");

	}

	/*
	 * Method: validateChargesSummaryForBorrowingPoint Description: Navigate to
	 * upcoming booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Kamalesh Changes By:
	 */
	public void validateChargesSummaryForBorrowingPoint() {
		getElementInView(chargerSummaryHeader);
		assertTrue(verifyObjectDisplayed(chargerSummaryHeader), "Charges summary header not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForBorrowingPoint", Status.PASS,
				"Charges summary header  present");
		assertTrue(verifyObjectDisplayed(membershipChargesHeader), "Membership charges header not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForBorrowingPoint", Status.PASS,
				"Membership charges header  present");

		assertTrue(verifyObjectDisplayed(totalPointLabel), "Total Point Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForBorrowingPoint", Status.PASS,
				"Total Point Label  present");
		assertTrue(verifyObjectDisplayed(totalPointValue), "Total Point Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForBorrowingPoint", Status.PASS,
				"Total Point value present");

		clickElementBy(totalPointLabel);
		getElementInView(availablePointLabel);
		assertTrue(verifyObjectDisplayed(availablePointLabel), "Available Point Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForBorrowingPoint", Status.PASS,
				"Available Point Label  present");
		assertTrue(verifyObjectDisplayed(availablePointValue), "Available Point Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForBorrowingPoint", Status.PASS,
				"Available Point value present");

		assertFalse(verifyObjectDisplayed(rentedPointLabel), "Rented Point Label present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Rented Point Label not present");

		assertTrue(verifyObjectDisplayed(borrowedPointLabel), "Borrow Point not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Borrowed Point Section present");
		assertTrue(verifyObjectDisplayed(borrowedPointValue)
				&& getElementText(borrowedPointValue).trim().equals(CUIReservationBalancePage_Web.pointBorrowed),
				"Borrow Point value not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Borrowed Point value present and is equal to the number of pointy borrowed on the Reservation Balance Page");

		getElementInView(housekeepingCreditsLabel);
		assertTrue(verifyObjectDisplayed(housekeepingCreditsLabel), "Housekeeping Credits Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Housekeeping Credits  Label  present");
		assertTrue(verifyObjectDisplayed(housekeepingCreditsValue), "Housekeeping Credits Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Housekeeping Credits value present");

		assertTrue(verifyObjectDisplayed(reservationTransactionLabel), "Reservation Transaction Label not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Reservation Transaction Label  present");
		assertTrue(verifyObjectDisplayed(reservationTransactionValue), "Reservation Transaction Value not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Reservation Transaction value present");

	}

	/*
	 * Method: validatePaymentChargesForPaypalPayment Description: Navigate to
	 * upcoming booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Kamalesh Changes By:
	 */
	public void validatePaymentChargesForPaypalPayment() {
		getElementInView(paymentChargesHeader);
		assertTrue(verifyObjectDisplayed(paymentChargesHeader), "Payment Charges Header not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Payment Charges Header present");

		assertTrue(
				verifyObjectDisplayed(paypalEmail)
						&& getElementText(paypalEmail).trim().equals(testData.get("PaypalEmail")),
				"Paypal payment details not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Paypal payment details present and is same as used for the payment");
	}

	/*
	 * Method: validatePaymentChargesForPaypalPayment Description: Navigate to
	 * upcoming booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Kamalesh Changes By:
	 */
	public void validateRentedPointUnderPaymentCharges() {
		getElementInView(rentedPointValueUnderPayment);
		assertTrue(verifyObjectDisplayed(rentedPointLabelUnderPayment),
				"Rented Label under payment charges not present");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Rented Label under payment charges  present");

		assertTrue(
				verifyObjectDisplayed(rentedPointValueUnderPayment) && getElementText(rentedPointValueUnderPayment)
						.trim().equals(CUIReservationBalancePage_Web.pointRentedPrice),
				"Rented value under payment charges not present ");
		tcConfig.updateTestReporter("CUIBookPage", "validateChargesSummaryForPaypalPayment", Status.PASS,
				"Rented value under payment charges present and is same as that on the Reservation Balance Page");
	}

	/*
	 * Method: validateSpecialRequestSection Description: Navigate to upcoming
	 * booking page from reservation confirmation Page Date field Date: June/2020
	 * Author: Kamalesh Changes By:
	 */
	public void validateSpecialRequestSection() {

		assertTrue(verifyObjectDisplayed(specialRequestHeader), "Special Request header not present");
		assertTrue(verifyObjectDisplayed(specialRequestValue), "Special Request value not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateConfirmationHeader", Status.PASS,
				"Special Request header and value present");
	}

	/*
	 * Method: validateReservationSummaryDetails Description: Navigate to upcoming
	 * booking page from reservation confirmation Page Date field Date: June/2020
	 * Author: Kamalesh Changes By:
	 */

	public void validateReservationSummaryDetails() {

		assertTrue(verifyObjectDisplayed(reservationSummaryHeader), "Reservation summary header not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Summary header and message present");

		assertTrue(verifyObjectDisplayed(reservationNumberHeader), "Reservation number header not present");
		assertTrue(getElementText(reservationNumber).length() > 0, "Reservation number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Reservation Number present : " + getElementText(reservationNumber));

		assertTrue(verifyObjectDisplayed(memberNumberHeader), "Member number header not present");
		assertTrue(getElementText(memberNumber).length() > 0, "Member number not present");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Member Number present : " + getElementText(memberNumber));

		assertTrue(verifyObjectDisplayed(travelerHeader), "Traveller header not present");
		assertTrue(getElementText(travellerName).equalsIgnoreCase(tcConfig.getTestData().get("GuestFullName")),
				"Traveller name doesn't match guest information");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Traveller Name matches guest information");

		assertTrue(verifyObjectDisplayed(emailHeader), "Email header not present");
		assertTrue(getElementText(email).equalsIgnoreCase(tcConfig.getTestData().get("guestEmail")),
				"Traveller email doesn't match guest information");
		tcConfig.updateTestReporter("CUIBookConfirmationPage_Web", "validateReservationSummary", Status.PASS,
				"Traveller Email matches guest information");

	}

	/*
	 * Method: validatePointProtectionUnderPaymentCharges Description: Navigate to
	 * upcoming booking page from reservation confirmation Page Date field Date:
	 * June/2020 Author: Kamalesh Changes By:
	 */
	public void validatePointProtectionUnderPaymentCharges(String flag) {
		switch (flag) {

		case "YES":
			assertTrue(verifyObjectDisplayed(pointProtectionLabelUnderPayment),
					"Point Protection Label under payment charges not present");
			tcConfig.updateTestReporter("CUIBookPage", "validatePointProtectionUnderPaymentCharges", Status.PASS,
					"Point Protection Label under payment charges  present");

			assertTrue(verifyObjectDisplayed(pointProtectionValueUnderPayment),
					"Point Protection value under payment charges not present ");
			tcConfig.updateTestReporter("CUIBookPage", "validatePointProtectionUnderPaymentCharges", Status.PASS,
					"Point Protection value under payment charges present");
			break;
		case "NO":
			assertFalse(verifyObjectDisplayed(pointProtectionLabelUnderPayment),
					"Point Protection present under payment charges section");
			tcConfig.updateTestReporter("CUIBookPage", "validatePointProtectionUnderPaymentCharges", Status.PASS,
					"Point Protection not present under payment charges section");
			break;

		}

	}
}
