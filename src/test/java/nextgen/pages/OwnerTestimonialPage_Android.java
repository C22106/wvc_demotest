package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class OwnerTestimonialPage_Android extends OwnerTestimonialPage_IOS {

	public static final Logger log = Logger.getLogger(OwnerTestimonialPage_Android.class);

	public OwnerTestimonialPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}