package nextgen.pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

//import com.wvo.salepoint.pages.LogInPage;

public class CUIResortSearchResultsPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUIResortSearchResultsPage_Web.class);

	public CUIResortSearchResultsPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	JavascriptExecutor js = (JavascriptExecutor) driver;

	protected CUIMyAccountPage_Web myaccountPage = new CUIMyAccountPage_Web(tcConfig);
	protected By textLocation = By.xpath("//input[@placeholder = 'Enter a location']");
	protected By feedbk_no = By.xpath("//a[@id = 'oo_no_thanks']");
	protected By srchHeader = By
			.xpath("//div[contains(@id,'availability')]/form/h2[text() = 'Find Your Next Vacation']");
	protected By textlocationfield = By.xpath("//span[text() = 'Search for a location or resort']");
	protected By textdatefield = By.xpath("//span[text() = 'Select Dates']");
	protected By filterheader = By.xpath("//p[text() = 'FILTER & SORT']");
	protected By numberofresort = By.xpath("//h2[contains(text(),'resorts found')]");
	protected By nextmonth = By.xpath("//div[@aria-label = 'Move forward to switch to the next month.']");
	protected By txtdatesel = By.xpath("//input[@id = 'searchDate']");
	protected By btndone = By.xpath("//button[text() ='Done']");
	protected By btnsrchavl = By.xpath("//button[text() ='Search Availability']");
	protected By searchresult = By.xpath("//section/div[@class='resort-card']/preceding-sibling::h3");
	protected By resortcard = By.xpath("//div[@class = 'resort-card']");
	protected By maptext = By.xpath("//h4[text() = 'SHOW MAP']");
	protected By clearlocationtext = By.xpath("//button[@class = 'input-clear-button']");
	protected By locationEnterAgain = By.xpath("//input[@placeholder = 'Enter a location']");
	protected By resortCard = By.xpath("//div[@class = 'resort-card']");
	protected By resortNames = By.xpath("//div[contains(@class,'resort-card__name')]//a");
	protected By resortCardAddress = By
			.xpath("//div[@class = 'resort-card']//p[contains(@class,'resort-card__address')]");
	protected By resortCardTags = By
			.xpath("//div[contains(@class,'resort-card__name')]//..//div[contains(@class,'resort-card__tags')]");
	protected By resortCardImages = By.xpath(
			"//div[contains(@class,'resort-card')]//figure[contains(@class,'resort-card__image')]/button[@aria-label='Add to Wishlist']");
	protected By ObjResortCardViewLabel = By
			.xpath("//div[contains(@class,'resort-card__content')]/following-sibling::div/a[contains(.,'Choose')]");
	protected By totalBookButton = By.xpath("//a[contains(text(),'Book')]");
	protected By textBookDetails = By.xpath("//span[text() = 'Reservation Details']");
	protected By exitBookingPopUp = By.xpath("//div[@class='title-3 text-black margin-bottom-1']");
	protected By exitBookingCTA = By.xpath("//button[contains(.,'Exit')]");
	protected By map = By.xpath("//div[@class = 'availability-map ']");
	protected By ownerFullName = By.xpath("//div[@class='h5 text-white margin-0' and contains(.,'WELCOME')]");
	protected By covidAlertClose = By.xpath("//button[contains(@class,'setCookie') and contains(.,'Close')]");

	/*
	 * Method: clickFirstAvailableUnit Description:Validate Hold Tight Loading
	 * Date field Date: Nov/2020 Author: Saket Sharma Changes By: NA
	 */
	public void clickFirstAvailableUnit() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, ObjResortCardViewLabel, 120);
		if (getList(ObjResortCardViewLabel).size() > 0) {
			WebElement viewclk = getList(ObjResortCardViewLabel).get(0);
			scrollDownForElementJSWb(viewclk);
			clickElementJSWithWait(viewclk);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("CUISearchPage", "clickAvailableUnit", Status.FAIL, "Resort Card not present");
		}

	}

	/*
	 * Method: clickBookButton Description:Validate Hold Tight Loading Date
	 * field Date: nov/2020 Author: Saket Sharma Changes By: NA
	 */
	public void clickBookButton() {
		String unitForBooking = testData.get("bookUnitType");
		if (getList(totalBookButton).size() > 0) {
			clickElementJSWithWait(getObject(By.xpath("//a[contains(text(),'" + unitForBooking
					+ "')]/ancestor::div[contains(@class,'resort-card__unit')]//a[contains(text(),'Book')]")));
			waitUntilElementVisibleBy(driver, textBookDetails, 120);
			if (verifyObjectDisplayed(textBookDetails)) {
				tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.PASS,
						"Successfully clicked on Book Button");
			} else {
				tcConfig.updateTestReporter("CUISearchPage", "clickBookButton", Status.FAIL,
						"Failed to click on Book Button");
			}
		}
	}

	public void validateExitBookingPopUp() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, exitBookingPopUp, 120);
		assertTrue(verifyObjectDisplayed(exitBookingPopUp), "Exit booking popup is not Displayed");
		tcConfig.updateTestReporter("CUISearchPage", "validateExitBookingPopUp", Status.PASS,
				"Exit booking popup is Displayed");
		clickElementJSWithWait(exitBookingCTA);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		assertTrue(verifyObjectDisplayed(map), "Failed to navigated to resort search page");
		tcConfig.updateTestReporter("CUISearchPage", "validateExitBookingPopUp", Status.PASS,
				"Successfully navigated to resort search page");
	}

	public void navigateBackToResultSearchPage() {
		driver.navigate().back();
	}

	public void closeAlert() {
		try {
			// WebElement covidClose = getList(covidAlertClose).get(0);
			waitUntilElementVisibleBy(driver, covidAlertClose, 20);
			if (verifyObjectDisplayed(covidAlertClose)) {
				clickElementBy(covidAlertClose);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("Club Wyndham Homepgage", "closeAlert", Status.PASS,
						"Successfully Closed Alert");
			}
		} catch (Exception e) {
			log.error("Covid Message Not Present");

		}

	}

	public void validateNumberOfResortCard() {
		// closeAlert();
		pageCheck();
		waitUntilElementVisible(driver, driver.findElement(resortCard), 120);
		List<WebElement> noresortcard = driver.findElements(resortCard);
		List<WebElement> noresortcardNames = driver.findElements(resortNames);
		List<WebElement> noresortcardAddress = driver.findElements(resortCardAddress);
		List<WebElement> noresortcardTags = driver.findElements(resortCardTags);
		List<WebElement> noresortcardImages = driver.findElements(resortCardImages);
		if (noresortcard.size() > 0) {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
					"Number of Displayed Resort is : " + noresortcard.size());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.FAIL,
					"Resort not found");
		}

		if (noresortcardNames.size() == noresortcard.size()) {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
					"Number of Resort Name is : " + noresortcardNames.size());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.FAIL,
					"Resort name is not Displayed");
		}

		if (noresortcardAddress.size() == noresortcard.size()) {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
					"Number of Resort Address is : " + noresortcardAddress.size());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.FAIL,
					"Resort Address is not displayed");
		}

		if (noresortcardTags.size() == noresortcard.size()) {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
					"Number of Resort Tags is : " + noresortcardTags.size());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.FAIL,
					"Resort Tags are not displayed");
		}

		if (noresortcardImages.size() == noresortcard.size()) {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
					"Number of Resort Images is : " + noresortcardImages.size());
		} else {
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.FAIL,
					"Resort Images is not displayed");
		}
	}

	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	protected By resortTitle = By.xpath("(//div[contains(@class,'title-1')])[1]");

	public void validateResortDetailsPage() throws InterruptedException {
		String strResortName = "";
		List<WebElement> noresortcard = driver.findElements(resortCard);

		for (int i = 0; i < noresortcard.size(); i++) {
			List<WebElement> noresortcardNames = driver.findElements(resortNames);
			strResortName = noresortcardNames.get(i).getText().trim();
			noresortcardNames.get(i).click();
			pageCheck();
			waitUntilElementVisible(driver, driver.findElement(homeBreadcrumb), 120);
			WebElement homeBreadcrumbdb = driver.findElement(homeBreadcrumb);
			getElementInView(homeBreadcrumbdb);
			String strResortTitle = driver.findElement(resortTitle).getText().trim();
			Assert.assertEquals(strResortTitle, strResortName);
			tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
					"Navigated to Resort Details Page " + strResortTitle);
			driver.navigate().back();
			pageCheck();
			myaccountPage.enterLocation();
			myaccountPage.selectCheckinDate();
			myaccountPage.selectCheckoutDate();
			myaccountPage.selectCalendarDone();
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
	}

	protected By viewMonthlyAvailability = By
			.xpath("//div[contains(@class,'small')]/a[contains(text(),'View Monthly Availability')]");

	public void navigateToResortPage() {
		List<WebElement> noresortcardNames = driver.findElements(resortNames);
		String strResortName = noresortcardNames.get(0).getText().split("›")[0].trim();
		noresortcardNames.get(0).click();
		waitUntilElementVisible(driver, driver.findElement(homeBreadcrumb), 120);

		getElementInView(homeBreadcrumb);
		String strResortTitle = driver.findElement(resortTitle).getText().trim();
		Assert.assertEquals(strResortTitle, strResortName);
		tcConfig.updateTestReporter("CUISearchPage", "validateNumberOfResortCard()", Status.PASS,
				"Navigated to Resort Details Page " + strResortTitle);

		assertTrue(verifyObjectDisplayed(viewMonthlyAvailability), "View Monthly Availability Button is not Displayed");
		tcConfig.updateTestReporter("CUISearchPage", "navigateToResortPage()", Status.PASS,
				"View Monthly Availability button is Displayed");
	}

}
