package nextgen.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUIBookingUpgradePage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUIBookingUpgradePage_Web.class);

	public CUIBookingUpgradePage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	CUIBookPage_Web bookPage = new CUIBookPage_Web(tcConfig);
	protected By headerVIPUpgrades = By
			.xpath("//h1[contains(.,'VIP Upgrades')] | //div[@class = 'steps__title' and contains(.,'VIP Upgrades')]");
	protected By selectUpgrade = By.xpath("//label[contains(@for,'upgradeItem')]");
	protected By upMsgTooltip = By.xpath("//span[@data-tooltip-class='tooltip']");
	protected By headerTravelerInfo = By.xpath(
			"//h1[contains(.,'Traveler Info')] | //div[@class = 'steps__title' and contains(.,'Traveler Info')]");
	protected By bookingHeader = By.xpath("//div[@class='booking-header__desktop']//li[@class='step -active']/span");
	protected By upgradeInfoMsg = By.xpath(
			"//p[contains(.,'Opt-in to automatically be upgraded if a') and contains(.,'better suite') and contains(.,'becomes available prior to your check-in date. Select an option below:')]");
	protected By upgradeNowMsg = By.xpath("//h2[text()='Upgrade Now']");
	protected By optInFutureUpgrades = By.xpath("//h2[text()='Opt-In to Future Upgrades']");
	protected By vipSuiteUpgMsg = By
			.xpath("//p[text()='Take advantage of your VIP suite upgrade by selecting an option below:']");
	protected By optInRadio = By.xpath(
			"//div/label[@for='optIn' and text()='Upgrade me if the next desirable suite becomes available']/preceding-sibling::input[@id='optIn' and @type='radio']");
	protected By upgradeMeRadioBtn = By.xpath(
			"//div/label[@for='optIn' and text()='Upgrade me if the next desirable suite becomes available']/preceding-sibling::input[@id='optIn' and @type='radio']");

	protected By keepUnitRadio = By.xpath(
			"//div/label[@for='keepUnit' and text()='Keep My Selected Suite']/preceding-sibling::input[@id='keepUnit' and @type='radio']");
	protected By buttonContinue = By.xpath("//div[contains(@class,'booking')]//button[text() = 'Continue']");
	protected By upgradeNowHeader = By.xpath("//h2[text()='Upgrade Now']");
	protected By optInFutureUpgradesHeader = By.xpath("//h2[contains(text(),'Opt-In to Future Upgrades')]");
	protected By betterSuiteInstructionTxt = By
			.xpath("//p[contains(text(),'Opt-in') and contains(.,'becomes available prior')]");
	protected By betterSuiteInstructionTxt_InstantUpgrade = By
			.xpath("//p[contains(text(),'Select an available') and contains(.,'for an immediate upgrade.')]");

	protected By betterSuiteToolTip = By.id("optInHeader");
	protected By betterSuiteToolTipTxt = By.xpath("//div[@role='tooltip'and contains(text(),'Better suites')]");
	protected By largerSuiteRadioBtn = By.xpath("//span[text()='Larger Suite']/ancestor::label");
	private By tooltipBetterSuite = By.xpath("//span[@data-tooltip-class='tooltip']");
	private By tooltipMsgBetterSuite = By.xpath(
			"//div[@role='tooltip' and @aria-hidden='false' and contains(text(),'Better suites will have features that are preferable to your current selection-')]");
	protected By unitType = By.xpath("//input[@name='chosenUpgrade']/parent::div/label");
	protected By viewSuiteDetailsLink = By.xpath("//a[@class='link-caret']");
	protected By unitDetailsPopUp = By.id("UnitDetailsLabel");
	protected By closePopUp = By.xpath("//button[@class='close-button']");
	protected By keepMySelectedSuiteTitle = By.xpath("//h3[text()='Keep My Selected Suite']");
	protected By firstUpgradeOption = By.xpath("//label[@for='upgradeItem-0-0']");
	protected By mySelectedUpgradeOption = By.xpath("//label[@for='keep_unit']");
	protected By skipUpgrade = By.xpath("//label[text()='I do not want to opt-in to future upgrades at this time']");
	protected By optInOption = By.xpath(
			"//h3[contains(@class,'subtitle') and contains(text(),'Select the option')]//..//label//span[@class='upgrade-option__text']");

	protected By labelModifyUpgradeSelection = By.xpath("//div[@class = 'steps__title' and contains(.,'Modify Upgrade Selection')]");
	protected By labelCurrentVIPPreference = By.xpath("//div[contains(@class,'steps') and contains(.,'CURRENT VIP UPGRADE PREFERENCE')]");
	protected By currentVIPPreferenceValue = By.xpath("//div[contains(@class,'steps') and contains(.,'CURRENT VIP UPGRADE PREFERENCE')]/../p/span[contains(text(),'VIP Upgrade Skipped')]");
	protected By labelFutureUpgradeOption = By.xpath("//div[contains(@class,'steps') and contains(.,'Future Upgrade Options')]");
	protected By upgradeMeModifyRadioBtn = By.xpath("//div/label[@for='optIn' and text()='Automatically upgrade my suite if a larger suite becomes available prior to my check-in date.']/preceding-sibling::input[@id='optIn' and @type='radio']");
	protected By currentVIPUpgradeOption = By.xpath("//div[contains(@class,'steps') and contains(.,'CURRENT VIP UPGRADE PREFERENCE')]/../p/span");
	protected By btnExitBooking = By.xpath("//button[contains(text(),'Exit Booking')]");
	protected By map = By.xpath("//div[@class = 'availability-map ']");
	
	/*
	 * Method: verifyAndOptForFutureUpgradeMultipleOption Description:Verify and
	 * opt for Future Upgrade Date field Date: Dec/2020 Author: Priya Changes
	 * By: NA
	 */
	
	
	public void verifyAndOptForFutureUpgradeMultipleOption() {

		assertTrue(verifyObjectDisplayed(vipSuiteUpgMsg),
				"Take advantage of your VIP suite upgrade by selecting an option below: - Message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"VIP Upgrade message present");
		 

		assertTrue(verifyObjectDisplayed(optInFutureUpgradesHeader), "Future Upgrade message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Future VIP Upgrade message present");

		assertTrue(verifyObjectDisplayed(upgradeInfoMsg),
				"Upgrade info message not present - Opt-in to automatically be upgraded..");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Upgrade info message present");

		assertTrue(getObject(upgradeMeRadioBtn).isEnabled(), "Opt in radio button with proper verbiage not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Opt in radio button with proper verbiage present");

		assertTrue(getObject(keepUnitRadio).isEnabled(), "Keep unit radio button with proper verbiage not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Keep unit radio button with proper verbiage present");

		clickElementBy(upgradeMeRadioBtn);

		assertTrue(getObject(upgradeMeRadioBtn).isSelected(), "Opt in radio button not selected");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Opt in radio button with proper verbiage present");

		assertTrue(verifyObjectDisplayed(selectOptionHeader),
				"If you choose to upgrade, your new suite may not be Presidential Reserve.");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Presidential Disclaimer Text message present");

		List<WebElement> allOptions = getList(optInOption);
		for (WebElement we : allOptions) {
			getElementInView(we);
			assertTrue(verifyObjectDisplayed(we), "Option for Upgrade is Displayed");
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
					" Option for Upgrade is Displayed as: " + getElementText(we));
		}

	}
	
	/*
	 * Method: modifyForFutureUpgradeDisclaimer Description:Verify the Future Upgrade Disclaimer Text
	 * field Date: Dec/2020 Author: Priya Changes By: NA
	 */
	
	public void modifyForFutureUpgradeDisclaimer() {

		assertTrue(verifyObjectDisplayed(labelModifyUpgradeSelection),
				"Modify Upgrade Selection: - Header not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Modify Upgrade Selection header present");
		 

		assertTrue(verifyObjectDisplayed(labelCurrentVIPPreference), "Current VIP Preferences label not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Current VIP Preferences label present");

		assertTrue(verifyObjectDisplayed(currentVIPPreferenceValue),
				"Current VIP Upgrade option value is not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Current VIP Upgrade preference value is present" +getElementText(currentVIPPreferenceValue));
		
		assertTrue(verifyObjectDisplayed(labelFutureUpgradeOption),
				"Future Upgrade Option is not Present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Future Upgrade Option is Present");
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(selectOptionHeader),
				"If you choose to upgrade, your new suite may not be Presidential Reserve.");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Presidential Disclaimer Text message present");

	}
	
	/*
	 * Method: modifyForFutureUpgradeMultipleOption Description:To verify Multiply VIP Upgrade option
	 * field Date: Dec/2020 Author: Priya Changes By: NA
	 */
	
	public void modifyForFutureUpgradeMultipleOption() {

		assertTrue(verifyObjectDisplayed(labelModifyUpgradeSelection),
				"Modify Upgrade Selection: - Header not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Modify Upgrade Selection header present");
		 

		assertTrue(verifyObjectDisplayed(labelCurrentVIPPreference), "Current VIP Preferences label not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Current VIP Preferences label present");

		assertTrue(verifyObjectDisplayed(currentVIPUpgradeOption),
				"Current VIP Upgrade value is not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Current VIP Upgrade preference value is present: "+getElementText(currentVIPUpgradeOption));
		
		assertTrue(verifyObjectDisplayed(labelFutureUpgradeOption),
				"Future Upgrade Option is not Present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Future Upgrade Option is Present");
		
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(selectOptionHeader),
				"If you choose to upgrade, your new suite may not be Presidential Reserve.");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "modifyForFutureUpgradeDisclaimer", Status.PASS,
				"Presidential Disclaimer Text message present");
		
		List<WebElement> allOptions = getList(optInOption);
		for (WebElement we : allOptions) {
			getElementInView(we);
			assertTrue(verifyObjectDisplayed(we), "Option for Upgrade is Displayed");
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
					" Option for Upgrade is Displayed as: " + getElementText(we));
		}

	}
	
	/*
	 * Method: optionInstantUpgrade Description:Choose Instant Upgrade Option Date
	 * field Date: June/2020 Author: Abhijeet Changes By: NA
	 */
	public void optionInstantUpgrade() {
		String upgradeRequired = testData.get("UpgradeRequired");
		waitUntilElementVisibleBy(driver, headerVIPUpgrades, 120);
		Assert.assertTrue(verifyObjectDisplayed(headerVIPUpgrades));
		if (upgradeRequired.equalsIgnoreCase("YES")) {
			String upgradeTo = testData.get("UpgradeTo");
			clickElementJSWithWait(
					By.xpath("//label[contains(@for,'upgradeItem') and contains(text(),'" + upgradeTo + "')]"));
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "optionInstantUpgrade", Status.PASS,
					"Upgraded to: " + upgradeTo);
			bookPage.clickContinueButtonForBooking();
		} else {
			bookPage.clickContinueButtonForBooking();
		}
	}

	/*
	 * Method: verifyFirstStep Description:Veriying the first step of the booking
	 * header Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void verifyFirstStep() {
		waitUntilElementVisibleBy(driver, headerVIPUpgrades, 120);
		Assert.assertTrue(verifyObjectDisplayed(headerVIPUpgrades), "VIP upgrade step not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyFirstStep", Status.PASS,
				"VIP Upgrade step present");

	}

	/*
	 * Method: optForFutureUpgrade Description:Verify and opt for Future Upgrade
	 * Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void verifyAndDeclineFutureUpgrade() {

		assertTrue(verifyObjectDisplayed(vipSuiteUpgMsg),
				"Take advantage of your VIP suite upgrade by selecting an option below: - Message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"VIP Upgrade message present");

		assertFalse(verifyObjectDisplayed(upgradeNowMsg), "Upgrade Now - Message present for future upgrade scenario");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"Upgrade Now message not present");

		assertTrue(verifyObjectDisplayed(optInFutureUpgradesHeader), "Future Upgrade message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"Future VIP Upgrade message present");

		assertTrue(verifyObjectDisplayed(upgradeInfoMsg),
				"Upgrade info message not present - Opt-in to automatically be upgraded..");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"Upgrade info message present");

		hoverOnElement(tooltipBetterSuite);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(tooltipMsgBetterSuite), "Tool tip message for better suite not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"Tool tip message for better suite present");

		assertTrue(getObject(optInRadio).isEnabled(), "Opt in radio button with proper verbiage not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"Opt in radio button with proper verbiage present");

		assertTrue(getObject(keepUnitRadio).isEnabled(), "Keep unit radio button with proper verbiage not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"Keep unit radio button with proper verbiage present");

		clickElementBy(keepUnitRadio);

		assertTrue(getObject(keepUnitRadio).isSelected(), "Keep unit radio button not selected");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndDeclineFutureUpgrade", Status.PASS,
				"Keep unit radio button with proper verbiage present");

	}

	/*
	 * Method: verifyFirstStepOnPageLoad Description:Veriying the first step of the
	 * booking header Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyFirstStepOnPageLoad() {
		String userType = testData.get("User_Type");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String headerValue = getElementText(bookingHeader);
		if (userType.equalsIgnoreCase("VIP")) {
			Assert.assertTrue(verifyObjectDisplayed(headerVIPUpgrades));
			if (headerValue.contains("VIP UPGRADES") && getElementText(headerVIPUpgrades).contains("1. VIP UPGRADES")) {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.PASS,
						"The First step of the booking header for VIP User type is " + headerValue
								+ " and the page header is " + getElementText(headerVIPUpgrades));
			} else {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.FAIL,
						"The First step for VIP User type is " + headerValue);
			}
		} else if (userType.equalsIgnoreCase("Non-VIP")) {
			Assert.assertTrue(verifyObjectDisplayed(headerTravelerInfo));
			if (headerValue.contains("TRAVELER INFO")) {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.PASS,
						"The First step for Non VIP User type is " + headerValue);
			} else {
				tcConfig.updateTestReporter("CUIVIPUpgradePage", "verifyFirstStep", Status.FAIL,
						"The First step for Non VIP User type is " + headerValue);
			}
		}
	}

	/*
	 * Method: optForUpgrade Description:Choose the available upgrade Date field
	 * Date: June/2020 Author: Monideep Changes By: NA
	 */
	public void optForUpgrade() {
		getElementInView(selectUpgrade);
		clickElementJSWithWait(selectUpgrade);
		tcConfig.updateTestReporter("CUIBooCUIBookingUpgradePagekPage", "optionInstantUpgrade", Status.PASS,
				"Upgrade option selected successfully");

	}

	/*
	 * Method: optForFutureUpgrade Description:Verify and opt for Future Upgrade
	 * Date field Date: June/2020 Author: Monideep Changes By: NA
	 */
	protected By selectOptionHeader = By
			.xpath("//h3[contains(@class,'subtitle') and contains(text(),'Select the option')]");

	public void verifyAndOptForFutureUpgrade() {

		assertTrue(verifyObjectDisplayed(vipSuiteUpgMsg),
				"Take advantage of your VIP suite upgrade by selecting an option below: - Message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"VIP Upgrade message present");

		/*
		 * assertFalse(verifyObjectDisplayed(upgradeNowMsg),
		 * "Upgrade Now - Message present for future upgrade scenario");
		 * tcConfig.updateTestReporter("CUIBookingUpgradePage",
		 * "verifyAndOptForFutureUpgrade", Status.PASS,
		 * "Upgrade Now message not present");
		 */

		assertTrue(verifyObjectDisplayed(optInFutureUpgradesHeader), "Future Upgrade message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Future VIP Upgrade message present");

		assertTrue(verifyObjectDisplayed(upgradeInfoMsg),
				"Upgrade info message not present - Opt-in to automatically be upgraded..");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Upgrade info message present");

		assertTrue(getObject(optInRadio).isEnabled(), "Opt in radio button with proper verbiage not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Opt in radio button with proper verbiage present");

		assertTrue(getObject(keepUnitRadio).isEnabled(), "Keep unit radio button with proper verbiage not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Keep unit radio button with proper verbiage present");

		clickElementBy(optInRadio);

		assertTrue(getObject(optInRadio).isSelected(), "Opt in radio button not selected");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Opt in radio button with proper verbiage present");

		assertTrue(verifyObjectDisplayed(selectOptionHeader),
				"Select the option that's most important to you in an upgraded suite:");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyAndOptForFutureUpgrade", Status.PASS,
				"Select info message present");

		List<WebElement> allOptions = getList(optInOption);
		for (WebElement we : allOptions) {
			getElementInView(we);
			assertTrue(verifyObjectDisplayed(we), "Option for Upgrade is Displayed");
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
					" Option for Upgrade is Displayed as: " + getElementText(we));
		}

	}

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button Date
	 * field Date: June/2020 Author: Abhijeet Changes By: Kamalesh Gupta
	 */

	public void clickContinueButton() {
		clickContinueBtn(buttonContinue, "CUIBookingUpgradePage");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		Assert.assertTrue(verifyObjectDisplayed(headerTravelerInfo));
		tcConfig.updateTestReporter("clickContinueBooking", "clickContinueButton", Status.PASS,
				"Successfully navigated to Next Page");

	}

	/*
	 * Method: clickContinueButtonForBooking Description:Click Continue Button
	 * 
	 */
	public void clickContinueBtn(By by, String pageName) {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		getElementInView(by);
		if (verifyObjectDisplayed(by) && verifyObjectEnabled(by)) {
			clickElementJSWithWait(by);
			tcConfig.updateTestReporter(pageName, "clickContinueButton", Status.PASS,
					"Continue Button is enabled and displayed successfully and it is clicked");
		} else {
			tcConfig.updateTestReporter(pageName, "clickContinueButton", Status.FAIL,
					"Continue Button is not dispalyed");
		}
	}

	/*
	 * Method: verifyBetterSuiteInstructionTxt Description:Click Continue Button
	 * Date field Date: June/2020 Author: Kamalesh Gupta Changes By:
	 */
	public void verifyBetterSuiteInstructionTxt() {
		assertTrue(verifyObjectDisplayed(betterSuiteInstructionTxt),
				"Better Suite Instruction Txt : Message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyBetterSuiteInstructionTxt", Status.PASS,
				"Better Suite Instruction text is displayed successfully - "
						+ getElementText(betterSuiteInstructionTxt));
	}

	/*
	 * Method: verifyUpgradeMeRadioBtnPresence Description:Click Continue Button
	 * Date field Date: June/2020 Author: Kamalesh Gupta Changes By:
	 */

	public void verifyUpgradeMeRadioBtnPresence() {
		waitUntilElementVisibleBy(driver, headerVIPUpgrades, 120);
		assertTrue(verifyObjectDisplayed(upgradeMeRadioBtn),
				getElementText(upgradeMeRadioBtn) + " Radio Button is not displayed");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyUpgradeMeRadioBtnPresence", Status.PASS,
				getElementText(upgradeMeRadioBtn) + " Radio Button is displayed successfully");
	}

	/*
	 * Method: verifyUpgradeMeRadioBtnPresence Description:Click Continue Button
	 * Date field Date: June/2020 Author: Kamalesh Gupta Changes By:
	 */

	public void verifyKeepMySelectedSuiteRadioBtnPresence() {
		waitUntilElementVisibleBy(driver, headerVIPUpgrades, 120);
		assertTrue(verifyObjectDisplayed(keepUnitRadio),
				getElementText(keepUnitRadio) + " Radio Button is not displayed");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyKeepMySelectedSuiteRadioBtnPresence", Status.PASS,
				getElementText(keepUnitRadio) + " Radio Button is displayed successfully");
	}

	/*
	 * Method: verifyContinueBtnDisableOnPageLoad Description:Click Continue Button
	 * Date field Date: June/2020 Author: Kamalesh Gupta Changes By:
	 */
	public void verifyContinueBtnDisableOnPageLoad() {
		assertFalse(verifyObjectDisplayed(buttonContinue), "Continue Button is enabled");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyContinueBtnDisableOnPageLoad", Status.PASS,
				"Continue Button is disabled on page load");
	}

	/*
	 * Method: optForFutureUpgrade Description:Verify and opt for Future Upgrade
	 * Date field Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifyInstantUpgrade() {
		assertTrue(verifyObjectDisplayed(vipSuiteUpgMsg),
				"Take advantage of your VIP suite upgrade by selecting an option below: - Message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
				"Vip Suite Upgrade Message present as :" + getElementText(vipSuiteUpgMsg));

		/*
		 * assertTrue(verifyObjectDisplayed(upgradeNowMsg),
		 * "Upgrade Now - Message not present");
		 * tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade",
		 * Status.PASS, "Upgrade Now message present");
		 */

		assertTrue(verifyObjectDisplayed(betterSuiteInstructionTxt_InstantUpgrade),
				"Better Suite Instruction Txt : Message not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
				"Better Suite Instruction text is displayed successfully - "
						+ getElementText(betterSuiteInstructionTxt_InstantUpgrade));

		hoverOnElement(tooltipBetterSuite);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		assertTrue(verifyObjectDisplayed(tooltipMsgBetterSuite), "Tool tip message for better suite not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
				"Tool tip message for better suite present");

		assertTrue(verifyObjectDisplayed(keepMySelectedSuiteTitle), "Keep My Selected Suite Title not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
				"Keep My Selected Suite Title present");

		List<WebElement> allOptions = getList(unitType);
		for (WebElement we : allOptions) {
			getElementInView(we);
			assertTrue(verifyObjectDisplayed(we), "Unit Type not displayed");
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
					" Unit Type is displayed as: " + getElementText(we));
		}

		List<WebElement> allLinks = getList(viewSuiteDetailsLink);
		for (WebElement we : allLinks) {
			getElementInView(we);
			assertTrue(verifyObjectDisplayed(we), "View Suite Details link not displayed");
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
					" View Suite Details link displayed");

			we.click();
			waitUntilElementVisibleBy(driver, unitDetailsPopUp, 120);
			assertTrue(verifyObjectDisplayed(unitDetailsPopUp), "Unit Details Pop Up Model not displayed");
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
					" Unit Details Pop Up Model  displayed");

			assertTrue(verifyObjectDisplayed(getList(closePopUp).get(getList(closePopUp).size() - 1)),
					"Close Button on the Pop Up Model not displayed");
			tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
					" Close Button on the Pop Up Model is displayed");
			getList(closePopUp).get(getList(closePopUp).size() - 1).click();
		}

		getElementInView(firstUpgradeOption);
		assertTrue(verifyObjectDisplayed(firstUpgradeOption), "First Upgrade Option Not present");
		clickElementBy(firstUpgradeOption);
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
				"First Upgrade Option Clicked");

		getElementInView(mySelectedUpgradeOption);
		assertTrue(verifyObjectDisplayed(mySelectedUpgradeOption), "My Selected Upgrade Option Not present");
		clickElementBy(mySelectedUpgradeOption);
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
				"My Selected Upgrade Option Clicked");

	}

	/*
	 * Method: verifySkipUpgradeOption Description:Verify Skip Upgrade Date field
	 * Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void verifySkipUpgradeOption() {
		getElementInView(optInRadio);
		assertTrue(verifyObjectDisplayed(optInRadio),
				"Upgrade me if the next desirable larger suite becomes available - Radio button not present");
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifySkipUpgradeOption", Status.PASS,
				"Upgrade me if the next desirable larger suite becomes available- Radio button Present");

		assertTrue(verifyObjectDisplayed(skipUpgrade),
				"I do not want to opt-in to future upgrades at this time - Radio button not present");
		clickElementBy(skipUpgrade);
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifySkipUpgradeOption", Status.PASS,
				"I do not want to opt-in to future upgrades at this time- Radio button Present and it is selected");

	}

	/*
	 * Method: selectFirstUpgradeOption Description:Verify Skip Upgrade Date field
	 * Date: June/2020 Author: Kamalesh Changes By: NA
	 */
	public void selectFirstUpgradeOption() {
		getElementInView(firstUpgradeOption);
		assertTrue(verifyObjectDisplayed(firstUpgradeOption), "First Upgrade Option Not present");
		clickElementBy(firstUpgradeOption);
		tcConfig.updateTestReporter("CUIBookingUpgradePage", "verifyInstantUpgrade", Status.PASS,
				"First Upgrade Option Clicked");
	}

}