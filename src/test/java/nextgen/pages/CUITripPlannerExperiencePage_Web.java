package nextgen.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class CUITripPlannerExperiencePage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUITripPlannerExperiencePage_Web.class);

	public CUITripPlannerExperiencePage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	protected By labelPlanningToStay = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'What do you want to do on your vacation?')]");
	protected By checkboxRelax = By.xpath("//label[contains(.,'Relax & Play By The Water')]");
	protected By checkboxBreathe = By.xpath("//label[contains(.,'Breathe In Mountain Air')]");
	protected By checkboxExplore = By.xpath("//label[contains(.,'Experience The City')]");
	protected By checkboxExperience = By.xpath("//label[contains(.,'Explore The Attractions')]");
	protected By checkboxHitTheLinks = By.xpath("//label[contains(.,'Hit The Links')]");
	protected By checkboxAnything = By.xpath("//label[contains(.,'Anything')]");
	protected By buttonNext = By.xpath("(//button[contains(@class,'button expanded') and contains(.,'View Results')])[1] | (//button[contains(@class,'button expanded') and contains(.,'View Results')])[2]");
	protected By buttonGoBack = By.xpath("(//button[contains(@class,'prev-link-caret') and contains(.,'Go Back')])");
	protected By labelPageHeader = By.xpath("//div[contains(@class,'title-3 medium') and contains(.,'Your Customized Resort List')]");
	protected By labelEditSelections = By.xpath("//span[@class='margin-right-1' and contains(.,'Edit')]");
	protected By labelBudget = By.xpath("//span[@class='caption-3' and contains(.,'Stay')]");
	//WebElement clkexp = getObject(By.xpath("//label[@for and contains(.,'ss')]"));
			
	/*
	 * Method: selectExperience Description:Select Experience Validate Date:
	 * Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void selectExperiences() {
		Assert.assertFalse(getObject(buttonNext).isEnabled(),"Nextbutton is Enabled");
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		int exp = 0;
		String Experiences = testData.get("Experiences").trim();
		String exparr[] = Experiences.split(";");
		for (int experienceIterator = 0; experienceIterator < exparr.length; experienceIterator++) {
			WebElement clkexp = getObject(
					By.xpath("//label[@for and contains(.,'" + exparr[experienceIterator] + "')]"));
			
			getElementInView(clkexp);
			clickElementJSWithWait(clkexp);
			Assert.assertTrue(getObject(buttonNext).isEnabled(),"Nextbutton is disabled");
			tcConfig.updateTestReporter("CUITripPlannerExperiencePage", "selectExperience", Status.PASS,
					"Clicked on Experiences and selected experience is: " + exparr[experienceIterator]);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			exp = exp + 1;

		}
		
	}
	
	/*
	 * Method: selectExperience Description:Select Experience Validate Date:
	 * Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void selectExperience() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String experience = testData.get("Experiences").trim();
		if (experience.equals("Relax & Play By The Water")) {
			getElementInView(checkboxRelax);
			clickElementJSWithWait(checkboxRelax);
		} else if (experience.equals("Breathe In Mountain Air")) {
			getElementInView(checkboxBreathe);
			clickElementJSWithWait(checkboxBreathe);
		} else if (experience.equals("Experience The City")) {
			getElementInView(checkboxExperience);
			clickElementJSWithWait(checkboxExperience);
		} else if (experience.equals("Explore The Attractions")) {
			getElementInView(checkboxExplore);
			clickElementJSWithWait(checkboxExplore);
		} else if (experience.equals("Hit The Links")) {
			getElementInView(checkboxHitTheLinks);
			clickElementJSWithWait(checkboxHitTheLinks);
		} else if (experience.equals("Anything")) {
			getElementInView(checkboxAnything);
			clickElementJSWithWait(checkboxAnything);
		} else {
			tcConfig.updateTestReporter("CUITripPlannerExperiencePage", "selectSort", Status.FAIL,
					"Failed to click on filter! Check Data");
		}

	}
	
	/*
	 * Method: selectSecondExperience Description:Select Second Experience Validate Date:
	 * Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void selectSecondExperience() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		String experienceSecond = testData.get("SecondExperiences").trim();
		if (experienceSecond.equals("Relax & Play By The Water")) {
			getElementInView(checkboxRelax);
			clickElementJSWithWait(checkboxRelax);
		} else if (experienceSecond.equals("Breathe In Mountain Air")) {
			getElementInView(checkboxBreathe);
			clickElementJSWithWait(checkboxBreathe);
		} else if (experienceSecond.equals("Experience The City")) {
			getElementInView(checkboxExperience);
			clickElementJSWithWait(checkboxExperience);
		} else if (experienceSecond.equals("Explore The Attractions")) {
			getElementInView(checkboxExplore);
			clickElementJSWithWait(checkboxExplore);
		} else if (experienceSecond.equals("Hit The Links")) {
			getElementInView(checkboxHitTheLinks);
			clickElementJSWithWait(checkboxHitTheLinks);
		} else if (experienceSecond.equals("Anything")) {
			getElementInView(checkboxAnything);
			clickElementJSWithWait(checkboxAnything);
		} else {
			tcConfig.updateTestReporter("CUITripPlannerExperiencePage", "selectSecondExperience", Status.PASS,
					"Second Experince not present");
			Assert.assertTrue(getObject(buttonNext).isEnabled(),"Nextbutton is not Enabled");
		}

	}
	
	/*
	 * Method: clickContinueCTA Description: click Continue CTA
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void clickContinueCTA() {
		clickElementJSWithWait(buttonNext);
		waitUntilElementVisibleBy(driver, labelBudget, 120);
		tcConfig.updateTestReporter("CUITripPlannerExperiencePage", "clickContinueCTA", Status.PASS,
				"Successfully clicked next button");
	}

}
