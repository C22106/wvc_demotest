package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenFeaturedDestinations_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenFeaturedDestinations_Web.class);

	public NextGenFeaturedDestinations_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By featuredDestinationHdr = By.xpath(/*
													 * "//div[@class='wyn-headline ']/p[contains(.,'Featured Destination')]"
													 */
			"(//div[contains(text(),'Featured Destinations')])[1]");
	protected By featDestSubHdr = By
			.xpath("(//div[contains(text(),'Featured Destinations')])[1]/../div[@class='title-1']");
	protected By featDestDesc = By.xpath(
			/*
			 * "//div[@class='wyn-headline ']/p[contains(.,'Featured Destination')]/ancestor::div[@class='wyn-card__content']//div[2]/p"
			 */
			"(//div[contains(text(),'Featured Destinations')])[1]/../div[@class='title-1']//..//div[@class='body-1']/p");
	protected By featDestImage = By.xpath(
			/*
			 * "//div[@class='wyn-headline ']/p[contains(.,'Featured Destination')]/ancestor::div[@class='slick-track']//img"
			 */
			"(//div[contains(text(),'Featured Destinations')])[1]/ancestor::div[@class='cardBanner']//img");
	protected By featDestBrdCrm = By.xpath(/* "//nav[@class='wyn-breadcrumbs']//a[contains(.,'Featured')]" */
			"//div[@class='breadcrumb']//ul//li/a[text()='Featured Destinations']");

	protected By destGuidesHdr = By.xpath(/* "//h2[contains(.,'Destination Guides')]" */
			"//div[contains(text(),'Destination Guides')]");
	protected By caraouselVal = By.xpath("//div[@class='wyn-carousel__container']");
	protected By destGuideArtImages = By.xpath(
			"//h2[contains(.,'Destination Guide')]/ancestor::div[@class='cards']//div[@class='slick-slide slick-active']//img");
	protected By destGuideArtHeader = By.xpath(
			"//h2[contains(.,'Destination Guide')]/ancestor::div[@class='cards']//div[@class='slick-slide slick-active']//h3");
	protected By destGuideArtDesc = By.xpath(
			"//h2[contains(.,'Destination Guide')]/ancestor::div[@class='cards']//div[@class='slick-slide slick-active']//p");
	protected By destGuideArtReadMore = By.xpath(
			"//h2[contains(.,'Destination Guide')]/ancestor::div[@class='cards']//div[@class='slick-slide slick-active']//div[@class='wyn-button-cta']");
	protected By nextArrow = By.xpath("//button[@class='slick-next slick-arrow']");

	protected By slickDotsActive = By.xpath("//ul[@class='slick-dots']//li"); // class
																				// contains
																				// (active)

	protected By bannerCarHdr = By.xpath(/*"//div[@class='banner-carousel']//div/p"*/
			/*"//div[contains(text(),'Keep calm')]");*/
	/*"(//div[@class='card-section']/div[contains(text(),'Explore New York City')])[1]"*/
			"(//div[contains(text(),'READ YOUR WINTER MAGAZINE')])[1]");
	protected By bannerCaroslSubHdr = By.xpath(/*"//div[@class='banner-carousel']//h2"*/
			/*"//div[contains(text(),'Keep calm')]/../div[@class='title-1']"*/
			/*"(//div[@class='card-section']/div[@class='subtitle-3 bold']/p/b[contains(text(),'Get busy')])[1]"*/
			"(//div[contains(text(),'READ YOUR WINTER MAGAZINE')])[1]/../div[contains(@class,'body-1')]");
	protected By bannerCarouselReadMore = By.xpath(/*"//div[@class='banner-carousel']//a[contains(.,'Read')]"*/
			/*"//div[contains(text(),'Keep calm')]/../a[text()='Read More']"*/
			/*"(//div[@class='card-section']/div[@class='link-caret-dynamic body-1-link'])[6]"*/
			"(//div[contains(text(),'READ YOUR WINTER MAGAZINE')])[1]/..//a"); 
	// Speakeasies
	// protected By speakEasiesBrdCrm =
	// By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Speakeasies')]");
	protected By speakEasiesBrdCrm = By.xpath(/*"//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel Inspiration')]"*/
			"//div[@class='breadcrumb']//a[contains(.,'New')]");
	protected By travelInspHdr = By.xpath(/*"//h2[contains(.,'Travel Inspiration')]"*/
			"//div[contains(text(),'Travel Inspiration')]");
	protected By travelInspirationBread = By
			.xpath(/*"//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel Inspiration')]"*/
					"//div[@class='breadcrumb']//a[contains(.,'Travel Inspiration')]");
	protected By travelInspSeeMore = By
			.xpath(/*"//h2[contains(.,'Travel')]/ancestor::div[@class='cards']//a[contains(.,'See more travel')]"*/
					"((//div[contains(text(),'Travel Inspiration')]/ancestor::div[contains(@class,'aem-GridColumn')]))[2]//a[contains(text(),'See More')]");
	protected By travelInspDesc = By
			.xpath(/*"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='cards']//div[1]/p"*/
					"(//div[contains(text(),'Travel Inspiration')]//following::div[contains(@class,'aem-GridColumn')])[1]//p");
	protected By travelInspArticleImages = By
			.xpath(/*"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//img"*/
					"(//div[contains(text(),'Travel Inspiration')]//following::div[contains(@class,'aem-GridColumn')])[1]//img");
	protected By travelInspArticleHdr = By
			.xpath(/*"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//h3"*/
					"(//div[contains(text(),'Travel Inspiration')]//following::div[contains(@class,'aem-GridColumn')])[1]//div[contains(@class,'subtitle-2')]");
	protected By travelInspArticleDesc = By
			.xpath(/*"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//p"*/
					"(//div[contains(text(),'Travel Inspiration')]//following::div[contains(@class,'aem-GridColumn')])[1]//p");
	protected By travelInspArtBtn = By.xpath(
			/*"//h2[contains(.,'Travel Inspiration')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//div[@class='wyn-button-cta']"*/
			"(//div[contains(text(),'Travel Inspiration')]//following::div[contains(@class,'aem-GridColumn')])[1]//a//div[contains(@class,'body-1') and contains(text(),'Read More')]");

	protected By sunSandHdr = By.xpath(/*"//h2[contains(.,'Sun, sand')]"*/
			"//div[contains(text(),'Sun, sand')]");
	protected By sunSandSeeAll = By
			.xpath(/*"//h2[contains(.,'Sun, sand')]/ancestor::div[@class='cards']//a[contains(.,'See All Beach')]"*/
					"((//div[contains(text(),'Sun, sand')]/ancestor::div[contains(@class,'aem-GridColumn')]))[2]//a[contains(text(),'See All')]");
	protected By sunSandImage = By
			.xpath("(//div[contains(text(),'Sun, sand')]//following::div[contains(@class,'aem-GridColumn')])[1]//img");
	protected By sunSandArtHdr = By
			.xpath(/*"//h2[contains(.,'Sun, sand')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//h3"*/
					"(//div[contains(text(),'Sun, sand')]//following::div[contains(@class,'aem-GridColumn')])[1]//div[contains(@class,'subtitle-2')]");
	protected By sunSandArtLocations = By
			.xpath("(//div[contains(text(),'Sun, sand')]//following::div[contains(@class,'aem-GridColumn')])[1]//p");
	protected By sunSandArtBtn = By.xpath(
			/*"//h2[contains(.,'Sun, sand')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//div[@class='wyn-button-cta']"*/
			"(//div[contains(text(),'Sun, sand')]//following::div[contains(@class,'aem-GridColumn')])[1]//a//div[contains(@class,'body-1') and contains(text(),'Explore')]");
	protected By wyndhamHotels = By.xpath(/*"//nav[@class='wyn-breadcrumbs']//a[contains(.,'Wyndham Hotel')]"*/
			"//div[@class='breadcrumb']//a[contains(.,'Resorts')]");

	protected By dealsOffrshdr = By.xpath(/*"//h2[contains(.,'Deals & Offers')]"*/
			"//div[contains(text(),'DEALS AND OFFERS')]");
	protected By dealsOffrsSeeAll = By
			.xpath(/*"//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[contains(.,'See All Deals & Offers ')]"*/
					"((//div[contains(text(),'DEALS')]/ancestor::div[contains(@class,'aem-GridColumn')]))[2]//a[contains(text(),'See All')]");
	protected By dealsOffrsArtImages = By
			.xpath(/*"//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//img"*/
					"(//div[contains(text(),'DEALS')]//following::div[contains(@class,'aem-GridColumn')])[1]//img");
	protected By dealsOffrsArtHdr = By
			.xpath(/*"//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//h3"*/
					"(//div[contains(text(),'DEALS')]//following::div[contains(@class,'aem-GridColumn')])[1]//div[contains(@class,'subtitle-2')]");
	protected By dealsOffrsArtDesc = By
			.xpath(/*"//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//p"*/
					"(//div[contains(text(),'DEALS')]//following::div[contains(@class,'aem-GridColumn')])[1]//p");
	protected By dealsOffrsArtBtn = By.xpath(
			/*"//h2[contains(.,'Deals')]/ancestor::div[@class='cards']//a[@class='wyn-card ']//div[@class='wyn-button-cta']"*/
			"(//div[contains(text(),'DEALS')]//following::div[contains(@class,'aem-GridColumn')])[1]//a//div[contains(@class,'body-1') and contains(text(),'See Offers')]");
	protected By travelDealsBRCm = By.xpath(/*"//nav[@class='wyn-breadcrumbs']//a[contains(.,'Travel')]"*/
			"//div[@class='breadcrumb']//a[contains(.,'Travel')]");
	// registeredMarkValidation
	protected By regesiteredTitle = By
			.xpath(/*"//h2[contains(.,'Sun')]/ancestor::div[@id='nextgen-cards']//h3[contains(.,'®')]"*/
					"(//div[contains(text(),'Sun, sand')]//following::div[contains(@class,'aem-GridColumn')])[1]//div[contains(@class,'subtitle-2') and contains(text(),'®')]");
	// h2[contains(.,'Sun')]/ancestor::div[@id='nextgen-cards']//h3[contains(.,'®')]/ancestor::a//div[@class='wyn-button-cta']
	public By regestiredResortPage = By.xpath(/*"//div[@id='main-content']//h1"*/
			"//h1[contains(@class,'title-1')]");
	public By registeredResortTitle = By.xpath(/*"//div[@id='main-content']//h1[contains(.,'®')]"*/
			"//div[contains(@class,'title-1') and contains(text(),'®')]");

	// New Orleans Resort Page
	protected By newOrleansArticle = By.xpath("//h3[contains(.,'Enjoy New Orleans')]");
	// div[@class='slick-slide slick-active']//h3[contains(.,'Enjoy New Orleans')]
	protected By newOrleansTitle = By.xpath("//div[@class='wyn-headline ']//p[contains(.,'New Orleans')]");
	protected By newOrleansMainhdr = By
			.xpath("//div[@class='wyn-headline ']//p[contains(.,'New Orleans')]/following-sibling::h2");
	protected By newOrleansDesc = By.xpath(
			"//div[@class='wyn-headline ']/p[contains(.,'New')]/ancestor::div[@class='wyn-card__content']//div[@class='wyn-l-content wyn-card__copy']/p");
	protected By newOrleansBrdCrm = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'New Orleans')]");
	protected By newOrleansNolaFest = By.xpath("//h2[@class='wyn-headline__title' and contains(.,'NOLA Festiv')]");
	protected By newOrleansImg = By.xpath("//div[@class='wyn-hero wyn-hero--light']//img");
	protected By nolaFestDesc = By
			.xpath("//div[@class='wyn-headline ' and contains(.,'NOLA Festiv')]/following-sibling::p");
	protected By nolaFestSubhdr = By
			.xpath("//div[contains(.,'NOLA Festiv')]/ancestor::div[@gtm_component='icon-list']//h3");
	protected By nolaFestSubDesc = By.xpath("//div[@class='wyn-icon-card__copy']//p");

	protected By discoverBig = By.xpath("//h2[contains(.,'Discover the big')]");
	protected By seeAllResortCTA = By.xpath("//a[contains(.,'See all Resort')]");
	protected By discoverBigHeroImg = By.xpath("//div[@class='wyn-hero wyn-hero--light']//img");
	protected By totalresorts = By.xpath("//h2[@role='status' and contains(.,'Show')]");

	/*
	 * Method featDestHeroVal Description Featured Resorts Page Nav and Validtions
	 * Date Jul/2019 Author Unnat Jain Changes By
	 */
	public void featDestHeroVal() {

		if (verifyObjectDisplayed(featuredDestinationHdr)) {
			tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.PASS,
					"Featured Destinations Header  Present");

			if (verifyObjectDisplayed(featDestImage)) {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.PASS,
						"Featured Destinations Image present");

			} else {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.FAIL,
						"Featured Destinations Image not present");
			}

			if (verifyObjectDisplayed(featDestBrdCrm)) {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.PASS,
						"Featured Destinations Breadcrumb present");

			} else {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.FAIL,
						"Featured Destinations Breadcrumb not present");
			}

			if (verifyObjectDisplayed(featDestSubHdr)) {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.PASS,
						"Featured Destinations Title present as: " + getElementText(featDestSubHdr));

			} else {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.FAIL,
						"Featured Destinations Title not present");
			}

			if (verifyObjectDisplayed(featDestDesc)) {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.PASS,
						"Featured Destinations description  present");

			} else {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.FAIL,
						"Featured Destinations description not  present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenFeaturedDestinations", "featDestHeroVal", Status.FAIL,
					"Featured Destinations header not  present");
		}

	}

	/*
	 * Method destinationGuidesVal Description Feat Dst Destination Guides
	 * Validations Date Jul/2019 Author Unnat Jain Changes By
	 */
	protected By cardReadMore = By.xpath(
			"//div[contains(@class,'slick-active')]//a[contains(@class,'ctaText link')]//div[@class='body-1-link' and contains(text(),'Read')]");
	protected By testimonialTitles = By.xpath("//div[contains(@class,'slick-active')]//div[@class='subtitle-2 bold']");
	protected By testimonialDesc = By.xpath("//div[contains(@class,'slick-active')]//div[@class='subtitle-3 bold']");
	protected By testimonialDescLE = By.xpath("//div[contains(@class,'slick-active')]//div[@class='body-1']/p");

	public void destinationGuidesVal() {

		getElementInView(destGuidesHdr);

		if (verifyObjectDisplayed(destGuidesHdr)) {
			tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal", Status.PASS,
					"Destination Guides Header present");

		} else {
			tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal", Status.FAIL,
					"Destination Guides Header not present");
		}

		/*
		 * if (verifyObjectDisplayed(caraouselVal)) {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.PASS, "Carousel Present on the homepage");
		 * 
		 * List<WebElement> slickBtnList = driver.findElements(slickDotsActive);
		 * 
		 * for (int i = 0; i < slickBtnList.size(); i++) { if
		 * (slickBtnList.get(i).getAttribute("class").contains("active")) {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.PASS, "Page " + (i + 1) + " of carousel displayed");
		 * clickElementBy(nextArrow);
		 * waitForSometime(tcConfig.getConfig().get("LowWait")); } else {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.FAIL, "Carousel not working as expected"); }
		 * 
		 * }
		 * 
		 * if (verifyObjectDisplayed(destGuideArtHeader)) {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.PASS, "Destination Guide Article presnt with headers"); } else {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.FAIL, "Destination Guide Article not presnt with headers"); }
		 * 
		 * if (verifyObjectDisplayed(destGuideArtImages)) {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.PASS, "Destination Guide Images presnt"); } else {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.FAIL, "Destination Guide Images not presnt"); }
		 * 
		 * if (verifyObjectDisplayed(destGuideArtDesc)) {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.PASS, "Destination Guide Descriptions present"); } else {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.FAIL, "Destination Guide Descriptions not presnt"); }
		 * 
		 * if (verifyObjectDisplayed(destGuideArtReadMore)) {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.PASS, "Destination Guide Read More presnt"); } else {
		 * tcConfig.updateTestReporter("NextGenFeaturedDest", "destinationGuidesVal",
		 * Status.FAIL, "Destination Guide Read More not presnt"); }
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenFeaturedDest",
		 * "destinationGuidesVal", Status.FAIL, "Carousel not  Present on the page"); }
		 */

		/*List<WebElement> readMoreList = driver.findElements(cardReadMore);

		for (int i = 0; i < readMoreList.size(); i++) {
			List<WebElement> titleList = driver.findElements(testimonialTitles);
			List<WebElement> readMoreList1 = driver.findElements(cardReadMore);
			String articleHeader = titleList.get(i).getText().toLowerCase();
			tcConfig.updateTestReporter("NextGenHomePage", "ownerCardsSectionVal", Status.PASS,
					(i + 1) + " Card Header is: " + titleList.get(i).getText());
			getElementInView(readMoreList1.get(i));
			clickElementWb(readMoreList1.get(i));

			String strPageTitle = driver.getTitle().toUpperCase();

			if (strPageTitle.contains(testData.get("clubWyndham").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerCardsSectionVal", Status.PASS,
						"Navigated to respective page via read more Cta");

				driver.navigate().back();
				waitUntilElementVisibleBy(driver, testimonialTitles, 120);

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerCardsSectionVal", Status.FAIL,
						"Navigation to respective page failed via read more Cta");
			}

		}*/

	}

	/*
	 * Method speakEasiesVal Description Feat Dst Speak Easies Validations Date
	 * Jul/2019 Author Unnat Jain Changes By
	 */
	public void speakEasiesVal() {

		getElementInView(bannerCarHdr);

		if (verifyObjectDisplayed(bannerCarHdr)) {
			tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.PASS,
					"City Speakeasies section present");
			if (verifyObjectDisplayed(bannerCaroslSubHdr)) {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.PASS,
						"City Speakeasies title present" + getElementText(bannerCaroslSubHdr));

			} else {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.FAIL,
						"City Speakeasies title not present");
			}

			if (verifyObjectDisplayed(bannerCarouselReadMore)) {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.PASS,
						"City Speakeasies read more button present");
				
				/*String winhandlebefr= driver.getWindowHandle();
				clickElementBy(bannerCarouselReadMore);
				
				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);

				}*/
				/*String href = getElementAttribute(bannerCarouselReadMore, "href");
				clickElementBy(bannerCarouselReadMore);
				//waitUntilElementVisibleBy(driver, speakEasiesBrdCrm, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strTitle = driver.getCurrentUrl();
				
				if (strTitle.contains(href)) {
					tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.PASS,
							"Navigted to assoicated page");

				} else {
					tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.FAIL,
							"Navigtion to assoicated page failed");
				}

				driver.navigate().back();
				driver.close();
				driver.switchTo().window(winhandlebefr);*/

			} else {
				tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.FAIL,
						"City Speakeasies read mor button not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenFeaturedDestinations", "speakEasiesVal", Status.FAIL,
					"City Speakeasies section not present");
		}

	}

	/*
	 * Method travelInspVal Description Feat Dst Travel inspiration Validations Date
	 * Jul/2019 Author Unnat Jain Changes By
	 */
	public void travelInspVal() {

		waitUntilElementVisibleBy(driver, travelInspHdr, 120);

		if (verifyObjectDisplayed(travelInspHdr)) {
			getElementInView(travelInspHdr);

			tcConfig.updateTestReporter("NextGenFeaturedDestinations", "travelInspVal", Status.PASS,
					"Travel Inspirations section with title present on the page");

			if (verifyObjectDisplayed(travelInspArticleHdr)) {
				List<WebElement> totalCardsList = driver.findElements(travelInspArticleHdr);
				List<WebElement> totalCardsImageList = driver.findElements(travelInspArticleImages);
				List<WebElement> totalCardsTitleList = driver.findElements(travelInspArticleHdr);
				List<WebElement> totalCardsDescList = driver.findElements(travelInspArticleDesc);
				List<WebElement> totalCardsReadMoreList = driver.findElements(travelInspArtBtn);

				tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
						"Total Cards in Travel Inspirations section is " + totalCardsList.size());

				if (totalCardsList.size() == totalCardsImageList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
							"Image present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
							"Image not present in all cards");
				}

				if (totalCardsList.size() == totalCardsTitleList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
							"Title present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
							"Title not present in all cards");
				}

				if (totalCardsList.size() == totalCardsDescList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
							"Description present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
							"Description not present in all cards");
				}

				if (totalCardsList.size() == totalCardsReadMoreList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
							"Read More present in all cards");

					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
							"Clicking on first Card with title " + totalCardsTitleList.get(0).getText());

					totalCardsReadMoreList.get(0).click();

					waitUntilElementVisibleBy(driver, travelInspirationBread, 50);
					if (verifyObjectDisplayed(travelInspirationBread)) {
						tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
								"Clicked Resort page navigated" + driver.getTitle());

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
								"Clicked resort page navigation error, title " + driver.getTitle());
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, travelInspHdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
							"Read More not present in all cards");
				}

				if (verifyObjectDisplayed(travelInspSeeMore)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
							"See all Travel Inspirations link present on homepage");

					clickElementJSWithWait(travelInspSeeMore);

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains(testData.get("AllTravelInsp").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.PASS,
								"Navigated to All Travel Inspirations page");

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
								"All Travel Inspirations page navigation failed");
					}

					driver.navigate().back();

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
							"See all Travel Inspirations link not present on homepage");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
						"Cards not present in Travel Inspirations page");

			}

		} else {
			tcConfig.updateTestReporter("NextGenFeatDest", "travelInspVal", Status.FAIL,
					"Travel Inspiration section with title not present on the page");
		}

	}

	/*
	 * Method dealsOffersVal Description Feat Dst Deals and Offers Validations Date
	 * Jul/2019 Author Unnat Jain Changes By
	 */
	public void dealsOffersVal() {

		waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

		if (verifyObjectDisplayed(dealsOffrshdr)) {
			getElementInView(dealsOffrshdr);

			tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
					"Deals and offers section with title present on the page");

			if (verifyObjectDisplayed(dealsOffrsArtHdr)) {
				List<WebElement> totalCardsList = driver.findElements(dealsOffrsArtHdr);
				List<WebElement> totalCardsImageList = driver.findElements(dealsOffrsArtImages);
				List<WebElement> totalCardsTitleList = driver.findElements(dealsOffrsArtHdr);
				List<WebElement> totalCardsDescList = driver.findElements(dealsOffrsArtDesc);
				List<WebElement> totalCardsSeeOffList = driver.findElements(dealsOffrsArtBtn);

				tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
						"Total Cards in Deals and Offers section is " + totalCardsList.size());

				if (totalCardsList.size() == totalCardsImageList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
							"Image present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.FAIL,
							"Image not present in all cards");
				}

				if (totalCardsList.size() == totalCardsTitleList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
							"Title present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.FAIL,
							"Title not present in all cards");
				}

				if (totalCardsList.size() == totalCardsDescList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
							"Description present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.FAIL,
							"Description not present in all cards");
				}

				if (totalCardsList.size() == totalCardsSeeOffList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
							"See Offers present in all cards");

					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
							"Clicking on first Card with title " + totalCardsTitleList.get(0).getText());

					String firstTitle = totalCardsTitleList.get(0).getText().toUpperCase();
					totalCardsSeeOffList.get(0).click();

					waitUntilElementVisibleBy(driver, travelDealsBRCm, 120);
					if (verifyObjectDisplayed(travelDealsBRCm)) {
						tcConfig.updateTestReporter("NextGenFeatDest", "homepageHeroImageVal", Status.PASS,
								"Clicked Resort page navigated" + driver.getTitle());

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "homepageHeroImageVal", Status.FAIL,
								"Clicked resort page navigation error, title " + driver.getTitle());
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.FAIL,
							"See Offers not present in all cards");
				}

				if (verifyObjectDisplayed(dealsOffrsSeeAll)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.PASS,
							"See all deals and offers link present on homepage");
					getElementInView(dealsOffrsSeeAll);
					clickElementBy(dealsOffrsSeeAll);

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains(testData.get("AllDeals").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenFeatDest", "homepageHeroImageVal", Status.PASS,
								"Navigated to All deals and offers page");

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "homepageHeroImageVal", Status.FAIL,
								"All deals and offers page navigation failed");
					}
					driver.navigate().back();

					waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.FAIL,
							"See all deals and offers link not present on page");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.FAIL,
						"Cards not present in Deals and offers page");

			}

		} else {
			tcConfig.updateTestReporter("NextGenFeatDest", "dealsOffersVal", Status.FAIL,
					"Deals and offers section with title not present on the page");
		}

	}

	/*
	 * Method sunSandFamlyResort Description: Feat Dst Sun Sand and Family Resorts
	 * Validations validation Date Jul/2019 Author Unnat Jain Changes By
	 */
	public void sunSandFamlyResort() {

		waitUntilElementVisibleBy(driver, sunSandHdr, 120);

		if (verifyObjectDisplayed(sunSandHdr)) {
			getElementInView(sunSandHdr);

			tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
					"Sun, Sand Family Resort  section with title present on the page");

			if (verifyObjectDisplayed(sunSandArtHdr)) {
				List<WebElement> totalCardsList = driver.findElements(sunSandArtHdr);
				List<WebElement> totalCardsImageList = driver.findElements(sunSandImage);
				List<WebElement> totalCardsTitleList = driver.findElements(sunSandArtHdr);
				List<WebElement> totalCardsLocList = driver.findElements(sunSandArtLocations);
				List<WebElement> totalCardsSeeOffList = driver.findElements(sunSandArtBtn);

				tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
						"Total Cards in Sun, Sand Family Resort section is " + totalCardsList.size());

				if (totalCardsList.size() == totalCardsImageList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
							"Image present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
							"Image not present in all cards");
				}

				if (totalCardsList.size() == totalCardsTitleList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
							"Title present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
							"Title not present in all cards");
				}

				if (totalCardsList.size() == totalCardsLocList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
							"Location present in all cards");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
							"Location not present in all cards");
				}

				if (totalCardsList.size() == totalCardsSeeOffList.size()) {

					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
							"See Offers present in all cards");

					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
							"Clicking on first Card with title " + totalCardsTitleList.get(0).getText());

					String firstTitle = totalCardsTitleList.get(0).getText().toUpperCase();
					totalCardsSeeOffList.get(0).click();

					waitUntilElementVisibleBy(driver, wyndhamHotels, 40);
					if (verifyObjectDisplayed(wyndhamHotels)) {
						tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
								"Clicked Resort page navigated" + driver.getTitle());

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
								"Clicked resort page navigation error, title " + driver.getTitle());
					}

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
							"See Offers not present in all cards");
				}

				try {
					List<WebElement> registeredTitleLinks = driver.findElements(regesiteredTitle);
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
							"There are " + registeredTitleLinks.size() + " with registered mark");

					getElementInView(registeredTitleLinks.get(0));

					if (verifyElementDisplayed(registeredTitleLinks.get(0))) {
						tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
								"Link with Registered mark is: " + registeredTitleLinks.get(0).getText());

						registeredTitleLinks.get(0).click();

						waitUntilElementVisibleBy(driver, registeredResortTitle, 120);

						if (verifyObjectDisplayed(registeredResortTitle)) {
							tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
									"Header with Registered mark is displayed as "
											+ getElementText(registeredResortTitle));
						} else {
							tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
									"Header with Registered mark is not displayed");
						}

						driver.navigate().back();

						waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
								"Registered Links not present");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
							"Failed to validated Registered Mark Link");
				}

				if (verifyObjectDisplayed(sunSandSeeAll)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
							"See all Sun, Sand Family Resort link present on homepage");
					getElementInView(sunSandSeeAll);
					clickElementBy(sunSandSeeAll);

					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getCurrentUrl().toUpperCase().contains(testData.get("exploreResortUrl").toUpperCase())) {
						tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.PASS,
								"Navigated to All Sun, Sand Family Resort page");

						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
								"All Sun, Sand Family Resort page navigation failed");
					}
					driver.navigate().back();

					waitUntilElementVisibleBy(driver, dealsOffrshdr, 120);

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
							"See all Sun, Sand Family Resort link not present on page");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
						"Cards not present in Sun, Sand Family Resort page");

			}

		} else {
			tcConfig.updateTestReporter("NextGenFeatDest", "sunSandFamilyResort", Status.FAIL,
					"Sun, Sand Family Resort section with title not present on the page");
		}

	}

	/*
	 * Method newOrleanspageVal Description: New Orleans Resort Page Validations
	 * validation Date Oct/2019 Author Unnat Jain Changes By
	 */
	public void newOrleanspageVal() {

		getElementInView(newOrleansArticle);

		if (verifyObjectDisplayed(newOrleansArticle)) {
			tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
					"New Orleans article present in the Page ");

			clickElementJSWithWait(newOrleansArticle);
			// driver.navigate().to("https://clubwyndham.wyndhamdestinations.com/resorts/featured-destinations/new-orleans");
			waitUntilElementVisibleBy(driver, newOrleansTitle, 120);

			if (verifyObjectDisplayed(newOrleansTitle)) {
				tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
						"New Orleans Resort Navigated");

				if (verifyObjectDisplayed(newOrleansBrdCrm)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
							"New Orleans Breadcrumb present");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
							"New Orleans Breadcrumb not present");
				}

				if (verifyObjectDisplayed(newOrleansMainhdr)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
							"New Orleans Title present as: " + getElementText(newOrleansMainhdr));

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
							"New Orleans Title not present");
				}

				if (verifyObjectDisplayed(newOrleansDesc)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
							"New Orleans description  present");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
							"New Orleans description not  present");
				}

				if (verifyObjectDisplayed(newOrleansImg)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
							"New Orleans Img  present");

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
							"New Orleans Img not  present");
				}

				getElementInView(newOrleansNolaFest);

				if (verifyObjectDisplayed(newOrleansNolaFest)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
							"Nola Fest header section present ");

					List<WebElement> happyPlaceSubTitle = driver.findElements(nolaFestSubhdr);
					List<WebElement> happyPlaceSubDesc = driver.findElements(nolaFestSubDesc);
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
							"Total Subtitle: " + happyPlaceSubTitle.size());
					for (int i = 0; i < happyPlaceSubTitle.size(); i++) {
						tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
								(i + 1) + " title is " + happyPlaceSubTitle.get(i).getText());

					}

					if (happyPlaceSubTitle.size() >= happyPlaceSubDesc.size()) {
						tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
								"Nola Fest descriptions present ");

					} else {
						tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
								"Nola Fest descriptions not present in all the sub titles ");

					}

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
							"Nola Fest header section not present ");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
						"New Orleans Resort Navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
					"New Orleans article not present in the Page ");
		}

		getElementInView(discoverBig);

		if (verifyObjectDisplayed(discoverBig)) {
			tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
					"Discover the Big Easy header section present ");

			if (verifyObjectDisplayed(discoverBigHeroImg)) {
				tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
						"Discover the Big Easy hero image present ");

				if (verifyObjectDisplayed(seeAllResortCTA)) {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.PASS,
							"See All Resort CTA present");

					clickElementBy(seeAllResortCTA);

					waitUntilElementVisibleBy(driver, totalresorts, 120);

					if (verifyObjectDisplayed(totalresorts)) {
						tcConfig.updateTestReporter("NextGenResortsPage", "newOrleansValidations", Status.PASS,
								"Resorts page Navigated");
						driver.navigate().back();
						waitUntilElementVisibleBy(driver, discoverBig, 120);

					} else {
						tcConfig.updateTestReporter("NextGenResortsPage", "newOrleansValidations", Status.FAIL,
								"Resorts page Navigation failed");

					}

				} else {
					tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
							"See All Resort CTA not present");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
						"Discover the Big Easy hero image not present ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenFeatDest", "newOrleansValidations", Status.FAIL,
					"Discover the Big Easy header section not present ");
		}

	}

}
