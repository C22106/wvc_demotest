package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenOwnerGuidePage_IOS extends NextGenOwnerGuidePage_Web {

	public static final Logger log = Logger.getLogger(NextGenOwnerGuidePage_IOS.class);

	public NextGenOwnerGuidePage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
