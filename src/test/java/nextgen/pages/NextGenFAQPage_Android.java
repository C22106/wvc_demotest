package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenFAQPage_Android extends NextGenFAQPage_Web {

	public static final Logger log = Logger.getLogger(NextGenFAQPage_Android.class);

	public NextGenFAQPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
