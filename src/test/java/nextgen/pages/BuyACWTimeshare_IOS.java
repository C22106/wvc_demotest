package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class BuyACWTimeshare_IOS extends BuyACWTimeshare_Web {

	public static final Logger log = Logger.getLogger(BuyACWTimeshare_IOS.class);

	public BuyACWTimeshare_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
