package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NextGenFAQPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenFAQPage_Web.class);

	public NextGenFAQPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By helpNavHeader = By.xpath(/*
											 * "//div[@class='grid-container ']//li[@role='menuitem' and contains(.,'Help')]"
											 */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Help']");
	protected By faqPageNav = By.xpath(/* "//a[@data-eventname='header']//span[contains(.,'FAQ')]" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'FAQ')]");
	protected By faqHeader = By.xpath(/* "//div[@class='wyn-headline ']//h2" */
			"(//div[@class='title-1' and contains(text(),'FAQS')])[1]");
	protected By faqSubHeader = By.xpath(/* "//div[@class='wyn-headline ']//h3" */
			"(//div[@class='subtitle-3' and contains(text(),'Find')])[1]");
	protected By faqHeroImg = By.xpath(/* "//div[@class='wyn-hero__image']//img" */
			"(//div[@class='subtitle-3' and contains(text(),'Find')])[1]/ancestor::div[@class='banner']//img");
	protected By faqSearchContainer = By.xpath("//input[@class='wyn-form__field__input']");
	protected By faqSearchBtn = By.xpath("//label[@class='wyn-form__field__label']/button");
	protected By clearSearch = By.xpath("//a[@aria-label='Clear search']");
	protected By faqBrdCrmb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'FAQ')]");
	protected By faqMainHdr = By.xpath(/* "//div[@role='main']//h2" */
			"//div[@class='contentSlice']//h6");
	protected By expandAllCTA = By.xpath("//a[contains(.,'Expand')]");
	protected By collapseAllCTA = By.xpath("//a[contains(.,'Collapse')]");
	protected By faqQuestions = By.xpath(/*
											 * "//div[@class='wyn-accordion']//span[@class='wyn-accordion__trigger-label']"
											 */
			"((//div[@class='contentSlice']//h6)[1]//..//..//..//..//div[@class='accordionSlice'])[1]//li/a");
	protected By collapsedQuestion = By.xpath("//div[@class='wyn-js-accordion']");
	protected By expandedQuestion = By.xpath("//div[@class='wyn-js-accordion is-active']");
	protected By accordionExpand = By.xpath("//div[@class='wyn-accordion']//span[@class='wyn-accordion__indicator']");
	protected By totalResultsCount = By.xpath("//div[@class='wyn-hero-search__results-count wyn-type-title']");
	protected By highlightedResults = By.xpath("//mark");

	/*
	 * Method: faqPageNav Description: FAQ page navigation Date: Aug/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void faqPageNav() {

		waitUntilElementVisibleBy(driver, helpNavHeader, 120);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(helpNavHeader)).build().perform();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(faqPageNav)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageNav", Status.PASS, "FAQ present in Sub Nav");
			driver.findElement(faqPageNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageNav", Status.FAIL, "FAQ not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, faqHeader, 120);

		if (verifyObjectDisplayed(faqHeader)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageNav", Status.PASS, "FAQ Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageNav", Status.FAIL, "FAQ Page Navigation failed");
		}

	}

	/*
	 * Method: faqPageVal Description: FAQ page Validations: Date Aug/2019 Author:
	 * Unnat Jain Changes By
	 */
	public void faqPageVal() {
		if (verifyObjectDisplayed(faqHeader)) {

			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.PASS,
					"FAQ header present as " + getElementText(faqHeader));

		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.FAIL, "FAQ header not present");
		}

		if (verifyObjectDisplayed(faqHeroImg)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.PASS, "FAQ  hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.FAIL,
					"FAQ  page hero Image not present");
		}

		if (verifyObjectDisplayed(faqBrdCrmb)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.PASS, "FAQ Breadcrumb present");

		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.FAIL, "FAQ Breadcrumb not present");
		}

		if (verifyObjectDisplayed(faqSubHeader)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.PASS, "FAQ Desc present");

		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqPageVal", Status.FAIL, "FAQ Desc not present");
		}

	}

	/*
	 * Method: faqSearchVal Description: FAQ Search Validations: Date Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void faqSearchVal() {
		getElementInView(faqSearchContainer);
		if (verifyObjectDisplayed(faqSearchContainer)) {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.PASS, "FAQ Search Container present");

			fieldDataEnter(faqSearchContainer, testData.get("strSearch"));

			clickElementBy(faqSearchBtn);

			if (verifyObjectDisplayed(totalResultsCount)) {
				tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.PASS,
						"Total results displayed: " + getElementText(totalResultsCount));
				try {
					List<WebElement> highlightedResultList = driver.findElements(highlightedResults);

					tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.PASS,
							"Results Highlighted total: " + highlightedResultList.size());

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.FAIL,
							"Results not highlighted");
				}
				getElementInView(clearSearch);
				clickElementBy(clearSearch);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.navigate().refresh();
				try {
					List<WebElement> highlightedResultList = driver.findElements(highlightedResults);
					highlightedResultList.get(0).click();
					tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.FAIL,
							"Search results not cleared");

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.PASS,
							"Search Results are cleared");
				}

			} else {
				tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.FAIL,
						"Total results count not displayed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqSearchVal", Status.FAIL,
					"FAQ Search Container not present");
		}

	}

	/*
	 * Method: faqAccordianVal Description: FAQ Accordian Validations: Date Aug/2019
	 * Author: Unnat Jain Changes By
	 */
	public void faqAccordianVal() {

		if (verifyObjectDisplayed(faqMainHdr)) {

			List<WebElement> headerList = driver.findElements(faqMainHdr);
			for (int i = 0; i < headerList.size(); i++) {
				tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.PASS,
						"FAQ Main Header present " + getElementText(headerList.get(i)));

				List<WebElement> questionsList = driver.findElements(By.xpath("((//div[@class='contentSlice']//h6)["
						+ (i + 1) + "]//..//..//..//..//div[@class='accordionSlice'])[" + (i + 1) + "]//li/a"));
				tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.PASS,
						"FAQ questions displayed total: " + questionsList.size());
				if (verifyElementDisplayed(driver.findElement(By.xpath("((//div[@class='contentSlice']//h6)[" + (i + 1)
						+ "]//..//..//..//..//div[@class='accordionSlice'])[" + (i + 1) + "]//ul/div")))) {
					tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.PASS,
							"Expand All CTA present");
					clickElementBy(By.xpath("((//div[@class='contentSlice']//h6)[" + (i + 1)
							+ "]//..//..//..//..//div[@class='accordionSlice'])[" + (i + 1) + "]//ul/div"));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					List<WebElement> expandedQuestions = driver
							.findElements(By.xpath("((//div[@class='contentSlice']//h6)[" + (i + 1)
									+ "]//..//..//..//..//div[@class='accordionSlice'])[" + (i + 1)
									+ "]//li[@class='accordion-item is-active']/a"));
					if (expandedQuestions.size() == questionsList.size()) {
						tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.PASS,
								"All the questions are expanded");
					} else {
						tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.FAIL,
								"All the questions are  not expanded");
					}

					if (verifyElementDisplayed(
							driver.findElement(By.xpath("((//div[@class='contentSlice']//h6)[" + (i + 1)
									+ "]//..//..//..//..//div[@class='accordionSlice'])[" + (i + 1) + "]//ul/div")))) {
						tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.PASS,
								"Close All CTA present");
						clickElementBy(By.xpath("((//div[@class='contentSlice']//h6)[" + (i + 1)
								+ "]//..//..//..//..//div[@class='accordionSlice'])[" + (i + 1) + "]//ul/div"));
						List<WebElement> collapsedQuestions = driver
								.findElements(By.xpath("((//div[@class='contentSlice']//h6)[" + (i + 1)
										+ "]//..//..//..//..//div[@class='accordionSlice'])[" + (i + 1) + "]//li/a"));
						if (collapsedQuestions.size() == questionsList.size()) {
							tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.PASS,
									"All the questions are collapsed");
						} else {
							tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.FAIL,
									"All the questions are  not collapsed");
						}
					} else {
						tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.FAIL,
								"Close All CTA present");
					}
				} else {
					tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.FAIL,
							"Expand All CTA not not present");
				}

			}

		} else {
			tcConfig.updateTestReporter("NextGenFAQPage", "faqAccordianVal", Status.FAIL,
					"FAQ Main Header not present");
		}

	}

}
