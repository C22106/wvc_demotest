package nextgen.pages;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenSearchResultPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenSearchResultPage_Web.class);

	public NextGenSearchResultPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By searchResultsHeader = By.xpath(/* "//h1[contains(.,'Search')]" */
			"//div[contains(text(),'Search')]");
	protected By searchResultCount = By.xpath("(//div[@class='padding-1 padding-vertical-3']//p)[1]");
	protected By searchResultsFor = By.xpath("(//div[@class='padding-1 padding-vertical-3']//p)[1]/strong");
	protected By searchResultsTitle = By.xpath("//div[@class='margin-bottom-3']//a");
	protected By pagination = By.xpath("//button[@class='link-button']");
	protected By searchResultAdddress = By
			.xpath("//div[@class='wyn-aem-search__results' and contains(.,'/content/wyndham')]");

	protected By NextGenCWLogo = By.xpath("//div[@class='column is-narrow wyn-header__logo']");
	protected By nextGenHeroImage = By
			.xpath("//div[@class='slick-slide slick-current slick-active']//div[@class='wyn-hero__image']");
	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	protected By homeBreadcrumbNav = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Home')]");
	protected By acceptButton = By.xpath("//button[contains(.,'Accept')]");
	protected By searchIcon = By.xpath("//span[contains(.,'search')]");
	protected By searchContainer = By.xpath("//div[@role='combobox']//input");
	protected By searchButton = By.xpath("//button[contains(.,'Search')]");
	protected By searchSuggestion = By
			.xpath("//div[@class='react-autosuggest__section-container react-autosuggest__section-container--first']");

	// Wyndham Sweeps
	protected By heroImage = By.xpath("//div[@class='wyn-hero__image']//img");
	protected By sweepsBrdCrm = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Sweeps')]");
	protected By pageHeader = By.xpath("//h2[contains(.,'Enter for a Chance')]");
	protected By sweepsForm = By.xpath("//div[@id='SweepstakesFormReact']");
	protected By sweepsFirstName = By.xpath("//input[@id='firstName']");
	protected By sweepsLastName = By.xpath("//input[@id='lastName']");
	protected By sweepsAddress = By.xpath("//input[@id='address']");
	protected By sweepsCity = By.xpath("//input[@id='city']");
	protected By sweepsZip = By.xpath("//input[@id='zipCode']");
	protected By sweepsEmail = By.xpath("//input[@id='email']");
	protected By sweepsPhone = By.xpath("//input[@id='altphone']");
	protected By sweepsHomePh = By.xpath("//input[@id='PhoneNumber']");
	protected By maritalStatus = By.xpath("//select[@name='MaritalStatus']");
	protected By sweepsState = By.xpath("//select[@name='state']");
	protected By sweepsAge = By.xpath("//select[@name='age']");
	protected By sweepsIncome = By.xpath("//select[@name='income']");
	protected By accessCode = By.xpath("//span[contains(.,'Access')]/parent::div/following-sibling::label");
	protected By sweepsAccessCode = By.xpath("//input[@id='accessCode']");
	protected By agreeBnDisable = By.xpath("//button[contains(.,'I agree') and @disabled]");
	protected By notAgreeDisable = By.xpath("//button[contains(.,'I do not') and @disabled]");
	protected By agreeBnEnable = By.xpath("//button[contains(.,'I agree')");
	protected By notAgreeEnable = By.xpath("//button[contains(.,'I do not')");
	protected By thankYouNote = By.xpath("//h4[contains(.,'Thanks for your')]");
	protected By closePopUp = By.xpath("//span[@class='wyn-modal__close wyn-modal__close--secondary']");
	String strAccesCode;

	/*
	 * Method: searchResultsValidatins Description: Search Results Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void searchResultsValidatins() {

		waitForSometime(tcConfig.getConfig().get("LongWait"));
		waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

		if (verifyObjectDisplayed(searchResultsHeader)) {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");

			String strResultsCount = getElementText(searchResultCount);
			String strResultsFor = getElementText(searchResultsFor);

			if (strResultsCount.toUpperCase().contains("did not match any documents")) {
				tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
						"No Results generated for searched keyword: " + strResultsFor);
			} else {

				String strResults = StringUtils.substringBetween(strResultsCount, "Results 1", "for");
				String[] strTotalResult = strResults.trim().split("of");
				strResults = strTotalResult[strTotalResult.length - 1];
				tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
						"Total Results: " + strResults + " for" + strResultsFor);

				List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
				tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
						"No Of Results on first page " + listOfResults.size());

				if (listOfResults.size() >= 10) {
					if (verifyObjectDisplayed(pagination)) {
						tcConfig.updateTestReporter("NextGenHomePage", "searchResultsValidatins", Status.PASS,
								"Pagination Available on the page");
					}
				} else {
					System.out.println("Less than 10 results hence no pagination");
				}

				/*
				 * try { List<WebElement> resultAdddressList =
				 * driver.findElements(searchResultAdddress);
				 * 
				 * if (verifyElementDisplayed(resultAdddressList.get(0))) {
				 * tcConfig.updateTestReporter("NextGenHomePage", "searchResultsValidatins",
				 * Status.FAIL, "Search Results Address and Date displayed"); } else {
				 * tcConfig.updateTestReporter("NextGenHomePage", "searchResultsValidatins",
				 * Status.PASS, "Search Results Address and Date not displayed"); }
				 * 
				 * } catch (Exception e) { tcConfig.updateTestReporter("NextGenHomePage",
				 * "searchResultsValidatins", Status.PASS,
				 * "Search Results Address and Date not displayed"); }
				 */

				String firstTitle = listOfResults.get(0).getText();

				tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
						"First Result title " + firstTitle);

				listOfResults.get(0).click();

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				/*
				 * if (driver.getTitle().toUpperCase().contains("ERROR")) {
				 * tcConfig.updateTestReporter("NextGenSearchResultPage",
				 * "searchResultsValidatins", Status.FAIL,
				 * "Failed to Navigate to first Search related page " + firstTitle); } else {
				 * tcConfig.updateTestReporter("NextGenSearchResultPage",
				 * "searchResultsValidatins", Status.PASS,
				 * "Navigated to first Search related page " + driver.getTitle().toUpperCase());
				 * 
				 * }
				 */

				WebElement eleTitle = driver.findElement(
						By.xpath("//div[contains(@class,'title-1') and contains(text(),'" + firstTitle + "')]"));
				waitUntilObjectVisible(driver, eleTitle, 120);
				if (driver
						.findElement(
								By.xpath("//div[contains(@class,'title-1') and contains(text(),'" + firstTitle + "')]"))
						.getText().equals(firstTitle)) {
					scrollDownForElementJSWb(driver.findElement(
							By.xpath("//div[contains(@class,'title-1') and contains(text(),'" + firstTitle + "')]")));
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
							"Navigated to first Search related page " + firstTitle);
				} else {
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
							"Failed to Navigate to first Search related page " + firstTitle);
				}

				driver.navigate().back();

				waitUntilObjectVisible(driver, searchResultsHeader, 120);

			}

		} else {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

	/*
	 * Method: resortSearchValidatins Description: Resort Search Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void resortSearchValidatins() {

		waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

		if (verifyObjectDisplayed(searchResultsHeader)) {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "resortSearchValidatins", Status.PASS,
					"Navigated to Search Results page");

			String strResultsCount = getElementText(searchResultCount);
			String strResultsFor = getElementText(searchResultsFor);

			if (strResultsCount.toUpperCase().contains("did not match any documents")) {
				tcConfig.updateTestReporter("NextGenSearchResultPage", "resortSearchValidatins", Status.PASS,
						"No Results generated for searched keyword: " + strResultsFor);
			} else {

				String strResults = StringUtils.substringBetween(strResultsCount, "Results 1", "for");
				String[] strTotalResult = strResults.trim().split("of");
				strResults = strTotalResult[strTotalResult.length - 1];
				tcConfig.updateTestReporter("SearchResultPage", "resortSearchValidatins", Status.PASS,
						"Total Results: " + strResults + " for" + strResultsFor);

				List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
				tcConfig.updateTestReporter("NextGenSearchResultPage", "resortSearchValidatins", Status.PASS,
						"No Of Results on first page " + listOfResults.size());

				try {
					List<WebElement> resultAdddressList = driver.findElements(searchResultAdddress);

					if (verifyElementDisplayed(resultAdddressList.get(0))) {
						tcConfig.updateTestReporter("NextGenHomePage", "searchResultsValidatins", Status.FAIL,
								"Search Results Address and Date displayed");
					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "searchResultsValidatins", Status.PASS,
								"Search Results Address and Date not displayed");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenHomePage", "searchResultsValidatins", Status.PASS,
							"Search Results Address and Date not displayed");
				}

				/*
				 * if (listOfResults.size() >= 10) { if (verifyObjectDisplayed(pagination)) {
				 * tcConfig.updateTestReporter("NextGenHomePage", "resortSearchValidatins",
				 * Status.PASS, "Pagination Available on the page"); } else {
				 * tcConfig.updateTestReporter("NextGenHomePage", "resortSearchValidatins",
				 * Status.FAIL, "Pagination Not available on the page"); } } else {
				 * System.out.println("Less than 10 results hence no pagination" ); }
				 */
				/*
				 * String firstTitle=listOfResults.get(0).getText();
				 * 
				 * tcConfig.updateTestReporter("NextGenSearchResultPage",
				 * "resortSearchValidatins", Status.PASS, "First Result title "+firstTitle);
				 * 
				 * listOfResults.get(0).click();
				 * 
				 * waitForSometime(tcConfig.getConfig().get("LongWait"));
				 * 
				 * 
				 * if(driver.getTitle().toUpperCase().contains("ERROR")) {
				 * tcConfig.updateTestReporter("NextGenSearchResultPage",
				 * "resortSearchValidatins", Status.FAIL,
				 * "Failed to Navigate to first Search related page " +firstTitle); }else {
				 * tcConfig.updateTestReporter("NextGenSearchResultPage",
				 * "resortSearchValidatins", Status.PASS,
				 * "Navigated to first Search related page " +driver.getTitle().toUpperCase());
				 * 
				 * }
				 * 
				 * driver.navigate().back();
				 * 
				 * waitUntilElementVisibleBy(driver,searchResultsHeader, 120);
				 */

			}

		} else {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "resortSearchValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

	/*
	 * Method: wyndhamSweepsNav Description: Wyndham Sweeps Navigation: Date
	 * Dec/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamSweepsNav() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, searchIcon, 120);

		if (verifyObjectDisplayed(searchIcon)) {

			tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsNav", Status.PASS,
					"Search Icon Present on Homepage");

			clickElementBy(searchIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchContainer)) {

				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsNav", Status.PASS,
						"Search Container displayed with text: "
								+ driver.findElement(searchContainer).getAttribute("placeholder"));

				driver.findElement(searchContainer).click();
				driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsNav", Status.PASS,
						"Search Keyword Entered");

				clickElementJSWithWait(searchButton);

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

				if (verifyObjectDisplayed(searchResultsHeader)) {
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
							"Navigated to Search Results page");

					List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.PASS,
							"No Of Results on first page " + listOfResults.size());

					for (int i = 0; i < listOfResults.size(); i++) {

						if (listOfResults.get(i).getText().equalsIgnoreCase("Wyndham Sweeps")) {
							listOfResults.get(i).click();

							waitUntilElementVisibleBy(driver, pageHeader, 120);
						}

					}

				} else {
					tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
							"Unable to navigate to search results page");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsNav", Status.FAIL,
						"Error in getting the search container");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsNav", Status.FAIL,
					"Error in getting the search Icon");
		}

	}

	/*
	 * Method: wyndhamSweepsVal Description: Wyndham Sweeps Validations: Date
	 * Dec/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamSweepsVal() {

		if (verifyObjectDisplayed(pageHeader)) {
			tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.FAIL,
					"Navigated To Wyndham Sweeps Page, Main Header Present");

			if (verifyObjectDisplayed(heroImage)) {
				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Hero Image Present");
			} else {
				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Hero Image not Present");
			}

			if (verifyObjectDisplayed(sweepsBrdCrm)) {
				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Breadcrumb Present");
			} else {
				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Breadcrumb not Present");
			}

			if (verifyObjectDisplayed(sweepsForm)) {
				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.PASS,
						"Wyndham Sweeps Form Present");

				getElementInView(sweepsFirstName);

				if (verifyObjectDisplayed(sweepsFirstName)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(sweepsFirstName);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(sweepsFirstName, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsLastName)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(sweepsLastName, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(sweepsAddress)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Address Field Present");

					fieldDataEnter(sweepsAddress, testData.get("Address"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Address Field not present");
				}

				if (verifyObjectDisplayed(sweepsCity)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"City Field Present");

					fieldDataEnter(sweepsCity, testData.get("City"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"City Field not present");
				}

				if (verifyObjectDisplayed(sweepsPhone)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(sweepsPhone, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(sweepsEmail)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(sweepsEmail, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(sweepsZip)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Zip Field Present");

					fieldDataEnter(sweepsZip, testData.get("ZipCode"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Zip Field not present");
				}

				if (verifyObjectDisplayed(sweepsHomePh)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Home Phone Field Present");

					fieldDataEnter(sweepsHomePh, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Home Phone Field not present");
				}

				if (verifyObjectDisplayed(maritalStatus)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Marital Status Drop Down present");

					new Select(driver.findElement(maritalStatus)).selectByVisibleText(testData.get("Marital Status"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Marital Status Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsAge)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Age Drop Down present");

					new Select(driver.findElement(sweepsAge)).selectByVisibleText(testData.get("Age"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Age Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsIncome)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Income Drop Down present");

					new Select(driver.findElement(sweepsIncome)).selectByVisibleText(testData.get("Income"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Income Drop Down not present");
				}

				if (verifyObjectDisplayed(sweepsState)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"State Drop Down present");

					new Select(driver.findElement(sweepsState)).selectByValue(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(accessCode)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Access Code present as: " + getElementText(accessCode));

					strAccesCode = getElementText(accessCode);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code not present");
				}

				if (verifyObjectDisplayed(agreeBnDisable)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Disabled before enetering Acces code");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button Enabled before enetering Acces code");
				}

				if (verifyObjectDisplayed(notAgreeDisable)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Do Not Agree Button Disabled before enetering Acces code");

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Do Not Agree Button Enabled before enetering Acces code");
				}

				if (verifyObjectDisplayed(sweepsAccessCode)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Access Code Field Present");

					fieldDataEnter(sweepsAccessCode, strAccesCode);
					driver.switchTo().activeElement().sendKeys(Keys.TAB);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Access Code Field not present");
				}

				if (verifyObjectDisplayed(agreeBnEnable)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
							"Agree Button Enabled After enetering Access code");
					clickElementBy(agreeBnEnable);

					waitUntilElementVisibleBy(driver, thankYouNote, 50);

					if (verifyObjectDisplayed(thankYouNote)) {
						tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.PASS,
								"Form Submission Successful");

						if (verifyObjectDisplayed(closePopUp)) {
							clickElementBy(closePopUp);
						} else {
							tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
									"Close Btn not present");
						}

					} else {
						tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
								"Form Submission not Successful");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wyndhamSweepsVal", Status.FAIL,
							"Agree Button disabled before enetering Acces code");
				}

			} else {
				tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.FAIL,
						"Wyndham Sweeps Form not Present");
			}

		} else {
			tcConfig.updateTestReporter("NextgenSearchPage", "wyndhamSweepsVal", Status.FAIL,
					"Wyndham Sweeps Navigation Failed");
		}

	}

}
