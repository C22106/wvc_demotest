package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenResortsNewsPage_IOS extends NextGenResortsNewsPage_Web {

	public static final Logger log = Logger.getLogger(NextGenResortsNewsPage_IOS.class);

	public NextGenResortsNewsPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
