package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenFeaturedDestinations_Android extends NextGenFeaturedDestinations_Web {

	public static final Logger log = Logger.getLogger(NextGenFeaturedDestinations_Android.class);

	public NextGenFeaturedDestinations_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
