package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUITripPlannerTravelSeasonPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUITripPlannerTravelSeasonPage_Web.class);

	public CUITripPlannerTravelSeasonPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	
	protected By labelPlanningToStay = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'When are you looking to travel?')]");
	protected By radioButtonSummer = By.xpath("//label[contains(.,'Summer')]");
	protected By radioButtonFall = By.xpath("//label[contains(.,'Fall')]");
	protected By radioButtonWinter = By.xpath("//label[contains(.,'Winter')]");
	protected By radioButtonSpring = By.xpath("//label[contains(.,'Spring')]");
	protected By radioButtonAnytime = By.xpath("//label[contains(.,'Anytime')]");
	protected By buttonNext = By.xpath("(//button[contains(@class,'button expanded') and contains(.,'Next')])[1] | (//button[contains(@class,'button expanded') and contains(.,'Next')])[2]");
	protected By buttonGoBack = By.xpath("(//button[contains(@class,'prev-link-caret') and contains(.,'Go Back')])");
	protected By labelWhereYouWant = By.xpath("//div[contains(@class,'input__content__title') and contains(.,'Where do you want to vacation?')]");
	
	/*
	 * Method: selectSeason Description: Select Season
	 *  Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void selectSeason() {
		Assert.assertFalse(getObject(buttonNext).isEnabled(),"Nextbutton is Enabled");
		WebElement selectRadio = getObject(
				By.xpath("//label[contains(.,'"+ testData.get("Season") +"')]"));
		getElementInView(selectRadio);
		clickElementJSWithWait(selectRadio);
		tcConfig.updateTestReporter("CUITripPlannerTravelSeasonPage", "selectSeason", Status.PASS,
				"Clicked on and selected season is: "  + testData.get("Season"));
	}
	
	/*
	 * Method: clickContinueCTA Description: click Continue CTA
	 * Date: Jan/2021 Author: Saket Sharma Changes By: NA
	 */
	public void clickContinueCTA() {
		clickElementJSWithWait(buttonNext);
		waitUntilElementVisibleBy(driver, labelWhereYouWant, 120);
		tcConfig.updateTestReporter("CUITripPlannerTravelSeasonPage", "clickContinueCTA", Status.PASS,
				"Successfully clicked next button");
	}

}

