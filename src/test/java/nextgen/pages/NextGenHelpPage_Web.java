package nextgen.pages;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class NextGenHelpPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(NextGenHelpPage_Web.class);

	public NextGenHelpPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By helpNavHeader = By.xpath(/* "//div[@class='grid-container ']//a[@title='Help']" */
			"//ul[contains(@class,'global-navigation')]//li/a[text()='Help']");
	protected By helpSubHeader = By.xpath("//div[@class='wyn-headline ']/p[contains(.,'Help')]/following-sibling::h1");
	protected By helpDesc = By.xpath("//div[@class='wyn-headline ']/p[contains(.,'Help')]/following-sibling::h3");
	protected By helpHeader = By.xpath("//div[@class='wyn-headline ']/p[contains(.,'Help')]");
	protected By helpHeaderImg = By
			.xpath("//div[@class='wyn-headline ']/p[contains(.,'Help')]/ancestor::div[@class='slick-track']//img");
	protected By helpBreadCrumb = By.xpath(/* "//nav[@class='wyn-breadcrumbs']//a[contains(.,'Help')]" */
			"//div[@class='breadcrumb']//a[contains(.,'Help')]");
	protected By contactClubWynHeader = By.xpath("//h2[contains(.,'Contact Club Wyndham')]");
	protected By contactClubwynDesc = By.xpath("//h2[contains(.,'Contact Club Wyndham')]/parent::div/parent::div/p");
	protected By contactClubWynSubHeaders = By
			.xpath("//h2[contains(.,'Contact Club Wyndham')]/ancestor::div[@class='icon-list']//h3");
	protected By contactClubWynSubDesc = By
			.xpath("//h2[contains(.,'Contact Club Wyndham')]/ancestor::div[@class='icon-list']//p/a");

	protected By helpCardsImg = By.xpath("//a[@class='wyn-card ']//img");
	protected By helpCardsHdr = By.xpath("//a[@class='wyn-card ']//h3");
	protected By helpCardDesc = By.xpath("//a[@class='wyn-card ']//p");
	protected By helpCardReadMore = By
			.xpath("//a[@class='wyn-card ']//div[@class='wyn-button-cta' and contains(.,'Read')]");

	protected By clubWynOwnerHdr = By.xpath("//div[@class='wyn-headline ']/p[contains(.,'Owner')]");
	protected By clubWynOwnerSubHdr = By
			.xpath("//div[@class='wyn-headline ']/p[contains(.,'Owner')]/following-sibling::h2");
	protected By clubWynDesc = By.xpath(
			"//div[@class='wyn-headline ']/p[contains(.,'Owner')]/ancestor::div[@class='wyn-card__content']//div[2]/p");
	protected By clubWynCTA = By.xpath(
			"//div[@class='wyn-headline ']/p[contains(.,'Owner')]/ancestor::div[@class='wyn-card__content']//div/a");

	protected By formTitle = By.xpath("//form/div");
	protected By formSubTitle = By.xpath(/* "//form//p/p" */
			"//form//div[@class='body-1']");
	protected By firstNameField = By.xpath("//input[@id='firstName']");
	protected By lastNameField = By.xpath("//input[@id='lastName']");

	protected By wynFirstNameField = By.xpath("//input[@id='firstName']");
	protected By FirstNameField2 = By.xpath("//span[text()='First Name *']");
	protected By wynLastNameField = By.xpath("//input[@id='lastName']");
	protected By LastNameField2 = By.xpath("//span[text()='Last Name *']");
	protected By phoneField = By.id("phoneNumber");
	protected By emailField = By.id("email");
	protected By memberNo = By.xpath("//input[@id='membershipNumber']//following-sibling::span");
	protected By wynFirstNameError = By.xpath("//div[@id='firstname-message' and contains(.,'not a valid name')]");
	protected By wynLastNameError = By.xpath("//div[@id='lastname-message' and contains(.,'not a valid name')]");

	protected By firstNameError = By.xpath("//div[@id='firstName-message' and contains(.,'not a valid name')]");
	protected By lastNameError = By.xpath("//div[@id='lastName-message' and contains(.,'not a valid name')]");
	protected By phoneError = By.xpath("//div[@id='phone-message' and contains(.,'not a valid phone')]");
	protected By emailError = By.xpath("//div[@id='email-message' and contains(.,'not a valid email')]");
	protected By memberNoError = By.xpath("//div[@id='membernumber-message' and contains(.,'not a valid member')]");
	protected By checkBox = By.xpath("//form//input[@type='checkbox']");
	protected By submitButton = By.xpath("//form//button[contains(.,'Submit')]");
	/* "//button[contains(text(),'SUBMIT')]") */
	protected By submitButton1 = By.xpath("//form//button[contains(.,'SUBMIT')]");
	protected By thankConfirm = By.xpath(/* "//div[@class='wyn-message__content']//h4" */
			"//div[contains(@class,'title-2') and contains(text(),'Thank you')]");
	protected By closeMsg = By.xpath("//span[@class='wyn-modal__close wyn-modal__close--secondary']");
	protected By generalNav = By.xpath("//nav[@class='wyn-breadcrumbs']");
	protected By stateSelect = By.xpath("//select[@name='State']");

	// wyndhamCaresPage
	protected By scamBustersHdr = By.xpath(/* "//h2[contains(.,'Scambusters on')]" */
			"(//div[@class='title-1' and contains(text(),'SCAMBUSTERS')])[1]");
	protected By wyndhamCaresLink = By.xpath(
			"//div[contains(@class,'footer')]//ul[@class='no-bullet']//ul[@class='no-bullet']/li/a[contains(text(),'Wyndham Cares')]");

	// protected By wyndhamCaresLink =
	// By.xpath("//div[@gtm_component='footer']//li/a[contains(.,'Wyndham
	// Cares')]");
	protected By wyndhamCaresHeader = By.xpath(/* "//div[@class='wyn-headline ']//p[contains(.,'Wyndham Cares')]" */
			"(//div[contains(@class,'caption-1') and contains(text(),'Wyndham Cares')])[1]");
	protected By wynCaresSubHdr = By.xpath(
			/* "//div[@class='wyn-headline ']//p[contains(.,'Wyndham Cares')]/ancestor::div[@class='wyn-card__content']//h2" */
			"(//div[contains(@class,'caption-1') and contains(text(),'Wyndham Cares')])[1]/../div[@class='title-1']");
	protected By wynCaresDesc = By.xpath(
			"(//div[contains(@class,'caption-1') and contains(text(),'Wyndham Cares')])[1]/../div[@class='body-1']");
	protected By wynCaresBrdCrmb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Wyndham Cares')]");

	protected By pointsOptionsHdr = By.xpath(/* "//h2[contains(.,'Points ')]" */
			"//div[contains(text(),'Points ')]");
	protected By pointsSubHdr = By
			.xpath(/* "//h2[contains(.,'Points ')]/ancestor::div[@class='columns is-marginless']//h3" */
					"//div[contains(text(),'Points ')]//following::ul//div[@class='subtitle-1']");
	protected By pointsMainDesc = By
			.xpath(/* "//h2[contains(.,'Points ')]/ancestor::div[@class='wyn-headline ']/following-sibling::p" */
					"//div[contains(text(),'Points ')]/../div[@class='body-1']");
	protected By learnMoreLink = By
			.xpath(/* "//h2[contains(.,'Points ')]/ancestor::div[@class='columns is-marginless']//p/a" */
					"//div[contains(text(),'Points ')]//following::ul//div//a[contains(text(),'Learn')]");
	protected By pointsSubDesc = By.xpath(
			/* "//h2[contains(.,'Points ')]/ancestor::div[@class='columns is-marginless']//div[@class='wyn-icon-card__copy']/p[1]" */
			"//div[contains(text(),'Points ')]//following::ul//div//p[@class='body-1']");

	protected By contentBlockImgWynCare = By.xpath(/* "//div[@class='contentblock-carousel']//img" */
			"//div[@id='wyndhamCares']//img");
	protected By contentBlockHdrs = By.xpath(/* "//div[@class='contentblock-carousel']//h2" */
			"//div[@id='wyndhamCares']//div[contains(@class,'subtitle-2')]");
	protected By contentBlockDesc = By
			.xpath("//div[@class='contentblock-carousel']//div[@class='wyn-l-content wyn-card__copy']");
	protected By needHelpSection = By.xpath(/* "//div[@class='background-container']//p[contains(.,'Need Help')]" */
			"//h3[contains(text(),'Need Help?')]");
	protected By needHelpContact = By.xpath(/* "//div[@class='background-container']//p[contains(.,'Contact')]" */
			"//h3[contains(text(),'Need Help?')]/../h6[contains(text(),'Contact')]");

	protected By needHelpFormHdr = By.xpath(/* "//form/div[contains(.,'Need Help')]" */
			/* "//div[contains(@class,'title-2') and contains(text(),'Need Help')]" */
			"//div[contains(@class,'title-2') and contains(text(),'Ownership Concerns')]|//div[contains(@class,'title-2') and contains(text(),'Need Help')]");

	// about us
	@FindBy(xpath = /* "//div[@gtm_component='footer']//li/a[contains(.,'About')]" */"//div[contains(@class,'footer')]//ul[@class='no-bullet']//ul[@class='no-bullet']/li/a[contains(text(),'About Us')]")
	WebElement aboutUsLink;
	protected By aboutUsHeader = By.xpath(/* "//div[@class='wyn-headline ']/h2[contains(.,'About Us')]" */
			"(//div[@class='title-1' and contains(text(),'ABOUT US')])[1]");
	protected By aboutUsHeroImg = By.xpath(/* "//div[@class='wyn-hero__image']/img" */
			"(//div[@class='title-1' and contains(text(),'ABOUT US')])[1]/../../../..//img");
	protected By aboutUsNav = By.xpath("//div[@class='breadcrumb']//a[contains(.,'About')]");
	protected By aboutUsDesc = By.xpath(/* "//div[@id='main-content']/div[1]//p" */
			"(//div[@class='title-1' and contains(text(),'ABOUT US')])[1]//..//div[@class='body-1']/p");
	protected By whatIsWynDestHdr = By.xpath("//h3[contains(.,'What Is Wyndham Dest')]");
	protected By whatIsWynDestDesc = By.xpath(
			/* "//h2[contains(.,'What Is Wyndham Dest')]/parent::div/following-sibling::div/p" */"//h3[contains(.,'What Is Wyndham Dest')]/parent::div//following::div[@class='contentSlice']//p");

	protected By brandCardsImg = By.xpath(/* "//div[@class='cards']//img" */
			"//div[@class='cardComponent']//img");
	protected By brandHdr = By.xpath(/* "//div[@class='cards']//h3" */
			"//div[@class='cardComponent']//div[contains(@class,'subtitle-2')]");
	protected By brandDesc = By.xpath(/* "//div[@class='cards']//p" */
			"//div[@class='cardComponent']//div[contains(@class,'body-1')]/p");
	protected By brandLearnMore = By
			.xpath(/* "//div[@class='cards']//div[@class='wyn-card__cta' and contains(.,'Learn More')]" */
					"//div[@class='cardComponent']//div[contains(@class,'body-1') and contains(text(),'Learn')]");

	// div[@gtm_component_type='twoColumnMedia']
	protected By rciVacayImages = By.xpath(/* "//div[@gtm_component_type='twoColumnMedia']//img" */
			"//section[@class='banner']//img");
	protected By rciVacayHdr = By.xpath(/* "//div[@gtm_component_type='twoColumnMedia']//h2" */
			"//section[@class='banner']//div[contains(@class,'title-1') and contains(text(),'RCI')]");
	// protected By rciVacayDec =
	// By.xpath("//div[@gtm_component_type='twoColumnMedia']//p[1]");
	protected By rciVacayDec = By.xpath(/* "//div[@gtm_component_type='twoColumnMedia']//descendant::p[1]" */
			"//section[@class='banner']//div[contains(@class,'title-1') and contains(text(),'RCI')]/../div[contains(@class,'subtitle-3')]");
	protected By rciSubDesc = By.xpath(
			"//section[@class='banner']//div[contains(@class,'title-1') and contains(text(),'RCI')]/../div[contains(@class,'body-1')]");

	protected By rciLearnMore = By.xpath(/* "//div[@gtm_component_type='twoColumnMedia']//a[contains(.,'Learn')]" */
			"//section[@class='banner']//div[contains(@class,'title-1') and contains(text(),'RCI')]//../a[text()='Learn More']");

	// publications

	protected By publicationLink = By.xpath(
			"//div[contains(@class,'footer')]//ul[@class='no-bullet']//ul[@class='no-bullet']/li/a[contains(text(),'Publications')]");
	protected By publicationHdr = By.xpath(/* "//h2[contains(.,'Publication')]" */
			"(//div[contains(text(),'PUBLICATIONS')])[1]");
	protected By publicationsIamge = By
			.xpath(/* "//h2[contains(.,'Publication')]/ancestor::div[@class='wyn-hero wyn-hero--large']//img" */
					"((//div[contains(text(),'PUBLICATIONS')])[1]/ancestor::div//section[contains(@class,'banner')])[1]");
	protected By publicationBrdCrm = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Publication')]");
	protected By publicationsDesc = By.xpath(/* "//div[@class='wyn-rich-text wyn-l-content']//p" */
			"((//div[contains(text(),'PUBLICATIONS')])[1]/ancestor::div//section[contains(@class,'banner')])[1]//p");

	protected By publicationCardsImg = By.xpath(/* "//div[@class='cards']//img" */
			"//div[@class='cardComponent']//img");
	protected By publicationsCardsHdr = By.xpath(/* "//div[@class='cards']//h3" */
			"//div[@class='cardComponent']//div[contains(@class,'subtitle-2')]");
	protected By publicationsCardsDesc = By.xpath(/* "//div[@class='cards']//p" */
			"//div[@class='cardComponent']//div[contains(@class,'body-1')]/p");
	protected By publicationsCardsCta = By.xpath(/* "//div[@class='cards']//div[@class='wyn-button-cta']" */
			"//div[@class='cardComponent']//div[contains(@class,'body-1') and contains(text(),'View')]");

	// Glossary
	protected By glossaryPageNav = By.xpath(/* "//div[@class='wyn-fly-out']//ul//li[contains(.,'Glossary')]/a" */
			"//div[contains(@class,'global-navigation')]//li/a[contains(.,'Glossary')]");
	protected By glossaryImg = By.xpath(/* "//div[@class='wyn-hero__image']//img" */
			"(//div[@class='title-1' and contains(text(),'GLOSSARY')])[1]/ancestor::div[@class='banner']//img");
	protected By glossaryHeader = By.xpath(/* "//h2[contains(.,'Glossary')]" */
			"(//div[@class='title-1' and contains(text(),'GLOSSARY')])[1]");
	protected By glossaryBrdCrmb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Glossary')]");
	protected By pageHeader = By.xpath(/* "//h2[contains(.,'Club Wyndham Glossary')]" */
			"//h3[contains(.,'Club Wyndham Glossary')]");
	protected By navigationLetters = By.xpath(/* "//div[@class='wyn-rich-text wyn-l-content']//h5[@style]/a" */
			"//div[@class='contentSlice']//div[contains(@class,'title-1')]");
	protected By mailLetters = By.xpath(/* "//div[@class='wyn-rich-text wyn-l-content']//h1" */
			"//div[@class='contentSlice']//div[contains(@class,'body-1')]//h5");
	protected By backToTopCTA = By.xpath(/*
											 * "//div[@class='wyn-rich-text wyn-l-content']//a[contains(.,'Back to top')]"
											 */
			"//div[@class='contentSlice']//div[contains(@class,'title-1')]//..//..//..//..//..//a[text()='Back to top']");

	protected By footerHelp = By.xpath("//div[contains(@class,'footer')]//span[text()='Help']");

	/*
	 * Method: helpPageNavigation Description: Help page Navigation: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void helpPageNavigation() {

		waitUntilElementVisibleBy(driver, helpNavHeader, 120);

		if (verifyObjectDisplayed(helpNavHeader)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
					"Help tab present in main Nav");
			try {
				if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
					scrollDownForElementJSBy(footerHelp);
					clickElementBy(footerHelp);
				}
			} catch (Exception e) {
				clickElementJSWithWait(helpNavHeader);
			}

			waitUntilElementVisibleBy(driver, helpBreadCrumb, 120);

			if (verifyObjectDisplayed(helpBreadCrumb)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
						"Help page Navigated, Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
						"Help page nav failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
					"Help Tab not present in main Nav");
		}

	}

	/*
	 * Method: helpPageValidations Description: Help page Linsk and Sections
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void helpPageValidations() {

		if (verifyObjectDisplayed(helpHeader)) {

			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
					"Help header present as " + getElementText(helpHeader));

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
					"Help header not present");
		}

		if (verifyObjectDisplayed(helpHeaderImg)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
					"Help page hero Image present");

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
					"Help page hero Image not present");
		}

		if (verifyObjectDisplayed(helpSubHeader)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
					"Help sub header present as " + getElementText(helpSubHeader));

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
					"Help sub header not present");
		}

		if (verifyObjectDisplayed(helpDesc)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.PASS,
					"Help Description presents");

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageNavigation", Status.FAIL,
					"Help Description not  presents");
		}

	}

	/*
	 * Method: contactClubWyndhamVal Description: Contact Club Wyndham
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void contactClubWyndhamVal() {

		getElementInView(contactClubWynHeader);

		if (verifyObjectDisplayed(contactClubWynHeader)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "contactClubWyndhamVal", Status.PASS,
					"Contact Club Wyndham Header present as:  " + getElementText(contactClubWynHeader));
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "contactClubWyndhamVal", Status.FAIL,
					"Contact Club Wyndham Header not present");
		}
		if (verifyObjectDisplayed(contactClubwynDesc)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "contactClubWyndhamVal", Status.PASS,
					"Contact CW description present");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "contactClubWyndhamVal", Status.FAIL,
					"Contact CW description not present");
		}

		List<WebElement> contactCWSubHdrLst = driver.findElements(contactClubWynSubHeaders);
		List<WebElement> contactCWSubDescLst = driver.findElements(contactClubWynSubDesc);
		for (int i = 0; i < contactCWSubHdrLst.size(); i++) {
			tcConfig.updateTestReporter("NextGenHelpPage", "contactClubWyndhamVal", Status.PASS,
					"Contact CW sub header " + (i + 1) + " present as: " + getElementText(contactCWSubHdrLst.get(i)));
		}

		if (contactCWSubDescLst.size() >= 3) {
			tcConfig.updateTestReporter("NextGenHelpPage", "contactClubWyndhamVal", Status.PASS,
					"Contact CW sub description present in all headers");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "contactClubWyndhamVal", Status.FAIL,
					"Contact CW sub description not present in all headers");
		}

	}

	/*
	 * Method: helpPageCardsVal Description: Help Page Cards Section
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void helpPageCardsVal() {

		List<WebElement> subSectionList = driver.findElements(helpCardsHdr);
		getElementInView(subSectionList.get(0));

		// List<WebElement> subSectionReadMore=
		// driver.findElements(helpCardReadMore);
		tcConfig.updateTestReporter("NextGenHelpPage", "helpPageCardsVal", Status.PASS,
				"Total Sub Articles: " + subSectionList.size());
		for (int i = 0; i < subSectionList.size(); i++) {
			List<WebElement> subSectionList1 = driver.findElements(helpCardsHdr);
			List<WebElement> subSectionReadMore = driver.findElements(helpCardReadMore);
			getElementInView(subSectionList1.get(i));
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageCardsVal", Status.PASS,
					"Header No " + (i + 1) + " is " + subSectionList1.get(i).getText());
			String strmainNav = subSectionList1.get(i).getText().toUpperCase();
			clickElementJS(subSectionReadMore.get(i));

			waitUntilElementVisibleBy(driver, generalNav, 120);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (getElementText(generalNav).toUpperCase().contains(strmainNav)) {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerHelpSubLinks", Status.PASS,
						"Navigation to " + (i + 1) + " link sucessful with header " + driver.getTitle());

				// driver.switchTo().activeElement().sendKeys(Keys.END);
				driver.navigate().back();
				waitUntilElementVisibleBy(driver, helpHeader, 120);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("NextGenFooterPage", "footerHelpSubLinks", Status.FAIL,
						"Navigation to " + (i + 1) + " link failed");
			}

		}

		List<WebElement> subSectionImage = driver.findElements(helpCardsImg);
		List<WebElement> subSectionDesc = driver.findElements(helpCardDesc);

		if (subSectionImage.size() == subSectionList.size()) {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageCardsVal", Status.PASS,
					"Images present in all section");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageCardsVal", Status.FAIL,
					"Images not present in all section");
		}

		if (subSectionDesc.size() >= subSectionList.size()) {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageCardsVal", Status.PASS,
					"Desc present in all section");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "helpPageCardsVal", Status.FAIL,
					"Desc not present in all section");
		}

	}

	/*
	 * Method: clubWynOwnerVal Description: Club Wynhdma Owner Validations
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void clubWynOwnerVal() {

		getElementInView(contactClubWynHeader);

		if (verifyObjectDisplayed(clubWynOwnerHdr)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.PASS,
					"Club Wyndham  Owner Header present as:  " + getElementText(contactClubWynHeader));
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.FAIL,
					"Club Wyndham  Owner Header not present");
		}

		if (verifyObjectDisplayed(clubWynOwnerSubHdr)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.PASS,
					"Club Wyndham  Owner SubHeader present as:  " + getElementText(clubWynOwnerSubHdr));
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.FAIL,
					"Club Wyndham  Owner SubHeader not present");
		}

		if (verifyObjectDisplayed(clubWynDesc)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.PASS,
					"Club Wyndham  Owner description present");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.FAIL,
					"Club Wyndham  Owner description not present");
		}

		if (verifyObjectDisplayed(clubWynCTA)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.PASS,
					"Club Wyndham  Owner Login CTA present");

			clickElementBy(clubWynCTA);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));

			if (driver.getTitle().toUpperCase().contains(testData.get("clubWyndham").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.PASS,
						"Navigated to Club Wyndham Page");

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.FAIL,
						"Club Wyndham Login page navigation failed");
				driver.switchTo().window(tabs2.get(0));
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "clubWynOwnerVal", Status.FAIL,
					"Club Wyndham  Owner CTA not present");
		}

	}

	/*
	 * Method: incorrectFormVal Description: Incorrect form Validdations
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void incorrectFormVal() {

		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			getElementInView(formTitle);
			tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameField);
						e.sendKeys(testData.get("firstNameWrongData") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(firstNameField, testData.get("firstNameWrongData"));
					}

					if (verifyObjectDisplayed(firstNameError)) {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
								"First Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"First Name Error not Present");
					}

				} else if (verifyObjectDisplayed(wynFirstNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(wynFirstNameField);
						e.clear();
						e.sendKeys(testData.get("firstNameWrongData") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(wynFirstNameField, testData.get("firstNameWrongData"));
					}

					if (verifyObjectDisplayed(wynFirstNameError)) {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
								"First Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"First Name Error not Present");
					}

				} else {

					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(lastNameField, testData.get("lastNameWrongData"));

					if (verifyObjectDisplayed(lastNameError)) {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
								"Last Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"Last Name Error not Present");
					}

				} else if (verifyObjectDisplayed(wynLastNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(wynLastNameField, testData.get("lastNameWrongData"));

					if (verifyObjectDisplayed(wynLastNameError)) {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
								"Last Name Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"Last Name Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(phoneField, testData.get("phoneWrongData"));

					if (verifyObjectDisplayed(phoneError)) {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
								"Phone Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"Phone Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(emailField, testData.get("emailWrongData"));

					if (verifyObjectDisplayed(emailError)) {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
								"Email Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"Email Error not Present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(memberNo)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Member No Field Present");

					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					// fieldDataEnter(memberNo, );
					driver.switchTo().activeElement().sendKeys(testData.get("phoneWrongData"));

					if (verifyObjectDisplayed(memberNoError)) {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
								"Member No Error Present");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"Member No Error not Present");
					}
				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else

		{
			tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	/*
	 * Method: formValidations Description: Complete Form validations
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void formValidations() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			getElementInView(formTitle);
			tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(wynFirstNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(wynFirstNameField);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(wynFirstNameField, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(wynLastNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(wynLastNameField, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(phoneField, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(emailField, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(memberNo)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Member Number Field present");
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
					// fieldDataEnter(memberNo, );
					driver.switchTo().activeElement().sendKeys(testData.get("memberNo"));
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Member Number Field not present");
					// new
					// Select(driver.findElement(stateSelect)).selectByValue(testData.get("stateSel"));
				}

				if (verifyObjectDisplayed(submitButton)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButton);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitUntilElementVisibleBy(driver, thankConfirm, 50);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Form Submission Successful");

					if (verifyObjectDisplayed(closeMsg)) {
						clickElementBy(closeMsg);
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"Close Btn not present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	/*
	 * Method: wyndhamCaresNav Description: Wyndham Cares Navigation
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamCaresNav() {

		waitUntilElementVisibleBy(driver, wyndhamCaresLink, "120");
		getElementInView(wyndhamCaresLink);
		if (verifyObjectDisplayed(wyndhamCaresLink)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.PASS,
					"Wyndham cares Link Present");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			/*
			 * if(browserName.equalsIgnoreCase("Firefox")){
			 * scrollDownForElementJSWb(wyndhamCaresLink); scrollUpByPixel(150);
			 * 
			 * clickElementJSWithWait(wyndhamCaresLink); }else{
			 */
			/*
			 * Actions action = new Actions(driver);
			 * action.moveToElement(wyndhamCaresLink);
			 * action.click(wyndhamCaresLink); Action ac = action.build();
			 * ac.perform();
			 * waitForSometime(tcConfig.getConfig().get("MedWait"));
			 */
			clickElementJSWithWait(wyndhamCaresLink);
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyObjectDisplayed(wyndhamCaresHeader)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.PASS,
						"Navigated to Wyndham Cares Link page");

				if (verifyObjectDisplayed(wynCaresSubHdr)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.PASS,
							"Wyndham Cares Sub header present as " + getElementText(wynCaresSubHdr));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.FAIL,
							"Wyndham Cares Sub header not present");

				}

				if (verifyObjectDisplayed(wynCaresDesc)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.PASS,
							"Wyndham Cares Hero Description present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.FAIL,
							"Wyndham Cares Hero Description not present");

				}

				if (verifyObjectDisplayed(wynCaresBrdCrmb)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.PASS,
							"Wyn Cares breadcrumb present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.FAIL,
							"Wyn Cares breadcrumb not present");

				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.FAIL,
						"Navigation to Wyndham Cares link failed");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresNav", Status.FAIL,
					"Wyndham cares Link not Present");

		}

	}

	/*
	 * Method: wyndhamCaresSectionVal Description: Wyndham Craes Page
	 * Validations : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void wyndhamCaresSectionVal() {

		getElementInView(pointsOptionsHdr);

		if (verifyObjectDisplayed(pointsOptionsHdr)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.PASS,
					"Points Option Header Section present in the page as " + getElementText(pointsOptionsHdr));

			if (verifyObjectDisplayed(pointsMainDesc)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.PASS,
						"Points Option Main Description present");

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.FAIL,
						"Points Option Main Description not present");
			}

			List<WebElement> pointsSubHdrList = driver.findElements(pointsSubHdr);
			List<WebElement> pointsSubDescList = driver.findElements(pointsSubDesc);
			List<WebElement> pointsSubLearnMoreCTA = driver.findElements(learnMoreLink);
			for (int i = 0; i < pointsSubHdrList.size(); i++) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.PASS,
						"Points sub header " + (i + 1) + " present as: " + getElementText(pointsSubHdrList.get(i)));
			}

			if (pointsSubDescList.size() == 3) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.PASS,
						"Points sub description present in all headers");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.FAIL,
						"Points sub description not present in all headers");
			}

			if (pointsSubLearnMoreCTA.size() == 3) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.PASS,
						"Points Sub Learn More CTA present");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.FAIL,
						"Points Sub Learn More CTA not present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "wyndhamCaresSectionVal", Status.FAIL,
					"Points Option Header Section not present in the page");
		}

	}

	/*
	 * Method: wynCaresContentBlkSection Description: Wyndham cares Content
	 * Block Section Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void wynCaresContentBlkSection() {
		try {

			List<WebElement> contentBlckImageList = driver.findElements(contentBlockImgWynCare);
			List<WebElement> contentBlckHdrsList = driver.findElements(contentBlockHdrs);
			// List<WebElement> contentBlckDescList =
			// driver.findElements(contentBlockDesc);

			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.PASS,
					"Content Blocks present Total: " + contentBlckHdrsList.size());

			for (int i = 0; i < contentBlckHdrsList.size(); i++) {
				getElementInView(contentBlckHdrsList.get(i));
				tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.PASS,
						"Content Block " + (i + 1) + " header is: " + contentBlckHdrsList.get(i).getText());

				if (verifyElementDisplayed(contentBlckImageList.get(i))) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.PASS,
							"Image present in " + (i + 1) + " content block");
				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.FAIL,
							"Image not present in " + (i + 1) + " content block");
				}

				/*
				 * if (verifyElementDisplayed(contentBlckDescList.get(i))) {
				 * tcConfig.updateTestReporter("NextGenHelpPage",
				 * "wynCaresContentBlkSection", Status.PASS,
				 * "Description present in " + (i + 1) + " content block"); }
				 * else { tcConfig.updateTestReporter("NextGenHelpPage",
				 * "wynCaresContentBlkSection", Status.FAIL,
				 * "Description not present in " + (i + 1) + " content block");
				 * }
				 */

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.FAIL,
					"error in Content Block validations");

		}

	}

	public void needHelpSectionValidation() {
		waitUntilElementVisibleBy(driver, needHelpSection, 120);
		getElementInView(needHelpSection);

		if (verifyObjectDisplayed(needHelpSection)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.PASS,
					"Need Help Section present in the page");

			if (verifyObjectDisplayed(needHelpContact)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.PASS,
						"" + getElementText(needHelpContact));

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.FAIL,
						"Need Help Description not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresContentBlkSection", Status.FAIL,
					"Need Help Section not present in the page");
		}
	}

	/*
	 * Method: wynCaresPageFormVal Description: Wyn Cares Page Form Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void wynCaresPageFormVal() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, needHelpFormHdr, 120);
		getElementInView(needHelpFormHdr);
		if (verifyObjectDisplayed(needHelpFormHdr)) {

			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
					"Form Title present with header " + getElementText(needHelpFormHdr));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(wynFirstNameField) || verifyObjectDisplayed(FirstNameField2)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {

						WebElement e = driver.findElement(wynFirstNameField);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnterAction(wynFirstNameField, testData.get("firstName") + Keys.TAB);
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(wynLastNameField) || verifyObjectDisplayed(LastNameField2)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Last Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(lastNameField);
						e.sendKeys(testData.get("lastName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnterAction(wynLastNameField, testData.get("lastName") + Keys.TAB);
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(phoneField, testData.get("phoneNo") + Keys.TAB);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnterAction(emailField, testData.get("email") + Keys.TAB);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(memberNo)) {
					tcConfig.updateTestReporter("NextGenHomePage", "wynCaresPageFormVal", Status.PASS,
							"Member Number Field present");
					fieldDataEnterAction(memberNo, testData.get("memberNo") + Keys.TAB);
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "wynCaresPageFormVal", Status.FAIL,
							"Member Number Field not present");
				}

				try {
					List<WebElement> checkBoxList = driver.findElements(checkBox);
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Total " + checkBoxList.size() + " terms and condition chechkbox present in the page");

					for (int i = 0; i < checkBoxList.size(); i++) {
						clickElementJSWithWait(checkBoxList.get(i));
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"T&C checkbox error");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(submitButton1)) {
					getElementInView(submitButton1);
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButton1);

				} else if (verifyObjectDisplayed(submitButton)) {
					getElementInView(submitButton);
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButton);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitUntilElementVisibleBy(driver, thankConfirm, 240);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.PASS,
							"Form Submission Successful");

					/*
					 * if (verifyObjectDisplayed(closeMsg)) {
					 * clickElementBy(closeMsg); } else {
					 * tcConfig.updateTestReporter("NextGenHelpPage",
					 * "incorrectFormVal", Status.FAIL, "Close Btn not present"
					 * ); }
					 */

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "wynCaresPageFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	/*
	 * Method: wynCaresPageFormVal Description: Wyn Cares Page Form Validations:
	 * Date Dec 03/2019 Author: Unnat Jain Changes By
	 */
	public void wynCareFormSubmit() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, needHelpFormHdr, 120);

		if (verifyObjectDisplayed(needHelpFormHdr)) {
			getElementInView(needHelpFormHdr);
			tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
					"Form Title present with header " + getElementText(needHelpFormHdr));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(wynFirstNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					if (browserName.equalsIgnoreCase("EDGE")) {
						WebElement e = driver.findElement(firstNameField);
						e.clear();
						e.sendKeys(testData.get("firstName") + Keys.TAB);
						driver.switchTo().activeElement().sendKeys(Keys.TAB);
					} else {
						fieldDataEnter(wynFirstNameField, testData.get("firstName"));
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(wynLastNameField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					fieldDataEnter(wynLastNameField, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(phoneField, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					fieldDataEnter(emailField, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				try {
					List<WebElement> checkBoxList = driver.findElements(checkBox);
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Total " + checkBoxList.size() + " terms and condition chechkbox present in the page");

					for (int i = 0; i < checkBoxList.size(); i++) {
						clickElementJSWithWait(checkBoxList.get(i));
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"T&C checkbox error");
				}

				if (verifyObjectDisplayed(submitButton)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Submit Present present");

					clickElementJSWithWait(submitButton);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitUntilElementVisibleBy(driver, thankConfirm, 50);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.PASS,
							"Form Submission Successful without member number");

					if (verifyObjectDisplayed(closeMsg)) {
						clickElementBy(closeMsg);
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
								"Close Btn not present");
					}

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	/*
	 * Method: aboutUsNav Description: About Us Navigation and Validations: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void aboutUsNav() {

		waitUntilElementVisibleWb(driver, aboutUsLink, "120");
		getElementInView(aboutUsLink);
		if (verifyElementDisplayed(aboutUsLink)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.PASS, "About us Link Present");

			if (browserName.equalsIgnoreCase("edge")) {
				clickElementJSWithWait(aboutUsLink);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				Actions action = new Actions(driver);
				action.moveToElement(aboutUsLink);
				action.click(aboutUsLink);
				Action ac = action.build();
				ac.perform();
				driver.switchTo().activeElement().sendKeys(Keys.ENTER);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

			if (verifyObjectDisplayed(aboutUsHeader)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.PASS,
						"Navigated to About Us Link page");

				if (verifyObjectDisplayed(aboutUsHeroImg)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.PASS, "About Us Image present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.FAIL,
							"About Us Image not present");

				}

				if (verifyObjectDisplayed(aboutUsDesc)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.PASS,
							"About Us Hero Description present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.FAIL,
							"About Us Hero Description not present");

				}

				if (verifyObjectDisplayed(aboutUsNav)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.PASS,
							"About Us Navigation present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.FAIL,
							"About Us Navigation not present");

				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.FAIL,
						"Navigation to About Us link failed");

			}

			getElementInView(whatIsWynDestHdr);

			if (verifyObjectDisplayed(whatIsWynDestHdr)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.PASS,
						"What is Wyndham Destination Header present");

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.FAIL,
						"What is Wyndham Destination Header not present");

			}

			List<WebElement> descriptionList = driver.findElements(whatIsWynDestDesc);
			if (verifyObjectDisplayed(whatIsWynDestDesc) && descriptionList.size() == 2) {

				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.PASS,
						"What is Wyndham Destination Desc present");

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.FAIL,
						"What is Wyndham Destination Desc not present");

			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsNav", Status.FAIL, "About us Link not Present");

		}

	}

	/*
	 * Method: aboutUsContentVal Description: About Us Navigation and
	 * Validations: Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void aboutUsContentVal() {
		try {
			List<WebElement> cardsImageList = driver.findElements(brandCardsImg);
			List<WebElement> cardsHdrsList = driver.findElements(brandHdr);
			List<WebElement> cardsDescList = driver.findElements(brandDesc);

			tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsContentVal", Status.PASS,
					"Content Blocks present Total: " + cardsHdrsList.size());

			for (int i = 0; i < cardsHdrsList.size(); i++) {
				getElementInView(cardsHdrsList.get(i));
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsContentVal", Status.PASS,
						"Content Block " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

				if (verifyElementDisplayed(cardsImageList.get(i))) {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsContentVal", Status.PASS,
							"Image present in " + (i + 1) + " content block");
				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsContentVal", Status.FAIL,
							"Image not present in " + (i + 1) + " content block");
				}

				if (verifyElementDisplayed(cardsDescList.get(i))) {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsContentVal", Status.PASS,
							"Description present in " + (i + 1) + " content block");
				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsContentVal", Status.FAIL,
							"Description not present in " + (i + 1) + " content block");
				}

			}

			for (int j = 0; j < cardsHdrsList.size(); j++) {
				List<WebElement> cardsLearnMoreList = driver.findElements(brandLearnMore);
				if (verifyElementDisplayed(cardsLearnMoreList.get(j))) {

					cardsLearnMoreList.get(j).click();

					waitForSometime(tcConfig.getConfig().get("LongWait"));

					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					if (tabs2.size() == 2) {
						driver.switchTo().window(tabs2.get(1));

						if (driver.getCurrentUrl().toUpperCase()
								.contains(testData.get("header" + (j + 1) + "").toUpperCase())) {

							tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
									"Navigated to " + (j + 1) + " page");

							waitForSometime(tcConfig.getConfig().get("MedWait"));
							try {
								if (verifyElementDisplayed(driver
										.findElement(By.xpath("//div[@id='modal-alert']//button[@class='close']")))) {
									driver.findElement(By.xpath("//div[@id='modal-alert']//button[@class='close']"))
											.click();
									waitForSometime(tcConfig.getConfig().get("MedWait"));
								}
							} catch (Exception e) {

							}
							driver.close();
							waitForSometime(tcConfig.getConfig().get("MedWait"));

							driver.switchTo().window(tabs2.get(0));

						}
					} else {
						if (driver.getCurrentUrl().toUpperCase()
								.contains(testData.get("header" + (j + 1) + "").toUpperCase())) {
							tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
									"Navigated to " + (j + 1) + " page");
							waitForSometime(tcConfig.getConfig().get("MedWait"));
							driver.navigate().back();

							waitUntilElementVisibleBy(driver, brandLearnMore, 120);
						}
					}

				} else {
					tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.FAIL,
							"Navigated to " + (j + 1) + " page failed");
				}
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsContentVal", Status.FAIL,
					"Brands Cards are not present in the page");
		}
	}

	/*
	 * Method: aboutUsTwoConVal Description: About Us Two Container Val: Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void aboutUsTwoConVal() {

		try {
			waitUntilElementVisibleBy(driver, rciVacayImages, 120);
			getElementInView(rciVacayImages);

			if (verifyObjectDisplayed(rciVacayImages)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.PASS,
						"RCI Image is displayed");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.FAIL,
						"RCI Image is not displayed");
			}

			if (verifyObjectDisplayed(rciVacayHdr)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.PASS,
						"RCI Header is displayed");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.FAIL,
						"RCI header is not displayed");
			}

			if (verifyObjectDisplayed(rciVacayDec)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.PASS,
						"RCI Description is displayed");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.FAIL,
						"RCI Description is not displayed");
			}

			if (verifyObjectDisplayed(rciSubDesc)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.PASS,
						"RCI Sub Description is displayed");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.FAIL,
						"RCI Sub Description is not displayed");
			}

			if (verifyObjectDisplayed(rciLearnMore)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.PASS,
						"RCI Learn More button is displayed");
				clickElementBy(rciLearnMore);

				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));

				if (driver.getCurrentUrl().toUpperCase().contains(testData.get("subHeader1").toUpperCase())) {

					tcConfig.updateTestReporter("BuyACWTimeshare", "twoContainerValidations", Status.PASS,
							"Navigated to RCI page");

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

					waitUntilElementVisibleBy(driver, rciLearnMore, 120);

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.FAIL,
							"RCI Learn More button is not displayed");
				}

				/*
				 * List<WebElement> cardsImageList =
				 * driver.findElements(rciVacayImages); List<WebElement>
				 * cardsHdrsList = driver.findElements(rciVacayHdr);
				 * List<WebElement> cardsDescList =
				 * driver.findElements(rciVacayDec);
				 * 
				 * // List<WebElement> cardsLearnMoreList = //
				 * driver.findElements(rciLearnMore);
				 * 
				 * tcConfig.updateTestReporter("NextGenHelpPage",
				 * "aboutUsTwoConVal", Status.PASS,
				 * "Two Content Blocks present Total: " + cardsHdrsList.size());
				 * 
				 * for (int i = 0; i < cardsHdrsList.size(); i++) {
				 * getElementInView(cardsHdrsList.get(i));
				 * tcConfig.updateTestReporter("NextGenHelpPage",
				 * "aboutUsTwoConVal", Status.PASS, "Two Content Block " + (i +
				 * 1) + " header is: " + cardsHdrsList.get(i).getText());
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
				 * (verifyElementDisplayed(cardsImageList.get(i))) {
				 * tcConfig.updateTestReporter("NextGenHelpPage",
				 * "aboutUsTwoConVal", Status.PASS, "Image present in " + (i +
				 * 1) + " content block"); } else {
				 * tcConfig.updateTestReporter("NextGenHelpPage",
				 * "aboutUsTwoConVal", Status.FAIL, "Image not present in " + (i
				 * + 1) + " content block"); }
				 * 
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * 
				 * getElementInView(cardsDescList.get(i)); if
				 * (verifyElementDisplayed(cardsDescList.get(i))) {
				 * tcConfig.updateTestReporter("NextGenHelpPage",
				 * "aboutUsTwoConVal", Status.PASS, "Description present in " +
				 * (i + 1) + " content block"); } else {
				 * tcConfig.updateTestReporter("NextGenHelpPage",
				 * "aboutUsTwoConVal", Status.FAIL,
				 * "Description not present in " + (i + 1) + " content block");
				 * }
				 * 
				 * }
				 * 
				 * for (int j = 0; j < cardsHdrsList.size(); j++) {
				 * List<WebElement> cardsLearnMoreList1 =
				 * driver.findElements(rciLearnMore); if
				 * (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {
				 * 
				 * clickElementJSWithWait(cardsLearnMoreList1.get(j));
				 * 
				 * waitForSometime(tcConfig.getConfig().get("LongWait"));
				 * 
				 * // driver.navigate().refresh();
				 * 
				 * if (driver.getCurrentUrl().toUpperCase()
				 * .contains(testData.get("subHeader" + (j + 1) +
				 * "").toUpperCase())) {
				 * 
				 * tcConfig.updateTestReporter("BuyACWTimeshare",
				 * "aboutUsTwoConVal", Status.PASS, "Navigated to " + (j + 1) +
				 * " page");
				 * 
				 * } else { tcConfig.updateTestReporter("BuyACWTimeshare",
				 * "aboutUsTwoConVal", Status.FAIL, "Navigated to " + (j + 1) +
				 * " page failed"); } driver.navigate().back();
				 * 
				 * waitUntilElementVisibleBy(driver, aboutUsHeader, 120);
				 * 
				 * } else { tcConfig.updateTestReporter("BuyACWTimeshare",
				 * "aboutUsTwoConVal", Status.FAIL,
				 * "Learn more link not present in the content for " + (j + 1) +
				 * " block"); } }
				 */
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHelpPage", "aboutUsTwoConVal", Status.FAIL,
					"Brands Cards are not present in the page " + e.getMessage());
		}
	}

	/*
	 * Method: publicationsPageNav Description: Publicatiosns page Validations:
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */

	protected By skipButton = By.xpath("//div[@id='tutorialButtonSkip']");
	protected By frame = By.xpath("//iframe[@title='menus']");

	public void publicationsPageNav() {

		waitUntilElementVisibleBy(driver, publicationLink, "120");
		getElementInView(publicationLink);
		if (verifyObjectDisplayed(publicationLink)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS, "Publications Link Present");

			clickElementBy(publicationLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(publicationHdr)) {
				tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
						"Navigated to Publications Link page");

				if (verifyObjectDisplayed(publicationsIamge)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
							"Publications Image present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
							"Publications Image not present");

				}

				if (verifyObjectDisplayed(publicationsDesc)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
							"Publications Hero Description present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
							"Publications Hero Description not present");

				}

				if (verifyObjectDisplayed(publicationBrdCrm)) {
					tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
							"Publications Navigation present");

				} else {
					tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
							"Publications Navigation not present");

				}

			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
						"Navigation to Publications link failed");

			}

			try {
				List<WebElement> cardsImageList = driver.findElements(publicationCardsImg);
				List<WebElement> cardsHdrsList = driver.findElements(publicationsCardsHdr);
				List<WebElement> cardsDescList = driver.findElements(publicationsCardsDesc);
				// List<WebElement> cardsLearnMoreList =
				// driver.findElements(rciLearnMore);

				tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
						"Publications cards present Total: " + cardsHdrsList.size());

				for (int i = 0; i < cardsHdrsList.size() - 4; i++) {
					getElementInView(cardsHdrsList.get(i));
					tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
							"Card No " + (i + 1) + " header is: " + cardsHdrsList.get(i).getText());

					if (verifyElementDisplayed(cardsImageList.get(i))) {
						tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
								"Image present in " + (i + 1) + " content block");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
								"Image not present in " + (i + 1) + " content block");
					}

					if (verifyElementDisplayed(cardsDescList.get(i))) {
						tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
								"Description present in " + (i + 1) + " content block");
					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
								"Description not present in " + (i + 1) + " content block");
					}

				}

				for (int j = 0; j < cardsHdrsList.size() - 4; j++) {
					List<WebElement> cardsLearnMoreList1 = driver.findElements(publicationsCardsCta);
					if (verifyElementDisplayed(cardsLearnMoreList1.get(j))) {

						cardsLearnMoreList1.get(j).click();

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						/*
						 * ArrayList<String> tabs2 = new
						 * ArrayList<String>(driver.getWindowHandles());
						 * driver.switchTo().window(tabs2.get(1));
						 */

						try {
							// waitUntilObjectVisible(driver, skipButton, 120);
							WebElement eleFrame = driver.findElement(frame);
							driver.switchTo().frame(eleFrame);
							if (verifyElementDisplayed(driver.findElement(skipButton))) {
								clickElementJSWithWait(skipButton);
							}
							driver.switchTo().defaultContent();
						} catch (Exception e) {

						}

						if (driver.getCurrentUrl().toUpperCase()
								.contains(testData.get("header" + (j + 1) + "").toUpperCase())) {

							tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.PASS,
									"Navigated to " + (j + 1) + " page");

						} else {
							tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
									"Navigated to " + (j + 1) + " page failed");
						}
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						/*
						 * driver.close();
						 * waitForSometime(tcConfig.getConfig().get("MedWait"));
						 */

						/* driver.switchTo().window(tabs2.get(0)); */

						driver.navigate().back();
						waitUntilElementVisibleBy(driver, publicationsCardsCta, 120);

					} else {
						tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
								" CTA link not present in the content for " + (j + 1) + " block");
					}
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
						"Publications Cards are not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "publicationsNav", Status.FAIL,
					"Publications Link not Present");

		}

	}

	/*
	 * Method: navigateToScambuster Description: Scambuster page navigateion:
	 * Date Dec/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToScambuster() {
		String appURL = testData.get("URL");

		appURL = appURL.concat("/us/en/help/wyndham-cares/scambusters");
		driver.navigate().to(appURL);

		waitUntilElementVisibleBy(driver, scamBustersHdr, 120);

		if (verifyObjectDisplayed(scamBustersHdr)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToScambuster", Status.PASS,
					"Scambusters page Navigated, Header present");

		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToScambuster", Status.FAIL,
					"Scambusters Page navigation failed");

		}

	}

	/*
	 * Method: navigateToGlossary Description: Glossary page navigation: Date
	 * Dec/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToGlossary() {

		waitUntilElementVisibleBy(driver, helpNavHeader, 120);
		hoverOnElement(helpNavHeader);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(glossaryPageNav)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
					"Glossary present in Sub Nav");
			driver.findElement(glossaryPageNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
					"Glossary not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, glossaryHeader, 120);

		if (verifyObjectDisplayed(glossaryHeader)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
					"Glossary Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
					"Glossary Page Navigation failed");
		}

	}

	/*
	 * Method: glossaryPageValidations Description: Glossary page Validations:
	 * Date Dec/2019 Author: Unnat Jain Changes By
	 */
	public void glossaryPageValidations() {
		if (verifyObjectDisplayed(glossaryHeader)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
					"Glossary Page Header present");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
					"Glossary Page Header not present");
		}

		if (verifyObjectDisplayed(glossaryImg)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
					"Glossary Page Hero Image present");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
					"Glossary Page Hero Image not present");
		}

		if (verifyObjectDisplayed(glossaryBrdCrmb)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
					"Glossary Page Breadcrumb present");
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
					"Glossary Page Breadcrumb not present");
		}

		if (verifyObjectDisplayed(pageHeader)) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
					"Glossary Page sub header present as: " + getElementText(pageHeader));
		} else {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
					"Glossary Page sub header  not present");
		}

		try {
			List<WebElement> navLetterList = driver.findElements(navigationLetters);
			List<WebElement> letterHdrList = driver.findElements(mailLetters);
			List<WebElement> backToTopList = driver.findElements(backToTopCTA);
			getElementInView(navLetterList.get(7));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// int locA1 = navLetterList.get(7).getLocation().getY();
			// int locA2 = letterHdrList.get(7).getLocation().getY();
			// int locx1 = navLetterList.get(7).getLocation().getX();
			//
			JavascriptExecutor js = (JavascriptExecutor) driver;
			Long beforeScroll = (Long) js.executeScript("return window.pageYOffset;");

			if (navLetterList.size() == 26) {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
						"Total 26 Navigation Letters Link Displayed");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
						"Total 26 Navigation Letters Link not Displayed");
			}

			if (letterHdrList.size() == 65) {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
						"Total 26 Letter Headers Displayed");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
						"Total 26 Letter Headers  not Displayed");
			}

			if (backToTopList.size() == 26) {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
						"Total 26 Back To Top Link Displayed");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
						"Total 26 Back To Top Link not Displayed");
			}

			navLetterList.get(7).click();

			Long afterScroll = (Long) js.executeScript("return window.pageYOffset;");

			// int locB1 = navLetterList.get(7).getLocation().
			// int locx = navLetterList.get(7).getLocation().getX();
			// int locB2 = letterHdrList.get(7).getLocation().getY();
			// int locA3 = backToTopList.get(7).getLocation().getY();

			if (beforeScroll == afterScroll) {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
						"Navigation Link Scroll Functionality not Working As Expected");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
						"Navigation Link Scroll Functionality Working As Expected");
			}

			// if (locA2 == locB2) {
			// tcConfig.updateTestReporter("NextGenHelpPage",
			// "navigateToGlossary", Status.FAIL,
			// "Did not Scroll to letter: " + navLetterList.get(7).getText());
			// } else {
			// tcConfig.updateTestReporter("NextGenHelpPage",
			// "navigateToGlossary", Status.PASS,
			// "Scrolled to letter: " + navLetterList.get(7).getText());
			// }

			Long beforeScroll1 = (Long) js.executeScript("return window.pageYOffset;");

			getElementInView(backToTopList.get(7));
			backToTopList.get(7).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			int locB3 = backToTopList.get(7).getLocation().getY();
			Long afterScroll1 = (Long) js.executeScript("return window.pageYOffset;");
			if (beforeScroll1 == afterScroll1) {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
						"Back To Top Link did not work as expected");
			} else {
				tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.PASS,
						"Back To Top Link working as expected");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NextGenHelpPage", "navigateToGlossary", Status.FAIL,
					"Error in scrolling functionality validations");
		}

	}

	public void navigatetoPublicationPage() {
		waitUntilElementVisibleBy(driver, publicationLink, "120");
		getElementInView(publicationLink);
		if (verifyObjectDisplayed(publicationLink)) {
			tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.PASS,
					"Publications Link Present");
			clickElementBy(publicationLink);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(publicationHdr)) {
				tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.PASS,
						"Navigated to Publications Link page");
			} else {
				tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.FAIL,
						"Not Navigated to Publications Link page");
			}
		} else {
			tcConfig.updateTestReporter("NextGenPublicationPage", "navigatetoPublicationPage", Status.FAIL,
					"Publications Link not Present");
		}
	}

	protected By formHeader = By
			.xpath("//form[@id='orderDirectory']//div[contains(@class,'subtitle-1') and contains(text(),'Order')]");
	protected By formSubHeader = By
			.xpath("//form[@id='orderDirectory']//div[contains(@class,'body-1') and contains(text(),'Fill out')]");
	protected By memberID = By.xpath("//span[contains(text(),'Membership Number *')]");
	protected By memberInputfield = By.xpath(/* "//input[@name='memberNumber']" */
			"//input[@name='membershipNumber']");
	protected By memberInputError = By.xpath(/* "//span[contains(@class,'memberNumberError is-visible')]" */
			"//span[contains(@class,'membershipNumberError is-visible')]");
	protected By Address1Label = By.xpath("//span[contains(text(),'Address 1 *')]");
	protected By Address1Inputfield = By.xpath("//input[@name='addressLine1']");
	protected By Address1Error = By.xpath("//span[contains(@class,'addressLine1Error is-visible')]");
	protected By Address2Error = By.xpath("//span[contains(@class,'addressLine2Error is-visible')]");
	protected By Address2Label = By.xpath("//span[contains(text(),'Address 2')]");
	protected By Address2Inputfield = By.xpath("//input[@name='addressLine2']");
	protected By cityLabel = By.xpath("//span[contains(text(),'City *')]");
	protected By cityInputfield = By.xpath("//input[@name='city']");
	protected By cityError = By.xpath("//span[contains(@class,'cityError is-visible')]");
	protected By stateLabel = By.xpath("//span[contains(text(),'State *')]");
	protected By stateDropDown = By.xpath("//select[@id='state']");
	protected By zipCodeLabel = By.xpath("//span[contains(text(),'Zip Code *')]");
	protected By zipCodeInput = By.xpath("//input[@name='postalCode']");
	protected By zipCodeError = By.xpath("//span[contains(@class,'postalCodeError is-visible')]");
	protected By leadIDCheckbox = By.xpath("//input[@id='leadAgree']");
	protected By submitBtn = By.xpath("//button[contains(text(),'SUBMIT')]");
	protected By successMessage = By
			.xpath("//div[@class='title-2' and contains(text(),'Your Order Has Been Successfully Submitted!')]");
	protected By successSubDescription = By
			.xpath("//div[@class='body-1']/p[text()='Please allow 7-10 business days for delivery.']");

	public void validatePublicationFormfields() {
		getElementInView(formHeader);
		assertTrue(verifyObjectDisplayed(formHeader), "Publication Form header is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Publication Form header is Present");

		assertTrue(verifyObjectDisplayed(formSubHeader), "Publication Form Sub header is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Publication Form Sub header is Present");

		assertTrue(verifyObjectDisplayed(memberID), "Member ID Label is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Member ID Label is Present");

		assertTrue(verifyObjectDisplayed(Address1Label), "Publication Form Address 1 Label is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Publication Form Address 1 Label is Present");

		assertTrue(verifyObjectDisplayed(Address2Label), "Publication Form Address 2 Label is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Publication Form Address 2 Label is Present");

		assertTrue(verifyObjectDisplayed(cityLabel), "Publication Form City Label is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Publication Form City Label is Present");

		assertTrue(verifyObjectDisplayed(stateLabel), "Publication Form State Label is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Publication Form State Label is Present");

		assertTrue(verifyObjectDisplayed(zipCodeLabel), "Publication Form Zip code Label is Present");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Publication Form Zip code Label is Present");

		assertTrue(getElementAttribute(submitBtn, "class").contains("disabled"), "Submit button is Disabled");
		tcConfig.updateTestReporter("PublicationPage", "validatePublicationFormfields", Status.PASS,
				"Submit button is Disabled");
	}

	public void validateFormError() {
		clickElementJSWithWait(memberInputfield);
		fieldDataEnter(memberInputfield, testData.get("memberNo") + Keys.ENTER);
		assertTrue(verifyObjectDisplayed(memberInputError), "Error Message for Wrong Member ID is not displayed");
		tcConfig.updateTestReporter("PublicationPage", "validateFormError", Status.PASS,
				"Error Message for Wrong Member ID is displayed");

		clickElementJSWithWait(zipCodeInput);
		fieldDataEnter(zipCodeInput, testData.get("ZipCodeWrongData") + Keys.ENTER);
		assertTrue(verifyObjectDisplayed(zipCodeError), "Error Message for Wrong Zip Code is not displayed");
		tcConfig.updateTestReporter("PublicationPage", "validateFormError", Status.PASS,
				"Error Message for Wrong Code is displayed");

		driver.navigate().refresh();

	}

	public void fillUpOrderDirectoryForm() {
		if (verifyObjectDisplayed(formHeader)) {

			tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
					"Order Directory form is Present");

			getElementInView(Address1Inputfield);

			if (verifyObjectDisplayed(Address1Inputfield)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Address 1 Field Present");

				if (browserName.equalsIgnoreCase("EDGE")) {
					WebElement e = driver.findElement(Address1Inputfield);
					e.clear();
					e.sendKeys(testData.get("Address") + Keys.TAB);
					driver.switchTo().activeElement().sendKeys(Keys.TAB);
				} else {
					fieldDataEnter(Address1Inputfield, testData.get("Address"));
				}

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Address 1 Field not present");
			}

			if (verifyObjectDisplayed(Address2Inputfield)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Address 2 Field Present");

				fieldDataEnter(Address2Inputfield, testData.get("Address"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Address 2 Field not present");
			}

			if (verifyObjectDisplayed(cityInputfield)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"City Field Present");

				fieldDataEnter(cityInputfield, testData.get("City"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"City Field not present");
			}

			if (verifyObjectDisplayed(stateDropDown)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"State Drop Down present");

				new Select(driver.findElement(stateDropDown)).selectByVisibleText(testData.get("stateSel"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"State Drop Down not present");
			}

			if (verifyObjectDisplayed(zipCodeInput)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Zip Field Present");

				fieldDataEnter(zipCodeInput, testData.get("ZipCode"));

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Zip Field not present");
			}

			if (getElementAttribute(submitBtn, "class").contains("disabled")) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Submit Button Disabled before selecting the Confirm Checkbox");

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Agree Button Enabled before selecting the Confirm Checkbox");
			}

			getElementInView(leadIDCheckbox);
			if (verifyObjectDisplayed(leadIDCheckbox)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Confirm Checkbox is Present");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(leadIDCheckbox);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.switchTo().activeElement().sendKeys(Keys.TAB);

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Confirm Checkbox is not Present");
			}
			waitForSometime(tcConfig.getConfig().get("LongWait"));
			if (verifyObjectDisplayed(submitBtn)) {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
						"Agree Button Enabled After selecting the Confirm Checkbox");
				clickElementJSWithWait(submitBtn);
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				waitUntilElementVisibleBy(driver, successMessage, 240);

				if (verifyObjectDisplayed(successMessage) && verifyObjectDisplayed(successSubDescription)) {
					tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.PASS,
							"Form Submission Successful");

				} else {
					tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextgenPublicationsPage", "fillUpOrderDirectoryForm", Status.FAIL,
						"Agree Button disabled after selecting the Confirm Checkbox");
			}

		}
	}

}
