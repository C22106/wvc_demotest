package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenFooterPage_Android extends NextGenFooterPage_Web {

	public static final Logger log = Logger.getLogger(NextGenFooterPage_Android.class);

	public NextGenFooterPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
