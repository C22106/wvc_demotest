package nextgen.pages;

import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class CUIReservationDetailsPage_Web extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(CUIReservationDetailsPage_Web.class);

	public CUIReservationDetailsPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}
	protected By cancellationPolicyHeader = By
			.xpath("//div[contains(@class,'hide-for-large')]/div/h3");
	protected By cancellationPolicyDescription = By
			.xpath("//div[contains(@class,'hide-for-large')]/div/h3/following-sibling::p");
	protected By cancelledResortHeader = By.xpath("// div[contains(@class,'reservation')]//h1/a");
	protected By cancellationHeader = By
			.xpath("// div[contains(@class,'show')]//header[contains(@class,'cancellation')]");
	protected By cancellationCheckmark = By
			.xpath("// div[contains(@class,'show')]//header[contains(@class,'cancellation')]//*[local-name()='svg']");
	protected By cancellationDate = By
			.xpath("// div[contains(@class,'show')]//span[contains(.,'Date Cancel')]/following-sibling::span");
	protected By cancellationBy = By
			.xpath("// div[contains(@class,'show')]//span[contains(.,'Cancelled By')]/following-sibling::span");

	protected By reimbursementHeader = By.xpath("// h2[contains(.,'Reimbursement')]");
	protected By totalReimbursePoints = By.xpath("// span[contains(.,'Points')]/following-sibling::span");
	protected By viewCancellationPolicyLink = By.xpath("//div[contains(@class,'hide-for-large')]//a[contains(.,'Cancellation')]");
	/*
	 * Method: cancelledReservationName Description: Reservation Resort Name
	 * Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelledReservationName() {

		waitUntilElementVisibleBy(driver, cancelledResortHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(cancelledResortHeader));
		// reservation Resort Name from test data
		String strReservationName = testData.get("ReservationResortName").toUpperCase();
		if (getElementText(cancelledResortHeader).toUpperCase().contains(strReservationName)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelledReservationName", Status.PASS,
					"Cancelled Reservation Resort Name Displayed correctly " + strReservationName);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelledReservationName", Status.FAIL,
					"Cancelled  Reservation Resort name not Displayed correctly " + strReservationName);
		}
	}

	/*
	 * Method: cancelledReservationContentBox Description: Reservation Content
	 * Box Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelledReservationContentBox() {
		elementPresenceVal(cancellationHeader, "Cancellation Header ", "CUIReservationDetailsPage",
				"cancelledReservationContentBox");
		elementPresenceVal(cancellationCheckmark, "Cancellation Check Mark  ", "CUIReservationDetailsPage",
				"cancelledReservationContentBox");

	}

	/*
	 * Method: cancelledReservationContentBox Description: Reservation Content
	 * Box Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void cancelledDateValidation() throws Exception {
		String cancelledDate = getElementText(cancellationDate);
		Date convertedDate = convertStringToDate(cancelledDate, "MM/dd/yyyy");
		String cancelledOn = testData.get("ReservationCancelledDate");

		if (cancelledOn.equalsIgnoreCase("Today")) {
			currentDateValidation(cancelledDate);
		} else if (cancelledOn.contains("/")) {
			previousDateValidation(cancelledOn, convertedDate);
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "cancelledDateValidation", Status.FAIL,
					"Error in Verifying Cancelled Dates");
		}

	}

	/*
	 * Method: cancelledReservationContentBox Description: Reservation Content
	 * Box Check Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void currentDateValidation(String cancelledDate) throws Exception {
		if (dateDifference("MM/dd/yyyy", getCurrentDateInSpecificFormat("MM/dd/yyyy"), cancelledDate) == 0) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "currentDateValidation", Status.PASS,
					"Today's date is displayed Correctly");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "currentDateValidation", Status.FAIL,
					"Today's date is not displayed correctly");
		}
	}

	/*
	 * Method: previousDateValidation Description: Reservation Content Box Check
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void previousDateValidation(String cancelledOn, Date applicationDate) throws ParseException {
		Date cancelledDate = convertStringToDate(cancelledOn, "MM/dd/yyyy");

		if (cancelledDate.equals(applicationDate)) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "previousDateValidation", Status.FAIL,
					"Cancelled Previous date is displayed Correctly");
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "previousDateValidation", Status.FAIL,
					"Cancelled Previous is not displayed correctly");
		}
	}

	/*
	 * Method: reservationCancelledBy Description: Reservation Cancelled By
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reservationCancelledBy() {
		fieldPresenceVal(cancellationBy, "Reservation Cancelled By Name ", "CUIReservationDetailsPage",
				"reservationCancelledBy", " and is " + getElementText(cancellationBy));
	}

	/*
	 * Method: reimbursedCharges Description: Reimbursed Charges Validation
	 * Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reimbursedCharges() {

		Assert.assertTrue(verifyObjectDisplayed(reimbursementHeader));
		getElementInView(reimbursementHeader);
		tcConfig.updateTestReporter("CUIReservationDetailsPage", "reimbursedCharges", Status.PASS,
				"Reimbursed Header displayed");
		if (CUICancelReservation_Web.pointsReimbursed.contains(getElementText(totalReimbursePoints))) {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "reimbursedCharges", Status.PASS,
					"Reimbursed Points displayed correctly as : " + getElementText(totalReimbursePoints));
		} else {
			tcConfig.updateTestReporter("CUIReservationDetailsPage", "reimbursedCharges", Status.FAIL,
					"Reimbursed Points not displayed correctly " + getElementText(totalReimbursePoints));
		}

	}

}

