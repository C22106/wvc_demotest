package nextgen.pages;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class CUIMyOwnershipPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(CUIMyOwnershipPage_Web.class);

	public CUIMyOwnershipPage_Web(TestConfig tcconfig) {
		super(tcconfig);

	}

	protected By ownerFullName = By.xpath("//h4[contains(.,'WELCOME')]/parent::div/h2");
	protected By myAccountDropDown = By
			.xpath("//div[@class='global-navigation__wrapper']//button[contains(@class,'container')]");
	protected By dropDownOwnership = By.xpath(
			"(//div[@class='top-bar-right']//div[@class='global-account__links']//li[contains(@class,'ownership')]/a | //div[@class='top-bar-right']//div[@class='global-account__links']//li[contains(@class,'membership')]/a)");
	protected By myOwnershipHeader = By.xpath(
			"(//h1[contains(@class,'title') and contains(.,'OWNERSHIP')]|//h1[contains(@class,'title') and contains(.,'MEMBERSHIP')])");

	protected By memberNumberHeader = By.xpath("//h2[contains(.,'Member Number')]/following-sibling::p");
	protected By memberNumberIcon = By.xpath("//h2[contains(.,'Member Number')]/parent::div/preceding-sibling::div");
	protected By membershipTypeHeader = By.xpath("//h2[contains(.,'Membership Type')]/following-sibling::p");
	protected By membershipTypeIcon = By
			.xpath("//h2[contains(.,'Membership Type')]/parent::div/preceding-sibling::div");
	protected By membershipExpiration = By
			.xpath("//h2[contains(.,'Membership Type')]/following-sibling::div[contains(.,'Expiration')]");
	protected By ownerStatus = By.xpath("//h2[contains(.,'Status')]/following-sibling::p");
	protected By ownerStatusIcon = By.xpath("//h2[contains(.,'Status')]/parent::div/preceding-sibling::div");
	protected By statusExpiration = By
			.xpath("//h2[contains(.,'Status')]/following-sibling::div[contains(.,'Expiration')]");
	protected By additionalProgramsHeader = By.xpath("//h2[contains(.,'Additional')]");
	protected By additionalPrograms = By.xpath("//h2[contains(.,'Additional')]/following-sibling::p");

	protected By financialDetailsHeader = By.xpath("// h1[contains(.,'Financial Details')]");
	protected By makePaymentHeader = By
			.xpath("//section[contains(@class,'financial-details')]//div[contains(text(),'Make a Payment')]");
	protected By financialDetailsInfo = By
			.xpath("//section[contains(@class,'financial-details')]//p[contains(.,'method of payment')]");
	protected By enrollButton = By
			.xpath("//section[contains(@class,'financial-details')]//input[contains(@value,'Enroll')]");
	protected By payPalLink = By.xpath("//section[contains(@class,'financial-details')]//a[contains(.,'PayPal')]");
	protected By hiddenValueXpath = By.xpath("//form[@id='westernUnionForm']//input");
	// input[@type='hidden' and @value='WVR' and @name='club' and @id='club']
	protected By speedPayLink = By.xpath("//form[@id='westernUnionForm']//input[contains(@value,'Speedpay')]");
	protected By speedPayNavigation = By.id("wvr_logo");

	protected By myContractHeader = By.xpath("// h1[contains(.,'My Contract')]");
	protected By contractHeaders = By.xpath("// div[contains(@class,'my-account-header')]/h2[contains(.,'Contract')]");
	protected By seeMoreContractCTA = By.xpath("// section[@class='contracts']//button");
	protected By contractOwnerName = By.xpath("// h2[contains(.,'Owner(s)')]/following-sibling::p");
	protected By contractPoints = By.xpath("// h2[contains(.,'Points Owned')]/following-sibling::p");
	protected By contractType = By.xpath("//div[contains(.,'Contract Type')]/following-sibling::p");
	protected By homeResortLink = By.xpath("// h2[contains(.,'Home Resort')]/following-sibling::div//a");
	protected By homeResortText = By.xpath(
			"(// h2[contains(.,'Home Resort')]/following-sibling::p | // h2[contains(.,'Home Resort')]/following-sibling::div//a)");
	protected By resortPageNavigation = By.xpath("//h1[contains(@class,'title-1')]");
	protected By reciprocalResortText = By.xpath(
			"(// h2[contains(.,'Reciprocal Resort')]/following-sibling::p| // h2[contains(.,'Reciprocal Resort')]/following-sibling::div//a)");
	protected By reciprocalResortLink = By.xpath("// h2[contains(.,'Reciprocal Resort')]/following-sibling::div//a");
	protected By contractDetails = By.xpath("// h2[contains(.,'Contract Details')]/following-sibling::p");
	protected By contractUseYear = By.xpath("// h2[contains(.,'Use Year')]/following-sibling::p");
	protected By contractToolTip = By.xpath("// span[contains(@id,'contract-details-tooltip')]");
	protected By contractToolTipInfo = By.xpath("// div[contains(@class,'tooltip')]");

	protected By pointOfContactHeader = By.xpath("//section[contains(@class,'point-of-contact')]//h1");
	protected By pointOfContactInfo = By
			.xpath("//section[contains(@class,'point-of-contact')]//h1/following-sibling::p/span");

	protected By ownerName = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Owner')]/following-sibling::p");
	protected By ownerNameIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Owner')]/parent::div/preceding-sibling::div");
	protected By ownerAddress = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Address')]/following-sibling::p");
	protected By ownerAddressIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Address')]/parent::div/preceding-sibling::div");
	protected By ownerEmail = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Email')]/following-sibling::p");
	protected By ownerEmailIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Email')]/parent::div/preceding-sibling::div");
	protected By ownerPhone = By
			.xpath("//section[contains(@class,'point-of-contact')]//h2[contains(.,'Phone')]/following-sibling::p");
	protected By ownerPhoneIcon = By.xpath(
			"//section[contains(@class,'point-of-contact')]//h2[contains(.,'Phone')]/parent::div/preceding-sibling::div");

	protected By paymentType = By.xpath("//select[@id = 'payment-type']");
	protected By labelDiscoveryPoints = By
			.xpath("//div[@class='cell small-12 large-4 padding-right-1']//p[contains(text(),'300,000')]");
	protected By toolTip = By.xpath("//span[@data-tooltip-class='tooltip info']");
	protected By tootlTipText = By.xpath("//div[@role='tooltip' and contains(.,'Your contract type')]");
	
	/*
	 * Method: validateToolTip Description:validate tool tip Page Date:
	 * Feb/2021 Author: Saket Sharma Changes By: NA
	 */
	public void validateToolTip(){
		waitUntilElementVisibleBy(driver, toolTip, 80);
		toolTipValidations(toolTip, "Accessible suites", tootlTipText);
	}
	
	/*
	 * * *************************Method: toolTipValidations ***
	 * ***************Description: toolTipValidations****
	 */

	public void toolTipValidations(By tootlTipXpath, String toolTipFor, By tootlTipTextXpath) {

		if (verifyObjectDisplayed(tootlTipXpath)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Tool Tip Present in the page for : " + toolTipFor);
			getElementInView(tootlTipXpath);
			clickElementBy(tootlTipXpath);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			toolTipCTAValidations(tootlTipTextXpath, toolTipFor);
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Tool Tip not Present in the page for : " + toolTipFor);
		}

	}
	
	/*
	 * Method: toolTipCTAValidations Description: Tool Tip CTA Validations
	 * 
	 */
	public void toolTipCTAValidations(By tootlTipTextXpath, String toolTipFor) {

		String strFlag = getElementAttribute(tootlTipTextXpath, "aria-hidden");
		if (strFlag.contains("false")) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Tool Tip opened for " + toolTipFor);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Failed to open Tool tip for" + toolTipFor);
		}
	}

	/*
	 * Method: navigateToMyOwnershipPage Description:Navigate to My Ownership Page
	 * Val Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void navigateToMyOwnershipPage() {
		waitUntilElementVisibleBy(driver, ownerFullName, 60);
		if (verifyObjectDisplayed(myAccountDropDown)) {
			clickElementBy(myAccountDropDown);
			waitUntilElementVisibleBy(driver, dropDownOwnership, 60);
			clickElementBy(dropDownOwnership);
			pageCheck();
			waitUntilElementVisibleBy(driver, myOwnershipHeader, 60);
			Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader));
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.PASS,
					"User Navigated to My Ownership page");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "navigateToMyOwnershipPage", Status.FAIL,
					"User failed to Navigated to My Ownership page");
		}
	}

	/*
	 * Method: verifyMemberNumber Description:verify Member Number Page Val Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyMemberNumber() {
		// Member Number
		String strMemberNumber = testData.get("MemberNumber");

		if (getElementText(memberNumberHeader).contains(strMemberNumber)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMemberNumber", Status.PASS,
					"Member Number Header present with number " + strMemberNumber);
			elementPresenceVal(memberNumberIcon, "Member Number Icon ", "CUIMyOwnershipPage_Web", "verifyMemberNumber");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMemberNumber", Status.FAIL,
					"Member Number Header not present with number " + strMemberNumber);
		}
	}

	/*
	 * Method: verifyMembershipType Description:verify Membership type Page Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyMembershipType() {
		// Membership type
		String strMemberType = testData.get("MemberType");

		if (getElementText(membershipTypeHeader).contains(strMemberType)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMemberNumber", Status.PASS,
					"Membership Type Header present with Type " + strMemberType);
			elementPresenceVal(membershipTypeIcon, "Membership Type Icon ", "CUIMyOwnershipPage_Web",
					"verifyMembershipType");
			if (verifyObjectDisplayed(membershipExpiration)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMembershipType", Status.PASS,
						"Membership Expiration dtae present " + getElementText(membershipExpiration));
			}
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyMembershipType", Status.FAIL,
					"Membership Type Header not present with Type " + strMemberType);
		}
	}

	/*
	 * Method: verifyVIPStatusType Description:verify VIP Status type Page Val Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyVIPStatusType() {
		// Status type
		String strStatusType = testData.get("UserStatus");

		if (getElementText(ownerStatus).contains(strStatusType)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyVIPStatusType", Status.PASS,
					"Status Header present with Type " + strStatusType);
			if (verifyObjectDisplayed(statusExpiration)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyVIPStatusType", Status.PASS,
						"Status Expiration dtae present " + getElementText(statusExpiration));
			}
			elementPresenceVal(ownerStatusIcon, "Status Icon ", "CUIMyOwnershipPage_Web", "verifyVIPStatusType");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyVIPStatusType", Status.FAIL,
					"Status Header not present with Type " + strStatusType);
		}
	}

	/*
	 * Method: verifyAdditionalProgram Description:verify Additional Program Val
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyAdditionalProgram() {

		if (verifyObjectDisplayed(additionalProgramsHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAdditionalProgram", Status.PASS,
					"Additional Program Header present with total " + getList(additionalPrograms).size() + " programs");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAdditionalProgram", Status.WARNING,
					"Additional Program List Not Present");
		}
	}

	/*
	 * Method: financialDetailsHeader Description:verify Financial Details section *
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void financialDetailsHeader() {
		getElementInView(financialDetailsHeader);
		if (verifyObjectDisplayed(financialDetailsHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.PASS,
					"Financial Details Section present in the page");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.FAIL,
					"Financial Details Section not present in the page");
		}
	}

	/*
	 * Method: myContractsHeader Description:verify My contract header section *
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void myContractsHeader() {
		getElementInView(myContractHeader);
		if (verifyObjectDisplayed(myContractHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.PASS,
					"My Contracts Header present in the page with Total contract " + totalContracts());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "financialDetailsHeader", Status.FAIL,
					"My Contract Headers not present in the page");
		}
	}

	/*
	 * Method: expandContracts Description:Return Expand Total Contract Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void expandContracts() {
		if (totalContracts() > 2) {
			if (verifyObjectDisplayed(seeMoreContractCTA)) {
				getElementInView(seeMoreContractCTA);
				clickElementBy(seeMoreContractCTA);
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "expandContracts", Status.PASS,
						"See More Contracts CTA present and clicked as there are more than 2 contracts");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				getElementInView(myContractHeader);
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "expandContracts", Status.FAIL,
						"See More Contracts CTA not present even when there are more than 2 contracts");
			}
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "expandContracts", Status.PASS,
					"See More Contracts CTA not present as there are less than 2 contracts");
		}

	}

	/*
	 * Method: totalContractsValidations Description:Total contract match the number
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void totalContractsValidations() {
		if (getList(contractHeaders).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "totalContractsValidations", Status.PASS,
					"Total Contract Number and Contract Card displayed are equal as: "
							+ getList(contractHeaders).size());
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "totalContractsValidations", Status.FAIL,
					"Total Contract Number and contract Cards count are different");
		}

	}

	/*
	 * Method: ownerNameValidations Description:Total owner name match the total
	 * contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void ownerNameValidations() {
		if (getList(contractOwnerName).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "ownerNameValidations", Status.PASS,
					"Owner Name present in all cards: " + getList(contractOwnerName).size());
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "ownerNameValidations", Status.FAIL,
					"Owner Name is not present in all contracts");
		}

	}

	/*
	 * Method: pointsOwnedValidations Description:Total Points Owned match the total
	 * contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void pointsOwnedValidations() {
		if (getList(contractPoints).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "pointsOwnedValidations", Status.PASS,
					"Points Owned present in all cards: " + getList(contractPoints).size());
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "pointsOwnedValidations", Status.FAIL,
					"Points Owned is not present in all contracts");
		}

	}

	/*
	 * Method: contractDetailsValidations Description:Total Contract Details match
	 * the total contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void contractDetailsValidations() {
		if (getList(contractDetails).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsValidations", Status.PASS,
					"Contract Details present in all cards: " + getList(contractType).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsValidations", Status.WARNING,
					"Contract Details is not present in all contracts");
		}

	}

	/*
	 * Method: contractDetailsToolTip Description:Contract Details Tool Tip Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void contractDetailsToolTip() {
		if (getList(contractDetails).size() > 0) {

			if (getList(contractToolTip).size() == getList(contractDetails).size()) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsToolTip", Status.PASS,
						"Tool tip present in all contract details card" + getList(contractType).size());
				getElementInView(getList(contractToolTip).get(0));
				getList(contractToolTip).get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				toolTipCTAValidations(0);
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "contractDetailsToolTip", Status.FAIL,
						"Tool tip not present in all contract details card");
			}
		}
	}

	/*
	 * Method: toolTipCTAValidations Description: Tool Tip CTA Validations Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void toolTipCTAValidations(int contractDetails) {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String strFlag = getList(contractToolTipInfo).get(contractDetails).getAttribute("aria-hidden");
		if (strFlag.contains("false")) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "toolTipCTAValidations", Status.PASS,
					"Tool Tip opened");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "toolTipCTAValidations", Status.FAIL,
					"Failed to open Tool tip");
		}
	}

	/*
	 * Method: useYearValidations Description:Total Use year match the total
	 * contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void useYearValidations() {
		if (getList(contractUseYear).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearValidations", Status.PASS,
					"Use Year present in all cards: " + getList(contractUseYear).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearValidations", Status.FAIL,
					"Points Owned is not present in all contracts");
		}

	}

	/*
	 * Method: useYearDateFormat Description: Use year date format contract number
	 * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void useYearDateFormat() {
		if (getList(contractUseYear).size() > 0) {
			if (isValidDate(getList(contractUseYear).get(0).getText(), "MMMM dd")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearDateFormat", Status.PASS,
						"Use Year date displayed in correct format" + getList(contractUseYear).get(0).getText());

			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "useYearDateFormat", Status.FAIL,
						"Use Year date not displayed in correct format" + getList(contractUseYear).get(0).getText());
			}
		}

	}

	/*
	 * Method: homeResortValidations Description:Total Home Resort match the total
	 * contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void homeResortValidations() {
		if (getList(homeResortText).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortValidations", Status.PASS,
					"Home Resort present in all cards" + getList(homeResortText).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortValidations", Status.WARNING,
					"Home Resort is not present in all contracts, total : " + getList(homeResortText).size());
		}

	}

	/*
	 * Method: homeResortLinkValidations Description:Total Home Resort match the
	 * total contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void homeResortLinkValidations() {
		if (getList(homeResortText).size() > 0) {
			if (verifyObjectDisplayed(getList(homeResortLink).get(0))) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortLinkValidations", Status.PASS,
						"Home Resort Link present in some cards" + getList(homeResortLink).size());
				homeResortFirstLinkValidation();

			} else if (getList(homeResortText).size() > 0) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortLinkValidations", Status.FAIL,
						"Home Resort is displayed as text in all the contracts card");
			}
		}
	}

	/*
	 * Method: homeResortFirstLinkValidation Description:Points Link Elements
	 * Validations * Date field Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void homeResortFirstLinkValidation() {
		String resortName = getList(homeResortLink).get(0).getText().toUpperCase();
		getElementText(getList(homeResortLink).get(0));
		clickElementBy(getList(homeResortLink).get(0));
		waitUntilElementVisibleBy(driver, resortPageNavigation, 120);
		Assert.assertTrue(verifyObjectDisplayed(resortPageNavigation));
		if (getElementText(resortPageNavigation).toUpperCase().contains(resortName)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortFirstLinkValidation", Status.PASS,
					"Home Resort Link Navigated");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "homeResortFirstLinkValidation", Status.FAIL,
					"Home Resort Link Navigation failed");
		}
		navigateBack();
		waitUntilElementVisibleBy(driver, myOwnershipHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader));

	}

	/*
	 * Method: reciprocalResortValidations Description:Total reciprocal Resort match
	 * the total contract number Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reciprocalResortValidations() {

		if (getList(reciprocalResortText).size() == totalContracts()) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortValidations", Status.PASS,
					"Reciprocal Resort present in all cards" + getList(reciprocalResortText).size());

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortValidations", Status.WARNING,
					"Reciprocal Resort is not present in all contracts, total : "
							+ getList(reciprocalResortText).size());
		}

	}

	/*
	 * Method: reciprocalResortLinkValidations Description:Total Reciprocal Resort
	 * match the total contract number Date: Jun/2020 Author: Unnat Jain Changes By:
	 * NA
	 */
	public void reciprocalResortLinkValidations() {
		if (getList(reciprocalResortText).size() > 0) {
			if (verifyObjectDisplayed(getList(reciprocalResortLink).get(0))) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortLinkValidations", Status.PASS,
						"Reciprocal Resort Link present in some cards" + getList(reciprocalResortLink).size());
				reciprocalResortFirstLinkValidation();

			} else if (getList(reciprocalResortText).size() > 0) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortLinkValidations", Status.WARNING,
						"Reciprocal Resort is displayed as text in all the contracts card");
			}
		}
	}

	/*
	 * Method: reciprocalResortFirstLinkValidation Description:Points Link Elements
	 * Validations * Date field Date: June/2020 Author: Unnat Jain Changes By: NA
	 */
	public void reciprocalResortFirstLinkValidation() {
		String resortName = getList(reciprocalResortLink).get(0).getText().toUpperCase();
		getElementText(getList(reciprocalResortLink).get(0));
		clickElementBy(getList(reciprocalResortLink).get(0));
		waitUntilElementVisibleBy(driver, resortPageNavigation, 120);
		Assert.assertTrue(verifyObjectDisplayed(resortPageNavigation));
		if (getElementText(resortPageNavigation).toUpperCase().contains(resortName)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortFirstLinkValidation", Status.PASS,
					"Reciprocal Resort Link Navigated");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "reciprocalResortFirstLinkValidation", Status.FAIL,
					"Reciprocal Resort Link Navigation failed");
		}
		navigateBack();
		waitUntilElementVisibleBy(driver, myOwnershipHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader));

	}

	/*
	 * Method: verifyPointOfContact Description:verify POC Val Date: Jun/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void verifyPointOfContact() {
		getElementInView(pointOfContactHeader);
		if (verifyObjectDisplayed(pointOfContactHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPointOfContact", Status.PASS,
					"Point fo Contact Header present");
			elementPresenceVal(pointOfContactInfo, "Point of Contact Info ", "CUIMyOwnershipPage_Web",
					"verifyPointOfContact");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPointOfContact", Status.FAIL,
					"Point fo Contact Header not present");
		}
	}

	/*
	 * Method: verifyOwnerName Description:verify Owner name Val Date: Jun/2020
	 * Author: Unnat Jain Changes By: NA
	 */
	public void verifyOwnerName() {
		// Owner Name
		String strOwnerName = testData.get("CompanyName");

		if (getElementText(ownerName).contains(strOwnerName)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyOwnerName", Status.PASS,
					"Owner Name Header present with Name " + strOwnerName);
			elementPresenceVal(ownerNameIcon, "Owner Name Icon ", "CUIMyOwnershipPage_Web", "verifyOwnerName");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyOwnerName", Status.FAIL,
					"Owner Name Header not present with Name " + strOwnerName);
		}
	}

	/*
	 * Method: verifyEmail Description:verify Email Page Val Date: Jun/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void verifyEmail() {
		// Email
		String strEmail = testData.get("Email");

		if (getElementText(ownerEmail).contains(strEmail)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyEmail", Status.PASS,
					"Email Header present with Email " + strEmail);
			elementPresenceVal(ownerEmailIcon, "Email Icon ", "CUIMyOwnershipPage_Web", "verifyEmail");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyEmail", Status.FAIL,
					"Email Header not present with email " + strEmail);
		}
	}

	/*
	 * Method: verifyAddress Description:verify Address Val Date: Jun/2020 Author:
	 * Unnat Jain Changes By: NA
	 */
	public void verifyAddress() {
		// Member Number
		String strAddress = testData.get("Address");
		String applicationAddress = getElementText(ownerAddress);
		try {
			applicationAddress = getElementText(ownerAddress).replace("\n", "");
		} catch (Exception e) {
			Log.info("Adress does not contain next line");
		}

		if (applicationAddress.contains(strAddress)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAddress", Status.PASS,
					"Address Header present with address " + strAddress);

			elementPresenceVal(ownerAddressIcon, "Address Icon ", "CUIMyOwnershipPage_Web", "verifyAddress");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyAddress", Status.FAIL,
					"Address Header not present with address " + strAddress);
		}
	}

	/*
	 * Method: verifyPhoneNumber Description:verify Phone Number Page Val Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void verifyPhoneNumber() {
		// Phone Number
		String strPhoneNumber = testData.get("PhoneNumber");

		if (getElementText(ownerPhone).contains(strPhoneNumber)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPhoneNumber", Status.PASS,
					"Phone Number Header present with number " + strPhoneNumber);
			elementPresenceVal(ownerPhoneIcon, "Phone Number Icon ", "CUIMyOwnershipPage_Web", "verifyPhoneNumber");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyPhoneNumber", Status.FAIL,
					"Phone Number Header not present with number " + strPhoneNumber);
		}
	}

	/*
	 * Method: totalContracts Description:Return Total Contract Number Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public int totalContracts() {
		// My Contracts String
		String totalContracts = getElementText(myContractHeader);
		String splitContract[] = totalContracts.split("\\(");
		totalContracts = splitContract[splitContract.length - 1];
		totalContracts = totalContracts.replace(")", "").trim();
		// Contracts in Integer
		int totalCotractNumber = Integer.parseInt(totalContracts);
		return totalCotractNumber;
	}

	/*
	 * Method: makePaymentHeader Description:verify make Payment header * Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void makePaymentHeader() {

		getElementInView(makePaymentHeader);
		if (verifyObjectDisplayed(makePaymentHeader)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "makePaymentHeader", Status.PASS,
					"Make Payment header present in the page");
			elementPresenceVal(financialDetailsInfo, "Financial Details Info Text ", "CUIMyOwnershipPage_Web",
					"makePaymentHeader");
			elementPresenceVal(enrollButton, "Enroll in Autopay Button ", "CUIMyOwnershipPage_Web",
					"makePaymentHeader");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "makePaymentHeader", Status.FAIL,
					"Make Payment header not present in the page");
		}
	}

	/*
	 * Method: speedPayLinkPresence Description:Speed Pay Link validations * Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void speedPayLinkPresence() {

		if (verifyObjectDisplayed(speedPayLink)) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkPresence", Status.PASS,
					"Speed Pay Link Present");

			elementPresenceVal(payPalLink, "PayPal Link", "CUIMyOwnershipPage_Web", "speedPayLinkPresence");

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkPresence", Status.FAIL,
					"Speed Pay Link not Present");
		}
	}

	/*
	 * Method: clickPayPalLink Description:Click Link PayPal present in My Ownership
	 * Page Date: Jun/2020 Author: Abhijeet Roy Changes By: NA
	 */

	public void clickPayPalLink() {
		if (verifyObjectDisplayed(payPalLink)) {
			clickElementJSWithWait(payPalLink);
			waitUntilElementInVisible(driver, loadingSpinner, 120);
			pageCheck();
			waitUntilElementVisibleBy(driver, paymentType, 120);
			if (verifyObjectDisplayed(paymentType)) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickPayPalLink", Status.PASS,
						"Successfully clicked on Paypal Link");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickPayPalLink", Status.FAIL,
						"Failed to click on Paypal Link");
			}

		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "clickPayPalLink", Status.FAIL,
					"PayPal Link Not present in My Ownership Page");
		}
	}

	/*
	 * Method: hiddenValueAttribute Description:Speed Pay Link WVR attribute
	 * validations * Date: Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void hiddenValueAttribute() {
		try {
			if (getList(hiddenValueXpath).get(0).getAttribute("value").contains("WVR")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.PASS,
						"Hidden Attribute has the value as WVR");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
						"Hidden Attribute does not have the value as WVR");
			}

			if (getList(hiddenValueXpath).get(0).getAttribute("name").contains("club")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.PASS,
						"Hidden Attribute has the name as club");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
						"Hidden Attribute does not have the name as club");
			}

			if (getList(hiddenValueXpath).get(0).getAttribute("id").contains("club")) {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.PASS,
						"Hidden Attribute has the id as club");
			} else {
				tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
						"Hidden Attribute does not have the id as club");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "hiddenValueAttribute", Status.FAIL,
					"Failed to validate hidden attribute xpath");
		}

	}

	/*
	 * Method: speedPayLinkValidation Description:Speed Pay Link validations * Date:
	 * Jun/2020 Author: Unnat Jain Changes By: NA
	 */
	public void speedPayLinkValidation() {
		getElementInView(speedPayLink);
		clickElementBy(speedPayLink);
		ArrayList<String> newWindow = new ArrayList<String>(driver.getWindowHandles());
		newTabNavigations(newWindow);
		waitUntilElementVisibleBy(driver, speedPayNavigation, 120);
		Assert.assertTrue(verifyObjectDisplayed(speedPayNavigation));
		if (driver.getCurrentUrl().contains(testData.get("newPageURL"))) {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkValidation", Status.PASS,
					"Speed Pay Link Navigated");
		} else {
			tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "speedPayLinkValidation", Status.FAIL,
					"Speedpay Link Navigation failed");
		}
		navigateToMainTab(newWindow);
		waitUntilElementVisibleBy(driver, myOwnershipHeader, 120);
		Assert.assertTrue(verifyObjectDisplayed(myOwnershipHeader));

	}
	/*
	 * Method: verifyOwnerMembershipType Description:verify membership types * Date:
	 * sept/2020 Author: Saket Sharma Changes By: NA
	 */

	public void verifyOwnerMembershipType() {

		String strMemberType = testData.get("header1");
		waitUntilElementVisibleBy(driver, membershipTypeHeader, "ExplicitLongWait");
		System.out.println(getElementText(membershipTypeHeader));
		Assert.assertTrue(getElementText(membershipTypeHeader).trim().contains(strMemberType));
		tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyOwnerMembershipType", Status.PASS,
				"Membership Type Header present with Type " + strMemberType + "in header");

		Assert.assertTrue(getElementText(contractType).trim().contains(strMemberType));
		tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyOwnerMembershipType", Status.PASS,
				"Membership Type Header present with Type " + strMemberType + "in contract type");

	}
	
	protected By membershipPointsLabel = By.xpath("//div[contains(text(),'Membership Points')]");

	public void verifyDiscoveryPoints() {
		String strStatusType = testData.get("header1");

		Assert.assertTrue(getElementText(labelDiscoveryPoints).contains(strStatusType));
		tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "verifyDiscoveryPoints", Status.PASS,
				"Discovery Header present with " + strStatusType + " points");

	}

	public void userPointsOwnedValidations() {
		getElementInView(contractHeaders);
		Assert.assertTrue(verifyObjectDisplayed(membershipPointsLabel));
		tcConfig.updateTestReporter("CUIMyOwnershipPage_Web", "userPointsOwnedValidations", Status.PASS,
				"Membership Points Owned By the owner label is present");
	}

}
