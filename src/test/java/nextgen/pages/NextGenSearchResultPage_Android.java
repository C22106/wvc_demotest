package nextgen.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NextGenSearchResultPage_Android extends NextGenSearchResultPage_Web {

	public static final Logger log = Logger.getLogger(NextGenSearchResultPage_Android.class);

	public NextGenSearchResultPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
