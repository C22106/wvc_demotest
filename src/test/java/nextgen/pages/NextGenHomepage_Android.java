package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NextGenHomepage_Android extends NextGenHomepage_Web {

	public static final Logger log = Logger.getLogger(NextGenHomepage_Android.class);

	public NextGenHomepage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		nextGenHeroImage = By.xpath(/* "//div[@class='blockVideo']/div[@class='video']" */
				"//div[@id='mobile']//div[@aria-hidden='false']//section[contains(@class,'banner')]//img");
		searchContainer1 = By.xpath("//div//input[contains(@class,'mobile-search__input')]");
		searchButton1 = By.xpath("(//div//button[contains(@class,'mobile-search__icon')])[1]");

	}

	public By hamburger = By.xpath("//button[contains(@class,'hamburger')]");
	public By clickSignIn = By.xpath("(//li[contains(@class,'loggedOut')]/a[contains(text(),'Sign In')])[1]");
	// public By hamburger=By.xpath("//div[@id='nav-icon4']/span[1]");

	public void clickHamburger() {
		try {
			waitUntilObjectClickable(driver, hamburger, 120);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		driver.findElement(hamburger).click();
		tcConfig.updateTestReporter("NextGenHomepage_IOS", "clickHamburger", Status.PASS, "Hamburger Menu clicked");
	}

	@Override
	public void searchField() {

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilObjectVisible(driver, searchContainer1, 120);
		if (verifyObjectDisplayed(searchContainer1)) {

			tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.PASS,
					"Search Container displayed with text: "
							+ driver.findElement(searchContainer1).getAttribute("placeholder"));

			driver.findElement(searchContainer1).click();
			driver.findElement(searchContainer1).sendKeys(testData.get("strSearch"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.PASS, "Search Keyword Entered");

			clickElementJSWithWait(searchButton1);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "searchField", Status.FAIL,
					"Error in getting the search container");
		}

	}

	@Override
	public void launchAppNextGen() {
		driver.navigate().to(testData.get("URL"));
		String urlCheck = testData.get("URL");
		// driver.manage().window().maximize();

		waitUntilElementVisibleBy(driver, nextGenHeroImage, 120);

		if (verifyObjectDisplayed(nextGenHeroImage)) {
			if (urlCheck.toUpperCase().contains("QA")) {
				System.out.println("Testing in QA link");
			} else if (urlCheck.toUpperCase().contains("PROD")) {
				System.out.println("Testing in PROD link");
			} else if (urlCheck.toUpperCase().contains("STAGE")) {
				System.out.println("Testing in Stage link");
			}
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
					"Next Gen Club Wyndham Navigation Successful");

			try {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilObjectVisible(driver, acceptButton, 20);
				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("Club Wyndham Homepgage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementJSWithWait(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No Button");
			}

			try {
				List<WebElement> covidClose = driver.findElements(covidAlertClose);
				waitUntilObjectVisible(driver, covidClose.get(1), 20);
				if (covidClose.get(1).isDisplayed()) {
					covidClose.get(1).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					tcConfig.updateTestReporter("Club Wyndham Homepgage", "closeAlert", Status.PASS,
							"Successfully Closed Alert");
				}

			} catch (Exception e) {
				log.error("Covid Message Not Present");

			}

		} else {
			tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.FAIL,
					"Next Gen Club Wyndham Navigation Unsuccessful");
		}

	}

	public void clickSignInButtonMobile() {
		waitUntilElementVisibleBy(driver, clickSignIn, 120);
		if (verifyElementDisplayed(driver.findElement(clickSignIn))) {
			tcConfig.updateTestReporter("NextGenHomePage", "clickSignInButton", Status.PASS,
					"Sign In button is displayed");
			clickElementBy(clickSignIn);
		}
	}

	@Override
	public void formValidations() {

		driver.navigate().refresh();
		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			scrollDownForElementJSBy(formTitle);
			tcConfig.updateTestReporter("NextGenHomePage", "formValidations", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formSubTitle)) {
				tcConfig.updateTestReporter("NextGenHomePage", "formValidations", Status.PASS,
						"Form SubTitle present with header " + getElementText(formSubTitle));

				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"First Name Field Present");

					//fieldDataEnter(firstNameField, testData.get("firstName"));
					sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name *", testData.get("firstName"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Last Name Field Present");

					//fieldDataEnter(lastNameField, testData.get("lastName"));
					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name *", testData.get("lastName"));//

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Phone Field Present");

					//fieldDataEnter(phoneField, testData.get("phoneNo"));
					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Phone Number *", testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Email Field Present");

					//fieldDataEnter(emailField, testData.get("email"));
					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address *", testData.get("email"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(stateSelect)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"State Drop Down present");
					new Select(driver.findElement(stateSelect)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(submitButton) || verifyObjectDisplayed(submitButtonold)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Submit button present");

					if (verifyObjectDisplayed(submitButton))
						clickElementJSWithWait(submitButton);
					else
						clickElementJSWithWait(submitButtonold);

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Submit Button not present");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, thankConfirm, 120);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.PASS,
							"Form Submission Successful");

					/*
					 * if (verifyObjectDisplayed(closeMsg)) {
					 * clickElementBy(closeMsg); } else {
					 * tcConfig.updateTestReporter("NextGenHomePage",
					 * "incorrectFormVal", Status.FAIL, "Close Btn not present"
					 * ); }
					 */

				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "incorrectFormVal", Status.FAIL,
					"Form Title not present with header ");
		}

	}

}
