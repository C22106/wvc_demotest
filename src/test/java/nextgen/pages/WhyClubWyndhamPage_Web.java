package nextgen.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.TestConfig;

public class WhyClubWyndhamPage_Web extends NextGenBasePage {

	public static final Logger log = Logger.getLogger(WhyClubWyndhamPage_Web.class);

	public WhyClubWyndhamPage_Web(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	// WhyClubWyndham
	protected By whyClubWyndhamBreadcrumb = By
			.xpath("//ul[contains(@class,'breadcrumbs')]//li/a[text()='Club Benefits']");
	protected By whyClubWyndhamHeader = By
			.xpath("//ul[contains(@class,'global-navigation')]//li/a[text()='Club Benefits']");

	protected By videoPlay = By.xpath("(//div[@class='videoBanner']//div[@class='video']//a)[1]");
	protected By videoOpen = By.xpath("//div[@class='wyn-video-player__icon']//img");
	protected By openedVideoHeader = By.xpath(
			/* "(//div[@class='headers']//h1" */"//div[@class='headers']/h1[text()=' Own Your Vacation Dreams With Club Wyndham ']");
	protected By playButton = By.xpath("//div[@class='play-icon']");
	protected By closeVideo = By.xpath("//button[@class='close-button']");
	protected By yourVacations = By.xpath("//h3[contains(.,'Your Vacations')]");
	protected By yourVacayDesc = By.xpath("//h3[contains(.,'Your Vacations')]/parent::div//p");
	protected By carouselQuotes = By.xpath("//div[@role='tabpanel']//div[contains(@class,'quoteBanner')]");
	protected By nextButton = By.xpath("//div[contains(@class,'myNext-beInTheClub')]");
	protected By aciveCarousel = By
			.xpath("//div[@role='tabpanel']//div[contains(@class,'quoteBanner')]//div[@class='title-3']");

	protected By activeCardQuote = By.xpath("//div[@aria-hidden='false']//blockquote/h4/p");

	protected By makeVacayHeader = By.xpath("//div[contains(text(),'Make Vacation')]");
	protected By makeVacayDesc = By.xpath("//div[contains(text(),'Make Vacation')]/parent::div//p");
	protected By makeVacayImage = By
			.xpath("//div[contains(text(),'Make Vacation')]/parent::div/preceding-sibling::div/img");

	protected By chooseTimeshareHeader = By.xpath("(//div[contains(text(),'Choose Timeshare Resorts')])[1]");
	protected By chooseTimeshareDesc = By
			.xpath("(//div[contains(text(),'Choose Timeshare Resorts')])[1]/parent::div//p");
	protected By chooseTimeshareImage = By
			.xpath("(//div[contains(text(),'Choose Timeshare Resorts')])[1]/parent::div/preceding-sibling::div/img");
	protected By reimagineVacHeader = By.xpath("//div[contains(text(),'Reimagine Your')]");
	protected By reimagineVacDesc = By.xpath("//div[contains(text(),'Reimagine Your')]/parent::div//p");
	protected By reimagineVacImage = By
			.xpath("//div[contains(text(),'Reimagine Your')]/parent::div/preceding-sibling::div/img");
	protected By joinTheClubHeader = By.xpath("(//div[contains(text(),'Join the Club')])[1]");
	protected By joinTheClubDesc = By.xpath("(//div[contains(text(),'Join the Club')])[1]/parent::div//p");
	protected By joinTheClubImage = By
			.xpath("(//div[contains(text(),'Join the Club')])[1]/parent::div/preceding-sibling::div/img");
	protected By startSavingHeader = By.xpath("//div[contains(text(),'Start Saving')]");
	protected By startSavingDesc = By.xpath("//div[contains(text(),'Start Saving')]/parent::div//p");
	protected By startSavingImage = By
			.xpath("//div[contains(text(),'Start Saving')]/parent::div/preceding-sibling::div/img");

	protected By twoColumnVal = By.xpath("//div[@class='cardComponent']//img");
	protected By twoCoumnHeader = By.xpath("//div[@class='cardComponent']//div[contains(@class,'subtitle-2 bold')]");
	protected By readMoreLink = By.xpath("//div[@class='cardComponent']//a/div[contains(@class,'body-1-link')]");

	// OwnerTestimonials

	// content updated for family and vacation 27/09
	protected By ownerTestNav = By
			.xpath("//ul[contains(@class,'first-sub')]//li//div//ul//li//a[contains(.,'Owner Testimonials')]");
	protected By ownerTestimonialsHeroImage = By.xpath(
			"(//div[@class='cardBanner']//following-sibling::div/img[contains(@alt,'owner-testimonial-hero')])[1]");
	protected By ownerTestimonialsHeader = By
			.xpath("(//div[contains(@class,'bannerCard')]/div[@class='caption-1'])[1]");
	protected By ownerTestimonialsBreadcrumb = By
			.xpath("//nav[@role='navigation']//a[contains(.,'Owner Testimonials')]");
	protected By familyHeader = By.xpath("//h3[contains(.,'The Family That Plays Together')]");
	protected By familyAuthor = By.xpath(
			"//h3[contains(.,'The Family That Plays Together')]/ancestor::div[@class='wyn-card__content']//div/p[2]");
	protected By familyDesc = By.xpath(
			"//h3[contains(.,'The Family That Plays Together')]/ancestor::div[@class='wyn-card__content']//div/p[1]");
	protected By familyImg = By.xpath("//h3[contains(.,'The Family That Plays Together')]/ancestor::a//img");

	protected By vacationHeader = By.xpath("//h3[contains(.,'Making Vacation a Priority')]");
	protected By vacationAuthor = By.xpath(
			"//h3[contains(.,'Making Vacation a Priority')]/ancestor::div[@class='wyn-card__content']//div/p[2]");
	protected By vacationDesc = By.xpath(
			"//h3[contains(.,'Making Vacation a Priority')]/ancestor::div[@class='wyn-card__content']//div/p[1]");
	protected By vacationImg = By.xpath("//h3[contains(.,'Making Vacation a Priority')]/ancestor::a//img");
	/*
	 * protected By testimonialCards = By.xpath(
	 * "//div[contains(@class,'slick-active')]//div/a[@data-eventname='Card']/img"
	 * );
	 */
	protected By testimonialCards = By.xpath("//div[contains(@class,'slick-active')]//div/a[@data-eventname='Card']");
	/*
	 * protected By testimonialImage = By.xpath(
	 * "//div[contains(@class,'slick-active')]//div/a[@data-eventname='Card']/img"
	 * );
	 */
	protected By testimonialImage = By.xpath("//div[contains(@class,'slick-active')]//div/a[@data-eventname='Card']");
	protected By testimonialTitles = By.xpath("//div[contains(@class,'slick-active')]//div[@class='subtitle-2 bold']");
	protected By testimonialDesc = By.xpath("//div[contains(@class,'slick-active')]//div[@class='subtitle-3 bold']");
	protected By testimonialDescLE = By.xpath("//div[contains(@class,'slick-active')]//div[@class='body-1']/p");
	protected By cardReadMore = By.xpath(
			"//div[contains(@class,'slick-active')]//a[contains(@class,'ctaText link')]//div[@class='body-1-link' and contains(text(),'Read')]");
	protected By cardHeader = By.xpath("//div[@aria-hidden='false']//a[@class='wyn-card ']//h3");
	protected By pageHeader = By.xpath("//div[contains(text(),'Owner Testimonials')]//following::h3");
	/*
	 * protected By testimonialRead = By.xpath(
	 * "//div[contains(@class,'slick-active')]//a[contains(@class,'ctaText link')]//div[@class='body-1-link' and contains(text(),'Read')]"
	 * );
	 */
	protected By testimonialRead = By.xpath(
			"//div[contains(@class,'slick-active')]//a[contains(@class,'ctaText text-primary gaChecker')]//div[@class='link-caret-dynamic body-1-link' and contains(text(),'Read')]");
	/*
	 * protected By familyHeader = By.xpath(
	 * "//h2[contains(.,'The Family That Plays Together')]"); protected By
	 * familyAuthor = By.xpath(
	 * "//h2[contains(.,'The Family That Plays Together')]/ancestor::div[@class='wyn-card__content']//h3/p"
	 * ); protected By familyDesc = By.xpath(
	 * "//h2[contains(.,'The Family That Plays Together')]/ancestor::div[@class='wyn-card__content']//div/p"
	 * ); protected By familyImg = By.xpath(
	 * "//h2[contains(.,'The Family That Plays Together')]/ancestor::div[@class='wyn-l-large-wrapper--content-carousel columns is-vcentered']//img"
	 * );
	 */

	// prideOfOwnership
	protected By prideNav = By
			.xpath("//ul[contains(@class,'first-sub')]//li//div//ul//li//a[contains(.,'Pride of Ownership')]");
	/* "//div[@class='wyn-fly-out']//li[contains(.,'Pride of Owner')]//a"); */
	protected By prideHeader = By.xpath("(//div[contains(@class,'bannerCard')]/div[@class='caption-1'])[1]");
	protected By prideHeroImg = By
			.xpath("(//div[@class='cardBanner']//following-sibling::div/img[contains(@src,'pride-of-ownership')])[1]");
	protected By prideDesc = By.xpath("(//div[contains(@class,'bannerCard')]/div[@class='body-1'])[1]/p");
	protected By prideBreadcrumb = By.xpath("//nav[@role='navigation']//a[contains(.,'Pride of Ownership')]");
	protected By peopleLikeYouHeader = By.xpath("(//div[contains(text(),'More People Like You')])[1]");
	protected By peopleLikeYouSubHeader = By.xpath("(//div[contains(text(),'WELCOME TO THE CLUB')])[1]");
	protected By peopleDesc = By
			.xpath("//div[contains(text(),'More People Like You')]/..//div[contains(@class,'body-1')]/p");
	protected By peopleSubTitle = By.xpath(
			"//div[contains(text(),'More People Like You')]//following::div[contains(@class,'text-black subtitle-1')]");
	protected By ownerQuote = By.xpath("//div[@class='title-3']");

	protected By lifeTimeHeader = By.xpath("(//div[contains(text(),'A Lifetime of Vacations')])[3]");
	protected By lifeTimeImage = By.xpath(
			"((//div[contains(text(),'A Lifetime of Vacations')])[3]/../..//following::img[contains(@src,'why-wyndham')])[1]");
	protected By lifeTimeDesc = By
			.xpath("(//div[contains(text(),'A Lifetime of Vacations')])[3]/../div[contains(@class,'body-1')]/ul/li/b");
	protected By benefitsOfOwnerShipsBtn = By.xpath(
			"((//div[contains(text(),'A Lifetime of Vacations')])[3]//following::a[text()='Benefits of Ownership'])[1]");

	protected By topNotchHeader = By.xpath("(//div[contains(text(),'Top-Notch Resorts')])[3]");
	protected By topNotchImage = By.xpath(
			"((//div[contains(text(),'Top-Notch Resorts')])[3]/../..//following::img[contains(@src,'why-wyndham')])[1]");
	protected By topNotchDesc = By
			.xpath("(//div[contains(text(),'Top-Notch Resorts')])[3]/../div[contains(@class,'body')]/ul/li/b");
	protected By viewDestinationsBtn = By
			.xpath("((//div[contains(text(),'Top-Notch Resorts')])[3]//following::a[text()='View Destinations'])[1]");

	protected By truePeaceHeader = By.xpath("(//div[contains(text(),'True Peace of Mind')])[3]");
	protected By truePeaceImage = By.xpath(
			"((//div[contains(text(),'True Peace of Mind')])[3]/../..//following::img[contains(@src,'why-wyndham')])[1]");
	protected By truePeaceDesc = By
			.xpath("(//div[contains(text(),'True Peace of Mind')])[3]/../div[contains(@class,'body-1')]/ul/li/b");
	protected By newOwnerBtn = By.xpath(
			"((//div[contains(text(),'True Peace of Mind')])[3]//following::a[text()='New Owner Quick Start'])[1]");

	// tourtheclub
	protected By tourTheClubNav = By.xpath("//ul[@class='nested menu']//li//a[text()='Tour the Club']");
	/*
	 * "//ul[contains(@class,'first-sub')]//li//div//a//span[contains(.,'Tour')]//.."
	 */
	/* ("//div[@class='wyn-fly-out']//li[contains(.,'Tour')]//a"); */
	protected By tourTheClubHeader = By.xpath(
			/* "//div[@class='banner-carousel']//h1[contains(.,'Tour the Club')]" */"//div[@class='videoBanner']//div[@class='title-1' and contains(.,'TOUR THE CLUB')]");
	protected By tourClubDesc = By.xpath(
			/* "//div[@class='banner-carousel']//div[2]/p" */"//div[@class='videoBanner']//div[@class='title-1' and contains(.,'TOUR THE CLUB')]/..//p");
	protected By bookYouTour = By.xpath("//div[@class='banner-carousel']//a");
	protected By tourClubVideoPlay = By.xpath(
			/* "//div[@class='banner-carousel']//div[@class='wyn-video-player__icon']" */"//div[@class='videoBanner']//a[contains(@class,'korModalActivate')]");
	protected By tourClubBreadcrumb = By.xpath(
			/* "//nav[@class='wyn-breadcrumbs']//a[contains(.,'Tour the Club')]" */"//div[@class='breadcrumb']//li/a[text()='Tour the Club']");
	protected By videoFrame = By.xpath("//iframe[@class='wyn-video-player__iframe']");
	// protected By closeVideo=By.xpath("//span[@class='wyn-modal__close']");
	protected By whatToExpect = By.xpath(
			/* "//h4[@class='wyn-title__subtitle']" */"//div[contains(@class,'caption-1') and contains(text(),'Why Tour?')]");
	protected By mainHeader = By.xpath("//h4[@class='wyn-title__subtitle']/ancestor::div[@class='icon-list']//h2");
	protected By whatToExpecteDesc = By.xpath(
			/*
			 * "//h4[@class='wyn-title__subtitle']/ancestor::div[@class='icon-list']//h2/parent::div/following-sibling::p"
			 */"//div[contains(@class,'caption-1') and contains(text(),'Why Tour?')]/../../../../../../../..//div[@class='contentSlice']//div[contains(@class,'subtitle-1')]/..//div[contains(@class,'body-1')]/p");
	protected By whatToExpectSubHeader = By.xpath(
			/* "//h4[@class='wyn-title__subtitle']/ancestor::div[@class='icon-list']//h3" */"//div[contains(@class,'caption-1') and contains(text(),'Why Tour?')]/../../../../../../../..//div[@class='contentSlice']//div[contains(@class,'subtitle-1')]");
	protected By subHeaderDesc = By.xpath(
			/*
			 * "//h4[@class='wyn-title__subtitle']/ancestor::div[@class='icon-list']//div[@class='wyn-icon-card__content']//p"
			 */
			"//div[contains(@class,'caption-1') and contains(text(),'Why Tour?')]/../../../../../../../..//div[@class='contentSlice']//div[contains(@class,'subtitle-1')]/..//div[contains(@class,'body-1')]/p");
	protected By bucketListHeader = By.xpath(
			/* "//h2[contains(.,'EXPLORE')]" */"//div[contains(@class,'caption-1') and contains(text(),'EXPLORE')]");
	protected By bucketListDesc = By.xpath(/*
											 * "//div[@class='cards']//div[@class='wyn-l-content wyn-l-content-wrapper--left']/p"
											 */
			"//div[contains(@class,'caption-1') and contains(text(),'EXPLORE')]//following::div[contains(@class,'aem-GridColumn')]//div[@class='contentSlice']//p");
	protected By totalCards = By.xpath(/* "//a[@class='wyn-card ']" */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='cardComponent']//img");
	protected By cardsImage = By.xpath(/* "//a[@class='wyn-card ']//img[@class='wyn-image lazyloaded']" */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='cardComponent']//img");
	protected By cardsTitle = By.xpath(/* "//a[@class='wyn-card ']//div[@class='wyn-title']//h3" */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='cardComponent']//img//..//..//div[contains(@class,'subtitle-2')]");
	protected By cardsDesc = By.xpath(/* "//a[@class='wyn-card ']//div[@class='wyn-l-content wyn-card__copy']//p" */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='cardComponent']//img//..//..//div[contains(@class,'body-1')]//p/b");
	protected By viewOffersCta = By.xpath(/*
											 * "//a[@class='wyn-card ']//div[@class='wyn-button-cta' and contains(.,'View Offers')]"
											 */
			"//div[contains(@class,'aem-GridColumn')]//div[@class='cardComponent']//img//..//..//div[contains(@class,'body-1') and contains(text(),'View Offers')]");
	protected By featuredPackage = By.xpath("(//div[contains(text(),'FEATURED PACKAGE')])[1]");
	protected By packageHeader = By.xpath(/* "//p[contains(.,'FEAT')]/ancestor::div[@gtm_component='banner']//h2" */
			"(//div[contains(text(),'FEATURED PACKAGE')])[1]/../div[@class='title-1']");
	protected By packageDesc = By.xpath(/* "//p[contains(.,'FEAT')]/ancestor::div[@gtm_component='banner']//h3//p" */
			"(//div[contains(text(),'FEATURED PACKAGE')])[1]/../div[@class='body-1']/p");
	protected By bookNowBtn = By.xpath(/*
										 * "//p[contains(.,'FEAT')]/ancestor::div[@gtm_component='banner']//a[contains(.,'Book Now')]"
										 */
			"(//div[contains(text(),'FEATURED PACKAGE')])[1]/../div[@class='body-1']/../a[text()='Book Now']");

	protected By seeMoreDest = By.xpath("//a[contains(.,'See More Dest')]");
	protected By clubWyndhamLogo = By.xpath("//div[@gtm_component='header']//img[@alt='CLUB WYNDHAM']");
	protected By footerClubBenefits = By.xpath("//div[contains(@class,'footer')]//span[text()='Club Benefits']");

	/*
	 * Method: navigateToWhyClubWyndhamPage Description: Navigate to Why Club
	 * Wyndham Page : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToWhyClubWyndhamPage() {
		waitUntilElementVisibleBy(driver, whyClubWyndhamHeader, 120);
		if (verifyObjectDisplayed(whyClubWyndhamHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.PASS,
					"Why Club Wyndham present in main Nav");
			try {
				if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
					scrollDownForElementJSBy(footerClubBenefits);
					clickElementBy(footerClubBenefits);
					pageCheck();
				}
			} catch (Exception e) {
				clickElementJSWithWait(whyClubWyndhamHeader);
				pageCheck();
			}
			waitUntilElementVisibleBy(driver, whyClubWyndhamBreadcrumb, 120);
			getElementInView(whyClubWyndhamBreadcrumb);
			if (verifyObjectDisplayed(whyClubWyndhamBreadcrumb)) {
				tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.PASS,
						"Why Club Wyndham page Navigated, Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.FAIL,
						"Why Club Wyndham page nav failed");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToWhyClubWyndhamPage", Status.FAIL,
					"Why Club Wyndham not present in main Nav");
		}
	}

	/*
	 * Method: whyClubWyndhamVal Description: Why Club Wyndham Validations :
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	protected By acceptButton = By.xpath("//button[contains(.,'Accept')]");
	protected By timeShareCloseBtn = By.xpath("(//button[@class='close-button'])[1]");

	public void whyClubWyndhamVal() {

		try {
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(acceptButton)) {
				tcConfig.updateTestReporter("DestinationsLoginPage", "launchApplication", Status.PASS,
						"Accept Cookies Button Present");

				clickElementBy(acceptButton);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

		} catch (Exception e) {
			System.out.println("No Button");
		}

		waitUntilElementVisibleBy(driver, videoPlay, 120);

		if (verifyObjectDisplayed(videoPlay)) {
			tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
					"Video present in the section");

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "videoFunctionalityVal", Status.PASS,
					"Video not  present in the section");
		}

		getElementInView(yourVacations);

		if (verifyObjectDisplayed(yourVacations)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Your vacations header present");
			if (verifyObjectDisplayed(yourVacayDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Your vacations description present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Your vacations description not  present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
					"Your vacations header not  present");
		}

		// carouselValidations();

		getElementInView(makeVacayHeader);

		if (verifyObjectDisplayed(makeVacayHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Make vacations header present");
			if (verifyObjectDisplayed(makeVacayDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Make vacations description present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Make vacations description not  present");
			}

			if (verifyObjectDisplayed(makeVacayImage)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Make vacations Image present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Make vacations image not  present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
					"Make vacations header not  present");
		}

		getElementInView(chooseTimeshareHeader);

		if (verifyObjectDisplayed(chooseTimeshareHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Choose Timeshare header present");
			if (verifyObjectDisplayed(chooseTimeshareDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Choose Timeshare description present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Choose Timeshare description not  present");
			}

			if (verifyObjectDisplayed(chooseTimeshareImage)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Choose Timeshare Image present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Choose Timeshare image not  present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
					"Choose Timeshare header not  present");
		}

		getElementInView(reimagineVacHeader);

		if (verifyObjectDisplayed(reimagineVacHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Reimagine Vacation header present");
			if (verifyObjectDisplayed(reimagineVacDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Reimagine Vacation description present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Reimagine Vacation description not  present");
			}

			if (verifyObjectDisplayed(reimagineVacImage)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Reimagine Vacation Image present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Reimagine Vacation image not  present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
					"Reimagine Vacation header not  present");
		}

		getElementInView(joinTheClubHeader);

		if (verifyObjectDisplayed(joinTheClubHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Join the club header present");
			if (verifyObjectDisplayed(joinTheClubDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Join the club description present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Join the club description not  present");
			}

			if (verifyObjectDisplayed(joinTheClubImage)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Join the club Image present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Join the club image not  present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
					"Join the club header not  present");
		}

		getElementInView(startSavingHeader);

		if (verifyObjectDisplayed(startSavingHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Start Saving header present");
			if (verifyObjectDisplayed(startSavingDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Start Saving description present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Start Saving description not  present");
			}

			if (verifyObjectDisplayed(startSavingImage)) {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						"Start Saving Image present");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
						"Start Saving image not  present");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
					"Start Saving header not  present");
		}

		getElementInView(twoColumnVal);

		if (verifyObjectDisplayed(twoColumnVal)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Two Coloumn section present in the page");
			List<WebElement> sectionHeader = driver.findElements(twoCoumnHeader);
			int totalSize = sectionHeader.size();
			for (int i = 0; i < totalSize; i++) {
				List<WebElement> sectionHeader1 = driver.findElements(twoCoumnHeader);
				List<WebElement> readMoreList = driver.findElements(readMoreLink);
				tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
						(i + 1) + " header is: " + sectionHeader1.get(i).getText());

				readMoreList.get(i).click();

				waitForSometime(tcConfig.getConfig().get("LongWait"));
				// driver.navigate().refresh();
				if (driver.getTitle().toUpperCase().contains(testData.get("header" + (i + 1) + "").toUpperCase())) {

					tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
							"Navigated to " + (i + 1) + " page");

					driver.navigate().back();

					waitUntilElementVisibleBy(driver, twoColumnVal, 120);

				}

			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Two Coloumn section not present in the page");
		}

	}

	/*
	 * Method: carouselValidations Description: Why Club wyndham Carousel
	 * Validations : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void carouselValidations() {

		getElementInView(carouselQuotes);

		if (verifyObjectDisplayed(carouselQuotes)) {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.PASS,
					"Carousel present on the page");

			List<WebElement> slickBtnList = driver.findElements(aciveCarousel);

			for (int i = 0; i < slickBtnList.size(); i++) {

				if (!slickBtnList.get(i).getText().equals("")) {
					tcConfig.updateTestReporter("NextGenHomePage", "carouselValidations", Status.PASS, (i + 1)
							+ " Card is active in Carousel with title: " + slickBtnList.get(i).getText().trim());

					clickElementBy(nextButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "hpFeaturedResortVal", Status.FAIL,
							"Carousel not woring as expected");
				}

			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "whyClubWyndhamVal", Status.FAIL,
					"Carousel not present on the page");
		}

	}

	/*
	 * Method: navigateToOwnerTestimonial Description: Owner Testimoniual Page
	 * navigation : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navigateToOwnerTestimonial() {
		waitUntilElementVisibleBy(driver, whyClubWyndhamHeader, 120);
		hoverOnElement(whyClubWyndhamHeader);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(ownerTestNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToOwnerTestimonial", Status.PASS,
					"Owner Testimonial present in Sub Nav");
			clickElementJSWithWait(ownerTestNav);
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToOwnerTestimonial", Status.FAIL,
					"Owner Testimonial not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, ownerTestimonialsHeader, 120);
		String strHeaderText = driver.findElement(ownerTestimonialsHeader).getText().trim();
		if (verifyObjectDisplayed(ownerTestimonialsHeader) && strHeaderText.equals("OWNER TESTIMONIALS")) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToOwnerTestimonial", Status.PASS,
					"Owner Testimonial Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToOwnerTestimonial", Status.FAIL,
					"Owner Testimonial Page Navigation failed");
		}

	}

	/*
	 * Method: navToPrideOfOwnershipPage Description: Pride of ownership Page
	 * navigation : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void navToPrideOfOwnershipPage() {

		waitUntilElementVisibleBy(driver, whyClubWyndhamHeader, 120);
		hoverOnElement(whyClubWyndhamHeader);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(prideNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navToPrideOfOwnershipPage", Status.PASS,
					"Pride of ownership present in Sub Nav");
			driver.findElement(prideNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navToPrideOfOwnershipPage", Status.FAIL,
					"Pride of ownership not present in sub nav");
		}

		waitUntilElementVisibleBy(driver, prideHeader, 120);

		if (verifyObjectDisplayed(prideHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navToPrideOfOwnershipPage", Status.PASS,
					"Pride of ownership Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navToPrideOfOwnershipPage", Status.FAIL,
					"Pride of ownership Page Navigation failed");
		}

	}

	/*
	 * Method: prideOfOwnerValidations Description: Pride of ownership
	 * validations : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void prideOfOwnerValidations() {

		if (verifyObjectDisplayed(prideHeroImg)) {
			tcConfig.updateTestReporter("NextGenHomePage", "prideOfOwnerValidations", Status.PASS,
					"Pride Of Ownership Hero Image Present ");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "prideOfOwnerValidations", Status.FAIL,
					"Pride Of Ownership  Hero Image not Present ");
		}

		if (verifyObjectDisplayed(prideBreadcrumb)) {
			tcConfig.updateTestReporter("NextGenHomePage", "prideOfOwnerValidations", Status.PASS,
					"Pride Of Ownership Bread crumb present");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "prideOfOwnerValidations", Status.FAIL,
					"Pride Of Ownership bread crumb not present");
		}

		if (verifyObjectDisplayed(prideDesc)) {
			tcConfig.updateTestReporter("NextGenHomePage", "prideOfOwnerValidations", Status.PASS,
					"Pride Of Ownership Desc present");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "prideOfOwnerValidations", Status.FAIL,
					"Pride Of Ownership Desc not present");
		}

	}

	/*
	 * Method: ownerTestimonialValidations Description: Owner Testimonial page
	 * validations : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void ownerTestimonialValidations() {

		waitUntilElementVisibleBy(driver, ownerTestimonialsHeroImage, 120);
		if (verifyObjectDisplayed(ownerTestimonialsHeroImage)) {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerTestimonialValidations", Status.PASS,
					"Owner Testimonial Hero Image Present ");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerTestimonialValidations", Status.FAIL,
					"Owner Testimonial Hero Image not Present ");
		}
		waitUntilObjectVisible(driver, ownerTestimonialsBreadcrumb, 120);
		if (verifyObjectDisplayed(ownerTestimonialsBreadcrumb)) {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerTestimonialValidations", Status.PASS,
					"Owner Testimonial Bread crumb present");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerTestimonialValidations", Status.FAIL,
					"Owner Testimonial bread crumb not present");
		}

	}

	/*
	 * Method: ownerFamilySectionVal Description: Owner Family section
	 * validations : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void ownerFamilySectionVal() {

		getElementInView(familyHeader);

		if (verifyObjectDisplayed(familyHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.PASS,
					"Family That Plays Together Header present in the page");

			if (verifyObjectDisplayed(familyImg)) {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.PASS,
						"Family That Plays Together Image present in the page");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.FAIL,
						"Family That Plays Together Image not present in the page");
			}

			if (verifyObjectDisplayed(familyAuthor)) {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.PASS,
						"Family That Plays Together Author name present in the page");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.FAIL,
						"Family That Plays Together Author Name not present in the page");
			}

			if (verifyObjectDisplayed(familyDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.PASS,
						"Family That Plays Together Desc present in the page");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.FAIL,
						"Family That Plays Together Desc not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerFamilySectionVal", Status.FAIL,
					"Family That Plays Together Header not present in the page");
		}

	}

	/*
	 * Method: ownerVacationSectionVal Description: Owner Vacation Section
	 * validation : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void ownerVacationSectionVal() {

		getElementInView(vacationHeader);

		if (verifyObjectDisplayed(vacationHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.PASS,
					"Making Vacation a Priority Header present in the page");

			if (verifyObjectDisplayed(vacationImg)) {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.PASS,
						"Making Vacation a Priority Image present in the page");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.FAIL,
						"Making Vacation a Priority Image not present in the page");
			}

			if (verifyObjectDisplayed(vacationAuthor)) {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.PASS,
						"Making Vacation a Priority Author name present in the page");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.FAIL,
						"Making Vacation a Priority Author Name not present in the page");
			}

			if (verifyObjectDisplayed(vacationDesc)) {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.PASS,
						"Making Vacation a Priority Desc present in the page");

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.FAIL,
						"Making Vacation a Priority Desc not present in the page");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerVacationSectionVal", Status.FAIL,
					"Making Vacation a Priority Header not present in the page");
		}

	}

	/*
	 * Method: ownerCardsSectionVal Description: Owner Cards section validation
	 * : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void ownerCardsSectionVal() {

		// waitUntilElementVisibleBy(driver, testimonialCards, 120);
		getElementInView(testimonialCards);

		List<WebElement> testimonialCardsList = driver.findElements(testimonialCards);

		List<WebElement> testimonialImageList = driver.findElements(testimonialImage);
		List<WebElement> testimonialTitlesList = driver.findElements(testimonialTitles);
		List<WebElement> testimonialDescList = driver.findElements(testimonialDesc);// 16
		List<WebElement> testimonialDescListLE = driver.findElements(testimonialDescLE);
		List<WebElement> testimonialReadMore = driver.findElements(testimonialRead);
		int descTotal = testimonialDescList.size();

		if (testimonialCardsList.size() == testimonialImageList.size()) {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerCardsSectionVal", Status.PASS,
					"Testimonial Cards images present for all cards");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerCardsSectionVal", Status.FAIL,
					"Testimonial Cards Images not present for all cards");
		}

		/*
		 * if (testimonialCardsList.size() == testimonialTitlesList.size()) {
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.PASS,
		 * "Testimonial Cards Title present for all cards"); } else {
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.FAIL,
		 * "Testimonial Cards  Titles not present for all cards"); }
		 * 
		 * if (testimonialCardsList.size() == descTotal) {
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.PASS,
		 * "Testimonial Cards Desc present for all cards"); } else if
		 * (testimonialCardsList.size() <= testimonialDescListLE.size()) {
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.PASS,
		 * "Testimonial Cards Desc present for all cards"); } else {
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.FAIL,
		 * "Testimonial Cards description not present for all cards"); }
		 * 
		 * if (testimonialCardsList.size() == testimonialReadMore.size())
		 * 
		 * { tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.PASS,
		 * "Testimonial Cards CTA present for all cards"); } else {
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.FAIL,
		 * "Testimonial Cards CTA not present for all cards"); }
		 */

		/*
		 * List<WebElement> readMoreList = driver.findElements(cardReadMore);
		 * 
		 * for (int i = 0; i < readMoreList.size(); i++) { List<WebElement>
		 * titleList = driver.findElements(testimonialTitles); List<WebElement>
		 * readMoreList1 = driver.findElements(cardReadMore); String
		 * articleHeader = titleList.get(i).getText().toLowerCase(); String
		 * breadCrumpHeader = titleList.get(i).getText();
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.PASS, (i + 1) + " Card Header is: " +
		 * titleList.get(i).getText()); getElementInView(readMoreList1.get(i));
		 * clickElementWb(readMoreList1.get(i));
		 * 
		 * waitUntilElementVisibleBy(driver, pageHeader, 120);
		 * getElementInView(pageHeader); if
		 * (getElementText(pageHeader).toLowerCase().contains(articleHeader) &&
		 * verifyElementDisplayed(driver
		 * .findElement(By.xpath("//ul[@class='breadcrumbs']//li/a[text()='" +
		 * breadCrumpHeader + "']")))) {
		 * tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.PASS,
		 * "Navigated to respective page via read more Cta");
		 * 
		 * driver.navigate().back(); waitUntilElementVisibleBy(driver,
		 * testimonialCards, 120);
		 * 
		 * } else { tcConfig.updateTestReporter("NextGenHomePage",
		 * "ownerCardsSectionVal", Status.FAIL,
		 * "Navigation to respective page failed via read more Cta"); }
		 * 
		 * }
		 */

	}

	/*
	 * Method: peopleLikeYouVal Description: People like you section validation
	 * : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void peopleLikeYouVal() {

		getElementInView(peopleLikeYouHeader);

		if (verifyObjectDisplayed(peopleLikeYouHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.PASS,
					"More People Like You Header present in the page");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.FAIL,
					"More People Like You Header not present in the page");
		}

		if (verifyObjectDisplayed(peopleLikeYouSubHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.PASS,
					"Sub Header present as: " + getElementText(peopleLikeYouSubHeader));
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.FAIL, "Sub Header not present");
		}
		if (verifyObjectDisplayed(peopleDesc)) {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.PASS,
					"More People Like You Description present");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.FAIL,
					"More People Like You Description not present in the page");
		}

		if (verifyObjectDisplayed(peopleSubTitle)) {
			List<WebElement> peopleSubtitleList = driver.findElements(peopleSubTitle);

			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.PASS,
					"More People Like You sub title present, total: " + peopleSubtitleList.size());
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.FAIL,
					"More People Like You subTitle not present in the page");
		}

		getElementInView(ownerQuote);

		if (verifyObjectDisplayed(ownerQuote)) {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.PASS,
					"Owner Quote present in the page");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "peopleLikeYouVal", Status.FAIL,
					"Owner Quote not present in the page");
		}

	}

	/*
	 * Method: lifeTimeVacayVal Description: Life Time vacation validation :
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void lifeTimeVacayVal() {

		getElementInView(lifeTimeHeader);

		if (verifyObjectDisplayed(lifeTimeHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.PASS,
					"Lifetime Header present on the page");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.FAIL,
					"Lifetime Header  not present in the page");
		}

		if (verifyObjectDisplayed(lifeTimeDesc)) {
			List<WebElement> lifeTimeDescList = driver.findElements(lifeTimeDesc);
			for (int i = 0; i < lifeTimeDescList.size(); i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.PASS,
						"Description present in Lifetime section" + "" + lifeTimeDescList.get(i).getText().trim());
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.FAIL,
					"Description not present in Lifetime section");
		}

		if (verifyObjectDisplayed(lifeTimeImage)) {
			tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.PASS,
					"Image present in Lifetime section");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.FAIL,
					"Image not present in Lifetime section");
		}

		if (verifyObjectDisplayed(benefitsOfOwnerShipsBtn)) {
			tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.PASS,
					"Benefits of Ownership Button present in the section");
			clickElementBy(benefitsOfOwnerShipsBtn);

			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (driver.getTitle().toUpperCase().contains(testData.get("benefitsOfOwnerships").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.PASS,
						"Navigated to associated Page");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.FAIL,
						"Error in navigating associated page");
			}

			driver.navigate().back();
			waitUntilElementVisibleBy(driver, benefitsOfOwnerShipsBtn, 120);

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "lifeTimeVacayVal", Status.FAIL,
					"Benefits of Ownership  Button not present in the section");
		}

	}

	/*
	 * Method: topNotchVal Description: Top Notch Section validations : Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void topNotchVal() {

		getElementInView(topNotchHeader);

		if (verifyObjectDisplayed(topNotchHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.PASS,
					"Top Notch Header present on the page");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.FAIL,
					"Top Notch Header  not present in the page");
		}

		if (verifyObjectDisplayed(topNotchDesc)) {
			List<WebElement> topNotchList = driver.findElements(topNotchDesc);
			for (int i = 0; i < topNotchList.size(); i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.PASS,
						"Description present in Top Notch section is displayed for " + (i + 1) + ""
								+ topNotchList.get(i).getText());
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.FAIL,
					"Description not present in Top Notch section");
		}

		if (verifyObjectDisplayed(topNotchImage)) {
			tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.PASS,
					"Image present in Top Notch section");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.FAIL,
					"Image not present in Top Notch section");
		}

		if (verifyObjectDisplayed(viewDestinationsBtn)) {
			tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.PASS,
					"View Destinations Button present in the section");
			clickElementBy(viewDestinationsBtn);

			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (driver.getCurrentUrl().toUpperCase().contains(testData.get("viewDestinations").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.PASS,
						"Navigated to associated Page");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.FAIL,
						"Error in navigating associated page");
			}

			driver.navigate().back();
			waitUntilElementVisibleBy(driver, viewDestinationsBtn, 120);

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "topNotchVal", Status.FAIL,
					"View Destinatios Button not present in the section");
		}

	}

	/*
	 * Method: navigateToWhereAReWeGoing Description: True Peace section
	 * validations : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void truePeaceVal() {

		getElementInView(truePeaceHeader);

		if (verifyObjectDisplayed(truePeaceHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.PASS,
					"True Peace present on the page");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.FAIL,
					"True Peace  not present in the page");
		}

		if (verifyObjectDisplayed(truePeaceDesc)) {
			List<WebElement> allList = driver.findElements(truePeaceDesc);
			for (int i = 0; i < allList.size(); i++) {
				tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.PASS,
						"Description present in True Peace section" + allList.get(i).getText());
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.FAIL,
					"Description not present in True Peace section");
		}

		if (verifyObjectDisplayed(truePeaceImage)) {
			tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.PASS,
					"Image present in True Peace section");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.FAIL,
					"Image not present in True Peace section");
		}

		if (verifyObjectDisplayed(newOwnerBtn)) {
			tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.PASS,
					"New OWner Button present in the section");
			clickElementBy(newOwnerBtn);

			waitForSometime(tcConfig.getConfig().get("LongWait"));

			if (driver.getTitle().toUpperCase().contains(testData.get("newOwner").toUpperCase())) {
				tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.PASS,
						"Navigated to associated Page");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.FAIL,
						"Error in navigating associated page");
			}

			driver.navigate().back();
			waitUntilElementVisibleBy(driver, newOwnerBtn, 120);

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "truePeaceVal", Status.FAIL,
					"New Owner Button not present in the section");
		}

	}

	/*
	 * Method: navigateToTourClub Description: Navigate to Tour Our Club page :
	 * Date Jul/2019 Author: Unnat Jain Changes By
	 */

	protected By tourCaption = By.xpath(
			"(//div[@class='cardBanner']//img[contains(@src,'Destination-Hero')]//..//..//div[@class='caption-1'])[1]");

	public void navigateToTourClub() {
		waitUntilElementVisibleBy(driver, whyClubWyndhamHeader, 120);
		hoverOnElement(whyClubWyndhamHeader);

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(tourTheClubNav)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.PASS,
					"Tour to club present in Sub Nav");
			driver.findElement(tourTheClubNav).click();
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.FAIL,
					"Tour to club not present in sub nav");
		}

		featuredPackagesVal();

		waitUntilElementVisibleBy(driver, tourClubBreadcrumb, 120);
		getElementInView(tourClubBreadcrumb);
		if (verifyObjectDisplayed(tourClubBreadcrumb)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.PASS,
					"Tour to club breadcrum present");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.FAIL,
					"Tour to club breadcrum not present");
		}

		waitUntilElementVisibleBy(driver, tourTheClubHeader, 120);
		getElementInView(tourTheClubHeader);
		if (verifyObjectDisplayed(tourTheClubHeader)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.PASS,
					"Tour to club Page Navigated");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.FAIL,
					"Tour to club Page Navigation failed");
		}

		if (verifyObjectDisplayed(tourClubDesc)) {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.PASS,
					"Tour to club description present");
		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub", Status.FAIL,
					"Tour to club description not present");
		}

		// if (verifyObjectDisplayed(bookYouTour)) {
		// tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub",
		// Status.PASS,
		// "Book Your Tour Button present");
		// clickElementBy(bookYouTour);
		// waitForSometime(tcConfig.getConfig().get("LongWait"));
		//
		// if (driver.getTitle().toUpperCase().contains("CLUB WYNDHAM")) {
		// tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub",
		// Status.PASS,
		// "Navigated to associated page");
		// } else {
		// tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub",
		// Status.FAIL,
		// "Navigation to associated page failed");
		// }
		//
		// driver.navigate().back();
		//
		// waitUntilElementVisibleBy(driver, tourTheClubHeader, 120);
		//
		// } else {
		// tcConfig.updateTestReporter("NextGenHomePage", "navigateToTourClub",
		// Status.FAIL,
		// "Book Your Tour Button not present");
		// }

	}

	/*
	 * Method: tourTheclubVal Description: Tour The club page validations : Date
	 * Jul/2019 Author: Unnat Jain Changes By
	 */
	public void tourTheclubVal() {

		waitUntilElementVisibleBy(driver, whatToExpect, 120);

		getElementInView(whatToExpect);

		if (verifyObjectDisplayed(whatToExpect)) {

			tcConfig.updateTestReporter("WhyClubWyndhamPage", "WhatToExpectVal", Status.PASS,
					"What To Expect/Why Tour Header present on homepage");

			List<WebElement> whatToExpectTitleList = driver.findElements(whatToExpectSubHeader);
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "WhatToExpectVal", Status.PASS,
					"Total " + whatToExpectTitleList.size() + " sub title present in What To Expect");

			List<WebElement> whatToExpectSubDesc = driver.findElements(subHeaderDesc);

			for (int i = 0; i < whatToExpectTitleList.size(); i++) {

				tcConfig.updateTestReporter("WhyClubWyndhamPage", "WhatToExpectVal", Status.PASS,
						"Title " + (i + 1) + " " + whatToExpectTitleList.get(i).getText());

				if (verifyElementDisplayed(whatToExpectSubDesc.get(i))) {
					tcConfig.updateTestReporter("WhyClubWyndhamPage", "WhatToExpectVal", Status.PASS,
							"Description present for " + (i + 1) + " title");
				} else {
					tcConfig.updateTestReporter("WhyClubWyndhamPage", "WhatToExpectVal", Status.FAIL,
							"Description is not present for " + (i + 1) + " title ");
				}

			}

			/*
			 * if (verifyObjectDisplayed(mainHeader)) {
			 * tcConfig.updateTestReporter("WhyClubWyndhamPage",
			 * "WhatToExpectVal", Status.PASS, "Main Header is present " +
			 * getElementText(mainHeader)); } else {
			 * tcConfig.updateTestReporter("WhyClubWyndhamPage",
			 * "WhatToExpectVal", Status.FAIL, "Main Header is not present " );
			 * }
			 */

		} else {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "WhatToExpectVal", Status.FAIL,
					"What To Expect/Why Tour Header not present on homepage");
		}

		waitUntilElementVisibleBy(driver, tourClubVideoPlay, 120);

		if (verifyObjectDisplayed(tourClubVideoPlay)) {
			tcConfig.updateTestReporter("NextGenHomePage", "tourTheclubVal", Status.PASS,
					"Video present in the section");

			// getElementInView(tourClubVideoPlay);
			clickElementJSWithWait(tourClubVideoPlay);

			WebElement eleVideo = driver.findElement(By.xpath("//iframe[@class='video']"));
			driver.switchTo().frame(eleVideo);
			waitUntilElementVisibleBy(driver, openedVideoHeader, 120);

			if (verifyObjectDisplayed(openedVideoHeader)) {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "tourTheclubVal", Status.PASS,
						"Header present in the section");
			} else {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "tourTheclubVal", Status.FAIL,
						"Header not present in the section");
			}

			if (verifyObjectDisplayed(playButton)) {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "tourTheclubVal", Status.PASS,
						"Video Opened and play button present ");
			} else {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "tourTheclubVal", Status.FAIL,
						"Play button not present");
			}

			driver.switchTo().defaultContent();

			if (verifyObjectDisplayed(closeVideo)) {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "tourTheclubVal", Status.PASS,
						"Close Video Button present");
				clickElementBy(closeVideo);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, tourTheClubHeader, 120);

			} else {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "tourTheclubVal", Status.FAIL,
						"Close Video Button not present");
			}

		} else {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "tourTheclubVal", Status.FAIL,
					"Video not  present in the section");
		}

	}

	/*
	 * Method: exploreBucketListVal Description: Explore bucket list valdations
	 * : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void exploreBucketListVal() {
		waitUntilElementVisibleBy(driver, bucketListHeader, 120);
		getElementInView(bucketListHeader);

		if (verifyObjectDisplayed(bucketListHeader)) {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "exploreBucketListVal", Status.PASS,
					"Explore vacation Header present");
		} else {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "exploreBucketListVal", Status.FAIL,
					"Explore vacation Header not present");
		}

		if (verifyObjectDisplayed(bucketListDesc)) {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "exploreBucketListVal", Status.PASS,
					"Explore vacationt description  present");
		} else {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "exploreBucketListVal", Status.FAIL,
					"Explore Vacation description not present");
		}

		List<WebElement> bucketListCards = driver.findElements(totalCards);

		if (verifyObjectDisplayed(totalCards)) {
			tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
					"Destinations Cards present on the page, total: " + bucketListCards.size());

			List<WebElement> ImageList = driver.findElements(cardsImage);
			List<WebElement> TitlesList = driver.findElements(cardsTitle);
			List<WebElement> viewOffersList = driver.findElements(viewOffersCta);

			if (bucketListCards.size() == ImageList.size()) {
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
						"Explore Vacation Destination Cards images present for all cards");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.FAIL,
						"Explore Vacation Destination Cards Images not present for all cards");
			}

			if (bucketListCards.size() == TitlesList.size()) {
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
						"Explore Vacation Destination Cards Title present for all cards");
			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.FAIL,
						"Explore Vacation Destination Cards  Titles not present for all cards");
			}

			if (bucketListCards.size() == viewOffersList.size()) {
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
						"Explore Vacation Destination Cards View offers CTA present for all cards");

				viewOffersList.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().contains("packages")) {
					tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
							"Navigated to associated page");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.FAIL,
							"Navigated to associated page failed");

				}

				driver.navigate().back();
				waitUntilElementVisibleBy(driver, tourTheClubHeader, 120);

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.FAIL,
						"Explore Vacation Destination Cards view offers CTA not present for all cards");
			}

			if (verifyObjectDisplayed(seeMoreDest)) {
				getElementInView(seeMoreDest);
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
						"See More Dest Link Present");

				clickElementBy(seeMoreDest);
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (verifyObjectDisplayed(clubWyndhamLogo)) {
					tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
							"Navigated to associated page");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.FAIL,
							"Navigated to associated page failed");

				}

				driver.navigate().back();
				waitUntilElementVisibleBy(driver, tourTheClubHeader, 120);

			} else {
				tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.FAIL,
						"Explore Vacation Destination Cards view offers CTA not present for all cards");
			}

		} else {
			tcConfig.updateTestReporter("NextGenHomePage", "ownerCardsSectionVal", Status.FAIL,
					"Explore Vacation Destination Cards not present on the page");
		}

	}

	/*
	 * Method: featuredPackagesVal Description: Featured Packages section
	 * validations : Date Jul/2019 Author: Unnat Jain Changes By
	 */
	public void featuredPackagesVal() {

		getElementInView(featuredPackage);
		if (verifyObjectDisplayed(featuredPackage)) {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.PASS,
					"Feature Package header present");
			if (verifyObjectDisplayed(packageHeader)) {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.PASS,
						"Feature Package main header present " + getElementText(packageHeader));

			} else {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.FAIL,
						"Feature Package main header not present");
			}

			if (verifyObjectDisplayed(packageDesc)) {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.PASS,
						"Feature Package description present");

			} else {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.FAIL,
						"Feature Package description not present");
			}

			if (verifyObjectDisplayed(bookNowBtn)) {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.PASS,
						"Book Now Button present");
				clickElementBy(bookNowBtn);

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().contains("package")) {
					tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.PASS,
							"Navigated to associated page");
				} else {
					tcConfig.updateTestReporter("NextGenHomePage", "exploreBucketListVal", Status.FAIL,
							"Navigated to associated page failed");

				}

				driver.navigate().back();

			} else {
				tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.FAIL,
						"Book Now Button not present");
			}

		} else {
			tcConfig.updateTestReporter("WhyClubWyndhamPage", "featuredPackagesVal", Status.FAIL,
					"Feature Package header not present");
		}

	}

}
