package salepoint.scripts;

import java.util.Map;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import salepoint.pages.SalePointEditForm;
import salepoint.pages.SalePointResaleContractSelection;
import salepoint.pages.SalePointUpgradeContract;
import salepoint.pages.SalePointResalePage;
import salepoint.pages.SalePointAdminPage;
//import salepoint.pages.SalePointWBWMenuPage;
import salepoint.pages.SalePointHomePage;
import salepoint.pages.SalePointLoginPage;
import salepoint.pages.SalePointOwnerPage;
import salepoint.pages.SalePointPaymentScreen;
import salepoint.pages.SalePointWBW;
import salepoint.pages.SalePointWBWMenuPage;
import salepoint.pages.SalePointWVR;
import salepoint.pages.SalepointCommissionandIncentivePage;
import salepoint.pages.SalepointCompanySelect;
import salepoint.pages.SalepointContractDataEntryPage;
import salepoint.pages.SalepointContractFeesPage;
import salepoint.pages.SalepointContractManagementPage;
import salepoint.pages.SalepointContractPaymentPage;
import salepoint.pages.SalepointContractPriceOverridePage;
import salepoint.pages.SalepointContractPrintPage;
import salepoint.pages.SalepointContractProcessingOwners;
import salepoint.pages.SalepointContractSummaryPage;
import salepoint.pages.SalepointDiscUpgradePage;
import salepoint.pages.SalepointDiscoveryPackagePage;
import salepoint.pages.SalepointEditContractPage;
import salepoint.pages.SalepointEditUserInformation;
import salepoint.pages.SalepointInventorySelectionPage;
import salepoint.pages.SalepointLocateCustomerPage;
import salepoint.pages.SalepointMiscellaneousInformationPage;
import salepoint.pages.SalepointTradesPage;
import salepoint.pages.WBWCombinePage;
import salepoint.pages.WBWComissionsPage;
import salepoint.pages.WBWContractManagementPage;
import salepoint.pages.WBWDownPaymentsPage;
import salepoint.pages.WBWEditContractPage;
import salepoint.pages.WBWFinanceInformationPage;
import salepoint.pages.WBWOwnerInformationPage;
import salepoint.pages.WBWOwnersListPage;
import salepoint.pages.WBWPaymentOptionsPage;
import salepoint.pages.WBWPurchaseInformationPage;
import salepoint.pages.WBWSplitPage;
import salepoint.pages.WBWSplitProcessPage;
import salepoint.pages.WBWSummaryPage;
import salepoint.pages.WBWUpgradeContractPage;
import salepoint.pages.WSAHomePage;
import salepoint.pages.WSALoginPage;
import salepoint.pages.WSANewProposalPage;
import salepoint.pages.WSARetrieveTourPage;
import salepoint.pages.WSATourLookupPage;
import salepoint.pages.WVRAPCombinePage;
import salepoint.pages.WVRAPComissionsPage;
import salepoint.pages.WVRAPContractManagementPage;
import salepoint.pages.WVRAPDownPaymentsPage;
import salepoint.pages.WVRAPFinanceInformationPage;
import salepoint.pages.WVRAPOwnerInformationPage;
import salepoint.pages.WVRAPOwnersListPage;
import salepoint.pages.WVRAPPaymentOptionsPage;
import salepoint.pages.WVRAPPurchaseInformationPage;
import salepoint.pages.WVRAPSplitPage;
import salepoint.pages.WVRAPSplitProcessPage;
import salepoint.pages.WVRAPSummaryPage;
import salepoint.pages.WVRAPTourInformationPage;
import salepoint.pages.WVRAPUpgradeContractPage;
import salepoint.pages.WVREditSiteInfoPage;
import automation.core.TestBase;

public class SalePointScripts extends TestBase {
	public static final Logger log = Logger.getLogger(SalePointScripts.class);

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc001_SP_WVRContractCreation(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc004_SP_WVRContractCreation_ErrorValidation(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);

		try {
			sp.launchApplication();
			sp.loginToApp();
			spCompanySelect.companySelect();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.errorMessageValidation();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc005_SP_WVRContractCreation_ErrorValidationSSN(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.validateInvalidSSN_Error();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc006_SP_WVR_Validate_BatchReportByDate(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getDataClickDateOption();
			sptransmitContract.checkReportResult();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc010_SP_WVR_Validate_BatchReportByOpen(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionOpen();
			sptransmitContract.checkReportResult();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event: tc003_SP_WVRContractCreation Description:
	 * WVR Contract Booking in Salepoint application and Finalising
	 */

	@Test(dataProvider = "testData")
	public void tc011_SP_WVRContractCreation_Void(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.voidContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event: tc_030_SP_WVR_UDI_Contaract_AutomationFlow
	 * Description: WVR UDI Contaract Automation Flow
	 */

	@Test(dataProvider = "testData")
	public void tc_030_SP_WVR_UDI_Contaract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		WSARetrieveTourPage wsaRetrieve = new WSARetrieveTourPage(tcconfig);

		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWVR spWVR = new SalePointWVR(tcconfig);
		SalePointOwnerPage spOwnerPage = new SalePointOwnerPage(tcconfig);
		SalePointPaymentScreen spPayment = new SalePointPaymentScreen(tcconfig);
		SalepointContractSummaryPage spContract = new SalepointContractSummaryPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaRetrieve.navigatetoretrievePitch();
			wsaRetrieve.retrievePitch();
			wsaLogin.WSALogoutApp();

			spLogin.loginToSP("IE");
			spHome.companySelectSalePoint();
			try {
				spWVR.sp_WVR_ValidateAutoPopUp();
			} catch (Exception e) {
				tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.INFO,
						"Automation Pop Up message is not Displyaed for first Search");
				try {
					spWVR.sp_WVR_ValidateAutoPopUp();
				} catch (Exception e2) {
					tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.FAIL,
							"Automation Pop Up message is not Displyaed for Second Search");
				}
			}
			spOwnerPage.sp_WVR_edit_PrimaryOwner();
			spPayment.sp_WVR_edit_Payment();
			spContract.sp_Contract_WVR_Save();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.voidContractAutomation();
			spLogin.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event:
	 * tc_031_SP_WVR_Discovery_Contaract_AutomationFlow Description: WVR
	 * Discovery Contaract Automation Flow
	 */

	@Test(dataProvider = "testData")
	public void tc_031_SP_WVR_Discovery_Contaract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		WSARetrieveTourPage wsaRetrieve = new WSARetrieveTourPage(tcconfig);

		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWVR spWVR = new SalePointWVR(tcconfig);
		SalePointOwnerPage spOwnerPage = new SalePointOwnerPage(tcconfig);
		SalePointPaymentScreen spPayment = new SalePointPaymentScreen(tcconfig);
		SalepointContractSummaryPage spContract = new SalepointContractSummaryPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaRetrieve.navigatetoretrievePitch();
			wsaRetrieve.retrievePitch();
			wsaLogin.WSALogoutApp();
			spLogin.loginToSP("IE");
			spHome.companySelectSalePoint();
			try {
				spWVR.sp_WVR_ValidateAutoPopUp();
			} catch (Exception e) {
				tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.INFO,
						"Automation Pop Up message is not Displyaed for first Search");
				try {
					spWVR.sp_WVR_ValidateAutoPopUp();
				} catch (Exception e2) {
					tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.FAIL,
							"Automation Pop Up message is not Displyaed for Second Search");
				}
			}
			spOwnerPage.sp_WVR_edit_PrimaryOwner();
			spPayment.sp_WVR_edit_Payment();
			spContract.sp_Contract_WVR_Discovery_Save();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.voidContractAutomation();
			spLogin.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Abhijeet Function/event:
	 * tc_033_SP_WBW_Experience_Contaract_AutomationFlow Description: Experience
	 * Contaract Automation Flow
	 */

	@Test(dataProvider = "testData")
	public void tc_033_SP_WBW_Experience_Contaract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		WSARetrieveTourPage wsaRetrieve = new WSARetrieveTourPage(tcconfig);

		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBW = new SalePointWBW(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaRetrieve.navigatetoretrievePitch();
			wsaRetrieve.retrievePitch();
			wsaLogin.WSALogoutApp();
			// spLogin.loginToSP(strBrowserInUse);
			spLogin.loginToSP("IE");
			spHome.companySelectSalePoint();
			spWBW.sp_WBW_Contract_Create_Automation_Flow();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Abhijeet Function/event:
	 * tc_034_SP_WBW_Worldmark_Contaract_AutomationFlow Description: WBW
	 * Worldmark Contaract Automation Flow
	 */

	@Test(dataProvider = "testData")
	public void tc_034_SP_WBW_Worldmark_Contaract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		WSARetrieveTourPage wsaRetrieve = new WSARetrieveTourPage(tcconfig);

		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBW = new SalePointWBW(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaRetrieve.navigatetoretrievePitch();
			wsaRetrieve.retrievePitch();
			wsaLogin.WSALogoutApp();
			// spLogin.loginToSP(strBrowserInUse);
			spLogin.loginToSP("IE");
			spHome.companySelectSalePoint();
			spWBW.sp_WBW_Contract_Create_Automation_Flow();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_001_WBW_Experience_Contract_Finalize Test Case:
	 * TC020_SalePoint_WBW_ Experience Contract_Payment_Financed_CC
	 * Autopay_Discover Description: WBW Experience Contract Creation with CC
	 * Auto Pay and Discovery, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_001_WBW_Experience_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWDownPaymentsPage wbwDownPayment = new WBWDownPaymentsPage(tcconfig);
		WBWFinanceInformationPage wbwFinanceInfo = new WBWFinanceInformationPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterTourID();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.selectPointsPackage();
			wbwPurchaseInfo.clickNextButton();
			wbwDownPayment.downPaymentPageNavigation();
			wbwDownPayment.selectFullDownPayment();
			wbwDownPayment.clickNextButton();
			wbwFinanceInfo.financeInformationPageNavigation();
			wbwFinanceInfo.selectPaymentMethod();
			wbwFinanceInfo.enterPaymentDetails();
			wbwFinanceInfo.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_002_WBW_Worldmark_Contract_Finalize Test Case:
	 * TC021_SalePoint_WBW_Verify that User is able to create Worldmark
	 * Contract_Financed_CC Auto_Master Description: WBW Worldmark Contract
	 * Creation with CC Autopay and Master Card, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_002_WBW_Worldmark_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterTourID();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_003_WBW_Conversion_Contract_Finalize Test Case:
	 * TC022_SalePoint_WBW_Verify that user is able to create Conversion
	 * Contract with Financed_CC auto Pay_CC VISA Description: WBW Conversion
	 * Contract Creation with CC Autopay and CC VISA, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_003_WBW_Conversion_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterMemberNumber();
			spWBWPage.clickNextButton();
			spWBWPage.enterTourID();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.finalizeContract();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_004_WBW_Worldmark_EditContract_Finalize Test Case:
	 * TC030_SalePoint_WBW_Verify user can edit the existing WBW Worldmark
	 * contract and update the primary owner Description: WBW WorldMark Contract
	 * Creation, and then edit any info and finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_004_WBW_Worldmark_EditContract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
		WBWEditContractPage editContract = new WBWEditContractPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterTourID();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			editContract.editContractPageNavigation();
			editContract.enterMemberNumber();
			editContract.clickNextButton();
			wbwOwnersList.editPrimaryOwnerandUpdate();
			wbwOwnersList.actionOnSecondaryOwner(false);
			wbwOwnersList.clickNextButton();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_005_WBW_Upgrade_Contract_Finalize Test Case:
	 * TC023_SalePoint_WBW_Upgrade_Verify that user is able to Upgrade existing
	 * Contract_ payment CC AMEX Description: WBW Upgrade Contract Creation with
	 * CC Autopay and AMEX, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_005_WBW_Upgrade_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
		WBWUpgradeContractPage wbwUpgradeContract = new WBWUpgradeContractPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			wbwUpgradeContract.enterMemberNumber();
			wbwUpgradeContract.clickSearchButton();
			wbwUpgradeContract.enterTSCCodeOnly();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.selectSignature();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_006_WBW_Upgrade_Edit_Finalize Test Case:
	 * TC031_SalePoint_WBW_Upgrade_Verify that user can able to edit existing
	 * Upgrade WBW manual contract and update Description: WBW Edit Upgrade
	 * Contract which is in In Progress Status, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_006_WBW_Upgrade_Edit_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
		WBWUpgradeContractPage wbwUpgradeContract = new WBWUpgradeContractPage(tcconfig);
		WBWEditContractPage editContract = new WBWEditContractPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			wbwUpgradeContract.enterMemberNumber();
			wbwUpgradeContract.clickSearchButton();
			wbwUpgradeContract.enterTSCCodeOnly();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			editContract.editContractPageNavigation();
			editContract.enterMemberNumber();
			editContract.clickNextButton();
			wbwUpgradeContract.enterTSCCodeOnly();
			wbwOwnersList.editPrimaryOwnerandUpdate();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.selectSignature();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_007_WBW_Upgrade_Contract_Void Test Case:
	 * TC023_SalePoint_WBW_Upgrade_Verify that user is able to Upgrade existing
	 * Contract_ payment CC AMEX Description: WBW Upgrade Contract Creation with
	 * CC Autopay and AMEX, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_007_WBW_Upgrade_Contract_Void(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
		WBWComissionsPage wbwComission = new WBWComissionsPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
		WBWUpgradeContractPage wbwUpgradeContract = new WBWUpgradeContractPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			wbwUpgradeContract.enterMemberNumber();
			wbwUpgradeContract.clickSearchButton();
			wbwUpgradeContract.enterTSCCodeOnly();
			spWBWPage.acceptOrDeclineAutomationFlow(false);
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectPaymentType();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectDownPayment();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwComission.comissionNavigation();
			wbwComission.enterSalePerson();
			wbwComission.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.selectSignature();
			wbwSummary.voidContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_008_WVRAP_Experience_Contract_Finalize Test Case:
	 * TC042_SalePoint_WVRAP_ Experience Contract_Payment_Financed_CC
	 * Autopay_Diners down payment Description: WVRAP Experience Contract
	 * Creation with CC Auto Pay and Diners, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_008_WVRAP_Experience_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WVRAPTourInformationPage wvrapTourPage = new WVRAPTourInformationPage(tcconfig);
		WVRAPOwnersListPage wvrapOwnersList = new WVRAPOwnersListPage(tcconfig);
		WVRAPPurchaseInformationPage wvrapPurchaseInfo = new WVRAPPurchaseInformationPage(tcconfig);
		WVRAPDownPaymentsPage wvrapDownPayment = new WVRAPDownPaymentsPage(tcconfig);
		WVRAPFinanceInformationPage wvrapFinanceInfo = new WVRAPFinanceInformationPage(tcconfig);
		WVRAPComissionsPage wvrapComission = new WVRAPComissionsPage(tcconfig);
		WVRAPSummaryPage wvrapSummary = new WVRAPSummaryPage(tcconfig);
		WVRAPContractManagementPage wvrapContractMgmt = new WVRAPContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wvrapTourPage.navigateToMenuURL();
			wvrapTourPage.enterTourID();
			wvrapTourPage.acceptOrDeclineAutomationFlow(false);
			wvrapOwnersList.ownerInformationPageNavigation();
			wvrapPurchaseInfo.purchaseInformationNavigation();
			wvrapPurchaseInfo.selectPaymentType();
			wvrapPurchaseInfo.selectPointsPackage();
			wvrapPurchaseInfo.clickNextButton();
			wvrapDownPayment.downPaymentPageNavigation();
			wvrapDownPayment.selectFullDownPayment();
			wvrapDownPayment.clickNextButton();
			wvrapFinanceInfo.financeInformationPageNavigation();
			wvrapFinanceInfo.selectPaymentMethod();
			wvrapFinanceInfo.enterPaymentDetails();
			wvrapFinanceInfo.clickNextButton();
			wvrapComission.comissionNavigation();
			wvrapComission.enterFrontRep();
			wvrapComission.enterFrontManager();
			wvrapComission.enterExitRep();
			wvrapComission.clickNextButton();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.saveContract();
			wvrapSummary.clickPrintCTA();
			wvrapSummary.captureContractNumber();
			wvrapSummary.captureMemberNumber();
			wvrapContractMgmt.contractManagementPageNavigation();
			wvrapContractMgmt.selectBeginDate(1);
			wvrapContractMgmt.clickNextButton();
			wvrapContractMgmt.selectContract();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_009_WVRAP_Worldmark_Contract_Finalize Test Case:
	 * TC043_SalePoint_WVRAP_Verify that User is able to create Worldmark
	 * Contract_Financed_CC autopay_VISA down payment type Description: WVRAP
	 * Worldmark Contract Creation with CC Auto Pay and Visa, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_009_WVRAP_Worldmark_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WVRAPTourInformationPage wvrapTourPage = new WVRAPTourInformationPage(tcconfig);
		WVRAPOwnersListPage wvrapOwnersList = new WVRAPOwnersListPage(tcconfig);
		WVRAPPurchaseInformationPage wvrapPurchaseInfo = new WVRAPPurchaseInformationPage(tcconfig);
		WVRAPPaymentOptionsPage wvrapPaymentOptions = new WVRAPPaymentOptionsPage(tcconfig);
		WVRAPComissionsPage wvrapComission = new WVRAPComissionsPage(tcconfig);
		WVRAPSummaryPage wvrapSummary = new WVRAPSummaryPage(tcconfig);
		WVRAPContractManagementPage wvrapContractMgmt = new WVRAPContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wvrapTourPage.navigateToMenuURL();
			wvrapTourPage.enterTourID();
			wvrapTourPage.acceptOrDeclineAutomationFlow(false);
			wvrapOwnersList.ownerInformationPageNavigation();
			wvrapPurchaseInfo.purchaseInformationNavigation();
			wvrapPurchaseInfo.selectPaymentType();
			wvrapPurchaseInfo.clickNextButton();
			wvrapPaymentOptions.paymentOptionsNavigation();
			wvrapPaymentOptions.selectDownPayment();
			wvrapPaymentOptions.selectPaymentMethod();
			wvrapPaymentOptions.enterPaymentDetails();
			wvrapPaymentOptions.selectSameAutoPay();
			wvrapPaymentOptions.clickNextButton();
			wvrapComission.comissionNavigation();
			wvrapComission.enterSalePerson();
			wvrapComission.clickNextButton();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.saveContract();
			wvrapSummary.clickPrintCTA();
			wvrapSummary.captureContractNumber();
			wvrapSummary.captureMemberNumber();
			wvrapContractMgmt.contractManagementPageNavigation();
			wvrapContractMgmt.selectBeginDate(1);
			wvrapContractMgmt.clickNextButton();
			wvrapContractMgmt.selectContract();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_010_WVRAP_Conversion_Contract_Finalize Test Case:
	 * TC044_SalePoint_WVRAP_Verify that user is able to create Conversion
	 * Contract with Financed_CC auto Pay Description: WVRAP Conversion Contract
	 * Creation with CC Auto Pay, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_010_WVRAP_Conversion_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WVRAPTourInformationPage wvrapTourPage = new WVRAPTourInformationPage(tcconfig);
		WVRAPOwnersListPage wvrapOwnersList = new WVRAPOwnersListPage(tcconfig);
		WVRAPPurchaseInformationPage wvrapPurchaseInfo = new WVRAPPurchaseInformationPage(tcconfig);
		WVRAPPaymentOptionsPage wvrapPaymentOptions = new WVRAPPaymentOptionsPage(tcconfig);
		WVRAPComissionsPage wvrapComission = new WVRAPComissionsPage(tcconfig);
		WVRAPSummaryPage wvrapSummary = new WVRAPSummaryPage(tcconfig);
		WVRAPContractManagementPage wvrapContractMgmt = new WVRAPContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wvrapTourPage.navigateToMenuURL();
			wvrapTourPage.enterMemberNumber();
			wvrapTourPage.acceptOrDeclineAutomationFlow(false);
			wvrapOwnersList.ownerInformationPageNavigation();
			wvrapTourPage.enterTourID();
			wvrapPurchaseInfo.purchaseInformationNavigation();
			wvrapPurchaseInfo.selectPaymentType();
			wvrapPurchaseInfo.clickNextButton();
			wvrapPaymentOptions.paymentOptionsNavigation();
			wvrapPaymentOptions.selectDownPayment();
			wvrapPaymentOptions.selectPaymentMethod();
			wvrapPaymentOptions.enterPaymentDetails();
			wvrapPaymentOptions.selectSameAutoPay();
			wvrapPaymentOptions.clickNextButton();
			wvrapComission.comissionNavigation();
			wvrapComission.enterSalePerson();
			wvrapComission.enterCloser();
			wvrapComission.enterSpecialist();
			wvrapComission.clickNextButton();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.saveContract();
			wvrapSummary.clickPrintCTA();
			wvrapSummary.captureContractNumber();
			wvrapSummary.captureMemberNumber();
			wvrapContractMgmt.contractManagementPageNavigation();
			wvrapContractMgmt.selectBeginDate(1);
			wvrapContractMgmt.clickNextButton();
			wvrapContractMgmt.selectContract();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_011_WVRAP_Upgrade_Contract_Finalize Test Case:
	 * TC045_SalePoint_WVRAP_Upgrade_Verify that user is able to Upgrade
	 * Contract_ payment ACH Autopay Description: WVRAP Upgrade Contract
	 * Creation with ACH, Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_011_WVRAP_Upgrade_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WVRAPTourInformationPage wvrapTourPage = new WVRAPTourInformationPage(tcconfig);
		WVRAPPaymentOptionsPage wvrapPaymentOptions = new WVRAPPaymentOptionsPage(tcconfig);
		WVRAPComissionsPage wvrapComission = new WVRAPComissionsPage(tcconfig);
		WVRAPSummaryPage wvrapSummary = new WVRAPSummaryPage(tcconfig);
		WVRAPContractManagementPage wvrapContractMgmt = new WVRAPContractManagementPage(tcconfig);
		WVRAPUpgradeContractPage wvrapUpgrade = new WVRAPUpgradeContractPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wvrapTourPage.navigateToMenuURL();
			wvrapUpgrade.enterMemberNumber();
			wvrapUpgrade.clickSearchButton();
			wvrapUpgrade.enterTSCCodeOnly();
			wvrapPaymentOptions.paymentOptionsNavigation();
			wvrapPaymentOptions.selectDownPayment();
			wvrapPaymentOptions.selectPaymentMethod();
			wvrapPaymentOptions.enterPaymentDetails();
			wvrapPaymentOptions.selectSameAutoPay();
			wvrapPaymentOptions.clickNextButton();
			wvrapComission.comissionNavigation();
			wvrapComission.enterProjectDirector();
			wvrapComission.clickNextButton();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.saveContract();
			wvrapSummary.clickPrintCTA();
			wvrapSummary.captureContractNumber();
			wvrapSummary.captureMemberNumber();
			wvrapContractMgmt.contractManagementPageNavigation();
			wvrapContractMgmt.selectBeginDate(1);
			wvrapContractMgmt.clickNextButton();
			wvrapContractMgmt.selectContract();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.selectSignature();
			wvrapSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_012_WBW_Combine_Contract_Finalize Test Case:
	 * TC028_SalePoint_WBW_ Create a WBW Combine Manual contract Description:
	 * WBW Combine Contract Creation and Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_012_WBW_Combine_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWCombinePage wbwCombine = new WBWCombinePage(tcconfig);
		WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
		WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
		WBWFinanceInformationPage wbwFinanceInfo = new WBWFinanceInformationPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			wbwCombine.combineNavigation();
			wbwCombine.enterMemberNumber();
			wbwCombine.clickNextButton();
			wbwCombine.checkTotalContracts(1);
			wbwCombine.clickAddContracts();
			wbwCombine.combineNavigation();
			wbwCombine.enterCombineMemberNumber();
			wbwCombine.clickNextButton();
			wbwCombine.checkTotalContracts(2);
			wbwCombine.clickProcessCombine();
			wbwOwnersList.ownerInformationPageNavigation();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.selectFamilyCombine();
			if (wbwPurchaseInfo.checkSelectedPaymentType().equalsIgnoreCase("finance")) {
				wbwPurchaseInfo.clickNextButton();
				wbwFinanceInfo.financeInformationPageNavigation();
				wbwFinanceInfo.selectPaymentMethod();
				wbwFinanceInfo.enterPaymentDetails();
				wbwFinanceInfo.clickNextButton();
				wbwFinanceInfo.duesInformationPageNavigation();
				wbwFinanceInfo.selectDuesMethod();
				wbwFinanceInfo.enterPaymentDetails();
				wbwFinanceInfo.clickNextButton();
			} else {
				wbwPurchaseInfo.clickNextButton();
			}
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_013_WBW_ContractManagementDetails Test Case:
	 * TC050_Salepoint_Search_WBW Contract in Batch Report By Sales Type option
	 * Description: WBW Contract Management All Contract Details Check
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_013_WBW_ContractManagementDetails(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(2);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.verifyAllContractDetails();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_014_WVRAP_Combine_Contract_Finalize Test Case:
	 * TC053_SalePoint_Create Combine WVRAP contract from SalePointt
	 * Description: WVRAP Combine Contract Creation and Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_014_WVRAP_Combine_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WVRAPTourInformationPage wvrapTourPage = new WVRAPTourInformationPage(tcconfig);
		WVRAPCombinePage wvrapCombine = new WVRAPCombinePage(tcconfig);
		WVRAPOwnersListPage wvrapOwnersList = new WVRAPOwnersListPage(tcconfig);
		WVRAPPurchaseInformationPage wvrapPurchaseInfo = new WVRAPPurchaseInformationPage(tcconfig);
		WVRAPFinanceInformationPage wvrapFinanceInfo = new WVRAPFinanceInformationPage(tcconfig);
		WVRAPSummaryPage wvrapSummary = new WVRAPSummaryPage(tcconfig);
		WVRAPContractManagementPage wvrapContractMgmt = new WVRAPContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wvrapTourPage.navigateToMenuURL();
			wvrapCombine.combineNavigation();
			wvrapCombine.enterMemberNumber();
			wvrapCombine.clickNextButton();
			wvrapCombine.checkTotalContracts(1);
			wvrapCombine.clickAddContracts();
			wvrapCombine.combineNavigation();
			wvrapCombine.enterCombineMemberNumber();
			wvrapCombine.clickNextButton();
			wvrapCombine.checkTotalContracts(2);
			wvrapCombine.clickProcessCombine();
			wvrapOwnersList.ownerInformationPageNavigation();
			wvrapPurchaseInfo.purchaseInformationNavigation();
			wvrapPurchaseInfo.selectFamilyCombine();
			if (wvrapPurchaseInfo.checkSelectedPaymentType().equalsIgnoreCase("finance")) {
				wvrapPurchaseInfo.clickNextButton();
				wvrapFinanceInfo.financeInformationPageNavigation();
				wvrapFinanceInfo.selectPaymentMethod();
				wvrapFinanceInfo.enterPaymentDetails();
				wvrapFinanceInfo.clickNextButton();
				wvrapFinanceInfo.duesInformationPageNavigation();
				wvrapFinanceInfo.selectDuesMethod();
				wvrapFinanceInfo.enterPaymentDetails();
				wvrapFinanceInfo.clickNextButton();
			} else {
				wvrapPurchaseInfo.clickNextButton();
			}
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.saveContract();
			wvrapSummary.clickPrintCTA();
			wvrapSummary.captureContractNumber();
			wvrapSummary.captureMemberNumber();
			wvrapContractMgmt.contractManagementPageNavigation();
			wvrapContractMgmt.selectBeginDate(1);
			wvrapContractMgmt.clickNextButton();
			wvrapContractMgmt.selectContract();
			wvrapSummary.summaryPageNavigation();
			wvrapSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_015_WVRAP_Split_Contract_Finalize Test Case: TC046_Manual
	 * Flow_Salepoint_WVRAP_Verify that the user is able to create new contract
	 * Split type for WVRAP entity Description: WVRAP Split Contract Creation
	 * with Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_015_WVRAP_Split_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		WVRAPTourInformationPage wvrapTourPage = new WVRAPTourInformationPage(tcconfig);
		WVRAPOwnerInformationPage wvrapOwnersList = new WVRAPOwnerInformationPage(tcconfig);
		WVRAPSplitProcessPage wvrapSplitProcess = new WVRAPSplitProcessPage(tcconfig);
		WVRAPSplitPage wvrapSplit = new WVRAPSplitPage(tcconfig);
		WVRAPSummaryPage wvrapSummary = new WVRAPSummaryPage(tcconfig);
		WVRAPContractManagementPage wvrapContractMgmt = new WVRAPContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			wvrapTourPage.navigateToMenuURL();
			wvrapSplit.enterMemberNumber();
			wvrapSplit.splitInformationNavigation();
			wvrapSplit.checkTotalContracts(1);
			wvrapSplit.clickAddSplit();
			wvrapOwnersList.enterOwnerInformation();
			wvrapSplitProcess.splitProcessingNavigation();
			wvrapSplitProcess.enterSecondaryCredit();
			wvrapSplitProcess.selectFamilySplit();
			wvrapSummary.saveContract();
			wvrapSummary.selectSplitTransferor();
			wvrapSummary.clickPrintCTA();
			wvrapContractMgmt.contractManagementPageNavigation();
			wvrapContractMgmt.selectBeginDate(1);
			wvrapContractMgmt.clickNextButton();
			wvrapContractMgmt.selectContract();
			wvrapSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_016_WBW_Split_Contract_Finalize Test Case: TC029_Manual
	 * Flow_Salepoint_WBW_Verify that the user is able to create new contract
	 * Split type for WBW entity Description: WBW Split Contract Creation with
	 * Finalize
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_016_WBW_Split_Contract_Finalize(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHome = new SalePointHomePage(tcconfig);
		SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
		WBWOwnerInformationPage wbwOwnersList = new WBWOwnerInformationPage(tcconfig);
		WBWSplitProcessPage wbwSplitProcess = new WBWSplitProcessPage(tcconfig);
		WBWSplitPage wbwSplit = new WBWSplitPage(tcconfig);
		WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
		WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

		try {
			spLogin.launchApplication();

			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			wbwSplit.enterMemberNumber();
			wbwSplit.splitInformationNavigation();
			wbwSplit.checkTotalContracts(1);
			wbwSplit.clickAddSplit();
			wbwOwnersList.enterOwnerInformation();
			wbwSplitProcess.splitProcessingNavigation();
			wbwSplitProcess.enterSecondaryCredit();
			wbwSplitProcess.selectFamilySplit();
			wbwSummary.saveContract();
			wbwSummary.selectSplitTransferor();
			wbwSummary.clickPrintCTA();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.selectSignature();
			wbwSummary.finalizeContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_016_WBW_Experience_Contract_AutomationFlow Test Case:
	 * TC035_SalePoint_WBW_ Automation Flow to Salepoint_Verify that Experience
	 * pitch created in WSA is retrieved in Salepoint and a contract is able to
	 * be voided_Experience pitch_CC Payment Description: WBW Experience
	 * Contract Creation with Automation Flow
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_017_WBW_Experience_Contract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		WSARetrieveTourPage wsaRetrieve = new WSARetrieveTourPage(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaRetrieve.navigatetoretrievePitch();
			wsaRetrieve.retrievePitch();
			wsaLogin.WSALogoutApp();
			changeBrowser("IE");
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointHomePage spHome = new SalePointHomePage(tcconfig);
			SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
			WBWFinanceInformationPage wbwFinanceInfo = new WBWFinanceInformationPage(tcconfig);
			WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
			WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterTourID();
			spWBWPage.acceptOrDeclineAutomationFlow(true);
			wbwSummary.summaryPageNavigation();
			wbwSummary.clickEditFinanceInfo();
			wbwFinanceInfo.financeInformationPageNavigation();
			wbwFinanceInfo.selectPaymentMethod();
			wbwFinanceInfo.enterPaymentDetails();
			wbwFinanceInfo.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.voidContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_018_WBW_Upgrade_Contract_AutomationFlow Test Case:
	 * TC027_SalePoint_WBW_ Automation Flow to Salepoint_Verify that an Upgrade
	 * pitch created in WSA is retrieved in Salepoint and a contract is able to
	 * be created_Upgrade pitch_CC Payment Description: WBW Upgrade Contract
	 * Creation with Automation Flow
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_018_WBW_Upgrade_Contract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();

			changeBrowser("IE");
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointHomePage spHome = new SalePointHomePage(tcconfig);
			SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
			WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
			WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
			WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
			WBWPurchaseInformationPage wbwPurchaseInfo = new WBWPurchaseInformationPage(tcconfig);
			WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);
			WBWUpgradeContractPage wbwUpgradeContract = new WBWUpgradeContractPage(tcconfig);
			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			wbwUpgradeContract.enterMemberNumber();
			wbwUpgradeContract.clickSearchButton();
			spWBWPage.acceptOrDeclineAutomationFlow(true);
			wbwSummary.summaryPageNavigation();
			wbwSummary.clickEditFinanceInfo();
			wbwOwnersList.clickNextButton();
			wbwPurchaseInfo.purchaseInformationNavigation();
			wbwPurchaseInfo.updateProcessingFee();
			wbwPurchaseInfo.clickNextButton();
			wbwPaymentOptions.paymentOptionsNavigation();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwSummary.summaryPageNavigation();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.voidContract();
			spLogin.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event:
	 * salepoint_tc_019_WBW_Conversion_Contract_AutomationFlow Test Case:
	 * TC026_SalePoint_WBW_ Automation Flow to Salepoint_Verify that Conversion
	 * pitch created in WSA is retrieved in Salepoint and a contract is able to
	 * be created_Conversion pitch_CC Payment Description: WBW Conversion
	 * Contract Creation with Automation Flow
	 */
	@Test(dataProvider = "testData")
	public void salepoint_tc_019_WBW_Conversion_Contract_AutomationFlow(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();

			changeBrowser("IE");
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointHomePage spHome = new SalePointHomePage(tcconfig);
			SalePointWBW spWBWPage = new SalePointWBW(tcconfig);
			WBWSummaryPage wbwSummary = new WBWSummaryPage(tcconfig);
			WBWContractManagementPage wbwContractMgmt = new WBWContractManagementPage(tcconfig);
			WBWOwnersListPage wbwOwnersList = new WBWOwnersListPage(tcconfig);
			WBWPaymentOptionsPage wbwPaymentOptions = new WBWPaymentOptionsPage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spHome.companySelectSalePoint();
			spWBWPage.navigateToMenuURL();
			spWBWPage.enterMemberNumber();
			spWBWPage.clickNextButton();
			spWBWPage.acceptOrDeclineAutomationFlow(true);
			wbwSummary.summaryPageNavigation();
			wbwSummary.clickEditFinanceInfo();
			wbwOwnersList.clickNextButton();
			wbwPaymentOptions.selectPaymentMethod();
			wbwPaymentOptions.enterPaymentDetails();
			wbwPaymentOptions.selectSameAutoPay();
			wbwPaymentOptions.clickNextButton();
			wbwSummary.saveContract();
			wbwSummary.clickPrintCTA();
			wbwSummary.captureContractNumber();
			wbwSummary.captureMemberNumber();
			wbwContractMgmt.contractManagementPageNavigation();
			wbwContractMgmt.selectBeginDate(1);
			wbwContractMgmt.clickNextButton();
			wbwContractMgmt.selectContract();
			wbwSummary.summaryPageNavigation();
			wbwSummary.voidContract();
			spLogin.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Unnat Jain Function/event: salepoint_tc_047_WVR_OptIn_Contract Test
	 * Case: TC047_Salepoint_Opt In Contract Description: WVR Opt In Contract
	 * Creation
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_047_WVR_OptIn_Contract(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Unnat Jain Function/event: salepoint_tc_048_WVR_OptOut_Contract
	 * Test Case: Description: WVR Opt Out Contract UDI/Club
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_048_WVR_OptOut_Contract(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);
		WVREditSiteInfoPage editSiteInfo = new WVREditSiteInfoPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			editSiteInfo.editSiteInfoNavigation();
			editSiteInfo.selectSite();
			editSiteInfo.selectOptOut();
			editSiteInfo.selectOptOutTerm();
			editSiteInfo.clickSaveButton();
			spEditUser.Update_DefaultLocation();
			sp.Logout();
			sp.loginToApp();
			spCompanySelect.companySelect();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.selectRCIRadio();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase(false);
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: tc055_SP_WVR_InvalidDownpaymentValidation
	 * Description: TC055_SalePoint_WVR_Validate downpayment adjustment with
	 * invalid payment value WVR Contract Booking in Salepoint application and
	 * Finalising
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_055_WVR_InvalidDownpaymentValidation(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
		SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spPayment.selectPaymentMethod();
			spPayment.enterInvalidPaymentAmount();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: salepoint_tc_018_WVR_EditUDIContract
	 * Description: TC018_Salepoint_WVR_Verify that the user is able to edit UDI
	 * manual contract
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_018_WVR_EditUDIContract(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
		SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointEditContractPage spEditContractPage = new SalepointEditContractPage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			spHomePage.navigateToPage("EditContractURL");
			spEditContractPage.editContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: salepoint_tc_010_WVR_CreateDiscoveryContract
	 * Description: TC010_Salepoint_WVR_Verify that the user is able Create
	 * Discovery manual contract
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_010_WVR_CreateDiscoveryContract(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointDiscoveryPackagePage spDiscPack = new SalepointDiscoveryPackagePage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointEditContractPage spEditContractPage = new SalepointEditContractPage(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spLocateCustomer.navigateToMenu("");
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spDiscPack.selectPackage();
			spDiscPack.clickNextButton();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			spHomePage.navigateToPage("EditContractURL");
			spEditContractPage.editContract();
			spHomePage.navigateToPage("printDocumentURL");
			spContractPrintPage.contractSearchandPrint();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: salepoint_tc_014_WVR_CreateTradeContract
	 * Description: TC010_Salepoint_WVR_Verify that the user is able Create
	 * Trade contract
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_014_WVR_CreateTradeContract(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointTradesPage spTradesPage = new SalepointTradesPage(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
		SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spHomePage.navigateToPage("TradeURL");
			spTradesPage.enterContractInfo();
			spTradesPage.viewTradedContract();
			spTradesPage.processTrade();
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Utsav Function/event: salepoint_tc_012_WVR_DiscUpgradeContract
	 * Description: TC012_SalePoint_WVR_Discovery upgrade_Verify that user is
	 * able to create Discovery Upgrade Contract with auto pay CC VISA
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_012_WVR_DiscUpgradeContract(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointDiscUpgradePage spDiscUpgradePage = new SalepointDiscUpgradePage(tcconfig);
		SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
		SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
		SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
		SalepointContractPaymentPage spPayment = new SalepointContractPaymentPage(tcconfig);
		SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
		SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
		SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
		SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
		SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
		SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
		SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spHomePage.navigateToPage("DiscUpgradeURL");
			spDiscUpgradePage.enterMemberInfo();
			spDiscUpgradePage.processUpgrade();
			spLocateCustomer.provideCRSNumber();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sp.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}
	}

	/*
	 * Name: Utsav Function/event: salepoint_tc_052_WVR_MGVCContract_AutoFlow
	 * Description: TC052_SalePoint_WVR_Create New MGVC Contract using pitch
	 * created in WSA
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_052_WVR_MGVCContract_AutoFlow(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();
			changeBrowser("IE");

			SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointWVR spWVR = new SalePointWVR(tcconfig);
			SalePointOwnerPage spOwnerPage = new SalePointOwnerPage(tcconfig);
			SalePointPaymentScreen spPayment = new SalePointPaymentScreen(tcconfig);
			SalepointContractSummaryPage spContract = new SalepointContractSummaryPage(tcconfig);
			SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spEditUser.Update_DefaultLocation();
			try {
				spWVR.sp_WVR_ValidateAutoPopUp();
			} catch (Exception e) {
				tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.INFO,
						"Automation Pop Up message is not Displyaed for first Search");
				try {
					spWVR.sp_WVR_ValidateAutoPopUp();
				} catch (Exception e2) {
					tcconfig.updateTestReporter("SalePointWVRMenuPage", "sp_WVR_ValidateAutoPopUp", Status.FAIL,
							"Automation Pop Up message is not Displyaed for Second Search");
				}
			}
			spOwnerPage.sp_WVR_edit_PrimaryOwner();
			spPayment.sp_WVR_edit_Payment();
			spContract.sp_Contract_WVR_Save();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.voidContractAutomation();
			spLogin.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event:
	 * salepoint_tc_013_WVR_DiscUpgradeContract_AutoFlow Description:
	 * TC013_Salepoint_Automation Flow to Salepoint_WVR_Verify that a Discovery
	 * Upgrade- UDI pitch created in WSA is retrieved in Salepoint and a
	 * contract is able to be created
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_013_WVR_DiscUpgradeContract_AutoFlow(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);

		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();
			changeBrowser("IE");

			SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
			SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
			SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
			SalepointDiscUpgradePage spDiscUpgradePage = new SalepointDiscUpgradePage(tcconfig);
			SalepointLocateCustomerPage spLocateCustomer = new SalepointLocateCustomerPage(tcconfig);
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointOwnerPage spOwnerPage = new SalePointOwnerPage(tcconfig);
			SalePointPaymentScreen spPayment = new SalePointPaymentScreen(tcconfig);
			SalepointContractSummaryPage spContract = new SalepointContractSummaryPage(tcconfig);
			SalepointContractManagementPage sptransmitContract = new SalepointContractManagementPage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spHomePage.navigateToPage("DiscUpgradeURL");
			spDiscUpgradePage.enterMemberInfo();
			spDiscUpgradePage.processUpgrade();
			spLocateCustomer.provideCRSNumber();
			spOwnerPage.sp_WVR_edit_PrimaryOwner();
			spPayment.sp_WVR_edit_Payment();
			spContract.sp_Contract_WVR_Save();
			sptransmitContract.navigateToContractManagementPage();
			sptransmitContract.clickOptionAllByDate();
			sptransmitContract.getBatchReportData();
			sptransmitContract.voidContractAutomation();
			spLogin.LogoutSP();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

		}

	}

	/*
	 * Name: Utsav Function/event:
	 * salepoint_tc_005_WVR_MGVCContract_SecOwner_AutoFlow Description:
	 * TC005_SalePoint_WVR_New Contract UDI_Verify that user is able to create
	 * UDI MGVC Standard contract with New Member by Swapping Primary Owner
	 * converts automation flow to Manual
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_005_WVR_MGVCContract_SecOwner_AutoFlow(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();

			changeBrowser("IE");
			SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
			SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointWVR spWVR = new SalePointWVR(tcconfig);
			SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
			SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
			SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
			SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
			SalepointContractPaymentPage spConPayment = new SalepointContractPaymentPage(tcconfig);
			SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
			SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
			SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
			SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
			SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
			SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spWVR.sp_WVR_DismissAutoPopUp();
			spContractOwners.changeStatus();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spConPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event:
	 * salepoint_tc_007_WVR_MGVCPRContract_SecOwner_AutoFlow Description:
	 * TC007_SalePoint_WVR_New Contract UDI_Verify that user is able to create
	 * UDI MGVC-PR contract by Swapping Primary Owner converts automation flow
	 * to Manual Swapping Primary Owner converts automation flow to Manual
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_007_WVR_MGVCPRContract_SecOwner_AutoFlow(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();

			changeBrowser("IE");
			SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
			SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointWVR spWVR = new SalePointWVR(tcconfig);
			SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
			SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
			SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
			SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
			SalepointContractPaymentPage spConPayment = new SalepointContractPaymentPage(tcconfig);
			SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
			SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
			SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
			SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
			SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
			SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spWVR.sp_WVR_DismissAutoPopUp();
			spContractOwners.changeStatus();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spConPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event:
	 * salepoint_tc_008_WVR_CWAContract_SecOwner_AutoFlow Description:
	 * TC008_SalePoint_WVR_New Contract UDI_Verify that user is able to create
	 * UDI CWA contract with New Member by Swapping Primary Owner converts
	 * automation flow to Manual Swapping Primary Owner converts automation flow
	 * to Manual
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_008_WVR_CWAContract_SecOwner_AutoFlow(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		try {
			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();

			changeBrowser("IE");

			SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
			SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointWVR spWVR = new SalePointWVR(tcconfig);
			SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
			SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
			SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
			SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
			SalepointContractPaymentPage spConPayment = new SalepointContractPaymentPage(tcconfig);
			SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
			SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
			SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
			SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
			SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
			SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spWVR.sp_WVR_DismissAutoPopUp();
			spContractOwners.changeStatus();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spConPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event:
	 * salepoint_tc_009_WVR_CWAPRContract_SecOwner_AutoFlow Description:
	 * TC009_SalePoint_WVR_New Contract UDI_Verify that user is able to create
	 * UDI CWA-PR contract by Swapping Primary Owner converts automation flow to
	 * Manual Swapping Primary Owner converts automation flow to Manual
	 */

	@Test(dataProvider = "testData")
	public void salepoint_tc_009_WVR_CWAPRContract_SecOwner_AutoFlow(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		WSALoginPage wsaLogin = new WSALoginPage(tcconfig);
		WSAHomePage wsaHome = new WSAHomePage(tcconfig);
		WSATourLookupPage wsaTour = new WSATourLookupPage(tcconfig);
		WSANewProposalPage wsaPitchCreate = new WSANewProposalPage(tcconfig);
		try {

			wsaLogin.launchApplication();

			wsaLogin.loginToWSA(strBrowserInUse);
			wsaHome.selectEntityWSA();
			wsaHome.pitchTypeSelectionWSA();
			wsaTour.tourLookUpWSA();
			wsaPitchCreate.createNewPitchWSA();
			wsaLogin.WSALogoutApp();

			changeBrowser("IE");
			SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
			SalepointEditUserInformation spEditUser = new SalepointEditUserInformation(tcconfig);
			SalePointLoginPage spLogin = new SalePointLoginPage(tcconfig);
			SalePointWVR spWVR = new SalePointWVR(tcconfig);
			SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
			SalepointContractProcessingOwners spContractOwners = new SalepointContractProcessingOwners(tcconfig);
			SalepointContractDataEntryPage spDataEntry = new SalepointContractDataEntryPage(tcconfig);
			SalepointInventorySelectionPage spInventory = new SalepointInventorySelectionPage(tcconfig);
			SalepointContractPaymentPage spConPayment = new SalepointContractPaymentPage(tcconfig);
			SalepointCommissionandIncentivePage spSalesPerson = new SalepointCommissionandIncentivePage(tcconfig);
			SalepointMiscellaneousInformationPage spmiscellaneous = new SalepointMiscellaneousInformationPage(tcconfig);
			SalepointContractSummaryPage spContractSummaryPage = new SalepointContractSummaryPage(tcconfig);
			SalepointContractPrintPage spContractPrintPage = new SalepointContractPrintPage(tcconfig);
			SalepointContractFeesPage spFeesPage = new SalepointContractFeesPage(tcconfig);
			SalepointContractPriceOverridePage spPriceOver = new SalepointContractPriceOverridePage(tcconfig);

			spLogin.navigateToSalepointApplication();
			spLogin.loginToApp();
			spCompanySelect.companySelect();
			spEditUser.Update_DefaultLocation();
			spWVR.sp_WVR_DismissAutoPopUp();
			spContractOwners.changeStatus();
			spContractOwners.contractProcessingOwnersNew();
			spDataEntry.contractProcessingDataEntrySelection();
			spDataEntry.clickNextButton();
			spInventory.contractProcessingInventrySelectionPhase();
			spFeesPage.contractFeesScreen();
			spPriceOver.clicknextButton();
			spConPayment.contractProcessingPayment();
			spSalesPerson.selectSalesperson();
			spmiscellaneous.contrctProcessingMiscellaneousInformation();
			spContractSummaryPage.clicksave();
			spContractPrintPage.contractClickPrintButton();
			spContractPrintPage.contractProcessingContractPrinting();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	//// WORKDAY SCRIPTS

	/*
	 * Name: Utsav Function/event: Description:
	 * TC_301_WD_SaleRep_Search_Criteria_Validation_WBW
	 */
	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_301_WD_SaleRep_Search_Criteria_Validation_WBW(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.go_to_Admin_SSAssignment();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: Description:
	 * TC_302_WD_SaleRep_Search_Criteria_Validation_WVR
	 */
	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_302_WD_SaleRep_Search_Criteria_Validation_WVR(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.go_to_Admin_SSAssignment();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_303_WD_SaleRep_Search_All_Salesman_per_site_WBW
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_303_WD_SaleRep_Search_All_Salesman_per_site_WBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.All_Salesman_Validation();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_304_WD_SaleRep_Search_All_Salesman_per_site_WVR
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_304_WD_SaleRep_Search_All_Salesman_per_site_WVR(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.All_Salesman_Validation();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: Description:
	 * TC_305_WD_SaleRep_Search_Name_WBW
	 */
	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_305_WD_SaleRep_Search_Name_WBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_Name_Validation();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_306_WD_SaleRep_Search_Name_WVR
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_306_WD_SaleRep_Search_Name_WVR(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_Name_Validation();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_307_WD_SaleRep_Search_Salesman_Number_WBW
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_307_WD_SaleRep_Search_Salesman_Number_WBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_id_Validation();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_308_WD_SaleRep_Search_Salesman_Number_WVR
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_308_WD_SaleRep_Search_Salesman_Number_WVR(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_id_Validation();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_309_WD_SaleRep_Edit_Assign_Site_Multiple_WBW
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_309_WD_SaleRep_Edit_Assign_Site_Multiple_WBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_id_Validation();
			spAdminpage.Select_Mutliple_Site_Assign();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_310_WD_SaleRep_Edit_Assign_Site_Multiple_WVR
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_310_WD_SaleRep_Edit_Assign_Site_Multiple_WVR(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Search_Salesman_id_Validation();
			spAdminpage.Select_Mutliple_Site_Assign();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_311_WD_SaleRep_Role_Based_Test_Access_WBW
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_311_WD_SaleRep_Role_Based_Test_Access_WBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Acess_Validation_Positive();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: Description:
	 * TC_312_WD_SaleRep_Role_Based_Test_Access_WVR
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_312_WD_SaleRep_Role_Based_Test_Access_WVR(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Acess_Validation_Positive();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: Description:
	 * TC_313_WD_SaleRep_Role_Based_Test_No_Access_WBW
	 */
	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_313_WD_SaleRep_Role_Based_Test_No_Access_WBW(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Acess_Validation_Negative();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * Name: Utsav Function/event: Description:
	 * TC_314_WD_SaleRep_Role_Based_Test_No_Access_WVR
	 */

	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_314_WD_SaleRep_Role_Based_Test_No_Access_WVR(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Acess_Validation_Negative();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: Description:
	 * TC_315_WD_SaleRep_Search_Search_Category_validation_WVRAP
	 */
	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_315_WD_SaleRep_Search_Search_Category_validation_WVRAP(Map<String, String> testData)
			throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.validate_selected_category();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Name: Utsav Function/event: Description:
	 * TC_316_WD_SalesRep_Role_Based_Test_No_Access_WVRAP
	 */
	@Test(dataProvider = "testData", groups = { "Workday" })
	public void TC_316_WD_SalesRep_Role_Based_Test_No_Access_WVRAP(Map<String, String> testData) throws Exception {
		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointAdminPage spAdminpage = new SalePointAdminPage(tcconfig);
		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spAdminpage.Acess_Validation_Negative();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	// WYN RESALE

	/*
	 * 
	 * Function/event: tc005_SP_WBW_VerifyHardStopMessage_NonPaidOffWAAM
	 * Description: Verify that sale point is displaying hard stop message when
	 * trying to upgrade for WBW WAAM that owns a non-paid off WAAM
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_505_SP_WBW_VerifyHardStopMessage_NonPaidOffWAAM(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointUpgradeContract spUpgradeContractPage = new SalePointUpgradeContract(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spUpgradeContractPage.memberSearchForNonPaidOffWAAM();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event: tc006_SP_WBW_VerifyNoHardStopMessage_PaidOffWAAM
	 * Description: Verify that sale point is not displaying hard stop message
	 * when trying to upgrade for WBW WAAM that owns a paid off WAAM
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_506_SP_WBW_VerifyNoHardStopMessage_PaidOffWAAM(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointUpgradeContract spUpgradeContractPage = new SalePointUpgradeContract(tcconfig);

		try {
			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spUpgradeContractPage.memberSearchForPaidOffWAAM();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc007_SPResale_WVR_VerifyCreateandTransmit_BuyerContract_OtherVCCPayment_NewOwner
	 * Description: Verify user is able to create and transmit a buyer contract
	 * with Other VCC as payment method and WVR tours created from Salesforce
	 * for new owner
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_507_SPResale_WVR_VerifyCreateandTransmit_BuyerContract_OtherVCCPayment_NewOwner(
			Map<String, String> testData) throws Exception {

		setupTestData(testData);

		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.authorizeContract();
			spResalecontract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc007_SPResale_WVR_VerifyCreateandTransmit_BuyerContract_OtherVCCPayment_NewOwner
	 * Description: Verify user is able to create and transmit a buyer contract
	 * with Other VCC as payment method and WVR tours created from Salesforce
	 * for new owner
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_508_SPResale_WVR_VerifyCreateandTransmit_BuyerContract_OtherVCCPayment_ExistingOwner(
			Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.authorizeContract();
			spResalecontract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	/*
	 * 
	 * Function/event:
	 * tc009_SPResale_WVR_VerifyDefaultDataNotEditable_BuyerContract_NewOwner
	 * Description: Verify user is unable to change default data for buyer
	 * contract while editing it for new users
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_509_SPResale_WVR_VerifyDefaultDataNotEditable_BuyerContract_NewOwner(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.defaultData_NotEditable();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc010_SPResale_WVR_ValidateStatus_ContractSelection_RemoveHold
	 * Description: Verify user is able to change the status of resale eligible
	 * contract to HOLD and accordingly HOLD USER column value will be updated
	 * and Verify on clicking the Remove Hold button STATUS column value will be
	 * updated to Available and HOLD USER column value will be blank
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_510_SPResale_WVR_ValidateStatus_ContractSelection_RemoveHold(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.validateStatus_resaleContractSelection();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event: tc011_SPResale_WVR_PrintForm_AlwaysPrintSelected
	 * Description: Verify user is able to print a form for resale contract when
	 * Always Print radio button is selected for Resale in Edit Print Forms and
	 * Rules screen
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_511_SPResale_WVR_PrintForm_AlwaysPrintSelected(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);
		SalePointEditForm spEditForm = new SalePointEditForm(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spEditForm.checkAlwaysPrintForForms();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event: tc012_SPResale_WVR_VoidContract Description: Verify
	 * system shall set the status on the resale contract to available when the
	 * buyers contract is voided
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_512_SPResale_WVR_VoidContract(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.defaultData_NotEditable();
			spResalecontract.voidContract();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc013_SPResale_WVR_ContractSorting_ExpirationDate_AscendingOrder
	 * Description: Verify the contracts will be sorted based on the expiration
	 * date in ascending order in Contract Selection page
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_513_SPResale_WVR_ContractSorting_ExpirationDate_AscendingOrder(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.sortingContract_ExpirationDate();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event: tc014_SPResale_WVR_DefaultValueNotEditable_TotalNewPoints
	 * Description: Verify pop up message will get displayed if user changes pre
	 * populated value from Total New Points field in Inventory Selection Phase
	 * screen
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_514_SPResale_WVR_DefaultValueNotEditable_TotalNewPoints(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.editTotalNewPoints();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event: tc015_SPResale_WVR_AuthorizeContract Description:
	 * Contract Creation for VCC approved tour and then authorization of the
	 * same contract.
	 * 
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_515_SPResale_WVR_AuthorizeContract(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.authorizeContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc016_SPResale_WVR_CheckThirdPartyClosingFeeType_ACHAutoPay Description:
	 * Verify user is able to check pool value, BMT value and phase value for a
	 * wyndham resale buyer contract in CSS application for ACH payment method
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_516_SPResale_WVR_CheckThirdPartyClosingFeeType_ACHAutoPay(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.transmitContract();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc017_SPResale_WVR_CheckThirdPartyClosingFeeType_CCAutoPay Description:
	 * Verify user is able to check pool value, BMT value and phase value for a
	 * wyndham resale buyer contract in CSS application for CC Auto payment
	 * method
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_517_SPResale_WVR_CheckThirdPartyClosingFeeType_CCAutoPay(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.transmitContract();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event: tc018_SPResale_WVR_CheckThirdPartyClosingFeeType_Cash
	 * Description: Verify user is able to check pool value, BMT value and phase
	 * value for a wyndham resale buyer contract in CSS application for Cash
	 * payment method
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_518_SPResale_WVR_CheckThirdPartyClosingFeeType_Cash(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.transmitContract();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc019_SPResale_WVR_CheckThirdPartyClosingFeeType_ACHAutoPay_ExistingOwner
	 * Description: Verify closing cost fee is displaying as 135$ in summary
	 * page and in contract PDF for a resale contract with ACH as loan payment
	 * method for existing member number
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_519_SPResale_WVR_CheckThirdPartyClosingFeeType_ACHAutoPay_ExistingOwner(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.transmitContract();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc020_SPResale_WVR_CheckThirdPartyClosingFeeType_CCAutoPay_ExistingOwner
	 * Description: Verify user is able to check pool value, BMT value and phase
	 * value for a wyndham resale buyer contract in CSS application for CC Auto
	 * payment method
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_520_SPResale_WVR_CheckThirdPartyClosingFeeType_CCAutoPay_ExistingOwner(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.transmitContract();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * 
	 * Function/event:
	 * tc021_SPResale_WVR_CheckThirdPartyClosingFeeType_Cash_ExistingOwner
	 * Description: Verify user is able to check pool value, BMT value and phase
	 * value for a wyndham resale buyer contract in CSS application for Cash
	 * payment method
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })
	public void TC_521_SPResale_WVR_CheckThirdPartyClosingFeeType_Cash_ExistingOwner(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection();
			spResalecontract.contractFlow();
			spResalecontract.transmitContract();

			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc022_SPResale_WVR_validateSplitAndThirdPartyIndicator
	 * Description:Verify Split Indicator and Third Party Indicator on upgrade
	 * purchase screen Date: Apr/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWVR" })

	public void TC_522_SPResale_WVR_validateSplitAndThirdPartyIndicator(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResaleContractSelection spResalecontract = new SalePointResaleContractSelection(tcconfig);

		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spResalecontract.resaleContractSelection_MultiContract();
			spResalecontract.validateSplitAndThirdPartyIndicator();
			sp.Logout();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_01_ResaleWBW_BuyerContractCreationAndFinalize
	 * Description:Verify user is able to create and transmit a buyer contract
	 * and WBW tours created from Salesforce for new owner_Cash Date: Apr/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWBW" })

	public void tc_523_ResaleWBW_BuyerContractCreationAndFinalize_Financed(Map<String, String> testData)
			throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResalePage resalecontract = new SalePointResalePage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spHomePage.changeSite();
			resalecontract.resaleContractSelectionAndRelease();
			resalecontract.resaleContractSelection();
			resalecontract.enterMemberNumberAndTourId("Financed");
			resalecontract.removeAdditionalSecondaryOwner();
			resalecontract.worldmakePayment("Financed");
			resalecontract.contractConfirmation("Financed");
			resalecontract.finalizeContract();
			sp.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_02_ResaleWBW_BuyerContractCreationAndFinalize_Cash
	 * Description:Verify user is able to create and transmit a buyer contract
	 * and WBW tours created from Salesforce for new owner_Cash Date: Apr/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWBW" })

	public void tc_524_ResaleWBW_BuyerContractCreationAndFinalize_Cash(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResalePage resalecontract = new SalePointResalePage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spHomePage.changeSite();
			resalecontract.resaleContractSelection();
			resalecontract.enterMemberNumberAndTourId("Cash");
			resalecontract.removeAdditionalSecondaryOwner();
			resalecontract.worldmakePayment("Cash");
			resalecontract.contractConfirmation("Cash");
			resalecontract.finalizeContract();
			sp.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_03_ResaleWBW_BuyerContractCreationAndVoid Description:Verify
	 * system will change the status on the resale contract to available when
	 * the buyers contract is voided Date: Apr/2020 Author: Kamalesh Gupta
	 * Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWBW" })

	public void tc_525_ResaleWBW_BuyerContractCreationAndVoid(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResalePage resalecontract = new SalePointResalePage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spHomePage.changeSite();
			resalecontract.resaleContractSelection();
			resalecontract.enterMemberNumberAndTourId("Financed");
			resalecontract.removeAdditionalSecondaryOwner();
			resalecontract.worldmakePayment("Financed");
			resalecontract.contractConfirmation("Financed");
			resalecontract.voidContract();
			resalecontract.checkStatus_VoidContract();
			sp.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_04_ResaleWBW_ContractCreation_VCC Description:Verify user is
	 * able to create resale contract with VCC payment and authorize payment for
	 * a buyer contract associated with a resale eligible contrac Date: Apr/2020
	 * Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWBW" })

	public void tc_526_ResaleWBW_ContractCreation_VCC(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResalePage resalecontract = new SalePointResalePage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spHomePage.changeSite();
			resalecontract.resaleContractSelection();
			resalecontract.enterMemberNumberAndTourId("Financed");
			resalecontract.removeAdditionalSecondaryOwner();
			resalecontract.worldmakePayment_VCC();
			resalecontract.contractConfirmation("Financed");
			resalecontract.finalizeContract();
			sp.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_05_ResaleWBW_validateSplitAndThirdPartyIndicator
	 * Description:Verify Split Indicator and Third Party Indicator on upgrade
	 * purchase screen Date: Apr/2020 Author: Kamalesh Gupta Changes By: NA
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWBW" })

	public void tc_527_ResaleWBW_validateSplitAndThirdPartyIndicator(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResalePage resalecontract = new SalePointResalePage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spHomePage.changeSite();
			resalecontract.resaleContractSelection_MultiContract();
			resalecontract.validateSplitAndThirdPartyIndicator();
			sp.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * Method: tc_06_ResaleWBW_validateRowColor_ResaleContract
	 * Description:Verify that System highlighted the contracts in yellow and
	 * red if If expiration date is within 15 days, the entire row will be
	 * highlighted in Yellow Date: Apr/2020 Author: Kamalesh Gupta Changes By:
	 * NA
	 */

	@Test(dataProvider = "testData", groups = { "ResaleWBW" })

	public void tc_528_ResaleWBW_validateRowColor_ResaleContract(Map<String, String> testData) throws Exception {

		setupTestData(testData);
		SalePointLoginPage sp = new SalePointLoginPage(tcconfig);
		SalepointCompanySelect spCompanySelect = new SalepointCompanySelect(tcconfig);
		SalePointResalePage resalecontract = new SalePointResalePage(tcconfig);
		SalePointHomePage spHomePage = new SalePointHomePage(tcconfig);
		try {

			sp.launchApplication();

			sp.loginToApp();
			spCompanySelect.companySelect();
			spHomePage.changeSite();
			resalecontract.validateRowColor_ResaleContract();
			sp.Logout();
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}