package salepoint.pages;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.crypto.SealedObject;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointTradesPage extends SalePointBasePage {

	public SalepointTradesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}
	public static final Logger log = Logger.getLogger(SalepointTradesPage.class);

	
	@FindBy(xpath = "//input[@id = 'contract_number']")
	protected WebElement txtContractNumber;
	
	@FindBy(xpath = "//input[@value = 'Next']")
	protected WebElement buttonNext;
	
	@FindBy(xpath = "//input[@value = 'View Traded Contracts']")
	protected WebElement btnViewTradedContract;
	
	@FindBy(xpath = "//input[@value = 'Process Trades']")
	protected WebElement btnProcessTrades;

	
	public void enterContractInfo(){
		waitUntilElementVisibleIE(driver, txtContractNumber, "ExplicitLongWait");
		fieldDataEnter(txtContractNumber, testData.get("TradeContractNo"));
		clickElementJS(buttonNext);
		
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"User is able to search with the contract number");

		} catch (Exception e) {
			log.info("No Alert Present");
		}
	}
	
	public void viewTradedContract(){
		waitUntilElementVisibleIE(driver, btnViewTradedContract, "ExplicitLongWait");
		clickElementJS(btnViewTradedContract);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"User is able to click view traded contract button");		
	}
	
	public void processTrade(){
		waitUntilElementVisibleIE(driver, btnProcessTrades, "ExplicitLongWait");
		clickElementJS(btnProcessTrades);
		try {
			new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().dismiss();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			log.info("No Alert Present");
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"User is able to click Process Trade button");
		
	}
	
}