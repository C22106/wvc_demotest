package salepoint.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointInventorySelectionPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointInventorySelectionPage.class);

	public SalepointInventorySelectionPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(id = "newPoints")
	protected WebElement newPoints;

	@FindBy(id = "newPoints")
	protected WebElement newPointsCheck;

	@FindBy(xpath = "//select[@id='inventorySite']")
	protected WebElement inventory_Site;

	@FindBy(xpath = "//select[@id='inventoryPhase']")
	protected WebElement inventory_phase;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	/*
	 * Method: contractProcessingInventrySelectionPhase Description:Contract
	 * Processing Select Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void contractProcessingInventrySelectionPhase() throws Exception {

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLongWait");

		if (verifyElementDisplayed(newPoints)) {

			String strInvName = testData.get("invSite");
			(inventory_Site).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			driver.findElement(By.xpath("//select/option[contains(.,'" + strInvName + "')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String strInvPhase = testData.get("invPhase");
			(inventory_phase).click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			driver.findElement(By.xpath("//select/option[contains(.,'" + strInvPhase + "')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "NewPoints Field not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: contractProcessingInventrySelectionPhase Description:Contract
	 * Processing Select Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void contractProcessingInventrySelectionPhase(Boolean invSelectionRequired) throws Exception {

		waitUntilElementVisibleIE(driver, newPointsCheck, "ExplicitLongWait");

		if (verifyElementDisplayed(newPoints)) {
			if (invSelectionRequired) {
				String strInvName = testData.get("invSite");
				(inventory_Site).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.findElement(By.xpath("//select/option[contains(.,'" + strInvName + "')]")).click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				String strInvPhase = testData.get("invPhase");
				(inventory_phase).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.findElement(By.xpath("//select/option[contains(.,'" + strInvPhase + "')]")).click();

				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
			newPoints.click();
			newPoints.clear();
			newPoints.sendKeys(testData.get("strPoints"));

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "NewPoints Field not present");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		clickElementJS(nextVal);

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
}
