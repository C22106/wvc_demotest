package salepoint.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointContractPrintPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractPrintPage.class);

	public SalepointContractPrintPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public static String strContractNumber;
	public static String strMemberNumber;
	public static String strBonusContractNumber;
	@FindBy(id = "Print")
	protected WebElement printClick;
	@FindBy(xpath = "//select[@id = 'justification']")
	protected WebElement printOption;
	@FindBy(xpath = "//div[contains(text(),'Contract Printing')]")
	protected WebElement headerContractPrinting;
	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNumber;
	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	protected WebElement contractNumber;
	@FindBy(xpath = "//td[contains(.,'The Bonus Contract Number is')]")
	protected WebElement bonusContractNumber;
	@FindBy(xpath = "//input[contains(@id,'contractNumberId')]")
	protected WebElement contractSearchtxt;
	@FindBy(xpath = "//input[@value='Search']")
	protected WebElement contractSearchBtn;
	@FindBy(xpath="//input[@value='Print Contract Documents']")
	protected WebElement printContractBtn;

	/*
	 * Method: contractClickPrintButton Description:Click Contract Print Button
	 * Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void contractClickPrintButton() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, printClick, "ExplicitLongWait");
		if (verifyObjectDisplayed(printClick)) {
			clickElementJSWithWait(printClick);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(printOption)) {
				selectByIndex(printOption, 2);
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Navigated to Print PDF Page and clicked on Print Button");
				clickElementJSWithWait(printClick);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Failed to click Print PDF button");
		}
	}

	/*
	 * Method: contractProcessingContractPrinting Description:Contract
	 * Processing and printing Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void contractProcessingContractPrinting() {
		waitUntilElementVisibleIE(driver, headerContractPrinting, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(headerContractPrinting));
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Contract Printing Screen present");
		waitUntilElementVisibleIE(driver, memberNumber, "ExplicitLongWait");
		Assert.assertTrue(verifyObjectDisplayed(contractNumber), "Contract Number not Displayed");
		strContractNumber = (contractNumber).getText().trim();
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Contract Number is: " + strContractNumber);
		Assert.assertTrue(verifyObjectDisplayed(memberNumber), "Member Number not displayed");
		strMemberNumber = (memberNumber).getText().trim();
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, strMemberNumber);
	}
	
	public void contractSearchandPrint(){
		waitUntilElementVisibleIE(driver, contractSearchtxt, "ExplicitLongWait");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Navigate to Print contract Page");
		fieldDataEnter(contractSearchtxt, SalepointContractPrintPage.strContractNumber);
		clickElementJS(contractSearchBtn);
		waitUntilElementVisibleIE(driver, printContractBtn, "ExplicitLongWait");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Navigate to contract summary page");
		clickElementJS(printContractBtn);
		contractClickPrintButton();
	}

}
