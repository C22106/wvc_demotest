package salepoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWPurchaseInformationPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWPurchaseInformationPage.class);

	public WBWPurchaseInformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Purchase Information')]")
	protected WebElement purchaseInformationHeader;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Purchase')]")
	protected WebElement purchaseInformationHeaderDiv;

	@FindBy(name = "paymentType")
	protected List<WebElement> paymentSelect;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//select[@name='explID']")
	protected WebElement selectPackage;

	@FindBy(id = "procFeeTd")
	protected WebElement requiredProcessingFee;

	@FindBy(name = "noPacRate")
	protected WebElement pacInterest;

	@FindBy(name = "procFeeCollected")
	protected WebElement enterProcessingFee;

	@FindBy(xpath = "//input[@name='paymentType' and @CHECKED]")
	protected WebElement selectedPaymentType;

	@FindBy(xpath = "//input[@name='familyCombine' and @value='Yes']")
	protected WebElement familyCombineYes;

	/*
	 * Method: purchaseInformationNavigation Description:Navigate to Purchase
	 * Info Page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void purchaseInformationNavigation() throws Exception {
		waitUntilElementVisible(driver, purchaseInformationHeader, "ExplicitMedWait");
		Assert.assertTrue(
				verifyElementDisplayed(purchaseInformationHeader)
						|| verifyElementDisplayed(purchaseInformationHeaderDiv),
				"Purchase Information page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Navigaetd To Purchase Information Page");

	}

	/*
	 * Method: selectPaymentType Description:Select payment Type Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectPaymentType() {
		if (testData.get("PaymentType").toLowerCase().contains("financed")) {
			paymentSelect.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSelect.get(0).click();
		} else {
			paymentSelect.get(1).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			paymentSelect.get(1).click();
			updateProcessingFee();
		}

	}

	/*
	 * Method: checkSelectedPaymentType Check payment Type Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public String checkSelectedPaymentType() {
		if (selectedPaymentType.getAttribute("value").contains("finance")) {
			return "finance";
		} else {
			return "cash";
		}

	}

	/*
	 * Method: selectFamilyCombine Description:Select Family Combine Type Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void selectFamilyCombine() {
		if (verifyObjectDisplayed(familyCombineYes)) {
			familyCombineYes.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				updateNoPacRate();

			} catch (Exception e) {
				System.out.println("No Alert Present after Login");
			}

		}

	}

	/*
	 * Method: updateNoPacRate Description:Update No Pac rate Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void updateNoPacRate() {
		try {
			if (verifyObjectDisplayed(pacInterest)) {
				pacInterest.clear();
				pacInterest.sendKeys("1.52");
			}
		} catch (Exception e) {
			log.info("Couldn't Update pac rate ");
		}
	}

	/*
	 * Method: updateProcessingFee Description:Update Fee Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void updateProcessingFee() {

		try {
			String getFee = getElementText(requiredProcessingFee);
			if (verifyObjectDisplayed(enterProcessingFee)) {
				enterProcessingFee.clear();
				enterProcessingFee.sendKeys(getFee.substring(1).trim());
			}
		} catch (Exception e) {
			log.info("Couldn't Update Processing Fee");
		}
	}

	/*
	 * Method: selectPointsPackage Description:Select Points Package Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectPointsPackage() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectPackage, testData.get("strPoints"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextButton);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();

		} catch (Exception e) {
			log.info("no Alert is displayed");
		}

	}
}