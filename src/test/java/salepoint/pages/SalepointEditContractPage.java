package salepoint.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointEditContractPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointEditContractPage.class);

	public SalepointEditContractPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[contains(@name,'contract_number')]")
	protected WebElement txtContractNumber;
	@FindBy(xpath ="//input[@name='search' and @class='button']")
	protected WebElement btnSearch;	
	@FindBy(xpath ="//div[contains(text(),'Edit Contract')]")
	protected WebElement txtEditContractHeader;
	/*
	 * Method: Update_DefaultLocation Description:Update Deafult Location Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void editContract() {
		waitUntilElementVisibleIE(driver, txtContractNumber, "ExplicitMedWait");
		fieldDataEnter(txtContractNumber, SalepointContractPrintPage.strContractNumber);
		clickElementJS(btnSearch);		
		waitUntilElementVisibleIE(driver, txtEditContractHeader, "ExplicitMedWait");
		if (verifyElementDisplayed(txtEditContractHeader)){
			tcConfig.updateTestReporter(getClassName(), getMethodName(),
					Status.PASS, "User is able to edit a contract");
		}else{
			tcConfig.updateTestReporter(getClassName(), getMethodName(),
					Status.FAIL, "User is not able to edit a "+testData.get("SalesType")+" contract");
		}		
	}
}
