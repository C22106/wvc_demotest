package salepoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WVRAPSplitPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WVRAPSplitPage.class);

	public WVRAPSplitPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Split Information')]")
	protected WebElement splitInformation;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(name = "member_number")
	protected WebElement memberNumber;

	@FindBy(xpath = "//td[@class='warning_text']")
	protected List<WebElement> totalContracts;

	@FindBy(name = "AddSplit")
	protected WebElement addSplit;

	@FindBy(name = "process")
	protected WebElement processCombine;

	/*
	 * Method: splitInformationNavigation Description:Split Information Page Nav
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void splitInformationNavigation() throws Exception {
		waitUntilElementVisible(driver, splitInformation, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(splitInformation), "Split Information page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Navigated To Split Information Page");

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextButton);

	}

	/*
	 * Method: checkTotalContracts Description:Check Total Contracts Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void checkTotalContracts(int totalContractList) {
		waitUntilElementVisibleIE(driver, totalContracts.get(0), "ExplicitLongWait");
		Assert.assertTrue(totalContracts.size() == totalContractList,
				totalContractList + " contract(s) displayed incorrectly");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				totalContractList + " contract(s) displayed correctly");

	}

	/*
	 * Method: enterMemberNumber Description:enter Member Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */

	public void enterMemberNumber() throws Exception {

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, memberNumber, "ExplicitLongWait");
		if (verifyElementDisplayed(memberNumber)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Member Number Field Navigation Successful");
			memberNumber.click();
			memberNumber.sendKeys(testData.get("strMemberNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member Number Field Not Present ");
		}
	}

	/*
	 * Method: clickAddSplit Description:Click Add Split Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickAddSplit() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		Assert.assertTrue(verifyObjectDisplayed(addSplit), "Add Split Button Not Displayed");
		clickElementJS(addSplit);

	}

}