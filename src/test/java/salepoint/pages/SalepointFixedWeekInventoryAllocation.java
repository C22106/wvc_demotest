package salepoint.pages;
/*package com.wvo.UIScripts.SalePoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.wvo.automation.core.FunctionalComponents;
import com.wvo.automation.core.TestConfig;


public class SalepointFixedWeekInventoryAllocation extends FunctionalComponents {

	public static final Logger log = Logger.getLogger(SalepointFixedWeekInventoryAllocation.class);

	public SalepointFixedWeekInventoryAllocation(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	By sellingSiteDropDowninInventoryAllocationScreen = By.xpath("//select[@name='siteNo']");
	@FindBy(xpath = "//input[@name='viewSheetsBtn']")
	WebElement viewSheetsButtoninInventoryAllocationScreen; 
	
	@FindBy(xpath = "//input[contains (@name,'viewAll')]")
	WebElement viewAllCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'udiInv')]")
	WebElement udiCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'fwInv')]")
	WebElement fworFloatCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'inHouse')]")
	WebElement inHouseCheckBoxButton;
	

	@FindBy(xpath = "//input[contains (@name,'frontLine')]")
	WebElement frontLineCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'exitLine')]")
	WebElement exitLineCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'biennialAnnual')]")
	WebElement biennialAnnualCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'biennialEven')]")
	WebElement biennialEvenCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'biennialOdd')]")
	WebElement biennialOddCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'vpExceptionYes')]")
	WebElement vpExceptionYesCheckBoxButton;
	
	@FindBy(xpath = "//input[contains (@name,'vpExceptionNo')]")
	WebElement vpExceptionNoCheckBoxButton;
	
	
	@FindBy(xpath = "//input[@name='done']")
	WebElement doneButtoninInventoryAllocationSheetScreen;
	
	@FindBy (xpath = "//a[contains (@name,'viewSheet')]")
	WebElement viewAllSheetsHyperlinkinInventoryAllocationScreen;
	
	
	@FindBy(xpath = "//input[@name='newTier3']")
	WebElement newTier3Button;

	
	By propertySiteDropDowninTier3SetupScreen = By.xpath("//select[@name='propertySiteSB']");
	
	
	@FindBy(xpath = "//input[@name='inHouseCB']")
	WebElement inHouseSalesLineCheckBox;
	
	
	@FindBy(xpath = "//input[@name='frontLineCB']")
	WebElement frontLineSalesLineCheckBox;
	
	@FindBy(xpath = "//input[@name='exitLineCB']")
	WebElement exitLineSalesLineCheckBox;
	
	By inventoryDropDowninTier3SetupScreen = By.xpath("//select[@name='inventorySB']");
	
	By netPricingDropDowninTier3SetupScreen = By.xpath("//select[@name='pricingSchemeSB']");
	
	@FindBy(xpath = "//input[@name='next' and @value='Add']")
	WebElement addButton;
	
	
	@FindBy(xpath = "//input[@name='tier3Selection']")  //and @value='19021864']")
	WebElement tier3PackageRadioButtton;
	
	@FindBy(xpath = "//input[@name='enrollmentFee']")  
	WebElement enrollmentFee;
	
	@FindBy(xpath = "//input[@name='processingFee']")  
	WebElement processingFee;
	
	@FindBy(xpath = "//input[@value='Next']")
	WebElement nextVal;
	
	
*//**
 * This method is to set up the inventory for Fixed Week Contract Creation
 * 
 * @author C40122
 * @param MenuURL
 * @throws Exception
 *//*


public void setupInventoryforFixedWeekContractCreation() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	driver.navigate().to(testData.get("MenuURL_InventoryAllocation"));
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	selectSellingSiteinInventoryAllocationScreen();
	clickonViewSheetsButton();
	clickonAlltheCheckBox();
	clickonViewAllSheetsHyperlinkinInventoryAllocationScreen();
	clickonNewTier3Button();
	selectPropertySiteinTier3SetupScreen();
	selectInventoryTypeinTier3SetupScreen();
	selectAllSalesLinesinTier3SetupScreen();
	selectInventoryinTier3SetupScreen();
	selectNetPricinginTier3SetupScreen();
	clickonAddButton();
	
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to select the selling site in the Inventory allocation Screen
 * 
 * @author C40122
 * @param SellingSite
 * @throws Exception
 *//*


public void selectSellingSiteinInventoryAllocationScreen() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	new Select(driver.findElement(sellingSiteDropDowninInventoryAllocationScreen)).selectByVisibleText(testData.get("SellingSite"));
	
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	tcConfig.updateTestReporter("SalePointInventorySetUpPage", "selectSellingSiteinInventoryAllocationScreen", Status.PASS,
			"Selling Site Selected in Inventory Allocation Screen");

}

*//**
 * This method is to click on the View Sheets Button presrent in the Inventory Set Up Screen
 * 
 * @author C40122
 * @throws Exception
 *//*


public void clickonViewSheetsButton() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	viewSheetsButtoninInventoryAllocationScreen.click();
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to click on all the check box present in the Inventory Set Up Screen
 * 
 * @author C40122
 * @throws Exception
 *//*


public void clickonAlltheCheckBox() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	viewAllCheckBoxButton.click();
	udiCheckBoxButton.click();
	fworFloatCheckBoxButton.click();
	inHouseCheckBoxButton.click();
	frontLineCheckBoxButton.click();
	exitLineCheckBoxButton.click();
	biennialAnnualCheckBoxButton.click();
	biennialEvenCheckBoxButton.click();
	biennialOddCheckBoxButton.click();
	vpExceptionYesCheckBoxButton.click();
	vpExceptionNoCheckBoxButton.click();

	waitForSometime(tcConfig.getConfig().get("LowWait"));
	tcConfig.updateTestReporter("SalePointInventorySetUpPage", "clickonAlltheCheckBox", Status.PASS,
			"All the check boxes are selected in Inventory Allocation Screen");

}


*//**
 * This method is to click on View all Sheets Hyper Link in the inventory allocation screen
 * 
 * @author C40122
 * @throws Exception
 *//*

public void clickonViewAllSheetsHyperlinkinInventoryAllocationScreen() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	viewAllSheetsHyperlinkinInventoryAllocationScreen.click();
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to click on the new Tier 3 Button
 * 
 * @author C40122
 * @throws Exception
 *//*


public void clickonNewTier3Button() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	newTier3Button.click();
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to select the Property Site in the Tier 3 Set up Screen
 * 
 * @author C40122
 * @param PropertySite
 * @throws Exception
 *//*


public void selectPropertySiteinTier3SetupScreen() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	new Select(driver.findElement(propertySiteDropDowninTier3SetupScreen)).selectByVisibleText(testData.get("PropertySite"));
	
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to select the inventory Type in the Tier 3 Set Up Screen
 * 
 * @author C40122
 * @param InventoryType
 * @throws Exception
 *//*


public void selectInventoryTypeinTier3SetupScreen() throws Exception {

	waitForSometime(tcConfig.getConfig().get("LowWait"));

		String InventoryType = testData.get("InventoryType");
		WebElement Inventory = driver.findElement(By.xpath("//input[@name='invSubTypeRB' and @value='FWK']"));
		
		clickElementJSWithWait(Inventory);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		
}

*//**
 * This method is to select all the Sales Line Options present in the Tier 3 Set Up Screen
 * 
 * @author C40122
 * @throws Exception
 *//*


public void selectAllSalesLinesinTier3SetupScreen() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	inHouseSalesLineCheckBox.click();
	frontLineSalesLineCheckBox.click();;
    exitLineSalesLineCheckBox.click();		
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to select the Inventory in the Tier 3 Set Up Screen
 * 
 * @author C40122
 * @throws Exception
 *//*


public void selectInventoryinTier3SetupScreen() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	new Select(driver.findElement(inventoryDropDowninTier3SetupScreen)).selectByIndex(1);
	//new Select(driver.findElement(inventoryDropDowninTier3SetupScreen)).selectByVisibleText(testData.get("Inventory"));	
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to select the net pricing amount in the Tier 3 Set Up screen in SalePoint
 * 
 * @author C40122
 * @param NetPricing
 * @throws Exception
 *//*

public void selectNetPricinginTier3SetupScreen() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	new Select(driver.findElement(netPricingDropDowninTier3SetupScreen)).selectByVisibleText(testData.get("NetPricing"));	
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}

*//**
 * This method is to click on the add button present in the SalePoint Inventory Set up Page
 * 
 * @author C40122
 * @throws Exception
 *//*



public void clickonAddButton() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	
	if (verifyElementDisplayed(addButton)) {
		
		tcConfig.updateTestReporter("SalePointInventorySetUpPage", "clickonAddButton", Status.PASS,
				"All the vales are selected and add inventory button is available in Inventory Allocation Screen");

		addButton.click();

	}
	
	waitForSometime(tcConfig.getConfig().get("LowWait"));

}


//Prerana

*//**
 * This method is to select Tier 3 Package during the inventory setup process for Fixed week
 * 
 * @author C40122 
 * @throws Exception
 *//*

public void selectTier3Package() throws Exception {
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	clickElementJSWithWait(tier3PackageRadioButtton);
	waitForSometime(tcConfig.getConfig().get("LowWait"));
	

}

public void clicknextButton(){
	clickElementJS(nextVal);
	waitForSometime(tcConfig.getConfig().get("LowWait"));
}
}
*/