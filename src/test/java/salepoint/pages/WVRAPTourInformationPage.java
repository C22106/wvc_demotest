package salepoint.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WVRAPTourInformationPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WVRAPTourInformationPage.class);

	public WVRAPTourInformationPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(name = "tourID")
	protected WebElement tourID;
	
	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;
	
	@FindBy(name = "member_number")
	protected WebElement memberNumber;
	
	
	/*
	 * Method: navigateToMenuURL Description:Navigate To Menu URL Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void navigateToMenuURL() {
		// Enter the MENu URL to Navigate to Tour Search page
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	
	/*
	 * Method: enterTourID Description:enter Tour ID Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */

	public void enterTourID() throws Exception {

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, tourID, "ExplicitLongWait");
		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// select wave time
			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"New Contract Page Navigation Error");
		}
	}

	/*
	 * Method: acceptOrDeclineAutomationFlow Description:Accept Or Decline Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */

	public void acceptOrDeclineAutomationFlow(Boolean automation) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.alertIsPresent());

			if (ExpectedConditions.alertIsPresent() != null) {

				if (automation == true) {
					driver.switchTo().alert().accept();
				} else {
					driver.switchTo().alert().dismiss();
				}
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Automation Pop Up message displayed");
			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
						"Automation Pop Up is not available");
			}

		} catch (Exception e) {
			log.info("no Alert is displayed");
		}
	}
	
	/*
	 * Method: enterMemberNumber Description:enter Member Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */

	public void enterMemberNumber() throws Exception {

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, memberNumber, "ExplicitLongWait");
		if (verifyElementDisplayed(memberNumber)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Member Number Field Navigation Successful");
			memberNumber.click();
			memberNumber.sendKeys(testData.get("strMemberNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member Number Field Not Present ");
		}
	}
	
}