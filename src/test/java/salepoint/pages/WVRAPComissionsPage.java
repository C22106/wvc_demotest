package salepoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WVRAPComissionsPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WVRAPComissionsPage.class);

	public WVRAPComissionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Commissions')]")
	protected WebElement comissionsHeader;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(name = "efName")
	protected WebElement frontLineRep;
	
	@FindBy(name = "emName")
	protected WebElement frontLineManager;
	
	@FindBy(name = "erName")
	protected WebElement exitRep;
	
	@FindBy(name = "salesman")
	protected WebElement salesPerson;
	
	@FindBy(name = "closer")
	protected WebElement closerPerson;
	
	@FindBy(name = "SPresenter")
	protected WebElement specialistPerson;
	
	@FindBy(name = "umName")
	protected WebElement projectDirector;


	/*
	 * Method: comissionNavigation Description:Comission Page Nav Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void comissionNavigation() throws Exception {
		waitUntilElementVisible(driver, comissionsHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(comissionsHeader), "Comissions page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Comissions Page");

	}

	/*
	 * Method: enterProjectDirector Description:Enter Project Director Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterProjectDirector() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		projectDirector.clear();
		projectDirector.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	
	/*
	 * Method: enterFrontRep Description:Enter Sales Person Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterFrontRep() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		frontLineRep.clear();
		frontLineRep.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	
	/*
	 * Method: enterFrontManager Description:Enter Sales Person Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterFrontManager() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		frontLineManager.clear();
		frontLineManager.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	
	/*
	 * Method: enterExitRep Description:Enter Sales Person Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterExitRep() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		exitRep.clear();
		exitRep.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextButton);

	}
	
	/*
	 * Method: enterSalePerson Description:Enter Sales Person Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterSalePerson() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		salesPerson.clear();
		salesPerson.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	
	/*
	 * Method: enterSpecialist Description:Enter Sales Person Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterSpecialist() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		specialistPerson.clear();
		specialistPerson.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
	
	/*
	 * Method: enterCloser Description:Enter Sales Person Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterCloser() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		closerPerson.clear();
		closerPerson.sendKeys(testData.get("strSalePer"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}
}