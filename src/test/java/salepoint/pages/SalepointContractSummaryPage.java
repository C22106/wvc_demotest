package salepoint.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointContractSummaryPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointContractSummaryPage.class);

	public SalepointContractSummaryPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@value='Save and Print' and contains(@onclick,'Print')]")
	protected WebElement savecontract1;
	@FindBy(id = "Print")
	protected WebElement printClick;
	@FindBy(id = "Print")
	protected WebElement print;
	@FindBy(xpath = "//select[@name='justification']")
	protected WebElement docSign;
	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNoCheck;
	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	protected WebElement conNo;
	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNo;
	public static String strContractNo = "";
	String strMemberNo = "";
	@FindBy(xpath = "//div[contains(text(),'Summary']")
	protected WebElement pageSummary;
	@FindBy(xpath = "//input[@name='bonuspoints']")
	protected WebElement bonusPoints;
	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;
	@FindBy(xpath = "//tr[@text()='FAIRSHARE PLUS POINTS']//following-sibling::tr")
	protected WebElement fairsharePoints;
	@FindBy(xpath = "//tr[@text()='Tier:']//following-sibling::tr")
	protected WebElement tier;
	@FindBy(xpath = "//td[@text()='Go! Points']//parent::tr//following-sibling::tr[0]")
	protected WebElement goPoints;
	@FindBy(xpath = "//input[@value='Save and Print']")
	protected WebElement saveClick;
	@FindBy(xpath = "//input[@value='Save and Print']")
	protected WebElement savecontract;

	/*
	 * Method: validateDetails Description:Validate Details Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void validateDetails() throws Exception {
		waitUntilElementVisibleIE(driver, pageSummary, "ExplicitLongWait");
		if (verifyElementDisplayed(pageSummary)) {
			String strFairSharePoints = testData.get("strPoints");
			String getFairSharePoints = fairsharePoints.getText();
			if (strFairSharePoints == getFairSharePoints) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Correct points displayed in the FairShare field :" + fairsharePoints.getText());
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Incorrect points displayed in the FairShare field :" + fairsharePoints.getText());
			}
		}
	}

	/*
	 * Method: clicksave Description:click Save Date: 2020 Author:Abhijeet Roy
	 * Changes By: NA
	 */

	public void clicksave() throws Exception {
		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLongWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

	}

	/*
	 * Method: sp_Contract_WVR_Save Description:Save WVR Contract Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_Contract_WVR_Save() throws Exception {

		if (verifyElementDisplayed(savecontract1)) {
			// validate_SummaryScreen();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, "ExplicitLongWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		try {
			waitUntilElementVisibleIE(driver, docSign, "ExplicitLowWait");
			if (verifyElementDisplayed(docSign)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				new Select((docSign)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		} catch (Exception e) {
			System.out.println(" No Docu Sign option");

		}

		// Click on Print
		waitUntilElementVisibleIE(driver, print, "ExplicitLongWait");
		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLongWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: sp_Contract_WVR_Discovery_Save Description:Save Discovery
	 * Contract Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_Contract_WVR_Discovery_Save() throws Exception {

		if (verifyElementDisplayed(savecontract1)) {
			// validate_SummaryScreen();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontract1);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				System.out.println("");
			}
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisibleIE(driver, printClick, "ExplicitLowWait");

		if (verifyElementDisplayed(print)) {
			clickElementJSWithWait(print);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}

		try {
			waitUntilElementVisibleIE(driver, docSign, "ExplicitLowWait");
			if (verifyElementDisplayed(docSign)) {
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				new Select((docSign)).selectByIndex(1);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementJSWithWait(print);
			}
		} catch (Exception e) {
			System.out.println(" No Docu Sign option");

		}

		waitUntilElementVisibleIE(driver, memberNoCheck, "ExplicitLowWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText().trim();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			System.out.println("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: validate_SummaryScreen Description:Validate Summary Screen Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */
	public void validate_SummaryScreen() {
		String strOwnerName = SalePointOwnerPage.strOwnerName;
		String customerNumber = testData.get("strTourId");
		String strPaymentTotal = SalePointPaymentScreen.strTotalPaymentAmount;

		By txtOwnerName = By.xpath("//td[contains(text()," + strOwnerName + "]");
		By txtCustNumber = By.xpath("//td[contains(text()," + customerNumber + ")]");
		By txtPaymentValue = By.xpath("//td[contains(text()," + strPaymentTotal + "]");

		List<WebElement> ownerList = driver.findElements(txtOwnerName);
		int ownerNameCount = ownerList.size();

		List<WebElement> PaymentTotal = driver.findElements(txtPaymentValue);
		int paymentTotalCount = PaymentTotal.size();

		if (ownerNameCount >= 1 && verifyObjectDisplayed(txtCustNumber) && paymentTotalCount >= 1) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"All details are displayed in Contract Summary Page :  " + " Owner Name - " + strOwnerName
							+ " Customer Number - " + customerNumber + " Payment value - " + strPaymentTotal);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"All details are NOT displayed in Contract Summary Page :  " + " Owner Name - " + strOwnerName
							+ " Customer Number - " + customerNumber + " Payment value - " + strPaymentTotal);

		}

	}

}
