package salepoint.pages;

import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWContractManagementPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWContractManagementPage.class);

	public WBWContractManagementPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Contract Management')]")
	protected WebElement contractMangementHeader;

	@FindBy(xpath = "//select[@name='beginDateDay']")
	protected WebElement selectBeginDateDay;

	@FindBy(xpath = "//select[@name='beginDateMonth']")
	protected WebElement selectBeginDateMonth;

	@FindBy(xpath = "//select[@name='beginDateYear']")
	protected WebElement selectBeginDateYear;

	@FindBy(name = "next")
	protected WebElement nextButton;

	@FindBy(xpath = "//tr[contains(@class,'Row')]/td[1]")
	protected List<WebElement> allMemberNumber;

	@FindBy(xpath = "//tr[contains(@class,'Row')]/td[2]")
	protected List<WebElement> allContractNumber;

	@FindBy(xpath = "//tr[contains(@class,'Row')]/td[3]")
	protected List<WebElement> allMemberName;

	@FindBy(xpath = "//tr[contains(@class,'Row')]/td[4]")
	protected List<WebElement> allContractSalesType;

	@FindBy(xpath = "//tr[contains(@class,'Row')]/td[5]")
	protected List<WebElement> allContractStatus;

	@FindBy(xpath = "//tr[contains(@class,'Row')]/td[7]")
	protected List<WebElement> allContractDate;


	/*
	 * Method: contractManagementPageNavigation Description:Contract management
	 * Page Nav Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void contractManagementPageNavigation() throws Exception {
		driver.navigate().to(testData.get("contractMgmt"));
		waitUntilElementVisible(driver, contractMangementHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(contractMangementHeader), "Contract Management page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Navigated To Contract Management Page");
	}

	/*
	 * Method: selectBeginDate Description:Select Begin date Page Nav Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectBeginDate(int todaysDateMinusDays) {
		DateTime dateTime = new DateTime();
		DateTime getYesterdaysDate = (dateTime.minusDays(todaysDateMinusDays));
		Date convert = getYesterdaysDate.toDate();
		DateFormat formatDate = new SimpleDateFormat("dd-MMM-yyyy");
		String getDate = formatDate.format(convert);
		String getDay = getDate.split("-")[0];
		String getMonth = getDate.split("-")[1];
		String getYear = getDate.split("-")[2];
		if (getDay.startsWith("0")) {
			getDay = getDay.replace("0", "");
		}
		selectByText(selectBeginDateDay, getDay);
		selectByText(selectBeginDateMonth, getMonth);
		selectByText(selectBeginDateYear, getYear);

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}

	/*
	 * Method: selectContract Description:Select Contract Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectContract() {
		String strMemberNo = testData.get("strMemberNumber");
		waitUntilElementVisibleIE(driver,
				By.xpath("//tr/td/a[contains(@href,'Process')and contains(.,'" + strMemberNo + "')]"),
				"ExplicitLongWait");
		if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Member No Verified and is: " + strMemberNo);
			WebElement selectMember = driver
					.findElement(By.xpath("//tr/td/a[contains(@href,'Process')and contains(.,'" + strMemberNo + "')]"));
			clickElementJS(selectMember);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Member No Not Verified");

		}

	}

	/*
	 * Method: verifyTotalMemberNumbers Description:Verify Member Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public Integer verifyTotalMemberNumbers() {
		List<String> getCount = new ArrayList<String>();
		for (int totalMembers = 0; totalMembers < allMemberNumber.size(); totalMembers++) {
			if (!allMemberNumber.get(totalMembers).getText().isEmpty()) {
				getCount.add(allMemberNumber.get(totalMembers).getText());
			} else {
				Assert.assertTrue(false, "Member Number is Empty in Some Rows");
			}
		}
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Total " + getCount.size() + " members present in the list");
		return getCount.size();

	}

	/*
	 * Method: verifyAllContractDetails Description:Verify Contract Details
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void verifyAllContractDetails() {
		waitUntilElementVisibleIE(driver, allMemberNumber.get(0), "ExplicitLongWait");
		int totalMembers = verifyTotalMemberNumbers();
		verifyTotalContractNumbers(totalMembers);
		verifyTotalMemberName(totalMembers);
		verifyTotalContractType(totalMembers);
		verifyTotalContractStatus(totalMembers);
		verifyTotalContractCreation(totalMembers);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "All Details Verified Sucessfully");

	}

	/*
	 * Method: verifyTotalContractNumbers Description:Verify Contracts Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void verifyTotalContractNumbers(int totalMember) {
		List<String> getCount = new ArrayList<String>();
		for (int totalContracts = 0; totalContracts < totalMember; totalContracts++) {
			if (!allContractNumber.get(totalContracts).getText().isEmpty()) {
				getCount.add(allContractNumber.get(totalContracts).getText());
			} else {
				Assert.assertTrue(false, "Contract Number is Empty in Some Rows");
			}
		}
		Assert.assertTrue(getCount.size() == totalMember, "Contract Number not present in all rows");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Total " + getCount.size() + " Contracts present in the list");
	}

	/*
	 * Method: verifyTotalMemberName Description:Verify Member Name Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void verifyTotalMemberName(int totalMember) {
		List<String> getCount = new ArrayList<String>();
		for (int totalContracts = 0; totalContracts < totalMember; totalContracts++) {
			if (!allMemberName.get(totalContracts).getText().isEmpty()) {
				getCount.add(allMemberName.get(totalContracts).getText());
			} else {
				Assert.assertTrue(false, "Member Name is Empty in Some Rows");
			}
		}
		Assert.assertTrue(getCount.size() == totalMember, "Memeber name not present in all rows");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Total " + getCount.size() + " Name present in the list");
	}

	/*
	 * Method: verifyTotalContractType Description:Verify Contracts Type Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void verifyTotalContractType(int totalMember) {
		List<String> getCount = new ArrayList<String>();
		for (int totalContracts = 0; totalContracts < totalMember; totalContracts++) {
			if (!allContractSalesType.get(totalContracts).getText().isEmpty()) {
				getCount.add(allContractSalesType.get(totalContracts).getText());
			} else {
				Assert.assertTrue(false, "Contract Sales Type is Empty in Some Rows");
			}
		}
		Assert.assertTrue(getCount.size() == totalMember, "Contract Sales Type not present in all rows");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Total " + getCount.size() + " Contract Type present in the list");
	}

	/*
	 * Method: verifyTotalContractStatus Description:Verify Contracts Status
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void verifyTotalContractStatus(int totalMember) {
		List<String> getCount = new ArrayList<String>();
		for (int totalContracts = 0; totalContracts < totalMember; totalContracts++) {
			if (!allContractStatus.get(totalContracts).getText().isEmpty()) {
				getCount.add(allContractStatus.get(totalContracts).getText());
			} else {
				Assert.assertTrue(false, "Contract Status is Empty in Some Rows");
			}
		}
		Assert.assertTrue(getCount.size() == totalMember, "Contract Status not present in all rows");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Total " + getCount.size() + " Contract Staus present in the list");
	}

	/*
	 * Method: verifyTotalContractCreation Description:Verify Contracts Creation
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void verifyTotalContractCreation(int totalMember) {
		List<String> getCount = new ArrayList<String>();
		for (int totalContracts = 0; totalContracts < totalMember; totalContracts++) {
			if (!allContractDate.get(totalContracts).getText().isEmpty()) {
				getCount.add(allContractDate.get(totalContracts).getText());
			} else {
				Assert.assertTrue(false, "Contract Date is Empty in Some Rows");
			}
		}
		Assert.assertTrue(getCount.size() == totalMember, "Contract Date not present in all rows");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Total " + getCount.size() + " Contract Date present in the list");
	}

}
