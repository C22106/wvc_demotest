package salepoint.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWOwnersListPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWOwnersListPage.class);
	WBWOwnerInformationPage ownerInfo = new WBWOwnerInformationPage(tcConfig);

	public WBWOwnersListPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Owner Information')]")
	protected WebElement ownerInformationHeader;

	@FindBy(xpath = "(//tr[@class='page_title']//div[contains(.,'Owner')] | //tr[@class='page_title' and contains(.,'Owner Information')])")
	protected WebElement ownerDetailsPage;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected List<WebElement> deleteSecondaryOwner;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//parent::tr//following-sibling::tr[@class='evenRow']//a[contains(.,'Edit')]")
	protected WebElement editPrimaryOwner;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement secondaryOwners;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[1]")
	protected WebElement deleteFirstOwner;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	protected WebElement primaryOwner;

	@FindBy(name = "next")
	protected WebElement nextButton;

	@FindBy(xpath = "//button[@name='Next']")
	protected WebElement nextButtonValue;

	@FindBy(xpath = "//a[contains(.,'Make Primary')]")
	protected WebElement makeOwnerPrimary;

	@FindBy(xpath = "//input[@type='checkbox']//parent::td//preceding-sibling::td[contains(.,'Make Primary')]")
	protected List<WebElement> loanEnableOwner;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//parent::tr//following-sibling::tr[@class='evenRow']//td[2]")
	protected WebElement primaryOwnerBand;

	/*
	 * Method: ownerInfoPageValidation Description:Validate navigation to Owner
	 * Information page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void ownerInformationPageNavigation() {
		waitUntilElementVisible(driver, ownerDetailsPage, "ExplicitMedWait");
		if (verifyObjectDisplayed(ownerInformationHeader)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Navigated to Primary Owner Information Page", true);
			ownerInfo.enterOwnerInformation();
			waitUntilElementVisible(driver, ownerDetailsPage, "ExplicitMedWait");
			actionOnSecondaryOwner(false);
		} else if (verifyObjectDisplayed(ownerDetailsPage)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigated to Owner Details Page",
					true);
			actionOnSecondaryOwner(true);
			waitUntilElementVisible(driver, ownerDetailsPage, "ExplicitMedWait");
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Owner Information Page navigation Failed", true);
		}
		clickNextButton();
	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitUntilElementVisible(driver, ownerDetailsPage, "ExplicitMedWait");
		if (verifyObjectDisplayed(nextButton)) {
			clickElementJS(nextButton);
		} else if (verifyObjectDisplayed(nextButtonValue)) {
			clickElementJSWithWait(nextButtonValue);

		}

	}

	/*
	 * Method: actionOnSecondaryOwner Description:Take appropriate action on
	 * Secondary Owners page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void actionOnSecondaryOwner(Boolean editInformation) {
		int totalSecondaryOwner;
		if (verifyObjectDisplayed(secondaryOwners)) {
			totalSecondaryOwner = deleteSecondaryOwner.size();

			if (verifyElementDisplayed(primaryOwner)) {
				if (checkPrimaryOnwerBand() == true) {
					deleteSecondaryOwners(totalSecondaryOwner);
				} else {
					makeLoanEnabledOwnerPrimary();
					deleteSecondaryOwners(totalSecondaryOwner);
				}
			} else {
				if (financedBandMembersList().size() > 0) {
					makeLoanEnabledOwnerPrimary();
				} else {
					makeOneOwnerPirmary();
				}
				deleteSecondaryOwners(totalSecondaryOwner);
			}

		}
		if (editInformation == true) {
			editPrimaryOwnerInformation();
		}
	}

	/*
	 * Method: editPrimaryOwnerInformation Description:Edit Primary Owner
	 * Information page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void editPrimaryOwnerInformation() {
		if (verifyObjectDisplayed(editPrimaryOwner)) {
			editPrimaryOwner.click();
			ownerInfo.enterOwnerInformation();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Primary Owner Edit CTA not present");

		}
	}

	/*
	 * Method: deleteSecondaryOwners Description:Delete Secondary owners* Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void deleteSecondaryOwners(int howManySecondaryOwnersToDelete) {

		for (int i = 0; i < howManySecondaryOwnersToDelete; i++) {

			clickElementJSWithWait(deleteFirstOwner);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					log.info(e);
				}
			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		}
	}

	/*
	 * Method: makeOneOwnerPirmary Description:Make a Secondary Owner Primary *
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void makeOneOwnerPirmary() {
		if (verifyObjectDisplayed(makeOwnerPrimary)) {
			clickElementJSWithWait(makeOwnerPrimary);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		}
	}

	/*
	 * Method: editPrimaryOwnerandUpdate Description:Edit Primary Owner and
	 * Update Information page Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void editPrimaryOwnerandUpdate() {
		waitUntilElementVisibleIE(driver, editPrimaryOwner, "ExplicitLongWait");
		if (verifyObjectDisplayed(editPrimaryOwner)) {
			editPrimaryOwner.click();
			ownerInfo.updateEmailAddress();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Primary Owner Edit CTA not present");

		}
	}

	/*
	 * Method: financedBandMembersList Description:Making a Financed Band
	 * Secondary Owner Primary Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public List<WebElement> financedBandMembersList() {

		List<WebElement> elementList = new ArrayList<WebElement>();
		String bandName = "C";
		List<WebElement> tempList = null;
		String strTemp = null;
		for (int bands = 0; bands < 4; bands++) {
			//td[contains(.,'A')][2]
			switch (bands) {
			case 0:
				bandName = "A";
				strTemp = "//td[contains(.,'" + bandName
						+ "')][2]//following-sibling::td//a[contains(.,'Make Primary')]";
				tempList = driver.findElements(By.xpath(strTemp));
				if (tempList.size() > 0) {
					elementList.add(tempList.get(0));
				}
				break;
			case 1:
				bandName = "A1";
				strTemp = "//td[contains(.,'" + bandName
						+ "')][2]//following-sibling::td//a[contains(.,'Make Primary')]";
				tempList = driver.findElements(By.xpath(strTemp));
				if (tempList.size() > 0) {
					elementList.add(tempList.get(0));
				}
				break;
			case 2:
				bandName = "B";
				strTemp = "//td[contains(.,'" + bandName
						+ "')][2]//following-sibling::td//a[contains(.,'Make Primary')]";
				tempList = driver.findElements(By.xpath(strTemp));
				if (tempList.size() > 0) {
					elementList.add(tempList.get(0));
				}
				;
				break;
			case 3:
				bandName = "B1";
				strTemp = "//td[contains(.,'" + bandName
						+ "')][2]//following-sibling::td//a[contains(.,'Make Primary')]";
				tempList = driver.findElements(By.xpath(strTemp));
				if (tempList.size() > 0) {
					elementList.add(tempList.get(0));
				}
				break;
			default:
				log.info("No Band Member found");
			}

		}

		return elementList;

	}

	/*
	 * Method: checkPrimaryOnwerBand Description:Check Band of Primary Owner
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public Boolean checkPrimaryOnwerBand() {
		Boolean bandCheck = false;
		try {
			for (int bands = 0; bands < 4; bands++) {
				switch (bands) {
				case 0:
					if (primaryOwnerBand.getText().contains("A")) {
						bandCheck = true;
						break;
					}

				case 1:
					if (primaryOwnerBand.getText().contains("A1")) {
						bandCheck = true;
						break;
					}

				case 2:
					if (primaryOwnerBand.getText().contains("B")) {
						bandCheck = true;
						break;
					}

				case 3:
					if (primaryOwnerBand.getText().contains("B1")) {
						bandCheck = true;
						break;
					}

				default:
					bandCheck = false;
					break;
				}
			}

		} catch (Exception e) {
			log.info("No Band Found");
		}

		return bandCheck;

	}

	/*
	 * Method: makeLoanEnabledOwnerPrimary Description:Makeing a Loan Enabled
	 * Secondary Owner Primary Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void makeLoanEnabledOwnerPrimary() {
		List<WebElement> bandMemberList = financedBandMembersList();
		if (bandMemberList.size() > 0) {
			clickElementJSWithWait(bandMemberList.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			log.info("No Owner to be made Primary");

		}
	}

}