package salepoint.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointExperiencePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointExperiencePage.class);

	public SalePointExperiencePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='ok']")
	protected WebElement okBtn;

	@FindBy(name = "tourID")
	protected WebElement tourID;

	@FindBy(name = "tourTime")
	protected WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement next1;

	@FindBy(id = "wbwcommissions_salesMan")
	protected WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteSec;

	@FindBy(id = "saveContract")
	protected WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	protected WebElement generate;

	@FindBy(xpath = "//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]")
	protected WebElement conNo;

	protected String strContractNo = "";

	/*
	 * Method: sp_Ex_ContractFlow Description:Experience Contract Flow Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_Ex_ContractFlow() throws Exception {

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Experience Page Navigation Succesful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));

			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));

			next1.click();

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Experience Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyElementDisplayed(deleteSec)) {
			List<WebElement> list = driver.findElements(By.xpath("//a[contains(.,'Delete')]"));

			list.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.switchTo().alert().accept();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			System.out.println("No Secondary owner good to go");
		}

		next1.click();

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		List<WebElement> list1 = driver.findElements(By.name("paymentType"));

		/* WebElement element = driver.findElement(By.id("gbqfd")); */
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", list1.get(1));

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		// 20,000 Credits - $2,980.00
		/*
		 * List<WebElement> list2 = driver.findElements(By.name("explID"));
		 * 
		 * 
		 * list2.get(1).click();
		 */

		new Select(driver.findElement(By.name("explID"))).selectByVisibleText(testData.get("strPoints"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		next1.click();

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		next1.click();

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisible(driver, saleper, "ExplicitLowWait");

		saleper.click();
		saleper.sendKeys(testData.get("strSalePer"));

		next1.click();
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		waitUntilElementVisible(driver, savecontract, "ExplicitLowWait");

		if (verifyElementDisplayed(savecontract)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			savecontract.click();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}
		waitUntilElementVisible(driver, generate, "ExplicitLowWait");

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			generate.click();
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

	}

	@FindBy(xpath = "//input[@value='Experience']")
	protected WebElement experienceClick;

	/*
	 * Method: sp_Ex_contractSearch Description:Search ContractDate: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_Ex_contractSearch() throws Exception {

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyElementDisplayed(experienceClick)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Management Page Displayed");

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", experienceClick);

			next1.click();

			List<WebElement> list1 = driver.findElements(By.xpath("//div/table/tbody/tr"));

			WebElement wb = list1.get(list1.size() - 1);

			WebElement wbName = driver.findElement(By.xpath(wb + "/td/a"));

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Verified Member No as: " + wbName.getText());

			List<WebElement> list2 = driver.findElements(By.xpath(wb + "/td"));

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Contract No Verified As: "
					+ list2.get(1).getText() + " Owners Name as: " + list2.get(2).getText());

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					" Sales Type Verified As: " + list2.get(3).getText() + " Date Booked: " + list2.get(6).getText());

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

}
