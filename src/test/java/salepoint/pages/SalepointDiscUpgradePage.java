package salepoint.pages;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.crypto.SealedObject;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointDiscUpgradePage extends SalePointBasePage {

	public SalepointDiscUpgradePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
	}
	public static final Logger log = Logger.getLogger(SalepointDiscUpgradePage.class);

	
	@FindBy(xpath = "//input[@name = 'member_number']")
	protected WebElement txtMemberNumber;	
	@FindBy(xpath = "//input[@value = 'Search']")
	protected WebElement buttonSearch;	
	@FindBy(xpath = "//input[@value = 'Process Upgrade']")
	protected WebElement btnProcessUpgrade;

	
	public void enterMemberInfo(){
		waitUntilElementVisibleIE(driver, txtMemberNumber, "ExplicitLongWait");
		fieldDataEnter(txtMemberNumber, testData.get("DiscMemberNo"));
		clickElementJS(buttonSearch);		
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"User is able to search with the Member number");
	}

	
	public void processUpgrade(){
		waitUntilElementVisibleIE(driver, btnProcessUpgrade, "ExplicitLongWait");
		clickElementJS(btnProcessUpgrade);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"User is able to click Process Upgrade button");
		
	}
	
}