package salepoint.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointPaymentScreen extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointHomePage.class);
	SalePointOwnerEditPage spOwnerEdit = new SalePointOwnerEditPage(tcConfig);
	public static String strTotalPaymentAmount;

	public SalePointPaymentScreen(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@class='button' and @id='loan']")
	protected WebElement PaymentEdit;

	@FindBy(xpath = "//input[contains(@name,'cc_payment_member_name')]")
	protected WebElement ccName;

	@FindBy(xpath = "//input[contains(@name,'cc_payment_account_number') or contains(@name,'cc_account_number')]")
	protected WebElement ccNumber;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_type') or contains(@name,'cc_type')]")
	protected WebElement ccType;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_month') or contains(@name,'cc_exp_month')]")
	protected WebElement ccMonth;

	@FindBy(xpath = "//select[contains(@name,'cc_payment_exp_year') or contains(@name,'cc_exp_year')]")
	protected WebElement ccYear;

	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	protected WebElement cashSelect;

	@FindBy(xpath = "//td[contains(.,'CC Auto')]/input[@type='radio']")
	protected WebElement ccSelect;

	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achSelect;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculate;

	@FindBy(xpath = "//input[@value='Save and Print' and contains(@onclick,'Print')]")
	protected WebElement savecontract;

	@FindBy(xpath = "//table[contains(@class,'client_table')]//td[contains(@id,'totalId')]")
	protected WebElement txtTotalAmount;

	/*
	 * Method: sp_WVR_edit_Payment Description:Edit payment Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WVR_edit_Payment() throws Exception {

		if (verifyObjectDisplayed(PaymentEdit)) {

			PaymentEdit.click();
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (ExpectedConditions.alertIsPresent() != null) {
				try {
					driver.switchTo().alert().accept();
				} catch (Exception e) {
					log.info("no alert is displayed");
				}
			}

			waitUntilElementVisibleIE(driver, ccSelect, "ExplicitLowWait");

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// Enter Credit Card Info
			ccName.clear();
			ccName.click();
			ccName.sendKeys(testData.get("CCHolder"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			ccType.click();
			String cardType = testData.get("CCType");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select/option[contains(.,'" + cardType + "')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			ccNumber.clear();
			ccNumber.click();
			ccNumber.sendKeys(testData.get("CCNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			ccMonth.click();
			String expMnth = testData.get("CCMonth");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select/option[contains(.,'" + expMnth + "')]")).click();

			ccYear.click();
			String expYr = testData.get("CCYear");
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select/option[contains(.,'" + expYr + "')]")).click();

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			strTotalPaymentAmount = txtTotalAmount.getText();
			// Click on Next
			waitUntilElementVisibleIE(driver, nextVal, "ExplicitLongWait");
			clickElementJS(nextVal);

			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					log.info("no Alert is displayed");
				}

			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, calculate, "ExplicitLowWait");

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			// Calculate the Rate
			if (verifyElementDisplayed(calculate)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Money Screen present");

				clickElementJS(calculate);
				log.info("clicked calculate button");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				// Clcik on Next button
				waitUntilElementVisibleIE(driver, nextVal, "ExplicitLongWait");
				nextVal.click();
				// clickElementJS(nextVal);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				log.info("clicked Next button after calculate");
			} else {
				log.info("calculate element not diaplyed");
			}

			if (ExpectedConditions.alertIsPresent() != null) {
				try {

					driver.switchTo().alert().accept();

				} catch (Exception e) {
					log.info("no Alert is displayed");
				}

			}

		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		waitUntilElementVisibleIE(driver, savecontract, "ExplicitLongWait");
	}

}
