package salepoint.pages;



import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointUpgradeContract extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointUpgradeContract.class);

	public SalePointUpgradeContract(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	
	private By searchButton = By.xpath("//input[@name='search']");
	private By nextButton = By.xpath("//input[@name='next']");
	private By tscTxtBx = By.xpath("//input[@name='tsc']");
	
	private By contractUpgradePage = By.xpath("//tr[@class='page_title']//div[contains(.,'Upgrade-Purchase Information')]");
	private By OwnerInfoScreenPage = By.xpath("//tr[@class='page_title']//div[contains(.,'Owner Info Screen')]");
	private By errorUpgradePage = By.xpath("//div[@class='error_elem']");
	
	@FindBy(xpath = "//input[@name='memberNo']")
	WebElement memberNumber;
	
	
	public void memberSearchForNonPaidOffWAAM(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		if(verifyObjectDisplayed(contractUpgradePage)){
			String memberNo=testData.get("MemberNumber"); 
			
			tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForNonPaidOffWAAM", Status.PASS, "Upgrade-Purchase Information Page Displayed");
			
			memberNumber.sendKeys(memberNo);
				
				tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForNonPaidOffWAAM", Status.PASS, "Member No Entered as: "+memberNo);
			
				driver.findElement(searchButton).click();
				
				
				waitForSometime(tcConfig.getConfig().get("LongWait"));
			
				if(driver.findElement(errorUpgradePage).getText().contains("Selected WAAM contract is not eligible for trade since it is not paid off.")){
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForNonPaidOffWAAM", Status.PASS, "Hard stop message is displayed when trying to upgrade for WBW WAAM that owns a non-paid off WAAM");
				}else{
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForNonPaidOffWAAM", Status.FAIL, "Hard stop message is not displayed when trying to upgrade for WBW WAAM that owns a non-paid off WAAM");
				}
			
		}else{
			
			tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForNonPaidOffWAAM", Status.FAIL, "Error in Upgrade-Purchase Information Page");
		}	
	}
	
	public void memberSearchForPaidOffWAAM(){
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		
		if(verifyObjectDisplayed(contractUpgradePage)){
			String memberNo=testData.get("MemberNumber"); 
			
			tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.PASS, "Upgrade-Purchase Information Page Displayed");
			
			memberNumber.sendKeys(memberNo);
				
				tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.PASS, "Member No Entered as: "+memberNo);
			
				driver.findElement(searchButton).click();
				
				
				waitForSometime(tcConfig.getConfig().get("LongWait"));
			
				
				try{
					new WebDriverWait(driver, 15).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				
					}catch(Exception e){
						System.out.println("No Alert Present after Login");
					}
				
					
				if(driver.findElement(errorUpgradePage).getText().contains("Selected WAAM contract is not eligible for trade since it is not paid off.")){
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.FAIL, "Hard stop message is displayed when trying to upgrade for WBW WAAM that owns a paid off WAAM");
					
				}else{
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.PASS, "Hard stop message is not displayed when trying to upgrade for WBW WAAM that owns a paid off WAAM");
				}
				
				String grossPurchasePrice= driver.findElement(By.xpath("//td[@id='tradePurchPriceTd']")).getText();
				grossPurchasePrice=grossPurchasePrice.substring(grossPurchasePrice.indexOf('$') + 1, grossPurchasePrice.length());
				grossPurchasePrice=grossPurchasePrice.replaceAll(",","");
				System.out.println(grossPurchasePrice);
				if (Double.parseDouble(grossPurchasePrice) > 0.00) {
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.PASS,
							"Member search result displayed");
				} else {
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.FAIL,
							"Member search result not displayed");
				}
				driver.findElement(tscTxtBx).sendKeys(testData.get("strTourId"));
				driver.findElement(nextButton).click();
				
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				
				if(verifyObjectDisplayed(OwnerInfoScreenPage)){
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.PASS,
							"Owner Info Screen page displayed");
				} else {
					tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.FAIL,
							"Owner Info Screen page not displayed");
					}
		}else{
			
			tcConfig.updateTestReporter("SalePointUpgradeContract", "memberSearchForPaidOffWAAM", Status.FAIL, "Error in Upgrade-Purchase Information Page");
		}	
	}

}
