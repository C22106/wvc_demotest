package salepoint.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointOwnerPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointHomePage.class);
	SalePointOwnerEditPage spOwnerEdit = new SalePointOwnerEditPage(tcConfig);
	public static String strOwnerName;

	public SalePointOwnerPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@class='button' and @id='primaryOwner']")
	protected WebElement primaryOwnerEdit;

	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Owners')]")
	protected WebElement headerContractProcessOwner;

	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Customer Information')]")
	protected WebElement headerCustInfo;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected List<WebElement> deleteOwner;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteOwnerObj;

	@FindBy(xpath = "//a[contains(.,'Status')]")
	protected WebElement StatusOwner;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	protected WebElement pName;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[2]")
	protected WebElement deleteListOfOwner;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[1]")
	protected WebElement deleteFirstOwner;

	@FindBy(xpath = "(//a[contains(.,'Delete')])")
	protected WebElement deleteonesecOwner;

	@FindBy(xpath = "(//a[contains(.,'Status')])[1]")
	protected WebElement statusListOfOwner;

	@FindBy(xpath = "//div[contains(text(),'Edit Primary Owner')]")
	protected WebElement editPrimaryOwner;

	@FindBy(xpath = "//input[@value='Save and Print' and contains(@onclick,'Print')]")
	protected WebElement saveClick;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//input[@name='first_name']")
	protected WebElement txtFirstName;

	@FindBy(xpath = "//input[@name='last_name']")
	protected WebElement txtLastName;

	/*
	 * Method: sp_WVR_edit_PrimaryOwner Description:Edit Primary Owner Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WVR_edit_PrimaryOwner() throws Exception {

		int noOfSecOwn = 0;

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLongWait");

		if (verifyObjectDisplayed(primaryOwnerEdit)) {

			primaryOwnerEdit.click();

			// Owner Page - Primary Owner and Secondary Owner

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(headerContractProcessOwner)) {

				// check the secondary owner count with delete buttons

				if (verifyObjectDisplayed(deleteOwnerObj)) {
					// secOwnlist = deleteOwner;
					noOfSecOwn = deleteOwner.size();
				}

				// check primary owner

				if (verifyElementDisplayed(pName)) {
					// strOwnerName=pName.getText();
					System.out.println("Primary Owner Exists :" + strOwnerName);
					// delete all secondary owner
					for (int i = 0; i < noOfSecOwn; i++) {
						clickElementJSWithWait(deleteFirstOwner);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
				} else {

					log.info("No Primary Owner Exists");
					// delete all secondary owner except one

					for (int i = 0; i < (noOfSecOwn - 1); i++) {
						clickElementJSWithWait(deleteListOfOwner);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

					// make one secondary to primary

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(statusListOfOwner)) {
						clickElementJSWithWait(statusListOfOwner);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}
				}

			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(editPrimaryOwner)) {
				strOwnerName = txtFirstName.getAttribute("value") + " " + txtLastName.getAttribute("value");
				spOwnerEdit.sp_WVR_Owner_Edit_Form();
				waitUntilElementVisible(driver, headerContractProcessOwner, "ExplicitLongWait");

			}
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(headerContractProcessOwner)) {

				// check the sec6ondary owner count with delete buttons
				if (verifyObjectDisplayed(deleteOwnerObj)) {
					// secOwnlist = deleteOwner;
					noOfSecOwn = deleteOwner.size();
				}

				// delete all secondary owner
				for (int i = 0; i < noOfSecOwn; i++) {
					clickElementJSWithWait(deleteFirstOwner);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					driver.switchTo().alert().accept();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

				// Click On Next
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyElementDisplayed(nextVal)) {
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJS(nextVal);
				}
			}

		} else {
			System.out.println("No Primary owner edit button");
		}

		waitUntilElementVisibleIE(driver, saveClick, "ExplicitLongWait");

	}

}
