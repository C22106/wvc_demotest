package salepoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWPaymentOptionsPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWPaymentOptionsPage.class);

	public WBWPaymentOptionsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Payment')]")
	protected WebElement paymentOptionsHeader;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Payment')]")
	protected WebElement paymentOptionsHeaderDiv;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//select[contains(@id,'pymtTypeId')]")
	protected List<WebElement> selectDownPayment;

	@FindBy(name = "useSameAutoPay")
	protected WebElement useSameAutoPay;

	@FindBy(xpath = "//select[@id='duesPACId']")
	protected WebElement selectPaymentMethod;

	@FindBy(xpath = "//input[contains(@name,'Token')]")
	protected WebElement tokenInputBox;

	@FindBy(xpath = "//select[@name='duesCCPACInfo.cardType']")
	protected WebElement selectCardType;

	@FindBy(name = "duesCCPACInfo.cardMemberName")
	protected WebElement nameOnCard;

	@FindBy(name = "duesCCPACInfo.cardNumber")
	protected WebElement enterCardNumber;

	@FindBy(xpath = "//select[@name='duesCCPACInfo.expireMonth']")
	protected WebElement selectExpiryMonth;

	@FindBy(xpath = "//select[@name='duesCCPACInfo.expireYear']")
	protected WebElement selectExpiryYear;

	/*
	 * Method: downPaymentPageNavigation Description: Payments Options Page Nav
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void paymentOptionsNavigation() throws Exception {
		waitUntilElementVisible(driver, paymentOptionsHeaderDiv, "ExplicitLongWait");
		Assert.assertTrue(
				verifyElementDisplayed(paymentOptionsHeader) || verifyElementDisplayed(paymentOptionsHeaderDiv),
				"Payment Options page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Payment Options Page");

	}

	/*
	 * Method: selectPaymentMethod Description:Select Payment Method Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectDownPayment() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectDownPayment.get(0), testData.get("downPayment"));
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}

	/*
	 * Method: selectPaymentMethod Description:Select Payment Method Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectPaymentMethod() {
		waitUntilElementVisibleIE(driver, selectPaymentMethod, "ExplicitMedWait");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectPaymentMethod, testData.get("PaymentBy"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	/*
	 * Method: enterPaymentDetails Description:enter Payment Details Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterPaymentDetails() {
		if (testData.get("PaymentBy").toLowerCase().contains("cc")) {
			waitUntilElementVisibleIE(driver, tokenInputBox, "ExplicitMedWait");
			sendKeyboardKeys(Keys.TAB);
			enterCCAutoPayDetails();
		} else if (testData.get("PaymentBy").toLowerCase().contains("no")) {
			log.info("No Auto Pay Selected");
		} else {
			waitUntilElementVisibleIE(driver, tokenInputBox, "ExplicitMedWait");
			// write code for Auto Pay
		}

	}

	/*
	 * Method: enterCcAutoPayDetails Description:enter Auto Pay Payment Details
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void enterCCAutoPayDetails() {
		selectByText(selectCardType, testData.get("CCType"));
		nameOnCard.clear();
		nameOnCard.sendKeys(testData.get("CCHolder"));
		enterCardNumber.clear();
		enterCardNumber.sendKeys(testData.get("CCNumber"));
		selectByText(selectExpiryMonth, testData.get("CCMonth"));
		selectByText(selectExpiryYear, testData.get("CCYear"));
	}

	/*
	 * Method: selectSameAutoPay Description:SelectSameAuto Pay Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectSameAutoPay() {
		if (verifyElementDisplayed(useSameAutoPay)) {
			clickElementJS(useSameAutoPay);
		}

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();

		} catch (Exception e) {
			log.info("no Alert is displayed");
		}

	}
}