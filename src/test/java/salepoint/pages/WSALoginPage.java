package salepoint.pages;

import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WSALoginPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WSALoginPage.class);

	public WSALoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@id='fldUsername']")
	protected WebElement textUsernameWSA;

	@FindBy(xpath = "//input[@id='fldPassword']")
	protected WebElement textPasswordWSA;

	@FindBy(xpath = "//button[@name='cn-submit']")
	protected WebElement buttonLogInWSA;

	@FindBy(xpath = "//img[@title='Change Sales Site']")
	protected WebElement changeSiteiconWSA;

	@FindBy(xpath = "//input[@name='logon']")
	protected WebElement buttonLogIn;

	@FindBy(xpath = "//img[@title='Change Sales Site']")
	protected WebElement changeSiteicon;

	@FindBy(xpath = "//a[text()='Logout']")
	protected WebElement LogoutBtn;

	@FindBy(xpath = "//div[@class='msg inform']")
	protected WebElement logoutMsg;

	@FindBy(xpath = "//img[@title='Home']")
	protected WebElement homeIcon;

	 
	/*
	 * Method: launchAppWSA Description:Launch App Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */ 
	
	public void launchAppWSA(String strBrowser) {

		//this.driver = checkAndInitBrowser(strBrowser);
		driver.navigate().to((testData.get("WSAURL")));
		// driver.manage().window().maximize();
	}
	
	
	/*
	 * Method : launchApplication Parameters :None Description : launch
	 * application in browser Author : CTS Date : 2020 Change By : None
	 */

	public void launchApplication() {
		String url = testData.get("URL");
		driver.manage().deleteAllCookies();
		driver.navigate().to(url);
		driver.navigate().refresh();
		driver.manage().window().maximize();
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
				"Application is Launched Successfully , URL : " + url);

	}

	/*
	 * Method: setUserNameWSA Description:Set Username Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void setUserNameWSA(String strUserName) throws Exception {
		waitUntilElementVisible(driver, textUsernameWSA, "ExplicitLongWait");
		/*
		 * driver.manage().deleteAllCookies(); driver.navigate().refresh();
		 */
		waitUntilElementVisible(driver, textUsernameWSA, "ExplicitLongWait");
		textUsernameWSA.sendKeys(strUserName);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Username is entered for WSA Login");
	}

	/*
	 * Method: setPasswordWSA Description:Set password Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void setPasswordWSA(String strPassword) throws Exception {
		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");

		textPasswordWSA.sendKeys(dString);
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Password is entered for WSA Login");
	}

	/*
	 * Method: loginToWSA Description:Login To WSA Date: 2020 Author:Abhijeet
	 * Roy Changes By: NA
	 */

	public void loginToWSA(String strBrowser) throws Exception {

		// launchAppWSA(strBrowser);
		String userid = testData.get("WSA_UserID");
		String password = testData.get("WSA_Password");
		setUserNameWSA(userid);
		setPasswordWSA(password);
		buttonLogInWSA.click();
		waitUntilElementVisible(driver, changeSiteiconWSA, "ExplicitLongWait");
		driver.navigate().refresh();
		waitUntilElementVisible(driver, changeSiteiconWSA, "ExplicitLongWait");

		if (verifyElementDisplayed(changeSiteiconWSA)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log in button clicked");
		} else {
			launchAppWSA(strBrowser);

			setUserNameWSA(userid);
			setPasswordWSA(password);
			buttonLogIn.click();
			waitUntilElementVisible(driver, changeSiteicon, "ExplicitLongWait");
			driver.navigate().refresh();
			waitUntilElementVisible(driver, changeSiteicon, "ExplicitLongWait");

			if (verifyElementDisplayed(changeSiteicon)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log in button clicked");
			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Log in button clicked : But application not opened");
			}
		}
	}

	/*
	 * Method: WSALogoutApp Description:Log Out Date: 2020 Author:Abhijeet Roy
	 * Changes By: NA
	 */

	public void WSALogoutApp() throws Exception {
		try {
			waitUntilElementVisible(driver, LogoutBtn, "ExplicitLongWait");
			LogoutBtn.click();
			waitUntilElementVisible(driver, textUsernameWSA, "ExplicitLongWait");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Log Out Succesful");
		} catch (Exception e) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Log Out Failed" + e.getMessage());
		}
	}

}
