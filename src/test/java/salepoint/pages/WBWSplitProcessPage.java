package salepoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WBWSplitProcessPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WBWSplitProcessPage.class);

	public WBWSplitProcessPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Contract Processing')]")
	protected WebElement splitProcessing;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(name = "credits2")
	protected WebElement splitCreditSecondary;

	@FindBy(xpath = "//input[@name='paymentType' and @CHECKED]")
	protected WebElement selectedPaymentType;
	
	@FindBy(xpath = "//input[@name='familySplit' and @value='Yes']")
	protected WebElement familySplitYes;

	@FindBy(name = "AddSplit")
	protected WebElement addSplit;

	@FindBy(name = "process")
	protected WebElement processCombine;

	/*
	 * Method: splitProcessingNavigation Description:Split Processing Page Nav
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void splitProcessingNavigation() throws Exception {
		waitUntilElementVisible(driver, splitProcessing, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(splitProcessing), "Split Processing page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigated To Split Processing Page");

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		clickElementJS(nextButton);

	}

	/*
	 * Method: enterSecondaryCredit Description:Enter Secondary Credit Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void enterSecondaryCredit() {
		if (splitCreditSecondary.isEnabled()) {
			splitCreditSecondary.sendKeys(String.valueOf(testData.get("SecondaryCredits")));
			sendKeyboardKeys(Keys.TAB);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}

	}

	/*
	 * Method: selectFamilySplit Description:Select Family Split Type Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void selectFamilySplit() {
		if (verifyObjectDisplayed(familySplitYes)) {
			familySplitYes.click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				new WebDriverWait(driver, 10).until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				System.out.println("No Alert Present after Login");
			}

		}

	}

}