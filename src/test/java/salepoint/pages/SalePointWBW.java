package salepoint.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalePointWBW extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalePointWBW.class);

	public SalePointWBW(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//input[@name='ok']")
	protected WebElement okBtn;

	@FindBy(name = "tourID")
	protected WebElement tourID;

	@FindBy(name = "tsc")
	protected WebElement tscCode;

	@FindBy(name = "member_number")
	protected WebElement memberNumber;

	@FindBy(name = "tourTime")
	protected WebElement tourTime;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//input[@name='search']")
	protected WebElement searchButton;

	@FindBy(xpath = "//input[@name='salesman']")
	protected WebElement saleper;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteSec;

	@FindBy(xpath = "//input[@value='Save and Print']")
	protected WebElement savecontract;

	@FindBy(xpath = "//input[@name='print']")
	protected WebElement generate;

	@FindBy(xpath = "//td[contains(.,'The Contract Number for this contract is:')]/b")
	protected WebElement conNo;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNo;

	String strContractNo = "";
	String strMemberNo = "";

	@FindBy(id = "customerNumber")
	protected WebElement customerNo;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextVal;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	protected WebElement saleType;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	protected WebElement udiSelect;

	@FindBy(xpath = "//input[@name='sales_types' and @value='prestige_club']")
	protected WebElement CWPR;

	@FindBy(id = "Print")
	protected WebElement print;

	@FindBy(id = "newPoints")
	protected WebElement newPoints;

	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculate;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextClick;

	@FindBy(id = "customerNumber")
	protected WebElement customerNoInput;

	@FindBy(xpath = "//input[@name='fvl_saleType' and @value='M']")
	protected WebElement saleTypeSelect;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteCheck;

	@FindBy(xpath = "//input[@name='sales_types' and @value='UDI']")
	protected WebElement udiClick;

	@FindBy(id = "newPoints")
	protected WebElement newPointsCheck;

	@FindBy(xpath = "//input[@value='Calculate']")
	protected WebElement calculateClick;

	@FindBy(xpath = "//input[@name='salesman']")
	protected WebElement salePersonSelect;

	@FindBy(xpath = "//input[@value='Save and Print']")
	protected WebElement saveClick;

	@FindBy(id = "Print")
	protected WebElement printClick;

	@FindBy(xpath = "//td[contains(.,'The Primary Owner Member Number is')]")
	protected WebElement memberNoCheck;

	@FindBy(xpath = "//input[@name='ssn']")
	protected WebElement ssnSelect;

	@FindBy(xpath = "//input[@name='dobmonth']")
	protected WebElement dobMonth;

	@FindBy(xpath = "//input[@name='dobday']")
	protected WebElement dobDay;

	@FindBy(xpath = "//input[@name='dobyear']")
	protected WebElement dobYear;

	@FindBy(xpath = "//input[@name='sex' and @value='M']")
	protected WebElement genderSelect;

	@FindBy(xpath = "//td[contains(.,'Cash Sale')]/input[@type='radio']")
	protected WebElement cashSelect;

	@FindBy(xpath = "//td[contains(.,'ACH Auto Pay')]/input[@type='radio']")
	protected WebElement achSelect;

	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Owners')]")
	protected WebElement headerContractProcessOwner;

	@FindBy(xpath = "//div[contains(text(),'Contract Processing - Customer Information')]")
	protected WebElement headerCustInfo;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteOwner;

	@FindBy(xpath = "//a[contains(.,'Status')]")
	protected WebElement StatusOwner;

	@FindBy(xpath = "//select[@name='dobmonth']")
	protected WebElement MonthFromDrpDwn;

	@FindBy(xpath = "//select[@name='dobday']")
	protected WebElement DateFromDrpDwn;

	@FindBy(xpath = "//select[@name='dobyear']")
	protected WebElement YearFromDrpDwn;

	@FindBy(id = "saveContract")
	protected WebElement saveContractClick;

	@FindBy(xpath = "//input[@id='saveContract']")
	protected WebElement savecontractWBW;

	@FindBy(xpath = "//tr/td[contains(text(),'Finance Information')]/../td/input[@type='button']")
	protected WebElement editPaymentWBW;

	@FindBy(xpath = "//tr/td[contains(text(),'Owner Information')]/../td/input[@type='button']")
	protected WebElement editMemberWBW;

	@FindBy(xpath = "//tr/td[contains(text(),'Finance Information')]/../td/input[@type='button']")
	protected WebElement editPaymentWBWelmnt;

	@FindBy(xpath = "//tr/td[contains(text(),'Owner Information')]/../td/input[@type='button']")
	protected WebElement editMemberWBWelmnt;

	@FindBy(xpath = "//input[@name='homePhone']")
	protected WebElement homePhoneSelect;

	@FindBy(xpath = "//input[@name='homePhone']")
	protected WebElement homePhoneWBW;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Primary Owner Information')]")
	protected WebElement primaryOwnerPage;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Finance Information')]")
	protected WebElement financePage;

	@FindBy(xpath = "//tr[@class='page_title']//div[contains(.,'Owners')]")
	protected WebElement ownerDetailsPage;

	@FindBy(xpath = "//input[@name='socialSecurityNumber']")
	protected WebElement ssnWBW;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextOwner;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement next2;

	@FindBy(xpath = "//input[@name='socialSecurityNumber']")
	protected WebElement ssnWBWSelect;

	@FindBy(xpath = "//td[contains(.,'Primary Owner')]//tr[@class='evenRow'][1]")
	protected WebElement pNameWBW;

	@FindBy(xpath = "//a[contains(.,'Edit')][1]")
	protected WebElement editPOwnerWBW;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected List<WebElement> deleteOwnerWBW;

	@FindBy(xpath = "//a[contains(.,'Delete')]")
	protected WebElement deleteOwnerWBWObj;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[1]")
	protected WebElement deleteFirstOwnerWBW;

	@FindBy(xpath = "(//a[contains(.,'Delete')])[2]")
	protected WebElement deleteListOfOwnerWBW;

	@FindBy(xpath = "//a[contains(.,'Make Primary')]")
	protected WebElement MakePrimaryWBW;

	@FindBy(xpath = "//select[contains(@id,'loanPACId')]")
	protected WebElement contractPayMethod;

	@FindBy(xpath = "//input[contains(@name,'ccPacNameOnCard')]")
	protected WebElement ccNameWBW;

	@FindBy(xpath = "//input[contains(@name,'ccPacCardNum')]")
	protected WebElement ccNumberWBW;

	@FindBy(xpath = "//select[contains(@name,'ccPacCardType')]")
	protected WebElement ccTypeWBW;

	@FindBy(xpath = "//select[contains(@name,'ccPacCardExpiresMonth')]")
	protected WebElement ccMonthWBW;

	@FindBy(xpath = "//select[contains(@name,'ccPacCardExpiresYear')]")
	protected WebElement ccYearWBW;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement financeNext;

	@FindBy(xpath = "//input[@value='Experience']")
	protected WebElement experienceSelect;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	protected WebElement summaryPage;

	@FindBy(xpath = "//input[@value='WorldMark']")
	protected WebElement worldMarkSelect;

	@FindBy(xpath = "//input[@value='Next']")
	protected WebElement nextValue;

	@FindBy(xpath = "//select[@name='beginDateMonth']")
	protected WebElement MonthFromDrpDwnWBW;

	@FindBy(xpath = "//select[@name='beginDateDay']")
	protected WebElement DateFromDrpDwnWBW;

	@FindBy(xpath = "//select[@name='beginDateYear']")
	protected WebElement YearFromDrpDwnWBW;

	@FindBy(xpath = "//td[contains(.,'V')]")
	protected WebElement VoidElement;

	@FindBy(xpath = "//td[contains(.,'Void')]")
	List<WebElement> VoidElementWBW;

	@FindBy(xpath = "//td[contains(.,'Void')]")
	protected WebElement VoidElementWBWObj;

	@FindBy(xpath = "//input[@value='Experience']")
	protected WebElement experienceClick;

	@FindBy(xpath = "//input[@value='WorldMark']")
	protected WebElement worldmarkClick;

	@FindBy(xpath = "//tr[@class='page_title']//div")
	protected WebElement contractSummary;

	@FindBy(xpath = "//input[@value='Finalize']")
	protected WebElement finalizeButton;

	@FindBy(xpath = "//input[@value='Void']")
	protected WebElement voidButton;

	/*
	 * Method: sp_WBW_Contract_Create_Automation_Flow Description:Create
	 * Contract WBW Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WBW_Contract_Create_Automation_Flow() throws Exception {

		// Enter Tour Number and check for Automation Pop Up *Try for 2 times *

		try {
			sp_WBW_ValidateAutoPopUp();
			waitUntilElementVisibleIE(driver, saveContractClick, "ExplicitLongWait");
			sp_WBW_Contract_FullFlow();
			sp_Ex_contractSearchAndVoid();
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Automation Pop Up message is Displyaed");

		} catch (Exception e) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
					"Automation Pop Up message is not Displyaed for first Search");

			try {

				sp_WBW_ValidateAutoPopUp();
				waitUntilElementVisibleIE(driver, saveContractClick, "ExplicitLongWait");
				sp_WBW_Contract_FullFlow();
				sp_Ex_contractSearchAndVoid();
				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WBW_Contract_Create_Automation_Flow",
						Status.PASS, "Automation Pop Up message is Displyaed after 2nd Try");

			} catch (Exception e2) {

				tcConfig.updateTestReporter("SalePointWBWMenuPage", "sp_WBW_Contract_Create_Automation_Flow",
						Status.FAIL, "Automation Pop Up message is not Displyaed for Second Search");

			}

		}

	}

	/*
	 * Method: sp_WBW_ValidateAutoPopUp Description:Validate Auto Pop Up Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WBW_ValidateAutoPopUp() throws Exception {

		// Enter the MENu URL to Navigate to Tour Search page
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, tourID, "ExplicitLongWait");
		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// select wave time
			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Page Navigation Error");
		}

		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.alertIsPresent());

			if (ExpectedConditions.alertIsPresent() != null) {

				driver.switchTo().alert().accept();
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Automation Pop Up message");
			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
						"Automation Pop Up is not available");
			}

		} catch (Exception e) {
			log.info("no Alert is displayed");
		}
	}

	/*
	 * Method: sp_WBW_edit_PrimaryOwner Description:Edit Primary Owner Date:
	 * 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WBW_edit_PrimaryOwner() throws Exception {

		int noOfSecOwnWBW = 0;
		List<WebElement> secOwnlistWBW = null;

		// Edit the Owner

		if (verifyObjectDisplayed(editMemberWBW)) {

			editMemberWBW.click();

			if (verifyObjectDisplayed(primaryOwnerPage)) {

				// Edit Owner Info

				sp_WBW_Owner_Edit_Form();

			} else if (verifyElementDisplayed(ownerDetailsPage)) {
				log.info("OwnerDetails Page Directly Opened");

				// check the secondary owner count with delete buttons

				if (verifyObjectDisplayed(deleteOwnerWBWObj)) {
					// secOwnlistWBW = driver.findElements(deleteOwnerWBW);

					noOfSecOwnWBW = deleteOwnerWBW.size();

				}

				// check primary owner

				if (verifyElementDisplayed(pNameWBW)) {

					// log.info("Primary Owner Exists");

					// delete all secondary owner
					for (int i = 0; i < noOfSecOwnWBW; i++) {

						clickElementJSWithWait(deleteFirstOwnerWBW);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

						if (ExpectedConditions.alertIsPresent() != null) {
							try {

								driver.switchTo().alert().accept();

							} catch (Exception e) {
								log.info(e);
							}
						}
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}
				} else {
					log.info("Primary Owner Does not exist");

					// delete all secondary owner except one

					for (int i = 0; i < (noOfSecOwnWBW - 1); i++) {

						// clickElementJSWithWait(secOwnlist.get(0));
						clickElementJSWithWait(deleteListOfOwnerWBW);

						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					}

					// make one secondary to primary

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(MakePrimaryWBW)) {
						clickElementJSWithWait(MakePrimaryWBW);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						driver.switchTo().alert().accept();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

				}
				log.info("Page 0");

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (verifyObjectDisplayed(editPOwnerWBW)) {

					clickElementJSWithWait(editPOwnerWBW);

					log.info("Page 1");

					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (verifyObjectDisplayed(primaryOwnerPage)) {

						sp_WBW_Owner_Edit_Form();

					}

					else {
						log.info("Primary Owner Page is not opened");
					}

				}

			} else {
				log.info("No Primary owner edit button");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			waitUntilElementVisibleIE(driver, nextOwner, "ExplicitLongWait");

			clickElementJS(nextOwner);

		} else {
			log.info("No Primary owner edit button");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Owner Edit Option is not available");

		}

	}

	/*
	 * Method: sp_WBW_edit_Payment Description:WBW Wdit Payment Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WBW_edit_Payment() throws Exception {

		waitUntilElementVisibleIE(driver, editPaymentWBW, "ExplicitLongWait");

		clickElementJS((editPaymentWBW));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, financePage, "ExplicitLongWait");

		waitUntilElementVisible(driver, contractPayMethod, "ExplicitLongWait");

		if (verifyElementDisplayed(contractPayMethod)) {

			contractPayMethod.click();
			String payMethod = testData.get("PayMethod");

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			driver.findElement(By.xpath("//select[contains(@id,'loanPACId')]/option[contains(.,'" + payMethod + "')]"))
					.click();

		}

		waitUntilElementVisible(driver, ccNameWBW, "ExplicitLongWait");

		ccNameWBW.clear();
		ccNameWBW.click();
		ccNameWBW.sendKeys(testData.get("CCHolder"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		ccTypeWBW.click();
		String cardType = testData.get("CCType");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(By.xpath("//select[contains(@name,'ccPacCardType')]/option[contains(.,'" + cardType + "')]"))
				.click();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		ccNumberWBW.clear();
		ccNumberWBW.click();
		ccNumberWBW.sendKeys(testData.get("CCNumber"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		ccMonthWBW.click();
		String expMnth = testData.get("CCMonth");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(
				By.xpath("//select[contains(@name,'ccPacCardExpiresMonth')]/option[contains(.,'" + expMnth + "')]"))
				.click();

		ccYearWBW.click();
		String expYr = testData.get("CCYear");
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.findElement(
				By.xpath("//select[contains(@name,'ccPacCardExpiresYear')]/option[contains(.,'" + expYr + "')]"))
				.click();

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, financeNext, "ExplicitLongWait");
		clickElementJS(financeNext);

		waitUntilElementVisibleIE(driver, saveContractClick, "ExplicitLongWait");
	}

	/*
	 * Method: sp_WBW_Owner_Edit_Form Description:WBW Owner Edit Form Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WBW_Owner_Edit_Form() throws Exception {

		try {
			// Enter the SSN
			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (ssnWBWSelect.isEnabled() == false) {

				log.info("SSN Not Required User Can Proceed");

			} else {
				log.info("Page 3");

				if (ssnWBWSelect.getAttribute("value").isEmpty() == true) {
					log.info("Page 4");

					ssnWBWSelect.clear();
					ssnWBWSelect.sendKeys(testData.get("ssnNumber"));
				}
			}

			// Enter the Home Ph Number
			if (homePhoneWBW.getAttribute("value").equals("")) {
				homePhoneWBW.sendKeys(testData.get("homeNumber"));

			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// Click on Next
			clickElementJS(nextOwner);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Owner Edit Information is completed");

		} catch (Exception e) {
			log.info("Wrong Value Entered");
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Wrong Value Entered");

		}

	}

	/*
	 * Method: sp_Contract_WBW_Save Description: Save WBW Contract Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */
	public void sp_Contract_WBW_Save() throws Exception {

		if (verifyElementDisplayed(savecontractWBW)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Save Button Clicked");

			clickElementJS(savecontractWBW);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				log.info("");
			}
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Save Page Navigation Error");

		}

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleIE(driver, generate, "ExplicitLongWait");

		if (verifyElementDisplayed(generate)) {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Review Page Displayed and Generate Button Clicked");
			// clickElementJSWithWait(generate);
			clickElementJS(generate);

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Generate contract Page Navigation Error");
		}

		// waitUntilElementVisibleIE(driver,
		// By.xpath("//FORM[1]/TABLE[1]/TBODY[1]/TR[4]/TD[1]/B[1]"), 120);
		waitUntilElementVisibleIE(driver, conNo, "ExplicitLongWait");

		if (verifyElementDisplayed(conNo)) {

			// testData.put("strContract", conNo.getText());
			strContractNo = conNo.getText();

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Created with number: " + conNo.getText());
		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Contract Creation failed ");
		}

		if (verifyElementDisplayed(memberNo)) {

			strMemberNo = memberNo.getText().split("\\:")[1].trim();
			log.info("MemberNo " + strMemberNo);

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Member No: " + strMemberNo);

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member number could not be retrieved");
		}

	}

	/*
	 * Method: sp_WBW_Contract_FullFlow Description:WBW Full Flow Date: 2020
	 * Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_WBW_Contract_FullFlow() throws Exception {

		sp_WBW_edit_PrimaryOwner();
		sp_WBW_edit_Payment();
		sp_Contract_WBW_Save();

	}

	/*
	 * Method: sp_Ex_contractSearchAndVoid Description:Search and Void Contract
	 * Date: 2020 Author:Abhijeet Roy Changes By: NA
	 */

	public void sp_Ex_contractSearchAndVoid() throws Exception {

		String fromDate = addDateInSpecificFormat("MM/dd/yyyy", -1);
		String[] arrDate = fromDate.split("/");
		String strMonthFromDrpDwn = arrDate[0];
		String strDateFromDrpDwn = arrDate[1];
		String strYearFromDrpDwn = arrDate[2];
		List<WebElement> voidList = null;

		int voidCountBefore = 0;
		int voidCountAfter = 0;

		if (strMonthFromDrpDwn.startsWith("0")) {
			strMonthFromDrpDwn = strMonthFromDrpDwn.replace("0", "");
		}
		if (strDateFromDrpDwn.startsWith("0")) {
			strDateFromDrpDwn = strDateFromDrpDwn.replace("0", "");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("contractMgmt"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

		waitUntilElementVisibleIE(driver, experienceSelect, "ExplicitLongWait");

		if (verifyElementDisplayed(experienceClick)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Contract Management Page Displayed");

			new Select(MonthFromDrpDwnWBW).selectByValue(strMonthFromDrpDwn);
			new Select(DateFromDrpDwnWBW).selectByValue(strDateFromDrpDwn);
			new Select(YearFromDrpDwnWBW).selectByValue(strYearFromDrpDwn);

			// JavascriptExecutor executor = (JavascriptExecutor) driver;
			clickElementJS(experienceClick);

			// clickElementJS(worldmarkClick);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			clickElementJS(nextButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"),
					"ExplicitLongWait");

			try {
				if (verifyObjectDisplayed(VoidElementWBWObj)) {

					voidCountBefore = VoidElementWBW.size();

				} else {
					voidCountBefore = 0;
				}
			} catch (Exception e) {
				voidCountBefore = 0;
			}

			if (verifyElementDisplayed(driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]")))) {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Member No Verified and is: " + strMemberNo);
				WebElement wb1 = driver.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"));
				clickElementJS(wb1);

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL, "Member No Not Verified");

			}

			waitUntilElementVisibleIE(driver, summaryPage, "ExplicitLongWait");

			if (verifyElementDisplayed(contractSummary)) {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Summary Page Navigation Successful and Verified");

				driver.switchTo().activeElement().sendKeys(Keys.PAGE_DOWN);

				if (verifyElementDisplayed(voidButton)) {

					clickElementJSWithWait(voidButton);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().alert().accept();

					waitForSometime(tcConfig.getConfig().get("LowWait"));
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					waitUntilElementVisibleIE(driver, By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]"),
							"ExplicitLongWait");
					String updatedStatus = driver
							.findElement(By.xpath("//tr/td/a[contains(.,'" + strMemberNo + "')]/../../td[5]"))
							.getText();

					if (updatedStatus.equalsIgnoreCase("Void")) {
						tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Contract Void");
					} else {

						tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
								"Contract was not Void");

					}
				}

			} else {
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
						"Summary Page Navigation Failed");
			}

		} else {
			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Contract Management Page Not Displayed");
		}

	}

	/*
	 * Method: navigateToMenuURL Description:Navigate To Menu URL Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void navigateToMenuURL() {
		// Enter the MENu URL to Navigate to Tour Search page
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		driver.navigate().to(testData.get("MenuURL"));

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	/*
	 * Method: enterTourID Description:enter Tour ID Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */

	public void enterTourID() throws Exception {

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, tourID, "ExplicitLongWait");
		if (verifyElementDisplayed(tourID)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"New Contract Page Navigation Successful");
			tourID.click();
			tourID.sendKeys(testData.get("strTourId"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// select wave time
			new Select(driver.findElement(By.name("tourTime"))).selectByVisibleText(testData.get("strWaveTime"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementJS(nextButton);
			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"New Contract Page Navigation Error");
		}
	}

	/*
	 * Method: enterMemberNumber Description:enter Member Number Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */

	public void enterMemberNumber() throws Exception {

		// Enter Customer/Tour Number

		waitUntilElementVisibleIE(driver, memberNumber, "ExplicitLongWait");
		if (verifyElementDisplayed(memberNumber)) {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
					"Member Number Field Navigation Successful");
			memberNumber.click();
			memberNumber.sendKeys(testData.get("strMemberNumber"));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {

			tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.FAIL,
					"Member Number Field Not Present ");
		}
	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}

	/*
	 * Method: acceptOrDeclineAutomationFlow Description:Accept Or Decline Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */

	public void acceptOrDeclineAutomationFlow(Boolean automation) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.alertIsPresent());

			if (ExpectedConditions.alertIsPresent() != null) {

				if (automation == true) {
					driver.switchTo().alert().accept();
				} else {
					driver.switchTo().alert().dismiss();
				}
				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS,
						"Automation Pop Up message displayed");
			} else {

				tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.INFO,
						"Automation Pop Up is not available");
			}

		} catch (Exception e) {
			log.info("no Alert is displayed");
		}
	}

}