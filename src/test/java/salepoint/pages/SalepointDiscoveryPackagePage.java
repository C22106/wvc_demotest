package salepoint.pages;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.crypto.SealedObject;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class SalepointDiscoveryPackagePage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(SalepointDiscoveryPackagePage.class);

	public SalepointDiscoveryPackagePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}
	
	@FindBy(xpath = "//div[contains(.,'Contract Processing - Discovery Package Selection')]")
	protected WebElement headerDiscoveryPackage;
	
	@FindBy(xpath = "//input[@value = 'Next']")
	protected WebElement clickNext;
	
	public void selectPackage(){
		String Package = testData.get("DiscoverPackage");
		waitUntilElementVisibleIE(driver, headerDiscoveryPackage, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(headerDiscoveryPackage), "Select Page Page not displayed");
		new Select(driver.findElement(By.xpath("//select[@name = 'invPkg']"))).selectByVisibleText(Package);
		tcConfig.updateTestReporter(getClassName(),getMethodName(), Status.PASS,
				"Package Seleceted as: "+Package);
	}
	
	public void clickNextButton(){
		waitUntilElementVisibleIE(driver, clickNext, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(clickNext), "Next Button is Displayed");
		clickElementJS(clickNext);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
	}
}