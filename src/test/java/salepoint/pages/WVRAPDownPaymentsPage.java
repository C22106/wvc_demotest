package salepoint.pages;

import java.awt.event.ActionEvent;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class WVRAPDownPaymentsPage extends SalePointBasePage {

	public static final Logger log = Logger.getLogger(WVRAPDownPaymentsPage.class);

	public WVRAPDownPaymentsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	@FindBy(xpath = "//tr[@class='page_title' and contains(.,'Down Payments')]")
	protected WebElement downPaymentsHeader;

	@FindBy(xpath = "//input[@name='next']")
	protected WebElement nextButton;

	@FindBy(xpath = "//select[@name='downPymtType']")
	protected List<WebElement> selectDownPayment;

	@FindBy(name = "downPymtCCCheckNum")
	protected List<WebElement> downPaymentCardNumber;

	@FindBy(name = "downPymtExpiresMonth")
	protected List<WebElement> downPaymentsExpiryMonth;

	@FindBy(name = "downPymtExpiresYear")
	protected List<WebElement> downPaymentsExpiryYear;

	/*
	 * Method: downPaymentPageNavigation Description:Down Payments Page Nav
	 * Date: 2020 Author:Unnat Jain Changes By: NA
	 */
	public void downPaymentPageNavigation() throws Exception {
		waitUntilElementVisible(driver, downPaymentsHeader, "ExplicitLongWait");
		Assert.assertTrue(verifyElementDisplayed(downPaymentsHeader), "Purchase Information page not displayed");
		tcConfig.updateTestReporter(getClassName(), getMethodName(), Status.PASS, "Navigaetd To Down Payments Page");

	}

	/*
	 * Method: selectFullDownPayment Description:Select Full Down Payment Date:
	 * 2020 Author:Unnat Jain Changes By: NA
	 */
	public void selectFullDownPayment() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(selectDownPayment.get(0), testData.get("downPayment"));
		sendKeyboardKeys(Keys.TAB);
		enterCardNumber();
		selectExpiryMonth();
		selectExpiryYear();
	}

	/*
	 * Method: selectExpiryYear Description:Select Month Date: 2020 Author:Unnat
	 * Jain Changes By: NA
	 */
	public void selectExpiryYear() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(downPaymentsExpiryYear.get(0), testData.get("CCYear"));

	}

	/*
	 * Method: selectExpiryMonth Description:Select Month Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void selectExpiryMonth() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		selectByText(downPaymentsExpiryMonth.get(0), testData.get("CCMonth"));

	}

	/*
	 * Method: enterCardNumber Description:Enter Card Last Four Digit Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void enterCardNumber() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		String cardLastFourDigits = testData.get("CCNumber");
		cardLastFourDigits = cardLastFourDigits.substring(cardLastFourDigits.length() - 4, cardLastFourDigits.length());
		downPaymentCardNumber.get(0).clear();
		downPaymentCardNumber.get(0).sendKeys(cardLastFourDigits);

	}

	/*
	 * Method: clickNextButton Description:Click Next Button Date: 2020
	 * Author:Unnat Jain Changes By: NA
	 */
	public void clickNextButton() {
		clickElementJS(nextButton);

	}
}