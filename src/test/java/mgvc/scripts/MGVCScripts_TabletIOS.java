package mgvc.scripts;

import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import mgvc.pages.MGVCHomePage;
import mgvc.pages.MGVCHomePage_IOS;
import mgvc.pages.MGVCHomePage_Web;

public class MGVCScripts_TabletIOS extends TestBase {
	public static final Logger log = Logger.getLogger(MGVCScripts_TabletIOS.class);

	@Test(dataProvider = "testData", groups = { "header", "regression", "IOS" })

	public void tc01_MGVC_Header_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			MGVCHomePage homePage = new MGVCHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateLogo();
			homePage.validateHomeHeaderMenu();

			/*homePage.validateHomeHeaderSubMenu("Discover Benefits", "Buying a Timeshare", 4);
			homePage.validateHomeHeaderSubMenu("Discover Benefits", "Using Your Timeshare", 4);
			homePage.validateHomeHeaderSubMenu("Discover Benefits", "Upgrade Your Ownership", 4);
			homePage.validateHomeHeaderSubMenu("Discover Benefits", "VIP By Wyndham", 4);

			homePage.validateHomeHeaderSubMenu("Resorts", "Resort News", 3);
			homePage.validateHomeHeaderSubMenu("Resorts", "Featured Destinations", 3);
			homePage.validateHomeHeaderSubMenu("Resorts", "Travel Inspiration", 3);
			
			homePage.validateHomeHeaderSubMenu("Owner Guide", "Dive Into Margarita Life", 3);
			homePage.validateHomeHeaderSubMenu("Owner Guide", "New Owner Quick Start", 3);
			homePage.validateHomeHeaderSubMenu("Owner Guide", "Resources", 3);
			
			homePage.validateHomeHeaderSubMenu("Deals & Offers", "Owner Exclusives", 4);
			homePage.validateHomeHeaderSubMenu("Deals & Offers", "Owner Travel Deals", 4);
			homePage.validateHomeHeaderSubMenu("Deals & Offers", "Partner Offers", 4);
			homePage.validateHomeHeaderSubMenu("Deals & Offers", "VIP Exclusives", 4);

			homePage.validateHomeHeaderSubMenu("Help", "Ask & Answer", 3);
			homePage.validateHomeHeaderSubMenu("Help", "Glossary", 3);
			homePage.validateHomeHeaderSubMenu("Help", "Wyndham Cares", 3);

			homePage.validateHomeHeaderSubMenu("COVID-19", "At-Home Benefits", 6);
			homePage.validateHomeHeaderSubMenu("COVID-19", "Financial Assistance", 6);
			homePage.validateHomeHeaderSubMenu("COVID-19", "Keep Your Points Safe", 6);
			homePage.validateHomeHeaderSubMenu("COVID-19", "Protecting Your Ownership", 6);
			homePage.validateHomeHeaderSubMenu("COVID-19", "Resort Updates", 6);
			homePage.validateHomeHeaderSubMenu("COVID-19", "Vacation Ready", 6);*/

			/*homePage.validateRegisterLink();
			homePage.validateSignInLink();*/

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(/*retryAnalyzer = RetryFailedTest.class,*/dataProvider = "testData", groups = { "footer", "regression", "IOS" })

	public void tc02_MGVC_Validate_Footer(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			MGVCHomePage homePage = new MGVCHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateFooterMenu();

			homePage.validateHomeFooterSubMenu("Discover Benefits", "Buying a Timeshare", 4);
			homePage.validateHomeFooterSubMenu("Discover Benefits", "Using Your Timeshare", 4);
			homePage.validateHomeFooterSubMenu("Discover Benefits", "Upgrade Your Ownership", 4);
			homePage.validateHomeFooterSubMenu("Discover Benefits", "VIP By Wyndham", 4);

			homePage.validateHomeFooterSubMenu("Resorts", "Resort News", 3);
			homePage.validateHomeFooterSubMenu("Resorts", "Featured Destinations", 3);
			homePage.validateHomeFooterSubMenu("Resorts", "Travel Inspiration", 3);
			
			homePage.validateHomeFooterSubMenu("Owner Guide", "Overview and Performance", 3);
			homePage.validateHomeFooterSubMenu("Owner Guide", "New Owner Quick Start", 3);
			homePage.validateHomeFooterSubMenu("Owner Guide", "Resources", 3);
			
			homePage.validateHomeFooterSubMenu("Deals & Offers", "Owner Exclusives", 4);
			homePage.validateHomeFooterSubMenu("Deals & Offers", "Owner Travel Deals", 4);
			homePage.validateHomeFooterSubMenu("Deals & Offers", "Partner Offers", 4);
			homePage.validateHomeFooterSubMenu("Deals & Offers", "VIP Exclusives", 4);

			homePage.validateHomeFooterSubMenu("Help", "Ask & Answer", 3);
			homePage.validateHomeFooterSubMenu("Help", "COVID-19", 3);
			homePage.validateHomeFooterSubMenu("Help", "Wyndham Cares", 3);
			
			homePage.validateDisclaimerText();

			/*homePage.validateFacebookLink();
			homePage.validateInstagramLink();
			homePage.validateTwitterLink();
			homePage.validatePinterestLink();*/
			

			homePage.validatefooterlogo();
			homePage.validatefooterCopyRight();
			
			/*homePage.validateTermsofUse();
			homePage.validatePrivacyNotice();
			homePage.validatePrivacySettings();
			homePage.validateDoNotSellInfo();*/

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "search", "regression", "IOS" })

	public void tc03_MGVC_Search_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			MGVCHomePage homePage = new MGVCHomePage_Web(tcconfig);
			homePage.launchApplication();			
			homePage.validateSearchFunctionality();
			homePage.searchResultsValidatins();
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "HomePageForm", "regression", "IOS" })

	public void tc04_MGVC_HomePage_Form_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			MGVCHomePage homePage = new MGVCHomePage_IOS(tcconfig);
			homePage.launchApplication();
			homePage.formValidations();
			
			

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	@Test(dataProvider = "testData", groups = { "validateCTA", "regression", "IOS" })

	public void tc05_MGVC_Banner_with_CTA_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			MGVCHomePage homePage = new MGVCHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateBanners();
			//homePage.validateCardComponent();

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}
	
	@Test(dataProvider = "testData", groups = { "testimonials", "regression", "IOS" })

	public void tc06_MGVC_Testimonials_Validation(Map<String, String> testData) throws Exception {
		setupTestData(testData);
		try {
			MGVCHomePage homePage = new MGVCHomePage_Web(tcconfig);
			homePage.launchApplication();
			homePage.validateFeaturedResort();
			homePage.validateWelcomeToTheClub();
			homePage.validateBlissfulBenefits();
			homePage.validateGuidesInspirations();
			/*homePage.validateCardComponent();
			homePage.navigateBack();*/
			
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}