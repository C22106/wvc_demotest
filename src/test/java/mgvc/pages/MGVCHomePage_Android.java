package mgvc.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class MGVCHomePage_Android extends MGVCHomePage {

	public MGVCHomePage_Android(TestConfig tcconfig) {
		super(tcconfig);
		discoverBenefits = By
				.xpath("//div[contains(@class,'MGVCHeader')]//li/a/span[contains(text(),'Discover Benefits')]/..");
		resorts = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a/span[contains(text(),'Resorts')]/..");
		ownerGuide = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a/span[contains(text(),'Owner Guide')]/..");
		dealsOffers = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a/span[contains(text(),'Deals & Offers')]/..");
		help = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a/span[contains(text(),'Help')]/..");
		COVID19Link = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a/span[contains(text(),'COVID-19')]/..");
		margarittaVilleLogo = By.xpath("//a[img[contains(@alt,'Margaritaville Logo')]]");
		registerLink = By.xpath("(//a[contains(text(),'Register')])[2]");
		signInLink = By.xpath("(//a[contains(text(),'Sign In')])[2]");
		searchContainer = By.xpath("//input[@type='text']");
		searchIcon = By.xpath("//div[contains(@class,'input-group-button')]/button[@type='submit']");
		formTitle = By.xpath("//div[contains(@class,'fovalidateFeaturedResortrmTitle')]|//div[contains(@class,'formTitle')]");
		formDescription = By.xpath(
				"//div[contains(@class,'formTitle')]/../../..//div[contains(@class,'body-1') and contains(text(),'Please take a few moments')]");
		featuredResortImage = By.xpath(
				"(//div[contains(.,'Margaritaville Vacation Club by Wyndham - Nashville')]/div[@gtm_component]//div[contains(@class,'cardBanner')]//img)[1]");
		featuredResortTitle = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Margaritaville Vacation Club by Wyndham - Nashville')]");
		featuredResortBody = By.xpath(
				"(//div[contains(.,'Margaritaville Vacation Club by Wyndham - Nashville')]/div[@gtm_component]//div[contains(@class,'cardBanner')]//div[contains(@class,'body')])[1]");
		btnLearnMorefeaturedResort = By.xpath(
				"(//div[contains(.,'Margaritaville Vacation Club by Wyndham - Nashville')]/div[@gtm_component]//div[contains(@class,'cardBanner')]//a)[1]");
		
		welcomeToTheClubImage = By.xpath(
				"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[@class='cardBanner']//img)[2]");
		welcomeToTheClubTitle = By.xpath(
				"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[2]");
		welcomeToTheClubBody = By.xpath(
				"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[2]");
		btnLearnMorewelcomeToTheClub = By.xpath(
				"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[2]");
		
		blissfulBenefitsImage = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]/../../../..//img");
		blissfulBenefitsTitle = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]");
		blissfulBenefitsBody = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]/../div[@class='body-1']");
		btnLearnMoreblissfulBenefits = By.xpath(
				"//div[@class='hide-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]/../a");
		
		featureGuideImage = By
				.xpath("(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[@class='cardBanner']//img)[1]");
		featureGuideTitle = By.xpath(
				"(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[1]");
		featureGuideBody = By.xpath(
				"(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[1]");
		btnLearnMorefeatureGuide = By.xpath(
				"(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[1]");
	}

	protected By hamburgerCTA = By.xpath("(//div[contains(@class,'MGVCHeader')]//div[contains(@id,'nav')]/span)[1]");
	protected By moreResultsLink = By.xpath("//a[contains(text(),'More Results')]");

	@Override
	public void validateLogo() {
		assertTrue(verifyObjectDisplayed(margarittaVilleLogo), "Header MGVC logo is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateLogo", Status.PASS, "Header MGVC logo is displayed");

		assertTrue(getElementAttribute(margarittaVilleLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Header MGVC logo url not correct");
		tcConfig.updateTestReporter("MGVCHomePage", "validateLogo", Status.PASS, "Header MGVC logo is present");
	}

	@Override
	public void validateHomeHeaderMenu() {
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(discoverBenefits), "Discover Benefits menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Discover Benefits menu item is present");
		navigateValidateAndReturn(discoverBenefits);
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(resorts), "Resorts menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Resorts menu item is present");
		navigateValidateAndReturn(resorts);
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(ownerGuide), "Owner Guide menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Owner Guide menu item is present");
		navigateValidateAndReturn(ownerGuide);
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(dealsOffers), "Deals and Offers menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Deals and Offers menu item is present");
		navigateValidateAndReturn(dealsOffers);
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(help), "Help menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS, "Help menu item is present");
		navigateValidateAndReturn(help);
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(COVID19Link), "Covid 19 menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Covid 19 menu item is present");
		navigateValidateAndReturn(COVID19Link);
	}

	@Override
	public void validateHomeHeaderSubMenu(String headerMenuItem, String subMenu, int count) {
		String href = "";

		if (count == 0) {
			clickElementBy(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem + "']]]"));
			pageCheck();

			assertTrue(
					verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[@data-eventlabel='" + subMenu + "']")),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();

		} else {
			clickElementBy(hamburgerCTA);
			waitForSometime("1");
			clickElementBy(By
					.xpath("//li[contains(@role,'treeitem')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
							+ headerMenuItem + "')]/parent::a/following-sibling::button"));
			waitForSometime("1");

			assertTrue(
					getList(By
							.xpath("//div[contains(@class,'mobileNav')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
									+ headerMenuItem + "')]/../..//ul/li")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By
									.xpath("//div[contains(@class,'mobileNav')]//a[contains(@class,'dropMenuItem')]/span[contains(text(),'"
											+ headerMenuItem + "')]/../..//ul/li")).size());

			assertTrue(
					verifyObjectDisplayed(
							By.xpath("//li[contains(@role,'treeitem')]//a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");
			if (subMenu.equals("Vacation Ready")) {
				href = "https://clubwyndham.wyndhamdestinations.com/us/en/resorts/vacation-ready";
			} else {
				href = getElementAttribute(
						By.xpath("//li[contains(@role,'treeitem')]//a[contains(.,'" + subMenu + "')]"), "href");
			}
			clickElementJSWithWait(By.xpath("//li[contains(@role,'treeitem')]//a[contains(.,'" + subMenu + "')]"));
			pageCheck();
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), subMenu + " navigations not successful");
			tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");
			navigateBack();
			pageCheck();

		}

	}

	@Override
	public void validateRegisterLink() {
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(registerLink), "Register Link is not Displayed");
		tcConfig.updateTestReporter("MGVCHomePage", "validateRegisterLink", Status.INFO, "Register Link is displayed");
		String href = getElementAttribute(registerLink, "href");
		clickElementBy(registerLink);
		pageCheck();
		assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Page navigation is not Successfull");
		tcConfig.updateTestReporter("MGVCHomePage", "validateRegisterLink", Status.INFO, "Page is Displayed");
		navigateToURL(url);
		driver.get(url);
		pageCheck();
	}

	@Override
	public void validateSignInLink() {
		clickElementBy(hamburgerCTA);
		assertTrue(verifyObjectDisplayed(signInLink), "Sign In Link is not Displayed");
		tcConfig.updateTestReporter("MGVCHomePage", "validateSignInLink", Status.INFO, "SignIn Link is displayed");
		String href = getElementAttribute(signInLink, "href");
		clickElementBy(signInLink);
		pageCheck();
		assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Page navigation is not Successfull");
		tcConfig.updateTestReporter("MGVCHomePage", "validateSignInLink", Status.INFO, "Page is Displayed");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href");
		String title = getElementAttribute(link, "data-eventlabel");
		clickElementBy(link);
		pageCheck();
		assertTrue(getCurrentURL().trim().equalsIgnoreCase(href),
				"Link Navigation not correct, expected : " + href + " | Actual : " + getCurrentURL().trim());
		tcConfig.updateTestReporter("TimeshareHomePage", "navigateValidateAndReturn", Status.PASS,
				"Latest News Title : " + title + " navigating to link : " + href + " successful");

		navigateBack();
		pageCheck();

	}

	@Override
	public void validateFooterMenu() {

		waitUntilElementVisibleBy(driver, footerAboutUs, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(footerAboutUs), "Help menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS, "Help menu item is present");
		navigateValidateAndReturn(footerAboutUs);

		
		/*waitUntilElementVisibleBy(driver, footerCareers, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(footerCareers), "Help menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS, "Help menu item is present");
		navigateValidateAndReturntoPage(footerCareers, "tnlcareers");*/
		 

	}

	@Override
	public void navigateValidateAndReturntoPage(By link, String menu) {

		String href = getElementAttribute(link, "href").trim();
		clickElementBy(link);
		// pageCheck();
		waitForSometime("3");
		String title = driver.getCurrentUrl().trim();
		assertTrue(title.contains(menu), "Link Navigation not correct, expected : " + href + " | Actual : " + title);
		tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
				"Page: " + title + " navigating to link : " + href + " successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validateInstagramLink() {

		assertTrue(verifyObjectDisplayed(instaIcon), "Instagram icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateInstagramLink", Status.PASS, "Instagram icon is present");
		clickElementBy(instaIcon);
		waitForSometime("3");
		assertTrue(driver.getCurrentUrl().contains("https://www.instagram.com/"),
				"Instagram link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateInstagramLink", Status.PASS,
				"Instagram link navigation successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validateTwitterLink() {

		getElementInView(twitterIcon);
		assertTrue(verifyObjectDisplayed(twitterIcon), "Twitter icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateTwitterLink", Status.PASS, "Twitter icon is present");
		clickElementBy(twitterIcon);
		waitForSometime("3");
		assertTrue(driver.getCurrentUrl().contains("https://mobile.twitter.com/"),
				"Twitter link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateTwitterLink", Status.PASS,
				"Twitter link navigation successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validatePinterestLink() {

		getElementInView(pinterestIcon);
		assertTrue(verifyObjectDisplayed(pinterestIcon), "Pinterest icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePinterestLink", Status.PASS, "Pinterest icon is present");
		clickElementBy(pinterestIcon);
		waitForSometime("3");
		assertTrue(getCurrentURL().contains("https://www.pinterest.com/"), "Pinterest link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePinterestLink", Status.PASS,
				"Pinterest link navigation successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validateFacebookLink() {

		getElementInView(facebookIcon);
		assertTrue(verifyObjectDisplayed(facebookIcon), "Facebook icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFacebookLink", Status.PASS, "Facebook icon is present");
		clickElementBy(facebookIcon);
		waitForSometime("3");
		assertTrue(getCurrentURL().contains("https://m.facebook.com/"), "Facebook link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFacebookLink", Status.PASS,
				"Facebook link navigation successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validateTermsofUse() {

		assertTrue(verifyObjectDisplayed(termsOfUse), "Terms of Use link is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateTermsofUse", Status.PASS, "Terms of Use link is present");
		clickElementBy(termsOfUse);
		waitForSometime("3");
		assertTrue(driver.getCurrentUrl().contains(/*url+"us/en/terms-of-use"*/"https://www.travelandleisureco.com/us/en/terms-of-use"),
				"Terms of Use link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateInstagramLink", Status.PASS,
				"Terms of Use link navigation successful");

		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validatePrivacyNotice() {

		assertTrue(verifyObjectDisplayed(privacyNotice), "Privacy notice link is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePrivacyNotice", Status.PASS,
				"Privacy notice link is present");
		clickElementBy(privacyNotice);
		waitForSometime("3");
		assertTrue(driver.getCurrentUrl().contains(/*url+"us/en/privacy-notice"*/"https://www.travelandleisureco.com/us/en/privacy-notice"),
				"Privacy Notice link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateInstagramLink", Status.PASS,
				"Privacy Notice link navigation successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validatePrivacySettings() {

		assertTrue(verifyObjectDisplayed(privacySettings), "Privacy settings link is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePrivacySettings", Status.PASS,
				"Privacy settings link is present");
		clickElementBy(privacySettings);
		waitForSometime("3");
		assertTrue(driver.getCurrentUrl().contains("https://info.evidon.com/pub_info/2073?v=1&nt=1&nw=true"),
				"Privacy Settings link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateInstagramLink", Status.PASS,
				"Privacy Settings link navigation successful");
		navigateToURL(url);
		driver.get(url);
		pageCheck();

	}

	@Override
	public void validateSearchFunctionality() {
		String searchValue = testData.get("strSearch");
		clickElementBy(hamburgerCTA);
		waitUntilElementVisibleBy(driver, searchContainer, "ExplicitLongWait");
		fieldDataEnter(searchContainer, searchValue);
		clickElementBy(searchIcon);
		pageCheck();
		assertTrue(verifyObjectDisplayed(searchTitle), "Search Page is not Loaded");

	}

	@Override
	public void searchResultsValidatins() {

		waitUntilElementVisibleBy(driver, searchTitle, 120);

		if (verifyObjectDisplayed(searchTitle)) {
			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");

			String strResultsFor = getElementText(resultNumber);

			String strResults = strResultsFor.split(" ")[0];

			tcConfig.updateTestReporter("MGVCHomePage", "searchFunctionality", Status.PASS,
					"Total Results: " + strResults + " for" + strResultsFor);

			List<WebElement> listOfTotalResults = driver.findElements(resultList);
			List<WebElement> listFirstResults = driver.findElements(firstPageList);
			List<WebElement> listCTA = driver.findElements(searchListLink);
			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"No Of Results on first page " + listFirstResults.size());

			if (listOfTotalResults.size() >= 10) {
				if (verifyObjectDisplayed(moreResultsLink)) {
					tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
							"More Results Available on the page");
				}
			} else {
				System.out.println("Less than 10 results hence no More Results link");
			}

			String firstTitle = listFirstResults.get(0).getText();
			String href = getElementAttribute(listCTA.get(0), "href").trim().replace(".html", "");

			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"First Result title " + firstTitle);

			clickElementBy(listFirstResults.get(0));
			pageCheck();
			assertTrue(getCurrentURL().trim().contains(href), "Navigations not successfull");

			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to first Search related page " + firstTitle);

			driver.navigate().back();

			pageCheck();

		} else {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

	@Override
	public void formValidations() {

		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			scrollDownForElementJSBy(formTitle);
			tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formDescription)) {
				tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
						"Form SubTitle present with header " + getElementText(formDescription));

				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"First Name Field Present");
					sendKeysByScriptExecutor((RemoteWebDriver) driver, "First Name *", testData.get("firstName"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"Last Name Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Last Name *", testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS, "Phone Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Phone Number *", testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS, "Email Field Present");

					sendKeysByScriptExecutor((RemoteWebDriver) driver, "Email Address *", testData.get("email"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(stateSelect)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"State Drop Down present");
					new Select(driver.findElement(stateSelect)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS, "T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(submitButton)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"Submit button present");

					if (verifyObjectDisplayed(submitButton))
						clickElementJSWithWait(submitButton);

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Submit Button not present");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, thankConfirm, 120);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"Form Submission Successful");

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	public void validateBanners() {

		assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
		String bannerTitle = getElementText(bannerTitleObj);
		String bannerSubTitle = getElementText(bannerSubTitleObj);
		assertTrue(!bannerTitle.trim().equals(""), "Banner title absent");
		assertTrue(!bannerSubTitle.trim().equals(""), "Banner Sub title absent");
		assertTrue(verifyObjectDisplayed(learnMoreBtn), "Pick Your Paradise button is absent");
		String linkdetails = getElementAttribute(learnMoreBtn, "href");
		clickElementJSWithWait(learnMoreBtn);
		pageCheck();
		String getUrl = driver.getCurrentUrl().trim();
		assertTrue(getUrl.equalsIgnoreCase(linkdetails), "Navigation to Pick your paradise link not correct");
		tcConfig.updateTestReporter("MGVCHomePage", "validateBanners", Status.PASS,
				"Banner Title : " + bannerTitle + " navigating to link : " + linkdetails + " successful");
		navigateBack();
		pageCheck();

	}

	@Override
	public void validateCardComponent() {

		List<WebElement> cardList = getList(cardComponent);
		for (int i = 0; i < cardList.size(); i++) {
			getElementInView(cardComponent);

			cardList = getList(cardComponent);

			assertTrue(cardList.get(i).findElement(By.xpath("./a")).isDisplayed(), "cards link not present");
			String hrefCard = cardList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(cardList.get(i).findElement(By.xpath("./div/a")).isDisplayed(), "pictures link not present");
			String hrefPicture = cardList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Card section link not present");
			String hrefCardSection = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Cards");

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.isDisplayed(), "card section title not present");
			String cardTitle = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateCardComponent", Status.PASS,
					"All links are in sync within the Card, which is : " + hrefCard);
			String strHref = cardList.get(i).findElement(By.xpath("./div/div[@class='card-section']/a[2]"))
					.getAttribute("href").trim();
			WebElement eleLearnMore = cardList.get(i).findElement(By.xpath("./div/div[@class='card-section']/a[2]"));
			getElementInView(eleLearnMore);
			clickElementJSWithWait(eleLearnMore);
			pageCheck();

			String title = driver.getCurrentUrl().trim();
			assertTrue(title.toLowerCase().contains(strHref.toLowerCase()),
					"Link Navigation not correct, expected : " + title + " | Actual : " + strHref);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + cardTitle + " navigating to link : " + title + " successful");
			navigateToURL(url);
			driver.get(url);
			pageCheck();

		}
	}
	
	@Override
	public void validateFeaturedResort() {

		getElementInView(featuredResortImage);

		assertTrue(verifyObjectDisplayed(featuredResortImage), "Featured Resort image is absent");
		assertTrue(verifyObjectDisplayed(featuredResortTitle), "Featured Resort title is absent");
		assertTrue(getElementText(featuredResortTitle).trim().length() > 0, "Featured Resort title is blank");
		String title = getElementText(featuredResortTitle).trim();

		assertTrue(verifyObjectDisplayed(featuredResortBody), "Featured Resort body is absent");
		assertTrue(getElementText(featuredResortBody).trim().length() > 0, "Featured Resort body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorefeaturedResort), "Featured Resort button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedResort", Status.PASS,
				"Featured Resort content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorefeaturedResort, "href");
		clickElementBy(btnLearnMorefeaturedResort);
		pageCheck();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedResort", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}
	
	@Override
	public void validateWelcomeToTheClub() {

		getElementInView(welcomeToTheClubImage);

		assertTrue(verifyObjectDisplayed(welcomeToTheClubImage), "Welcome To the Club image is absent");
		assertTrue(verifyObjectDisplayed(welcomeToTheClubTitle), "Welcome To the Club title is absent");
		assertTrue(getElementText(welcomeToTheClubTitle).trim().length() > 0, "Welcome To the Club title is blank");
		String title = getElementText(welcomeToTheClubTitle).trim();

		assertTrue(verifyObjectDisplayed(welcomeToTheClubBody), "Welcome To the Club body is absent");
		assertTrue(getElementText(welcomeToTheClubBody).trim().length() > 0, "Welcome To the Club body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorewelcomeToTheClub), "Welcome To the Club button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateWelcomeToTheClub", Status.PASS,
				"Welcome To the Club content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorewelcomeToTheClub, "href");
		clickElementBy(btnLearnMorewelcomeToTheClub);
		pageCheck();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateWelcomeToTheClub", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	@Override
	public void validateBlissfulBenefits() {

		getElementInView(blissfulBenefitsImage);

		assertTrue(verifyObjectDisplayed(blissfulBenefitsImage), "Blissful Benefits image is absent");
		assertTrue(verifyObjectDisplayed(blissfulBenefitsTitle), "Blissful Benefits title is absent");
		assertTrue(getElementText(blissfulBenefitsTitle).trim().length() > 0, "Blissful Benefits title is blank");
		String title = getElementText(blissfulBenefitsTitle).trim();

		assertTrue(verifyObjectDisplayed(blissfulBenefitsBody), "Blissful Benefits body is absent");
		assertTrue(getElementText(blissfulBenefitsBody).trim().length() > 0, "Blissful Benefits body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreblissfulBenefits), "Blissful Benefits button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateBlissfulBenefits", Status.PASS,
				"Blissful Benefits content is displayed Successfully");

		String href = getElementAttribute(btnLearnMoreblissfulBenefits, "href");
		clickElementBy(btnLearnMoreblissfulBenefits);
		pageCheck();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateBlissfulBenefits", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}
	
	@Override
	public void validateGuidesInspirations() {
		getElementInView(guideInspirationTitle);
		assertTrue(verifyObjectDisplayed(guideInspirationTitle), "Guides and Inspiration Title is absent");
		assertTrue(verifyObjectDisplayed(guideInspirationDescription), "Guides and Inspiration Description is absent");
		assertTrue(verifyObjectDisplayed(seeAllGuideCTA), "Guides and Inspiration CTA is absent");
		String href = getElementAttribute(seeAllGuideCTA, "href");
		clickElementBy(seeAllGuideCTA);
		pageCheck();
		getElementInView(breadcrumbLink);
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().equals(href),
				"Navigation to See All Guides link not correct");

	}
	
	@Override
	public void validateFeaturedGuide() {

		getElementInView(featureGuideImage);

		assertTrue(verifyObjectDisplayed(featureGuideImage), "Featured Guide image is absent");
		assertTrue(verifyObjectDisplayed(featureGuideTitle), "Featured Guide title is absent");
		assertTrue(getElementText(featureGuideTitle).trim().length() > 0, "Featured Guide title is blank");
		String title = getElementText(featureGuideTitle).trim();

		assertTrue(verifyObjectDisplayed(featureGuideBody), "Featured Guide body is absent");
		assertTrue(getElementText(featureGuideBody).trim().length() > 0, "Featured Guide body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorefeatureGuide), "Featured Guide button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedGuide", Status.PASS,
				"Featured Guide content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorefeatureGuide, "href");
		clickElementBy(btnLearnMorefeatureGuide);
		pageCheck();
		assertTrue(/*getElementAttribute(breadcrumbLink, "href")*/getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedGuide", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}


}
