package mgvc.pages;

import static org.testng.Assert.assertTrue;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public abstract class MGVCHomePage extends FunctionalComponents {

	protected By cookieAlert = By.xpath("//button[contains(@class,'setCookie') and contains(text(),'Got it')]");
	protected By margarittaVilleLogo = By.xpath("//a[img[contains(@alt,'img')]]");
	protected By brandLogoComponent = By
			.xpath("//div[contains(@class,'tlFooter')]//div[contains(@class,'logo-footer')]/a[img]");

	protected By discoverBenefits = By
			.xpath("//div[contains(@class,'MGVCHeader')]//li/a[contains(text(),'Discover Benefits')]");
	protected By resorts = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a[contains(text(),'Resorts')]");
	protected By ownerGuide = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a[contains(text(),'Owner Guide')]");
	protected By dealsOffers = By
			.xpath("//div[contains(@class,'MGVCHeader')]//li/a[contains(text(),'Deals & Offers')]");
	protected By help = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a[contains(text(),'Help')]");
	protected By COVID19Link = By.xpath("//div[contains(@class,'MGVCHeader')]//li/a[contains(text(),'COVID-19')]");

	protected By footerDiscoverBenefits = By
			.xpath("//div[contains(@class,'genericFooter')]//a[span[contains(text(),'Discover Benefits')]]");
	protected By footerResorts = By
			.xpath("//div[contains(@class,'genericFooter')]//a[span[contains(text(),'Resorts')]]");
	protected By footerOwnerGuide = By
			.xpath("//div[contains(@class,'genericFooter')]//a[span[contains(text(),'Owner Guide')]]");
	protected By footerDealsOffers = By
			.xpath("//div[contains(@class,'genericFooter')]//a[span[contains(text(),'Deals & Offers')]]");
	protected By footerHelp = By.xpath("//div[contains(@class,'genericFooter')]//a[span[contains(text(),'Help')]]");
	protected By footerAboutUs = By.xpath("//div[contains(@class,'genericFooter')]//li/a[contains(text(),'About Us')]");
	protected By footerCareers = By.xpath("//div[contains(@class,'genericFooter')]//li/a[contains(text(),'Careers')]");
	protected By disclaimerText = By.xpath(
			"//div[contains(@class,'disclaimer')and contains(text(),'THIS ADVERTISING MATERIAL IS BEING USED FOR THE PURPOSE OF SOLICITING THE SALE OF TIME-SHARE PROPERTY OR INTERESTS IN TIME-SHARE PROPERTY.')]");

	protected By slickDots = By.xpath("//ul[@class='slick-dots']/li");
	protected By bannerImgSrc = By.xpath("//div[@class='image-quote-banner']//img");
	protected By bannerTitleObj = By.xpath("//div[@class='image-quote-banner']//div[contains(@class,'title')]");
	protected By bannerSubTitleObj = By.xpath("//div[@class='image-quote-banner']//div[contains(@class,'body')]");
	protected By learnMoreBtn = By.xpath("//div[@class='image-quote-banner']//a[text()='Pick Your Paradise']");
	protected By breadcrumbLink = By.xpath("//ul/li[last()]/a[@data-eventname='breadCrumb']");
	protected By exploringTimeshareHeader = By.xpath("//div[text()='Exploring Timeshare Ownership']");
	protected By cardComponent = By.xpath("//div[@class='cardComponent']//div[@class='cell']/div/div[a]");

	protected By welcomeToTheClubImage = By.xpath(
			"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[@class='cardBanner']//img)[1]");
	protected By welcomeToTheClubTitle = By.xpath(
			"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[1]");
	protected By welcomeToTheClubBody = By.xpath(
			"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[1]");
	protected By btnLearnMorewelcomeToTheClub = By.xpath(
			"(//div[contains(.,'Get A Wave From It All')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[1]");

	protected By featureGuideImage = By
			.xpath("(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[@class='cardBanner']//img)[1]");
	protected By featureGuideTitle = By.xpath(
			"(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[1]");
	protected By featureGuideBody = By.xpath(
			"(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[1]");
	protected By btnLearnMorefeatureGuide = By.xpath(
			"(//div[contains(.,'Play In Nashville')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[1]");

	protected By notanOwnerImage = By.xpath(
			"(//div[contains(.,'Get Suite Deals On Paradise')]/div[@gtm_component]//div[@class='cardBanner']//img)[1]");
	protected By notanOwnerTitle = By.xpath(
			"(//div[contains(.,'Get Suite Deals On Paradise')]/div[@gtm_component]//div[@class='cardBanner']//div[@class='title-1'])[1]");
	protected By notanOwnerBody = By.xpath(
			"(//div[contains(.,'Get Suite Deals On Paradise')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/div[contains(@class,'body')])[1]");
	protected By btnLearnMorenotanOwner = By.xpath(
			"(//div[contains(.,'Get Suite Deals On Paradise')]/div[@gtm_component]//div[contains(@class,'bannerCard')]/a)[1]");

	protected By blissfulBenefitsImage = By.xpath(
			"//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]/../../../..//img");
	protected By blissfulBenefitsTitle = By.xpath(
			"//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]");
	protected By blissfulBenefitsBody = By.xpath(
			"//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]/../div[@class='body-1']");
	protected By btnLearnMoreblissfulBenefits = By.xpath(
			"//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Feel Fine On Cloud Nine')]/../a");

	protected By featuredResortImage = By.xpath(
			"(//div[contains(.,'Margaritaville Vacation Club by Wyndham - Nashville')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//img)[1]");
	protected By featuredResortTitle = By.xpath(
			"//div[@class='show-for-large']//div[contains(@class,'title-1') and contains(text(),'Margaritaville Vacation Club by Wyndham - Nashville')]");
	protected By featuredResortBody = By.xpath(
			"(//div[contains(.,'Margaritaville Vacation Club by Wyndham - Nashville')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//div[contains(@class,'body')])[1]");
	protected By btnLearnMorefeaturedResort = By.xpath(
			"(//div[contains(.,'Margaritaville Vacation Club by Wyndham - Nashville')]/div[@gtm_component]//div[contains(@class,'blockBannerLeft')]//a)[1]");

	protected By guideInspirationTitle = By.xpath("//div[contains(text(),'GUIDES & INSPIRATION')]");
	protected By guideInspirationDescription = By
			.xpath("//div[contains(text(),'GUIDES & INSPIRATION')]/../div[contains(@class,'body-1')]");
	protected By seeAllGuideCTA = By.xpath("//div[contains(text(),'GUIDES & INSPIRATION')]/../../../../..//a");

	protected By keystoVacationComponent = By.xpath(
			"//div[contains(.,'Keys To A Great Timeshare Vacation')]/div[@class='cardComponent']//div[@class='cell']/div/div[a]");
	protected By latestNewsHeader = By.xpath("//div[@class='contentSlice' and contains(.,'Latest News')]");
	protected By latestNewsComponent = By.xpath(
			"//div[contains(.,'Latest News')]/div[@class='indexCardComponent']//div[@class='cell']/div/div[a and @data-ishidden='false']");
	protected By careerFooter = By.xpath("(//div[contains(@class,'tlFooter')]//a[span[text()='Careers']])[1]");
	protected By contactUsFooter = By.xpath("(//div[contains(@class,'tlFooter')]//a[span[text()='Contact Us']])[1]");
	protected By investorsFooter = By.xpath("(//div[contains(@class,'tlFooter')]//a[span[text()='Investors']])[1]");
	protected By newsFooter = By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='News']]");
	protected By socialResponsibilityFooter = By
			.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='Social Responsibility']]");
	protected By ourBrandsFooter = By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='Our Brands']]");
	protected By OurCompanyFooter = By.xpath("//div[contains(@class,'tlFooter')]//a[span[text()='Our Company']]");

	protected By facebookPageObj = By.xpath("(//a[contains(@href,'www.facebook.com/travelleisureco')])[1]");

	protected By twitterIcon = By
			.xpath("//div[contains(@class,'genericFooter')]//li/a[contains(@href,'https://twitter.com/MargVilleVC')]");
	protected By twitterPageObj = By.xpath("(//span[text()='Travel + Leisure Co.'])[1]");
	protected By pinterestIcon = By.xpath(
			"//div[contains(@class,'genericFooter')]//li/a[contains(@href,'https://www.pinterest.com/MvilleVC')]");

	protected By linkedInIcon = By.xpath(
			"(//div[contains(@class,'tlFooter')]//li/a[contains(@href,'https://www.linkedin.com/company/travelleisureco')])[1]");
	protected By facebookIcon = By.xpath(
			"//div[contains(@class,'genericFooter')]//li/a[contains(@href,'https://www.facebook.com/MargaritavilleVacationClub')]");

	protected By instaIcon = By.xpath(
			"//div[contains(@class,'genericFooter')]//li/a[contains(@href,'https://www.instagram.com/margaritavillevc/')]");
	protected By instaPageObj = By.xpath("//a[@page_id='profilePage' and text()='www.timeshare.com']");

	protected By youtubeIcon = By
			.xpath("//li/a[contains(@href,'https://www.youtube.com/channel/UCwy9UWOWSLAypQmetVS95Cw')]");
	protected By youtubePageObj = By.xpath("//div[@id='text-container' and contains(.,'Timeshare Info')]");
	protected By footerWyndhamLogo = By.xpath("//div[contains(@class,'footerFooter')]//a[img[@alt='Wyndham Logo']]");
	protected By footerCopyRight = By.xpath("//div[contains(text(),'©2021 Wyndham Destinations.')]");
	protected By privacyNotice = By.xpath("//ul[@class='menu']//a[@data-eventlabel='Privacy Notice']");
	protected By termsOfUse = By.xpath("//ul[@class='menu']//a[@data-eventlabel='Terms of Use']");
	protected By privacySettings = By.xpath("//ul[@class='menu']//a[@data-eventlabel='Privacy Settings']");
	protected By supportTimeshare = By.xpath(
			"(//ul[@class='quickLinks menu float-right']//a[@data-eventlabel='Proudly Supports Timeshare.com'])[1]");

	protected By doNotsellInfo = By.xpath("//a[text()='Do Not Sell My Personal Information']");
	protected By optOutForm = By.xpath("//div[text()='Opt Out Request Form']");
	protected By closeAlert = By.xpath("//button[contains(text(),'Close')]");
	protected By registerLink = By.xpath("(//a[contains(text(),'Register')])[1]");
	protected By signInLink = By.xpath("(//a[contains(text(),'Sign In')])[1]");

	protected By formTitle = By.xpath(
			"//form[@id='MGVCInterestedOwnership']//div[contains(@class,'formTitle')]|//form[@id='mgvc-interested-ownership']//div[contains(@class,'formTitle')]");
	protected By formDescription = By.xpath(
			"//form[@id='MGVCInterestedOwnership']//div[contains(@class,'formTitle')]/../../..//div[contains(@class,'body-1') and contains(text(),'Please take a few moments')]|//form[@id='mgvc-interested-ownership']//div[contains(@class,'formTitle')]/../../..//div[contains(@class,'body-1') and contains(text(),'Please take a few moments')]");
	protected By firstNameField = By.xpath("//input[@id='firstName']//following-sibling::span");
	protected By lastNameField = By.xpath("//input[@id='lastName']//following-sibling::span");
	protected By phoneField = By.xpath(/* "//input[@id='PhoneNumber']//following-sibling::span" */
			"//input[contains(@id,'phoneNumber')]//following-sibling::span");
	protected By emailField = By.xpath("//input[@id='email']//following-sibling::span");
	protected By memberNumField = By.xpath("//input[@id='membershipNumber']//following-sibling::span");
	protected By checkBox = By.xpath("//form//input[@type='checkbox']");
	protected By submitButton = By.xpath("//form//button[contains(.,'Submit')]");
	protected By stateSelect = By.xpath("//select[@name='state']");
	protected By thankConfirm = By.xpath("//div[@class='title-2' and contains(.,'Thank you')]");

	protected By searchContainer = By.xpath("//input[@type='search']");
	protected By searchIcon = By.xpath("//div[contains(text(),'Search')]");
	protected By searchTitle = By.xpath("//div[contains(@class,'search-title')]");
	protected By resultNumber = By.xpath("//div[contains(@class,'resultNumber')]");
	protected By resultList = By.xpath("//ul[contains(@class,'searchResults')]/li//div[@class='searchItemTitle']");
	protected By paginationTab = By.xpath("//ul[contains(@class,'pagination')]");
	protected By paginationNext = By.xpath("//li[@class='pagination-next']");
	protected By firstPageList = By.xpath(
			"//ul[contains(@class,'searchResults')]/li[@class='searchItem page-1']//div[@class='searchItemTitle']");
	protected By searchListLink = By.xpath(
			"//ul[contains(@class,'searchResults')]/li[@class='searchItem page-1']//div[@class='searchItemTitle']/..");

	protected String url;

	public MGVCHomePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	public void launchApplication() {

		url = tcConfig.getTestData().get("URL");
		driver.get(url);
		pageCheck();
		if (verifyObjectDisplayed(closeAlert)) {
			clickElementBy(closeAlert);
		}
		if (verifyObjectDisplayed(cookieAlert)) {
			clickElementBy(cookieAlert);
		}

		tcConfig.updateTestReporter("MGVCHomePage", "launchApplication", Status.PASS,
				"MGVC URL was launched successfully");
	}

	public void validateSearchFunctionality() {
		String searchValue = testData.get("strSearch");
		assertTrue(verifyObjectDisplayed(searchIcon), "Search Icon is not Displayed");
		clickElementBy(searchIcon);
		waitUntilElementVisibleBy(driver, searchContainer, "ExplicitLongWait");
		fieldDataEnter(searchContainer, searchValue);
		clickElementBy(searchIcon);
		pageCheck();
		assertTrue(verifyObjectDisplayed(searchTitle), "Search Page is not Loaded");

	}

	public void searchResultsValidatins() {

		waitUntilElementVisibleBy(driver, searchTitle, 120);

		if (verifyObjectDisplayed(searchTitle)) {
			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");

			String strResultsFor = getElementText(resultNumber);

			String strResults = strResultsFor.split(" ")[0];

			tcConfig.updateTestReporter("MGVCHomePage", "searchFunctionality", Status.PASS,
					"Total Results: " + strResults + " for" + strResultsFor);

			List<WebElement> listOfTotalResults = driver.findElements(resultList);
			List<WebElement> listFirstResults = driver.findElements(firstPageList);
			List<WebElement> listCTA = driver.findElements(searchListLink);
			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"No Of Results on first page " + listFirstResults.size());

			if (listOfTotalResults.size() >= 10) {
				if (verifyObjectDisplayed(paginationTab)) {
					tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
							"Pagination Available on the page");
				}
			} else {
				System.out.println("Less than 10 results hence no pagination");
			}

			String firstTitle = listFirstResults.get(0).getText();
			String href = getElementAttribute(listCTA.get(0), "href").trim().replace(".html", "");

			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"First Result title " + firstTitle);

			clickElementBy(listFirstResults.get(0));
			pageCheck();
			assertTrue(getCurrentURL().trim().contains(href), "Navigations not successfull");

			tcConfig.updateTestReporter("MGVCHomePage", "searchResultsValidatins", Status.PASS,
					"Navigated to first Search related page " + firstTitle);

			driver.navigate().back();

			pageCheck();

		} else {
			tcConfig.updateTestReporter("NextGenSearchResultPage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

	protected By margarittaVilleLogoIOS = By.xpath("//a[img[contains(@alt,'Margaritaville Logo')]]");

	public void validateLogo() {
		/*try {
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
				assertTrue(verifyObjectDisplayed(margarittaVilleLogoIOS), "Header MGVC logo is not present");
				tcConfig.updateTestReporter("MGVCHomePage", "validateLogo", Status.PASS,
						"Header MGVC logo is displayed");

				assertTrue(getElementAttribute(margarittaVilleLogoIOS, "href").equalsIgnoreCase(url + "us/en"),
						"Header MGVC logo url not correct");
				tcConfig.updateTestReporter("MGVCHomePage", "validateLogo", Status.PASS, "Header MGVC logo is present");
			} else {*/
				assertTrue(verifyObjectDisplayed(margarittaVilleLogo), "Header MGVC logo is not present");
				tcConfig.updateTestReporter("MGVCHomePage", "validateLogo", Status.PASS,
						"Header MGVC logo is displayed");

				assertTrue(getElementAttribute(margarittaVilleLogo, "href").equalsIgnoreCase(url + "us/en"),
						"Header MGVC logo url not correct");
				tcConfig.updateTestReporter("MGVCHomePage", "validateLogo", Status.PASS, "Header MGVC logo is present");
			/*}
		} catch (Exception e) {
			log.info("No logo Present");
		}*/

	}

	public void validateRegisterLink() {
		assertTrue(verifyObjectDisplayed(registerLink), "Register Link is not Displayed");
		tcConfig.updateTestReporter("MGVCHomePage", "validateRegisterLink", Status.INFO, "Register Link is displayed");
		String href = getElementAttribute(registerLink, "href");
		clickElementBy(registerLink);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Page navigation is not Successfull");
			tcConfig.updateTestReporter("MGVCHomePage", "validateRegisterLink", Status.INFO, "Page is Displayed");
			navigateToMainTab(allTabs);
		} else {
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Page navigation is not Successfull");
			tcConfig.updateTestReporter("MGVCHomePage", "validateRegisterLink", Status.INFO, "Page is Displayed");
			navigateBack();
			pageCheck();
		}

	}

	public void validateSignInLink() {
		assertTrue(verifyObjectDisplayed(signInLink), "Sign In Link is not Displayed");
		tcConfig.updateTestReporter("MGVCHomePage", "validateSignInLink", Status.INFO, "SignIn Link is displayed");
		String href = getElementAttribute(signInLink, "href");
		clickElementBy(signInLink);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Page navigation is not Successfull");
			tcConfig.updateTestReporter("MGVCHomePage", "validateSignInLink", Status.INFO, "Page is Displayed");
			navigateToMainTab(allTabs);
		} else {
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href), "Page navigation is not Successfull");
			tcConfig.updateTestReporter("MGVCHomePage", "validateSignInLink", Status.INFO, "Page is Displayed");
			navigateBack();
			pageCheck();
		}
	}

	public void validateBrandLogo() {
		getElementInView(brandLogoComponent);
		assertTrue(verifyObjectDisplayed(brandLogoComponent), "Brand logos are not Displayed");
		List<WebElement> brandLogos = getList(brandLogoComponent);
		for (int i = 1; i < brandLogos.size(); i++) {
			brandLogos = getList(brandLogoComponent);
			assertTrue(brandLogos.get(i).isDisplayed(), "Brand Logo is not Displayed");
			String href = brandLogos.get(i).getAttribute("href").trim();
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBrandLogo", Status.PASS,
					"Brand logo is present for " + href);
			if (i == 1) {
				href = "https://www.wyndhamdestinations.com/";
			} else if (i == 2) {
				href = "https://www.panoramaco.com/";
			} else if (i == 3) {
				href = "https://stage.travelandleisureco.com/us/en/our-brands/travel-leisure-group";
			}

			clickElementBy(brandLogos.get(i));
			pageCheck();
			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				String getCurrentURL = driver.getCurrentUrl().trim();
				assertTrue(href.contains(getCurrentURL), "Link Navigation is not Correct");
				tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBrandLogo", Status.PASS,
						"Link navigated to correct page" + getCurrentURL);
				navigateToMainTab(allTabs);
				pageCheck();
			} else {
				String getCurrentURL = driver.getCurrentUrl().trim();
				assertTrue(href.contains(getCurrentURL), "Link Navigation is not Correct");
				tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateBrandLogo", Status.PASS,
						"Link navigated to correct page" + getCurrentURL);
				navigateBack();
				pageCheck();
			}

		}

	}

	public void validateHomeHeaderMenu() {

		assertTrue(verifyObjectDisplayed(discoverBenefits), "Discover Benefits menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Discover Benefits menu item is present");
		navigateValidateAndReturn(discoverBenefits);

		assertTrue(verifyObjectDisplayed(resorts), "Resorts menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Resorts menu item is present");
		navigateValidateAndReturn(resorts);

		assertTrue(verifyObjectDisplayed(ownerGuide), "Owner Guide menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Owner Guide menu item is present");
		navigateValidateAndReturn(ownerGuide);

		assertTrue(verifyObjectDisplayed(dealsOffers), "Deals and Offers menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Deals and Offers menu item is present");
		navigateValidateAndReturn(dealsOffers);

		assertTrue(verifyObjectDisplayed(help), "Help menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS, "Help menu item is present");
		navigateValidateAndReturn(help);

		assertTrue(verifyObjectDisplayed(COVID19Link), "Covid 19 menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderMenu", Status.PASS,
				"Covid 19 menu item is present");
		navigateValidateAndReturn(COVID19Link);
	}

	public void validateHomeHeaderSubMenu(String headerMenuItem, String subMenu, int count) {

		if (count == 0) {
			clickElementBy(By.xpath("//*[@class='menuPrimaryItems'][a[span[text()='" + headerMenuItem + "']]]"));
			pageCheck();

			assertTrue(verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();

		} else {
			waitForSometime("5");
			hoverOnElement(By
					.xpath("//div[contains(@class,'MGVCHeader')]//li[contains(@class,'dropdownItem')]/a[contains(text(),'"
							+ headerMenuItem + "')]"
			/*
			 * "//*[contains(@class,'submenu-item')][a[span[text()='"+
			 * headerMenuItem+"']]]"
			 */));
			waitForSometime("8");

			assertTrue(
					getList(By
							.xpath("//div[contains(@class,'MGVCHeader')]//li[contains(@class,'dropdownItem')]/a[contains(text(),'"
									+ headerMenuItem + "')]/../div[contains(@class,'dropdwonContent')]//ul/li"))
											.size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By
									.xpath("//div[contains(@class,'MGVCHeader')]//li[contains(@class,'dropdownItem')]/a[contains(text(),'"
											+ headerMenuItem + "')]/../div[contains(@class,'dropdwonContent')]//ul/li"))
													.size());

			assertTrue(
					verifyObjectDisplayed(By.xpath(
							"//div[contains(@class,'dropdwonContent')]//ul/li/a[contains(text(),'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("TimeshareHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			clickElementJSWithWait(
					By.xpath("//div[contains(@class,'dropdwonContent')]//ul/li/a[contains(text(),'" + subMenu + "')]"));

			pageCheck();
			if (subMenu.equalsIgnoreCase("Resort News")) {
				subMenu = "News";
			} else if (subMenu.equalsIgnoreCase("Ask & Answer")) {
				subMenu = "Ask and Answer";
			}

			if (subMenu.equalsIgnoreCase("Vacation Ready")) {
				assertTrue(
						getCurrentURL().trim().equalsIgnoreCase(
								"https://clubwyndham.wyndhamdestinations.com/us/en/resorts/vacation-ready"),
						"Vacation Ready page not navigated");
				tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						subMenu + " navigations successful");
			} else {
				getElementInView(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']"));
				assertTrue(verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
						subMenu + " navigations not successful");

				tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderSubMenu", Status.PASS,
						subMenu + " navigations successful");
			}

			navigateBack();
			pageCheck();

		}

	}

	public void formValidations() {

		getElementInView(formTitle);
		waitUntilElementVisibleBy(driver, formTitle, 120);

		if (verifyObjectDisplayed(formTitle)) {
			scrollDownForElementJSBy(formTitle);
			tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
					"Form Title present with header " + getElementText(formTitle));
			if (verifyObjectDisplayed(formDescription)) {
				tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
						"Form SubTitle present with header " + getElementText(formDescription));

				if (verifyObjectDisplayed(firstNameField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"First Name Field Present");
					fieldDataEnterAction(firstNameField, testData.get("firstName"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"First Name Field not present");
				}

				if (verifyObjectDisplayed(lastNameField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"Last Name Field Present");

					fieldDataEnterAction(lastNameField, testData.get("lastName"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Last Name Field not present");
				}

				if (verifyObjectDisplayed(phoneField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS, "Phone Field Present");

					fieldDataEnterAction(phoneField, testData.get("phoneNo"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Phone Field not present");
				}

				if (verifyObjectDisplayed(emailField)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS, "Email Field Present");

					fieldDataEnterAction(emailField, testData.get("email"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Email Field not present");
				}

				if (verifyObjectDisplayed(stateSelect)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"State Drop Down present");
					new Select(driver.findElement(stateSelect)).selectByVisibleText(testData.get("stateSel"));

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"State Drop Down not present");
				}

				if (verifyObjectDisplayed(checkBox)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS, "T&C checkbox present");

					clickElementJSWithWait(checkBox);

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"T&C checkbox not present");
				}

				if (verifyObjectDisplayed(submitButton)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"Submit button present");

					if (verifyObjectDisplayed(submitButton))
						clickElementJSWithWait(submitButton);

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Submit Button not present");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				waitUntilElementVisibleBy(driver, thankConfirm, 120);

				if (verifyObjectDisplayed(thankConfirm)) {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.PASS,
							"Form Submission Successful");

				} else {
					tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
							"Form Submission not Successful");
				}

			} else {
				tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
						"Form SubTitle not present with header ");
			}

		} else {
			tcConfig.updateTestReporter("MGVCHomePage", "formValidations", Status.FAIL,
					"Form Title not present with header ");
		}

	}

	public void validateBanners() {

		assertTrue(getElementAttribute(bannerImgSrc, "src").contains(".jpg"), "Image source not present");
		String bannerTitle = getElementText(bannerTitleObj);
		String bannerSubTitle = getElementText(bannerSubTitleObj);
		assertTrue(!bannerTitle.trim().equals(""), "Banner title absent");
		assertTrue(!bannerSubTitle.trim().equals(""), "Banner Sub title absent");
		assertTrue(verifyObjectDisplayed(learnMoreBtn), "Pick Your Paradise button is absent");
		String linkdetails = getElementAttribute(learnMoreBtn, "href");
		clickElementJSWithWait(learnMoreBtn);
		pageCheck();
		String getUrl = driver.getCurrentUrl().trim();
		assertTrue(getUrl.equalsIgnoreCase(linkdetails), "Navigation to Pick your paradise link not correct");
		tcConfig.updateTestReporter("MGVCHomePage", "validateBanners", Status.PASS,
				"Banner Title : " + bannerTitle + " navigating to link : " + linkdetails + " successful");
		navigateBack();
		pageCheck();

	}

	public void validateGuidesInspirations() {
		getElementInView(guideInspirationTitle);
		assertTrue(verifyObjectDisplayed(guideInspirationTitle), "Guides and Inspiration Title is absent");
		assertTrue(verifyObjectDisplayed(guideInspirationDescription), "Guides and Inspiration Description is absent");
		assertTrue(verifyObjectDisplayed(seeAllGuideCTA), "Guides and Inspiration CTA is absent");
		String href = getElementAttribute(seeAllGuideCTA, "href");
		clickElementBy(seeAllGuideCTA);
		pageCheck();
		getElementInView(breadcrumbLink);
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().equals(href),
				"Navigation to See All Guides link not correct");

	}

	public void validateCardComponent() {

		List<WebElement> cardList = getList(cardComponent);
		for (int i = 0; i < cardList.size(); i++) {
			getElementInView(cardComponent);

			cardList = getList(cardComponent);

			assertTrue(cardList.get(i).findElement(By.xpath("./a")).isDisplayed(), "cards link not present");
			String hrefCard = cardList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(cardList.get(i).findElement(By.xpath("./div/a")).isDisplayed(), "pictures link not present");
			String hrefPicture = cardList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Card section link not present");
			String hrefCardSection = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Cards");

			assertTrue(cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.isDisplayed(), "card section title not present");
			String cardTitle = cardList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-2')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateCardComponent", Status.PASS,
					"All links are in sync within the Card, which is : " + hrefCard);
			String strHref = cardList.get(i).findElement(By.xpath("./div/div[@class='card-section']/a[2]"))
					.getAttribute("href").trim();
			WebElement eleLearnMore = cardList.get(i).findElement(By.xpath("./div/div[@class='card-section']/a[2]"));
			getElementInView(eleLearnMore);
			clickElementJSWithWait(eleLearnMore);
			pageCheck();

			ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
			if (allTabs.size() == 2) {
				newTabNavigations(allTabs);
				String title = driver.getCurrentUrl().trim();
				assertTrue(title.toLowerCase().contains(strHref.toLowerCase()),
						"Link Navigation not correct, expected : " + title + " | Actual : " + strHref);
				tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page: " + cardTitle + " navigating to link : " + title + " successful");
				navigateToMainTab(allTabs);
				pageCheck();
			} else {
				String title = driver.getCurrentUrl().trim();
				assertTrue(title.toLowerCase().contains(strHref.toLowerCase()),
						"Link Navigation not correct, expected : " + title + " | Actual : " + strHref);
				tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
						"Page: " + cardTitle + " navigating to link : " + title + " successful");
				navigateBack();
				pageCheck();
			}

		}
	}

	public void validateFeaturedResort() {

		getElementInView(featuredResortImage);

		assertTrue(verifyObjectDisplayed(featuredResortImage), "Featured Resort image is absent");
		assertTrue(verifyObjectDisplayed(featuredResortTitle), "Featured Resort title is absent");
		assertTrue(getElementText(featuredResortTitle).trim().length() > 0, "Featured Resort title is blank");
		String title = getElementText(featuredResortTitle).trim();

		assertTrue(verifyObjectDisplayed(featuredResortBody), "Featured Resort body is absent");
		assertTrue(getElementText(featuredResortBody).trim().length() > 0, "Featured Resort body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorefeaturedResort), "Featured Resort button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedResort", Status.PASS,
				"Featured Resort content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorefeaturedResort, "href");
		clickElementBy(btnLearnMorefeaturedResort);
		pageCheck();
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedResort", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	public void validateWelcomeToTheClub() {

		getElementInView(welcomeToTheClubImage);

		assertTrue(verifyObjectDisplayed(welcomeToTheClubImage), "Welcome To the Club image is absent");
		assertTrue(verifyObjectDisplayed(welcomeToTheClubTitle), "Welcome To the Club title is absent");
		assertTrue(getElementText(welcomeToTheClubTitle).trim().length() > 0, "Welcome To the Club title is blank");
		String title = getElementText(welcomeToTheClubTitle).trim();

		assertTrue(verifyObjectDisplayed(welcomeToTheClubBody), "Welcome To the Club body is absent");
		assertTrue(getElementText(welcomeToTheClubBody).trim().length() > 0, "Welcome To the Club body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorewelcomeToTheClub), "Welcome To the Club button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateWelcomeToTheClub", Status.PASS,
				"Welcome To the Club content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorewelcomeToTheClub, "href");
		clickElementBy(btnLearnMorewelcomeToTheClub);
		pageCheck();
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateWelcomeToTheClub", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	public void validateBlissfulBenefits() {

		getElementInView(blissfulBenefitsImage);

		assertTrue(verifyObjectDisplayed(blissfulBenefitsImage), "Blissful Benefits image is absent");
		assertTrue(verifyObjectDisplayed(blissfulBenefitsTitle), "Blissful Benefits title is absent");
		assertTrue(getElementText(blissfulBenefitsTitle).trim().length() > 0, "Blissful Benefits title is blank");
		String title = getElementText(blissfulBenefitsTitle).trim();

		assertTrue(verifyObjectDisplayed(blissfulBenefitsBody), "Blissful Benefits body is absent");
		assertTrue(getElementText(blissfulBenefitsBody).trim().length() > 0, "Blissful Benefits body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMoreblissfulBenefits), "Blissful Benefits button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateBlissfulBenefits", Status.PASS,
				"Blissful Benefits content is displayed Successfully");

		String href = getElementAttribute(btnLearnMoreblissfulBenefits, "href");
		clickElementBy(btnLearnMoreblissfulBenefits);
		pageCheck();
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateBlissfulBenefits", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	public void validateFeaturedGuide() {

		getElementInView(featureGuideImage);

		assertTrue(verifyObjectDisplayed(featureGuideImage), "Featured Guide image is absent");
		assertTrue(verifyObjectDisplayed(featureGuideTitle), "Featured Guide title is absent");
		assertTrue(getElementText(featureGuideTitle).trim().length() > 0, "Featured Guide title is blank");
		String title = getElementText(featureGuideTitle).trim();

		assertTrue(verifyObjectDisplayed(featureGuideBody), "Featured Guide body is absent");
		assertTrue(getElementText(featureGuideBody).trim().length() > 0, "Featured Guide body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorefeatureGuide), "Featured Guide button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedGuide", Status.PASS,
				"Featured Guide content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorefeatureGuide, "href");
		clickElementBy(btnLearnMorefeatureGuide);
		pageCheck();
		assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
				"Navigation to learn more link not correct");

		tcConfig.updateTestReporter("MGVCHomePage", "validateFeaturedGuide", Status.PASS,
				"Content Title : " + title + " navigating to link : " + href + " successful");
		navigateBack();
		pageCheck();
	}

	public void validateNotAnOwner() {

		getElementInView(notanOwnerImage);

		assertTrue(verifyObjectDisplayed(notanOwnerImage), "Not an Owner image is absent");
		assertTrue(verifyObjectDisplayed(notanOwnerTitle), "Not an Owner title is absent");
		assertTrue(getElementText(notanOwnerTitle).trim().length() > 0, "Not an Owner title is blank");
		String title = getElementText(notanOwnerTitle).trim();

		assertTrue(verifyObjectDisplayed(notanOwnerBody), "Not an Owner body is absent");
		assertTrue(getElementText(notanOwnerBody).trim().length() > 0, "Not an Owner body is blank");

		assertTrue(verifyObjectDisplayed(btnLearnMorenotanOwner), "Not an Owner button is absent");

		tcConfig.updateTestReporter("MGVCHomePage", "validateNotAnOwner", Status.PASS,
				"Not an Owner content is displayed Successfully");

		String href = getElementAttribute(btnLearnMorenotanOwner, "href");
		clickElementBy(btnLearnMorenotanOwner);
		pageCheck();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if (tabs.size() == 2) {
			newTabNavigations(tabs);
			assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
					"Navigation to learn more link not correct");

			tcConfig.updateTestReporter("MGVCHomePage", "validateNotAnOwner", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(tabs);
			pageCheck();
		} else {
			assertTrue(/* getElementAttribute(breadcrumbLink, "href") */getCurrentURL().trim().equalsIgnoreCase(href),
					"Navigation to learn more link not correct");

			tcConfig.updateTestReporter("MGVCHomePage", "validateNotAnOwner", Status.PASS,
					"Content Title : " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}

	public void validateLatestNews() {

		assertTrue(verifyObjectDisplayed(latestNewsHeader), "Latest News header is absent");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLatestNews", Status.PASS,
				"Latest News header is present");

		List<WebElement> newsList = getList(latestNewsComponent);

		for (int i = 0; i < newsList.size(); i++) {

			getElementInView(latestNewsComponent);
			newsList = getList(latestNewsComponent);

			assertTrue(newsList.get(i).findElement(By.xpath("./a")).isDisplayed(),
					"Latest News cards link not present");
			String hrefCard = newsList.get(i).findElement(By.xpath("./a")).getAttribute("href").trim();

			assertTrue(newsList.get(i).findElement(By.xpath("./div/a")).isDisplayed(),
					"Latest News pictures link not present");
			String hrefPicture = newsList.get(i).findElement(By.xpath("./div/a")).getAttribute("href").trim();

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.isDisplayed(), "Latest News card section link not present");
			String hrefCardSection = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a[div[contains(@class,'subtitle')]]"))
					.getAttribute("href").trim();

			assertTrue(hrefCard.equalsIgnoreCase(hrefPicture) && hrefCard.equalsIgnoreCase(hrefCardSection),
					"All links are not in sync within the Latest News card");

			assertTrue(newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.isDisplayed(), "Latest News card section title not present");
			String cardTitle = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[contains(@class,'subtitle-3')]"))
					.getText().trim();

			tcConfig.updateTestReporter("TimeshareHomePage", "validateLatestNews", Status.PASS,
					"All links are in sync within the Latest News Card, which is : " + hrefCard);

			WebElement eleReadMore = newsList.get(i)
					.findElement(By.xpath("./div/div[@class='card-section']/a/div[text()='Read More']"));
			clickElementBy(eleReadMore);

			pageCheck();
			getElementInView(breadcrumbLink);
			assertTrue(getElementAttribute(breadcrumbLink, "href").trim().equalsIgnoreCase(hrefCard),
					"Navigation to learn more link not correct");
			tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLatestNews", Status.PASS,
					"Latest News Title : " + cardTitle + " navigating to link : " + hrefCard + " successful");
			navigateBack();
			pageCheck();

		}
	}

	public void validateFooterMenu() {

		waitUntilElementVisibleBy(driver, footerDiscoverBenefits, "ExplicitLongWait");
		getElementInView(footerDiscoverBenefits);
		assertTrue(verifyObjectDisplayed(footerDiscoverBenefits), "Discover Benefits menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS,
				"Discover Benefits menu item is present");
		navigateValidateAndReturn(footerDiscoverBenefits);

		waitUntilElementVisibleBy(driver, footerResorts, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(footerResorts), "Resorts menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS, "Resorts menu item is present");
		navigateValidateAndReturn(footerResorts);

		waitUntilElementVisibleBy(driver, footerOwnerGuide, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(footerOwnerGuide), "Owner Guide menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS,
				"Owner Guide menu item is present");
		navigateValidateAndReturn(footerOwnerGuide);

		waitUntilElementVisibleBy(driver, footerDealsOffers, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(footerDealsOffers), "Deals & Offers menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS,
				"Deals & Offer menu item is present");
		navigateValidateAndReturn(footerDealsOffers);

		waitUntilElementVisibleBy(driver, footerHelp, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(footerHelp), "Help menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS, "Help menu item is present");
		navigateValidateAndReturn(footerHelp);

		waitUntilElementVisibleBy(driver, footerAboutUs, "ExplicitLongWait");
		assertTrue(verifyObjectDisplayed(footerAboutUs), "About Us menu item is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu", Status.PASS, "Help menu item is present");
		navigateValidateAndReturn(footerAboutUs);

		/*
		 * waitUntilElementVisibleBy(driver, footerCareers, "ExplicitLongWait");
		 * assertTrue(verifyObjectDisplayed(footerCareers),
		 * "Career menu item is not present");
		 * tcConfig.updateTestReporter("MGVCHomePage", "validateFooterMenu",
		 * Status.PASS, "Help menu item is present");
		 * navigateValidateAndReturntoPage(footerCareers, "tnlcareers");
		 */

	}

	public void navigateValidateAndReturn(By link) {

		String href = getElementAttribute(link, "href");
		// String title = getElementAttribute(link, "data-eventlabel");
		clickElementJSWithWait(link);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			assertTrue(getCurrentURL().trim().equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + getCurrentURL().trim());
			tcConfig.updateTestReporter("MGVCHomePage", "navigateValidateAndReturn", Status.PASS,
					"navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			pageCheck();
		} else {
			assertTrue(/* getElementAttribute(breadcrumbLink, "href") */
					getCurrentURL().trim().equalsIgnoreCase(href),
					"Link Navigation not correct, expected : " + href + " | Actual : " + getCurrentURL().trim());
			tcConfig.updateTestReporter("MGVCHomePage", "navigateValidateAndReturn", Status.PASS,
					"navigating to link : " + href + " successful");

			navigateBack();
			pageCheck();
		}

	}

	public void navigateValidateAndReturntoPage(By link, String menu) {

		String href = getElementAttribute(link, "href").trim();
		clickElementJSWithWait(link);
		pageCheck();
		ArrayList<String> allTabs = new ArrayList<String>(driver.getWindowHandles());
		if (allTabs.size() == 2) {
			newTabNavigations(allTabs);
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.contains(menu),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateToMainTab(allTabs);
			pageCheck();
		} else {
			String title = driver.getCurrentUrl().trim();
			assertTrue(title.contains(menu),
					"Link Navigation not correct, expected : " + href + " | Actual : " + title);
			tcConfig.updateTestReporter("DestinationsHomePage", "navigateValidateAndReturn", Status.PASS,
					"Page: " + title + " navigating to link : " + href + " successful");
			navigateBack();
			pageCheck();
		}

	}

	public void validateHomeFooterSubMenu(String headerMenuItem, String subMenu, int count) {

		if (count == 0) {
			navigateValidateAndReturn(
					By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='" + headerMenuItem + "']]"));

		} else {

			assertTrue(
					getList(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='" + headerMenuItem
							+ "']]/following-sibling::ul//a")).size() == count,
					"List size doesn't match, expected count=" + count + " | actual count="
							+ getList(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='"
									+ headerMenuItem + "']]/following-sibling::ul//a")).size());

			assertTrue(
					verifyObjectDisplayed(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='"
							+ headerMenuItem + "']]/following-sibling::ul//a[contains(.,'" + subMenu + "')]")),
					headerMenuItem + " menu item's sub menu item " + subMenu + " is not present");

			tcConfig.updateTestReporter("MGVCHomePage", "validateHomeFooterSubMenu", Status.PASS,
					headerMenuItem + " menu item's sub menu item " + subMenu + " is present");

			clickElementJSWithWait(By.xpath("//div[contains(@class,'genericFooter')]//a[span[text()='" + headerMenuItem
					+ "']]/following-sibling::ul//a[contains(.,'" + subMenu + "')]"));
			pageCheck();

			if (subMenu.equalsIgnoreCase("Resort News")) {
				subMenu = "News";
			} else if (subMenu.equalsIgnoreCase("Ask & Answer")) {
				subMenu = "Ask and Answer";
			} else if (subMenu.equalsIgnoreCase("Overview and Performance")) {
				subMenu = "Dive Into Margarita Life";
			} else if (subMenu.equalsIgnoreCase("COVID-19")) {
				subMenu = "COVID-19 Information";
			}

			getElementInView(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']"));
			assertTrue(verifyObjectDisplayed(By.xpath("//nav[@role='navigation']//a[text()='" + subMenu + "']")),
					subMenu + " navigations not successful");

			tcConfig.updateTestReporter("MGVCHomePage", "validateHomeHeaderSubMenu", Status.PASS,
					subMenu + " navigations successful");

			navigateBack();
			pageCheck();

		}

	}

	public void validateDisclaimerText() {
		getElementInView(disclaimerText);
		assertTrue(verifyObjectDisplayed(disclaimerText), "Disclaimer Text is not Displayed");
		tcConfig.updateTestReporter("MGVCHomePage", "validateDisclaimerText", Status.INFO,
				"Disclaimer Text is Displayed");
	}

	public void validateInstagramLink() throws InterruptedException {

		assertTrue(verifyObjectDisplayed(instaIcon), "Instagram icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateInstagramLink", Status.PASS, "Instagram icon is present");
		String href = getElementAttribute(instaIcon, "href");
		clickElementBy(instaIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		// assertTrue(verifyObjectDisplayed(instaPageObj), "Instagram link
		// navigation not successful");
		assertTrue(driver.getCurrentUrl().contains("https://www.instagram.com/"),
				"Instagram link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateInstagramLink", Status.PASS,
				"Instagram link navigation successful");
		navigateToMainTab(tabs2);

	}

	public void validateTwitterLink() {

		getElementInView(twitterIcon);
		assertTrue(verifyObjectDisplayed(twitterIcon), "Twitter icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateTwitterLink", Status.PASS, "Twitter icon is present");

		clickElementBy(twitterIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		assertTrue(driver.getCurrentUrl().contains("https://twitter.com/MargVilleVC"),
				"Twitter link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateTwitterLink", Status.PASS,
				"Twitter link navigation successful");
		navigateToMainTab(tabs2);
		// navigateBack();
		pageCheck();

	}

	public void validateLinkedInLink() {

		getElementInView(linkedInIcon);
		assertTrue(verifyObjectDisplayed(linkedInIcon), "LinkedIn icon is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLinkedInLink", Status.PASS,
				"LinkedIn icon is present");
		String href = getElementAttribute(linkedInIcon, "href");
		clickElementBy(linkedInIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		String url = driver.getCurrentUrl().trim();
		assertTrue(url.contains("https://www.linkedin.com"), "LinkedIn link navigation not successful");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateLinkedInLink", Status.PASS,
				"LinkedIn link navigation successful");
		navigateToMainTab(tabs2);
		// navigateBack();
		pageCheck();

	}

	public void validatePinterestLink() {

		getElementInView(pinterestIcon);
		assertTrue(verifyObjectDisplayed(pinterestIcon), "Pinterest icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePinterestLink", Status.PASS, "Pinterest icon is present");
		String href = getElementAttribute(pinterestIcon, "href");
		clickElementBy(pinterestIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		String url = driver.getCurrentUrl().trim();
		assertTrue(url.contains("https://www.pinterest.com/mvilleVC/"), "Pinterest link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePinterestLink", Status.PASS,
				"Pinterest link navigation successful");
		navigateToMainTab(tabs2);
		// navigateBack();
		pageCheck();

	}

	public void validateFacebookLink() {

		getElementInView(facebookIcon);
		assertTrue(verifyObjectDisplayed(facebookIcon), "Facebook icon is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFacebookLink", Status.PASS, "Facebook icon is present");
		String href = getElementAttribute(facebookIcon, "href");
		clickElementBy(facebookIcon);

		ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
		pageCheck();
		newTabNavigations(tabs2);
		String url = driver.getCurrentUrl().trim();
		assertTrue(href.contains(url), "Facebook link navigation not successful");
		tcConfig.updateTestReporter("MGVCHomePage", "validateFacebookLink", Status.PASS,
				"Facebook link navigation successful");
		navigateToMainTab(tabs2);
		// navigateBack();
		pageCheck();

	}

	public void validatefooterlogo() {

		String url = testData.get("URL");
		assertTrue(verifyObjectDisplayed(footerWyndhamLogo), "Footer Margaritaville logo is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validatefooterlogo", Status.PASS,
				"Footer Margaritaville logo is present");

		assertTrue(getElementAttribute(footerWyndhamLogo, "href").equalsIgnoreCase(url + "us/en"),
				"Footer url not correct");
		tcConfig.updateTestReporter("MGVCHomePage", "validatefooterlogo", Status.PASS,
				"Footer Margaritaville logo is present");

	}

	public void validateTermsofUse() {

		assertTrue(verifyObjectDisplayed(termsOfUse), "Terms of Use link is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateTermsofUse", Status.PASS, "Terms of Use link is present");
		navigateValidateAndReturn(termsOfUse);

	}

	public void validatePrivacyNotice() {

		assertTrue(verifyObjectDisplayed(privacyNotice), "Privacy notice link is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePrivacyNotice", Status.PASS,
				"Privacy notice link is present");
		navigateValidateAndReturn(privacyNotice);

	}

	public void validatePrivacySettings() {

		assertTrue(verifyObjectDisplayed(privacySettings), "Privacy settings link is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validatePrivacySettings", Status.PASS,
				"Privacy settings link is present");
		navigateValidateAndReturntoPage(privacySettings, "info.evidon");

	}

	public void validateSupportTimeshare() {

		assertTrue(verifyObjectDisplayed(supportTimeshare), "Support Timeshare link is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validateSupportTimeshare", Status.PASS,
				"Support Timeshare link is present");
		navigateValidateAndReturntoPage(supportTimeshare, "timeshare");

	}

	public void validatefooterCopyRight() {

		assertTrue(verifyObjectDisplayed(footerCopyRight), "Footer CopyRight Label is not present");
		tcConfig.updateTestReporter("TravelPlusLeisureHomePage", "validatefooterCopyRight", Status.PASS,
				"Footer CopyRight Label is present" + " " + getElementText(footerCopyRight));

	}

	public void validateDoNotSellInfo() {
		assertTrue(verifyObjectDisplayed(doNotsellInfo), "Do not sell info link is not present");
		tcConfig.updateTestReporter("MGVCHomePage", "validateDoNotSellInfo", Status.PASS,
				"Do not sell info link is present");
		clickElementBy(doNotsellInfo);
		pageCheck();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if (tabs.size() == 2) {
			newTabNavigations(tabs);
			getElementInView(optOutForm);
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("MGVCHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateToMainTab(tabs);
		} else {
			getElementInView(optOutForm);
			assertTrue(verifyObjectDisplayed(optOutForm), "Opt out form not displayed");
			tcConfig.updateTestReporter("MGVCHomePage", "validateDoNotSellInfo", Status.PASS,
					"Opt out form is displayed");
			navigateBack();
			pageCheck();
		}

	}

	public void brokenLinkValidation() throws InterruptedException {

		String url = "";
		// String xpath = "";
		String linkText = "";
		int numberOfLinksTested = 0;
		int numberOfLinksBroken = 0;
		int numberOfLinksWithServerSideErrors = 0;
		int numberOfLinksWithClientSideErrors = 0;
		int numberOfLinksRedirected = 0;
		int numberOfMalformedURL = 0;
		HttpURLConnection con = null;
		int respCode = 200;
		String respMessage = "";
		List<WebElement> links = driver.findElements(By.tagName("a"));
		int totalNumberOfLinksPresentInThisPage = links.size();
		Iterator<WebElement> it = links.iterator();
		boolean validLinkReportingToggleEnabled = true; // will report valid
														// links when this
														// toggle is set to true

		tcConfig.updateTestReporter("FunctionalComponent", "brokenLinkValidation", Status.PASS,
				"Navigated to Associated page", false);

		log.info("Total number of links present in this page::" + totalNumberOfLinksPresentInThisPage);
		while (it.hasNext()) {
			try {
				WebElement linkElement = it.next();
				linkText = linkElement.getText();
				url = linkElement.getAttribute("href");

				if (url == null || url.isEmpty()) {
					System.out.println("URL is either not configured for anchor tag or it is empty");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"URL:" + url + ": URL is either not configured for anchor tag or it is empty", false);
					continue;
				}

				if (url.contains("html")) {
					System.out.println("URL contians html");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"URL:" + url + ": contains HTML", false);
					continue;
				}

				/*
				 * if(!url.startsWith(homePage)){ System.out.println(
				 * "URL belongs to another domain, skipping it.");
				 * report.info("URL:"+url +
				 * ": URL belongs to another domain, skipping it."); continue; }
				 */

				con = (HttpURLConnection) (new URL(url).openConnection());
				con.setRequestMethod("HEAD");
				con.connect();

				respCode = con.getResponseCode();
				respMessage = con.getResponseMessage();

				if (respCode == 400 || respCode == 404) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a broken link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a broken link. ",
							false);
					numberOfLinksBroken++;
				}
				if (respCode >= 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a link with server side error");

					tcConfig.updateTestReporter("Functional components",
							"brokenLinkValidation", Status.FAIL, "[Response Code: " + respCode + "] [Response message: "
									+ respMessage + "] [URL:" + url + "]: URL is a link having server side error. ",
							false);
					numberOfLinksWithServerSideErrors++;
				} else if (respCode > 400 && respCode != 404 && respCode < 500) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a potentially broken link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a potentially broken link, generating runtime error. ",
							false);
					numberOfLinksWithClientSideErrors++;
				} else if (respCode >= 300 && respCode < 400) {
					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a redirect link");
					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [URL:" + url
									+ "]: URL is a redirect link. ",
							false);
					numberOfLinksRedirected++;
				} // Valid link reporting is disabled by default
				else if (respCode < 300 && validLinkReportingToggleEnabled) {

					System.out.println("Validating URL:" + url);
					log.info("Validating URL:" + url);

					if (linkText.isEmpty()) {
						log.info("Link text :" + "[No text is associated with this link]");
					} else {
						log.info("Link text :" + linkText);
					}

					System.out.println(url + " is a valid link");

					tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.PASS,
							"[Response Code: " + respCode + "] [Response message: " + respMessage + "] [ URL:" + url
									+ "]: URL is a valid link. ",
							false);
				}
			} catch (Exception e) {

				tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.FAIL,
						"Exception while validating URL:" + url + ":" + e, false);
				if (e.toString().contains("MalformedURLException")) {
					numberOfMalformedURL++;
				}
			}
			numberOfLinksTested++;
			// Thread.sleep(1000);
		}

		log.info("Number of Links Tested: " + numberOfLinksTested);
		log.info("Number of Broken links found, http response code [400,404]: " + numberOfLinksBroken);
		log.info("Number of links with Server side runtime errors, http response code [>=500]: "
				+ numberOfLinksWithServerSideErrors);
		log.info("Number of links with Client side runtime errors, http response code [400-499 except 400,404]: "
				+ numberOfLinksWithClientSideErrors);
		log.info("Number of links redirected, http response code [300-399]: " + numberOfLinksRedirected);
		log.info("Number of Malformed URLs found: " + numberOfMalformedURL);
		log.info("Completed scanning page: " + driver.getCurrentUrl());

	}

	public void brokenImagesValidations() {
		Integer iBrokenImageCount = 0;
		String status;
		List<WebElement> image_list = new ArrayList<WebElement>();
		driver.get(tcConfig.getTestData().get("URL"));
		driver.manage().window().maximize();

		try {
			iBrokenImageCount = 0;
			image_list = driver.findElements(By.tagName("img"));
			/* Print the total number of images on the page */
			log.info("The page under test has " + image_list.size() + " images");
			for (WebElement img : image_list) {
				if (img != null) {
					String imageSrc = img.getAttribute("src");
					if (imageSrc == null || imageSrc.isEmpty()) {
						System.out.println("Image is either not configured for img tag or it is empty");
						tcConfig.updateTestReporter("Functional components", "brokenLinkValidation", Status.WARNING,
								"Image:" + imageSrc + ": Image is either not configured for anchor tag or it is empty",
								false);
						continue;
					}
					URL url = new URL(imageSrc);
					URLConnection urlconnection = url.openConnection();
					HttpsURLConnection httpsURLConnection = (HttpsURLConnection) urlconnection;
					httpsURLConnection.setConnectTimeout(5000);
					httpsURLConnection.connect();

					if (httpsURLConnection.getResponseCode() == 200) {
						log.info(imageSrc + ">>" + httpsURLConnection.getResponseCode() + ">>"
								+ httpsURLConnection.getResponseMessage());
						tcConfig.updateTestReporter("Functional components", "brokenImagesValidations", Status.PASS,
								"[Response Code: " + httpsURLConnection.getResponseCode() + "] [Response message: "
										+ httpsURLConnection.getResponseMessage() + "] [URL:" + url
										+ "]: URL is not a broken image.",
								false);
					} else {
						System.err.println(imageSrc + ">>" + httpsURLConnection.getResponseCode() + ">>"
								+ httpsURLConnection.getResponseMessage());
						tcConfig.updateTestReporter("Functional components", "brokenImagesValidations", Status.WARNING,
								"[Response Code: " + httpsURLConnection.getResponseCode() + "] [Response message: "
										+ httpsURLConnection.getResponseMessage() + "] [URL:" + url
										+ "]: URL is a broken image. ",
								false);
						iBrokenImageCount++;

					}
					httpsURLConnection.disconnect();

				}
			}
		} catch (Exception e) {
			status = "failed";
			log.info(e.getMessage());
		}

		log.info("Number of Links Tested: " + image_list.size());
		log.info("Number of Broken images found, http response code [400,404]: " + iBrokenImageCount);
	}

	public void launchApplicationforBrokenLinkValidation() {
		driver.get(tcConfig.getTestData().get("URL"));

	}

}
