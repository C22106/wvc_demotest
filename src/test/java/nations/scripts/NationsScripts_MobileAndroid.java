package nations.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import nations.pages.NationsAssociateVoicesPage;
import nations.pages.NationsAssociateVoicesPage_Android;
import nations.pages.NationsCompanyNewsPage;
import nations.pages.NationsCompanyNewsPage_Android;
import nations.pages.NationsDestinationUPage;
import nations.pages.NationsDestinationUPage_Android;
import nations.pages.NationsHomePage;
import nations.pages.NationsHomePage_Android;
import nations.pages.NationsLoginPage;
import nations.pages.NationsLoginPage_Android;
import nations.pages.NationsResourcesPage;
import nations.pages.NationsResourcesPage_Android;
import nations.pages.NationsSearchResultPage;
import nations.pages.NationsSearchResultPage_Android;
import nations.pages.NationsSpinInfoPage;
import nations.pages.NationsSpinInfoPage_Android;
import nations.pages.NationsUserProfilePage;
import nations.pages.NationsUserProfilePage_Android;

public class NationsScripts_MobileAndroid extends TestBase {
	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_001_WN_HomepageHeaderContent � Description: Homepage header content
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch1"})

	public void TC_001_WN_HomepageHeaderContent(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.stockValidations();
			homePage.mainNavigationsTabValidations();
			homePage.logOutMyProfileVal();

			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
			// restart("CHROME");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_002_WN_HomepageCompanyNewsValidations � Description: Company News
	 * Homepage Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch1"})

	public void TC_002_WN_HomepageCompanyNewsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.closeHamburgerMenu();
			homePage.homepageCompanyNews();
			homePage.openHamburgerMenu();
			homePage.logOutMyProfileVal();

			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_003_WN_CompanyNewsPageValidations � Description: Company News page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch1"})

	public void TC_003_WN_CompanyNewsPageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsCompanyNewsPage companyNews = new NationsCompanyNewsPage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			companyNews.companyNewsPageNav();
			companyNews.companyNewsViewFunctionality();
			companyNews.companyNewsCardsCheck();
			// companyNews.companyNewsFilterValdns(); //filter validation
			homePage.openHamburgerMenu();
			homePage.logOutMyProfileVal();

			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_004_WN_AssociatePageValidations � Description: Company News page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch1"})

	public void TC_004_WN_AssociatePageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.closeHamburgerMenu();
			homePage.associateVoiceShareNowButtonValidation();
			homePage.openHamburgerMenu();
			associateVoices.associateVoicePageNav();
			associateVoices.associateVoiceFilterCheck();
			associateVoices.associateVoiceViewFunctionality();
			associateVoices.associateVoiceCardsCheck(); // comment change

			homePage.openHamburgerMenu();
			homePage.logOutMyProfileVal();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_005_WN_HomepageAssociateVoiceValidations � Description: Associate
	 * Voices Homepage Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch1"})

	public void TC_005_WN_HomepageAssociateVoiceValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.closeHamburgerMenu();
			homePage.homepageAssociateVoices();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				homePage.associateVoiceSharePost();// comment change
				associateVoices.associateVoiceNewPost(); // comment change
			} else {
				System.out.println("No edits were made on the site");
			}
			homePage.openHamburgerMenu();
			homePage.logOutMyProfileVal();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_006_WN_SearchFunctionalityValidations � Description: Search Results
	 * Homepage Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch1"})

	public void TC_006_WN_SearchFunctionalityValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsSearchResultPage search = new NationsSearchResultPage_Android(tcconfig);
		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.searchField();

			search.searchResultsValidatins();
			homePage.closeHamburgerMenu();
			homePage.logOutMyProfileVal();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_007_WN_CompanyNewsPostContent � Description: Company News page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch2"})

	public void TC_007_WN_CompanyNewsPostContent(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsCompanyNewsPage companyNews = new NationsCompanyNewsPage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			companyNews.companyNewsPageNav();
			companyNews.companyNewsCardsContent(); // comment change
			homePage.openHamburgerMenu();
			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_008_WN_AssociateVoicesEditPost � Description: Associate Voices Edit
	 * post Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch2"})

	public void TC_008_WN_AssociateVoicesEditPost(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();

			associateVoices.associateVoicePageNav();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				associateVoices.editAssociatePost(); // comment change
			} else {
				System.out.println("No edits were made on the site");
			}
			homePage.openHamburgerMenu();
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}

			// restart("CHROME");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_009_WN_WynNetworkHomepageVal � Description: Validate user on Wyn
	 * Network and homepage Global Header and Footer
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch2"})

	public void TC_009_WN_WynNetworkHomepageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);

		try {

			login.loginPageValidations();

			login.wynNetworkVal();
			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.headerFooterVal();

			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_010_WN_NationsComponentValdns � Description: Validate different
	 * componets such as action blocks, pdf, expand collapse
	 * 
	 
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch2"})

	public void TC_010_WN_NationsComponentValdns(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsSpinInfoPage spinInfo = new NationsSpinInfoPage_Android(tcconfig);
		NationsResourcesPage resourcePage = new NationsResourcesPage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.closeHamburgerMenu();
			spinInfo.updatedNavSpinInfo();
			spinInfo.updatedPDFVal();
			homePage.openHamburgerMenu();
			resourcePage.complianceResNav();
			resourcePage.accordionTabsValidations();
			homePage.openHamburgerMenu();
			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}*/

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_011_WN_UserProfileValidations � Description: Validate User Profile
	 * Page
	 * 
	 */
	/*@Test(dataProvider = "testData", groups={"nation","mobile","android","batch2"})

	public void TC_011_WN_UserProfileValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsUserProfilePage userProfile = new NationsUserProfilePage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();

			userProfile.userProfileNav();
			userProfile.userProfileValidations();
			homePage.openHamburgerMenu();
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}*/

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_012_WN_ExternalUserProfileVal � Description: Validate User Profile and
	 * external user profile
	 * 
	 
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch2"})

	public void TC_012_WN_ExternalUserProfileVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsAssociateVoicesPage assoVoices = new NationsAssociateVoicesPage_Android(tcconfig);
		NationsUserProfilePage userProfile = new NationsUserProfilePage_Android(tcconfig);

		try {

			login.launchApplication();

			
			 * homePage.openHamburgerMenu(); homePage.userProfileName();
			 * 
			 * assoVoices.associateVoicePageNav();
			 * 
			 * userProfile.loggedUserName(); userProfile.loggedUserProfileVal();
			 
			homePage.openHamburgerMenu();
			assoVoices.associateVoicePageNav();

			userProfile.externalUserName();
			// userProfile.externalUserProfileVal();
			homePage.openHamburgerMenu();
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}*/

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_013_WN_DestinationUValidations � Description: Validate User Profile
	 * and external user profile
	 * 
	 */
	/*@Test(dataProvider = "testData", groups={"nation","mobile","android","batch3"})

	public void TC_013_WN_DestinationUValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsDestinationUPage destU = new NationsDestinationUPage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();

			destU.destinationUPageNav();
			destU.startYourCareerNav();
			destU.destinationUPageNav();
			destU.growYourExpertiseNav();
			destU.destinationUPageNav();
			destU.devlopYourPotentialNav();
			homePage.openHamburgerMenu();
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}*/

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_014_WN_FooterValidations� Description: Nations Footer Links
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch3"})

	public void TC_014_WN_FooterValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();
			homePage.closeHamburgerMenu();
			homePage.footerPresence();
			homePage.footerSocialIcons();
			homePage.footerWynLinks();
			// homePage.footerBrands();
			homePage.openHamburgerMenu();
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_015_WN_AssociateVoiceLikeVal� Description: Nations Footer Links
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch3"})

	public void TC_015_WN_AssociateVoiceLikeVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage_Android(tcconfig);
		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();

			associateVoices.associateVoicePageNav();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				associateVoices.noLikeCountVerification();
				homePage.openHamburgerMenu();
				associateVoices.associateVoicePageNav();
				associateVoices.likedCountVerification();
			} else {
				System.out.println("No Edits were made on the site");
			}
			homePage.openHamburgerMenu();
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_016_WN_CompanyNewsLikeVal� Description: Nations Footer Links
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch3"})

	public void TC_016_WN_CompanyNewsLikeVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);
		NationsCompanyNewsPage companyNews = new NationsCompanyNewsPage_Android(tcconfig);
		try {

			login.launchApplication();

			homePage.openHamburgerMenu();
			homePage.userProfileName();

			companyNews.companyNewsPageNav();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				companyNews.noLikeCountVerification();
				homePage.openHamburgerMenu();
				companyNews.companyNewsPageNav();
				companyNews.likedCountVerification();
			} else {
				System.out.println("No Edits were made on the site");
			}
			homePage.openHamburgerMenu();
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event: TC_017_WN_ArticleImageVal�
	 * Description: Article Image Open Validations
	 * 
	 */
	/*@Test(dataProvider = "testData", groups={"nation","mobile","android","batch3"})

	public void TC_017_WN_ArticleImageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.imageVideoOpenVal();
			homePage.openHamburgerMenu();
			login.logoutNations();

			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}

	
	 * � Name:Unnat � � Version: 1 � function/event: TC_018_WN_URLRedirectVal�
	 * Description: URL Redirect Validations
	 * 
	 
	@Test(dataProvider = "testData", groups={"nation","mobile","android","batch3"})

	public void TC_018_WN_URLRedirectVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage_Android(tcconfig);
		NationsHomePage homePage = new NationsHomePage_Android(tcconfig);

		try {

			login.launchApplication();

			homePage.urlRedirectVal();

			// login.logoutNations();

			if (strBrowserInUse.contains("EDGE")) {
				////cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// ////cleanUp();
		}

	}*/

}