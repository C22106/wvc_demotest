package nations.scripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import automation.core.TestBase;
import nations.pages.NationsAssociateVoicesPage;
import nations.pages.NationsCompanyNewsPage;
import nations.pages.NationsDestinationUPage;
import nations.pages.NationsHomePage;
import nations.pages.NationsLoginPage;
import nations.pages.NationsResourcesPage;
import nations.pages.NationsSearchResultPage;
import nations.pages.NationsSpinInfoPage;
import nations.pages.NationsUserProfilePage;

public class NationsScripts_Web extends TestBase {
	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_001_WN_HomepageHeaderContent � Description: Homepage header content
	 * validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_001_WN_HomepageHeaderContent(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);

		try {

			login.launchApplication();
			homePage.userProfileName();
			homePage.stockValidations();
			homePage.mainNavigationsTabValidations();
			homePage.bannerCarouselvalidations();

			homePage.logOutMyProfileVal();

			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
			// restart("CHROME");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_002_WN_HomepageCompanyNewsValidations � Description: Company News
	 * Homepage Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "batch1"})

	public void TC_002_WN_HomepageCompanyNewsValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			homePage.homepageCompanyNews();

			login.logoutNations();

			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_003_WN_CompanyNewsPageValidations � Description: Company News page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_003_WN_CompanyNewsPageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsCompanyNewsPage companyNews = new NationsCompanyNewsPage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			companyNews.companyNewsPageNav();
			companyNews.companyNewsViewFunctionality();
			companyNews.companyNewsCardsCheck();
			// companyNews.companyNewsFilterValdns(); //filter validation

			login.logoutNations();

			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_004_WN_AssociatePageValidations � Description: Company News page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "batch1"})

	public void TC_004_WN_AssociatePageValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();
			homePage.associateVoiceShareNowButtonValidation();
			associateVoices.associateVoicePageNav();
			associateVoices.associateVoiceFilterCheck();
			associateVoices.associateVoiceViewFunctionality();
			associateVoices.associateVoiceCardsCheck(); // comment change

			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_005_WN_HomepageAssociateVoiceValidations � Description: Associate
	 * Voices Homepage Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "batch1"})

	public void TC_005_WN_HomepageAssociateVoiceValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			homePage.homepageAssociateVoices();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				homePage.associateVoiceSharePost();// comment change

				associateVoices.associateVoiceNewPost(); // comment change
			} else {
				System.out.println("No edits were made on the site");
			}
			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_006_WN_SearchFunctionalityValidations � Description: Search Results
	 * Homepage Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_006_WN_SearchFunctionalityValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsSearchResultPage search = new NationsSearchResultPage(tcconfig);
		try {

			login.launchApplication();

			homePage.userProfileName();
			homePage.searchField();

			search.searchResultsValidatins();

			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_007_WN_CompanyNewsPostContent � Description: Company News page
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_007_WN_CompanyNewsPostContent(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsCompanyNewsPage companyNews = new NationsCompanyNewsPage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			companyNews.companyNewsPageNav();
			companyNews.companyNewsCardsContent(); // comment change

			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_008_WN_AssociateVoicesEditPost � Description: Associate Voices Edit
	 * post Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_008_WN_AssociateVoicesEditPost(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			associateVoices.associateVoicePageNav();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				associateVoices.editAssociatePost(); // comment change
			} else {
				System.out.println("No edits were made on the site");
			}
			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}

			// restart("CHROME");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_009_WN_WynNetworkHomepageVal � Description: Validate user on Wyn
	 * Network and homepage Global Header and Footer
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_009_WN_WynNetworkHomepageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);

		try {

			login.loginPageValidations();
			login.wynNetworkVal();

			homePage.userProfileName();
			homePage.headerFooterVal();

			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_010_WN_NationsComponentValdns � Description: Validate different
	 * componets such as action blocks, pdf, expand collapse
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_010_WN_NationsComponentValdns(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsSpinInfoPage spinInfo = new NationsSpinInfoPage(tcconfig);
		NationsResourcesPage resourcePage = new NationsResourcesPage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			spinInfo.updatedNavSpinInfo();
			spinInfo.updatedPDFVal();

			resourcePage.complianceResNav();
			resourcePage.accordionTabsValidations();

			login.logoutNations();
			// restart("CHROME");
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_011_WN_UserProfileValidations � Description: Validate User Profile
	 * Page
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_011_WN_UserProfileValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsUserProfilePage userProfile = new NationsUserProfilePage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			userProfile.userProfileNav();
			userProfile.userProfileValidations();

			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_012_WN_ExternalUserProfileVal � Description: Validate User Profile and
	 * external user profile
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "batch2"})

	public void TC_012_WN_ExternalUserProfileVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsAssociateVoicesPage assoVoices = new NationsAssociateVoicesPage(tcconfig);
		NationsUserProfilePage userProfile = new NationsUserProfilePage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			assoVoices.associateVoicePageNav();

			userProfile.loggedUserName();
			userProfile.loggedUserProfileVal();

			assoVoices.associateVoicePageNav();

			userProfile.externalUserName();
			// userProfile.externalUserProfileVal();

			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_013_WN_DestinationUValidations � Description: Validate User Profile
	 * and external user profile
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_013_WN_DestinationUValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsDestinationUPage destU = new NationsDestinationUPage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			destU.destinationUPageNav();
			destU.startYourCareerNav();
			destU.destinationUPageNav();
			destU.growYourExpertiseNav();
			destU.destinationUPageNav();

			destU.devlopYourPotentialNav();

			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// loginPage.logOutApplictaion();
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));

			// loginPage.logOutApplictaion();
			// //cleanUp();
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_014_WN_FooterValidations� Description: Nations Footer Links
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_014_WN_FooterValidations(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);

		try {

			login.launchApplication();

			homePage.userProfileName();

			homePage.footerPresence();
			homePage.footerSocialIcons();
			homePage.footerWynLinks();
			homePage.footerBrands();

			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_015_WN_AssociateVoiceLikeVal� Description: Nations Footer Links
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_015_WN_AssociateVoiceLikeVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsAssociateVoicesPage associateVoices = new NationsAssociateVoicesPage(tcconfig);
		try {

			login.launchApplication();

			homePage.userProfileName();

			associateVoices.associateVoicePageNav();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				associateVoices.noLikeCountVerification();
				associateVoices.associateVoicePageNav();
				associateVoices.likedCountVerification();
			} else {
				System.out.println("No Edits were made on the site");
			}

			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event:
	 * TC_016_WN_CompanyNewsLikeVal� Description: Nations Footer Links
	 * Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "CB"})

	public void TC_016_WN_CompanyNewsLikeVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);
		NationsCompanyNewsPage companyNews = new NationsCompanyNewsPage(tcconfig);
		try {

			login.launchApplication();

			homePage.userProfileName();

			companyNews.companyNewsPageNav();
			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				companyNews.noLikeCountVerification();
				companyNews.companyNewsPageNav();
				companyNews.likedCountVerification();
			} else {
				System.out.println("No Edits were made on the site");
			}

			login.logoutNations();
			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}
		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event: TC_017_WN_ArticleImageVal�
	 * Description: Article Image Open Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "batch3"})

	public void TC_017_WN_ArticleImageVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);

		try {

			// login.launchApplication(strBrowserInUse, strBrowserName,
			// strModel,
			// strPlatformName, strPlatformVR, strBrowserVersion);

			homePage.imageVideoOpenVal();

			login.logoutNations();

			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

	/*
	 * � Name:Unnat � � Version: 1 � function/event: TC_018_WN_URLRedirectVal�
	 * Description: URL Redirect Validations
	 * 
	 */
	@Test(dataProvider = "testData", groups={"nations", "batch3"})

	public void TC_018_WN_URLRedirectVal(Map<String, String> testData) throws Exception {

		setupTestData(testData);

		NationsLoginPage login = new NationsLoginPage(tcconfig);
		NationsHomePage homePage = new NationsHomePage(tcconfig);

		try {

			login.launchApplication();

			homePage.urlRedirectVal();

			// login.logoutNations();

			if (strBrowserInUse.contains("EDGE")) {
				//cleanUp();
			}

		} catch (Exception e) {
			Assert.assertTrue(false,
					getTestName() + " failed in " + getClassName() + " due to : " + tcconfig.getHandleException(e));
		}

	}

}