package nations.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsAssociateVoicesPage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsAssociateVoicesPage.class);

	public NationsAssociateVoicesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By stayEngagedTab = By.xpath("//li[@class='has-submenu']/a[contains(.,'Stay Engaged')]");
	protected By associateVoiceTab = By
			.xpath("//div[@class='wyn-fly-out__container']//li/a[contains(.,'Associate Voice')]");
	protected By viewChange = By.xpath("//ul[@class='wyn-thumbnail-list__toggle']//a");
	protected By checkCardView = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-thumbnail-list__card__item']/parent::div");

	protected By associateVoiceHeader = By.xpath("//h3[contains(.,'Associate Voice')]");
	protected By associateVoiceCards = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']");
	protected By associateVoiceImg = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']/a//img");
	protected By associateVoiceName = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//span[@class='wyn-type-body-3']");
	protected By associateVoiceDate = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//span[@class='wyn-type-body-3 wyn-card-content__copy__date']");
	protected By associateVoiceTitle = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//a/p");
	protected By associateVoiceLikes = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//li[contains(.,'Likes')]");
	protected By associateVoiceComments = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//li[contains(.,'Comment')]");
	protected By associateVoiceBreadCrumb = By
			.xpath("//nav[@class='wyn-breadcrumbs']//li[contains(.,'Associate Voice')]");
	protected By pageContent = By.id("main-content");
	protected By pagination = By.xpath("//ul[@class='pagination']");
	protected By associateVoiceFilterOptn = By.xpath("//div[@class='wyn-form__filters']//select/option");
	protected By associateVoiceFilterBox = By.xpath("//div[@class='wyn-form__filters']//select");

	protected By newPostHeader = By.xpath("//h1[contains(.,'New Post')]");
	protected By titleFeild = By.xpath("//input[@name='title']");
	protected By iFrameSwitch = By.xpath("//div[@id='cke_1_contents']//iframe");
	protected By descriptionField = By
			.xpath("//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']");
	protected By categoriesHeader = By.xpath("//h3[contains(.,'Categories')]");
	protected By categoriesType = By.xpath("//ul[@class='wyn-new-post__categories']/li");
	protected By galleryItemsHeader = By.xpath("//h3[contains(.,'Gallery Items')]");
	protected By galleryMediaCTA = By.xpath("//a[contains(.,'Gallery Media')]");
	protected By chooseFile = By.xpath("//span[contains(.,'Choose File')]");
	protected By enterUrl = By.xpath("//a[@title='Link (Ctrl+K)']");
	protected By postButton = By.xpath("//button[contains(.,'Post')]");
	protected By postUserVer = By.xpath("//div[@class='wyn-author-profile__author__content']/div");
	protected By postHeaderVer = By.xpath("//div[@class='wyn-edit-post__component']/h1");
	protected By postDescrVer = By.xpath("//div[@class='wyn-edit-post__component']/p");
	protected By profileName = By.xpath("//div[@class='wyn-nation-header__profile']//a");

	protected By editPost = By.xpath("//div[@class='wyn-edit-post__cta-wrapper']/a[contains(.,'Edit post')]");
	protected By editFrame = By.xpath("//iframe[@title='Rich Text Editor, wynCkeditorEdit']");
	protected By removeImage = By.xpath("//a[@class='wyn-edit-post__cta-wrapper__cta wyn-new-post__image__remove']");
	protected By saveChanges = By.xpath("//button[contains(.,'Save Changes')]");
	// protected By iFrameSwitch =
	// By.xpath("//div[@id='cke_1_contents']//iframe");
	// protected By iFrameSwitchReply = By.xpath("//iframe[@title='Rich Text
	// Editor, wynCkeditor-0']");

	protected By commentBox = By
			.xpath("//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']/p");
	protected By postCmnt = By.xpath("//button[@id='submitget']");
	protected By commntThread = By.xpath("//ul[@id='wyn-comment-thread']/li");
	protected By commntBy = By.xpath("//div[@class='wyn-comments__comment__content']/div/a");
	protected By commentDate = By.xpath(
			"//div[@class='wyn-comments__comment__content']//div[@class='wyn-type-body-2 wyn-l-margin-xsmall--bottom']");
	protected By commentValidations = By.xpath("//div[@class='wyn-comments__comment__content__data']");
	protected By commentImage = By.xpath("//div[@class='wyn-comments__comment__profile']//img");
	protected By likesCount = By.xpath("//span[@id='likesCount']");

	protected By totalLikes = By.xpath("//span[@class='wyn-social-cta__likes wyn-type-body-3']");
	protected By totalComments = By.xpath("//span[@class='wyn-social-cta__comment wyn-type-body-3']");
	protected By likeButton = By.xpath("//button[@id='like']");
	protected By printCta = By.xpath("//a[@role='button' and contains(.,'Print')]");

	protected By articleLike = By.xpath("//span//span[@id='likesCount']");
	protected By articleComment = By.xpath("//span//span[@id='commentsCount']");

	public void associateVoicePageNav() {

		waitUntilElementVisibleBy(driver, stayEngagedTab, 120);

		if (verifyObjectDisplayed(stayEngagedTab)) {

			clickElementJS(stayEngagedTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, associateVoiceTab, 120);
			if (verifyObjectDisplayed(associateVoiceTab)) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoicePageNav", Status.PASS,
						"Associate Voice Tab present on the homepage");
				clickElementJSWithWait(associateVoiceTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoicePageNav", Status.FAIL,
						"Associate Voice Tab not present on the homepage");
			}

			waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);
			if (verifyObjectDisplayed(associateVoiceHeader)) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoicePageNav", Status.PASS,
						"Navigated to Associate Voice page, Header present");
			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoicePageNav", Status.FAIL,
						"Navigation to Associate Voice page failed, Header not present");
			}
		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoicePageNav", Status.FAIL,
					"Stay Engaged tab not present on the homepage");
		}

	}

	public void associateVoiceFilterCheck() {

		waitUntilObjectVisible(driver, associateVoiceFilterBox, 120);

		if (verifyObjectDisplayed(associateVoiceFilterBox)) {

			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.PASS,
					"Associate Voice Filter option present");
			clickElementBy(associateVoiceFilterBox);
			List<WebElement> listFilter = driver.findElements(associateVoiceFilterOptn);

			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.PASS,
					"Total Filer Option present: " + listFilter.size());

			String strFilterFull = "";

			for (int i = 1; i < listFilter.size(); i++) {

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.PASS,
						i + ". Filter Option " + listFilter.get(i).getText());

				if (listFilter.get(i).getText().contains(testData.get("strFilter"))) {
					strFilterFull = listFilter.get(i).getText();
				} else {

				}

			}

			try {
				new Select(driver.findElement(associateVoiceFilterBox)).selectByVisibleText(strFilterFull);
				// new
				// Select(driver.findElement(associateVoiceFilterBox)).selectByValue(arg0);
			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.FAIL,
						"The filter entered is not present in the option");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				List<WebElement> cnCardName = driver.findElements(associateVoiceName);

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.PASS,
						"Total " + cnCardName.size() + " post present for " + strFilterFull);

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.PASS,
						"No post present for " + testData.get("strFilter"));
			}

		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.FAIL,
					"Associate Voice Filter option not present");
		}

		try {
			new Select(driver.findElement(associateVoiceFilterBox)).selectByVisibleText(testData.get("ResetFilter"));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceFilterCheck", Status.FAIL,
					"The filter entered is not present in the option");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void associateVoiceViewFunctionality() {
		try {
			waitUntilObjectVisible(driver, checkCardView, 20);
		} catch (Exception e) {
		}
		List<WebElement> diffViewList = driver.findElements(viewChange);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		List<WebElement> cardViewList = driver.findElements(checkCardView);
		if (diffViewList.get(0).getAttribute("class").contains("active")) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality", Status.PASS,
					"By Default Grid View is active on Company News Page");
			if (cardViewList.get(0).getAttribute("class").contains("quarter")) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality", Status.PASS,
						"Cards are displayed in Grid View as expected");
			} else if (cardViewList.get(0).getAttribute("class").contains("full")) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality", Status.FAIL,
						"Cards are displayed in List View");
			}
			clickElementJSWithWait(diffViewList.get(1));
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (diffViewList.get(1).getAttribute("class").contains("active")) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality", Status.PASS,
						"Clicked on List View, List view is active");
				waitForSometime(tcConfig.getConfig().get("LongWait"));
				// System.out.println("card view class value:
				// "+cardViewList.get(0).getAttribute("class"));
				if (cardViewList.get(0).getAttribute("class").contains("full")) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality",
							Status.PASS, "Cards are displayed in List View as expected");
					clickElementJSWithWait(diffViewList.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} else if (cardViewList.get(0).getAttribute("class").contains("quarter")) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality",
							Status.FAIL, "Cards are displayed in Grid View");
				}
			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality", Status.FAIL,
						"List View not active when clicked");
			}
		} else if (diffViewList.get(0).getAttribute("class").contains("active")) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceViewFunctionality", Status.FAIL,
					"By Default List View is active on Company News Page");
		}
	}

	public void associateVoiceCardsCheck() {

		waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(associateVoiceHeader)) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
					"Associate Voice Header Present on the page");

			List<WebElement> cnCardImg = driver.findElements(associateVoiceImg);
			List<WebElement> cnCardName = driver.findElements(associateVoiceName);
			List<WebElement> cnCardDate = driver.findElements(associateVoiceDate);
			List<WebElement> cnCardTitle = driver.findElements(associateVoiceTitle);
			List<WebElement> cnCardLikes = driver.findElements(associateVoiceLikes);
			List<WebElement> cnCardComments = driver.findElements(associateVoiceComments);

			tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
					"Total Cards Present in the Associate Voice: " + cnCardTitle.size());
			// scrollDownByPixel(400);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/* if (cnCardTitle.size() == cnCardComments.size()) { */
			if (cnCardTitle.size() == (cnCardName.size())) {
				if (cnCardTitle.size() == cnCardDate.size()) {
					if (cnCardTitle.size() == (cnCardTitle.size())) {
						/* if (cnCardTitle.size() == cnCardLikes.size()) { */
						if (cnCardTitle.size() == cnCardImg.size()) {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
									Status.PASS,
									"Images, Name, Date, Title, Likes and Comments present for each card in Associate Voice");
						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
									Status.FAIL, "Images not present for all the Cards in Associate Voice");
						}
						/*
						 * } else { tcConfig.updateTestReporter(
						 * "NationsassociateVoicePage",
						 * "associateVoiceCardsCheck", Status.FAIL,
						 * "Likes not present for all the Cards in Associate Voice"
						 * ); }
						 */
					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
								Status.FAIL, "Title not present for all the Cards in Associate Voice");
					}
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Date not present for all the Cards in Associate Voice");
				}
			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Name not present for all the Cards in Associate Voice");
			}
			/*
			 * } else { tcConfig.updateTestReporter("NationsassociateVoicePage",
			 * "associateVoiceCardsCheck", Status.FAIL,
			 * "Comments not present for all the Cards in Associate Voice"); }
			 */

			if (cnCardDate.size() >= 12) {
				if (verifyObjectDisplayed(pagination)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Pagination Available on the page");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Pagination Not available on the page");
				}
			} else {
				System.out.println("Less than 12 cards hence no pagination");
			}

			try {

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"Card Title: " + cnCardTitle.get(0).getText());
				String firstCardTitle = cnCardTitle.get(0).getText().trim();

				clickElementJSWithWait(cnCardTitle.get(0));

				try {
					waitUntilElementVisibleBy(driver, pageContent, 120);
				} catch (Exception e) {
					System.out.println("");
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.getTitle().contains(firstCardTitle)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Navigated to first card related page in Associate Voice");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Failed to navigate to first card related page");
				}

				scrollDownForElementJSBy(By.xpath("//h2[contains(.,'Comments')]"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(likesCount)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "associateVoiceCardsCheck", Status.PASS,
							"Likes Count present and is: " + getElementText(likesCount));
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "associateVoiceCardsCheck", Status.FAIL,
							"Likes Count not present");
				}
				if (testData.get("editingOnSite").toUpperCase().contains("YES")) {

					commentAssoVoices();
				} else {
					System.out.println("No Updates were made to the site");
				}
				driver.navigate().back();

				waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);

				if (verifyObjectDisplayed(associateVoiceHeader)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Navigated back to Associate Voice Page");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Failed to navigate to Associate Voice Page");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Error in clicking first card");
			}

		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"Associate Voice Header not Present on the page");
		}

	}

	public void commentAssoVoices() {
		try {
			if (browserName.equalsIgnoreCase("Edge")) {
				driver.switchTo().frame(1);
			} else {
				WebElement we = driver.findElement(iFrameSwitch);
				driver.switchTo().frame(we);
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(commentBox)) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.findElement(commentBox).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.findElement(commentBox).sendKeys(testData.get("strComment"));

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"Comment Field Present, and Data entered as: " + testData.get("strComment"));

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Comment Field not  Present");
			}

			driver.switchTo().defaultContent();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCmnt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"Error in Commenting");
		}
		try {
			List<WebElement> commentList = driver.findElements(commentValidations);
			Boolean replyChck = false;
			for (int i = 0; i < commentList.size(); i++) {
				if (commentList.get(i).getText().contains(testData.get("strComment"))) {
					replyChck = true;
					break;
				} else {
					replyChck = false;
				}

			}

			if (replyChck = true) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"Comment Posted By User is reflected in the page");
			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Comment Posted By User is no reflected in the page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"No Comments on the post");
		}
		try {
			List<WebElement> commentImageList = driver.findElements(commentImage);
			List<WebElement> commntByList = driver.findElements(commntBy);
			List<WebElement> commentDateList = driver.findElements(commentDate);

			scrollDownForElementJSWb(commentDateList.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String strName = commntByList.get(0).getText();
			// strName = StringUtils.substringBetween(strName, "<div>",
			// "<div>");

			if (verifyElementDisplayed(commntByList.get(0))) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"First Comment present and was posted by " + strName + " and on "
								+ commentDateList.get(0).getText());
				if (verifyElementDisplayed(commentImageList.get(0))) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Image present in the first comment");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Image not present in the first comment");
				}

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"First comment name not present");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"Erro In Comment Validations");
		}
	}

	protected By btnChooseFile = By.id("wyn-new-post__image__file");
	protected By labelChooseFile = By.xpath("//span[contains(text(),'Choose File')]");
	protected By txtFileUploaded = By.xpath("//label[@id='fileLabel' and not(contains(text(),'No file chosen'))]");
	protected By uploadImagesection = By.xpath("//div[@class='wyn-image-gallery']");

	public void associateVoiceNewPost() {

		waitUntilElementVisibleBy(driver, newPostHeader, 120);

		if (verifyObjectDisplayed(newPostHeader)) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
					"Navigated to New Post Page, Header Present");

			if (verifyObjectDisplayed(titleFeild)) {

				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM");
				String time = dateFormat.format(new Date());
				System.out.println(time);

				fieldDataEnter(titleFeild, testData.get("strTitle") + time);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				driver.switchTo().activeElement().sendKeys(Keys.TAB);

				waitForSometime(tcConfig.getConfig().get("MedWaitWait"));
				// driver.switchTo().activeElement().sendKeys(testData.get("strDescription"));

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"Title Field Present, and Data entered as: " + testData.get("strTitle"));

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Title Field not  Present");
			}

			try {
				if (browserName.equalsIgnoreCase("edge")) {
					driver.switchTo().frame(0);
				} else {
					WebElement we = driver.findElement(iFrameSwitch);
					driver.switchTo().frame(we);
				}
				if (verifyObjectDisplayed(descriptionField)) {

					// JavascriptExecutor js= (JavascriptExecutor)driver;
					// js.executeScript("arguments[0].value='"+testData.get("strDescription")+"';",descriptionField);

					// fieldDataEnter(descriptionField,
					// testData.get("strDescription"));
					driver.findElement(descriptionField).sendKeys(testData.get("strDescription"));

					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
							"Description Field Present, and Data entered as: " + testData.get("strDescription"));

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
							"Description Field not  Present");
				}

				driver.switchTo().defaultContent();

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Error in getting the description field");
			}

			if (verifyObjectDisplayed(categoriesHeader)) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"Categories header Present");

				List<WebElement> categoriesList = driver.findElements(categoriesType);
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"No of Categories Present " + categoriesList.size());

				for (int i = 0; i < categoriesList.size(); i++) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
							(i + 1) + " Category is " + categoriesList.get(i).getText());

				}

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Categories header not Present");
			}

			if (verifyObjectDisplayed(galleryItemsHeader)) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"Gallery Items Header Present");

				if (verifyObjectDisplayed(galleryMediaCTA)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
							"Gallery Media CTA  Present");
					clickElementBy(galleryMediaCTA);

					if (verifyObjectDisplayed(chooseFile)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
								"User Has option to upload an image");

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
								"Image Uploade option not present");
					}

					if (verifyObjectDisplayed(enterUrl)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
								"User has option to enter video URL");

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
								"Video Enter option not present");
					}

					if (verifyObjectDisplayed(removeImage)) {
						clickElementBy(removeImage);

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
								"Remove Image CTA not present");
					}

					if (verifyObjectDisplayed(galleryMediaCTA)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
								"Gallery Media CTA  Present");
						getElementInView(galleryMediaCTA);
						clickElementJSWithWait(galleryMediaCTA);
						if (verifyObjectDisplayed(labelChooseFile)) {
							//sendKeysByScriptExecutor((RemoteWebDriver) driver, "Choose File", "C:\\gitWorkspace\\regression_automation\\resources\\images\\WD Logo.png");
							sendKeysBy(btnChooseFile, "C:\\gitWorkspace\\regression_automation\\resources\\images\\WD Logo.png");
							waitUntilObjectVisible(driver, txtFileUploaded, 120);
							tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost",
									Status.PASS, "Image has been uploaded");
						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost",
									Status.FAIL, "User Has not an option to upload an image");
						}

					}

					if (verifyObjectDisplayed(postButton)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
								"Posting the post button present");

						clickElementBy(postButton);

						waitUntilElementVisibleBy(driver, postHeaderVer, 120);

						if (verifyObjectDisplayed(postHeaderVer) && verifyObjectDisplayed(uploadImagesection)
								&& driver.findElement(postHeaderVer).getText().toUpperCase()
										.contains(testData.get("strTitle").toUpperCase())) {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost",
									Status.PASS, "Title is present and matches with the title posted, that is: "
											+ testData.get("strTitle"));

							List<WebElement> listDescription = driver.findElements(postDescrVer);

							for (int i = 0; i < listDescription.size(); i++) {

								if (listDescription.get(i).getText().toUpperCase()
										.contains(testData.get("strDescription").toUpperCase())) {
									tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost",
											Status.PASS,
											"Description is present and matches with the Description posted, that is: "
													+ testData.get("strDescription"));

									break;
								} else {
									if (i == (listDescription.size() - 1)) {
										tcConfig.updateTestReporter("NationsassociateVoicePage",
												"associateVoiceNewPost", Status.PASS,
												"Description does not match with the new description");

									} else {

									}
								}

							}

						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost",
									Status.FAIL, "Title does not match with entered title");
						}

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
								"Posting button not present");
					}

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
							"Gallery Media CTA not Present");
				}

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Gallery Items Header not Present");
			}
		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
					"Failed to Navigate to New Post Page");
		}

	}

	public void editAssociatePost() {

		waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);

		if (verifyObjectDisplayed(associateVoiceHeader)) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.PASS,
					"Associate Voice Header Present on the page");

			List<WebElement> cnCardName = driver.findElements(associateVoiceName);
			List<WebElement> cnCardTitle = driver.findElements(associateVoiceTitle);
			for (int i = 0; i < cnCardName.size(); i++) {
				String strCardName = cnCardName.get(i).getText().toUpperCase();
				String strProfileName = driver.findElement(profileName).getText().toUpperCase();

				if (strProfileName.contains(strCardName)) {
					scrollDownForElementJSWb(cnCardName.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.PASS,
							"Associate Voice Post is present on the page which is posted by: " + strProfileName);

					cnCardTitle.get(i).click();

					waitUntilElementVisibleBy(driver, postHeaderVer, 120);

					if (verifyObjectDisplayed(postHeaderVer)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.PASS,
								"Navigated to Associate previous Post with Title: " + getElementText(postHeaderVer));

						if (verifyObjectDisplayed(editPost)) {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.PASS,
									"Edit Option present on the page");

							clickElementBy(editPost);

							waitUntilElementVisibleBy(driver, titleFeild, 120);

							editPostDataEnter();

						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.FAIL,
									"Edit Option not present on the page");
						}

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.FAIL,
								"Associate previous Post naviagtion failed");
					}

					break;
				} else {
					if (i == (cnCardName.size() - 1)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.PASS,
								"No Post present for Associate Voices which was posted by: " + strProfileName);

						break;
					} else {

					}

				}

			}

		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost", Status.FAIL,
					"Associate Voice Header not Present on the page");
		}
	}

	public void editPostDataEnter() {

		waitUntilElementVisibleBy(driver, titleFeild, 120);

		if (verifyObjectDisplayed(titleFeild)) {

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM");
			String time = dateFormat.format(new Date());
			System.out.println(time);
			// fieldDataEnter(titleFeild, testData.get("strEditTitle"));
			driver.findElement(titleFeild).click();
			driver.findElement(titleFeild).clear();
			driver.findElement(titleFeild).sendKeys(testData.get("strEditTitle") + time);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			driver.switchTo().activeElement().sendKeys(Keys.TAB);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			// driver.switchTo().activeElement().sendKeys(testData.get("strDescription"));

			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
					"Title Field Present, and new title entered as: " + testData.get("strEditTitle"));

		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
					"Title Field not  Present");
		}

		try {
			if (browserName.equalsIgnoreCase("edge")) {
				driver.switchTo().frame(0);
			} else {

				WebElement we = driver.findElement(editFrame);
				driver.switchTo().frame(we);
			}
			if (verifyObjectDisplayed(descriptionField)) {

				// JavascriptExecutor js= (JavascriptExecutor)driver;
				// js.executeScript("arguments[0].value='"+testData.get("strDescription")+"';",descriptionField);

				// fieldDataEnter(descriptionField,
				// testData.get("strDescription"));
				driver.findElement(descriptionField).click();
				driver.findElement(descriptionField).clear();
				driver.findElement(descriptionField).sendKeys(testData.get("strEditDescription"));

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Description Field Present, and new description entered as: "
								+ testData.get("strEditDescription"));

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
						"Description Field not  Present");
			}

			driver.switchTo().defaultContent();

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
					"Error in getting the description field");
		}

		if (verifyObjectDisplayed(categoriesHeader)) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
					"Categories header Present");

			List<WebElement> categoriesList = driver.findElements(categoriesType);
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
					"No of Categories Present " + categoriesList.size());

			for (int i = 0; i < categoriesList.size(); i++) {
				if (categoriesList.get(i).getText().contains(testData.get("strFilter"))) {
					categoriesList.get(i).click();
					break;
				}

			}

		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
					"Categories header not Present");
		}

		if (verifyObjectDisplayed(galleryItemsHeader)) {
			scrollDownForElementJSBy(galleryItemsHeader);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
					"Gallery Items Header Present");

			if (verifyObjectDisplayed(galleryMediaCTA)) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Gallery Media CTA  Present");

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				if (verifyObjectDisplayed(saveChanges)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Save Changes button present");
					scrollDownForElementJSBy(saveChanges);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(300);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					// waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementJSWithWait(saveChanges);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					waitUntilElementVisibleBy(driver, postHeaderVer, 120);

					scrollDownForElementJSBy(postHeaderVer);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(150);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					if (verifyObjectDisplayed(postHeaderVer) && driver.findElement(postHeaderVer).getText()
							.toUpperCase().contains(testData.get("strEditTitle").toUpperCase())) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
								"Title is present and matches with the title posted, that is: "
										+ testData.get("strEditTitle"));

						List<WebElement> listDescription = driver.findElements(postDescrVer);

						for (int i = 0; i < listDescription.size(); i++) {

							if (listDescription.get(i).getText().toUpperCase()
									.contains(testData.get("strEditDescription").toUpperCase())) {
								tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter",
										Status.PASS,
										"Description is present and matches with the Description posted, that is: "
												+ testData.get("strEditDescription"));

								break;
							} else {
								if (i == (listDescription.size() - 1)) {
									tcConfig.updateTestReporter("NationsassociateVoicePage", "editAssociatePost",
											Status.PASS, "Description does not match with the new description");

								} else {

								}

							}

						}

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
								"Title does not match with entered title");
					}

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
							"Posting button not present");
				}

			}

		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
					"Gallery Items Header not Present");
		}

	}

	public void noLikeCountVerification() {

		List<WebElement> likesList = driver.findElements(totalLikes);
		/// List<WebElement> commentsList= driver.findElements(totalComments);
		int tempArticles = likesList.size();
		for (int i = 0; i < tempArticles; i++) {
			List<WebElement> tempLikesList = driver.findElements(totalLikes);
			List<WebElement> tempCommentsList = driver.findElements(totalComments);
			List<WebElement> cnCardTitle = driver.findElements(associateVoiceTitle);

			String strLikes = tempLikesList.get(i).getText();
			String strComments = tempCommentsList.get(i).getText();

			strLikes = strLikes.replace("Likes", "").trim();
			strComments = strComments.replace("Comments", "").trim();

			int likesOnArt = Integer.parseInt(strLikes);
			int commentsOnArt = Integer.parseInt(strComments);

			if (likesOnArt == 0) {

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Likes on the Article " + (i + 1) + " is " + strLikes);

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Comments on the Article " + (i + 1) + " is " + strComments);

				cnCardTitle.get(i).click();

				waitUntilElementVisibleBy(driver, printCta, 120);

				if (verifyObjectDisplayed(likeButton)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Like Button Present");
					if (browserName.equalsIgnoreCase("Firefox")) {
						scrollDownForElementJSBy(likeButton);
						scrollUpByPixel(400);
					} else {
						getElementInView(likeButton);
					}

					clickElementJSWithWait(likeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementInVisible(driver, likeButton, 60);
					if (verifyObjectDisplayed(likeButton)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
								"Like Button Displayed after liking the post");
						break;
					} else {
						String strLikes1 = getElementText(articleLike);
						String strComments1 = getElementText(articleComment);

						// strLikes1 = strLikes1.replace("Likes", "").trim();
						// strComments1 = strComments1.replace("Comments",
						// "").trim();

						int likesOnThisArt = Integer.parseInt(strLikes1);
						int commentsOnThisArt = Integer.parseInt(strComments1);

						if ((likesOnArt + 1) == likesOnThisArt && commentsOnArt == commentsOnThisArt) {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
									"Like Count is updated, and Comment Count is the same");
							break;
						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
									"Error in Comment and Like Count update");
							break;
						}

					}

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Already Liked by the user");
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);
					continue;

				}

			} else {

				continue;
			}

		}

	}

	public void likedCountVerification() {

		List<WebElement> likesList = driver.findElements(totalLikes);
		/// List<WebElement> commentsList= driver.findElements(totalComments);
		int tempArticles = likesList.size();
		for (int i = 0; i < tempArticles; i++) {
			List<WebElement> tempLikesList = driver.findElements(totalLikes);
			List<WebElement> tempCommentsList = driver.findElements(totalComments);
			List<WebElement> cnCardTitle = driver.findElements(associateVoiceTitle);

			String strLikes = tempLikesList.get(i).getText();
			String strComments = tempCommentsList.get(i).getText();

			strLikes = strLikes.replace("Likes", "").trim();
			strComments = strComments.replace("Comments", "").trim();

			int likesOnArt = Integer.parseInt(strLikes);
			int commentsOnArt = Integer.parseInt(strComments);

			if (likesOnArt > 0) {

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Likes on the Article " + (i + 1) + " is " + strLikes);

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Comments on the Article " + (i + 1) + " is " + strComments);

				cnCardTitle.get(i).click();

				waitUntilElementVisibleBy(driver, printCta, 120);

				if (verifyObjectDisplayed(likeButton)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Like Button Present");
					if (browserName.equalsIgnoreCase("Firefox")) {
						scrollDownForElementJSBy(likeButton);
						scrollUpByPixel(400);
					} else {
						getElementInView(likeButton);
					}

					clickElementJSWithWait(likeButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementInVisible(driver, likeButton, 60);
					if (verifyObjectDisplayed(likeButton)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
								"Like Button Displayed after liking the post");
						break;
					} else {
						String strLikes1 = getElementText(articleLike);
						String strComments1 = getElementText(articleComment);

						// strLikes1 = strLikes1.replace("Likes", "").trim();
						// strComments1 = strComments1.replace("Comments",
						// "").trim();

						int likesOnThisArt = Integer.parseInt(strLikes1);
						int commentsOnThisArt = Integer.parseInt(strComments1);

						if ((likesOnArt + 1) == likesOnThisArt && commentsOnArt == commentsOnThisArt) {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
									"Like Count is updated, and Comment Count is the same");
							break;
						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
									"Error in Comment and Like Count update");
							break;
						}

					}

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Already Liked by the user");
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);
					continue;

				}

			} else {

				continue;
			}

		}

	}

}
