package nations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsLoginPage_Android extends NationsLoginPage {

	public static final Logger log = Logger.getLogger(NationsLoginPage_Android.class);

	public NationsLoginPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		logOutTab = By.xpath("//span[@class='wyn-type-body-2']//a[contains(.,'Log out')]");
		profileName = By.xpath(
				"//*[contains(@class,'wyn-nation-header__navigation__profile-mobile')]//*[@class='wyn-my-profile-link']");
		nationsUserName = By.xpath("//input[@id='okta-signin-username']");
		nationsPassword = By.xpath("//input[@id='okta-signin-password']");
	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public void logoutNations() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, profileName, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "User Name Visible");

			waitUntilElementVisibleBy(driver, logOutTab, 120);
			if (verifyObjectDisplayed(logOutTab)) {
				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS, "Log Out Tab Present");
				clickElementJSWithWait(logOutTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				try {
					waitUntilObjectVisible(driver, oktaProfile, 20);
					if (verifyObjectDisplayed(oktaProfile)) {
						clickElementBy(oktaProfile);

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						clickElementJSWithWait(signOff);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}
				} catch (Exception e) {
					System.out.println("No need to sign off");
				}

				waitUntilObjectVisible(driver, loginPageLogo, 120);
				if (verifyObjectDisplayed(loginPageLogo)) {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "Logout Successful");

				} else {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.FAIL,
							"Navigation to login page failed, Logout Unsuccessfully");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.FAIL, "Log Out Tab not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void loginToApplication() {

		try {
			/* waitUntilObjectVisible(driver, loginBtn, 10); */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(loginBtn)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"Login Button Present");
				clickElementBy(loginBtn);

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"User already logged in");
			}

			waitUntilObjectVisible(driver, nationsUserName, 30);
			if (verifyObjectDisplayed(nationsUserName)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"Navigated to Okta Login Page");

				setUserName(testData.get("strUserName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				setPassword(testData.get("strPassword"));

				clickElementBy(signInBtn);

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.FAIL,
						"Navigation to login page failed");
			}

			waitUntilObjectVisible(driver, nationsLogo, 30);
			if (verifyObjectDisplayed(nationsLogo)) {
				System.out.println("No Login Required");
			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.FAIL,
						"Failed to Navigate to Nations Login Page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
					"Okta Login is Not required");
		}
	}

	public void launchApplication(String strBrowserInUse, String strBrowserName, String strModel,
			String strPlatformName, String strPlatformVR, String strBrowserVersion, String strLocation) {
		this.driver = checkAndInitBrowser(strBrowserInUse, strBrowserName, strModel, strPlatformName, strPlatformVR,
				strBrowserVersion, strLocation);
		driver.navigate().to(testData.get("URL"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		driver.navigate().refresh();
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		loginToApplication();
		waitUntilElementVisibleBy(driver, acceptButton, 30);
		try {
			if (verifyObjectDisplayed(acceptButton)) {
				tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
						"Accept Cookies Button Present");

				clickElementBy(acceptButton);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}

		} catch (Exception e) {
			System.out.println("No T&C Button");
		}
	}
}
