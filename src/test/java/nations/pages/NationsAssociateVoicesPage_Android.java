package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsAssociateVoicesPage_Android extends NationsAssociateVoicesPage {

	public static final Logger log = Logger.getLogger(NationsAssociateVoicesPage_Android.class);

	public NationsAssociateVoicesPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void associateVoiceCardsCheck() {

		waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(associateVoiceHeader)) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
					"Associate Voice Header Present on the page");

			List<WebElement> cnCardImg = driver.findElements(associateVoiceImg);
			List<WebElement> cnCardName = driver.findElements(associateVoiceName);
			List<WebElement> cnCardDate = driver.findElements(associateVoiceDate);
			List<WebElement> cnCardTitle = driver.findElements(associateVoiceTitle);
			List<WebElement> cnCardLikes = driver.findElements(associateVoiceLikes);
			List<WebElement> cnCardComments = driver.findElements(associateVoiceComments);

			tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
					"Total Cards Present in the Associate Voice: " + cnCardTitle.size());
			// scrollDownByPixel(400);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			/*if (cnCardTitle.size() == cnCardComments.size()) {*/
				if (cnCardTitle.size() == (cnCardName.size())) {
					if (cnCardTitle.size() == cnCardDate.size()) {
						if (cnCardTitle.size() == (cnCardTitle.size())) {
							/*if (cnCardTitle.size() == cnCardLikes.size()) {
								if (cnCardTitle.size() == cnCardImg.size()) {
									tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
											Status.PASS,
											"Images, Name, Date, Title, Likes and Comments present for each card in Associate Voice");
								} else {
									tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
											Status.FAIL, "Images not present for all the Cards in Associate Voice");
								}
							} else {
								tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
										Status.FAIL, "Likes not present for all the Cards in Associate Voice");
							}*/
						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
									Status.FAIL, "Title not present for all the Cards in Associate Voice");
						}
					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck",
								Status.FAIL, "Date not present for all the Cards in Associate Voice");
					}
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Name not present for all the Cards in Associate Voice");
				}
			/*} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Comments not present for all the Cards in Associate Voice");
			}*/

			if (cnCardDate.size() >= 12) {
				if (verifyObjectDisplayed(pagination)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Pagination Available on the page");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Pagination Not available on the page");
				}
			} else {
				System.out.println("Less than 12 cards hence no pagination");
			}

			try {

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"Card Title: " + cnCardTitle.get(0).getText());
				String firstCardTitle = cnCardTitle.get(0).getText().trim();

				clickElementJSWithWait(cnCardTitle.get(0));

				try {
					waitUntilElementVisibleBy(driver, pageContent, 120);
				} catch (Exception e) {
					System.out.println("");
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.getTitle().contains(firstCardTitle)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Navigated to first card related page in Associate Voice");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Failed to navigate to first card related page");
				}

				scrollDownForElementJSBy(By.xpath("//h2[contains(.,'Comments')]"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (verifyObjectDisplayed(likesCount)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "associateVoiceCardsCheck", Status.PASS,
							"Likes Count present and is: " + getElementText(likesCount));
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "associateVoiceCardsCheck", Status.FAIL,
							"Likes Count not present");
				}
				if (testData.get("editingOnSite").toUpperCase().contains("YES")) {

					commentAssoVoices();
				} else {
					System.out.println("No Updates were made to the site");
				}
				driver.navigate().back();

				waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);

				if (verifyObjectDisplayed(associateVoiceHeader)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Navigated back to Associate Voice Page");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Failed to navigate to Associate Voice Page");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Error in clicking first card");
			}

		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"Associate Voice Header not Present on the page");
		}

	}

	public void commentAssoVoices() {
		driver.switchTo().defaultContent();
		try {

			if (browserName.equalsIgnoreCase("Edge")) {
				driver.switchTo().frame(1);
			} else {
				WebElement we = driver.findElement(iFrameSwitch);
				driver.switchTo().frame(we);
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(commentBox)) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				sendValuesIOS(driver, "Comments", testData.get("strComment"), commentBox, 1);

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"Comment Field Present, and Data entered as: " + testData.get("strComment"));

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Comment Field not  Present");
			}

			driver.switchTo().defaultContent();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCmnt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"Error in Commenting");
		}
		try {
			List<WebElement> commentList = driver.findElements(commentValidations);
			Boolean replyChck = false;
			for (int i = 0; i < commentList.size(); i++) {
				if (commentList.get(i).getText().contains(testData.get("strComment"))) {
					replyChck = true;
					break;
				} else {
					replyChck = false;
				}

			}

			if (replyChck = true) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"Comment Posted By User is reflected in the page");
			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"Comment Posted By User is no reflected in the page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"No Comments on the post");
		}
		try {
			List<WebElement> commentImageList = driver.findElements(commentImage);
			List<WebElement> commntByList = driver.findElements(commntBy);
			List<WebElement> commentDateList = driver.findElements(commentDate);

			scrollDownForElementJSWb(commentDateList.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String strName = commntByList.get(0).getText();
			// strName = StringUtils.substringBetween(strName, "<div>",
			// "<div>");

			if (verifyElementDisplayed(commntByList.get(0))) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
						"First Comment present and was posted by " + strName + " and on "
								+ commentDateList.get(0).getText());
				if (verifyElementDisplayed(commentImageList.get(0))) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.PASS,
							"Image present in the first comment");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
							"Image not present in the first comment");
				}

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
						"First comment name not present");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceCardsCheck", Status.FAIL,
					"Error In Comment Validations");
		}
	}

	@Override
	public void associateVoiceNewPost() {

		waitUntilElementVisibleBy(driver, newPostHeader, 120);

		if (verifyObjectDisplayed(newPostHeader)) {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
					"Navigated to New Post Page, Header Present");

			if (verifyObjectDisplayed(titleFeild)) {
				fieldDataEnter(titleFeild, testData.get("strTitle"));
				driver.findElement(titleFeild).sendKeys(Keys.TAB);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"Title Field Present, and Data entered as: " + testData.get("strTitle"));

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Title Field not  Present");
			}

			try {
				if (browserName.equalsIgnoreCase("edge")) {
					driver.switchTo().frame(0);
				} else {
					WebElement we = driver.findElement(iFrameSwitch);
					driver.switchTo().frame(we);
				}
				if (verifyObjectDisplayed(descriptionField)) {

					// JavascriptExecutor js= (JavascriptExecutor)driver;
					// js.executeScript("arguments[0].value='"+testData.get("strDescription")+"';",descriptionField);

					// fieldDataEnter(descriptionField,
					// testData.get("strDescription"));
					driver.findElement(descriptionField).sendKeys(testData.get("strDescription"));
					// sendValuesIOS(driver,"New
					// Post",testData.get("strDescription"),descriptionField,1);
					driver.findElement(descriptionField).sendKeys(Keys.TAB);
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
							"Description Field Present, and Data entered as: " + testData.get("strDescription"));

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
							"Description Field not  Present");
				}

				driver.switchTo().defaultContent();

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Error in getting the description field");
			}

			if (verifyObjectDisplayed(categoriesHeader)) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"Categories header Present");

				List<WebElement> categoriesList = driver.findElements(categoriesType);
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"No of Categories Present " + categoriesList.size());

				for (int i = 0; i < categoriesList.size(); i++) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
							(i + 1) + " Category is " + categoriesList.get(i).getText());

				}

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Categories header not Present");
			}

			if (verifyObjectDisplayed(galleryItemsHeader)) {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
						"Gallery Items Header Present");

				if (verifyObjectDisplayed(galleryMediaCTA)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
							"Gallery Media CTA  Present");
					clickElementBy(galleryMediaCTA);

					if (verifyObjectDisplayed(chooseFile)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
								"User Has option to upload an image");

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
								"Image Uploade option not present");
					}

					/*
					 * if (verifyObjectDisplayed(enterUrl)) {
					 * tcConfig.updateTestReporter("NationsassociateVoicePage",
					 * "associateVoiceNewPost", Status.PASS, "User has option to enter video URL");
					 * 
					 * } else { tcConfig.updateTestReporter("NationsassociateVoicePage",
					 * "associateVoiceNewPost", Status.FAIL, "Video Enter option not present"); }
					 */

					if (verifyObjectDisplayed(removeImage)) {
						clickElementBy(removeImage);

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
								"Remove Image CTA not present");
					}
					waitUntilElementVisibleBy(driver, postButton, 120);
					if (verifyObjectDisplayed(postButton)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.PASS,
								"Posting the post button present");

						clickElementJSWithWait(postButton);

						waitUntilElementVisibleBy(driver, postHeaderVer, 120);
						List<WebElement> listDescription = driver.findElements(postDescrVer);
						for (int i = 0; i < listDescription.size(); i++) {

							if (listDescription.get(i).getText().toUpperCase()
									.contains(testData.get("strDescription").toUpperCase())) {
								tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost",
										Status.PASS,
										"Description is present and matches with the Description posted, that is: "
												+ testData.get("strDescription"));
								break;
							} else {
								if (i == (listDescription.size() - 1)) {
									tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost",
											Status.PASS, "Description does not match with the new description");

								} else {

								}
							}

						}

					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
								"Posting button not present");
					}

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
							"Gallery Media CTA not Present");
				}

			} else {
				tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
						"Gallery Items Header not Present");
			}
		} else {
			tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
					"Failed to Navigate to New Post Page");
		}

	}
}
