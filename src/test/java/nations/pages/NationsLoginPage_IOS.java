package nations.pages;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsLoginPage_IOS extends NationsLoginPage {

	public static final Logger log = Logger.getLogger(NationsLoginPage_IOS.class);

	public NationsLoginPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		acceptButton = By.xpath("//div[contains(@class,'wyn-alert-message')]//a[@class='wyn-alert-message__close']");
		PageFactory.initElements(tcconfig.getDriver(), this);
		logOutTab = By.xpath("//span[@class='wyn-type-body-2']//a[contains(.,'Log out')]");
		profileName = By.xpath(
				"//*[contains(@class,'wyn-nation-header__navigation__profile-mobile')]//*[@class='wyn-my-profile-link']");
	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public void loginToApplication() throws Exception {

		try {
			/* waitUntilObjectVisible(driver, loginBtn, 10); */
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(loginBtn)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"Login Button Present");
				clickElementBy(loginBtn);

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"User already logged in");
			}

			waitUntilObjectVisible(driver, nationsUserName, 30);
			if (verifyObjectDisplayed(nationsUserName)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"Navigated to Okta Login Page");

				setUserName(testData.get("strUserName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				setPassword(testData.get("strPassword"));

				clickElementBy(signInBtn);

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.FAIL,
						"Navigation to login page failed");
			}

			waitUntilObjectVisible(driver, nationsLogo, 30);
			if (verifyObjectDisplayed(nationsLogo)) {
				System.out.println("No Login Required");
			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.FAIL,
						"Failed to Navigate to Nations Login Page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
					"Okta Login is Not required");
		}
	}

	protected By profileName_Ipad = By.xpath("//div[@class='wyn-nation-header__profile']//a");
	protected By logOutTab_Ipad = By.xpath("//div[@class='column wyn-type-body-2']/a[contains(.,'Log out')]");

	public void logoutNations() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, profileName, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));

		if (verifyObjectDisplayed(profileName) || verifyObjectDisplayed(profileName_Ipad)) {
			tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "User Name Visible");
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
				clickElementBy(profileName_Ipad);
			}
			waitUntilElementVisibleBy(driver, logOutTab, 120);
			if (verifyObjectDisplayed(logOutTab) || verifyObjectDisplayed(logOutTab_Ipad)) {
				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS, "Log Out Tab Present");
				clickElementJSWithWait(logOutTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				try {
					waitUntilObjectVisible(driver, oktaProfile, 20);
					if (verifyObjectDisplayed(oktaProfile)) {
						clickElementBy(oktaProfile);

						waitForSometime(tcConfig.getConfig().get("MedWait"));

						clickElementJSWithWait(signOff);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}
				} catch (Exception e) {
					System.out.println("No need to sign off");
				}

				waitUntilObjectVisible(driver, loginPageLogo, 120);
				if (verifyObjectDisplayed(loginPageLogo)) {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "Logout Successful");

				} else {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.FAIL,
							"Navigation to login page failed, Logout Unsuccessfully");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.FAIL, "Log Out Tab not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void clearCache() {
		// ClearSafariCache(driver);
		clearCacheSafari();

		// Go to Home Page
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("keySequence", "HOME ");
		((JavascriptExecutor) driver).executeScript("mobile:presskey", param);

		// Launch Menu and then launch safari browser
		Map<String, Object> params1 = new HashMap<>();
		params1.put("identifier", "com.apple.mobilesafari");
		((JavascriptExecutor) driver).executeScript("mobile:application:open", params1);
		startApp("Safari", driver);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		switchToContext(driver, "WEBVIEW");
	}
}
