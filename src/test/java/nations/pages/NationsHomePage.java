package nations.pages;

import java.awt.AWTException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsHomePage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsHomePage.class);

	public NationsHomePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By nationsLogo = By.xpath("//span[@class='wyn-header__logo-label']");
	protected By wynHeroBanner = By.xpath("//div[@class='wyn-carousel__container']");

	protected By profileImage = By.xpath("//div[@class='wyn-nation-header__profile']//img");
	protected By profileName = By.xpath("//div[@class='wyn-nation-header__profile']//a");
	protected By stockPrice = By.xpath("//span[@class='wyn-stock-ticker__wrapper__price']");
	protected By stockChange = By.xpath("//div[@class='wyn-stock-ticker__wrapper__change']/span");
	protected By stockDate = By.xpath("//div[@class='wyn-stock-ticker__wrapper__stock']");

	protected By myProfileTab = By.xpath("//div[@class='column wyn-type-body-2' and contains(.,'My profile')]");
	protected By logOutTab = By.xpath("//div[@class='column wyn-type-body-2']/a[contains(.,'Log out')]");
	protected By loginPageLogo = By.xpath("//div[@class='okta-sign-in-header auth-header']");

	protected By mainNavTabs = By.xpath("//li[@class='has-submenu']/a");
	protected By headerSubMenu = By.xpath("//li[@class='has-submenu is-active']//a[@class='wyn-fly-out__headline ']");
	// protected By heroBanner = By.xpath("//div[@class='slick-track']/div[@role]");

	// img[@alt='Hero banner']
	// protected By heroBannerTitle =
	// By.xpath("//div[@class='slick-track']/div[@role]//h2");
	//
	// div[@class='wyn-banner__copy__title wyn-l-padding-xsmall--bottom
	// wyn-type-display-2']
	protected By heroBanner = By.xpath("//div[@class='wyn-carousel__container']//div[@class and @role]");
	protected By heroBannerTitle = By
			.xpath("//div[@class='wyn-carousel__container']//div[@class and @role]//div[@class='wyn-card__title']");
	protected By bannerCTA = By
			.xpath("//div[@class='wyn-carousel__container']//div[@class and @role]//div[@class='wyn-card__cta']/a");

	protected By carouselNext = By.xpath("//button[@class='slick-arrow slick-next']");
	protected By carouselPrev = By.xpath("//button[@class='slick-arrow slick-prev']");
	// protected By pageTitle=By.xpath("//title");
	protected By pageContent = By.id("main-content");

	protected By hpCompanyNewsHeader = By.xpath("//h3[contains(.,'Company News')]");
	protected By hpCompanyNewsSeeMoreBtn = By
			.xpath("//div[@id='ReactListing']//a[@class='wyn-button-primary' and contains(.,'See')]");
	protected By hpCompanyNewsCards = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']");
	protected By hpCompanyNewsImg = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']/a//img");
	protected By hpCompanyNewsDate = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__copy__border wyn-type-subheading-1']//span");
	protected By hpCompanyNewsTitle = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']//a/p");
	protected By hpCompanyNewsLikes = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']//li[contains(.,'Likes')]");
	protected By hpCompanyNewsComments = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']//li[contains(.,'Comment')]");
	protected By companyNewsBreadCrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//li[contains(.,'Company News')]");

	protected By hpAssociateVoiceHeader = By.xpath("//h3[contains(.,'Associate Voice')]");
	protected By newPostHeader = By.xpath("//h1[contains(.,'New Post')]");
	protected By hpAssociateVoiceSeeMoreBtn = By
			.xpath("//div[@id='ReactListingVoices']//a[@class='wyn-button-primary' and contains(.,'See')]");
	protected By hpAssociateVoiceCards = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']");
	protected By hpAssociateVoiceImg = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']/a//img");
	protected By hpAssociateVoiceName = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//span[@class='wyn-type-body-3']");
	protected By hpAssociateVoiceDate = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//span[@class='wyn-type-body-3 wyn-card-content__copy__date']");
	protected By hpAssociateVoiceTitle = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//a/p");
	protected By hpAssociateVoiceLikes = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//li[contains(.,'Likes')]");
	protected By hpAssociateVoiceComments = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//li[contains(.,'Comment')]");
	protected By associateVoiceBreadCrumb = By
			.xpath("//nav[@class='wyn-breadcrumbs']//li[contains(.,'Associate Voice')]");
	protected By associateVoiceSharePost = By.xpath("//div[@class='wyn-content-block__cta']//a[contains(.,'Share')]");

	protected By searchIcon = By.xpath("//button[@class='wyn-js-search-input__toggle']");
	protected By alertBar = By.xpath("//div[@class='wyn-alert-message wyn-js-alert-message is-active']/a");
	protected By searchIconActive = By.xpath("//button[@class='wyn-js-search-input__toggle is-active']");
	protected By searchContainer = By.xpath("//input[@class='wyn-search-input__field']");
	protected By searchButton = By.xpath("//button[contains(.,'Search')]");

	protected By wynFooter = By.xpath("//div[@class='wyn-footer']");
	protected By wynFooterLogo = By.xpath("//div[@class='wyn-footer']//img[@class='wyn-footer__logo']");
	protected By globalNav = By.xpath("//div[@class='wyn-nation-header__top']");

	protected By oktaProfile = By.xpath("//div[@data-se='user-menu']");
	protected By signOff = By.xpath("//p[@class='option-title' and contains(.,'Sign out')]");

	protected By footer = By.xpath("//div[@class='wyn-footer']");
	protected By footerWynLogo = By.xpath("//div[@class='wyn-footer']//img[@class='wyn-footer__logo']");
	protected By footerSocialIcons = By.xpath("//div[@class='column is-gapless wyn-footer__social']//img");
	protected By footerBrandLogos = By.xpath("//div[@class='wyn-footer__brands wyn-l-padding-xlarge--bottom']//a");
	protected By footerBrandOrder = By.xpath("//div[@class='columns is-multiline is-centered']//a/img");
	protected By footerWynPages = By.xpath("//div[@class='wyn-footer__navlinks wyn-l-wrapper--edge']//a");
	protected By footerLegalLinks = By.xpath("//div[@class='wyn-footer__legal']//a");
	protected By footerCorporateFacilities = By
			.xpath("//div[@class='wyn-footer']//a[contains(.,'Corporate Facilities')]");
	protected By footerFactSheetLink = By
			.xpath("//div[@class='wyn-footer']//a[contains(.,'Wyndham Destinations Fact Sheet')]");
	protected By factSheetPdf = By.xpath("//embed"); // type-application/pdf
	protected By factSheetHeader = By.xpath("//h1[contains(.,'Wyndham Destinations Fact Sheet')]");
	protected By factSheetLink = By.xpath("//a[contains(.,'View')]");
	protected By alertPresence = By.xpath("//div[@class='wyn-alert-message__panel']");
	protected By closeAlert = By.id("remove");
	protected By closeAlertSVG = By.xpath("//*name()='svg' and @id='remove'");

	protected By hawaiiPost = By.xpath("//p[contains(.,'2019 Hawaii Resort Ops')]");

	protected By postHeaderVer = By.xpath("//div[@class='wyn-edit-post__component']/h1");
	protected By articleImage = By.xpath("//div[@class='wyn-image-gallery']//img[1]");

	protected By articleOpened = By.xpath("//div[@class='fr-container']//div[@class='fr-content']/img");
	protected By articleCloseCTA = By.xpath("//div[@class='fr-close-icon']");

	public void footerPresence() {

		waitUntilObjectVisible(driver, footer, 120);
		if (verifyObjectDisplayed(footer)) {

			scrollDownForElementJSBy(footer);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			scrollUpByPixel(200);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NationsHomePage", "footerPresence", Status.PASS,
					"Footer is present in the page");
			if (verifyObjectDisplayed(footerWynLogo)) {
				tcConfig.updateTestReporter("NationsHomePage", "footerPresence", Status.PASS,
						"Wyndham Logo is present in Footer");
			} else {
				tcConfig.updateTestReporter("NationsHomePage", "footerPresence", Status.FAIL,
						"Wyndham Logo is not present in Footer");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerPresence", Status.FAIL,
					"Footer is not present in the page");
		}

	}

	public void footerSocialIcons() {

		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());

			for (int i = 0; i < socialIconsList.size(); i++) {
				String strSiteName = null;

				switch (i) {
				case 0:
					strSiteName = "FACEBOOK";
					break;
				case 1:
					strSiteName = "TWITTER";
					break;
				case 2:
					strSiteName = "INSTAGRAM";
					break;
				case 3:
					strSiteName = "YOUTUBE";
					break;
				case 4:
					strSiteName = "LINKED";
					break;

				default:
					tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}

				clickElementJS(socialIconsList.get(i));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs2.get(1));
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getTitle().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}

	}

	public void footerWynLinks() {

		if (verifyObjectDisplayed(footerWynPages)) {
			List<WebElement> wynLinksList = driver.findElements(footerWynPages);
			tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
					"Wyndham Nations Sub Links Present in the Footer,Total: " + wynLinksList.size());

			if (verifyObjectDisplayed(footerCorporateFacilities)) {
				tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
						"Clicking on one of the Link to verify, Corporate Facilities");

				clickElementJSWithWait(footerCorporateFacilities);
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());

				if (tabs2.size() == 2) {
					driver.switchTo().window(tabs2.get(1));
					waitForSometime(tcConfig.getConfig().get("LongWait"));
					if (driver.getTitle().toUpperCase().contains("CORPORATE")) {
						tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
								"Corporate Facilities associated Link opened");
						driver.close();
						waitForSometime(tcConfig.getConfig().get("MedWait"));
						driver.switchTo().window(tabs2.get(0));

					} else {
						tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
								"Corporate Facilities Associated Link opening error");

					}
				} else {
					if (driver.getTitle().toUpperCase().contains("CORPORATE")) {
						tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
								"Corporate Facilities associated Link opened");

						driver.navigate().back();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} else {
						tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
								"Corporate Facilities Associated Link opening error");

					}

				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
						"Wyndham Hotels & Resorts Link not present in footer");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
					"Wyndham Destiantion Sub Links not present in the Footer");
		}

	}

	public void footerBrands() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(footerBrandLogos)) {
			List<WebElement> brandsList = driver.findElements(footerBrandLogos);
			tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total: " + brandsList.size());

			clickElementJS(brandsList.get(brandsList.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs2.get(1));
			if (browserName.equalsIgnoreCase("IE")) {
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}
			if (driver.getTitle().toUpperCase().contains("REWARDS")) {
				tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.PASS,
						"Wyndham Rewards Page Opened");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				driver.close();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				driver.switchTo().window(tabs2.get(0));

				waitUntilObjectVisible(driver, footer, 120);
				if (verifyObjectDisplayed(footer)) {
					tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.PASS,
							"Navigated Back to homepage");

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.FAIL,
							"Homepage Navigation Failed");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.FAIL,
						"Wyndham Rewards Page not Opened");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.FAIL,
					"Wyndham Brands Logo not present in footer");
		}
	}

	public void searchField() {

		waitUntilObjectVisible(driver, searchIcon, 120);

		List<WebElement> l = driver.findElements(alertBar);
		if (l.size() > 0) {
			clickElementJSWithWait(l.get(l.size() - 1));
			;
		}

		if (verifyObjectDisplayed(searchIcon)) {

			tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.PASS,
					"Search Icon Present on Homepage");

			clickElementBy(searchIcon);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(searchContainer)) {

				// fieldDataEnter(searchContainer, testData.get("strSearch"));

				driver.findElement(searchContainer).click();
				driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				;
				tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.PASS, "Search Keyword Entered");

				clickElementBy(searchButton);

				waitForSometime(tcConfig.getConfig().get("MedWait"));

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.FAIL,
						"Error in getting the search container");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.FAIL,
					"Error in getting the search Icon");
		}

	}

	public void userProfileName() {
		waitUntilElementVisibleBy(driver, profileName, 120);

		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS,
					"User Logged In: " + getElementText(profileName));
			if (verifyObjectDisplayed(profileImage)) {
				tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS,
						"User profile Image present");

				if (verifyObjectDisplayed(alertPresence)) {
					tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "Alert Present");
					try {
						Actions actionbld = new Actions(driver);
						actionbld
								.moveToElement(driver.findElement(By
										.xpath("//div[contains(@class,'wyn-alert-message')]//a[@class='wyn-alert-message__close']")))
								.click().perform();
						// actionbld.click(driver.findElement(By.xpath("(//*[local-name()='svg']//*[local-name()='use'])[8]"))).build().perform();
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					} catch (Exception e) {
						System.out.println(e);
					}

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS,
							"Alert not present");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
						"User profile Image not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void stockValidations() {
		waitUntilElementVisibleBy(driver, stockPrice, 120);

		if (verifyObjectDisplayed(stockPrice)) {
			tcConfig.updateTestReporter("NationsHomePage", "stockValidations", Status.PASS,
					"Stock Price present: " + getElementText(stockPrice));

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "stockValidations", Status.FAIL,
					"Stock price not present");
		}

		if (verifyObjectDisplayed(stockChange)) {
			tcConfig.updateTestReporter("NationsHomePage", "stockValidations", Status.PASS,
					"Stock Change present: " + getElementText(stockChange));

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "stockValidations", Status.FAIL,
					"Stock Change not present");
		}

		if (verifyObjectDisplayed(stockDate)) {
			tcConfig.updateTestReporter("NationsHomePage", "stockValidations", Status.PASS,
					"Stock Source and Date present as: " + getElementText(stockDate) + " respectively");

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "stockValidations", Status.FAIL,
					"Stock Date not present");
		}

	}

	public void bannerCarouselvalidations() {
		try {
			List<WebElement> heroBannerList = driver.findElements(heroBanner);
			List<WebElement> heroBannerTitleList = driver.findElements(heroBannerTitle);

			if (heroBannerList.size() == 1) {

				tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.PASS,
						"Hero Banner Present on the homepage, 1. title as: " + heroBannerTitleList.get(0).getText());

			} else {
				int bannerList = heroBannerList.size();
				for (int i = 0; i < bannerList; i++) {
					tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.PASS,
							"Hero Banner Present on the homepage, " + (i + 1) + ". title as: "
									+ heroBannerTitleList.get(i).getText());
					List<WebElement> heroBannerCTA = driver.findElements(bannerCTA);
					String href = getElementAttribute(heroBannerCTA.get(i), "href");
					clickElementWb(heroBannerCTA.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
					driver.switchTo().window(tabs2.get(1));
					try {
						waitUntilElementVisibleBy(driver, pageContent, 10);
					} catch (Exception e) {
						System.out.println("");
					}
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					String strEnv = null;
					String urlCheck = testData.get("URL");
					if (urlCheck.toUpperCase().contains("QA")) {
						strEnv = "QA";
					} else if (urlCheck.toUpperCase().contains("WWW")) {
						strEnv = "WWW";
					} else if (urlCheck.toUpperCase().contains("STAGE")) {
						strEnv = "STAGE";
					}

					if (driver.getCurrentUrl().contains(href)) {
						tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.PASS,
								"Navigated to related page as :" + driver.getTitle());
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.FAIL,
								(i + 1) + " URLNavigated to incorrect ENV. ");
						
					}

					driver.close();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					driver.switchTo().window(tabs2.get(0));

					waitUntilObjectVisible(driver, wynHeroBanner, 120);
					if (verifyObjectDisplayed(wynHeroBanner)) {
						tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.PASS,
								"Navigated back to Homepage");
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.FAIL,
								"Error in navigating back to homepage");
					}

					if (verifyObjectDisplayed(carouselNext)) {
						tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.PASS,
								"Carousel next Arrow Present");
						clickElementBy(carouselNext);
						waitForSometime(tcConfig.getConfig().get("LowWait"));

					} else if (i == bannerList - 1) {
						tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.PASS,
								"Last Banner Present");
						break;
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.FAIL,
								"Carousel next Arrow not Present");
					}

				}

			}

			// for(int j=0; j<bannerList;j++) {
			//
			// if(verifyObjectDisplayed(carouselPrev)) {
			// tcConfig.updateTestReporter("NationsHomePage",
			// "bannerCarouselvalidations", Status.PASS,
			// "Carousel Prev Arrow Present");
			// clickElementBy(carouselPrev);
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			//
			//
			// }else if(j==bannerList-1){
			// tcConfig.updateTestReporter("NationsHomePage",
			// "bannerCarouselvalidations", Status.PASS,
			// "First Banner Present");
			// break;
			// }else {
			// tcConfig.updateTestReporter("NationsHomePage",
			// "bannerCarouselvalidations", Status.FAIL,
			// "Carousel Prev Arrow not Present");
			// }
			//
			//
			//
			// }
			// }

			// try {
			// clickElementWb(heroBannerTitleList.get(0));
			// waitForSometime(tcConfig.getConfig().get("LongWait"));
			// try {
			// waitUntilElementVisibleBy(driver, pageContent, 120);
			// }catch(Exception e) {
			// System.out.println("");
			// }
			// waitForSometime(tcConfig.getConfig().get("MedWait"));
			// tcConfig.updateTestReporter("NationsHomePage",
			// "bannerCarouselvalidations", Status.PASS,
			// "Navigated to First Banner related page as :"+driver.getTitle());
			// waitForSometime(tcConfig.getConfig().get("LowWait"));
			//
			// driver.navigate().back();
			//
			// }catch(Exception e) {
			// tcConfig.updateTestReporter("NationsHomePage",
			// "bannerCarouselvalidations", Status.FAIL,
			// "Error in Clicking First Header");
			// }
			//
			// waitUntilObjectVisible(driver, wynHeroBanner, 120);
			// if(verifyObjectDisplayed(wynHeroBanner)) {
			// tcConfig.updateTestReporter("NationsHomePage",
			// "bannerCarouselvalidations", Status.PASS,
			// "Navigated back to Homepage");
			// }else {
			// tcConfig.updateTestReporter("NationsHomePage",
			// "bannerCarouselvalidations", Status.FAIL,
			// "Error in navigating back to homepage");
			// }

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsHomePage", "bannerCarouselvalidations", Status.FAIL,
					"Error in getting Hero Banner List");
		}

	
}

	public void mainNavigationsTabValidations() {
		waitUntilElementVisibleBy(driver, mainNavTabs, 120);
		List<WebElement> mainMenuList = driver.findElements(mainNavTabs);

		for (int i = 0; i < mainMenuList.size(); i++) {
			tcConfig.updateTestReporter("GlobalNavigationPage", "mainNavigationsTabValidations", Status.PASS,
					mainMenuList.get(i).getText() + " tab is present in the global nav");
			clickElementJS(mainMenuList.get(i));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> subMenuList = driver.findElements(headerSubMenu);
			for (int j = 0; j < subMenuList.size(); j++) {
				tcConfig.updateTestReporter("GlobalNavigationPage", "mainNavigationsTabValidations", Status.PASS,
						subMenuList.get(j).getText() + " is present in the " + mainMenuList.get(i).getText()
								+ "  menu");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
			}
		}

		clickElementWb(mainMenuList.get(mainMenuList.size() - 1));
	}

	public void urlRedirectVal() {
		String loop = testData.get("tempLoop");
		int tempLoop = Integer.parseInt(loop);

		for (int i = 0; i < tempLoop; i++) {
			String oldUrl = testData.get("navigateURL" + (i + 1));
			String redirectUrl = testData.get("redirectURL" + (i + 1));
			driver.navigate().to(oldUrl);

			if (driver.getCurrentUrl().toUpperCase().contains(redirectUrl.toUpperCase())) {

				tcConfig.updateTestReporter("NationsHomePage", "urlRedirectVal", Status.PASS,
						"Navigated to correct URL" + driver.getCurrentUrl());
			} else {
				tcConfig.updateTestReporter("NationsHomePage", "urlRedirectVal", Status.FAIL,
						"Did not navigate to correct URL" + driver.getCurrentUrl());
			}

		}

	}

	public void logOutMyProfileVal() {
		waitUntilElementVisibleBy(driver, profileName, 120);
		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS, "User Name Visible");
			clickElementBy(profileName);
			waitUntilElementVisibleBy(driver, logOutTab, 120);
			if (verifyObjectDisplayed(logOutTab)) {

				if (verifyObjectDisplayed(myProfileTab)) {
					tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS,
							"My Profile Tab Present");

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.FAIL,
							"My Profile Tab not present");
				}

				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS, "Log Out Tab Present");

				clickElementJSWithWait(logOutTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				try {
					waitUntilObjectVisible(driver, oktaProfile, 20);
					if (verifyObjectDisplayed(oktaProfile)) {
						clickElementBy(oktaProfile);

						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementJSWithWait(signOff);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}
				} catch (Exception e) {
					System.out.println("No need to sign off");
				}

				waitUntilObjectVisible(driver, loginPageLogo, 120);
				if (verifyObjectDisplayed(loginPageLogo)) {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS,
							"Logout Successful");

				} else {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.FAIL,
							"Navigation to login page failed, Logout Unsuccessfully");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.FAIL,
						"Log Out Tab not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void homepageCompanyNews() {

		waitUntilElementVisibleBy(driver, hpCompanyNewsHeader, 120);

		if (verifyObjectDisplayed(hpCompanyNewsHeader)) {
			tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
					"Company News Header Present on the Homepage");

			List<WebElement> cnCardsList = driver.findElements(hpCompanyNewsCards);
			List<WebElement> cnCardImg = driver.findElements(hpCompanyNewsImg);
			List<WebElement> cnCardDate = driver.findElements(hpCompanyNewsDate);
			List<WebElement> cnCardTitle = driver.findElements(hpCompanyNewsTitle);
			List<WebElement> cnCardLikes = driver.findElements(hpCompanyNewsLikes);
			List<WebElement> cnCardComments = driver.findElements(hpCompanyNewsComments);

			tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
					"Total Cards Present in the Company News: " + cnCardsList.size());
			scrollDownByPixel(400);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (cnCardsList.size() == cnCardImg.size()) {
				if (cnCardsList.size() == cnCardDate.size()) {
					if (cnCardsList.size() == cnCardTitle.size()) {
						/*if (cnCardsList.size() == cnCardLikes.size()) {
							if (cnCardsList.size() == cnCardComments.size()) {
								tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
										"Images, Date, Title, Likes and Comments present for each card in Company News");
							} else {
								tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
										"Comments not present for all the Cards in Company News");
							}
						} else {
							tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
									"Likes not present for all the Cards in Company News");
						}*/
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
								"Title not present for all the Cards in Company News");
					}
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Date not present for all the Cards in Company News");
				}
			} else {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
						"Images not present for all the Cards in Company News");
			}

			try {

				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
						"Card Title: " + cnCardTitle.get(0).getText());
				String firstCardTitle = cnCardTitle.get(0).getText().trim();

				cnCardTitle.get(0).click();

				try {
					waitUntilElementVisibleBy(driver, pageContent, 120);
				} catch (Exception e) {
					System.out.println("");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (driver.getTitle().contains(firstCardTitle)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
							"Navigated to first card related page in Company news");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Failed to navigate to first card related page");
				}

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, hpCompanyNewsHeader, 120);

				if (verifyObjectDisplayed(hpCompanyNewsHeader)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
							"Navigated back to homepage");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Failed to navigate to homepage");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
						"Error in clicking first card");
			}

			if (verifyObjectDisplayed(hpCompanyNewsSeeMoreBtn)) {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
						"Company News See More button present on the homepage");

				waitUntilElementVisibleBy(driver, hpCompanyNewsHeader, 120);

				List<WebElement> l = driver.findElements(alertBar);
				if (l.size() > 0) {
					clickElementJSWithWait(l.get(l.size() - 1));
					;
				}

				getElementInView(hpCompanyNewsSeeMoreBtn);
				clickElementJSWithWait(hpCompanyNewsSeeMoreBtn);
				pageCheck();
				if (verifyObjectDisplayed(hpCompanyNewsHeader)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
							"Navigated to Company News page");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Failed to navigate to Company News page");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
						"Company News See More button not present on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
					"Company News Header not Present on the Homepage");
		}

	}

	public void homepageAssociateVoices() throws AWTException {

		waitUntilElementVisibleBy(driver, hpAssociateVoiceHeader, 120);
		scrollDownForElementJSBy(hpAssociateVoiceHeader);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(hpAssociateVoiceHeader)) {
			tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
					"Associate Voice Header Present on the Homepage");

			// List<WebElement>cnCardsList=driver.findElements(hpAssociateVoiceCards);
			List<WebElement> cnCardImg = driver.findElements(hpAssociateVoiceImg);
			List<WebElement> cnCardName = driver.findElements(hpAssociateVoiceName);
			List<WebElement> cnCardDate = driver.findElements(hpAssociateVoiceDate);
			List<WebElement> cnCardTitle = driver.findElements(hpAssociateVoiceTitle);
			List<WebElement> cnCardLikes = driver.findElements(hpAssociateVoiceLikes);
			List<WebElement> cnCardComments = driver.findElements(hpAssociateVoiceComments);

			tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
					"Total Cards Present in the Associate Voice: " + cnCardDate.size());
			// scrollDownByPixel(400);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (cnCardDate.size() == cnCardImg.size()) {
				if (cnCardDate.size() == (cnCardName.size())) {
					if (cnCardDate.size() == cnCardDate.size()) {
						if (cnCardDate.size() == (cnCardTitle.size())) {
							/*if (cnCardDate.size() == cnCardLikes.size()) {
								if (cnCardDate.size() == cnCardComments.size()) {
									tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice",
											Status.PASS,
											"Images, Date, Title, Likes and Comments present for each card in Associate Voice");
								} else {
									tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice",
											Status.FAIL,
											"Comments not present for all the Cards in Associate Voice");
								}
							} else {
								tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
										"Likes not present for all the Cards in Associate Voice");
							}*/
						} else {
							tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
									"Title not present for all the Cards in Associate Voice");
						}
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
								"Date not present for all the Cards in Associate Voice");
					}
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
							"Name not present for all the Cards in Associate Voice");
				}
			} else {
				tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
						"Images not present for all the Cards in Associate Voice");
			}

			/*
			 * if(verifyObjectDisplayed(hawaiiPost)){
			 * tcConfig.updateTestReporter("NationsHomePage",
			 * "homepageAssociateVoice", Status.PASS,
			 * getElementText(hawaiiPost)+
			 * " present in the homepage Associate Voices section"); }else{
			 * tcConfig.updateTestReporter("NationsHomePage",
			 * "homepageAssociateVoice", Status.FAIL,
			 * "2019 Hawaii Resort Ops Regional Conference post not present in the Associate voices section"
			 * ); }
			 */

			try {

				tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
						"Card Title: " + cnCardTitle.get(0).getText());
				String firstCardTitle = cnCardTitle.get(0).getText().trim();

				cnCardTitle.get(0).click();

				try {
					waitUntilElementVisibleBy(driver, pageContent, 120);
				} catch (Exception e) {
					System.out.println("");
				}

				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (driver.getTitle().contains(firstCardTitle)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
							"Navigated to first card related page in Associate Voices");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
							"Failed to navigate to first card related page");
				}

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, hpAssociateVoiceHeader, 120);

				if (verifyObjectDisplayed(hpAssociateVoiceHeader)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
							"Navigated back to homepage");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
							"Failed to navigate to homepage");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
						"Error in clicking first card");
			}

			waitUntilElementVisibleBy(driver, hpAssociateVoiceSeeMoreBtn, 120);

			scrollDownForElementJSBy(hpAssociateVoiceSeeMoreBtn);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(300);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(hpAssociateVoiceSeeMoreBtn)) {
				tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
						"Associate Voice See More button present on the homepage");

				/*
				 * if(browserName.equalsIgnoreCase("firefox")){ Robot r=new
				 * Robot(); r.mouseWheel(-10); }
				 */
				System.out.println("done");
				clickElementBy(hpAssociateVoiceSeeMoreBtn);

				waitUntilElementVisibleBy(driver, hpAssociateVoiceHeader, 120);

				if (verifyObjectDisplayed(hpAssociateVoiceHeader)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.PASS,
							"Navigated to Associate Voice page");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
							"Failed to navigate to Associate Voice page");
				}

				driver.navigate().back();
				waitUntilElementVisibleBy(driver, hpAssociateVoiceHeader, 120);
			} else {
				tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
						"Associate Voice See More button not present on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "homepageAssociateVoice", Status.FAIL,
					"Associate Voice Header not Present on the Homepage");
		}

	}

	public void associateVoiceSharePost() {
		waitUntilElementVisibleBy(driver, hpAssociateVoiceHeader, 120);
		scrollDownForElementJSBy(hpAssociateVoiceHeader);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(hpAssociateVoiceHeader)) {
			tcConfig.updateTestReporter("NationsHomePage", "associateVoiceSharePost", Status.PASS,
					"Associate Voice Header Present on the Homepage");

			if (verifyObjectDisplayed(associateVoiceSharePost)) {
				tcConfig.updateTestReporter("NationsHomePage", "associateVoiceSharePost", Status.PASS,
						"Associate Voices Share Post Card present on the homepage");

				clickElementBy(associateVoiceSharePost);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "associateVoiceSharePost", Status.FAIL,
						"Associate Voices Share Post not Card present on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "associateVoiceSharePost", Status.FAIL,
					"Associate Voice Header not Present on the Homepage");
		}
	}

	public void headerFooterVal() {

		if (verifyObjectDisplayed(globalNav)) {
			tcConfig.updateTestReporter("NationsHomePage", "headerFooterVal", Status.PASS,
					"Global Header present on the homepage");

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "headerFooterVal", Status.FAIL,
					"Global Header not present on the homepage");
		}

		if (browserName.equalsIgnoreCase("Chrome")) {
			driver.switchTo().activeElement().sendKeys(Keys.END);
		}

		if (verifyObjectDisplayed(wynFooter)) {
			tcConfig.updateTestReporter("NationsHomePage", "headerFooterVal", Status.PASS,
					"Global Footer present on the homepage");

			if (verifyObjectDisplayed(wynFooterLogo)) {
				tcConfig.updateTestReporter("NationsHomePage", "headerFooterVal", Status.PASS,
						" Footer Wyn Logo present on the homepage");

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "headerFooterVal", Status.FAIL,
						"Global Footer Wyn Logo not present on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "headerFooterVal", Status.FAIL,
					"Global Footer not present on the homepage");
		}

	}

	// syba
	public void associateVoiceShareNowButtonValidation() {

		waitUntilElementVisibleBy(driver, hpAssociateVoiceHeader, 120);
		scrollDownForElementJSBy(hpAssociateVoiceHeader);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(hpAssociateVoiceHeader)) {
			tcConfig.updateTestReporter("NationsHomePage", "associateVoiceShareNowButtonValidation", Status.PASS,
					"Associate Voice Header Present on the Homepage");

			if (verifyObjectDisplayed(associateVoiceSharePost)) {
				tcConfig.updateTestReporter("NationsHomePage", "associateVoiceShareNowButtonValidation", Status.PASS,
						"Associate Voices Share Post Card present on the homepage");

				clickElementBy(associateVoiceSharePost);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				waitUntilElementVisibleBy(driver, newPostHeader, 120);

				if (verifyObjectDisplayed(newPostHeader)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceShareNowButtonValidation",
							Status.PASS, "Navigated to New Post Page, Header Present");
				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "associateVoiceNewPost", Status.FAIL,
							"Failed to Navigate to New Post Page");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "associateVoiceShareNowButtonValidation", Status.FAIL,
						"Associate Voices Share Post not Card present on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "associateVoiceSharePost", Status.FAIL,
					"Associate Voice Header not Present on the Homepage");
		}
	}

	public void imageVideoOpenVal() {

		String strEnv = testData.get("URL").toUpperCase();

		NationsLoginPage login = new NationsLoginPage(tcConfig);
		if (strEnv.contains("QA")) {
			driver.navigate().to(testData.get("QaImageURL"));
			driver.manage().window().maximize();
			//login.directLogin();

			waitUntilElementVisibleBy(driver, pageContent, 120);

			if (verifyObjectDisplayed(postHeaderVer)) {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
						"Article Page Navigated");

				try {
					List<WebElement> imageList = driver.findElements(articleImage);
					if (verifyElementDisplayed(imageList.get(0))) {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
								"Image/Video Present in the article");

						imageList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						List<WebElement> openedArticle = driver.findElements(articleCloseCTA);
						if (verifyElementDisplayed(openedArticle.get(0))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}

						} else if (verifyElementDisplayed(openedArticle.get(1))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
						} else {

							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
									"Image could not be opened when clicked");
						}
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
								"Image/Video not Present in the article");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
							"Image/Video not Present in the article");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
						"Article Page Navigation failed");

			}

		} else if (strEnv.contains("STAGE")) {

			driver.navigate().to(testData.get("StageImageURL"));
			driver.manage().window().maximize();
			//login.directLogin();
			waitUntilElementVisibleBy(driver, pageContent, 120);

			if (verifyObjectDisplayed(postHeaderVer)) {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
						"Article Page Navigated");

				try {
					List<WebElement> imageList = driver.findElements(articleImage);
					if (verifyElementDisplayed(imageList.get(0))) {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
								"Image/Video Present in the article");

						imageList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						List<WebElement> openedArticle = driver.findElements(articleCloseCTA);
						if (verifyElementDisplayed(openedArticle.get(0))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}

						} else if (verifyElementDisplayed(openedArticle.get(1))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
						} else {

							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
									"Image could not be opened when clicked");
						}
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
								"Image/Video not Present in the article");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
							"Image/Video not Present in the article");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
						"Article Page Navigation failed");

			}

		} else if (strEnv.contains("WWW")) {

			driver.navigate().to(testData.get("ProdImageURL"));
			driver.manage().window().maximize();
			login.directLogin();
			waitUntilElementVisibleBy(driver, pageContent, 120);

			if (verifyObjectDisplayed(postHeaderVer)) {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
						"Article Page Navigated");

				try {
					List<WebElement> imageList = driver.findElements(articleImage);
					if (verifyElementDisplayed(imageList.get(0))) {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
								"Image/Video Present in the article");

						imageList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						List<WebElement> openedArticle = driver.findElements(articleCloseCTA);
						if (verifyElementDisplayed(openedArticle.get(0))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}

						} else if (verifyElementDisplayed(openedArticle.get(1))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
						} else {

							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
									"Image could not be opened when clicked");
						}
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
								"Image/Video not Present in the article");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
							"Image/Video not Present in the article");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
						"Article Page Navigation failed");

			}

		}

	}

}
