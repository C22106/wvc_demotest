package nations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class NationsSearchResultPage_IOS extends NationsSearchResultPage {

	public static final Logger log = Logger.getLogger(NationsSearchResultPage_IOS.class);

	public NationsSearchResultPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
