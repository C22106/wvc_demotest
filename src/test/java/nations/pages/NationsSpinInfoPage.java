package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsSpinInfoPage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsSpinInfoPage.class);

	public NationsSpinInfoPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By companyInfoTab = By.xpath("//a[contains(.,'Company Information')]");
	protected By spinInfoCTA = By.xpath("//li[@class='has-submenu is-active']//a[contains(.,'Spin Information')]");
	protected By spinInfoHeader = By.xpath("//h1[contains(.,'Spin Information')]");
	protected By actionBlock = By.xpath("//div[@class='columns wyn-action-blocks']");
	protected By pdfLinks = By.xpath("//div[@class='columns wyn-action-blocks']//li/a");
	protected By pdfValdns = By.xpath("//embed | //div[@class='pdfViewer']");

	// protected By videoArticles = By.xpath("//a[@class='wyn-js-video-player
	// wyn-video-player']");
	protected By videoArticles = By.xpath("//div[@class='wyn-card-list__card']");
	protected By videoFrame = By.xpath("//div[@class='wyn-video-player__wrapper']//video");
	protected By closeVideo = By.xpath("//span[@class='wyn-modal__close']");

	protected By spinInfoHp = By.xpath("//a[contains(.,'Spin Information')]");
	protected By spinInfoPDF = By.xpath("//span[contains(.,'Open PDF')]");

	public void spinInfoPageNav() {

		waitUntilElementVisibleBy(driver, companyInfoTab, 120);

		if (verifyObjectDisplayed(companyInfoTab)) {

			clickElementBy(companyInfoTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, spinInfoCTA, 120);
			if (verifyObjectDisplayed(spinInfoCTA)) {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
						"Spin Info tab present on the homepage");
				clickElementBy(spinInfoCTA);

			} else {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL,
						"Spin Info tab not present on the homepage");
			}

			waitUntilElementVisibleBy(driver, spinInfoHeader, 120);
			if (verifyObjectDisplayed(spinInfoHeader)) {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
						"Navigated to Spin Info page, Header and Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL,
						"Navigation to Spin Info page failed, Header not present");
			}
		} else {
			tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL,
					"Company Info tab not present on the homepage");
		}

	}

	public void updatedNavSpinInfo() {
		waitUntilElementVisibleBy(driver, spinInfoHp, 120);

		if (verifyObjectDisplayed(spinInfoHp)) {

			scrollDownForElementJSBy(spinInfoHp);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(300);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.PASS,
					"Spin Info tab present on the homepage");

			clickElementBy(spinInfoHp);

			waitUntilElementVisibleBy(driver, spinInfoHeader, 120);
			if (verifyObjectDisplayed(spinInfoHeader)) {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.PASS,
						"Navigated to Spin Info page, Header and Breadcrumb present");
				List<WebElement> videoArticleList = driver.findElements(videoArticles);
				getElementInView(videoArticleList.get(0));
				if (verifyElementDisplayed(videoArticleList.get(0))) {
					tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.PASS,
							"Spin Info Videos present on the page, total: " + videoArticleList.size());

					videoArticleList.get(0).click();

					waitUntilElementVisibleBy(driver, videoFrame, 20);

					if (verifyObjectDisplayed(videoFrame)) {
						tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.PASS,
								"Video Opened");
						if (verifyObjectDisplayed(closeVideo)) {
							tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.PASS,
									"Video Close Option Present");
							clickElementBy(closeVideo);
							waitForSometime(tcConfig.getConfig().get("LowWait"));

						} else {
							tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.FAIL,
									"Video Close Option not Present");
						}

					} else {
						tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.FAIL,
								"Video did not open");
					}

				} else {
					tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.FAIL,
							"Spin Info Videos not present");
				}

			} else {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.FAIL,
						"Navigation to Spin Info page failed, Header not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsSpinInfoPage", "updatedNavSpinInfo", Status.FAIL,
					"Spin Info tab not present on the homepage");
		}

	}

	public void updatedPDFVal() {
		try {
			List<WebElement> pdfLinks = driver.findElements(spinInfoPDF);

			tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
					"Links present on the page, Total: " + pdfLinks.size());

			pdfLinks.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			try {
				waitUntilElementVisibleBy(driver, pdfValdns, 60);
				if (driver.getCurrentUrl().contains("pdf")) {
					tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
							"The linked clicked contains pdf and is opened with URL: " + driver.getCurrentUrl());
				} else {
					tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL,
							"The link clicked is not a pdf link, with URL: " + driver.getCurrentUrl());
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
						"The link clicked is not a pdf link, with URL: " + driver.getCurrentUrl());
			}
			driver.navigate().back();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL, "No Links Present");
		}

	}

	public void spinInfoPDFVald() {
		// waitUntilElementVisibleBy(driver, actionBlock, 120);
		try {
			List<WebElement> actionBlockList = driver.findElements(actionBlock);

			tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
					"Action Blocks present on the page, Total: " + actionBlockList.size());

			try {
				List<WebElement> linkList = driver.findElements(pdfLinks);

				tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
						"Links present on the page, Total: " + linkList.size());

				linkList.get(0).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				try {
					waitUntilElementVisibleBy(driver, pdfValdns, 60);
					if (driver.getCurrentUrl().contains("pdf")) {
						tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
								"The linked clicked contains pdf and is opened with URL: " + driver.getCurrentUrl());
					} else {
						tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL,
								"The link clicked is not a pdf link, with URL: " + driver.getCurrentUrl());
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.PASS,
							"The link clicked is not a pdf link, with URL: " + driver.getCurrentUrl());
				}

				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL, "No Links Present");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsSpinInfoPage", "spinInfoPageNav", Status.FAIL,
					"No Action blocks present on the page");
		}
	}

}
