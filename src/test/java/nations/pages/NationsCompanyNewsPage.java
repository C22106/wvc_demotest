package nations.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsCompanyNewsPage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsCompanyNewsPage.class);

	public NationsCompanyNewsPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By stayEngagedTab = By.xpath("//li[@class='has-submenu']/a[contains(.,'Stay Engaged')]");
	protected By companyNewsTab = By.xpath("//div[@class='wyn-fly-out__container']//li/a[contains(.,'Company News')]");
	protected By viewChange = By.xpath("//ul[@class='wyn-thumbnail-list__toggle']//a");
	protected By checkCardView = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-thumbnail-list__card__item']/parent::div");

	protected By companyNewsHeader = By.xpath("//h3[contains(.,'Company News')]");
	protected By companyNewsCards = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']");
	protected By companyNewsImg = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']/a//img");
	protected By companyNewsDate = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']//span[@class='wyn-type-body-3']");
	protected By companyNewsTitle = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']//a/p");
	protected By companyNewsLikes = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']//li[contains(.,'Likes')]");
	protected By companyNewsComments = By.xpath(
			"//h3[contains(.,'Company News')]/ancestor::div[@id='ReactListing']//div[@class='wyn-card-content__container']//li[contains(.,'Comment')]");
	protected By companyNewsBreadCrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//li[contains(.,'Company News')]");
	protected By pageContent = By.id("main-content");
	protected By pagination = By.xpath("//ul[@class='pagination']");
	protected By articleDate = By.xpath("//div[@class='wyn-edit-post__component']/div");
	protected By paginationNo = By.xpath("//ul[@class='pagination']/li");
	protected By selectBusinessLine = By.xpath("//select[@aria-label='Business Line']");
	protected By selectLocation = By.xpath("//select[@aria-label='Location']");
	protected By businessLineOptions = By.xpath("//select[@aria-label='Business Line']/option");
	protected By locationOption = By.xpath("//select[@aria-label='Location']/option");
	protected By postContent = By.xpath("//div[@class='wyn-edit-post__component']");
	protected By printCTA = By.xpath("//div[@class='wyn-print']/a");
	protected By galleryImage = By.xpath("//div[@class='wyn-image-gallery']//img");
	protected By likesCount = By.xpath("//span[@id='likesCount']");
	protected By commentsCount = By.xpath("//span[@id='commentsCount']");
	protected By likeBtn = By.xpath("//button[@id='like']");
	protected By iFrameSwitch = By.xpath("//div[@id='cke_1_contents']//iframe");
	protected By iFrameSwitchReply = By.xpath("//iframe[@title='Rich Text Editor, wynCkeditor-0']");

	protected By commentBox = By
			.xpath("//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']");
	protected By postCmnt = By.xpath("//button[@id='submitget']");
	protected By commntThread = By.xpath("//ul[@id='wyn-comment-thread']/li");
	protected By commntBy = By.xpath("//div[@class='wyn-comments__comment__content']/div");
	protected By commentDate = By.xpath(
			"//div[@class='wyn-comments__comment__content']//div[@class='wyn-type-body-2 wyn-l-margin-xsmall--bottom']");
	protected By editComment = By.xpath("//div[@class='wyn-comments__comment__content']//a[contains(.,'Edit')]");
	protected By replyFirstComment = By.xpath(
			"//a[@class='wyn-chevron-cta wyn-type-body-3 wyn-comments__comment__content__reply-cta wyn-reply-wynCkeditor-0']");
	protected By postReply = By.xpath("//div[@class='wyn-comments__comment__content']//a[contains(.,'Post')]");
	// ul[@id='wyn-comment-thread']/li//li
	protected By commentValidations = By.xpath("//div[@class='wyn-comments__comment__content__data']");

	protected By totalLikes = By.xpath("//span[@class='wyn-social-cta__likes wyn-type-body-3']");
	protected By totalComments = By.xpath("//span[@class='wyn-social-cta__comment wyn-type-body-3']");
	protected By likeButton = By.xpath("//button[@id='like']");
	protected By printCta = By.xpath("//a[@role='button' and contains(.,'Print')]");

	protected By articleLike = By.xpath("//span//span[@id='likesCount']");
	protected By articleComment = By.xpath("//span//span[@id='commentsCount']");

	public void companyNewsPageNav() {

		waitUntilElementVisibleBy(driver, stayEngagedTab, 120);

		if (verifyObjectDisplayed(stayEngagedTab)) {

			clickElementBy(stayEngagedTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, companyNewsTab, 120);
			if (verifyObjectDisplayed(companyNewsTab)) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsPageNav", Status.PASS,
						"Company News Tab present on the homepage");
				clickElementBy(companyNewsTab);

			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsPageNav", Status.FAIL,
						"Company News Tab not present on the homepage");
			}

			waitUntilElementVisibleBy(driver, companyNewsHeader, 120);
			if (verifyObjectDisplayed(companyNewsHeader)) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsPageNav", Status.PASS,
						"Navigated to Company News page, Header and Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsPageNav", Status.FAIL,
						"Navigation to Company News page failed, Header not present");
			}
		} else {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsPageNav", Status.FAIL,
					"Stay Engaged tab not present on the homepage");
		}

	}

	public void companyNewsViewFunctionality() {

		List<WebElement> diffViewList = driver.findElements(viewChange);
		List<WebElement> cardViewList = driver.findElements(checkCardView);
		if (diffViewList.get(0).getAttribute("class").contains("active")) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality", Status.PASS,
					"By Default Grid View is active on Company News Page");

			if (cardViewList.get(0).getAttribute("class").contains("quarter")) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality", Status.PASS,
						"Cards are displayed in Grid View as expected");
			} else if (cardViewList.get(0).getAttribute("class").contains("full")) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality", Status.FAIL,
						"Cards are displayed in List View");
			}

			// scrollDownForElementJSWb(diffViewList.get(1));
			// scrollUpByPixel(300);
			clickElementJSWithWait(diffViewList.get(1));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (diffViewList.get(1).getAttribute("class").contains("active")) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality", Status.PASS,
						"Clicked on List View, List view is active");

				if (cardViewList.get(1).getAttribute("class").contains("full")) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality",
							Status.PASS, "Cards are displayed in List View as expected");

					clickElementJSWithWait(diffViewList.get(0));

					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else if (cardViewList.get(1).getAttribute("class").contains("quarter")) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality",
							Status.FAIL, "Cards are displayed in Grid View");
				}

			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality", Status.FAIL,
						"List View not active when clicked");
			}

		} else if (diffViewList.get(1).getAttribute("class").contains("active")) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsViewFunctionality", Status.FAIL,
					"By Default List View is active on Company News Page");
		}

	}

	public void companyNewsCardsCheck() {

		waitUntilElementVisibleBy(driver, companyNewsHeader, 120);

		if (verifyObjectDisplayed(companyNewsHeader)) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
					"Company News Header Present on the CompanyNewspage");

			List<WebElement> cnCardsList = driver.findElements(companyNewsCards);
			List<WebElement> cnCardImg = driver.findElements(companyNewsImg);
			List<WebElement> cnCardDate = driver.findElements(companyNewsDate);
			List<WebElement> cnCardTitle = driver.findElements(companyNewsTitle);
			List<WebElement> cnCardLikes = driver.findElements(companyNewsLikes);
			List<WebElement> cnCardComments = driver.findElements(companyNewsComments);

			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
					"Total Cards Present in the Company News: " + cnCardsList.size());
			scrollDownByPixel(400);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (cnCardsList.size() == cnCardImg.size()) {
				if (cnCardsList.size() == cnCardDate.size()) {
					if (cnCardsList.size() == cnCardTitle.size()) {
						/*if (cnCardsList.size() == cnCardLikes.size()) {
							if (cnCardsList.size() == cnCardComments.size()) {
								tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck",
										Status.PASS,
										"Images, Date, Title, Likes and Comments present for each card in Company News");
							} else {
								tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck",
										Status.FAIL, "Comments not present for all the Cards in Company News");
							}
						} else {
							tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck",
									Status.FAIL, "Likes not present for all the Cards in Company News");
						}*/
					} else {
						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
								"Title not present for all the Cards in Company News");
					}
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Date not present for all the Cards in Company News");
				}
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
						"Images not present for all the Cards in Company News");
			}

			try {
				List<WebElement> cnCardTitle1 = driver.findElements(companyNewsTitle);
				List<WebElement> cnCardDate1 = driver.findElements(companyNewsDate);
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
						"Card Title: " + cnCardTitle1.get(0).getText() + " and date: " + cnCardDate1.get(0).getText());
				String firstCardTitle = cnCardTitle1.get(0).getText().trim();
				getElementInView(cnCardTitle1.get(0));
				String strDate = cnCardDate1.get(0).getText();

				if (browserName.equalsIgnoreCase("CHROME")) {

				} else {
					driver.switchTo().activeElement().sendKeys(Keys.HOME);
					waitForSometime(tcConfig.getConfig().get("MedWait"));
				}

				// if(strDate.contains(",")){
				// strDate= strDate.replace(",", "");
				//
				// }
				//
				// if(strDate.contains(" ")){
				// strDate= strDate.replace(" ", " ");
				//
				// }
				//
				// if(strDate.contains(" ")){
				// strDate= strDate.replace(" ", "-");
				//
				// }
				Date date1 = new SimpleDateFormat("MMM dd, yyyy").parse(strDate);
				cnCardTitle1.get(0).click();

				try {
					waitUntilElementVisibleBy(driver, pageContent, 120);
				} catch (Exception e) {
					System.out.println("");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (driver.getTitle().contains(firstCardTitle)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"Navigated to first card related page in Company news");
					if (verifyObjectDisplayed(articleDate)) {
						String fullDate = getElementText(articleDate);
						fullDate = fullDate.trim();
						fullDate = fullDate.substring(0, fullDate.length() - 13);

						String newDate = fullDate.trim();
						Date date2 = new SimpleDateFormat("MMMM dd, yyyy").parse(newDate);

						if (date1.equals(date2)) {
							tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck",
									Status.PASS, "Both pages date are equal");
						} else {
							tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck",
									Status.FAIL, "Article date displayed as diferrent in both pages");
						}

					} else {
						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
								"Article Date not present inside deatils page");
					}

				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Failed to navigate to first card related page");
				}

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, companyNewsHeader, 120);

				if (verifyObjectDisplayed(companyNewsHeader)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"Navigated back to Company News Page");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Failed to navigate to Company News Page");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
						"Error in clicking first card");
			}

			/*
			 * if (verifyObjectDisplayed(selectBusinessLine)) {
			 * tcConfig.updateTestReporter("NationsCompanyNewsPage",
			 * "companyNewsCardsCheck", Status.PASS,
			 * "Select Business Line Filter Present"); } else {
			 * tcConfig.updateTestReporter("NationsCompanyNewsPage",
			 * "companyNewsCardsCheck", Status.FAIL,
			 * "Select Business Line Filter not Present"); }
			 * 
			 * if (verifyObjectDisplayed(selectLocation)) {
			 * tcConfig.updateTestReporter("NationsCompanyNewsPage",
			 * "companyNewsCardsCheck", Status.PASS,
			 * "Select Location Filter Present"); } else {
			 * tcConfig.updateTestReporter("NationsCompanyNewsPage",
			 * "companyNewsCardsCheck", Status.FAIL,
			 * "Select Location not Filter Present"); }
			 */

			try {
				scrollDownForElementJSBy(pagination);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(pagination)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"Pagination Present on the page");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Failed to navigate to Company News Page");
				}

				List<WebElement> paginationsList = driver.findElements(paginationNo);

				if (paginationsList.get(0).getAttribute("class").toLowerCase().contains("active")) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"On first page by default");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Not On first page by default");
				}

				paginationsList.get(1).click();

				List<WebElement> paginationsList2 = driver.findElements(paginationNo);
				if (paginationsList2.get(1).getAttribute("class").toLowerCase().contains("active")) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"Second Page navigated");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Error in navigating second page");
				}

			} catch (Exception e) {
				System.out.println("Pagination Not available");
			}

		} else {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
					"Company News Header not Present on the CompanyNewspage");
		}

	}

	public void companyNewsCardsContent() {

		waitUntilElementVisibleBy(driver, companyNewsHeader, 120);

		if (verifyObjectDisplayed(companyNewsHeader)) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
					"Company News Header Present on the CompanyNewspage");
			// List<WebElement> cnCardsList =
			// driver.findElements(companyNewsCards);
			List<WebElement> cnCardTitle = driver.findElements(companyNewsTitle);

			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
					"Card Title: " + cnCardTitle.get(1).getText());
			String firstCardTitle = cnCardTitle.get(1).getText().trim();

			cnCardTitle.get(1).click();

			try {
				waitUntilElementVisibleBy(driver, pageContent, 120);
			} catch (Exception e) {
				System.out.println("");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (driver.getTitle().contains(firstCardTitle)) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Navigated to clicked Card with title: " + driver.getTitle());
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Failed to navigate to clicked card related page");
			}

			try {
				List<WebElement> bodyContent = driver.findElements(postContent);

				if (verifyObjectDisplayed(postContent)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
							"Content present in the card");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
							"No Content present inside the card");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"No Content present inside the card");
			}

			try {
				List<WebElement> imageContent = driver.findElements(galleryImage);

				if (verifyElementDisplayed(imageContent.get(0))) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
							"Images present in the card");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
							"No Images present inside the card");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"No Images present inside the card");
			}

			scrollDownForElementJSBy(printCTA);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(printCTA)) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Print option present in the card");
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Print option not present in the card");
			}

			if (verifyObjectDisplayed(likesCount)) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Likes Count present and is: " + getElementText(likesCount));
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Likes Count not present");
			}
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (verifyObjectDisplayed(likeBtn)) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"User has the option to like the card");
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"User has already liked the card");
			}

			if (verifyObjectDisplayed(commentsCount)) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Comments Count present and is: " + getElementText(commentsCount));
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Comments Count not present");
			}

			if (testData.get("editingOnSite").toUpperCase().contains("YES")) {
				commentsAndReplyInCN();

			} else {
				System.out.println("No Updates were made to the site");
			}

		} else {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
					"Company News Header not Present on the CompanyNewspage");
		}
	}

	public void commentsAndReplyInCN() {

		scrollDownForElementJSBy(By.xpath("//h2[contains(.,'Comments')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			if (browserName.equalsIgnoreCase("edge")) {
				driver.switchTo().frame(0);
			} else {
				WebElement we = driver.findElement(iFrameSwitch);
				driver.switchTo().frame(we);
			}

			if (verifyObjectDisplayed(commentBox)) {

				driver.findElement(commentBox).click();
				driver.findElement(commentBox).sendKeys(testData.get("strComment"));

				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Comment Field Present, and Data entered as: " + testData.get("strComment"));

			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Comment Field not  Present");
			}

			driver.switchTo().defaultContent();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCmnt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
					"Error in Commenting");
		}
		try {
			List<WebElement> commentList = driver.findElements(commentValidations);
			Boolean replyChck = false;
			for (int i = 0; i < commentList.size(); i++) {
				if (commentList.get(i).getText().contains(testData.get("strComment"))) {
					replyChck = true;
					break;
				} else {
					replyChck = false;
				}

			}

			if (replyChck = true) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Comment Posted By User is reflected in the page");
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Comment Posted By User is no reflected in the page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
					"No Comments on the post");
		}
		try {
			List<WebElement> commentThreadList = driver.findElements(commntThread);
			List<WebElement> commntByList = driver.findElements(commntBy);
			List<WebElement> commentDateList = driver.findElements(commentDate);

			scrollDownForElementJSWb(commentDateList.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String strName = commntByList.get(0).getAttribute("outerHTML");
			strName = StringUtils.substringBetween(strName, "<div>", "<div>");

			if (verifyElementDisplayed(commntByList.get(0))) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"First Comment present and was posted by " + strName + " and on "
								+ commentDateList.get(0).getText());
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"First comment name not present");
			}

			try {
				scrollDownForElementJSBy(replyFirstComment);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Reply option present in first comment");
				clickElementBy(replyFirstComment);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				List<WebElement> iFrameList = driver.findElements(iFrameSwitchReply);

				try {
					if (browserName.equalsIgnoreCase("edge")) {
						driver.switchTo().frame(1);
					} else {
						driver.switchTo().frame(iFrameList.get((iFrameList.size() - 1)));

					}

					List<WebElement> commentBoxList = driver.findElements(commentBox);

					if (verifyElementDisplayed(commentBoxList.get(commentBoxList.size() - 1))) {

						commentBoxList.get(commentBoxList.size() - 1).click();
						commentBoxList.get(commentBoxList.size() - 1).sendKeys(testData.get("strReply"));

						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
								"Reply Comment Field Present, and Data entered as: " + testData.get("strReply"));

					} else {
						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
								"Reply Comment Field not  Present");
					}

					driver.switchTo().defaultContent();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(postReply);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
							"Error in Replying");
				}

				try {
					List<WebElement> commentList = driver.findElements(commentValidations);

					Boolean replyChck = false;
					for (int i = 0; i < commentList.size(); i++) {
						if (commentList.get(i).getText().contains(testData.get("strReply"))) {
							replyChck = true;
							break;
						} else {
							replyChck = false;
						}

					}

					if (replyChck = true) {
						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
								"Reply Posted By User is reflected in the page");
					} else {
						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
								"Reply Posted By User is no reflected in the page");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
							"No Comments on the post");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Reply option not present on the first comment");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
					"No Comments on the post");
		}

	}

	public void companyNewsBusinessLineFilter() {

		waitUntilElementVisibleBy(driver, selectBusinessLine, 120);

		if (verifyObjectDisplayed(selectBusinessLine)) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsFilterValdns", Status.PASS,
					"Business Line Filter Present on Company News Page");

			clickElementBy(selectBusinessLine);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			List<WebElement> blOptionsList = driver.findElements(businessLineOptions);

			for (int i = 1; i < blOptionsList.size(); i++) {

				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsFilterValdns", Status.PASS,
						"Business Line Filter Option #" + i + " is " + blOptionsList.get(i).getText());

			}

			try {
				new Select(driver.findElement(selectBusinessLine)).selectByVisibleText(testData.get("strBLFilter"));
			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsFilterValdns", Status.FAIL,
						"The filter entered is not present in the option");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				List<WebElement> cnCardName = driver.findElements(companyNewsTitle);

				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsFilterValdns", Status.PASS,
						"Total " + cnCardName.size() + " post present for " + testData.get("strBLFilter"));

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsFilterValdns", Status.PASS,
						"No post present for " + testData.get("strBLFilter"));
			}

			try {
				new Select(driver.findElement(selectBusinessLine)).selectByVisibleText("Select Business Line");
			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsFilterValdns", Status.FAIL,
						"The filter entered is not present in the option");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsFilterValdns", Status.FAIL,
					"Business Line Filter not Present on Company News Page");
		}

	}

	public void companyNewsLocationFilter() {

		waitUntilElementVisibleBy(driver, selectLocation, 120);

		if (verifyObjectDisplayed(selectLocation)) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsLocationFilter", Status.PASS,
					"Location Present on Company News Page");

			clickElementBy(selectLocation);

			List<WebElement> blOptionsList = driver.findElements(locationOption);

			for (int i = 1; i < blOptionsList.size(); i++) {

				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsLocationFilter", Status.PASS,
						"Location Filter Option #" + i + " is " + blOptionsList.get(i).getText());

			}

			try {
				new Select(driver.findElement(selectLocation)).selectByVisibleText(testData.get("strLocationFilter"));
			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsLocationFilter", Status.FAIL,
						"The filter entered is not present in the option");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			try {
				List<WebElement> cnCardName = driver.findElements(companyNewsTitle);

				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsLocationFilter", Status.PASS,
						"Total " + cnCardName.size() + " post present for " + testData.get("strLocationFilter"));

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsLocationFilter", Status.PASS,
						"No post present for " + testData.get("strLocationFilter"));
			}

			try {
				new Select(driver.findElement(selectLocation)).selectByVisibleText("Select Location");
			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsLocationFilter", Status.FAIL,
						"The filter entered is not present in the option");
			}

			waitForSometime(tcConfig.getConfig().get("LowWait"));

		} else {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsLocationFilter", Status.FAIL,
					"Business Line Filter not Present on Company News Page");
		}

	}

	public void locationAndBusinessFilter() {

		try {
			new Select(driver.findElement(selectBusinessLine)).selectByVisibleText(testData.get("strBLFilter"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "locationAndBusinessFilter", Status.FAIL,
					"The filter entered is not present in the option");
		}

		try {
			new Select(driver.findElement(selectLocation)).selectByVisibleText(testData.get("strLocationFilter"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "locationAndBusinessFilter", Status.FAIL,
					"The filter entered is not present in the option");
		}

		waitForSometime(tcConfig.getConfig().get("LowWait"));
		try {
			List<WebElement> cnCardName = driver.findElements(companyNewsTitle);

			tcConfig.updateTestReporter("NationsCompanyNewsPage", "locationAndBusinessFilter", Status.PASS,
					"Total " + cnCardName.size() + " post present for " + testData.get("strBLFilter") + " and "
							+ testData.get("strLocationFilter"));

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "locationAndBusinessFilter", Status.PASS,
					"No post present for " + testData.get("strBLFilter") + " and " + testData.get("strLocationFilter"));
		}

	}

	public void companyNewsFilterValdns() {
		companyNewsBusinessLineFilter();
		companyNewsLocationFilter();
		locationAndBusinessFilter();

	}

	public void noLikeCountVerification() {

		List<WebElement> likesList = driver.findElements(totalLikes);
		/// List<WebElement> commentsList= driver.findElements(totalComments);
		int tempArticles = likesList.size();
		for (int i = 0; i < tempArticles; i++) {
			List<WebElement> tempLikesList = driver.findElements(totalLikes);
			List<WebElement> tempCommentsList = driver.findElements(totalComments);
			List<WebElement> cnCardTitle = driver.findElements(companyNewsTitle);

			String strLikes = tempLikesList.get(i).getText();
			String strComments = tempCommentsList.get(i).getText();

			strLikes = strLikes.replace("Likes", "").trim();
			strComments = strComments.replace("Comments", "").trim();

			int likesOnArt = Integer.parseInt(strLikes);
			int commentsOnArt = Integer.parseInt(strComments);

			if (likesOnArt == 0) {

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Likes on the Article " + (i + 1) + " is " + strLikes);

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Comments on the Article " + (i + 1) + " is " + strComments);

				cnCardTitle.get(i).click();

				waitUntilElementVisibleBy(driver, printCta, 120);

				if (verifyObjectDisplayed(likeButton)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Like Button Present");

					if (browserName.equalsIgnoreCase("Firefox")) {
						scrollDownForElementJSBy(likeButton);
						scrollUpByPixel(400);
					} else {
						getElementInView(likeButton);
					}

					clickElementJSWithWait(likeButton);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementInVisible(driver, likeButton, 60);
					if (verifyObjectDisplayed(likeButton)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
								"Like Button Displayed after liking the post");
						break;
					} else {
						String strLikes1 = getElementText(articleLike);
						String strComments1 = getElementText(articleComment);

						// strLikes1 = strLikes1.replace("Likes", "").trim();
						// strComments1 = strComments1.replace("Comments",
						// "").trim();

						int likesOnThisArt = Integer.parseInt(strLikes1);
						int commentsOnThisArt = Integer.parseInt(strComments1);

						if ((likesOnArt + 1) == likesOnThisArt && commentsOnArt == commentsOnThisArt) {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter",
									Status.PASS, "Like Count is updated, and Comment Count is the same");
							break;
						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter",
									Status.FAIL, "Error in Comment and Like Count update");
							break;
						}

					}

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Already Liked by the user");
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, companyNewsHeader, 120);
					continue;

				}

			} else {

				continue;
			}

		}

	}

	public void likedCountVerification() {

		List<WebElement> likesList = driver.findElements(totalLikes);
		/// List<WebElement> commentsList= driver.findElements(totalComments);
		int tempArticles = likesList.size();
		for (int i = 0; i < tempArticles; i++) {
			List<WebElement> tempLikesList = driver.findElements(totalLikes);
			List<WebElement> tempCommentsList = driver.findElements(totalComments);
			List<WebElement> cnCardTitle = driver.findElements(companyNewsTitle);

			String strLikes = tempLikesList.get(i).getText();
			String strComments = tempCommentsList.get(i).getText();

			strLikes = strLikes.replace("Likes", "").trim();
			strComments = strComments.replace("Comments", "").trim();

			int likesOnArt = Integer.parseInt(strLikes);
			int commentsOnArt = Integer.parseInt(strComments);

			if (likesOnArt > 0) {

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Likes on the Article " + (i + 1) + " is " + strLikes);

				tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
						"Total Comments on the Article " + (i + 1) + " is " + strComments);

				cnCardTitle.get(i).click();

				waitUntilElementVisibleBy(driver, printCta, 120);

				if (verifyObjectDisplayed(likeButton)) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Like Button Present");
					if (browserName.equalsIgnoreCase("Firefox")) {
						scrollDownForElementJSBy(likeButton);
						scrollUpByPixel(400);
					} else {
						getElementInView(likeButton);
					}

					clickElementJSWithWait(likeButton);

					waitForSometime(tcConfig.getConfig().get("LowWait"));

					waitUntilElementInVisible(driver, likeButton, 60);
					if (verifyObjectDisplayed(likeButton)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.FAIL,
								"Like Button Displayed after liking the post");
						break;
					} else {
						String strLikes1 = getElementText(articleLike);
						String strComments1 = getElementText(articleComment);

						// strLikes1 = strLikes1.replace("Likes", "").trim();
						// strComments1 = strComments1.replace("Comments",
						// "").trim();

						int likesOnThisArt = Integer.parseInt(strLikes1);
						int commentsOnThisArt = Integer.parseInt(strComments1);

						if ((likesOnArt + 1) == likesOnThisArt && commentsOnArt == commentsOnThisArt) {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter",
									Status.PASS, "Like Count is updated, and Comment Count is the same");
							break;
						} else {
							tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter",
									Status.FAIL, "Error in Comment and Like Count update");
							break;
						}

					}

				} else {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "editPostDataEnter", Status.PASS,
							"Already Liked by the user");
					driver.navigate().back();
					waitUntilElementVisibleBy(driver, companyNewsHeader, 120);
					continue;

				}

			} else {

				continue;
			}

		}

	}

}
