package nations.pages;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsSearchResultPage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsSearchResultPage.class);

	public NationsSearchResultPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	protected By searchResultsHeader = By.xpath("//h1[contains(.,'Search results')]");
	protected By searchResultCount = By
			.xpath("//div[@class='wyn-l-medium-wrapper--space wyn-l-content wyn-l-padding-medium--top']");
	protected By searchResultsFor = By
			.xpath("//div[@class='wyn-l-medium-wrapper--space wyn-l-content wyn-l-padding-medium--top']/b");
	protected By searchResultsTitle = By.xpath("//div[@class='wyn-aem-search__results']/a");
	protected By pagination = By.xpath("//div[@class='wyn-aem-search__pagination']");

	public void searchResultsValidatins() {

		waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

		if (verifyObjectDisplayed(searchResultsHeader)) {
			tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");

			String searchURL = driver.getCurrentUrl();
			String URL = testData.get("urlSearchPage");
			if (searchURL.contains(URL)) {

				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.FAIL,
						"Nations Search Result page long URL displayed.The url is : " + searchURL);

			}

			else {
				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"Nations Search Result page displayed with correct URL.The url is : " + searchURL);
			}

			String strResultsCount = getElementText(searchResultCount);
			String strResultsFor = getElementText(searchResultsFor);

			if (strResultsCount.toUpperCase().contains("did not match any documents")) {
				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"No Results generated for searched keyword: " + strResultsFor);
			} else {

				String strResults = StringUtils.substringBetween(strResultsCount, "Results 1", "for");
				String[] strTotalResult = strResults.trim().split("of");
				strResults = strTotalResult[strTotalResult.length - 1];
				tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
						"Total Results: " + strResults + " for" + strResultsFor);

				List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"No Of Results on first page " + listOfResults.size());

				if (listOfResults.size() >= 10) {
					if (verifyObjectDisplayed(pagination)) {
						tcConfig.updateTestReporter("NationsHomePage", "searchResultsValidatins", Status.PASS,
								"Pagination Available on the page");
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "searchResultsValidatins", Status.FAIL,
								"Pagination Not available on the page");
					}
				} else {
					System.out.println("Less than 10 results hence no pagination");
				}

				String firstTitle = listOfResults.get(0).getText();
				if (firstTitle.contains("-")) {
					firstTitle = firstTitle.replace("-", " ");
				} else {

				}

				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"First Result title " + firstTitle);

				listOfResults.get(0).click();

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getTitle().toUpperCase().contains(firstTitle.toUpperCase())) {
					tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
							"Navigated to first Search related page " + firstTitle);
				} else {
					tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.FAIL,
							"Failed to Navigate to first Search related page " + firstTitle);
				}

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

			}

		} else {
			tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

}
