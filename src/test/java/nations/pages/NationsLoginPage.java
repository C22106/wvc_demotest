package nations.pages;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsLoginPage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsLoginPage.class);

	public NationsLoginPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By welcomeHeader = By.xpath("//h1[contains(.,'Welcome')]");
	protected By stockTicker = By.xpath("//div[@class='wyn-stock-ticker']");
	protected By loginImage = By.xpath("//img[@class='cq-dd-image']");
	protected By loginHeroImage = By.xpath("//div[@class='column wyn-login__logo wyn-object-fit-ie']");
	// protected By
	// seviceDeskMail=By.xpath("//a[contains(.,'servicedesk@wyn.com')]");
	// protected By helpNumber=By.xpath("//a[contains(.,'304-8080')]");
	protected By loginBtn = By.xpath("//a[contains(.,'Login')]");
	protected By nationsHeroImage = By.xpath("//img[@alt='Hero banner']");

	protected By acceptButton = By.xpath("//button[contains(.,'Accept')]");
	protected By nationsLogo = By.xpath("//span[@class='wyn-header__logo-label']");
	protected By homeBreadcrumb = By.xpath("//div[@class='breadcrumb']//a[contains(.,'Home')]");
	protected By nationsUserName = By.id("okta-signin-username");
	protected By nationsPassword = By.id("okta-signin-password");
	protected By signInBtn = By.id("okta-signin-submit");
	protected By loginPageLogo = By.xpath("//div[@class='okta-sign-in-header auth-header']");
	protected By profileName = By.xpath("//div[@class='wyn-nation-header__profile']//a");
	protected By logOutTab = By.xpath("//div[@class='column wyn-type-body-2']/a[contains(.,'Log out')]");
	protected By oktaProfile = By.xpath("//div[@data-se='user-menu']");
	protected By signOff = By.xpath("//p[@class='option-title' and contains(.,'Sign out')]");

	public void setUserName(String strUserName) throws Exception {
		driver.findElement(nationsUserName).click();
		driver.findElement(nationsUserName).clear();
		waitUntilElementVisibleBy(driver, nationsUserName, 120);
		driver.findElement(nationsUserName).sendKeys(strUserName);
		tcConfig.updateTestReporter("NationsLoginPage", "setUserName", Status.PASS, "Username is entered for Login");
	}

	/**
	 * enter password
	 * 
	 * @param strPassword
	 * @throws Exception
	 */
	public void setPassword(String strPassword) throws Exception {

		byte[] decodedString = DatatypeConverter.parseBase64Binary(strPassword);
		String dString = new String(decodedString, "UTF-8");
		driver.findElement(nationsPassword).clear();
		driver.findElement(nationsPassword).sendKeys(dString);
		tcConfig.updateTestReporter("NationsLoginPage", "setPassword", Status.PASS, "Entered password for login");
	}

	public void loginToApplication() throws Exception {

		try {
			/*
			 * waitUntilObjectVisible(driver, welcomeHeader, 5);
			 * waitForSometime(tcConfig.getConfig().get("MedWait")); if
			 * (verifyObjectDisplayed(welcomeHeader)) {
			 * tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication",
			 * Status.PASS, "Navigated to Nations Login Page"); if
			 * (verifyObjectDisplayed(loginBtn)) {
			 * tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication",
			 * Status.PASS, "Login Button Present"); clickElementBy(loginBtn);
			 * 
			 * } else { tcConfig.updateTestReporter("NationsLoginPage",
			 * "loginToApplication", Status.FAIL, "Login button not present"); }
			 */

			waitUntilObjectVisible(driver, loginPageLogo, 120);
			if (verifyObjectDisplayed(loginPageLogo)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"Navigated to Okta Login Page");

				setUserName(testData.get("strUserName"));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				setPassword(testData.get("strPassword"));

				clickElementBy(signInBtn);

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.FAIL,
						"Navigation to login page failed");
			}

			/*
			 * } else if (verifyObjectDisplayed(nationsLogo)) {
			 * System.out.println("No Login Required"); }else {
			 * tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication",
			 * Status.FAIL, "Failed to Navigate to Nations Login Page"); }
			 */

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
					"Okta Login is Not required");
		}
	}

	public void launchApplication(){
		driver.navigate().to(testData.get("URL"));
		String urlCheck = testData.get("URL");
		// driver.manage().window().maximize();

		try {
			loginToApplication();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		waitUntilElementVisibleBy(driver, nationsLogo, 120);

		if (verifyObjectDisplayed(nationsLogo)) {
			if (urlCheck.toUpperCase().contains("QA")) {
				System.out.println("Testing in QA link");
			} else if (urlCheck.toUpperCase().contains("PROD")) {
				System.out.println("Testing in PROD link");
			}
			tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
					"Wyndham Nations Navigation Successful");

			try {
				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementBy(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No T&C Button");
			}

		} else {
			tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.FAIL,
					"Wyndham Nations Navigation Unsuccessful");
		}

	}

	public void navigateToHomepage(String strHome) {

		if (strHome.equalsIgnoreCase("logo")) {
			tcConfig.updateTestReporter("NationsLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage");

			clickElementBy(nationsLogo);

			waitUntilObjectVisible(driver, nationsHeroImage, 120);

			if (verifyObjectDisplayed(nationsHeroImage)) {

				tcConfig.updateTestReporter("NationsLoginPage", "navigateToHomepage", Status.PASS,
						"Wyndham Nations Navigation Successful");

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "navigateToHomepage", Status.FAIL,
						"Wyndham Nations Navigation Unsuccessful");
			}

		} else if (strHome.equalsIgnoreCase("breadcrumb")) {
			tcConfig.updateTestReporter("NationsLoginPage", "navigateToHomepage", Status.PASS,
					"Navigating Back To Homepage");

			clickElementBy(homeBreadcrumb);

			waitUntilObjectVisible(driver, nationsHeroImage, 120);

			if (verifyObjectDisplayed(nationsHeroImage)) {

				tcConfig.updateTestReporter("NationsLoginPage", "navigateToHomepage", Status.PASS,
						"Wyndham Nations Navigation Successful");

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "navigateToHomepage", Status.FAIL,
						"Wyndham Nations Navigation Unsuccessful");
			}
		} else {
			tcConfig.updateTestReporter("NationsLoginPage", "navigateToHomepage", Status.FAIL,
					"Unknown keyword entered, Failed to navigate to homepage");

		}

	}

	public void wynNetworkVal() {

		driver.navigate().to(testData.get("URL"));
		tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
				"Navigating Again to Nations Login URL");

		try {
			waitUntilElementVisibleBy(driver, loginBtn, 20);

			if (verifyObjectDisplayed(loginBtn)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
						"Navigated to Nations Login Page");
				if (verifyObjectDisplayed(loginBtn)) {
					tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
							"Login Button Present");
					clickElementBy(loginBtn);

				}

			}

		} catch (Exception e) {
			System.out.println("Login Click Not Required");
		}

		try {
			waitUntilElementVisibleBy(driver, nationsLogo, 120);

			if (verifyObjectDisplayed(nationsLogo)) {

				tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
						"Wyndham Nations Navigation Successful, without Login");

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.FAIL,
						"Wyndham Nations Navigation Unsuccessful, without login");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.FAIL,
					"Wyndham Nations Navigation Unsuccessful, without login");
		}

	}

	public void loginPageValidations() throws Exception{ 
		driver.navigate().to(testData.get("URL"));
		// String urlCheck = testData.get("URL");
		// driver.manage().window().maximize();

		waitUntilElementVisibleBy(driver, loginBtn, 120);
		if (verifyObjectDisplayed(welcomeHeader)) {
			tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.PASS,
					"Navigated to Nations Login Page, Welcome Title present");
			if (verifyObjectDisplayed(stockTicker)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.PASS,
						"Stock Ticker Present on the page");

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.FAIL,
						"Stock Ticker not Present on the page");
			}

			if (verifyObjectDisplayed(loginHeroImage)) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.PASS,
						"Login Image present on the page");

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.FAIL,
						"Login Image not present on the page");
			}

			String strServiceMail = testData.get("strServiceEmail");
			String strHelpNo = testData.get("strHelpNo");

			if (verifyObjectDisplayed(By.xpath("//a[contains(.,'" + strServiceMail + "')]"))) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.PASS,
						"Service Desk Email present as: " + strServiceMail);

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.FAIL,
						"Service desk email not present");
			}

			if (verifyObjectDisplayed(By.xpath("//p[contains(.,'" + strHelpNo + "')]"))) {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.PASS,
						"Help Desk No. present as: " + strHelpNo);

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.FAIL,
						"Help Desk No not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsLoginPage", "loginPageValidations", Status.PASS,
					"Welcome Title not present, Directly Logged In");
		}

		loginToApplication();

		waitUntilElementVisibleBy(driver, nationsLogo, 120);

		if (verifyObjectDisplayed(nationsLogo)) {

			tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
					"Wyndham Nations Navigation Successful");

			try {
				if (verifyObjectDisplayed(acceptButton)) {
					tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
							"Accept Cookies Button Present");

					clickElementBy(acceptButton);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				}

			} catch (Exception e) {
				System.out.println("No T&C Button");
			}

		} else {
			tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.FAIL,
					"Wyndham Nations Navigation Unsuccessful");
		}

	}

	public void logoutNations() {
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		waitUntilElementVisibleBy(driver, profileName, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (browserName.equalsIgnoreCase("Chrome")) {
			driver.switchTo().activeElement().sendKeys(Keys.HOME);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "User Name Visible");
			clickElementBy(profileName);
			waitUntilElementVisibleBy(driver, logOutTab, 120);
			if (verifyObjectDisplayed(logOutTab)) {

				tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "Log Out Tab Present");

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				clickElementBy(logOutTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				try {
					waitUntilObjectVisible(driver, oktaProfile, 20);
					if (verifyObjectDisplayed(oktaProfile)) {
						clickElementBy(oktaProfile);

						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementBy(signOff);
						waitForSometime(tcConfig.getConfig().get("MedWait"));
					}
				} catch (Exception e) {
					System.out.println("No need to sign off");
				}

				waitUntilObjectVisible(driver, loginPageLogo, 120);

				if (verifyObjectDisplayed(loginPageLogo)) {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "Logout Successful");

				} else {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.FAIL,
							"Navigation to login page failed, Logout Unsuccessfully");
				}

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.FAIL,
						"Log Out Tab not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsLoginPage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void directLogin() {
		waitUntilObjectVisible(driver, loginPageLogo, 120);
		if (verifyObjectDisplayed(loginPageLogo)) {
			tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.PASS,
					"Navigated to Okta Login Page");

			try {
				setUserName(testData.get("strUserName"));

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				setPassword(testData.get("strPassword"));

			} catch (Exception e) {

				e.printStackTrace();
			}
			clickElementBy(signInBtn);

			waitUntilElementVisibleBy(driver, nationsLogo, 120);

			if (verifyObjectDisplayed(nationsLogo)) {

				tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
						"Wyndham Nations Navigation Successful");

				try {
					if (verifyObjectDisplayed(acceptButton)) {
						tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.PASS,
								"Accept Cookies Button Present");

						clickElementBy(acceptButton);
						waitForSometime(tcConfig.getConfig().get("LowWait"));
					}

				} catch (Exception e) {
					System.out.println("No T&C Button");
				}

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "launchApplication", Status.FAIL,
						"Wyndham Nations Navigation Unsuccessful");
			}

		} else {
			tcConfig.updateTestReporter("NationsLoginPage", "loginToApplication", Status.FAIL,
					"Navigation to login page failed");
		}
	}

	public void clearCache() {

	}
}
