package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsResourcesPage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsResourcesPage.class);

	public NationsResourcesPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By resourcesTab = By.xpath("//a[contains(.,'Resources')]");
	protected By compliancePolicies = By.xpath("//li[@class='has-submenu']//a[contains(.,'Compliance')]");
	protected By complianceResCTA = By
			.xpath("//li[@class='has-submenu is-active']//a[contains(.,'Compliance Resource')]");
	protected By complianceResHeader = By.xpath("//h1[contains(.,'Compliance Resource')]");
	protected By accordianVal = By.xpath("//div[@class='accordion']");
	protected By accordianTabs = By.xpath("//div[@class='wyn-accordion']//button");
	protected By activeTab = By.xpath("//div[@class='wyn-js-accordion wyn-accordion__trigger is-active']");

	// protected By compliancePoliciesTab =
	// By.xpath("//li[@class='has-submenu']//a[contains(.,'Compliance')]");

	public void complianceResNav() {

		try {
			waitUntilElementVisibleBy(driver, resourcesTab, 120);
		} catch (Exception e) {
			waitForSometime(tcConfig.getConfig().get("MedWait"));
		}
		if (verifyObjectDisplayed(resourcesTab)) {

			clickElementBy(resourcesTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, complianceResCTA, 120);
			if (verifyObjectDisplayed(complianceResCTA)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
						"Compliance Resource Tab present on the homepage");
				clickElementBy(complianceResCTA);

			} else if (verifyObjectDisplayed(compliancePolicies)) {

				clickElementBy(compliancePolicies);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, complianceResCTA, 120);
				if (verifyObjectDisplayed(complianceResCTA)) {
					tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
							"Compliance Resource Tab present on the homepage");
					clickElementBy(complianceResCTA);

				} else {
					tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
							"Compliance Resource Tab not present on the homepage");
				}

			}

			waitUntilElementVisibleBy(driver, complianceResHeader, 120);
			if (verifyObjectDisplayed(complianceResHeader)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
						"Navigated to Compliance Resource page, Header and Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
						"Navigation to Compliance Resources page failed, Header not present");
			}
		} else if (verifyObjectDisplayed(compliancePolicies)) {

			clickElementJSWithWait(compliancePolicies);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, complianceResCTA, 120);
			if (verifyObjectDisplayed(complianceResCTA)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
						"Compliance Resource Tab present on the homepage");
				clickElementJSWithWait(complianceResCTA);

			} else {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
						"Compliance Resource Tab not present on the homepage");
			}
		} else {
			tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
					"Compliance Resource Tab not present on the homepage");
		}

	}

	public void accordionTabsValidations() {

		waitUntilElementVisibleBy(driver, accordianVal, 120);

		if (verifyObjectDisplayed(accordianVal)) {
			tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.PASS,
					"Accordion is present on the page");

			List<WebElement> accordionList = driver.findElements(accordianTabs);

			tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.PASS,
					"Total Number of Tabs present is: " + accordionList.size());

			accordionList.get(0).click();
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			if (verifyObjectDisplayed(activeTab)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.PASS,
						"First accordion is opened and is active, with title: " + accordionList.get(0).getText());

			} else {
				tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.FAIL,
						"First accordion is not active");
			}

			if (accordionList.size() > 1) {
				scrollDownForElementJSWb(accordionList.get(1));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(300);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				accordionList.get(1).click();
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				List<WebElement> activeTabList = driver.findElements(activeTab);

				if (verifyElementDisplayed(activeTabList.get(1))) {
					tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.PASS,
							"Second accordion is opened and is active, with title: " + accordionList.get(1).getText());

					accordionList.get(1).click();
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					List<WebElement> activeTab2List = driver.findElements(activeTab);

					if (activeTab2List.size() <= 1) {
						tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.PASS,
								"Second accordion is closed");

					} else {
						tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.FAIL,
								"Second Accordion could not be closed");
					}

					scrollDownForElementJSWb(accordionList.get(0));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(300);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

				} else {
					tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.FAIL,
							"Second accordion is not active");
				}

			}

			clickElementJSWithWait(accordionList.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			// List<WebElement> activeTab2List = driver.findElements(activeTab);
			if (!verifyObjectDisplayed(activeTab)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.PASS,
						"First accordion is closed");

			} else {
				tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.FAIL,
						"First Accordion could not be closed");
			}

		} else {
			tcConfig.updateTestReporter("NationsResourcesPage", "accordionTabsValidations", Status.FAIL,
					"Accordion is not present on the page");
		}

	}

}
