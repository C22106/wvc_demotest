package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsHomePage_Android extends NationsHomePage {

	public static final Logger log = Logger.getLogger(NationsHomePage_Android.class);

	public NationsHomePage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		logOutTab = By.xpath("//span[@class='wyn-type-body-2']//a[contains(.,'Log out')]");

		profileName = By.xpath(
				"//*[contains(@class,'wyn-nation-header__navigation__profile-mobile')]//*[@class='wyn-my-profile-link']");
	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public By alertClose = By
			.xpath("//div[contains(@class,'wyn-alert-message')]//a[@class='wyn-alert-message__close']");

	public void userProfileName() {
		waitUntilElementVisibleBy(driver, profileName, 120);

		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS,
					"User Logged In: " + getElementText(profileName));

			try {
				scrollDownForElementJSBy(alertPresence);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (verifyObjectDisplayed(alertPresence)) {
				tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "Alert Present");
				try {
					driver.findElement(alertClose).click();
					clickElementJSWithWait(alertClose);
					// Actions actionbld = new Actions(driver);
					// actionbld.moveToElement().click().perform();
					// actionbld.click(driver.findElement(By.xpath("(//*[local-name()='svg']//*[local-name()='use'])[8]"))).build().perform();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "Alert Closed");
				} catch (Exception e) {
					System.out.println(e);
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "Alert not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void logOutMyProfileVal() {
		waitUntilElementVisibleBy(driver, profileName, 120);
		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS, "User Name Visible");

			waitUntilElementVisibleBy(driver, logOutTab, 120);
			if (verifyObjectDisplayed(logOutTab)) {
				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS, "Log Out Tab Present");
				clickElementJSWithWait(logOutTab);
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				try {
					waitUntilObjectVisible(driver, oktaProfile, 20);
					if (verifyObjectDisplayed(oktaProfile)) {
						clickElementBy(oktaProfile);

						waitForSometime(tcConfig.getConfig().get("MedWait"));
						clickElementJSWithWait(signOff);
						waitForSometime(tcConfig.getConfig().get("MedWait"));

					}
				} catch (Exception e) {
					System.out.println("No need to sign off");
				}

				waitUntilObjectVisible(driver, loginPageLogo, 120);
				if (verifyObjectDisplayed(loginPageLogo)) {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "Logout Successful");

				} else {
					tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.FAIL,
							"Navigation to login page failed, Logout Unsuccessfully");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.FAIL, "Log Out Tab not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void searchField() {

		waitUntilObjectVisible(driver, searchContainer, 120);

		List<WebElement> l = driver.findElements(alertBar);
		if (l.size() > 0) {
			clickElementJSWithWait(l.get(l.size() - 1));

		}

		if (verifyObjectDisplayed(searchContainer)) {

			// fieldDataEnter(searchContainer, testData.get("strSearch"));

			driver.findElement(searchContainer).click();
			driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.PASS, "Search Keyword Entered");

			clickElementBy(searchButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.FAIL,
					"Error in getting the search container");
		}

	}

	public void footerSocialIcons() {

		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());

			for (int i = 0; i < socialIconsList.size(); i++) {
				String strSiteName = null;

				switch (i) {
				case 0:
					strSiteName = "FACEBOOK";
					break;
				case 1:
					strSiteName = "TWITTER";
					break;
				case 2:
					strSiteName = "INSTAGRAM";
					break;
				case 3:
					strSiteName = "YOUTUBE";
					break;
				case 4:
					strSiteName = "LINKED";
					break;

				default:
					tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}
				socialIconsList = driver.findElements(footerSocialIcons);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				String strUrl = driver.getCurrentUrl();
				scrollDownForElementJSBy(footerSocialIcons);
				clickElementJSWithWait(socialIconsList.get(i));

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				driver.get(strUrl);

			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}

	}

	public void footerWynLinks() {
		openHamburgerMenu();
		String strUrl = driver.getCurrentUrl();
		List<WebElement> we = driver.findElements(footerWynPages);
		for (int i = 0; i < we.size(); i++) {
			if (we.get(i).getText().equalsIgnoreCase("Company Information")
					|| we.get(i).getText().equalsIgnoreCase("Compliance & Policies")
					|| we.get(i).getText().equalsIgnoreCase("Learning & Career Development")
					|| we.get(i).getText().equalsIgnoreCase("Resources")
					|| we.get(i).getText().equalsIgnoreCase("Stay Engaged")) {
				we.get(i).click();
			}
		}

		if (verifyObjectDisplayed(footerWynPages)) {
			List<WebElement> wynLinksList = driver.findElements(footerWynPages);
			tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
					"Wyndham Nations Sub Links Present in the Footer,Total: " + wynLinksList.size());

			if (verifyObjectDisplayed(footerCorporateFacilities)) {
				tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
						"Clicking on one of the Link to verify, Corporate Facilities");

				clickElementJSWithWait(footerCorporateFacilities);
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getCurrentUrl().toUpperCase().contains("CORPORATE")) {
					tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
							"Corporate Facilities associated Link opened");

					driver.get(strUrl);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
							"Corporate Facilities Associated Link opening error");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
						"Wyndham Hotels & Resorts Link not present in footer");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
					"Wyndham Destiantion Sub Links not present in the Footer");
		}

	}

	public void imageVideoOpenVal() {

		String strEnv = testData.get("URL").toUpperCase();

		NationsLoginPage login = new NationsLoginPage(tcConfig);
		if (strEnv.contains("QA")) {
			driver.navigate().to(testData.get("QaImageURL"));
			// driver.manage().window().maximize();

			waitUntilElementVisibleBy(driver, pageContent, 120);

			if (verifyObjectDisplayed(postHeaderVer)) {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
						"Article Page Navigated");

				try {
					List<WebElement> imageList = driver.findElements(articleImage);
					if (verifyElementDisplayed(imageList.get(0))) {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
								"Image/Video Present in the article");

						imageList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						List<WebElement> openedArticle = driver.findElements(articleCloseCTA);
						if (verifyElementDisplayed(openedArticle.get(0))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}

						} else if (verifyElementDisplayed(openedArticle.get(1))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
						} else {

							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
									"Image could not be opened when clicked");
						}
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
								"Image/Video not Present in the article");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
							"Image/Video not Present in the article");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
						"Article Page Navigation failed");

			}

		} else if (strEnv.contains("STAGE")) {

			driver.navigate().to(testData.get("StageImageURL"));
			// driver.manage().window().maximize();
			waitUntilElementVisibleBy(driver, pageContent, 120);

			if (verifyObjectDisplayed(postHeaderVer)) {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
						"Article Page Navigated");

				try {
					List<WebElement> imageList = driver.findElements(articleImage);
					if (verifyElementDisplayed(imageList.get(0))) {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
								"Image/Video Present in the article");

						imageList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));
						List<WebElement> openedArticle = driver.findElements(articleCloseCTA);
						if (verifyElementDisplayed(openedArticle.get(0))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}

						} else if (verifyElementDisplayed(openedArticle.get(1))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
						} else {

							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
									"Image could not be opened when clicked");
						}
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
								"Image/Video not Present in the article");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
							"Image/Video not Present in the article");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
						"Article Page Navigation failed");

			}

		} else if (strEnv.contains("WWW")) {

			driver.navigate().to(testData.get("ProdImageURL"));
			driver.manage().window().maximize();

			waitUntilElementVisibleBy(driver, pageContent, 120);

			if (verifyObjectDisplayed(postHeaderVer)) {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
						"Article Page Navigated");

				try {
					List<WebElement> imageList = driver.findElements(articleImage);
					if (verifyElementDisplayed(imageList.get(0))) {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
								"Image/Video Present in the article");

						imageList.get(0).click();
						waitForSometime(tcConfig.getConfig().get("LowWait"));

						List<WebElement> openedArticle = driver.findElements(articleCloseCTA);
						if (verifyElementDisplayed(openedArticle.get(0))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}

						} else if (verifyElementDisplayed(openedArticle.get(1))) {
							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.PASS,
									"Image is opened in the article");

							List<WebElement> closearticle = driver.findElements(articleCloseCTA);
							try {
								closearticle.get(0).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							} catch (Exception e) {
								closearticle.get(1).click();
								waitForSometime(tcConfig.getConfig().get("LowWait"));
							}
						} else {

							tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
									"Image could not be opened when clicked");
						}
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
								"Image/Video not Present in the article");
					}

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
							"Image/Video not Present in the article");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "imageVideoOpenVal", Status.FAIL,
						"Article Page Navigation failed");

			}

		}

	}

}
