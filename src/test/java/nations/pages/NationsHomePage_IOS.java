package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsHomePage_IOS extends NationsHomePage {

	public static final Logger log = Logger.getLogger(NationsHomePage_IOS.class);

	public NationsHomePage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		logOutTab = By.xpath("//span[@class='wyn-type-body-2']//a[contains(.,'Log out')]");

		profileName = By.xpath(
				"//*[contains(@class,'wyn-nation-header__navigation__profile-mobile')]//*[@class='wyn-my-profile-link']");
	}

	String name = null;
	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public By profileName_Ipad = By.xpath("//div[@class='wyn-nation-header__profile']//a");
	public By alertClose = By
			.xpath("//div[contains(@class,'wyn-alert-message')]//a[@class='wyn-alert-message__close']");

	public void userProfileName() {

		// waitUntilElementVisibleBy(driver, profileName, 120);
		waitForSometime(tcConfig.getConfig().get("MedWait"));
		if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
			name = getElementText(profileName_Ipad);
			String[] Updatedname = name.split(",");
			name = Updatedname[1].trim();
		} else {
			name = getElementText(profileName);
		}

		if (verifyObjectDisplayed(profileName) || verifyObjectDisplayed(profileName_Ipad)) {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "User Logged In: " + name);

			try {
				scrollDownForElementJSBy(alertPresence);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (verifyObjectDisplayed(alertPresence)) {
				tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "Alert Present");
				try {
					driver.findElement(alertClose).click();
					clickElementJSWithWait(alertClose);
					// Actions actionbld = new Actions(driver);
					// actionbld.moveToElement().click().perform();
					// actionbld.click(driver.findElement(By.xpath("(//*[local-name()='svg']//*[local-name()='use'])[8]"))).build().perform();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "Alert Closed");
				} catch (Exception e) {
					System.out.println(e);
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.PASS, "Alert not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	protected By logOutTab_ipad = By.xpath("//div[@class='column wyn-type-body-2']/a[contains(.,'Log out')]");

	public void logOutMyProfileVal() {

		waitForSometime(tcConfig.getConfig().get("MedWait"));
		// waitUntilElementVisibleBy(driver, profileName, 120);
		if (verifyObjectDisplayed(profileName) || verifyObjectDisplayed(profileName_Ipad)) {
			tcConfig.updateTestReporter("NationsHomePage", "logoutNations", Status.PASS, "User Name Visible");
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
				clickElementBy(profileName_Ipad);
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString().contains("iPad")) {
				try {
					clickIOSElement("Log out", 1, logOutTab);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				clickElementJSWithWait(logOutTab);
			}
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			try {
				waitUntilObjectVisible(driver, oktaProfile, 20);
				if (verifyObjectDisplayed(oktaProfile)) {
					clickElementBy(oktaProfile);

					waitForSometime(tcConfig.getConfig().get("MedWait"));
					clickElementJSWithWait(signOff);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				}
			} catch (Exception e) {
				System.out.println("No need to sign off");
			}

			waitUntilObjectVisible(driver, loginPageLogo, 240);
			if (verifyObjectDisplayed(loginPageLogo)) {
				tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.PASS, "Logout Successful");

			} else {
				tcConfig.updateTestReporter("NationsLoginPage", "logoutNations", Status.FAIL,
						"Navigation to login page failed, Logout Unsuccessfully");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "userProfileName", Status.FAIL,
					"User profile Name not present");
		}

	}

	public void searchField() {

		waitUntilObjectVisible(driver, searchContainer, 120);

		List<WebElement> l = driver.findElements(alertBar);
		if (l.size() > 0) {
			clickElementJSWithWait(l.get(l.size() - 1));

		}

		if (verifyObjectDisplayed(searchContainer)) {

			// fieldDataEnter(searchContainer, testData.get("strSearch"));

			driver.findElement(searchContainer).click();
			driver.findElement(searchContainer).sendKeys(testData.get("strSearch"));

			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.PASS, "Search Keyword Entered");

			clickElementBy(searchButton);

			waitForSometime(tcConfig.getConfig().get("MedWait"));

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "searchField", Status.FAIL,
					"Error in getting the search container");
		}

	}

	public void footerSocialIcons() {

		String URL = driver.getCurrentUrl();
		if (verifyObjectDisplayed(footerSocialIcons)) {

			List<WebElement> socialIconsList = driver.findElements(footerSocialIcons);
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons Present in the Footer,Total: " + socialIconsList.size());

			for (int i = 0; i < socialIconsList.size(); i++) {
				String strSiteName = null;
				String imagePath = null;

				switch (i) {
				case 0:
					strSiteName = "FACEBOOK";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/facebooklogo1.png";
					} else if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						imagePath = "PUBLIC:/automation_images/facebooklogo_ipad1.png";
					}
					break;
				case 1:
					strSiteName = "TWITTER";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/twitterlogo1.png";
					} else if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						imagePath = "PUBLIC:/automation_images/twitterlogo_ipad1.png";
					}
					break;
				case 2:
					strSiteName = "INSTAGRAM";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/instalogo1.png";
					} else if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						imagePath = "PUBLIC:/automation_images/instalogo_ipad1.png";
					}
					break;
				case 3:
					strSiteName = "YOUTUBE";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/youtubelogo1.png";
					} else if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						imagePath = "PUBLIC:/automation_images/youtubelogo_ipad1.png";
					}
					break;
				case 4:
					strSiteName = "LINKED";
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPhone")) {
						imagePath = "PUBLIC:/automation_images/linkedinlogo1.png";
					} else if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						imagePath = "PUBLIC:/automation_images/linkedinlogo_ipad1.png";
					}
					break;

				default:
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Invalid Social Site");
					break;
				}

				try {
					scrollDownForElementJSWb(driver.findElements(footerSocialIcons).get(1));
					if (((RemoteWebDriver) driver).getCapabilities().getCapability("model").toString()
							.contains("iPad")) {
						scrollUpByPixel(200);
					}
					clickIOSImage(imagePath, socialIconsList.get(i));

					waitForSometime(tcConfig.getConfig().get("LongWait"));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (driver.getCurrentUrl().toUpperCase().contains(strSiteName)) {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
							"Wyndham " + strSiteName + " page is displayed");

				} else {
					tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.FAIL,
							"Wyndham " + strSiteName + " page is not displayed");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));
				driver.get(URL);
				waitForSometime(tcConfig.getConfig().get("MedWait"));
			}

		} else {
			tcConfig.updateTestReporter("FooterLinksPage", "footerSocialIcons", Status.PASS,
					"Social Icons are not present in the Footer");
		}

	}

	public void footerWynLinks() {

		List<WebElement> we = driver.findElements(footerWynPages);
		for (int i = 0; i < we.size(); i++) {
			if (we.get(i).getText().equalsIgnoreCase("Company Information")
					|| we.get(i).getText().equalsIgnoreCase("Compliance & Policies")
					|| we.get(i).getText().equalsIgnoreCase("Learning & Career Development")
					|| we.get(i).getText().equalsIgnoreCase("Resources")
					|| we.get(i).getText().equalsIgnoreCase("Stay Engaged")) {
				we.get(i).click();

			}
		}

		if (verifyObjectDisplayed(footerWynPages)) {
			List<WebElement> wynLinksList = driver.findElements(footerWynPages);
			tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
					"Wyndham Nations Sub Links Present in the Footer,Total: " + wynLinksList.size());

			if (verifyObjectDisplayed(footerCorporateFacilities)) {
				tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
						"Clicking on one of the Link to verify, Corporate Facilities");

				clickElementJSWithWait(footerCorporateFacilities);
				waitForSometime(tcConfig.getConfig().get("LongWait"));

				// ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());

				/*
				 * if (tabs2.size() == 2) { driver.switchTo().window(tabs2.get(1));
				 * waitForSometime(tcConfig.getConfig().get("LongWait")); if
				 * (driver.getTitle().toUpperCase().contains("CORPORATE")) {
				 * tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
				 * "Corporate Facilities associated Link opened"); driver.close();
				 * waitForSometime(tcConfig.getConfig().get("MedWait"));
				 * driver.switchTo().window(tabs2.get(0));
				 * 
				 * } else { tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks",
				 * Status.FAIL, "Corporate Facilities Associated Link opening error");
				 * 
				 * } } else {
				 */
				if (driver.getCurrentUrl().toUpperCase().contains("CORPORATE")) {
					tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.PASS,
							"Corporate Facilities associated Link opened");

					driver.navigate().back();
					waitForSometime(tcConfig.getConfig().get("MedWait"));

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
							"Corporate Facilities Associated Link opening error");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
						"Wyndham Hotels & Resorts Link not present in footer");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerWynLinks", Status.FAIL,
					"Wyndham Destiantion Sub Links not present in the Footer");
		}

	}

	public void homepageCompanyNews() {

		waitUntilElementVisibleBy(driver, hpCompanyNewsHeader, 120);

		if (verifyObjectDisplayed(hpCompanyNewsHeader)) {
			tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
					"Company News Header Present on the Homepage");

			List<WebElement> cnCardsList = driver.findElements(hpCompanyNewsCards);
			List<WebElement> cnCardImg = driver.findElements(hpCompanyNewsImg);
			List<WebElement> cnCardDate = driver.findElements(hpCompanyNewsDate);
			List<WebElement> cnCardTitle = driver.findElements(hpCompanyNewsTitle);
			List<WebElement> cnCardLikes = driver.findElements(hpCompanyNewsLikes);
			List<WebElement> cnCardComments = driver.findElements(hpCompanyNewsComments);

			tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
					"Total Cards Present in the Company News: " + cnCardsList.size());
			scrollDownByPixel(400);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (cnCardsList.size() == cnCardImg.size()) {
				if (cnCardsList.size() == cnCardDate.size()) {
					if (cnCardsList.size() == cnCardTitle.size()) {
						/*if (cnCardsList.size() == cnCardLikes.size()) {
							if (cnCardsList.size() == cnCardComments.size()) {
								tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
										"Images, Date, Title, Likes and Comments present for each card in Company News");
							} else {
								tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
										"Comments not present for all the Cards in Company News");
							}
						} else {
							tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
									"Likes not present for all the Cards in Company News");
						}*/
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
								"Title not present for all the Cards in Company News");
					}
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Date not present for all the Cards in Company News");
				}
			} else {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
						"Images not present for all the Cards in Company News");
			}

			try {

				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
						"Card Title: " + cnCardTitle.get(0).getText());
				String firstCardTitle = cnCardTitle.get(0).getText().trim();

				cnCardTitle.get(0).click();

				try {
					waitUntilElementVisibleBy(driver, pageContent, 120);
				} catch (Exception e) {
					System.out.println("");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (driver.getTitle().contains(firstCardTitle)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
							"Navigated to first card related page in Company news");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Failed to navigate to first card related page");
				}

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, hpCompanyNewsHeader, 120);

				if (verifyObjectDisplayed(hpCompanyNewsHeader)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
							"Navigated back to homepage");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Failed to navigate to homepage");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
						"Error in clicking first card");
			}
			waitUntilElementVisibleBy(driver, hpCompanyNewsSeeMoreBtn, 120);
			if (verifyObjectDisplayed(hpCompanyNewsSeeMoreBtn)) {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
						"Company News See More button present on the homepage");

				waitUntilElementVisibleBy(driver, hpCompanyNewsHeader, 120);

				List<WebElement> l = driver.findElements(alertBar);
				if (l.size() > 0) {
					clickElementJSWithWait(l.get(l.size() - 1));
					;
				}

				scrollDownForElementJSBy(hpCompanyNewsSeeMoreBtn);
				scrollUpByPixel(300);
				clickElementJSWithWait(hpCompanyNewsSeeMoreBtn);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, hpCompanyNewsHeader, 120);
				if (verifyObjectDisplayed(hpCompanyNewsHeader)) {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.PASS,
							"Navigated to Company News page");
				} else {
					tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
							"Failed to navigate to Company News page");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
						"Company News See More button not present on the homepage");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "homepageCompanyNews", Status.FAIL,
					"Company News Header not Present on the Homepage");
		}

	}

	public void footerBrands() {
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		if (verifyObjectDisplayed(footerBrandLogos)) {
			List<WebElement> brandsList = driver.findElements(footerBrandLogos);
			tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.PASS,
					"Wyndham Brand Logos Present in the Footer, Total: " + brandsList.size());

			// clickElementJS(brandsList.get(brandsList.size() - 1));
			scrollDownForElementJSBy(footerBrandLogos);
			// scrollDownByPixel(350);
			clickIOSElement("REWARDS", 1, brandsList.get(brandsList.size() - 1));
			waitForSometime(tcConfig.getConfig().get("LongWait"));

			/*
			 * ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
			 * driver.switchTo().window(tabs2.get(1)); if
			 * (browserName.equalsIgnoreCase("IE")) {
			 * waitForSometime(tcConfig.getConfig().get("MedWait")); }
			 */
			if (driver.getTitle().toUpperCase().contains("REWARDS")) {
				tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.PASS,
						"Wyndham Rewards Page Opened");
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				// driver.close();
				driver.navigate().back();
				waitForSometime(tcConfig.getConfig().get("MedWait"));

				// driver.switchTo().window(tabs2.get(0));

				waitUntilObjectVisible(driver, footer, 120);
				if (verifyObjectDisplayed(footer)) {
					tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.PASS,
							"Navigated Back to homepage");

				} else {
					tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.FAIL,
							"Homepage Navigation Failed");
				}

			} else {
				tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.FAIL,
						"Wyndham Rewards Page not Opened");
			}

		} else {
			tcConfig.updateTestReporter("NationsHomePage", "footerBrands", Status.FAIL,
					"Wyndham Brands Logo not present in footer");
		}
	}
}
