package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsUserProfilePage_IOS extends NationsUserProfilePage {

	public static final Logger log = Logger.getLogger(NationsUserProfilePage_IOS.class);

	public NationsUserProfilePage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		shareYourStoryCTA = By.xpath("//h1[text()='Your Shared Stories']");
		PageFactory.initElements(tcconfig.getDriver(), this);
		profileName = By.xpath(
				"//*[contains(@class,'wyn-nation-header__navigation__profile-mobile')]//*[@class='wyn-my-profile-link']");
	}

	public By authorProfile = By.xpath("//div[@class='wyn-author-profile__author__profile']");

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public String userProfileNav() {

		waitUntilElementVisibleBy(driver, profileName, 120);

		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.PASS, "User Name Visible");
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			clickElementBy(profileName);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			strName = getElementText(userName);
			waitForSometime(tcConfig.getConfig().get("MedWait"));
			if (verifyObjectDisplayed(userName)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.PASS,
						"Navigated to My profile Page");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.FAIL,
						"My Profile page navigation failed");
			}

		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.FAIL,
					"User Name not visible");
		}
		return strName;

	}

	public void userProfileValidations() {

		waitUntilElementVisibleBy(driver, userName, 120);

		if (strName.toUpperCase().contains(driver.findElement(userName).getText().toUpperCase())) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"User Name Title present as: " + getElementText(userName));
		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"User name Title not present and/or is incorrect");
		}

		if (verifyObjectDisplayed(profileImage)) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"Profile Image Space Present");
			if (verifyObjectDisplayed(imageSize)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Recommended Image Size present as: " + getElementText(imageSize));

			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
						"Recommended Image size text is not present");
			}

			if (verifyObjectDisplayed(editProfileImage)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Profile Image Editing Link present");

			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
						"Profile Image Editing Link not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"Profile Image Space not Present");
		}

		try {
			List<WebElement> userLocationUnitList = driver.findElements(userLocationUnit);
			String strLocation = userLocationUnitList.get(1).getText();
			String strBusinessUnit = userLocationUnitList.get(0).getText();

			if (userLocationUnitList.get(0).getText().equals("")) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Business Unit not present for current user");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Business Unit present as: " + strBusinessUnit);
			}

			if (userLocationUnitList.get(1).getText().equals("")) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Location not present for current user");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Location present as: " + strLocation);
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"Error in getting Business Unit or Location");
		}

		if (verifyObjectDisplayed(sharedStoriesHeader)) {

			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"Your Shared Stries Header Displayed");

			if (verifyObjectDisplayed(shareYourStoryCTA)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Share Your story CTA present");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
						"Share Your story CTA not present");
			}
		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"Your Shared Stries Header not Displayed");
		}

		try {
			List<WebElement> sharedStoriesList = driver.findElements(storiesLink);

			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"Number of stories Shared by user is: " + sharedStoriesList.size()
							+ ", also links to the stories are present");

			if (sharedStoriesList.size() == 0) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"No Stories Shared by this user, hence no links");
			} else {

				try {

					tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.PASS,
							"Card Title: " + sharedStoriesList.get(0).getText());
					String firstCardTitle = sharedStoriesList.get(0).getText().trim();

					sharedStoriesList.get(0).click();

					try {
						waitUntilElementVisibleBy(driver, pageContent, 120);
					} catch (Exception e) {
						System.out.println("");
					}

					waitForSometime(tcConfig.getConfig().get("MedWait"));

					if (driver.getTitle().contains(firstCardTitle)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.PASS,
								"Navigated to first shared story by the associate with title: " + firstCardTitle);
					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.FAIL,
								"Failed to navigate to first card related page");
					}

					driver.navigate().back();

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.FAIL,
							"Error in clicking first card");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"No Stories Shared by this user, hence no links");
		}

	}

	public void externalUserName() {

		try {
			List<WebElement> userNameList1 = driver.findElements(associateVoiceName);

			tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.PASS,
					"Associates Voices Names Present, Total: " + userNameList1.size());
			int tempProfile = userNameList1.size();
			int j = 0;
			for (int i = 0; i < tempProfile; i++) {
				List<WebElement> userNameList = driver.findElements(associateVoiceName);
				// List<MobileElement> mobileEle = ((MobileElement)
				// driver).findElements(associateVoiceName);
				strExternalUser = userNameList.get(i).getText().toUpperCase();
				if (i == 0) {
					strPrevUser = "$123#";
				}

				if (strExternalUser.contains(testData.get("strLoggedUser").toUpperCase())) {
					System.out.println("User same as logged in user" + strExternalUser);
					continue;
				} else if (strExternalUser.contains(strPrevUser)) {
					System.out.println("User same as previous user" + strExternalUser);
					continue;
				} else if (i == userNameList.size() - 1) {
					tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.PASS,
							"No Associate Voice Story Present other than logged user");
				} else {
					j = j + 1;

					tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.PASS,
							"Associates Voices story with user: " + strExternalUser);

					scrollDownForElementJSWb(userNameList.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(300);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					// clickElementJSWithWait(userNameList.get(i));
					/*
					 * @SuppressWarnings("rawtypes") TouchAction Action = new
					 * TouchAction((PerformsTouchActions) driver); Action.tap((TapOptions)
					 * userNameList.get(i)).perform();
					 */

					// new TouchAction((PerformsTouchActions) driver).tap(PointOption.point(90,
					// 304)).perform();
					// mobileEle.get(i).click();
					userNameList.get(i).click();
					// tapByCoordinates(userNameList.get(i));
					strPrevUser = strExternalUser;
					// tapByCoordinates(authorProfile);
					// driver.findElement(authorProfile).click();
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					externalUserProfileVal(strExternalUser);
					if (j == 3) {
						break;
					}
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.FAIL,
					"No Associate Voice Story Present");
		}
		// return strExternalUser;

	}

}
