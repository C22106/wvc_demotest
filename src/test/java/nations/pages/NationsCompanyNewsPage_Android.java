package nations.pages;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsCompanyNewsPage_Android extends NationsCompanyNewsPage {

	public static final Logger log = Logger.getLogger(NationsCompanyNewsPage_Android.class);

	public NationsCompanyNewsPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void companyNewsCardsCheck() {

		waitUntilElementVisibleBy(driver, companyNewsHeader, 120);

		if (verifyObjectDisplayed(companyNewsHeader)) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
					"Company News Header Present on the CompanyNewspage");

			List<WebElement> cnCardsList = driver.findElements(companyNewsCards);
			List<WebElement> cnCardImg = driver.findElements(companyNewsImg);
			List<WebElement> cnCardDate = driver.findElements(companyNewsDate);
			List<WebElement> cnCardTitle = driver.findElements(companyNewsTitle);
			/*List<WebElement> cnCardLikes = driver.findElements(companyNewsLikes);
			List<WebElement> cnCardComments = driver.findElements(companyNewsComments);*/

			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
					"Total Cards Present in the Company News: " + cnCardsList.size());
			scrollDownByPixel(400);

			waitForSometime(tcConfig.getConfig().get("LowWait"));
			if (cnCardsList.size() == cnCardImg.size()) {
				if (cnCardsList.size() == cnCardDate.size()) {
					if (cnCardsList.size() == cnCardTitle.size()) {
						/*if (cnCardsList.size() == cnCardLikes.size()) {
							if (cnCardsList.size() == cnCardComments.size()) {
								tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck",
										Status.PASS,
										"Images, Date, Title, Likes and Comments present for each card in Company News");
							} else {
								tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck",
										Status.FAIL, "Comments not present for all the Cards in Company News");
							}
						} else {
							tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
									"Likes not present for all the Cards in Company News");
						}*/
					} else {
						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
								"Title not present for all the Cards in Company News");
					}
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Date not present for all the Cards in Company News");
				}
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
						"Images not present for all the Cards in Company News");
			}

			try {

				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
						"Card Title: " + cnCardTitle.get(0).getText());
				String firstCardTitle = cnCardTitle.get(0).getText().trim();

				cnCardTitle.get(0).click();

				try {
					waitUntilElementVisibleBy(driver, pageContent, 120);
				} catch (Exception e) {
					System.out.println("");
				}

				waitForSometime(tcConfig.getConfig().get("MedWait"));

				if (driver.getTitle().contains(firstCardTitle)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"Navigated to first card related page in Company news");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Failed to navigate to first card related page");
				}

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, companyNewsHeader, 120);

				if (verifyObjectDisplayed(companyNewsHeader)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"Navigated back to Company News Page");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Failed to navigate to Company News Page");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
						"Error in clicking first card");
			}

			try {
				scrollDownForElementJSBy(pagination);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				if (verifyObjectDisplayed(pagination)) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"Pagination Present on the page");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Failed to navigate to Company News Page");
				}

				List<WebElement> paginationsList = driver.findElements(paginationNo);

				if (paginationsList.get(0).getAttribute("class").toLowerCase().contains("active")) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.PASS,
							"On first page by default");
				} else {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
							"Not On first page by default");
				}

				// clickIOSElement("2", 1, paginationsList.get(1));
				paginationsList.get(2).click();

				/*
				 * List<WebElement> paginationsList2 = driver.findElements(paginationNo); if
				 * (paginationsList2.get(1).getAttribute("class").toLowerCase().contains(
				 * "active")) { tcConfig.updateTestReporter("NationsCompanyNewsPage",
				 * "companyNewsCardsCheck", Status.PASS, "Second Page navigated"); } else {
				 * tcConfig.updateTestReporter("NationsCompanyNewsPage",
				 * "companyNewsCardsCheck", Status.FAIL, "Error in navigating second page"); }
				 */

			} catch (Exception e) {
				System.out.println("Pagination Not available");
			}

		} else {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsCheck", Status.FAIL,
					"Company News Header not Present on the CompanyNewspage");
		}

	}

	public void commentsAndReplyInCN() {

		scrollDownForElementJSBy(By.xpath("//h2[contains(.,'Comments')]"));
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		try {
			if (browserName.equalsIgnoreCase("edge")) {
				driver.switchTo().frame(0);
			} else {
				WebElement we = driver.findElement(iFrameSwitch);
				driver.switchTo().frame(we);
				// driver.switchTo().frame(driver.findElement(iFrameSwitch));
			}

			if (verifyObjectDisplayed(commentBox)) {

				driver.findElement(commentBox).click();
				driver.findElement(commentBox).sendKeys(testData.get("strComment"));

				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Comment Field Present, and Data entered as: " + testData.get("strComment"));

			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Comment Field not  Present");
			}

			driver.switchTo().defaultContent();
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			clickElementBy(postCmnt);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
					"Error in Commenting");
		}
		try {
			List<WebElement> commentList = driver.findElements(commentValidations);
			Boolean replyChck = false;
			for (int i = 0; i < commentList.size(); i++) {
				if (commentList.get(i).getText().contains(testData.get("strComment"))) {
					replyChck = true;
					break;
				} else {
					replyChck = false;
				}

			}

			if (replyChck = true) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Comment Posted By User is reflected in the page");
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Comment Posted By User is no reflected in the page");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
					"No Comments on the post");
		}
		try {
			List<WebElement> commentThreadList = driver.findElements(commntThread);
			List<WebElement> commntByList = driver.findElements(commntBy);
			List<WebElement> commentDateList = driver.findElements(commentDate);

			scrollDownForElementJSWb(commentDateList.get(0));
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(150);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			String strName = commntByList.get(0).getAttribute("outerHTML");
			strName = StringUtils.substringBetween(strName, "<div>", "<div>");

			if (verifyElementDisplayed(commntByList.get(0))) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"First Comment present and was posted by " + strName + " and on "
								+ commentDateList.get(0).getText());
			} else {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"First comment name not present");
			}

			try {
				scrollDownForElementJSBy(replyFirstComment);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(150);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
						"Reply option present in first comment");
				clickElementBy(replyFirstComment);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				List<WebElement> iFrameList = driver.findElements(iFrameSwitchReply);

				try {
					if (browserName.equalsIgnoreCase("edge")) {
						driver.switchTo().frame(1);
					} else {
						driver.switchTo().frame(iFrameList.get((iFrameList.size() - 1)));

					}

					List<WebElement> commentBoxList = driver.findElements(commentBox);

					if (verifyElementDisplayed(commentBoxList.get(commentBoxList.size() - 1))) {

						commentBoxList.get(commentBoxList.size() - 1).click();
						commentBoxList.get(commentBoxList.size() - 1).sendKeys(testData.get("strReply"));

						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.PASS,
								"Reply Comment Field Present, and Data entered as: " + testData.get("strReply"));

					} else {
						tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
								"Reply Comment Field not  Present");
					}

					driver.switchTo().defaultContent();
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					clickElementBy(postReply);
					waitForSometime(tcConfig.getConfig().get("LowWait"));
				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
							"Error in Replying");
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
						"Reply option not present on the first comment");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsCompanyNewsPage", "companyNewsCardsContent", Status.FAIL,
					"No Comments on the post");
		}

	}

}
