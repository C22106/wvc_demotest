package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsUserProfilePage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsUserProfilePage.class);

	public NationsUserProfilePage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By sharedStoriesHeader = By.xpath("//h1[contains(.,'Your')]");
	protected By exsharedStoriesHeader = By.xpath("//h1[contains(.,'Shared')]");
	protected By userName = By.xpath("//div[@class='column wyn-profile__info is-4']/h3");
	protected By storiesLink = By.xpath("//div[@class='wyn-card-content__container']");
	protected By sharedUser = By.xpath("//span[@class='wyn-type-body-3']");
	protected By myProfileTab = By.xpath("//div[@class='column wyn-type-body-2']/a[contains(.,'My profile')]");
	protected By profileName = By.xpath("//div[@class='wyn-nation-header__profile']//a");
	protected By profileImage = By.xpath("//div[@class='column wyn-profile__info is-4']/img");
	protected By editProfileImage = By.xpath("//span[contains(.,'Edit profile')]");
	protected By imageSize = By.xpath("//div[@class='column wyn-profile__info is-4']/label[@id='fileLabel']//span");
	protected By userLocationUnit = By.xpath("//div[@class='column wyn-profile__info is-4']/ul/li");
	protected By shareYourStoryCTA = By.xpath("//div[@class='wyn-header__search-icon']//a[contains(.,'Share')]");
	protected By pageContent = By.id("main-content");
	protected By associateVoiceName = By.xpath(
			"//h3[contains(.,'Associate Voice')]/ancestor::div[@id='ReactListingVoices']//div[@class='wyn-card-content__container']//span[@class='wyn-type-body-3']/a");
	protected By associateVoiceHeader = By.xpath("//h3[contains(.,'Associate Voice')]");
	protected String strName = null;
	protected String strExternalUser;
	protected String strPrevUser;

	public String userProfileNav() {

		waitUntilElementVisibleBy(driver, profileName, 120);

		if (verifyObjectDisplayed(profileName)) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.PASS, "User Name Visible");
			strName = getElementText(profileName);
			clickElementBy(profileName);
			waitUntilElementVisibleBy(driver, myProfileTab, 120);

			if (verifyObjectDisplayed(myProfileTab)) {

				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.PASS,
						"My Profile Tab Present");

				waitForSometime(tcConfig.getConfig().get("LowWait"));
				clickElementBy(myProfileTab);

				waitUntilElementVisibleBy(driver, userName, 120);

				if (verifyObjectDisplayed(userName)) {
					tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.PASS,
							"Navigated to My profile Page");
				} else {
					tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.FAIL,
							"My Profile page navigation failed");
				}

			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.FAIL,
						"My Profile Tab not Present");
			}

		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileNav", Status.FAIL,
					"User Name not visible");
		}
		return strName;

	}

	public void userProfileValidations() {

		waitUntilElementVisibleBy(driver, userName, 120);

		if (strName.toUpperCase().contains(driver.findElement(userName).getText().toUpperCase())) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"User Name Title present as: " + getElementText(userName));
		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"User name Title not present and/or is incorrect");
		}

		if (verifyObjectDisplayed(profileImage)) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"Profile Image Space Present");
			if (verifyObjectDisplayed(imageSize)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Recommended Image Size present as: " + getElementText(imageSize));

			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
						"Recommended Image size text is not present");
			}

			if (verifyObjectDisplayed(editProfileImage)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Profile Image Editing Link present");

			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
						"Profile Image Editing Link not present");
			}

		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"Profile Image Space not Present");
		}

		try {
			List<WebElement> userLocationUnitList = driver.findElements(userLocationUnit);
			String strLocation = userLocationUnitList.get(1).getText();
			String strBusinessUnit = userLocationUnitList.get(0).getText();

			if (userLocationUnitList.get(0).getText().equals("")) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Business Unit not present for current user");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Business Unit present as: " + strBusinessUnit);
			}

			if (userLocationUnitList.get(1).getText().equals("")) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Location not present for current user");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Location present as: " + strLocation);
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"Error in getting Business Unit or Location");
		}

		if (verifyObjectDisplayed(sharedStoriesHeader)) {

			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"Your Shared Stries Header Displayed");

			if (verifyObjectDisplayed(shareYourStoryCTA)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"Share Your story CTA present");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
						"Share Your story CTA not present");
			}
		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.FAIL,
					"Your Shared Stries Header not Displayed");
		}

		try {
			List<WebElement> sharedStoriesList = driver.findElements(storiesLink);

			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"Number of stories Shared by user is: " + sharedStoriesList.size()
							+ ", also links to the stories are present");

			if (sharedStoriesList.size() == 0) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
						"No Stories Shared by this user, hence no links");
			} else {

				try {

					tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.PASS,
							"Card Title: " + sharedStoriesList.get(0).getText());
					String firstCardTitle = sharedStoriesList.get(0).getText().trim();

					sharedStoriesList.get(0).click();

					try {
						waitUntilElementVisibleBy(driver, pageContent, 120);
					} catch (Exception e) {
						System.out.println("");
					}

					waitForSometime(tcConfig.getConfig().get("LongWait"));

					if (driver.getTitle().contains(firstCardTitle)) {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.PASS,
								"Navigated to first shared story by the associate with title: " + firstCardTitle);
					} else {
						tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.PASS,
								"Navigated to first card related page");
					}

					driver.navigate().back();

				} catch (Exception e) {
					tcConfig.updateTestReporter("NationsassociateVoicePage", "userProfileValidations", Status.FAIL,
							"Error in clicking first card");
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "userProfileValidations", Status.PASS,
					"No Stories Shared by this user, hence no links");
		}

	}

	public void loggedUserName() {
		try {
			List<WebElement> userNameList = driver.findElements(associateVoiceName);

			tcConfig.updateTestReporter("NationsAssociateVoicesPage", "loggedUserName", Status.PASS,
					"Associates Voices Names Present, Total: " + userNameList.size());

			for (int i = 0; i < userNameList.size(); i++) {
				String strUserName = userNameList.get(i).getText().toUpperCase();

				if (strUserName.contains(testData.get("strLoggedUser").toUpperCase())) {
					tcConfig.updateTestReporter("NationsAssociateVoicesPage", "loggedUserName", Status.PASS,
							"Associates Voices story with logged user present");

					scrollDownForElementJSWb(userNameList.get(i));
					waitForSometime(tcConfig.getConfig().get("MedWait"));
					scrollUpByPixel(300);
					waitForSometime(tcConfig.getConfig().get("MedWait"));

					userNameList.get(i).click();

					break;

				} else {
					if (i == userNameList.size() - 1) {
						tcConfig.updateTestReporter("NationsAssociateVoicesPage", "loggedUserName", Status.PASS,
								"No Associate Voice Story Present with logged user");

					}
				}

			}
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsAssociateVoicesPage", "loggedUserName", Status.FAIL,
					"No Associate Voice Story Present");
		}

	}

	public void externalUserName() {

		try {
			List<WebElement> userNameList1 = driver.findElements(associateVoiceName);

			tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.PASS,
					"Associates Voices Names Present, Total: " + userNameList1.size());
			int tempProfile = userNameList1.size();
			int j = 0;
			for (int i = 0; i < tempProfile; i++) {
				List<WebElement> userNameList = driver.findElements(associateVoiceName);
				// List<MobileElement> mobileEle = ((MobileElement)
				// driver).findElements(associateVoiceName);
				strExternalUser = userNameList.get(i).getText().toUpperCase();
				if (i == 0) {
					strPrevUser = "$123#";
				}

				if (strExternalUser.contains(testData.get("strLoggedUser").toUpperCase())) {
					System.out.println("User same as logged in user" + strExternalUser);
					continue;
				} else if (strExternalUser.contains(strPrevUser)) {
					System.out.println("User same as previous user" + strExternalUser);
					continue;
				} else if (i == userNameList.size() - 1) {
					tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.PASS,
							"No Associate Voice Story Present other than logged user");
				} else {
					j = j + 1;

					tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.PASS,
							"Associates Voices story with user: " + strExternalUser);

					scrollDownForElementJSWb(userNameList.get(i));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(300);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					// clickElementJSWithWait(userNameList.get(i));

					userNameList.get(i).click();
					// tapByCoordinates(userNameList.get(i));
					strPrevUser = strExternalUser;
					externalUserProfileVal(strExternalUser);
					if (j == 3) {
						break;
					}
				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsAssociateVoicesPage", "externalUserName", Status.FAIL,
					"No Associate Voice Story Present");
		}
		// return strExternalUser;

	}

	public void loggedUserProfileVal() {

		try {
			waitUntilElementVisibleBy(driver, userName, 20);
			String strUser = getElementText(userName);

			if (driver.findElement(userName).getText().toUpperCase()
					.contains(testData.get("strLoggedUser").toUpperCase())) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
						"User Name Title present as: " + getElementText(userName));
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.FAIL,
						"User name Title not present and/or is incorrect");
			}

			if (verifyObjectDisplayed(profileImage)) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
						"Profile Image Space Present");

				if (verifyObjectDisplayed(editProfileImage)) {
					tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
							"Profile Image Editing Link present");

				} else {
					tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.FAIL,
							"Profile Image Editing Link not present");
				}

			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.FAIL,
						"Profile Image Space not Present");
			}

			try {
				List<WebElement> userLocationUnitList = driver.findElements(userLocationUnit);
				String strLocation = userLocationUnitList.get(1).getText();
				String strBusinessUnit = userLocationUnitList.get(0).getText();

				if (userLocationUnitList.get(0).getText().equals("")) {
					tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
							"Business Unit not present for current user");
				} else {
					tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
							"Business Unit present as: " + strBusinessUnit);
				}

				if (userLocationUnitList.get(1).getText().equals("")) {
					tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
							"Location not present for current user");
				} else {
					tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
							"Location present as: " + strLocation);
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.FAIL,
						"Error in getting Business Unit or Location");
			}

			if (verifyObjectDisplayed(sharedStoriesHeader)) {

				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
						"Your Shared Stries Header Displayed");

			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.FAIL,
						"Your Shared Stries Header not Displayed");
			}
			try {
				List<WebElement> sharedStoriesList = driver.findElements(storiesLink);

				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
						"Number of stories Shared by user is: " + sharedStoriesList.size()
								+ ", also links to the stories are present");

				Boolean temp = false;
				for (int i = 0; i < sharedStoriesList.size(); i++) {

					if (strUser.trim().toUpperCase().contains(testData.get("strLoggedUser"))) {
						temp = true;

					} else {
						temp = false;
						break;
					}

				}

				if (temp = true) {
					tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
							"All the shared Stories displayed are of " + strUser);
				} else {
					tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.FAIL,
							"All the shared Stories displayed are not of " + strUser);
				}

			} catch (Exception e) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
						"No Stories Shared by this user, hence no links");
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "loggedUserProfileVal", Status.PASS,
					"External Profile Validation ");

		}

	}

	public void externalUserProfileVal(String strExternalUser) {

		waitUntilElementVisibleBy(driver, userName, 20);

		if (strExternalUser.toUpperCase().contains(driver.findElement(userName).getText().toUpperCase())) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
					"User Name Title present as: " + getElementText(userName));
		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.FAIL,
					"User name Title not present and/or is incorrect");
		}

		if (verifyObjectDisplayed(profileImage)) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
					"Profile Image Space Present");

		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.FAIL,
					"Profile Image Space not Present");
		}

		try {
			List<WebElement> userLocationUnitList = driver.findElements(userLocationUnit);
			String strLocation = userLocationUnitList.get(1).getText();
			String strBusinessUnit = userLocationUnitList.get(0).getText();

			if (userLocationUnitList.get(0).getText().equals("")) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
						"Business Unit not present for current user");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
						"Business Unit present as: " + strBusinessUnit);
			}

			if (userLocationUnitList.get(1).getText().equals("")) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
						"Location not present for current user");
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
						"Location present as: " + strLocation);
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.FAIL,
					"Error in getting Business Unit or Location");
		}

		if (verifyObjectDisplayed(exsharedStoriesHeader)) {

			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
					"User Shared Stries Header Displayed");

		} else {
			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.FAIL,
					"User Shared Stries Header not Displayed");
		}
		try {
			List<WebElement> sharedStoriesList = driver.findElements(storiesLink);

			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
					"Number of stories Shared by user is: " + sharedStoriesList.size()
							+ ", also links to the stories are present");

			Boolean temp = false;
			for (int i = 0; i < sharedStoriesList.size(); i++) {

				if (getElementText(sharedUser).trim().toUpperCase().contains(strExternalUser)) {
					temp = true;

				} else {
					temp = false;
					break;
				}

			}

			if (temp = true) {
				tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
						"All the shared Stories displayed are of " + strExternalUser);
			} else {
				tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.FAIL,
						"All the shared Stories displayed are not of " + strExternalUser);
			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsUserProfilePage", "externalUserProfileVal", Status.PASS,
					"No Stories Shared by this user, hence no links");
		}

		driver.navigate().back();
		waitUntilElementVisibleBy(driver, associateVoiceHeader, 120);

	}
}
