package nations.pages;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsSearchResultPage_Android extends NationsSearchResultPage {

	public static final Logger log = Logger.getLogger(NationsSearchResultPage_Android.class);

	public NationsSearchResultPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	public void searchResultsValidatins() {

		waitUntilElementVisibleBy(driver, searchResultsHeader, 120);
		if (verifyObjectDisplayed(searchResultsHeader)) {
			tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
					"Navigated to Search Results page");
			String searchURL = driver.getCurrentUrl();
			if (searchURL.toLowerCase().contains("search-results")) {
				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"Nations Search Result page long URL displayed.The url is : " + searchURL);
			}
			String strResultsCount = getElementText(searchResultCount);
			String strResultsFor = getElementText(searchResultsFor);

			if (strResultsCount.toUpperCase().contains("did not match any documents")) {
				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"No Results generated for searched keyword: " + strResultsFor);
			} else {

				String strResults = StringUtils.substringBetween(strResultsCount, "Results 1", "for");
				String[] strTotalResult = strResults.trim().split("of");
				strResults = strTotalResult[strTotalResult.length - 1];
				tcConfig.updateTestReporter("SearchResultPage", "searchFunctionality", Status.PASS,
						"Total Results: " + strResults + " for" + strResultsFor);

				List<WebElement> listOfResults = driver.findElements(searchResultsTitle);
				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"No Of Results on first page " + listOfResults.size());

				if (listOfResults.size() >= 10) {
					if (verifyObjectDisplayed(pagination)) {
						tcConfig.updateTestReporter("NationsHomePage", "searchResultsValidatins", Status.PASS,
								"Pagination Available on the page");
					} else {
						tcConfig.updateTestReporter("NationsHomePage", "searchResultsValidatins", Status.FAIL,
								"Pagination Not available on the page");
					}
				} else {
					System.out.println("Less than 10 results hence no pagination");
				}

				String firstTitle = listOfResults.get(0).getText();
				if (firstTitle.contains("-")) {
					firstTitle = firstTitle.replace("-", " ");
				} else {

				}

				tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
						"First Result title " + firstTitle);

				listOfResults.get(0).click();

				waitForSometime(tcConfig.getConfig().get("LongWait"));

				if (driver.getTitle().toUpperCase().contains(firstTitle.toUpperCase())) {
					tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.PASS,
							"Navigated to first Search related page " + firstTitle);
				} else {
					tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.FAIL,
							"Failed to Navigate to first Search related page " + firstTitle);
				}

				driver.navigate().back();

				waitUntilElementVisibleBy(driver, searchResultsHeader, 120);

			}

		} else {
			tcConfig.updateTestReporter("NationsSearchResultPage", "searchResultsValidatins", Status.FAIL,
					"Unable to navigate to search results page");
		}

	}

}
