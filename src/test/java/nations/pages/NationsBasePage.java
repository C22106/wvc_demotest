package nations.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.Status;

import automation.core.BrowserDriverManager;
import automation.core.FunctionalComponents;
import automation.core.TestConfig;

public class NationsBasePage extends FunctionalComponents {

	public NationsBasePage(TestConfig tcconfig) {
		super(tcconfig);
	}

	/*
	 * Method : checkAndInitBrowser Parameters :None Description : Initiate the
	 * Driver Browser with additional parameters Author : CTS Date : 2019 Change By
	 * : None
	 */

	public WebDriver checkAndInitBrowser(String strBrowser, String Platform_Name, String Platform_Version, String Model,
			String Browser_Name, String Browser_Version, String Location) {
		if (tcConfig.getDriver() == null) {
			BrowserDriverManager tb = new BrowserDriverManager();
			try {
				WebDriver driver = tb.initDriver(strBrowser, Platform_Name, Platform_Version, Model, Browser_Name,
						Browser_Version, Location);
				tcConfig.setDriver(driver);
			} catch (Exception e) {
				log.info("Unable to launch driver : Error -" + e.getMessage());

			}
		}

		return tcConfig.getDriver();

	}

	protected By hamburger = By.xpath("//button[contains(@class,'hamburger')]");

	public void openHamburgerMenu() {

		waitUntilObjectVisible(driver, hamburger, 120);
		if (verifyObjectDisplayed(hamburger)) {

			try {
				// clickIOSImage(imagePath, hamburger);
				driver.findElement(hamburger).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("HomePage_Mobile", "openHamburgerMenu", Status.PASS,
						"Hamburger is displayed");
			} catch (Exception ex) {
				tcConfig.updateTestReporter("HomePage_Mobile", "openHamburgerMenu", Status.FAIL,
						"Hamburger clicking is failed due to " + ex.getMessage());

			}

		} else {
			tcConfig.updateTestReporter("HomePage_Mobile", "openHamburgerMenu", Status.FAIL, "Hamburger not visible ");
		}
	}

	public void closeHamburgerMenu() {

		waitUntilObjectVisible(driver, hamburger, 120);
		if (verifyObjectDisplayed(hamburger)) {

			try {
				// clickIOSImage(imagePath, hamburger);
				driver.findElement(hamburger).click();
				waitForSometime(tcConfig.getConfig().get("MedWait"));
				tcConfig.updateTestReporter("HomePage_Mobile", "openHamburgerMenu", Status.PASS, "Hamburger is closed");
			} catch (Exception ex) {
				tcConfig.updateTestReporter("HomePage_Mobile", "openHamburgerMenu", Status.FAIL,
						"Hamburger closing is failed due to " + ex.getMessage());

			}

		} else {
			tcConfig.updateTestReporter("HomePage_Mobile", "openHamburgerMenu", Status.FAIL,
					"Hamburger close option not visible ");
		}
	}
}
