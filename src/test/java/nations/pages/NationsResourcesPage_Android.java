package nations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsResourcesPage_Android extends NationsResourcesPage {

	public static final Logger log = Logger.getLogger(NationsResourcesPage_Android.class);

	public NationsResourcesPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		compliancePolicies = By.xpath("//li[@class='has-submenu']//a[@title='Compliance & Policies']");
	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	public void complianceResNav() {
		if (verifyObjectDisplayed(resourcesTab)) {

			clickElementBy(resourcesTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, complianceResCTA, 120);
			if (verifyObjectDisplayed(complianceResCTA)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
						"Compliance Resource Tab present on the homepage");
				clickElementBy(complianceResCTA);

			} else if (verifyObjectDisplayed(compliancePolicies)) {

				clickElementBy(compliancePolicies);
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				waitUntilElementVisibleBy(driver, complianceResCTA, 120);
				if (verifyObjectDisplayed(complianceResCTA)) {
					tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
							"Compliance Resource Tab present on the homepage");
					clickElementBy(complianceResCTA);

				} else {
					tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
							"Compliance Resource Tab not present on the homepage");
				}

			}

			waitUntilElementVisibleBy(driver, complianceResHeader, 120);
			if (verifyObjectDisplayed(complianceResHeader)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
						"Navigated to Compliance Resource page, Header and Breadcrumb present");
			} else {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
						"Navigation to Compliance Resources page failed, Header not present");
			}
		} else if (verifyObjectDisplayed(compliancePolicies)) {

			clickElementJSWithWait(compliancePolicies);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, complianceResCTA, 120);
			if (verifyObjectDisplayed(complianceResCTA)) {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.PASS,
						"Compliance Resource Tab present on the homepage");
				clickElementJSWithWait(complianceResCTA);

			} else {
				tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
						"Compliance Resource Tab not present on the homepage");
			}
		} else {
			tcConfig.updateTestReporter("NationsResourcesPage", "complianceResNav", Status.FAIL,
					"Compliance Resource Tab not present on the homepage");
		}

	}
}
