package nations.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class NationsDestinationUPage extends NationsBasePage {

	public static final Logger log = Logger.getLogger(NationsDestinationUPage.class);

	public NationsDestinationUPage(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

	/**
	 * 
	 * 
	 * @param userid
	 * @param password
	 * @return
	 * @throws Exception
	 */

	protected By learningDevTab = By.xpath("//li[@class='has-submenu']//a[@title='Learning & Career Development']");
	protected By destinationUTab = By.xpath("//li[@class='has-submenu is-active']//a[contains(.,'Destination U')]");
	protected By destinationUHeader = By.xpath("//h1[contains(.,'Destination U')]");
	protected By destinationUHeaderProd = By.xpath("//div[@class='wyn-banner__copy']/div[contains(.,'Destination U')]");

	protected By startYourRoleCarousel = By
			.xpath("//div[@class='wyn-carousel__item']//h3[contains(.,'Start Your Role')]");
	protected By growYourExpertiseCarousel = By
			.xpath("//div[@class='wyn-carousel__item']//h3[contains(.,'Grow Your Expertise')]");
	protected By devlopYourPotentialCarousel = By
			.xpath("//div[@class='wyn-carousel__item']//h3[contains(.,'Develop Your Potential')]");

	protected By destinationUBreadcrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Destination')]");

	protected By startYourRoleBreadcrumb = By.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Start Your')]");
	protected By growYourExpertiseBreadcrumb = By
			.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Grow Your Expertise')]");
	protected By devlopYourPotentialBreadcrumb = By
			.xpath("//nav[@class='wyn-breadcrumbs']//a[contains(.,'Develop Your Potential')]");

	protected By tabContainer = By.xpath("//div[@class='wyn-sliding-tabs-multiple__container']");
	protected By slidingTabs = By.xpath("//div[@class='wyn-sliding-tabs-multiple__container']//a");
	protected By nextButton = By.xpath("//button[@aria-label='Next']");
	protected By activeCards = By.xpath("//div[@class='wyn-sliding-tabs-multiple__container']//a/parent::div");
	protected By leadHeaders = By.xpath("//div[@class='wyn-tabs__content is-active']//div[@class='cards']//h2");

	String totalCard = "//div[@class='wyn-tabs__content is-active']//div[@class='cards'][";
	String leadHeader = "]//h2";
	String leadCards = "]//h3";

	public void destinationUPageNav() {

		waitUntilElementVisibleBy(driver, learningDevTab, 120);

		if (verifyObjectDisplayed(learningDevTab)) {

			clickElementBy(learningDevTab);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			waitUntilElementVisibleBy(driver, destinationUTab, 120);
			if (verifyObjectDisplayed(destinationUTab)) {
				tcConfig.updateTestReporter("NationsDestinationUPage", "destinationUPageNav", Status.PASS,
						"Destination U tab present on the homepage");
				clickElementJSWithWait(destinationUTab);

			} else {
				tcConfig.updateTestReporter("NationsDestinationUPage", "destinationUPageNav", Status.FAIL,
						"Destination U tab not present on the homepage");
			}

			waitForSometime(tcConfig.getConfig().get("MedWait"));

			if (verifyObjectDisplayed(destinationUHeader) || verifyObjectDisplayed(destinationUHeaderProd)) {
				tcConfig.updateTestReporter("NationsDestinationUPage", "destinationUPageNav", Status.PASS,
						"Navigated to Destination U page, Header present");
			} else {
				tcConfig.updateTestReporter("NationsDestinationUPage", "destinationUPageNav", Status.FAIL,
						"Navigation to Destination U page failed, Header not present");
			}
		} else {
			tcConfig.updateTestReporter("NationsDestinationUPage", "destinationUPageNav", Status.FAIL,
					"Learning Info tab not present on the homepage");
		}

	}

	public void verifyDestinationUTabs() {
		scrollDownForElementJSBy(startYourRoleCarousel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(150);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(startYourRoleCarousel)) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "verifyDestinationUTabs", Status.PASS,
					"Start Your Role tab present");
		} else {
			tcConfig.updateTestReporter("NationsDestinationUPage", "verifyDestinationUTabs", Status.FAIL,
					"Start Your Role tab not present");
		}

		if (verifyObjectDisplayed(growYourExpertiseCarousel)) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "verifyDestinationUTabs", Status.PASS,
					"Grow Your Expertise tab present");
		} else {
			tcConfig.updateTestReporter("NationsDestinationUPage", "verifyDestinationUTabs", Status.FAIL,
					"Grow Your Expertisetab not present");
		}

		if (verifyObjectDisplayed(devlopYourPotentialCarousel)) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "verifyDestinationUTabs", Status.PASS,
					"Devlop your Potential tab present");
		} else {
			tcConfig.updateTestReporter("NationsDestinationUPage", "verifyDestinationUTabs", Status.FAIL,
					"Devlop your Potential tab not present");
		}

	}

	public void startYourCareerNav() {

		scrollDownForElementJSBy(startYourRoleCarousel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(300);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(startYourRoleCarousel)) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "startYourCareerNav", Status.PASS,
					"Start Your Role tab present");

			clickElementBy(startYourRoleCarousel);

			waitUntilElementVisibleBy(driver, startYourRoleBreadcrumb, 120);
			if (verifyObjectDisplayed(startYourRoleBreadcrumb)) {
				tcConfig.updateTestReporter("NationsDestinationUPage", "startYourCareerNav", Status.PASS,
						"Start Your Role page Navigated");
			} else {
				tcConfig.updateTestReporter("NationsDestinationUPage", "startYourCareerNav", Status.FAIL,
						"Start Your Role Page Navigation Failed");
			}

			leadHeaderAndCardsCheck();

		} else {
			tcConfig.updateTestReporter("NationsDestinationUPage", "startYourCareerNav", Status.FAIL,
					"Start Your Role tab not present");
		}

		driver.switchTo().activeElement().sendKeys(Keys.HOME);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void devlopYourPotentialNav() {

		scrollDownForElementJSBy(devlopYourPotentialCarousel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(300);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(devlopYourPotentialCarousel)) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "devlopYourPotentialNav", Status.PASS,
					"Devlop Your Potential tab present");

			clickElementBy(devlopYourPotentialCarousel);

			waitUntilElementVisibleBy(driver, devlopYourPotentialBreadcrumb, 120);
			if (verifyObjectDisplayed(devlopYourPotentialBreadcrumb)) {
				tcConfig.updateTestReporter("NationsDestinationUPage", "devlopYourPotentialNav", Status.PASS,
						"Devlop Your Potential page Navigated");
			} else {
				tcConfig.updateTestReporter("NationsDestinationUPage", "devlopYourPotentialNav", Status.FAIL,
						"Devlop Your Potential Page Navigation Failed");
			}

			leadHeaderAndCardsCheck();

		} else {
			tcConfig.updateTestReporter("NationsDestinationUPage", "devlopYourPotentialNav", Status.FAIL,
					"Devlop Your Potential tab not present");
		}

		driver.switchTo().activeElement().sendKeys(Keys.HOME);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
	}

	public void growYourExpertiseNav() {

		scrollDownForElementJSBy(growYourExpertiseCarousel);
		waitForSometime(tcConfig.getConfig().get("LowWait"));
		scrollUpByPixel(300);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

		if (verifyObjectDisplayed(growYourExpertiseCarousel)) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "growYourPotentialNav", Status.PASS,
					"Grow Your Expertise tab present");

			clickElementBy(growYourExpertiseCarousel);

			waitUntilElementVisibleBy(driver, growYourExpertiseBreadcrumb, 120);
			if (verifyObjectDisplayed(growYourExpertiseBreadcrumb)) {
				tcConfig.updateTestReporter("NationsDestinationUPage", "growYourPotentialNav", Status.PASS,
						"Grow Your Expertise page Navigated");
			} else {
				tcConfig.updateTestReporter("NationsDestinationUPage", "growYourPotentialNav", Status.FAIL,
						"Grow Your Expertise Page Navigation Failed");
			}

			leadHeaderAndCardsCheck();

		} else {
			tcConfig.updateTestReporter("NationsDestinationUPage", "growYourPotentialNav", Status.FAIL,
					"Grow Your Expertise tab not present");
		}

		driver.switchTo().activeElement().sendKeys(Keys.HOME);
		waitForSometime(tcConfig.getConfig().get("LowWait"));

	}

	public void leadHeaderAndCardsCheck() {
		try {
			waitUntilElementVisibleBy(driver, tabContainer, 120);

			scrollDownForElementJSBy(tabContainer);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(300);
			waitForSometime(tcConfig.getConfig().get("LowWait"));

			tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
					"Sliding Tabs Container Present in the page");
		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.FAIL,
					"Sliding Tabs Container not Present in the page");
		}

		try {
			List<WebElement> slidingTabsList = driver.findElements(slidingTabs);
			tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
					"Total Number Of Sliding Tabs is: " + slidingTabsList.size());
			for (int i = 0; i < slidingTabsList.size(); i++) {

				scrollDownForElementJSWb(slidingTabsList.get(0));
				waitForSometime(tcConfig.getConfig().get("LowWait"));
				scrollUpByPixel(300);
				waitForSometime(tcConfig.getConfig().get("LowWait"));

				if (i > 0 && i < 3) {
					slidingTabsList.get(i).click();
				} else if (i >= 3) {
					if (verifyObjectDisplayed(nextButton)) {
						clickElementBy(nextButton);
						// waitForSometime(tcConfig.getConfig().get("LowWait"));
						tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.FAIL,
								"Next Button present in the page");
					} else {
						tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
								"Next Button not present in the page");
					}
					slidingTabsList.get(i).click();
				}

				tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
						" Tab No " + (i + 1) + "is: " + slidingTabsList.get(i).getText());

				List<WebElement> slidingTabsActive = driver.findElements(activeCards);

				if (slidingTabsActive.get(i).getAttribute("class").contains("active")) {
					tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
							(i + 1) + " Sliding Tabs is active");
				} else {
					tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.FAIL,
							(i + 1) + " Sliding Tabs is not active or clickable");
				}

				List<WebElement> leadHeaderList = driver.findElements(leadHeaders);

				tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
						"Total Number Of Lead Headers is: " + leadHeaderList.size());

				for (int j = 0; j < leadHeaderList.size(); j++) {
					List<WebElement> leadCardsList = driver.findElements(By.xpath(totalCard + (j + 1) + leadCards));
					scrollDownForElementJSWb(leadHeaderList.get(j));
					waitForSometime(tcConfig.getConfig().get("LowWait"));
					scrollUpByPixel(300);
					waitForSometime(tcConfig.getConfig().get("LowWait"));

					tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
							" Lead Header No " + (j + 1) + " is: " + leadHeaderList.get(j).getText());
					if (leadCardsList.size() > 0) {
						tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.PASS,
								"Total Number Of Lead Cards in: " + leadHeaderList.get(j).getText() + " is: "
										+ leadCardsList.size());
					} else {
						tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.FAIL,
								"There are No Cards in: " + leadHeaderList.get(j).getText());
					}

				}

			}

		} catch (Exception e) {
			tcConfig.updateTestReporter("NationsDestinationUPage", "leadHeaderAndCardsCheck", Status.FAIL,
					"Error in Getting Sliding tabs and Cards");
		}

	}

}
