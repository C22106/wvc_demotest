package destinations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import automation.core.TestConfig;

public class SocialResponsibilitiesPage_Android extends SocialResponsibilitiesPage {

	public static final Logger log = Logger.getLogger(SocialResponsibilitiesPage_Android.class);

	public SocialResponsibilitiesPage_Android(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);
		msgFromMichealAB = By.xpath("//a[contains(.,'Message')]");

	}

	public void messageFromMichaelValidations() {
		openHamburgerMenu();
		if (verifyObjectDisplayed(msgFromMichealAB)) {
			scrollDownForElementJSBy(msgFromMichealAB);
			waitForSometime(tcConfig.getConfig().get("LowWait"));
			scrollUpByPixel(100);

			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations", Status.PASS,
					"Message From Michael Action Block and Header Present");
			if (verifyObjectDisplayed(msgMichealABImage)) {
				// if (verifyObjectDisplayed(msgMichealReadMore)) {
				if (verifyObjectDisplayed(msgMichealdescription)) {

					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
							Status.PASS,
							"Message From Michael Action Block Image, Description and ReadMore CTA present");

					clickElementBy(msgFromMichealAB);

					waitUntilElementVisibleBy(driver, msgMichealHeader, 120);

					if (verifyObjectDisplayed(msgMichealHeader)) {
						if (verifyObjectDisplayed(wynBanners)) {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
									Status.PASS, "Navigated to Message from Michael page and Banner present");
						} else {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
									Status.FAIL, "Banner not present");
						}
						if (verifyObjectDisplayed(msgMichealProfileBlock)) {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
									Status.PASS, "Message From Michael Profile Block content present");
						} else {
							tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
									Status.FAIL, "Message From Michael Profile Block content not present");
						}

					} else {
						tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
								Status.FAIL, "Navigation to Msg from Michael unsuccessful");
					}

				} else {
					tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations",
							Status.FAIL, "Message From Michael Description not present");
				}

			} else {
				tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations", Status.FAIL,
						"Message From Michael Image not present");
			}

		} else {
			// updated as of 1024, not present in the page
			tcConfig.updateTestReporter("SocialResponsibilitiesPage", "messageFromMichaelValidations", Status.PASS,
					"Message From Michael Action Block and Header not Present");
		}

	}

}