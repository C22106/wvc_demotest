package destinations.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import automation.core.TestConfig;

public class SearchResultPage_IOS extends SearchResultPage {

	public static final Logger log = Logger.getLogger(SearchResultPage_IOS.class);

	public SearchResultPage_IOS(TestConfig tcconfig) {
		super(tcconfig);
		PageFactory.initElements(tcconfig.getDriver(), this);

	}

}
